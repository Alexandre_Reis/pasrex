/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:47
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTaxaGlobalCollection : esEntityCollection
	{
		public esTaxaGlobalCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TaxaGlobalCollection";
		}

		#region Query Logic
		protected void InitQuery(esTaxaGlobalQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTaxaGlobalQuery);
		}
		#endregion
		
		virtual public TaxaGlobal DetachEntity(TaxaGlobal entity)
		{
			return base.DetachEntity(entity) as TaxaGlobal;
		}
		
		virtual public TaxaGlobal AttachEntity(TaxaGlobal entity)
		{
			return base.AttachEntity(entity) as TaxaGlobal;
		}
		
		virtual public void Combine(TaxaGlobalCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TaxaGlobal this[int index]
		{
			get
			{
				return base[index] as TaxaGlobal;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TaxaGlobal);
		}
	}



	[Serializable]
	abstract public class esTaxaGlobal : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTaxaGlobalQuery GetDynamicQuery()
		{
			return null;
		}

		public esTaxaGlobal()
		{

		}

		public esTaxaGlobal(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTaxaGlobal)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTaxaGlobal);
			else
				return LoadByPrimaryKeyStoredProcedure(idTaxaGlobal);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTaxaGlobal)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTaxaGlobal);
			else
				return LoadByPrimaryKeyStoredProcedure(idTaxaGlobal);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTaxaGlobal)
		{
			esTaxaGlobalQuery query = this.GetDynamicQuery();
			query.Where(query.IdTaxaGlobal == idTaxaGlobal);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTaxaGlobal)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTaxaGlobal",idTaxaGlobal);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTaxaGlobal": this.str.IdTaxaGlobal = (string)value; break;							
						case "IdTabelaGlobal": this.str.IdTabelaGlobal = (string)value; break;							
						case "IdTabelaAssociada": this.str.IdTabelaAssociada = (string)value; break;							
						case "Prioridade": this.str.Prioridade = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTaxaGlobal":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTaxaGlobal = (System.Int32?)value;
							break;
						
						case "IdTabelaGlobal":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabelaGlobal = (System.Int32?)value;
							break;
						
						case "IdTabelaAssociada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabelaAssociada = (System.Int32?)value;
							break;
						
						case "Prioridade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Prioridade = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TaxaGlobal.IdTaxaGlobal
		/// </summary>
		virtual public System.Int32? IdTaxaGlobal
		{
			get
			{
				return base.GetSystemInt32(TaxaGlobalMetadata.ColumnNames.IdTaxaGlobal);
			}
			
			set
			{
				base.SetSystemInt32(TaxaGlobalMetadata.ColumnNames.IdTaxaGlobal, value);
			}
		}
		
		/// <summary>
		/// Maps to TaxaGlobal.IdTabelaGlobal
		/// </summary>
		virtual public System.Int32? IdTabelaGlobal
		{
			get
			{
				return base.GetSystemInt32(TaxaGlobalMetadata.ColumnNames.IdTabelaGlobal);
			}
			
			set
			{
				if(base.SetSystemInt32(TaxaGlobalMetadata.ColumnNames.IdTabelaGlobal, value))
				{
					this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TaxaGlobal.IdTabelaAssociada
		/// </summary>
		virtual public System.Int32? IdTabelaAssociada
		{
			get
			{
				return base.GetSystemInt32(TaxaGlobalMetadata.ColumnNames.IdTabelaAssociada);
			}
			
			set
			{
				if(base.SetSystemInt32(TaxaGlobalMetadata.ColumnNames.IdTabelaAssociada, value))
				{
					this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TaxaGlobal.Prioridade
		/// </summary>
		virtual public System.Int32? Prioridade
		{
			get
			{
				return base.GetSystemInt32(TaxaGlobalMetadata.ColumnNames.Prioridade);
			}
			
			set
			{
				base.SetSystemInt32(TaxaGlobalMetadata.ColumnNames.Prioridade, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TabelaTaxaAdministracao _UpToTabelaTaxaAdministracaoByIdTabelaGlobal;
		[CLSCompliant(false)]
		internal protected TabelaTaxaAdministracao _UpToTabelaTaxaAdministracaoByIdTabelaAssociada;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTaxaGlobal entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTaxaGlobal
			{
				get
				{
					System.Int32? data = entity.IdTaxaGlobal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTaxaGlobal = null;
					else entity.IdTaxaGlobal = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTabelaGlobal
			{
				get
				{
					System.Int32? data = entity.IdTabelaGlobal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabelaGlobal = null;
					else entity.IdTabelaGlobal = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTabelaAssociada
			{
				get
				{
					System.Int32? data = entity.IdTabelaAssociada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabelaAssociada = null;
					else entity.IdTabelaAssociada = Convert.ToInt32(value);
				}
			}
				
			public System.String Prioridade
			{
				get
				{
					System.Int32? data = entity.Prioridade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Prioridade = null;
					else entity.Prioridade = Convert.ToInt32(value);
				}
			}
			

			private esTaxaGlobal entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTaxaGlobalQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTaxaGlobal can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TaxaGlobal : esTaxaGlobal
	{

				
		#region UpToTabelaTaxaAdministracaoByIdTabelaGlobal - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TaxaGlobal_TabelaTaxaAdministracao_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracao UpToTabelaTaxaAdministracaoByIdTabelaGlobal
		{
			get
			{
				if(this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal == null
					&& IdTabelaGlobal != null					)
				{
					this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal = new TabelaTaxaAdministracao();
					this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabelaGlobal", this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal);
					this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal.Query.Where(this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal.Query.IdTabela == this.IdTabelaGlobal);
					this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal.Query.Load();
				}

				return this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal;
			}
			
			set
			{
				this.RemovePreSave("UpToTabelaTaxaAdministracaoByIdTabelaGlobal");
				

				if(value == null)
				{
					this.IdTabelaGlobal = null;
					this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal = null;
				}
				else
				{
					this.IdTabelaGlobal = value.IdTabela;
					this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal = value;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabelaGlobal", this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTabelaTaxaAdministracaoByIdTabelaAssociada - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TaxaGlobal_TabelaTaxaAdministracao_FK2
		/// </summary>

		[XmlIgnore]
		public TabelaTaxaAdministracao UpToTabelaTaxaAdministracaoByIdTabelaAssociada
		{
			get
			{
				if(this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada == null
					&& IdTabelaAssociada != null					)
				{
					this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada = new TabelaTaxaAdministracao();
					this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabelaAssociada", this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada);
					this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada.Query.Where(this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada.Query.IdTabela == this.IdTabelaAssociada);
					this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada.Query.Load();
				}

				return this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada;
			}
			
			set
			{
				this.RemovePreSave("UpToTabelaTaxaAdministracaoByIdTabelaAssociada");
				

				if(value == null)
				{
					this.IdTabelaAssociada = null;
					this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada = null;
				}
				else
				{
					this.IdTabelaAssociada = value.IdTabela;
					this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada = value;
					this.SetPreSave("UpToTabelaTaxaAdministracaoByIdTabelaAssociada", this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal != null)
			{
				this.IdTabelaGlobal = this._UpToTabelaTaxaAdministracaoByIdTabelaGlobal.IdTabela;
			}
			if(!this.es.IsDeleted && this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada != null)
			{
				this.IdTabelaAssociada = this._UpToTabelaTaxaAdministracaoByIdTabelaAssociada.IdTabela;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTaxaGlobalQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TaxaGlobalMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTaxaGlobal
		{
			get
			{
				return new esQueryItem(this, TaxaGlobalMetadata.ColumnNames.IdTaxaGlobal, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTabelaGlobal
		{
			get
			{
				return new esQueryItem(this, TaxaGlobalMetadata.ColumnNames.IdTabelaGlobal, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTabelaAssociada
		{
			get
			{
				return new esQueryItem(this, TaxaGlobalMetadata.ColumnNames.IdTabelaAssociada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Prioridade
		{
			get
			{
				return new esQueryItem(this, TaxaGlobalMetadata.ColumnNames.Prioridade, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TaxaGlobalCollection")]
	public partial class TaxaGlobalCollection : esTaxaGlobalCollection, IEnumerable<TaxaGlobal>
	{
		public TaxaGlobalCollection()
		{

		}
		
		public static implicit operator List<TaxaGlobal>(TaxaGlobalCollection coll)
		{
			List<TaxaGlobal> list = new List<TaxaGlobal>();
			
			foreach (TaxaGlobal emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TaxaGlobalMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TaxaGlobalQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TaxaGlobal(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TaxaGlobal();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TaxaGlobalQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TaxaGlobalQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TaxaGlobalQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TaxaGlobal AddNew()
		{
			TaxaGlobal entity = base.AddNewEntity() as TaxaGlobal;
			
			return entity;
		}

		public TaxaGlobal FindByPrimaryKey(System.Int32 idTaxaGlobal)
		{
			return base.FindByPrimaryKey(idTaxaGlobal) as TaxaGlobal;
		}


		#region IEnumerable<TaxaGlobal> Members

		IEnumerator<TaxaGlobal> IEnumerable<TaxaGlobal>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TaxaGlobal;
			}
		}

		#endregion
		
		private TaxaGlobalQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TaxaGlobal' table
	/// </summary>

	[Serializable]
	public partial class TaxaGlobal : esTaxaGlobal
	{
		public TaxaGlobal()
		{

		}
	
		public TaxaGlobal(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TaxaGlobalMetadata.Meta();
			}
		}
		
		
		
		override protected esTaxaGlobalQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TaxaGlobalQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TaxaGlobalQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TaxaGlobalQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TaxaGlobalQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TaxaGlobalQuery query;
	}



	[Serializable]
	public partial class TaxaGlobalQuery : esTaxaGlobalQuery
	{
		public TaxaGlobalQuery()
		{

		}		
		
		public TaxaGlobalQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TaxaGlobalMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TaxaGlobalMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TaxaGlobalMetadata.ColumnNames.IdTaxaGlobal, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TaxaGlobalMetadata.PropertyNames.IdTaxaGlobal;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaGlobalMetadata.ColumnNames.IdTabelaGlobal, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TaxaGlobalMetadata.PropertyNames.IdTabelaGlobal;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaGlobalMetadata.ColumnNames.IdTabelaAssociada, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TaxaGlobalMetadata.PropertyNames.IdTabelaAssociada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaGlobalMetadata.ColumnNames.Prioridade, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TaxaGlobalMetadata.PropertyNames.Prioridade;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TaxaGlobalMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTaxaGlobal = "IdTaxaGlobal";
			 public const string IdTabelaGlobal = "IdTabelaGlobal";
			 public const string IdTabelaAssociada = "IdTabelaAssociada";
			 public const string Prioridade = "Prioridade";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTaxaGlobal = "IdTaxaGlobal";
			 public const string IdTabelaGlobal = "IdTabelaGlobal";
			 public const string IdTabelaAssociada = "IdTabelaAssociada";
			 public const string Prioridade = "Prioridade";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TaxaGlobalMetadata))
			{
				if(TaxaGlobalMetadata.mapDelegates == null)
				{
					TaxaGlobalMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TaxaGlobalMetadata.meta == null)
				{
					TaxaGlobalMetadata.meta = new TaxaGlobalMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTaxaGlobal", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTabelaGlobal", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTabelaAssociada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Prioridade", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TaxaGlobal";
				meta.Destination = "TaxaGlobal";
				
				meta.spInsert = "proc_TaxaGlobalInsert";				
				meta.spUpdate = "proc_TaxaGlobalUpdate";		
				meta.spDelete = "proc_TaxaGlobalDelete";
				meta.spLoadAll = "proc_TaxaGlobalLoadAll";
				meta.spLoadByPrimaryKey = "proc_TaxaGlobalLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TaxaGlobalMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
