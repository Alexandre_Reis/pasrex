/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:23 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				

using Financial.Common;
using Financial.Contabil;
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esTabelaTaxaPerformanceCollection : esEntityCollection
	{
		public esTabelaTaxaPerformanceCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaTaxaPerformanceCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaTaxaPerformanceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaTaxaPerformanceQuery);
		}
		#endregion
		
		virtual public TabelaTaxaPerformance DetachEntity(TabelaTaxaPerformance entity)
		{
			return base.DetachEntity(entity) as TabelaTaxaPerformance;
		}
		
		virtual public TabelaTaxaPerformance AttachEntity(TabelaTaxaPerformance entity)
		{
			return base.AttachEntity(entity) as TabelaTaxaPerformance;
		}
		
		virtual public void Combine(TabelaTaxaPerformanceCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaTaxaPerformance this[int index]
		{
			get
			{
				return base[index] as TabelaTaxaPerformance;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaTaxaPerformance);
		}
	}



	[Serializable]
	abstract public class esTabelaTaxaPerformance : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaTaxaPerformanceQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaTaxaPerformance()
		{

		}

		public esTabelaTaxaPerformance(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaTaxaPerformanceQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esTabelaTaxaPerformanceQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "TipoCalculo": this.str.TipoCalculo = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "PercentualIndice": this.str.PercentualIndice = (string)value; break;							
						case "TaxaJuros": this.str.TaxaJuros = (string)value; break;							
						case "TipoApropriacaoJuros": this.str.TipoApropriacaoJuros = (string)value; break;							
						case "ContagemDiasJuros": this.str.ContagemDiasJuros = (string)value; break;							
						case "BaseAnoJuros": this.str.BaseAnoJuros = (string)value; break;							
						case "TaxaPerformance": this.str.TaxaPerformance = (string)value; break;							
						case "BaseApuracao": this.str.BaseApuracao = (string)value; break;							
						case "NumeroDiasPagamento": this.str.NumeroDiasPagamento = (string)value; break;							
						case "DiaRenovacao": this.str.DiaRenovacao = (string)value; break;							
						case "NumeroMesesRenovacao": this.str.NumeroMesesRenovacao = (string)value; break;							
						case "ImpactaPL": this.str.ImpactaPL = (string)value; break;							
						case "CalculaBenchmarkNegativo": this.str.CalculaBenchmarkNegativo = (string)value; break;							
						case "ZeraPerformanceNegativa": this.str.ZeraPerformanceNegativa = (string)value; break;							
						case "CalculaPerformanceResgate": this.str.CalculaPerformanceResgate = (string)value; break;							
						case "NumeroDiasPagamentoResgate": this.str.NumeroDiasPagamentoResgate = (string)value; break;							
						case "TipoCalculoResgate": this.str.TipoCalculoResgate = (string)value; break;							
						case "DataFim": this.str.DataFim = (string)value; break;							
						case "IdEventoProvisao": this.str.IdEventoProvisao = (string)value; break;							
						case "IdEventoPagamento": this.str.IdEventoPagamento = (string)value; break;							
						case "DataFimApropriacao": this.str.DataFimApropriacao = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "DataUltimoCortePfee": this.str.DataUltimoCortePfee = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "TipoCalculo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCalculo = (System.Byte?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "PercentualIndice":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualIndice = (System.Decimal?)value;
							break;
						
						case "TaxaJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaJuros = (System.Decimal?)value;
							break;
						
						case "TipoApropriacaoJuros":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoApropriacaoJuros = (System.Byte?)value;
							break;
						
						case "ContagemDiasJuros":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDiasJuros = (System.Byte?)value;
							break;
						
						case "BaseAnoJuros":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.BaseAnoJuros = (System.Int16?)value;
							break;
						
						case "TaxaPerformance":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaPerformance = (System.Decimal?)value;
							break;
						
						case "BaseApuracao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.BaseApuracao = (System.Byte?)value;
							break;
						
						case "NumeroDiasPagamento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.NumeroDiasPagamento = (System.Byte?)value;
							break;
						
						case "DiaRenovacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.DiaRenovacao = (System.Byte?)value;
							break;
						
						case "NumeroMesesRenovacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.NumeroMesesRenovacao = (System.Byte?)value;
							break;
						
						case "CalculaBenchmarkNegativo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.CalculaBenchmarkNegativo = (System.Byte?)value;
							break;
						
						case "NumeroDiasPagamentoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.NumeroDiasPagamentoResgate = (System.Byte?)value;
							break;
						
						case "TipoCalculoResgate":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCalculoResgate = (System.Byte?)value;
							break;
						
						case "DataFim":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFim = (System.DateTime?)value;
							break;
						
						case "IdEventoProvisao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoProvisao = (System.Int32?)value;
							break;
						
						case "IdEventoPagamento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEventoPagamento = (System.Int32?)value;
							break;
						
						case "DataFimApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimApropriacao = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "DataUltimoCortePfee":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataUltimoCortePfee = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaPerformanceMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(TabelaTaxaPerformanceMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaPerformanceMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				base.SetSystemInt32(TabelaTaxaPerformanceMetadata.ColumnNames.IdCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.TipoCalculo
		/// </summary>
		virtual public System.Byte? TipoCalculo
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.TipoCalculo);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.TipoCalculo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(TabelaTaxaPerformanceMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(TabelaTaxaPerformanceMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.PercentualIndice
		/// </summary>
		virtual public System.Decimal? PercentualIndice
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaPerformanceMetadata.ColumnNames.PercentualIndice);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaPerformanceMetadata.ColumnNames.PercentualIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.TaxaJuros
		/// </summary>
		virtual public System.Decimal? TaxaJuros
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaPerformanceMetadata.ColumnNames.TaxaJuros);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaPerformanceMetadata.ColumnNames.TaxaJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.TipoApropriacaoJuros
		/// </summary>
		virtual public System.Byte? TipoApropriacaoJuros
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.TipoApropriacaoJuros);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.TipoApropriacaoJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.ContagemDiasJuros
		/// </summary>
		virtual public System.Byte? ContagemDiasJuros
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.ContagemDiasJuros);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.ContagemDiasJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.BaseAnoJuros
		/// </summary>
		virtual public System.Int16? BaseAnoJuros
		{
			get
			{
				return base.GetSystemInt16(TabelaTaxaPerformanceMetadata.ColumnNames.BaseAnoJuros);
			}
			
			set
			{
				base.SetSystemInt16(TabelaTaxaPerformanceMetadata.ColumnNames.BaseAnoJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.TaxaPerformance
		/// </summary>
		virtual public System.Decimal? TaxaPerformance
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaPerformanceMetadata.ColumnNames.TaxaPerformance);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaPerformanceMetadata.ColumnNames.TaxaPerformance, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.BaseApuracao
		/// </summary>
		virtual public System.Byte? BaseApuracao
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.BaseApuracao);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.BaseApuracao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.NumeroDiasPagamento
		/// </summary>
		virtual public System.Byte? NumeroDiasPagamento
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.NumeroDiasPagamento);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.NumeroDiasPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.DiaRenovacao
		/// </summary>
		virtual public System.Byte? DiaRenovacao
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.DiaRenovacao);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.DiaRenovacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.NumeroMesesRenovacao
		/// </summary>
		virtual public System.Byte? NumeroMesesRenovacao
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.NumeroMesesRenovacao);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.NumeroMesesRenovacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.ImpactaPL
		/// </summary>
		virtual public System.String ImpactaPL
		{
			get
			{
				return base.GetSystemString(TabelaTaxaPerformanceMetadata.ColumnNames.ImpactaPL);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaPerformanceMetadata.ColumnNames.ImpactaPL, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.CalculaBenchmarkNegativo
		/// </summary>
		virtual public System.Byte? CalculaBenchmarkNegativo
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.CalculaBenchmarkNegativo);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.CalculaBenchmarkNegativo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.ZeraPerformanceNegativa
		/// </summary>
		virtual public System.String ZeraPerformanceNegativa
		{
			get
			{
				return base.GetSystemString(TabelaTaxaPerformanceMetadata.ColumnNames.ZeraPerformanceNegativa);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaPerformanceMetadata.ColumnNames.ZeraPerformanceNegativa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.CalculaPerformanceResgate
		/// </summary>
		virtual public System.String CalculaPerformanceResgate
		{
			get
			{
				return base.GetSystemString(TabelaTaxaPerformanceMetadata.ColumnNames.CalculaPerformanceResgate);
			}
			
			set
			{
				base.SetSystemString(TabelaTaxaPerformanceMetadata.ColumnNames.CalculaPerformanceResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.NumeroDiasPagamentoResgate
		/// </summary>
		virtual public System.Byte? NumeroDiasPagamentoResgate
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.NumeroDiasPagamentoResgate);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.NumeroDiasPagamentoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.TipoCalculoResgate
		/// </summary>
		virtual public System.Byte? TipoCalculoResgate
		{
			get
			{
				return base.GetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.TipoCalculoResgate);
			}
			
			set
			{
				base.SetSystemByte(TabelaTaxaPerformanceMetadata.ColumnNames.TipoCalculoResgate, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.DataFim
		/// </summary>
		virtual public System.DateTime? DataFim
		{
			get
			{
				return base.GetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataFim);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataFim, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.IdEventoProvisao
		/// </summary>
		virtual public System.Int32? IdEventoProvisao
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoProvisao);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoProvisao, value))
				{
					this._UpToContabRoteiroByIdEventoProvisao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.IdEventoPagamento
		/// </summary>
		virtual public System.Int32? IdEventoPagamento
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoPagamento);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoPagamento, value))
				{
					this._UpToContabRoteiroByIdEventoPagamento = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.DataFimApropriacao
		/// </summary>
		virtual public System.DateTime? DataFimApropriacao
		{
			get
			{
				return base.GetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataFimApropriacao);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataFimApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaPerformance.DataUltimoCortePfee
		/// </summary>
		virtual public System.DateTime? DataUltimoCortePfee
		{
			get
			{
				return base.GetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataUltimoCortePfee);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTaxaPerformanceMetadata.ColumnNames.DataUltimoCortePfee, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected ContabRoteiro _UpToContabRoteiroByIdEventoProvisao;
		[CLSCompliant(false)]
		internal protected ContabRoteiro _UpToContabRoteiroByIdEventoPagamento;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaTaxaPerformance entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCalculo
			{
				get
				{
					System.Byte? data = entity.TipoCalculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCalculo = null;
					else entity.TipoCalculo = Convert.ToByte(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String PercentualIndice
			{
				get
				{
					System.Decimal? data = entity.PercentualIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualIndice = null;
					else entity.PercentualIndice = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaJuros
			{
				get
				{
					System.Decimal? data = entity.TaxaJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaJuros = null;
					else entity.TaxaJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoApropriacaoJuros
			{
				get
				{
					System.Byte? data = entity.TipoApropriacaoJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoApropriacaoJuros = null;
					else entity.TipoApropriacaoJuros = Convert.ToByte(value);
				}
			}
				
			public System.String ContagemDiasJuros
			{
				get
				{
					System.Byte? data = entity.ContagemDiasJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDiasJuros = null;
					else entity.ContagemDiasJuros = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAnoJuros
			{
				get
				{
					System.Int16? data = entity.BaseAnoJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAnoJuros = null;
					else entity.BaseAnoJuros = Convert.ToInt16(value);
				}
			}
				
			public System.String TaxaPerformance
			{
				get
				{
					System.Decimal? data = entity.TaxaPerformance;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaPerformance = null;
					else entity.TaxaPerformance = Convert.ToDecimal(value);
				}
			}
				
			public System.String BaseApuracao
			{
				get
				{
					System.Byte? data = entity.BaseApuracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseApuracao = null;
					else entity.BaseApuracao = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroDiasPagamento
			{
				get
				{
					System.Byte? data = entity.NumeroDiasPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroDiasPagamento = null;
					else entity.NumeroDiasPagamento = Convert.ToByte(value);
				}
			}
				
			public System.String DiaRenovacao
			{
				get
				{
					System.Byte? data = entity.DiaRenovacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DiaRenovacao = null;
					else entity.DiaRenovacao = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroMesesRenovacao
			{
				get
				{
					System.Byte? data = entity.NumeroMesesRenovacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroMesesRenovacao = null;
					else entity.NumeroMesesRenovacao = Convert.ToByte(value);
				}
			}
				
			public System.String ImpactaPL
			{
				get
				{
					System.String data = entity.ImpactaPL;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ImpactaPL = null;
					else entity.ImpactaPL = Convert.ToString(value);
				}
			}
				
			public System.String CalculaBenchmarkNegativo
			{
				get
				{
					System.Byte? data = entity.CalculaBenchmarkNegativo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaBenchmarkNegativo = null;
					else entity.CalculaBenchmarkNegativo = Convert.ToByte(value);
				}
			}
				
			public System.String ZeraPerformanceNegativa
			{
				get
				{
					System.String data = entity.ZeraPerformanceNegativa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ZeraPerformanceNegativa = null;
					else entity.ZeraPerformanceNegativa = Convert.ToString(value);
				}
			}
				
			public System.String CalculaPerformanceResgate
			{
				get
				{
					System.String data = entity.CalculaPerformanceResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaPerformanceResgate = null;
					else entity.CalculaPerformanceResgate = Convert.ToString(value);
				}
			}
				
			public System.String NumeroDiasPagamentoResgate
			{
				get
				{
					System.Byte? data = entity.NumeroDiasPagamentoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroDiasPagamentoResgate = null;
					else entity.NumeroDiasPagamentoResgate = Convert.ToByte(value);
				}
			}
				
			public System.String TipoCalculoResgate
			{
				get
				{
					System.Byte? data = entity.TipoCalculoResgate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCalculoResgate = null;
					else entity.TipoCalculoResgate = Convert.ToByte(value);
				}
			}
				
			public System.String DataFim
			{
				get
				{
					System.DateTime? data = entity.DataFim;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFim = null;
					else entity.DataFim = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdEventoProvisao
			{
				get
				{
					System.Int32? data = entity.IdEventoProvisao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoProvisao = null;
					else entity.IdEventoProvisao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEventoPagamento
			{
				get
				{
					System.Int32? data = entity.IdEventoPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoPagamento = null;
					else entity.IdEventoPagamento = Convert.ToInt32(value);
				}
			}
				
			public System.String DataFimApropriacao
			{
				get
				{
					System.DateTime? data = entity.DataFimApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimApropriacao = null;
					else entity.DataFimApropriacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataUltimoCortePfee
			{
				get
				{
					System.DateTime? data = entity.DataUltimoCortePfee;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataUltimoCortePfee = null;
					else entity.DataUltimoCortePfee = Convert.ToDateTime(value);
				}
			}
			

			private esTabelaTaxaPerformance entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaTaxaPerformanceQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaTaxaPerformance can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaTaxaPerformance : esTabelaTaxaPerformance
	{

				
		#region CalculoPerformance - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - TabelaTaxaPerformance_CalculoPerformance_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoPerformance CalculoPerformance
		{
			get
			{
				if(this._CalculoPerformance == null)
				{
					this._CalculoPerformance = new CalculoPerformance();
					this._CalculoPerformance.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("CalculoPerformance", this._CalculoPerformance);
				
					if(this.IdTabela != null)
					{
						this._CalculoPerformance.Query.Where(this._CalculoPerformance.Query.IdTabela == this.IdTabela);
						this._CalculoPerformance.Query.Load();
					}
				}

				return this._CalculoPerformance;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoPerformance != null) 
				{ 
					this.RemovePostOneSave("CalculoPerformance"); 
					this._CalculoPerformance = null;
					
				} 
			}          			
		}

		private CalculoPerformance _CalculoPerformance;
		#endregion

				
		#region CalculoPerformanceHistoricoCollectionByIdTabela - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TabelaTaxaPerformance_CalculoPerformanceHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public CalculoPerformanceHistoricoCollection CalculoPerformanceHistoricoCollectionByIdTabela
		{
			get
			{
				if(this._CalculoPerformanceHistoricoCollectionByIdTabela == null)
				{
					this._CalculoPerformanceHistoricoCollectionByIdTabela = new CalculoPerformanceHistoricoCollection();
					this._CalculoPerformanceHistoricoCollectionByIdTabela.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CalculoPerformanceHistoricoCollectionByIdTabela", this._CalculoPerformanceHistoricoCollectionByIdTabela);
				
					if(this.IdTabela != null)
					{
						this._CalculoPerformanceHistoricoCollectionByIdTabela.Query.Where(this._CalculoPerformanceHistoricoCollectionByIdTabela.Query.IdTabela == this.IdTabela);
						this._CalculoPerformanceHistoricoCollectionByIdTabela.Query.Load();

						// Auto-hookup Foreign Keys
						this._CalculoPerformanceHistoricoCollectionByIdTabela.fks.Add(CalculoPerformanceHistoricoMetadata.ColumnNames.IdTabela, this.IdTabela);
					}
				}

				return this._CalculoPerformanceHistoricoCollectionByIdTabela;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CalculoPerformanceHistoricoCollectionByIdTabela != null) 
				{ 
					this.RemovePostSave("CalculoPerformanceHistoricoCollectionByIdTabela"); 
					this._CalculoPerformanceHistoricoCollectionByIdTabela = null;
					
				} 
			} 			
		}

		private CalculoPerformanceHistoricoCollection _CalculoPerformanceHistoricoCollectionByIdTabela;
		#endregion

				
		#region UpToContabRoteiroByIdEventoProvisao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabRoteiro_TabelaTaxaPerformance_FK1
		/// </summary>

		[XmlIgnore]
		public ContabRoteiro UpToContabRoteiroByIdEventoProvisao
		{
			get
			{
				if(this._UpToContabRoteiroByIdEventoProvisao == null
					&& IdEventoProvisao != null					)
				{
					this._UpToContabRoteiroByIdEventoProvisao = new ContabRoteiro();
					this._UpToContabRoteiroByIdEventoProvisao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabRoteiroByIdEventoProvisao", this._UpToContabRoteiroByIdEventoProvisao);
					this._UpToContabRoteiroByIdEventoProvisao.Query.Where(this._UpToContabRoteiroByIdEventoProvisao.Query.IdEvento == this.IdEventoProvisao);
					this._UpToContabRoteiroByIdEventoProvisao.Query.Load();
				}

				return this._UpToContabRoteiroByIdEventoProvisao;
			}
			
			set
			{
				this.RemovePreSave("UpToContabRoteiroByIdEventoProvisao");
				

				if(value == null)
				{
					this.IdEventoProvisao = null;
					this._UpToContabRoteiroByIdEventoProvisao = null;
				}
				else
				{
					this.IdEventoProvisao = value.IdEvento;
					this._UpToContabRoteiroByIdEventoProvisao = value;
					this.SetPreSave("UpToContabRoteiroByIdEventoProvisao", this._UpToContabRoteiroByIdEventoProvisao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabRoteiroByIdEventoPagamento - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabRoteiro_TabelaTaxaPerformance_FK2
		/// </summary>

		[XmlIgnore]
		public ContabRoteiro UpToContabRoteiroByIdEventoPagamento
		{
			get
			{
				if(this._UpToContabRoteiroByIdEventoPagamento == null
					&& IdEventoPagamento != null					)
				{
					this._UpToContabRoteiroByIdEventoPagamento = new ContabRoteiro();
					this._UpToContabRoteiroByIdEventoPagamento.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabRoteiroByIdEventoPagamento", this._UpToContabRoteiroByIdEventoPagamento);
					this._UpToContabRoteiroByIdEventoPagamento.Query.Where(this._UpToContabRoteiroByIdEventoPagamento.Query.IdEvento == this.IdEventoPagamento);
					this._UpToContabRoteiroByIdEventoPagamento.Query.Load();
				}

				return this._UpToContabRoteiroByIdEventoPagamento;
			}
			
			set
			{
				this.RemovePreSave("UpToContabRoteiroByIdEventoPagamento");
				

				if(value == null)
				{
					this.IdEventoPagamento = null;
					this._UpToContabRoteiroByIdEventoPagamento = null;
				}
				else
				{
					this.IdEventoPagamento = value.IdEvento;
					this._UpToContabRoteiroByIdEventoPagamento = value;
					this.SetPreSave("UpToContabRoteiroByIdEventoPagamento", this._UpToContabRoteiroByIdEventoPagamento);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_TabelaTaxaPerformance_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CalculoPerformanceHistoricoCollectionByIdTabela", typeof(CalculoPerformanceHistoricoCollection), new CalculoPerformanceHistorico()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToContabRoteiroByIdEventoProvisao != null)
			{
				this.IdEventoProvisao = this._UpToContabRoteiroByIdEventoProvisao.IdEvento;
			}
			if(!this.es.IsDeleted && this._UpToContabRoteiroByIdEventoPagamento != null)
			{
				this.IdEventoPagamento = this._UpToContabRoteiroByIdEventoPagamento.IdEvento;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CalculoPerformanceHistoricoCollectionByIdTabela != null)
			{
				foreach(CalculoPerformanceHistorico obj in this._CalculoPerformanceHistoricoCollectionByIdTabela)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTabela = this.IdTabela;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
			if(this._CalculoPerformance != null)
			{
				if(this._CalculoPerformance.es.IsAdded)
				{
					this._CalculoPerformance.IdTabela = this.IdTabela;
				}
			}
		}
		
	}



	[Serializable]
	abstract public class esTabelaTaxaPerformanceQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaPerformanceMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCalculo
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.TipoCalculo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem PercentualIndice
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.PercentualIndice, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaJuros
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.TaxaJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoApropriacaoJuros
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.TipoApropriacaoJuros, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContagemDiasJuros
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.ContagemDiasJuros, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAnoJuros
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.BaseAnoJuros, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TaxaPerformance
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.TaxaPerformance, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem BaseApuracao
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.BaseApuracao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroDiasPagamento
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.NumeroDiasPagamento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DiaRenovacao
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.DiaRenovacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroMesesRenovacao
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.NumeroMesesRenovacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ImpactaPL
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.ImpactaPL, esSystemType.String);
			}
		} 
		
		public esQueryItem CalculaBenchmarkNegativo
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.CalculaBenchmarkNegativo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ZeraPerformanceNegativa
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.ZeraPerformanceNegativa, esSystemType.String);
			}
		} 
		
		public esQueryItem CalculaPerformanceResgate
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.CalculaPerformanceResgate, esSystemType.String);
			}
		} 
		
		public esQueryItem NumeroDiasPagamentoResgate
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.NumeroDiasPagamentoResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoCalculoResgate
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.TipoCalculoResgate, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataFim
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.DataFim, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdEventoProvisao
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoProvisao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEventoPagamento
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoPagamento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataFimApropriacao
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.DataFimApropriacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataUltimoCortePfee
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaPerformanceMetadata.ColumnNames.DataUltimoCortePfee, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaTaxaPerformanceCollection")]
	public partial class TabelaTaxaPerformanceCollection : esTabelaTaxaPerformanceCollection, IEnumerable<TabelaTaxaPerformance>
	{
		public TabelaTaxaPerformanceCollection()
		{

		}
		
		public static implicit operator List<TabelaTaxaPerformance>(TabelaTaxaPerformanceCollection coll)
		{
			List<TabelaTaxaPerformance> list = new List<TabelaTaxaPerformance>();
			
			foreach (TabelaTaxaPerformance emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaTaxaPerformanceMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaPerformanceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaTaxaPerformance(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaTaxaPerformance();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaTaxaPerformanceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaPerformanceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaTaxaPerformanceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaTaxaPerformance AddNew()
		{
			TabelaTaxaPerformance entity = base.AddNewEntity() as TabelaTaxaPerformance;
			
			return entity;
		}

		public TabelaTaxaPerformance FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as TabelaTaxaPerformance;
		}


		#region IEnumerable<TabelaTaxaPerformance> Members

		IEnumerator<TabelaTaxaPerformance> IEnumerable<TabelaTaxaPerformance>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaTaxaPerformance;
			}
		}

		#endregion
		
		private TabelaTaxaPerformanceQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaTaxaPerformance' table
	/// </summary>

	[Serializable]
	public partial class TabelaTaxaPerformance : esTabelaTaxaPerformance
	{
		public TabelaTaxaPerformance()
		{

		}
	
		public TabelaTaxaPerformance(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaPerformanceMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaTaxaPerformanceQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaPerformanceQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaTaxaPerformanceQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaPerformanceQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaTaxaPerformanceQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaTaxaPerformanceQuery query;
	}



	[Serializable]
	public partial class TabelaTaxaPerformanceQuery : esTabelaTaxaPerformanceQuery
	{
		public TabelaTaxaPerformanceQuery()
		{

		}		
		
		public TabelaTaxaPerformanceQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaTaxaPerformanceMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaTaxaPerformanceMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.IdCarteira, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.TipoCalculo, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.TipoCalculo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.IdIndice, 4, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.PercentualIndice, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.PercentualIndice;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.TaxaJuros, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.TaxaJuros;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.TipoApropriacaoJuros, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.TipoApropriacaoJuros;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.ContagemDiasJuros, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.ContagemDiasJuros;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.BaseAnoJuros, 9, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.BaseAnoJuros;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.TaxaPerformance, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.TaxaPerformance;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.BaseApuracao, 11, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.BaseApuracao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.NumeroDiasPagamento, 12, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.NumeroDiasPagamento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.DiaRenovacao, 13, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.DiaRenovacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.NumeroMesesRenovacao, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.NumeroMesesRenovacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.ImpactaPL, 15, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.ImpactaPL;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.CalculaBenchmarkNegativo, 16, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.CalculaBenchmarkNegativo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.ZeraPerformanceNegativa, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.ZeraPerformanceNegativa;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.CalculaPerformanceResgate, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.CalculaPerformanceResgate;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.NumeroDiasPagamentoResgate, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.NumeroDiasPagamentoResgate;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.TipoCalculoResgate, 20, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.TipoCalculoResgate;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.DataFim, 21, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.DataFim;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoProvisao, 22, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.IdEventoProvisao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoPagamento, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.IdEventoPagamento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.DataFimApropriacao, 24, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.DataFimApropriacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.DataPagamento, 25, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaPerformanceMetadata.ColumnNames.DataUltimoCortePfee, 26, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTaxaPerformanceMetadata.PropertyNames.DataUltimoCortePfee;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaTaxaPerformanceMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoCalculo = "TipoCalculo";
			 public const string IdIndice = "IdIndice";
			 public const string PercentualIndice = "PercentualIndice";
			 public const string TaxaJuros = "TaxaJuros";
			 public const string TipoApropriacaoJuros = "TipoApropriacaoJuros";
			 public const string ContagemDiasJuros = "ContagemDiasJuros";
			 public const string BaseAnoJuros = "BaseAnoJuros";
			 public const string TaxaPerformance = "TaxaPerformance";
			 public const string BaseApuracao = "BaseApuracao";
			 public const string NumeroDiasPagamento = "NumeroDiasPagamento";
			 public const string DiaRenovacao = "DiaRenovacao";
			 public const string NumeroMesesRenovacao = "NumeroMesesRenovacao";
			 public const string ImpactaPL = "ImpactaPL";
			 public const string CalculaBenchmarkNegativo = "CalculaBenchmarkNegativo";
			 public const string ZeraPerformanceNegativa = "ZeraPerformanceNegativa";
			 public const string CalculaPerformanceResgate = "CalculaPerformanceResgate";
			 public const string NumeroDiasPagamentoResgate = "NumeroDiasPagamentoResgate";
			 public const string TipoCalculoResgate = "TipoCalculoResgate";
			 public const string DataFim = "DataFim";
			 public const string IdEventoProvisao = "IdEventoProvisao";
			 public const string IdEventoPagamento = "IdEventoPagamento";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCarteira = "IdCarteira";
			 public const string TipoCalculo = "TipoCalculo";
			 public const string IdIndice = "IdIndice";
			 public const string PercentualIndice = "PercentualIndice";
			 public const string TaxaJuros = "TaxaJuros";
			 public const string TipoApropriacaoJuros = "TipoApropriacaoJuros";
			 public const string ContagemDiasJuros = "ContagemDiasJuros";
			 public const string BaseAnoJuros = "BaseAnoJuros";
			 public const string TaxaPerformance = "TaxaPerformance";
			 public const string BaseApuracao = "BaseApuracao";
			 public const string NumeroDiasPagamento = "NumeroDiasPagamento";
			 public const string DiaRenovacao = "DiaRenovacao";
			 public const string NumeroMesesRenovacao = "NumeroMesesRenovacao";
			 public const string ImpactaPL = "ImpactaPL";
			 public const string CalculaBenchmarkNegativo = "CalculaBenchmarkNegativo";
			 public const string ZeraPerformanceNegativa = "ZeraPerformanceNegativa";
			 public const string CalculaPerformanceResgate = "CalculaPerformanceResgate";
			 public const string NumeroDiasPagamentoResgate = "NumeroDiasPagamentoResgate";
			 public const string TipoCalculoResgate = "TipoCalculoResgate";
			 public const string DataFim = "DataFim";
			 public const string IdEventoProvisao = "IdEventoProvisao";
			 public const string IdEventoPagamento = "IdEventoPagamento";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string DataUltimoCortePfee = "DataUltimoCortePfee";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaTaxaPerformanceMetadata))
			{
				if(TabelaTaxaPerformanceMetadata.mapDelegates == null)
				{
					TabelaTaxaPerformanceMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaTaxaPerformanceMetadata.meta == null)
				{
					TabelaTaxaPerformanceMetadata.meta = new TabelaTaxaPerformanceMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCalculo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("PercentualIndice", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoApropriacaoJuros", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContagemDiasJuros", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAnoJuros", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TaxaPerformance", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("BaseApuracao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroDiasPagamento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DiaRenovacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroMesesRenovacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ImpactaPL", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CalculaBenchmarkNegativo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ZeraPerformanceNegativa", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CalculaPerformanceResgate", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("NumeroDiasPagamentoResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoCalculoResgate", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataFim", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdEventoProvisao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEventoPagamento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataFimApropriacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataUltimoCortePfee", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "TabelaTaxaPerformance";
				meta.Destination = "TabelaTaxaPerformance";
				
				meta.spInsert = "proc_TabelaTaxaPerformanceInsert";				
				meta.spUpdate = "proc_TabelaTaxaPerformanceUpdate";		
				meta.spDelete = "proc_TabelaTaxaPerformanceDelete";
				meta.spLoadAll = "proc_TabelaTaxaPerformanceLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaTaxaPerformanceLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaTaxaPerformanceMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
