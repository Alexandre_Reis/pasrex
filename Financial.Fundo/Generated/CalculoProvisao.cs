/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 1/15/2015 2:21:10 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		



		





		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Fundo
{

	[Serializable]
	abstract public class esCalculoProvisaoCollection : esEntityCollection
	{
		public esCalculoProvisaoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CalculoProvisaoCollection";
		}

		#region Query Logic
		protected void InitQuery(esCalculoProvisaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCalculoProvisaoQuery);
		}
		#endregion
		
		virtual public CalculoProvisao DetachEntity(CalculoProvisao entity)
		{
			return base.DetachEntity(entity) as CalculoProvisao;
		}
		
		virtual public CalculoProvisao AttachEntity(CalculoProvisao entity)
		{
			return base.AttachEntity(entity) as CalculoProvisao;
		}
		
		virtual public void Combine(CalculoProvisaoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CalculoProvisao this[int index]
		{
			get
			{
				return base[index] as CalculoProvisao;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CalculoProvisao);
		}
	}



	[Serializable]
	abstract public class esCalculoProvisao : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCalculoProvisaoQuery GetDynamicQuery()
		{
			return null;
		}

		public esCalculoProvisao()
		{

		}

		public esCalculoProvisao(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCalculoProvisaoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esCalculoProvisaoQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "IdCarteira": this.str.IdCarteira = (string)value; break;							
						case "ValorDia": this.str.ValorDia = (string)value; break;							
						case "ValorAcumulado": this.str.ValorAcumulado = (string)value; break;							
						case "DataFimApropriacao": this.str.DataFimApropriacao = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "ValorCPMFDia": this.str.ValorCPMFDia = (string)value; break;							
						case "ValorCPMFAcumulado": this.str.ValorCPMFAcumulado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "IdCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteira = (System.Int32?)value;
							break;
						
						case "ValorDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDia = (System.Decimal?)value;
							break;
						
						case "ValorAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAcumulado = (System.Decimal?)value;
							break;
						
						case "DataFimApropriacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimApropriacao = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "ValorCPMFDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMFDia = (System.Decimal?)value;
							break;
						
						case "ValorCPMFAcumulado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCPMFAcumulado = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CalculoProvisao.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(CalculoProvisaoMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(CalculoProvisaoMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisao.IdCarteira
		/// </summary>
		virtual public System.Int32? IdCarteira
		{
			get
			{
				return base.GetSystemInt32(CalculoProvisaoMetadata.ColumnNames.IdCarteira);
			}
			
			set
			{
				if(base.SetSystemInt32(CalculoProvisaoMetadata.ColumnNames.IdCarteira, value))
				{
					this._UpToCarteiraByIdCarteira = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisao.ValorDia
		/// </summary>
		virtual public System.Decimal? ValorDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoProvisaoMetadata.ColumnNames.ValorDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoProvisaoMetadata.ColumnNames.ValorDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisao.ValorAcumulado
		/// </summary>
		virtual public System.Decimal? ValorAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoProvisaoMetadata.ColumnNames.ValorAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoProvisaoMetadata.ColumnNames.ValorAcumulado, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisao.DataFimApropriacao
		/// </summary>
		virtual public System.DateTime? DataFimApropriacao
		{
			get
			{
				return base.GetSystemDateTime(CalculoProvisaoMetadata.ColumnNames.DataFimApropriacao);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoProvisaoMetadata.ColumnNames.DataFimApropriacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisao.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(CalculoProvisaoMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(CalculoProvisaoMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisao.ValorCPMFDia
		/// </summary>
		virtual public System.Decimal? ValorCPMFDia
		{
			get
			{
				return base.GetSystemDecimal(CalculoProvisaoMetadata.ColumnNames.ValorCPMFDia);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoProvisaoMetadata.ColumnNames.ValorCPMFDia, value);
			}
		}
		
		/// <summary>
		/// Maps to CalculoProvisao.ValorCPMFAcumulado
		/// </summary>
		virtual public System.Decimal? ValorCPMFAcumulado
		{
			get
			{
				return base.GetSystemDecimal(CalculoProvisaoMetadata.ColumnNames.ValorCPMFAcumulado);
			}
			
			set
			{
				base.SetSystemDecimal(CalculoProvisaoMetadata.ColumnNames.ValorCPMFAcumulado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteira;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCalculoProvisao entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteira
			{
				get
				{
					System.Int32? data = entity.IdCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteira = null;
					else entity.IdCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorDia
			{
				get
				{
					System.Decimal? data = entity.ValorDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDia = null;
					else entity.ValorDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAcumulado = null;
					else entity.ValorAcumulado = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataFimApropriacao
			{
				get
				{
					System.DateTime? data = entity.DataFimApropriacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimApropriacao = null;
					else entity.DataFimApropriacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorCPMFDia
			{
				get
				{
					System.Decimal? data = entity.ValorCPMFDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMFDia = null;
					else entity.ValorCPMFDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCPMFAcumulado
			{
				get
				{
					System.Decimal? data = entity.ValorCPMFAcumulado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCPMFAcumulado = null;
					else entity.ValorCPMFAcumulado = Convert.ToDecimal(value);
				}
			}
			

			private esCalculoProvisao entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCalculoProvisaoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCalculoProvisao can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CalculoProvisao : esCalculoProvisao
	{

				
		#region UpToCarteiraByIdCarteira - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Carteira_CalculoProvisao_FK1
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteira
		{
			get
			{
				if(this._UpToCarteiraByIdCarteira == null
					&& IdCarteira != null					)
				{
					this._UpToCarteiraByIdCarteira = new Carteira();
					this._UpToCarteiraByIdCarteira.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Where(this._UpToCarteiraByIdCarteira.Query.IdCarteira == this.IdCarteira);
					this._UpToCarteiraByIdCarteira.Query.Load();
				}

				return this._UpToCarteiraByIdCarteira;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteira");
				

				if(value == null)
				{
					this.IdCarteira = null;
					this._UpToCarteiraByIdCarteira = null;
				}
				else
				{
					this.IdCarteira = value.IdCarteira;
					this._UpToCarteiraByIdCarteira = value;
					this.SetPreSave("UpToCarteiraByIdCarteira", this._UpToCarteiraByIdCarteira);
				}
				
			}
		}
		#endregion
		

		#region UpToTabelaProvisao - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - TabelaProvisao_CalculoProvisao_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaProvisao UpToTabelaProvisao
		{
			get
			{
				if(this._UpToTabelaProvisao == null
					&& IdTabela != null					)
				{
					this._UpToTabelaProvisao = new TabelaProvisao();
					this._UpToTabelaProvisao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTabelaProvisao", this._UpToTabelaProvisao);
					this._UpToTabelaProvisao.Query.Where(this._UpToTabelaProvisao.Query.IdTabela == this.IdTabela);
					this._UpToTabelaProvisao.Query.Load();
				}

				return this._UpToTabelaProvisao;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToTabelaProvisao");

				if(value == null)
				{
					this._UpToTabelaProvisao = null;
				}
				else
				{
					this._UpToTabelaProvisao = value;
					this.SetPreSave("UpToTabelaProvisao", this._UpToTabelaProvisao);
				}
				
				
			} 
		}

		private TabelaProvisao _UpToTabelaProvisao;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTabelaProvisao != null)
			{
				this.IdTabela = this._UpToTabelaProvisao.IdTabela;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCalculoProvisaoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CalculoProvisaoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteira
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoMetadata.ColumnNames.IdCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorDia
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoMetadata.ColumnNames.ValorDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoMetadata.ColumnNames.ValorAcumulado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataFimApropriacao
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoMetadata.ColumnNames.DataFimApropriacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorCPMFDia
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoMetadata.ColumnNames.ValorCPMFDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCPMFAcumulado
		{
			get
			{
				return new esQueryItem(this, CalculoProvisaoMetadata.ColumnNames.ValorCPMFAcumulado, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CalculoProvisaoCollection")]
	public partial class CalculoProvisaoCollection : esCalculoProvisaoCollection, IEnumerable<CalculoProvisao>
	{
		public CalculoProvisaoCollection()
		{

		}
		
		public static implicit operator List<CalculoProvisao>(CalculoProvisaoCollection coll)
		{
			List<CalculoProvisao> list = new List<CalculoProvisao>();
			
			foreach (CalculoProvisao emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CalculoProvisaoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoProvisaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CalculoProvisao(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CalculoProvisao();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CalculoProvisaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoProvisaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CalculoProvisaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CalculoProvisao AddNew()
		{
			CalculoProvisao entity = base.AddNewEntity() as CalculoProvisao;
			
			return entity;
		}

		public CalculoProvisao FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as CalculoProvisao;
		}


		#region IEnumerable<CalculoProvisao> Members

		IEnumerator<CalculoProvisao> IEnumerable<CalculoProvisao>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CalculoProvisao;
			}
		}

		#endregion
		
		private CalculoProvisaoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CalculoProvisao' table
	/// </summary>

	[Serializable]
	public partial class CalculoProvisao : esCalculoProvisao
	{
		public CalculoProvisao()
		{

		}
	
		public CalculoProvisao(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CalculoProvisaoMetadata.Meta();
			}
		}
		
		
		
		override protected esCalculoProvisaoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CalculoProvisaoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CalculoProvisaoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CalculoProvisaoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CalculoProvisaoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CalculoProvisaoQuery query;
	}



	[Serializable]
	public partial class CalculoProvisaoQuery : esCalculoProvisaoQuery
	{
		public CalculoProvisaoQuery()
		{

		}		
		
		public CalculoProvisaoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CalculoProvisaoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CalculoProvisaoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CalculoProvisaoMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoProvisaoMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoMetadata.ColumnNames.IdCarteira, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CalculoProvisaoMetadata.PropertyNames.IdCarteira;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoMetadata.ColumnNames.ValorDia, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoProvisaoMetadata.PropertyNames.ValorDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoMetadata.ColumnNames.ValorAcumulado, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoProvisaoMetadata.PropertyNames.ValorAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoMetadata.ColumnNames.DataFimApropriacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoProvisaoMetadata.PropertyNames.DataFimApropriacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoMetadata.ColumnNames.DataPagamento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CalculoProvisaoMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoMetadata.ColumnNames.ValorCPMFDia, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoProvisaoMetadata.PropertyNames.ValorCPMFDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CalculoProvisaoMetadata.ColumnNames.ValorCPMFAcumulado, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CalculoProvisaoMetadata.PropertyNames.ValorCPMFAcumulado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CalculoProvisaoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFDia = "ValorCPMFDia";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdCarteira = "IdCarteira";
			 public const string ValorDia = "ValorDia";
			 public const string ValorAcumulado = "ValorAcumulado";
			 public const string DataFimApropriacao = "DataFimApropriacao";
			 public const string DataPagamento = "DataPagamento";
			 public const string ValorCPMFDia = "ValorCPMFDia";
			 public const string ValorCPMFAcumulado = "ValorCPMFAcumulado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CalculoProvisaoMetadata))
			{
				if(CalculoProvisaoMetadata.mapDelegates == null)
				{
					CalculoProvisaoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CalculoProvisaoMetadata.meta == null)
				{
					CalculoProvisaoMetadata.meta = new CalculoProvisaoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAcumulado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataFimApropriacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorCPMFDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCPMFAcumulado", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CalculoProvisao";
				meta.Destination = "CalculoProvisao";
				
				meta.spInsert = "proc_CalculoProvisaoInsert";				
				meta.spUpdate = "proc_CalculoProvisaoUpdate";		
				meta.spDelete = "proc_CalculoProvisaoDelete";
				meta.spLoadAll = "proc_CalculoProvisaoLoadAll";
				meta.spLoadByPrimaryKey = "proc_CalculoProvisaoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CalculoProvisaoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
