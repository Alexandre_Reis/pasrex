﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/12/2015 18:15:52
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.RendaFixa;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Investidor;
using Financial.Investidor.Enums;

namespace Financial.Fundo
{
	public partial class PortfolioPadraoCollection : esPortfolioPadraoCollection
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        public void ClonaMovimentacao(DateTime dataInicio, DateTime dataFim)
        {          
            try
            {
                StringBuilder sqlText = new StringBuilder();
                sqlText.AppendLine("begin try");
                sqlText.AppendLine("	begin transaction");

                #region Delete
                //Deleta posições das movimentações
                sqlText.AppendLine("	DELETE pos FROM PosicaoRendaFixa pos INNER JOIN OperacaoRendaFixa op ON op.IdOperacao = pos.IdOperacao  INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");
                sqlText.AppendLine("	DELETE pos FROM PosicaoFundo pos     INNER JOIN OperacaoFundo op     ON op.IdOperacao = pos.IdOperacao  INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");
                sqlText.AppendLine("	DELETE pos FROM PosicaoRendaFixaHistorico pos INNER JOIN OperacaoRendaFixa op ON op.IdOperacao = pos.IdOperacao  INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");
                sqlText.AppendLine("	DELETE pos FROM PosicaoFundoHistorico pos     INNER JOIN OperacaoFundo op     ON op.IdOperacao = pos.IdOperacao  INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");
                sqlText.AppendLine("	DELETE pos FROM PosicaoRendaFixaAbertura pos INNER JOIN OperacaoRendaFixa op ON op.IdOperacao = pos.IdOperacao  INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");
                sqlText.AppendLine("	DELETE pos FROM PosicaoFundoAbertura pos     INNER JOIN OperacaoFundo op     ON op.IdOperacao = pos.IdOperacao  INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");

                //Deleta movimentações das carteiras trader
                sqlText.AppendLine("	DELETE op FROM OperacaoRendaFixa op INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");
                sqlText.AppendLine("	DELETE op FROM OperacaoBolsa op     INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.Data between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");
                sqlText.AppendLine("	DELETE op FROM OperacaoBMF op       INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.Data between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");
                sqlText.AppendLine("	DELETE op FROM OperacaoFundo op     INNER JOIN Trader T ON T.IdTrader = OP.IdTrader WHERE op.IdCliente = t.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'");
                #endregion

                #region Renda Fixa
                sqlText.Append(" INSERT INTO OperacaoRendaFixa ( ");

                string strFrom = " FROM OperacaoRendaFixa op  INNER JOIN Trader ON (op.IdTrader = Trader.IdTrader) WHERE Trader.IdCarteira is not null and op.IdCliente <> Trader.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'";
                string strSelect = " SELECT ";

                OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                int count = 0;
                int columnCount = operacaoRendaFixa.es.Meta.Columns.Count;
                foreach (esColumnMetadata colOperacao in operacaoRendaFixa.es.Meta.Columns)
                {
                    count++;

                    if (colOperacao.Name == OperacaoRendaFixaMetadata.ColumnNames.IdOperacao)
                        continue;

                    //Insert
                    sqlText.Append(colOperacao.Name);
                    if (count != columnCount)
                        sqlText.Append(",");
                    else
                        sqlText.Append(")");

                    //Select
                    if (colOperacao.Name == OperacaoRendaFixaMetadata.ColumnNames.IdCliente)
                        strSelect += "Trader.IdCarteira";
                    else if (colOperacao.Name == OperacaoRendaFixaMetadata.ColumnNames.IdTrader)
                        strSelect += "Trader.IdTrader";
                    else if (colOperacao.Name == OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoResgatada)
                        strSelect += "null";
                    else if (colOperacao.Name == OperacaoRendaFixaMetadata.ColumnNames.IdPosicaoResgatada)
                        strSelect += "null";
                    else if (colOperacao.Name == OperacaoRendaFixaMetadata.ColumnNames.Fonte)
                        strSelect += ((byte)FonteOperacaoTitulo.Manual).ToString();
                    else
                        strSelect += colOperacao.Name;

                    if (count != columnCount)
                        strSelect += ",";                    
                }

                sqlText.Append(strSelect + strFrom);
                sqlText.AppendLine();
                #endregion

                #region Operacao Bolsa
                sqlText.Append(" INSERT INTO OperacaoBolsa ( ");
                strFrom = " FROM OperacaoBolsa op INNER JOIN Trader ON (op.IdTrader = Trader.IdTrader) WHERE Trader.IdCarteira is not null and op.IdCliente <> Trader.IdCarteira AND op.Data between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'";
                strSelect = " SELECT ";
                count = 0;

                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                columnCount = operacaoBolsa.es.Meta.Columns.Count;
                foreach (esColumnMetadata colOperacao in operacaoBolsa.es.Meta.Columns)
                {
                    count++;

                    if (colOperacao.Name == OperacaoBolsaMetadata.ColumnNames.IdOperacao)
                        continue;

                    //Insert
                    sqlText.Append(colOperacao.Name);
                    if (count != columnCount)
                        sqlText.Append(",");
                    else
                        sqlText.Append(")");

                    //Select
                    if (colOperacao.Name == OperacaoBolsaMetadata.ColumnNames.IdCliente)
                        strSelect += "Trader.IdCarteira";
                    else if (colOperacao.Name == OperacaoBolsaMetadata.ColumnNames.CalculaDespesas)
                        strSelect += "'N'";
                    else if (colOperacao.Name == OperacaoBolsaMetadata.ColumnNames.IdTrader)
                        strSelect += "Trader.IdTrader";
                    else if (colOperacao.Name == OperacaoBolsaMetadata.ColumnNames.TipoOperacao)
                    {
                        strSelect += "CASE WHEN op." + OperacaoBolsaMetadata.ColumnNames.TipoOperacao + " = '" + TipoOperacaoBolsa.CompraDaytrade + "'";
                        strSelect += "THEN '" + TipoOperacaoBolsa.Compra + "'";
                        strSelect += "WHEN op." + OperacaoBolsaMetadata.ColumnNames.TipoOperacao + " = '" + TipoOperacaoBolsa.VendaDaytrade + "'";
                        strSelect += "THEN '" + TipoOperacaoBolsa.Venda + "'";
                        strSelect += " ELSE op." + OperacaoBolsaMetadata.ColumnNames.TipoOperacao;
                        strSelect += " END";
                    }
                    else if (colOperacao.Name == OperacaoBolsaMetadata.ColumnNames.Fonte)
                        strSelect += ((byte)FonteOperacaoBolsa.Gerencial).ToString();
                    else
                        strSelect += colOperacao.Name;

                    if (count != columnCount)
                        strSelect += ",";

                }

                sqlText.Append(strSelect + strFrom);
                sqlText.AppendLine();
                #endregion

                #region Operacao BMF
                sqlText.Append(" INSERT INTO OperacaoBMF ( ");
                strFrom = " FROM OperacaoBMF op INNER JOIN Trader ON (op.IdTrader = Trader.IdTrader) WHERE Trader.IdCarteira is not null and op.IdCliente <> Trader.IdCarteira AND op.Data between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'";
                strSelect = " SELECT ";
                count = 0;
                OperacaoBMF operacaoBMF = new OperacaoBMF();
                columnCount = operacaoBMF.es.Meta.Columns.Count;
                foreach (esColumnMetadata colOperacao in operacaoBMF.es.Meta.Columns)
                {
                    count++;

                    if (colOperacao.Name == OperacaoBMFMetadata.ColumnNames.IdOperacao)
                        continue;

                    //Insert
                    sqlText.Append(colOperacao.Name);
                    if (count != columnCount)
                        sqlText.Append(",");
                    else
                        sqlText.Append(")");

                    //Select
                    if (colOperacao.Name == OperacaoBMFMetadata.ColumnNames.IdCliente)
                        strSelect += "Trader.IdCarteira";
                    else if (colOperacao.Name == OperacaoBMFMetadata.ColumnNames.CalculaDespesas)
                        strSelect += "'N'";
                    else if (colOperacao.Name == OperacaoBMFMetadata.ColumnNames.IdTrader)
                        strSelect += "Trader.IdTrader";
                    else if (colOperacao.Name == OperacaoBMFMetadata.ColumnNames.TipoOperacao)
                    {                        
                        strSelect += "CASE WHEN op." + OperacaoBMFMetadata.ColumnNames.TipoOperacao + " = '" + TipoOperacaoBMF.CompraDaytrade + "'";
                        strSelect += "THEN '" + TipoOperacaoBMF.Compra + "'";
                        strSelect += "WHEN op." + OperacaoBMFMetadata.ColumnNames.TipoOperacao + " = '" + TipoOperacaoBMF.VendaDaytrade + "'";
                        strSelect += "THEN '" + TipoOperacaoBMF.Venda + "'";
                        strSelect += " ELSE op." + OperacaoBMFMetadata.ColumnNames.TipoOperacao;
                        strSelect += " END";
                    }
                    else if (colOperacao.Name == OperacaoBMFMetadata.ColumnNames.Fonte)
                        strSelect += ((byte)FonteOperacaoBMF.Gerencial).ToString();
                    else
                        strSelect += colOperacao.Name;

                    if (count != columnCount)
                        strSelect += ",";
                }

                sqlText.Append(strSelect + strFrom);
                sqlText.AppendLine();
                #endregion

                #region Operacao Fundo
                sqlText.Append(" INSERT INTO OperacaoFundo ( ");
                strFrom = " FROM OperacaoFundo op INNER JOIN Trader ON (op.IdTrader = Trader.IdTrader) WHERE Trader.IdCarteira is not null and op.IdCliente <> Trader.IdCarteira AND op.DataOperacao between '" + dataInicio.ToString("yyyyMMdd") + "' and '" + dataFim.ToString("yyyyMMdd") + "'";
                strSelect = " SELECT ";
                count = 0;
                OperacaoFundo operacaoFundo = new OperacaoFundo();
                columnCount = operacaoFundo.es.Meta.Columns.Count;
                foreach (esColumnMetadata colOperacao in operacaoFundo.es.Meta.Columns)
                {
                    count++;

                    if (colOperacao.Name == OperacaoFundoMetadata.ColumnNames.IdOperacao)
                        continue;

                    //Insert
                    sqlText.Append(colOperacao.Name);
                    if (count != columnCount)
                        sqlText.Append(",");
                    else
                        sqlText.Append(")");

                    //Select
                    if (colOperacao.Name == OperacaoFundoMetadata.ColumnNames.IdCliente)
                        strSelect += "Trader.IdCarteira";
                    else if (colOperacao.Name == OperacaoFundoMetadata.ColumnNames.IdCarteira)
                        strSelect += "op.IdCarteira";
                    else if (colOperacao.Name == OperacaoFundoMetadata.ColumnNames.IdTrader)
                        strSelect += "Trader.IdTrader";
                    else if (colOperacao.Name == OperacaoFundoMetadata.ColumnNames.IdOperacaoResgatada)
                        strSelect += "null";
                    else if (colOperacao.Name == OperacaoFundoMetadata.ColumnNames.IdPosicaoResgatada)
                        strSelect += "null";
                    else if (colOperacao.Name == OperacaoFundoMetadata.ColumnNames.TipoResgate)
                    {
                        string strTipoOperacao = (int)TipoOperacaoFundo.ResgateBruto + "," + (int)TipoOperacaoFundo.ResgateCotas + "," + (int)TipoOperacaoFundo.ResgateCotasEspecial + "," + (int)TipoOperacaoFundo.ResgateLiquido + "," + (int)TipoOperacaoFundo.ResgateTotal + "," + (int)TipoOperacaoFundo.RetiradaAtivoImpactoCota + "," + (int)TipoOperacaoFundo.RetiradaAtivoImpactoQtde;
                        strSelect += "CASE WHEN op." + OperacaoFundoMetadata.ColumnNames.TipoOperacao + " IN (" + strTipoOperacao + ") ";
                        strSelect += "THEN " + (int)TipoResgateFundo.FIFO;
                        strSelect += " ELSE null ";
                        strSelect += " END";
                    }
                    else if (colOperacao.Name == OperacaoFundoMetadata.ColumnNames.Fonte)
                        strSelect += ((byte)FonteOperacaoFundo.Manual).ToString();
                    else
                        strSelect += colOperacao.Name;

                    if (count != columnCount)
                        strSelect += ",";
                }

                sqlText.Append(strSelect + strFrom);
                sqlText.AppendLine();
                #endregion

                sqlText.AppendLine(" UPDATE Cliente SET Status = " + (int)StatusCliente.Aberto + " FROM Cliente C INNER JOIN Trader T ON t.IdCarteira = c.IdCliente WHERE t.IdCarteira is not null");

                sqlText.AppendLine("	commit ");
                sqlText.AppendLine("end try");
                sqlText.AppendLine("begin catch");
                sqlText.AppendLine("    DECLARE @ErrorMessage NVARCHAR(4000);");
                sqlText.AppendLine("    DECLARE @ErrorSeverity INT;");
                sqlText.AppendLine("    DECLARE @ErrorState INT;");
                sqlText.AppendLine("    SELECT @ErrorMessage = ERROR_MESSAGE(),");
                sqlText.AppendLine("           @ErrorSeverity = ERROR_SEVERITY(),");
                sqlText.AppendLine("           @ErrorState = ERROR_STATE();");                
                sqlText.AppendLine("    RAISERROR (@ErrorMessage, -- Message text.");
                sqlText.AppendLine("               @ErrorSeverity, -- Severity.");
                sqlText.AppendLine("               @ErrorState -- State.");
                sqlText.AppendLine("               );");
                sqlText.AppendLine("	rollback");
                sqlText.AppendLine("end catch	");

                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlText.ToString());
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void ClonaLiquidacao(int idCliente, DateTime data)
        {
            PortfolioPadrao portfolioPadrao = new PortfolioPadrao();
            portfolioPadrao = portfolioPadrao.RetornaPortfoliosVigentes(data);

            this.ClonaLiquidacao(idCliente, data, portfolioPadrao);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="portfolioPadrao"></param>
        public void ClonaLiquidacao(int idCliente, DateTime data, PortfolioPadrao portfolioPadrao)
        {
            if (portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) == 0)
                return;

            int idCarteiraDespesasReceitas = portfolioPadrao.IdCarteiraDespesasReceitas.Value;
            int idCarteiraTaxaGestao = portfolioPadrao.IdCarteiraTaxaGestao.Value;
            int idCarteiraTaxaPerformance = portfolioPadrao.IdCarteiraTaxaPerformance.Value;

            try
            {
                if (idCarteiraDespesasReceitas == idCliente || idCarteiraTaxaGestao == idCliente || idCarteiraTaxaPerformance == idCliente)
                {
                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(idCliente);

                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCliente, cliente.IdMoeda.Value);

                    StringBuilder sqlText = new StringBuilder();
                    sqlText.AppendLine("begin try");
                    sqlText.AppendLine("	begin transaction");

                    sqlText.AppendLine("	DELETE liq FROM Liquidacao liq            WHERE liq.IdCliente in " + "(" + idCliente + ")" + " AND liq.DataLancamento >= '" + data.ToString("yyyyMMdd") + "' AND liq.Origem <> " +  Convert.ToInt32(OrigemLancamentoLiquidacao.CarteiraGerencial));
                    sqlText.AppendLine("	DELETE liq FROM LiquidacaoAbertura liq    WHERE liq.IdCliente in " + "(" + idCliente + ")" + " AND liq.DataLancamento  >= '" + data.ToString("yyyyMMdd") + "' AND liq.Origem <> " + Convert.ToInt32(OrigemLancamentoLiquidacao.CarteiraGerencial));
                    sqlText.AppendLine("	DELETE liq FROM LiquidacaoHistorico liq   WHERE liq.IdCliente in " + "(" + idCliente + ")" + " AND liq.DataLancamento  >= '" + data.ToString("yyyyMMdd") + "' AND liq.Origem <> " + Convert.ToInt32(OrigemLancamentoLiquidacao.CarteiraGerencial));

                    Liquidacao liquidacao = new Liquidacao();
                    int columnCount = liquidacao.es.Meta.Columns.Count;
                    int count = 0;
                    string strInsert = " INSERT INTO Liquidacao (";
                    string strSelect = " SELECT ";
                    foreach (esColumnMetadata colLiquidacao in liquidacao.es.Meta.Columns)
                    {
                        count++;

                        if (colLiquidacao.Name == LiquidacaoMetadata.ColumnNames.IdLiquidacao)
                            continue;

                        //Insert
                        strInsert += colLiquidacao.Name;
                        if (count != columnCount)
                            strInsert += ",";
                        else
                            strInsert += ")";

                        //Select
                        if (colLiquidacao.Name == LiquidacaoMetadata.ColumnNames.IdCliente)
                            strSelect += idCliente;
                        else if (colLiquidacao.Name == LiquidacaoMetadata.ColumnNames.IdConta)
                            strSelect += idContaDefault;
                        else
                            strSelect += colLiquidacao.Name;

                        if (count != columnCount)
                            strSelect += ",";
                    }

                    sqlText.AppendLine(strInsert);
                    sqlText.AppendLine(strSelect);
                    sqlText.AppendLine("		FROM LiquidacaoHistorico liqHistorico");
                    sqlText.AppendLine("		where  liqHistorico.idCliente not in ((select idCarteira from Trader where IdCarteira is not null))"); //Exclui liquidação dos traders                    
                    sqlText.AppendLine("		  and liqHistorico.DataLancamento = '" + data.ToString("yyyyMMdd") + "'");
                    sqlText.AppendLine("		  and liqHistorico.DataLancamento = liqHistorico.DataHistorico");
                    sqlText.AppendLine("		  and liqHistorico.Valor <> 0");

                    if (idCarteiraTaxaGestao == idCliente)
                    {
                        sqlText.AppendLine(" and liqHistorico.IdCliente not in (" + idCarteiraDespesasReceitas + "," + idCarteiraTaxaPerformance + ")"); //Exclui liquidação das outros portfolios                        

                        List<int> lstOrigem = new List<int>();                        
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao);                        
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.TaxaGestao);

                        sqlText.Append("  and liqHistorico.Origem in (");
                        count = 0;
                        foreach (int fonte in lstOrigem)
                        {
                            count++;
                            sqlText.Append(fonte);

                            if (count != lstOrigem.Count)
                                sqlText.Append(",");
                        }
                        sqlText.Append(")");
                    }
                    else if (idCarteiraTaxaPerformance == idCliente)
                    {
                        sqlText.AppendLine(" and liqHistorico.IdCliente not in (" + idCarteiraDespesasReceitas + "," + idCarteiraTaxaGestao + ")"); //Exclui liquidação das outros portfolios                        

                        List<int> lstOrigem = new List<int>();
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.TaxaPerformance);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance);

                        sqlText.Append("  and liqHistorico.Origem in (");
                        count = 0;
                        foreach (int fonte in lstOrigem)
                        {
                            count++;
                            sqlText.Append(fonte);

                            if (count != lstOrigem.Count)
                                sqlText.Append(",");
                        }
                        sqlText.Append(")");
                    }
                    else
                    {
                        sqlText.AppendLine(" and liqHistorico.IdCliente not in (" + idCarteiraTaxaPerformance + "," + idCarteiraTaxaGestao + ")"); //Exclui liquidação das outros portfolios

                        List<int> lstOrigem = new List<int>();
                        lstOrigem.Add(OrigemLancamentoLiquidacao.AjusteCompensacaoCota);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.AjusteCompensacaoCotaEvento);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Outros);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Cambio.Custos);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Cambio.PrincipalDestino);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Cambio.PrincipalOrigem);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Cambio.Tributos);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.CPMF);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.PagtoProvisaoOutros);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaCustodia);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaFiscalizacaoCVM);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.ProvisaoOutros);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.TaxaCustodia);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.TaxaFiscalizacaoCVM);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Margem.ChamadaMargem);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Margem.DevolucaoMargem);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.IR.IRFonteDayTrade);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.IR.IRFonteOperacao);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.IR.IRRendaVariavel);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao);
                        lstOrigem.Add(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao);

                        sqlText.Append("  and liqHistorico.Origem in (");
                        count = 0;
                        foreach(int fonte in lstOrigem)
                        {
                            count++;
                            sqlText.Append(fonte);

                            if(count != lstOrigem.Count)
                                sqlText.Append(",");
                        }
                        sqlText.Append(")");
                    }                   

                    sqlText.AppendLine("	commit ");
                    sqlText.AppendLine("end try");
                    sqlText.AppendLine("begin catch");
                    sqlText.AppendLine("    DECLARE @ErrorMessage NVARCHAR(4000);");
                    sqlText.AppendLine("    DECLARE @ErrorSeverity INT;");
                    sqlText.AppendLine("    DECLARE @ErrorState INT;");
                    sqlText.AppendLine("    SELECT @ErrorMessage = ERROR_MESSAGE(),");
                    sqlText.AppendLine("           @ErrorSeverity = ERROR_SEVERITY(),");
                    sqlText.AppendLine("           @ErrorState = ERROR_STATE();");
                    sqlText.AppendLine("    RAISERROR (@ErrorMessage, -- Message text.");
                    sqlText.AppendLine("               @ErrorSeverity, -- Severity.");
                    sqlText.AppendLine("               @ErrorState -- State.");
                    sqlText.AppendLine("               );");
                    sqlText.AppendLine("	rollback");
                    sqlText.AppendLine("end catch	");

                    esUtility u = new esUtility();
                    u.ExecuteNonQuery(esQueryType.Text, sqlText.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ResetaStatusPortfolioPadrao(PortfolioPadrao portfolioPadrao)
        {
            if(portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) == 0)
                return;

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(portfolioPadrao.IdCarteiraDespesasReceitas.Value))
            {
                cliente.Status = (int)StatusCliente.Aberto;
                cliente.Save();
            }

            cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(portfolioPadrao.IdCarteiraTaxaGestao.Value))
            {
                cliente.Status = (int)StatusCliente.Aberto;
                cliente.Save();
            }

            cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(portfolioPadrao.IdCarteiraTaxaPerformance.Value))
            {
                cliente.Status = (int)StatusCliente.Aberto;
                cliente.Save();
            }
        }

	}
}
