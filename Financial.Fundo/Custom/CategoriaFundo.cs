/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           22/2/2007 16:16:45
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
	public partial class CategoriaFundo : esCategoriaFundo
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCategoria"></param>
        /// <param name="descricao"></param>
        /// <param name="idLista"></param>
        public void CriaCategoraFundo(int idCategoria, string descricao, int idLista)
        {
            StringBuilder sqlBuilder = new StringBuilder();
            esParameters esParams = new esParameters();

            esParams.Add(CategoriaFundoMetadata.ColumnNames.IdCategoria, idCategoria);
            esParams.Add(CategoriaFundoMetadata.ColumnNames.Descricao, descricao);
            esParams.Add(CategoriaFundoMetadata.ColumnNames.IdLista, idLista);

            sqlBuilder.Append("SET IDENTITY_INSERT CategoriaFundo ON ");

            sqlBuilder.Append("INSERT INTO CategoriaFundo (");
            sqlBuilder.Append(CategoriaFundoMetadata.ColumnNames.IdCategoria + ", ");
            sqlBuilder.Append(CategoriaFundoMetadata.ColumnNames.Descricao + ", ");
            sqlBuilder.Append(CategoriaFundoMetadata.ColumnNames.IdLista);
            sqlBuilder.Append(") VALUES ( ");
            sqlBuilder.Append("@" + CategoriaFundoMetadata.ColumnNames.IdCategoria);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + CategoriaFundoMetadata.ColumnNames.Descricao);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + CategoriaFundoMetadata.ColumnNames.IdLista);
            sqlBuilder.Append(")");

            sqlBuilder.Append("SET IDENTITY_INSERT CategoriaFundo OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
        }
	}
}
