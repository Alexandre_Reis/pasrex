﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Fundo.Exceptions;
using log4net;
using Financial.Investidor;

namespace Financial.Fundo
{
	public partial class CalculoProvisao : esCalculoProvisao
	{
        /// <summary>
        /// Função básica para copiar de CalculoProvisao para CalculoProvisaoHistorico.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCarteira, DateTime data)
        {
            try
            {
                CalculoProvisaoHistoricoCollection calculoProvisaoDeletarHistoricoCollection = new CalculoProvisaoHistoricoCollection();
                calculoProvisaoDeletarHistoricoCollection.DeletaCalculoProvisaoHistoricoDataHistoricoMaiorIgual(idCarteira, data);
            }
            catch { }

            CalculoProvisaoCollection calculoProvisaoCollection = new CalculoProvisaoCollection();
            calculoProvisaoCollection.BuscaCalculoProvisaoCompleta(idCarteira);
            
            CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
            //
            #region Copia de CalculoProvisao para CalculoProvisaoHistorico
            for (int i = 0; i < calculoProvisaoCollection.Count; i++)
            {
                CalculoProvisao calculoProvisao = calculoProvisaoCollection[i];

                CalculoProvisaoHistorico calculoProvisaoHistorico = calculoProvisaoHistoricoCollection.AddNew();

                calculoProvisaoHistorico.DataHistorico = data;
                calculoProvisaoHistorico.IdTabela = calculoProvisao.IdTabela;
                calculoProvisaoHistorico.IdCarteira = calculoProvisao.IdCarteira;
                calculoProvisaoHistorico.ValorDia = calculoProvisao.ValorDia;
                calculoProvisaoHistorico.ValorAcumulado = calculoProvisao.ValorAcumulado;
                calculoProvisaoHistorico.DataFimApropriacao = calculoProvisao.DataFimApropriacao;
                calculoProvisaoHistorico.DataPagamento = calculoProvisao.DataPagamento;
                calculoProvisaoHistorico.ValorCPMFDia = calculoProvisao.ValorCPMFDia;
                calculoProvisaoHistorico.ValorCPMFAcumulado = calculoProvisao.ValorCPMFAcumulado;
            }
            #endregion

            calculoProvisaoHistoricoCollection.Save();
        }

        /// <summary>
        /// Calcula os valores (diário e acumulado) dos vários tipos de provisão, 
        /// além da CPMF incidente (somente se > 01/01/2008).
        /// Lança na Liquidacao o valor acumulado da data (provisão e CPMF (somente se > 01/01/2008)).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void CalculaProvisao(int idCarteira, DateTime data)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);
            int idLocal = cliente.IdLocal.Value;

            TabelaProvisaoCollection tabelaProvisaoCollection = new TabelaProvisaoCollection();
            CalculoProvisaoCollection calculoProvisaoCollectionInsert = new CalculoProvisaoCollection();
            LiquidacaoCollection liquidacaoCollectionInsert = new LiquidacaoCollection();
            
            tabelaProvisaoCollection.BuscaTabelaProvisao(idCarteira, data);

            for (int i = 0; i < tabelaProvisaoCollection.Count; i++)
            {
                #region Carregamento das variáveis locais de TabelaProvisao
                TabelaProvisao tabelaProvisao = tabelaProvisaoCollection[i];
                int idTabela = tabelaProvisao.IdTabela.Value;
                int idCadastro = tabelaProvisao.IdCadastro.Value;
                int tipoCalculo = tabelaProvisao.TipoCalculo.Value; //Indica se provisiona ou é diferido
                decimal valorTotal = tabelaProvisao.ValorTotal.Value;
                int contagemDias = tabelaProvisao.ContagemDias.Value;
                int diaRenovacao = tabelaProvisao.DiaRenovacao.Value;
                int numeroMesesRenovacao = tabelaProvisao.NumeroMesesRenovacao.Value;
                #endregion

                #region Cálculo da provisão e da cpmf dia e inserção em Liquidacao
                CalculoProvisao calculoProvisao = new CalculoProvisao();
                //Se já tiver registro faz update, senão insert do primeiro registro                    
                DateTime dataFimApropriacao;
                decimal valorAcumulado = 0;
                DateTime dataPagamento = new DateTime();
                if (calculoProvisao.LoadByPrimaryKey(idTabela))
                {
                    dataFimApropriacao = calculoProvisao.DataFimApropriacao.Value;
                    dataPagamento = calculoProvisao.DataPagamento.Value;
                    
                    #region Apropria mais 1 dia - Atualiza ValorDia, ValorAcumulado
                    valorAcumulado = calculoProvisao.ValorAcumulado.Value;
                    int numeroDias;
                    int numeroDiasNaoUteis = 1;
                    if (contagemDias == (int)ContagemDiasProvisao.Uteis)
                    {
                        if (data > dataFimApropriacao)
                            throw new Exception("No cálculo de Provisão, a data de ínicio da corretagem é posterior a data fim da apropriação!");

                        if (idLocal != LocalFeriadoFixo.Brasil)
                            numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, idLocal, TipoFeriado.Outros);
                        else
                            numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    { //Para dias corridos, é preciso contar tb final de semana e feriado anterior
                        DateTime dataUtilAnterior  = new DateTime();
                        if (idLocal != LocalFeriadoFixo.Brasil)
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                        else
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                        DateTime dataInicioContagem = dataUtilAnterior;
                        if (DateTime.Compare(tabelaProvisao.DataReferencia.Value, dataUtilAnterior) > 0)
                            dataInicioContagem = tabelaProvisao.DataReferencia.Value;
                        else
                            dataInicioContagem = dataInicioContagem.AddDays(1);

                        if (dataInicioContagem > dataFimApropriacao)
                            throw new Exception("No cálculo de Provisão, a data de ínicio da corretagem é posterior a data fim da apropriação!");

                        numeroDias = Calendario.NumeroDias(dataInicioContagem, dataFimApropriacao);

                        if (dataUtilAnterior.Month != data.Month)
                        {
                            dataUtilAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                            numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            if (DateTime.Compare(tabelaProvisao.DataReferencia.Value, dataUtilAnterior) > 0)
                            {
                                dataUtilAnterior = tabelaProvisao.DataReferencia.Value;
                                numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            }
                        }
                        if (DateTime.Compare(dataUtilAnterior, data) < 0) numeroDiasNaoUteis = Calendario.NumeroDias(dataUtilAnterior, data);
                    }

                    decimal valorApropriar;
                    decimal valorDia;

                    if (numeroDias == 0)
                    {
                        valorApropriar = 0;
                        valorDia = 0;
                    }
                    else
                    {
                        //Faz os devidos cálculos para o caso de Provisão ou Diferido
                        if (tipoCalculo == (int)TipoCalculoProvisao.Provisao)
                        {
                            valorApropriar = valorTotal - valorAcumulado;
                            valorDia = Math.Round(valorApropriar / numeroDias, 2);
                            valorDia = valorDia * numeroDiasNaoUteis;
                            valorAcumulado += valorDia;                            
                        }
                        else
                        {
                            valorDia = Math.Round(valorAcumulado / numeroDias, 2);
                            valorDia = valorDia * numeroDiasNaoUteis;
                            valorAcumulado -= valorDia;                            
                        }
                    }

                    calculoProvisao.ValorDia = valorDia;
                    calculoProvisao.ValorAcumulado = valorAcumulado;
                    #endregion                    

                    //CHECA SE A DATAFIM APONTA PARA A DATA, SE APONTAR USA ELA!!!!
                    if (tabelaProvisao.DataFim.HasValue)
                    {
                        if (tabelaProvisao.DataFim.Value.CompareTo(data) == 0)
                        {
                            dataFimApropriacao = tabelaProvisao.DataFim.Value;
                            calculoProvisao.DataFimApropriacao = dataFimApropriacao;
                            calculoProvisao.ValorAcumulado = 0;
                        }
                    }
                                        
                    calculoProvisao.Save();
                }
                else
                {
                    //Cria novo registro, calculando as datas de fim de apropriação e de pagamento
                    #region Insert
                    int ano = data.Year;
                    int mes = data.Month;

                    //Se nada for especificado a respeito do dia de renovação, 
                    //assume-se como último dia do mês
                    if (diaRenovacao == 0)
                    {
                        if (idLocal != LocalFeriadoFixo.Brasil)
                            dataFimApropriacao = Calendario.RetornaUltimoDiaUtilMes(data, numeroMesesRenovacao, idLocal, TipoFeriado.Outros);
                        else
                            dataFimApropriacao = Calendario.RetornaUltimoDiaUtilMes(data, numeroMesesRenovacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);                                                    
                    }
                    else
                    {
                        dataFimApropriacao = new DateTime(ano, mes, 1);
                        dataFimApropriacao = dataFimApropriacao.AddMonths(numeroMesesRenovacao);
                        if (diaRenovacao > Calendario.RetornaUltimoDiaCorridoMes(dataFimApropriacao,0).Day)
                            diaRenovacao = Calendario.RetornaUltimoDiaCorridoMes(dataFimApropriacao,0).Day;
                        dataFimApropriacao = new DateTime(dataFimApropriacao.Year, dataFimApropriacao.Month, diaRenovacao);
                        if (idLocal != LocalFeriadoFixo.Brasil)
                        {
                            if (!Calendario.IsDiaUtil(dataFimApropriacao, idLocal, TipoFeriado.Outros))
                            {
                                dataFimApropriacao = Calendario.AdicionaDiaUtil(dataFimApropriacao, 1, idLocal, TipoFeriado.Outros);
                            }
                        }
                        else
                        {
                            if (!Calendario.IsDiaUtil(dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                            {
                                dataFimApropriacao = Calendario.AdicionaDiaUtil(dataFimApropriacao, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                        }
                    }

                    if (tabelaProvisao.DataFim.HasValue && tabelaProvisao.DataFim.Value.CompareTo(dataFimApropriacao) < 0)
                        dataFimApropriacao = tabelaProvisao.DataFim.Value;

                    int numeroDias;
                    int numeroDiasNaoUteis = 1;
                    if (contagemDias == (int)ContagemDiasProvisao.Uteis)
                    {
                        if (idLocal != LocalFeriadoFixo.Brasil)
                        {
                            numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, idLocal, TipoFeriado.Outros);
                        }
                        else
                        {
                            numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                    }
                    else
                    { //Para dias corridos, é preciso contar tb final de semana e feriado anterior
                        DateTime dataUtilAnterior;
                        if (idLocal != LocalFeriadoFixo.Brasil)
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                        }
                        else
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }

                        DateTime dataInicioContagem = dataUtilAnterior;
                        if (DateTime.Compare(tabelaProvisao.DataReferencia.Value, dataUtilAnterior) > 0)
                            dataInicioContagem = tabelaProvisao.DataReferencia.Value;
                        else
                            dataInicioContagem = dataInicioContagem.AddDays(1);

                        numeroDias = Calendario.NumeroDias(dataInicioContagem, dataFimApropriacao);

                        if (dataUtilAnterior.Month != data.Month)
                        {
                            dataUtilAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                            numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            if (DateTime.Compare(tabelaProvisao.DataReferencia.Value, dataUtilAnterior) > 0)
                            {
                                dataUtilAnterior = tabelaProvisao.DataReferencia.Value;
                                numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            }
                        }
                        if (DateTime.Compare(dataUtilAnterior, data) < 0) numeroDiasNaoUteis = Calendario.NumeroDias(dataUtilAnterior, data);

                    }

                    decimal valorApropriar;
                    decimal valorDia;                    
                    //Faz os devidos cálculos para o caso de Provisão ou Diferido
                    if (tipoCalculo == (int)TipoCalculoProvisao.Provisao)
                    {
                        valorApropriar = valorTotal;
                        valorDia = Math.Round(valorApropriar / numeroDias, 2);
                        valorDia = valorDia * numeroDiasNaoUteis;
                        valorAcumulado = valorDia;
                        //Calcula data de pagamento
                        int numeroDiasPagamento = tabelaProvisao.NumeroDiasPagamento.Value;
                        if (idLocal != LocalFeriadoFixo.Brasil)
                        {
                            dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, idLocal, TipoFeriado.Outros);
                        }
                        else
                        {
                            dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                    }
                    else
                    {
                        valorAcumulado = valorTotal;
                        valorDia = Math.Round(valorAcumulado / numeroDias, 2);
                        valorDia = valorDia * numeroDiasNaoUteis;
                        valorAcumulado -= valorDia;
                        dataPagamento = dataFimApropriacao; //Diferido ñ tem data de pagamento
                    }

                    #region Insert em CalculoProvisao
                    CalculoProvisao calculoProvisaoInsert = new CalculoProvisao();
                    calculoProvisaoInsert.AddNew();
                    calculoProvisaoInsert.IdTabela = idTabela;
                    calculoProvisaoInsert.IdCarteira = idCarteira;
                    calculoProvisaoInsert.ValorDia = valorDia;
                    calculoProvisaoInsert.ValorAcumulado = valorAcumulado;
                    calculoProvisaoInsert.DataFimApropriacao = dataFimApropriacao;
                    calculoProvisaoInsert.DataPagamento = dataPagamento;
                    calculoProvisaoInsert.ValorCPMFDia = 0;
                    calculoProvisaoInsert.ValorCPMFAcumulado = 0;

                    calculoProvisaoCollectionInsert.AttachEntity(calculoProvisaoInsert);
                    #endregion

                    #endregion
                }

                if (valorAcumulado != 0)
                {
                    if (tipoCalculo == (int)TipoCalculoProvisao.Provisao)
                    {
                        valorAcumulado = valorAcumulado * -1;                        
                    }

                    //Se for Diferido com dataFim na data, simplesmente não lança nada
                    if ((DateTime.Compare(data, dataFimApropriacao) != 0) ||
                        (tipoCalculo == (int)TipoCalculoProvisao.Provisao && tabelaProvisao.DataFim.HasValue && tabelaProvisao.DataFim.Value.CompareTo(data) == 0))
                    {
                        
                        //Busca o idContaDefault do cliente
                        Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                        int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                        //
                        #region Insert na Liquidacao (valor da provisão)
                        string descricao = tabelaProvisao.UpToCadastroProvisaoByIdCadastro.Descricao; //Hierarquico
                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.AddNew();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataPagamento;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = valorAcumulado;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.ProvisaoOutros;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.IdConta = idContaDefault;

                        liquidacaoCollectionInsert.AttachEntity(liquidacao);
                        #endregion
                    }
                }
                #endregion
            }

            //Save das collections
            calculoProvisaoCollectionInsert.Save();
            liquidacaoCollectionInsert.Save();
        }

        /// <summary>
        /// Checa provisões com fim de apropriação na data, projetando os valores da provisão 
        /// e CPMF (somente se > 01/01/2008) para a data de pagamento na Liquidacao.
        /// Zera a CalculoProvisao, calculando novas datas de fim de apropriação e pagamento.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void TrataVencimentoProvisao(int idCarteira, DateTime data)
        {
            CalculoProvisaoCollection calculoProvisaoCollection = new CalculoProvisaoCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            calculoProvisaoCollection.BuscaCalculoProvisaoVencidas(idCarteira, data);

            for (int i = 0; i < calculoProvisaoCollection.Count; i++)
            {
                #region Carregamento das variáveis locais de CalculoProvisao
                CalculoProvisao calculoProvisao = calculoProvisaoCollection[i];
                int idTabela = calculoProvisao.IdTabela.Value;
                DateTime dataPagamento = calculoProvisao.DataPagamento.Value;
                #endregion

                #region Busca informações da tabela de Provisão
                TabelaProvisao tabelaProvisao = new TabelaProvisao();
                tabelaProvisao.LoadByPrimaryKey(idTabela);

                int tipoCalculo = tabelaProvisao.TipoCalculo.Value;
                int diaRenovacao = tabelaProvisao.DiaRenovacao.Value;
                int numeroMesesRenovacao = tabelaProvisao.NumeroMesesRenovacao.Value;
                int numeroDiasPagamento = tabelaProvisao.NumeroDiasPagamento.Value;
                decimal valorTotal = tabelaProvisao.ValorTotal.Value;
                #endregion

                #region Lança em Liquidacao apenas valores de provisão (diferido simplesmente morre ao final do período)
                if (tipoCalculo != (int)TipoCalculoProvisao.Diferido)
                {
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                    //
                    #region Insert na Liquidacao (valor da provisão)
                    valorTotal = valorTotal * -1;
                    string descricao = tabelaProvisao.UpToCadastroProvisaoByIdCadastro.Descricao; //Hierarquico
                    descricao = "Pagamento " + descricao;
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.AddNew();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataPagamento;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorTotal;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoProvisaoOutros;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.IdConta = idContaDefault;

                    liquidacaoCollection.AttachEntity(liquidacao);
                    #endregion

                    #region Insert na Liquidacao (valor da CPMF)
                    DateTime dataCPMF = new DateTime(2008, 01, 01);
                    if (DateTime.Compare(data, dataCPMF) < 0)
                    {
                        decimal valorCPMF = Utilitario.Truncate(valorTotal * 0.38M / 100M, 2);
                        descricao = "CPMF s/ Pagamento " + descricao;
                        liquidacao = new Liquidacao();
                        liquidacao.AddNew();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataPagamento;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = valorCPMF;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.CPMF;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.IdConta = idContaDefault;

                        liquidacaoCollection.AttachEntity(liquidacao);
                    }
                    #endregion
                }
                #endregion

                //Zera os valores acumulados e diário (provisão e CPMF)
                calculoProvisao.ValorAcumulado = 0;
                calculoProvisao.ValorCPMFAcumulado = 0;
                calculoProvisao.ValorDia = 0;
                
                #region Checa se existe dataFim para a provisão
                DateTime dataFim;
                bool terminoProvisao = false;
                if (tabelaProvisao.DataFim.HasValue)
                {
                    dataFim = tabelaProvisao.DataFim.Value;
                    if (DateTime.Compare(dataFim, data) <= 0)
                    {
                        terminoProvisao = true;
                    }
                }
                #endregion

                if (!terminoProvisao)
                {
                    #region Atualiza datas de fim de apropriação e de pagamento
                    int ano = data.Year;
                    int mes = data.Month;

                    //Calcula nova data de fim de apropriação
                    DateTime dataFimApropriacao;

                    if (diaRenovacao == 0)
                    {//Se nada for especificado a respeito do dia de renovação, assume-se como último dia útil do mês
                        dataFimApropriacao = Calendario.RetornaUltimoDiaUtilMes(data.AddMonths(1), numeroMesesRenovacao,
                                                                                LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        mes = mes + numeroMesesRenovacao;
                        if (mes > 12)
                        {
                            ano += 1;
                            mes = mes - 12;
                        }

                        //Testar dia do mes valido (se dia for 29, nao existe 29 de fevereiro em todos os anos)                        
                        DateTime dataInicioMes = new DateTime(ano, mes , 1);
                        DateTime dataFimMes = dataInicioMes.AddMonths(1).AddDays(-1);

                        if (diaRenovacao > dataFimMes.Day)
                        {
                            dataFimApropriacao = dataFimMes;
                        }
                        else
                        {                            
                            dataFimApropriacao = new DateTime(ano, mes , diaRenovacao);
                        }

                        if (!Calendario.IsDiaUtil(dataFimApropriacao))
                        {
                            dataFimApropriacao = Calendario.AdicionaDiaUtil(dataFimApropriacao, 1);
                        }
                    }

                    //Calcula nova data de pagamento
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento,
                                                               LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    calculoProvisao.DataFimApropriacao = dataFimApropriacao;
                    calculoProvisao.DataPagamento = dataPagamento;
                    #endregion
                }
                else
                {
                    if (!tabelaProvisao.DataFim.HasValue)
                    {
                        tabelaProvisao.DataFim = data;
                    }
                    tabelaProvisao.Save();
                }
            }

            //Save das collections
            calculoProvisaoCollection.Save();
            liquidacaoCollection.Save();
        }            

	}
}
