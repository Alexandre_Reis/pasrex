/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/05/2015 13:26:36
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
	public partial class MediaMovelCollection : esMediaMovelCollection
	{
        public void DeletaMediaMovel(int idCliente, DateTime data)
        {
            this.Query.Where(this.Query.IdCliente.Equal(idCliente)
                             & this.Query.DataAtual.GreaterThanOrEqual(data));

            this.Query.Load();
            this.MarkAllAsDeleted();
            this.Save();
        }
	}
}
