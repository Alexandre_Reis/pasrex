﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2014 13:20:01
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
	public partial class CalculoAdministracaoAssociadaHistoricoCollection : esCalculoAdministracaoAssociadaHistoricoCollection
	{
        /// <summary>
        /// Carrega o objeto CalculoAdministracaoHistoricoCollection com todos os campos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaCalculoAdministracaoHistoricoCompleta(int idCarteira, DateTime dataHistorico)
        {

            this.Query
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCarteira == idCarteira);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Deleta todas as posições históricas com data >= que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaCalculoAdministracaoHistoricoDataHistoricoMaiorIgual(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdTabela, this.query.DataHistorico)
                    .Where(this.Query.IdCarteira == idCarteira,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
	}
}
