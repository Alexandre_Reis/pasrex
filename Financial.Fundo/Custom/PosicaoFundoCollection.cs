﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

using Financial.Investidor;
using Financial.Investidor.Enums;

namespace Financial.Fundo
{
    public partial class PosicaoFundoCollection : esPosicaoFundoCollection
    {
        // Construtor
        // Cria uma nova PosicaoFundoCollection com os dados de PosicaoFundoHistoricoCollection
        // Todos do dados de PosicaoFundoHistoricoCollection são copiados com excessão da dataHistorico
        public PosicaoFundoCollection(PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection) {
            for (int i = 0; i < posicaoFundoHistoricoCollection.Count; i++) {
                //
                PosicaoFundo p = new PosicaoFundo();

                // Para cada Coluna de PosicaoFundoHistorico copia para PosicaoFundo
                foreach (esColumnMetadata colPosicaoHistorico in posicaoFundoHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != PosicaoFundoHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colPosicaoFundo = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (posicaoFundoHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
                            p.SetColumn(colPosicaoFundo.Name, posicaoFundoHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdCarteira, ValorBruto.Sum.
        /// Group By IdCarteira
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoFundoAgrupadoEnquadra(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira, this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCliente == idCliente)
                 .GroupBy(this.Query.IdCarteira);

            bool retorno = this.Query.Load();

            return retorno;
        }
        
        /// <summary>
        /// Deleta todas as posições do idCliente.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaPosicaoFundo(int idCliente)
        {
            this.Query
                    .Select(this.Query.IdPosicao)
                    .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Método de inserção em PosicaoFundo, a partir de PosicaoFundoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InserePosicaoFundoHistorico(int idCliente, DateTime dataHistorico)
        {
            #region Copia de posicaoFundoHistorico para PosicaoFundo
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoFundo ON ");
            sqlBuilder.Append("INSERT INTO PosicaoFundo (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoFundoHistorico WHERE DataHistorico = " + "'" + dataHistorico.ToString("yyyyMMdd") + "' AND IdCliente = " + idCliente;
            int count = 0;

            PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
            int columnCount = posicaoFundoHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoFundo in posicaoFundoHistorico.es.Meta.Columns)
            {
                count++;

                if (colPosicaoFundo.Name == PosicaoFundoHistoricoMetadata.ColumnNames.DataHistorico)
                    continue;

                //Insert
                sqlBuilder.Append(colPosicaoFundo.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoFundo.Name;

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoFundo OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }


        /// <summary>
        /// Método de inserção em PosicaoFundo, a partir de PosicaoFundoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataAbertura"></param>
        public void InserePosicaoFundoAbertura(int idCliente, DateTime dataAbertura)
        {
            #region Copia de posicaoFundoHistorico para PosicaoFundo
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoFundo ON ");
            sqlBuilder.Append("INSERT INTO PosicaoFundo (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoFundoAbertura WHERE DataHistorico = " + "'" + dataAbertura.ToString("yyyyMMdd") + "' AND IdCliente = " + idCliente;
            int count = 0;

            PosicaoFundoAbertura posicaoFundoAbertura = new PosicaoFundoAbertura();
            int columnCount = posicaoFundoAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoFundo in posicaoFundoAbertura.es.Meta.Columns)
            {
                count++;

                if (colPosicaoFundo.Name == PosicaoFundoAberturaMetadata.ColumnNames.DataHistorico)
                    continue;

                //Insert
                sqlBuilder.Append(colPosicaoFundo.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoFundo.Name;

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoFundo OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com todos os campos de PosicaoFundo.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoFundoCompleta(int idCliente)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente);
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdPosicao, IdCarteira, CotaDia, 
        /// ValorBruto, Quantidade, ValorLiquido.
        /// Quantidade.GreaterThan(0).
        /// OrderBy(this.Query.IdCarteira).
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoOrdenada(int idCliente)
        {
            this.QueryReset();
            this.Query.Select()
                      .Where(this.Query.IdCliente == idCliente, this.Query.Quantidade.GreaterThan(0))
                      .OrderBy(this.Query.IdCarteira.Ascending);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com o campo IdCarteira.
        /// Usa this.Query.es.Distinct = true;
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoDistinctFundo(int idCliente)
        {
            this.QueryReset();
            this.Query.Select(this.Query.IdCarteira)
                      .Where(this.Query.IdCliente == idCliente);

            this.Query.es.Distinct = true;

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os todos os campos de PosicaoFundoCollection.
        /// Filtra com Quantidade diferente de zero.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoFundo(int idCliente)
        {
            return this.BuscaPosicaoFundo(idCliente, false);
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os todos os campos de PosicaoFundoCollection.
        /// Filtra com Quantidade diferente de zero.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="incluirZerada"></param>        
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoFundo(int idCliente, bool incluirZerada)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente);

            if (!incluirZerada)
                this.Query.Where(this.Query.Quantidade.NotEqual(0));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes, PosicaoIncorporada.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena ascendente pela data de conversão.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="idCarteira"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoFundoFIFO(int idCliente, int idCarteira)
        {
            return BuscaPosicaoFundoFIFO(idCliente, idCarteira, null);
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes, PosicaoIncorporada.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena ascendente pela data de conversão.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="idCarteira"></param>   
        /// <param name="lstIdPosicoesDebitadas"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoFundoFIFO(int idCliente, int idCarteira, List<int> lstPosicoesJaCarregadas)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.IdOperacao, this.Query.DataAplicacao, this.Query.DataConversao,
                         this.Query.CotaAplicacao, this.Query.CotaDia, this.Query.Quantidade,
                         this.Query.DataUltimaCobrancaIR, this.Query.ValorIOFVirtual,
                         this.Query.ValorBruto, this.Query.ValorLiquido,
                         this.Query.ValorIOF, this.Query.ValorIR, this.Query.ValorPerformance,
                         this.Query.QuantidadeAntesCortes, this.Query.PosicaoIncorporada, this.Query.QuantidadeBloqueada)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdCarteira == idCarteira,
                        this.Query.Quantidade.NotEqual(0))
                 .OrderBy(this.Query.DataConversao.Ascending);

            if (lstPosicoesJaCarregadas != null && lstPosicoesJaCarregadas.Count > 0)
                this.Query.Where(this.Query.IdPosicao.NotIn(lstPosicoesJaCarregadas.ToArray()));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes, PosicaoIncorporada.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena descendente pela data de conversão.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="idCarteira"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoFundoLIFO(int idCliente, int idCarteira)
        {
            return BuscaPosicaoFundoLIFO(idCliente, idCarteira, null);
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes, PosicaoIncorporada.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena descendente pela data de conversão.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="idCarteira"></param>   
        /// <param name="lstIdPosicoesDebitadas"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoFundoLIFO(int idCliente, int idCarteira, List<int> lstPosicoesJaCarregadas)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.IdOperacao, this.Query.DataAplicacao, this.Query.DataConversao,
                         this.Query.CotaAplicacao, this.Query.CotaDia, this.Query.Quantidade,
                         this.Query.DataUltimaCobrancaIR, this.Query.ValorIOFVirtual,
                         this.Query.ValorBruto, this.Query.ValorLiquido,
                         this.Query.ValorIOF, this.Query.ValorIR, this.Query.ValorPerformance,
                         this.Query.QuantidadeAntesCortes, this.Query.PosicaoIncorporada, this.Query.QuantidadeBloqueada)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdCarteira == idCarteira,
                        this.Query.Quantidade.NotEqual(0))
                 .OrderBy(this.Query.DataConversao.Descending);

            if (lstPosicoesJaCarregadas != null && lstPosicoesJaCarregadas.Count > 0)
                this.Query.Where(this.Query.IdPosicao.NotIn(lstPosicoesJaCarregadas.ToArray()));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes, PosicaoIncorporada.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena ascendente pela soma de IR + IOF.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="idCarteira"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoFundoMenorImposto(int idCliente, int idCarteira)
        {
            return BuscaPosicaoFundoLIFO(idCliente, idCarteira, null);
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdPosicao, DataAplicacao, DataConversao,
        /// CotaAplicacao, CotaDia, Quantidade, DataUltimaCobrancaIR, ValorIOFVirtual, ValorBruto, ValorLiquido,
        /// ValorIOF, ValorIR, ValorPerformance, QuantidadeAntesCortes, PosicaoIncorporada.
        /// Filtra com Quantidade diferente de zero.
        /// Ordena ascendente pela soma de IR + IOF.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="idCarteira"></param>   
        /// <param name="lstIdPosicoesDebitadas"></param>   
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoFundoMenorImposto(int idCliente, int idCarteira, List<int> lstPosicoesJaCarregadas)
        {
            #region Montagem do OrderBy ->> colunas = "(ValorIR + ValorIOF) / Quantidade)"
            StringBuilder colunas = new StringBuilder();
            string valorIr = PosicaoFundoMetadata.ColumnNames.ValorIR;
            string valorIOF = PosicaoFundoMetadata.ColumnNames.ValorIOF;
            string quantidade = PosicaoFundoMetadata.ColumnNames.Quantidade;
            colunas = colunas.Append("<( [").Append(valorIr).Append("] + [").Append(valorIOF).Append("] )").Append("/ [")
                             .Append(quantidade).Append("] As Impostos>");
            #endregion

            this.QueryReset();
            this.Query
                 .Select(colunas.ToString(), this.Query.IdPosicao, this.Query.IdOperacao, this.Query.DataAplicacao, this.Query.DataConversao,
                         this.Query.CotaAplicacao, this.Query.CotaDia, this.Query.Quantidade,
                         this.Query.DataUltimaCobrancaIR, this.Query.ValorIOFVirtual,
                         this.Query.ValorBruto, this.Query.ValorLiquido,
                         this.Query.ValorIOF, this.Query.ValorIR, this.Query.ValorPerformance,
                         this.Query.QuantidadeAntesCortes, this.Query.PosicaoIncorporada, this.Query.QuantidadeBloqueada)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdCarteira == idCarteira,
                        this.Query.Quantidade.NotEqual(0))
                 .OrderBy("Impostos", esOrderByDirection.Ascending);

            if (lstPosicoesJaCarregadas != null && lstPosicoesJaCarregadas.Count > 0)
                this.Query.Where(this.Query.IdPosicao.NotIn(lstPosicoesJaCarregadas.ToArray()));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdCarteira, Quantidade.Sum, CotaDia.Avg, CotaAplicacao.Avg, 
        /// ValorBruto.Sum, ValorLiquido.Sum.
        /// Group By IdCarteira
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoFundoAgrupado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira,
                         this.Query.Quantidade.Sum(),
                         this.Query.CotaDia.Avg(),
                         this.Query.CotaAplicacao.Avg(),
                         this.Query.ValorBruto.Sum(),
                         this.Query.ValorLiquido.Sum(),
                         this.Query.ValorAplicacao.Sum())
                 .Where(this.Query.IdCliente == idCliente)
                 .GroupBy(this.Query.IdCarteira);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// /// Calcula quantidade que foi convertida, porém não houve liquidação financeira (Resgate)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaPendenciaLiquidacao(int idCliente, DateTime data)
        {
            DetalheResgateFundoCollection detalheColl = new DetalheResgateFundoCollection();
            DetalheResgateFundoQuery detalheQuery = new DetalheResgateFundoQuery("detalhe");
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("operacao");

            detalheQuery.Select(detalheQuery.Quantidade.Sum(), detalheQuery.ValorLiquido.Sum(), detalheQuery.IdPosicaoResgatada);
            detalheQuery.InnerJoin(operacaoFundoQuery).On(detalheQuery.IdOperacao.Equal(operacaoFundoQuery.IdOperacao));
            detalheQuery.Where(operacaoFundoQuery.DataLiquidacao.GreaterThan(data));
            detalheQuery.GroupBy(detalheQuery.IdPosicaoResgatada);

            List<DetalheResgateFundo> lstDetalheResgateFundo = new List<DetalheResgateFundo>();
            if (detalheColl.Load(detalheQuery) && detalheColl.Count > 0)
                lstDetalheResgateFundo = (List<DetalheResgateFundo>)detalheColl;
            else
                return;

            PosicaoFundoCollection posicaoFundoColl = new PosicaoFundoCollection();            
            if (!posicaoFundoColl.BuscaPosicaoFundo(idCliente, true))
                return;

            foreach (PosicaoFundo posicaoFundo in posicaoFundoColl)
            {
                decimal qtdePendente = 0;
                decimal valorPendente = 0;
                int idPosicao = posicaoFundo.IdPosicao.Value;

                DetalheResgateFundo detalheResgateFundo = lstDetalheResgateFundo.Find(delegate(DetalheResgateFundo x) { return x.IdPosicaoResgatada == idPosicao; });
                if (detalheResgateFundo != null && posicaoFundo.IdPosicao > 0)
                {
                    qtdePendente = Convert.ToDecimal(detalheResgateFundo.GetColumn("Quantidade"));
                    valorPendente = Convert.ToDecimal(detalheResgateFundo.GetColumn("ValorLiquido"));
                }

                posicaoFundo.QtdePendenteLiquidacao = qtdePendente;
                posicaoFundo.ValorPendenteLiquidacao = valorPendente;
            }

            posicaoFundoColl.Save();
        }

        public string SelecionaPerfilSuitability(int idCliente)
        {
            List<string> listaPerfilApurado = new List<string>();

            //Seleciona total do fundo
            this.Query.Select(this.Query.ValorBruto.Sum());
            this.Query.Where(this.Query.IdCliente.Equal(idCliente));
            this.Query.Load();
            decimal valorTotalCarteira = this[0].ValorBruto.GetValueOrDefault(0);

            if (valorTotalCarteira == 0)
                return "Indefinido";

            //Seleciona Regras de apuração
            SuitabilityApuracaoRiscoCollection suitabilityApuracaoRiscoCollection = new SuitabilityApuracaoRiscoCollection();
            suitabilityApuracaoRiscoCollection.LoadAll();

            //Seleciona totais por perfil
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            this.Query.Select(carteiraCollection.Query.PerfilRisco.As("idPerfil"), this.Query.ValorBruto.Sum());
            this.Query.InnerJoin(carteiraCollection.Query).On(this.Query.IdCarteira.Equal(carteiraCollection.Query.IdCarteira));
            this.Query.Where(this.Query.IdCliente.Equal(idCliente));
            this.Query.GroupBy(carteiraCollection.Query.PerfilRisco);
            this.Query.Load();

            if (this.Count == 0)
                return "Indefinido";

            SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
            foreach (PosicaoFundo posicaoFundo in this)
            {
                if (posicaoFundo.GetColumn("idPerfil").Equals(null))
                    continue;

                decimal valorPosicao = posicaoFundo.ValorBruto.Value;
                decimal percentualPosicao = (valorPosicao / valorTotalCarteira) * 100;
                int idPerfil = Convert.ToInt16(posicaoFundo.GetColumn("idPerfil"));
                suitabilityPerfilInvestidor.LoadByPrimaryKey(idPerfil);

                //Verifica em qual regra a posicao consolidada por perfil se encaixa
                foreach (SuitabilityApuracaoRisco suitabilityApuracaoRisco in suitabilityApuracaoRiscoCollection)
                {
                    List<SuitabilityPerfilInvestidor> listaPerfilFundo = suitabilityApuracaoRisco.ListaPerfilRiscoFundo();

                    if (listaPerfilFundo.Contains(suitabilityPerfilInvestidor))
                    {
                        if (suitabilityApuracaoRisco.Criterio.Equals(SuitabilityCriterio.Igual))
                        {
                            if (percentualPosicao == suitabilityApuracaoRisco.Percentual)
                                listaPerfilApurado.Add(suitabilityApuracaoRisco.UpToSuitabilityPerfilInvestidorByPerfilCarteira.Perfil);
                        }
                        if (suitabilityApuracaoRisco.Criterio.Equals(SuitabilityCriterio.MaiorIgual))
                        {
                            if (percentualPosicao >= suitabilityApuracaoRisco.Percentual)
                                listaPerfilApurado.Add(suitabilityApuracaoRisco.UpToSuitabilityPerfilInvestidorByPerfilCarteira.Perfil);
                        }
                        if (suitabilityApuracaoRisco.Criterio.Equals(SuitabilityCriterio.MenorIgual))
                        {
                            if (percentualPosicao >= suitabilityApuracaoRisco.Percentual)
                                listaPerfilApurado.Add(suitabilityApuracaoRisco.UpToSuitabilityPerfilInvestidorByPerfilCarteira.Perfil);
                        }
                    }
                }
            }
            return "";
        }

    }
}
