﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           6/3/2007 15:10:41
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
	public partial class CalculoProvisaoHistorico : esCalculoProvisaoHistorico
	{
        /// <summary>
        /// Adiciona uma coluna Virtual dinamicamente
        /// O Tipo da Coluna adicionado é considerado como sendo String
        /// </summary>
        /// <param name="column"></param>
        public void AddVirtualColumn(string column)
        {
            //this.Table = new DataTable();
            if (this.Table != null && !this.Table.Columns.Contains(column))
            {
                this.Table.Columns.Add(column, typeof(System.String));
            }
        }
	}
}
