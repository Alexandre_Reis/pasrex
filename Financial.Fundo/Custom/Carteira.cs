using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Exceptions;
using log4net;
using Financial.Fundo.Enums;
using Financial.Util;
using Financial.Investidor;
using Financial.Interfaces.Import.RendaFixa;
using Financial.ContaCorrente;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using System.IO;
using Financial.Interfaces.Import.Fundo;
using Financial.Common;
using Financial.CRM;
using System.Globalization;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using Financial.ContaCorrente.Enums;
using Financial.BMF.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Interfaces.Import.YMF;
using Financial.InvestidorCotista.Enums;
using Financial.Interfaces.Export.Fundo;
using Financial.Investidor.Controller;
using System.Data.SqlClient;
using Financial.InvestidorCotista;
using System.Configuration;
using Financial.Swap;
using Financial.Security;
using Financial.Bolsa.Exceptions;
using Financial.Interfaces.Import.Offshore;
using Financial.Util.Enums;

namespace Financial.Fundo
{
    public partial class Carteira : esCarteira
    {
        const int ID_INDICE_BENCHMARK = 1;

        public class BooleanosFluxoCaixa
        {
            public bool RendaFixa;
            public bool ResgatesNaoConvertidos;
            public bool PosicaoFundos;
            public bool PosicaoBolsa;
        }

        public class ConfigXMLAnbima
        {
            public bool ImportaCota;
            public bool ImportaBolsa;
            public bool ImportaBTC;
            public bool ImportaRendaFixa;
            public bool ImportaFundos;
            public bool ImportaLiquidacao;
        }

        public class CarteiraDiaria
        {
            private HistoricoCota historicoCota;
            private PosicaoBolsaCollection posicaoBolsaCollection;
            private PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection;
            private PosicaoBMFCollection posicaoBMFCollection;
            private PosicaoBMFCollection posicaoBMFOpcoesCollection;
            private PosicaoTermoBolsaCollection posicaoTermoBolsaCollection;
            private PosicaoFundoCollection posicaoFundoCollection;
            private PosicaoBolsaCollection posicaoBolsaOpcoesCollection;
            private PosicaoRendaFixaCollection posicaoRendaFixaPublicoCollection;
            private PosicaoRendaFixaCollection posicaoRendaFixaPrivadoCollection;
            private PosicaoRendaFixaCollection posicaoRendaFixaDebentureCollection;
            private LiquidacaoCollection liquidacaoCollection;
            private PosicaoSwapCollection posicaoSwapCollection;

            private SaldoCaixa saldoCaixa; //Caso se queira consolidar o caixa
            private SaldoCaixaCollection saldoCaixaCollection; //Caso se queira trabalhar o caixa conta a conta

            public HistoricoCota HistoricoCota
            {
                get { return historicoCota; }
                set { historicoCota = value; }
            }
            public PosicaoBolsaCollection PosicaoBolsaCollection
            {
                get { return posicaoBolsaCollection; }
                set { posicaoBolsaCollection = value; }
            }
            public PosicaoEmprestimoBolsaCollection PosicaoEmprestimoBolsaCollection
            {
                get { return posicaoEmprestimoBolsaCollection; }
                set { posicaoEmprestimoBolsaCollection = value; }
            }
            public PosicaoBMFCollection PosicaoBMFCollection
            {
                get { return posicaoBMFCollection; }
                set { posicaoBMFCollection = value; }
            }
            public PosicaoBMFCollection PosicaoBMFOpcoesCollection
            {
                get { return posicaoBMFOpcoesCollection; }
                set { posicaoBMFOpcoesCollection = value; }
            }
            public PosicaoTermoBolsaCollection PosicaoTermoBolsaCollection
            {
                get { return posicaoTermoBolsaCollection; }
                set { posicaoTermoBolsaCollection = value; }
            }
            public PosicaoFundoCollection PosicaoFundoCollection
            {
                get { return posicaoFundoCollection; }
                set { posicaoFundoCollection = value; }
            }
            public PosicaoBolsaCollection PosicaoBolsaOpcoesCollection
            {
                get { return posicaoBolsaOpcoesCollection; }
                set { posicaoBolsaOpcoesCollection = value; }
            }
            public PosicaoRendaFixaCollection PosicaoRendaFixaPublicoCollection
            {
                get { return posicaoRendaFixaPublicoCollection; }
                set { posicaoRendaFixaPublicoCollection = value; }
            }
            public PosicaoRendaFixaCollection PosicaoRendaFixaPrivadoCollection
            {
                get { return posicaoRendaFixaPrivadoCollection; }
                set { posicaoRendaFixaPrivadoCollection = value; }
            }
            public PosicaoRendaFixaCollection PosicaoRendaFixaDebentureCollection
            {
                get { return posicaoRendaFixaDebentureCollection; }
                set { posicaoRendaFixaDebentureCollection = value; }
            }
            public LiquidacaoCollection LiquidacaoCollection
            {
                get { return liquidacaoCollection; }
                set { liquidacaoCollection = value; }
            }

            public PosicaoSwapCollection PosicaoSwapCollection
            {
                get { return posicaoSwapCollection; }
                set { posicaoSwapCollection = value; }
            }

            public SaldoCaixa SaldoCaixa
            {
                get { return saldoCaixa; }
                set { saldoCaixa = value; }
            }

            public SaldoCaixaCollection SaldoCaixaCollection
            {
                get { return saldoCaixaCollection; }
                set { saldoCaixaCollection = value; }
            }


        }

        private SIAnbid.Fundo fundoSIAnbid;
        public SIAnbid.Fundo FundoSIAnbid
        {
            get
            {
                return this.fundoSIAnbid;
            }
        }

        private HistoricoCotaCollection cotas;

        /// <summary>
        /// True se ativo. False se não ativo.
        /// </summary>
        public bool IsAtivo
        {
            get { return this.StatusAtivo == (int)StatusAtivoCarteira.Ativo; }
        }

        /// <summary>
        /// Carrega o objeto Carteira com os campos CalculaIOF, CasasDecimaisQuantidade, CotaInicial,
        /// IdAgenteAdministrador, TipoCota, TipoCusto, TipoTributacao, TruncaQuantidade, TruncaFinanceiro,
        /// DiasCotizacaoResgate, DiasLiquidacaoResgate, ContagemDiasConversaoResgate.
        /// CarteiraNaoCadastradaException para 0 registro.
        /// </summary>
        /// <param name="idCarteira"></param>
        public void BuscaCarteiraCalculoTributo(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CalculaIOF, this.Query.CasasDecimaisQuantidade, this.Query.CotaInicial,
                         this.Query.IdAgenteAdministrador, this.Query.TipoCota, this.Query.TipoCusto,
                         this.Query.TipoTributacao, this.Query.TruncaQuantidade, this.Query.TruncaFinanceiro,
                         this.Query.DiasCotizacaoResgate, this.Query.DiasLiquidacaoResgate, this.Query.ContagemDiasConversaoResgate,
                         this.Query.ContagemPrazoIOF, this.Query.DiasAposComeCotasIR, this.Query.DiasAposResgateIR, this.Query.ProjecaoIRComeCotas,
                         this.Query.ProjecaoIRResgate, this.Query.CompensacaoPrejuizo, this.Query.RealizaCompensacaoDePrejuizo,
                         this.Query.Fie)
                 .Where(this.Query.IdCarteira == idCarteira);

            this.Query.Load();

            if (!this.es.HasData)
            {
                throw new CarteiraNaoCadastradaException("Carteira " + idCarteira + " não cadastrada.");
            }
        }

        /// <summary>
        /// Retorna o tipo de custo da carteira (médio ou por aplicação).
        /// CarteiraNaoCadastradaException para 0 registro.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <returns>Tipo de cota</returns>
        public int BuscaTipoCusto(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.TipoCusto)
                 .Where(this.Query.IdCarteira == idCarteira);

            this.Query.Load();

            if (!this.es.HasData)
            {
                throw new CarteiraNaoCadastradaException("Carteira " + idCarteira + " não cadastrada.");
            }

            return this.TipoCusto.Value;
        }

        /// <summary>
        /// Retorna o tipo de tributação da carteira (Ações, RendaFixa CurtoPrazo, RendaFixa LongoPrazo).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <exception cref="CarteiraNaoCadastradaException">CarteiraNaoCadastradaException para 0 registro</exception>
        /// <returns>Tipo Tributacao</returns>
        public int BuscaTipoTributacao(int idCarteira)
        {

            this.QueryReset();
            this.Query
                 .Select(this.Query.TipoTributacao)
                 .Where(this.Query.IdCarteira == idCarteira);

            this.Query.Load();

            if (!this.es.HasData)
            {
                throw new CarteiraNaoCadastradaException("Carteira " + idCarteira + " não cadastrada.");
            }

            return this.TipoTributacao.Value;
        }

        /// <summary>
        /// Retorna o tipo de rentabilidade da carteira (30 a 30 ou 1o a 1o).
        /// CarteiraNaoCadastradaException para 0 registro.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <returns>TipoRentabilidade</returns>
        public int BuscaTipoRentabilidade(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.TipoRentabilidade)
                 .Where(this.Query.IdCarteira == idCarteira);

            this.Query.Load();

            if (!this.es.HasData)
            {
                throw new CarteiraNaoCadastradaException("Carteira " + idCarteira + " não cadastrada.");
            }

            return this.TipoRentabilidade.Value;
        }

        /// <summary>
        /// Retorna booleano indicando se o Fundo possui posição em algum mercado em determinada data
        /// </summary>
        /// <param name="idCarteira"></param>   
        /// <param name="data"></param>    
        /// <returns></returns>
        public bool isPossuiPosicao(int idCarteira, DateTime data)
        {
            int totalPosicao = 0;

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
            PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
            LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            

            posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                            posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoRendaFixaHistoricoCollection.Query.es.Top = 1;
            posicaoRendaFixaHistoricoCollection.Query.Load();
            totalPosicao = posicaoRendaFixaHistoricoCollection.Count;
            if (totalPosicao > 0)
            {
                return true;
            }

            posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoFundoHistoricoCollection.Query.es.Top = 1; 
            posicaoFundoHistoricoCollection.Query.Load();
            totalPosicao = posicaoFundoHistoricoCollection.Count;
            if (totalPosicao > 0)
            {
                return true;
            }

            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoBolsaHistoricoCollection.Query.es.Top = 1;
            posicaoBolsaHistoricoCollection.Query.Load();
            totalPosicao = posicaoBolsaHistoricoCollection.Count;
            if (totalPosicao > 0)
            {
                return true;
            }

            posicaoBMFHistoricoCollection.Query.Where(posicaoBMFHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                      posicaoBMFHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoBMFHistoricoCollection.Query.es.Top = 1;
            posicaoBMFHistoricoCollection.Query.Load();
            totalPosicao = posicaoBMFHistoricoCollection.Count;
            if (totalPosicao > 0)
            {
                return true;
            }

            posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                                  posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoEmprestimoBolsaHistoricoCollection.Query.es.Top = 1;
            posicaoEmprestimoBolsaHistoricoCollection.Query.Load();
            totalPosicao = posicaoEmprestimoBolsaHistoricoCollection.Count;
            if (totalPosicao > 0)
            {
                return true;
            }

            posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                             posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoTermoBolsaHistoricoCollection.Query.es.Top = 1;
            posicaoTermoBolsaHistoricoCollection.Query.Load();
            totalPosicao = posicaoTermoBolsaHistoricoCollection.Count;
            if (totalPosicao > 0)
            {
                return true;
            }

            posicaoSwapHistoricoCollection.Query.Where(posicaoSwapHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                       posicaoSwapHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoSwapHistoricoCollection.Query.es.Top = 1;
            posicaoSwapHistoricoCollection.Query.Load();
            totalPosicao = posicaoSwapHistoricoCollection.Count;
            if (totalPosicao > 0)
            {
                return true;
            }

            liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                      liquidacaoHistoricoCollection.Query.DataHistorico.Equal(data));
            liquidacaoHistoricoCollection.Query.es.Top = 1;
            liquidacaoHistoricoCollection.Query.Load();
            totalPosicao = liquidacaoHistoricoCollection.Count;
            if (totalPosicao > 0)
            {
                return true;
            }

            saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCarteira),
                                             saldoCaixaCollection.Query.Data.Equal(data));
            saldoCaixaCollection.Query.Load();
            totalPosicao = saldoCaixaCollection.Count;
            if (totalPosicao > 0)
            {
                return true;
            }
            return false;
        }



        /// <summary>
        /// Retorna booleano indicando se a carteira calcula IOF para os cotistas.
        /// </summary>
        /// <returns></returns>
        public bool IsCalculaIOF()
        {
            return this.CalculaIOF == "S";
        }

        public bool InitFundoSIAnbid()
        {
            this.fundoSIAnbid = new SIAnbid.Fundo();
            return this.fundoSIAnbid.LoadByCodFundo(this.CodigoAnbid, null, true);
        }

        public void ImportaCotasSIAnbid()
        {
            this.ImportaCotasSIAnbid(null);
        }

        /// <summary>
        /// Executa a importacao de cotas a partir do sistema SIAnbid
        /// O parametro de data é opcional. Caso seja especificado, serão
        /// importadas todas as cotas a partir da data passada (útil para corrigir cotas históricas)
        /// Caso a data não seja passada, serão importadas todas as cotas futuras do fundo
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ImportaCotasSIAnbid(DateTime? dataInicio)
        {
            if (string.IsNullOrEmpty(this.CodigoAnbid))
            {
                throw new Exception("Fundo " + this.IdCarteira + " não possui Código Anbima definido");
            }

            //Carregar historico cota para pegar a cota mais recente e checar por duplicatas antes de inserir
            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
            historicoCotaCollection.LoadByIdCarteira(this.IdCarteira.Value, "Data", esOrderByDirection.Descending);
            bool temHistoricoCota = historicoCotaCollection.Count > 0;

            if (!dataInicio.HasValue)
            {
                //Se nao existe uma dataInicio informada, tentar carregar cotas futuras.
                if (temHistoricoCota)
                {
                    dataInicio = historicoCotaCollection[0].Data.Value;
                    dataInicio = dataInicio.Value.AddDays(1);
                }
            }

            SIAnbid sianbid = new SIAnbid();
            List<SIAnbid.Cota> cotas = sianbid.RetornaCotas(this.CodigoAnbid, dataInicio);

            bool saveHistoricoCotaCollection = false;
            DateTime minDateCota = new DateTime(2400, 1, 1);
            foreach (SIAnbid.Cota cota in cotas)
            {
                bool deferSave = true;
                HistoricoCota historicoCota = null;
                if (temHistoricoCota)
                {
                    historicoCota = historicoCotaCollection.FindByPrimaryKey(cota.Data, this.IdCarteira.Value);
                }
                else
                {
                    if (cota.Data < minDateCota)
                    {
                        minDateCota = cota.Data;
                    }
                }

                if (historicoCota == null)
                {
                    historicoCota = new HistoricoCota();
                    deferSave = false;
                }
                else
                {
                    saveHistoricoCotaCollection = true;
                }

                historicoCota.Save(this.IdCarteira.Value, cota.Data, cota.Valor, cota.PL, deferSave);
            }

            if (saveHistoricoCotaCollection)
            {
                historicoCotaCollection.Save();
            }

            if (!String.IsNullOrEmpty(this.CodigoBloomberg))
            {
                #region Faz a carga complementar via site da Bloomberg
                HistoricoCota historicoCotaMax = new HistoricoCota();
                historicoCotaMax.Query.Select(historicoCotaMax.Query.Data.Max());
                historicoCotaMax.Query.Where(historicoCotaMax.Query.IdCarteira.Equal(this.IdCarteira.Value));
                historicoCotaMax.Query.Load();

                if (historicoCotaMax.Data.HasValue)
                {
                    DateTime dataInicioImport = Calendario.AdicionaDiaUtil(historicoCotaMax.Data.Value, 1);
                    DateTime dataFimImport = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                    if (dataInicioImport <= dataFimImport)
                    {
                        string[] bloombergString = this.CodigoBloomberg.Split(':');
                        string codigoBloomberg = bloombergString[0];
                        string extensao = bloombergString[1];

                        List<string> listaAtivos = new List<string>();
                        List<string> listaEspec = new List<string>();
                        listaAtivos.Add(codigoBloomberg);
                        listaEspec.Add(extensao);

                        CotacaoBloomberg cotacaoBloombergExec = new CotacaoBloomberg();
                        List<CotacaoBloomberg> listaCotacao = cotacaoBloombergExec.CarregaCotacaoBloomberg(listaAtivos, listaEspec, dataInicioImport, dataFimImport);

                        foreach (CotacaoBloomberg cotacaoBloomberg in listaCotacao)
                        {
                            DateTime dataCota = cotacaoBloomberg.Data;
                            decimal cotaDia = cotacaoBloomberg.Pu;

                            HistoricoCota historicoCota = new HistoricoCota();
                            historicoCota.Save(this.IdCarteira.Value, dataCota, cotaDia, 0, false);
                        }
                    }
                }
                #endregion
            }

            //Caso nao existam cotas carregadas, ajustar data inicio cota
            if (!temHistoricoCota)
            {
                this.DataInicioCota = minDateCota;
                this.Save();
            }
        }

        /// <summary>
        /// Retorna booleano indicando se deve ser calculada Taxa de Fiscalização CVM para a carteira.
        /// </summary>
        /// <returns></returns>
        public bool IsCobraTaxaFiscalizacaoCVM()
        {
            return this.CobraTaxaFiscalizacaoCVM == "S";
        }

        /// <summary>
        /// Retorna booleano indicando se trunca a Quantidade de Cotas.
        /// </summary>
        /// <returns></returns>
        public bool IsTruncaQuantidade()
        {
            return this.TruncaQuantidade == "S";
        }

        /// <summary>
        /// Retorna booleano indicando se trunca a o Valor da Cota.
        /// </summary>
        /// <returns></returns>
        public bool IsTruncaCota()
        {
            return this.TruncaCota == "S";
        }

        /// <summary>
        /// Retorna booleano indicando se trunca a o Valor do Saldo Financeiro.
        /// </summary>
        /// <returns></returns>
        public bool IsTruncaFinanceiro()
        {
            return this.TruncaFinanceiro == "S";
        }

        /// <summary>
        /// Carrega o objeto Carteira com os campos CobraTaxaFiscalizacaoCVM, TipoCVM.
        /// CarteiraNaoCadastradaException para 0 registro.
        /// </summary>
        /// <param name="idCarteira"></param>        
        public void BuscaCalculoTaxaFiscalizacaoCVM(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CobraTaxaFiscalizacaoCVM, this.Query.TipoCVM)
                 .Where(this.Query.IdCarteira == idCarteira);

            this.Query.Load();

            if (!this.es.HasData)
            {
                throw new CarteiraNaoCadastradaException("Carteira " + idCarteira + " não cadastrada.");
            }
        }

        /// <summary>
        /// Retorna booleano indicando se o Tipo Cota da carteira é Abertura
        /// </summary>
        /// <returns></returns>
        public bool IsTipoCotaAbertura()
        {
            return this.TipoCota == (int)TipoCotaFundo.Abertura;
        }

        /// <summary>
        /// Retorna booleano indicando se o Tipo Cota da carteira é Fechamento
        /// </summary>
        /// <returns></returns>
        public bool IsTipoCotaFechamento()
        {
            return this.TipoCota == (int)TipoCotaFundo.Fechamento;
        }

        /// <summary>
        /// Retorna booleano indicando se o Tipo de Fundo é FDIC
        /// </summary>
        /// <returns></returns>
        public bool IsFDIC()
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdTipo);
            cliente.LoadByPrimaryKey(campos, this.IdCarteira.Value);

            return cliente.IsTipoClienteFDIC();
        }

        /// <summary>
        /// Retorna Tipo FDIC (Fechado ou Aberto)
        /// </summary>
        /// <returns></returns>
        public TipoFDIC TipoFDIC
        {
            get
            {
                TipoFDIC tipoFDIC;

                if (this.TipoTributacao == (byte)TipoTributacaoFundo.LPrazo_SemComeCotas ||
                    this.TipoTributacao == (byte)TipoTributacaoFundo.CPrazo_SemComeCotas)
                {
                    tipoFDIC = TipoFDIC.Fechado;
                }
                else
                {
                    tipoFDIC = TipoFDIC.Aberto;
                }
                return tipoFDIC;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCarteiraReplicar"></param>
        /// <returns>Clone do Objeto Carteira com idCarteiraReplicar</returns>
        public Carteira ReplicaCarteira(int idCarteira, int idCarteiraReplicar)
        {
            Carteira c = new Carteira();
            c.LoadByPrimaryKey(idCarteiraReplicar);
            c.MarkAllColumnsAsDirty(DataRowState.Added);
            c.IdCarteira = idCarteira;
            //
            return c;
        }

        /// <summary>
        /// Testa se data é maior que a data atual da carteira, se for retorna a data atual da carteira.
        /// Testa se data é menor que a data de inicio da cota, se for retorna a data de inicio da cota.
        /// Testa se é dia não útil, se for joga um dia a frente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public DateTime RetornaDataReferencia(int idCarteira, DateTime data)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(campos, idCarteira);
            if (DateTime.Compare(data, cliente.DataDia.Value) >= 0)
            {
                return cliente.DataDia.Value;
            }

            Carteira carteira = new Carteira();
            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.DataInicioCota);
            carteira.LoadByPrimaryKey(campos, idCarteira);

            if (DateTime.Compare(carteira.DataInicioCota.Value, data) >= 0)
            {
                return carteira.DataInicioCota.Value;
            }

            if (cliente.IdLocal.Value == LocalFeriadoFixo.Brasil)
            {
                if (!Calendario.IsDiaUtil(data))
                {
                    return Calendario.AdicionaDiaUtil(data, 1);
                }
            }
            else
            {
                if (!Calendario.IsDiaUtil(data, cliente.IdLocal.Value, TipoFeriado.Outros))
                {
                    return Calendario.AdicionaDiaUtil(data, 1, cliente.IdLocal.Value, TipoFeriado.Outros);
                }
            }

            return data;
        }

        public decimal CalculaPLMedio12Meses(DateTime data)
        {
            return CalculaPLMedio(12, data);
        }

        public decimal CalculaPLMedio(int numeroMeses, DateTime data)
        {
            CalculoMedida calculoMedida = new CalculoMedida(this.IdCarteira.Value);
            return calculoMedida.CalculaPLMedio(numeroMeses, data);
        }

        public int RetornaDiasLiquidacaoResgate(int idCarteira, DateTime data, byte diasConversao, byte diasLiquidacao, ContagemDiasLiquidacaoResgate contagemDiasLiquidacaoResgate,
                                                ContagemDiasPrazoIOF contagemDiasPrazoIOF)
        {
            int diasLiquidacaoResgate = 0;
            if (contagemDiasPrazoIOF == ContagemDiasPrazoIOF.PrazoCorrido || contagemDiasPrazoIOF == ContagemDiasPrazoIOF.Direta || contagemDiasPrazoIOF == ContagemDiasPrazoIOF.Santander)
            {
                return 0;
            }
            else
            {
                if (diasLiquidacao < diasConversao)
                {
                    diasLiquidacaoResgate = diasLiquidacao;
                }
                else
                {
                    diasLiquidacaoResgate = diasLiquidacao - diasConversao;
                }
            }

            return diasLiquidacaoResgate;
        }

        public int RetornaDiasLiquidacaoComeCotas(int idCarteira, DateTime data, byte diasConversao, byte diasLiquidacao, ContagemDiasLiquidacaoResgate contagemDiasLiquidacaoResgate,
                                                  ContagemDiasPrazoIOF contagemDiasPrazoIOF)
        {
            int diasLiquidacaoResgate = 0;
            if (contagemDiasPrazoIOF == ContagemDiasPrazoIOF.Direta)
            {
                return 0;
            }
            
            if (contagemDiasPrazoIOF == ContagemDiasPrazoIOF.PrazoCorrido)
            {
                if (diasLiquidacao < diasConversao)
                {
                    diasLiquidacaoResgate = diasLiquidacao;
                }
                else
                {
                    diasLiquidacaoResgate = diasLiquidacao - diasConversao;
                }
            }
            else if (contagemDiasPrazoIOF == ContagemDiasPrazoIOF.Santander)
            {
                DateTime aux = data.AddDays(3);

                diasLiquidacaoResgate = Calendario.NumeroDias(data, aux);
            }
            else
            {
                //Mellon
                DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, 4);
                TimeSpan diff = dataLiquidacao - data;
                diasLiquidacaoResgate = diff.Days;
            }

            return diasLiquidacaoResgate;
        }

        public bool InitCotas()
        {
            this.cotas = new HistoricoCotaCollection();
            return this.cotas.LoadByIdCarteira(this.IdCarteira.Value, "Data", esOrderByDirection.Ascending);
        }

        public bool InitCotas(DateTime dataInicio, DateTime dataFim)
        {
            this.cotas = new HistoricoCotaCollection();
            return this.cotas.LoadByIdCarteira(this.IdCarteira.Value, "Data", esOrderByDirection.Ascending, dataInicio, dataFim);
        }

        public void RemoveCotas()
        {
            this.InitCotas();
            this.cotas.MarkAllAsDeleted();
            this.cotas.Save();
            this.cotas = null;
        }

        public void ExportaCarteiraXMLAnbid(DateTime dataExportacao)
        {
            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(this.IdCarteira.Value);

            Financial.Interfaces.Export.Fundo.XMLAnbid xmlAnbid = new Financial.Interfaces.Export.Fundo.XMLAnbid();

            xmlAnbid.XMLAnbidViewModel = new XMLAnbidViewModel();

            xmlAnbid.XMLAnbidViewModel.arquivoposicao_4_01 = new ArquivoPosicao();
            xmlAnbid.XMLAnbidViewModel.arquivoposicao_4_01.fundo = new Financial.Interfaces.Export.Fundo.Fundo();
            Financial.Interfaces.Export.Fundo.Fundo fundo = xmlAnbid.XMLAnbidViewModel.arquivoposicao_4_01.fundo;
            fundo.header = new Header();

            #region Preenchimento da seção do Header
            fundo.header.cnpj = pessoa.Cpfcnpj;
            fundo.header.isin = "************";
            fundo.header.nome = this.Nome.ToUpper();
            fundo.header.dtposicao = dataExportacao.ToString("yyyy-MM-dd");
            AgenteMercado agenteMercado = new AgenteMercado();

            agenteMercado.LoadByPrimaryKey(this.IdAgenteAdministrador.Value);
            fundo.header.nomeadm = agenteMercado.Nome;
            fundo.header.cnpjadm = agenteMercado.Cnpj;

            agenteMercado.LoadByPrimaryKey(this.IdAgenteGestor.Value);
            fundo.header.nomegestor = agenteMercado.Nome;
            fundo.header.cnpjgestor = agenteMercado.Cnpj;

            if (this.IdAgenteCustodiante.HasValue)
            {
                agenteMercado.LoadByPrimaryKey(this.IdAgenteCustodiante.Value);
                fundo.header.nomecustodiante = agenteMercado.Nome;
                fundo.header.cnpjcustodiante = agenteMercado.Cnpj;
            }
            else
            {
                fundo.header.nomecustodiante = "";
                fundo.header.cnpjcustodiante = "";
            }

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(dataExportacao, this.IdCarteira.Value);

            fundo.header.valorcota = historicoCota.CotaFechamento.Value;
            fundo.header.quantidade = historicoCota.QuantidadeFechamento.Value;
            fundo.header.patliq = historicoCota.PLFechamento.Value;
            //fundo.header.valorativos = FALTA
            //fundo.header.valorreceber = FALTA
            //fundo.header.valorpagar = FALTA
            //fundo.header.vlcotasemitir = FALTA
            //fundo.header.vlcotasresgatar = FAlTA
            fundo.header.codanbid = this.CodigoAnbid;
            //fundo.header.tipofundo = FALTA
            //fundo.header.nivelrsc = FALTA
            #endregion

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(this.IdCarteira),
                posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(dataExportacao));
            posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoCollection.Query);
            #region Preenchimento das seções de renda fixa
            foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixaHistorico.IdTitulo.Value);

                PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

                if (papelRendaFixa.TipoPapel == (byte)TipoPapelTitulo.Publico)
                {
                    TitPublico titPublico = new TitPublico();
                    titPublico.isin = String.IsNullOrEmpty(tituloRendaFixa.CodigoIsin) ? "************" :
                        tituloRendaFixa.CodigoIsin;
                    titPublico.codativo = ""; //TODO: mapping
                    titPublico.cusip = "";
                    titPublico.dtemissao = tituloRendaFixa.DataEmissao.Value.ToString("yyyy-MM-dd"); ;
                    titPublico.dtoperacao = posicaoRendaFixaHistorico.DataOperacao.Value.ToString("yyyy-MM-dd"); ;
                    titPublico.dtvencimento = tituloRendaFixa.DataVencimento.Value.ToString("yyyy-MM-dd"); ;
                    titPublico.qtdisponivel = posicaoRendaFixaHistorico.Quantidade.Value;
                    titPublico.qtgarantia = posicaoRendaFixaHistorico.QuantidadeBloqueada.Value;
                    titPublico.depgar = 0;
                    titPublico.pucompra = posicaoRendaFixaHistorico.PUOperacao.Value;
                    titPublico.puvencimento = 0;
                    titPublico.puposicao = posicaoRendaFixaHistorico.PUMercado.Value;
                    titPublico.puemissao = tituloRendaFixa.PUNominal.Value;
                    titPublico.principal = posicaoRendaFixaHistorico.PUOperacao.Value *
                        (posicaoRendaFixaHistorico.Quantidade.Value + posicaoRendaFixaHistorico.QuantidadeBloqueada.Value);
                    titPublico.tributos = posicaoRendaFixaHistorico.ValorIR.Value;
                    titPublico.valorfindisp = posicaoRendaFixaHistorico.PUMercado.Value * posicaoRendaFixaHistorico.Quantidade.Value;
                    titPublico.valorfinemgar = posicaoRendaFixaHistorico.PUMercado.Value * posicaoRendaFixaHistorico.QuantidadeBloqueada.Value;
                    titPublico.coupom = tituloRendaFixa.Taxa.Value;

                    string indexador;
                    if (tituloRendaFixa.IdIndice.HasValue)
                    {
                        indexador = xmlAnbid.RetornaIndice(tituloRendaFixa.IdIndice.Value);
                    }
                    else
                    {
                        indexador = "PRE";
                    }

                    titPublico.indexador = indexador;
                    titPublico.percindex = tituloRendaFixa.Percentual.HasValue ? tituloRendaFixa.Percentual.Value : 100;
                    titPublico.caracteristica = posicaoRendaFixaHistorico.TipoNegociacao ==
                        (byte)TipoNegociacaoTitulo.Vencimento ? "V" : "N";

                    titPublico.percprovcred = 0; //Implementar

                    if (posicaoRendaFixaHistorico.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda ||
                        posicaoRendaFixaHistorico.TipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra)
                    {
                        titPublico.compromisso = new Compromisso();
                        titPublico.compromisso.dtretorno = posicaoRendaFixaHistorico.DataVolta.Value.ToString("yyyy-MM-dd");
                        titPublico.compromisso.puretorno = posicaoRendaFixaHistorico.PUVolta.Value;
                        titPublico.compromisso.indexadorcomp = titPublico.indexador;
                        titPublico.compromisso.perindexcomp = titPublico.percindex;
                        titPublico.compromisso.txoperacao = posicaoRendaFixaHistorico.TaxaVolta.Value;
                        titPublico.compromisso.classecomp = posicaoRendaFixaHistorico.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda ?
                            "C" : "V";
                    }

                    titPublico.classeoperacao = posicaoRendaFixaHistorico.Quantidade < 0 ?
                        "V" : "C"; //Validar

                    titPublico.idinternoativo = tituloRendaFixa.IdTitulo.Value;
                    titPublico.nivelrsc = "";

                    if (xmlAnbid.XMLAnbidViewModel.arquivoposicao_4_01.fundo.titpublico == null)
                    {
                        xmlAnbid.XMLAnbidViewModel.arquivoposicao_4_01.fundo.titpublico = new List<TitPublico>();
                    }
                    xmlAnbid.XMLAnbidViewModel.arquivoposicao_4_01.fundo.titpublico.Add(titPublico);
                }

            }

            #endregion

            xmlAnbid.ExportaArquivoXML();

        }

        /// <summary>
        /// Realiza um De-Para das Seções DataTables para o objeto CarteiraDiaria.
        /// </summary>
        /// <param name="sr"></param>
        /// <exception cref="">throws Exception se não encontrar Pessoa com o CPF</exception> 
        public CarteiraDiaria MontaCarteiraXMLAnbid(StreamReader sr)
        {
            CarteiraDiaria carteiraDiaria = new CarteiraDiaria();

            HistoricoCota historicoCota = new HistoricoCota();
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            PosicaoBolsaCollection posicaoBolsaOpcoesCollection = new PosicaoBolsaCollection();
            PosicaoBMFCollection posicaoBMFOpcoesCollection = new PosicaoBMFCollection();
            PosicaoRendaFixaCollection posicaoRendaFixaPublicoCollection = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaCollection posicaoRendaFixaPrivadoCollection = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaCollection posicaoRendaFixaDebentureCollection = new PosicaoRendaFixaCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            SaldoCaixa saldoCaixa = new SaldoCaixa();

            Financial.Interfaces.Import.Fundo.XMLAnbid x = new Financial.Interfaces.Import.Fundo.XMLAnbid();
            x.LerArquivoXML(sr);
            //
            int? idClienteCarteira = null;

            DataTable header = x.Secao.GetHeader;
            //
            DataTable titulosPublicos = x.Secao.GetTituloPublico;
            DataTable titulosPrivados = x.Secao.GetTituloPrivado;
            DataTable titulosDebentures = x.Secao.GetDebentures;
            DataTable acoes = x.Secao.GetAcoes;
            DataTable opcoesAcoes = x.Secao.GetOpcoesAcoes;
            DataTable opcoesDeriv = x.Secao.GetOpcoesDeriv;
            DataTable futuros = x.Secao.GetFuturos;
            DataTable termoRV = x.Secao.GetTermoRV;
            DataTable cotas = x.Secao.GetCotas;
            DataTable caixa = x.Secao.GetCaixa;
            DataTable provisao = x.Secao.GetProvisao;
            //

            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
            AgenteMercado agenteMercado = new AgenteMercado();            //
            int idAgenteDefault = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

            string cnpjcpf = "";
            if (header.Rows[0].Table.Columns.Contains("cnpjcpf") && header.Rows[0]["cnpjcpf"] != null)
            {
                cnpjcpf = ((string)header.Rows[0]["cnpjcpf"]).Trim();
            }
            else if (header.Rows[0].Table.Columns.Contains("cnpj") && header.Rows[0]["cnpj"] != null)
            {
                cnpjcpf = ((string)header.Rows[0]["cnpj"]).Trim();
            }

            if (cnpjcpf != "")
            {
                PessoaCollection p = new PessoaCollection();

                PessoaQuery pessoaQuery = new PessoaQuery("E");
                ClienteQuery clienteQuery = new ClienteQuery("L");

                pessoaQuery.Select(pessoaQuery.IdPessoa);
                pessoaQuery.Where(pessoaQuery.Cpfcnpj == cnpjcpf);
                pessoaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdPessoa == pessoaQuery.IdPessoa);
                pessoaQuery.OrderBy(pessoaQuery.IdPessoa.Ascending);

                p.Load(pessoaQuery);

                if (p.Query.Load())
                {
                    idClienteCarteira = p[0].IdPessoa.Value;
                }
                else
                {
                    throw new Exception("CPF/CNPJ não cadastrado da carteira: " + cnpjcpf);
                }

                // String Decimal aparecem com formato de ponto no arquivo XML
                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ".";

                historicoCota.AjustePL = 0;
                historicoCota.Data = x.GetDate(Convert.ToString(header.Rows[0]["dtposicao"]));
                historicoCota.IdCarteira = idClienteCarteira;
                historicoCota.PatrimonioBruto = Convert.ToDecimal(header.Rows[0]["patliq"], provider);
                historicoCota.PLAbertura = Convert.ToDecimal(header.Rows[0]["patliq"], provider);
                historicoCota.PLFechamento = Convert.ToDecimal(header.Rows[0]["patliq"], provider);

                //Testa se tem a informacao de cota, se tiver entao e do tipo fundo e nao carteira
                if (header.Rows[0].Table.Columns.Contains("valorcota") && header.Rows[0]["valorcota"] != null)
                {
                    historicoCota.CotaAbertura = Convert.ToDecimal(header.Rows[0]["valorcota"], provider);
                    historicoCota.CotaBruta = Convert.ToDecimal(header.Rows[0]["valorcota"], provider);
                    historicoCota.CotaFechamento = Convert.ToDecimal(header.Rows[0]["valorcota"], provider);
                    historicoCota.QuantidadeFechamento = Convert.ToDecimal(header.Rows[0]["quantidade"], provider);
                }
                else
                {
                    historicoCota.CotaAbertura = 0;
                    historicoCota.CotaBruta = 0;
                    historicoCota.CotaFechamento = 0;
                    historicoCota.QuantidadeFechamento = 0;
                }

                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idClienteCarteira.Value);

                #region Monta PosicaoBolsa p/Acoes e PosicaoEmprestimoBolsa
                foreach (DataRow row in acoes.Rows)
                {
                    string classeOperacao = ((string)row["classeoperacao"]).Trim();

                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    if (!ativoBolsa.LoadByPrimaryKey(((string)row["codativo"]).Trim()))
                    {
                        throw new Exception("Ativo de bolsa não cadastrado - " + ((string)row["codativo"]).Trim());
                    }

                    switch (classeOperacao)
                    {
                        case "C":
                        case "V":
                            #region Carrega em PosicaoBolsa
                            PosicaoBolsa posicao = posicaoBolsaCollection.AddNew();
                            posicao.IdCliente = idClienteCarteira;
                            posicao.IdAgente = idAgenteDefault;
                            posicao.CdAtivoBolsa = ((string)row["codativo"]).Trim();
                            posicao.QuantidadeBloqueada = 0;

                            decimal quantidadeDisponivel = Convert.ToDecimal(row["qtdisponivel"], provider);
                            decimal quantidadeGarantia = Convert.ToDecimal(row["qtgarantia"], provider);

                            posicao.PUMercado = Convert.ToDecimal(row["puposicao"], provider);
                            //
                            posicao.Quantidade = classeOperacao == "V"
                                                 ? -1 * (quantidadeDisponivel + quantidadeGarantia)
                                                 : quantidadeDisponivel + quantidadeGarantia;

                            posicao.ValorMercado = Math.Round(posicao.PUMercado.Value * posicao.Quantidade.Value, 2);

                            posicao.PUCusto = posicao.PUMercado.Value; //NÃO TEM NO XML
                            posicao.PUCustoLiquido = posicao.PUMercado.Value; //NÃO TEM NO XML
                            posicao.QuantidadeInicial = posicao.Quantidade.Value;
                            posicao.TipoMercado = TipoMercadoBolsa.MercadoVista;
                            posicao.ValorCustoLiquido = posicao.ValorMercado; //NÃO TEM NO XML
                            posicao.ResultadoRealizar = 0;

                            #endregion
                            break;
                        case "D":
                        case "T":
                            #region Carrega em PosicaoEmprestimoBolsa
                            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection.AddNew();
                            posicaoEmprestimoBolsa.IdCliente = idClienteCarteira;
                            posicaoEmprestimoBolsa.IdAgente = idAgenteDefault;
                            posicaoEmprestimoBolsa.CdAtivoBolsa = ((string)row["codativo"]).Trim();
                            posicaoEmprestimoBolsa.Quantidade = Convert.ToDecimal(row["qtdisponivel"], provider) + Convert.ToDecimal(row["qtgarantia"], provider);
                            posicaoEmprestimoBolsa.ValorMercado = Convert.ToDecimal(row["valorfindisp"], provider) + Convert.ToDecimal(row["valorfinemgar"], provider);
                            posicaoEmprestimoBolsa.PUMercado = Convert.ToDecimal(row["puposicao"], provider);

                            //
                            // Armazena D ou T

                            int ponta = classeOperacao == "D" ? (int)PontaEmprestimoBolsa.Doador : (int)PontaEmprestimoBolsa.Tomador;
                            posicaoEmprestimoBolsa.PontaEmprestimo = (byte)ponta;
                            //
                            if (!String.IsNullOrEmpty(Convert.ToString(row["dtvencalug"])))
                            {
                                posicaoEmprestimoBolsa.DataVencimento = x.GetDate(Convert.ToString(row["dtvencalug"]));
                            }

                            posicaoEmprestimoBolsa.TaxaOperacao = Convert.ToDecimal(row["txalug"], provider);
                            posicaoEmprestimoBolsa.NumeroContrato = 0;

                            posicaoEmprestimoBolsa.DataRegistro = posicaoEmprestimoBolsa.DataVencimento.Value; //NÃO TEM NO XML
                            posicaoEmprestimoBolsa.PULiquidoOriginal = 0; //NÃO TEM NO XML
                            posicaoEmprestimoBolsa.TaxaComissao = 0; //NÃO TEM NO XML
                            posicaoEmprestimoBolsa.TipoEmprestimo = (byte)TipoEmprestimoBolsa.Voluntario; //NÃO TEM NO XML
                            posicaoEmprestimoBolsa.ValorBase = 0; //NÃO TEM NO XML
                            posicaoEmprestimoBolsa.ValorCorrigidoCBLC = 0; //NÃO TEM NO XML
                            posicaoEmprestimoBolsa.ValorCorrigidoComissao = 0; //NÃO TEM NO XML
                            posicaoEmprestimoBolsa.ValorCorrigidoJuros = 0; //NÃO TEM NO XML                            
                            #endregion
                            break;
                    }
                }
                #endregion

                #region Monta PosicaoBolsa p/ Opcoes Bolsa
                foreach (DataRow row in opcoesAcoes.Rows)
                {
                    string classeOperacao = ((string)row["classeoperacao"]).Trim();

                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    if (!ativoBolsa.LoadByPrimaryKey(((string)row["codativo"]).Trim()))
                    {
                        throw new Exception("Ativo de bolsa não cadastrado - " + ((string)row["codativo"]).Trim());
                    }

                    #region Carrega em PosicaoBolsaOpcoes
                    PosicaoBolsa posicao = posicaoBolsaOpcoesCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;
                    posicao.IdAgente = idAgenteDefault;
                    posicao.CdAtivoBolsa = ((string)row["codativo"]).Trim();
                    //
                    posicao.ValorMercado = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["valorfinanceiro"], provider))
                                         : Convert.ToDecimal(row["valorfinanceiro"], provider);
                    //                                                                 
                    posicao.PUMercado = Convert.ToDecimal(row["puposicao"], provider);
                    //
                    posicao.Quantidade = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["qtdisponivel"], provider))
                                         : Convert.ToDecimal(row["qtdisponivel"], provider);

                    if (!String.IsNullOrEmpty(Convert.ToString(row["dtvencimento"])))
                    {
                        posicao.DataVencimento = x.GetDate(Convert.ToString(row["dtvencimento"]));
                    }

                    posicao.PUCusto = posicao.PUMercado; //NÃO TEM NO XML
                    posicao.PUCustoLiquido = posicao.PUMercado; //NÃO TEM NO XML

                    posicao.QuantidadeBloqueada = 0;
                    posicao.QuantidadeInicial = posicao.Quantidade.Value;
                    posicao.ResultadoRealizar = 0;

                    ativoBolsa = new AtivoBolsa();
                    if (!ativoBolsa.LoadByPrimaryKey(posicao.CdAtivoBolsa))
                    {
                        throw new Exception("Opção de Bolsa não cadastrada: " + posicao.CdAtivoBolsa);
                    }

                    posicao.TipoMercado = ativoBolsa.TipoMercado;
                    posicao.ValorCustoLiquido = posicao.ValorMercado; //NÃO TEM NO XML
                    #endregion
                }
                #endregion

                #region Monta PosicaoBMF Futuros
                DateTime dataLiquidacaoBMF = Calendario.AdicionaDiaUtil(x.GetDate(Convert.ToString(header.Rows[0]["dtposicao"]).Trim()), 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                foreach (DataRow row in futuros.Rows)
                {
                    string cnpjCorretora = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["cnpjcorretora"])))
                    {
                        cnpjCorretora = Utilitario.RemoveCaracteresEspeciais(Convert.ToString(row["cnpjcorretora"]));
                    }

                    AgenteMercado agenteMercadoCorretora = new AgenteMercado();
                    agenteMercadoCorretora.Query.Select(agenteMercadoCorretora.Query.IdAgente);
                    agenteMercadoCorretora.Query.Where(agenteMercadoCorretora.Query.Cnpj.Equal(cnpjCorretora));

                    int idAgenteCorretora = 0;
                    if (agenteMercadoCorretora.Query.Load())
                    {
                        idAgenteCorretora = agenteMercadoCorretora.IdAgente.Value;
                    }
                    else
                    {
                        idAgenteCorretora = idAgenteDefault;
                    }

                    string classeOperacao = ((string)row["classeoperacao"]).Trim();
                    decimal valorAjuste = Convert.ToDecimal(row["vlajuste"], provider);
                    //        
                    PosicaoBMF posicao = posicaoBMFCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;
                    posicao.IdAgente = idAgenteDefault;
                    posicao.CdAtivoBMF = ((string)row["ativo"]).Trim();
                    posicao.Serie = ((string)row["serie"]).Trim();

                    decimal quantidade = Convert.ToDecimal(row["quantidade"], provider);
                    posicao.Quantidade = classeOperacao == "V"
                                         ? -1 * Convert.ToInt32(Math.Truncate(quantidade))
                                         : Convert.ToInt32(Math.Truncate(quantidade));
                    //
                    posicao.ValorMercado = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["vltotalpos"], provider))
                                         : Convert.ToDecimal(row["vltotalpos"], provider);
                    //

                    posicao.PUMercado = 0; //NÃO TEM NO XML
                    posicao.DataVencimento = x.GetDate(Convert.ToString(row["dtvencimento"]));
                    posicao.PUCusto = 0;
                    posicao.PUCustoLiquido = 0; //NÃO TEM NO XML
                    posicao.ResultadoRealizar = 0;
                    posicao.QuantidadeInicial = posicao.Quantidade.Value;
                    posicao.AjusteDiario = valorAjuste;
                    posicao.AjusteAcumulado = 0;
                    posicao.TipoMercado = (byte)TipoMercadoBMF.Futuro;
                    posicao.ValorCustoLiquido = 0; //NÃO TEM NO XML
                }
                #endregion

                #region Monta PosicaoBMF Opcoes
                foreach (DataRow row in opcoesDeriv.Rows)
                {
                    string classeOperacao = ((string)row["classeoperacao"]).Trim();
                    //        
                    PosicaoBMF posicao = posicaoBMFOpcoesCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;
                    posicao.IdAgente = idAgenteDefault;
                    posicao.CdAtivoBMF = ((string)row["ativo"]).Trim();
                    posicao.Serie = ((string)row["serie"]).Trim();

                    AtivoBMF ativoBMF = new AtivoBMF();
                    if (!ativoBMF.LoadByPrimaryKey(posicao.CdAtivoBMF, posicao.Serie))
                    {
                        throw new Exception("Opção de BMF não cadastrada: " + posicao.CdAtivoBMF + " " + posicao.Serie);
                    }

                    decimal quantidade = Convert.ToDecimal(row["quantidade"], provider);

                    posicao.Quantidade = classeOperacao == "V"
                                         ? -1 * Convert.ToInt32(Math.Truncate(quantidade))
                                         : Convert.ToInt32(Math.Truncate(quantidade));
                    //
                    posicao.ValorMercado = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["valorfinanceiro"], provider))
                                         : Convert.ToDecimal(row["valorfinanceiro"], provider);
                    //

                    posicao.DataVencimento = x.GetDate(Convert.ToString(row["dtvencimento"]));

                    posicao.PUMercado = Convert.ToDecimal(row["puposicao"], provider);

                    decimal premio = Convert.ToDecimal(row["premio"], provider);

                    decimal puCusto = posicao.Quantidade.Value == 0 ? 0 : (premio / posicao.Quantidade.Value) / ativoBMF.Peso.Value;

                    posicao.PUCusto = puCusto;
                    posicao.PUCustoLiquido = puCusto;
                    posicao.ValorCustoLiquido = Utilitario.Truncate(posicao.Quantidade.Value * puCusto * ativoBMF.Peso.Value, 2);
                    posicao.ResultadoRealizar = Utilitario.Truncate((posicao.PUMercado.Value - posicao.PUCustoLiquido.Value) * posicao.Quantidade.Value, 2);
                    posicao.AjusteDiario = 0;
                    posicao.AjusteAcumulado = 0;
                    posicao.QuantidadeInicial = posicao.Quantidade.Value;

                    posicao.TipoMercado = ativoBMF.TipoMercado.Value;
                }
                #endregion

                #region Monta PosicaoFundo
                int? idFundo = null;
                foreach (DataRow row in cotas.Rows)
                {
                    // Pega IdCarteira por CNPJ
                    string cnpjFundo = ((string)row["cnpjfundo"]).Trim();
                    //
                    PessoaCollection pessoa = new PessoaCollection();
                    pessoa.Query.Select(pessoa.Query.IdPessoa).Where(pessoa.Query.Cpfcnpj == cnpjFundo).OrderBy(pessoa.Query.IdPessoa.Ascending);
                    if (pessoa.Query.Load())
                    {
                        idFundo = pessoa[0].IdPessoa.Value;
                    }
                    else
                    {
                        throw new Exception("CPF/CNPJ não cadastrado do fundo: " + cnpjFundo);
                    }
                    //                    
                    PosicaoFundo posicao = posicaoFundoCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;
                    posicao.IdCarteira = idFundo;
                    //
                    posicao.Quantidade = Convert.ToDecimal(row["qtdisponivel"], provider);
                    posicao.CotaDia = Convert.ToDecimal(row["puposicao"], provider);
                    posicao.ValorBruto = posicao.Quantidade * posicao.CotaDia;
                    posicao.CotaAplicacao = Convert.ToDecimal(row["puposicao"], provider);
                    posicao.DataAplicacao = x.GetDate(Convert.ToString(header.Rows[0]["dtposicao"]).Trim()); //NÃO TEM NO XML
                    posicao.DataConversao = x.GetDate(Convert.ToString(header.Rows[0]["dtposicao"]).Trim()); //NÃO TEM NO XML
                    posicao.DataUltimaCobrancaIR = x.GetDate(Convert.ToString(header.Rows[0]["dtposicao"]).Trim()); //NÃO TEM NO XML
                    posicao.QuantidadeAntesCortes = posicao.Quantidade.Value; //NÃO TEM NO XML
                    posicao.QuantidadeBloqueada = 0;
                    posicao.QuantidadeInicial = posicao.Quantidade.Value; //NÃO TEM NO XML
                    posicao.ValorAplicacao = Math.Round(posicao.CotaAplicacao.Value * posicao.Quantidade.Value, 2); //NÃO TEM NO XML
                    posicao.ValorIOF = 0; //NÃO TEM NO XML
                    posicao.ValorIOFVirtual = 0; //NÃO TEM NO XML
                    posicao.PosicaoIncorporada = "N"; //NÃO TEM NO XML
                    posicao.ValorIR = 0; //NÃO TEM NO XML
                    posicao.ValorLiquido = posicao.ValorBruto.Value; //NÃO TEM NO XML
                    posicao.ValorPerformance = 0; //NÃO TEM NO XML
                    posicao.ValorRendimento = 0; //NÃO TEM NO XML
                }
                #endregion

                #region Monta PosicaoRendaFixa (Titulos Publicos)
                foreach (DataRow row in titulosPublicos.Rows)
                {
                    string classeOperacao = ((string)row["classeoperacao"]).Trim();

                    string isin = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["isin"])))
                    {
                        isin = Convert.ToString(row["isin"]);
                    }

                    string codAtivo = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["codativo"])))
                    {
                        codAtivo = Convert.ToString(row["codativo"]);
                    }

                    string descricao = "";
                    if (codAtivo != "")
                    {
                        switch (codAtivo)
                        {
                            case "100000":
                                descricao = "LTN";
                                break;
                            case "210100":
                                descricao = "LFT";
                                break;
                            case "760199":
                                descricao = "NTNB";
                                break;
                            case "770100":
                                descricao = "NTNC";
                                break;
                            case "950199":
                                descricao = "NTNF";
                                break;
                            default:
                                break;
                        }
                    }

                    if (descricao == "")
                    {
                        descricao = isin;
                    }

                    #region Carrega em PosicaoRendaFixa
                    PosicaoRendaFixa posicao = posicaoRendaFixaPublicoCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;

                    string codigoInterface = ((string)row["idinternoativo"]).Trim();
                    DateTime dataEmissao = x.GetDate(Convert.ToString(row["dtemissao"]));
                    DateTime dataVencimento = x.GetDate(Convert.ToString(row["dtvencimento"]));
                    decimal puEmissao = Convert.ToDecimal(row["puemissao"], provider);

                    decimal percentual = 0;
                    if (!String.IsNullOrEmpty(Convert.ToString(row["percindex"])))
                    {
                        percentual = Convert.ToDecimal(row["percindex"], provider);
                    }

                    decimal taxa = 0;
                    if (!String.IsNullOrEmpty(Convert.ToString(row["coupom"])))
                    {
                        taxa = Convert.ToDecimal(row["coupom"], provider);
                    }

                    short? idIndice = null;
                    if (Convert.ToString(row["indexador"]) != "PRE")
                    {
                        idIndice = x.RetornaIdIndice(Convert.ToString(row["indexador"]));
                    }
                    else
                    {
                        percentual = 0;
                    }


                    //Campos do compromisso
                    bool compromisso = false;
                    DateTime? dataVolta = null;
                    decimal? puVolta = null; ;
                    decimal? taxaVolta = null;
                    string classeCompromissada = "";

                    try
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(row["dtretorno"])))
                        {
                            dataVolta = x.GetDate(Convert.ToString(row["dtretorno"]));
                            puVolta = Convert.ToDecimal(row["puretorno"], provider);
                            taxaVolta = Convert.ToDecimal(row["txoperacao"], provider);
                            classeCompromissada = Convert.ToString(row["classecomp"]);

                            compromisso = true;
                        }
                    }
                    catch (Exception)
                    {
                    }

                    PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                    int idPapel = papelRendaFixa.RetornaPapelPublico(Convert.ToInt32(codAtivo));

                    int idTitulo = 0;
                    TituloRendaFixa titulo = new TituloRendaFixa();
                    titulo.Query.Where(titulo.Query.CodigoInterface.Equal(codigoInterface));
                    if (!titulo.Query.Load())
                    {
                        titulo = new TituloRendaFixa();
                        DateTime? dataAux = dataEmissao;
                        titulo.CriaTitulo(codigoInterface, descricao, idPapel, (int)ListaEmissorFixo.TesouroNacional, dataEmissao, dataVencimento, puEmissao, taxa, idIndice, percentual,
                                            (byte)TipoMTMTitulo.Andima_TituloPublico);
                    }

                    idTitulo = titulo.IdTitulo.Value;
                    //
                    posicao.IdTitulo = idTitulo;
                    posicao.DataOperacao = x.GetDate(Convert.ToString(row["dtoperacao"]));
                    posicao.DataLiquidacao = x.GetDate(Convert.ToString(row["dtoperacao"]));
                    posicao.PUOperacao = Convert.ToDecimal(row["pucompra"], provider);
                    posicao.PUMercado = Convert.ToDecimal(row["puposicao"], provider);
                    posicao.ValorIR = Convert.ToDecimal(row["tributos"], provider);
                    posicao.ValorMercado = Convert.ToDecimal(row["valorfindisp"], provider) + Convert.ToDecimal(row["valorfinemgar"], provider);
                    //
                    posicao.Quantidade = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["qtdisponivel"], provider))
                                         : Convert.ToDecimal(row["qtdisponivel"], provider);
                    posicao.QuantidadeBloqueada = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["qtgarantia"], provider))
                                         : Convert.ToDecimal(row["qtgarantia"], provider);
                    posicao.Quantidade += posicao.QuantidadeBloqueada;

                    posicao.DataVencimento = dataVencimento;
                    posicao.QuantidadeInicial = posicao.Quantidade.Value;

                    if (!compromisso)
                    {
                        posicao.TipoOperacao = (byte)TipoOperacaoTitulo.CompraFinal;
                    }
                    else
                    {
                        if (classeCompromissada == "C")
                        {
                            posicao.TipoOperacao = (byte)TipoOperacaoTitulo.CompraRevenda;
                        }
                        else
                        {
                            posicao.TipoOperacao = (byte)TipoOperacaoTitulo.VendaRecompra;
                        }

                        posicao.PUVolta = puVolta;
                        posicao.TaxaVolta = taxaVolta;
                        posicao.DataVolta = dataVolta;
                        posicao.ValorVolta = Math.Round(puVolta.Value * posicao.Quantidade.Value, 2);
                    }

                    posicao.PUCurva = posicao.PUMercado;
                    posicao.PUCorrecao = posicao.PUMercado;
                    posicao.PUJuros = 0;
                    posicao.TaxaOperacao = taxa;
                    posicao.TipoNegociacao = (byte)TipoNegociacaoTitulo.Negociacao;
                    posicao.ValorIOF = 0;
                    posicao.ValorJuros = 0;
                    posicao.CustoCustodia = 0;
                    posicao.ValorCurva = Math.Round(posicao.PUCurva.Value * posicao.Quantidade.Value, 2);
                    posicao.ValorCorrecao = Math.Round(posicao.PUCorrecao.Value * posicao.Quantidade.Value, 2);
                    #endregion
                }
                #endregion

                #region Monta PosicaoRendaFixa (Titulos Privados)
                foreach (DataRow row in titulosPrivados.Rows)
                {
                    string classeOperacao = ((string)row["classeoperacao"]).Trim();

                    string isin = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["isin"])))
                    {
                        isin = Convert.ToString(row["isin"]);
                    }

                    string codAtivo = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["codativo"])))
                    {
                        codAtivo = Convert.ToString(row["codativo"]);
                    }

                    string cusip = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["cusip"])))
                    {
                        cusip = Convert.ToString(row["cusip"]);
                    }

                    string descricao = "";
                    if (cusip != "")
                    {
                        descricao = cusip;
                    }
                    else if (codAtivo != "")
                    {
                        descricao = codAtivo;
                    }

                    #region Carrega em PosicaoRendaFixa
                    PosicaoRendaFixa posicao = posicaoRendaFixaPrivadoCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;

                    string codigoInterface = ((string)row["idinternoativo"]).Trim();
                    DateTime dataEmissao = x.GetDate(Convert.ToString(row["dtemissao"]));
                    DateTime dataVencimento = x.GetDate(Convert.ToString(row["dtvencimento"]));
                    decimal puEmissao = Convert.ToDecimal(row["puemissao"], provider);

                    decimal percentual = 0;
                    if (!String.IsNullOrEmpty(Convert.ToString(row["percindex"])))
                    {
                        percentual = Convert.ToDecimal(row["percindex"], provider);
                    }

                    decimal taxa = 0;
                    if (!String.IsNullOrEmpty(Convert.ToString(row["coupom"])))
                    {
                        taxa = Convert.ToDecimal(row["coupom"], provider);
                    }

                    short? idIndice = null;
                    if (Convert.ToString(row["indexador"]) != "PRE")
                    {
                        idIndice = x.RetornaIdIndice(Convert.ToString(row["indexador"]));
                    }
                    else
                    {
                        percentual = 0;
                    }

                    TituloRendaFixa titulo = new TituloRendaFixa();
                    titulo.Query.Where(titulo.Query.CodigoInterface.Equal(codigoInterface));
                    if (!titulo.Query.Load())
                    {
                        string cnpj = Convert.ToString(row["cnpjemissor"]).Trim();

                        EmissorCollection emissorCollection = new EmissorCollection();
                        emissorCollection.Query.Select(emissorCollection.Query.IdEmissor);
                        emissorCollection.Query.Where(emissorCollection.Query.Cnpj.Equal(cnpj));
                        emissorCollection.Query.Load();

                        int idEmissor = 0;
                        if (emissorCollection.Count > 0)
                        {
                            idEmissor = emissorCollection[0].IdEmissor.Value;
                        }
                        else
                        {
                            SetorCollection setorCollection = new SetorCollection();
                            setorCollection.Query.Where(setorCollection.Query.Nome.Like("%OUTRO%"));
                            setorCollection.Query.Load();

                            if (setorCollection.Count == 0)
                            {
                                throw new Exception("Nao existe emissor cadastrado para o CNPJ " + cnpj);
                            }

                            Emissor emissor = new Emissor();
                            emissor.Cnpj = cnpj;
                            emissor.IdSetor = setorCollection[0].IdSetor.Value;
                            emissor.Nome = cnpj;
                            emissor.TipoEmissor = (byte)TipoEmissor.PessoaJuridica;
                            emissor.Save();

                            idEmissor = emissor.IdEmissor.Value;
                        }


                        int idPapel = 0;
                        PapelRendaFixaCollection papelRendaFixaCollection = new PapelRendaFixaCollection();
                        papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.TipoPapel.Equal((byte)TipoPapelTitulo.Privado));
                        papelRendaFixaCollection.Query.Load();

                        bool achou = false;
                        foreach (PapelRendaFixa papelRendaFixa in papelRendaFixaCollection) 
                        {
                            if (descricao != "")
                            {

                                if (descricao.Length >= 5 && descricao.Replace("-", "").Substring(0, 5).Contains(papelRendaFixa.Descricao.Replace("-", "")))
                                {
                                    achou = true;
                                    idPapel = papelRendaFixa.IdPapel.Value;
                                }
                                else if (descricao.Length >= 4 && descricao.Replace("-", "").Substring(0, 4).Contains(papelRendaFixa.Descricao.Replace("-", "")))
                                {
                                    achou = true;
                                    idPapel = papelRendaFixa.IdPapel.Value;
                                }
                                else if (descricao.Length >= 3 && descricao.Replace("-", "").Substring(0, 3).Contains(papelRendaFixa.Descricao.Replace("-", "")))
                                {
                                    achou = true;
                                    idPapel = papelRendaFixa.IdPapel.Value;
                                }
                                else if (descricao.Length >= 2 && descricao.Replace("-", "").Substring(0, 2).Contains(papelRendaFixa.Descricao.Replace("-", "")))
                                {
                                    achou = true;
                                    idPapel = papelRendaFixa.IdPapel.Value;
                                }
                                else if (!descricao.Contains("-") && descricao.Contains(papelRendaFixa.Descricao.Replace("-", "")))
                                {
                                    achou = true;
                                    idPapel = papelRendaFixa.IdPapel.Value;
                                }
                            }
                        }

                        if (!achou)
                        {
                            if (papelRendaFixaCollection.Count > 0)
                            {
                                idPapel = papelRendaFixaCollection[0].IdPapel.Value;
                            }
                            else
                            {
                                throw new Exception("Título não cadastrado - " + codigoInterface + ". Não foi encontrado papel associado a esta descrição " + descricao);
                            }
                        }

                        titulo = new TituloRendaFixa();
                        titulo.CriaTitulo(codigoInterface, descricao, idPapel, idEmissor, dataEmissao, dataVencimento, puEmissao, taxa, idIndice, percentual,
                                            (byte)TipoMTMTitulo.NaoFaz);
                    }
                    titulo.CodigoIsin = isin;
                    titulo.Save();
                    //
                    posicao.IdTitulo = titulo.IdTitulo.Value;
                    posicao.DataOperacao = x.GetDate(Convert.ToString(row["dtoperacao"]));
                    posicao.DataLiquidacao = x.GetDate(Convert.ToString(row["dtoperacao"]));
                    posicao.PUOperacao = Convert.ToDecimal(row["pucompra"], provider);
                    posicao.PUMercado = Convert.ToDecimal(row["puposicao"], provider);
                    posicao.ValorIR = Convert.ToDecimal(row["tributos"], provider); ;
                    posicao.ValorMercado = Convert.ToDecimal(row["valorfindisp"], provider) + Convert.ToDecimal(row["valorfinemgar"], provider);
                    //
                    posicao.Quantidade = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["qtdisponivel"], provider))
                                         : Convert.ToDecimal(row["qtdisponivel"], provider);
                    posicao.QuantidadeBloqueada = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["qtgarantia"], provider))
                                         : Convert.ToDecimal(row["qtgarantia"], provider);
                    posicao.Quantidade += posicao.QuantidadeBloqueada;

                    posicao.DataVencimento = dataVencimento;
                    posicao.QuantidadeInicial = posicao.Quantidade.Value;
                    posicao.TipoOperacao = (byte)TipoOperacaoTitulo.CompraFinal;
                    posicao.PUCurva = posicao.PUMercado;
                    posicao.PUCorrecao = posicao.PUMercado;
                    posicao.PUJuros = 0;
                    posicao.TaxaOperacao = taxa;
                    posicao.TipoNegociacao = (byte)TipoNegociacaoTitulo.Negociacao;
                    posicao.ValorIOF = 0;
                    posicao.ValorJuros = 0;
                    posicao.CustoCustodia = 0;
                    posicao.ValorCurva = Math.Round(posicao.PUCurva.Value * posicao.Quantidade.Value, 2);
                    posicao.ValorCorrecao = Math.Round(posicao.PUCorrecao.Value * posicao.Quantidade.Value, 2);
                    #endregion
                }
                #endregion

                #region Monta PosicaoRendaFixa (Debentures)
                foreach (DataRow row in titulosDebentures.Rows)
                {
                    string classeOperacao = ((string)row["classeoperacao"]).Trim();

                    string isin = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["isin"])))
                    {
                        isin = Convert.ToString(row["isin"]);
                    }

                    string codAtivo = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["coddeb"])))
                    {
                        codAtivo = Convert.ToString(row["coddeb"]);
                    }

                    string cusip = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["cusip"])))
                    {
                        cusip = Convert.ToString(row["cusip"]);
                    }

                    string codDeb = "";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["coddeb"])))
                    {
                        codDeb = Convert.ToString(row["coddeb"]);
                    }

                    string debConv = "N";
                    if (!String.IsNullOrEmpty(Convert.ToString(row["debconv"])))
                    {
                        debConv = Convert.ToString(row["debconv"]);
                    }

                    string descricao = "";
                    if (codAtivo != "")
                    {
                        descricao = codAtivo;
                    }
                    else if (cusip != "")
                    {
                        descricao = cusip;
                    }

                    #region Carrega em PosicaoRendaFixa
                    PosicaoRendaFixa posicao = posicaoRendaFixaPrivadoCollection.AddNew();
                    posicao.IdCliente = idClienteCarteira;

                    string codigoInterface = ((string)row["idinternoativo"]).Trim();
                    DateTime dataEmissao = x.GetDate(Convert.ToString(row["dtemissao"]));
                    DateTime dataVencimento = x.GetDate(Convert.ToString(row["dtvencimento"]));
                    decimal puEmissao = Convert.ToDecimal(row["puemissao"], provider);

                    decimal percentual = 0;
                    if (!String.IsNullOrEmpty(Convert.ToString(row["percindex"])))
                    {
                        percentual = Convert.ToDecimal(row["percindex"], provider);
                    }

                    decimal taxa = 0;
                    if (!String.IsNullOrEmpty(Convert.ToString(row["coupom"])))
                    {
                        taxa = Convert.ToDecimal(row["coupom"], provider);
                    }

                    short? idIndice = null;
                    if (Convert.ToString(row["indexador"]) != "PRE")
                    {
                        idIndice = x.RetornaIdIndice(Convert.ToString(row["indexador"]));
                    }
                    else
                    {
                        percentual = 0;
                    }

                    TituloRendaFixa titulo = new TituloRendaFixa();
                    titulo.Query.Where(titulo.Query.CodigoInterface.Equal(codigoInterface));
                    if (!titulo.Query.Load())
                    {
                        string cnpj = Convert.ToString(row["cnpjemissor"]).Trim();

                        EmissorCollection emissorCollection = new EmissorCollection();
                        emissorCollection.Query.Select(emissorCollection.Query.IdEmissor);
                        emissorCollection.Query.Where(emissorCollection.Query.Cnpj.Equal(cnpj));
                        emissorCollection.Query.Load();

                        int idEmissor = 0;
                        if (emissorCollection.Count > 0)
                        {
                            idEmissor = emissorCollection[0].IdEmissor.Value;
                        }
                        else
                        {
                            SetorCollection setorCollection = new SetorCollection();
                            setorCollection.Query.Where(setorCollection.Query.Nome.Like("%OUTRO%"));
                            setorCollection.Query.Load();

                            if (setorCollection.Count == 0)
                            {
                                throw new Exception("Nao existe emissor cadastrado para o CNPJ " + cnpj);
                            }

                            Emissor emissor = new Emissor();
                            emissor.Cnpj = cnpj;
                            emissor.IdSetor = setorCollection[0].IdSetor.Value;
                            emissor.Nome = cnpj;
                            emissor.TipoEmissor = (byte)TipoEmissor.PessoaJuridica;
                            emissor.Save();

                            idEmissor = emissor.IdEmissor.Value;
                        }


                        int idPapel = 0;
                        PapelRendaFixaCollection papelRendaFixaCollection = new PapelRendaFixaCollection();
                        papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.TipoPapel.Equal((byte)TipoPapelTitulo.Privado),
                                                             papelRendaFixaCollection.Query.Descricao.Like("Deb%"));

                        if (idIndice.HasValue)
                        {
                            papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.TipoRentabilidade.Equal((byte)TipoRentabilidadeTitulo.PosFixado));
                        }
                        else
                        {
                            papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.TipoRentabilidade.Equal((byte)TipoRentabilidadeTitulo.PreFixado));
                        }

                        papelRendaFixaCollection.Query.Load();


                        if (papelRendaFixaCollection.Count > 0)
                        {
                            idPapel = papelRendaFixaCollection[0].IdPapel.Value;
                        }
                        else
                        {
                            if (papelRendaFixaCollection.Count > 0)
                            {
                                idPapel = papelRendaFixaCollection[0].IdPapel.Value;
                            }
                            else
                            {
                                throw new Exception("Título não cadastrado - " + codigoInterface + ". Não foi encontrado papel associado a esta descrição " + cusip);
                            }
                        }

                        titulo = new TituloRendaFixa();
                        titulo.CriaTitulo(codigoInterface, descricao, idPapel, idEmissor, dataEmissao, dataVencimento, puEmissao, taxa, idIndice, percentual, (byte)TipoMTMTitulo.Mercado_Debenture);
                    }

                    //Salva informações adicionais/especificas da debenture
                    titulo.CodigoCustodia = codDeb;
                    titulo.DebentureConversivel = debConv;
                    titulo.Save();
                    //*****************************************************
                    //
                    posicao.IdTitulo = titulo.IdTitulo.Value;
                    posicao.DataOperacao = x.GetDate(Convert.ToString(row["dtoperacao"]));
                    posicao.DataLiquidacao = x.GetDate(Convert.ToString(row["dtoperacao"]));
                    posicao.PUOperacao = Convert.ToDecimal(row["pucompra"], provider);
                    posicao.PUMercado = Convert.ToDecimal(row["puposicao"], provider);
                    posicao.ValorIR = Convert.ToDecimal(row["tributos"], provider); ;
                    posicao.ValorMercado = Convert.ToDecimal(row["valorfindisp"], provider) + Convert.ToDecimal(row["valorfinemgar"], provider);
                    //
                    posicao.Quantidade = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["qtdisponivel"], provider))
                                         : Convert.ToDecimal(row["qtdisponivel"], provider);
                    posicao.QuantidadeBloqueada = classeOperacao == "V"
                                         ? -1 * (Convert.ToDecimal(row["qtgarantia"], provider))
                                         : Convert.ToDecimal(row["qtgarantia"], provider);
                    posicao.Quantidade += posicao.QuantidadeBloqueada;

                    posicao.DataVencimento = dataVencimento;
                    posicao.QuantidadeInicial = posicao.Quantidade.Value;
                    posicao.TipoOperacao = (byte)TipoOperacaoTitulo.CompraFinal;
                    posicao.PUCurva = posicao.PUMercado;
                    posicao.PUCorrecao = posicao.PUMercado;
                    posicao.PUJuros = 0;
                    posicao.TaxaOperacao = taxa;
                    posicao.TipoNegociacao = (byte)TipoNegociacaoTitulo.Negociacao;
                    posicao.ValorIOF = 0;
                    posicao.ValorJuros = 0;
                    posicao.CustoCustodia = 0;
                    posicao.ValorCurva = Math.Round(posicao.PUCurva.Value * posicao.Quantidade.Value, 2);
                    posicao.ValorCorrecao = Math.Round(posicao.PUCorrecao.Value * posicao.Quantidade.Value, 2);
                    #endregion
                }
                #endregion

                #region Monta SaldoCaixa
                decimal sumTotalCaixa = 0.0M;
                foreach (DataRow row in caixa.Rows)
                {
                    sumTotalCaixa += Convert.ToDecimal(row["saldo"], provider);
                }

                saldoCaixa.IdCliente = idClienteCarteira;
                saldoCaixa.SaldoAbertura = sumTotalCaixa;
                saldoCaixa.SaldoFechamento = sumTotalCaixa;
                saldoCaixa.Data = x.GetDate(Convert.ToString(header.Rows[0]["dtposicao"]).Trim());
                saldoCaixa.IdConta = idContaDefault;
                #endregion

                #region Monta Liquidacao
                foreach (DataRow row in provisao.Rows)
                {
                    decimal valor = Convert.ToDecimal(row["valor"], provider);

                    if (Convert.ToString(row["credeb"]) == "D")
                    {
                        valor = valor * -1;
                    }

                    Liquidacao liquidacao = liquidacaoCollection.AddNew();
                    liquidacao.DataLancamento = x.GetDate(Convert.ToString(header.Rows[0]["dtposicao"]).Trim());

                    try
                    {
                        liquidacao.DataVencimento = x.GetDate(Convert.ToString(row["dt"]));
                    }
                    catch (Exception e)
                    {
                        liquidacao.DataVencimento = new DateTime(4000, 02, 29);
                    }


                    liquidacao.Descricao = x.RetornaTipoLancamento(Convert.ToInt32(row["codprov"]));

                    if (liquidacao.DataVencimento.Value <= liquidacao.DataLancamento)
                    {
                        liquidacao.Descricao = liquidacao.Descricao + ">> (Já Vencido)";
                    }

                    liquidacao.Valor = valor;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idClienteCarteira;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Outros;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                }
                #endregion

                carteiraDiaria.HistoricoCota = historicoCota;
                carteiraDiaria.PosicaoBolsaCollection = posicaoBolsaCollection;
                carteiraDiaria.PosicaoEmprestimoBolsaCollection = posicaoEmprestimoBolsaCollection;
                carteiraDiaria.PosicaoBMFCollection = posicaoBMFCollection;
                carteiraDiaria.PosicaoTermoBolsaCollection = posicaoTermoBolsaCollection;
                carteiraDiaria.PosicaoFundoCollection = posicaoFundoCollection;
                carteiraDiaria.PosicaoBolsaOpcoesCollection = posicaoBolsaOpcoesCollection;
                carteiraDiaria.PosicaoBMFOpcoesCollection = posicaoBMFOpcoesCollection;
                carteiraDiaria.PosicaoRendaFixaPublicoCollection = posicaoRendaFixaPublicoCollection;
                carteiraDiaria.PosicaoRendaFixaPrivadoCollection = posicaoRendaFixaPrivadoCollection;
                carteiraDiaria.PosicaoRendaFixaDebentureCollection = posicaoRendaFixaDebentureCollection;
                carteiraDiaria.LiquidacaoCollection = liquidacaoCollection;
                carteiraDiaria.SaldoCaixa = saldoCaixa;
            }

            return carteiraDiaria;
        }

        public void SalvaCarteiraDiaria(CarteiraDiaria carteiraDiaria, ConfigXMLAnbima configXMLAnbima)
        {
            int idCarteira = carteiraDiaria.HistoricoCota.IdCarteira.Value;
            DateTime dataReferencia = carteiraDiaria.HistoricoCota.Data.Value;

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            if (configXMLAnbima.ImportaCota)
            {
                HistoricoCota historicoCotaDeletar = new HistoricoCota();
                historicoCotaDeletar.Query.Where(historicoCotaDeletar.Query.IdCarteira.Equal(idCarteira),
                                                 historicoCotaDeletar.Query.Data.Equal(dataReferencia));
                if (historicoCotaDeletar.Query.Load())
                {
                    historicoCotaDeletar.MarkAsDeleted();
                    historicoCotaDeletar.Save();
                }
            }

            if (configXMLAnbima.ImportaLiquidacao)
            {
                SaldoCaixaCollection saldoCaixaCollectionDeletar = new SaldoCaixaCollection();
                saldoCaixaCollectionDeletar.Query.Where(saldoCaixaCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                        saldoCaixaCollectionDeletar.Query.Data.Equal(dataReferencia));
                saldoCaixaCollectionDeletar.Query.Load();
                saldoCaixaCollectionDeletar.MarkAllAsDeleted();
                saldoCaixaCollectionDeletar.Save();
            }

            #region apagar posicoes abertura
            if (configXMLAnbima.ImportaLiquidacao)
            {
                LiquidacaoAberturaCollection liquidacaoAberturaCollectionDeletar = new LiquidacaoAberturaCollection();
                liquidacaoAberturaCollectionDeletar.Query.Where(liquidacaoAberturaCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                                 liquidacaoAberturaCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                liquidacaoAberturaCollectionDeletar.Query.Load();
                liquidacaoAberturaCollectionDeletar.MarkAllAsDeleted();
                liquidacaoAberturaCollectionDeletar.Save();
            }


            if (configXMLAnbima.ImportaBolsa)
            {
                PosicaoBMFAberturaCollection posicaoBMFAberturaCollectionDeletar = new PosicaoBMFAberturaCollection();
                posicaoBMFAberturaCollectionDeletar.Query.Where(posicaoBMFAberturaCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                          posicaoBMFAberturaCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoBMFAberturaCollectionDeletar.Query.Load();
                posicaoBMFAberturaCollectionDeletar.MarkAllAsDeleted();
                posicaoBMFAberturaCollectionDeletar.Save();

                PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollectionDeletar = new PosicaoBolsaAberturaCollection();
                posicaoBolsaAberturaCollectionDeletar.Query.Where(posicaoBolsaAberturaCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                            posicaoBolsaAberturaCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoBolsaAberturaCollectionDeletar.Query.Load();
                posicaoBolsaAberturaCollectionDeletar.MarkAllAsDeleted();
                posicaoBolsaAberturaCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaBTC)
            {
                PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollectionDeletar = new PosicaoEmprestimoBolsaAberturaCollection();
                posicaoEmprestimoBolsaAberturaCollectionDeletar.Query.Where(posicaoEmprestimoBolsaAberturaCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                                             posicaoEmprestimoBolsaAberturaCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoEmprestimoBolsaAberturaCollectionDeletar.Query.Load();
                posicaoEmprestimoBolsaAberturaCollectionDeletar.MarkAllAsDeleted();
                posicaoEmprestimoBolsaAberturaCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaFundos)
            {
                PosicaoFundoAberturaCollection posicaoFundoAberturaCollectionDeletar = new PosicaoFundoAberturaCollection();
                posicaoFundoAberturaCollectionDeletar.Query.Where(posicaoFundoAberturaCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                                   posicaoFundoAberturaCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoFundoAberturaCollectionDeletar.Query.Load();
                posicaoFundoAberturaCollectionDeletar.MarkAllAsDeleted();
                posicaoFundoAberturaCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaRendaFixa)
            {
                PosicaoRendaFixaAberturaCollection posicaoRendaFixaAberturaCollectionDeletar = new PosicaoRendaFixaAberturaCollection();
                posicaoRendaFixaAberturaCollectionDeletar.Query.Where(posicaoRendaFixaAberturaCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                                posicaoRendaFixaAberturaCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoRendaFixaAberturaCollectionDeletar.Query.Load();
                posicaoRendaFixaAberturaCollectionDeletar.MarkAllAsDeleted();
                posicaoRendaFixaAberturaCollectionDeletar.Save();
            }
            #endregion

            #region apagar posicoes historico
            if (configXMLAnbima.ImportaLiquidacao)
            {
                LiquidacaoHistoricoCollection liquidacaoHistoricoCollectionDeletar = new LiquidacaoHistoricoCollection();
                liquidacaoHistoricoCollectionDeletar.Query.Where(liquidacaoHistoricoCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                                 liquidacaoHistoricoCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                liquidacaoHistoricoCollectionDeletar.Query.Load();
                liquidacaoHistoricoCollectionDeletar.MarkAllAsDeleted();
                liquidacaoHistoricoCollectionDeletar.Save();
            }


            if (configXMLAnbima.ImportaBolsa)
            {
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollectionDeletar = new PosicaoBMFHistoricoCollection();
                posicaoBMFHistoricoCollectionDeletar.Query.Where(posicaoBMFHistoricoCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                          posicaoBMFHistoricoCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoBMFHistoricoCollectionDeletar.Query.Load();
                posicaoBMFHistoricoCollectionDeletar.MarkAllAsDeleted();
                posicaoBMFHistoricoCollectionDeletar.Save();

                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollectionDeletar = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollectionDeletar.Query.Where(posicaoBolsaHistoricoCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                            posicaoBolsaHistoricoCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoBolsaHistoricoCollectionDeletar.Query.Load();
                posicaoBolsaHistoricoCollectionDeletar.MarkAllAsDeleted();
                posicaoBolsaHistoricoCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaBTC)
            {
                PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollectionDeletar = new PosicaoEmprestimoBolsaHistoricoCollection();
                posicaoEmprestimoBolsaHistoricoCollectionDeletar.Query.Where(posicaoEmprestimoBolsaHistoricoCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                                             posicaoEmprestimoBolsaHistoricoCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoEmprestimoBolsaHistoricoCollectionDeletar.Query.Load();
                posicaoEmprestimoBolsaHistoricoCollectionDeletar.MarkAllAsDeleted();
                posicaoEmprestimoBolsaHistoricoCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaFundos)
            {
                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollectionDeletar = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollectionDeletar.Query.Where(posicaoFundoHistoricoCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                                   posicaoFundoHistoricoCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoFundoHistoricoCollectionDeletar.Query.Load();
                posicaoFundoHistoricoCollectionDeletar.MarkAllAsDeleted();
                posicaoFundoHistoricoCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaRendaFixa)
            {
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollectionDeletar = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollectionDeletar.Query.Where(posicaoRendaFixaHistoricoCollectionDeletar.Query.IdCliente.Equal(idCarteira),
                                                                posicaoRendaFixaHistoricoCollectionDeletar.Query.DataHistorico.Equal(dataReferencia));
                posicaoRendaFixaHistoricoCollectionDeletar.Query.Load();
                posicaoRendaFixaHistoricoCollectionDeletar.MarkAllAsDeleted();
                posicaoRendaFixaHistoricoCollectionDeletar.Save();
            }
            #endregion

            #region apagar posicoes
            if (configXMLAnbima.ImportaLiquidacao)
            {
                LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
                liquidacaoCollectionDeletar.Query.Where(liquidacaoCollectionDeletar.Query.IdCliente.Equal(idCarteira));
                liquidacaoCollectionDeletar.Query.Load();
                liquidacaoCollectionDeletar.MarkAllAsDeleted();
                liquidacaoCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaBolsa)
            {
                PosicaoBMFCollection posicaoBMFCollectionDeletar = new PosicaoBMFCollection();
                posicaoBMFCollectionDeletar.Query.Where(posicaoBMFCollectionDeletar.Query.IdCliente.Equal(idCarteira));
                posicaoBMFCollectionDeletar.Query.Load();
                posicaoBMFCollectionDeletar.MarkAllAsDeleted();
                posicaoBMFCollectionDeletar.Save();

                PosicaoBolsaCollection posicaoBolsaCollectionDeletar = new PosicaoBolsaCollection();
                posicaoBolsaCollectionDeletar.Query.Where(posicaoBolsaCollectionDeletar.Query.IdCliente.Equal(idCarteira));
                posicaoBolsaCollectionDeletar.Query.Load();
                posicaoBolsaCollectionDeletar.MarkAllAsDeleted();
                posicaoBolsaCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaBTC)
            {
                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollectionDeletar = new PosicaoEmprestimoBolsaCollection();
                posicaoEmprestimoBolsaCollectionDeletar.Query.Where(posicaoEmprestimoBolsaCollectionDeletar.Query.IdCliente.Equal(idCarteira));
                posicaoEmprestimoBolsaCollectionDeletar.Query.Load();
                posicaoEmprestimoBolsaCollectionDeletar.MarkAllAsDeleted();
                posicaoEmprestimoBolsaCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaFundos)
            {
                PosicaoFundoCollection posicaoFundoCollectionDeletar = new PosicaoFundoCollection();
                posicaoFundoCollectionDeletar.Query.Where(posicaoFundoCollectionDeletar.Query.IdCliente.Equal(idCarteira));
                posicaoFundoCollectionDeletar.Query.Load();
                posicaoFundoCollectionDeletar.MarkAllAsDeleted();
                posicaoFundoCollectionDeletar.Save();
            }

            if (configXMLAnbima.ImportaRendaFixa)
            {
                PosicaoRendaFixaCollection posicaoRendaFixaCollectionDeletar = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollectionDeletar.Query.Where(posicaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(idCarteira));
                posicaoRendaFixaCollectionDeletar.Query.Load();
                posicaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
                posicaoRendaFixaCollectionDeletar.Save();
            }
            #endregion


            if (configXMLAnbima.ImportaCota)
            {
                carteiraDiaria.HistoricoCota.Save();
            }

            if (configXMLAnbima.ImportaLiquidacao)
            {
                carteiraDiaria.LiquidacaoCollection.Save();
                carteiraDiaria.SaldoCaixa.Save();
            }

            if (configXMLAnbima.ImportaBolsa)
            {
                carteiraDiaria.PosicaoBMFCollection.Save();
                carteiraDiaria.PosicaoBMFOpcoesCollection.Save();
                carteiraDiaria.PosicaoBolsaCollection.Save();
                carteiraDiaria.PosicaoBolsaOpcoesCollection.Save();
            }

            if (configXMLAnbima.ImportaBTC)
            {
                carteiraDiaria.PosicaoEmprestimoBolsaCollection.Save();
            }

            if (configXMLAnbima.ImportaFundos)
            {
                carteiraDiaria.PosicaoFundoCollection.Save();
            }

            if (configXMLAnbima.ImportaRendaFixa)
            {
                //gera operação de Renda Fixa.                
                OperacaoRendaFixaCollection operacaoRFCollectionDel = new OperacaoRendaFixaCollection();
                operacaoRFCollectionDel.Query.Where(operacaoRFCollectionDel.Query.IdCliente.Equal(idCarteira),
                                                    operacaoRFCollectionDel.Query.DataRegistro.Equal(dataReferencia));
                operacaoRFCollectionDel.Query.Load();
                operacaoRFCollectionDel.MarkAllAsDeleted();
                operacaoRFCollectionDel.Save();                

                foreach (PosicaoRendaFixa posicaoRFDebenture in carteiraDiaria.PosicaoRendaFixaDebentureCollection)
                {
                    #region OperacaoRendaFixa
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.IdCliente = posicaoRFDebenture.IdCliente;
                    operacaoRendaFixa.IdTitulo = posicaoRFDebenture.IdTitulo;
                    operacaoRendaFixa.TipoOperacao = posicaoRFDebenture.TipoOperacao;
                    operacaoRendaFixa.Quantidade = posicaoRFDebenture.Quantidade;
                    operacaoRendaFixa.DataOperacao = posicaoRFDebenture.DataOperacao;
                    operacaoRendaFixa.PUOperacao = posicaoRFDebenture.PUMercado;
                    operacaoRendaFixa.TaxaOperacao = posicaoRFDebenture.TaxaOperacao;
                    operacaoRendaFixa.DataVolta = posicaoRFDebenture.DataVolta;
                    operacaoRendaFixa.TaxaVolta = posicaoRFDebenture.TaxaVolta;
                    operacaoRendaFixa.PUVolta = posicaoRFDebenture.PUVolta;
                    operacaoRendaFixa.IdCustodia = posicaoRFDebenture.IdCustodia;
                    operacaoRendaFixa.TipoNegociacao = posicaoRFDebenture.TipoNegociacao;
                    operacaoRendaFixa.DataRegistro = dataReferencia;
                    operacaoRendaFixa.DataLiquidacao = posicaoRFDebenture.DataLiquidacao;
                    operacaoRendaFixa.Valor = posicaoRFDebenture.ValorMercado;
                    operacaoRendaFixa.IdCustodia = (byte)LocalCustodiaFixo.CBLC;
                    operacaoRendaFixa.IdLiquidacao = (byte)ClearingFixo.CBLC;
                    operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.OpenVirtual;               
                    operacaoRendaFixa.Save();
                    #endregion

                    //atualiza IdOperacao na posicao
                    posicaoRFDebenture.IdOperacao = operacaoRendaFixa.IdOperacao; 
                    
                }
                carteiraDiaria.PosicaoRendaFixaDebentureCollection.Save();


                foreach (PosicaoRendaFixa posicaoRFPrivado in carteiraDiaria.PosicaoRendaFixaPrivadoCollection)
                {
                    #region OperacaoRendaFixa
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.IdCliente = posicaoRFPrivado.IdCliente;
                    operacaoRendaFixa.IdTitulo = posicaoRFPrivado.IdTitulo;
                    operacaoRendaFixa.TipoOperacao = posicaoRFPrivado.TipoOperacao;
                    operacaoRendaFixa.Quantidade = posicaoRFPrivado.Quantidade;
                    operacaoRendaFixa.DataOperacao = posicaoRFPrivado.DataOperacao;
                    operacaoRendaFixa.PUOperacao = posicaoRFPrivado.PUMercado;
                    operacaoRendaFixa.TaxaOperacao = posicaoRFPrivado.TaxaOperacao;
                    operacaoRendaFixa.DataVolta = posicaoRFPrivado.DataVolta;
                    operacaoRendaFixa.TaxaVolta = posicaoRFPrivado.TaxaVolta;
                    operacaoRendaFixa.PUVolta = posicaoRFPrivado.PUVolta;
                    operacaoRendaFixa.IdCustodia = posicaoRFPrivado.IdCustodia;
                    operacaoRendaFixa.TipoNegociacao = posicaoRFPrivado.TipoNegociacao;
                    operacaoRendaFixa.DataRegistro = dataReferencia;
                    operacaoRendaFixa.DataLiquidacao = posicaoRFPrivado.DataLiquidacao;
                    operacaoRendaFixa.Valor = posicaoRFPrivado.ValorMercado;
                    operacaoRendaFixa.IdCustodia = (byte)LocalCustodiaFixo.CBLC;
                    operacaoRendaFixa.IdLiquidacao = (byte)ClearingFixo.CBLC;
                    operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.OpenVirtual;                   
                    operacaoRendaFixa.Save();
                    #endregion

                    //atualiza IdOperacao na posicao
                    posicaoRFPrivado.IdOperacao = operacaoRendaFixa.IdOperacao;

                }
                carteiraDiaria.PosicaoRendaFixaPrivadoCollection.Save();


                foreach (PosicaoRendaFixa posicaoRFPublico in carteiraDiaria.PosicaoRendaFixaPublicoCollection)
                {
                    #region OperacaoRendaFixa
                    OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                    operacaoRendaFixa.IdCliente = posicaoRFPublico.IdCliente;
                    operacaoRendaFixa.IdTitulo = posicaoRFPublico.IdTitulo;
                    operacaoRendaFixa.TipoOperacao = posicaoRFPublico.TipoOperacao;
                    operacaoRendaFixa.Quantidade = posicaoRFPublico.Quantidade;
                    operacaoRendaFixa.DataOperacao = posicaoRFPublico.DataOperacao;
                    operacaoRendaFixa.PUOperacao = posicaoRFPublico.PUMercado;
                    operacaoRendaFixa.TaxaOperacao = posicaoRFPublico.TaxaOperacao;
                    operacaoRendaFixa.DataVolta = posicaoRFPublico.DataVolta;
                    operacaoRendaFixa.TaxaVolta = posicaoRFPublico.TaxaVolta;
                    operacaoRendaFixa.PUVolta = posicaoRFPublico.PUVolta;
                    operacaoRendaFixa.IdCustodia = posicaoRFPublico.IdCustodia;
                    operacaoRendaFixa.TipoNegociacao = posicaoRFPublico.TipoNegociacao;
                    operacaoRendaFixa.DataRegistro = dataReferencia;
                    operacaoRendaFixa.DataLiquidacao = posicaoRFPublico.DataLiquidacao;
                    operacaoRendaFixa.Valor = posicaoRFPublico.ValorMercado;
                    operacaoRendaFixa.IdCustodia = (byte)LocalCustodiaFixo.CBLC;
                    operacaoRendaFixa.IdLiquidacao = (byte)ClearingFixo.CBLC;
                    operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.OpenVirtual;
                    operacaoRendaFixa.Save();
                    #endregion

                    //atualiza IdOperacao na posicao
                    posicaoRFPublico.IdOperacao = operacaoRendaFixa.IdOperacao;

                }
                carteiraDiaria.PosicaoRendaFixaPublicoCollection.Save();
                
                carteiraDiaria.PosicaoTermoBolsaCollection.Save();
            }


            //Refazer para abertura
            if (configXMLAnbima.ImportaLiquidacao)
            {
                LiquidacaoAberturaCollection liquidacaoAberturaCollection = new LiquidacaoAberturaCollection(carteiraDiaria.LiquidacaoCollection, dataReferencia);
                liquidacaoAberturaCollection.Save();

                LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection(carteiraDiaria.LiquidacaoCollection, dataReferencia);
                liquidacaoHistoricoCollection.Save();
            }

            if (configXMLAnbima.ImportaBolsa)
            {
                PosicaoBMFAberturaCollection posicaoBMFAberturaCollection = new PosicaoBMFAberturaCollection(carteiraDiaria.PosicaoBMFCollection, dataReferencia);
                posicaoBMFAberturaCollection.Save();

                PosicaoBMFAberturaCollection posicaoBMFOpcoesAberturaCollection = new PosicaoBMFAberturaCollection(carteiraDiaria.PosicaoBMFOpcoesCollection, dataReferencia);
                posicaoBMFOpcoesAberturaCollection.Save();

                PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection(carteiraDiaria.PosicaoBolsaCollection, dataReferencia);
                posicaoBolsaAberturaCollection.Save();

                PosicaoBolsaAberturaCollection posicaoBolsaOpcoesAberturaCollection = new PosicaoBolsaAberturaCollection(carteiraDiaria.PosicaoBolsaOpcoesCollection, dataReferencia);
                posicaoBolsaOpcoesAberturaCollection.Save();



                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection(carteiraDiaria.PosicaoBMFCollection, dataReferencia);
                posicaoBMFHistoricoCollection.Save();

                PosicaoBMFHistoricoCollection posicaoBMFOpcoesHistoricoCollection = new PosicaoBMFHistoricoCollection(carteiraDiaria.PosicaoBMFOpcoesCollection, dataReferencia);
                posicaoBMFOpcoesHistoricoCollection.Save();

                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection(carteiraDiaria.PosicaoBolsaCollection, dataReferencia);
                posicaoBolsaHistoricoCollection.Save();

                PosicaoBolsaHistoricoCollection posicaoBolsaOpcoesHistoricoCollection = new PosicaoBolsaHistoricoCollection(carteiraDiaria.PosicaoBolsaOpcoesCollection, dataReferencia);
                posicaoBolsaOpcoesHistoricoCollection.Save();
            }

            if (configXMLAnbima.ImportaBTC)
            {
                PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection(carteiraDiaria.PosicaoEmprestimoBolsaCollection, dataReferencia);
                posicaoEmprestimoBolsaAberturaCollection.Save();

                PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection(carteiraDiaria.PosicaoEmprestimoBolsaCollection, dataReferencia);
                posicaoEmprestimoBolsaHistoricoCollection.Save();
            }

            if (configXMLAnbima.ImportaFundos)
            {
                PosicaoFundoAberturaCollection posicaoFundoAberturaCollection = new PosicaoFundoAberturaCollection(carteiraDiaria.PosicaoFundoCollection, dataReferencia);
                posicaoFundoAberturaCollection.Save();

                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection(carteiraDiaria.PosicaoFundoCollection, dataReferencia);
                posicaoFundoHistoricoCollection.Save();
            }

            if (configXMLAnbima.ImportaRendaFixa)
            {
                PosicaoRendaFixaAberturaCollection posicaoRendaFixaDebentureAberturaCollection = new PosicaoRendaFixaAberturaCollection(carteiraDiaria.PosicaoRendaFixaDebentureCollection, dataReferencia);
                posicaoRendaFixaDebentureAberturaCollection.Save();

                PosicaoRendaFixaAberturaCollection posicaoRendaFixaPrivadoAberturaCollection = new PosicaoRendaFixaAberturaCollection(carteiraDiaria.PosicaoRendaFixaPrivadoCollection, dataReferencia);
                posicaoRendaFixaPrivadoAberturaCollection.Save();

                PosicaoRendaFixaAberturaCollection posicaoRendaFixaPublicoAberturaCollection = new PosicaoRendaFixaAberturaCollection(carteiraDiaria.PosicaoRendaFixaPublicoCollection, dataReferencia);
                posicaoRendaFixaPublicoAberturaCollection.Save();

                PosicaoTermoBolsaAberturaCollection posicaoTermoBolsaAberturaCollection = new PosicaoTermoBolsaAberturaCollection(carteiraDiaria.PosicaoTermoBolsaCollection, dataReferencia);
                posicaoTermoBolsaAberturaCollection.Save();


                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaDebentureHistoricoCollection = new PosicaoRendaFixaHistoricoCollection(carteiraDiaria.PosicaoRendaFixaDebentureCollection, dataReferencia);
                posicaoRendaFixaDebentureHistoricoCollection.Save();

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaPrivadoHistoricoCollection = new PosicaoRendaFixaHistoricoCollection(carteiraDiaria.PosicaoRendaFixaPrivadoCollection, dataReferencia);
                posicaoRendaFixaPrivadoHistoricoCollection.Save();

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaPublicoHistoricoCollection = new PosicaoRendaFixaHistoricoCollection(carteiraDiaria.PosicaoRendaFixaPublicoCollection, dataReferencia);
                posicaoRendaFixaPublicoHistoricoCollection.Save();

                PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection(carteiraDiaria.PosicaoTermoBolsaCollection, dataReferencia);
                posicaoTermoBolsaHistoricoCollection.Save();
            }

            cliente.Status = (byte)StatusCliente.Fechado;
            cliente.Save();
        }

        /// <summary>
        /// Retorna o valor total da carteira, levando em cota o número de dias para liquidação passado. Se passar -1 retorna o valor total da carteira.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="diasLiquidacao"></param>
        /// <returns></returns>
        public decimal RetornaCarteiraLiquida(int idCarteira, int diasLiquidacao, DateTime? dataHistorico)
        {
            decimal valorCarteira = 0;
            if (dataHistorico.HasValue)
            {
                PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                valorCarteira += posicaoRendaFixaHistorico.RetornaValorMercado(idCarteira, dataHistorico.Value);

                if (diasLiquidacao >= 3)
                {
                    PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                    valorCarteira += posicaoBolsaHistorico.RetornaValorMercadoAcoes(idCarteira, dataHistorico.Value);
                }

                if (diasLiquidacao >= 1)
                {
                    PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                    valorCarteira += posicaoBolsaHistorico.RetornaValorMercadoOpcaoPosicaoComprada(idCarteira, dataHistorico.Value);
                }

                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
                valorCarteira += posicaoEmprestimoBolsaHistorico.RetornaValorMercadoNet(idCarteira, dataHistorico.Value);

                PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                valorCarteira += posicaoFundoHistorico.RetornaValorMercado(idCarteira, dataHistorico.Value, diasLiquidacao);
            }
            else
            {
                PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                valorCarteira += posicaoRendaFixa.RetornaValorMercado(idCarteira);

                if (diasLiquidacao >= 3)
                {
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    valorCarteira += posicaoBolsa.RetornaValorMercadoAcoes(idCarteira);
                }

                if (diasLiquidacao >= 1)
                {
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    valorCarteira += posicaoBolsa.RetornaValorMercadoOpcaoPosicaoComprada(idCarteira);
                }

                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
                valorCarteira += posicaoEmprestimoBolsa.RetornaValorMercadoNet(idCarteira);

                PosicaoFundo posicaoFundo = new PosicaoFundo();
                valorCarteira += posicaoFundo.RetornaValorMercado(idCarteira, diasLiquidacao);
            }

            return valorCarteira;
        }

        /// <summary>
        /// Retorna prazo médio da carteira, de acordo com a metodologia da Receita.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataReferencia"></param>
        /// <returns></returns>
        public decimal RetornaPrazoMedio(int idCliente, DateTime dataReferencia)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            bool historico = dataReferencia < cliente.DataDia.Value;

            decimal prazoMedio = 0;

            decimal totalFinanceiro = 0;
            decimal totalFinanceiroPrazo = 0;

            #region Caixa
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.BuscaSaldoCaixa(idCliente, dataReferencia);

            decimal valorCaixa = saldoCaixa.SaldoFechamento.HasValue ? saldoCaixa.SaldoFechamento.Value : 0;
            totalFinanceiro += valorCaixa;
            totalFinanceiroPrazo += valorCaixa;
            #endregion

            #region Fundos
            if (historico)
            {
                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                             posicaoFundoHistoricoCollection.Query.ValorBruto.Sum());
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                            posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(dataReferencia));
                posicaoFundoHistoricoCollection.Query.GroupBy(posicaoFundoHistoricoCollection.Query.IdCarteira);
                posicaoFundoHistoricoCollection.Query.Load();

                foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                {
                    int idCarteira = posicaoFundoHistorico.IdCarteira.Value;
                    decimal valor = posicaoFundoHistorico.ValorBruto.Value;

                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.TipoTributacao);
                    carteira.LoadByPrimaryKey(campos, idCarteira);
                    if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.CurtoPrazo)
                    {
                        totalFinanceiro += valor;
                        totalFinanceiroPrazo += valor;
                    }
                    else if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.LongoPrazo)
                    {
                        totalFinanceiro += valor;
                        totalFinanceiroPrazo += valor * 366M;
                    }
                }
            }
            else
            {
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira,
                                                    posicaoFundoCollection.Query.ValorBruto.Sum());
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCarteira);
                posicaoFundoCollection.Query.Load();

                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    int idCarteira = posicaoFundo.IdCarteira.Value;
                    decimal valor = posicaoFundo.ValorBruto.Value;

                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.TipoTributacao);
                    carteira.LoadByPrimaryKey(campos, idCarteira);
                    if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.CurtoPrazo)
                    {
                        totalFinanceiro += valor;
                        totalFinanceiroPrazo += valor;
                    }
                    else if (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.LongoPrazo)
                    {
                        totalFinanceiro += valor;
                        totalFinanceiroPrazo += valor * 366M;
                    }
                }
            }
            #endregion

            if (historico)
            {
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollectionCompromissada = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaCollectionCompromissada.Query.Select(posicaoRendaFixaCollectionCompromissada.Query.DataVencimento,
                                                                     posicaoRendaFixaCollectionCompromissada.Query.ValorMercado.Sum());
                posicaoRendaFixaCollectionCompromissada.Query.Where(posicaoRendaFixaCollectionCompromissada.Query.IdCliente.Equal(idCliente),
                                                                    posicaoRendaFixaCollectionCompromissada.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                                                    posicaoRendaFixaCollectionCompromissada.Query.DataHistorico.Equal(dataReferencia));
                posicaoRendaFixaCollectionCompromissada.Query.GroupBy(posicaoRendaFixaCollectionCompromissada.Query.DataVencimento);
                posicaoRendaFixaCollectionCompromissada.Query.Load();

                foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollectionCompromissada)
                {
                    int numeroDias = Calendario.NumeroDias(dataReferencia, posicaoRendaFixa.DataVencimento.Value);

                    totalFinanceiro += posicaoRendaFixa.ValorMercado.Value;
                    totalFinanceiroPrazo += posicaoRendaFixa.ValorMercado.Value * numeroDias;
                }

                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollectionFinal = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaCollectionFinal.Query.Select(posicaoRendaFixaCollectionFinal.Query.IdTitulo,
                                                             posicaoRendaFixaCollectionFinal.Query.ValorMercado.Sum());
                posicaoRendaFixaCollectionFinal.Query.Where(posicaoRendaFixaCollectionFinal.Query.IdCliente.Equal(idCliente),
                                                            posicaoRendaFixaCollectionFinal.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                                            posicaoRendaFixaCollectionFinal.Query.DataHistorico.Equal(dataReferencia));
                posicaoRendaFixaCollectionFinal.Query.GroupBy(posicaoRendaFixaCollectionFinal.Query.IdTitulo);
                posicaoRendaFixaCollectionFinal.Query.Load();

                foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollectionFinal)
                {
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    decimal prazoMedioTitulo = tituloRendaFixa.RetornaPrazoMedioTitulo(posicaoRendaFixa.IdTitulo.Value, dataReferencia);

                    totalFinanceiro += posicaoRendaFixa.ValorMercado.Value;
                    totalFinanceiroPrazo += posicaoRendaFixa.ValorMercado.Value * prazoMedioTitulo;
                }
            }
            else
            {
                PosicaoRendaFixaCollection posicaoRendaFixaCollectionCompromissada = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollectionCompromissada.Query.Select(posicaoRendaFixaCollectionCompromissada.Query.DataVencimento,
                                                                     posicaoRendaFixaCollectionCompromissada.Query.ValorMercado.Sum());
                posicaoRendaFixaCollectionCompromissada.Query.Where(posicaoRendaFixaCollectionCompromissada.Query.IdCliente.Equal(idCliente),
                                                                    posicaoRendaFixaCollectionCompromissada.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda));
                posicaoRendaFixaCollectionCompromissada.Query.GroupBy(posicaoRendaFixaCollectionCompromissada.Query.DataVencimento);
                posicaoRendaFixaCollectionCompromissada.Query.Load();

                foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollectionCompromissada)
                {
                    int numeroDias = Calendario.NumeroDias(dataReferencia, posicaoRendaFixa.DataVencimento.Value);

                    totalFinanceiro += posicaoRendaFixa.ValorMercado.Value;
                    totalFinanceiroPrazo += posicaoRendaFixa.ValorMercado.Value * numeroDias;
                }

                PosicaoRendaFixaCollection posicaoRendaFixaCollectionFinal = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollectionFinal.Query.Select(posicaoRendaFixaCollectionFinal.Query.IdTitulo,
                                                             posicaoRendaFixaCollectionFinal.Query.ValorMercado.Sum());
                posicaoRendaFixaCollectionFinal.Query.Where(posicaoRendaFixaCollectionFinal.Query.IdCliente.Equal(idCliente),
                                                            posicaoRendaFixaCollectionFinal.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));
                posicaoRendaFixaCollectionFinal.Query.GroupBy(posicaoRendaFixaCollectionFinal.Query.IdTitulo);
                posicaoRendaFixaCollectionFinal.Query.Load();

                foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollectionFinal)
                {
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    decimal prazoMedioTitulo = tituloRendaFixa.RetornaPrazoMedioTitulo(posicaoRendaFixa.IdTitulo.Value, dataReferencia);

                    totalFinanceiro += posicaoRendaFixa.ValorMercado.Value;
                    totalFinanceiroPrazo += posicaoRendaFixa.ValorMercado.Value * prazoMedioTitulo;
                }
            }

            if (totalFinanceiro != 0)
            {
                prazoMedio = totalFinanceiroPrazo / totalFinanceiro;
            }

            return prazoMedio;
        }

        public void ImportaFundoYMF(FundoYMF fundoYMF, FundoYMFInMemory fundoYMFInMemory)
        {
            
            const string NOME_AGENTE_MERCADO = "Padrão";

            Pessoa pessoa = new PessoaCollection().BuscaPessoaPorCodigoInterface(fundoYMF.CdCliente);
            if (pessoa == null)
            {
                throw new Exception("Não foi possível encontrar o cliente cadastrado com o código YMF: " + fundoYMF.CdCliente);
            }

            if (!this.IdCarteira.HasValue)
            {
                this.IdCarteira = pessoa.IdPessoa;
            }

            this.Nome = pessoa.Nome;
            this.Apelido = pessoa.Apelido;

            FundoParametroYMF fundoParametroYMF = fundoYMFInMemory.FundoParametroDictionary[fundoYMF.CdFundo];
            this.TipoCota = (byte)(fundoParametroYMF.IcAfCalculoCota == Financial.Interfaces.Import.YMF.Enums.TipoCalculoCotaYMF.Abertura ?
                TipoCotaFundo.Abertura : TipoCotaFundo.Fechamento);

            TipoFundoYMF tipoFundoYMF;
            try
            {
                tipoFundoYMF = fundoYMFInMemory.TipoFundoDictionary[fundoParametroYMF.CdTipoFundo];
            }
            catch
            {
                tipoFundoYMF = new TipoFundoYMF();
            }
            this.TipoCarteira = (byte)(tipoFundoYMF.IcFvCarteira == Financial.Interfaces.Import.YMF.Enums.TipoCarteiraYMF.RendaFixa ?
                TipoCarteiraFundo.RendaFixa : TipoCarteiraFundo.RendaVariavel);

            this.StatusAtivo = (byte)StatusAtivoCarteira.Ativo;
            this.IdIndiceBenchmark = ID_INDICE_BENCHMARK;

            this.CasasDecimaisCota = (byte)fundoParametroYMF.QtDecimalValorCota;
            this.CasasDecimaisQuantidade = (byte)fundoParametroYMF.QtDecimalQtdeCotas;
            this.TruncaCota = (fundoParametroYMF.IcAtValorCota == Financial.Interfaces.Import.YMF.Enums.TruncaCotaYMF.Trunca ?
                "S" : "N");
            this.TruncaQuantidade = (fundoParametroYMF.IcAtQuantidadeCotas == Financial.Interfaces.Import.YMF.Enums.TruncaCotaYMF.Trunca ?
                "S" : "N");
            this.TruncaFinanceiro = (fundoParametroYMF.IcAtFinanceiro == Financial.Interfaces.Import.YMF.Enums.TruncaCotaYMF.Trunca ?
                "S" : "N");

            this.CotaInicial = 1;

            if (fundoYMFInMemory.FundoLimiteDictionary.ContainsKey(fundoYMF.CdFundo))
            {
                FundoLimiteYMF fundoLimiteYMF = fundoYMFInMemory.FundoLimiteDictionary[fundoYMF.CdFundo];
                this.ValorMinimoAplicacao = fundoLimiteYMF.VlAplicacaoMinimo;
                this.ValorMinimoResgate = fundoLimiteYMF.VlResgateMinimo;
                this.ValorMinimoSaldo = fundoLimiteYMF.VlSaldoMinimo;
                this.ValorMaximoAplicacao = fundoLimiteYMF.VlAplicacaoMaximo;
                this.ValorMinimoInicial = fundoLimiteYMF.VlAplicacaoInicial;
            }
            else
            {
                this.ValorMinimoAplicacao = 0;
                this.ValorMinimoResgate = 0;
                this.ValorMinimoSaldo = 0;
                this.ValorMaximoAplicacao = 0;
                this.ValorMinimoInicial = 0;
            }

            this.TipoRentabilidade = (byte)(fundoParametroYMF.IcUpCalculoRentab == Financial.Interfaces.Import.YMF.Enums.IcUpCalculoRentab.PrimeiroDia ?
                TipoRentabilidadeFundo.PrimeiroPrimeiro : TipoRentabilidadeFundo.FinalFinal);

            FundoIRYMF fundoIRYMF = fundoYMFInMemory.FundoIRDictionary[fundoYMF.CdFundo];
            this.TipoCusto = (byte)(fundoIRYMF.IcSnCustoMedio == "S" ?
                TipoCustoFundo.MedioAplicado : TipoCustoFundo.Aplicacao);

            this.DiasCotizacaoAplicacao = (byte)fundoParametroYMF.QtDiasLiqFisEntrada;
            this.DiasCotizacaoResgate = (byte)fundoParametroYMF.QtDiasLiqFisSaida;
            this.DiasLiquidacaoAplicacao = (byte)fundoParametroYMF.QtDiasLiqFinanEntrada;
            this.DiasLiquidacaoResgate = (byte)fundoParametroYMF.QtDiasLiqFinanSaida;

            if (this.TipoCarteira == (byte)TipoCarteiraFundo.RendaFixa)
            {
                this.TipoTributacao = (byte)(fundoIRYMF.IcLcCarteiraFundo == Financial.Interfaces.Import.YMF.Enums.IcLcCarteiraFundo.CurtoPrazo ?
               TipoTributacaoFundo.CurtoPrazo : TipoTributacaoFundo.LongoPrazo);

                this.CalculaIOF = "S";
            }
            else
            {
                //Carteira de Renda variável
                this.TipoTributacao = (byte)TipoTributacaoFundo.Acoes;
                this.CalculaIOF = "N";
            }

            this.DiasAniversario = 0;
            this.TipoAniversario = 0;

            CategoriaFundoCollection categoriasFundo = new CategoriaFundoCollection();
            categoriasFundo.Query.Where(categoriasFundo.Query.Descricao.ToUpper().Like("%FUNDO%"));
            categoriasFundo.Query.Load();
            SubCategoriaFundoCollection subCategoriasFundo = new SubCategoriaFundoCollection();
            subCategoriasFundo.Query.Where(subCategoriasFundo.Query.Descricao.ToUpper().Like("%FUNDO%"));
            subCategoriasFundo.Query.Load();

            this.IdCategoria = categoriasFundo[0].IdCategoria;
            this.IdSubCategoria = subCategoriasFundo[0].IdSubCategoria;

            AgenteMercado agenteMercado = new AgenteMercadoCollection().BuscaAgenteMercadoPorNome(NOME_AGENTE_MERCADO);
            if (agenteMercado == null)
            {
                throw new Exception("Agente de mercado não encontrado: " + NOME_AGENTE_MERCADO);
            }

            this.IdAgenteAdministrador = agenteMercado.IdAgente;
            this.IdAgenteGestor = agenteMercado.IdAgente;
            this.IdAgenteCustodiante = agenteMercado.IdAgente;

            this.CobraTaxaFiscalizacaoCVM = "N";

            this.DataInicioCota = DateTime.Now;
            this.CalculaEnquadra = "N";
            this.TipoCalculoRetorno = (byte)CalculoRetornoCarteira.Cota;

            this.ContagemDiasConversaoResgate = (byte)(fundoParametroYMF.IcUcCalcDiasFinanSai == Financial.Interfaces.Import.YMF.Enums.IcUcCalcDiasFinanSai.Uteis ?
                ContagemDiasLiquidacaoResgate.DiasUteis : ContagemDiasLiquidacaoResgate.DiasCorridos);

            this.ProjecaoIRResgate = (byte)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate;

            this.TipoVisaoFundo = (byte)Financial.Captacao.Enums.TipoVisaoFundoRebate.NaoTrata;
            this.DistribuicaoDividendo = (byte)DistribuicaoDividendosCarteira.NaoDistribui;

            this.ContagemPrazoIOF = (byte)ContagemDiasPrazoIOF.Direta;
            this.PrioridadeOperacao = (byte)PrioridadeOperacaoFundo.ResgateAntes;

            this.CompensacaoPrejuizo = (byte)(fundoParametroYMF.IcBlCalculoRendCompensacao == Financial.Interfaces.Import.YMF.Enums.IcBlCalculoRendCompensacao.Bruto ?
                TipoCompensacaoPrejuizo.Padrao : TipoCompensacaoPrejuizo.Direto);

        }

        public void GuardaInfoHistoricoCotaYMF(HistoricoCota historicoCotaMaisAntiga)
        {
            this.CotaInicial = (this.TipoCota == (byte)TipoCotaFundo.Abertura) ?
                historicoCotaMaisAntiga.CotaAbertura : historicoCotaMaisAntiga.CotaFechamento;
            this.DataInicioCota = historicoCotaMaisAntiga.Data;
        }

        public void ImportaFundoSIAnbid(string codigoAnbid)
        {
            ImportaFundoSIAnbid(codigoAnbid, Convert.ToInt32(codigoAnbid), null);
        }

        public void ImportaFundoSIAnbid(string codigoAnbid, int? idCarteira, int? idPessoa)
        {
            Financial.Interfaces.Import.RendaFixa.SIAnbid.Fundo fundo = new Financial.Interfaces.Import.RendaFixa.SIAnbid.Fundo();
            fundo.LoadByCodFundo(codigoAnbid, null, true);

            Cliente cliente = new Cliente();
            if (!idCarteira.HasValue)
            {                
                cliente.Query.Select(cliente.Query.IdCliente.Max());
                if (cliente.Query.Load())
                    idCarteira = cliente.IdCliente.GetValueOrDefault(0) + 1;
            }

            Pessoa pessoa = new Pessoa();
            if (!idPessoa.HasValue)
            {                
                pessoa.Query.Select(pessoa.Query.IdPessoa.Max());
                if (pessoa.Query.Load())
                    idPessoa = pessoa.IdPessoa.GetValueOrDefault(0) + 1;
            }

            this.IdCarteira = idCarteira.Value;
            this.Nome = fundo.NomeFantasia;
            this.Apelido = fundo.NomeFantasia;

            if (!string.IsNullOrEmpty(fundo.CotaAbertura))
            {
                if (fundo.CotaAbertura.ToUpper() == "S")
                {
                    this.TipoCota = (byte)TipoCotaFundo.Abertura;
                    this.TipoRentabilidade = (byte)TipoRentabilidadeFundo.PrimeiroPrimeiro;
                }
                else
                {
                    this.TipoCota = (byte)TipoCotaFundo.Fechamento;
                    this.TipoRentabilidade = (byte)TipoRentabilidadeFundo.FinalFinal;
                }
            }

            if (fundo.IdIndiceBenchmark == 0 || fundo.IdIndiceBenchmark == null)
            {
                fundo.IdIndiceBenchmark = ID_INDICE_BENCHMARK;
            }

            this.TipoCarteira = fundo.TipoCarteira;
            this.StatusAtivo = (byte)StatusAtivoCarteira.Ativo;
            this.IdIndiceBenchmark = fundo.IdIndiceBenchmark == 0 ? ListaIndiceFixo.CDI : fundo.IdIndiceBenchmark;
            this.CasasDecimaisCota = 8;
            this.CasasDecimaisQuantidade = 8;
            this.TruncaCota = "N";
            this.TruncaQuantidade = "N";
            this.TruncaFinanceiro = "N";
            this.CotaInicial = fundo.CotaInicial;
            this.ValorMinimoAplicacao = fundo.ValorMinimoAplicacao;
            this.ValorMinimoResgate = fundo.ValorMinimoResgate;
            this.ValorMinimoSaldo = fundo.ValorMinimoSaldo;
            this.ValorMinimoInicial = fundo.ValorMinimoInicial;
            this.TipoCusto = (byte)TipoCustoFundo.Aplicacao;
            this.DiasCotizacaoAplicacao = fundo.DiasCotizacaoAplicacao;
            this.DiasCotizacaoResgate = fundo.DiasCotizacaoResgate;
            this.DiasLiquidacaoAplicacao = fundo.DiasLiquidacaoAplicacao;
            this.TipoTributacao = fundo.TipoTributacao;
            this.CalculaIOF = fundo.CalculaIOF;
            this.DiasAniversario = 0;
            this.TipoAniversario = 0;

            if (fundo.IdCategoria.HasValue)
            {
                CategoriaFundo categoria = new CategoriaFundo();
                SubCategoriaFundo subCategoria = new SubCategoriaFundo();

                int idCategoria = fundo.IdCategoria.Value;
                if (!categoria.LoadByPrimaryKey(idCategoria))
                {
                    string descricao = idCategoria + " - Importação Fundo - SIAN";
                    int idListaCategoria = (int)ListaCategoriaFundoFixo.Padrao;

                    categoria.CriaCategoraFundo(idCategoria, descricao, idListaCategoria);
                    subCategoria.CriaSubCategoraFundo(idCategoria, idCategoria, descricao);
                }
            }

            this.IdCategoria = fundo.IdCategoria;
            this.IdSubCategoria = fundo.IdCategoria;

            this.PrioridadeOperacao = (byte)PrioridadeOperacaoFundo.ResgateAntes;

            //Criar agente mercado se nao existir
            AgenteMercado agenteAdministrador = new AgenteMercado();
            if (!agenteAdministrador.BuscaPorCodigoAnbid(fundo.CodigoAdministrador))
            {
                string strCodAnbid = "Cód.Anbid - " + fundo.CodigoAdministrador;
                throw new Exception("Agentes de mercado com " + strCodAnbid + " não encontrado, favor cadastrar/atualizar o agente: '" + fundo.NomeAdministrador + "' com o mesmo");
            }

            this.IdAgenteAdministrador = agenteAdministrador.IdAgente;
            this.IdAgenteCustodiante = agenteAdministrador.IdAgente;

            if (string.IsNullOrEmpty(fundo.CodigoGestor))
            {
                this.IdAgenteGestor = agenteAdministrador.IdAgente;
            }
            else
            {
                AgenteMercado agenteGestor = new AgenteMercado();
                if (!agenteGestor.BuscaPorCodigoAnbid(fundo.CodigoGestor))
                {
                    string strCodAnbid = "Cód.Anbid - " + fundo.CodigoGestor;
                    throw new Exception("Agentes de mercado com " + strCodAnbid + " não encontrado, favor cadastrar/atualizar o agente: '" + fundo.NomeGestor + "' com o mesmo");
                }
                this.IdAgenteGestor = agenteGestor.IdAgente;
            }

            this.CobraTaxaFiscalizacaoCVM = "N";
            this.DataInicioCota = fundo.DataInicioCota;
            this.CalculaEnquadra = "N";

            if(this.DiasCotizacaoResgate < 5)
            {
                this.DiasLiquidacaoResgate = fundo.DiasLiquidacaoResgate;
                this.ContagemDiasConversaoResgate = (int)ContagemDiasLiquidacaoResgate.DiasUteis;
            }
            else
            {
                int diasLiqReg = fundo.DiasCotizacaoResgate.Value - fundo.DiasLiquidacaoResgate.Value;
                diasLiqReg = Math.Abs(diasLiqReg);
                this.DiasLiquidacaoResgate = Convert.ToByte(diasLiqReg);
                this.ContagemDiasConversaoResgate = (int)ContagemDiasLiquidacaoResgate.DiasCorridos;
            }

            if (this.UpToAgenteMercadoByIdAgenteAdministrador.Nome.ToUpper().Contains("MELLON"))
            {
                this.ContagemPrazoIOF = (byte)ContagemDiasPrazoIOF.Mellon;
            }
            else
            {
                this.ContagemPrazoIOF = (byte)ContagemDiasPrazoIOF.PrazoCorrido;
            }

            this.ProjecaoIRResgate = (byte)Financial.Tributo.Custom.CalculoTributo.ProjecaoIRCotista.DiaLiquidacaoResgate;
            this.TipoVisaoFundo = (byte)Financial.Captacao.Enums.TipoVisaoFundoRebate.NaoTrata;
            this.DistribuicaoDividendo = (byte)DistribuicaoDividendosCarteira.NaoDistribui;
            this.CompensacaoPrejuizo = (byte)TipoCompensacaoPrejuizo.Padrao;
            this.TipoCalculoRetorno = (byte)CalculoRetornoCarteira.Cota;

            Estrategia estrategia = new Estrategia();
            if (!estrategia.BuscaPorDescricao(fundo.Estrategia))
            {
                estrategia.Save(fundo.Estrategia, false);
            }
            this.IdEstrategia = estrategia.IdEstrategia;

            this.CodigoAnbid = fundo.CodigoFundo;

            #region Busca IdPessoa
            PessoaCollection pessoaCollection = new PessoaCollection();
            pessoaCollection.Query.es.Top = 1;
            pessoaCollection.Query.Select(pessoaCollection.Query.IdPessoa);
            pessoaCollection.Query.Where(pessoaCollection.Query.Cpfcnpj.Equal(fundo.CNPJ));
            #endregion

            pessoa = new Pessoa();
            if (pessoaCollection.Count > 0 && pessoaCollection[0].IdPessoa != null)
            {
                pessoa.Load(pessoaCollection.Query);
            }
            else
            {
                #region Criar pessoa automaticamente
                pessoa.IdPessoa = idPessoa;
                pessoa.Nome = fundo.NomeFantasia;
                pessoa.Apelido = fundo.NomeFantasia;
                pessoa.Tipo = (byte)Financial.CRM.Enums.TipoPessoa.Juridica;
                pessoa.DataCadatro = DateTime.Now;
                pessoa.DataUltimaAlteracao = DateTime.Now;
                pessoa.Cpfcnpj = fundo.CNPJ;
                #endregion
            }
                   
            #region Criar cliente
            cliente = new Cliente();
            cliente.IdCliente = idCarteira.Value;
            cliente.Nome = pessoa.Nome;
            cliente.Apelido = pessoa.Apelido;
            cliente.IdTipo = TipoClienteFixo.Fundo;
            cliente.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
            cliente.Status = (byte)StatusCliente.Aberto;
            cliente.IdPessoa = idPessoa;

            DateTime dataImplantacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            if (!Util.Calendario.IsDiaUtil(dataImplantacao))
            {
                dataImplantacao = Calendario.SubtraiDiaUtil(dataImplantacao, 1);
            }
            cliente.DataDia = cliente.DataInicio = cliente.DataImplantacao = dataImplantacao;

            cliente.ApuraGanhoRV = "N";
            cliente.TipoControle = (byte)TipoControleCliente.ApenasCotacao;
            cliente.IsentoIR = "S";
            cliente.IsentoIOF = "S";
            cliente.IdMoeda = (int)Financial.Common.Enums.ListaMoedaFixo.Real;
            cliente.ZeraCaixa = "N";
            cliente.DescontaTributoPL = (byte)DescontoPLCliente.BrutoSemImpacto;
            cliente.GrossUP = (byte)GrossupCliente.NaoFaz;

            GrupoProcessamentoCollection grupoProcessamentoCollection = new GrupoProcessamentoCollection();
            grupoProcessamentoCollection.Query.Where(grupoProcessamentoCollection.Query.Descricao.Like("%fundo%"));
            grupoProcessamentoCollection.Query.Load();

            if (grupoProcessamentoCollection.Count == 0)
            {
                grupoProcessamentoCollection.QueryReset();
                grupoProcessamentoCollection.LoadAll();
            }

            cliente.IdGrupoProcessamento = grupoProcessamentoCollection[0].IdGrupoProcessamento.Value;
            cliente.IsProcessando = "N";
            cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
            cliente.CalculaRealTime = "N";
            cliente.CalculaGerencial = "N";
            if (cliente.IdTipo.Value == TipoClienteFixo.Fundo)
                cliente.IdLocalNegociacao = (int)LocalNegociacaoFixo.Brasil;
            #endregion

            using (esTransactionScope scope = new esTransactionScope())
            {

                pessoa.Save();
                cliente.Save();
                this.Save();

                scope.Complete();
            }
            #region Cadastro de Taxa de Administração
            if (fundo.TaxaAdministracao.HasValue && fundo.TaxaAdministracao.Value != 0)
            {
                TabelaTaxaAdministracao taxaAdm = new TabelaTaxaAdministracao();
                taxaAdm.IdCarteira = this.IdCarteira;
                taxaAdm.DataReferencia = this.DataInicioCota;
                taxaAdm.TipoCalculo = (byte)TipoCalculoAdministracao.PercentualPL;
                taxaAdm.TipoApropriacao = (byte)TipoApropriacaoAdministracao.Linear;
                taxaAdm.Taxa = fundo.TaxaAdministracao.Value;
                taxaAdm.BaseApuracao = (byte)BaseApuracaoAdministracao.PL_DiaAnterior;
                taxaAdm.BaseAno = (short)BaseAnoAdministracao.Base252;
                taxaAdm.TipoLimitePL = (byte)TipoLimitePLAdministracao.TotalPL;
                taxaAdm.ValorLimite = 0;
                taxaAdm.ContagemDias = (byte)Financial.Util.Enums.ContagemDias.Uteis;
                taxaAdm.ValorMinimo = 0;
                taxaAdm.ValorMaximo = 0;
                taxaAdm.NumeroMesesRenovacao = 1;
                taxaAdm.NumeroDiasPagamento = 0;
                taxaAdm.ImpactaPL = "S";
                taxaAdm.IdCadastro = 1;
                taxaAdm.ValorFixoTotal = null;
                taxaAdm.DataFim = null;
                taxaAdm.IdEventoProvisao = null;
                taxaAdm.IdEventoPagamento = null;
                taxaAdm.Save();
            }
            #endregion

            this.ImportaCotasSIAnbid();
        }

        public void ImportaCotasYMF()
        {
            //fundos.BuscaCarteirasFundosAnbima();

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(this.IdCarteira.Value);

            ClienteInterface clienteInterface = new ClienteInterface();
            clienteInterface.LoadByPrimaryKey(cliente.IdCliente.Value);

            DateTime? dataMaxCota = new HistoricoCota().BuscaDataMaxCota(this.IdCarteira.Value);
            if (!dataMaxCota.HasValue)
            {
                dataMaxCota = new DateTime(1900, 1, 1);
            }
            DataTable posicaoFundoDataTable = this.CarregaTabelaPosicaoFundoYMF(clienteInterface.CodigoYMF, dataMaxCota.Value);
            if (posicaoFundoDataTable.Rows.Count == 0)
            {
                //throw new Exception("Nenhuma cota retornada para a carteira: " + this.IdCarteira.Value);
                return;
            }

            foreach (DataRow dataRow in posicaoFundoDataTable.Rows)
            {

                HistoricoCota historicoCota = new HistoricoCota();
                DateTime dataCota = (DateTime)dataRow["DT_POSICAO"];

                if (!historicoCota.LoadByPrimaryKey(dataCota, cliente.IdCliente.Value))
                {
                    historicoCota = new HistoricoCota();
                }

                PosicaoFundoYMF posicaoFundoYMF = new PosicaoFundoYMF();
                posicaoFundoYMF.CdFundo = (string)dataRow["CD_FUNDO"];
                posicaoFundoYMF.DtPosicao = dataCota;
                posicaoFundoYMF.VlPatrimonioAbertura = Convert.ToDecimal(dataRow["VL_PATRIMONIO_BRUTO"]);
                posicaoFundoYMF.VlCotaAbertura = Convert.ToDecimal(dataRow["VL_COTA_LIQUIDA"]);
                posicaoFundoYMF.VlCotaPrePfee = dataRow["VL_COTA_LIQUIDA"] == DBNull.Value ?
                    posicaoFundoYMF.VlCotaPrePfee :
                    Convert.ToDecimal(dataRow["VL_COTA_LIQUIDA"]);
                posicaoFundoYMF.VlPatrimonioFechamentoTotal = Convert.ToDecimal(dataRow["VL_PATRIMONIO_BRUTO"]);
                posicaoFundoYMF.QtCotasFechamentoTotal = null;
                posicaoFundoYMF.VlCotaFechamento = Convert.ToDecimal(dataRow["VL_COTA_LIQUIDA"]);

                historicoCota.ImportaPosicaoFundoYMF(posicaoFundoYMF);

                historicoCota.Save();
            }


        }

        public SqlConnection CreateSqlConnectionYMFSAC()
        {
            return this.CreateSqlConnectionYMF("YMF_SAC");
        }

        public SqlConnection CreateSqlConnectionYMFCOT()
        {
            return this.CreateSqlConnectionYMF("YMF_COT");
        }

        private SqlConnection CreateSqlConnectionYMF(string initialCatalog)
        {
            string connectionString = ConfigurationManager.ConnectionStrings[initialCatalog + "_ConnectionString"].ConnectionString;
            //string connectionString = "Data Source=125.50.0.212;Initial Catalog=" + initialCatalog + ";User ID=financial;Password=paulista2012;";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            return conn;
        }

        public string RetornaCodigoAtivoSAC(string codFundoCOT)
        {
            SqlDataAdapter adapterMapping = new SqlDataAdapter();
            DataTable dataTableMapping = new DataTable();
            using (SqlConnection connCOT = this.CreateSqlConnectionYMFCOT())
            {
                //Precisamos primeiro fazer o DE-PARA entre codigo COT e SAC
                dataTableMapping.TableName = "COT_FUNDO_PARAMETRO";
                using (SqlCommand cmdMapping = new SqlCommand())
                {
                    cmdMapping.CommandText = String.Format(
                        "select cd_fundo, cd_ativo from cot_fundo_parametro where " +
                        "dt_inicio_validade in " +
                            "(select max(dt_inicio_validade) from cot_fundo_parametro where cd_fundo = '{0}')" +
                        "and cd_fundo = '{0}'",
                        codFundoCOT);

                    cmdMapping.CommandType = System.Data.CommandType.Text;
                    cmdMapping.Connection = connCOT;

                    adapterMapping.SelectCommand = cmdMapping;
                    adapterMapping.Fill(dataTableMapping);
                }
            }
            return (string)dataTableMapping.Rows[0]["CD_ATIVO"];

        }

        private DataTable CarregaTabelaPosicaoFundoYMF(string codFundoCOT, DateTime dataUltimaCota)
        {


            string codFundoSAC = RetornaCodigoAtivoSAC(codFundoCOT);

            DataTable dataTable = new DataTable();

            using (SqlConnection connSAC = this.CreateSqlConnectionYMFSAC())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    dataTable.TableName = "SAC_POSICAO_FUNDO";
                    string sacTableName = "SAC_CL_INTEGRACAO_TX1";

                    cmd.CommandText = String.Format(
                        "select *, '{3}' CD_FUNDO from {2} where CLCLI_CD='{0}' and DT_POSICAO > '{1}'",
                        codFundoSAC, dataUltimaCota.ToString("yyyy-MM-dd"), sacTableName, codFundoCOT);

                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = connSAC;

                    adapter.SelectCommand = cmd;
                    adapter.Fill(dataTable);
                }
            }
            return dataTable;
        }

        public List<int> GetIdsCotistas()
        {
            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.Query.Select(posicaoCotistaCollection.Query.IdCotista);
            posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(this.IdCarteira));
            posicaoCotistaCollection.Query.es.Distinct = true;
            posicaoCotistaCollection.Load(posicaoCotistaCollection.Query);

            List<int> idsCotistas = new List<int>();
            foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
            {
                idsCotistas.Add(posicaoCotista.IdCotista.Value);
            }
            return idsCotistas;
        }

        /// <summary>
        /// Retorna o fluxo total na data, considerando lancamentos em Liquidacao, bem como vencimentos relativos a posições de ativos e resgates a liquidar.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLiquidacao"></param>
        /// <returns></returns>
        public decimal RetornaFluxoLiquidarData(Cliente cliente, DateTime dataReferencia, DateTime dataLiquidacao, BooleanosFluxoCaixa booleanosFluxoCaixa)
        {
            return RetornaFluxoLiquidarData(cliente, dataReferencia, dataLiquidacao, "", booleanosFluxoCaixa);
        }

        /// <summary>
        /// Retorna o fluxo total na data, considerando lancamentos em Liquidacao, bem como vencimentos relativos a posições de ativos e resgates a liquidar.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLiquidacao"></param>
        /// <returns></returns>
        public decimal RetornaFluxoLiquidarData(Cliente cliente, DateTime dataReferencia, DateTime dataLiquidacao, string descricao, BooleanosFluxoCaixa booleanosFluxoCaixa)
        {
            int idCliente = cliente.IdCliente.Value;
            DateTime dataDia = cliente.DataDia.Value;

            bool historico = dataReferencia < dataDia;

            decimal valorLiquidacao = 0;

            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
            liquidacao.Query.Where(liquidacao.Query.IdCliente == idCliente,
                                   liquidacao.Query.Situacao.Equal((byte)SituacaoLancamentoLiquidacao.Normal),
                                   liquidacao.Query.DataVencimento.Equal(dataLiquidacao));

            if (!String.IsNullOrEmpty(descricao))
            {
                liquidacao.Query.Where(liquidacao.Query.Descricao.Equal(descricao));
            }

            liquidacao.Query.Load();

            if (liquidacao.Valor.HasValue)
            {
                valorLiquidacao = liquidacao.Valor.HasValue ? liquidacao.Valor.Value : 0;
            }

            if (dataLiquidacao > dataDia)
            {
                if (booleanosFluxoCaixa.RendaFixa && (String.IsNullOrEmpty(descricao) || descricao == "Vencimento em títulos"))
                {
                    #region Renda Fixa
                    TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                    PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                    PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");

                    if (historico)
                    {
                        #region Posicao Renda Fixa
                        posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.Quantidade,
                                                             posicaoRendaFixaHistoricoQuery.ValorMercado,
                                                             tituloRendaFixaQuery.PUNominal,
                                                             tituloRendaFixaQuery.IdIndice);
                        posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                                            posicaoRendaFixaHistoricoQuery.DataVencimento.Equal(dataLiquidacao),
                                                            posicaoRendaFixaHistoricoQuery.Quantidade.NotEqual(0),
                                                            posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                                            posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataReferencia));

                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaHistoricoCollection)
                        {
                            decimal valor = 0;
                            if (Convert.IsDBNull(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice)))
                            {
                                valor = posicaoRendaFixa.Quantidade.Value * Convert.ToDecimal(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.PUNominal)); //Pre-Fixcado
                                valorLiquidacao += valor;
                            }
                            else
                            {
                                valor = posicaoRendaFixa.ValorMercado.Value; //Pos-Fixado
                                valorLiquidacao += valor;
                            }
                        }

                        PosicaoRendaFixaHistorico posicaoRendaFixaHistoricoCompromisso = new PosicaoRendaFixaHistorico();
                        posicaoRendaFixaHistoricoCompromisso.Query.Select(posicaoRendaFixaHistoricoCompromisso.Query.ValorVolta.Sum());
                        posicaoRendaFixaHistoricoCompromisso.Query.Where(posicaoRendaFixaHistoricoCompromisso.Query.IdCliente.Equal(idCliente),
                                                                      posicaoRendaFixaHistoricoCompromisso.Query.DataVolta.Equal(dataLiquidacao),
                                                                      posicaoRendaFixaHistoricoCompromisso.Query.Quantidade.NotEqual(0),
                                                                      posicaoRendaFixaHistoricoCompromisso.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                                                      posicaoRendaFixaHistoricoCompromisso.Query.DataHistorico.Equal(dataReferencia));
                        posicaoRendaFixaHistoricoCompromisso.Query.Load();

                        if (posicaoRendaFixaHistoricoCompromisso.ValorVolta.HasValue)
                        {
                            valorLiquidacao += posicaoRendaFixaHistoricoCompromisso.ValorVolta.Value;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Posicao Renda Fixa
                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.Quantidade,
                                                     posicaoRendaFixaQuery.ValorMercado,
                                                     tituloRendaFixaQuery.PUNominal,
                                                     tituloRendaFixaQuery.IdIndice);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaQuery.DataVencimento.Equal(dataLiquidacao),
                                                    posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                                    posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));

                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal valor = 0;
                            if (Convert.IsDBNull(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice)))
                            {
                                valor = posicaoRendaFixa.Quantidade.Value * Convert.ToDecimal(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.PUNominal)); //Pre-Fixcado
                                valorLiquidacao += valor;
                            }
                            else
                            {
                                valor = posicaoRendaFixa.ValorMercado.Value; //Pos-Fixado
                                valorLiquidacao += valor;
                            }
                        }

                        PosicaoRendaFixa posicaoRendaFixaCompromisso = new PosicaoRendaFixa();
                        posicaoRendaFixaCompromisso.Query.Select(posicaoRendaFixaCompromisso.Query.ValorVolta.Sum());
                        posicaoRendaFixaCompromisso.Query.Where(posicaoRendaFixaCompromisso.Query.IdCliente.Equal(idCliente),
                                                          posicaoRendaFixaCompromisso.Query.DataVolta.Equal(dataLiquidacao),
                                                          posicaoRendaFixaCompromisso.Query.Quantidade.NotEqual(0),
                                                          posicaoRendaFixaCompromisso.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda));
                        posicaoRendaFixaCompromisso.Query.Load();

                        if (posicaoRendaFixaCompromisso.ValorVolta.HasValue)
                        {
                            valorLiquidacao += posicaoRendaFixaCompromisso.ValorVolta.Value;
                        }
                        #endregion
                    }

                    #region AgendaRendaFixa
                    AgendaRendaFixaQuery agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
                    agendaRendaFixaQuery.Select(agendaRendaFixaQuery.Valor,
                                                posicaoRendaFixaQuery.Quantidade);
                    agendaRendaFixaQuery.InnerJoin(posicaoRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == agendaRendaFixaQuery.IdTitulo);
                    agendaRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                               agendaRendaFixaQuery.Valor.IsNotNull(),
                                               agendaRendaFixaQuery.DataPagamento.Equal(dataLiquidacao));
                    AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
                    agendaRendaFixaCollection.Load(agendaRendaFixaQuery);

                    foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
                    {
                        valorLiquidacao += Math.Round(agendaRendaFixa.Valor.Value * Convert.ToDecimal(agendaRendaFixa.GetColumn(PosicaoRendaFixaMetadata.ColumnNames.Quantidade)), 2);
                    }
                    #endregion
                    #endregion
                }

                if (booleanosFluxoCaixa.PosicaoBolsa)
                {
                    #region Ações e opções de bolsa
                    DateTime dataD3 = Calendario.AdicionaDiaUtil(dataReferencia, 3, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    DateTime dataD1 = Calendario.AdicionaDiaUtil(dataReferencia, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    if (historico)
                    {
                        if (dataLiquidacao == dataD3)
                        {
                            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                            posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
                            posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                                             posicaoBolsaHistorico.Query.Quantidade.NotEqual(0),
                                                             posicaoBolsaHistorico.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario),
                                                             posicaoBolsaHistorico.Query.DataHistorico.Equal(dataReferencia));
                            posicaoBolsaHistorico.Query.Load();

                            if (posicaoBolsaHistorico.ValorMercado.HasValue && (String.IsNullOrEmpty(descricao) || descricao == "Liquidez em ações"))
                            {
                                valorLiquidacao += posicaoBolsaHistorico.ValorMercado.Value;
                            }
                        }
                        else
                        {
                            if (dataLiquidacao == dataD1)
                            {
                                PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                                posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
                                posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                                         posicaoBolsaHistorico.Query.Quantidade.NotEqual(0),
                                                         posicaoBolsaHistorico.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                         posicaoBolsaHistorico.Query.DataHistorico.Equal(dataReferencia));
                                posicaoBolsaHistorico.Query.Load();

                                if (posicaoBolsaHistorico.ValorMercado.HasValue && (String.IsNullOrEmpty(descricao) || descricao == "Liquidez em opções de ações"))
                                {
                                    valorLiquidacao += posicaoBolsaHistorico.ValorMercado.Value;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (dataLiquidacao == dataD3)
                        {
                            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                            posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                            posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                     posicaoBolsa.Query.Quantidade.NotEqual(0),
                                                     posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
                            posicaoBolsa.Query.Load();

                            if (posicaoBolsa.ValorMercado.HasValue && (String.IsNullOrEmpty(descricao) || descricao == "Liquidez em ações"))
                            {
                                valorLiquidacao += posicaoBolsa.ValorMercado.Value;
                            }
                        }
                        else
                        {
                            if (dataLiquidacao == dataD1)
                            {
                                PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                         posicaoBolsa.Query.Quantidade.NotEqual(0),
                                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
                                posicaoBolsa.Query.Load();

                                if (posicaoBolsa.ValorMercado.HasValue && (String.IsNullOrEmpty(descricao) || descricao == "Liquidez em opções de ações"))
                                {
                                    valorLiquidacao += posicaoBolsa.ValorMercado.Value;
                                }
                            }
                        }
                    }
                    #endregion
                }

                if (booleanosFluxoCaixa.PosicaoFundos && (String.IsNullOrEmpty(descricao) || descricao == "Liquidez em fundos"))
                {
                    #region Posições em fundos
                    if (historico)
                    {
                        PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
                        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

                        posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.ValorBruto.Sum(),
                                                         carteiraQuery.DiasLiquidacaoResgate,
                                                         carteiraQuery.DiasCotizacaoResgate,
                                                         carteiraQuery.ContagemDiasConversaoResgate);
                        posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
                        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente),
                                                        posicaoFundoHistoricoQuery.Quantidade.NotEqual(0),
                                                        posicaoFundoHistoricoQuery.DataHistorico.Equal(dataReferencia));
                        posicaoFundoHistoricoQuery.GroupBy(carteiraQuery.DiasLiquidacaoResgate,
                                                          carteiraQuery.DiasCotizacaoResgate,
                                                          carteiraQuery.ContagemDiasConversaoResgate);

                        PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                        posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

                        foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                        {
                            int diasLiquidacaoResgate = Convert.ToInt32(posicaoFundoHistorico.GetColumn(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate));
                            int diasCotizacaoResgate = Convert.ToInt32(posicaoFundoHistorico.GetColumn(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate));
                            byte contagemDiasConversaoResgate = Convert.ToByte(posicaoFundoHistorico.GetColumn(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate));

                            DateTime dataLiquidacaoFundo = new DateTime();
                            if (contagemDiasConversaoResgate == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                            {
                                dataLiquidacaoFundo = Calendario.AdicionaDiaUtil(dataReferencia, diasLiquidacaoResgate);
                            }
                            else
                            {
                                dataLiquidacaoFundo = dataReferencia.AddDays(diasCotizacaoResgate);
                                dataLiquidacaoFundo = Calendario.AdicionaDiaUtil(dataLiquidacaoFundo, diasLiquidacaoResgate);
                            }

                            decimal valorBruto = posicaoFundoHistorico.ValorBruto.Value;

                            if (dataLiquidacaoFundo == dataLiquidacao)
                            {
                                valorLiquidacao += valorBruto;
                            }
                        }
                    }
                    else
                    {
                        PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
                        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

                        posicaoFundoQuery.Select(posicaoFundoQuery.ValorBruto.Sum(),
                                                 carteiraQuery.DiasLiquidacaoResgate,
                                                 carteiraQuery.DiasCotizacaoResgate,
                                                 carteiraQuery.ContagemDiasConversaoResgate);
                        posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
                        posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente),
                                                posicaoFundoQuery.Quantidade.NotEqual(0));
                        posicaoFundoQuery.GroupBy(carteiraQuery.DiasLiquidacaoResgate,
                                                  carteiraQuery.DiasCotizacaoResgate,
                                                  carteiraQuery.ContagemDiasConversaoResgate);

                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        posicaoFundoCollection.Load(posicaoFundoQuery);

                        foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                        {
                            int diasLiquidacaoResgate = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate));
                            int diasCotizacaoResgate = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate));
                            byte contagemDiasConversaoResgate = Convert.ToByte(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate));

                            DateTime dataLiquidacaoFundo = new DateTime();
                            if (contagemDiasConversaoResgate == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                            {
                                dataLiquidacaoFundo = Calendario.AdicionaDiaUtil(dataReferencia, diasLiquidacaoResgate);
                            }
                            else
                            {
                                dataLiquidacaoFundo = dataReferencia.AddDays(diasCotizacaoResgate);
                                dataLiquidacaoFundo = Calendario.AdicionaDiaUtil(dataLiquidacaoFundo, diasLiquidacaoResgate);
                            }

                            decimal valorBruto = posicaoFundo.ValorBruto.Value;

                            if (dataLiquidacaoFundo == dataLiquidacao)
                            {
                                valorLiquidacao += valorBruto;
                            }
                        }
                    }
                    #endregion
                }

                if (booleanosFluxoCaixa.ResgatesNaoConvertidos && !booleanosFluxoCaixa.PosicaoFundos && (String.IsNullOrEmpty(descricao) || descricao == "Resgates não cotizados"))
                {
                    #region Resgates Não Convertidos
                    OperacaoFundo operacaoFundo = new OperacaoFundo();
                    operacaoFundo.Query.Select(operacaoFundo.Query.ValorBruto.Sum());
                    operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCliente),
                                              operacaoFundo.Query.ValorBruto.IsNotNull(),
                                              operacaoFundo.Query.DataConversao.GreaterThan(dataReferencia),
                                              operacaoFundo.Query.DataLiquidacao.Equal(dataLiquidacao),
                                              operacaoFundo.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ResgateBruto));
                    operacaoFundo.Query.Load();

                    if (operacaoFundo.ValorBruto.HasValue)
                    {
                        valorLiquidacao += operacaoFundo.ValorBruto.Value;
                    }

                    operacaoFundo = new OperacaoFundo();
                    operacaoFundo.Query.Select(operacaoFundo.Query.ValorLiquido.Sum());
                    operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCliente),
                                              operacaoFundo.Query.ValorLiquido.IsNotNull(),
                                              operacaoFundo.Query.DataConversao.GreaterThan(dataReferencia),
                                              operacaoFundo.Query.DataLiquidacao.Equal(dataLiquidacao),
                                              operacaoFundo.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ResgateLiquido));
                    operacaoFundo.Query.Load();

                    if (operacaoFundo.ValorLiquido.HasValue)
                    {
                        valorLiquidacao += operacaoFundo.ValorLiquido.Value;
                    }

                    OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
                    operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.IdCarteira);
                    operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                        operacaoFundoCollection.Query.DataConversao.GreaterThan(dataReferencia),
                                                        operacaoFundoCollection.Query.DataLiquidacao.Equal(dataLiquidacao),
                                                        operacaoFundoCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ResgateTotal));
                    operacaoFundoCollection.Query.es.Distinct = true;
                    operacaoFundoCollection.Query.Load();

                    foreach (OperacaoFundo operacaoFundoId in operacaoFundoCollection)
                    {
                        int idCarteira = operacaoFundoId.IdCarteira.Value;
                        PosicaoFundo posicaoFundo = new PosicaoFundo();
                        decimal quantidadeCotas = posicaoFundo.RetornaQuantidadeAtivo(idCliente, idCarteira);

                        HistoricoCota historicoCota = new HistoricoCota();
                        historicoCota.BuscaValorCota(idCarteira, dataReferencia);

                        decimal valorPosicao = 0;
                        if (historicoCota.CotaFechamento.HasValue)
                        {
                            valorPosicao = Math.Round(quantidadeCotas * historicoCota.CotaFechamento.Value, 2);
                        }

                        valorLiquidacao += valorPosicao;
                    }

                    OperacaoCotista operacaoCotista = new OperacaoCotista();
                    operacaoCotista.Query.Select(operacaoCotista.Query.ValorBruto.Sum());
                    operacaoCotista.Query.Where(operacaoCotista.Query.IdCarteira.Equal(idCliente),
                                              operacaoCotista.Query.ValorBruto.IsNotNull(),
                                              operacaoCotista.Query.DataConversao.GreaterThan(dataReferencia),
                                              operacaoCotista.Query.DataLiquidacao.Equal(dataLiquidacao),
                                              operacaoCotista.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ResgateBruto));
                    operacaoCotista.Query.Load();

                    if (operacaoCotista.ValorBruto.HasValue)
                    {
                        valorLiquidacao -= operacaoCotista.ValorBruto.Value;
                    }

                    operacaoCotista = new OperacaoCotista();
                    operacaoCotista.Query.Select(operacaoCotista.Query.ValorLiquido.Sum());
                    operacaoCotista.Query.Where(operacaoCotista.Query.IdCarteira.Equal(idCliente),
                                              operacaoCotista.Query.ValorLiquido.IsNotNull(),
                                              operacaoCotista.Query.DataConversao.GreaterThan(dataReferencia),
                                              operacaoCotista.Query.DataLiquidacao.Equal(dataLiquidacao),
                                              operacaoCotista.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ResgateLiquido));
                    operacaoCotista.Query.Load();

                    if (operacaoCotista.ValorLiquido.HasValue)
                    {
                        valorLiquidacao -= operacaoCotista.ValorLiquido.Value;
                    }

                    OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                    operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdCotista);
                    operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCliente),
                                                          operacaoCotistaCollection.Query.DataConversao.GreaterThan(dataReferencia),
                                                          operacaoCotistaCollection.Query.DataLiquidacao.Equal(dataLiquidacao),
                                                          operacaoCotistaCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ResgateTotal));
                    operacaoCotistaCollection.Query.es.Distinct = true;
                    operacaoCotistaCollection.Query.Load();

                    foreach (OperacaoCotista operacaoCotistaId in operacaoCotistaCollection)
                    {
                        int idCotista = operacaoCotistaId.IdCotista.Value;
                        PosicaoCotista posicaoCotista = new PosicaoCotista();
                        decimal quantidadeCotas = posicaoCotista.RetornaTotalCotas(idCliente, idCotista);

                        HistoricoCota historicoCota = new HistoricoCota();
                        historicoCota.BuscaValorCota(idCliente, dataReferencia);

                        decimal valorPosicao = 0;
                        if (historicoCota.CotaFechamento.HasValue)
                        {
                            valorPosicao = Math.Round(quantidadeCotas * historicoCota.CotaFechamento.Value, 2);
                        }

                        valorLiquidacao -= valorPosicao;
                    }
                    #endregion
                }
            }

            return valorLiquidacao;
        }

        /// <summary>
        /// Retorna o fluxo total na data, considerando lancamentos em Liquidacao, bem como vencimentos relativos a posições de ativos e resgates a liquidar.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLiquidacao"></param>
        /// <returns></returns>
        public decimal RetornaFluxoLiquidarAposData(Cliente cliente, DateTime dataReferencia, DateTime dataLiquidacao, BooleanosFluxoCaixa booleanosFluxoCaixa)
        {
            return RetornaFluxoLiquidarAposData(cliente, dataReferencia, dataLiquidacao, "", booleanosFluxoCaixa);
        }

        /// <summary>
        /// Retorna o fluxo total na data, considerando lancamentos em Liquidacao, bem como vencimentos relativos a posições de ativos e resgates a liquidar.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLiquidacao"></param>
        /// <returns></returns>
        public decimal RetornaFluxoLiquidarAposData(Cliente cliente, DateTime dataReferencia, DateTime dataLiquidacao, string descricao, BooleanosFluxoCaixa booleanosFluxoCaixa)
        {
            int idCliente = cliente.IdCliente.Value;
            DateTime dataDia = cliente.DataDia.Value;

            bool historico = dataReferencia < dataDia;

            decimal valorLiquidacao = 0;

            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
            liquidacao.Query.Where(liquidacao.Query.IdCliente == idCliente,
                                   liquidacao.Query.Situacao.Equal((byte)SituacaoLancamentoLiquidacao.Normal),
                                   liquidacao.Query.DataVencimento.GreaterThanOrEqual(dataLiquidacao));

            if (!String.IsNullOrEmpty(descricao))
            {
                liquidacao.Query.Where(liquidacao.Query.Descricao.Equal(descricao));
            }

            liquidacao.Query.Load();

            if (liquidacao.Valor.HasValue)
            {
                valorLiquidacao = liquidacao.Valor.HasValue ? liquidacao.Valor.Value : 0;
            }

            if (dataLiquidacao > dataDia)
            {
                if (booleanosFluxoCaixa.RendaFixa && (String.IsNullOrEmpty(descricao) || descricao == "Vencimento em títulos"))
                {
                    #region Renda Fixa
                    TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                    PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                    PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");

                    if (historico)
                    {
                        #region Posicao Renda Fixa
                        posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.Quantidade,
                                                             posicaoRendaFixaHistoricoQuery.ValorMercado,
                                                             tituloRendaFixaQuery.PUNominal,
                                                             tituloRendaFixaQuery.IdIndice);
                        posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                                            posicaoRendaFixaHistoricoQuery.DataVencimento.GreaterThanOrEqual(dataLiquidacao),
                                                            posicaoRendaFixaHistoricoQuery.Quantidade.NotEqual(0),
                                                            posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                                            posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataReferencia));

                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaHistoricoCollection)
                        {
                            decimal valor = 0;
                            if (Convert.IsDBNull(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice)))
                            {
                                valor = posicaoRendaFixa.Quantidade.Value * Convert.ToDecimal(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.PUNominal)); //Pre-Fixcado
                                valorLiquidacao += valor;
                            }
                            else
                            {
                                valor = posicaoRendaFixa.ValorMercado.Value; //Pos-Fixado
                                valorLiquidacao += valor;
                            }
                        }

                        PosicaoRendaFixaHistorico posicaoRendaFixaHistoricoCompromisso = new PosicaoRendaFixaHistorico();
                        posicaoRendaFixaHistoricoCompromisso.Query.Select(posicaoRendaFixaHistoricoCompromisso.Query.ValorVolta.Sum());
                        posicaoRendaFixaHistoricoCompromisso.Query.Where(posicaoRendaFixaHistoricoCompromisso.Query.IdCliente.Equal(idCliente),
                                                                      posicaoRendaFixaHistoricoCompromisso.Query.DataVolta.GreaterThanOrEqual(dataLiquidacao),
                                                                      posicaoRendaFixaHistoricoCompromisso.Query.Quantidade.NotEqual(0),
                                                                      posicaoRendaFixaHistoricoCompromisso.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda),
                                                                      posicaoRendaFixaHistoricoCompromisso.Query.DataHistorico.Equal(dataReferencia));
                        posicaoRendaFixaHistoricoCompromisso.Query.Load();

                        if (posicaoRendaFixaHistoricoCompromisso.ValorVolta.HasValue)
                        {
                            valorLiquidacao += posicaoRendaFixaHistoricoCompromisso.ValorVolta.Value;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Posicao Renda Fixa
                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.Quantidade,
                                                     posicaoRendaFixaQuery.ValorMercado,
                                                     tituloRendaFixaQuery.PUNominal,
                                                     tituloRendaFixaQuery.IdIndice);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaQuery.DataVencimento.GreaterThanOrEqual(dataLiquidacao),
                                                    posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                                    posicaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));

                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal valor = 0;
                            if (Convert.IsDBNull(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice)))
                            {
                                valor = posicaoRendaFixa.Quantidade.Value * Convert.ToDecimal(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.PUNominal)); //Pre-Fixcado
                                valorLiquidacao += valor;
                            }
                            else
                            {
                                valor = posicaoRendaFixa.ValorMercado.Value; //Pos-Fixado
                                valorLiquidacao += valor;
                            }
                        }

                        PosicaoRendaFixa posicaoRendaFixaCompromisso = new PosicaoRendaFixa();
                        posicaoRendaFixaCompromisso.Query.Select(posicaoRendaFixaCompromisso.Query.ValorVolta.Sum());
                        posicaoRendaFixaCompromisso.Query.Where(posicaoRendaFixaCompromisso.Query.IdCliente.Equal(idCliente),
                                                          posicaoRendaFixaCompromisso.Query.DataVolta.GreaterThanOrEqual(dataLiquidacao),
                                                          posicaoRendaFixaCompromisso.Query.Quantidade.NotEqual(0),
                                                          posicaoRendaFixaCompromisso.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda));
                        posicaoRendaFixaCompromisso.Query.Load();

                        if (posicaoRendaFixaCompromisso.ValorVolta.HasValue)
                        {
                            valorLiquidacao += posicaoRendaFixaCompromisso.ValorVolta.Value;
                        }
                        #endregion
                    }

                    #region AgendaRendaFixa
                    AgendaRendaFixaQuery agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
                    agendaRendaFixaQuery.Select(agendaRendaFixaQuery.Valor,
                                                posicaoRendaFixaQuery.Quantidade);
                    agendaRendaFixaQuery.InnerJoin(posicaoRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == agendaRendaFixaQuery.IdTitulo);
                    agendaRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                               agendaRendaFixaQuery.Valor.IsNotNull(),
                                               agendaRendaFixaQuery.DataPagamento.GreaterThanOrEqual(dataLiquidacao));
                    AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
                    agendaRendaFixaCollection.Load(agendaRendaFixaQuery);

                    foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
                    {
                        valorLiquidacao += Math.Round(agendaRendaFixa.Valor.Value * Convert.ToDecimal(agendaRendaFixa.GetColumn(PosicaoRendaFixaMetadata.ColumnNames.Quantidade)), 2);
                    }
                    #endregion
                    #endregion
                }

                if (booleanosFluxoCaixa.PosicaoFundos && (String.IsNullOrEmpty(descricao) || descricao == "Liquidez em fundos"))
                {
                    #region Posições em fundos
                    if (historico)
                    {
                        PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
                        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

                        posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.ValorBruto.Sum(),
                                                         carteiraQuery.DiasLiquidacaoResgate,
                                                         carteiraQuery.DiasCotizacaoResgate,
                                                         carteiraQuery.ContagemDiasConversaoResgate);
                        posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
                        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente),
                                                        posicaoFundoHistoricoQuery.Quantidade.NotEqual(0),
                                                        posicaoFundoHistoricoQuery.DataHistorico.Equal(dataReferencia));
                        posicaoFundoHistoricoQuery.GroupBy(carteiraQuery.DiasLiquidacaoResgate,
                                                          carteiraQuery.DiasCotizacaoResgate,
                                                          carteiraQuery.ContagemDiasConversaoResgate);

                        PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                        posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

                        foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                        {
                            int diasLiquidacaoResgate = Convert.ToInt32(posicaoFundoHistorico.GetColumn(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate));
                            int diasCotizacaoResgate = Convert.ToInt32(posicaoFundoHistorico.GetColumn(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate));
                            byte contagemDiasConversaoResgate = Convert.ToByte(posicaoFundoHistorico.GetColumn(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate));

                            DateTime dataLiquidacaoFundo = new DateTime();
                            if (contagemDiasConversaoResgate == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                            {
                                dataLiquidacaoFundo = Calendario.AdicionaDiaUtil(dataReferencia, diasLiquidacaoResgate);
                            }
                            else
                            {
                                dataLiquidacaoFundo = dataReferencia.AddDays(diasCotizacaoResgate);
                                dataLiquidacaoFundo = Calendario.AdicionaDiaUtil(dataLiquidacaoFundo, diasLiquidacaoResgate);
                            }

                            decimal valorBruto = posicaoFundoHistorico.ValorBruto.Value;

                            if (dataLiquidacaoFundo >= dataLiquidacao)
                            {
                                valorLiquidacao += valorBruto;
                            }
                        }
                    }
                    else
                    {
                        PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
                        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

                        posicaoFundoQuery.Select(posicaoFundoQuery.ValorBruto.Sum(),
                                                 carteiraQuery.DiasLiquidacaoResgate,
                                                 carteiraQuery.DiasCotizacaoResgate,
                                                 carteiraQuery.ContagemDiasConversaoResgate);
                        posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
                        posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente),
                                                posicaoFundoQuery.Quantidade.NotEqual(0));
                        posicaoFundoQuery.GroupBy(carteiraQuery.DiasLiquidacaoResgate,
                                                  carteiraQuery.DiasCotizacaoResgate,
                                                  carteiraQuery.ContagemDiasConversaoResgate);

                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        posicaoFundoCollection.Load(posicaoFundoQuery);

                        foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                        {
                            int diasLiquidacaoResgate = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate));
                            int diasCotizacaoResgate = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate));
                            byte contagemDiasConversaoResgate = Convert.ToByte(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate));

                            DateTime dataLiquidacaoFundo = new DateTime();
                            if (contagemDiasConversaoResgate == (byte)ContagemDiasLiquidacaoResgate.DiasUteis)
                            {
                                dataLiquidacaoFundo = Calendario.AdicionaDiaUtil(dataReferencia, diasLiquidacaoResgate);
                            }
                            else
                            {
                                dataLiquidacaoFundo = dataReferencia.AddDays(diasCotizacaoResgate);
                                dataLiquidacaoFundo = Calendario.AdicionaDiaUtil(dataLiquidacaoFundo, diasLiquidacaoResgate);
                            }

                            decimal valorBruto = posicaoFundo.ValorBruto.Value;

                            if (dataLiquidacaoFundo >= dataLiquidacao)
                            {
                                valorLiquidacao += valorBruto;
                            }
                        }
                    }
                    #endregion
                }

                if (booleanosFluxoCaixa.ResgatesNaoConvertidos && !booleanosFluxoCaixa.PosicaoFundos && (String.IsNullOrEmpty(descricao) || descricao == "Resgates não cotizados"))
                {
                    #region Resgates Não Convertidos
                    OperacaoFundo operacaoFundo = new OperacaoFundo();
                    operacaoFundo.Query.Select(operacaoFundo.Query.ValorBruto.Sum());
                    operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCliente),
                                              operacaoFundo.Query.ValorBruto.IsNotNull(),
                                              operacaoFundo.Query.DataConversao.GreaterThan(dataReferencia),
                                              operacaoFundo.Query.DataLiquidacao.GreaterThanOrEqual(dataLiquidacao),
                                              operacaoFundo.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ResgateBruto));
                    operacaoFundo.Query.Load();

                    if (operacaoFundo.ValorBruto.HasValue)
                    {
                        valorLiquidacao += operacaoFundo.ValorBruto.Value;
                    }

                    operacaoFundo = new OperacaoFundo();
                    operacaoFundo.Query.Select(operacaoFundo.Query.ValorLiquido.Sum());
                    operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCliente),
                                              operacaoFundo.Query.ValorLiquido.IsNotNull(),
                                              operacaoFundo.Query.DataConversao.GreaterThan(dataReferencia),
                                              operacaoFundo.Query.DataLiquidacao.GreaterThanOrEqual(dataLiquidacao),
                                              operacaoFundo.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ResgateLiquido));
                    operacaoFundo.Query.Load();

                    if (operacaoFundo.ValorLiquido.HasValue)
                    {
                        valorLiquidacao += operacaoFundo.ValorLiquido.Value;
                    }

                    OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
                    operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.IdCarteira);
                    operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                        operacaoFundoCollection.Query.DataConversao.GreaterThan(dataReferencia),
                                                        operacaoFundoCollection.Query.DataLiquidacao.GreaterThanOrEqual(dataLiquidacao),
                                                        operacaoFundoCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ResgateTotal));
                    operacaoFundo.Query.es.Distinct = true;
                    operacaoFundo.Query.Load();

                    foreach (OperacaoFundo operacaoFundoId in operacaoFundoCollection)
                    {
                        int idCarteira = operacaoFundoId.IdCarteira.Value;
                        PosicaoFundo posicaoFundo = new PosicaoFundo();
                        decimal quantidadeCotas = posicaoFundo.RetornaQuantidadeAtivo(idCliente, idCarteira);

                        HistoricoCota historicoCota = new HistoricoCota();
                        historicoCota.BuscaValorCota(idCarteira, dataDia);

                        decimal valorPosicao = 0;
                        if (historicoCota.CotaFechamento.HasValue)
                        {
                            valorPosicao = Math.Round(quantidadeCotas * historicoCota.CotaFechamento.Value, 2);
                        }

                        valorLiquidacao += valorPosicao;
                    }

                    OperacaoCotista operacaoCotista = new OperacaoCotista();
                    operacaoCotista.Query.Select(operacaoCotista.Query.ValorBruto.Sum());
                    operacaoCotista.Query.Where(operacaoCotista.Query.IdCarteira.Equal(idCliente),
                                              operacaoCotista.Query.ValorBruto.IsNotNull(),
                                              operacaoCotista.Query.DataConversao.GreaterThan(dataReferencia),
                                              operacaoCotista.Query.DataLiquidacao.GreaterThanOrEqual(dataLiquidacao),
                                              operacaoCotista.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ResgateBruto));
                    operacaoCotista.Query.Load();

                    if (operacaoCotista.ValorBruto.HasValue)
                    {
                        valorLiquidacao -= operacaoCotista.ValorBruto.Value;
                    }

                    operacaoCotista = new OperacaoCotista();
                    operacaoCotista.Query.Select(operacaoCotista.Query.ValorLiquido.Sum());
                    operacaoCotista.Query.Where(operacaoCotista.Query.IdCarteira.Equal(idCliente),
                                              operacaoCotista.Query.ValorLiquido.IsNotNull(),
                                              operacaoCotista.Query.DataConversao.GreaterThan(dataReferencia),
                                              operacaoCotista.Query.DataLiquidacao.GreaterThanOrEqual(dataLiquidacao),
                                              operacaoCotista.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ResgateLiquido));
                    operacaoCotista.Query.Load();

                    if (operacaoCotista.ValorLiquido.HasValue)
                    {
                        valorLiquidacao -= operacaoCotista.ValorLiquido.Value;
                    }

                    OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                    operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdCotista);
                    operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCliente),
                                                          operacaoCotistaCollection.Query.DataConversao.GreaterThan(dataReferencia),
                                                          operacaoCotistaCollection.Query.DataLiquidacao.GreaterThanOrEqual(dataLiquidacao),
                                                          operacaoCotistaCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.ResgateTotal));
                    operacaoCotista.Query.es.Distinct = true;
                    operacaoCotista.Query.Load();

                    foreach (OperacaoCotista operacaoCotistaId in operacaoCotistaCollection)
                    {
                        int idCotista = operacaoCotistaId.IdCotista.Value;
                        PosicaoCotista posicaoCotista = new PosicaoCotista();
                        decimal quantidadeCotas = posicaoCotista.RetornaTotalCotas(idCliente, idCotista);

                        HistoricoCota historicoCota = new HistoricoCota();
                        historicoCota.BuscaValorCota(idCliente, dataDia);

                        decimal valorPosicao = 0;
                        if (historicoCota.CotaFechamento.HasValue)
                        {
                            valorPosicao = Math.Round(quantidadeCotas * historicoCota.CotaFechamento.Value, 2);
                        }

                        valorLiquidacao -= valorPosicao;
                    }
                    #endregion
                }
            }

            return valorLiquidacao;
        }

        public decimal RetornaValorMercado(int idCliente)
        {
            decimal valorData = 0;
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");

            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum());
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente),
                                    posicaoBolsaQuery.Quantidade.NotEqual(0));

            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            posicaoBolsa.Load(posicaoBolsaQuery);

            decimal valorPosicao = 0;
            if (posicaoBolsa.ValorMercado.HasValue)
            {
                valorPosicao = posicaoBolsa.ValorMercado.Value;
                valorData += valorPosicao;
            }

            PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("P");

            posicaoBMFQuery.Select(posicaoBMFQuery.ValorMercado.Sum());
            posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idCliente),
                                  posicaoBMFQuery.Quantidade.NotEqual(0),
                                  posicaoBMFQuery.TipoMercado.In((byte)TipoMercadoBMF.Disponivel,
                                                                 (byte)TipoMercadoBMF.OpcaoDisponivel,
                                                                 (byte)TipoMercadoBMF.OpcaoFuturo));

            PosicaoBMF posicaoBMF = new PosicaoBMF();
            posicaoBMF.Load(posicaoBMFQuery);

            if (posicaoBMF.ValorMercado.HasValue)
            {
                valorPosicao = posicaoBMF.ValorMercado.Value;
                valorData += valorPosicao;
            }


            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");

            posicaoFundoQuery.Select(posicaoFundoQuery.ValorBruto.Sum());
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente),
                                    posicaoFundoQuery.Quantidade.NotEqual(0));

            PosicaoFundo posicaoFundo = new PosicaoFundo();
            posicaoFundo.Load(posicaoFundoQuery);

            if (posicaoFundo.ValorBruto.HasValue)
            {
                valorPosicao = posicaoFundo.ValorBruto.Value;
                valorData += valorPosicao;
            }


            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                        posicaoRendaFixaQuery.Quantidade.NotEqual(0));

            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            posicaoRendaFixa.Load(posicaoRendaFixaQuery);

            if (posicaoRendaFixa.ValorMercado.HasValue)
            {
                valorPosicao = posicaoRendaFixa.ValorMercado.Value;
                valorData += valorPosicao;
            }

            PosicaoSwapQuery posicaoSwapQuery = new PosicaoSwapQuery("P");

            posicaoSwapQuery.Select(posicaoSwapQuery.Saldo.Sum());
            posicaoSwapQuery.Where(posicaoSwapQuery.IdCliente.Equal(idCliente));

            PosicaoSwap posicaoSwap = new PosicaoSwap();
            posicaoSwap.Load(posicaoSwapQuery);

            if (posicaoSwap.Saldo.HasValue)
            {
                valorPosicao = posicaoSwap.Saldo.Value;
                valorData += valorPosicao;
            }

            return valorData;
        }

        public decimal RetornaValorMercadoHistorico(int idCliente, DateTime dataHistorico)
        {
            decimal valorDataHistorico = 0;
            PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");

            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCliente),
                                             posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataHistorico),
                                             posicaoBolsaHistoricoQuery.Quantidade.NotEqual(0));

            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            posicaoBolsaHistorico.Load(posicaoBolsaHistoricoQuery);

            decimal valorPosicao = 0;
            if (posicaoBolsaHistorico.ValorMercado.HasValue)
            {
                valorPosicao = posicaoBolsaHistorico.ValorMercado.Value;
                valorDataHistorico += valorPosicao;
            }

            PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");

            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.ValorMercado.Sum());
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCliente),
                                           posicaoBMFHistoricoQuery.Quantidade.NotEqual(0),
                                           posicaoBMFHistoricoQuery.DataHistorico.Equal(dataHistorico),
                                           posicaoBMFHistoricoQuery.TipoMercado.In((byte)TipoMercadoBMF.Disponivel,
                                                                                   (byte)TipoMercadoBMF.OpcaoDisponivel,
                                                                                   (byte)TipoMercadoBMF.OpcaoFuturo));

            PosicaoBMFHistorico posicaoBMFHistorico = new PosicaoBMFHistorico();
            posicaoBMFHistorico.Load(posicaoBMFHistoricoQuery);

            if (posicaoBMFHistorico.ValorMercado.HasValue)
            {
                valorPosicao = posicaoBMFHistorico.ValorMercado.Value;
                valorDataHistorico += valorPosicao;
            }


            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");

            posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.ValorBruto.Sum());
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente),
                                             posicaoFundoHistoricoQuery.DataHistorico.Equal(dataHistorico),
                                             posicaoFundoHistoricoQuery.Quantidade.NotEqual(0));

            PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
            posicaoFundoHistorico.Load(posicaoFundoHistoricoQuery);

            if (posicaoFundoHistorico.ValorBruto.HasValue)
            {
                valorPosicao = posicaoFundoHistorico.ValorBruto.Value;
                valorDataHistorico += valorPosicao;
            }


            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");

            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.ValorMercado.Sum());
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                             posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataHistorico),
                                             posicaoRendaFixaHistoricoQuery.Quantidade.NotEqual(0));

            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
            posicaoRendaFixaHistorico.Load(posicaoRendaFixaHistoricoQuery);

            if (posicaoRendaFixaHistorico.ValorMercado.HasValue)
            {
                valorPosicao = posicaoRendaFixaHistorico.ValorMercado.Value;
                valorDataHistorico += valorPosicao;
            }

            PosicaoSwapHistoricoQuery posicaoSwapHistoricoQuery = new PosicaoSwapHistoricoQuery("P");

            posicaoSwapHistoricoQuery.Select(posicaoSwapHistoricoQuery.Saldo.Sum());
            posicaoSwapHistoricoQuery.Where(posicaoSwapHistoricoQuery.IdCliente.Equal(idCliente),
                                            posicaoSwapHistoricoQuery.DataHistorico.Equal(dataHistorico));

            PosicaoSwapHistorico posicaoSwapHistorico = new PosicaoSwapHistorico();
            posicaoSwapHistorico.Load(posicaoSwapHistoricoQuery);

            if (posicaoSwapHistorico.Saldo.HasValue)
            {
                valorPosicao = posicaoSwapHistorico.Saldo.Value;
                valorDataHistorico += valorPosicao;
            }

            return valorDataHistorico;
        }

        public List<CalculoFinanceiro.CashFlow> RetornaListaFluxoCaixa(int idCliente, DateTime dataInicio, DateTime dataFim, decimal valorInicial, decimal valorFinal)
        {
            List<CalculoFinanceiro.CashFlow> lista = new List<CalculoFinanceiro.CashFlow>();

            if (valorInicial != 0)
            {
                CalculoFinanceiro.CashFlow cfInicial = new CalculoFinanceiro.CashFlow((double)valorInicial * -1, dataInicio);
                lista.Add(cfInicial);
            }

            if (valorFinal != 0)
            {
                CalculoFinanceiro.CashFlow cfFinal = new CalculoFinanceiro.CashFlow((double)valorFinal, dataFim);
                lista.Add(cfFinal);
            }

            OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
            operacaoBolsaQuery.Select(operacaoBolsaQuery.ValorLiquido.Sum(),
                                      operacaoBolsaQuery.Data,
                                      operacaoBolsaQuery.TipoOperacao);
            operacaoBolsaQuery.Where(operacaoBolsaQuery.Data.GreaterThan(dataInicio),
                                     operacaoBolsaQuery.Data.LessThanOrEqual(dataFim),
                                     operacaoBolsaQuery.IdCliente.Equal(idCliente));

            operacaoBolsaQuery.GroupBy(operacaoBolsaQuery.Data, operacaoBolsaQuery.TipoOperacao);

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Load(operacaoBolsaQuery);

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                DateTime data = operacaoBolsa.Data.Value;
                decimal valor = operacaoBolsa.ValorLiquido.Value;

                if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Compra ||
                    operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade ||
                    operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Deposito)
                {
                    valor = valor * -1;
                }

                int index = lista.FindIndex(delegate(CalculoFinanceiro.CashFlow cf) { return cf.data == data; });

                if (index == -1)
                {
                    CalculoFinanceiro.CashFlow cf = new CalculoFinanceiro.CashFlow((double)valor, data);
                    lista.Add(cf);
                }
                else
                {
                    lista[index].valor += (double)valor;
                }
            }


            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
            operacaoBMFQuery.Select(operacaoBMFQuery.ValorLiquido.Sum(),
                                    operacaoBMFQuery.Data,
                                    operacaoBMFQuery.TipoOperacao);
            operacaoBMFQuery.Where(operacaoBMFQuery.Data.GreaterThan(dataInicio),
                                   operacaoBMFQuery.Data.LessThanOrEqual(dataFim),
                                   operacaoBMFQuery.IdCliente.Equal(idCliente),
                                   operacaoBMFQuery.TipoMercado.In((byte)TipoMercadoBMF.OpcaoDisponivel,
                                                                   (byte)TipoMercadoBMF.OpcaoFuturo));

            operacaoBMFQuery.GroupBy(operacaoBMFQuery.Data, operacaoBMFQuery.TipoOperacao);

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                DateTime data = operacaoBMF.Data.Value;
                decimal valor = operacaoBMF.ValorLiquido.Value;

                if (operacaoBMF.TipoOperacao == TipoOperacaoBMF.Compra ||
                    operacaoBMF.TipoOperacao == TipoOperacaoBMF.CompraDaytrade ||
                    operacaoBMF.TipoOperacao == TipoOperacaoBMF.Deposito)
                {
                    valor = valor * -1;
                }

                int index = lista.FindIndex(delegate(CalculoFinanceiro.CashFlow cf) { return cf.data == data; });

                if (index == -1)
                {
                    CalculoFinanceiro.CashFlow cf = new CalculoFinanceiro.CashFlow((double)valor, data);
                    lista.Add(cf);
                }
                else
                {
                    lista[index].valor += (double)valor;
                }
            }


            operacaoBMFQuery = new OperacaoBMFQuery("O");
            operacaoBMFQuery.Select(operacaoBMFQuery.Ajuste.Sum(),
                                    operacaoBMFQuery.Corretagem.Sum(),
                                    operacaoBMFQuery.Emolumento.Sum(),
                                    operacaoBMFQuery.Registro.Sum(),
                                    operacaoBMFQuery.Data,
                                    operacaoBMFQuery.TipoOperacao);
            operacaoBMFQuery.Where(operacaoBMFQuery.Data.GreaterThan(dataInicio),
                                   operacaoBMFQuery.Data.LessThanOrEqual(dataFim),
                                   operacaoBMFQuery.IdCliente.Equal(idCliente),
                                   operacaoBMFQuery.TipoMercado.In((byte)TipoMercadoBMF.Futuro));

            operacaoBMFQuery.GroupBy(operacaoBMFQuery.Data, operacaoBMFQuery.TipoOperacao);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                DateTime data = operacaoBMF.Data.Value;
                decimal valor = operacaoBMF.Ajuste.Value - operacaoBMF.Corretagem.Value - operacaoBMF.Emolumento.Value - operacaoBMF.Registro.Value;

                int index = lista.FindIndex(delegate(CalculoFinanceiro.CashFlow cf) { return cf.data == data; });

                if (index == -1)
                {
                    CalculoFinanceiro.CashFlow cf = new CalculoFinanceiro.CashFlow((double)valor, data);
                    lista.Add(cf);
                }
                else
                {
                    lista[index].valor += (double)valor;
                }
            }


            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            operacaoFundoQuery.Select(operacaoFundoQuery.ValorBruto.Sum(),
                                      operacaoFundoQuery.ValorIR.Sum(),
                                      operacaoFundoQuery.DataConversao,
                                      operacaoFundoQuery.TipoOperacao);
            operacaoFundoQuery.Where(operacaoFundoQuery.DataConversao.GreaterThan(dataInicio),
                                     operacaoFundoQuery.DataConversao.LessThanOrEqual(dataFim),
                                     operacaoFundoQuery.IdCliente.Equal(idCliente));

            operacaoFundoQuery.GroupBy(operacaoFundoQuery.DataConversao, operacaoFundoQuery.TipoOperacao);

            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Load(operacaoFundoQuery);

            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
            {
                DateTime dataConversao = operacaoFundo.DataConversao.Value;

                decimal valor = 0;
                if (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
                {
                    valor = operacaoFundo.ValorBruto.Value * -1;
                }
                else if (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto ||
                         operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                         operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                         operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal ||
                         operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.Amortizacao ||
                         operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.Juros ||
                         operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.AmortizacaoJuros)
                {
                    valor = operacaoFundo.ValorBruto.Value;
                }
                else if (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ComeCotas)
                {
                    valor = operacaoFundo.ValorIR.Value;
                }

                int index = lista.FindIndex(delegate(CalculoFinanceiro.CashFlow cf) { return cf.data == dataConversao; });

                if (index == -1)
                {
                    CalculoFinanceiro.CashFlow cf = new CalculoFinanceiro.CashFlow((double)valor, dataConversao);
                    lista.Add(cf);
                }
                else
                {
                    lista[index].valor += (double)valor;
                }
            }

            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery.Valor.Sum(),
                                          operacaoRendaFixaQuery.DataOperacao,
                                          operacaoRendaFixaQuery.TipoOperacao);
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao.GreaterThan(dataInicio),
                                     operacaoRendaFixaQuery.DataOperacao.LessThanOrEqual(dataFim),
                                     operacaoRendaFixaQuery.IdCliente.Equal(idCliente));

            operacaoRendaFixaQuery.GroupBy(operacaoRendaFixaQuery.DataOperacao, operacaoRendaFixaQuery.TipoOperacao);

            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);

            foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
            {
                DateTime dataOperacao = operacaoRendaFixa.DataOperacao.Value;

                decimal valor = operacaoRendaFixa.Valor.Value;
                if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda)
                {
                    valor = valor * -1;
                }

                int index = lista.FindIndex(delegate(CalculoFinanceiro.CashFlow cf) { return cf.data == dataOperacao; });

                if (index == -1)
                {
                    CalculoFinanceiro.CashFlow cf = new CalculoFinanceiro.CashFlow((double)valor, dataOperacao);
                    lista.Add(cf);
                }
                else
                {
                    lista[index].valor += (double)valor;
                }
            }

            LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("L");
            liquidacaoRendaFixaQuery.Select(liquidacaoRendaFixaQuery.ValorBruto.Sum(),
                                            liquidacaoRendaFixaQuery.DataLiquidacao);
            liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.DataLiquidacao.GreaterThan(dataInicio),
                                           liquidacaoRendaFixaQuery.DataLiquidacao.LessThanOrEqual(dataFim),
                                           liquidacaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                           liquidacaoRendaFixaQuery.TipoLancamento.NotEqual((byte)TipoLancamentoLiquidacao.Venda));

            liquidacaoRendaFixaQuery.GroupBy(liquidacaoRendaFixaQuery.DataLiquidacao);
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollection.Load(liquidacaoRendaFixaQuery);

            foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
            {
                DateTime dataLiquidacao = liquidacaoRendaFixa.DataLiquidacao.Value;
                decimal valorLiquidacaoRF = liquidacaoRendaFixa.ValorBruto.Value;

                int index = lista.FindIndex(delegate(CalculoFinanceiro.CashFlow cf) { return cf.data == dataLiquidacao; });

                if (index == -1)
                {
                    CalculoFinanceiro.CashFlow cf = new CalculoFinanceiro.CashFlow((double)valorLiquidacaoRF, dataLiquidacao);
                    lista.Add(cf);
                }
                else
                {
                    lista[index].valor += (double)valorLiquidacaoRF;
                }
            }

            LiquidacaoSwapQuery liquidacaoSwapQuery = new LiquidacaoSwapQuery("L");
            OperacaoSwapQuery operacaoSwapQuery = new OperacaoSwapQuery("O");
            PosicaoSwapHistoricoQuery posicaoSwapHistoricoQuery = new PosicaoSwapHistoricoQuery("P");
            liquidacaoSwapQuery.Select(liquidacaoSwapQuery.ValorAntecipacao.Sum(),
                                       liquidacaoSwapQuery.Data);
            liquidacaoSwapQuery.InnerJoin(posicaoSwapHistoricoQuery).On(posicaoSwapHistoricoQuery.IdPosicao == liquidacaoSwapQuery.IdPosicao);
            liquidacaoSwapQuery.InnerJoin(operacaoSwapQuery).On(operacaoSwapQuery.IdOperacao == posicaoSwapHistoricoQuery.IdOperacao);
            liquidacaoSwapQuery.Where(liquidacaoSwapQuery.Data.GreaterThan(dataInicio),
                                      liquidacaoSwapQuery.Data.LessThanOrEqual(dataFim),
                                      liquidacaoSwapQuery.IdCliente.Equal(idCliente),
                                      posicaoSwapHistoricoQuery.DataHistorico.Equal(operacaoSwapQuery.DataEmissao));

            liquidacaoSwapQuery.GroupBy(liquidacaoSwapQuery.Data);
            LiquidacaoSwapCollection liquidacaoSwapCollection = new LiquidacaoSwapCollection();
            liquidacaoSwapCollection.Load(liquidacaoSwapQuery);

            foreach (LiquidacaoSwap liquidacaoSwap in liquidacaoSwapCollection)
            {
                DateTime dataLiquidacao = liquidacaoSwap.Data.Value;
                decimal rendimento = liquidacaoSwap.ValorAntecipacao.Value;

                int index = lista.FindIndex(delegate(CalculoFinanceiro.CashFlow cf) { return cf.data == dataLiquidacao; });

                if (index == -1)
                {
                    CalculoFinanceiro.CashFlow cf = new CalculoFinanceiro.CashFlow((double)rendimento, dataLiquidacao);
                    lista.Add(cf);
                }
                else
                {
                    lista[index].valor += (double)rendimento;
                }
            }

            ProventoBolsaClienteQuery proventoBolsaClienteQuery = new ProventoBolsaClienteQuery("P");
            proventoBolsaClienteQuery.Select(proventoBolsaClienteQuery.CdAtivoBolsa,
                                             proventoBolsaClienteQuery.DataEx,
                                             proventoBolsaClienteQuery.DataPagamento,
                                             proventoBolsaClienteQuery.TipoProvento,
                                             proventoBolsaClienteQuery.Valor);
            proventoBolsaClienteQuery.Where(proventoBolsaClienteQuery.DataEx.GreaterThanOrEqual(dataInicio),
                                            proventoBolsaClienteQuery.DataEx.LessThanOrEqual(dataFim),
                                            proventoBolsaClienteQuery.IdCliente.Equal(idCliente));

            proventoBolsaClienteQuery.GroupBy(proventoBolsaClienteQuery.CdAtivoBolsa,
                                             proventoBolsaClienteQuery.DataEx,
                                             proventoBolsaClienteQuery.DataPagamento,
                                             proventoBolsaClienteQuery.TipoProvento,
                                             proventoBolsaClienteQuery.Valor);
            ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollection.Load(proventoBolsaClienteQuery);

            foreach (ProventoBolsaCliente proventoBolsaCliente in proventoBolsaClienteCollection)
            {
                DateTime dataEx = proventoBolsaCliente.DataEx.Value;
                decimal valor = proventoBolsaCliente.Valor.Value;

                int index = lista.FindIndex(delegate(CalculoFinanceiro.CashFlow cf) { return cf.data == dataEx; });

                if (index == -1)
                {
                    CalculoFinanceiro.CashFlow cf = new CalculoFinanceiro.CashFlow((double)valor, dataEx);
                    lista.Add(cf);
                }
                else
                {
                    lista[index].valor += (double)valor;
                }
            }

            lista.Sort(delegate(CalculoFinanceiro.CashFlow cf1, CalculoFinanceiro.CashFlow cf2) { return cf1.data.CompareTo(cf2.data); });

            return lista;
        }

        /// <summary>
        /// Faz a carga das posições e liquidação de carteira, permitindo realizar a explosão dos ativos dos fundos em carteira.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="explodeFundos"></param>
        public CarteiraDiaria MontaCarteira(int idCarteira, DateTime data, bool explodeFundos)
        {
            List<int> fundosDesconsiderar = new List<int>();
            List<int> fundosSemAtivos = new List<int>();
            return MontaCarteira(idCarteira, data, explodeFundos, fundosDesconsiderar, fundosSemAtivos);
        }

        /// <summary>
        /// Faz a carga das posições e liquidação de carteira, permitindo realizar a explosão dos ativos dos fundos em carteira.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="explodeFundos"></param>
        public CarteiraDiaria MontaCarteira(int idCarteira, DateTime data, bool explodeFundos,
            List<int> fundosDesconsiderar, List<int> fundosSemAtivos)
        {
            CarteiraDiaria carteiraDiaria = new CarteiraDiaria();

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            PosicaoBolsaCollection posicaoBolsaOpcoesCollection = new PosicaoBolsaCollection();
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            PosicaoBMFCollection posicaoBMFOpcoesCollection = new PosicaoBMFCollection();
            PosicaoRendaFixaCollection posicaoRendaFixaPublicoCollection = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaCollection posicaoRendaFixaPrivadoCollection = new PosicaoRendaFixaCollection();
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();

            Cliente cliente = new Cliente();
            cliente.Query.Select(cliente.Query.DataDia);
            cliente.Query.Select(cliente.Query.TipoControle);
            cliente.Query.Where(cliente.Query.IdCliente.Equal(idCarteira));
            cliente.Query.Load();

            #region Trata Posicao (menos fundos) e Liquidacao
            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data),
                                                        posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
            posicaoBolsaHistoricoCollection.Query.Load();

            posicaoBolsaCollection = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);

            PosicaoBolsaHistoricoCollection posicaoBolsaOpcoesHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaOpcoesHistoricoCollection.Query.Where(posicaoBolsaOpcoesHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                              posicaoBolsaOpcoesHistoricoCollection.Query.DataHistorico.Equal(data),
                                                              posicaoBolsaOpcoesHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
            posicaoBolsaOpcoesHistoricoCollection.Query.Load();

            posicaoBolsaOpcoesCollection = new PosicaoBolsaCollection(posicaoBolsaOpcoesHistoricoCollection);

            PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
            posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                                  posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoEmprestimoBolsaHistoricoCollection.Query.Load();

            posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection(posicaoEmprestimoBolsaHistoricoCollection);

            PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                             posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoTermoBolsaHistoricoCollection.Query.Load();

            posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection(posicaoTermoBolsaHistoricoCollection);


            PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
            posicaoSwapHistoricoCollection.Query.Where(posicaoSwapHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                             posicaoSwapHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoSwapHistoricoCollection.Query.Load();

            posicaoSwapCollection = new PosicaoSwapCollection(posicaoSwapHistoricoCollection);



            PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoCollection.Query.Where(posicaoBMFHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                      posicaoBMFHistoricoCollection.Query.DataHistorico.Equal(data),
                                                      posicaoBMFHistoricoCollection.Query.TipoMercado.In(TipoMercadoBMF.Futuro, TipoMercadoBMF.Disponivel));
            posicaoBMFHistoricoCollection.Query.Load();

            posicaoBMFCollection = new PosicaoBMFCollection(posicaoBMFHistoricoCollection);

            PosicaoBMFHistoricoCollection posicaoBMFOpcoesHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFOpcoesHistoricoCollection.Query.Where(posicaoBMFOpcoesHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                            posicaoBMFOpcoesHistoricoCollection.Query.DataHistorico.Equal(data),
                                                            posicaoBMFOpcoesHistoricoCollection.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo));
            posicaoBMFOpcoesHistoricoCollection.Query.Load();

            posicaoBMFOpcoesCollection = new PosicaoBMFCollection(posicaoBMFOpcoesHistoricoCollection);

            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                                 posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(data),
                                                 papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Publico));
            posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);

            posicaoRendaFixaPublicoCollection = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);

            papelRendaFixaQuery = new PapelRendaFixaQuery("A");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");

            posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCarteira),
                                                 posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(data),
                                                 papelRendaFixaQuery.TipoPapel.Equal((byte)TipoPapelTitulo.Privado));
            posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);

            posicaoRendaFixaPrivadoCollection = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);

            LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
            liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                      liquidacaoHistoricoCollection.Query.DataVencimento.GreaterThan(data),
                                                      liquidacaoHistoricoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                                      liquidacaoHistoricoCollection.Query.DataHistorico.Equal(data));
            liquidacaoHistoricoCollection.Query.Load();
            liquidacaoCollection = new LiquidacaoCollection(liquidacaoHistoricoCollection);
            #endregion

            saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCarteira),
                                             saldoCaixaCollection.Query.Data.Equal(data));
            saldoCaixaCollection.Query.Load();


            #region Trata a parte de fundos (com e sem explosão)
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                        posicaoFundoHistoricoCollection.Query.IdCliente,
                                                         posicaoFundoHistoricoCollection.Query.CotaDia.Avg(),
                                                         posicaoFundoHistoricoCollection.Query.ValorBruto.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.Quantidade.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.ValorAplicacao.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.ValorIOF.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.ValorIOFVirtual.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.ValorIR.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.ValorLiquido.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.ValorPerformance.Sum(),
                                                         posicaoFundoHistoricoCollection.Query.ValorRendimento.Sum());
            posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                            posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(data),
                                                            posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoFundoHistoricoCollection.Query.GroupBy(posicaoFundoHistoricoCollection.Query.IdCliente,
                posicaoFundoHistoricoCollection.Query.IdCarteira);
            posicaoFundoHistoricoCollection.Query.Load();

            if (explodeFundos)
            {
                #region Com explosão
                foreach (PosicaoFundoHistorico posicaoFundoExplosao in posicaoFundoHistoricoCollection)
                {
                    int idFundo = posicaoFundoExplosao.IdCarteira.Value;

                    Cliente clienteFundo = new Cliente();
                    clienteFundo.Query.Select(clienteFundo.Query.TipoControle);
                    clienteFundo.Query.Where(clienteFundo.Query.IdCliente.Equal(idFundo));
                    clienteFundo.Query.Load();

                    decimal valorFundo = posicaoFundoExplosao.ValorBruto.Value;

                    if (!fundosDesconsiderar.Contains(idFundo))
                    {
                        if (valorFundo != 0)
                        {
                            HistoricoCota historicoCota = new HistoricoCota();
                            historicoCota.Query.Select(historicoCota.Query.PLAbertura,
                                                       historicoCota.Query.PLFechamento);
                            historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idFundo),
                                                      historicoCota.Query.Data.Equal(data));
                            if (historicoCota.Query.Load())
                            {
                                Carteira carteira = new Carteira();
                                carteira.Query.Select(carteira.Query.TipoCota, carteira.Query.ExplodeCotasDeFundos);
                                carteira.Query.Where(carteira.Query.IdCarteira.Equal(idFundo));
                                carteira.Query.Load();

                                decimal valorPL = 0;
                                if (carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                                {
                                    valorPL = historicoCota.PLAbertura.Value;
                                }
                                else if (historicoCota.PLFechamento.HasValue)
                                {
                                    valorPL = historicoCota.PLFechamento.Value;
                                }

                                decimal fatorAjuste = 0.0M;
                                bool possuiPosicao = true;
                                if (clienteFundo.TipoControle.Value == (byte)TipoControleCliente.ApenasCotacao)
                                {
                                    possuiPosicao = isPossuiPosicao(posicaoFundoExplosao.IdCarteira.Value, data);
                                }

                                if (valorPL != 0 && possuiPosicao && carteira.ExplodeCotasDeFundos == "S" )
                                {
                                    fundosDesconsiderar.Add(idCarteira);
                                    CarteiraDiaria carteiraExplodida = this.MontaCarteira(posicaoFundoExplosao.IdCarteira.Value, data, true, fundosDesconsiderar, fundosSemAtivos);

                                    //ajusta fatorajuste desprezando liquidação e caixa.
                                    decimal valorDesconsiderar = 0;

                                    foreach (Liquidacao liquidacaoCarteiraExplodida in carteiraExplodida.LiquidacaoCollection)
                                    {
                                        valorDesconsiderar += (decimal)liquidacaoCarteiraExplodida.Valor;
                                    }

                                    foreach (SaldoCaixa saldoCaixaCarteiraExplodida in carteiraExplodida.SaldoCaixaCollection)
                                    {
                                        valorDesconsiderar += (decimal)saldoCaixaCarteiraExplodida.SaldoFechamento;
                                    }

                                    foreach (PosicaoFundo posicaofundoExplodida in carteiraExplodida.PosicaoFundoCollection)
                                    {
                                        if (!fundosSemAtivos.Contains(posicaofundoExplodida.IdCarteira.Value))
                                        {
                                            valorDesconsiderar += (decimal)posicaofundoExplodida.ValorBruto;
                                        }
                                    }

                                    if (fundosDesconsiderar.Count > 0)
                                    {
                                        PosicaoFundoHistoricoCollection posicaoFundodesconsiderarHistoricoCollection = new PosicaoFundoHistoricoCollection();
                                        posicaoFundodesconsiderarHistoricoCollection.Query.Select(posicaoFundodesconsiderarHistoricoCollection.Query.IdCarteira,
                                                                                                  posicaoFundodesconsiderarHistoricoCollection.Query.ValorBruto.Sum());
                                        posicaoFundodesconsiderarHistoricoCollection.Query.Where(posicaoFundodesconsiderarHistoricoCollection.Query.IdCliente.Equal(idFundo),
                                                                                        posicaoFundodesconsiderarHistoricoCollection.Query.DataHistorico.Equal(data),
                                                                                        posicaoFundodesconsiderarHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                                        posicaoFundodesconsiderarHistoricoCollection.Query.IdCarteira.In(fundosDesconsiderar));
                                        posicaoFundodesconsiderarHistoricoCollection.Query.GroupBy(posicaoFundodesconsiderarHistoricoCollection.Query.IdCarteira);
                                        posicaoFundodesconsiderarHistoricoCollection.Query.Load();
                                        foreach (PosicaoFundoHistorico posicaoFundoDesconsiderar in posicaoFundodesconsiderarHistoricoCollection)
                                        {
                                            valorDesconsiderar += (decimal)posicaoFundoDesconsiderar.ValorBruto;
                                        }
                                    }


                                    decimal denominadorAjuste = valorPL - valorDesconsiderar;
                                    fatorAjuste = denominadorAjuste == 0 ? 0 : (valorFundo / denominadorAjuste);

                                    decimal valorMercadoTotal = 0;

                                    #region Ajuste nos valores e quantidades das posicoes explodidas
                                    foreach (PosicaoBolsa posicaoBolsaCarteiraExplodida in carteiraExplodida.PosicaoBolsaCollection)
                                    {
                                        posicaoBolsaCarteiraExplodida.Quantidade = posicaoBolsaCarteiraExplodida.Quantidade.Value * fatorAjuste;
                                        posicaoBolsaCarteiraExplodida.ResultadoRealizar = posicaoBolsaCarteiraExplodida.ResultadoRealizar.Value * fatorAjuste;
                                        posicaoBolsaCarteiraExplodida.ValorCustoLiquido = posicaoBolsaCarteiraExplodida.ValorCustoLiquido.Value * fatorAjuste;
                                        posicaoBolsaCarteiraExplodida.ValorMercado = posicaoBolsaCarteiraExplodida.ValorMercado.Value * fatorAjuste;
                                        posicaoBolsaCollection.Add(posicaoBolsaCarteiraExplodida);
                                        valorMercadoTotal += posicaoBolsaCarteiraExplodida.ValorMercado.Value;
                                    }
                                    foreach (PosicaoBolsa posicaoBolsaOpcoesCarteiraExplodida in carteiraExplodida.PosicaoBolsaOpcoesCollection)
                                    {
                                        posicaoBolsaOpcoesCarteiraExplodida.Quantidade = posicaoBolsaOpcoesCarteiraExplodida.Quantidade.Value * fatorAjuste;
                                        posicaoBolsaOpcoesCarteiraExplodida.ResultadoRealizar = posicaoBolsaOpcoesCarteiraExplodida.ResultadoRealizar.Value * fatorAjuste;
                                        posicaoBolsaOpcoesCarteiraExplodida.ValorCustoLiquido = posicaoBolsaOpcoesCarteiraExplodida.ValorCustoLiquido.Value * fatorAjuste;
                                        posicaoBolsaOpcoesCarteiraExplodida.ValorMercado = posicaoBolsaOpcoesCarteiraExplodida.ValorMercado.Value * fatorAjuste;
                                        posicaoBolsaOpcoesCollection.Add(posicaoBolsaOpcoesCarteiraExplodida);
                                        valorMercadoTotal += posicaoBolsaOpcoesCarteiraExplodida.ValorMercado.Value;
                                    }
                                    foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsaCarteiraExplodida in carteiraExplodida.PosicaoEmprestimoBolsaCollection)
                                    {
                                        posicaoEmprestimoBolsaCarteiraExplodida.Quantidade = posicaoEmprestimoBolsaCarteiraExplodida.Quantidade.Value * fatorAjuste;
                                        posicaoEmprestimoBolsaCarteiraExplodida.ValorBase = posicaoEmprestimoBolsaCarteiraExplodida.ValorBase.Value * fatorAjuste;
                                        posicaoEmprestimoBolsaCarteiraExplodida.ValorCorrigidoCBLC = posicaoEmprestimoBolsaCarteiraExplodida.ValorCorrigidoCBLC.Value * fatorAjuste;
                                        posicaoEmprestimoBolsaCarteiraExplodida.ValorCorrigidoComissao = posicaoEmprestimoBolsaCarteiraExplodida.ValorCorrigidoComissao.Value * fatorAjuste;
                                        posicaoEmprestimoBolsaCarteiraExplodida.ValorCorrigidoJuros = posicaoEmprestimoBolsaCarteiraExplodida.ValorCorrigidoJuros.Value * fatorAjuste;
                                        posicaoEmprestimoBolsaCarteiraExplodida.ValorDiarioCBLC = posicaoEmprestimoBolsaCarteiraExplodida.ValorDiarioCBLC.Value * fatorAjuste;
                                        posicaoEmprestimoBolsaCarteiraExplodida.ValorDiarioComissao = posicaoEmprestimoBolsaCarteiraExplodida.ValorDiarioComissao.Value * fatorAjuste;
                                        posicaoEmprestimoBolsaCarteiraExplodida.ValorDiarioJuros = posicaoEmprestimoBolsaCarteiraExplodida.ValorDiarioJuros.Value * fatorAjuste;
                                        posicaoEmprestimoBolsaCarteiraExplodida.ValorMercado = posicaoEmprestimoBolsaCarteiraExplodida.ValorMercado.Value * fatorAjuste;
                                        posicaoEmprestimoBolsaCollection.Add(posicaoEmprestimoBolsaCarteiraExplodida);
                                        valorMercadoTotal += posicaoEmprestimoBolsaCarteiraExplodida.ValorMercado.Value;
                                    }
                                    foreach (PosicaoTermoBolsa posicaoTermoBolsaCarteiraExplodida in carteiraExplodida.PosicaoTermoBolsaCollection)
                                    {
                                        posicaoTermoBolsaCarteiraExplodida.Quantidade = posicaoTermoBolsaCarteiraExplodida.Quantidade.Value * fatorAjuste;
                                        posicaoTermoBolsaCarteiraExplodida.RendimentoApropriado = posicaoTermoBolsaCarteiraExplodida.RendimentoApropriado.Value * fatorAjuste;
                                        posicaoTermoBolsaCarteiraExplodida.RendimentoApropriar = posicaoTermoBolsaCarteiraExplodida.RendimentoApropriar.Value * fatorAjuste;
                                        posicaoTermoBolsaCarteiraExplodida.RendimentoDia = posicaoTermoBolsaCarteiraExplodida.RendimentoDia.Value * fatorAjuste;
                                        posicaoTermoBolsaCarteiraExplodida.RendimentoTotal = posicaoTermoBolsaCarteiraExplodida.RendimentoTotal.Value * fatorAjuste;
                                        posicaoTermoBolsaCarteiraExplodida.ValorCorrigido = posicaoTermoBolsaCarteiraExplodida.ValorCorrigido.Value * fatorAjuste;
                                        posicaoTermoBolsaCarteiraExplodida.ValorCurva = posicaoTermoBolsaCarteiraExplodida.ValorCurva.Value * fatorAjuste;
                                        posicaoTermoBolsaCarteiraExplodida.ValorMercado = posicaoTermoBolsaCarteiraExplodida.ValorMercado.Value * fatorAjuste;
                                        posicaoTermoBolsaCarteiraExplodida.ValorTermo = posicaoTermoBolsaCarteiraExplodida.ValorTermo.Value * fatorAjuste;
                                        posicaoTermoBolsaCarteiraExplodida.ValorTermoLiquido = posicaoTermoBolsaCarteiraExplodida.ValorTermoLiquido.Value * fatorAjuste;
                                        posicaoTermoBolsaCollection.Add(posicaoTermoBolsaCarteiraExplodida);
                                        valorMercadoTotal += posicaoTermoBolsaCarteiraExplodida.ValorMercado.Value;
                                    }

                                    foreach (PosicaoSwap posicaoSwapCarteiraExplodida in carteiraExplodida.PosicaoSwapCollection)
                                    {

                                        posicaoSwapCarteiraExplodida.ValorBase = posicaoSwapCarteiraExplodida.ValorBase.Value * fatorAjuste;
                                        posicaoSwapCarteiraExplodida.ValorContraParte = posicaoSwapCarteiraExplodida.ValorContraParte.Value * fatorAjuste;
                                        posicaoSwapCarteiraExplodida.Saldo = posicaoSwapCarteiraExplodida.Saldo.Value * fatorAjuste;
                                        posicaoSwapCarteiraExplodida.ValorParte = posicaoSwapCarteiraExplodida.ValorParte.Value * fatorAjuste;
                                        posicaoSwapCarteiraExplodida.ValorIR = posicaoSwapCarteiraExplodida.ValorIR.Value * fatorAjuste;
                                        posicaoSwapCollection.Add(posicaoSwapCarteiraExplodida);
                                        valorMercadoTotal += posicaoSwapCarteiraExplodida.Saldo.Value;
                                    }


                                    foreach (PosicaoBMF posicaoBMFCarteiraExplodida in carteiraExplodida.PosicaoBMFCollection)
                                    {
                                        posicaoBMFCarteiraExplodida.AjusteAcumulado = posicaoBMFCarteiraExplodida.AjusteAcumulado.Value * fatorAjuste;
                                        posicaoBMFCarteiraExplodida.AjusteDiario = posicaoBMFCarteiraExplodida.AjusteDiario.Value * fatorAjuste;
                                        posicaoBMFCarteiraExplodida.Quantidade = Convert.ToInt32(posicaoBMFCarteiraExplodida.Quantidade.Value * fatorAjuste);
                                        posicaoBMFCarteiraExplodida.ResultadoRealizar = posicaoBMFCarteiraExplodida.ResultadoRealizar.Value * fatorAjuste;
                                        posicaoBMFCarteiraExplodida.ValorCustoLiquido = posicaoBMFCarteiraExplodida.ValorCustoLiquido.Value * fatorAjuste;
                                        posicaoBMFCarteiraExplodida.ValorMercado = posicaoBMFCarteiraExplodida.ValorMercado.Value * fatorAjuste;
                                        posicaoBMFCollection.Add(posicaoBMFCarteiraExplodida);
                                        valorMercadoTotal += posicaoBMFCarteiraExplodida.ValorMercado.Value;
                                    }
                                    foreach (PosicaoBMF posicaoBMFOpcoesCarteiraExplodida in carteiraExplodida.PosicaoBMFOpcoesCollection)
                                    {
                                        posicaoBMFOpcoesCarteiraExplodida.Quantidade = Convert.ToInt32(posicaoBMFOpcoesCarteiraExplodida.Quantidade.Value * fatorAjuste);
                                        posicaoBMFOpcoesCarteiraExplodida.ResultadoRealizar = posicaoBMFOpcoesCarteiraExplodida.ResultadoRealizar.Value * fatorAjuste;
                                        posicaoBMFOpcoesCarteiraExplodida.ValorCustoLiquido = posicaoBMFOpcoesCarteiraExplodida.ValorCustoLiquido.Value * fatorAjuste;
                                        posicaoBMFOpcoesCarteiraExplodida.ValorMercado = posicaoBMFOpcoesCarteiraExplodida.ValorMercado.Value * fatorAjuste;
                                        posicaoBMFOpcoesCollection.Add(posicaoBMFOpcoesCarteiraExplodida);
                                        valorMercadoTotal += posicaoBMFOpcoesCarteiraExplodida.ValorMercado.Value;
                                    }
                                    foreach (PosicaoFundo posicaoFundoCarteiraExplodida in carteiraExplodida.PosicaoFundoCollection)
                                    {
                                        posicaoFundoCarteiraExplodida.Quantidade = posicaoFundoCarteiraExplodida.Quantidade.Value * fatorAjuste;
                                        posicaoFundoCarteiraExplodida.ValorAplicacao = posicaoFundoCarteiraExplodida.ValorAplicacao.Value * fatorAjuste;
                                        posicaoFundoCarteiraExplodida.ValorBruto = posicaoFundoCarteiraExplodida.ValorBruto.Value * fatorAjuste;
                                        posicaoFundoCarteiraExplodida.ValorIOF = posicaoFundoCarteiraExplodida.ValorIOF.Value * fatorAjuste;
                                        posicaoFundoCarteiraExplodida.ValorIOFVirtual = posicaoFundoCarteiraExplodida.ValorIOFVirtual.Value * fatorAjuste;
                                        posicaoFundoCarteiraExplodida.ValorIR = posicaoFundoCarteiraExplodida.ValorIR.Value * fatorAjuste;
                                        posicaoFundoCarteiraExplodida.ValorLiquido = posicaoFundoCarteiraExplodida.ValorLiquido.Value * fatorAjuste;
                                        posicaoFundoCarteiraExplodida.ValorPerformance = posicaoFundoCarteiraExplodida.ValorPerformance.Value * fatorAjuste;
                                        posicaoFundoCarteiraExplodida.ValorRendimento = posicaoFundoCarteiraExplodida.ValorRendimento.Value * fatorAjuste;
                                        posicaoFundoCollection.Add(posicaoFundoCarteiraExplodida);
                                        valorMercadoTotal += posicaoFundoCarteiraExplodida.ValorBruto.Value;
                                    }
                                    foreach (PosicaoRendaFixa posicaoRendaFixaPublicoCarteiraExplodida in carteiraExplodida.PosicaoRendaFixaPublicoCollection)
                                    {
                                        posicaoRendaFixaPublicoCarteiraExplodida.Quantidade = posicaoRendaFixaPublicoCarteiraExplodida.Quantidade.Value * fatorAjuste;
                                        posicaoRendaFixaPublicoCarteiraExplodida.ValorCorrecao = posicaoRendaFixaPublicoCarteiraExplodida.ValorCorrecao.Value * fatorAjuste;
                                        posicaoRendaFixaPublicoCarteiraExplodida.ValorCurva = posicaoRendaFixaPublicoCarteiraExplodida.ValorCurva.Value * fatorAjuste;
                                        posicaoRendaFixaPublicoCarteiraExplodida.ValorIOF = posicaoRendaFixaPublicoCarteiraExplodida.ValorIOF.Value * fatorAjuste;
                                        posicaoRendaFixaPublicoCarteiraExplodida.ValorIR = posicaoRendaFixaPublicoCarteiraExplodida.ValorIR.Value * fatorAjuste;
                                        posicaoRendaFixaPublicoCarteiraExplodida.ValorJuros = posicaoRendaFixaPublicoCarteiraExplodida.ValorJuros.Value * fatorAjuste;
                                        posicaoRendaFixaPublicoCarteiraExplodida.ValorMercado = posicaoRendaFixaPublicoCarteiraExplodida.ValorMercado.Value * fatorAjuste;

                                        if (posicaoRendaFixaPublicoCarteiraExplodida.ValorVolta.HasValue)
                                        {
                                            posicaoRendaFixaPublicoCarteiraExplodida.ValorVolta = posicaoRendaFixaPublicoCarteiraExplodida.ValorVolta.Value * fatorAjuste;
                                        }
                                        posicaoRendaFixaPublicoCollection.Add(posicaoRendaFixaPublicoCarteiraExplodida);
                                        valorMercadoTotal += posicaoRendaFixaPublicoCarteiraExplodida.ValorMercado.Value;
                                    }
                                    foreach (PosicaoRendaFixa posicaoRendaFixaPrivadoCarteiraExplodida in carteiraExplodida.PosicaoRendaFixaPrivadoCollection)
                                    {
                                        posicaoRendaFixaPrivadoCarteiraExplodida.Quantidade = posicaoRendaFixaPrivadoCarteiraExplodida.Quantidade.Value * fatorAjuste;
                                        posicaoRendaFixaPrivadoCarteiraExplodida.ValorCorrecao = posicaoRendaFixaPrivadoCarteiraExplodida.ValorCorrecao.Value * fatorAjuste;
                                        posicaoRendaFixaPrivadoCarteiraExplodida.ValorCurva = posicaoRendaFixaPrivadoCarteiraExplodida.ValorCurva.Value * fatorAjuste;
                                        posicaoRendaFixaPrivadoCarteiraExplodida.ValorIOF = posicaoRendaFixaPrivadoCarteiraExplodida.ValorIOF.Value * fatorAjuste;
                                        posicaoRendaFixaPrivadoCarteiraExplodida.ValorIR = posicaoRendaFixaPrivadoCarteiraExplodida.ValorIR.Value * fatorAjuste;
                                        posicaoRendaFixaPrivadoCarteiraExplodida.ValorJuros = posicaoRendaFixaPrivadoCarteiraExplodida.ValorJuros.Value * fatorAjuste;
                                        posicaoRendaFixaPrivadoCarteiraExplodida.ValorMercado = posicaoRendaFixaPrivadoCarteiraExplodida.ValorMercado.Value * fatorAjuste;

                                        if (posicaoRendaFixaPrivadoCarteiraExplodida.ValorVolta.HasValue)
                                        {
                                            posicaoRendaFixaPrivadoCarteiraExplodida.ValorVolta = posicaoRendaFixaPrivadoCarteiraExplodida.ValorVolta.Value * fatorAjuste;
                                        }
                                        posicaoRendaFixaPrivadoCollection.Add(posicaoRendaFixaPrivadoCarteiraExplodida);
                                        valorMercadoTotal += posicaoRendaFixaPrivadoCarteiraExplodida.ValorMercado.Value;
                                    }

                                    foreach (Liquidacao liquidacaoCarteiraExplodida in carteiraExplodida.LiquidacaoCollection)
                                    {
                                        liquidacaoCarteiraExplodida.Valor = liquidacaoCarteiraExplodida.Valor.Value * fatorAjuste;
                                        liquidacaoCollection.Add(liquidacaoCarteiraExplodida);
                                    }

                                    foreach (SaldoCaixa saldoCaixaCarteiraExplodida in carteiraExplodida.SaldoCaixaCollection)
                                    {
                                        saldoCaixaCarteiraExplodida.SaldoAbertura = saldoCaixaCarteiraExplodida.SaldoAbertura * fatorAjuste;
                                        saldoCaixaCarteiraExplodida.SaldoFechamento = saldoCaixaCarteiraExplodida.SaldoFechamento * fatorAjuste;
                                        saldoCaixaCollection.Add(saldoCaixaCarteiraExplodida);
                                    }

                                    #endregion


                                }
                                else //Se não tiver PL (não tem carteira) então adiciona a propria posição no fundo
                                {
                                    PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollectionExplosao = new PosicaoFundoHistoricoCollection();
                                    posicaoFundoHistoricoCollectionExplosao.AttachEntity(posicaoFundoExplosao);
                                    PosicaoFundoCollection posicaoFundoCollectionExplosao = new PosicaoFundoCollection(posicaoFundoHistoricoCollectionExplosao);

                                    posicaoFundoCollection.Combine(posicaoFundoCollectionExplosao);
                                    fundosSemAtivos.Add(idFundo);
                                }
                            }
                            else //se não tem cota
                            {
                                PosicaoFundoHistoricoCollection collectionIncluir = new PosicaoFundoHistoricoCollection();
                                collectionIncluir.Add(posicaoFundoExplosao);
                                posicaoFundoCollection.Add(new PosicaoFundoCollection(collectionIncluir));
                                fundosSemAtivos.Add(idFundo);
                            }
                        }
                    }
                }
                #endregion
            }
            else
            {
                #region Sem explosão
                posicaoFundoCollection = new PosicaoFundoCollection(posicaoFundoHistoricoCollection);
                #endregion
            }
            #endregion


            carteiraDiaria.PosicaoBolsaCollection = posicaoBolsaCollection;
            carteiraDiaria.PosicaoBolsaOpcoesCollection = posicaoBolsaOpcoesCollection;
            carteiraDiaria.PosicaoEmprestimoBolsaCollection = posicaoEmprestimoBolsaCollection;
            carteiraDiaria.PosicaoTermoBolsaCollection = posicaoTermoBolsaCollection;
            carteiraDiaria.PosicaoSwapCollection = posicaoSwapCollection;
            carteiraDiaria.PosicaoBMFCollection = posicaoBMFCollection;
            carteiraDiaria.PosicaoBMFOpcoesCollection = posicaoBMFOpcoesCollection;
            carteiraDiaria.PosicaoFundoCollection = posicaoFundoCollection;
            carteiraDiaria.PosicaoRendaFixaPublicoCollection = posicaoRendaFixaPublicoCollection;
            carteiraDiaria.PosicaoRendaFixaPrivadoCollection = posicaoRendaFixaPrivadoCollection;
            carteiraDiaria.LiquidacaoCollection = liquidacaoCollection;
            carteiraDiaria.SaldoCaixaCollection = saldoCaixaCollection;

            return carteiraDiaria;
        }

        public void ProcessaIndicadores(int idCarteira, DateTime data, bool processaIndicadorCarteira)
        {
            HistoricoCota historicoCotaAtual = new HistoricoCota();
            HistoricoCota historicoCotaAnterior = new HistoricoCota();
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            decimal? cotaPatrimonial = null;
            decimal? cotaRendimento = null;
            decimal? retornoAno = null;
            decimal? retornoMes = null;
            decimal? retornoDia = null;
            decimal? retornoDiaCotaRendimento = null;
            decimal? retornoInicio = null;
            decimal? volatilidade = null;

            CalculoMedida calculoMedida;

            calculoMedida = new CalculoMedida(idCarteira);
            try
            {
                retornoAno = calculoMedida.CalculaRetornoAno(data);
                retornoMes = calculoMedida.CalculaRetornoMes(data);
                retornoDia = calculoMedida.CalculaRetornoDia(data);
                retornoInicio = calculoMedida.CalculaRetornoInicio(data);
            }
            catch { }

            try
            {
                DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)cliente.IdLocal.GetValueOrDefault((int)LocalFeriadoFixo.Brasil), TipoFeriado.Outros);
                historicoCotaAtual.LoadByPrimaryKey(data, idCarteira);                
                historicoCotaAnterior.LoadByPrimaryKey(dataAnterior, idCarteira);

                retornoDiaCotaRendimento = (historicoCotaAtual.CotaRendimento.Value / historicoCotaAnterior.CotaRendimento.Value) - 1;

                cotaPatrimonial = historicoCotaAtual.CotaFechamento;
                cotaRendimento = historicoCotaAtual.CotaRendimento;
            }
            catch { }

            IndicadoresCarteira indicadoresCarteira = new IndicadoresCarteira();

            if (!indicadoresCarteira.LoadByPrimaryKey(data, idCarteira))
            {
                indicadoresCarteira = new IndicadoresCarteira();
                indicadoresCarteira.IdCarteira = idCarteira;
                indicadoresCarteira.Data = data;
            }

            indicadoresCarteira.RetornoAno = retornoAno;
            indicadoresCarteira.RetornoMes = retornoMes;
            indicadoresCarteira.RetornoDia = retornoDia;
            indicadoresCarteira.RetornoInicio = retornoInicio;
            indicadoresCarteira.Volatilidade = null;
            indicadoresCarteira.Sharpe3Meses = null;
            indicadoresCarteira.Sharpe6Meses = null;
            indicadoresCarteira.Sharpe12Meses = null;

            indicadoresCarteira.RetornoDiaCotaRendimento = retornoDiaCotaRendimento;
            indicadoresCarteira.CotaPatrimonial = cotaPatrimonial;
            indicadoresCarteira.CotaRendimento = cotaRendimento;

            if (processaIndicadorCarteira)
            {
                try
                {
                    DateTime dataInicio = Calendario.SubtraiDiaUtil(data, 252);
                    // Deve verificar se a data inicio for menor que a data inicio da carteira utilizar a data inicio da carteira
                    if (calculoMedida.DataInicio.Value > dataInicio)
                    {
                        dataInicio = calculoMedida.DataInicio.Value;
                    }
                    volatilidade = calculoMedida.CalculaVolatilidade(dataInicio, data);
                }
                catch { }

                indicadoresCarteira.Volatilidade = volatilidade;

                calculoMedida = new CalculoMedida(idCarteira, this.RetornaBenchmarkCarteira(idCarteira), DateTime.Now.Date, data);
                calculoMedida.InitListaRetornoMensal(calculoMedida.DataInicio.Value, data);
                List<CalculoMedida.EstatisticaRetornoMensal> lstRetornoMensal = calculoMedida.RetornaListaRetornosMensais(calculoMedida.DataInicio.Value, data);
                int elementos = lstRetornoMensal.Count;

                DateTime data3Meses;
                DateTime data6Meses;
                DateTime data12Meses;
                if (elementos > 3)
                {
                    data3Meses = lstRetornoMensal[elementos - 3].Data;

                    try
                    {
                        indicadoresCarteira.Sharpe3Meses = calculoMedida.calcularSharpeProcessamento(data3Meses.Date, data.Date);
                    }
                    catch
                    {
                        //Erro sem tratamento
                    }

                }
                else
                    indicadoresCarteira.Sharpe3Meses = 0;

                if (elementos > 6)
                {
                    data6Meses = lstRetornoMensal[elementos - 6].Data;

                    try
                    {
                        indicadoresCarteira.Sharpe6Meses = calculoMedida.calcularSharpeProcessamento(data6Meses.Date, data.Date);
                    }
                    catch
                    {
                        //Erro sem tratamento
                    }
                }
                else
                    indicadoresCarteira.Sharpe6Meses = 0;

                if (elementos > 12)
                {
                    data12Meses = lstRetornoMensal[elementos - 12].Data;

                    try
                    {
                        indicadoresCarteira.Sharpe12Meses = calculoMedida.calcularSharpeProcessamento(data12Meses.Date, data.Date);
                    }
                    catch
                    {
                        //Erro sem tratamento
                    }
                }
                else
                    indicadoresCarteira.Sharpe12Meses = 0;
            }

            indicadoresCarteira.Save();
        }

        public int RetornaBenchmarkCarteira(int idCarteira)
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery();

            carteiraQuery.Select(carteiraQuery.IdIndiceBenchmark);
            carteiraQuery.Where(carteiraQuery.IdCarteira.Equal(idCarteira));

            Carteira carteira = new Carteira();
            carteira.Load(carteiraQuery);

            if (carteira.IdIndiceBenchmark.HasValue)
                return (int)carteira.IdIndiceBenchmark;

            return 0;
        }

        public bool compareSuitability(Carteira carteira)
        {
            return this.PerfilRisco.Equals(carteira.PerfilRisco);
        }

        public static int RetornaCondominioCarteira(int idCarteira, DateTime data)
        {
            int CondominioCarteira = 0;

            FundoInvestimentoFormaCondominioCollection formaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();
            formaCondominioCollection.Query.es.Top = 1;
            formaCondominioCollection.Query.OrderBy(formaCondominioCollection.Query.DataInicioVigencia.Descending);
            formaCondominioCollection.Query.Where(formaCondominioCollection.Query.IdCarteira == idCarteira, 
                                                  formaCondominioCollection.Query.DataInicioVigencia.LessThan(data));

            formaCondominioCollection.Query.Load();

            if (formaCondominioCollection.Count > 0)
                CondominioCarteira = (int)formaCondominioCollection[0].FormaCondominio;
            else
            {
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                CondominioCarteira = (int)carteira.TipoFundo;
            }

            return CondominioCarteira;
        }

        public static int RetornaTipoTributacao(int idCarteira, DateTime data)
        {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            return RetornaTipoTributacao(carteira, data);
        }

        public static int RetornaTipoTributacao(Carteira carteira, DateTime data)
        {            
            int tipoTributacao = carteira.TipoTributacao.Value;
            int idCarteira = carteira.IdCarteira.Value;

            DesenquadramentoTributarioCollection desenquadramentoTributarioCollection = new DesenquadramentoTributarioCollection();
            desenquadramentoTributarioCollection.Query.Where(desenquadramentoTributarioCollection.Query.IdCarteira.Equal(idCarteira),
                                                             desenquadramentoTributarioCollection.Query.Data.LessThan(data));
            desenquadramentoTributarioCollection.Query.OrderBy(desenquadramentoTributarioCollection.Query.Data.Descending);

            desenquadramentoTributarioCollection.Query.Load();
            if (desenquadramentoTributarioCollection.Query.Load())
            {
                tipoTributacao = desenquadramentoTributarioCollection[0].ClassificacaoTributariaAtual.Value;
            }

            return tipoTributacao;
        }

        /// <summary>
        /// Se true, o ativo é isento já considerando desenquadramento e exceção tributaria
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool AtivoFundoIsento(int idCarteira, int? tipoInvestidor, DateTime data)
        {
            int tipoTributacao = RetornaTipoTributacao(idCarteira, data);

            if (tipoInvestidor.HasValue)
            {
                ExcecoesTributacaoIR excecaoTributariaIR = new ExcecoesTributacaoIR();
                excecaoTributariaIR.RetornaAliquota(idCarteira.ToString(), data, TipoMercado.Fundos, (ListaTipoInvestidor)tipoInvestidor.Value);

                if (excecaoTributariaIR.IsencaoIR.HasValue)
                    return (int)TipoTributacaoFundo.Isento == tipoTributacao && excecaoTributariaIR.IsencaoIR.Value == (int)ListaIsencaoIR.Isento;
            }

            return (int)TipoTributacaoFundo.Isento == tipoTributacao;
        }

        /// <summary>
        /// Retorna True se a carteira/cotista são a mesma pessoa
        /// </summary>
        /// <param name="carteira"></param>
        /// <param name="cotista"></param>
        /// <returns></returns>
        public bool CarteiraECotistaMesmaPessoa(Carteira carteira, Cotista cotista)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(carteira.IdCarteira.Value);

            return (cliente.IdPessoa == cotista.IdPessoa);
        }

        /// <summary>
        /// Retorna True se a carteira/cotista são a mesma pessoa
        /// </summary>
        /// <param name="carteira"></param>
        /// <param name="cliente"></param>
        /// <returns></returns>
        public bool CarteiraECotistaMesmaPessoa(Carteira carteira, Cliente cliente)
        {
            Cliente clienteCarteira = new Cliente();
            clienteCarteira.LoadByPrimaryKey(carteira.IdCarteira.Value);

            return (clienteCarteira.IdPessoa == cliente.IdPessoa);
        }

    }



}
