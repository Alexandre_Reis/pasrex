﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/12/2015 19:44:15
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Investidor;
using Financial.Util;
using Financial.Common.Enums;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;

namespace Financial.Fundo
{
	public partial class CalculoResultado : esCalculoResultado
	{
        public void CalculaResultado(int idCliente, DateTime data)
        {
            CalculoResultado calculoResultadoAnterior = new CalculoResultado();
            CotacaoIndice cotacaoCDI_D0 = new CotacaoIndice();
            HistoricoCota historicoCota = new HistoricoCota();
            HistoricoCota historicoCotaAnterior = new HistoricoCota();
            Cliente cliente = new Cliente();
            DateTime primeiroDiaUtilAno;
            DateTime primeiroDiaUtilMes;
            DateTime dataAnterior;
            CalculoResultado calculoResultado = new CalculoResultado();
            Trader trader = new Trader();
            bool utilizaPLD0_COF = ParametrosConfiguracaoSistema.Outras.CalculaCofUsandoPLD0.Equals("S");

            //Deleta registros
            this.DeletaCalculoResultado(idCliente, data);

            //Carrega cliente
            cliente.LoadByPrimaryKey(idCliente);

            int idLocalFeriado = cliente.IdLocal.Value;

            trader.Query.Where(trader.Query.IdCarteira.Equal(cliente.IdCliente.Value));
            bool carteiraTrader = trader.Query.Load();

            //Busca patrimonio de D0
            historicoCota.LoadByPrimaryKey(data, idCliente);
            decimal pl = historicoCota.PLFechamento.Value;

            if (cliente.DataImplantacao.Value != data)
            {
                //Busca primeiro dia útil do ano 
                primeiroDiaUtilAno = new DateTime(data.Year, 01, 01);
                if (!Calendario.IsDiaUtil(primeiroDiaUtilAno, idLocalFeriado, TipoFeriado.Outros))
                    primeiroDiaUtilAno = Calendario.AdicionaDiaUtil(primeiroDiaUtilAno, 1, idLocalFeriado, TipoFeriado.Outros);

                //Busca primeiro dia útil do mês 
                primeiroDiaUtilMes = new DateTime(data.Year, data.Month, 01);

                if (!Calendario.IsDiaUtil(primeiroDiaUtilMes, idLocalFeriado, TipoFeriado.Outros))
                    primeiroDiaUtilMes = Calendario.AdicionaDiaUtil(primeiroDiaUtilMes, 1, idLocalFeriado, TipoFeriado.Outros);

                //Busca primeiro dia útil do mês 
                dataAnterior = data;
                dataAnterior = Calendario.SubtraiDiaUtil(dataAnterior, 1, idLocalFeriado, TipoFeriado.Outros);

                //Busca resultado anterior para acumular
                calculoResultadoAnterior.LoadByPrimaryKey(dataAnterior, idCliente);

                //Busca patrimonio de D-1
                if (!historicoCotaAnterior.LoadByPrimaryKey(dataAnterior, idCliente))
                {
                    if (!utilizaPLD0_COF)
                    {
                        return;
                    }
                }

                //Busca Cotacao CDI de D0
                if (!cotacaoCDI_D0.LoadByPrimaryKey(data, (short)ListaIndiceFixo.CDI))
                    throw new Exception("Cotação de CDI não cadastrada para a data: " + data.ToString("dd/MM/yyyy"));

                //Calcula resultado                
                decimal plAnt = historicoCotaAnterior.PLFechamento.GetValueOrDefault(0);

                //Considera zera caixa no PL da carteira trader
                decimal vlZeraCaixa = 0;
                #region Busca zera caixa de trader
                if (carteiraTrader && cliente.ZeraCaixa.Equals("S"))
                {
                    LiquidacaoCollection liqColl = new LiquidacaoCollection();
                    liqColl.Query.Select(liqColl.Query.Valor.Sum(),
                                         liqColl.Query.Origem);
                    liqColl.Query.Where(liqColl.Query.DataLancamento.Equal(data) &
                                        liqColl.Query.IdCliente.Equal(idCliente) &
                                        liqColl.Query.Origem.In((int)OrigemLancamentoLiquidacao.Cotista.Resgate, (int)OrigemLancamentoLiquidacao.Cotista.Aplicacao));
                    liqColl.Query.GroupBy(liqColl.Query.Origem);

                    if (liqColl.Query.Load())
                    {
                        vlZeraCaixa = Math.Abs(liqColl[0].Valor.GetValueOrDefault(0));

                        if (liqColl[0].Origem.GetValueOrDefault(0) == (int)OrigemLancamentoLiquidacao.Cotista.Aplicacao)
                            vlZeraCaixa *= -1;
                    }
                }
                #endregion

                decimal valorCotista = 0;
                #region Busca aplicação/resgate
                OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.ValorBruto.Sum(),
                                                       operacaoCotistaCollection.Query.TipoOperacao);
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCliente),
                                                      operacaoCotistaCollection.Query.DataConversao.Equal(data));
                operacaoCotistaCollection.Query.GroupBy(operacaoCotistaCollection.Query.TipoOperacao);
                operacaoCotistaCollection.Query.Load();

                foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
                {
                    if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateBruto ||
                        operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateCotas ||
                        operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateLiquido ||
                        operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateTotal)
                    {
                        valorCotista += (operacaoCotista.ValorBruto.Value * -1);
                    }
                    else
                    {
                        valorCotista += operacaoCotista.ValorBruto.Value;
                    }                  
                }

                #endregion

                decimal valorPfee = 0;
                #region Taxa Pfee

                PortfolioPadrao portfolioPadrao = new PortfolioPadrao();
                portfolioPadrao = portfolioPadrao.RetornaPortfoliosVigentes(data);

                bool carteiraConsolidadora = false;
                if (portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) > 0)
                    carteiraConsolidadora = idCliente == portfolioPadrao.IdCarteiraTaxaPerformance.Value;

                if (carteiraConsolidadora)
                {
                    CalculoPerformanceHistorico calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                    calculoPerformanceHistorico.Query.Select(calculoPerformanceHistorico.Query.ValorDia.Sum());
                    calculoPerformanceHistorico.Query.Where(calculoPerformanceHistorico.Query.DataHistorico.LessThanOrEqual(data),
                                                            calculoPerformanceHistorico.Query.DataHistorico.GreaterThan(dataAnterior));

                    calculoPerformanceHistorico.Query.Load();

                    if (calculoPerformanceHistorico.ValorDia.HasValue)
                        valorPfee = calculoPerformanceHistorico.ValorDia.Value;                    
                }
                else
                {
                    CalculoPerformance calculoPerformance = new CalculoPerformance();
                    calculoPerformance.Query.Select(calculoPerformance.Query.ValorDia.Sum());
                    calculoPerformance.Query.Where(calculoPerformance.Query.IdCarteira.Equal(idCliente));

                    calculoPerformance.Query.Load();

                    if (calculoPerformance.ValorDia.HasValue)
                        valorPfee += calculoPerformance.ValorDia.Value;                                        
                }

                if (valorPfee == 0)
                {
                    LiquidacaoCollection liqPfeeColl = new LiquidacaoCollection();
                    liqPfeeColl.Query.Select(liqPfeeColl.Query.Valor.Sum());
                    liqPfeeColl.Query.Where(liqPfeeColl.Query.DataLancamento.Equal(data) &
                                  liqPfeeColl.Query.IdCliente.Equal(idCliente) &
                                  liqPfeeColl.Query.Origem.In((int)OrigemLancamentoLiquidacao.Provisao.TaxaPerformance, (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance));

                    if (liqPfeeColl.Query.Load())
                    {
                        valorPfee += liqPfeeColl[0].Valor.GetValueOrDefault(0);
                    }

                }
                #endregion

                decimal ajusteCota = 0;
                decimal ajusteCotaAnt = 0;
                #region Ajuste de compensacao
                Liquidacao liqAjuste = new Liquidacao();
                liqAjuste.Query.Select(liqAjuste.Query.Valor.Sum());
                liqAjuste.Query.Where(liqAjuste.Query.DataLancamento.Equal(data) &
                                      liqAjuste.Query.IdCliente.Equal(idCliente) &
                                      liqAjuste.Query.Origem.In((int)OrigemLancamentoLiquidacao.AjusteCompensacaoCota, (int)OrigemLancamentoLiquidacao.AjusteCompensacaoCotaEvento));

                if (liqAjuste.Query.Load())
                    ajusteCota += liqAjuste.Valor.GetValueOrDefault(0);


                LiquidacaoHistorico liqAjusteAnt = new LiquidacaoHistorico();
                liqAjusteAnt.Query.Select(liqAjusteAnt.Query.Valor.Sum());
                liqAjusteAnt.Query.Where(liqAjusteAnt.Query.DataLancamento.Equal(dataAnterior) &
                                      liqAjusteAnt.Query.DataLancamento.Equal(liqAjusteAnt.Query.DataHistorico) &
                                      liqAjusteAnt.Query.IdCliente.Equal(idCliente) &
                                      liqAjusteAnt.Query.Origem.In((int)OrigemLancamentoLiquidacao.AjusteCompensacaoCota, (int)OrigemLancamentoLiquidacao.AjusteCompensacaoCotaEvento));

                if (liqAjusteAnt.Query.Load())
                    ajusteCotaAnt += liqAjusteAnt.Valor.GetValueOrDefault(0);
                #endregion

                //Insere
                calculoResultado = new CalculoResultado();
                calculoResultado.IdCliente = idCliente;
                calculoResultado.DataReferencia = data;
                calculoResultado.Pl = pl;

                //Retira o ajuste de valor de cota, pfee e zera caixa (no caso de carteira trader)
                if (cliente.DescontaTributoPL == (byte)DescontoPLCliente.PLComDesconto)
                {
                    decimal plAux = 0;
                    plAux = (historicoCota.RetornaPatrimonioSemTributos(cliente, data) - valorCotista - ajusteCota - valorPfee) + vlZeraCaixa;
                    calculoResultado.PLCalculoPnL = plAux;
                }
                else
                {
                    calculoResultado.PLCalculoPnL = (pl - valorCotista - ajusteCota - valorPfee) + vlZeraCaixa;
                }

                decimal custo_Dia = Math.Round((utilizaPLD0_COF ? pl : plAnt) * (decimal)(Math.Pow(((double)cotacaoCDI_D0.Valor.Value / 100d + 1d), (1d / 252d)) - 1d), 2);
                decimal PnL_Dia = calculoResultado.PLCalculoPnL.Value - (plAnt - ajusteCotaAnt);
                decimal resultado_Dia = PnL_Dia - custo_Dia;

                custo_Dia = Math.Abs(custo_Dia);
                if (pl > 0)
                    custo_Dia *= (-1);

                calculoResultado.CustoDia = custo_Dia;
                calculoResultado.PnLDia = PnL_Dia;
                calculoResultado.ResultadoDia = resultado_Dia;

                if (data == primeiroDiaUtilMes)
                {
                    calculoResultado.CustoMes = custo_Dia;
                    calculoResultado.PnLMes = PnL_Dia;
                    calculoResultado.ResultadoMes = resultado_Dia;
                }
                else
                {
                    calculoResultado.CustoMes = calculoResultadoAnterior.CustoMes.GetValueOrDefault(0) + custo_Dia;
                    calculoResultado.PnLMes = calculoResultadoAnterior.PnLMes.GetValueOrDefault(0) + PnL_Dia;
                    calculoResultado.ResultadoMes = calculoResultadoAnterior.ResultadoMes.GetValueOrDefault(0) + resultado_Dia;
                }

                if (data == primeiroDiaUtilAno)
                {
                    calculoResultado.CustoAno = custo_Dia;
                    calculoResultado.PnLAno = PnL_Dia;
                    calculoResultado.ResultadoAno = resultado_Dia;
                }
                else
                {
                    calculoResultado.CustoAno = calculoResultadoAnterior.CustoAno.GetValueOrDefault(0) + custo_Dia;
                    calculoResultado.PnLAno = calculoResultadoAnterior.PnLAno.GetValueOrDefault(0) + PnL_Dia;
                    calculoResultado.ResultadoAno = calculoResultadoAnterior.ResultadoAno.GetValueOrDefault(0) + resultado_Dia;
                }
            }
            else //Primeiro Dia
            {
                calculoResultado = new CalculoResultado();
                calculoResultado.IdCliente = idCliente;
                calculoResultado.DataReferencia = data;
                calculoResultado.Pl = pl;
                calculoResultado.CustoDia = 0;
                calculoResultado.PnLDia = 0;
                calculoResultado.ResultadoDia = 0;
                calculoResultado.CustoMes = 0;
                calculoResultado.PnLMes = 0;
                calculoResultado.ResultadoMes = 0; 
                calculoResultado.CustoAno = 0;
                calculoResultado.PnLAno = 0;
                calculoResultado.ResultadoAno = 0;
            }

            calculoResultado.Save();
        }

        public void DeletaCalculoResultado(int idCliente, DateTime data)
        {
            CalculoResultadoCollection calculoResultadoColl = new CalculoResultadoCollection();

            calculoResultadoColl.Query.Where(calculoResultadoColl.Query.IdCliente.Equal(idCliente)
                                             & calculoResultadoColl.Query.DataReferencia.GreaterThanOrEqual(data));

            if (calculoResultadoColl.Query.Load())
            {
                calculoResultadoColl.MarkAllAsDeleted();
                calculoResultadoColl.Save();
            }
        }
	}
}
