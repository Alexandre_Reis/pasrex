/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 21/11/2014 18:38:12
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;

namespace Financial.Fundo
{
	public partial class PrazoMedio : esPrazoMedio
	{
        public void InserePrazoMedioPonderado(DateTime data, int idCliente, string descricao, List<int> lstPrazos, List<Decimal> lstValores, int enumMercadoAtivo, int? idPosicao)
        {
            decimal dividendo = 0;
            decimal divisor = 0;

            for (int count = 0; count <= lstPrazos.Count - 1; count++)
            {
                dividendo += lstPrazos[count] * lstValores[count];
                divisor += lstValores[count];
            }

            dividendo = (dividendo == 0 ? 1 : dividendo);
            divisor = (divisor == 0 ? 1 : divisor);
            this.InserePrazoMedio(data, idCliente, descricao, enumMercadoAtivo, divisor, (int)Math.Round(dividendo / divisor, 0d), idPosicao);
        }

        public void InserePrazoMedio(DateTime data, int idCliente, string descricao, int enumMercadoAtivo, decimal valorPosicao, int prazo, int? idPosicao)
        {
            PrazoMedioCollection prazoMedioColl = new PrazoMedioCollection();
            PrazoMedio prazoMedio = new PrazoMedio();

            prazoMedio.DataAtual = data;
            prazoMedio.IdCliente = idCliente;
            prazoMedio.Descricao = descricao;
            prazoMedio.MercadoAtivo = enumMercadoAtivo;
            prazoMedio.ValorPosicao = valorPosicao;
            prazoMedio.PrazoMedio = prazo;

            if (idPosicao.HasValue)
                prazoMedio.IdPosicao = idPosicao.Value;
            else
                prazoMedio.IdPosicao = null;

            prazoMedio.Save();
        }
	}
}
