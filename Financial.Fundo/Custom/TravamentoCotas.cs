/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/10/2014 17:11:35
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;

namespace Financial.Fundo
{
	public partial class TravamentoCotas : esTravamentoCotas
	{
        public void InsereTravamentoCotas(int idCarteira, DateTime dataReferencia, decimal valorCota, TipoTravamentoCota tipoTravamento)
        {
            this.DeletaTravamentoCotas(idCarteira, dataReferencia, tipoTravamento);

            TravamentoCotas travamento = new TravamentoCotas();
            travamento.IdCarteira = idCarteira;
            travamento.ValorCota = travamento.ValorCotaCalculada = valorCota;
            travamento.TipoBloqueio = (int)tipoTravamento;
            travamento.DataProcessamento = dataReferencia;

            travamento.Save();
        }

        public void DeletaTravamentoCotas(int idCarteira, DateTime dataReferencia, TipoTravamentoCota tipoTravamento)
        {
            TravamentoCotasCollection travamento = new TravamentoCotasCollection();

            travamento.Query.Where(travamento.Query.IdCarteira.Equal(idCarteira) & travamento.Query.DataProcessamento.Equal(dataReferencia) & travamento.Query.TipoBloqueio.Equal((int)tipoTravamento));

            if (travamento.Query.Load())
            {
                travamento.MarkAllAsDeleted();
                travamento.Save();
            }
        }
        
	}
}
