﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Exceptions;
using log4net;

namespace Financial.Fundo
{
	public partial class CadastroProvisao : esCadastroProvisao
	{
        /// <summary>
        /// Retorna o IdCadastro a partir do Tipo passado.        
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns>IdCadastro referente ao tipo passado</returns>
        public int RetornaIdCadastro(int tipo)
        {
            CadastroProvisaoCollection cadastroProvisaoCollection = new CadastroProvisaoCollection();

            cadastroProvisaoCollection.Query
                 .Select(cadastroProvisaoCollection.Query.IdCadastro)
                 .Where(cadastroProvisaoCollection.Query.Tipo == tipo);

            cadastroProvisaoCollection.Query.Load();

            if (cadastroProvisaoCollection.HasData)
            {
                return (int)((CadastroProvisao)cadastroProvisaoCollection[0]).IdCadastro;
            }
            else
            {
                throw new IdCadastroProvisaoNaoCadastradoException("Tipo de provisão " + tipo + " não cadastrado.");
            }                        
        }
	}
}
