﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 10/03/2015 17:09:57
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista;
using Financial.Investidor.Enums;
using Financial.Investidor;

namespace Financial.Fundo
{
	public partial class RentabilidadeSerieCollection : esRentabilidadeSerieCollection
	{
        /// <summary>
        /// Calcula rentabilidade de Fundo OffShore
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <param name="enumTipoProcessamento"></param>
        public void CalculaRentabilidadeSerieOffShore(DateTime data, int idFundoOffShore)
        {
            #region Objetos
            RentabilidadeSerieCollection rentabilidadeColl = new RentabilidadeSerieCollection();
            SeriesOffShoreCollection serieOffShoreCollection = new SeriesOffShoreCollection();
            RentabilidadeSerieCollection rentabilidadeSerieAnteriorColl = new RentabilidadeSerieCollection();
            List<RentabilidadeSerie> lstRentabilidadeSerieAnterior = new List<RentabilidadeSerie>();
            HistoricoCota historicoCotaAtual = new HistoricoCota();
            HistoricoCota historicoCotaAnterior = new HistoricoCota();
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            Cliente cliente = new Cliente();
            RentabilidadeSerie rentabilidadeAnterior;
            DateTime dataCotaAnterior;
            DateTime dataCotaAtual;
            #endregion 

            #region Deleta rentabilidade (Caso reprocessamento)
            rentabilidadeColl.Query.Where(rentabilidadeColl.Query.IdFundo.Equal(idFundoOffShore)
                                          & rentabilidadeColl.Query.Data.Equal(data));

            if (rentabilidadeColl.Query.Load())
            {
                rentabilidadeColl.MarkAllAsDeleted();
                rentabilidadeColl.Save();
            }
            rentabilidadeColl = new RentabilidadeSerieCollection();
            #endregion

            #region Carrega Cliente
            cliente.LoadByPrimaryKey(idFundoOffShore);

            if (cliente.IdTipo.Value != TipoClienteFixo.OffShore_PF && cliente.IdTipo.Value != TipoClienteFixo.OffShore_PJ)
                return;
            #endregion

            #region Historico Cota Atual/Cópia e anterior
            historicoCotaAtual.BuscaValorCotaDia(idFundoOffShore, data);

            if (historicoCotaAtual.Data.GetValueOrDefault(new DateTime()) == new DateTime())
                return;

            historicoCotaAnterior.BuscaValorCotaAnterior(idFundoOffShore, historicoCotaAtual.Data.Value.AddDays(-1));
            #endregion

            #region Datas
            dataCotaAnterior = historicoCotaAnterior.Data.GetValueOrDefault(new DateTime());
            dataCotaAtual = historicoCotaAtual.Data.Value;
            #endregion

            #region Carrega Rentabilidade anterior
            if (dataCotaAnterior != new DateTime())
            {
                rentabilidadeSerieAnteriorColl.Query.Where(rentabilidadeSerieAnteriorColl.Query.IdFundo.Equal(idFundoOffShore)
                                                           & rentabilidadeSerieAnteriorColl.Query.Data.Equal(dataCotaAnterior));

                if (rentabilidadeSerieAnteriorColl.Query.Load())
                    lstRentabilidadeSerieAnterior = (List<RentabilidadeSerie>)rentabilidadeSerieAnteriorColl;
            }
            #endregion

            #region Rentabilidade da série
            decimal rentabilidadeCalc = (historicoCotaAtual.CotaFechamento.Value / historicoCotaAnterior.CotaFechamento.GetValueOrDefault(1)) - 1;
            serieOffShoreCollection.Query.Where(serieOffShoreCollection.Query.IdClassesOffShore.Equal(idFundoOffShore));

            if (serieOffShoreCollection.Query.Load())
            {
                foreach (SeriesOffShore serieOffShore in serieOffShoreCollection)
                {
                    RentabilidadeSerie rentabilidade = rentabilidadeColl.AddNew();
                    rentabilidade.IdFundo = idFundoOffShore;
                    rentabilidade.Data = data;
                    rentabilidade.IdSerie = serieOffShore.IdSeriesOffShore.Value;
                    rentabilidade.QuantidadeSerie = posicaoCotista.RetornaTotalCotasPorSerie(idFundoOffShore, rentabilidade.IdSerie.Value, null);
                    rentabilidade.Rentabilidade = rentabilidadeCalc;

                    rentabilidadeAnterior = new RentabilidadeSerie(); ;
                    if (lstRentabilidadeSerieAnterior.Count > 0)
                        rentabilidadeAnterior = lstRentabilidadeSerieAnterior.Find(delegate(RentabilidadeSerie x) { return x.IdSerie == serieOffShore.IdSeriesOffShore.Value; });

                    if (rentabilidadeAnterior.IdSerie.GetValueOrDefault(0) != 0)
                        rentabilidade.RentabilidadeAcumuluda = (rentabilidadeAnterior.Rentabilidade.Value / serieOffShore.VlCotaInicial.Value) - 1;
                    else
                        rentabilidade.RentabilidadeAcumuluda = 0;
                    
                }
                rentabilidadeColl.Save();
            }
            #endregion
        }
	}
}
