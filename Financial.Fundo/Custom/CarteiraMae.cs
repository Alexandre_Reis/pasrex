/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/02/2011 17:56:48
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
    public partial class CarteiraMae : esCarteiraMae
	{
        /// <summary>
        /// Retorna se a Carteira passada � carteira m�e ou n�o
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <returns>True se carteira m�e ou False se n�o</returns>
        public bool IsCarteiraMae(int idCarteira)
        {
            this.QueryReset();
            this.Query.es.Distinct = true;
            this.Query
                 .Select(this.Query.IdCarteiraMae)
                 .Where(this.Query.IdCarteiraMae == idCarteira);

            this.Query.Load();

            if (this.es.HasData)
                return true;
            else
                return false;

        }
	}
}
