using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Common.Enums;
using Financial.Util;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo.Debug.HistoricoCota
{
    public class DebugCotaRecomposta
    {
        public decimal? CotaPreEvento;
        public decimal? CotaPosEvento;
        public DateTime? DataEvento;
        public decimal? ValorEvento;
        public byte? TipoEvento;
    }
}
