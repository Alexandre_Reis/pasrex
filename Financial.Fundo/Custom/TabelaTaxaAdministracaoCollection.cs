using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class TabelaTaxaAdministracaoCollection : esTabelaTaxaAdministracaoCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaTaxaAdministracaoCollection));

        /// <summary>
        /// Carrega o objeto TabelaTaxaAdministracaoCollection com todos os campos de TabelaTaxaAdministracao.
        /// Ordena decrescente por IdTabela, DataReferencia.
        /// Filtra somente com DataReferencia menor ou igual que a data, 
        /// e data menor ou igual a DataFim (ou DataFim = NULL).
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaTabelaTaxaAdministracao(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.DataReferencia.LessThanOrEqual(data))
                 .OrderBy(this.Query.IdCadastro.Descending,
                          this.Query.DataReferencia.Descending);

            return this.Query.Load();
        }

        public bool BuscaTabelaTaxaAdministracao(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCarteira == idCarteira)
                 .OrderBy(this.Query.IdCadastro.Descending,
                          this.Query.DataReferencia.Descending);

            return this.Query.Load();
        }

	}
}
