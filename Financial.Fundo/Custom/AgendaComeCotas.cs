/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/01/2015 11:01:59
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista;
using Financial.Tributo.Custom;
using Financial.Util;
using Financial.Fundo.Enums;
using Financial.Tributo.Enums;
using Financial.Common.Enums;
using Financial.Util.Enums;
using Financial.Investidor;
using Financial.Tributo;
using Financial.InvestidorCotista.Enums;

namespace Financial.Fundo
{
	public partial class AgendaComeCotas : esAgendaComeCotas
	{

        public static decimal fatorQuantidadeCotista(PosicaoCotista posicaoCotista, DateTime dataPosicao)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            CalculoTributo calculoTributo = new CalculoTributo();

            Financial.InvestidorCotista.Cotista cotista = new Financial.InvestidorCotista.Cotista();
            cotista.LoadByPrimaryKey(posicaoCotista.IdCotista.Value);

            if (cotista.IsentoIR.Equals("S"))
                return 1;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(posicaoCotista.IdCarteira.Value);

            DateTime dataFocal = Calendario.RetornaProximoComeCotas(posicaoCotista.DataAplicacao.Value);

            decimal RazaoCotasConsumidasNaoConsumidas = 1;

            decimal cotaAplicacao;
            historicoCota.BuscaValorCotaDia(posicaoCotista.IdCarteira.Value, posicaoCotista.DataAplicacao.Value);
            if (carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
            {
                cotaAplicacao = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaAplicacao = historicoCota.CotaFechamento.Value;
            }

            #region Verifica aliquotas
            int tipoTributacao = Carteira.RetornaTipoTributacao(posicaoCotista.IdCarteira.Value, dataPosicao);
            decimal aliquotaComeCotas = 0;
            if (tipoTributacao == (int)TipoTributacaoFundo.Acoes)
                aliquotaComeCotas = new decimal(0.15);
            else
            {
                if (tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo ||
                    tipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas)
                {
                    aliquotaComeCotas = new decimal(0.20);
                }
                else
                {
                    aliquotaComeCotas = new decimal(0.15);
                }
            }
            #endregion

            int diasLiquidacaoResgate = carteira.RetornaDiasLiquidacaoComeCotas(posicaoCotista.IdCarteira.Value, dataPosicao, carteira.DiasCotizacaoResgate.Value,
                                        carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                        (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            DateTime DataUltimaTributacao = posicaoCotista.DataAplicacao.Value;
            decimal CotaUltimaTributacao = cotaAplicacao;
            decimal IOFVirtualNaoConsumido = 0;
            decimal qtdIRComeCotas = 0;
            int seqComeCotas = 0;

            while (DateTime.Compare(dataPosicao, dataFocal) >= 0)
            {
                seqComeCotas++;

                #region Busca cota focal
                decimal cotaFocal;
                historicoCota.BuscaValorCotaDia(posicaoCotista.IdCarteira.Value, dataFocal);
                if (carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                {
                    cotaFocal = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaFocal = historicoCota.CotaFechamento.Value;
                }
                #endregion

                decimal rendimento = (cotaFocal - CotaUltimaTributacao) * RazaoCotasConsumidasNaoConsumidas;

                #region Calcula IOF
                decimal valorIOF = 0;
                if (cotista.IsentoIOF.Equals("N"))
                {

                    DateTime dataCalculoIOF = new DateTime();
                    if ((int)ContagemIOFVirtual.ContagemUltDiaUtil == carteira.ContaPrzIOFVirtual.Value)
                    {
                        dataCalculoIOF = dataFocal;
                    }
                    else if ((int)ContagemIOFVirtual.ContagemUltDiaCorrido == carteira.ContaPrzIOFVirtual.Value)
                    {
                        dataCalculoIOF = Calendario.RetornaUltimoDiaCorridoMes(dataFocal, 0);
                    }
                    else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value && (int)ContagemDiasPrazoIOF.Santander == carteira.ContagemPrazoIOF.Value)
                    {
                        dataCalculoIOF = dataFocal.AddDays((double)diasLiquidacaoResgate);

                        if (!Calendario.IsDiaUtil(dataCalculoIOF, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                            dataCalculoIOF = Calendario.AdicionaDiaUtil(dataCalculoIOF, 1, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    }
                    else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value)
                    {
                        if (carteira.ContagemDiasConversaoResgate.Equals((int)ContagemDias.Uteis))
                            dataCalculoIOF = Calendario.AdicionaDiaUtil(dataFocal, diasLiquidacaoResgate);
                        else
                            dataCalculoIOF = dataFocal.AddDays((double)diasLiquidacaoResgate);
                    }

                    valorIOF = calculoTributo.CalculaIOFFundo(1, cotaFocal, cotaAplicacao, dataCalculoIOF, posicaoCotista.DataAplicacao.Value);
                    valorIOF = valorIOF * RazaoCotasConsumidasNaoConsumidas;
                }
                #endregion

                decimal baseIr = rendimento - valorIOF + IOFVirtualNaoConsumido;
                decimal IRComeCotas = baseIr * aliquotaComeCotas;

                qtdIRComeCotas = IRComeCotas / cotaFocal;

                if (qtdIRComeCotas > 0)
                {
                    DataUltimaTributacao = dataFocal;
                    CotaUltimaTributacao = cotaFocal;
                }

                if (seqComeCotas == 1)  //sendo o primeiro come cotas transfiro o IOF para IOF Virtual que ser� usado no pr�ximo come cotas
                    IOFVirtualNaoConsumido = valorIOF;
                else //se houve ou n�o recolhimento de IR atualizo Ultima Tributa��o para zerar o IOF Virtual(se existir)
                    IOFVirtualNaoConsumido = 0;

                //Calcula a razao Cotas Consumidas dividido por Cotas Nao Consumidas pelo come cotas
                RazaoCotasConsumidasNaoConsumidas = RazaoCotasConsumidasNaoConsumidas - qtdIRComeCotas;

                //Aplicar a RazaoCotasConsumidasNaoConsumidas no IOF Virtual
                IOFVirtualNaoConsumido = IOFVirtualNaoConsumido * RazaoCotasConsumidasNaoConsumidas;

                //obter a pr�xima data de Come Cotas
                dataFocal = Calendario.RetornaProximoComeCotas(dataFocal);
            }

            return (1 / RazaoCotasConsumidasNaoConsumidas);

        }

        public static decimal fatorQuantidadeFundo(PosicaoFundo posicaoFundo, DateTime dataPosicao)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            CalculoTributo calculoTributo = new CalculoTributo();

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(posicaoFundo.IdCliente.Value);

            if (cliente.IsentoIR.Equals("S"))
                return 1;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(posicaoFundo.IdCarteira.Value);

            DateTime dataFocal = Calendario.RetornaProximoComeCotas(posicaoFundo.DataAplicacao.Value);

            decimal RazaoCotasConsumidasNaoConsumidas = 1;

            decimal cotaAplicacao;
            historicoCota.BuscaValorCotaDia(posicaoFundo.IdCarteira.Value, posicaoFundo.DataAplicacao.Value);
            if (carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
            {
                cotaAplicacao = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaAplicacao = historicoCota.CotaFechamento.Value;
            }

            #region Verifica aliquotas
            int tipoTributacao = Carteira.RetornaTipoTributacao(posicaoFundo.IdCarteira.Value, dataPosicao);
            decimal aliquotaComeCotas = 0;
            if (tipoTributacao == (int)TipoTributacaoFundo.Acoes)
                aliquotaComeCotas = new decimal(0.15);
            else
            {
                if (tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo ||
                    tipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas)
                {
                    aliquotaComeCotas = new decimal(0.20);
                }
                else
                {
                    aliquotaComeCotas = new decimal(0.15);
                }
            }
            #endregion

            int diasLiquidacaoResgate = carteira.RetornaDiasLiquidacaoComeCotas(posicaoFundo.IdCarteira.Value, dataPosicao, carteira.DiasCotizacaoResgate.Value,
                                        carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                        (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            DateTime DataUltimaTributacao = posicaoFundo.DataAplicacao.Value;
            decimal CotaUltimaTributacao = cotaAplicacao;
            decimal IOFVirtualNaoConsumido = 0;
            decimal qtdIRComeCotas = 0;
            int seqComeCotas = 0;

            while (DateTime.Compare(dataPosicao, dataFocal) >= 0)
            {
                seqComeCotas++;

                #region Busca cota focal
                decimal cotaFocal;
                historicoCota.BuscaValorCotaDia(posicaoFundo.IdCarteira.Value, dataFocal);
                if (carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                {
                    cotaFocal = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaFocal = historicoCota.CotaFechamento.Value;
                }
                #endregion

                decimal rendimento = (cotaFocal - CotaUltimaTributacao) * RazaoCotasConsumidasNaoConsumidas;

                #region Calcula IOF
                decimal valorIOF = 0;
                if (cliente.IsentoIOF.Equals("N"))
                {

                    DateTime dataCalculoIOF = new DateTime();
                    if ((int)ContagemIOFVirtual.ContagemUltDiaUtil == carteira.ContaPrzIOFVirtual.Value)
                    {
                        dataCalculoIOF = dataFocal;
                    }
                    else if ((int)ContagemIOFVirtual.ContagemUltDiaCorrido == carteira.ContaPrzIOFVirtual.Value)
                    {
                        dataCalculoIOF = Calendario.RetornaUltimoDiaCorridoMes(dataFocal, 0);
                    }
                    else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value && (int)ContagemDiasPrazoIOF.Santander == carteira.ContagemPrazoIOF.Value)
                    {
                        dataCalculoIOF = dataFocal.AddDays((double)diasLiquidacaoResgate);

                        if (!Calendario.IsDiaUtil(dataCalculoIOF, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                            dataCalculoIOF = Calendario.AdicionaDiaUtil(dataCalculoIOF, 1, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    }
                    else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value)
                    {
                        if (carteira.ContagemDiasConversaoResgate.Equals((int)ContagemDias.Uteis))
                            dataCalculoIOF = Calendario.AdicionaDiaUtil(dataFocal, diasLiquidacaoResgate);
                        else
                            dataCalculoIOF = dataFocal.AddDays((double)diasLiquidacaoResgate);
                    }

                    valorIOF = calculoTributo.CalculaIOFFundo(1, cotaFocal, cotaAplicacao, dataCalculoIOF, posicaoFundo.DataAplicacao.Value);
                    valorIOF = valorIOF * RazaoCotasConsumidasNaoConsumidas;
                }
                #endregion

                decimal baseIr = rendimento - valorIOF + IOFVirtualNaoConsumido;
                decimal IRComeCotas = baseIr * aliquotaComeCotas;

                qtdIRComeCotas = IRComeCotas / cotaFocal;

                if (qtdIRComeCotas > 0)
                {
                    DataUltimaTributacao = dataFocal;
                    CotaUltimaTributacao = cotaFocal;
                }

                if (seqComeCotas == 1)  //sendo o primeiro come cotas transfiro o IOF para IOF Virtual que ser� usado no pr�ximo come cotas
                    IOFVirtualNaoConsumido = valorIOF;
                else //se houve ou n�o recolhimento de IR atualizo Ultima Tributa��o para zerar o IOF Virtual(se existir)
                    IOFVirtualNaoConsumido = 0;

                //Calcula a razao Cotas Consumidas dividido por Cotas Nao Consumidas pelo come cotas
                RazaoCotasConsumidasNaoConsumidas = RazaoCotasConsumidasNaoConsumidas - qtdIRComeCotas;

                //Aplicar a RazaoCotasConsumidasNaoConsumidas no IOF Virtual
                IOFVirtualNaoConsumido = IOFVirtualNaoConsumido * RazaoCotasConsumidasNaoConsumidas;

                //obter a pr�xima data de Come Cotas
                dataFocal = Calendario.RetornaProximoComeCotas(dataFocal);
            }

            return (1 / RazaoCotasConsumidasNaoConsumidas);

        }

        public static AgendaComeCotas agendaComeCotasCotista(PosicaoCotista posicaoCotista, DateTime dataPosicao)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            CalculoTributo calculoTributo = new CalculoTributo();
            TabelaIOF tabelaIOF = new TabelaIOF();

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(posicaoCotista.IdCarteira.Value);

            Financial.InvestidorCotista.Cotista cotista = new Financial.InvestidorCotista.Cotista();
            cotista.LoadByPrimaryKey(posicaoCotista.IdCotista.Value);

            if (cotista.IsentoIR.Equals("S"))
                return null;

            int diasLiquidacaoResgate = carteira.RetornaDiasLiquidacaoComeCotas(posicaoCotista.IdCarteira.Value, dataPosicao, carteira.DiasCotizacaoResgate.Value,
                                        carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                        (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            #region Verifica aliquotas
            int tipoTributacao = Carteira.RetornaTipoTributacao(posicaoCotista.IdCarteira.Value, dataPosicao);
            decimal aliquotaComeCotas = 0;
            if (tipoTributacao == (int)TipoTributacaoFundo.Acoes)
                aliquotaComeCotas = new decimal(0.15);
            else
            {
                if (tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo ||
                    tipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas)
                {
                    aliquotaComeCotas = new decimal(0.20);
                }
                else
                {
                    aliquotaComeCotas = new decimal(0.15);
                }
            }
            #endregion

            DateTime dataFocal = Calendario.RetornaProximoComeCotas(posicaoCotista.DataAplicacao.Value);
            if (DateTime.Compare(dataPosicao, dataFocal) < 0)
                return null;

            decimal IOFVirtual = 0;
            decimal razaoCotasConsumidasPorNaoConsumidas = 1;
            DateTime dataTributacao = posicaoCotista.DataAplicacao.Value;
            DateTime dataUltimaTributacao = posicaoCotista.DataAplicacao.Value;
            decimal cotaTributacao = posicaoCotista.CotaAplicacao.Value;
            decimal cotaUltimaTributacao = posicaoCotista.CotaAplicacao.Value;
            decimal IOFUnitario = 0;
            decimal BaseIRUltimo = 0;
            decimal RendimentoUnitarioAcum = 0;
            decimal qtdIRComeCotas = 0;
            int SeqComeCotas = 0;
            decimal quantidadeFocal = posicaoCotista.QuantidadeAntesCortes.Value;
            decimal quantidadeUltimaTributacao = posicaoCotista.QuantidadeAntesCortes.Value;
            decimal cotaFocal = 0;
            DateTime dataCalculoIOF = new DateTime();
            decimal valorIOF = 0;
            decimal baseIr = 0;
            decimal IRComeCotas = 0;

            decimal residuo15 = new decimal(0.15);
            decimal residuo175 = new decimal(0.175);
            decimal residuo20 = new decimal(0.20);
            decimal residuo225 = new decimal(0.225);

            decimal valorResiduo15 = 0;
            decimal valorResiduo175 = 0;
            decimal valorResiduo20 = 0;
            decimal valorResiduo225 = 0;

            while (DateTime.Compare(dataPosicao, dataFocal) >= 0)
            {
                SeqComeCotas++;

                IOFVirtual = IOFVirtual * razaoCotasConsumidasPorNaoConsumidas;

                #region Busca cota focal
                historicoCota.BuscaValorCotaDia(posicaoCotista.IdCarteira.Value, dataFocal);
                if (carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                {
                    cotaFocal = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaFocal = historicoCota.CotaFechamento.Value;
                }
                #endregion

                #region Calcula IOF
                if (cotista.IsentoIOF.Equals("N"))
                {
                    if ((int)ContagemIOFVirtual.ContagemUltDiaUtil == carteira.ContaPrzIOFVirtual.Value)
                    {
                        dataCalculoIOF = dataFocal;
                    }
                    else if ((int)ContagemIOFVirtual.ContagemUltDiaCorrido == carteira.ContaPrzIOFVirtual.Value)
                    {
                        dataCalculoIOF = Calendario.RetornaUltimoDiaCorridoMes(dataFocal, 0);
                    }
                    else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value && (int)ContagemDiasPrazoIOF.Santander == carteira.ContagemPrazoIOF.Value)
                    {
                        dataCalculoIOF = dataFocal.AddDays((double)diasLiquidacaoResgate);

                        if (!Calendario.IsDiaUtil(dataCalculoIOF, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                            dataCalculoIOF = Calendario.AdicionaDiaUtil(dataCalculoIOF, 1, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    }
                    else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value)
                    {
                        if (carteira.ContagemDiasConversaoResgate.Equals((int)ContagemDias.Uteis))
                            dataCalculoIOF = Calendario.AdicionaDiaUtil(dataFocal, diasLiquidacaoResgate);
                        else
                            dataCalculoIOF = dataFocal.AddDays((double)diasLiquidacaoResgate);
                    }

                    valorIOF = calculoTributo.CalculaIOFFundo(1, cotaFocal, posicaoCotista.CotaAplicacao.Value, dataCalculoIOF, posicaoCotista.DataAplicacao.Value);
                }
                #endregion

                IOFUnitario = valorIOF / posicaoCotista.QuantidadeAntesCortes.Value;

                baseIr = ((cotaFocal - cotaTributacao) * quantidadeFocal) - valorIOF + IOFVirtual;

                if (baseIr > 0)
                {
                    BaseIRUltimo = baseIr;
                    decimal RendimentoUnitario = (cotaFocal - cotaTributacao) / posicaoCotista.QuantidadeAntesCortes.Value;
                    RendimentoUnitarioAcum = RendimentoUnitarioAcum + RendimentoUnitario;
                    IOFVirtual = 0;
                }

                IRComeCotas = baseIr * aliquotaComeCotas;

                valorResiduo15 = valorResiduo15 + Convert.ToDecimal(((residuo15 - aliquotaComeCotas) * baseIr) / posicaoCotista.QuantidadeAntesCortes.Value);
                valorResiduo175 = valorResiduo175 + Convert.ToDecimal(((residuo175 - aliquotaComeCotas) * baseIr) / posicaoCotista.QuantidadeAntesCortes.Value);
                valorResiduo20 = valorResiduo20 + Convert.ToDecimal(((residuo20 - aliquotaComeCotas) * baseIr) / posicaoCotista.QuantidadeAntesCortes.Value);
                valorResiduo225 = valorResiduo225 + Convert.ToDecimal(((residuo225 - aliquotaComeCotas) * baseIr) / posicaoCotista.QuantidadeAntesCortes.Value);

                qtdIRComeCotas = IRComeCotas / cotaFocal;

                //Transportar IOF_Virtual para IOF_VirtualNaoConsumido
                if (SeqComeCotas == 1)
                    IOFVirtual = valorIOF;

                if (qtdIRComeCotas > 0)
                {
                    quantidadeUltimaTributacao = quantidadeFocal;
                    quantidadeFocal = quantidadeFocal - qtdIRComeCotas;
                    razaoCotasConsumidasPorNaoConsumidas = quantidadeFocal / posicaoCotista.QuantidadeAntesCortes.Value;
                }

                cotaUltimaTributacao = cotaTributacao;
                dataUltimaTributacao = dataTributacao;

                dataTributacao = dataFocal;
                cotaTributacao = cotaFocal;

                dataFocal = Calendario.RetornaProximoComeCotas(dataFocal);

            }

            #region Cria agenda come cotas
            AgendaComeCotas agendaComeCotas = new AgendaComeCotas();
            agendaComeCotas.IdCarteira = posicaoCotista.IdCarteira;
            agendaComeCotas.DataLancamento = dataTributacao;
            agendaComeCotas.DataVencimento = dataTributacao;
            agendaComeCotas.TipoEvento = FonteOperacaoCotista.ComeCotas.GetHashCode();
            agendaComeCotas.IdPosicao = posicaoCotista.IdPosicao.Value;
            agendaComeCotas.TipoPosicao = TipoComeCotas.Cotista.GetHashCode();
            agendaComeCotas.DataUltimoIR = dataUltimaTributacao;
            agendaComeCotas.Quantidade = quantidadeFocal;
            agendaComeCotas.QuantidadeAntesCortes = posicaoCotista.QuantidadeAntesCortes;
            agendaComeCotas.ValorCotaAplic = posicaoCotista.CotaAplicacao.Value;
            agendaComeCotas.ValorCotaUltimoPagamentoIR = cotaUltimaTributacao;
            agendaComeCotas.ValorCota = cotaFocal;
            agendaComeCotas.AliquotaCC = aliquotaComeCotas * 100;
            agendaComeCotas.RendimentoBrutoDesdeAplicacao = Math.Round((cotaFocal - posicaoCotista.CotaAplicacao.Value) * quantidadeUltimaTributacao, 2, MidpointRounding.AwayFromZero);
            agendaComeCotas.RendimentoDesdeUltimoPagamentoIR = Math.Round((cotaTributacao - cotaUltimaTributacao) * quantidadeUltimaTributacao, 2, MidpointRounding.AwayFromZero);
            agendaComeCotas.NumDiasCorridosDesdeAquisicao = Calendario.NumeroDias(posicaoCotista.DataAplicacao.Value, dataTributacao);
            agendaComeCotas.PrazoIOF = Calendario.NumeroDias(posicaoCotista.DataAplicacao.Value, dataCalculoIOF);
            agendaComeCotas.AliquotaIOF = tabelaIOF.RetornaAliquotaIOF(posicaoCotista.DataAplicacao.Value, dataCalculoIOF);
            agendaComeCotas.ValorIOF = valorIOF / posicaoCotista.QuantidadeAntesCortes.Value;
            agendaComeCotas.ValorIOFVirtual = IOFVirtual / posicaoCotista.QuantidadeAntesCortes.Value;
            agendaComeCotas.PrejuizoUsado = 0;
            agendaComeCotas.RendimentoCompensado = baseIr;
            agendaComeCotas.ValorIRAgendado = (IRComeCotas - (qtdIRComeCotas * cotaFocal)) / posicaoCotista.QuantidadeAntesCortes.Value;
            agendaComeCotas.ValorIRPago = Math.Round(qtdIRComeCotas * cotaFocal, 2, MidpointRounding.AwayFromZero);

            if (Carteira.RetornaTipoTributacao(posicaoCotista.IdCarteira.Value, dataFocal).Equals((int)TipoTributacaoFundo.Acoes))
            {
                agendaComeCotas.Residuo15 = 0;
                agendaComeCotas.Residuo175 = 0;
                agendaComeCotas.Residuo20 = 0;
                agendaComeCotas.Residuo225 = 0;
            }
            else
            {

                agendaComeCotas.Residuo15 = valorResiduo15;
                agendaComeCotas.Residuo175 = valorResiduo175;
                agendaComeCotas.Residuo20 = valorResiduo20;
                agendaComeCotas.Residuo225 = valorResiduo225;
            }
            agendaComeCotas.QuantidadeComida = qtdIRComeCotas;
            agendaComeCotas.QuantidadeFinal = quantidadeFocal;
            agendaComeCotas.ExecucaoRecolhimento = 2;
            agendaComeCotas.TipoAliquotaIR = 1;
            agendaComeCotas.Save();
            #endregion

            return agendaComeCotas;

        }

        public static AgendaComeCotas agendaComeCotasFundo(PosicaoFundo posicaoFundo, DateTime dataPosicao)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            CalculoTributo calculoTributo = new CalculoTributo();
            TabelaIOF tabelaIOF = new TabelaIOF();

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(posicaoFundo.IdCarteira.Value);

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(posicaoFundo.IdCliente.Value);

            if (cliente.IsentoIR.Equals("S"))
                return null;

            int diasLiquidacaoResgate = carteira.RetornaDiasLiquidacaoComeCotas(posicaoFundo.IdCarteira.Value, dataPosicao, carteira.DiasCotizacaoResgate.Value,
                                        carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                        (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

            #region Verifica aliquotas
            int tipoTributacao = Carteira.RetornaTipoTributacao(posicaoFundo.IdCarteira.Value, dataPosicao);
            decimal aliquotaComeCotas = 0;
            if (tipoTributacao == (int)TipoTributacaoFundo.Acoes)
                aliquotaComeCotas = new decimal(0.15);
            else
            {
                if (tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo ||
                    tipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas)
                {
                    aliquotaComeCotas = new decimal(0.20);
                }
                else
                {
                    aliquotaComeCotas = new decimal(0.15);
                }
            }
            #endregion

            DateTime dataFocal = Calendario.RetornaProximoComeCotas(posicaoFundo.DataAplicacao.Value);
            if (DateTime.Compare(dataPosicao, dataFocal) < 0)
                return null;

            decimal IOFVirtual = 0;
            decimal razaoCotasConsumidasPorNaoConsumidas = 1;
            DateTime dataTributacao = posicaoFundo.DataAplicacao.Value;
            DateTime dataUltimaTributacao = posicaoFundo.DataAplicacao.Value;
            decimal cotaTributacao = posicaoFundo.CotaAplicacao.Value;
            decimal cotaUltimaTributacao = posicaoFundo.CotaAplicacao.Value;
            decimal IOFUnitario = 0;
            decimal BaseIRUltimo = 0;
            decimal RendimentoUnitarioAcum = 0;
            decimal qtdIRComeCotas = 0;
            int SeqComeCotas = 0;
            decimal quantidadeFocal = posicaoFundo.QuantidadeAntesCortes.Value;
            decimal quantidadeUltimaTributacao = posicaoFundo.QuantidadeAntesCortes.Value;
            decimal cotaFocal = 0;
            DateTime dataCalculoIOF = new DateTime();
            decimal valorIOF = 0;
            decimal baseIr = 0;
            decimal IRComeCotas = 0;

            decimal residuo15 = new decimal(0.15);
            decimal residuo175 = new decimal(0.175);
            decimal residuo20 = new decimal(0.20);
            decimal residuo225 = new decimal(0.225);

            decimal valorResiduo15 = 0;
            decimal valorResiduo175 = 0;
            decimal valorResiduo20 = 0;
            decimal valorResiduo225 = 0;

            while (DateTime.Compare(dataPosicao, dataFocal) >= 0)
            {
                SeqComeCotas++;

                IOFVirtual = IOFVirtual * razaoCotasConsumidasPorNaoConsumidas;

                #region Busca cota focal
                historicoCota.BuscaValorCotaDia(posicaoFundo.IdCarteira.Value, dataFocal);
                if (carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                {
                    cotaFocal = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaFocal = historicoCota.CotaFechamento.Value;
                }
                #endregion

                #region Calcula IOF
                if (cliente.IsentoIOF.Equals("N"))
                {
                    if ((int)ContagemIOFVirtual.ContagemUltDiaUtil == carteira.ContaPrzIOFVirtual.Value)
                    {
                        dataCalculoIOF = dataFocal;
                    }
                    else if ((int)ContagemIOFVirtual.ContagemUltDiaCorrido == carteira.ContaPrzIOFVirtual.Value)
                    {
                        dataCalculoIOF = Calendario.RetornaUltimoDiaCorridoMes(dataFocal, 0);
                    }
                    else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value && (int)ContagemDiasPrazoIOF.Santander == carteira.ContagemPrazoIOF.Value)
                    {
                        dataCalculoIOF = dataFocal.AddDays((double)diasLiquidacaoResgate);

                        if (!Calendario.IsDiaUtil(dataCalculoIOF, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                            dataCalculoIOF = Calendario.AdicionaDiaUtil(dataCalculoIOF, 1, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    }
                    else if ((int)ContagemIOFVirtual.SomaDiasConvResg == carteira.ContaPrzIOFVirtual.Value)
                    {
                        if (carteira.ContagemDiasConversaoResgate.Equals((int)ContagemDias.Uteis))
                            dataCalculoIOF = Calendario.AdicionaDiaUtil(dataFocal, diasLiquidacaoResgate);
                        else
                            dataCalculoIOF = dataFocal.AddDays((double)diasLiquidacaoResgate);
                    }

                    valorIOF = calculoTributo.CalculaIOFFundo(1, cotaFocal, posicaoFundo.CotaAplicacao.Value, dataCalculoIOF, posicaoFundo.DataAplicacao.Value);
                }
                #endregion

                IOFUnitario = valorIOF / posicaoFundo.QuantidadeAntesCortes.Value;

                baseIr = ((cotaFocal - cotaTributacao) * quantidadeFocal) - valorIOF + IOFVirtual;

                if (baseIr > 0)
                {
                    BaseIRUltimo = baseIr;
                    decimal RendimentoUnitario = (cotaFocal - cotaTributacao) / posicaoFundo.QuantidadeAntesCortes.Value;
                    RendimentoUnitarioAcum = RendimentoUnitarioAcum + RendimentoUnitario;
                    IOFVirtual = 0;
                }

                IRComeCotas = baseIr * aliquotaComeCotas;

                valorResiduo15 = valorResiduo15 + Convert.ToDecimal(((residuo15 - aliquotaComeCotas) * baseIr) / posicaoFundo.QuantidadeAntesCortes.Value);
                valorResiduo175 = valorResiduo175 + Convert.ToDecimal(((residuo175 - aliquotaComeCotas) * baseIr) / posicaoFundo.QuantidadeAntesCortes.Value);
                valorResiduo20 = valorResiduo20 + Convert.ToDecimal(((residuo20 - aliquotaComeCotas) * baseIr) / posicaoFundo.QuantidadeAntesCortes.Value);
                valorResiduo225 = valorResiduo225 + Convert.ToDecimal(((residuo225 - aliquotaComeCotas) * baseIr) / posicaoFundo.QuantidadeAntesCortes.Value);

                qtdIRComeCotas = IRComeCotas / cotaFocal;

                //Transportar IOF_Virtual para IOF_VirtualNaoConsumido
                if (SeqComeCotas == 1)
                    IOFVirtual = valorIOF;

                if (qtdIRComeCotas > 0)
                {
                    quantidadeUltimaTributacao = quantidadeFocal;
                    quantidadeFocal = quantidadeFocal - qtdIRComeCotas;
                    razaoCotasConsumidasPorNaoConsumidas = quantidadeFocal / posicaoFundo.QuantidadeAntesCortes.Value;
                }

                cotaUltimaTributacao = cotaTributacao;
                dataUltimaTributacao = dataTributacao;

                dataTributacao = dataFocal;
                cotaTributacao = cotaFocal;

                dataFocal = Calendario.RetornaProximoComeCotas(dataFocal);

            }

            #region Cria agenda come cotas
            AgendaComeCotas agendaComeCotas = new AgendaComeCotas();
            agendaComeCotas.IdCarteira = posicaoFundo.IdCarteira;
            agendaComeCotas.DataLancamento = dataTributacao;
            agendaComeCotas.DataVencimento = dataTributacao;
            agendaComeCotas.TipoEvento = FonteOperacaoCotista.ComeCotas.GetHashCode();
            agendaComeCotas.IdPosicao = posicaoFundo.IdPosicao.Value;
            agendaComeCotas.TipoPosicao = TipoComeCotas.Fundo.GetHashCode();
            agendaComeCotas.DataUltimoIR = dataUltimaTributacao;
            agendaComeCotas.Quantidade = quantidadeFocal;
            agendaComeCotas.QuantidadeAntesCortes = posicaoFundo.QuantidadeAntesCortes;
            agendaComeCotas.ValorCotaAplic = posicaoFundo.CotaAplicacao.Value;
            agendaComeCotas.ValorCotaUltimoPagamentoIR = cotaUltimaTributacao;
            agendaComeCotas.ValorCota = cotaFocal;
            agendaComeCotas.AliquotaCC = aliquotaComeCotas * 100;
            agendaComeCotas.RendimentoBrutoDesdeAplicacao = Math.Round((cotaFocal - posicaoFundo.CotaAplicacao.Value) * quantidadeUltimaTributacao, 2, MidpointRounding.AwayFromZero);
            agendaComeCotas.RendimentoDesdeUltimoPagamentoIR = Math.Round((cotaTributacao - cotaUltimaTributacao) * quantidadeUltimaTributacao, 2, MidpointRounding.AwayFromZero);
            agendaComeCotas.NumDiasCorridosDesdeAquisicao = Calendario.NumeroDias(posicaoFundo.DataAplicacao.Value, dataTributacao);
            agendaComeCotas.PrazoIOF = Calendario.NumeroDias(posicaoFundo.DataAplicacao.Value, dataCalculoIOF);
            agendaComeCotas.AliquotaIOF = tabelaIOF.RetornaAliquotaIOF(posicaoFundo.DataAplicacao.Value, dataCalculoIOF);
            agendaComeCotas.ValorIOF = valorIOF / posicaoFundo.QuantidadeAntesCortes.Value;
            agendaComeCotas.ValorIOFVirtual = IOFVirtual / posicaoFundo.QuantidadeAntesCortes.Value;
            agendaComeCotas.PrejuizoUsado = 0;
            agendaComeCotas.RendimentoCompensado = baseIr;
            agendaComeCotas.ValorIRAgendado = (IRComeCotas - (qtdIRComeCotas * cotaFocal)) / posicaoFundo.QuantidadeAntesCortes.Value;
            agendaComeCotas.ValorIRPago = Math.Round(qtdIRComeCotas * cotaFocal, 2, MidpointRounding.AwayFromZero);

            if (Carteira.RetornaTipoTributacao(posicaoFundo.IdCarteira.Value, dataFocal).Equals((int)TipoTributacaoFundo.Acoes))
            {
                agendaComeCotas.Residuo15 = 0;
                agendaComeCotas.Residuo175 = 0;
                agendaComeCotas.Residuo20 = 0;
                agendaComeCotas.Residuo225 = 0;
            }
            else
            {

                agendaComeCotas.Residuo15 = valorResiduo15;
                agendaComeCotas.Residuo175 = valorResiduo175;
                agendaComeCotas.Residuo20 = valorResiduo20;
                agendaComeCotas.Residuo225 = valorResiduo225;
            }
            agendaComeCotas.QuantidadeComida = qtdIRComeCotas;
            agendaComeCotas.QuantidadeFinal = quantidadeFocal;
            agendaComeCotas.ExecucaoRecolhimento = 2;
            agendaComeCotas.TipoAliquotaIR = 1;
            agendaComeCotas.Save();
            #endregion

            return agendaComeCotas;

        }


	}
}
