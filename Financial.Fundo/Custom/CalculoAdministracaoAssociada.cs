﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2014 13:17:59
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Fundo.Enums;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Fundo.Exceptions;
using Financial.Investidor.Enums;
using Financial.RendaFixa;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;

namespace Financial.Fundo
{
	public partial class CalculoAdministracaoAssociada : esCalculoAdministracaoAssociada
	{

        /// <summary>
        /// Calcula taxa de administracao por valor Fixo
        /// </summary>
        /// <param name="tabelaTaxaAdministracao"></param>
        /// <param name="idTabela"></param>
        /// <param name="idCadastro"></param>
        /// <param name="contagemDias"></param>
        /// <param name="numeroMesesRenovacao"></param>
        /// <param name="idLocal"></param>
        /// <param name="data"></param>
        /// <param name="idCarteira"></param>
        /// <returns></returns>
        public static CalculoAdministracaoAssociada CalculaTxAdmValorFixo(TabelaTaxaAdministracao tabelaTaxaAdministracao,
                                                           int idTabela,
                                                           int idCadastro,
                                                           int contagemDias,
                                                           int numeroMesesRenovacao,
                                                           short idLocal,
                                                           DateTime data,
                                                           int idCarteira)
        {
            CalculoAdministracaoAssociada calculoAdministracao = new CalculoAdministracaoAssociada();

            //Se já tiver registro faz update, senão insert do primeiro registro                    
            DateTime dataFimApropriacao = new DateTime();
            DateTime dataPagamento = new DateTime();
            decimal valorAcumulado = 0;
            decimal valorDia = 0;

            #region Cálculo da Taxa Administração por Valor Fixo
            decimal valorTotal = tabelaTaxaAdministracao.ValorFixoTotal.Value;

            if (valorTotal > 0)
            {
                if (calculoAdministracao.LoadByPrimaryKey(idTabela))
                {
                    #region Update
                    dataFimApropriacao = calculoAdministracao.DataFimApropriacao.Value;
                    dataPagamento = calculoAdministracao.DataPagamento.Value;
                    valorAcumulado = calculoAdministracao.ValorAcumulado.Value;
                    int numeroDias;
                    int numeroDiasNaoUteis = 1;
                    if (contagemDias == (int)ContagemDiasProvisao.Uteis)
                    {

                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao,
                                                                LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                        if (idLocal == (short)LocalFeriadoFixo.Brasil)
                        {
                            numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else
                        {
                            numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, (int)idLocal, TipoFeriado.Outros);
                        }
                    }
                    else
                    {
                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaCorridoMes(data, 1);

                        //Para dias corridos, é preciso contar tb final de semana e feriado anterior
                        DateTime dataUtilAnterior = new DateTime();
                        if (idLocal == (short)LocalFeriadoFixo.Brasil)
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                        }

                        DateTime dataInicioContagem = dataUtilAnterior;
                        if (DateTime.Compare(tabelaTaxaAdministracao.DataReferencia.Value, dataUtilAnterior) > 0)
                            dataInicioContagem = tabelaTaxaAdministracao.DataReferencia.Value;
                        else
                            dataInicioContagem = dataInicioContagem.AddDays(1);

                        numeroDias = Calendario.NumeroDias(dataInicioContagem, dataFimApropriacao);
                        if (dataUtilAnterior.Month != data.Month)
                        {
                            dataUtilAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                            numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            if (DateTime.Compare(tabelaTaxaAdministracao.DataReferencia.Value, dataUtilAnterior) > 0)
                            {
                                dataUtilAnterior = tabelaTaxaAdministracao.DataReferencia.Value;
                                numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            }
                        }
                        if (DateTime.Compare(dataUtilAnterior, data) < 0) numeroDiasNaoUteis = Calendario.NumeroDias(dataUtilAnterior, data);
                    }

                    decimal valorApropriar;
                    if (numeroDias == 0)
                    {
                        valorApropriar = 0;
                        valorDia = 0;
                    }
                    else
                    {
                        //Faz os devidos cálculos para o caso de Provisão ou Diferido
                        valorApropriar = valorTotal - valorAcumulado;
                        valorDia = Math.Round(valorApropriar / numeroDias, 2);
                        valorDia = valorDia * numeroDiasNaoUteis;
                        valorAcumulado += valorDia;
                    }

                    calculoAdministracao.ValorDia = valorDia;
                    calculoAdministracao.ValorAcumulado = valorAcumulado;
                    calculoAdministracao.ValorCPMFAcumulado = 0;
                    #endregion
                }
                else
                {
                    //Cria novo registro, calculando as datas de fim de apropriação e de pagamento
                    #region Insert
                    int ano = data.Year;
                    int mes = data.Month;

                    int numeroDias;
                    int numeroDiasNaoUteis = 1;
                    if (contagemDias == (int)ContagemDiasProvisao.Uteis)
                    {
                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao,
                                                                LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                        numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    { //Para dias corridos, й preciso contar tb final de semana e feriado anterior

                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaCorridoMes(data, 1);

                        DateTime dataUtilAnterior = new DateTime();
                        if (idLocal == (short)LocalFeriadoFixo.Brasil)
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                        }
                        DateTime dataInicioContagem = dataUtilAnterior;
                        if (DateTime.Compare(tabelaTaxaAdministracao.DataReferencia.Value, dataUtilAnterior) > 0)
                            dataInicioContagem = tabelaTaxaAdministracao.DataReferencia.Value;
                        else
                            dataInicioContagem = dataInicioContagem.AddDays(1);

                        numeroDias = Calendario.NumeroDias(dataInicioContagem, dataFimApropriacao);

                        if (dataUtilAnterior.Month != data.Month)
                        {
                            dataUtilAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                            numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            if (DateTime.Compare(tabelaTaxaAdministracao.DataReferencia.Value, dataUtilAnterior) > 0)
                            {
                                dataUtilAnterior = tabelaTaxaAdministracao.DataReferencia.Value;
                                numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            }
                        }
                        if (DateTime.Compare(dataUtilAnterior, data) < 0) numeroDiasNaoUteis = Calendario.NumeroDias(dataUtilAnterior, data);
                    }

                    decimal valorApropriar;
                    //Faz os devidos cálculos para o caso de Provisão ou Diferido
                    valorApropriar = valorTotal;
                    valorDia = Math.Round(valorApropriar / numeroDias, 2);
                    valorDia = valorDia * numeroDiasNaoUteis;
                    valorAcumulado = valorDia;

                    //Calcula data de pagamento
                    int numeroDiasPagamento = tabelaTaxaAdministracao.NumeroDiasPagamento.Value;
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento,
                                                                        LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    #region Insert em CalculoAdministracao
                    calculoAdministracao = new CalculoAdministracaoAssociada();
                    calculoAdministracao.IdTabela = idTabela;
                    calculoAdministracao.IdCarteira = idCarteira;
                    calculoAdministracao.ValorDia = valorDia;
                    calculoAdministracao.ValorAcumulado = valorAcumulado;
                    calculoAdministracao.DataFimApropriacao = dataFimApropriacao;
                    calculoAdministracao.DataPagamento = dataPagamento;
                    calculoAdministracao.ValorCPMFDia = 0;
                    calculoAdministracao.ValorCPMFAcumulado = 0;
                    #endregion
                    #endregion
                }
            }
            return calculoAdministracao;
            #endregion
        }

        /// <summary>
        /// Calcula taxa de administracao por percentual de PL
        /// </summary>
        /// <param name="tabelaTaxaAdministracao"></param>
        /// <param name="idTabela"></param>
        /// <param name="idCadastro"></param>
        /// <param name="contagemDias"></param>
        /// <param name="numeroMesesRenovacao"></param>
        /// <param name="idLocal"></param>
        /// <param name="data"></param>
        /// <param name="idCarteira"></param>
        /// <param name="cliente"></param>
        /// <param name="isIniciando"></param>
        /// <returns></returns>
        public static CalculoAdministracaoAssociada CalculaTxAdmPercPL(TabelaTaxaAdministracao tabelaTaxaAdministracao,
                                                int idTabela,
                                                int idCadastro,
                                                int contagemDias,
                                                int numeroMesesRenovacao,
                                                short idLocal,
                                                DateTime data,
                                                int idCarteira,
                                                Cliente cliente,
                                                bool isIniciando)
        {
            CalculoAdministracaoAssociada calculoAdministracao = new CalculoAdministracaoAssociada();

            //Se já tiver registro faz update, senão insert do primeiro registro                    
            DateTime dataFimApropriacao = new DateTime();
            DateTime dataPagamento = new DateTime();
            decimal valorAcumulado = 0;
            decimal valorDia = 0;


            #region Cálculo da Taxa Administração por % do PL
            #region Variáveis para cálculo de % sobre o PL
            if (!tabelaTaxaAdministracao.TipoApropriacao.HasValue || !tabelaTaxaAdministracao.Taxa.HasValue ||
                !tabelaTaxaAdministracao.BaseApuracao.HasValue || !tabelaTaxaAdministracao.BaseAno.HasValue ||
                !tabelaTaxaAdministracao.TipoLimitePL.HasValue)
            {
                throw new TabelaTaxaAdministracaoParametrosNulosException("Parâmetros da tabela de Taxa Administração nulos.");
            }
            int tipoApropriacao = tabelaTaxaAdministracao.TipoApropriacao.Value; //Exponencial ou Linear
            decimal taxa = tabelaTaxaAdministracao.Taxa.Value;
            List<TabelaEscalonamentoAdm> taxas = new List<TabelaEscalonamentoAdm>();
            int baseApuracao = tabelaTaxaAdministracao.BaseApuracao.Value; //Patr.Liquido/Patr.Bruto D0/D-1, 
            int baseAno = tabelaTaxaAdministracao.BaseAno.Value; //252, 360, 365, dias uteis ano
            int tipoLimitePL = tabelaTaxaAdministracao.TipoLimitePL.Value; //Total do PL, Limite Inferior
            #endregion

            DateTime dataAnterior = new DateTime();

            if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_DiaAnterior ||
                         baseApuracao == (int)BaseApuracaoAdministracao.PL_DiaAnterior)
            {
                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                }
            }

            bool historico = baseApuracao != (int)BaseApuracaoAdministracao.PBruto_Dia;
            DateTime dataAvaliacao = historico ? dataAnterior : data;

            decimal valorPL = 0;

            if (tabelaTaxaAdministracao.TipoGrupo.HasValue)
            {
                valorPL = CalculoAdministracao.RetornaValorMercadoSegmento(tabelaTaxaAdministracao, dataAvaliacao, historico);
            }
            else if (cliente.TipoControle.Value != (byte)TipoControleCliente.Cotista &&
                cliente.TipoControle.Value != (byte)TipoControleCliente.ApenasCotacao)
            {
                #region Busca o valor do PL (Bruto/Liquido, Abertura/Fechamento, D0/D-1)
                HistoricoCota historicoCota = new HistoricoCota();

                #region Busca o tipo de cota
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TipoCota);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                int tipoCota = carteira.TipoCota.Value;
                #endregion

                bool PLInexistente = false;
                if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_Dia)
                {
                    historicoCota.BuscaValorPatrimonioDia(idCarteira, data);
                    valorPL = historicoCota.PatrimonioBruto.Value;

                    if (cliente.IdTipo == TipoClienteFixo.FDIC)
                    {
                        PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                        decimal valorPLFilhas = Math.Abs(posicaoRendaFixa.RetornaValorNegativo(idCarteira));

                        valorPL += valorPLFilhas;
                    }
                }
                else if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_DiaAnterior ||
                         baseApuracao == (int)BaseApuracaoAdministracao.PL_DiaAnterior)
                {
                    try
                    {
                        historicoCota.BuscaValorPatrimonioDia(idCarteira, dataAnterior);
                    }
                    catch (Exception)
                    {
                        PLInexistente = true;

                        if (isIniciando)
                        {
                            valorPL = 0;
                        }
                        else
                        {
                            StringBuilder mensagem = new StringBuilder();
                            mensagem.Append("Patrimônio não cadastrado na data ")
                                    .Append(data + " para a carteira " + idCarteira);
                            throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
                        }
                    }

                    if (!PLInexistente)
                    {
                        if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_DiaAnterior)
                        {
                            valorPL = historicoCota.PatrimonioBruto.Value;
                        }
                        else if (baseApuracao == (int)BaseApuracaoAdministracao.PL_DiaAnterior)
                        {
                            if (tipoCota == (int)TipoCotaFundo.Abertura)
                            {
                                valorPL = historicoCota.PLAbertura.Value;
                            }
                            else
                            {
                                valorPL = historicoCota.PLFechamento.Value;
                            }
                        }

                        if (cliente.IdTipo == TipoClienteFixo.FDIC)
                        {
                            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                            decimal valorPLFilhas = Math.Abs(posicaoRendaFixaHistorico.RetornaValorNegativo(idCarteira, dataAnterior));

                            valorPL += valorPLFilhas;
                        }
                    }
                }
                #endregion
            }
            else
            {
                HistoricoCota historicoCota = new HistoricoCota();
                valorPL = historicoCota.RetornaPLComCotaInformada(idCarteira, data, cliente.TipoControle.Value, historico);
            }

            #region Verifica se existem Segmentos ou Ativos para serem excluidos do cálculo
            decimal valorExcluiPL = CalculoAdministracao.RetornaValorMercadoExcluiSegmento(tabelaTaxaAdministracao, dataAvaliacao, historico);
            valorPL -= valorExcluiPL;
            #endregion


            #region Tratamento de expurgo da Taxa Adm e faixas de escalonamento
            if (cliente.TipoControle.Value != (byte)TipoControleCliente.Cotista && cliente.TipoControle.Value != (byte)TipoControleCliente.ApenasCotacao)
            {
                //Se tiver algum expurgo (taxa adm calculada por mercado), ajusta o PL
                valorPL -= CalculoAdministracao.RetornaValorExpurgoAdministracao(idCarteira, idTabela, dataAvaliacao, historico);

                if (valorPL < 0)
                {
                    valorPL = 0;
                }


                #region Verifica se tem tabela de escalonamento e usa a taxa de acordo com a faixa de PL, se for o caso
                TabelaEscalonamentoAdmCollection tabelaEscalonamentoAdmCollection = new TabelaEscalonamentoAdmCollection();
                tabelaEscalonamentoAdmCollection.Query.Where(tabelaEscalonamentoAdmCollection.Query.IdTabela.Equal(idTabela),
                                                             tabelaEscalonamentoAdmCollection.Query.FaixaPL.LessThanOrEqual(valorPL));
                tabelaEscalonamentoAdmCollection.Query.OrderBy(tabelaEscalonamentoAdmCollection.Query.FaixaPL.Descending);
                tabelaEscalonamentoAdmCollection.Query.Load();
                if (tabelaTaxaAdministracao.EscalonaProporcional == "S")
                {
                    foreach (TabelaEscalonamentoAdm tabelaEscalonamentoAdm in tabelaEscalonamentoAdmCollection)
                    {
                        taxas.Add(tabelaEscalonamentoAdm);
                    }
                }
                else
                {
                    if (tabelaEscalonamentoAdmCollection.Count > 0)
                    {
                        taxa = tabelaEscalonamentoAdmCollection[0].Percentual.Value;
                    }
                }
                #endregion

                //Aplica o limite sobre o PL
                if (tipoLimitePL == (int)TipoLimitePLAdministracao.LimiteSuperior)
                {
                    if (!tabelaTaxaAdministracao.ValorLimite.HasValue)
                    {
                        throw new TabelaTaxaAdministracaoParametrosNulosException("Valor de limite superior de Taxa Administração nulo.");
                    }
                    decimal limiteSuperiorPL = tabelaTaxaAdministracao.ValorLimite.Value;

                    if (valorPL > limiteSuperiorPL)
                    {
                        valorPL -= limiteSuperiorPL;
                    }
                }
                else if (tipoLimitePL == (int)TipoLimitePLAdministracao.LimiteInferior)
                {
                    if (!tabelaTaxaAdministracao.ValorLimite.HasValue)
                    {
                        throw new TabelaTaxaAdministracaoParametrosNulosException("Valor de limite superior de Taxa Administração nulo.");
                    }
                    decimal limiteInferiorPL = tabelaTaxaAdministracao.ValorLimite.Value;

                    if (valorPL < limiteInferiorPL)
                    {
                        valorPL = limiteInferiorPL;
                    }
                }
            }
            #endregion

            #region Dedução da base de PL de todas as demais tabelas com tipoGrupo != null
            if (!tabelaTaxaAdministracao.TipoGrupo.HasValue)
            {
                //decimal valorOutros = CalculoAdministracao.RetornaValorMercadoOutros(idCarteira, idTabela, dataAvaliacao, historico);
                //valorPL -= valorOutros;
            }
            #endregion
            #endregion

            #region Calcula número de dias (período e Ano)
            int numeroDias;
            if (contagemDias == (int)ContagemDiasProvisao.Corridos)
            {
                //Para dias corridos, é preciso contar tb final de semana e feriado anterior
                DateTime dataUtilAnterior = new DateTime();
                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                }

                DateTime dataInicioContagem = dataUtilAnterior;
                numeroDias = Calendario.NumeroDias(dataInicioContagem, data);
            }
            else
            {
                numeroDias = 1;
            }

            //Calcula o nr de dias úteis para base de cálculo com número exato de dias úteis no ano
            if (baseAno == (int)BaseAnoAdministracao.BaseAnoUteisExato)
            {
                DateTime primeiroDiaUtilAno = new DateTime();
                DateTime ultimoDiaUtilAno = new DateTime();

                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    primeiroDiaUtilAno = Calendario.RetornaPrimeiroDiaUtilAno(data, 0);
                    ultimoDiaUtilAno = Calendario.RetornaUltimoDiaUtilAno(data, 0);
                    baseAno = Calendario.NumeroDias(primeiroDiaUtilAno, ultimoDiaUtilAno, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    primeiroDiaUtilAno = Calendario.RetornaPrimeiroDiaUtilAno(data, 0, (int)idLocal, TipoFeriado.Outros);
                    ultimoDiaUtilAno = Calendario.RetornaUltimoDiaUtilAno(data, 0, (int)idLocal, TipoFeriado.Outros);
                    baseAno = Calendario.NumeroDias(primeiroDiaUtilAno, ultimoDiaUtilAno, (int)idLocal, TipoFeriado.Outros);
                }
            }
            #endregion

            if (calculoAdministracao.LoadByPrimaryKey(idTabela))
            {
                #region Update
                dataFimApropriacao = calculoAdministracao.DataFimApropriacao.Value;
                dataPagamento = calculoAdministracao.DataPagamento.Value;
                valorAcumulado = calculoAdministracao.ValorAcumulado.Value;

                if (tabelaTaxaAdministracao.EscalonaProporcional == "S")
                {
                    valorDia = calculaValorDia(taxas, tipoApropriacao, valorPL, baseAno);
                }
                else
                {
                    valorDia = calculaValorDia(taxa, tipoApropriacao, valorPL, baseAno);
                }

                valorAcumulado += valorDia;

                calculoAdministracao.ValorDia = valorDia;
                calculoAdministracao.ValorAcumulado = valorAcumulado;
                calculoAdministracao.ValorCPMFAcumulado = 0;
                #endregion
            }
            else
            {
                //Cria novo registro, calculando as datas de fim de apropriação e de pagamento
                #region Insert
                int ano = data.Year;
                int mes = data.Month;

                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao, (int)idLocal, TipoFeriado.Outros);
                }

                if (tabelaTaxaAdministracao.EscalonaProporcional == "S")
                {
                    valorDia = calculaValorDia(taxas, tipoApropriacao, valorPL, baseAno);
                }
                else
                {
                    valorDia = calculaValorDia(taxa, tipoApropriacao, valorPL, baseAno);
                }

                //Calcula data de pagamento
                int numeroDiasPagamento = tabelaTaxaAdministracao.NumeroDiasPagamento.Value;

                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, (int)idLocal, TipoFeriado.Outros);
                }

                #region Insert em CalculoAdministracao
                calculoAdministracao = new CalculoAdministracaoAssociada();
                calculoAdministracao.IdTabela = idTabela;
                calculoAdministracao.IdCarteira = idCarteira;
                calculoAdministracao.ValorDia = valorDia;
                calculoAdministracao.ValorAcumulado = valorDia;
                calculoAdministracao.DataFimApropriacao = dataFimApropriacao;
                calculoAdministracao.DataPagamento = dataPagamento;
                calculoAdministracao.ValorCPMFDia = 0;
                calculoAdministracao.ValorCPMFAcumulado = 0;
                #endregion
                #endregion
            }

            return calculoAdministracao;
        }

        /// <summary>
        /// Efetua o lancamento
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="valorLancamento"></param> Valor calculado da Taxa Global que deve ser distribuido entre as associadas
        /// <param name="index"></param> Indica qual a posição da taxa associada na fila de distribuição
        /// <param name="valorPagar"></param> Valor devido as taxas de administração por prioridade
        /// <returns></returns>
        public decimal setLancamento(int idCarteira, 
                                     DateTime data, 
                                     decimal valorLancamento, 
                                     int index, 
                                     decimal valorPagar, 
                                     Boolean isAgrupado)
        {

            #region Insert na Liquidacao (valor da provisão) - "Pagamento de Taxa de Administração"
            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
            //

            decimal valor;
            decimal valorTaxa;

            //Verifica qual o valor utilizar se tipo de calculo for %Pl ou Valor Fixo
            TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
            tabelaTaxaAdministracao.LoadByPrimaryKey(this.IdTabela.Value);
            if (tabelaTaxaAdministracao.TipoCalculo == (int)TipoCalculoAdministracao.PercentualPLOuValorFixo && this.ValorAcumulado.Value < tabelaTaxaAdministracao.ValorFixoTotal)
            {
                valorTaxa = tabelaTaxaAdministracao.ValorFixoTotal.Value;
            }
            else
            {
                valorTaxa = this.ValorAcumulado.Value;
            }


            //Verifica valor a ser lancado de acordo com valor calculado pela taxa global
            if (valorTaxa > valorLancamento)
            {
                //se for primeiro item da lista, tem preferencia
                if (index == 0)
                {
                    valor = valorTaxa;
                    valorLancamento = 0;
                }
                else
                {
                    if (isAgrupado)
                    {
                        valor = (valorLancamento * valorTaxa) / valorPagar;

                    }
                    else
                    {
                        valor = valorLancamento;
                        valorLancamento = 0;
                    }
                }
            }
            else
            {

                if (isAgrupado)
                {
                    valor = (valorLancamento * valorTaxa) / valorPagar;
                }
                else
                {
                    valor = valorTaxa;
                    valorLancamento = valorLancamento - valorTaxa;
                }
            }

            //string descricao = "Pagamento de Taxa de Administração";
            string descricao = "Pagamento de " + this.UpToTabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Descricao;
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.DataLancamento = data;
            liquidacao.DataVencimento = this.DataPagamento.Value;
            liquidacao.Descricao = descricao;
            liquidacao.Valor = valor * -1;
            liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
            liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
            liquidacao.IdCliente = idCarteira;
            liquidacao.IdConta = idContaDefault;

            // Taxa Administracao
            if (this.UpToTabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo == 1)
            {
                liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao;
            }
            // Taxa Gestão
            else if (this.UpToTabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo == 2)
            {
                liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao;
            }
            // Taxa Custódia
            else if (this.UpToTabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo == 3)
            {
                liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaCustodia;
            }
            liquidacao.Save();
            #endregion

            return valorLancamento;
        }

        private static decimal calculaValorDia(decimal taxa, int tipoApropriacao, decimal valorPl, int baseAno)
        {
            decimal taxaDia;
            if (tipoApropriacao == (int)TipoApropriacaoAdministracao.Exponencial)
            {
                taxaDia = CalculoFinanceiro.ConverteBaseTaxa(baseAno, 1, taxa);
            }
            else //Linear
            {
                taxaDia = taxa / baseAno;
            }

            //Cálculo do valor da taxa de administração diária
            return Math.Round(taxaDia / 100 * valorPl, 2);

        }

        private static decimal calculaValorDia(List<TabelaEscalonamentoAdm> taxas, int tipoApropriacao, decimal valorPl, int baseAno)
        {
            decimal valorFaixa;
            decimal valorDia = 0;
            decimal taxaDia;
            foreach (TabelaEscalonamentoAdm tabelaEscalonamentoAdm in taxas)
            {
                if (tipoApropriacao == (int)TipoApropriacaoAdministracao.Exponencial)
                {
                    taxaDia = CalculoFinanceiro.ConverteBaseTaxa(baseAno, 1, Convert.ToDecimal(tabelaEscalonamentoAdm.Percentual));
                }
                else //Linear
                {
                    taxaDia = Convert.ToDecimal(tabelaEscalonamentoAdm.Percentual) / baseAno;
                }

                valorFaixa = valorPl - Convert.ToDecimal(tabelaEscalonamentoAdm.FaixaPL);
                valorDia = valorDia + (Math.Round(taxaDia / 100 * valorFaixa, 2));

                valorPl = valorPl - valorFaixa;

            }
            return valorDia;

        }

	}
}
