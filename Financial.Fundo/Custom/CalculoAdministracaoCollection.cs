﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class CalculoAdministracaoCollection : esCalculoAdministracaoCollection
	{
        /// <summary>
        /// Construtor
        /// Cria uma nova CalculoAdministracaoCollection com os dados de CalculoAdministracaoHistoricoCollection
        /// Todos do dados de CalculoAdministracaoHistoricoCollection são copiados com excessão da dataHistorico
        /// </summary>
        /// <param name="calculoAdministracaoHistoricoCollection"></param>
        public CalculoAdministracaoCollection(CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection) 
        {
            for (int i = 0; i < calculoAdministracaoHistoricoCollection.Count; i++) {
                //
                CalculoAdministracao c = new CalculoAdministracao();

                // Para cada Coluna de CalculoAdministracaoHistorico copia para CalculoAdministracao
                foreach (esColumnMetadata colCalculoAdministracaoHistorico in calculoAdministracaoHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colCalculoAdministracaoHistorico.PropertyName != CalculoAdministracaoHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colCalculoAdministracao = c.es.Meta.Columns.FindByPropertyName(colCalculoAdministracaoHistorico.PropertyName);
                        if (calculoAdministracaoHistoricoCollection[i].GetColumn(colCalculoAdministracaoHistorico.Name) != null) {
                            c.SetColumn(colCalculoAdministracao.Name, calculoAdministracaoHistoricoCollection[i].GetColumn(colCalculoAdministracaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(c);
            }
        }

        /// <summary>
        /// Carrega o objeto CalculoAdministracaoCollection com todos os campos de CalculoAdministracao.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="dataFimApropriacao">filtra provisões já vencidas (inclusive vcto em dia não útil)</param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaCalculoAdministracaoVencidas(int idCarteira, DateTime dataFimApropriacao)
        {
            CalculoAdministracaoQuery calculoAdministracaoQuery = new CalculoAdministracaoQuery("C");
            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");

            calculoAdministracaoQuery.Where(calculoAdministracaoQuery.IdCarteira.Equal(idCarteira),
                                       calculoAdministracaoQuery.DataFimApropriacao.LessThanOrEqual(dataFimApropriacao));
            calculoAdministracaoQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(calculoAdministracaoQuery.IdTabela == tabelaTaxaAdministracaoQuery.IdTabela);

            bool retorno = this.Load(calculoAdministracaoQuery);

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto CalculoAdministracaoCollection com todos os campos de CalculoAdministracao.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaCalculoAdministracaoCompleta(int idCarteira)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCarteira == idCarteira);
            this.Query.Load();
        }
        
        /// <summary>
        /// Deleta todos os registros de cálculo da carteira passada.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaCalculoAdministracao(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTabela)
                 .Where(this.Query.IdCarteira == idCarteira);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Insere novas registros na CalculoAdministracao a partir do histórico
        /// (CalculoAdministracaoHistorico) da data informada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void InsereCalculoAdministracaoHistorico(int idCarteira, DateTime data)
        {
            this.QueryReset();
            CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();
            calculoAdministracaoHistoricoCollection.BuscaCalculoAdministracaoHistoricoCompleta(idCarteira, data);
            for (int i = 0; i < calculoAdministracaoHistoricoCollection.Count; i++)
            {
                CalculoAdministracaoHistorico calculoAdministracaoHistorico = calculoAdministracaoHistoricoCollection[i];

                // Copia para CalculoAdministracao
                CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
                calculoAdministracao.IdTabela = calculoAdministracaoHistorico.IdTabela;
                calculoAdministracao.IdCarteira = calculoAdministracaoHistorico.IdCarteira;
                calculoAdministracao.ValorDia = calculoAdministracaoHistorico.ValorDia;
                calculoAdministracao.ValorAcumulado = calculoAdministracaoHistorico.ValorAcumulado;
                calculoAdministracao.DataFimApropriacao = calculoAdministracaoHistorico.DataFimApropriacao;
                calculoAdministracao.DataPagamento = calculoAdministracaoHistorico.DataPagamento;
                calculoAdministracao.ValorCPMFDia = calculoAdministracaoHistorico.ValorCPMFDia;
                calculoAdministracao.ValorCPMFAcumulado = calculoAdministracaoHistorico.ValorCPMFAcumulado;
                //
                this.AttachEntity(calculoAdministracao);
            }

            this.Save();
        }
	}
}
