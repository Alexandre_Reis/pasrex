using System;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
    public partial class PrejuizoFundoHistoricoCollection : esPrejuizoFundoHistoricoCollection
    {
        public void DeletaPrejuizoFundoHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.query.DataHistorico, this.query.IdCliente, this.query.IdCarteira, this.query.Data)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
    }
}