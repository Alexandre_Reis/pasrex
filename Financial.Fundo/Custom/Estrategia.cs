/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/02/2011 17:56:48
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
	public partial class Estrategia : esEstrategia
	{
        public bool BuscaPorDescricao(string descricao)
        {
            this.QueryReset();
            this.Query.es.Top = 1;
            this.Query.Where(this.query.Descricao == descricao);
            this.Query.Load();
            return this.es.HasData;
        }
        public void Save(string descricao, bool deferSave)
        {
            this.Descricao = descricao;

            if (!deferSave)
            {
                this.Save();
            }
        }
	}
}
