﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class CalculoPerformanceHistoricoCollection : esCalculoPerformanceHistoricoCollection
	{
        /// <summary>
        /// Carrega o objeto CalculoAdministracaoHistoricoCollection com todos os campos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaCalculoPerformanceHistoricoCompleta(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCarteira == idCarteira);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Deleta todas as posições históricas com data >= que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaCalculoPerformanceHistoricoDataHistoricoMaiorIgual(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdTabela, this.query.DataHistorico)
                    .Where(this.Query.IdCarteira == idCarteira,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

	}
}
