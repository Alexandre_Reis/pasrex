﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/05/2015 13:26:36
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.Fundo.Enums;
using Financial.Swap;
using Financial.BMF;
using Financial.Util;
using Financial.Common.Enums;

namespace Financial.Fundo
{
	public partial class MediaMovel : esMediaMovel
	{
        public void CalculaMediaMovel(int idCliente, DateTime data)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            CalculaMediaMovel(cliente, data);
        }

        public void CalculaMediaMovel(Cliente cliente, DateTime data)
        {
            MediaMovelCollection mediaMovelColl = new MediaMovelCollection();
            HistoricoCotaCollection historicoCotaColl;

            PosicaoSwapCollection posicaoSwapColl;
            PosicaoSwapQuery posicaoSwapQuery;
            PosicaoSwapHistoricoCollection posicaoSwapHistoricoColl;
            PosicaoSwapHistoricoQuery posicaoSwapHistoricoQuery;
                                                
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoColl;
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery;
            PosicaoFundoCollection posicaoFundoColl;
            PosicaoFundoQuery posicaoFundoQuery;
            PosicaoFundo posicaoFundoAux;

            PosicaoBMFHistoricoCollection posicaoBMFHistoricoColl;
            PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery;
            PosicaoBMFCollection posicaoBMFColl;
            PosicaoBMFQuery posicaoBMFQuery;
            
            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoColl;
            PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery;
            PosicaoBolsaCollection posicaoBolsaColl;
            PosicaoBolsaQuery posicaoBolsaQuery;
            PosicaoBolsa posicaoBolsaAux;

            CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");

            List<PosicaoFundoHistorico> lstPosicaoFundoHistorico = new List<PosicaoFundoHistorico>();
            List<PosicaoSwapHistorico> lstPosicaoSwapHistorico = new List<PosicaoSwapHistorico>();
            List<PosicaoBolsaHistorico> lstPosicaoBolsaHistorico = new List<PosicaoBolsaHistorico>();
            List<PosicaoBMFHistorico> lstPosicaoBMFHistorico = new List<PosicaoBMFHistorico>();

            int idCliente = cliente.IdCliente.Value;
            int idLocalFeriado = cliente.IdLocal.Value; 
            decimal divisor = 0;
            DateTime dataAtual = data;
            DateTime dataAux;

            #region Deleta Registro Antigo
            mediaMovelColl.DeletaMediaMovel(idCliente, data);
            #endregion

            #region Carrega Posição Fechamento
            //Fundo
            posicaoFundoColl = new PosicaoFundoCollection();
            posicaoFundoColl.Query.Select(posicaoFundoColl.Query.ValorBruto,
                                          posicaoFundoColl.Query.ValorLiquido, 
                                          posicaoFundoColl.Query.IdPosicao);
            posicaoFundoColl.Query.Where(posicaoFundoColl.Query.IdCliente.Equal(idCliente));
            posicaoFundoColl.Query.Load();

            //Swap
            posicaoSwapColl = new PosicaoSwapCollection();
            posicaoSwapColl.Query.Select(posicaoSwapColl.Query.Saldo, posicaoSwapColl.Query.IdPosicao);
            posicaoSwapColl.Query.Where(posicaoSwapColl.Query.IdCliente.Equal(idCliente));
            posicaoSwapColl.Query.Load();

            //Bolsa
            posicaoBolsaColl = new PosicaoBolsaCollection();
            posicaoBolsaColl.Query.Select(posicaoBolsaColl.Query.ValorMercado.Sum(),
                                          posicaoBolsaColl.Query.CdAtivoBolsa,
                                          posicaoBolsaColl.Query.IdAgente);
            posicaoBolsaColl.Query.Where(posicaoBolsaColl.Query.IdCliente.Equal(idCliente));
            posicaoBolsaColl.Query.GroupBy(posicaoBolsaColl.Query.CdAtivoBolsa,
                                           posicaoBolsaColl.Query.IdAgente);
            posicaoBolsaColl.Query.Load();

            //BMF
            posicaoBMFColl = new PosicaoBMFCollection();
            posicaoBMFColl.Query.Select(posicaoBMFColl.Query.ValorMercado.Sum(),
                                          posicaoBMFColl.Query.CdAtivoBMF,
                                          posicaoBMFColl.Query.Serie,
                                          posicaoBMFColl.Query.IdAgente);
            posicaoBMFColl.Query.Where(posicaoBMFColl.Query.IdCliente.Equal(idCliente));
            posicaoBMFColl.Query.GroupBy(posicaoBMFColl.Query.CdAtivoBMF,
                                         posicaoBMFColl.Query.Serie,
                                         posicaoBMFColl.Query.IdAgente);
            posicaoBMFColl.Query.Load();
            #endregion

            dataAux = dataAtual.AddYears(-1);
            if (!Calendario.IsDiaUtil(dataAux))
                dataAux = Calendario.AdicionaDiaUtil(dataAux, 1, idLocalFeriado, idLocalFeriado == LocalFeriadoFixo.Brasil ? TipoFeriado.Brasil : TipoFeriado.Outros);

            int numeroDiasUteisAno = Calendario.NumeroDias(dataAux, dataAtual, idLocalFeriado, idLocalFeriado == LocalFeriadoFixo.Brasil ? TipoFeriado.Brasil : TipoFeriado.Outros);

            #region PatrimLiquido12meses
            historicoCotaColl = new HistoricoCotaCollection();
            historicoCotaColl.Query.Select(historicoCotaColl.Query.PLFechamento.Sum(), historicoCotaColl.Query.IdCarteira.Count().As("Count"));
            historicoCotaColl.Query.Where(historicoCotaColl.Query.IdCarteira.Equal(idCliente) &
                                          historicoCotaColl.Query.Data.Between(dataAux, dataAtual));

            if (historicoCotaColl.Query.Load())
            {
                int aux = Convert.ToInt32(historicoCotaColl[0].GetColumn("Count"));
                divisor = aux < numeroDiasUteisAno ? aux : numeroDiasUteisAno;

                if (aux > 0) // O count da collection é igual a 1, por causa das funcoes executadas Sum e Count
                {
                    MediaMovel mediaMovel = mediaMovelColl.AddNew();
                    mediaMovel.MediaMovel = Utilitario.Truncate(historicoCotaColl[0].PLFechamento.Value / divisor, 2);
                    mediaMovel.TipoMediaMovel = (int)TipoMediaMovelFixo.PatrimLiquido12meses;
                    mediaMovel.DataAtual = data;
                    mediaMovel.IdCliente = idCliente;
                    mediaMovel.IdPosicao = null;
                    mediaMovel.ChaveComposta = null;
                }
            }
            #endregion

            dataAux = Calendario.SubtraiDiaUtil(dataAtual, 40, idLocalFeriado, idLocalFeriado == LocalFeriadoFixo.Brasil ? TipoFeriado.Brasil : TipoFeriado.Outros);
            #region PatrBruto40RV
            //Carrega Bolsa
            posicaoBolsaHistoricoColl = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoColl.Query.Select(posicaoBolsaHistoricoColl.Query.ValorMercado.Sum());
            posicaoBolsaHistoricoColl.Query.Where(posicaoBolsaHistoricoColl.Query.IdCliente.Equal(idCliente) &
                                                 posicaoBolsaHistoricoColl.Query.DataHistorico.Between(dataAux, dataAtual.AddDays(-1)));

            decimal patrimonioBolsa = 0;
            if (posicaoBolsaHistoricoColl.Query.Load())
                patrimonioBolsa = posicaoBolsaHistoricoColl[0].ValorMercado.GetValueOrDefault(0);

            posicaoBolsaAux = new PosicaoBolsa();
            patrimonioBolsa += posicaoBolsaAux.RetornaValorMercado(idCliente);

            //Carrega Fundo
            posicaoFundoHistoricoColl = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("Fundo");            
            carteiraQuery = new CarteiraQuery("carteira");

            posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.ValorBruto.Sum());
            posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(posicaoFundoHistoricoQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente) &
                                                 carteiraQuery.TipoCarteira.Equal((byte)TipoCarteiraFundo.RendaVariavel) &
                                                 posicaoFundoHistoricoQuery.DataHistorico.Between(dataAux, dataAtual.AddDays(-1)));

            decimal patrimonioFundo = 0;
            if (posicaoFundoHistoricoColl.Load(posicaoFundoHistoricoQuery))
                patrimonioFundo = posicaoFundoHistoricoColl[0].ValorBruto.GetValueOrDefault(0);

            posicaoFundoAux = new PosicaoFundo();
            patrimonioFundo += posicaoFundoAux.RetornaValorBruto(idCliente);

            if (patrimonioBolsa + patrimonioFundo > 0)
            {
                historicoCotaColl = new HistoricoCotaCollection();
                historicoCotaColl.Query.Where(historicoCotaColl.Query.IdCarteira.Equal(idCliente) &
                                              historicoCotaColl.Query.Data.Between(dataAux, dataAtual));

                if (historicoCotaColl.Query.Load())
                {
                    divisor = 40 < historicoCotaColl.Count ? 40 : historicoCotaColl.Count;
                    divisor = divisor == 0 ? 1 : divisor;

                    MediaMovel mediaMovel = mediaMovelColl.AddNew();
                    mediaMovel.MediaMovel = Utilitario.Truncate((patrimonioBolsa + patrimonioFundo) / divisor, 2);
                    mediaMovel.TipoMediaMovel = (int)TipoMediaMovelFixo.PatrBruto40RV;
                    mediaMovel.DataAtual = data;
                    mediaMovel.IdCliente = cliente.IdCliente.Value;
                    mediaMovel.IdPosicao = null;
                    mediaMovel.ChaveComposta = null;
                }
            }
            #endregion

            divisor = 90;
            dataAux = Calendario.SubtraiDiaUtil(dataAtual, 90, idLocalFeriado, idLocalFeriado == LocalFeriadoFixo.Brasil ? TipoFeriado.Brasil : TipoFeriado.Outros);

            if (cliente.DataImplantacao.Value > dataAux)
            {
                dataAux = cliente.DataImplantacao.Value;
                divisor = Calendario.NumeroDias(dataAux, dataAtual, idLocalFeriado, idLocalFeriado == LocalFeriadoFixo.Brasil ? TipoFeriado.Brasil : TipoFeriado.Outros);
                divisor++;
            }

            divisor = divisor == 0 ? 1 : divisor;

            #region PatrLiqSWAP_90_DU
            posicaoSwapHistoricoQuery = new PosicaoSwapHistoricoQuery("SwapHistorico");
            posicaoSwapHistoricoColl = new PosicaoSwapHistoricoCollection();
            posicaoSwapQuery = new PosicaoSwapQuery("Swap");

            posicaoSwapHistoricoQuery.Select(posicaoSwapHistoricoQuery.Saldo.Sum(),
                                             posicaoSwapHistoricoQuery.IdPosicao);
            posicaoSwapHistoricoQuery.InnerJoin(posicaoSwapQuery).On(posicaoSwapHistoricoQuery.IdPosicao.Equal(posicaoSwapQuery.IdPosicao));
            posicaoSwapHistoricoQuery.Where(posicaoSwapHistoricoQuery.IdCliente.Equal(idCliente) &
                                            posicaoSwapHistoricoQuery.DataHistorico.Between(dataAux, dataAtual.AddDays(-1)));
            posicaoSwapHistoricoQuery.GroupBy(posicaoSwapHistoricoQuery.IdPosicao);

            
            if (posicaoSwapHistoricoColl.Load(posicaoSwapHistoricoQuery))
                lstPosicaoSwapHistorico = (List<PosicaoSwapHistorico>)posicaoSwapHistoricoColl; 

            foreach (PosicaoSwap posicaoSwap in posicaoSwapColl)
            {
                List<PosicaoSwapHistorico> lstAuxiliar = lstPosicaoSwapHistorico.FindAll(delegate(PosicaoSwapHistorico x) { return x.IdPosicao == posicaoSwap.IdPosicao.Value; });

                decimal saldoHistorico = 0;
                foreach (PosicaoSwapHistorico posicaoHistorico in lstAuxiliar)                
                    saldoHistorico = posicaoHistorico.Saldo.Value;

                decimal saldo = posicaoSwap.Saldo.Value + saldoHistorico;

                MediaMovel mediaMovel = mediaMovelColl.AddNew();
                mediaMovel.MediaMovel = Utilitario.Truncate(saldo / divisor, 2);
                mediaMovel.TipoMediaMovel = (int)TipoMediaMovelFixo.PatrLiqSWAP_90_DU;
                mediaMovel.DataAtual = data;
                mediaMovel.IdCliente = idCliente;
                mediaMovel.IdPosicao = posicaoSwap.IdPosicao.Value;
                mediaMovel.ChaveComposta = null;
            }         
            #endregion

            #region PatrLiqBolsa_90_DU
            posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("BolsaHistorico"); 
            posicaoBolsaHistoricoColl = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaQuery = new PosicaoBolsaQuery("Bolsa");

            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                            posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                            posicaoBolsaHistoricoQuery.IdAgente);
            posicaoBolsaHistoricoQuery.InnerJoin(posicaoBolsaQuery).On(posicaoBolsaHistoricoQuery.CdAtivoBolsa.Equal(posicaoBolsaQuery.CdAtivoBolsa) &
                                                                   posicaoBolsaHistoricoQuery.IdAgente.Equal(posicaoBolsaQuery.IdAgente) &
                                                                   posicaoBolsaHistoricoQuery.IdCliente.Equal(posicaoBolsaQuery.IdCliente));
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCliente) &
                                             posicaoBolsaHistoricoQuery.DataHistorico.Between(dataAux, dataAtual.AddDays(-1)));
            posicaoBolsaHistoricoQuery.GroupBy(posicaoBolsaHistoricoQuery.CdAtivoBolsa,
                                               posicaoBolsaHistoricoQuery.IdAgente);

            if (posicaoBolsaHistoricoColl.Load(posicaoBolsaHistoricoQuery))
                lstPosicaoBolsaHistorico = (List<PosicaoBolsaHistorico>)posicaoBolsaHistoricoColl;

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaColl)
            {
                List<PosicaoBolsaHistorico> lstAuxiliar = lstPosicaoBolsaHistorico.FindAll(delegate(PosicaoBolsaHistorico x) { return x.CdAtivoBolsa.Equals(posicaoBolsa.CdAtivoBolsa) && x.IdAgente == posicaoBolsa.IdAgente.Value; });

                decimal valorMercadoHistorico = 0;
                foreach (PosicaoBolsaHistorico posicaoHistorico in lstAuxiliar)                
                    valorMercadoHistorico = posicaoHistorico.ValorMercado.Value;

                decimal valorMercado = posicaoBolsa.ValorMercado.Value + valorMercadoHistorico;

                MediaMovel mediaMovel = mediaMovelColl.AddNew();
                mediaMovel.MediaMovel = Utilitario.Truncate(valorMercado / divisor, 2);
                mediaMovel.TipoMediaMovel = (int)TipoMediaMovelFixo.PatrLiqBOLSA_90_DU;
                mediaMovel.DataAtual = data;
                mediaMovel.IdCliente = idCliente;
                mediaMovel.IdPosicao = null;
                mediaMovel.ChaveComposta = posicaoBolsa.CdAtivoBolsa.Trim().ToUpper() + "|" + posicaoBolsa.IdAgente.Value;
            }            
            #endregion

            #region PatrLiqBMF_90_DU
            posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("BMFHistorico");
            posicaoBMFHistoricoColl = new PosicaoBMFHistoricoCollection();
            posicaoBMFQuery = new PosicaoBMFQuery("BMF");

            posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.ValorMercado.Sum(),
                                            posicaoBMFHistoricoQuery.CdAtivoBMF,
                                            posicaoBMFHistoricoQuery.Serie,
                                            posicaoBMFHistoricoQuery.IdAgente);
            posicaoBMFHistoricoQuery.InnerJoin(posicaoBMFQuery).On(posicaoBMFHistoricoQuery.CdAtivoBMF.Equal(posicaoBMFQuery.CdAtivoBMF) &
                                                                   posicaoBMFHistoricoQuery.IdAgente.Equal(posicaoBMFQuery.IdAgente) &
                                                                   posicaoBMFHistoricoQuery.Serie.Equal(posicaoBMFQuery.Serie) &
                                                                   posicaoBMFHistoricoQuery.IdCliente.Equal(posicaoBMFQuery.IdCliente));
            posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCliente) &
                                             posicaoBMFHistoricoQuery.DataHistorico.Between(dataAux, dataAtual.AddDays(-1)));
            posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.CdAtivoBMF,
                                             posicaoBMFHistoricoQuery.Serie,
                                             posicaoBMFHistoricoQuery.IdAgente);

            if (posicaoBMFHistoricoColl.Load(posicaoBMFHistoricoQuery))
                lstPosicaoBMFHistorico = (List<PosicaoBMFHistorico>)posicaoBMFHistoricoColl;

            foreach (PosicaoBMF posicaoBMF in posicaoBMFColl)
            {
                List<PosicaoBMFHistorico> lstAuxiliar = lstPosicaoBMFHistorico.FindAll(delegate(PosicaoBMFHistorico x) { return x.CdAtivoBMF.Equals(posicaoBMF.CdAtivoBMF) && x.Serie.Equals(posicaoBMF.Serie) && x.IdAgente == posicaoBMF.IdAgente.Value; });

                decimal valorMercadoHistorico = 0;
                foreach (PosicaoBMFHistorico posicaoHistorico in lstAuxiliar)                
                    valorMercadoHistorico = posicaoHistorico.ValorMercado.Value;

                decimal valorMercado = posicaoBMF.ValorMercado.Value + valorMercadoHistorico;

                MediaMovel mediaMovel = mediaMovelColl.AddNew();
                mediaMovel.MediaMovel = Utilitario.Truncate(valorMercado / divisor, 2);
                mediaMovel.TipoMediaMovel = (int)TipoMediaMovelFixo.PatrLiqBMF_90_DU;
                mediaMovel.DataAtual = data;
                mediaMovel.IdCliente = idCliente;
                mediaMovel.IdPosicao = null;
                mediaMovel.ChaveComposta = posicaoBMF.CdAtivoBMF.Trim().ToUpper() + "|" + posicaoBMF.Serie.Trim().ToUpper() + "|" + posicaoBMF.IdAgente.Value;
            }  
            #endregion
            
            #region PatrLiqFundo_90_DU
            posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("FundoHistorico");
            posicaoFundoHistoricoColl = new PosicaoFundoHistoricoCollection();
            posicaoFundoQuery = new PosicaoFundoQuery("Fundo");

            posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.ValorLiquido.Sum(),
                                             posicaoFundoHistoricoQuery.IdPosicao);
            posicaoFundoHistoricoQuery.InnerJoin(posicaoFundoQuery).On(posicaoFundoHistoricoQuery.IdPosicao.Equal(posicaoFundoQuery.IdPosicao));
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente) &
                                            posicaoFundoHistoricoQuery.DataHistorico.Between(dataAux, dataAtual.AddDays(-1)));
            posicaoFundoHistoricoQuery.GroupBy(posicaoFundoHistoricoQuery.IdPosicao);


            if (posicaoFundoHistoricoColl.Load(posicaoFundoHistoricoQuery))
                lstPosicaoFundoHistorico = (List<PosicaoFundoHistorico>)posicaoFundoHistoricoColl;

            foreach (PosicaoFundo posicaoFundo in posicaoFundoColl)
            {
                List<PosicaoFundoHistorico> lstAuxiliar = lstPosicaoFundoHistorico.FindAll(delegate(PosicaoFundoHistorico x) { return x.IdPosicao == posicaoFundo.IdPosicao.Value; });

                decimal valorLiquidoHistorico = 0;
                foreach (PosicaoFundoHistorico posicaoHistorico in lstAuxiliar)                
                    valorLiquidoHistorico = posicaoHistorico.ValorLiquido.Value;

                decimal saldo = posicaoFundo.ValorLiquido.Value + valorLiquidoHistorico;

                MediaMovel mediaMovel = mediaMovelColl.AddNew();
                mediaMovel.MediaMovel = Utilitario.Truncate(saldo / divisor, 2);
                mediaMovel.TipoMediaMovel = (int)TipoMediaMovelFixo.PatrLiqFundo_90_DU;
                mediaMovel.DataAtual = data;
                mediaMovel.IdCliente = idCliente;
                mediaMovel.IdPosicao = posicaoFundo.IdPosicao.Value;
                mediaMovel.ChaveComposta = null;
            }     
            #endregion

            mediaMovelColl.Save();
        }
	}
}

