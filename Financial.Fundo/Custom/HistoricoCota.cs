﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Exceptions;
using log4net;
using Financial.Fundo.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.BMF;
using Financial.Swap;
using Financial.RendaFixa;
using Financial.InvestidorCotista;
using Financial.ContaCorrente;
using Financial.Investidor;
using Financial.Common;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Bolsa.Enums;
using Financial.ContaCorrente.Enums;
using System.IO;
using Financial.Investidor.Exceptions;
using Financial.Interfaces.Import.Fundo;
using Financial.CRM;
using System.Globalization;
using Financial.BMF.Enums;
using Financial.RendaFixa.Enums;
using Financial.Tributo.Enums;
using Financial.Util.Enums;
using Financial.Fundo.Debug.HistoricoCota;
using Financial.Fundo.Controller;
using System.Collections;
using Financial.Bolsa.Exceptions;

namespace Financial.Fundo
{
    public partial class HistoricoCota : esHistoricoCota
    {
        public enum TiposMercados
        {
            Acao = 1,
            OpcaoAcao = 2,
            TermoAcao = 3,
            BTC = 4,
            CustosBTC = 5,
            Proventos = 6,
            FuturoBMF = 10,
            OpcaoBMF = 11,
            Fundo = 20,
            FundoTributo = 21,
            RendaFixaPublico = 30,
            RendaFixaPublicoTributos = 31,
            RendaFixaPrivado = 32,
            RendaFixaPrivadoTributos = 33,
            TaxaAdm = 50,
            TaxaGestao = 51,
            Provisoes = 55,
            Diferimento = 56,
            Outros = 57,
            TaxaPfee = 60,
            TaxaPfeeLancamento = 61,
            Aplicacao = 100,
            Resgate = 101
        }

        public class AnaliseProvisao
        {
            private int idTabela;
            public int IdTabela
            {
                get { return idTabela; }
                set { idTabela = value; }
            }

            private decimal valor;
            public decimal Valor
            {
                get { return valor; }
                set { valor = value; }
            }

            private string descricao;
            public string Descricao
            {
                get { return descricao; }
                set { descricao = value; }
            }
        }

        public class AnaliseResultados
        {
            private int tipoMercado;
            public int TipoMercado
            {
                get { return tipoMercado; }
                set { tipoMercado = value; }
            }

            private string descricaoMercado;
            public string DescricaoMercado
            {
                get { return descricaoMercado; }
                set { descricaoMercado = value; }
            }

            private string descricaoAtivo;
            public string DescricaoAtivo
            {
                get { return descricaoAtivo; }
                set { descricaoAtivo = value; }
            }

            private decimal resultadoPeriodo;
            public decimal ResultadoPeriodo
            {
                get { return resultadoPeriodo; }
                set { resultadoPeriodo = value; }
            }

            private decimal percentualPL;
            public decimal PercentualPL
            {
                get { return percentualPL; }
                set { percentualPL = value; }
            }
        }

        public List<AnaliseResultados> RetornaAnaliseResultados(int idCarteira, DateTime data, DateTime dataHistorico)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            PortfolioPadrao portfolioPadrao = new PortfolioPadrao();
            portfolioPadrao = portfolioPadrao.RetornaPortfoliosVigentes(data);

            bool historico = data < cliente.DataDia.Value;
            bool lancaTributos = (cliente.DescontaTributoPL == (byte)DescontoPLCliente.PLComDesconto);
            bool carteiraConsolidadora = false;

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorPatrimonioDia(idCarteira, dataHistorico);
            decimal valorPLAnterior = historicoCota.PLFechamento.Value;

            List<AnaliseResultados> lista = new List<AnaliseResultados>();

            #region Bolsa (Ações)
            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim());
            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                        posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista,
                                                                                                             TipoMercadoBolsa.Imobiliario),
                                                        posicaoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoBolsaHistoricoCollection.Query.es.Distinct = true;
            posicaoBolsaHistoricoCollection.Query.Load();

            List<string> listaBolsa = new List<string>();

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                listaBolsa.Add(posicaoBolsaHistorico.CdAtivoBolsa);
            }

            if (historico)
            {
                posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                   posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista,
                                                                                                        TipoMercadoBolsa.Imobiliario),
                                                   posicaoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                   posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.es.Distinct = true;
                posicaoBolsaHistoricoCollection.Query.Load();

                foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                {
                    if (!listaBolsa.Contains(posicaoBolsaHistorico.CdAtivoBolsa))
                    {
                        listaBolsa.Add(posicaoBolsaHistorico.CdAtivoBolsa);
                    }
                }
            }
            else
            {
                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa.Trim());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCarteira),
                                                   posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista,
                                                                                               TipoMercadoBolsa.Imobiliario),
                                                   posicaoBolsaCollection.Query.Quantidade.NotEqual(0));
                posicaoBolsaCollection.Query.es.Distinct = true;
                posicaoBolsaCollection.Query.Load();

                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                {
                    if (!listaBolsa.Contains(posicaoBolsa.CdAtivoBolsa))
                    {
                        listaBolsa.Add(posicaoBolsa.CdAtivoBolsa);
                    }
                }
            }

            OperacaoBolsaCollection operacaoBolsaCollectionLista = new OperacaoBolsaCollection();
            operacaoBolsaCollectionLista.Query.Select(operacaoBolsaCollectionLista.Query.CdAtivoBolsa.Trim());
            operacaoBolsaCollectionLista.Query.Where(operacaoBolsaCollectionLista.Query.IdCliente.Equal(idCarteira),
                                                    operacaoBolsaCollectionLista.Query.Data.LessThanOrEqual(data),
                                                    operacaoBolsaCollectionLista.Query.Data.GreaterThan(dataHistorico),
                                                    operacaoBolsaCollectionLista.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista,
                                                                                                      TipoMercadoBolsa.Imobiliario));
            operacaoBolsaCollectionLista.Query.es.Distinct = true;
            operacaoBolsaCollectionLista.Query.Load();

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollectionLista)
            {
                if (!listaBolsa.Contains(operacaoBolsa.CdAtivoBolsa))
                {
                    listaBolsa.Add(operacaoBolsa.CdAtivoBolsa);
                }
            }

            foreach (string cdAtivoBolsa in listaBolsa)
            {
                decimal resultado = 0;
                OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
                operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.ValorLiquido.Sum(),
                                                     operacaoBolsaCollection.Query.Valor.Sum(),
                                                     operacaoBolsaCollection.Query.TipoOperacao);
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCarteira),
                                                    operacaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                    operacaoBolsaCollectionLista.Query.Data.LessThanOrEqual(data),
                                                    operacaoBolsaCollectionLista.Query.Data.GreaterThan(dataHistorico));
                operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.TipoOperacao);
                operacaoBolsaCollection.Query.Load();

                foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
                {
                    if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Compra ||
                        operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade ||
                        operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Deposito)
                    {
                        if (operacaoBolsa.ValorLiquido.Value != 0)
                        {
                            resultado -= operacaoBolsa.ValorLiquido.Value;
                        }
                        else
                        {
                            resultado -= operacaoBolsa.Valor.Value;
                        }
                    }
                    else if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Venda ||
                             operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.VendaDaytrade ||
                             operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Retirada)
                    {
                        if (operacaoBolsa.ValorLiquido.Value != 0)
                        {
                            resultado += operacaoBolsa.ValorLiquido.Value;
                        }
                        else
                        {
                            resultado += operacaoBolsa.Valor.Value;
                        }
                    }
                }

                PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
                posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCarteira),
                                                  posicaoBolsaHistorico.Query.DataHistorico.Equal(dataHistorico),
                                                  posicaoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoBolsaHistorico.Query.Load();

                if (posicaoBolsaHistorico.es.HasData && posicaoBolsaHistorico.ValorMercado.HasValue)
                {
                    resultado -= posicaoBolsaHistorico.ValorMercado.Value;
                }

                if (historico)
                {
                    posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                    posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
                    posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCarteira),
                                                      posicaoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                      posicaoBolsaHistorico.Query.DataHistorico.Equal(data));
                    posicaoBolsaHistorico.Query.Load();

                    if (posicaoBolsaHistorico.es.HasData && posicaoBolsaHistorico.ValorMercado.HasValue)
                    {
                        resultado += posicaoBolsaHistorico.ValorMercado.Value;
                    }
                }
                else
                {
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCarteira),
                                             posicaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.es.HasData && posicaoBolsa.ValorMercado.HasValue)
                    {
                        resultado += posicaoBolsa.ValorMercado.Value;
                    }
                }


                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = (int)TiposMercados.Acao;
                analiseResultados.DescricaoAtivo = cdAtivoBolsa;
                analiseResultados.DescricaoMercado = "Ações";
                analiseResultados.PercentualPL = Math.Round(resultado / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = resultado;

                lista.Add(analiseResultados);
            }
            #endregion

            #region Bolsa (Opções de Ações)
            posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim());
            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                        posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                        posicaoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoBolsaHistoricoCollection.Query.es.Distinct = true;
            posicaoBolsaHistoricoCollection.Query.Load();

            listaBolsa = new List<string>();

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                listaBolsa.Add(posicaoBolsaHistorico.CdAtivoBolsa);
            }

            if (historico)
            {
                posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                   posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                   posicaoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                   posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.es.Distinct = true;
                posicaoBolsaHistoricoCollection.Query.Load();

                foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                {
                    if (!listaBolsa.Contains(posicaoBolsaHistorico.CdAtivoBolsa))
                    {
                        listaBolsa.Add(posicaoBolsaHistorico.CdAtivoBolsa);
                    }
                }
            }
            else
            {
                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa.Trim());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCarteira),
                                                   posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                   posicaoBolsaCollection.Query.Quantidade.NotEqual(0));
                posicaoBolsaCollection.Query.es.Distinct = true;
                posicaoBolsaCollection.Query.Load();

                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                {
                    if (!listaBolsa.Contains(posicaoBolsa.CdAtivoBolsa))
                    {
                        listaBolsa.Add(posicaoBolsa.CdAtivoBolsa);
                    }
                }
            }

            operacaoBolsaCollectionLista = new OperacaoBolsaCollection();
            operacaoBolsaCollectionLista.Query.Select(operacaoBolsaCollectionLista.Query.CdAtivoBolsa.Trim());
            operacaoBolsaCollectionLista.Query.Where(operacaoBolsaCollectionLista.Query.IdCliente.Equal(idCarteira),
                                                    operacaoBolsaCollectionLista.Query.Data.LessThanOrEqual(data),
                                                    operacaoBolsaCollectionLista.Query.Data.GreaterThan(dataHistorico),
                                                    operacaoBolsaCollectionLista.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
            operacaoBolsaCollectionLista.Query.es.Distinct = true;
            operacaoBolsaCollectionLista.Query.Load();

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollectionLista)
            {
                if (!listaBolsa.Contains(operacaoBolsa.CdAtivoBolsa))
                {
                    listaBolsa.Add(operacaoBolsa.CdAtivoBolsa);
                }
            }

            foreach (string cdAtivoBolsa in listaBolsa)
            {
                decimal resultado = 0;
                OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
                operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.ValorLiquido.Sum(),
                                                     operacaoBolsaCollection.Query.Valor.Sum(),
                                                     operacaoBolsaCollection.Query.TipoOperacao);
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCarteira),
                                                    operacaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                    operacaoBolsaCollection.Query.Data.LessThanOrEqual(data),
                                                    operacaoBolsaCollection.Query.Data.GreaterThan(dataHistorico));
                operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.TipoOperacao);
                operacaoBolsaCollection.Query.Load();

                foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
                {
                    if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Compra ||
                        operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade ||
                        operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Deposito)
                    {
                        if (operacaoBolsa.ValorLiquido.Value != 0)
                        {
                            resultado -= operacaoBolsa.ValorLiquido.Value;
                        }
                        else
                        {
                            resultado -= operacaoBolsa.Valor.Value;
                        }
                    }
                    else if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Venda ||
                             operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.VendaDaytrade ||
                             operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Retirada)
                    {
                        if (operacaoBolsa.ValorLiquido.Value != 0)
                        {
                            resultado += operacaoBolsa.ValorLiquido.Value;
                        }
                        else
                        {
                            resultado += operacaoBolsa.Valor.Value;
                        }
                    }
                }

                PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
                posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCarteira),
                                                  posicaoBolsaHistorico.Query.DataHistorico.Equal(dataHistorico),
                                                  posicaoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoBolsaHistorico.Query.Load();

                if (posicaoBolsaHistorico.es.HasData && posicaoBolsaHistorico.ValorMercado.HasValue)
                {
                    resultado -= posicaoBolsaHistorico.ValorMercado.Value;
                }

                if (historico)
                {
                    posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                    posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
                    posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCarteira),
                                                      posicaoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                      posicaoBolsaHistorico.Query.DataHistorico.Equal(data));
                    posicaoBolsaHistorico.Query.Load();

                    if (posicaoBolsaHistorico.es.HasData && posicaoBolsaHistorico.ValorMercado.HasValue)
                    {
                        resultado += posicaoBolsaHistorico.ValorMercado.Value;
                    }
                }
                else
                {
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCarteira),
                                             posicaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.es.HasData && posicaoBolsa.ValorMercado.HasValue)
                    {
                        resultado += posicaoBolsa.ValorMercado.Value;
                    }
                }


                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = (int)TiposMercados.Acao;
                analiseResultados.DescricaoAtivo = cdAtivoBolsa;
                analiseResultados.DescricaoMercado = "Opções de Ações";
                analiseResultados.PercentualPL = valorPLAnterior == 0 ? 0 : Math.Round(resultado / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = resultado;

                lista.Add(analiseResultados);
            }
            #endregion

            #region Proventos (que estão na data "atual" e não estão em d-1 ou estão em d-1 com valor diferente)
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            if (historico)
            {
                LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
                liquidacaoHistoricoCollection.Query.Select(liquidacaoHistoricoCollection.Query.Descricao,
                                                        liquidacaoHistoricoCollection.Query.DataVencimento,
                                                        liquidacaoHistoricoCollection.Query.Origem,
                                                        liquidacaoHistoricoCollection.Query.Valor.Sum());
                liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                       liquidacaoHistoricoCollection.Query.DataHistorico.Equal(data),
                                                       liquidacaoHistoricoCollection.Query.DataVencimento.GreaterThanOrEqual(data),
                                                       liquidacaoHistoricoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                                       liquidacaoHistoricoCollection.Query.Origem.In(OrigemLancamentoLiquidacao.Bolsa.Dividendo,
                                                                                          OrigemLancamentoLiquidacao.Bolsa.JurosCapital,
                                                                                          OrigemLancamentoLiquidacao.Bolsa.Rendimento,
                                                                                          OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital,
                                                                                          OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes,
                                                                                          OrigemLancamentoLiquidacao.Bolsa.OutrosProventos));
                liquidacaoHistoricoCollection.Query.GroupBy(liquidacaoHistoricoCollection.Query.Descricao,
                                                 liquidacaoHistoricoCollection.Query.DataVencimento,
                                                 liquidacaoHistoricoCollection.Query.Origem);
                liquidacaoHistoricoCollection.Query.Load();

                liquidacaoCollection = new LiquidacaoCollection(liquidacaoHistoricoCollection);
            }
            else
            {
                liquidacaoCollection.Query.Select(liquidacaoCollection.Query.Descricao,
                                                liquidacaoCollection.Query.DataVencimento,
                                                liquidacaoCollection.Query.Origem,
                                                liquidacaoCollection.Query.Valor.Sum());
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCarteira),
                                       liquidacaoCollection.Query.DataVencimento.GreaterThanOrEqual(data),
                                       liquidacaoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                       liquidacaoCollection.Query.Origem.In(OrigemLancamentoLiquidacao.Bolsa.Dividendo,
                                                                          OrigemLancamentoLiquidacao.Bolsa.JurosCapital,
                                                                          OrigemLancamentoLiquidacao.Bolsa.Rendimento,
                                                                          OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital,
                                                                          OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes,
                                                                          OrigemLancamentoLiquidacao.Bolsa.OutrosProventos));
                liquidacaoCollection.Query.GroupBy(liquidacaoCollection.Query.Descricao,
                                                 liquidacaoCollection.Query.DataVencimento,
                                                 liquidacaoCollection.Query.Origem);
                liquidacaoCollection.Query.Load();
            }

            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                decimal valor = liquidacao.Valor.Value;

                LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();
                liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
                liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCarteira),
                                                liquidacaoHistorico.Query.DataHistorico.Equal(dataHistorico),
                                                liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(dataHistorico),
                                                liquidacaoHistorico.Query.Descricao.Equal(liquidacao.Descricao),
                                                liquidacaoHistorico.Query.DataVencimento.Equal(liquidacao.DataVencimento),
                                                liquidacaoHistorico.Query.Origem.Equal(liquidacao.Origem.Value));
                liquidacaoHistorico.Query.Load();

                decimal valorHistorico = 0;
                if (liquidacaoHistorico.Valor.HasValue)
                {
                    valorHistorico = liquidacaoHistorico.Valor.Value;
                }

                decimal diferenca = valor - valorHistorico;

                if (diferenca != 0)
                {
                    AnaliseResultados analiseResultados = new AnaliseResultados();
                    analiseResultados.TipoMercado = (int)TiposMercados.Proventos;
                    analiseResultados.DescricaoAtivo = liquidacao.Descricao;
                    analiseResultados.DescricaoMercado = "Proventos";
                    analiseResultados.PercentualPL = Math.Round(diferenca / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = diferenca;

                    lista.Add(analiseResultados);
                }
            }
            #endregion

            #region Proventos (que estão na data d-1 e NãO estão na data atual)
            LiquidacaoHistoricoCollection liquidacaoHistoricoProventosCollection = new LiquidacaoHistoricoCollection();
            liquidacaoHistoricoProventosCollection.Query.Select(liquidacaoHistoricoProventosCollection.Query.Descricao,
                                                    liquidacaoHistoricoProventosCollection.Query.DataVencimento,
                                                    liquidacaoHistoricoProventosCollection.Query.Origem,
                                                    liquidacaoHistoricoProventosCollection.Query.Valor.Sum());
            liquidacaoHistoricoProventosCollection.Query.Where(liquidacaoHistoricoProventosCollection.Query.IdCliente.Equal(idCarteira),
                                                   liquidacaoHistoricoProventosCollection.Query.DataHistorico.Equal(dataHistorico),
                                                   liquidacaoHistoricoProventosCollection.Query.DataVencimento.GreaterThan(dataHistorico),
                                                   liquidacaoHistoricoProventosCollection.Query.DataLancamento.LessThanOrEqual(dataHistorico),
                                                   liquidacaoHistoricoProventosCollection.Query.Origem.In(OrigemLancamentoLiquidacao.Bolsa.Dividendo,
                                                                                      OrigemLancamentoLiquidacao.Bolsa.JurosCapital,
                                                                                      OrigemLancamentoLiquidacao.Bolsa.Rendimento,
                                                                                      OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital,
                                                                                      OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes,
                                                                                      OrigemLancamentoLiquidacao.Bolsa.OutrosProventos));
            liquidacaoHistoricoProventosCollection.Query.GroupBy(liquidacaoHistoricoProventosCollection.Query.Descricao,
                                                                 liquidacaoHistoricoProventosCollection.Query.DataVencimento,
                                                                 liquidacaoHistoricoProventosCollection.Query.Origem);
            liquidacaoHistoricoProventosCollection.Query.Load();

            foreach (LiquidacaoHistorico liquidacaoHistorico in liquidacaoHistoricoProventosCollection)
            {
                decimal valorHistorico = liquidacaoHistorico.Valor.Value;
                string descricao = liquidacaoHistorico.Descricao;

                decimal valor = 0;
                if (historico)
                {
                    LiquidacaoHistorico liquidacaoHistoricoProventos = new LiquidacaoHistorico();
                    liquidacaoHistoricoProventos.Query.Select(liquidacaoHistoricoProventos.Query.Valor.Sum());
                    liquidacaoHistoricoProventos.Query.Where(liquidacaoHistoricoProventos.Query.IdCliente.Equal(idCarteira),
                                                    liquidacaoHistoricoProventos.Query.DataHistorico.Equal(data),
                                                    liquidacaoHistoricoProventos.Query.DataLancamento.LessThanOrEqual(data),
                                                    liquidacaoHistoricoProventos.Query.Descricao.Equal(liquidacaoHistorico.Descricao),
                                                    liquidacaoHistoricoProventos.Query.DataVencimento.Equal(liquidacaoHistorico.DataVencimento),
                                                    liquidacaoHistoricoProventos.Query.Origem.Equal(liquidacaoHistorico.Origem.Value));
                    liquidacaoHistoricoProventos.Query.Load();

                    if (liquidacaoHistoricoProventos.Valor.HasValue)
                    {
                        valor = liquidacaoHistoricoProventos.Valor.Value;
                    }
                }
                else
                {
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
                    liquidacao.Query.Where(liquidacao.Query.IdCliente.Equal(idCarteira),
                                        liquidacao.Query.DataLancamento.LessThanOrEqual(data),
                                        liquidacao.Query.Descricao.Equal(liquidacaoHistorico.Descricao),
                                        liquidacao.Query.DataVencimento.Equal(liquidacaoHistorico.DataVencimento),
                                        liquidacao.Query.Origem.Equal(liquidacaoHistorico.Origem.Value));
                    liquidacao.Query.Load();

                    if (liquidacao.Valor.HasValue)
                    {
                        valor = liquidacao.Valor.Value;
                    }
                }


                decimal diferenca = 0;
                if (valor == 0)
                {
                    diferenca = valorHistorico * -1;
                }

                if (diferenca != 0)
                {
                    AnaliseResultados analiseResultados = new AnaliseResultados();
                    analiseResultados.TipoMercado = (int)TiposMercados.Proventos;
                    analiseResultados.DescricaoAtivo = descricao;
                    analiseResultados.DescricaoMercado = "Proventos";
                    analiseResultados.PercentualPL = Math.Round(diferenca / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = diferenca;

                    lista.Add(analiseResultados);
                }
            }
            #endregion

            #region TermoBolsa (Ações)
            PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            posicaoTermoBolsaHistoricoCollection.Query.Select(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim());
            posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                        posicaoTermoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoTermoBolsaHistoricoCollection.Query.es.Distinct = true;
            posicaoTermoBolsaHistoricoCollection.Query.Load();

            List<string> listaTermoBolsa = new List<string>();

            foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico in posicaoTermoBolsaHistoricoCollection)
            {
                listaTermoBolsa.Add(posicaoTermoBolsaHistorico.CdAtivoBolsa);
            }

            if (historico)
            {
                posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
                posicaoTermoBolsaHistoricoCollection.Query.Select(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim());
                posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                                 posicaoTermoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                 posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoTermoBolsaHistoricoCollection.Query.es.Distinct = true;
                posicaoTermoBolsaHistoricoCollection.Query.Load();

                foreach (PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico in posicaoTermoBolsaHistoricoCollection)
                {
                    if (!listaTermoBolsa.Contains(posicaoTermoBolsaHistorico.CdAtivoBolsa))
                    {
                        listaTermoBolsa.Add(posicaoTermoBolsaHistorico.CdAtivoBolsa);
                    }
                }
            }
            else
            {
                PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.CdAtivoBolsa.Trim());
                posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0));
                posicaoTermoBolsaCollection.Query.es.Distinct = true;
                posicaoTermoBolsaCollection.Query.Load();

                foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
                {
                    if (!listaTermoBolsa.Contains(posicaoTermoBolsa.CdAtivoBolsa))
                    {
                        listaTermoBolsa.Add(posicaoTermoBolsa.CdAtivoBolsa);
                    }
                }
            }

            operacaoBolsaCollectionLista = new OperacaoBolsaCollection();
            operacaoBolsaCollectionLista.Query.Select(operacaoBolsaCollectionLista.Query.CdAtivoBolsa.Trim());
            operacaoBolsaCollectionLista.Query.Where(operacaoBolsaCollectionLista.Query.IdCliente.Equal(idCarteira),
                                                    operacaoBolsaCollectionLista.Query.Data.LessThanOrEqual(data),
                                                    operacaoBolsaCollectionLista.Query.Data.GreaterThan(dataHistorico),
                                                    operacaoBolsaCollectionLista.Query.TipoMercado.In(TipoMercadoBolsa.Termo));
            operacaoBolsaCollectionLista.Query.es.Distinct = true;
            operacaoBolsaCollectionLista.Query.Load();

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollectionLista)
            {
                if (!listaTermoBolsa.Contains(operacaoBolsa.CdAtivoBolsa))
                {
                    listaTermoBolsa.Add(operacaoBolsa.CdAtivoBolsa);
                }
            }

            foreach (string cdAtivoBolsa in listaTermoBolsa)
            {
                decimal resultado = 0;
                OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
                operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.ValorLiquido.Sum(),
                                                     operacaoBolsaCollection.Query.TipoOperacao);
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCarteira),
                                                    operacaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                    operacaoBolsaCollection.Query.Data.LessThanOrEqual(data),
                                                    operacaoBolsaCollection.Query.Data.GreaterThan(dataHistorico));
                operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.TipoOperacao);
                operacaoBolsaCollection.Query.Load();

                foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
                {
                    if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Compra)
                    {
                        resultado -= operacaoBolsa.ValorLiquido.Value;
                    }
                    else if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Venda)
                    {
                        resultado += operacaoBolsa.ValorLiquido.Value;
                    }
                }

                PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
                posicaoTermoBolsaHistorico.Query.Select(posicaoTermoBolsaHistorico.Query.ValorMercado.Sum());
                posicaoTermoBolsaHistorico.Query.Where(posicaoTermoBolsaHistorico.Query.IdCliente.Equal(idCarteira),
                                                       posicaoTermoBolsaHistorico.Query.DataHistorico.Equal(dataHistorico),
                                                       posicaoTermoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoTermoBolsaHistorico.Query.Load();

                if (posicaoTermoBolsaHistorico.es.HasData && posicaoTermoBolsaHistorico.ValorMercado.HasValue)
                {
                    resultado -= posicaoTermoBolsaHistorico.ValorMercado.Value;
                }

                if (historico)
                {
                    posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
                    posicaoTermoBolsaHistorico.Query.Select(posicaoTermoBolsaHistorico.Query.ValorMercado.Sum());
                    posicaoTermoBolsaHistorico.Query.Where(posicaoTermoBolsaHistorico.Query.IdCliente.Equal(idCarteira),
                                                           posicaoTermoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                           posicaoTermoBolsaHistorico.Query.DataHistorico.Equal(data));
                    posicaoTermoBolsaHistorico.Query.Load();

                    if (posicaoTermoBolsaHistorico.es.HasData && posicaoTermoBolsaHistorico.ValorMercado.HasValue)
                    {
                        resultado += posicaoTermoBolsaHistorico.ValorMercado.Value;
                    }
                }
                else
                {
                    PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                    posicaoTermoBolsa.Query.Select(posicaoTermoBolsa.Query.ValorMercado.Sum());
                    posicaoTermoBolsa.Query.Where(posicaoTermoBolsa.Query.IdCliente.Equal(idCarteira),
                                             posicaoTermoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                    posicaoTermoBolsa.Query.Load();

                    if (posicaoTermoBolsa.es.HasData && posicaoTermoBolsa.ValorMercado.HasValue)
                    {
                        resultado += posicaoTermoBolsa.ValorMercado.Value;
                    }
                }

                LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();
                liquidacaoTermoBolsaCollection.Query.Where(liquidacaoTermoBolsaCollection.Query.IdCliente.Equal(idCarteira),
                                                           liquidacaoTermoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                           liquidacaoTermoBolsaCollection.Query.DataMovimento.LessThanOrEqual(data),
                                                           liquidacaoTermoBolsaCollection.Query.DataMovimento.GreaterThan(dataHistorico));
                liquidacaoTermoBolsaCollection.Query.Load();

                foreach (LiquidacaoTermoBolsa liquidacaoTermoBolsa in liquidacaoTermoBolsaCollection)
                {
                    int idPosicao = liquidacaoTermoBolsa.IdPosicao.Value;
                    decimal valorTermoLiquidado = Math.Abs(liquidacaoTermoBolsa.Valor.Value);

                    PosicaoTermoBolsaAbertura posicaoTermoBolsaAbertura = new PosicaoTermoBolsaAbertura();
                    posicaoTermoBolsaAbertura.LoadByPrimaryKey(idPosicao, liquidacaoTermoBolsa.DataMovimento.Value);

                    if (posicaoTermoBolsaAbertura.Quantidade.Value > 0)
                    {
                        valorTermoLiquidado = valorTermoLiquidado * -1;
                    }

                    resultado += valorTermoLiquidado;
                }

                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = (int)TiposMercados.TermoAcao;
                analiseResultados.DescricaoAtivo = "Termo de " + AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa) + " " + AtivoBolsa.RetornaDataVencimentoTermo(cdAtivoBolsa).ToShortDateString();
                analiseResultados.DescricaoMercado = "Termos de Ações";
                analiseResultados.PercentualPL = Math.Round(resultado / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = resultado;

                lista.Add(analiseResultados);
            }
            #endregion

            #region EmprestimoBolsa (BTC)
            PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
            posicaoEmprestimoBolsaHistoricoCollection.Query.Select(posicaoEmprestimoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim(),
                                                                   posicaoEmprestimoBolsaHistoricoCollection.Query.PontaEmprestimo);
            posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                        posicaoEmprestimoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoEmprestimoBolsaHistoricoCollection.Query.GroupBy(posicaoEmprestimoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim(),
                                                                    posicaoEmprestimoBolsaHistoricoCollection.Query.PontaEmprestimo);
            posicaoEmprestimoBolsaHistoricoCollection.Query.Load();

            List<string> listaEmprestimoBolsaAtivo = new List<string>();
            List<byte> listaEmprestimoBolsaPonta = new List<byte>();
            List<string> listaEmprestimoBolsa = new List<string>();

            foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico in posicaoEmprestimoBolsaHistoricoCollection)
            {
                listaEmprestimoBolsaAtivo.Add(posicaoEmprestimoBolsaHistorico.CdAtivoBolsa);
                listaEmprestimoBolsaPonta.Add(posicaoEmprestimoBolsaHistorico.PontaEmprestimo.Value);
                listaEmprestimoBolsa.Add(posicaoEmprestimoBolsaHistorico.PontaEmprestimo.ToString() + posicaoEmprestimoBolsaHistorico.CdAtivoBolsa);
            }

            if (historico)
            {
                posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                posicaoEmprestimoBolsaHistoricoCollection.Query.Select(posicaoEmprestimoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim(),
                                                                       posicaoEmprestimoBolsaHistoricoCollection.Query.PontaEmprestimo);
                posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoEmprestimoBolsaHistoricoCollection.Query.GroupBy(posicaoEmprestimoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim(),
                                                                        posicaoEmprestimoBolsaHistoricoCollection.Query.PontaEmprestimo);
                posicaoEmprestimoBolsaHistoricoCollection.Query.Load();

                foreach (PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico in posicaoEmprestimoBolsaHistoricoCollection)
                {
                    if (!listaEmprestimoBolsa.Contains(posicaoEmprestimoBolsaHistorico.PontaEmprestimo.ToString() + posicaoEmprestimoBolsaHistorico.CdAtivoBolsa))
                    {
                        listaEmprestimoBolsaAtivo.Add(posicaoEmprestimoBolsaHistorico.CdAtivoBolsa);
                        listaEmprestimoBolsaPonta.Add(posicaoEmprestimoBolsaHistorico.PontaEmprestimo.Value);
                    }
                }
            }
            else
            {
                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                posicaoEmprestimoBolsaCollection.Query.Select(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa.Trim(),
                                                              posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo);
                posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCarteira),
                                                   posicaoEmprestimoBolsaCollection.Query.Quantidade.NotEqual(0));
                posicaoEmprestimoBolsaCollection.Query.GroupBy(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa.Trim(),
                                                               posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo);
                posicaoEmprestimoBolsaCollection.Query.Load();

                foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
                {
                    if (!listaEmprestimoBolsa.Contains(posicaoEmprestimoBolsa.PontaEmprestimo.ToString() + posicaoEmprestimoBolsa.CdAtivoBolsa))
                    {
                        listaEmprestimoBolsaAtivo.Add(posicaoEmprestimoBolsa.CdAtivoBolsa);
                        listaEmprestimoBolsaPonta.Add(posicaoEmprestimoBolsa.PontaEmprestimo.Value);
                    }
                }
            }

            int i = 0;
            foreach (string cdAtivoBolsa in listaEmprestimoBolsaAtivo)
            {
                decimal resultado = 0;
                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
                posicaoEmprestimoBolsaHistorico.Query.Select(posicaoEmprestimoBolsaHistorico.Query.ValorMercado.Sum());
                posicaoEmprestimoBolsaHistorico.Query.Where(posicaoEmprestimoBolsaHistorico.Query.IdCliente.Equal(idCarteira),
                                                  posicaoEmprestimoBolsaHistorico.Query.DataHistorico.Equal(dataHistorico),
                                                  posicaoEmprestimoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                  posicaoEmprestimoBolsaHistorico.Query.PontaEmprestimo.Equal(listaEmprestimoBolsaPonta[i]));
                posicaoEmprestimoBolsaHistorico.Query.Load();

                if (posicaoEmprestimoBolsaHistorico.es.HasData && posicaoEmprestimoBolsaHistorico.ValorMercado.HasValue)
                {
                    if (listaEmprestimoBolsaPonta[i] == (byte)PontaEmprestimoBolsa.Doador)
                    {
                        resultado -= posicaoEmprestimoBolsaHistorico.ValorMercado.Value;
                    }
                    else
                    {
                        resultado += posicaoEmprestimoBolsaHistorico.ValorMercado.Value;
                    }
                }

                if (historico)
                {
                    posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
                    posicaoEmprestimoBolsaHistorico.Query.Select(posicaoEmprestimoBolsaHistorico.Query.ValorMercado.Sum());
                    posicaoEmprestimoBolsaHistorico.Query.Where(posicaoEmprestimoBolsaHistorico.Query.IdCliente.Equal(idCarteira),
                                                             posicaoEmprestimoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                             posicaoEmprestimoBolsaHistorico.Query.DataHistorico.Equal(data),
                                                             posicaoEmprestimoBolsaHistorico.Query.PontaEmprestimo.Equal(listaEmprestimoBolsaPonta[i]));
                    posicaoEmprestimoBolsaHistorico.Query.Load();

                    if (posicaoEmprestimoBolsaHistorico.es.HasData && posicaoEmprestimoBolsaHistorico.ValorMercado.HasValue)
                    {
                        if (listaEmprestimoBolsaPonta[i] == (byte)PontaEmprestimoBolsa.Doador)
                        {
                            resultado += posicaoEmprestimoBolsaHistorico.ValorMercado.Value;
                        }
                        else
                        {
                            resultado -= posicaoEmprestimoBolsaHistorico.ValorMercado.Value;
                        }
                    }
                }
                else
                {
                    PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
                    posicaoEmprestimoBolsa.Query.Select(posicaoEmprestimoBolsa.Query.ValorMercado.Sum());
                    posicaoEmprestimoBolsa.Query.Where(posicaoEmprestimoBolsa.Query.IdCliente.Equal(idCarteira),
                                             posicaoEmprestimoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                             posicaoEmprestimoBolsa.Query.PontaEmprestimo.Equal(listaEmprestimoBolsaPonta[i]));
                    posicaoEmprestimoBolsa.Query.Load();

                    if (posicaoEmprestimoBolsa.es.HasData && posicaoEmprestimoBolsa.ValorMercado.HasValue)
                    {
                        if (listaEmprestimoBolsaPonta[i] == (byte)PontaEmprestimoBolsa.Doador)
                        {
                            resultado += posicaoEmprestimoBolsa.ValorMercado.Value;
                        }
                        else
                        {
                            resultado -= posicaoEmprestimoBolsa.ValorMercado.Value;
                        }
                    }
                }

                string ponta = listaEmprestimoBolsaPonta[i] == (byte)PontaEmprestimoBolsa.Doador ? "Doador" : "Tomador";
                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = (int)TiposMercados.BTC;
                analiseResultados.DescricaoAtivo = "BTC " + ponta + " - " + cdAtivoBolsa;
                analiseResultados.DescricaoMercado = "Variação BTC";
                analiseResultados.PercentualPL = Math.Round(resultado / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = resultado;

                lista.Add(analiseResultados);

                i++;
            }
            #endregion

            #region Custos BTC
            TabelaCustosBolsa tabelaCustosBolsa = new TabelaCustosBolsa();
            tabelaCustosBolsa.BuscaTabelaCustosBolsa(data, (Int16)TipoTaxaBolsa.EmprestimoBolsa.Voluntario);
            decimal valorMinimoCBLC = 0;
            if (tabelaCustosBolsa.MinimoCBLC.HasValue)
            {
                valorMinimoCBLC = tabelaCustosBolsa.MinimoCBLC.Value;
            }

            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCustosCollection = new PosicaoEmprestimoBolsaCollection();
            if (historico)
            {
                posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                posicaoEmprestimoBolsaHistoricoCollection.Query.Select(posicaoEmprestimoBolsaHistoricoCollection.Query.ValorCorrigidoCBLC.Sum(),
                                                              posicaoEmprestimoBolsaHistoricoCollection.Query.ValorCorrigidoComissao.Sum(),
                                                              posicaoEmprestimoBolsaHistoricoCollection.Query.ValorCorrigidoJuros.Sum(),
                                                              posicaoEmprestimoBolsaHistoricoCollection.Query.ValorBase.Sum(),
                                                              posicaoEmprestimoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim(),
                                                              posicaoEmprestimoBolsaHistoricoCollection.Query.DataVencimento,
                                                              posicaoEmprestimoBolsaHistoricoCollection.Query.PontaEmprestimo);
                posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoEmprestimoBolsaHistoricoCollection.Query.GroupBy(posicaoEmprestimoBolsaHistoricoCollection.Query.CdAtivoBolsa.Trim(),
                                                                        posicaoEmprestimoBolsaHistoricoCollection.Query.DataVencimento,
                                                                        posicaoEmprestimoBolsaHistoricoCollection.Query.PontaEmprestimo);
                posicaoEmprestimoBolsaHistoricoCollection.Query.Load();
                posicaoEmprestimoBolsaCustosCollection = new PosicaoEmprestimoBolsaCollection(posicaoEmprestimoBolsaHistoricoCollection);
            }
            else
            {

                posicaoEmprestimoBolsaCustosCollection.Query.Select(posicaoEmprestimoBolsaCustosCollection.Query.ValorCorrigidoCBLC.Sum(),
                                                              posicaoEmprestimoBolsaCustosCollection.Query.ValorCorrigidoComissao.Sum(),
                                                              posicaoEmprestimoBolsaCustosCollection.Query.ValorCorrigidoJuros.Sum(),
                                                              posicaoEmprestimoBolsaCustosCollection.Query.ValorBase.Sum(),
                                                              posicaoEmprestimoBolsaCustosCollection.Query.CdAtivoBolsa.Trim(),
                                                              posicaoEmprestimoBolsaCustosCollection.Query.DataVencimento,
                                                              posicaoEmprestimoBolsaCustosCollection.Query.PontaEmprestimo);
                posicaoEmprestimoBolsaCustosCollection.Query.Where(posicaoEmprestimoBolsaCustosCollection.Query.IdCliente.Equal(idCarteira),
                                                             posicaoEmprestimoBolsaCustosCollection.Query.Quantidade.NotEqual(0));
                posicaoEmprestimoBolsaCustosCollection.Query.GroupBy(posicaoEmprestimoBolsaCustosCollection.Query.CdAtivoBolsa.Trim(),
                                                              posicaoEmprestimoBolsaCustosCollection.Query.DataVencimento,
                                                              posicaoEmprestimoBolsaCustosCollection.Query.PontaEmprestimo);
                posicaoEmprestimoBolsaCustosCollection.Query.Load();
            }

            foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCustosCollection)
            {
                string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                byte ponta = posicaoEmprestimoBolsa.PontaEmprestimo.Value;
                decimal valorBase = posicaoEmprestimoBolsa.ValorBase.Value;
                decimal valorJuros = Math.Abs(posicaoEmprestimoBolsa.ValorCorrigidoJuros.Value) - Math.Abs(valorBase);
                decimal valorComissao = Math.Abs(posicaoEmprestimoBolsa.ValorCorrigidoComissao.Value) - Math.Abs(valorBase);
                decimal valorCBLC = Math.Abs(posicaoEmprestimoBolsa.ValorCorrigidoCBLC.Value) - Math.Abs(valorBase);

                if (valorCBLC < valorMinimoCBLC)
                {
                    valorCBLC = valorMinimoCBLC;
                }

                decimal custos = 0;
                if (ponta == (byte)PontaEmprestimoBolsa.Doador)
                {
                    custos = valorJuros - valorComissao;
                }
                else
                {
                    custos = valorJuros + valorComissao + valorCBLC;
                }

                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
                posicaoEmprestimoBolsaHistorico.Query.Select(posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoCBLC.Sum(),
                                                             posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoComissao.Sum(),
                                                             posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoJuros.Sum(),
                                                             posicaoEmprestimoBolsaHistorico.Query.ValorBase.Sum());
                posicaoEmprestimoBolsaHistorico.Query.Where(posicaoEmprestimoBolsaHistorico.Query.IdCliente.Equal(idCarteira),
                                                            posicaoEmprestimoBolsaHistorico.Query.Quantidade.NotEqual(0),
                                                            posicaoEmprestimoBolsaHistorico.Query.DataHistorico.Equal(dataHistorico),
                                                            posicaoEmprestimoBolsaHistorico.Query.CdAtivoBolsa.Equal(posicaoEmprestimoBolsa.CdAtivoBolsa),
                                                            posicaoEmprestimoBolsaHistorico.Query.DataVencimento.Equal(posicaoEmprestimoBolsa.DataVencimento.Value),
                                                            posicaoEmprestimoBolsaHistorico.Query.PontaEmprestimo.Equal(posicaoEmprestimoBolsa.PontaEmprestimo.Value));
                posicaoEmprestimoBolsaHistorico.Query.Load();

                decimal custosAnterior = 0;
                if (posicaoEmprestimoBolsaHistorico.es.HasData && posicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros.HasValue)
                {
                    valorBase = posicaoEmprestimoBolsaHistorico.ValorBase.Value;
                    valorJuros = Math.Abs(posicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros.Value) - Math.Abs(valorBase);
                    valorComissao = Math.Abs(posicaoEmprestimoBolsaHistorico.ValorCorrigidoComissao.Value) - Math.Abs(valorBase);
                    valorCBLC = Math.Abs(posicaoEmprestimoBolsaHistorico.ValorCorrigidoCBLC.Value) - Math.Abs(valorBase);

                    if (valorCBLC < valorMinimoCBLC)
                    {
                        valorCBLC = valorMinimoCBLC;
                    }

                    if (ponta == (byte)PontaEmprestimoBolsa.Doador)
                    {
                        custosAnterior = valorJuros - valorComissao;
                    }
                    else
                    {
                        custosAnterior = valorJuros + valorComissao + valorCBLC;
                    }
                }

                decimal diferenca = custos - custosAnterior;

                string pontaBTC = ponta == (byte)PontaEmprestimoBolsa.Doador ? "Doador" : "Tomador";

                LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
                liquidacaoEmprestimoBolsa.Query.Select(liquidacaoEmprestimoBolsa.Query.ValorLiquidacao.Sum());
                liquidacaoEmprestimoBolsa.Query.Where(liquidacaoEmprestimoBolsa.Query.IdCliente.Equal(idCarteira),
                                                      liquidacaoEmprestimoBolsa.Query.Data.Equal(data),
                                                      liquidacaoEmprestimoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                liquidacaoEmprestimoBolsa.Query.Load();

                decimal valorLiquidacao = 0;
                if (liquidacaoEmprestimoBolsa.es.HasData && liquidacaoEmprestimoBolsa.ValorLiquidacao.HasValue)
                {
                    valorLiquidacao = liquidacaoEmprestimoBolsa.ValorLiquidacao.Value;
                    diferenca += valorLiquidacao;
                }

                if (ponta == (byte)PontaEmprestimoBolsa.Tomador)
                {
                    diferenca = diferenca * -1;
                }

                if (diferenca != 0)
                {
                    AnaliseResultados analiseResultados = new AnaliseResultados();
                    analiseResultados.TipoMercado = (int)TiposMercados.CustosBTC;
                    analiseResultados.DescricaoAtivo = pontaBTC + " de " + cdAtivoBolsa + " Vcto: " + posicaoEmprestimoBolsa.DataVencimento.Value.ToShortDateString();
                    analiseResultados.DescricaoMercado = "Custos/Juros BTC";
                    analiseResultados.PercentualPL = Math.Round(diferenca / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = diferenca;

                    lista.Add(analiseResultados);
                }
            }
            #endregion

            #region BMF (Futuros)
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.CdAtivoBMF,
                                               operacaoBMFCollection.Query.Serie,
                                               operacaoBMFCollection.Query.Ajuste.Sum(),
                                               operacaoBMFCollection.Query.Corretagem.Sum(),
                                               operacaoBMFCollection.Query.Emolumento.Sum(),
                                               operacaoBMFCollection.Query.Registro.Sum(),
                                               operacaoBMFCollection.Query.TaxaClearingVlFixo.Sum());
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente.Equal(idCarteira),
                                              operacaoBMFCollection.Query.Data.LessThanOrEqual(data),
                                              operacaoBMFCollection.Query.Data.GreaterThan(dataHistorico),
                                              operacaoBMFCollection.Query.TipoMercado.Equal((byte)TipoMercadoBMF.Futuro));
            operacaoBMFCollection.Query.GroupBy(operacaoBMFCollection.Query.CdAtivoBMF,
                                               operacaoBMFCollection.Query.Serie);
            operacaoBMFCollection.Query.Load();

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string ativoBMF = operacaoBMF.CdAtivoBMF + operacaoBMF.Serie;
                decimal resultado = operacaoBMF.Ajuste.Value - (operacaoBMF.Corretagem.Value + operacaoBMF.Emolumento.Value + operacaoBMF.Registro.Value + operacaoBMF.TaxaClearingVlFixo.Value);

                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = (int)TiposMercados.FuturoBMF;
                analiseResultados.DescricaoAtivo = operacaoBMF.CdAtivoBMF + "-" + operacaoBMF.Serie;
                analiseResultados.DescricaoMercado = "Futuros BMF";                
                analiseResultados.PercentualPL = valorPLAnterior == 0 ? 0 : Math.Round(resultado / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = resultado;

                lista.Add(analiseResultados);
            }
            #endregion

            #region Opções BMF
            PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
            posicaoBMFHistoricoCollection.Query.Select(posicaoBMFHistoricoCollection.Query.CdAtivoBMF,
                                                       posicaoBMFHistoricoCollection.Query.Serie);
            posicaoBMFHistoricoCollection.Query.Where(posicaoBMFHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                    posicaoBMFHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                    posicaoBMFHistoricoCollection.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo),
                                                    posicaoBMFHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoBMFHistoricoCollection.Query.GroupBy(posicaoBMFHistoricoCollection.Query.CdAtivoBMF,
                                                        posicaoBMFHistoricoCollection.Query.Serie);
            posicaoBMFHistoricoCollection.Query.Load();

            List<string> listaBMF = new List<string>();
            List<string> listaBMFAtivo = new List<string>();
            List<string> listaBMFSerie = new List<string>();

            foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
            {
                listaBMFAtivo.Add(posicaoBMFHistorico.CdAtivoBMF);
                listaBMFSerie.Add(posicaoBMFHistorico.Serie);
                listaBMF.Add(posicaoBMFHistorico.CdAtivoBMF + posicaoBMFHistorico.Serie);
            }

            if (historico)
            {
                posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
                posicaoBMFHistoricoCollection.Query.Select(posicaoBMFHistoricoCollection.Query.CdAtivoBMF,
                                                           posicaoBMFHistoricoCollection.Query.Serie);
                posicaoBMFHistoricoCollection.Query.Where(posicaoBMFHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                         posicaoBMFHistoricoCollection.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo),
                                                         posicaoBMFHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                         posicaoBMFHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBMFHistoricoCollection.Query.GroupBy(posicaoBMFHistoricoCollection.Query.CdAtivoBMF,
                                                            posicaoBMFHistoricoCollection.Query.Serie);
                posicaoBMFHistoricoCollection.Query.Load();

                foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
                {
                    if (!listaBMF.Contains(posicaoBMFHistorico.CdAtivoBMF + posicaoBMFHistorico.Serie))
                    {
                        listaBMFAtivo.Add(posicaoBMFHistorico.CdAtivoBMF);
                        listaBMFSerie.Add(posicaoBMFHistorico.Serie);
                        listaBMF.Add(posicaoBMFHistorico.CdAtivoBMF + posicaoBMFHistorico.Serie);
                    }
                }
            }
            else
            {
                PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                posicaoBMFCollection.Query.Select(posicaoBMFCollection.Query.CdAtivoBMF,
                                                  posicaoBMFCollection.Query.Serie);
                posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idCarteira),
                                                 posicaoBMFCollection.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo),
                                                 posicaoBMFCollection.Query.Quantidade.NotEqual(0));
                posicaoBMFCollection.Query.GroupBy(posicaoBMFCollection.Query.CdAtivoBMF,
                                                   posicaoBMFCollection.Query.Serie);
                posicaoBMFCollection.Query.Load();

                foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
                {
                    if (!listaBMF.Contains(posicaoBMF.CdAtivoBMF + posicaoBMF.Serie))
                    {
                        listaBMFAtivo.Add(posicaoBMF.CdAtivoBMF);
                        listaBMFSerie.Add(posicaoBMF.Serie);
                        listaBMF.Add(posicaoBMF.CdAtivoBMF + posicaoBMF.Serie);
                    }
                }
            }

            OperacaoBMFCollection operacaoBMFCollectionLista = new OperacaoBMFCollection();
            operacaoBMFCollectionLista.Query.Select(operacaoBMFCollectionLista.Query.CdAtivoBMF,
                                                    operacaoBMFCollectionLista.Query.Serie);
            operacaoBMFCollectionLista.Query.Where(operacaoBMFCollectionLista.Query.IdCliente.Equal(idCarteira),
                                                   operacaoBMFCollectionLista.Query.Data.LessThanOrEqual(data),
                                                   operacaoBMFCollectionLista.Query.Data.GreaterThan(dataHistorico),
                                                   operacaoBMFCollectionLista.Query.TipoMercado.In(TipoMercadoBMF.OpcaoDisponivel, TipoMercadoBMF.OpcaoFuturo));
            operacaoBMFCollectionLista.Query.es.Distinct = true;
            operacaoBMFCollectionLista.Query.Load();

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollectionLista)
            {
                if (!listaBMF.Contains(operacaoBMF.CdAtivoBMF + operacaoBMF.Serie))
                {
                    listaBMFAtivo.Add(operacaoBMF.CdAtivoBMF);
                    listaBMFSerie.Add(operacaoBMF.Serie);
                    listaBMF.Add(operacaoBMF.CdAtivoBMF + operacaoBMF.Serie);
                }
            }

            for(i = 0; i < listaBMF.Count; i++)
            {
                string cdAtivoBMF = listaBMFAtivo[i];
                string serie = listaBMFSerie[i];

                decimal resultado = 0;
                operacaoBMFCollection = new OperacaoBMFCollection();
                operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.ValorLiquido.Sum(),
                                                   operacaoBMFCollection.Query.TipoOperacao);
                operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente.Equal(idCarteira),
                                                  operacaoBMFCollection.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                  operacaoBMFCollection.Query.Serie.Equal(serie),
                                                  operacaoBMFCollection.Query.Data.LessThanOrEqual(data),
                                                  operacaoBMFCollection.Query.Data.GreaterThan(dataHistorico));
                operacaoBMFCollection.Query.GroupBy(operacaoBMFCollection.Query.TipoOperacao);
                operacaoBMFCollection.Query.Load();

                foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
                {
                    if (operacaoBMF.TipoOperacao == TipoOperacaoBMF.Compra ||
                        operacaoBMF.TipoOperacao == TipoOperacaoBMF.CompraDaytrade)
                    {
                        resultado -= operacaoBMF.ValorLiquido.Value;
                    }
                    else if (operacaoBMF.TipoOperacao == TipoOperacaoBMF.Venda ||
                             operacaoBMF.TipoOperacao == TipoOperacaoBMF.VendaDaytrade)
                    {
                        resultado += operacaoBMF.ValorLiquido.Value;
                    }
                }

                PosicaoBMFHistorico posicaoBMFHistorico = new PosicaoBMFHistorico();
                posicaoBMFHistorico.Query.Select(posicaoBMFHistorico.Query.ValorMercado.Sum());
                posicaoBMFHistorico.Query.Where(posicaoBMFHistorico.Query.IdCliente.Equal(idCarteira),
                                              posicaoBMFHistorico.Query.DataHistorico.Equal(dataHistorico),
                                              posicaoBMFHistorico.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                              posicaoBMFHistorico.Query.Serie.Equal(serie));
                posicaoBMFHistorico.Query.Load();

                if (posicaoBMFHistorico.es.HasData && posicaoBMFHistorico.ValorMercado.HasValue)
                {
                    resultado -= posicaoBMFHistorico.ValorMercado.Value;
                }

                if (historico)
                {
                    posicaoBMFHistorico = new PosicaoBMFHistorico();
                    posicaoBMFHistorico.Query.Select(posicaoBMFHistorico.Query.ValorMercado.Sum());
                    posicaoBMFHistorico.Query.Where(posicaoBMFHistorico.Query.IdCliente.Equal(idCarteira),
                                                   posicaoBMFHistorico.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                                   posicaoBMFHistorico.Query.DataHistorico.Equal(data),
                                                   posicaoBMFHistorico.Query.Serie.Equal(serie));
                    posicaoBMFHistorico.Query.Load();

                    if (posicaoBMFHistorico.es.HasData && posicaoBMFHistorico.ValorMercado.HasValue)
                    {
                        resultado += posicaoBMFHistorico.ValorMercado.Value;
                    }
                }
                else
                {
                    PosicaoBMF posicaoBMF = new PosicaoBMF();
                    posicaoBMF.Query.Select(posicaoBMF.Query.ValorMercado.Sum());
                    posicaoBMF.Query.Where(posicaoBMF.Query.IdCliente.Equal(idCarteira),
                                           posicaoBMF.Query.CdAtivoBMF.Equal(cdAtivoBMF),
                                           posicaoBMF.Query.Serie.Equal(serie));
                    posicaoBMF.Query.Load();

                    if (posicaoBMF.es.HasData && posicaoBMF.ValorMercado.HasValue)
                    {
                        resultado += posicaoBMF.ValorMercado.Value;
                    }
                }

                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = (int)TiposMercados.OpcaoBMF;
                analiseResultados.DescricaoAtivo = cdAtivoBMF + "-" + serie;
                analiseResultados.DescricaoMercado = "Opções BMF";
                analiseResultados.PercentualPL = Math.Round(resultado / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = resultado;

                lista.Add(analiseResultados);
            }
            #endregion

            #region Fundos
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira);
            posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                        posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoFundoHistoricoCollection.Query.es.Distinct = true;
            posicaoFundoHistoricoCollection.Query.Load();

            List<int> listaFundo = new List<int>();

            foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
            {
                listaFundo.Add(posicaoFundoHistorico.IdCarteira.Value);
            }

            if (historico)
            {
                posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira);
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                            posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                            posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoFundoHistoricoCollection.Query.es.Distinct = true;
                posicaoFundoHistoricoCollection.Query.Load();

                foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                {
                    if (!listaFundo.Contains(posicaoFundoHistorico.IdCarteira.Value))
                    {
                        listaFundo.Add(posicaoFundoHistorico.IdCarteira.Value);
                    }
                }
            }
            else
            {
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira);
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCarteira),
                                                   posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoCollection.Query.es.Distinct = true;
                posicaoFundoCollection.Query.Load();

                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    if (!listaFundo.Contains(posicaoFundo.IdCarteira.Value))
                    {
                        listaFundo.Add(posicaoFundo.IdCarteira.Value);
                    }
                }
            }

            OperacaoFundoCollection operacaoFundoCollectionLista = new OperacaoFundoCollection();
            operacaoFundoCollectionLista.Query.Select(operacaoFundoCollectionLista.Query.IdCarteira);
            operacaoFundoCollectionLista.Query.Where(operacaoFundoCollectionLista.Query.IdCliente.Equal(idCarteira),
                                                    operacaoFundoCollectionLista.Query.DataConversao.LessThanOrEqual(data),
                                                    operacaoFundoCollectionLista.Query.DataConversao.GreaterThan(dataHistorico));
            operacaoFundoCollectionLista.Query.es.Distinct = true;
            operacaoFundoCollectionLista.Query.Load();

            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollectionLista)
            {
                if (!listaFundo.Contains(operacaoFundo.IdCarteira.Value))
                {
                    listaFundo.Add(operacaoFundo.IdCarteira.Value);
                }
            }

            foreach (int idFundo in listaFundo)
            {
                decimal resultado = 0;
                decimal resultadoIR = 0;
                decimal resultadoIOF = 0;
                OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
                operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.ValorBruto.Sum(),
                                                     operacaoFundoCollection.Query.ValorIR.Sum(),
                                                     operacaoFundoCollection.Query.ValorIOF.Sum(),
                                                     operacaoFundoCollection.Query.TipoOperacao);
                operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCarteira),
                                                    operacaoFundoCollection.Query.IdCarteira.Equal(idFundo),
                                                    operacaoFundoCollection.Query.DataConversao.LessThanOrEqual(data),
                                                    operacaoFundoCollection.Query.DataConversao.GreaterThan(dataHistorico));
                operacaoFundoCollection.Query.GroupBy(operacaoFundoCollection.Query.TipoOperacao);
                operacaoFundoCollection.Query.Load();

                foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
                {
                    if (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
                    {
                        resultado -= operacaoFundo.ValorBruto.Value;
                    }
                    else if (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto ||
                             operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                             operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                             operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal ||
                             operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.Amortizacao ||
                             operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.AmortizacaoJuros ||
                             operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.Juros)
                    {
                        resultado += operacaoFundo.ValorBruto.Value;
                        resultadoIR += operacaoFundo.ValorIR.Value;
                        resultadoIOF += operacaoFundo.ValorIOF.Value;
                    }
                    else if (operacaoFundo.TipoOperacao == (byte)TipoOperacaoFundo.ComeCotas)
                    {
                        resultado += operacaoFundo.ValorIR.Value;
                    }
                }

                PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                posicaoFundoHistorico.Query.Select(posicaoFundoHistorico.Query.ValorBruto.Sum(),
                                                    posicaoFundoHistorico.Query.ValorIOF.Sum(),
                                                    posicaoFundoHistorico.Query.ValorIR.Sum());
                posicaoFundoHistorico.Query.Where(posicaoFundoHistorico.Query.IdCliente.Equal(idCarteira),
                                                  posicaoFundoHistorico.Query.DataHistorico.Equal(dataHistorico),
                                                  posicaoFundoHistorico.Query.IdCarteira.Equal(idFundo));
                posicaoFundoHistorico.Query.Load();

                if (posicaoFundoHistorico.es.HasData && posicaoFundoHistorico.ValorBruto.HasValue)
                {
                    resultado -= posicaoFundoHistorico.ValorBruto.Value;
                    resultadoIR -= posicaoFundoHistorico.ValorIR.Value;
                    resultadoIOF -= posicaoFundoHistorico.ValorIOF.Value;
                }

                if (historico)
                {
                    posicaoFundoHistorico = new PosicaoFundoHistorico();
                    posicaoFundoHistorico.Query.Select(posicaoFundoHistorico.Query.ValorBruto.Sum(),
                                                        posicaoFundoHistorico.Query.ValorIOF.Sum(),
                                                        posicaoFundoHistorico.Query.ValorIR.Sum());
                    posicaoFundoHistorico.Query.Where(posicaoFundoHistorico.Query.IdCliente.Equal(idCarteira),
                                                     posicaoFundoHistorico.Query.IdCarteira.Equal(idFundo),
                                                     posicaoFundoHistorico.Query.DataHistorico.Equal(data));
                    posicaoFundoHistorico.Query.Load();

                    if (posicaoFundoHistorico.es.HasData && posicaoFundoHistorico.ValorBruto.HasValue)
                    {
                        resultado += posicaoFundoHistorico.ValorBruto.Value;
                        resultadoIR += posicaoFundoHistorico.ValorIR.Value;
                        resultadoIOF += posicaoFundoHistorico.ValorIOF.Value;
                    }
                }
                else
                {
                    PosicaoFundo posicaoFundo = new PosicaoFundo();
                    posicaoFundo.Query.Select(posicaoFundo.Query.ValorBruto.Sum(),
                                              posicaoFundo.Query.ValorIOF.Sum(),
                                              posicaoFundo.Query.ValorIR.Sum());
                    posicaoFundo.Query.Where(posicaoFundo.Query.IdCliente.Equal(idCarteira),
                                             posicaoFundo.Query.IdCarteira.Equal(idFundo));
                    posicaoFundo.Query.Load();

                    if (posicaoFundo.es.HasData && posicaoFundo.ValorBruto.HasValue)
                    {
                        resultado += posicaoFundo.ValorBruto.Value;
                        resultadoIR += posicaoFundo.ValorIR.Value;
                        resultadoIOF += posicaoFundo.ValorIOF.Value;
                    }
                }

                Pessoa pessoa = new Pessoa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(pessoa.Query.Apelido);
                pessoa.LoadByPrimaryKey(campos, idFundo);

                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = (int)TiposMercados.Fundo;
                analiseResultados.DescricaoAtivo = pessoa.Apelido + " - Vl.Bruto";
                analiseResultados.DescricaoMercado = "Fundos de Investimentos";
                analiseResultados.PercentualPL = Math.Round(resultado / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = resultado;

                lista.Add(analiseResultados);

                if (lancaTributos)
                {
                    if (resultadoIR != 0)
                    {
                        resultadoIR *= -1;
                        analiseResultados = new AnaliseResultados();
                        analiseResultados.TipoMercado = (int)TiposMercados.FundoTributo;
                        analiseResultados.DescricaoAtivo = pessoa.Apelido + " - Vl.IR";
                        analiseResultados.DescricaoMercado = "Fundos de Investimentos - Tributos";
                        analiseResultados.PercentualPL = Math.Round(resultadoIR / valorPLAnterior * 100M, 4);
                        analiseResultados.ResultadoPeriodo = resultadoIR;

                        lista.Add(analiseResultados);
                    }


                    if (resultadoIOF != 0)
                    {
                        resultadoIOF *= -1;
                        analiseResultados = new AnaliseResultados();
                        analiseResultados.TipoMercado = (int)TiposMercados.FundoTributo;
                        analiseResultados.DescricaoAtivo = pessoa.Apelido + " - Vl.IOF";
                        analiseResultados.DescricaoMercado = "Fundos de Investimentos - Tributos";
                        analiseResultados.PercentualPL = Math.Round(resultadoIOF / valorPLAnterior * 100M, 4);
                        analiseResultados.ResultadoPeriodo = resultadoIOF;

                        lista.Add(analiseResultados);
                    }
                }
            }
            #endregion

            #region RendaFixa (Titulo Publico e Privado)
            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoCollection.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdTitulo);
            posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                        posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                        posicaoRendaFixaHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoRendaFixaHistoricoCollection.Query.es.Distinct = true;
            posicaoRendaFixaHistoricoCollection.Query.Load();

            List<int> listaRendaFixa = new List<int>();

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
            {
                listaRendaFixa.Add(posicaoRendaFixaHistorico.IdTitulo.Value);
            }

            if (historico)
            {
                posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdTitulo);
                posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                                posicaoRendaFixaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoRendaFixaHistoricoCollection.Query.es.Distinct = true;
                posicaoRendaFixaHistoricoCollection.Query.Load();

                foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
                {
                    if (!listaRendaFixa.Contains(posicaoRendaFixaHistorico.IdTitulo.Value))
                    {
                        listaRendaFixa.Add(posicaoRendaFixaHistorico.IdTitulo.Value);
                    }
                }
            }
            else
            {
                PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.IdTitulo);
                posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCarteira),
                                                   posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0));
                posicaoRendaFixaCollection.Query.es.Distinct = true;
                posicaoRendaFixaCollection.Query.Load();

                foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                {
                    if (!listaRendaFixa.Contains(posicaoRendaFixa.IdTitulo.Value))
                    {
                        listaRendaFixa.Add(posicaoRendaFixa.IdTitulo.Value);
                    }
                }
            }

            OperacaoRendaFixaCollection operacaoRendaFixaCollectionLista = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollectionLista.Query.Select(operacaoRendaFixaCollectionLista.Query.IdTitulo);
            operacaoRendaFixaCollectionLista.Query.Where(operacaoRendaFixaCollectionLista.Query.IdCliente.Equal(idCarteira),
                                                    operacaoRendaFixaCollectionLista.Query.DataOperacao.LessThanOrEqual(data),
                                                    operacaoRendaFixaCollectionLista.Query.DataOperacao.GreaterThan(dataHistorico));
            operacaoRendaFixaCollectionLista.Query.es.Distinct = true;
            operacaoRendaFixaCollectionLista.Query.Load();

            foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollectionLista)
            {
                if (!listaRendaFixa.Contains(operacaoRendaFixa.IdTitulo.Value))
                {
                    listaRendaFixa.Add(operacaoRendaFixa.IdTitulo.Value);
                }
            }

            foreach (int idTitulo in listaRendaFixa)
            {
                decimal resultado = 0;
                decimal resultadoIR = 0;
                decimal resultadoIOF = 0;
                OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
                operacaoRendaFixaCollection.Query.Select(operacaoRendaFixaCollection.Query.Valor.Sum(),                                                         
                                                         operacaoRendaFixaCollection.Query.ValorIR.Sum(),
                                                         operacaoRendaFixaCollection.Query.ValorIOF.Sum(),
                                                         operacaoRendaFixaCollection.Query.TipoOperacao);
                operacaoRendaFixaCollection.Query.Where(operacaoRendaFixaCollection.Query.IdCliente.Equal(idCarteira),
                                                    operacaoRendaFixaCollection.Query.IdTitulo.Equal(idTitulo),
                                                    operacaoRendaFixaCollection.Query.DataOperacao.LessThanOrEqual(data),
                                                    operacaoRendaFixaCollection.Query.DataOperacao.GreaterThan(dataHistorico));
                operacaoRendaFixaCollection.Query.GroupBy(operacaoRendaFixaCollection.Query.TipoOperacao);
                operacaoRendaFixaCollection.Query.Load();

                foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
                {                    
                    decimal valorBruto = operacaoRendaFixa.Valor.Value;
                    decimal valorIOF = operacaoRendaFixa.ValorIOF.Value;
                    decimal valorIR = operacaoRendaFixa.ValorIR.Value;
                    
                    if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal ||
                        operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda)
                    {
                        resultado -= valorBruto;
                    }
                    else if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaFinal ||
                             operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal ||   
                             operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra)
                    {
                        resultado += valorBruto;
                        resultadoIR += valorIR;
                        resultadoIOF += valorIOF;
                    }
                }

                PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                posicaoRendaFixaHistorico.Query.Select(posicaoRendaFixaHistorico.Query.ValorMercado.Sum(),
                                                       posicaoRendaFixaHistorico.Query.ValorIOF.Sum(),
                                                       posicaoRendaFixaHistorico.Query.ValorIR.Sum());
                posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdCliente.Equal(idCarteira),
                                                  posicaoRendaFixaHistorico.Query.DataHistorico.Equal(dataHistorico),
                                                  posicaoRendaFixaHistorico.Query.IdTitulo.Equal(idTitulo));
                posicaoRendaFixaHistorico.Query.Load();

                if (posicaoRendaFixaHistorico.es.HasData && posicaoRendaFixaHistorico.ValorMercado.HasValue)
                {
                    resultado -= posicaoRendaFixaHistorico.ValorMercado.Value;
                    resultadoIR -= posicaoRendaFixaHistorico.ValorIR.Value;
                    resultadoIOF -= posicaoRendaFixaHistorico.ValorIOF.Value;
                }

                if (historico)
                {
                    posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                    posicaoRendaFixaHistorico.Query.Select(posicaoRendaFixaHistorico.Query.ValorMercado.Sum(),
                                                           posicaoRendaFixaHistorico.Query.ValorIOF.Sum(),
                                                           posicaoRendaFixaHistorico.Query.ValorIR.Sum());
                    posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdCliente.Equal(idCarteira),
                                                          posicaoRendaFixaHistorico.Query.IdTitulo.Equal(idTitulo),
                                                          posicaoRendaFixaHistorico.Query.DataHistorico.Equal(data));
                    posicaoRendaFixaHistorico.Query.Load();

                    if (posicaoRendaFixaHistorico.es.HasData && posicaoRendaFixaHistorico.ValorMercado.HasValue)
                    {
                        resultado += posicaoRendaFixaHistorico.ValorMercado.Value;
                        resultadoIR += posicaoRendaFixaHistorico.ValorIR.Value;
                        resultadoIOF += posicaoRendaFixaHistorico.ValorIOF.Value;
                    }
                }
                else
                {
                    PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                    posicaoRendaFixa.Query.Select(posicaoRendaFixa.Query.ValorMercado.Sum(),
                                                  posicaoRendaFixa.Query.ValorIR.Sum(),
                                                  posicaoRendaFixa.Query.ValorIOF.Sum());
                    posicaoRendaFixa.Query.Where(posicaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                             posicaoRendaFixa.Query.IdTitulo.Equal(idTitulo));
                    posicaoRendaFixa.Query.Load();

                    if (posicaoRendaFixa.es.HasData && posicaoRendaFixa.ValorMercado.HasValue)
                    {
                        resultado += posicaoRendaFixa.ValorMercado.Value;
                        resultadoIR += posicaoRendaFixa.ValorIR.Value;
                        resultadoIOF += posicaoRendaFixa.ValorIOF.Value;
                    }
                }

                LiquidacaoRendaFixa liquidacaoRendaFixa = new LiquidacaoRendaFixa();
                liquidacaoRendaFixa.Query.Select(liquidacaoRendaFixa.Query.ValorBruto.Sum(),
                                                 liquidacaoRendaFixa.Query.ValorIOF.Sum(),
                                                 liquidacaoRendaFixa.Query.ValorIR.Sum());
                liquidacaoRendaFixa.Query.Where(liquidacaoRendaFixa.Query.IdCliente.Equal(idCarteira),
                                                liquidacaoRendaFixa.Query.DataLiquidacao.LessThanOrEqual(data),
                                                liquidacaoRendaFixa.Query.DataLiquidacao.GreaterThan(dataHistorico),
                                                liquidacaoRendaFixa.Query.IdTitulo.Equal(idTitulo),
                                                liquidacaoRendaFixa.Query.TipoLancamento.NotIn((byte)TipoLancamentoLiquidacao.Venda));
                liquidacaoRendaFixa.Query.Load();

                decimal valorLiquidacaoRF = 0;
                if (liquidacaoRendaFixa.ValorBruto.HasValue)
                {
                    valorLiquidacaoRF = liquidacaoRendaFixa.ValorBruto.Value;
                    resultado += valorLiquidacaoRF;
                    resultadoIR += liquidacaoRendaFixa.ValorIR.Value;
                    resultadoIOF += liquidacaoRendaFixa.ValorIOF.Value;
                }

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tituloRendaFixa.Query.IdPapel);
                campos.Add(tituloRendaFixa.Query.DescricaoCompleta);
                tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);

                PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                campos = new List<esQueryItem>();
                campos.Add(papelRendaFixa.Query.TipoPapel);
                papelRendaFixa.LoadByPrimaryKey(campos, tituloRendaFixa.IdPapel.Value);

                string descricaoMercado = "";
                int tipoMercado = 0;
                int tipoMercadoTributos = 0;
                
                if (papelRendaFixa.TipoPapel.Value == (byte)TipoPapelTitulo.Privado)
                {
                    descricaoMercado = "Títulos Privados";
                    tipoMercado = (int)TiposMercados.RendaFixaPrivado;
                    tipoMercadoTributos = (int)TiposMercados.RendaFixaPrivadoTributos;
                }
                else
                {
                    descricaoMercado = "Títulos Públicos";
                    tipoMercado = (int)TiposMercados.RendaFixaPublico;
                    tipoMercadoTributos = (int)TiposMercados.RendaFixaPublicoTributos;
                }

                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = tipoMercado;
                analiseResultados.DescricaoAtivo = tituloRendaFixa.DescricaoCompleta + " - Vl.Bruto";
                analiseResultados.DescricaoMercado = descricaoMercado;
                analiseResultados.PercentualPL = Math.Round(resultado / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = resultado;

                lista.Add(analiseResultados);

                if (lancaTributos)
                {
                    if (resultadoIR != 0)
                    {
                        resultadoIR *= -1;
                        analiseResultados = new AnaliseResultados();
                        analiseResultados.TipoMercado = tipoMercadoTributos;
                        analiseResultados.DescricaoAtivo = tituloRendaFixa.DescricaoCompleta + " - Vl.IR";
                        analiseResultados.DescricaoMercado = descricaoMercado + " - Tributos";
                        analiseResultados.PercentualPL = Math.Round(resultadoIR / valorPLAnterior * 100M, 4);
                        analiseResultados.ResultadoPeriodo = resultadoIR;

                        lista.Add(analiseResultados);
                    }

                    if (resultadoIOF != 0)
                    {
                        resultadoIOF *= -1;
                        analiseResultados = new AnaliseResultados();
                        analiseResultados.TipoMercado = tipoMercadoTributos;
                        analiseResultados.DescricaoAtivo = tituloRendaFixa.DescricaoCompleta + " - Vl.IOF";
                        analiseResultados.DescricaoMercado = descricaoMercado + " - Tributos";
                        analiseResultados.PercentualPL = Math.Round(resultadoIOF / valorPLAnterior * 100M, 4);
                        analiseResultados.ResultadoPeriodo = resultadoIOF;

                        lista.Add(analiseResultados);
                    }
                }
            }
            #endregion

            #region Administração
            carteiraConsolidadora = false;
            if (portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) > 0)
                carteiraConsolidadora = idCarteira == portfolioPadrao.IdCarteiraDespesasReceitas.Value;

            AnaliseResultados analiseResultadosAdmin = RetornaAnaliseResultadosGestaoAdmin(idCarteira, data, dataHistorico, valorPLAnterior, historico, carteiraConsolidadora, TipoCadastroAdministracao.TaxaAdministracao);
            if(!string.IsNullOrEmpty(analiseResultadosAdmin.DescricaoAtivo))
                lista.Add(analiseResultadosAdmin);
            #endregion

            #region Gestão
            carteiraConsolidadora = false;
            if (portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) > 0)
                carteiraConsolidadora = idCarteira == portfolioPadrao.IdCarteiraTaxaGestao.Value;

            AnaliseResultados analiseResultadosGestao = RetornaAnaliseResultadosGestaoAdmin(idCarteira, data, dataHistorico, valorPLAnterior, historico, carteiraConsolidadora, TipoCadastroAdministracao.TaxaGestao);
            if (!string.IsNullOrEmpty(analiseResultadosGestao.DescricaoAtivo))
                lista.Add(analiseResultadosGestao);
            #endregion

            #region Diferimento e Provisões
            carteiraConsolidadora = false;
            if (portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) > 0)
                carteiraConsolidadora = idCarteira == portfolioPadrao.IdCarteiraDespesasReceitas.Value;

            List<AnaliseResultados> lstAnaliseResultadosProvisao = this.RetornaAnaliseResultadosProvisao(idCarteira, data, dataHistorico, valorPLAnterior, historico, carteiraConsolidadora, TipoCalculoProvisao.Provisao);
            if (lstAnaliseResultadosProvisao.Count > 0)
                lista.AddRange(lstAnaliseResultadosProvisao);
            
            List<AnaliseResultados> lstAnaliseResultadosDiferimento = this.RetornaAnaliseResultadosProvisao(idCarteira, data, dataHistorico, valorPLAnterior, historico, carteiraConsolidadora, TipoCalculoProvisao.Diferido);
            if (lstAnaliseResultadosDiferimento.Count > 0)
                lista.AddRange(lstAnaliseResultadosDiferimento);
            #endregion

            #region Taxa Pfee
            decimal valorPfee = 0;
            carteiraConsolidadora = false;
            if (portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) > 0)
                carteiraConsolidadora = idCarteira == portfolioPadrao.IdCarteiraTaxaPerformance.Value;

            if (historico || carteiraConsolidadora)
            {
                CalculoPerformanceHistorico calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                calculoPerformanceHistorico.Query.Select(calculoPerformanceHistorico.Query.ValorDia.Sum());
                calculoPerformanceHistorico.Query.Where(calculoPerformanceHistorico.Query.DataHistorico.LessThanOrEqual(data),
                                                        calculoPerformanceHistorico.Query.DataHistorico.GreaterThan(dataHistorico));

                if (!carteiraConsolidadora)
                    calculoPerformanceHistorico.Query.Where(calculoPerformanceHistorico.Query.IdCarteira.Equal(idCarteira));

                calculoPerformanceHistorico.Query.Load();

                if (calculoPerformanceHistorico.ValorDia.HasValue)
                {
                    valorPfee = calculoPerformanceHistorico.ValorDia.Value * -1;

                    AnaliseResultados analiseResultados = new AnaliseResultados();
                    analiseResultados.TipoMercado = (int)TiposMercados.TaxaPfee;
                    analiseResultados.DescricaoAtivo = "Valor apropriado no período";
                    analiseResultados.DescricaoMercado = "Taxa Performance (após cálculo da cota)";
                    analiseResultados.PercentualPL = valorPLAnterior == 0 ? 0 : Math.Round(valorPfee / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = valorPfee;

                    lista.Add(analiseResultados);
                }
            }
            else
            {
                CalculoPerformance calculoPerformance = new CalculoPerformance();
                calculoPerformance.Query.Select(calculoPerformance.Query.ValorDia.Sum());
                calculoPerformance.Query.Where(calculoPerformance.Query.IdCarteira.Equal(idCarteira));

                calculoPerformance.Query.Load();
                
                if (calculoPerformance.ValorDia.HasValue)
                {
                    valorPfee += calculoPerformance.ValorDia.Value * -1;
                }

                CalculoPerformanceHistorico calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                calculoPerformanceHistorico.Query.Select(calculoPerformanceHistorico.Query.ValorDia.Sum());
                calculoPerformanceHistorico.Query.Where(calculoPerformanceHistorico.Query.DataHistorico.LessThan(data) &
                                                        calculoPerformanceHistorico.Query.DataHistorico.GreaterThan(dataHistorico) &
                                                        calculoPerformanceHistorico.Query.IdCarteira.Equal(idCarteira));

                calculoPerformanceHistorico.Query.Load();

                if (calculoPerformanceHistorico.ValorDia.HasValue)
                {
                    valorPfee += calculoPerformanceHistorico.ValorDia.Value * -1;
                }

                if (valorPfee > 0)
                {
                    AnaliseResultados analiseResultados = new AnaliseResultados();
                    analiseResultados.TipoMercado = (int)TiposMercados.TaxaPfee;
                    analiseResultados.DescricaoAtivo = "Valor apropriado no período";
                    analiseResultados.DescricaoMercado = "Taxa Performance (após cálculo da cota)";                    
                    analiseResultados.PercentualPL = valorPLAnterior == 0 ? 0 : Math.Round(valorPfee / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = valorPfee;

                    lista.Add(analiseResultados);
                }
            }

            valorPfee = 0;
            LiquidacaoCollection liqPfeeColl = new LiquidacaoCollection();
            liqPfeeColl.Query.Select(liqPfeeColl.Query.Valor.Sum());
            liqPfeeColl.Query.Where(liqPfeeColl.Query.DataLancamento.Equal(data) &
                          liqPfeeColl.Query.IdCliente.Equal(idCarteira) &
                          liqPfeeColl.Query.Origem.In((int)OrigemLancamentoLiquidacao.Provisao.TaxaPerformance, (int)OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance));


            if (liqPfeeColl.Query.Load())
            {
                valorPfee += liqPfeeColl[0].Valor.GetValueOrDefault(0);

                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = (int)TiposMercados.TaxaPfeeLancamento;
                analiseResultados.DescricaoAtivo = "Lançamento no dia";
                analiseResultados.DescricaoMercado = "Taxa Performance (após cálculo da cota)";
                analiseResultados.PercentualPL = valorPLAnterior == 0 ? 0 : Math.Round(valorPfee / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = valorPfee;

                lista.Add(analiseResultados);
            }
            
            #endregion

            #region Outros Lancamentos Sinacor/CMDF (que estão na data "atual" e não estão em d-1 ou estão em d-1 com valor diferente)
            if (historico)
            {
                LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
                liquidacaoHistoricoCollection.Query.Select(liquidacaoHistoricoCollection.Query.Descricao,
                                                        liquidacaoHistoricoCollection.Query.DataVencimento,
                                                        liquidacaoHistoricoCollection.Query.Valor.Sum());
                liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCarteira),
                                                       liquidacaoHistoricoCollection.Query.DataVencimento.GreaterThanOrEqual(data),
                                                       liquidacaoHistoricoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                                       liquidacaoHistoricoCollection.Query.Origem.Equal(OrigemLancamentoLiquidacao.Outros),
                                                       liquidacaoHistoricoCollection.Query.Fonte.NotIn((byte)FonteLancamentoLiquidacao.Interno, (byte)FonteLancamentoLiquidacao.Manual),
                                                       liquidacaoHistoricoCollection.Query.DataHistorico.Equal(data));
                liquidacaoHistoricoCollection.Query.GroupBy(liquidacaoHistoricoCollection.Query.Descricao,
                                                            liquidacaoHistoricoCollection.Query.DataVencimento);
                liquidacaoHistoricoCollection.Query.Load();
                liquidacaoCollection = new LiquidacaoCollection(liquidacaoHistoricoCollection);
            }
            else
            {
                liquidacaoCollection = new LiquidacaoCollection();
                liquidacaoCollection.Query.Select(liquidacaoCollection.Query.Descricao,
                                                liquidacaoCollection.Query.DataVencimento,
                                                liquidacaoCollection.Query.Valor.Sum());
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCarteira),
                                       liquidacaoCollection.Query.DataVencimento.GreaterThanOrEqual(data),
                                       liquidacaoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                       liquidacaoCollection.Query.Origem.Equal(OrigemLancamentoLiquidacao.Outros),
                                       liquidacaoCollection.Query.Fonte.NotIn((byte)FonteLancamentoLiquidacao.Interno, (byte)FonteLancamentoLiquidacao.Manual));
                liquidacaoCollection.Query.GroupBy(liquidacaoCollection.Query.Descricao,
                                                 liquidacaoCollection.Query.DataVencimento);
                liquidacaoCollection.Query.Load();
            }

            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                decimal valor = liquidacao.Valor.Value;

                LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();
                liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
                liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCarteira),
                                                liquidacaoHistorico.Query.DataHistorico.Equal(dataHistorico),
                                                liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(dataHistorico),
                                                liquidacaoHistorico.Query.Descricao.Equal(liquidacao.Descricao),
                                                liquidacaoHistorico.Query.DataVencimento.Equal(liquidacao.DataVencimento),
                                                liquidacaoHistorico.Query.Origem.Equal(OrigemLancamentoLiquidacao.Outros),
                                                liquidacaoHistorico.Query.Fonte.NotIn((byte)FonteLancamentoLiquidacao.Interno, (byte)FonteLancamentoLiquidacao.Manual));
                liquidacaoHistorico.Query.Load();

                decimal valorHistorico = 0;
                if (liquidacaoHistorico.Valor.HasValue)
                {
                    valorHistorico = liquidacaoHistorico.Valor.Value;
                }

                decimal diferenca = valor - valorHistorico;

                if (diferenca != 0)
                {
                    AnaliseResultados analiseResultados = new AnaliseResultados();
                    analiseResultados.TipoMercado = (int)TiposMercados.Outros;
                    analiseResultados.DescricaoAtivo = liquidacao.Descricao;
                    analiseResultados.DescricaoMercado = "Outros (Lançamentos Manuais)";
                    analiseResultados.PercentualPL = Math.Round(diferenca / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = diferenca;

                    lista.Add(analiseResultados);
                }
            }
            #endregion

            #region Outros Lancamentos Sinacor/CMDF (que estão na data d-1 e NãO estão na data atual)
            LiquidacaoHistoricoCollection liquidacaoHistoricoManualCollection = new LiquidacaoHistoricoCollection();
            liquidacaoHistoricoManualCollection.Query.Select(liquidacaoHistoricoManualCollection.Query.Descricao,
                                                    liquidacaoHistoricoManualCollection.Query.DataVencimento,
                                                    liquidacaoHistoricoManualCollection.Query.Origem,
                                                    liquidacaoHistoricoManualCollection.Query.Valor.Sum());
            liquidacaoHistoricoManualCollection.Query.Where(liquidacaoHistoricoManualCollection.Query.IdCliente.Equal(idCarteira),
                                                   liquidacaoHistoricoManualCollection.Query.DataHistorico.Equal(dataHistorico),
                                                   liquidacaoHistoricoManualCollection.Query.DataVencimento.GreaterThan(dataHistorico),
                                                   liquidacaoHistoricoManualCollection.Query.DataLancamento.LessThanOrEqual(dataHistorico),
                                                   liquidacaoHistoricoManualCollection.Query.Origem.Equal(OrigemLancamentoLiquidacao.Outros),
                                                   liquidacaoHistoricoManualCollection.Query.Fonte.NotIn((byte)FonteLancamentoLiquidacao.Interno, (byte)FonteLancamentoLiquidacao.Manual));
            liquidacaoHistoricoManualCollection.Query.GroupBy(liquidacaoHistoricoManualCollection.Query.Descricao,
                                                                 liquidacaoHistoricoManualCollection.Query.DataVencimento,
                                                                 liquidacaoHistoricoManualCollection.Query.Origem);
            liquidacaoHistoricoManualCollection.Query.Load();

            foreach (LiquidacaoHistorico liquidacaoHistorico in liquidacaoHistoricoManualCollection)
            {
                decimal valorHistorico = liquidacaoHistorico.Valor.Value;
                string descricao = liquidacaoHistorico.Descricao;

                decimal valor = 0;
                if (historico)
                {
                    LiquidacaoHistorico liquidacaoHistoricoManual = new LiquidacaoHistorico();
                    liquidacaoHistoricoManual.Query.Select(liquidacaoHistoricoManual.Query.Valor.Sum());
                    liquidacaoHistoricoManual.Query.Where(liquidacaoHistoricoManual.Query.IdCliente.Equal(idCarteira),
                                                    liquidacaoHistoricoManual.Query.DataHistorico.Equal(data),
                                                    liquidacaoHistoricoManual.Query.DataLancamento.LessThanOrEqual(data),
                                                    liquidacaoHistoricoManual.Query.Descricao.Equal(liquidacaoHistorico.Descricao),
                                                    liquidacaoHistoricoManual.Query.DataVencimento.Equal(liquidacaoHistorico.DataVencimento),
                                                    liquidacaoHistoricoManual.Query.Origem.Equal(liquidacaoHistorico.Origem.Value));
                    liquidacaoHistoricoManual.Query.Load();

                    if (liquidacaoHistoricoManual.Valor.HasValue)
                    {
                        valor = liquidacaoHistoricoManual.Valor.Value;
                    }
                }
                else
                {
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
                    liquidacao.Query.Where(liquidacao.Query.IdCliente.Equal(idCarteira),
                                        liquidacao.Query.DataLancamento.LessThanOrEqual(data),
                                        liquidacao.Query.Descricao.Equal(liquidacaoHistorico.Descricao),
                                        liquidacao.Query.DataVencimento.Equal(liquidacaoHistorico.DataVencimento),
                                        liquidacao.Query.Origem.Equal(liquidacaoHistorico.Origem.Value));
                    liquidacao.Query.Load();

                    if (liquidacao.Valor.HasValue)
                    {
                        valor = liquidacao.Valor.Value;
                    }
                }


                decimal diferenca = 0;
                if (valor == 0)
                {
                    diferenca = valorHistorico * -1;
                }

                if (diferenca != 0)
                {
                    AnaliseResultados analiseResultados = new AnaliseResultados();
                    analiseResultados.TipoMercado = (int)TiposMercados.Outros;
                    analiseResultados.DescricaoAtivo = descricao;
                    analiseResultados.DescricaoMercado = "Outros (Lançamentos Manuais)";
                    analiseResultados.PercentualPL = Math.Round(diferenca / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = diferenca;

                    lista.Add(analiseResultados);
                }
            }
            #endregion

            #region Lançamentos manuais D-0
            if (historico)
            {
                liquidacaoHistoricoManualCollection = new LiquidacaoHistoricoCollection();
                liquidacaoHistoricoManualCollection.Query.Select(liquidacaoHistoricoManualCollection.Query.Descricao,
                                                        liquidacaoHistoricoManualCollection.Query.DataVencimento,
                                                        liquidacaoHistoricoManualCollection.Query.Origem,
                                                        liquidacaoHistoricoManualCollection.Query.Valor.Sum());
                liquidacaoHistoricoManualCollection.Query.Where(liquidacaoHistoricoManualCollection.Query.IdCliente.Equal(idCarteira),
                                                       liquidacaoHistoricoManualCollection.Query.DataHistorico.Equal(data),
                                                       liquidacaoHistoricoManualCollection.Query.DataLancamento.Equal(data),
                                                       liquidacaoHistoricoManualCollection.Query.Origem.In(OrigemLancamentoLiquidacao.Outros),
                                                       liquidacaoHistoricoManualCollection.Query.Fonte.In((byte)FonteLancamentoLiquidacao.Manual));
                liquidacaoHistoricoManualCollection.Query.GroupBy(liquidacaoHistoricoManualCollection.Query.Descricao,
                                                                     liquidacaoHistoricoManualCollection.Query.DataVencimento,
                                                                     liquidacaoHistoricoManualCollection.Query.Origem);

                if (liquidacaoHistoricoManualCollection.Query.Load())
                {
                    foreach (LiquidacaoHistorico liquidacao in liquidacaoHistoricoManualCollection)
                    {
                        decimal valor = liquidacao.Valor.GetValueOrDefault(0);
                        AnaliseResultados analiseResultados = new AnaliseResultados();
                        analiseResultados.TipoMercado = (int)TiposMercados.Outros;
                        analiseResultados.DescricaoAtivo = liquidacao.Descricao + "  - Dt.Vencto: " + liquidacao.DataVencimento.Value.ToString("dd/MM/yyyy");
                        analiseResultados.DescricaoMercado = "Outros (Lançamentos Manuais)";
                        analiseResultados.PercentualPL = valorPLAnterior == 0 ? 0 : Math.Round(valor / valorPLAnterior * 100M, 4);
                        analiseResultados.ResultadoPeriodo = valor;

                        lista.Add(analiseResultados);
                    }
                }
            }
            else
            {
                LiquidacaoCollection liquidacaoManualCollection = new LiquidacaoCollection();
                liquidacaoManualCollection.Query.Select(liquidacaoManualCollection.Query.Descricao,
                                                        liquidacaoManualCollection.Query.DataVencimento,
                                                        liquidacaoManualCollection.Query.Origem,
                                                        liquidacaoManualCollection.Query.Valor.Sum());
                liquidacaoManualCollection.Query.Where(liquidacaoManualCollection.Query.IdCliente.Equal(idCarteira),
                                                       liquidacaoManualCollection.Query.DataLancamento.GreaterThan(dataHistorico),
                                                       liquidacaoManualCollection.Query.DataLancamento.LessThanOrEqual(data),
                                                       liquidacaoManualCollection.Query.Origem.In(OrigemLancamentoLiquidacao.Outros),
                                                       liquidacaoManualCollection.Query.Fonte.In((byte)FonteLancamentoLiquidacao.Manual));
                liquidacaoManualCollection.Query.GroupBy(liquidacaoManualCollection.Query.Descricao,
                                                                     liquidacaoManualCollection.Query.DataVencimento,
                                                                     liquidacaoManualCollection.Query.Origem);
                if (liquidacaoManualCollection.Query.Load())
                {
                    foreach (Liquidacao liquidacao in liquidacaoManualCollection)
                    {
                        decimal valor = liquidacao.Valor.GetValueOrDefault(0);
                        AnaliseResultados analiseResultados = new AnaliseResultados();
                        analiseResultados.TipoMercado = (int)TiposMercados.Outros;
                        analiseResultados.DescricaoAtivo = liquidacao.Descricao + "  - Dt.Vencto: " + liquidacao.DataVencimento.Value.ToString("dd/MM/yyyy");
                        analiseResultados.DescricaoMercado = "Outros (Lançamentos Manuais)";
                        analiseResultados.PercentualPL = valorPLAnterior == 0 ? 0 : Math.Round(valor / valorPLAnterior * 100M, 4);
                        analiseResultados.ResultadoPeriodo = valor;

                        lista.Add(analiseResultados);
                    }
                }
            }

            #endregion

            #region Aplicação/Resgate
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.ValorBruto.Sum(),
                                                   operacaoCotistaCollection.Query.TipoOperacao);
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                  operacaoCotistaCollection.Query.DataConversao.LessThanOrEqual(data),
                                                  operacaoCotistaCollection.Query.DataConversao.GreaterThan(dataHistorico));
            operacaoCotistaCollection.Query.GroupBy(operacaoCotistaCollection.Query.TipoOperacao);
            operacaoCotistaCollection.Query.Load();

            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                AnaliseResultados analiseResultados = new AnaliseResultados();
                if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao)
                {
                    decimal valor = operacaoCotista.ValorBruto.Value;

                    analiseResultados.TipoMercado = (int)TiposMercados.Aplicacao;
                    analiseResultados.DescricaoAtivo = "Total Diário";
                    analiseResultados.DescricaoMercado = "Aplicações do Período (após cálculo da cota)";
                    analiseResultados.PercentualPL = Math.Round(valor / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = valor;

                    lista.Add(analiseResultados);
                }
                else if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateBruto ||
                         operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateCotas ||
                         operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateLiquido ||
                         operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateTotal)
                {
                    decimal valor = operacaoCotista.ValorBruto.Value * -1;

                    analiseResultados.TipoMercado = (int)TiposMercados.Resgate;
                    analiseResultados.DescricaoAtivo = "Total Diário";
                    analiseResultados.DescricaoMercado = "Resgates do Período (após cálculo da cota)";
                    analiseResultados.PercentualPL = Math.Round(valor / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = valor;

                    lista.Add(analiseResultados);
                }
            }
            #endregion

            return lista.FindAll(delegate(AnaliseResultados x) { return x.ResultadoPeriodo != 0; });
        }


        /// <summary>
        /// Calcula o valor total da carteira em Bolsa (incluindo termos e empréstimos de ações), 
        /// BMF, RendaFixa, Swap, Fundos.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataReferencia"></param>
        /// <returns>valor total da carteira</returns>
        public decimal CalculaCarteiraTotal(int idCarteira, DateTime dataReferencia, int idMoeda)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.ApuraGanhoRV);
            campos.Add(cliente.Query.DescontaTributoPL);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            PosicaoBolsa posicaoBolsa = new PosicaoBolsa(); 
            decimal valorBolsa = posicaoBolsa.RetornaValorMercado(idCarteira, idMoeda, dataReferencia);

            PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
            decimal valorTermoBolsa = posicaoTermoBolsa.RetornaValorMercado(idCarteira);

            posicaoTermoBolsa = new PosicaoTermoBolsa();
            decimal rendimentoApropriarTermoBolsa = posicaoTermoBolsa.RetornaRendimentoApropriar(idCarteira);

            //Atenção a este parâmetro, pois se o BTC não altera a custódia de ações, na contagem do PL a parte de BTC tb não pode computar
            decimal valorEmprestimoDoador = 0;
            decimal valorEmprestimoTomador = 0;
            if (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraTodas ||
                (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.NaoApura))
            {
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsaDoador = new PosicaoEmprestimoBolsa();
                valorEmprestimoDoador = posicaoEmprestimoBolsaDoador.RetornaValorMercado(idCarteira, Financial.Bolsa.Enums.PontaEmprestimoBolsa.Doador);

                PosicaoEmprestimoBolsa posicaoEmprestimoBolsaTomador = new PosicaoEmprestimoBolsa();
                valorEmprestimoTomador = posicaoEmprestimoBolsaTomador.RetornaValorMercado(idCarteira, Financial.Bolsa.Enums.PontaEmprestimoBolsa.Tomador);
            }

            PosicaoBMF posicaoBMF = new PosicaoBMF();
            decimal valorBmf = posicaoBMF.RetornaValorMercado(idCarteira, idMoeda, dataReferencia);

            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            PosicaoFundo posicaoFundo = new PosicaoFundo();
            decimal valorRendaFixa = 0;
            decimal valorFundo = 0;

            if (cliente.DescontaTributoPL.Value == (byte)DescontoPLCliente.PLComDesconto)
            {
                valorRendaFixa = posicaoRendaFixa.RetornaValorMercadoLiquido(idCarteira, idMoeda, dataReferencia);
                valorFundo = posicaoFundo.RetornaValorMercadoLiquido(idCarteira, idMoeda, dataReferencia);
            }
            else
            {
                valorRendaFixa = posicaoRendaFixa.RetornaValorMercado(idCarteira, idMoeda, dataReferencia);
                valorFundo = posicaoFundo.RetornaValorMercado(idCarteira, idMoeda, dataReferencia);
            }

            PosicaoSwap posicaoSwap = new PosicaoSwap();
            decimal valorSwap = posicaoSwap.RetornaValorSaldoTotal(idCarteira);

            #region Tratamento multi-moeda para o resto dos mercados (hoje assume que os ativos estão sempre em reais)
            if (idMoeda != (int)ListaMoedaFixo.Real)
            {
                //Pega a ptax para agilizar, se o cliente estiver em dólar
                decimal ptax = 0;
                decimal fatorConversao = 0;
                if (idMoeda == (int)ListaMoedaFixo.Dolar)
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, dataReferencia);
                    fatorConversao = 1 / ptax;
                }
                else
                {
                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoeda),
                                               conversaoMoeda.Query.IdMoedaPara.Equal((int)ListaMoedaFixo.Real));
                    if (conversaoMoeda.Query.Load())
                    {
                        int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();
                        fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, dataReferencia);
                    }
                }

                valorTermoBolsa = Math.Round(valorTermoBolsa * fatorConversao, 2);
                rendimentoApropriarTermoBolsa = Math.Round(rendimentoApropriarTermoBolsa * fatorConversao, 2);
                valorEmprestimoDoador = Math.Round(valorEmprestimoDoador * fatorConversao, 2);
                valorEmprestimoTomador = Math.Round(valorEmprestimoTomador * fatorConversao, 2);
                valorBmf = Math.Round(valorBmf * fatorConversao, 2);
                valorSwap = Math.Round(valorSwap * fatorConversao, 2);
            }
            #endregion

            decimal valorCarteira = Math.Round(valorBolsa + valorTermoBolsa - rendimentoApropriarTermoBolsa + valorEmprestimoDoador +
                                               (valorEmprestimoTomador * -1) +
                                               valorBmf + valorRendaFixa +
                                               valorSwap + valorFundo, 2);

            return valorCarteira;
            

        }

        /// <summary>
        /// Calcula o valor total da carteira em Bolsa (incluindo termos e empréstimos de ações), 
        /// BMF, RendaFixa, Swap, Fundos.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataReferencia"></param>
        /// <returns>valor total da carteira</returns>
        public decimal CalculaCarteiraTotalHistorico(int idCarteira, DateTime dataReferencia, int idMoeda)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.ApuraGanhoRV);
            campos.Add(cliente.Query.DescontaTributoPL);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
            decimal valorBolsa = posicaoBolsa.RetornaValorMercado(idCarteira, idMoeda, dataReferencia);

            PosicaoTermoBolsaHistorico posicaoTermoBolsa = new PosicaoTermoBolsaHistorico();
            decimal valorTermoBolsa = posicaoTermoBolsa.RetornaValorMercadoSum(idCarteira,dataReferencia);

            posicaoTermoBolsa = new PosicaoTermoBolsaHistorico();
            decimal rendimentoApropriarTermoBolsa = posicaoTermoBolsa.RetornaRendimentoApropriar(idCarteira,dataReferencia);

            //Atenção a este parâmetro, pois se o BTC não altera a custódia de ações, na contagem do PL a parte de BTC tb não pode computar
            decimal valorEmprestimoDoador = 0;
            decimal valorEmprestimoTomador = 0;
            if (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraTodas ||
                (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.NaoApura))
            {
                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaDoador = new PosicaoEmprestimoBolsaHistorico();
                valorEmprestimoDoador = posicaoEmprestimoBolsaDoador.RetornaValorMercado(idCarteira, Financial.Bolsa.Enums.PontaEmprestimoBolsa.Doador , dataReferencia);

                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaTomador = new PosicaoEmprestimoBolsaHistorico();
                valorEmprestimoTomador = posicaoEmprestimoBolsaTomador.RetornaValorMercado(idCarteira, Financial.Bolsa.Enums.PontaEmprestimoBolsa.Tomador , dataReferencia);
            }

            PosicaoBMFHistorico posicaoBMF = new PosicaoBMFHistorico();
            decimal valorBmf = posicaoBMF.RetornaValorMercado(idCarteira, idMoeda, dataReferencia);

            PosicaoRendaFixaHistorico posicaoRendaFixa = new PosicaoRendaFixaHistorico();
            PosicaoFundoHistorico posicaoFundo = new PosicaoFundoHistorico();
            decimal valorRendaFixa = 0;
            decimal valorFundo = 0;

            if (cliente.DescontaTributoPL.Value == (byte)DescontoPLCliente.PLComDesconto)
            {
                valorRendaFixa = posicaoRendaFixa.RetornaValorMercadoLiquido(idCarteira, idMoeda, dataReferencia);
                valorFundo = posicaoFundo.RetornaValorMercadoLiquido(idCarteira, idMoeda, dataReferencia);
            }
            else
            {
                valorRendaFixa = posicaoRendaFixa.RetornaValorMercado(idCarteira, idMoeda, dataReferencia);
                valorFundo = posicaoFundo.RetornaValorMercado(idCarteira, idMoeda, dataReferencia);
            }

            PosicaoSwapHistorico posicaoSwap = new PosicaoSwapHistorico();
            decimal valorSwap = posicaoSwap.RetornaValorSaldoTotal(idCarteira , dataReferencia);

            #region Tratamento multi-moeda para o resto dos mercados (hoje assume que os ativos estão sempre em reais)
            if (idMoeda != (int)ListaMoedaFixo.Real)
            {
                //Pega a ptax para agilizar, se o cliente estiver em dólar
                decimal ptax = 0;
                decimal fatorConversao = 0;
                if (idMoeda == (int)ListaMoedaFixo.Dolar)
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, dataReferencia);
                    fatorConversao = 1 / ptax;
                }
                else
                {
                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoeda),
                                               conversaoMoeda.Query.IdMoedaPara.Equal((int)ListaMoedaFixo.Real));
                    if (conversaoMoeda.Query.Load())
                    {
                        int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();
                        fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, dataReferencia);
                    }
                }

                valorTermoBolsa = Math.Round(valorTermoBolsa * fatorConversao, 2);
                rendimentoApropriarTermoBolsa = Math.Round(rendimentoApropriarTermoBolsa * fatorConversao, 2);
                valorEmprestimoDoador = Math.Round(valorEmprestimoDoador * fatorConversao, 2);
                valorEmprestimoTomador = Math.Round(valorEmprestimoTomador * fatorConversao, 2);
                valorBmf = Math.Round(valorBmf * fatorConversao, 2);
                valorSwap = Math.Round(valorSwap * fatorConversao, 2);
            }
            #endregion

            decimal valorCarteira = Math.Round(valorBolsa + valorTermoBolsa - rendimentoApropriarTermoBolsa + valorEmprestimoDoador +
                                               (valorEmprestimoTomador * -1) +
                                               valorBmf + valorRendaFixa +
                                               valorSwap + valorFundo, 2);

            return valorCarteira;


        }

        /// <summary>
        /// Calcula o valor total da carteira em Bolsa (incluindo termos e empréstimos de ações), 
        /// BMF, RendaFixa, Swap, Fundos.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>valor total da carteira</returns>
        public decimal CalculaCarteiraTotalAbertura(int idCarteira, DateTime dataHistorico, int idMoeda)
        {
            PosicaoBolsaAbertura posicaoBolsaAbertura = new PosicaoBolsaAbertura();

            //Tratamento multi-moeda
            decimal valorBolsa = idMoeda != (int)ListaMoedaFixo.Real
                                    ? posicaoBolsaAbertura.RetornaValorMercado(idCarteira, idMoeda, dataHistorico)
                                    : posicaoBolsaAbertura.RetornaValorMercado(idCarteira, dataHistorico);

            PosicaoTermoBolsaAbertura posicaoTermoBolsaAbertura = new PosicaoTermoBolsaAbertura();
            decimal valorTermoBolsa = posicaoTermoBolsaAbertura.RetornaValorMercado(idCarteira, dataHistorico);

            posicaoTermoBolsaAbertura = new PosicaoTermoBolsaAbertura();
            decimal rendimentoApropriarTermoBolsa = posicaoTermoBolsaAbertura.RetornaRendimentoApropriar(idCarteira, dataHistorico);

            PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsaDoador = new PosicaoEmprestimoBolsaAbertura();
            decimal valorEmprestimoDoador = posicaoEmprestimoBolsaDoador.RetornaValorMercado(idCarteira, dataHistorico, PontaEmprestimoBolsa.Doador);

            PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsaTomador = new PosicaoEmprestimoBolsaAbertura();
            decimal valorEmprestimoTomador = posicaoEmprestimoBolsaTomador.RetornaValorMercado(idCarteira, dataHistorico, PontaEmprestimoBolsa.Tomador);

            PosicaoBMFAbertura posicaoBMF = new PosicaoBMFAbertura();
            decimal valorBmf = posicaoBMF.RetornaValorMercado(idCarteira, dataHistorico);

            PosicaoRendaFixaAbertura posicaoRendaFixa = new PosicaoRendaFixaAbertura();
            PosicaoFundoAbertura posicaoFundo = new PosicaoFundoAbertura();

            decimal valorRendaFixa = 0;
            decimal valorFundo = 0;

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DescontaTributoPL);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            if (cliente.DescontaTributoPL.Value == (byte)DescontoPLCliente.PLComDesconto)
            {
                valorRendaFixa = posicaoRendaFixa.RetornaValorMercadoLiquido(idCarteira, dataHistorico);
                valorFundo = posicaoFundo.RetornaValorMercadoLiquido(idCarteira, idMoeda, dataHistorico);
            }
            else
            {
                valorRendaFixa = posicaoRendaFixa.RetornaValorMercado(idCarteira, dataHistorico);
                valorFundo = posicaoFundo.RetornaValorMercado(idCarteira, idMoeda, dataHistorico);
            }

            PosicaoSwapAbertura posicaoSwap = new PosicaoSwapAbertura();
            decimal valorSwap = posicaoSwap.RetornaValorSaldoTotal(idCarteira, dataHistorico);

            #region Tratamento multi-moeda para o resto dos mercados (hoje assume que os ativos estão sempre em reais)
            if (idMoeda != (int)ListaMoedaFixo.Real)
            {
                //Pega a ptax para agilizar, se o cliente estiver em dólar
                decimal ptax = 0;
                decimal fatorConversao = 0;
                if (idMoeda == (int)ListaMoedaFixo.Dolar)
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, dataHistorico);
                    fatorConversao = 1 / ptax;
                }
                else
                {
                    ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                    conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoeda),
                                               conversaoMoeda.Query.IdMoedaPara.Equal((int)ListaMoedaFixo.Real));
                    if (conversaoMoeda.Query.Load())
                    {
                        int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                        CotacaoIndice cotacaoIndice = new CotacaoIndice();
                        fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, dataHistorico);
                    }
                }

                valorTermoBolsa = Math.Round(valorTermoBolsa * fatorConversao, 2);
                rendimentoApropriarTermoBolsa = Math.Round(rendimentoApropriarTermoBolsa * fatorConversao, 2);
                valorEmprestimoDoador = Math.Round(valorEmprestimoDoador * fatorConversao, 2);
                valorEmprestimoTomador = Math.Round(valorEmprestimoTomador * fatorConversao, 2);
                valorBmf = Math.Round(valorBmf * fatorConversao, 2);
                valorRendaFixa = Math.Round(valorRendaFixa * fatorConversao, 2);
                valorSwap = Math.Round(valorSwap * fatorConversao, 2);
            }
            #endregion

            decimal valorCarteira = Math.Round(valorBolsa + valorTermoBolsa - rendimentoApropriarTermoBolsa + valorEmprestimoDoador +
                                               (valorEmprestimoTomador * -1) +
                                               valorBmf + valorRendaFixa +
                                               valorSwap + valorFundo, 2);

            return valorCarteira;
        }

        /// <summary>
        /// Calcula a qtde de cotas, a partir do somatório de cotas em PosicaoCotista.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void CalculaQuantidadeCotas(int idCarteira, DateTime data)
        {
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            decimal totalCotas = posicaoCotista.RetornaTotalCotas(idCarteira);

            HistoricoCota historicoCota = new HistoricoCota();
            if (historicoCota.BuscaQuantidadeCotas(idCarteira, data))
            {
                historicoCota.QuantidadeFechamento = totalCotas;
                historicoCota.Save();
            }

        }

        /// <summary>
        /// Carrega o objeto HistoricoCota com os campos IdCarteira, Data, QuantidadeFechamento.
        /// HistoricoCotaNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        public bool BuscaQuantidadeCotas(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira, this.Query.Data, this.Query.QuantidadeFechamento)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Data.Equal(data));

            return this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto HistoricoCota com os campos CotaAbertura, CotaFechamento, CotaBruta, QuantidadeFechamento, Data.
        /// HistoricoCotaNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        public void BuscaValorCotaDia(int idCarteira, DateTime data)
        {
            this.QueryReset();

            TravamentoCotas travamentoCotas = new TravamentoCotas();
            travamentoCotas.Query.Where(travamentoCotas.Query.IdCarteira.Equal(idCarteira)
                                        & travamentoCotas.Query.DataProcessamento.Equal(data));

            if (travamentoCotas.Query.Load())
            {
                this.CotaFechamento = travamentoCotas.ValorCota.Value;
                this.CotaAbertura = travamentoCotas.ValorCota.Value;
                this.CotaBruta = travamentoCotas.ValorCota.Value;

                return;
            }
            
            this.Query
                 .Select(this.Query.CotaAbertura, this.Query.CotaFechamento, this.Query.CotaBruta,
                         this.Query.QuantidadeFechamento, this.Query.Data)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Data.Equal(data));

            if (!this.Query.Load())
            {

                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                //Verifica se deve buscar cota em outra data
                if (carteira.BuscaCotaAnterior.Equals("S"))
                {
                    bool fundoOffShore = false;
                    #region Verifica se é fundo offshore
                    Cliente cliente = new Cliente();
                    if (cliente.LoadByPrimaryKey(carteira.IdCarteira.Value))
                        fundoOffShore = cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PJ || cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PF;
                    #endregion

                    this.QueryReset();
                    this.Query
                         .Select(this.Query.CotaAbertura, this.Query.CotaFechamento, this.Query.CotaBruta, this.Query.QuantidadeFechamento, this.Query.Data)
                         .Where(this.Query.IdCarteira == idCarteira & this.Query.Data.LessThan(data));

                    if (!fundoOffShore)
                        this.Query.Where(this.Query.Data.GreaterThanOrEqual(data.AddDays(-carteira.NumeroDiasBuscaCota.Value)));

                    this.Query.OrderBy(this.Query.Data.Descending);
                    this.Query.es.Top = 1;
                    if (!this.Query.Load())
                    {
                        StringBuilder mensagem = new StringBuilder();
                        mensagem.Append("Cota não cadastrada na data ")
                                .Append(data + " para a carteira " + idCarteira + " - " + carteira.Apelido);
                        throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
                    }

                }
                else
                {
                    StringBuilder mensagem = new StringBuilder();
                    mensagem.Append("Cota não cadastrada na data ")
                            .Append(data + " para a carteira " + idCarteira + " - " + carteira.Apelido);
                    throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
                }
            }            
        }

        /// <summary>
        /// Carrega o objeto HistoricoCota com os campos CotaAbertura, CotaFechamento. Leva em conta eventos de incorporação.
        /// HistoricoCotaNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        public void BuscaValorCotaDia(int idCarteira, DateTime data, DateTime dataCalculo)
        {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            int idTipoCliente = carteira.UpToCliente.IdTipo.Value;
            if (idTipoCliente == (int)TipoClienteFixo.OffShore_PF || idTipoCliente == (int)TipoClienteFixo.OffShore_PJ)
            {
                this.BuscaValorCotaOffShore(carteira, data, dataCalculo);
                return;
            }


            IncorporacaoFundoCollection incorporacaoFundoCollection = new IncorporacaoFundoCollection();
            incorporacaoFundoCollection.Query.Select(incorporacaoFundoCollection.Query.IdCarteiraOrigem,
                                                     incorporacaoFundoCollection.Query.Data);
            incorporacaoFundoCollection.Query.Where(incorporacaoFundoCollection.Query.IdCarteiraDestino.Equal(idCarteira),
                                                    incorporacaoFundoCollection.Query.Data.GreaterThanOrEqual(data),
                                                    incorporacaoFundoCollection.Query.Data.LessThanOrEqual(dataCalculo));
            incorporacaoFundoCollection.Query.Load();

            if (incorporacaoFundoCollection.Count > 0)
            {
                int idCarteiraOrigem = incorporacaoFundoCollection[0].IdCarteiraOrigem.Value;
                DateTime dataIncorporacao = incorporacaoFundoCollection[0].Data.Value;

                this.BuscaValorCotaDia(idCarteiraOrigem, data);
                decimal cotaInicialFechamento = this.CotaFechamento.Value;
                decimal cotaInicialAbertura = this.CotaAbertura.Value;

                this.BuscaValorCotaDia(idCarteiraOrigem, dataIncorporacao);
                decimal cotaIncorporadaFechamento = this.CotaFechamento.Value;
                decimal cotaIncorporadaAbertura = this.CotaAbertura.Value;

                this.BuscaValorCotaDia(idCarteira, dataIncorporacao);
                decimal cotaIncorporadoraFechamento = this.CotaFechamento.Value;
                decimal cotaIncorporadoraAbertura = this.CotaAbertura.Value;

                decimal cotaFechamento = cotaIncorporadoraFechamento / (cotaIncorporadaFechamento / cotaInicialFechamento);
                decimal cotaAbertura = cotaIncorporadoraAbertura / (cotaIncorporadaAbertura / cotaInicialAbertura);

                this.CotaFechamento = cotaFechamento;
                this.CotaAbertura = cotaAbertura;
            }
            else
            {
                this.BuscaValorCotaDia(idCarteira, data);
            }
        }

        public void BuscaValorCotaOffShore(Carteira carteira, DateTime data, DateTime dataCalculo)
        {
            decimal valorCotaBase = 1;
            decimal valorAquisicao = 0;
            decimal valorCalculo = 0;

            StringBuilder mensagem = new StringBuilder();
            
            SeriesOffShore serieAquisicao = new SeriesOffShore();
            serieAquisicao.Query.Where(serieAquisicao.Query.IdClassesOffShore.Equal((int)carteira.IdCarteira.Value));
            serieAquisicao.Query.Where(serieAquisicao.Query.DataInicioAplicacao.LessThanOrEqual(dataCalculo), serieAquisicao.Query.DataFimAplicacao.GreaterThanOrEqual(dataCalculo));
            if (serieAquisicao.Query.Load() && serieAquisicao.VlCotaBase.HasValue)
                valorCotaBase = serieAquisicao.VlCotaBase.Value;

            this.BuscaValorCotaDia(carteira.IdCarteira.Value, data);
            if (this.CotaAbertura.HasValue)
                valorAquisicao = this.CotaAbertura.Value;
            else
            {
                mensagem.Append("Cota não cadastrada na data " + data + " para a carteira " + carteira.IdCarteira.Value + " - " + carteira.Apelido);
                throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
            }

            this.BuscaValorCotaDia(carteira.IdCarteira.Value, dataCalculo);
            if (this.CotaAbertura.HasValue)
                valorCalculo = this.CotaAbertura.Value;
            else
            {
                mensagem.Append("Cota não cadastrada na data " + dataCalculo + " para a carteira " + carteira.IdCarteira.Value + " - " + carteira.Apelido);
                throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
            }

            this.CotaAbertura = (valorCalculo / valorAquisicao) * valorCotaBase;
            this.CotaFechamento = this.CotaAbertura;
            

            this.BuscaValorCotaDia(carteira.IdCarteira.Value, data);
            if (this.CotaAbertura.HasValue)
                this.CotaAbertura= this.CotaAbertura.Value;
            else
            {
                mensagem.Append("Cota não cadastrada na data " + data + " para a carteira " + carteira.IdCarteira.Value + " - " + carteira.Apelido);
                throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
            }

            this.BuscaValorCotaDia(carteira.IdCarteira.Value, dataCalculo);
            if (this.CotaAbertura.HasValue)
                this.CotaAbertura = this.CotaAbertura.Value;
            else
            {
                mensagem.Append("Cota não cadastrada na data " + dataCalculo + " para a carteira " + carteira.IdCarteira.Value + " - " + carteira.Apelido);
                throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
            }

            this.CotaAbertura = (valorCalculo / valorAquisicao) * valorCotaBase;
            this.CotaFechamento = this.CotaAbertura;

        }

        /// <summary>
        /// Carrega o objeto HistoricoCota com os campos PLAbertura, PLFechamento, PatrimonioBruto.
        /// HistoricoCotaNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        public void BuscaValorPatrimonioDia(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.PLAbertura, this.query.PLFechamento, this.query.PatrimonioBruto)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Data.Equal(data));

            if (!this.Query.Load())
            {
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.Apelido);
                carteira.LoadByPrimaryKey(campos, idCarteira);

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Patrimônio não cadastrado na data ")
                        .Append(data + " para a carteira " + idCarteira + " - " + carteira.Apelido);
                throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
            }
        }

        /// <summary>
        /// Carrega o objeto HistoricoCota com os campos CotaAbertura, CotaFechamento, CotaBruta.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        /// <returns>booleando indicando se achou registro</returns>
        public bool BuscaValorCota(int idCarteira, DateTime data)
        {
            this.QueryReset();

            this.Query
                 .Select(this.Query.CotaAbertura, this.Query.CotaFechamento, this.Query.CotaBruta, this.Query.CotaRendimento)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Data.Equal(data));

            bool retorno = this.Query.Load();

            TravamentoCotas travamentoCotas = new TravamentoCotas();
            travamentoCotas.Query.Where(travamentoCotas.Query.IdCarteira.Equal(idCarteira)
                                        & travamentoCotas.Query.DataProcessamento.Equal(data));

            if (travamentoCotas.Query.Load())
            {
                this.CotaFechamento = travamentoCotas.ValorCota.Value;
                this.CotaAbertura = travamentoCotas.ValorCota.Value;
                this.CotaBruta = travamentoCotas.ValorCota.Value;

                return true;
            }

            if (!retorno)
            {
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                //Verifica se deve buscar cota em outra data
                if (carteira.BuscaCotaAnterior.Equals("S"))
                {
                    bool fundoOffShore = false;
                    #region Verifica se é fundo offshore
                    Cliente cliente = new Cliente();
                    if (cliente.LoadByPrimaryKey(carteira.IdCarteira.Value))
                        fundoOffShore = cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PF || cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PJ;
                    #endregion

                    this.QueryReset();
                    this.Query
                         .Select(this.Query.CotaAbertura, this.Query.CotaFechamento, this.Query.CotaBruta, this.Query.CotaRendimento)
                         .Where(this.Query.IdCarteira == idCarteira & this.Query.Data.LessThan(data));

                    if (!fundoOffShore)
                        this.Query.Where(this.Query.Data.GreaterThanOrEqual(data.AddDays(-carteira.NumeroDiasBuscaCota.Value)));

                    this.Query.OrderBy(this.Query.Data.Descending);
                    this.Query.es.Top = 1;
                    retorno = this.Query.Load();
                }
            }

            return retorno;
        }

        public DateTime? BuscaDataMinCota(int idCarteira)
        {
            DateTime? dataMinCota = null;
            this.QueryReset();
            this.Query
                .Select(this.Query.Data.Min())
                .Where(this.Query.IdCarteira == idCarteira);


            object objDataMinCota = this.Query.ExecuteScalar();
            if (objDataMinCota != System.DBNull.Value)
            {
                dataMinCota = Convert.ToDateTime(objDataMinCota);
            }

            return dataMinCota;
        }

        public DateTime? BuscaDataMaxCota(int idCarteira)
        {
            DateTime? dataMaxCota = null;
            this.QueryReset();
            this.Query
                .Select(this.Query.Data.Max())
                .Where(this.Query.IdCarteira == idCarteira);


            object objDataMaxCota = this.Query.ExecuteScalar();
            if (objDataMaxCota != System.DBNull.Value)
            {
                dataMaxCota = Convert.ToDateTime(objDataMaxCota);
            }

            return dataMaxCota;
        }

        /// <summary>
        /// Carrega o objeto HistoricoCota com os campos CotaAbertura, CotaFechamento.
        /// Busca pela data da cota mais próxima à frente.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        /// <returns>booleando indicando se achou registro</returns>
        public bool BuscaValorCotaPosterior(int idCarteira, DateTime data)
        {
            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();

            historicoCotaCollection.Query
                 .Select(historicoCotaCollection.Query.CotaAbertura, historicoCotaCollection.Query.CotaFechamento)
                 .Where(historicoCotaCollection.Query.IdCarteira == idCarteira,
                        historicoCotaCollection.Query.Data.GreaterThanOrEqual(data))
                 .OrderBy(historicoCotaCollection.Query.Data.Ascending);

            historicoCotaCollection.Query.Load();

            if (historicoCotaCollection.HasData)
            {
                // Copia informações de historicoCotaCollection para historicoCota
                this.AddNew();
                this.CotaAbertura = ((HistoricoCota)historicoCotaCollection[0]).CotaAbertura;
                this.CotaFechamento = ((HistoricoCota)historicoCotaCollection[0]).CotaFechamento;
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Carrega o objeto HistoricoCota com os campos CotaAbertura, CotaFechamento.
        /// Busca pela data anterior da cota mais próxima .
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        /// <returns>booleando indicando se achou registro</returns>
        public bool BuscaValorCotaAnterior(int idCarteira, DateTime data)
        {
            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();

            historicoCotaCollection.Query
                 .Select(historicoCotaCollection.Query.CotaAbertura, historicoCotaCollection.Query.CotaFechamento, historicoCotaCollection.Query.Data)
                 .Where(historicoCotaCollection.Query.IdCarteira == idCarteira,
                        historicoCotaCollection.Query.Data.LessThanOrEqual(data))
                 .OrderBy(historicoCotaCollection.Query.Data.Descending);

            historicoCotaCollection.Query.Load();

            if (historicoCotaCollection.HasData)
            {
                // Copia informações de historicoCotaCollection para historicoCota
                this.AddNew();
                this.CotaAbertura = ((HistoricoCota)historicoCotaCollection[0]).CotaAbertura;
                this.CotaFechamento = ((HistoricoCota)historicoCotaCollection[0]).CotaFechamento;
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Carrega o objeto HistoricoCota com os campos CotaAbertura, CotaFechamento.
        /// Busca pela data anterior da cota mais próxima .
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        /// <returns>booleando indicando se achou registro</returns>
        public bool BuscaValorCotaDataAnterior(int idCarteira, DateTime data)
        {
            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();

            historicoCotaCollection.Query
                 .Select(historicoCotaCollection.Query.CotaAbertura, 
                         historicoCotaCollection.Query.CotaFechamento, 
                         historicoCotaCollection.Query.Data,
                         historicoCotaCollection.Query.CotaRendimento)
                 .Where(historicoCotaCollection.Query.IdCarteira == idCarteira,
                        historicoCotaCollection.Query.Data.LessThan(data))
                 .OrderBy(historicoCotaCollection.Query.Data.Descending);

            historicoCotaCollection.Query.es.Top = 1;

            historicoCotaCollection.Query.Load();

            if (historicoCotaCollection.HasData)
            {
                // Copia informações de historicoCotaCollection para historicoCota
                this.AddNew();
                this.CotaAbertura = ((HistoricoCota)historicoCotaCollection[0]).CotaAbertura;
                this.CotaFechamento = ((HistoricoCota)historicoCotaCollection[0]).CotaFechamento;
                this.CotaRendimento = ((HistoricoCota)historicoCotaCollection[0]).CotaRendimento;
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Retorna a quantidade de cotas da carteira na data.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>        
        /// <returns></returns>
        public decimal RetornaQuantidadeCotas(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.QuantidadeFechamento)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Data.Equal(data));

            this.Query.Load();

            decimal quantidade = 0;
            if (this.QuantidadeFechamento.HasValue)
                quantidade = this.QuantidadeFechamento.Value;

            return quantidade;
        }

        /// <summary>
        /// Lança automaticamente aplicação especial em cotas para a data de implantação de carteiras administradas.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void LancaCotasImplantacao(int idCarteira, DateTime data)
        {
            #region Busca TipoControle e IdMoeda do cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdMoeda);
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.IdPessoa);
            cliente.LoadByPrimaryKey(idCarteira);
            byte tipoControle = cliente.TipoControle.Value;
            int idMoeda = cliente.IdMoeda.Value;
            int idTipo = cliente.IdTipo.Value;
            #endregion

            if ((idTipo == (int)TipoClienteFixo.ClientePessoaFisica || idTipo == (int)TipoClienteFixo.ClientePessoaJuridica) &&
                (tipoControle == (byte)TipoControleCliente.CarteiraRentabil || tipoControle == (byte)TipoControleCliente.Completo))
            {
                int? idCarteiraAux = null;
                int idCotista; 
                Cotista cotista = new Cotista();
                cotista.Query.Where(cotista.Query.IdCarteira.Equal(idCarteira) & cotista.Query.IdPessoa.Equal(cliente.IdPessoa.Value));

                if (cotista.Query.Load())
                {
                    idCarteiraAux = cotista.IdCarteira.Value;
                    idCotista = cotista.IdCotista.Value;
                }
                else
                {
                    idCarteiraAux = idCotista = idCarteira;
                }
                Carteira carteira = new Carteira();
                campos = new List<esQueryItem>();
                campos.Add(carteira.Query.DataInicioCota);
                campos.Add(carteira.Query.CotaInicial);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                DateTime dataInicioCota = carteira.DataInicioCota.Value;
                decimal valorCotaInicial = carteira.CotaInicial.Value;

                OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdOperacao);
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteiraAux),
                                                      operacaoCotistaCollection.Query.DataOperacao.Equal(data),
                                                      operacaoCotistaCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.AplicacaoCotasEspecial),
                                                      operacaoCotistaCollection.Query.Fonte.NotEqual((byte)FonteOperacaoCotista.Manual));
                operacaoCotistaCollection.Query.Load();

                operacaoCotistaCollection.MarkAllAsDeleted();
                operacaoCotistaCollection.Save();

                PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                posicaoCotistaCollection.Query.Select(posicaoCotistaCollection.Query.IdPosicao);
                posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteiraAux));
                posicaoCotistaCollection.Query.Load();

                if (posicaoCotistaCollection.Count > 0) //Testa para os casos de carteiras na data de implantação, mas com carga histórica, neste caso não gera o lançamento
                    return;

                operacaoCotistaCollection = new OperacaoCotistaCollection();
                operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdOperacao);
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteiraAux),
                                                      operacaoCotistaCollection.Query.DataOperacao.Equal(data),
                                                      operacaoCotistaCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.AplicacaoCotasEspecial));
                operacaoCotistaCollection.Query.Load();

                if (operacaoCotistaCollection.Count > 0) //Testa para os casos de carteiras com aporte especial lançado manualmente, neste caso não gera o lançamento
                    return;

                decimal valorPL = 0;
                HistoricoCota historicoCota = new HistoricoCota();
                if (historicoCota.LoadByPrimaryKey(data, idCarteiraAux.Value))
                {
                    valorPL = historicoCota.PLFechamento.Value;
                }
                else
                {
                    return;
                }

                decimal quantidadeAplicar = Utilitario.Truncate(valorPL / valorCotaInicial, 8);

                OperacaoCotista operacaoCotista = new OperacaoCotista();
                operacaoCotista.CotaOperacao = valorCotaInicial;
                operacaoCotista.DataAgendamento = data;
                operacaoCotista.DataConversao = data;
                operacaoCotista.DataLiquidacao = data;
                operacaoCotista.DataOperacao = data;
                operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;
                operacaoCotista.IdCarteira = idCarteiraAux;
                operacaoCotista.IdCotista = idCotista;
                operacaoCotista.IdFormaLiquidacao = 1; //TODO FORÇADO!
                operacaoCotista.Quantidade = quantidadeAplicar;
                operacaoCotista.TipoOperacao = (byte)TipoOperacaoCotista.AplicacaoCotasEspecial;
                operacaoCotista.ValorBruto = 0;
                operacaoCotista.ValorLiquido = 0;
                operacaoCotista.DataRegistro = data;
                operacaoCotista.Save();
            }

        }

        /// <summary>
        /// Interface padrao para criacao de HistoricoCota.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="cota"></param>
        /// <param name="deferSave"></param>
        /// DeferSave é utilizado para postergarmos o save (importante se estamos numa collection e precisamos dar save na collection por performance)
        public void Save(int idCarteira, DateTime data, Decimal cota, Decimal pl, bool deferSave)
        {
            bool insert = !this.IdCarteira.HasValue;

            this.Data = data;
            this.IdCarteira = idCarteira;
            this.CotaAbertura = cota;
            this.CotaFechamento = cota;
            this.CotaBruta = cota;

            this.PatrimonioBruto = pl;
            this.PLAbertura = pl;
            this.PLFechamento = pl;
            this.QuantidadeFechamento = this.QuantidadeFechamento.HasValue ? this.QuantidadeFechamento.Value : 0;
            this.AjustePL = this.AjustePL.HasValue ? this.AjustePL.Value : 0;
            if (!deferSave)
            {
                this.Save();
            }
        }

        /// <summary>
        /// Lança um Resgate Bruto ou Aporte em Operação Cotista
        /// Se SaldoCaixa = 0 não faz nada
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ZeraCaixaAutomatico(int idCarteira, DateTime data)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            #region Busca cotista
            Cotista cotista = new Cotista();
            cotista.Query.Where(cotista.Query.IdCarteira.Equal(idCarteira) & cotista.Query.IdPessoa.Equal(cliente.IdPessoa.Value));

            int idCotista = idCarteira;            

            if (cotista.Query.Load())
                idCotista = cotista.IdCotista.Value;
            #endregion

            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            saldoCaixaCollection.Query.Select(saldoCaixaCollection.Query.SaldoAbertura.Sum(),
                                              saldoCaixaCollection.Query.SaldoFechamento.Sum(),
                                              saldoCaixaCollection.Query.IdConta);
            saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCarteira),
                                             saldoCaixaCollection.Query.Data.Equal(data));
            saldoCaixaCollection.Query.GroupBy(saldoCaixaCollection.Query.IdConta);
            saldoCaixaCollection.Query.Load();

            foreach (SaldoCaixa saldoCaixa in saldoCaixaCollection)
            {
                int idConta = (int)saldoCaixa.IdConta;

                decimal saldoCaixaFechamento = saldoCaixa.SaldoFechamento.Value;
                bool resgateTotal = false;

                //Se o saldo estiver zerado, não deve gerar operação de zera caixa
                if (saldoCaixaFechamento == 0)
                    continue;

                #region Prepara Delete OperacaoCotista por idCarteira e Data
                OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdOperacao);
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira) &
                                                      operacaoCotistaCollection.Query.DataRegistro.Equal(data) &
                                                      (operacaoCotistaCollection.Query.IdConta.IsNull() | operacaoCotistaCollection.Query.IdConta.Equal(idConta)) &
                                                      operacaoCotistaCollection.Query.TipoOperacao.In((byte)TipoOperacaoCotista.Aplicacao,
                                                                                                       (byte)TipoOperacaoCotista.ResgateBruto));
                operacaoCotistaCollection.MarkAllAsDeleted();
                operacaoCotistaCollection.Query.Load();
                #endregion

                #region Carteira Trader
                Trader trader = new Trader();
                trader.Query.Where(trader.Query.IdCarteira.Equal(idCarteira));

                //Se for carteira de trader, zera o caixa lançando liquidação inversa
                if (trader.Query.Load())
                {
                    string descricao = string.Empty;
                    decimal valor = 0;
                    int origem = 0;
                    valor = (-1) * saldoCaixaFechamento;
                    if (saldoCaixaFechamento > 0)
                    {
                        descricao = "Resgates no dia";
                        origem = (int)OrigemLancamentoLiquidacao.Cotista.Resgate;
                    }
                    else
                    {
                        descricao = "Aplicações no dia";
                        origem = (int)OrigemLancamentoLiquidacao.Cotista.Aplicacao;
                    }

                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = origem;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idConta;
                    liquidacao.Save();

                    continue;
                }
                #endregion

                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.Query.Select(historicoCota.Query.PLFechamento);
                historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                          historicoCota.Query.Data.Equal(data));
                historicoCota.Query.Load();

                //Importante checar se o PL é menor que o caixa, se for não posso zerar o caixa com resgate, 
                //pois zeraria a qtde de cotas e o PL!
                if (!historicoCota.PLFechamento.HasValue || saldoCaixaFechamento > historicoCota.PLFechamento.Value)
                {
                    return;
                }
                //Se for igual lança um resgate total                                
                else if (saldoCaixaFechamento == historicoCota.PLFechamento.Value)
                {
                    resgateTotal = true;
                }

                OperacaoCotista operacaoCotista = new OperacaoCotista();

                operacaoCotista.IdCarteira = idCarteira;
                operacaoCotista.IdCotista = idCotista;
                operacaoCotista.DataConversao = operacaoCotista.DataLiquidacao = operacaoCotista.DataOperacao = data;
                operacaoCotista.DataRegistro = data;
                operacaoCotista.DataAgendamento = data;
                operacaoCotista.ValorLiquido = 0;
                operacaoCotista.Quantidade = 0;
                operacaoCotista.IdFormaLiquidacao = 1; // Fixo
                operacaoCotista.Observacao = "";
                operacaoCotista.Fonte = (byte)FonteOperacaoCotista.ZeraCaixa;

                if (saldoCaixaFechamento > 0)
                {
                    #region Lança Resgate Bruto ou Total
                    // Resgate
                    if (resgateTotal)
                    {
                        operacaoCotista.TipoOperacao = (byte)TipoOperacaoCotista.ResgateTotal;
                    }
                    else
                    {
                        operacaoCotista.TipoOperacao = (byte)TipoOperacaoCotista.ResgateBruto;
                    }

                    operacaoCotista.TipoResgate = (byte)TipoResgateCotista.FIFO;
                    //       
                    operacaoCotista.ValorBruto = saldoCaixaFechamento;
                    //                            

                    #endregion
                }
                else
                {
                    #region Lança Aporte
                    // Aporte
                    operacaoCotista.TipoOperacao = (byte)TipoOperacaoCotista.Aplicacao;
                    //       
                    operacaoCotista.ValorBruto = Math.Abs(saldoCaixaFechamento);
                    //                            

                    #endregion
                }

                // Salva OperacaoCotista
                using (esTransactionScope scope = new esTransactionScope())
                {
                    operacaoCotistaCollection.Save(); // Delete
                    operacaoCotista.Save();
                    //
                    scope.Complete();
                }
            }
        }

        /// <summary>
        /// Gera resgates do tipo especial por cotas, para o total de liquidação de IR e IOF na data do cliente.
        /// O objetivo disto é não deixar impactar na cota o valor sendo liquidado.
        /// Retorna indicando se teve resgate de tributos na data!!!!
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public bool GeraResgateTributos(int idCarteira, DateTime data)
        {
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
            liquidacao.Query.Where(liquidacao.Query.IdCliente.Equal(idCarteira),
                                   liquidacao.Query.DataLancamento.Equal(data),
                                   liquidacao.Query.Origem.In((int)OrigemLancamentoLiquidacao.RendaFixa.IR,
                                                              (int)OrigemLancamentoLiquidacao.RendaFixa.IOF,
                                                              (int)OrigemLancamentoLiquidacao.Fundo.IRResgate,
                                                              (int)OrigemLancamentoLiquidacao.Fundo.IOFResgate,
                                                              (int)OrigemLancamentoLiquidacao.IR.IRRendaVariavel,
                                                              (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade,
                                                              (int)OrigemLancamentoLiquidacao.IR.IRFonteOperacao));
            liquidacao.Query.Load();

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            Cotista cotista = new Cotista();
            cotista.Query.Where(cotista.Query.IdCarteira.Equal(cliente.IdCliente.Value) &
                                cotista.Query.IdPessoa.Equal(cliente.IdPessoa.Value));

            cotista.Query.Load();

            decimal valor = 0;
            if (liquidacao.Valor.HasValue)
            {
                valor = Math.Abs(liquidacao.Valor.Value);
            }

            OperacaoFundo operacaoFundo = new OperacaoFundo();
            operacaoFundo.Query.Select(operacaoFundo.Query.ValorIR.Sum());
            operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCarteira),
                                      operacaoFundo.Query.DataOperacao.Equal(data),
                                      operacaoFundo.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ComeCotas));
            operacaoFundo.Query.Load();

            if (operacaoFundo.ValorIR.HasValue)
            {
                valor += operacaoFundo.ValorIR.Value;
            }

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(data, idCarteira);

            if (!historicoCota.PLFechamento.HasValue || historicoCota.QuantidadeFechamento.Value == 0)
            {
                return false;
            }

            decimal valorPL = historicoCota.PLFechamento.Value + valor; //Soma novamente o IR ao PL calculado
            decimal cotaFechamento = valorPL / historicoCota.QuantidadeFechamento.Value;

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Select(operacaoCotistaCollection.Query.IdOperacao);
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                  operacaoCotistaCollection.Query.DataOperacao.GreaterThanOrEqual(data),
                                                  operacaoCotistaCollection.Query.Fonte.Equal((byte)FonteOperacaoCotista.ResgateTributos));
            operacaoCotistaCollection.Query.Load();
            operacaoCotistaCollection.MarkAllAsDeleted();
            operacaoCotistaCollection.Save();

            if (valor > 0)
            {
                decimal quantidade = Utilitario.Truncate(valor / cotaFechamento, 8);
                OperacaoCotista operacaoCotista = new OperacaoCotista();
                operacaoCotista.CotaOperacao = cotaFechamento;
                operacaoCotista.DataAgendamento = data;
                operacaoCotista.DataConversao = data;
                operacaoCotista.DataLiquidacao = data;
                operacaoCotista.DataOperacao = data;
                operacaoCotista.DataRegistro = data;
                operacaoCotista.Fonte = (byte)FonteOperacaoCotista.ResgateTributos;
                operacaoCotista.IdCarteira = idCarteira;
                operacaoCotista.IdCotista = cotista.IdCotista;
                operacaoCotista.IdFormaLiquidacao = 1; //TODO AJUSTAR
                operacaoCotista.PrejuizoUsado = 0;
                operacaoCotista.Quantidade = quantidade;
                operacaoCotista.RendimentoResgate = 0;
                operacaoCotista.TipoOperacao = (byte)TipoOperacaoCotista.ResgateCotasEspecial;
                operacaoCotista.TipoResgate = (byte)TipoResgateCotista.FIFO;
                operacaoCotista.ValorBruto = valor;
                operacaoCotista.ValorCPMF = 0;
                operacaoCotista.ValorIOF = 0;
                operacaoCotista.ValorIR = 0;
                operacaoCotista.ValorLiquido = valor;
                operacaoCotista.ValorPerformance = 0;
                operacaoCotista.VariacaoResgate = 0;
                operacaoCotista.Save();
    
                //AJUSTA NOVAMENTE A QUANTIDADE, COTA E PL!!!!
                historicoCota.QuantidadeFechamento = historicoCota.QuantidadeFechamento.Value - quantidade;
                historicoCota.CotaFechamento = cotaFechamento;
                historicoCota.CotaBruta = cotaFechamento;
                historicoCota.PLFechamento = historicoCota.PLFechamento;
                historicoCota.PatrimonioBruto = historicoCota.PLFechamento;
                historicoCota.Save();

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Retorna o PL para fundos do tipo Somente Cotista ou Apenas Cotacao.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public decimal RetornaPLComCotaInformada(int idCarteira, DateTime data, byte tipoControle, bool historico)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            if (!historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                return 0;
            }

            decimal valorCota = 0;
            if (historicoCota.CotaFechamento.HasValue)
            {
                valorCota = historicoCota.CotaFechamento.Value;
            }

            decimal valorPL = 0;
            if (historico)
            {
                if (tipoControle == (byte)TipoControleCliente.Cotista)
                {
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
                    PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();

                    decimal totalCotas = posicaoCotistaHistorico.RetornaTotalCotas(idCarteira, dataAnterior);

                    valorPL = Math.Round(totalCotas * valorCota, 2);
                }
                else
                {
                    //Expurga carteiras mãe do cômputo//
                    PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                    posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.ValorBruto.Sum(),
                                                                 posicaoFundoHistoricoCollection.Query.IdCliente);
                    posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                                posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(data));
                    posicaoFundoHistoricoCollection.Query.GroupBy(posicaoFundoHistoricoCollection.Query.IdCliente);
                    posicaoFundoHistoricoCollection.Query.Load();

                    foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
                    {
                        int idCliente = posicaoFundoHistorico.IdCliente.Value;
                        decimal valorSaldo = posicaoFundoHistorico.ValorBruto.Value;

                        Cliente cliente = new Cliente();
                        if (!cliente.IsMae(idCliente))
                        {
                            valorPL += valorSaldo;
                        }
                    }
                    //
                }
            }
            else
            {
                if (tipoControle == (byte)TipoControleCliente.Cotista)
                {
                    PosicaoCotista posicaoCotista = new PosicaoCotista();
                    decimal totalCotas = posicaoCotista.RetornaTotalCotas(idCarteira);

                    valorPL = Math.Round(totalCotas * valorCota, 2);
                }
                else
                {
                    //Expurga carteiras mãe do cômputo//
                    PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                    posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.ValorBruto.Sum(),
                                                        posicaoFundoCollection.Query.IdCliente);
                    posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira));
                    posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCliente);
                    posicaoFundoCollection.Query.Load();

                    foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                    {
                        int idCliente = posicaoFundo.IdCliente.Value;
                        decimal valorSaldo = posicaoFundo.ValorBruto.Value;

                        Cliente cliente = new Cliente();
                        if (!cliente.IsMae(idCliente))
                        {
                            valorPL += valorSaldo;
                        }
                    }
                    //
                }
            }

            return valorPL;
        }

        /// <summary>
        /// Calcula o PL para fundos do tipo Somente Cotista ou Apenas Cotacao.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void CalculaPLComCotaInformada(int idCarteira, DateTime data, byte tipoControle)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            if (!historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                return;
            }

            decimal valorCota = 0;
            if (historicoCota.CotaFechamento.HasValue)
            {
                valorCota = historicoCota.CotaFechamento.Value;
            }

            decimal valorPL = 0;
            decimal? patrimonioBruto = null;
            if (tipoControle == (byte)TipoControleCliente.Cotista)
            {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                decimal totalCotas = posicaoCotista.RetornaTotalCotas(idCarteira);

                if (ParametrosConfiguracaoSistema.Fundo.SimulaYMFCOT)
                {
                    //Se estamos simulando o COT, precisamos deduzir o Valor da Performance da Cota Bruta
                    valorCota = this.RetornaCotaBrutaComPfee(idCarteira, data);
                    historicoCota.CotaFechamento = valorCota;
                    patrimonioBruto = Math.Round(totalCotas * historicoCota.CotaBruta.Value, 2);
                }

                valorPL = Math.Round(totalCotas * valorCota, 2);
            }
            else
            {
                PosicaoFundo posicaoFundo = new PosicaoFundo();
                valorPL = posicaoFundo.RetornaValorMercado(idCarteira, data);
            }

            historicoCota.CotaAbertura = valorCota;
            historicoCota.PatrimonioBruto = patrimonioBruto.HasValue ? patrimonioBruto : valorPL;
            historicoCota.PLAbertura = valorPL;
            historicoCota.PLFechamento = valorPL;
            historicoCota.Save();
        }

        /// <summary>
        /// Retorna a Soma de PL filtrado pelos fundos associados ao idGestor passado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idGestor"></param>
        /// <returns></returns>
        /// 
        public decimal RetornaPLPorGestor(int idGestor, DateTime data)
        {
            HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("H");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");

            historicoCotaQuery.Select(historicoCotaQuery.PLFechamento.Sum());
            historicoCotaQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == historicoCotaQuery.IdCarteira);
            historicoCotaQuery.Where(carteiraQuery.IdAgenteGestor.Equal(idGestor),
                                     historicoCotaQuery.Data.Equal(data));

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Load(historicoCotaQuery);
            return historicoCota.PLFechamento.HasValue ? historicoCota.PLFechamento.Value : 0;
        }

        public bool ImportaPosicaoFundoYMF(Financial.Interfaces.Import.YMF.PosicaoFundoYMF posicaoFundoYMF)
        {
            Cliente cliente = new ClienteCollection().BuscaClientePorCodigoYMF(posicaoFundoYMF.CdFundo.Trim());
            if (cliente == null)
            {
                throw new Exception("Cliente não cadastrado: " + posicaoFundoYMF.CdFundo);
            }

            this.IdCarteira = cliente.IdCliente;
            this.Data = posicaoFundoYMF.DtPosicao;
            this.PLAbertura = posicaoFundoYMF.VlPatrimonioAbertura.HasValue ? posicaoFundoYMF.VlPatrimonioAbertura : 0;

            this.CotaAbertura = posicaoFundoYMF.VlCotaAbertura.HasValue ? posicaoFundoYMF.VlCotaAbertura.Value : 0;
            this.CotaBruta = posicaoFundoYMF.VlCotaPrePfee.HasValue ? posicaoFundoYMF.VlCotaPrePfee :
                this.CotaAbertura.Value;
            this.PatrimonioBruto = posicaoFundoYMF.VlPatrimonioPrePfee.HasValue ? posicaoFundoYMF.VlPatrimonioPrePfee :
                this.PLAbertura;
            this.PLFechamento = posicaoFundoYMF.VlPatrimonioFechamentoTotal.HasValue ? posicaoFundoYMF.VlPatrimonioFechamentoTotal :
                0;
            this.QuantidadeFechamento = this.QuantidadeFechamento.HasValue ? this.QuantidadeFechamento.Value : 0;

            this.CotaFechamento = posicaoFundoYMF.VlCotaFechamento;
            this.AjustePL = 0;

            return true;
        }

        public decimal RetornaCotaRecompostaHistorica(int idCarteira, DateTime dataInicio, DateTime dataFim,
            out List<DebugCotaRecomposta> debugCotaRecompostaList)
        {
            return this.RetornaCotaRecompostaHistorica(idCarteira, dataInicio, dataFim, out debugCotaRecompostaList, null);
        }

        public decimal RetornaCotaRecompostaHistorica(int idCarteira, DateTime dataInicio, DateTime dataFim,
            out List<DebugCotaRecomposta> debugCotaRecompostaList, decimal? cotaInformada)
        {

            debugCotaRecompostaList = new List<DebugCotaRecomposta>();
            decimal cotaFinal = 0;

            if (cotaInformada.HasValue)
            {
                cotaFinal = cotaInformada.Value;
            }
            else
            {
                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.Query.Select(historicoCota.Query.CotaFechamento);
                historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                          historicoCota.Query.Data.Equal(dataInicio));


                if (historicoCota.Query.Load() && historicoCota.CotaFechamento.HasValue)
                {
                    cotaFinal = historicoCota.CotaFechamento.Value;
                }
            }

            if (cotaFinal != 0)
            {
                AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
                agendaFundoCollection.Query.Select(agendaFundoCollection.Query.Taxa,
                    agendaFundoCollection.Query.DataEvento,
                    agendaFundoCollection.Query.TipoEvento);
                agendaFundoCollection.Query.Where(agendaFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                  agendaFundoCollection.Query.DataEvento.GreaterThan(dataInicio),
                                                  agendaFundoCollection.Query.DataEvento.LessThan(dataFim),
                                                  agendaFundoCollection.Query.TipoEvento.In((byte)TipoEventoFundo.Amortizacao,
                                                                                            (byte)TipoEventoFundo.AmortizacaoJuros,
                                                                                            (byte)TipoEventoFundo.Juros));
                agendaFundoCollection.Query.OrderBy(agendaFundoCollection.Query.DataEvento.Ascending);
                agendaFundoCollection.Query.Load();

                foreach (AgendaFundo agendaFundo in agendaFundoCollection)
                {
                    DebugCotaRecomposta debugCotaRecomposta = new DebugCotaRecomposta();
                    debugCotaRecomposta.DataEvento = agendaFundo.DataEvento.Value;
                    debugCotaRecomposta.CotaPreEvento = cotaFinal;
                    debugCotaRecomposta.ValorEvento = agendaFundo.Taxa.Value;
                    if (agendaFundo.TipoEvento.Value == (byte)TipoEventoFundo.AmortizacaoJuros)
                    {
                        cotaFinal = cotaFinal - cotaFinal * (agendaFundo.Taxa.Value / 100M);
                    }
                    else if (agendaFundo.TipoEvento.Value == (byte)TipoEventoFundo.Amortizacao)
                    {
                        //Precisamos descobrir o valor absoluto da amortização
                        HistoricoCota historicoCotaDiaEvento = new HistoricoCota();
                        historicoCotaDiaEvento.LoadByPrimaryKey(agendaFundo.DataEvento.Value, idCarteira);
                        decimal cotaPosDiaEvento = historicoCotaDiaEvento.CotaFechamento.Value;
                        decimal cotaPreDiaEvento = cotaPosDiaEvento / (1M - agendaFundo.Taxa.Value / 100M);
                        decimal valorAmortizacao = cotaPreDiaEvento - cotaPosDiaEvento;

                        cotaFinal = cotaFinal - valorAmortizacao;
                    }
                    else
                    {
                        //Tipo Juros - Não fazer nada pois cota final = cota pré-evento
                    }

                    debugCotaRecomposta.CotaPosEvento = cotaFinal;
                    debugCotaRecomposta.TipoEvento = agendaFundo.TipoEvento.Value;
                    debugCotaRecompostaList.Add(debugCotaRecomposta);
                }
            }

            return cotaFinal;
        }

        public decimal RetornaCotaRecompostaAtual(int idCarteira, DateTime data,
            out List<DebugCotaRecomposta> debugCotaRecompostaList, byte tipoEvento)
        {
            return this.RetornaCotaRecompostaAtual(idCarteira, data, true, out debugCotaRecompostaList, null, tipoEvento);
        }

        public decimal RetornaCotaRecompostaAtual(int idCarteira, DateTime data, bool recompoeCotaJuros,
            out List<DebugCotaRecomposta> debugCotaRecompostaList, decimal? cotaInformada)
        {
            return this.RetornaCotaRecompostaAtual(idCarteira, data, recompoeCotaJuros,
            out debugCotaRecompostaList, cotaInformada, null);
        }

        public decimal RetornaCotaRecompostaAtual(int idCarteira, DateTime data, bool recompoeCotaJuros,
            out List<DebugCotaRecomposta> debugCotaRecompostaList, decimal? cotaInformada, byte? tipoEvento)
        {
            debugCotaRecompostaList = new List<DebugCotaRecomposta>();

            decimal cotaFinal = 0;
            if (cotaInformada.HasValue)
            {
                cotaFinal = cotaInformada.Value;
            }
            else
            {
                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.Query.Select(historicoCota.Query.CotaFechamento);
                historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                          historicoCota.Query.Data.Equal(data));


                if (historicoCota.Query.Load() && historicoCota.CotaFechamento.HasValue)
                {
                    cotaFinal = historicoCota.CotaFechamento.Value;
                }
            }

            if (cotaFinal != 0)
            {
                AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
                agendaFundoCollection.Query.Select(agendaFundoCollection.Query.Taxa,
                    agendaFundoCollection.Query.DataEvento,
                    agendaFundoCollection.Query.TipoEvento);
                agendaFundoCollection.Query.Where(agendaFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                  agendaFundoCollection.Query.DataEvento.Equal(data));

                if (tipoEvento.HasValue)
                {
                    agendaFundoCollection.Query.Where(agendaFundoCollection.Query.TipoEvento.Equal(tipoEvento));
                }
                else
                {
                    agendaFundoCollection.Query.Where(
                        agendaFundoCollection.Query.TipoEvento.In((byte)TipoEventoFundo.Amortizacao,
                                                                  (byte)TipoEventoFundo.AmortizacaoJuros,
                                                                  (byte)TipoEventoFundo.Juros));
                }


                agendaFundoCollection.Query.Load();

                foreach (AgendaFundo agendaFundo in agendaFundoCollection)
                {
                    DebugCotaRecomposta debugCotaRecomposta = new DebugCotaRecomposta();
                    debugCotaRecomposta.DataEvento = agendaFundo.DataEvento.Value;
                    debugCotaRecomposta.CotaPosEvento = cotaFinal; //Estamos na cota pós evento no dia da amortização !!!
                    debugCotaRecomposta.ValorEvento = agendaFundo.Taxa.Value;
                    if (agendaFundo.TipoEvento.Value == (byte)TipoEventoFundo.AmortizacaoJuros ||
                        agendaFundo.TipoEvento.Value == (byte)TipoEventoFundo.Amortizacao)
                    {
                        cotaFinal = cotaFinal / (1M - agendaFundo.Taxa.Value / 100M);
                    }
                    else
                    {
                        if (recompoeCotaJuros)
                        {
                            cotaFinal = cotaFinal / (1M - agendaFundo.Taxa.Value / 100M);
                        }
                    }

                    debugCotaRecomposta.CotaPosEvento = cotaFinal;
                    debugCotaRecomposta.TipoEvento = agendaFundo.TipoEvento.Value;
                    debugCotaRecompostaList.Add(debugCotaRecomposta);
                }
            }

            return cotaFinal;
        }

        public decimal RetornaCotaRecomposta(int idCarteira, DateTime dataReferencia, DateTime dataInicio)
        {
            decimal cotaFinal = 0;
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Query.Select(historicoCota.Query.CotaFechamento);
            historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                      historicoCota.Query.Data.Equal(dataReferencia));


            if (historicoCota.Query.Load() && historicoCota.CotaFechamento.HasValue)
            {
                cotaFinal = historicoCota.CotaFechamento.Value;
            }

            if (cotaFinal != 0)
            {
                AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
                agendaFundoCollection.Query.Select(agendaFundoCollection.Query.Taxa,
                    agendaFundoCollection.Query.DataEvento,
                    agendaFundoCollection.Query.TipoEvento);
                agendaFundoCollection.Query.Where(agendaFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                  agendaFundoCollection.Query.DataEvento.LessThanOrEqual(dataReferencia),
                                                  agendaFundoCollection.Query.DataEvento.GreaterThan(dataInicio),
                                                  agendaFundoCollection.Query.TipoEvento.In((byte)TipoEventoFundo.Amortizacao,
                                                                                            (byte)TipoEventoFundo.AmortizacaoJuros,
                                                                                            (byte)TipoEventoFundo.Juros));
                agendaFundoCollection.Query.Load();

                foreach (AgendaFundo agendaFundo in agendaFundoCollection)
                {
                    cotaFinal = cotaFinal / (1M - agendaFundo.Taxa.Value / 100M);
                }
            }

            return cotaFinal;
        }

        public void ProcessaIncorporacao(int idCliente, DateTime data)
        {
            IncorporacaoFundoCollection incorporacaoFundoCollection = new IncorporacaoFundoCollection();
            incorporacaoFundoCollection.Query.Where(incorporacaoFundoCollection.Query.Data.Equal(data));
            incorporacaoFundoCollection.Query.Load();

            #region Deleta movimentos de aplic e resgate de incorporação na OperacaoCotista
            if (incorporacaoFundoCollection.Count > 0)
            {
                OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
                operacaoCotistaCollectionDeletar.Query.Select(operacaoCotistaCollectionDeletar.Query.IdOperacao);
                operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCarteira.Equal(idCliente),
                                                             operacaoCotistaCollectionDeletar.Query.DataOperacao.Equal(data),
                                                             operacaoCotistaCollectionDeletar.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.IncorporacaoResgate));
                operacaoCotistaCollectionDeletar.Query.Load();
                operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
                operacaoCotistaCollectionDeletar.Save();

                List<int> listaDeletar = new List<int>();
                foreach (IncorporacaoFundo incorporacaoFundoDeletar in incorporacaoFundoCollection)
                {
                    if (incorporacaoFundoDeletar.IdCarteiraOrigem.Value == idCliente)
                    {
                        listaDeletar.Add(incorporacaoFundoDeletar.IdCarteiraDestino.Value);
                    }
                }

                if (listaDeletar.Count > 0)
                {
                    operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
                    operacaoCotistaCollectionDeletar.Query.Select(operacaoCotistaCollectionDeletar.Query.IdOperacao);
                    operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCarteira.In(listaDeletar),
                                                                 operacaoCotistaCollectionDeletar.Query.DataOperacao.Equal(data),
                                                                 operacaoCotistaCollectionDeletar.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.IncorporacaoAplicacao));
                    operacaoCotistaCollectionDeletar.Query.Load();
                    operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
                    operacaoCotistaCollectionDeletar.Save();
                }
            }
            #endregion

            foreach (IncorporacaoFundo incorporacaoFundo in incorporacaoFundoCollection)
            {
                int idFundoOrigem = incorporacaoFundo.IdCarteiraOrigem.Value;
                int idFundoDestino = incorporacaoFundo.IdCarteiraDestino.Value;
                decimal? taxa = incorporacaoFundo.Taxa;

                HistoricoIncorporacaoPosicaoCollection historicoIncorporacaoPosicaoCollectionDeletar = new HistoricoIncorporacaoPosicaoCollection();
                historicoIncorporacaoPosicaoCollectionDeletar.Query.Where(historicoIncorporacaoPosicaoCollectionDeletar.Query.IdCarteiraDestino.Equal(idFundoDestino),
                                                                          historicoIncorporacaoPosicaoCollectionDeletar.Query.IdCarteiraOrigem.Equal(idFundoOrigem),
                                                                          historicoIncorporacaoPosicaoCollectionDeletar.Query.DataIncorporacao.Equal(data));
                historicoIncorporacaoPosicaoCollectionDeletar.Query.Load();
                historicoIncorporacaoPosicaoCollectionDeletar.MarkAllAsDeleted();
                historicoIncorporacaoPosicaoCollectionDeletar.Save();

                Cliente fundoClienteOrigem = new Cliente();
                fundoClienteOrigem.LoadByPrimaryKey(idFundoOrigem);

                Cliente fundoClienteDestino = new Cliente();
                fundoClienteDestino.LoadByPrimaryKey(idFundoDestino);

                Carteira fundoDestino = new Carteira();
                fundoDestino.LoadByPrimaryKey(idFundoDestino);

                decimal cotaDestino = 0;
                HistoricoCota historicoCota = new HistoricoCota();
                try
                {
                    historicoCota.BuscaValorCotaDia(idFundoDestino, data);

                    if (fundoDestino.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                    {
                        cotaDestino = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        cotaDestino = historicoCota.CotaFechamento.Value;
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }

                decimal cotaOrigem = 0;
                historicoCota = new HistoricoCota();
                try
                {
                    historicoCota.BuscaValorCotaDia(idFundoOrigem, data);

                    if (fundoDestino.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                    {
                        cotaOrigem = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        cotaOrigem = historicoCota.CotaFechamento.Value;
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }

                #region Tratamento para PosicaoFundo (apenas transferências de posições e prejuízos)
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCarteira.Equal(idFundoOrigem),
                                                   posicaoFundoCollection.Query.IdCliente.Equal(idCliente));
                posicaoFundoCollection.Query.Load();

                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    decimal valorBruto = posicaoFundo.ValorBruto.Value;
                    decimal valorBrutoAntesCortes = posicaoFundo.QuantidadeAntesCortes.Value * posicaoFundo.CotaDia.Value;

                    decimal quantidadeConvertida = Math.Round(valorBruto / cotaDestino, fundoDestino.CasasDecimaisCota.Value);
                    decimal quantidadeAntesCortesConvertida = Math.Round(valorBrutoAntesCortes / cotaDestino, fundoDestino.CasasDecimaisCota.Value);

                    decimal cotaAplicacaoConvertida = Math.Round(cotaDestino * posicaoFundo.CotaAplicacao.Value / posicaoFundo.CotaDia.Value, 8);

                    posicaoFundo.Quantidade = quantidadeConvertida;
                    posicaoFundo.QuantidadeAntesCortes = quantidadeAntesCortesConvertida;
                    posicaoFundo.IdCarteira = idFundoDestino;
                    posicaoFundo.CotaDia = cotaDestino;
                    posicaoFundo.CotaAplicacao = cotaAplicacaoConvertida;
                    posicaoFundo.PosicaoIncorporada = "S";

                    HistoricoIncorporacaoPosicao historicoIncorporacaoPosicao = new HistoricoIncorporacaoPosicao();
                    historicoIncorporacaoPosicao.DataConversao = posicaoFundo.DataConversao.Value;
                    historicoIncorporacaoPosicao.DataIncorporacao = data;
                    historicoIncorporacaoPosicao.IdCarteiraDestino = idFundoDestino;
                    historicoIncorporacaoPosicao.IdCarteiraOrigem = idFundoOrigem;
                    historicoIncorporacaoPosicao.IdPosicao = posicaoFundo.IdPosicao.Value;
                    historicoIncorporacaoPosicao.Save();
                }
                posicaoFundoCollection.Save();

                PrejuizoFundoCollection prejuizoFundoCollection = new PrejuizoFundoCollection();
                prejuizoFundoCollection.Query.Where(prejuizoFundoCollection.Query.IdCarteira.Equal(idFundoOrigem),
                                                    prejuizoFundoCollection.Query.IdCliente.Equal(idCliente));
                prejuizoFundoCollection.Query.Load();

                foreach (PrejuizoFundo prejuizoFundo in prejuizoFundoCollection)
                {
                    PrejuizoFundo prejuizoFundoDestino = new PrejuizoFundo();
                    if (prejuizoFundoDestino.LoadByPrimaryKey(idCliente, idFundoDestino, prejuizoFundo.Data.Value))
                    {
                        prejuizoFundoDestino.ValorPrejuizo += prejuizoFundo.ValorPrejuizo.Value;
                        prejuizoFundoDestino.Save();
                    }
                    else
                    {
                        prejuizoFundoDestino = new PrejuizoFundo();
                        prejuizoFundoDestino.Data = prejuizoFundo.Data.Value;
                        prejuizoFundoDestino.DataLimiteCompensacao = prejuizoFundo.DataLimiteCompensacao;
                        prejuizoFundoDestino.IdCarteira = idFundoDestino;
                        prejuizoFundoDestino.IdCliente = idCliente;
                        prejuizoFundoDestino.ValorPrejuizo = prejuizoFundo.ValorPrejuizo.Value;
                        prejuizoFundoDestino.Save();
                    }

                    prejuizoFundo.ValorPrejuizo = 0;
                }

                prejuizoFundoCollection.Save();
                #endregion

                try
                {
                    #region Tratamento para PosicaoCotista (transferência de posições e prejuízos de cotistas e posições de carteira)
                    #region Transfere as posições e prejuizos a compensar de cotistas
                    PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                    posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idFundoOrigem),
                                                         posicaoCotistaCollection.Query.IdCotista.Equal(idCliente),
                                                         posicaoCotistaCollection.Query.Quantidade.NotEqual(0));
                    posicaoCotistaCollection.Query.Load();

                    foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                    {
                        if (fundoClienteDestino.DataDia.Value != data)
                        {
                            throw new Exception("Incorporação - O Fundo destino deve estar na data da incorporação para o processo ser realizado.");
                        }

                        decimal valorBruto = posicaoCotista.ValorBruto.Value;
                        decimal valorBrutoAntesCortes = posicaoCotista.QuantidadeAntesCortes.Value * posicaoCotista.CotaDia.Value;

                        decimal quantidadeConvertida = Math.Round(valorBruto / cotaDestino, fundoDestino.CasasDecimaisCota.Value);
                        decimal quantidadeAntesCortesConvertida = Math.Round(valorBrutoAntesCortes / cotaDestino, fundoDestino.CasasDecimaisCota.Value);

                        decimal cotaAplicacaoConvertida = Math.Round(cotaDestino * posicaoCotista.CotaAplicacao.Value / posicaoCotista.CotaDia.Value, 8);

                        decimal quantidadeOriginal = 0;
                        decimal valorAplicacaoOriginal = 0;
                        decimal valorBrutoOriginal = 0;
                        decimal valorLiquidoOriginal = 0;
                        decimal valorIROriginal = 0;
                        decimal valorIOFOriginal = 0;
                        decimal valorPfeeOriginal = 0;
                        decimal valorRendimentoOriginal = 0;

                        if (!taxa.HasValue || taxa.Value == 100)
                        {
                            quantidadeOriginal = posicaoCotista.Quantidade.Value;
                            valorAplicacaoOriginal = posicaoCotista.ValorAplicacao.Value;
                            valorBrutoOriginal = posicaoCotista.ValorBruto.Value;
                            valorLiquidoOriginal = posicaoCotista.ValorLiquido.Value;
                            valorIROriginal = posicaoCotista.ValorIR.Value;
                            valorIOFOriginal = posicaoCotista.ValorIOF.Value;
                            valorPfeeOriginal = posicaoCotista.ValorPerformance.Value;
                            valorRendimentoOriginal = posicaoCotista.ValorRendimento.Value;

                            //Incorporação total
                            posicaoCotista.Quantidade = quantidadeConvertida;
                            posicaoCotista.QuantidadeAntesCortes = quantidadeAntesCortesConvertida;
                            posicaoCotista.IdCarteira = idFundoDestino;
                            posicaoCotista.CotaDia = cotaDestino;
                            posicaoCotista.CotaAplicacao = cotaAplicacaoConvertida;
                            posicaoCotista.PosicaoIncorporada = "S";
                        }
                        else
                        {
                            quantidadeOriginal = Math.Round(posicaoCotista.Quantidade.Value * taxa.Value / 100M, fundoDestino.CasasDecimaisCota.Value);

                            quantidadeConvertida = Math.Round(quantidadeConvertida * taxa.Value / 100M, fundoDestino.CasasDecimaisCota.Value);
                            quantidadeAntesCortesConvertida = Math.Round(quantidadeAntesCortesConvertida * taxa.Value / 100M, fundoDestino.CasasDecimaisCota.Value);

                            //Incorporação parcial
                            posicaoCotista.Quantidade -= Math.Round(posicaoCotista.Quantidade.Value * taxa.Value / 100M, fundoDestino.CasasDecimaisCota.Value);
                            posicaoCotista.QuantidadeAntesCortes -= Math.Round(posicaoCotista.QuantidadeAntesCortes.Value * taxa.Value / 100M, fundoDestino.CasasDecimaisCota.Value); ;

                            valorAplicacaoOriginal = Math.Round(posicaoCotista.ValorAplicacao.Value * taxa.Value / 100M, 2);
                            posicaoCotista.ValorAplicacao -= valorAplicacaoOriginal;

                            valorBrutoOriginal = Math.Round(posicaoCotista.ValorBruto.Value * taxa.Value / 100M, 2);
                            posicaoCotista.ValorBruto -= valorBrutoOriginal;

                            valorLiquidoOriginal = Math.Round(posicaoCotista.ValorLiquido.Value * taxa.Value / 100M, 2);
                            posicaoCotista.ValorLiquido -= valorLiquidoOriginal;

                            valorIROriginal = Math.Round(posicaoCotista.ValorIR.Value * taxa.Value / 100M, 2);
                            posicaoCotista.ValorIR -= valorIROriginal;

                            valorIOFOriginal = Math.Round(posicaoCotista.ValorIOF.Value * taxa.Value / 100M, 2);
                            posicaoCotista.ValorIOF -= valorIOFOriginal;

                            valorPfeeOriginal = Math.Round(posicaoCotista.ValorPerformance.Value * taxa.Value / 100M, 2);
                            posicaoCotista.ValorPerformance -= valorPfeeOriginal;

                            valorRendimentoOriginal = Math.Round(posicaoCotista.ValorRendimento.Value * taxa.Value / 100M, 2);
                            posicaoCotista.ValorRendimento -= valorRendimentoOriginal;

                            PosicaoCotista posicaoCotistaNovo = new PosicaoCotista();
                            posicaoCotistaNovo.CotaAplicacao = cotaAplicacaoConvertida;
                            posicaoCotistaNovo.CotaDia = cotaDestino;
                            posicaoCotistaNovo.DataAplicacao = posicaoCotista.DataAplicacao;
                            posicaoCotistaNovo.DataConversao = posicaoCotista.DataConversao;
                            posicaoCotistaNovo.DataUltimaCobrancaIR = posicaoCotista.DataUltimaCobrancaIR;
                            posicaoCotistaNovo.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
                            posicaoCotistaNovo.IdCarteira = idFundoDestino;
                            posicaoCotistaNovo.IdCotista = posicaoCotista.IdCotista;
                            posicaoCotistaNovo.IdOperacao = posicaoCotista.IdOperacao;
                            posicaoCotistaNovo.Quantidade = quantidadeConvertida;
                            posicaoCotistaNovo.QuantidadeAntesCortes = quantidadeAntesCortesConvertida;
                            posicaoCotistaNovo.PosicaoIncorporada = "S";
                            posicaoCotistaNovo.QuantidadeBloqueada = 0;
                            posicaoCotistaNovo.QuantidadeInicial = posicaoCotista.QuantidadeInicial;
                            posicaoCotistaNovo.ValorAplicacao = valorAplicacaoOriginal;
                            posicaoCotistaNovo.ValorBruto = valorBrutoOriginal;
                            posicaoCotistaNovo.ValorIOF = valorIOFOriginal;
                            posicaoCotistaNovo.ValorIOFVirtual = 0;
                            posicaoCotistaNovo.ValorIR = valorIROriginal;
                            posicaoCotistaNovo.ValorLiquido = valorLiquidoOriginal;
                            posicaoCotistaNovo.ValorPerformance = valorPfeeOriginal;
                            posicaoCotistaNovo.ValorRendimento = valorRendimentoOriginal;
                            posicaoCotistaNovo.Save();
                        }

                        #region Saves em OperacaoCotista de origem e destino (resgate e aplicação)
                        OperacaoCotista operacaoCotistaOrigem = new OperacaoCotista();
                        operacaoCotistaOrigem.CotaOperacao = cotaOrigem;
                        operacaoCotistaOrigem.DataAgendamento = data;
                        operacaoCotistaOrigem.DataConversao = data;
                        operacaoCotistaOrigem.DataLiquidacao = data;
                        operacaoCotistaOrigem.DataOperacao = data;
                        operacaoCotistaOrigem.Fonte = (byte)FonteOperacaoCotista.Incorporacao;
                        operacaoCotistaOrigem.IdCarteira = idFundoOrigem;
                        operacaoCotistaOrigem.IdCotista = posicaoCotista.IdCotista.Value;
                        operacaoCotistaOrigem.IdFormaLiquidacao = 1; //Forcado
                        operacaoCotistaOrigem.IdPosicaoResgatada = posicaoCotista.IdPosicao.Value;
                        operacaoCotistaOrigem.PrejuizoUsado = 0;
                        operacaoCotistaOrigem.Quantidade = quantidadeOriginal;
                        operacaoCotistaOrigem.RendimentoResgate = 0;
                        operacaoCotistaOrigem.TipoOperacao = (byte)TipoOperacaoCotista.IncorporacaoResgate;
                        operacaoCotistaOrigem.TipoResgate = null;
                        operacaoCotistaOrigem.ValorBruto = valorAplicacaoOriginal;
                        operacaoCotistaOrigem.ValorCPMF = 0;
                        operacaoCotistaOrigem.ValorIOF = 0;
                        operacaoCotistaOrigem.ValorIR = 0;
                        operacaoCotistaOrigem.ValorLiquido = valorAplicacaoOriginal;
                        operacaoCotistaOrigem.ValorPerformance = 0;
                        operacaoCotistaOrigem.VariacaoResgate = 0;
                        operacaoCotistaOrigem.Save();

                        OperacaoCotista operacaoCotistaDestino = new OperacaoCotista();
                        operacaoCotistaDestino.CotaOperacao = cotaDestino;
                        operacaoCotistaDestino.DataAgendamento = data;
                        operacaoCotistaDestino.DataConversao = data;
                        operacaoCotistaDestino.DataLiquidacao = data;
                        operacaoCotistaDestino.DataOperacao = data;
                        operacaoCotistaDestino.Fonte = (byte)FonteOperacaoCotista.Incorporacao;
                        operacaoCotistaDestino.IdCarteira = idFundoDestino;
                        operacaoCotistaDestino.IdCotista = posicaoCotista.IdCotista.Value;
                        operacaoCotistaDestino.IdFormaLiquidacao = 1; //Forcado
                        operacaoCotistaDestino.IdPosicaoResgatada = posicaoCotista.IdPosicao.Value;
                        operacaoCotistaDestino.PrejuizoUsado = 0;
                        operacaoCotistaDestino.Quantidade = quantidadeConvertida;
                        operacaoCotistaDestino.RendimentoResgate = 0;
                        operacaoCotistaDestino.TipoOperacao = (byte)TipoOperacaoCotista.IncorporacaoAplicacao;
                        operacaoCotistaDestino.TipoResgate = null;
                        operacaoCotistaDestino.ValorBruto = valorAplicacaoOriginal;
                        operacaoCotistaDestino.ValorCPMF = 0;
                        operacaoCotistaDestino.ValorIOF = 0;
                        operacaoCotistaDestino.ValorIR = 0;
                        operacaoCotistaDestino.ValorLiquido = valorAplicacaoOriginal;
                        operacaoCotistaDestino.ValorPerformance = 0;
                        operacaoCotistaDestino.VariacaoResgate = 0;
                        operacaoCotistaDestino.Save();
                        #endregion

                        HistoricoIncorporacaoPosicao historicoIncorporacaoPosicao = new HistoricoIncorporacaoPosicao();
                        historicoIncorporacaoPosicao.DataConversao = posicaoCotista.DataConversao.Value;
                        historicoIncorporacaoPosicao.DataIncorporacao = data;
                        historicoIncorporacaoPosicao.IdCarteiraDestino = idFundoDestino;
                        historicoIncorporacaoPosicao.IdCarteiraOrigem = idFundoOrigem;
                        historicoIncorporacaoPosicao.IdPosicao = posicaoCotista.IdPosicao.Value;
                        historicoIncorporacaoPosicao.Save();
                    }

                    posicaoCotistaCollection.Save();

                    PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
                    prejuizoCotistaCollection.Query.Where(prejuizoCotistaCollection.Query.IdCarteira.Equal(idFundoOrigem),
                                                          prejuizoCotistaCollection.Query.IdCarteira.Equal(idCliente));
                    prejuizoCotistaCollection.Query.Load();

                    foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
                    {
                        PrejuizoCotista prejuizoCotistaDestino = new PrejuizoCotista();
                        if (prejuizoCotistaDestino.LoadByPrimaryKey(prejuizoCotista.IdCotista.Value, idFundoDestino, prejuizoCotista.Data.Value))
                        {
                            prejuizoCotistaDestino.ValorPrejuizo += prejuizoCotista.ValorPrejuizo.Value;
                            prejuizoCotistaDestino.Save();
                        }
                        else
                        {
                            prejuizoCotistaDestino = new PrejuizoCotista();
                            prejuizoCotistaDestino.Data = prejuizoCotista.Data.Value;
                            prejuizoCotistaDestino.DataLimiteCompensacao = prejuizoCotista.DataLimiteCompensacao;
                            prejuizoCotistaDestino.IdCarteira = idFundoDestino;
                            prejuizoCotistaDestino.IdCotista = prejuizoCotista.IdCotista.Value;
                            prejuizoCotistaDestino.ValorPrejuizo = prejuizoCotista.ValorPrejuizo.Value;
                            prejuizoCotistaDestino.Save();
                        }

                        prejuizoCotista.ValorPrejuizo = 0;
                    }

                    prejuizoCotistaCollection.Save();
                    #endregion

                    if (fundoClienteDestino.TipoControle.Value != (byte)TipoControleCliente.Cotista &&
                        fundoClienteOrigem.TipoControle.Value != (byte)TipoControleCliente.Cotista)
                    {
                        #region Transfere as posições de carteira
                        if (posicaoCotistaCollection.Count > 0)
                        {
                            //Busca o idContaDefault do cliente
                            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                            int idContaDefault = contaCorrente.RetornaContaDefault(idFundoDestino);

                            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                            posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idFundoOrigem));
                            posicaoBolsaCollection.Query.Load();

                            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                            {
                                posicaoBolsa.IdCliente = idFundoDestino;
                            }
                            posicaoBolsaCollection.Save();

                            #region Ajusta possíveis posições duplicadas e consolida o custo médio
                            PosicaoBolsaCollection posicaoBolsaCollectionDeletar = new PosicaoBolsaCollection();
                            PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection();
                            posicaoBolsaCollectionAux.Query.Where(posicaoBolsaCollectionAux.Query.IdCliente.Equal(idFundoDestino));
                            posicaoBolsaCollectionAux.Query.OrderBy(posicaoBolsaCollectionAux.Query.IdAgente.Ascending,
                                                                    posicaoBolsaCollectionAux.Query.CdAtivoBolsa.Ascending);
                            posicaoBolsaCollectionAux.Query.Load();

                            int i = 0;
                            while (i < posicaoBolsaCollectionAux.Count)
                            {
                                int idAgente = posicaoBolsaCollectionAux[i].IdAgente.Value;
                                string cdAtivoBolsa = posicaoBolsaCollectionAux[i].CdAtivoBolsa;

                                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);
                                decimal fator = fatorCotacaoBolsa.Fator.Value;

                                int idAgenteAnterior = idAgente;
                                string cdAtivoBolsaAnterior = cdAtivoBolsa;

                                decimal quantidadeFinal = 0;
                                decimal valorCustoFinal = 0;
                                decimal valorCustoLiquidoFinal = 0;
                                decimal valorMercadoFinal = 0;
                                decimal puCustoFinal = 0;
                                decimal puCustoLiquidoFinal = 0;
                                while (i < posicaoBolsaCollectionAux.Count && idAgente == idAgenteAnterior && cdAtivoBolsa == cdAtivoBolsaAnterior)
                                {
                                    decimal quantidade = posicaoBolsaCollectionAux[i].Quantidade.Value;
                                    decimal valorCusto = Utilitario.Truncate(quantidade * posicaoBolsaCollectionAux[i].PUCusto.Value / fator, 2);
                                    decimal valorCustoLiquido = Utilitario.Truncate(quantidade * posicaoBolsaCollectionAux[i].PUCustoLiquido.Value / fator, 2);
                                    decimal valorMercado = Utilitario.Truncate(quantidade * posicaoBolsaCollectionAux[i].PUMercado.Value / fator, 2);

                                    quantidadeFinal += quantidade;
                                    valorCustoFinal += valorCusto;
                                    valorCustoLiquidoFinal += valorCustoLiquido;
                                    valorMercadoFinal += valorMercado;

                                    puCustoFinal = (valorCustoFinal / quantidadeFinal) * fator;
                                    puCustoLiquidoFinal = (valorCustoLiquidoFinal / quantidadeFinal) * fator;

                                    i++;

                                    if (i < posicaoBolsaCollectionAux.Count)
                                    {
                                        idAgente = posicaoBolsaCollectionAux[i].IdAgente.Value;
                                        cdAtivoBolsa = posicaoBolsaCollectionAux[i].CdAtivoBolsa;
                                    }

                                    if (idAgente != idAgenteAnterior || cdAtivoBolsa != cdAtivoBolsaAnterior || i == posicaoBolsaCollectionAux.Count)
                                    {
                                        posicaoBolsaCollectionAux[i - 1].IdCliente = idFundoDestino;
                                        posicaoBolsaCollectionAux[i - 1].Quantidade = quantidadeFinal;
                                        posicaoBolsaCollectionAux[i - 1].ValorCustoLiquido = valorCustoLiquidoFinal;
                                        posicaoBolsaCollectionAux[i - 1].ValorMercado = valorMercadoFinal;
                                        posicaoBolsaCollectionAux[i - 1].PUCusto = puCustoFinal;
                                        posicaoBolsaCollectionAux[i - 1].PUCustoLiquido = puCustoLiquidoFinal;
                                    }
                                    else
                                    {
                                        posicaoBolsaCollectionDeletar.AttachEntity(posicaoBolsaCollectionAux[i - 1]);
                                    }
                                }
                            }
                            posicaoBolsaCollectionAux.Save();
                            posicaoBolsaCollectionDeletar.MarkAllAsDeleted();
                            posicaoBolsaCollectionDeletar.Save();
                            #endregion

                            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
                            posicaoBMFCollection.Query.Where(posicaoBMFCollection.Query.IdCliente.Equal(idFundoOrigem));
                            posicaoBMFCollection.Query.Load();

                            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
                            {
                                posicaoBMF.IdCliente = idFundoDestino;
                            }
                            posicaoBMFCollection.Save();

                            #region Ajusta possíveis posições duplicadas e consolida o custo médio
                            PosicaoBMFCollection posicaoBMFCollectionDeletar = new PosicaoBMFCollection();
                            PosicaoBMFCollection posicaoBMFCollectionAux = new PosicaoBMFCollection();
                            posicaoBMFCollectionAux.Query.Where(posicaoBMFCollectionAux.Query.IdCliente.Equal(idFundoDestino));
                            posicaoBMFCollectionAux.Query.OrderBy(posicaoBMFCollectionAux.Query.IdAgente.Ascending,
                                                                  posicaoBMFCollectionAux.Query.CdAtivoBMF.Ascending,
                                                                  posicaoBMFCollectionAux.Query.Serie.Ascending);
                            posicaoBMFCollectionAux.Query.Load();

                            i = 0;
                            while (i < posicaoBMFCollectionAux.Count)
                            {
                                int idAgente = posicaoBMFCollectionAux[i].IdAgente.Value;
                                string cdAtivoBMF = posicaoBMFCollectionAux[i].CdAtivoBMF;
                                string serie = posicaoBMFCollectionAux[i].Serie;

                                AtivoBMF ativoBMF = new AtivoBMF();
                                ativoBMF.LoadByPrimaryKey(cdAtivoBMF, serie);
                                decimal peso = ativoBMF.Peso.Value;

                                int idAgenteAnterior = idAgente;
                                string cdAtivoBMFAnterior = cdAtivoBMF;
                                string serieAnterior = serie;

                                int quantidadeFinal = 0;
                                decimal valorCustoFinal = 0;
                                decimal valorCustoLiquidoFinal = 0;
                                decimal valorMercadoFinal = 0;
                                decimal puCustoFinal = 0;
                                decimal puCustoLiquidoFinal = 0;
                                while (i < posicaoBMFCollectionAux.Count && idAgente == idAgenteAnterior && cdAtivoBMF == cdAtivoBMFAnterior && serie == serieAnterior)
                                {
                                    int quantidade = posicaoBMFCollectionAux[i].Quantidade.Value;
                                    decimal valorCusto = quantidade * posicaoBMFCollectionAux[i].PUCusto.Value * peso;
                                    decimal valorCustoLiquido = quantidade * posicaoBMFCollectionAux[i].PUCustoLiquido.Value * peso;
                                    decimal valorMercado = quantidade * posicaoBMFCollectionAux[i].PUMercado.Value * peso;

                                    quantidadeFinal += quantidade;
                                    valorCustoFinal += valorCusto;
                                    valorCustoLiquidoFinal += valorCustoLiquido;
                                    valorMercadoFinal += valorMercado;

                                    puCustoFinal = (valorCustoFinal / quantidadeFinal) / peso;
                                    puCustoLiquidoFinal = (valorCustoLiquidoFinal / quantidadeFinal) / peso;

                                    i++;

                                    if (i < posicaoBMFCollectionAux.Count)
                                    {
                                        idAgente = posicaoBMFCollectionAux[i].IdAgente.Value;
                                        cdAtivoBMF = posicaoBMFCollectionAux[i].CdAtivoBMF;
                                        serie = posicaoBMFCollectionAux[i].Serie;
                                    }

                                    if (idAgente != idAgenteAnterior || cdAtivoBMF != cdAtivoBMFAnterior || serie != serieAnterior || i == posicaoBMFCollectionAux.Count)
                                    {
                                        posicaoBMFCollectionAux[i - 1].IdCliente = idFundoDestino;
                                        posicaoBMFCollectionAux[i - 1].Quantidade = quantidadeFinal;
                                        posicaoBMFCollectionAux[i - 1].ValorCustoLiquido = valorCustoLiquidoFinal;
                                        posicaoBMFCollectionAux[i - 1].ValorMercado = valorMercadoFinal;
                                        posicaoBMFCollectionAux[i - 1].PUCusto = puCustoFinal;
                                        posicaoBMFCollectionAux[i - 1].PUCustoLiquido = puCustoLiquidoFinal;
                                    }
                                    else
                                    {
                                        posicaoBMFCollectionDeletar.AttachEntity(posicaoBMFCollectionAux[i - 1]);
                                    }
                                }
                            }
                            posicaoBMFCollectionAux.Save();
                            posicaoBMFCollectionDeletar.MarkAllAsDeleted();
                            posicaoBMFCollectionDeletar.Save();
                            #endregion

                            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                            posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idFundoOrigem));
                            posicaoEmprestimoBolsaCollection.Query.Load();

                            foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
                            {
                                posicaoEmprestimoBolsa.IdCliente = idFundoDestino;
                            }
                            posicaoEmprestimoBolsaCollection.Save();

                            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                            posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idFundoOrigem));
                            posicaoTermoBolsaCollection.Query.Load();

                            foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
                            {
                                posicaoTermoBolsa.IdCliente = idFundoDestino;
                            }
                            posicaoTermoBolsaCollection.Save();

                            PosicaoFundoCollection posicaoFundoAtivoCollection = new PosicaoFundoCollection();
                            posicaoFundoAtivoCollection.Query.Where(posicaoFundoAtivoCollection.Query.IdCliente.Equal(idFundoOrigem));
                            posicaoFundoAtivoCollection.Query.Load();

                            foreach (PosicaoFundo posicaoFundoAtivo in posicaoFundoAtivoCollection)
                            {
                                posicaoFundoAtivo.IdCliente = idFundoDestino;
                            }
                            posicaoFundoAtivoCollection.Save();

                            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                            posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idFundoOrigem));
                            posicaoRendaFixaCollection.Query.Load();

                            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                            {
                                posicaoRendaFixa.IdCliente = idFundoDestino;
                            }
                            posicaoRendaFixaCollection.Save();

                            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
                            posicaoSwapCollection.Query.Where(posicaoSwapCollection.Query.IdCliente.Equal(idFundoOrigem));
                            posicaoSwapCollection.Query.Load();

                            foreach (PosicaoSwap posicaoSwap in posicaoSwapCollection)
                            {
                                posicaoSwap.IdCliente = idFundoDestino;
                            }
                            posicaoSwapCollection.Save();

                            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idFundoOrigem));
                            liquidacaoCollection.Query.Load();

                            foreach (Liquidacao liquidacao in liquidacaoCollection)
                            {
                                liquidacao.IdCliente = idFundoDestino;
                                liquidacao.IdConta = idContaDefault;
                            }
                            liquidacaoCollection.Save();

                            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
                            saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idFundoOrigem),
                                                             saldoCaixaCollection.Query.Data.GreaterThanOrEqual(data));
                            saldoCaixaCollection.Query.Load();

                            foreach (SaldoCaixa saldoCaixa in saldoCaixaCollection)
                            {
                                saldoCaixa.IdCliente = idFundoDestino;
                                saldoCaixa.IdConta = idContaDefault;

                                SaldoCaixa saldoCaixaExiste = new SaldoCaixa();
                                if (saldoCaixaExiste.LoadByPrimaryKey(idFundoDestino, data, idContaDefault))
                                {
                                    saldoCaixaExiste.SaldoFechamento += saldoCaixa.SaldoFechamento.Value;
                                    saldoCaixaExiste.Save();
                                }
                                else
                                {
                                    SaldoCaixa saldoCaixaNovo = new SaldoCaixa();
                                    saldoCaixaNovo.IdCliente = idFundoDestino;
                                    saldoCaixaNovo.Data = data;
                                    saldoCaixaNovo.IdConta = idContaDefault;
                                    saldoCaixaNovo.SaldoAbertura = 0;
                                    saldoCaixaNovo.SaldoFechamento = saldoCaixa.SaldoFechamento.Value;

                                    saldoCaixaNovo.Save();
                                }

                                saldoCaixa.SaldoFechamento = 0;
                            }

                            ControllerFundo controllerFundo = new ControllerFundo();
                            controllerFundo.ProcessaCotaLiquidaFechamento(idFundoDestino, data);

                            PosicaoCotista posicaoCotistaCotas = new PosicaoCotista();
                            decimal quantidadeCotasNova = posicaoCotistaCotas.RetornaTotalCotas(idFundoDestino);

                            HistoricoCota historicoCotasAtualizar = new HistoricoCota();
                            historicoCotasAtualizar.LoadByPrimaryKey(data, idFundoDestino);
                            historicoCotasAtualizar.QuantidadeFechamento = quantidadeCotasNova;
                            historicoCotasAtualizar.Save();
                        }
                        #endregion
                    }
                    #endregion
                }
                catch (Exception)
                {
                    throw new Exception("Fundo destino precisa ser calculado antes de se calcular o Fundo origem.");
                }

            }
        }

        /// <summary>
        /// Efetua o processamento de Incorporação, Fusão e Cisão
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaIncorporacaoFusaoCisao(int idCliente, DateTime data)
        {
            EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.DataPosicao.Equal(data));
            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.Status.Equal("Ativo"));
            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.IdCarteiraOrigem.Equal(idCliente));
            eventoFundoCollection.Query.Load();

            foreach (EventoFundo eventoFundo in eventoFundoCollection)
            {
                //Apaga historico de Incorporacao
                this.excluiHistoricoIncorporacao(eventoFundo);

                //Exclui movimentos gerados
                this.ExcluiMovimentosGerados(eventoFundo, idCliente);
                //Apaga as posições geradas anteriormente
                this.ExcluiPosicoesGeradas(eventoFundo, idCliente);

                int idFundoOrigem = eventoFundo.IdCarteiraOrigem.Value;
                int idFundoDestino = eventoFundo.IdCarteiraDestino.Value;
                int tipoEvento = (int)eventoFundo.TipoEvento;
                decimal? taxa = eventoFundo.Percentual;

                Cliente fundoClienteOrigem = new Cliente();
                fundoClienteOrigem.LoadByPrimaryKey(idFundoOrigem);

                Cliente fundoClienteDestino = new Cliente();
                fundoClienteDestino.LoadByPrimaryKey(idFundoDestino);

                Carteira fundoDestino = new Carteira();
                fundoDestino.LoadByPrimaryKey(idFundoDestino);

                Carteira fundoOrigem = new Carteira();
                fundoOrigem.LoadByPrimaryKey(idFundoOrigem);

                decimal cotaDestino = 0;
                HistoricoCota historicoCota = new HistoricoCota();
                try
                {
                    historicoCota.BuscaValorCotaDia(idFundoDestino, data);

                    if (fundoDestino.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                    {
                        cotaDestino = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        cotaDestino = historicoCota.CotaFechamento.Value;
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }

                decimal cotaOrigem = 0;
                if (idFundoOrigem == idCliente)
                {
                    historicoCota = new HistoricoCota();
                    try
                    {
                        historicoCota.BuscaValorCotaDia(idFundoOrigem, data);

                        if (fundoDestino.TipoCota.Value == (byte)TipoCotaFundo.Abertura)
                        {
                            cotaOrigem = historicoCota.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaOrigem = historicoCota.CotaFechamento.Value;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception(e.Message);
                    }
                }
                this.eventoPosicaoFundo(eventoFundo, idCliente, cotaDestino, fundoDestino);
                this.eventoPrejuizoFundo(eventoFundo, idCliente);

                //Tratamento para posição cotista
                PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("PC");
                EventoFundoCautelaQuery eventoFundoCautelaQuery = new EventoFundoCautelaQuery("EFC");

                posicaoCotistaQuery.Select(posicaoCotistaQuery, eventoFundoCautelaQuery.Quantidade.As("QtdEvento"));
                posicaoCotistaQuery.InnerJoin(eventoFundoCautelaQuery).On(eventoFundoCautelaQuery.IdCotista.Equal(posicaoCotistaQuery.IdCotista) &
                                                                          eventoFundoCautelaQuery.DataAplicacao.Equal(posicaoCotistaQuery.DataAplicacao) &
                                                                          eventoFundoCautelaQuery.IdOperacao.Equal(posicaoCotistaQuery.IdOperacao.Coalesce("0")));
                posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCarteira.Equal(idFundoOrigem),
                                          posicaoCotistaQuery.IdCarteira.Equal(idCliente),
                                          eventoFundoCautelaQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo),
                                          eventoFundoCautelaQuery.Processar.In(Convert.ToInt16(StatusProcessamentoCisao.Processar), Convert.ToInt16(StatusProcessamentoCisao.Processado)));

                PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                posicaoCotistaCollection.Load(posicaoCotistaQuery);

                foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                {
                    if (fundoClienteDestino.DataDia.Value != data)
                    {
                        throw new Exception("Incorporação - O Fundo destino deve estar na data da incorporação para o processo ser realizado.");
                    }

                    decimal quantidade = Convert.ToDecimal(posicaoCotista.GetColumn("QtdEvento"));

                    this.eventoCotista(eventoFundo, posicaoCotista, cotaOrigem, cotaDestino, fundoDestino, fundoOrigem, quantidade);
                }
                posicaoCotistaCollection.Save();

                this.eventoPrejuizoCotista(eventoFundo, idCliente);

                if (fundoClienteDestino.TipoControle.Value != (byte)TipoControleCliente.Cotista &&
                    fundoClienteOrigem.TipoControle.Value != (byte)TipoControleCliente.Cotista)
                {
                    this.eventoBolsa(eventoFundo);
                    this.eventoBMF(eventoFundo);
                    this.eventoRendaFixa(eventoFundo);
                    this.eventoSwap(eventoFundo);
                    this.eventoFundo(eventoFundo);
                    this.eventoLiquidacao(eventoFundo);
                    this.eventoCaixa(eventoFundo);

                    ControllerFundo controllerFundo = new ControllerFundo();
                    controllerFundo.ProcessaCotaLiquidaFechamento(idFundoDestino, data, false);

                    PosicaoCotista posicaoCotista = new PosicaoCotista();
                    decimal quantidadeCotasNova = posicaoCotista.RetornaTotalCotas(eventoFundo.IdCarteiraDestino.Value);

                    HistoricoCota historicoCotasAtualizar = new HistoricoCota();
                    historicoCotasAtualizar.LoadByPrimaryKey(eventoFundo.DataPosicao.Value, eventoFundo.IdCarteiraDestino.Value);
                    historicoCotasAtualizar.QuantidadeFechamento = quantidadeCotasNova;
                    historicoCotasAtualizar.Save();

                    //Atualiza PL e cota do fundo origem
                    controllerFundo.ProcessaCotaBrutaFechamento(idFundoOrigem, data, false);
                    controllerFundo.ProcessaCotaLiquidaFechamento(idFundoOrigem, data);


                    #region Efetua ajuste para manter cota de origem igual a cota pré cisão
                    //Busca o idContaDefault do cliente-origem
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idFundoOrigem);

                    HistoricoCota historicoCotaOrigem = new HistoricoCota();
                    historicoCotaOrigem.LoadByPrimaryKey(data, idFundoOrigem);
                    decimal novaCota = historicoCotaOrigem.CotaFechamento.Value;
                    decimal quantidadeCotas = posicaoCotista.RetornaTotalCotas(idFundoOrigem);

                    decimal valorAjuste = (cotaOrigem - novaCota) * quantidadeCotas;

                    DateTime dataProxima = new DateTime();
                    if (fundoClienteOrigem.IdLocal.Value == (int)LocalFeriadoFixo.Brasil)
                    {
                        dataProxima = Calendario.AdicionaDiaUtil(data, 1);
                    }
                    else
                    {
                        dataProxima = Calendario.AdicionaDiaUtil(data, 1, fundoClienteOrigem.IdLocal.Value, TipoFeriado.Outros);
                    }

                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idFundoOrigem;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataProxima;
                    liquidacao.Descricao = "Ajuste de Compensação de Cota - Evento Cisão para o fundo - " + idFundoDestino.ToString();
                    liquidacao.Valor = valorAjuste;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Compensacao;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.AjusteCompensacaoCotaEvento;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Save();

                    EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                    eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                    eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Liquidacao;
                    eventoFundoPosicaoAtivos.IdPosicao = liquidacao.IdLiquidacao;
                    eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                    eventoFundoPosicaoAtivos.Save();

                    controllerFundo.ProcessaCotaBrutaFechamento(idFundoOrigem, data, false);
                    controllerFundo.ProcessaCotaLiquidaFechamento(idFundoOrigem, data);

                    //Verifica se possui travamento de cota
                    TravamentoCotasCollection travamentoCotasCollection = new TravamentoCotasCollection();
                    travamentoCotasCollection.Query.Where(travamentoCotasCollection.Query.IdCarteira.Equal(idFundoDestino),
                                                          travamentoCotasCollection.Query.DataProcessamento.Equal(data));
                    travamentoCotasCollection.Query.Load();
                    if (travamentoCotasCollection.Count > 0)
                    {
                        //Busca o idContaDefault do cliente-origem
                        idContaDefault = contaCorrente.RetornaContaDefault(idFundoDestino);

                        liquidacao = new Liquidacao();
                        liquidacao.IdCliente = idFundoDestino;
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataProxima;
                        liquidacao.Descricao = "Ajuste de Compensação de Cota - Evento Cisão do fundo - " + idFundoOrigem.ToString();
                        liquidacao.Valor = valorAjuste * (-1);
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Compensacao;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.AjusteCompensacaoCotaEvento;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdConta = idContaDefault;
                        liquidacao.Save();

                        LiquidacaoHistorico.GeraLiquidacaoHistorico(liquidacao, data);

                        eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                        eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                        eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Liquidacao;
                        eventoFundoPosicaoAtivos.IdPosicao = liquidacao.IdLiquidacao;
                        eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                        eventoFundoPosicaoAtivos.Save();
                    }

                    controllerFundo.ProcessaPLFechamentoDepoisCota(idFundoDestino, data, true);
                    controllerFundo.ProcessaCotaLiquidaFechamento(idFundoDestino, data, false);
                    #endregion

                    #region Caso seja um evento com transferencia total (100%), verifica se deve zerar saldo
                    if (eventoFundo.Percentual.Value == 100)
                    {
                        historicoCotaOrigem.LoadByPrimaryKey(data, idFundoOrigem);
                        if (historicoCotaOrigem.PLFechamento.Value != 0)
                        {
                            idContaDefault = contaCorrente.RetornaContaDefault(idFundoOrigem);

                            liquidacao = new Liquidacao();
                            liquidacao.IdCliente = idFundoOrigem;
                            liquidacao.DataLancamento = data;
                            liquidacao.DataVencimento = data;
                            liquidacao.Descricao = "Zeragem de saldo";
                            liquidacao.Valor = historicoCotaOrigem.PLFechamento.Value * (-1);
                            liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Compensacao;
                            liquidacao.Origem = (int)OrigemLancamentoLiquidacao.AjusteCompensacaoCotaEvento;
                            liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                            liquidacao.IdConta = idContaDefault;
                            liquidacao.Save();

                            eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                            eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                            eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Liquidacao;
                            eventoFundoPosicaoAtivos.IdPosicao = liquidacao.IdLiquidacao;
                            eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                            eventoFundoPosicaoAtivos.Save();

                            controllerFundo.ProcessaCotaBrutaFechamento(idFundoOrigem, data, false);
                            controllerFundo.ProcessaCotaLiquidaFechamento(idFundoOrigem, data);
                        }
                    }

                    #endregion
                }
            }
        }



        private void eventoPosicaoFundo(EventoFundo eventoFundo, int idCliente, decimal cotaDestino, Carteira fundoDestino)
        {
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("PF");
            EventoFundoCautelaQuery eventoFundoCautelaQuery = new EventoFundoCautelaQuery("EFC");

            posicaoFundoQuery.Select(posicaoFundoQuery, eventoFundoCautelaQuery.Quantidade.As("QtdEvento"));
            posicaoFundoQuery.InnerJoin(eventoFundoCautelaQuery).On(posicaoFundoQuery.IdCliente.Equal(eventoFundoCautelaQuery.IdCotista));
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCarteira.Equal(eventoFundo.IdCarteiraOrigem),
                                    posicaoFundoQuery.IdCliente.Equal(idCliente),
                                    eventoFundoCautelaQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.Load(posicaoFundoQuery);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                decimal quantidade = Convert.ToDecimal(posicaoFundo.GetColumn("QtdEvento"));
                decimal percentual = quantidade / posicaoFundo.Quantidade.Value;

                decimal valorBrutoDestino = (posicaoFundo.ValorBruto.Value * percentual);
                decimal valorBrutoOrigem = posicaoFundo.ValorBruto.Value - valorBrutoDestino;
                decimal valorBrutoAntesCortes = posicaoFundo.QuantidadeAntesCortes.Value * posicaoFundo.CotaDia.Value;

                decimal quantidadeConvertida = Math.Round(valorBrutoDestino / cotaDestino, fundoDestino.CasasDecimaisCota.Value);
                decimal quantidadeAntesCortesConvertida = Math.Round(valorBrutoAntesCortes / cotaDestino, fundoDestino.CasasDecimaisCota.Value);

                decimal cotaAplicacaoConvertida = Math.Round(cotaDestino * posicaoFundo.CotaAplicacao.Value / posicaoFundo.CotaDia.Value, 8);

                //Cria posição da carteira destino
                PosicaoFundo posicaoFundoDestino = this.copiaPosicaoFundo(posicaoFundo);
                posicaoFundoDestino.Quantidade = quantidadeConvertida;
                posicaoFundoDestino.QuantidadeAntesCortes = (quantidadeAntesCortesConvertida * percentual);
                posicaoFundoDestino.ValorAplicacao = (posicaoFundo.ValorAplicacao * percentual);
                posicaoFundoDestino.ValorBruto = (posicaoFundo.ValorBruto * percentual);
                posicaoFundoDestino.ValorLiquido = (posicaoFundo.ValorLiquido * percentual);
                posicaoFundoDestino.QuantidadeInicial = (posicaoFundo.QuantidadeInicial * percentual);
                posicaoFundoDestino.QuantidadeBloqueada = (posicaoFundo.QuantidadeBloqueada * percentual);
                posicaoFundoDestino.ValorIR = (posicaoFundo.ValorIR * percentual);
                posicaoFundoDestino.ValorIOF = (posicaoFundo.ValorIOF * percentual);
                posicaoFundoDestino.ValorPerformance = (posicaoFundo.ValorPerformance * percentual);
                posicaoFundoDestino.ValorIOFVirtual = (posicaoFundo.ValorIOFVirtual * percentual);
                posicaoFundoDestino.QuantidadeAntesCortes = (posicaoFundo.QuantidadeAntesCortes * percentual);
                posicaoFundoDestino.IdCarteira = eventoFundo.IdCarteiraDestino;
                posicaoFundoDestino.CotaDia = cotaDestino;
                posicaoFundoDestino.CotaAplicacao = cotaAplicacaoConvertida;
                posicaoFundoDestino.PosicaoIncorporada = "S";
                posicaoFundoDestino.Save();

                EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Fundos;
                eventoFundoPosicaoAtivos.IdPosicao = posicaoFundoDestino.IdPosicao;
                eventoFundoPosicaoAtivos.IdCliente = idCliente;
                eventoFundoPosicaoAtivos.Save();

                PosicaoFundo posicao = new PosicaoFundo();
                posicao.LoadByPrimaryKey(posicaoFundoDestino.IdPosicao.Value);

                EventoFundoMovimentoAtivos eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
                eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.Fundos;
                eventoFundoMovimentoAtivos.IdOperacao = posicao.IdOperacao;
                eventoFundoMovimentoAtivos.IdCliente = idCliente;
                eventoFundoMovimentoAtivos.Save();

                posicaoFundo.Quantidade = posicaoFundo.Quantidade - (posicaoFundo.Quantidade * percentual);
                posicaoFundo.ValorAplicacao = posicaoFundo.ValorAplicacao - (posicaoFundo.ValorAplicacao * percentual);
                posicaoFundo.ValorBruto = posicaoFundo.ValorBruto - (posicaoFundo.ValorBruto * percentual);
                posicaoFundo.ValorLiquido = posicaoFundo.ValorLiquido - (posicaoFundo.ValorLiquido * percentual);
                posicaoFundo.QuantidadeInicial = posicaoFundo.QuantidadeInicial - (posicaoFundo.QuantidadeInicial * percentual);
                posicaoFundo.QuantidadeBloqueada = posicaoFundo.QuantidadeBloqueada - (posicaoFundo.QuantidadeBloqueada * percentual);
                posicaoFundo.ValorIR = posicaoFundo.ValorIR - (posicaoFundo.ValorIR * percentual);
                posicaoFundo.ValorIOF = posicaoFundo.ValorIOF - (posicaoFundo.ValorIOF * percentual);
                posicaoFundo.ValorPerformance = posicaoFundo.ValorPerformance - (posicaoFundo.ValorPerformance * percentual);
                posicaoFundo.ValorIOFVirtual = posicaoFundo.ValorIOFVirtual - (posicaoFundo.ValorIOFVirtual * percentual);
                posicaoFundo.QuantidadeAntesCortes = posicaoFundo.QuantidadeAntesCortes - (posicaoFundo.QuantidadeAntesCortes * percentual);

                HistoricoIncorporacaoPosicao historicoIncorporacaoPosicao = new HistoricoIncorporacaoPosicao();
                historicoIncorporacaoPosicao.DataConversao = posicaoFundo.DataConversao.Value;
                historicoIncorporacaoPosicao.DataIncorporacao = eventoFundo.DataPosicao;
                historicoIncorporacaoPosicao.IdCarteiraDestino = eventoFundo.IdCarteiraDestino;
                historicoIncorporacaoPosicao.IdCarteiraOrigem = eventoFundo.IdCarteiraOrigem;
                historicoIncorporacaoPosicao.IdPosicao = posicaoFundo.IdPosicao.Value;
                historicoIncorporacaoPosicao.Save();



            }
            posicaoFundoCollection.Save();
        }

        private void eventoPrejuizoFundo(EventoFundo eventoFundo, int idCliente)
        {
            PrejuizoFundoCollection prejuizoFundoCollection = new PrejuizoFundoCollection();
            prejuizoFundoCollection.Query.Where(prejuizoFundoCollection.Query.IdCarteira.Equal(eventoFundo.IdCarteiraOrigem),
                                                prejuizoFundoCollection.Query.IdCliente.Equal(idCliente));
            prejuizoFundoCollection.Query.Load();

            foreach (PrejuizoFundo prejuizoFundo in prejuizoFundoCollection)
            {
                decimal prejuizoDestino = (prejuizoFundo.ValorPrejuizo.Value * eventoFundo.Percentual.Value) / 100;
                decimal prejuizoOrigem = (prejuizoFundo.ValorPrejuizo.Value - prejuizoDestino);

                PrejuizoFundo prejuizoFundoDestino = new PrejuizoFundo();
                if (prejuizoFundoDestino.LoadByPrimaryKey(idCliente, eventoFundo.IdCarteiraDestino.Value, prejuizoFundo.Data.Value))
                {
                    prejuizoFundoDestino.ValorPrejuizo += prejuizoDestino;
                    prejuizoFundoDestino.Save();
                }
                else
                {
                    prejuizoFundoDestino = new PrejuizoFundo();
                    prejuizoFundoDestino.Data = prejuizoFundo.Data.Value;
                    prejuizoFundoDestino.DataLimiteCompensacao = prejuizoFundo.DataLimiteCompensacao;
                    prejuizoFundoDestino.IdCarteira = eventoFundo.IdCarteiraDestino;
                    prejuizoFundoDestino.IdCliente = idCliente;
                    prejuizoFundoDestino.ValorPrejuizo = prejuizoDestino;
                    prejuizoFundoDestino.Save();
                }

                prejuizoFundo.ValorPrejuizo = prejuizoOrigem;
            }

            prejuizoFundoCollection.Save();
        }

        private void eventoCotista(EventoFundo eventoFundo, 
                                   PosicaoCotista posicaoCotista, 
                                   decimal cotaOrigem,
                                   decimal cotaDestino,
                                   Carteira fundoDestino,
                                   Carteira fundoOrigem,
                                   decimal quantidade)
        {
            //decimal valorBruto = posicaoCotista.ValorBruto.Value;
            decimal valorBruto = quantidade * cotaOrigem;
            decimal valorBrutoAntesCortes = posicaoCotista.QuantidadeAntesCortes.Value * posicaoCotista.CotaDia.Value;

            decimal quantidadeConvertida = Math.Round(valorBruto / cotaDestino, fundoDestino.CasasDecimaisCota.Value);
            decimal quantidadeAntesCortesConvertida = Math.Round(valorBrutoAntesCortes / cotaDestino, fundoDestino.CasasDecimaisCota.Value);

            decimal cotaAplicacaoConvertida = Math.Round(cotaDestino * posicaoCotista.CotaAplicacao.Value / posicaoCotista.CotaDia.Value, 8);

            decimal quantidadeOriginal = 0;
            decimal valorAplicacaoOriginal = 0;
            decimal valorBrutoOriginal = 0;
            decimal valorLiquidoOriginal = 0;
            decimal valorIROriginal = 0;
            decimal valorIOFOriginal = 0;
            decimal valorPfeeOriginal = 0;
            decimal valorRendimentoOriginal = 0;
            decimal quantidadeInicial = 0;

            PosicaoCotista posicaoCotistaNovo = null;

            if (!eventoFundo.Percentual.HasValue || eventoFundo.Percentual.Value == 100)
            {
                quantidadeOriginal = posicaoCotista.Quantidade.Value;
                valorAplicacaoOriginal = posicaoCotista.ValorAplicacao.Value;
                valorBrutoOriginal = posicaoCotista.ValorBruto.Value;
                valorLiquidoOriginal = posicaoCotista.ValorLiquido.Value;
                valorIROriginal = posicaoCotista.ValorIR.Value;
                valorIOFOriginal = posicaoCotista.ValorIOF.Value;
                valorPfeeOriginal = posicaoCotista.ValorPerformance.Value;
                valorRendimentoOriginal = posicaoCotista.ValorRendimento.Value;

                //Incorporação total
                posicaoCotista.Quantidade = quantidadeConvertida;
                posicaoCotista.QuantidadeAntesCortes = quantidadeAntesCortesConvertida;
                posicaoCotista.IdCarteira = eventoFundo.IdCarteiraDestino;
                posicaoCotista.CotaDia = cotaDestino;
                posicaoCotista.CotaAplicacao = cotaAplicacaoConvertida;
                posicaoCotista.PosicaoIncorporada = "S";

                EventoFundoPosicaoCautelas eventoFundoPosicaoCautelas = new EventoFundoPosicaoCautelas();
                eventoFundoPosicaoCautelas.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoPosicaoCautelas.IdPosicao = posicaoCotista.IdPosicao;
                eventoFundoPosicaoCautelas.IdCliente = eventoFundo.IdCarteiraOrigem;
                eventoFundoPosicaoCautelas.Save();
            }
            else
            {

                decimal percentual = quantidade / posicaoCotista.Quantidade.Value;
                quantidadeOriginal = Math.Round(posicaoCotista.Quantidade.Value * percentual, fundoDestino.CasasDecimaisCota.Value);

                //quantidadeConvertida = Math.Round(quantidadeConvertida * eventoFundo.Percentual.Value / 100M, fundoDestino.CasasDecimaisCota.Value);
                quantidadeAntesCortesConvertida = Math.Round(quantidadeAntesCortesConvertida * percentual, fundoDestino.CasasDecimaisQuantidade.Value);

                //Incorporação parcial
                if (fundoOrigem.IsTruncaQuantidade())
                {
                    posicaoCotista.Quantidade = Utilitario.Truncate(posicaoCotista.Quantidade.Value - (posicaoCotista.Quantidade.Value * percentual), fundoOrigem.CasasDecimaisQuantidade.Value);
                    posicaoCotista.QuantidadeAntesCortes = Math.Round(posicaoCotista.QuantidadeAntesCortes.Value - (posicaoCotista.QuantidadeAntesCortes.Value * percentual), fundoOrigem.CasasDecimaisQuantidade.Value);
                }
                else
                {
                    posicaoCotista.Quantidade = Math.Round(posicaoCotista.Quantidade.Value - (posicaoCotista.Quantidade.Value * percentual), fundoOrigem.CasasDecimaisQuantidade.Value);
                    posicaoCotista.QuantidadeAntesCortes = Math.Round(posicaoCotista.QuantidadeAntesCortes.Value - (posicaoCotista.QuantidadeAntesCortes.Value * percentual), fundoOrigem.CasasDecimaisQuantidade.Value);
                }

                valorAplicacaoOriginal = Math.Round(posicaoCotista.ValorAplicacao.Value * percentual, 2);
                posicaoCotista.ValorAplicacao -= valorAplicacaoOriginal;

                valorBrutoOriginal = Math.Round(posicaoCotista.ValorBruto.Value * percentual, 2);
                posicaoCotista.ValorBruto -= valorBrutoOriginal;

                valorLiquidoOriginal = Math.Round(posicaoCotista.ValorLiquido.Value * percentual, 2);
                posicaoCotista.ValorLiquido -= valorLiquidoOriginal;

                valorIROriginal = Math.Round(posicaoCotista.ValorIR.Value * percentual, 2);
                posicaoCotista.ValorIR -= valorIROriginal;

                valorIOFOriginal = Math.Round(posicaoCotista.ValorIOF.Value * percentual, 2);
                posicaoCotista.ValorIOF -= valorIOFOriginal;

                valorPfeeOriginal = Math.Round(posicaoCotista.ValorPerformance.Value * percentual, 2);
                posicaoCotista.ValorPerformance -= valorPfeeOriginal;

                valorRendimentoOriginal = Math.Round(posicaoCotista.ValorRendimento.Value * percentual, 2);
                posicaoCotista.ValorRendimento -= valorRendimentoOriginal;

                if (fundoOrigem.IsTruncaQuantidade())
                {
                    quantidadeInicial = Utilitario.Truncate(posicaoCotista.QuantidadeInicial.Value * percentual, fundoOrigem.CasasDecimaisQuantidade.Value);
                    posicaoCotista.QuantidadeInicial = Utilitario.Truncate(posicaoCotista.QuantidadeInicial.Value - quantidadeInicial, fundoOrigem.CasasDecimaisQuantidade.Value);
                }
                else
                {
                    quantidadeInicial = Math.Round(posicaoCotista.QuantidadeInicial.Value * percentual, fundoOrigem.CasasDecimaisQuantidade.Value);
                    posicaoCotista.QuantidadeInicial = Math.Round(posicaoCotista.QuantidadeInicial.Value - quantidadeInicial, fundoOrigem.CasasDecimaisQuantidade.Value);
                }


                posicaoCotistaNovo = new PosicaoCotista();
                posicaoCotistaNovo.CotaAplicacao = cotaAplicacaoConvertida;
                posicaoCotistaNovo.CotaDia = cotaDestino;
                posicaoCotistaNovo.DataAplicacao = posicaoCotista.DataAplicacao;
                posicaoCotistaNovo.DataConversao = posicaoCotista.DataConversao;
                posicaoCotistaNovo.DataUltimoCortePfee = posicaoCotista.DataUltimoCortePfee;
                posicaoCotistaNovo.IdCarteira = eventoFundo.IdCarteiraDestino;
                posicaoCotistaNovo.IdCotista = posicaoCotista.IdCotista;
                posicaoCotistaNovo.IdOperacao = posicaoCotista.IdOperacao;
                posicaoCotistaNovo.Quantidade = quantidadeConvertida;
                posicaoCotistaNovo.QuantidadeAntesCortes = quantidadeAntesCortesConvertida;
                posicaoCotistaNovo.PosicaoIncorporada = "S";
                posicaoCotistaNovo.QuantidadeBloqueada = 0;
                posicaoCotistaNovo.QuantidadeInicial = quantidadeConvertida;
                posicaoCotistaNovo.ValorAplicacao = valorAplicacaoOriginal;
                posicaoCotistaNovo.ValorBruto = valorBrutoOriginal;
                posicaoCotistaNovo.ValorIOF = valorIOFOriginal;
                posicaoCotistaNovo.ValorIOFVirtual = 0;
                posicaoCotistaNovo.ValorIR = valorIROriginal;
                posicaoCotistaNovo.ValorLiquido = valorLiquidoOriginal;
                posicaoCotistaNovo.ValorPerformance = valorPfeeOriginal;
                posicaoCotistaNovo.ValorRendimento = valorRendimentoOriginal;
                posicaoCotistaNovo.DataUltimaCobrancaIR = eventoFundo.DataPosicao.Value;
                posicaoCotistaNovo.Save();

                EventoFundoPosicaoCautelas eventoFundoPosicaoCautelas = new EventoFundoPosicaoCautelas();
                eventoFundoPosicaoCautelas.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoPosicaoCautelas.IdPosicao = posicaoCotistaNovo.IdPosicao;
                eventoFundoPosicaoCautelas.IdCliente = eventoFundo.IdCarteiraOrigem;
                eventoFundoPosicaoCautelas.Save();

            }

            OperacaoCotista operacaoCotistaOrigem = new OperacaoCotista();
            operacaoCotistaOrigem.CotaOperacao = cotaOrigem;
            operacaoCotistaOrigem.DataAgendamento = eventoFundo.DataPosicao;
            operacaoCotistaOrigem.DataConversao = eventoFundo.DataPosicao;
            operacaoCotistaOrigem.DataLiquidacao = eventoFundo.DataPosicao;
            operacaoCotistaOrigem.DataRegistro = eventoFundo.DataPosicao;
            operacaoCotistaOrigem.DataOperacao = eventoFundo.DataPosicao;
            operacaoCotistaOrigem.Fonte = (byte)FonteOperacaoCotista.Incorporacao;
            operacaoCotistaOrigem.IdCarteira = eventoFundo.IdCarteiraOrigem;
            operacaoCotistaOrigem.IdCotista = posicaoCotista.IdCotista.Value;
            operacaoCotistaOrigem.IdFormaLiquidacao = 1; //Forcado
            operacaoCotistaOrigem.IdPosicaoResgatada = posicaoCotista.IdPosicao.Value;
            operacaoCotistaOrigem.PrejuizoUsado = 0;
            operacaoCotistaOrigem.Quantidade = quantidadeOriginal;
            operacaoCotistaOrigem.RendimentoResgate = 0;
            operacaoCotistaOrigem.TipoOperacao = (byte)TipoOperacaoCotista.IncorporacaoResgate;
            operacaoCotistaOrigem.TipoResgate = null;
            operacaoCotistaOrigem.ValorBruto = valorAplicacaoOriginal;
            operacaoCotistaOrigem.ValorCPMF = 0;
            operacaoCotistaOrigem.ValorIOF = 0;
            operacaoCotistaOrigem.ValorIR = 0;
            operacaoCotistaOrigem.ValorLiquido = valorAplicacaoOriginal;
            operacaoCotistaOrigem.ValorPerformance = 0;
            operacaoCotistaOrigem.VariacaoResgate = 0;
            operacaoCotistaOrigem.Save();

            EventoFundoMovimentoCautelas eventoFundoMovimentoCautelas = new EventoFundoMovimentoCautelas();
            eventoFundoMovimentoCautelas.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoCautelas.IdOperacao = operacaoCotistaOrigem.IdOperacao;
            eventoFundoMovimentoCautelas.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoCautelas.Save();

            OperacaoCotista operacaoCotistaDestino = new OperacaoCotista();
            operacaoCotistaDestino.CotaOperacao = cotaDestino;
            operacaoCotistaDestino.DataAgendamento = eventoFundo.DataPosicao;
            operacaoCotistaDestino.DataConversao = eventoFundo.DataPosicao;
            operacaoCotistaDestino.DataLiquidacao = eventoFundo.DataPosicao;
            operacaoCotistaDestino.DataRegistro = eventoFundo.DataPosicao;
            operacaoCotistaDestino.DataOperacao = eventoFundo.DataPosicao;
            operacaoCotistaDestino.Fonte = (byte)FonteOperacaoCotista.Incorporacao;
            operacaoCotistaDestino.IdCarteira = eventoFundo.IdCarteiraDestino;
            operacaoCotistaDestino.IdCotista = posicaoCotista.IdCotista.Value;
            operacaoCotistaDestino.IdFormaLiquidacao = 1; //Forcado
            operacaoCotistaDestino.IdPosicaoResgatada = posicaoCotista.IdPosicao.Value;
            operacaoCotistaDestino.PrejuizoUsado = 0;
            operacaoCotistaDestino.Quantidade = quantidadeConvertida;
            operacaoCotistaDestino.RendimentoResgate = 0;
            operacaoCotistaDestino.TipoOperacao = (byte)TipoOperacaoCotista.IncorporacaoAplicacao;
            operacaoCotistaDestino.TipoResgate = null;
            operacaoCotistaDestino.ValorBruto = valorBrutoOriginal;
            operacaoCotistaDestino.ValorCPMF = 0;
            operacaoCotistaDestino.ValorIOF = 0;
            operacaoCotistaDestino.ValorIR = 0;
            operacaoCotistaDestino.ValorLiquido = valorBrutoOriginal;
            operacaoCotistaDestino.ValorPerformance = 0;
            operacaoCotistaDestino.VariacaoResgate = 0;
            operacaoCotistaDestino.Save();

            if (posicaoCotistaNovo != null)
            {
                posicaoCotistaNovo.IdOperacao = operacaoCotistaDestino.IdOperacao;
                posicaoCotistaNovo.Save();
            }

            eventoFundoMovimentoCautelas = new EventoFundoMovimentoCautelas();
            eventoFundoMovimentoCautelas.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoCautelas.IdOperacao = operacaoCotistaDestino.IdOperacao;
            eventoFundoMovimentoCautelas.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoCautelas.Save();

            HistoricoIncorporacaoPosicao historicoIncorporacaoPosicao = new HistoricoIncorporacaoPosicao();
            historicoIncorporacaoPosicao.DataConversao = posicaoCotista.DataConversao.Value;
            historicoIncorporacaoPosicao.DataIncorporacao = eventoFundo.DataPosicao;
            historicoIncorporacaoPosicao.IdCarteiraDestino = eventoFundo.IdCarteiraDestino;
            historicoIncorporacaoPosicao.IdCarteiraOrigem = eventoFundo.IdCarteiraOrigem;
            historicoIncorporacaoPosicao.IdPosicao = posicaoCotista.IdPosicao.Value;
            historicoIncorporacaoPosicao.Save();

            EventoFundoCautela eventoFundoCautela = new EventoFundoCautela();
            eventoFundoCautela.LoadByPrimaryKey(posicaoCotista.DataAplicacao.Value, 
                                                posicaoCotista.IdCotista.Value, 
                                                eventoFundo.IdEventoFundo.Value, 
                                                posicaoCotista.IdOperacao.Value);
            eventoFundoCautela.Processar = 2;
            eventoFundoCautela.Save();
        }

        private void eventoPrejuizoCotista(EventoFundo eventoFundo, int idCliente)
        {
            PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection();
            prejuizoCotistaCollection.Query.Where(prejuizoCotistaCollection.Query.IdCarteira.Equal(eventoFundo.IdCarteiraOrigem),
                                                  prejuizoCotistaCollection.Query.IdCarteira.Equal(idCliente));
            prejuizoCotistaCollection.Query.Load();

            foreach (PrejuizoCotista prejuizoCotista in prejuizoCotistaCollection)
            {
                decimal prejuizoDestino = (prejuizoCotista.ValorPrejuizo.Value * eventoFundo.Percentual.Value) / 100;
                decimal prejuizoOrigem = (prejuizoCotista.ValorPrejuizo.Value - prejuizoDestino);

                PrejuizoCotista prejuizoCotistaDestino = new PrejuizoCotista();
                if (prejuizoCotistaDestino.LoadByPrimaryKey(prejuizoCotista.IdCotista.Value, eventoFundo.IdCarteiraDestino.Value, prejuizoCotista.Data.Value))
                {
                    prejuizoCotistaDestino.ValorPrejuizo += prejuizoDestino;
                    prejuizoCotistaDestino.Save();
                }
                else
                {
                    prejuizoCotistaDestino = new PrejuizoCotista();
                    prejuizoCotistaDestino.Data = prejuizoCotista.Data.Value;
                    prejuizoCotistaDestino.DataLimiteCompensacao = prejuizoCotista.DataLimiteCompensacao;
                    prejuizoCotistaDestino.IdCarteira = eventoFundo.IdCarteiraDestino;
                    prejuizoCotistaDestino.IdCotista = prejuizoCotista.IdCotista.Value;
                    prejuizoCotistaDestino.ValorPrejuizo = prejuizoDestino;
                    prejuizoCotistaDestino.ValorPrejuizoOriginal = prejuizoDestino;
                    prejuizoCotistaDestino.Save();
                }

                prejuizoCotista.ValorPrejuizo = prejuizoOrigem;
            }

            prejuizoCotistaCollection.Save();

        }

        private void eventoBolsa(EventoFundo eventoFundo)
        {
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("PB");
            EventoFundoBolsaQuery eventoFundoBolsaQuery = new EventoFundoBolsaQuery("EFB");

            posicaoBolsaQuery.Select(posicaoBolsaQuery, eventoFundoBolsaQuery.Quantidade.As("QtdEvento"));
            posicaoBolsaQuery.InnerJoin(eventoFundoBolsaQuery).On(posicaoBolsaQuery.CdAtivoBolsa.Equal(eventoFundoBolsaQuery.CdAtivoBolsa) &
                                                                  posicaoBolsaQuery.IdAgente.Equal(eventoFundoBolsaQuery.IdAgente));
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(eventoFundo.IdCarteiraOrigem));
            posicaoBolsaQuery.Where(eventoFundoBolsaQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo),
                                    eventoFundoBolsaQuery.Processar.In(Convert.ToInt16(StatusProcessamentoCisao.Processar), Convert.ToInt16(StatusProcessamentoCisao.Processado)));

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                PosicaoBolsa posicaoBolsaDestino = this.copiaPosicaoBolsa(posicaoBolsa);

                decimal perc = Convert.ToDecimal(posicaoBolsa.GetColumn("QtdEvento")) / posicaoBolsaDestino.Quantidade.Value;
                posicaoBolsaDestino.IdCliente = eventoFundo.IdCarteiraDestino;
                posicaoBolsaDestino.Quantidade = Convert.ToDecimal(posicaoBolsa.GetColumn("QtdEvento"));
                posicaoBolsaDestino.ValorMercado = posicaoBolsaDestino.Quantidade * posicaoBolsaDestino.PUMercado;
                posicaoBolsaDestino.ValorCustoLiquido = posicaoBolsaDestino.Quantidade * posicaoBolsaDestino.PUCustoLiquido;
                posicaoBolsaDestino.ResultadoRealizar = posicaoBolsaDestino.ResultadoRealizar * perc;

                posicaoBolsaDestino.Save();

                EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Bolsa;
                eventoFundoPosicaoAtivos.IdPosicao = posicaoBolsaDestino.IdPosicao;
                eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                eventoFundoPosicaoAtivos.Save();

                posicaoBolsa.Quantidade = posicaoBolsa.Quantidade - posicaoBolsaDestino.Quantidade;
                posicaoBolsa.ValorMercado = posicaoBolsa.Quantidade * posicaoBolsa.PUMercado;
                posicaoBolsa.ValorCustoLiquido = posicaoBolsa.Quantidade * posicaoBolsa.PUCustoLiquido;
                posicaoBolsa.ResultadoRealizar = posicaoBolsa.ResultadoRealizar - posicaoBolsaDestino.ResultadoRealizar;

                this.geraMovimentoBolsa(posicaoBolsaDestino.Quantidade.Value, posicaoBolsa.IdPosicao.Value, eventoFundo);

                EventoFundoBolsa eventoFundoBolsa = new EventoFundoBolsa();
                eventoFundoBolsa.LoadByPrimaryKey(posicaoBolsa.CdAtivoBolsa, posicaoBolsa.IdAgente.Value, eventoFundo.IdEventoFundo.Value, Convert.ToInt16(TipoMercado.Bolsa));
                eventoFundoBolsa.Processar = 2;
                eventoFundoBolsa.Save();

            }
            posicaoBolsaCollection.Save();

        }

        private void eventoBMF(EventoFundo eventoFundo)
        {
            PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("PB");
            EventoFundoBMFQuery eventoFundoBMFQuery = new EventoFundoBMFQuery("EFB");

            posicaoBMFQuery.Select(posicaoBMFQuery, eventoFundoBMFQuery.Quantidade.As("QtdEvento"));
            posicaoBMFQuery.InnerJoin(eventoFundoBMFQuery).On(posicaoBMFQuery.CdAtivoBMF.Equal(eventoFundoBMFQuery.CdAtivoBMF) &
                                                              posicaoBMFQuery.Serie.Equal(eventoFundoBMFQuery.Serie) &
                                                              posicaoBMFQuery.IdAgente.Equal(eventoFundoBMFQuery.IdAgente));
            posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(eventoFundo.IdCarteiraOrigem));
            posicaoBMFQuery.Where(eventoFundoBMFQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            posicaoBMFQuery.Where(eventoFundoBMFQuery.Processar.In(Convert.ToInt16(StatusProcessamentoCisao.Processar), Convert.ToInt16(StatusProcessamentoCisao.Processado)));

            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            posicaoBMFCollection.Load(posicaoBMFQuery);

            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                PosicaoBMF posicaoBMFDestino = this.copiaPosicaoBMF(posicaoBMF);

                decimal perc = Convert.ToDecimal(posicaoBMF.GetColumn("QtdEvento")) / Convert.ToDecimal(posicaoBMFDestino.Quantidade.Value);
                posicaoBMFDestino.IdCliente = eventoFundo.IdCarteiraDestino;
                posicaoBMFDestino.Quantidade = Convert.ToInt32(posicaoBMF.GetColumn("QtdEvento"));
                posicaoBMFDestino.ValorMercado = posicaoBMFDestino.Quantidade * posicaoBMFDestino.PUMercado;
                posicaoBMFDestino.ValorCustoLiquido = posicaoBMFDestino.Quantidade * posicaoBMFDestino.PUCustoLiquido;
                posicaoBMFDestino.AjusteDiario = posicaoBMFDestino.AjusteDiario * perc;
                posicaoBMFDestino.Save();

                EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.BMF;
                eventoFundoPosicaoAtivos.IdPosicao = posicaoBMFDestino.IdPosicao;
                eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                eventoFundoPosicaoAtivos.Save();

                posicaoBMF.Quantidade = posicaoBMF.Quantidade - posicaoBMFDestino.Quantidade;
                posicaoBMF.ValorMercado = posicaoBMF.Quantidade * posicaoBMF.PUMercado;
                posicaoBMF.ValorCustoLiquido = posicaoBMF.Quantidade * posicaoBMF.PUCustoLiquido;
                posicaoBMF.AjusteDiario = posicaoBMF.AjusteDiario - posicaoBMFDestino.AjusteDiario;

                this.geraMovimentoBMF(posicaoBMFDestino.Quantidade.Value, posicaoBMF.IdPosicao.Value, eventoFundo);

                EventoFundoBMF eventoFundoBMF = new EventoFundoBMF();
                eventoFundoBMF.LoadByPrimaryKey(posicaoBMF.CdAtivoBMF, posicaoBMF.IdAgente.Value, eventoFundoBMF.IdEventoFundo.Value, posicaoBMF.Serie, Convert.ToInt16(TipoMercado.BMF));
                eventoFundoBMF.Processar = 2;
                eventoFundoBMF.Save();

            }
            posicaoBMFCollection.Save();

        }

        private void eventoRendaFixa(EventoFundo eventoFundo)
        {
            decimal qtdPosicaoOriginal = 0;

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("PRF");
            EventoFundoRendaFixaQuery eventoFundoRendaFixaQuery = new EventoFundoRendaFixaQuery("EFB");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery, eventoFundoRendaFixaQuery.Quantidade.As("QtdEvento"));
            posicaoRendaFixaQuery.InnerJoin(eventoFundoRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo.Equal(eventoFundoRendaFixaQuery.IdTitulo) &
                                                                          posicaoRendaFixaQuery.TipoOperacao.Equal(eventoFundoRendaFixaQuery.TipoOperacao) &
                                                                          posicaoRendaFixaQuery.DataOperacao.Equal(eventoFundoRendaFixaQuery.DataOperacao) &
                                                                          posicaoRendaFixaQuery.DataLiquidacao.Equal(eventoFundoRendaFixaQuery.DataLiquidacao));
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(eventoFundo.IdCarteiraOrigem));
            posicaoRendaFixaQuery.Where(eventoFundoRendaFixaQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            posicaoRendaFixaQuery.Where(eventoFundoRendaFixaQuery.Processar.In(Convert.ToInt16(StatusProcessamentoCisao.Processar), Convert.ToInt16(StatusProcessamentoCisao.Processado)));

            posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                decimal qtdTotal = 0;

                PosicaoRendaFixa posicaoRendaFixaDestino = this.copiaPosicaoRendaFixa(posicaoRendaFixa);

                PosicaoRendaFixaCollection posicaoRendaFixaTotal = new PosicaoRendaFixaCollection();
                posicaoRendaFixaTotal.Query.Select(posicaoRendaFixaTotal.Query.Quantidade.Sum());
                posicaoRendaFixaTotal.Query.Where(posicaoRendaFixaTotal.Query.IdTitulo.Equal(posicaoRendaFixa.IdTitulo.Value));
                posicaoRendaFixaTotal.Query.Where(posicaoRendaFixaTotal.Query.TipoOperacao.Equal(posicaoRendaFixa.TipoOperacao.Value));
                posicaoRendaFixaTotal.Query.Where(posicaoRendaFixaTotal.Query.DataOperacao.Equal(posicaoRendaFixa.DataOperacao.Value));
                posicaoRendaFixaTotal.Query.Where(posicaoRendaFixaTotal.Query.DataLiquidacao.Equal(posicaoRendaFixa.DataLiquidacao.Value));
                posicaoRendaFixaTotal.Query.Where(posicaoRendaFixaTotal.Query.IdCliente.Equal(posicaoRendaFixa.IdCliente.Value));

                if (posicaoRendaFixaTotal.Query.Load())
                {
                    qtdTotal = posicaoRendaFixaTotal[0].Quantidade.GetValueOrDefault(Convert.ToDecimal(posicaoRendaFixa.GetColumn("QtdEvento")));
                }
                else
                {
                    qtdTotal = Convert.ToDecimal(posicaoRendaFixa.GetColumn("QtdEvento"));
                }

                decimal perc = Convert.ToDecimal(posicaoRendaFixa.GetColumn("QtdEvento")) / qtdTotal;
                posicaoRendaFixaDestino.IdCliente = eventoFundo.IdCarteiraDestino;
                posicaoRendaFixaDestino.Quantidade = posicaoRendaFixaDestino.Quantidade * perc;
                posicaoRendaFixaDestino.ValorMercado = posicaoRendaFixaDestino.Quantidade * posicaoRendaFixaDestino.PUMercado;
                posicaoRendaFixaDestino.TipoOperacao = (byte)TipoOperacaoTitulo.Deposito;
                posicaoRendaFixaDestino.ValorCurva = posicaoRendaFixaDestino.ValorCurva * perc;
                posicaoRendaFixaDestino.Save();

                //Altera tipo de operação após criação da posição para que trigger de inclusão utilizar TipoOperacao = Deposito
                posicaoRendaFixaDestino.TipoOperacao = (byte)TipoOperacaoTitulo.CompraFinal;
                posicaoRendaFixaDestino.Save();

                EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.RendaFixa;
                eventoFundoPosicaoAtivos.IdPosicao = posicaoRendaFixaDestino.IdPosicao;
                eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;

                eventoFundoPosicaoAtivos.Save();

                posicaoRendaFixa.Quantidade = posicaoRendaFixa.Quantidade - posicaoRendaFixaDestino.Quantidade;
                posicaoRendaFixa.ValorMercado = posicaoRendaFixa.Quantidade * posicaoRendaFixa.PUMercado;
                posicaoRendaFixa.ValorCurva = posicaoRendaFixa.ValorCurva - posicaoRendaFixaDestino.ValorCurva;

                this.geraMovimentoRendaFixa(posicaoRendaFixaDestino, posicaoRendaFixa, eventoFundo);

                EventoFundoRendaFixa eventoFundoRendaFixa = new EventoFundoRendaFixa();
                eventoFundoRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.DataLiquidacao.Value, 
                                                      posicaoRendaFixa.DataOperacao.Value, 
                                                      eventoFundo.IdEventoFundo.Value, 
                                                      posicaoRendaFixa.IdTitulo.Value, 
                                                      Convert.ToInt16(TipoMercado.RendaFixa), 
                                                      posicaoRendaFixa.TipoOperacao.Value);
                eventoFundoRendaFixa.Processar = 2;
                eventoFundoRendaFixa.Save();

            }
            posicaoRendaFixaCollection.Save();

        }

        private void eventoSwap(EventoFundo eventoFundo)
        {
            PosicaoSwapQuery posicaoSwapQuery = new PosicaoSwapQuery("PS");
            EventoFundoSwapQuery eventoFundoSwapQuery = new EventoFundoSwapQuery("EFB");

            posicaoSwapQuery.Select(posicaoSwapQuery);
            posicaoSwapQuery.InnerJoin(eventoFundoSwapQuery).On(posicaoSwapQuery.NumeroContrato.Equal(eventoFundoSwapQuery.NumeroContrato) &
                                                             posicaoSwapQuery.DataEmissao.Equal(eventoFundoSwapQuery.DataEmissao) &
                                                             posicaoSwapQuery.DataVencimento.Equal(eventoFundoSwapQuery.DataVencimento) &
                                                             posicaoSwapQuery.TipoPonta.Equal(eventoFundoSwapQuery.TipoPonta) &
                                                             posicaoSwapQuery.TipoPontaContraParte.Equal(eventoFundoSwapQuery.TipoPontaContraParte));
            posicaoSwapQuery.Where(posicaoSwapQuery.IdCliente.Equal(eventoFundo.IdCarteiraOrigem));
            posicaoSwapQuery.Where(eventoFundoSwapQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            posicaoSwapQuery.Where(eventoFundoSwapQuery.Processar.In(Convert.ToInt16(StatusProcessamentoCisao.Processar), Convert.ToInt16(StatusProcessamentoCisao.Processado)));
            posicaoSwapQuery.Where(eventoFundoSwapQuery.Quantidade > 0);


            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();
            posicaoSwapCollection.Load(posicaoSwapQuery);

            foreach (PosicaoSwap posicaoSwap in posicaoSwapCollection)
            {
                PosicaoSwap posicaoSwapDestino = this.copiaPosicaoSwap(posicaoSwap);

                posicaoSwapDestino.IdCliente = eventoFundo.IdCarteiraDestino;
                posicaoSwapDestino.Save();

                EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Swap;
                eventoFundoPosicaoAtivos.IdPosicao = posicaoSwapDestino.IdPosicao;
                eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                eventoFundoPosicaoAtivos.Save();

                this.geraMovimentoSwap(posicaoSwapDestino, posicaoSwap, eventoFundo);

                //Se (Cisao Parcial) ou (Incorporacao Parcial) e ainda caso o SWAP não permaneça no fundo Origem, deve-se manter o SWAP original no fundo origem e criar o SWAP inverso para anular o resultado financeiro desse SWAP nesse fundo.
                if ((eventoFundo.TipoEvento == 1 || eventoFundo.TipoEvento == 3) && (eventoFundo.Percentual.HasValue && eventoFundo.Percentual < 100))
                {

                    PosicaoSwap posicaoSwapContraParteOperacao = this.copiaPosicaoSwap(posicaoSwap);

                    posicaoSwapContraParteOperacao.IdCliente = eventoFundo.IdCarteiraOrigem;
                    posicaoSwapContraParteOperacao.TipoPontaContraParte = posicaoSwap.TipoPonta;
                    posicaoSwapContraParteOperacao.IdIndiceContraParte = posicaoSwap.IdIndice;
                    posicaoSwapContraParteOperacao.TaxaJurosContraParte = posicaoSwap.TaxaJuros;
                    posicaoSwapContraParteOperacao.PercentualContraParte = posicaoSwap.Percentual;
                    posicaoSwapContraParteOperacao.TipoApropriacaoContraParte = posicaoSwap.TipoApropriacao;
                    posicaoSwapContraParteOperacao.ContagemDiasContraParte = posicaoSwap.ContagemDias;
                    posicaoSwapContraParteOperacao.BaseAnoContraParte = posicaoSwap.BaseAno;
                    posicaoSwapContraParteOperacao.ValorContraParte = posicaoSwap.ValorParte;

                    posicaoSwapContraParteOperacao.TipoPonta = posicaoSwap.TipoPontaContraParte;
                    posicaoSwapContraParteOperacao.IdIndice = posicaoSwap.IdIndiceContraParte;
                    posicaoSwapContraParteOperacao.TaxaJuros = posicaoSwap.TaxaJurosContraParte;
                    posicaoSwapContraParteOperacao.Percentual = posicaoSwap.PercentualContraParte;
                    posicaoSwapContraParteOperacao.TipoApropriacao = posicaoSwap.TipoApropriacaoContraParte;
                    posicaoSwapContraParteOperacao.ContagemDias = posicaoSwap.ContagemDiasContraParte;
                    posicaoSwapContraParteOperacao.BaseAno = posicaoSwap.BaseAnoContraParte;
                    posicaoSwapContraParteOperacao.ValorParte = posicaoSwap.ValorContraParte;

                    posicaoSwapContraParteOperacao.Saldo = posicaoSwap.Saldo * (-1);

                    posicaoSwapContraParteOperacao.Save();

                    eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                    eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                    eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Swap;
                    eventoFundoPosicaoAtivos.IdPosicao = posicaoSwapContraParteOperacao.IdPosicao;
                    eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                    eventoFundoPosicaoAtivos.Save();

                    PosicaoSwap posicao = new PosicaoSwap();
                    posicao.LoadByPrimaryKey(posicaoSwapContraParteOperacao.IdPosicao.Value);

                    EventoFundoMovimentoAtivos eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
                    eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                    eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.Swap;
                    eventoFundoMovimentoAtivos.IdOperacao = posicao.IdOperacao;
                    eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                    eventoFundoMovimentoAtivos.Save();
                }
                else
                {
                    posicaoSwap.MarkAsDeleted();
                }

                EventoFundoSwap eventoFundoSwap = new EventoFundoSwap();
                eventoFundoSwap.LoadByPrimaryKey(posicaoSwap.DataEmissao.Value, 
                                                 posicaoSwap.DataVencimento.Value, 
                                                 eventoFundo.IdEventoFundo.Value, 
                                                 posicaoSwap.NumeroContrato, 
                                                 Convert.ToInt16(TipoMercado.Swap), 
                                                 posicaoSwap.TipoPonta.Value, 
                                                 posicaoSwap.TipoPontaContraParte.Value);
                eventoFundoSwap.Processar = 2;
                eventoFundoSwap.Save();

            }
            posicaoSwapCollection.Save();
        }

        private void eventoFundo(EventoFundo eventoFundo)
        {
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("PRF");
            EventoFundoFundoQuery eventoFundoFundoQuery = new EventoFundoFundoQuery("EFB");
            
            posicaoFundoQuery.Select(posicaoFundoQuery, eventoFundoFundoQuery.Quantidade.As("QtdEvento"));
            posicaoFundoQuery.InnerJoin(eventoFundoFundoQuery).On(posicaoFundoQuery.IdCarteira.Equal(eventoFundoFundoQuery.IdCarteira));
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(eventoFundo.IdCarteiraOrigem));
            posicaoFundoQuery.Where(eventoFundoFundoQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            posicaoFundoQuery.Where(eventoFundoFundoQuery.Processar.In(Convert.ToInt16(StatusProcessamentoCisao.Processar), Convert.ToInt16(StatusProcessamentoCisao.Processado)));

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.Load(posicaoFundoQuery);

            //Seleciona quantidade total da posição por fundo
            posicaoFundoQuery = new PosicaoFundoQuery("PRF");
            eventoFundoFundoQuery = new EventoFundoFundoQuery("EFB");
            posicaoFundoQuery.Select(posicaoFundoQuery.IdCarteira, posicaoFundoQuery.Quantidade.Sum());
            posicaoFundoQuery.InnerJoin(eventoFundoFundoQuery).On(posicaoFundoQuery.IdCarteira.Equal(eventoFundoFundoQuery.IdCarteira));
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(eventoFundo.IdCarteiraOrigem));
            posicaoFundoQuery.Where(eventoFundoFundoQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            posicaoFundoQuery.Where(eventoFundoFundoQuery.Processar.In(Convert.ToInt16(StatusProcessamentoCisao.Processar), Convert.ToInt16(StatusProcessamentoCisao.Processado)));
            posicaoFundoQuery.GroupBy(posicaoFundoQuery.IdCarteira);

            PosicaoFundoCollection posicaoFundoQuantidadesPorFundo = new PosicaoFundoCollection();
            posicaoFundoQuantidadesPorFundo.Load(posicaoFundoQuery);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                PosicaoFundo posicaoFundoDestino = this.copiaPosicaoFundo(posicaoFundo);

                //Encontra quantidade total da posicao
                decimal quantidadePosicao = encontraPosicaoCliente(posicaoFundo.IdCarteira.Value, posicaoFundoQuantidadesPorFundo);

                decimal perc = Convert.ToDecimal(posicaoFundo.GetColumn("QtdEvento")) / quantidadePosicao;
                posicaoFundoDestino.IdCliente = eventoFundo.IdCarteiraDestino;
                posicaoFundoDestino.Quantidade = posicaoFundoDestino.Quantidade * perc;
                posicaoFundoDestino.ValorBruto = posicaoFundoDestino.Quantidade * posicaoFundoDestino.CotaDia;
                posicaoFundoDestino.ValorLiquido = posicaoFundoDestino.Quantidade * posicaoFundoDestino.CotaDia;
                posicaoFundoDestino.QuantidadeBloqueada = posicaoFundoDestino.QuantidadeBloqueada * perc;
                posicaoFundoDestino.ValorIR = posicaoFundoDestino.ValorIR * perc;
                posicaoFundoDestino.ValorIOF = posicaoFundoDestino.ValorIOF * perc;
                posicaoFundoDestino.ValorPerformance = posicaoFundoDestino.ValorPerformance * perc;
                posicaoFundoDestino.ValorIOFVirtual = posicaoFundoDestino.ValorIOFVirtual * perc;
                posicaoFundoDestino.QuantidadeAntesCortes = posicaoFundoDestino.QuantidadeAntesCortes * perc;
                posicaoFundoDestino.ValorRendimento = posicaoFundoDestino.ValorRendimento * perc;
                posicaoFundoDestino.DataUltimaCobrancaIR = eventoFundo.DataPosicao.Value;
                posicaoFundoDestino.Save();

                EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Fundos;
                eventoFundoPosicaoAtivos.IdPosicao = posicaoFundoDestino.IdPosicao;
                eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                eventoFundoPosicaoAtivos.Save();

                posicaoFundo.Quantidade = posicaoFundo.Quantidade - posicaoFundoDestino.Quantidade;
                posicaoFundo.ValorBruto = posicaoFundo.Quantidade * posicaoFundo.CotaDia;
                posicaoFundo.ValorLiquido = posicaoFundo.Quantidade * posicaoFundo.CotaDia;
                posicaoFundo.QuantidadeBloqueada = posicaoFundo.QuantidadeBloqueada - posicaoFundoDestino.QuantidadeBloqueada;
                posicaoFundo.ValorIR = posicaoFundo.ValorIR - posicaoFundoDestino.ValorIR;
                posicaoFundo.ValorIOF = posicaoFundo.ValorIOF - posicaoFundoDestino.ValorIOF;
                posicaoFundo.ValorPerformance = posicaoFundo.ValorPerformance - posicaoFundoDestino.ValorPerformance;
                posicaoFundo.ValorIOFVirtual = posicaoFundo.ValorIOFVirtual - posicaoFundoDestino.ValorIOFVirtual;
                posicaoFundo.QuantidadeAntesCortes = posicaoFundo.QuantidadeAntesCortes - posicaoFundoDestino.QuantidadeAntesCortes;
                posicaoFundo.ValorRendimento = posicaoFundo.ValorRendimento - posicaoFundoDestino.ValorRendimento;

                this.geraMovimentoFundos(posicaoFundoDestino, posicaoFundo, eventoFundo);

                EventoFundoFundo eventoFundoFundo = new EventoFundoFundo();
                eventoFundoFundo.LoadByPrimaryKey(posicaoFundo.IdCarteira.Value, 
                                                  eventoFundo.IdEventoFundo.Value, 
                                                  Convert.ToInt16(TipoMercado.Fundos));
                eventoFundoFundo.Processar = 2;
                eventoFundoFundo.Save();

            }
            posicaoFundoCollection.Save();

        }

        private decimal encontraPosicaoCliente(int idCarteira, PosicaoFundoCollection posicaoFundoQuantidadesPorFundo)
        {
            decimal quantidadeTotal = 0;

            foreach (PosicaoFundo posicaoFundo in posicaoFundoQuantidadesPorFundo)
            {
                if (posicaoFundo.IdCarteira.Equals(idCarteira))
                {
                    quantidadeTotal = posicaoFundo.Quantidade.Value;
                    break;
                }
            }

            return quantidadeTotal;
        }


        private void eventoLiquidacao(EventoFundo eventoFundo)
        {
            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(eventoFundo.IdCarteiraDestino.Value);

            LiquidacaoQuery liquidacaoQuery = new LiquidacaoQuery("L");
            EventoFundoLiquidacaoQuery eventoFundoLiquidacaoQuery = new EventoFundoLiquidacaoQuery("EFL");

            liquidacaoQuery.Select(liquidacaoQuery, eventoFundoLiquidacaoQuery.Quantidade.As("QtdEvento"), eventoFundoLiquidacaoQuery.Contador);
            liquidacaoQuery.InnerJoin(eventoFundoLiquidacaoQuery).On(liquidacaoQuery.DataLancamento.Equal(eventoFundoLiquidacaoQuery.DataLancamento) &
                                                                  liquidacaoQuery.DataVencimento.Equal(eventoFundoLiquidacaoQuery.DataVencimento) &
                                                                  liquidacaoQuery.Descricao.Equal(eventoFundoLiquidacaoQuery.Descricao));
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente.Equal(eventoFundo.IdCarteiraOrigem));
            liquidacaoQuery.Where(eventoFundoLiquidacaoQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            liquidacaoQuery.Where(eventoFundoLiquidacaoQuery.Processar.In(Convert.ToInt16(StatusProcessamentoCisao.Processar), Convert.ToInt16(StatusProcessamentoCisao.Processado)));

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Load(liquidacaoQuery);

            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                Liquidacao liquidacaoDestino = this.copiaLiquidacao(liquidacao);

                liquidacaoDestino.IdCliente = eventoFundo.IdCarteiraDestino;
                liquidacaoDestino.Valor = (Convert.ToDecimal(liquidacao.GetColumn("QtdEvento")) / Convert.ToInt32(liquidacao.GetColumn("Contador")));
                liquidacaoDestino.IdConta = idContaDefault;
                liquidacaoDestino.Save();

                EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
                eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Liquidacao;
                eventoFundoPosicaoAtivos.IdPosicao = liquidacaoDestino.IdLiquidacao;
                eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                eventoFundoPosicaoAtivos.Save();

                LiquidacaoHistorico.GeraLiquidacaoHistorico(liquidacaoDestino, eventoFundo.DataPosicao.Value);

                liquidacao.Valor = liquidacao.Valor - liquidacaoDestino.Valor;

                EventoFundoLiquidacao eventoFundoLiquidacao = new EventoFundoLiquidacao();
                eventoFundoLiquidacao.LoadByPrimaryKey(liquidacao.DataLancamento.Value, 
                                                       liquidacao.DataVencimento.Value, 
                                                       liquidacao.Descricao, 
                                                       eventoFundo.IdEventoFundo.Value, 
                                                       Convert.ToInt16(TipoMercado.Liquidacao));
                eventoFundoLiquidacao.Processar = 2;
                eventoFundoLiquidacao.Save();

            }
            liquidacaoCollection.Save();
        }

        private void eventoCaixa(EventoFundo eventoFundo)
        {
            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDestino = contaCorrente.RetornaContaDefault(eventoFundo.IdCarteiraDestino.Value);
            int idContaOrigem = contaCorrente.RetornaContaDefault(eventoFundo.IdCarteiraOrigem.Value);

            SaldoCaixaQuery saldoCaixaQuery = new SaldoCaixaQuery("SC");
            EventoFundoCaixaQuery eventoFundoCaixaQuery = new EventoFundoCaixaQuery("EFC");

            saldoCaixaQuery.Select(saldoCaixaQuery, eventoFundoCaixaQuery.Valor.As("QtdEvento"));
            saldoCaixaQuery.InnerJoin(eventoFundoCaixaQuery).On(saldoCaixaQuery.Data.Equal(eventoFundoCaixaQuery.Data) &
                                                                  saldoCaixaQuery.IdConta.Equal(eventoFundoCaixaQuery.IdConta) &
                                                                  saldoCaixaQuery.SaldoAbertura.Equal(eventoFundoCaixaQuery.SaldoAbertura));
            saldoCaixaQuery.Where(saldoCaixaQuery.IdCliente.Equal(eventoFundo.IdCarteiraOrigem));
            saldoCaixaQuery.Where(eventoFundoCaixaQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            saldoCaixaQuery.Where(eventoFundoCaixaQuery.Processar.In(Convert.ToInt16(StatusProcessamentoCisao.Processar), Convert.ToInt16(StatusProcessamentoCisao.Processado)));

            SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
            saldoCaixaCollection.Load(saldoCaixaQuery);

            foreach (SaldoCaixa saldoCaixa in saldoCaixaCollection)
            {
                //Gera linha de lancamento de caixa em liquidacao
                this.geraMovimentoLiquidacao(eventoFundo, idContaDestino, idContaOrigem, Convert.ToDecimal(saldoCaixa.GetColumn("QtdEvento")));

                SaldoCaixa saldoCaixaExiste = new SaldoCaixa();
                if (saldoCaixaExiste.LoadByPrimaryKey(eventoFundo.IdCarteiraDestino.Value, eventoFundo.DataPosicao.Value, idContaDestino))
                {
                    saldoCaixaExiste.SaldoFechamento += Convert.ToDecimal(saldoCaixa.GetColumn("QtdEvento"));
                    saldoCaixaExiste.Save();
                }
                else
                {
                    SaldoCaixa saldoCaixaNovo = new SaldoCaixa();
                    saldoCaixaNovo.IdCliente = eventoFundo.IdCarteiraDestino;
                    saldoCaixaNovo.Data = eventoFundo.DataPosicao;
                    saldoCaixaNovo.IdConta = idContaDestino;
                    saldoCaixaNovo.SaldoAbertura = 0;
                    saldoCaixaNovo.SaldoFechamento = Convert.ToDecimal(saldoCaixa.GetColumn("QtdEvento"));

                    saldoCaixaNovo.Save();
                }

                saldoCaixa.SaldoFechamento -= Convert.ToDecimal(saldoCaixa.GetColumn("QtdEvento"));

                EventoFundoCaixa eventoFundoCaixa = new EventoFundoCaixa();
                eventoFundoCaixa.LoadByPrimaryKey(saldoCaixa.Data.Value, 
                                                  saldoCaixa.IdConta.Value, 
                                                  eventoFundo.IdEventoFundo.Value, 
                                                  saldoCaixa.SaldoAbertura.Value, 
                                                  Convert.ToInt16(TipoMercado.Caixa));
                eventoFundoCaixa.Processar = 2;
                eventoFundoCaixa.Save();

            }
            saldoCaixaCollection.Save();
        }

        private void geraMovimentoLiquidacao(EventoFundo eventoFundo, int idContaDestino, int idContaOrigem, decimal valor)
        {
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.IdCliente = eventoFundo.IdCarteiraDestino;
            liquidacao.DataLancamento = eventoFundo.DataPosicao;
            liquidacao.DataVencimento = eventoFundo.DataPosicao;
            liquidacao.Descricao = "Transferência de saldo do fundo " + eventoFundo.IdCarteiraOrigem.ToString();
            liquidacao.Valor = valor;
            liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
            liquidacao.Origem = OrigemLancamentoLiquidacao.Fundo.TransferenciaSaldo;
            liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
            liquidacao.IdConta = idContaDestino;
            liquidacao.Save();

            EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
            eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Liquidacao;
            eventoFundoPosicaoAtivos.IdPosicao = liquidacao.IdLiquidacao;
            eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoPosicaoAtivos.Save();

            liquidacao = new Liquidacao();
            liquidacao.IdCliente = eventoFundo.IdCarteiraOrigem;
            liquidacao.DataLancamento = eventoFundo.DataPosicao;
            liquidacao.DataVencimento = eventoFundo.DataPosicao;
            liquidacao.Descricao = "Transferência de saldo para fundo " + eventoFundo.IdCarteiraDestino.ToString();
            liquidacao.Valor = valor * (-1);
            liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
            liquidacao.Origem = OrigemLancamentoLiquidacao.Fundo.TransferenciaSaldo;
            liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
            liquidacao.IdConta = idContaOrigem;
            liquidacao.Save();

            eventoFundoPosicaoAtivos = new EventoFundoPosicaoAtivos();
            eventoFundoPosicaoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoPosicaoAtivos.TipoMercado = (int)TipoMercado.Liquidacao;
            eventoFundoPosicaoAtivos.IdPosicao = liquidacao.IdLiquidacao;
            eventoFundoPosicaoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoPosicaoAtivos.Save();

        }

        private void geraMovimentoBolsa(decimal quantidade,
                                        int idPosicao,
                                        EventoFundo eventoFundo)
        {
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            posicaoBolsa.LoadByPrimaryKey(idPosicao);

            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(posicaoBolsa.CdAtivoBolsa));
            int idMoeda = ativoBolsa.IdMoeda.Value;

            //Operacao Retirada
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.IdCliente = eventoFundo.IdCarteiraOrigem;
            operacaoBolsa.CdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
            operacaoBolsa.IdAgenteCorretora = posicaoBolsa.IdAgente;
            operacaoBolsa.IdAgenteLiquidacao = posicaoBolsa.IdAgente;
            operacaoBolsa.IdMoeda = idMoeda;
            operacaoBolsa.TipoMercado = posicaoBolsa.TipoMercado;
            operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Retirada;
            operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.Primaria;
            operacaoBolsa.Pu = posicaoBolsa.PUCusto;
            operacaoBolsa.PULiquido = posicaoBolsa.PUCustoLiquido;
            operacaoBolsa.Valor = (posicaoBolsa.PUCustoLiquido * quantidade);
            operacaoBolsa.Quantidade = quantidade;
            operacaoBolsa.Data = eventoFundo.DataPosicao;
            operacaoBolsa.DataLiquidacao = eventoFundo.DataPosicao;
            operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
            operacaoBolsa.PercentualDesconto = Convert.ToDecimal(0.00);
            operacaoBolsa.IdLocal = Convert.ToInt16(LocalFeriadoFixo.Bovespa);
            operacaoBolsa.CalculaDespesas = "N";
            operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
            operacaoBolsa.IdLocalNegociacao = LocalNegociacaoFixo.BOVESPA;
            operacaoBolsa.IdClearing = (int)ClearingFixo.CBLC;
            operacaoBolsa.DataOperacao = eventoFundo.DataPosicao;

            operacaoBolsa.Save();

            EventoFundoMovimentoAtivos eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
            eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.Bolsa;
            eventoFundoMovimentoAtivos.IdOperacao = operacaoBolsa.IdOperacao;
            eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoAtivos.Save();

            //Operacao Deposito
            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.IdCliente = eventoFundo.IdCarteiraDestino;
            operacaoBolsa.CdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
            operacaoBolsa.IdAgenteCorretora = posicaoBolsa.IdAgente;
            operacaoBolsa.IdAgenteLiquidacao = posicaoBolsa.IdAgente;
            operacaoBolsa.IdMoeda = idMoeda;
            operacaoBolsa.TipoMercado = posicaoBolsa.TipoMercado;
            operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito;
            operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.Primaria;
            operacaoBolsa.Pu = posicaoBolsa.PUCusto;
            operacaoBolsa.PULiquido = posicaoBolsa.PUCustoLiquido;
            operacaoBolsa.Valor = (posicaoBolsa.PUCustoLiquido * quantidade);
            operacaoBolsa.Quantidade = quantidade;
            operacaoBolsa.Data = eventoFundo.DataPosicao;
            operacaoBolsa.DataLiquidacao = eventoFundo.DataPosicao;
            operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
            operacaoBolsa.PercentualDesconto = Convert.ToDecimal(0.00);
            operacaoBolsa.IdLocal = Convert.ToInt16(LocalFeriadoFixo.Bovespa);
            operacaoBolsa.CalculaDespesas = "N";
            operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
            operacaoBolsa.IdLocalNegociacao = LocalNegociacaoFixo.BOVESPA;
            operacaoBolsa.IdClearing = (int)ClearingFixo.CBLC;
            operacaoBolsa.DataOperacao = eventoFundo.DataPosicao;

            operacaoBolsa.Save();

            eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
            eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.Bolsa;
            eventoFundoMovimentoAtivos.IdOperacao = operacaoBolsa.IdOperacao;
            eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoAtivos.Save();

        }

        private void geraMovimentoBMF(decimal quantidade,
                                        int idPosicao,
                                        EventoFundo eventoFundo)
        {
            PosicaoBMF posicaoBMF = new PosicaoBMF();
            posicaoBMF.LoadByPrimaryKey(idPosicao);

            AtivoBMF ativoBMF = new AtivoBMF();
            ativoBMF.LoadByPrimaryKey(posicaoBMF.CdAtivoBMF, posicaoBMF.Serie);

            //Operacao Retirada
            OperacaoBMF operacaoBMF = new OperacaoBMF();
            operacaoBMF.IdCliente = eventoFundo.IdCarteiraOrigem;
            operacaoBMF.IdAgenteCorretora = posicaoBMF.IdAgente;
            operacaoBMF.IdAgenteLiquidacao = posicaoBMF.IdAgente;
            operacaoBMF.CdAtivoBMF = posicaoBMF.CdAtivoBMF;
            operacaoBMF.Serie = posicaoBMF.Serie;
            operacaoBMF.TipoMercado = posicaoBMF.TipoMercado;
            operacaoBMF.TipoOperacao = TipoOperacaoBMF.Venda; //TODO Verificar operacoes de Retirada
            operacaoBMF.Pu = posicaoBMF.PUCusto;
            operacaoBMF.PULiquido = posicaoBMF.PUCustoLiquido;
            operacaoBMF.Valor = (posicaoBMF.PUCusto * quantidade);
            operacaoBMF.ValorLiquido = (posicaoBMF.PUCustoLiquido * quantidade);
            operacaoBMF.Quantidade = Convert.ToInt32(quantidade);
            operacaoBMF.Data = eventoFundo.DataPosicao;
            operacaoBMF.Origem = (byte)OrigemOperacaoBMF.Primaria;
            operacaoBMF.Fonte = (byte)FonteOperacaoBMF.Interno;
            operacaoBMF.CalculaDespesas = "N";
            operacaoBMF.IdMoeda = ativoBMF.IdMoeda.Value;
            operacaoBMF.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
            operacaoBMF.IdLocalNegociacao = LocalNegociacaoFixo.BOVESPA;
            operacaoBMF.IdClearing = (int)ClearingFixo.CBLC;

            operacaoBMF.Save();

            EventoFundoMovimentoAtivos eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
            eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.BMF;
            eventoFundoMovimentoAtivos.IdOperacao = operacaoBMF.IdOperacao;
            eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoAtivos.Save();

            //Operacao Deposito
            operacaoBMF = new OperacaoBMF();
            operacaoBMF.IdCliente = eventoFundo.IdCarteiraDestino;
            operacaoBMF.IdAgenteCorretora = posicaoBMF.IdAgente;
            operacaoBMF.IdAgenteLiquidacao = posicaoBMF.IdAgente;
            operacaoBMF.CdAtivoBMF = posicaoBMF.CdAtivoBMF;
            operacaoBMF.Serie = posicaoBMF.Serie;
            operacaoBMF.TipoMercado = posicaoBMF.TipoMercado;
            operacaoBMF.TipoOperacao = TipoOperacaoBMF.Compra; //TODO Verificar operacoes de Retirada
            operacaoBMF.Pu = posicaoBMF.PUCusto;
            operacaoBMF.PULiquido = posicaoBMF.PUCustoLiquido;
            operacaoBMF.Valor = (posicaoBMF.PUCustoLiquido * quantidade);
            operacaoBMF.ValorLiquido = (posicaoBMF.PUCustoLiquido * quantidade);
            operacaoBMF.Quantidade = Convert.ToInt32(quantidade);
            operacaoBMF.Data = eventoFundo.DataPosicao;
            operacaoBMF.Origem = (byte)OrigemOperacaoBMF.Primaria;
            operacaoBMF.Fonte = (byte)FonteOperacaoBMF.Interno;
            operacaoBMF.CalculaDespesas = "N";
            operacaoBMF.IdMoeda = ativoBMF.IdMoeda.Value;
            operacaoBMF.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
            operacaoBMF.IdLocalNegociacao = LocalNegociacaoFixo.BOVESPA;
            operacaoBMF.IdClearing = (int)ClearingFixo.CBLC;

            operacaoBMF.Save();

            eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
            eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.BMF;
            eventoFundoMovimentoAtivos.IdOperacao = operacaoBMF.IdOperacao;
            eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoAtivos.Save();

        }

        private void geraMovimentoFundos(PosicaoFundo posicaoDestino, PosicaoFundo posicaoOrigem, EventoFundo eventoFundo)
        {
            //Retirada
            OperacaoFundo operacaoFundo = new OperacaoFundo();
            operacaoFundo.IdCliente = eventoFundo.IdCarteiraOrigem;
            operacaoFundo.IdCarteira = posicaoOrigem.IdCarteira;
            operacaoFundo.DataOperacao = eventoFundo.DataPosicao;
            operacaoFundo.DataConversao = eventoFundo.DataPosicao;
            operacaoFundo.DataLiquidacao = eventoFundo.DataPosicao;
            operacaoFundo.DataRegistro = eventoFundo.DataPosicao;
            operacaoFundo.TipoOperacao = (byte)TipoOperacaoFundo.ResgateCotasEspecial;
            operacaoFundo.DataAgendamento = eventoFundo.DataPosicao;
            operacaoFundo.TipoResgate = (byte)TipoResgateFundo.Especifico;
            operacaoFundo.Quantidade = posicaoOrigem.Quantidade;
            operacaoFundo.IdFormaLiquidacao = 1;
            operacaoFundo.IdPosicaoResgatada = posicaoOrigem.IdPosicao;
            operacaoFundo.IdOperacaoResgatada = posicaoOrigem.IdOperacao;
            operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Interno;
            operacaoFundo.ValoresColados = "N";
            operacaoFundo.IdLocalNegociacao = (int)LocalNegociacaoFixo.Brasil;
            operacaoFundo.CotaOperacao = posicaoOrigem.CotaDia;
            operacaoFundo.ValorBruto = posicaoOrigem.ValorBruto;
            operacaoFundo.ValorLiquido = posicaoOrigem.ValorLiquido;
            operacaoFundo.ValorIR = posicaoOrigem.ValorIR;
            operacaoFundo.ValorIOF = posicaoOrigem.ValorIOF;
            operacaoFundo.ValorPerformance = posicaoOrigem.ValorPerformance;
            operacaoFundo.RendimentoResgate = posicaoOrigem.ValorRendimento;

            operacaoFundo.Save();

            EventoFundoMovimentoAtivos eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
            eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.Fundos;
            eventoFundoMovimentoAtivos.IdOperacao = operacaoFundo.IdOperacao;
            eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoAtivos.Save();

            //Deposito
            operacaoFundo = new OperacaoFundo();
            operacaoFundo.IdCliente = eventoFundo.IdCarteiraDestino;
            operacaoFundo.IdCarteira = posicaoDestino.IdCarteira;
            operacaoFundo.DataOperacao = eventoFundo.DataPosicao;
            operacaoFundo.DataConversao = eventoFundo.DataPosicao;
            operacaoFundo.DataLiquidacao = eventoFundo.DataPosicao;
            operacaoFundo.DataRegistro = eventoFundo.DataPosicao;
            operacaoFundo.TipoOperacao = (byte)TipoOperacaoFundo.AplicacaoCotasEspecial;
            operacaoFundo.DataAgendamento = eventoFundo.DataPosicao;
            operacaoFundo.Quantidade = posicaoDestino.Quantidade;
            operacaoFundo.IdFormaLiquidacao = 1;
            operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Interno;
            operacaoFundo.ValoresColados = "N";
            operacaoFundo.IdLocalNegociacao = (int)LocalNegociacaoFixo.Brasil;
            operacaoFundo.CotaOperacao = posicaoDestino.CotaDia;
            operacaoFundo.ValorBruto = posicaoDestino.ValorBruto;
            operacaoFundo.ValorLiquido = posicaoDestino.ValorLiquido;
            operacaoFundo.ValorIR = posicaoDestino.ValorIR;
            operacaoFundo.ValorIOF = posicaoDestino.ValorIOF;
            operacaoFundo.ValorPerformance = posicaoDestino.ValorPerformance;
            operacaoFundo.RendimentoResgate = posicaoDestino.ValorRendimento;

            operacaoFundo.Save();

            //Atualiza operação na posição
            posicaoDestino.IdOperacao = operacaoFundo.IdOperacao;
            posicaoDestino.Save();

            eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
            eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.Fundos;
            eventoFundoMovimentoAtivos.IdOperacao = posicaoDestino.IdOperacao;
            eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoAtivos.Save();

        }

        private void geraMovimentoRendaFixa(PosicaoRendaFixa posicaoDestino, PosicaoRendaFixa posicaoOrigem, EventoFundo eventoFundo)
        {
            
            //Retirada
            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(posicaoOrigem.IdTitulo.Value);
            int tipoPapel = tituloRendaFixa.UpToPapelRendaFixaByIdPapel.TipoPapel.Value;

            operacaoRendaFixa.IdCliente = eventoFundo.IdCarteiraOrigem;
            operacaoRendaFixa.IdTitulo = posicaoOrigem.IdTitulo;
            operacaoRendaFixa.DataOperacao = eventoFundo.DataPosicao;
            operacaoRendaFixa.DataLiquidacao = eventoFundo.DataPosicao;
            operacaoRendaFixa.DataRegistro = eventoFundo.DataPosicao;
            operacaoRendaFixa.TipoOperacao = (byte)TipoOperacaoTitulo.Retirada;
            operacaoRendaFixa.PUOperacao = posicaoOrigem.PUMercado;
            operacaoRendaFixa.Quantidade = posicaoOrigem.Quantidade;
            operacaoRendaFixa.Valor = (posicaoOrigem.PUMercado * posicaoOrigem.Quantidade);
            operacaoRendaFixa.TaxaOperacao = posicaoOrigem.TaxaOperacao;
            operacaoRendaFixa.IdLiquidacao = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)ClearingFixo.Cetip : (byte)ClearingFixo.Selic);
            operacaoRendaFixa.IdCustodia = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)LocalCustodiaFixo.Cetip : (byte)LocalCustodiaFixo.Selic);
            operacaoRendaFixa.IdLocalNegociacao = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)LocalNegociacaoFixo.CETIP : (byte)LocalNegociacaoFixo.SELIC); 
            operacaoRendaFixa.IdAgenteCorretora = posicaoOrigem.IdAgente;
            operacaoRendaFixa.TipoNegociacao = posicaoOrigem.TipoNegociacao;
            operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.Automatico;
            operacaoRendaFixa.OperacaoTermo = posicaoOrigem.OperacaoTermo;
            operacaoRendaFixa.DataVolta = posicaoOrigem.DataVolta;
            operacaoRendaFixa.TaxaVolta = posicaoOrigem.TaxaVolta;
            operacaoRendaFixa.PUVolta = posicaoOrigem.PUVolta;
            operacaoRendaFixa.ValorVolta = posicaoOrigem.ValorVolta;
            operacaoRendaFixa.IdIndiceVolta = posicaoOrigem.IdIndiceVolta;

            operacaoRendaFixa.Save();

            EventoFundoMovimentoAtivos eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
            eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.RendaFixa;
            eventoFundoMovimentoAtivos.IdOperacao = operacaoRendaFixa.IdOperacao;
            eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoAtivos.Save();

            //Deposito
            operacaoRendaFixa = new OperacaoRendaFixa();
            operacaoRendaFixa.IdCliente = eventoFundo.IdCarteiraOrigem;
            operacaoRendaFixa.IdTitulo = posicaoDestino.IdTitulo;
            operacaoRendaFixa.DataOperacao = eventoFundo.DataPosicao;
            operacaoRendaFixa.DataLiquidacao = eventoFundo.DataPosicao;
            operacaoRendaFixa.DataRegistro = eventoFundo.DataPosicao;
            operacaoRendaFixa.TipoOperacao = (byte)TipoOperacaoTitulo.Deposito;
            operacaoRendaFixa.PUOperacao = posicaoDestino.PUMercado;
            operacaoRendaFixa.Quantidade = posicaoDestino.Quantidade;
            operacaoRendaFixa.Valor = (posicaoDestino.PUMercado * posicaoDestino.Quantidade);
            operacaoRendaFixa.TaxaOperacao = posicaoDestino.TaxaOperacao;
            operacaoRendaFixa.IdLiquidacao = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)ClearingFixo.Cetip : (byte)ClearingFixo.Selic);
            operacaoRendaFixa.IdCustodia = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)LocalCustodiaFixo.Cetip : (byte)LocalCustodiaFixo.Selic);
            operacaoRendaFixa.IdLocalNegociacao = (tipoPapel == (int)TipoPapelTitulo.Privado ? (byte)LocalNegociacaoFixo.CETIP : (byte)LocalNegociacaoFixo.SELIC);
            operacaoRendaFixa.IdAgenteCorretora = posicaoDestino.IdAgente;
            operacaoRendaFixa.TipoNegociacao = posicaoDestino.TipoNegociacao;
            operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.Automatico;
            operacaoRendaFixa.OperacaoTermo = posicaoDestino.OperacaoTermo;
            operacaoRendaFixa.DataVolta = posicaoDestino.DataVolta;
            operacaoRendaFixa.TaxaVolta = posicaoDestino.TaxaVolta;
            operacaoRendaFixa.PUVolta = posicaoDestino.PUVolta;
            operacaoRendaFixa.ValorVolta = posicaoDestino.ValorVolta;
            operacaoRendaFixa.IdIndiceVolta = posicaoDestino.IdIndiceVolta;

            operacaoRendaFixa.Save();

            //Atualiza operação na posição
            posicaoDestino.IdOperacao = operacaoRendaFixa.IdOperacao;
            posicaoDestino.Save();

            eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
            eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.RendaFixa;
            eventoFundoMovimentoAtivos.IdOperacao = posicaoDestino.IdOperacao;
            eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoAtivos.Save();
        }

        private void geraMovimentoSwap(PosicaoSwap posicaoDestino, PosicaoSwap posicaoOrigem, EventoFundo eventoFundo)
        {
            EventoFundoMovimentoAtivos eventoFundoMovimentoAtivos = null;

            if (!((eventoFundo.TipoEvento == 1 || eventoFundo.TipoEvento == 3) && (eventoFundo.Percentual.HasValue && eventoFundo.Percentual < 100)))
            {
                //Retirada
                OperacaoSwap operacaoSwap = new OperacaoSwap();
                operacaoSwap.IdCliente = eventoFundo.IdCarteiraOrigem;
                operacaoSwap.TipoRegistro = posicaoOrigem.TipoRegistro;
                operacaoSwap.IdAgente = posicaoOrigem.IdAgente;

                operacaoSwap.NumeroContrato = posicaoOrigem.NumeroContrato;
                operacaoSwap.ComGarantia = posicaoOrigem.ComGarantia;
                operacaoSwap.DataEmissao = posicaoOrigem.DataEmissao;
                operacaoSwap.DataRegistro = posicaoOrigem.DataEmissao;
                operacaoSwap.DataVencimento = posicaoOrigem.DataVencimento;
                operacaoSwap.ValorBase = posicaoOrigem.ValorBase;

                operacaoSwap.TipoPonta = posicaoOrigem.TipoPontaContraParte;
                operacaoSwap.Percentual = posicaoOrigem.PercentualContraParte;
                operacaoSwap.IdIndice = posicaoOrigem.IdIndiceContraParte;
                operacaoSwap.TipoApropriacao = posicaoOrigem.TipoApropriacaoContraParte;
                operacaoSwap.ContagemDias = posicaoOrigem.ContagemDiasContraParte;
                operacaoSwap.BaseAno = posicaoOrigem.BaseAnoContraParte;
                operacaoSwap.TaxaJuros = posicaoOrigem.TaxaJurosContraParte;
                operacaoSwap.ValorIndicePartida = posicaoOrigem.ValorIndicePartidaContraParte;

                operacaoSwap.TipoPontaContraParte = posicaoOrigem.TipoPonta;
                operacaoSwap.PercentualContraParte = posicaoOrigem.Percentual;
                operacaoSwap.IdIndiceContraParte = posicaoOrigem.IdIndice;
                operacaoSwap.TipoApropriacaoContraParte = posicaoOrigem.TipoApropriacao;
                operacaoSwap.ContagemDiasContraParte = posicaoOrigem.ContagemDias;
                operacaoSwap.BaseAnoContraParte = posicaoOrigem.BaseAno;
                operacaoSwap.TaxaJurosContraParte = posicaoOrigem.TaxaJuros;
                operacaoSwap.ValorIndicePartidaContraParte = posicaoOrigem.ValorIndicePartida;

                operacaoSwap.DiasCorridos = posicaoOrigem.DiasCorridos;
                operacaoSwap.DiasUteis = posicaoOrigem.DiasUteis;

                operacaoSwap.Save();

                eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
                eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
                eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.Swap;
                eventoFundoMovimentoAtivos.IdOperacao = operacaoSwap.IdOperacao;
                eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
                eventoFundoMovimentoAtivos.Save();
            }

            //Deposito
            OperacaoSwap operacaoSwapDeposito = new OperacaoSwap();
            operacaoSwapDeposito.IdCliente = eventoFundo.IdCarteiraOrigem;
            operacaoSwapDeposito.TipoRegistro = posicaoDestino.TipoRegistro;
            operacaoSwapDeposito.IdAgente = posicaoDestino.IdAgente;

            operacaoSwapDeposito.NumeroContrato = posicaoDestino.NumeroContrato;
            operacaoSwapDeposito.ComGarantia = posicaoDestino.ComGarantia;
            operacaoSwapDeposito.DataEmissao = posicaoDestino.DataEmissao;
            operacaoSwapDeposito.DataRegistro = posicaoDestino.DataEmissao;
            operacaoSwapDeposito.DataVencimento = posicaoDestino.DataVencimento;
            operacaoSwapDeposito.ValorBase = posicaoDestino.ValorBase;

            operacaoSwapDeposito.TipoPonta = posicaoDestino.TipoPontaContraParte;
            operacaoSwapDeposito.Percentual = posicaoDestino.PercentualContraParte;
            operacaoSwapDeposito.IdIndice = posicaoDestino.IdIndiceContraParte;
            operacaoSwapDeposito.TipoApropriacao = posicaoDestino.TipoApropriacaoContraParte;
            operacaoSwapDeposito.ContagemDias = posicaoDestino.ContagemDiasContraParte;
            operacaoSwapDeposito.BaseAno = posicaoDestino.BaseAnoContraParte;
            operacaoSwapDeposito.TaxaJuros = posicaoDestino.TaxaJurosContraParte;
            operacaoSwapDeposito.ValorIndicePartida = posicaoDestino.ValorIndicePartidaContraParte;

            operacaoSwapDeposito.TipoPontaContraParte = posicaoDestino.TipoPonta;
            operacaoSwapDeposito.PercentualContraParte = posicaoDestino.Percentual;
            operacaoSwapDeposito.IdIndiceContraParte = posicaoDestino.IdIndice;
            operacaoSwapDeposito.TipoApropriacaoContraParte = posicaoDestino.TipoApropriacao;
            operacaoSwapDeposito.ContagemDiasContraParte = posicaoDestino.ContagemDias;
            operacaoSwapDeposito.BaseAnoContraParte = posicaoDestino.BaseAno;
            operacaoSwapDeposito.TaxaJurosContraParte = posicaoDestino.TaxaJuros;
            operacaoSwapDeposito.ValorIndicePartidaContraParte = posicaoDestino.ValorIndicePartida;

            operacaoSwapDeposito.DiasCorridos = posicaoDestino.DiasCorridos;
            operacaoSwapDeposito.DiasUteis = posicaoDestino.DiasUteis;

            operacaoSwapDeposito.Save();

            posicaoDestino.IdOperacao = operacaoSwapDeposito.IdOperacao;
            posicaoDestino.Save();

            eventoFundoMovimentoAtivos = new EventoFundoMovimentoAtivos();
            eventoFundoMovimentoAtivos.IdEventoFundo = eventoFundo.IdEventoFundo;
            eventoFundoMovimentoAtivos.TipoMercado = (int)TipoMercado.Swap;
            eventoFundoMovimentoAtivos.IdOperacao = posicaoDestino.IdOperacao;
            eventoFundoMovimentoAtivos.IdCliente = eventoFundo.IdCarteiraOrigem;
            eventoFundoMovimentoAtivos.Save();

        }


        private PosicaoBolsa copiaPosicaoBolsa(PosicaoBolsa posicao)
        {
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();

            posicaoBolsa.IdAgente = posicao.IdAgente;
            posicaoBolsa.IdCliente = posicao.IdCliente;
            posicaoBolsa.CdAtivoBolsa = posicao.CdAtivoBolsa;
            posicaoBolsa.TipoMercado = posicao.TipoMercado;
            posicaoBolsa.DataVencimento = posicao.DataVencimento;
            posicaoBolsa.PUMercado = posicao.PUMercado;
            posicaoBolsa.PUCusto = posicao.PUCusto;
            posicaoBolsa.PUCustoLiquido = posicao.PUCustoLiquido;
            posicaoBolsa.ValorMercado = posicao.ValorMercado;
            posicaoBolsa.ValorCustoLiquido = posicao.ValorCustoLiquido;
            posicaoBolsa.Quantidade = posicao.Quantidade;
            posicaoBolsa.QuantidadeInicial = posicao.QuantidadeInicial;
            posicaoBolsa.QuantidadeBloqueada = posicao.QuantidadeBloqueada;
            posicaoBolsa.ResultadoRealizar = posicao.ResultadoRealizar;

            return posicaoBolsa;
        }

        private PosicaoSwap copiaPosicaoSwap(PosicaoSwap posicao)
        {
            PosicaoSwap posicaoSwap = new PosicaoSwap();

            posicaoSwap.IdAssessor = posicao.IdAssessor;
            posicaoSwap.TipoRegistro = posicao.TipoRegistro;
            posicaoSwap.NumeroContrato = posicao.NumeroContrato;
            posicaoSwap.DataEmissao = posicao.DataEmissao;
            posicaoSwap.DataVencimento = posicao.DataVencimento;
            posicaoSwap.ValorBase = posicao.ValorBase;
            posicaoSwap.TipoPonta = posicao.TipoPonta;
            posicaoSwap.IdIndice = posicao.IdIndice;
            posicaoSwap.TaxaJuros = posicao.TaxaJuros;
            posicaoSwap.Percentual = posicao.Percentual;
            posicaoSwap.TipoApropriacao = posicao.TipoApropriacao;
            posicaoSwap.ContagemDias = posicao.ContagemDias;
            posicaoSwap.BaseAno = posicao.BaseAno;
            posicaoSwap.TipoPontaContraParte = posicao.TipoPontaContraParte;
            posicaoSwap.IdIndiceContraParte = posicao.IdIndiceContraParte;
            posicaoSwap.TaxaJurosContraParte = posicao.TaxaJurosContraParte;
            posicaoSwap.PercentualContraParte = posicao.PercentualContraParte;
            posicaoSwap.TipoApropriacaoContraParte = posicao.TipoApropriacaoContraParte;
            posicaoSwap.ContagemDiasContraParte = posicao.ContagemDiasContraParte;
            posicaoSwap.BaseAnoContraParte = posicao.BaseAnoContraParte;
            posicaoSwap.IdAgente = posicao.IdAgente;
            posicaoSwap.ComGarantia = posicao.ComGarantia;
            posicaoSwap.DiasUteis = posicao.DiasUteis;
            posicaoSwap.DiasCorridos = posicao.DiasCorridos;
            posicaoSwap.ValorParte = posicao.ValorParte;
            posicaoSwap.ValorContraParte = posicao.ValorContraParte;
            posicaoSwap.Saldo = posicao.Saldo;
            posicaoSwap.ValorIndicePartida = posicao.ValorIndicePartida;
            posicaoSwap.ValorIndicePartidaContraParte = posicao.ValorIndicePartidaContraParte;
            posicaoSwap.IdEstrategia = posicao.IdEstrategia;
            posicaoSwap.ValorIR = posicao.ValorIR;

            return posicaoSwap;
        }

        private PosicaoBMF copiaPosicaoBMF(PosicaoBMF posicao)
        {
            PosicaoBMF posicaoBMF = new PosicaoBMF();

            posicaoBMF.IdAgente = posicao.IdAgente;
            posicaoBMF.CdAtivoBMF = posicao.CdAtivoBMF;
            posicaoBMF.Serie = posicao.Serie;
            posicaoBMF.TipoMercado = posicao.TipoMercado;
            posicaoBMF.DataVencimento = posicao.DataVencimento;
            posicaoBMF.PUMercado = posicao.PUMercado;
            posicaoBMF.PUCusto = posicao.PUCusto;
            posicaoBMF.PUCustoLiquido = posicao.PUCustoLiquido;
            posicaoBMF.Quantidade = posicao.Quantidade;
            posicaoBMF.QuantidadeInicial = posicao.QuantidadeInicial;
            posicaoBMF.ValorMercado = posicao.ValorMercado;
            posicaoBMF.ValorCustoLiquido = posicao.ValorCustoLiquido;
            posicaoBMF.ResultadoRealizar = posicao.ResultadoRealizar;
            posicaoBMF.AjusteDiario = posicao.AjusteDiario;
            posicaoBMF.AjusteAcumulado = posicao.AjusteAcumulado;

            return posicaoBMF;

        }

        private PosicaoRendaFixa copiaPosicaoRendaFixa(PosicaoRendaFixa posicao)
        {
            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();

            posicaoRendaFixa.IdTitulo = posicao.IdTitulo;
            posicaoRendaFixa.TipoOperacao = posicao.TipoOperacao;
            posicaoRendaFixa.DataVencimento = posicao.DataVencimento;
            posicaoRendaFixa.Quantidade = posicao.Quantidade;
            posicaoRendaFixa.QuantidadeBloqueada = posicao.QuantidadeBloqueada;
            posicaoRendaFixa.DataOperacao = posicao.DataOperacao;
            posicaoRendaFixa.DataLiquidacao = posicao.DataLiquidacao;
            posicaoRendaFixa.PUOperacao = posicao.PUOperacao;
            posicaoRendaFixa.PUCurva = posicao.PUCurva;
            posicaoRendaFixa.ValorCurva = posicao.ValorCurva;
            posicaoRendaFixa.PUMercado = posicao.PUMercado;
            posicaoRendaFixa.ValorMercado = posicao.ValorMercado;
            posicaoRendaFixa.PUJuros = posicao.PUJuros;
            posicaoRendaFixa.ValorJuros = posicao.ValorJuros;
            posicaoRendaFixa.DataVolta = posicao.DataVolta;
            posicaoRendaFixa.TaxaVolta = posicao.TaxaVolta;
            posicaoRendaFixa.PUVolta = posicao.PUVolta;
            posicaoRendaFixa.ValorVolta = posicao.ValorVolta;
            posicaoRendaFixa.QuantidadeInicial = posicao.QuantidadeInicial;
            posicaoRendaFixa.ValorIR = posicao.ValorIR;
            posicaoRendaFixa.ValorIOF = posicao.ValorIOF;
            posicaoRendaFixa.TipoNegociacao = posicao.TipoNegociacao;
            posicaoRendaFixa.PUCorrecao = posicao.PUCorrecao;
            posicaoRendaFixa.ValorCorrecao = posicao.ValorCorrecao;
            posicaoRendaFixa.TaxaOperacao = posicao.TaxaOperacao;
            posicaoRendaFixa.IdAgente = posicao.IdAgente;
            posicaoRendaFixa.IdCustodia = posicao.IdCustodia;
            posicaoRendaFixa.CustoCustodia = posicao.CustoCustodia;
            posicaoRendaFixa.IdIndiceVolta = posicao.IdIndiceVolta;
            posicaoRendaFixa.OperacaoTermo = posicao.OperacaoTermo;
            posicaoRendaFixa.ValorCurvaVencimento = posicao.ValorCurvaVencimento;
            posicaoRendaFixa.PUCurvaVencimento = posicao.PUCurvaVencimento;
            posicaoRendaFixa.AjusteMTM = posicao.AjusteMTM;
            posicaoRendaFixa.AjusteVencimento = posicao.AjusteVencimento;
            posicaoRendaFixa.TaxaMTM = posicao.TaxaMTM;

            return posicaoRendaFixa;
        }

        private PosicaoFundo copiaPosicaoFundo(PosicaoFundo posicao)
        {
            PosicaoFundo posicaoFundo = new PosicaoFundo();

            posicaoFundo.IdCarteira = posicao.IdCarteira;
            posicaoFundo.IdCliente = posicao.IdCliente;
            posicaoFundo.ValorAplicacao = posicao.ValorAplicacao;
            posicaoFundo.DataAplicacao = posicao.DataAplicacao;
            posicaoFundo.DataConversao = posicao.DataConversao;
            posicaoFundo.CotaAplicacao = posicao.CotaAplicacao;
            posicaoFundo.CotaDia = posicao.CotaDia;
            posicaoFundo.ValorBruto = posicao.ValorBruto;
            posicaoFundo.ValorLiquido = posicao.ValorLiquido;
            posicaoFundo.QuantidadeInicial = posicao.QuantidadeInicial;
            posicaoFundo.Quantidade = posicao.Quantidade;
            posicaoFundo.QuantidadeBloqueada = posicao.QuantidadeBloqueada;
            posicaoFundo.DataUltimaCobrancaIR = posicao.DataUltimaCobrancaIR;
            posicaoFundo.ValorIR = posicao.ValorIR;
            posicaoFundo.ValorIOF = posicao.ValorIOF;
            posicaoFundo.ValorPerformance = posicao.ValorPerformance;
            posicaoFundo.ValorIOFVirtual = posicao.ValorIOF;
            posicaoFundo.QuantidadeAntesCortes = posicao.QuantidadeAntesCortes;
            posicaoFundo.ValorRendimento = posicao.ValorRendimento;
            posicaoFundo.DataUltimoCortePfee = posicao.DataUltimoCortePfee;
            posicaoFundo.PosicaoIncorporada = posicao.PosicaoIncorporada;

            return posicaoFundo;
        }

        private Liquidacao copiaLiquidacao(Liquidacao liquidacao)
        {
            Liquidacao liquidacaoDestino = new Liquidacao();

            liquidacaoDestino.DataLancamento = liquidacao.DataLancamento;
            liquidacaoDestino.DataVencimento = liquidacao.DataVencimento;
            liquidacaoDestino.Descricao = liquidacao.Descricao;
            liquidacaoDestino.Valor = liquidacao.Valor;
            liquidacaoDestino.Situacao = liquidacao.Situacao;
            liquidacaoDestino.Origem = liquidacao.Origem;
            liquidacaoDestino.Fonte = liquidacao.Fonte;
            liquidacaoDestino.IdAgente = liquidacao.IdAgente;
            liquidacaoDestino.IdentificadorOrigem = liquidacao.IdentificadorOrigem;
            liquidacaoDestino.IdConta = liquidacao.IdConta;
            liquidacaoDestino.IdEvento = liquidacao.IdEvento;

            return liquidacaoDestino;
        }

        private void excluiHistoricoIncorporacao(EventoFundo eventoFundo)
        {
            HistoricoIncorporacaoPosicaoCollection historicoIncorporacaoPosicaoCollectionDeletar = new HistoricoIncorporacaoPosicaoCollection();
            historicoIncorporacaoPosicaoCollectionDeletar.Query.Where(historicoIncorporacaoPosicaoCollectionDeletar.Query.IdCarteiraDestino.Equal(eventoFundo.IdCarteiraDestino),
                                                                      historicoIncorporacaoPosicaoCollectionDeletar.Query.IdCarteiraOrigem.Equal(eventoFundo.IdCarteiraOrigem),
                                                                      historicoIncorporacaoPosicaoCollectionDeletar.Query.DataIncorporacao.Equal(eventoFundo.DataPosicao));
            historicoIncorporacaoPosicaoCollectionDeletar.Query.Load();
            historicoIncorporacaoPosicaoCollectionDeletar.MarkAllAsDeleted();
            historicoIncorporacaoPosicaoCollectionDeletar.Save();
        }

        public void ExcluiMovimentosGerados(EventoFundo eventoFundo, int idCliente)
        {
            //Exclui movimento Ativos
            EventoFundoMovimentoAtivosCollection eventoFundoMovimentoAtivosCollection = new EventoFundoMovimentoAtivosCollection();
            eventoFundoMovimentoAtivosCollection.Query.Where(eventoFundoMovimentoAtivosCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            if (idCliente > 0)
            {
                eventoFundoMovimentoAtivosCollection.Query.Where(eventoFundoMovimentoAtivosCollection.Query.IdCliente.Equal(idCliente));
            }
            eventoFundoMovimentoAtivosCollection.Query.Load();

            foreach (EventoFundoMovimentoAtivos eventoFundoMovimentoAtivos in eventoFundoMovimentoAtivosCollection)
            {

                switch (eventoFundoMovimentoAtivos.TipoMercado.Value)
                {
                    case 1:
                        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                        if (operacaoBolsa.LoadByPrimaryKey(eventoFundoMovimentoAtivos.IdOperacao.Value))
                        {
                            operacaoBolsa.MarkAsDeleted();
                            operacaoBolsa.Save();
                        }
                        break;
                    case 2:
                        OperacaoBMF operacaoBMF = new OperacaoBMF();
                        if (operacaoBMF.LoadByPrimaryKey(eventoFundoMovimentoAtivos.IdOperacao.Value))
                        {
                            operacaoBMF.MarkAsDeleted();
                            operacaoBMF.Save();
                        }
                        break;
                    case 3:
                        OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
                        if (operacaoRendaFixa.LoadByPrimaryKey(eventoFundoMovimentoAtivos.IdOperacao.Value))
                        {
                            operacaoRendaFixa.MarkAsDeleted();
                            operacaoRendaFixa.Save();
                        }
                        break;
                    case 4:
                        OperacaoSwap operacaoSwap = new OperacaoSwap();
                        if (operacaoSwap.LoadByPrimaryKey(eventoFundoMovimentoAtivos.IdOperacao.Value))
                        {
                            operacaoSwap.MarkAsDeleted();
                            operacaoSwap.Save();
                        }
                        break;
                    case 5:
                        OperacaoFundo operacaoFundo = new OperacaoFundo();
                        if (operacaoFundo.LoadByPrimaryKey(eventoFundoMovimentoAtivos.IdOperacao.Value))
                        {
                            operacaoFundo.MarkAsDeleted();
                            operacaoFundo.Save();
                        }
                        break;
                }

            }

            eventoFundoMovimentoAtivosCollection.MarkAllAsDeleted();
            eventoFundoMovimentoAtivosCollection.Save();

            //Exclui movimento Cautelas
            EventoFundoMovimentoCautelasQuery eventoFundoMovimentoCautelasQuery = new EventoFundoMovimentoCautelasQuery("EFMC");
            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("OC");

            operacaoCotistaQuery.InnerJoin(eventoFundoMovimentoCautelasQuery).On(operacaoCotistaQuery.IdOperacao.Equal(eventoFundoMovimentoCautelasQuery.IdOperacao));
            operacaoCotistaQuery.Where(eventoFundoMovimentoCautelasQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            if (idCliente > 0)
            {
                operacaoCotistaQuery.Where(eventoFundoMovimentoCautelasQuery.IdCliente.Equal(idCliente));
            }

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

            operacaoCotistaCollection.Load(operacaoCotistaQuery);

            operacaoCotistaCollection.MarkAllAsDeleted();
            operacaoCotistaCollection.Save();

            EventoFundoMovimentoCautelasCollection eventoFundoMovimentoCautelasCollection = new EventoFundoMovimentoCautelasCollection();
            eventoFundoMovimentoCautelasCollection.Query.Where(eventoFundoMovimentoCautelasCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            if (idCliente > 0)
            {
                eventoFundoMovimentoCautelasCollection.Query.Where(eventoFundoMovimentoCautelasCollection.Query.IdCliente.Equal(idCliente));
            }
            eventoFundoMovimentoCautelasCollection.Query.Load();
            eventoFundoMovimentoCautelasCollection.MarkAllAsDeleted();
            eventoFundoMovimentoCautelasCollection.Save();

        }

        public void ExcluiPosicoesGeradas(EventoFundo eventoFundo, int idCliente)
        {

            //Exclui posicao Ativos
            EventoFundoPosicaoAtivosCollection eventoFundoPosicaoAtivosCollection = new EventoFundoPosicaoAtivosCollection();
            eventoFundoPosicaoAtivosCollection.Query.Where(eventoFundoPosicaoAtivosCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            if (idCliente > 0)
            {
                eventoFundoPosicaoAtivosCollection.Query.Where(eventoFundoPosicaoAtivosCollection.Query.IdCliente.Equal(idCliente));
            }
            eventoFundoPosicaoAtivosCollection.Query.Load();

            foreach (EventoFundoPosicaoAtivos eventoFundoPosicaoAtivos in eventoFundoPosicaoAtivosCollection)
            {

                switch (eventoFundoPosicaoAtivos.TipoMercado.Value)
                {
                    case 1:
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        if (posicaoBolsa.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value))
                        {
                            posicaoBolsa.MarkAsDeleted();
                            posicaoBolsa.Save();

                            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                            if (posicaoBolsaHistorico.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value, eventoFundo.DataPosicao.Value))
                            {
                                posicaoBolsaHistorico.MarkAsDeleted();
                                posicaoBolsaHistorico.Save();
                            }
                        }
                        break;
                    case 2:
                        PosicaoBMF posicaoBMF = new PosicaoBMF();
                        if (posicaoBMF.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value))
                        {
                            posicaoBMF.MarkAsDeleted();
                            posicaoBMF.Save();

                            PosicaoBMFHistorico posicaoBMFHistorico = new PosicaoBMFHistorico();
                            if (posicaoBMFHistorico.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value, eventoFundo.DataPosicao.Value))
                            {
                                posicaoBMFHistorico.MarkAsDeleted();
                                posicaoBMFHistorico.Save();
                            }
                        }
                        break;
                    case 3:
                        PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                        if (posicaoRendaFixa.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value))
                        {
                            posicaoRendaFixa.MarkAsDeleted();
                            posicaoRendaFixa.Save();

                            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                            if (posicaoRendaFixaHistorico.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value, eventoFundo.DataPosicao.Value))
                            {
                                posicaoRendaFixaHistorico.MarkAsDeleted();
                                posicaoRendaFixaHistorico.Save();
                            }
                        }
                        break;
                    case 4:
                        PosicaoSwap posicaoSwap = new PosicaoSwap();
                        if (posicaoSwap.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value))
                        {
                            posicaoSwap.MarkAsDeleted();
                            posicaoSwap.Save();

                            PosicaoSwapHistorico posicaoSwapHistorico = new PosicaoSwapHistorico();
                            if (posicaoSwapHistorico.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value, eventoFundo.DataPosicao.Value))
                            {
                                posicaoSwapHistorico.MarkAsDeleted();
                                posicaoSwapHistorico.Save();
                            }
                        }
                        break;
                    case 5:
                        PosicaoFundo posicaoFundo = new PosicaoFundo();
                        if (posicaoFundo.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value))
                        {
                            posicaoFundo.MarkAsDeleted();
                            posicaoFundo.Save();

                            PosicaoFundoHistorico PosicaoFundoHistorico = new PosicaoFundoHistorico();
                            if (PosicaoFundoHistorico.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value, eventoFundo.DataPosicao.Value))
                            {
                                PosicaoFundoHistorico.MarkAsDeleted();
                                PosicaoFundoHistorico.Save();
                            }
                        }
                        break;
                    case 6:
                        Liquidacao liquidacao = new Liquidacao();
                        if (liquidacao.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value))
                        {
                            if (liquidacao.Origem.Equals((int)OrigemLancamentoLiquidacao.Fundo.TransferenciaSaldo) && eventoFundo.IdCarteiraDestino.Equals(liquidacao.IdCliente))
                            {
                                //Busca o idContaDefault do cliente
                                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                                int idContaDefault = contaCorrente.RetornaContaDefault(eventoFundo.IdCarteiraDestino.Value);

                                SaldoCaixa saldoCaixa = new SaldoCaixa();
                                saldoCaixa.LoadByPrimaryKey(liquidacao.IdCliente.Value, eventoFundo.DataPosicao.Value, idContaDefault);
                                saldoCaixa.SaldoFechamento -= liquidacao.Valor;
                                saldoCaixa.Save();
                            }

                            liquidacao.MarkAsDeleted();
                            liquidacao.Save();
                        }

                        LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();
                        if (liquidacaoHistorico.LoadByPrimaryKey(eventoFundoPosicaoAtivos.IdPosicao.Value, eventoFundo.DataPosicao.Value))
                        {
                            liquidacaoHistorico.MarkAsDeleted();
                            liquidacaoHistorico.Save();
                        }
                        break;
                }
            }

            eventoFundoPosicaoAtivosCollection.MarkAllAsDeleted();
            eventoFundoPosicaoAtivosCollection.Save();

            //Exclui movimento Cautelas
            EventoFundoPosicaoCautelasQuery eventoFundoPosicaoCautelasQuery = new EventoFundoPosicaoCautelasQuery("EFPC");
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("PC");

            posicaoCotistaQuery.InnerJoin(eventoFundoPosicaoCautelasQuery).On(posicaoCotistaQuery.IdPosicao.Equal(eventoFundoPosicaoCautelasQuery.IdPosicao));
            posicaoCotistaQuery.Where(eventoFundoPosicaoCautelasQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            if (idCliente > 0)
            {
                posicaoCotistaQuery.Where(eventoFundoPosicaoCautelasQuery.IdCliente.Equal(idCliente));
            }
            
            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();

            posicaoCotistaCollection.Load(posicaoCotistaQuery);

            posicaoCotistaCollection.MarkAllAsDeleted();
            posicaoCotistaCollection.Save();

            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("PCH");

            posicaoCotistaHistoricoQuery.InnerJoin(eventoFundoPosicaoCautelasQuery).On(posicaoCotistaHistoricoQuery.IdPosicao.Equal(eventoFundoPosicaoCautelasQuery.IdPosicao));
            posicaoCotistaHistoricoQuery.Where(eventoFundoPosicaoCautelasQuery.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico.Equal(eventoFundo.DataPosicao.Value));
            if (idCliente > 0)
            {
                posicaoCotistaHistoricoQuery.Where(eventoFundoPosicaoCautelasQuery.IdCliente.Equal(idCliente));
            }

            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();

            posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

            posicaoCotistaHistoricoCollection.MarkAllAsDeleted();
            posicaoCotistaHistoricoCollection.Save();

            EventoFundoPosicaoCautelasCollection eventoFundoPosicaoCautelasCollection = new EventoFundoPosicaoCautelasCollection();
            eventoFundoPosicaoCautelasCollection.Query.Where(eventoFundoPosicaoCautelasCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
            if (idCliente > 0)
            {
                eventoFundoPosicaoCautelasCollection.Query.Where(eventoFundoPosicaoCautelasCollection.Query.IdCliente.Equal(idCliente));
            }
            eventoFundoPosicaoCautelasCollection.Query.Load();
            eventoFundoPosicaoCautelasCollection.MarkAllAsDeleted();
            eventoFundoPosicaoCautelasCollection.Save();
        }

        public void ProcessaIngressoRetirada(int idCarteira, DateTime data)
        {
            decimal valorIngressoRetirada = this.CalculaValorIngressoRetirada(idCarteira, data);
            if(valorIngressoRetirada != 0)
            {
                HistoricoCota historicoCota = new HistoricoCota();
                historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira) &
                                          historicoCota.Query.Data.Equal(data));

                if (historicoCota.Query.Load())
                {
                    historicoCota.QuantidadeFechamento = (valorIngressoRetirada / historicoCota.CotaFechamento.Value) + historicoCota.QuantidadeFechamento.Value;
                    historicoCota.PLFechamento = historicoCota.QuantidadeFechamento * historicoCota.CotaFechamento.Value;
                    historicoCota.PLAbertura = historicoCota.QuantidadeFechamento * historicoCota.CotaAbertura.Value;
                    historicoCota.PatrimonioBruto = historicoCota.QuantidadeFechamento * historicoCota.CotaBruta.Value;

                    PosicaoCotista posicaoCotista = new PosicaoCotista();
                    posicaoCotista.DistribuiQtdeCotasPosIngRet(idCarteira, data, historicoCota);

                    historicoCota.Save();
                }
            }
        }

        public void ProcessaRetornoHistorico(DateTime dataReferencia)
        {
            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente);
            clienteCollection.Query.Where(clienteCollection.Query.TipoControle.Equal((byte)TipoControleCliente.Completo),
                                          clienteCollection.Query.DataDia.Equal(dataReferencia));
            clienteCollection.Query.Load();

            foreach (Cliente cliente in clienteCollection)
            {
                int idCliente = cliente.IdCliente.Value;

                HistoricoCotaCollection historicoCotaCollectionDeletar = new HistoricoCotaCollection();
                historicoCotaCollection.Query.Where(historicoCotaCollectionDeletar.Query.IdCarteira.Equal(idCliente),
                                                    historicoCotaCollectionDeletar.Query.Data.LessThan(dataReferencia));
                historicoCotaCollectionDeletar.Query.Load();
                historicoCotaCollectionDeletar.MarkAllAsDeleted();
                historicoCotaCollectionDeletar.Save();

                HistoricoCota historicoCotaData = new HistoricoCota();
                if (historicoCotaData.LoadByPrimaryKey(dataReferencia, idCliente))
                {
                    decimal quantidadeCotas = historicoCotaData.QuantidadeFechamento.Value;

                    //Hoje o critério é a data da aplicação em fundos mais antiga.
                    PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                    posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                       posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                       posicaoFundoCollection.Query.DataConversao.LessThan(dataReferencia));
                    posicaoFundoCollection.Query.OrderBy(posicaoFundoCollection.Query.DataConversao.Ascending);
                    posicaoFundoCollection.Query.Load();

                    if (posicaoFundoCollection.Count > 0)
                    {
                        DateTime dataInicio = posicaoFundoCollection[0].DataConversao.Value;

                        DateTime dataAux = Calendario.SubtraiDiaUtil(dataReferencia, 1);
                        while (dataAux >= dataInicio)
                        {
                            decimal valorMercado = 0;
                            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                            {
                                if (dataAux >= posicaoFundo.DataConversao.Value)
                                {
                                    decimal quantidadeAplicacao = posicaoFundo.Quantidade.Value;

                                    HistoricoCota historicoCota = new HistoricoCota();
                                    if (historicoCota.LoadByPrimaryKey(dataAux, posicaoFundo.IdCarteira.Value))
                                    {
                                        valorMercado += Math.Round(quantidadeAplicacao * historicoCota.CotaFechamento.Value, 2);
                                    }
                                }
                            }

                            decimal cota = Math.Round(valorMercado / quantidadeCotas, 8);

                            HistoricoCota historicoCotaInsert = historicoCotaCollection.AddNew();
                            historicoCotaInsert.AjustePL = 0;
                            historicoCotaInsert.CotaAbertura = cota;
                            historicoCotaInsert.CotaBruta = cota;
                            historicoCotaInsert.CotaFechamento = cota;
                            historicoCotaInsert.Data = dataAux;
                            historicoCotaInsert.IdCarteira = idCliente;
                            historicoCotaInsert.PatrimonioBruto = valorMercado;
                            historicoCotaInsert.PLAbertura = valorMercado;
                            historicoCotaInsert.PLFechamento = valorMercado;
                            historicoCotaInsert.QuantidadeFechamento = quantidadeCotas;

                            dataAux = Calendario.SubtraiDiaUtil(dataAux, 1);
                        }
                    }
                }
            }

            historicoCotaCollection.Save();
        }

        /// <summary>
        /// Ajusta cota liquida cotistas.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void AjustaCotaLiquidaYMF(int idCarteira, DateTime data)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            if (!historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                return;
            }

            //Se estamos simulando o COT, precisamos deduzir o Valor da Performance da Cota Bruta
            decimal valorCota = this.RetornaCotaBrutaComPfee(idCarteira, data);

            historicoCota.CotaFechamento = valorCota;
            historicoCota.CotaAbertura = valorCota;
            historicoCota.Save();
        }

        public decimal RetornaCotaBrutaComPfee(int idCarteira, DateTime data)
        {
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.Query.Select(posicaoCotista.Query.ValorPerformance.Sum(),
                                        posicaoCotista.Query.Quantidade.Sum());
            posicaoCotista.Query.Where(posicaoCotista.Query.IdCarteira.Equal(idCarteira),
                                       posicaoCotista.Query.ValorPerformance.IsNotNull());
            posicaoCotista.Query.Load();

            decimal valorPfeeCota = 0;
            if (posicaoCotista.ValorPerformance.HasValue)
            {
                decimal valorPfee = posicaoCotista.ValorPerformance.Value;
                decimal quantidade = posicaoCotista.Quantidade.Value;

                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);

                valorPfeeCota = Math.Round(valorPfee / quantidade, (int)carteira.CasasDecimaisCota);
            }

            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Query.Select(historicoCota.Query.CotaBruta);
            historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                      historicoCota.Query.Data.Equal(data));
            historicoCota.Query.Load();

            if (historicoCota.es.HasData && historicoCota.CotaBruta.HasValue)
            {
                return historicoCota.CotaBruta.Value - valorPfeeCota;
            }
            else
            {
                throw new Exception("Cota bruta não encontrada em " + data.ToShortDateString() + " para a carteira " + idCarteira.ToString());
            }

        }

        public decimal RetornaPatrimonioSemTributos(int idCliente, DateTime data)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            return this.RetornaPatrimonioSemTributos(cliente, data);
        }

        public decimal RetornaPatrimonioSemTributos(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            byte descontaTributoPL = cliente.DescontaTributoPL.Value;

            decimal valorPL = 0;

            this.BuscaValorPatrimonioDia(idCliente, data);

            if (this.PLFechamento.HasValue)
            {
                valorPL = this.PLFechamento.Value;
            }
            else
            {
                valorPL = this.PLAbertura.Value;
            }

            if (descontaTributoPL != (byte)DescontoPLCliente.PLComDesconto)
            {
                PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                posicaoFundoHistorico.Query.Select(posicaoFundoHistorico.Query.ValorIR.Sum(),
                                                   posicaoFundoHistorico.Query.ValorIOF.Sum());
                posicaoFundoHistorico.Query.Where(posicaoFundoHistorico.Query.IdCliente.Equal(idCliente),
                                                  posicaoFundoHistorico.Query.DataHistorico.Equal(data));
                posicaoFundoHistorico.Query.Load();

                decimal tributosFundos = posicaoFundoHistorico.ValorIR.HasValue ? posicaoFundoHistorico.ValorIR.Value + posicaoFundoHistorico.ValorIOF.Value : 0;

                PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                posicaoRendaFixaHistorico.Query.Select(posicaoRendaFixaHistorico.Query.ValorIR.Sum(),
                                                       posicaoRendaFixaHistorico.Query.ValorIOF.Sum());
                posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdCliente.Equal(idCliente),
                                                      posicaoRendaFixaHistorico.Query.DataHistorico.Equal(data));
                posicaoRendaFixaHistorico.Query.Load();

                decimal tributosRF = posicaoRendaFixaHistorico.ValorIR.HasValue ? posicaoRendaFixaHistorico.ValorIR.Value + posicaoRendaFixaHistorico.ValorIOF.Value : 0;

                valorPL -= (tributosFundos + tributosRF);
            }

            return valorPL;
        }

        public decimal CalculaValorIngressoRetirada(int idCarteira, DateTime data)
        {
            decimal valorMovimentado = 0;
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            #region RendaFixa
            OperacaoRendaFixaQuery opRendaFixaQuery = new OperacaoRendaFixaQuery("opRendaFixa");
            PosicaoRendaFixaQuery posRendaFixaQuery = new PosicaoRendaFixaQuery("posRendaFixa");
            DetalhePosicaoAfetadaRFQuery detalhePosAfetadaRFQuery = new DetalhePosicaoAfetadaRFQuery("detalheRendaFixa");
            OperacaoRendaFixaCollection opRendaFixaColl = new OperacaoRendaFixaCollection();

            opRendaFixaQuery.Select(opRendaFixaQuery.TipoOperacao,                                     
                                    posRendaFixaQuery.PUMercado.Coalesce("0"),
                                    detalhePosAfetadaRFQuery.QtdeMovimentada.Sum().Coalesce("opRendaFixa.Quantidade").As("Quantidade"));
            opRendaFixaQuery.LeftJoin(detalhePosAfetadaRFQuery).On(opRendaFixaQuery.IdOperacao.Equal(detalhePosAfetadaRFQuery.IdOperacao));
            opRendaFixaQuery.LeftJoin(posRendaFixaQuery).On(opRendaFixaQuery.IdOperacao.Equal(posRendaFixaQuery.IdOperacao) | detalhePosAfetadaRFQuery.IdPosicaoAfetada.Equal(posRendaFixaQuery.IdPosicao));
            opRendaFixaQuery.Where(opRendaFixaQuery.IdCliente.Equal(idCarteira) &
                                   opRendaFixaQuery.DataRegistro.Equal(data) &
                                   opRendaFixaQuery.TipoOperacao.In((byte)TipoOperacaoTitulo.IngressoAtivoImpactoQtde, (byte)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde));
            opRendaFixaQuery.GroupBy(opRendaFixaQuery.TipoOperacao, posRendaFixaQuery.PUMercado);

            if(opRendaFixaColl.Load(opRendaFixaQuery))
            {
                foreach (OperacaoRendaFixa opRendaFixa in opRendaFixaColl)
                {
                    decimal pu = 0;
                    decimal qtde = 0;
                    decimal valorOperacao = 0;
                    pu = Convert.ToDecimal(opRendaFixa.GetColumn(PosicaoRendaFixaMetadata.ColumnNames.PUMercado));
                    qtde = Convert.ToDecimal(opRendaFixa.GetColumn("Quantidade"));
                    valorOperacao = Math.Round(pu * qtde, 2);

                    if (opRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde)
                        valorOperacao *= (-1);

                    valorMovimentado += valorOperacao;
                }
            }            
            #endregion

            #region Bolsa
            OperacaoBolsaQuery opBolsaQuery = new OperacaoBolsaQuery("opBolsa");
            OperacaoBolsaCollection opBolsaColl = new OperacaoBolsaCollection();
            ClienteBolsa clienteBolsa = new ClienteBolsa();
            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
            bool emiteMsgSemCotacaoBolsa = ParametrosConfiguracaoSistema.Bolsa.EmiteMsgSemCotacaoBolsa;

            #region Busca o tipo de cotação - média ou fechamento            
            clienteBolsa.LoadByPrimaryKey(idCarteira);

            if (!clienteBolsa.es.HasData)
            {
                throw new ClienteBolsaNaoCadastradoException("Cliente " + idCarteira + " sem cadastro para Bolsa.");
            }
            int tipoCotacao = clienteBolsa.TipoCotacao.Value;
            #endregion

            opBolsaQuery.Select(opBolsaQuery.TipoOperacao, opBolsaQuery.CdAtivoBolsa, opBolsaQuery.Valor.Sum(), opBolsaQuery.Quantidade.Sum())
                            .Where(opBolsaQuery.IdCliente.Equal(idCarteira) &
                                   opBolsaQuery.Data.Equal(data) &
                                   opBolsaQuery.TipoOperacao.In(TipoOperacaoBolsa.IngressoAtivoImpactoQtde.ToString(), TipoOperacaoBolsa.RetiradaAtivoImpactoQtde.ToString()))
                            .GroupBy(opBolsaQuery.TipoOperacao, opBolsaQuery.CdAtivoBolsa)
                            .OrderBy(opBolsaQuery.CdAtivoBolsa.Ascending);

            
            if (opBolsaColl.Load(opBolsaQuery))
            {                
                Hashtable hsPuMercadoBolsa = new Hashtable();
                decimal valorMercado = 0;
                decimal puMercado = 0;
                foreach (OperacaoBolsa opBolsa in opBolsaColl)
                {

                    if (hsPuMercadoBolsa.Contains(opBolsa.CdAtivoBolsa))
                    {
                        puMercado = (decimal)hsPuMercadoBolsa[opBolsa.CdAtivoBolsa];
                    }
                    else
                    {
                        #region Calcula valor de Mercado
                        CotacaoBolsa cotacao = new CotacaoBolsa();
                        cotacao.BuscaCotacaoBolsa(opBolsa.CdAtivoBolsa, data);

                        decimal cotacaoDia;
                        if (tipoCotacao == (int)TipoCotacaoBolsa.Medio)
                        {
                            if (!cotacao.es.HasData || !cotacao.PUMedio.HasValue)
                            {
                                cotacao = new CotacaoBolsa();
                                cotacao.PUMedio = 0;
                            }
                            cotacaoDia = cotacao.PUMedio.Value;
                        }
                        else
                        {
                            if (!cotacao.es.HasData || !cotacao.PUFechamento.HasValue)
                            {
                                cotacao = new CotacaoBolsa();
                                cotacao.PUFechamento = 0;
                            }
                            cotacaoDia = cotacao.PUFechamento.Value;
                        }

                        //Se a cotação vier zerada, busca a última disponível (para a situação de feriado na Bovespa)
                        if (cotacaoDia == 0)
                        {
                            if (emiteMsgSemCotacaoBolsa)
                            {
                                throw new Exception("Cotação não disponível ou zerada para o ativo " + opBolsa.CdAtivoBolsa + " na data " + data.ToShortDateString() + ".");
                            }
                            else
                            {
                                CotacaoBolsa cotacaoAnterior = new CotacaoBolsa();
                                if (cotacaoAnterior.BuscaUltimaCotacaoBolsa(opBolsa.CdAtivoBolsa, data))
                                {
                                    if (tipoCotacao == (int)TipoCotacaoBolsa.Medio)
                                    {
                                        cotacaoDia = cotacaoAnterior.PUMedio.Value;
                                    }
                                    else if (cotacaoAnterior.PUFechamento.HasValue)
                                    {
                                        cotacaoDia = cotacaoAnterior.PUFechamento.Value;
                                    }
                                }
                            }
                        }

                        fator.BuscaFatorCotacaoBolsa(opBolsa.CdAtivoBolsa, data);

                        puMercado = cotacaoDia / (decimal)fator.Fator;
                        hsPuMercadoBolsa.Add(opBolsa.CdAtivoBolsa, puMercado);
                        #endregion
                    }

                    valorMercado = Utilitario.Truncate(puMercado * opBolsa.Quantidade.Value, 2);
                    valorMovimentado += (opBolsa.TipoOperacao.Equals(TipoOperacaoBolsa.RetiradaAtivoImpactoQtde.ToString()) ? valorMercado * (-1) : valorMercado);

                }
            }
            #endregion

            #region Fundo
            OperacaoFundoQuery opFundoQuery = new OperacaoFundoQuery("opFundo");
            OperacaoFundoCollection opFundoColl = new OperacaoFundoCollection();
            HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("historicoCota");
            opFundoQuery.Select(opFundoQuery.TipoOperacao, opFundoQuery.Quantidade.Sum(), historicoCotaQuery.CotaFechamento);
            opFundoQuery.InnerJoin(historicoCotaQuery).On(historicoCotaQuery.IdCarteira.Equal(opFundoQuery.IdCarteira) &
                                                          historicoCotaQuery.Data.Equal(opFundoQuery.DataOperacao));
            opFundoQuery.Where(opFundoQuery.IdCliente.Equal(idCarteira) &
                                   opFundoQuery.DataOperacao.Equal(data) &
                                   opFundoQuery.TipoOperacao.In((byte)TipoOperacaoFundo.IngressoAtivoImpactoQtde, (byte)TipoOperacaoFundo.RetiradaAtivoImpactoQtde));
            opFundoQuery.GroupBy(opFundoQuery.TipoOperacao, historicoCotaQuery.CotaFechamento);

            if (opFundoColl.Load(opFundoQuery))
            {
                foreach (OperacaoFundo opFundo in opFundoColl)
                {
                    decimal valorOperacao = Convert.ToDecimal(opFundo.GetColumn(HistoricoCotaMetadata.ColumnNames.CotaFechamento)) * opFundo.Quantidade.Value;

                    if (!string.IsNullOrEmpty(carteira.TruncaFinanceiro) && carteira.TruncaFinanceiro.Equals("S"))                    
                        valorOperacao = Utilitario.Truncate(valorOperacao, 2);                    
                    else                    
                        valorOperacao = Math.Round(valorOperacao, 2);                    

                    valorMovimentado += (opFundo.TipoOperacao.Equals((byte)TipoOperacaoFundo.RetiradaAtivoImpactoQtde) ? valorOperacao * (-1) : valorOperacao);
                }
            }
            #endregion

            return valorMovimentado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="valorPLAnterior"></param>
        /// <param name="historico"></param>
        /// <param name="filtraCarteira"></param>
        /// <param name="tipoCadastroAdmin"></param>
        /// <returns></returns>
        private AnaliseResultados RetornaAnaliseResultadosGestaoAdmin(int idCarteira, DateTime data, DateTime dataHistorico, decimal valorPLAnterior, bool historico, bool carteiraConsolidadora, TipoCadastroAdministracao tipoCadastroAdmin)
        {
            int tipo = (int)tipoCadastroAdmin;

            string descricao = "Taxa";
            int tipoMercado = (int)TiposMercados.TaxaAdm;
            if (TipoCadastroAdministracao.TaxaAdministracao == tipoCadastroAdmin)
            {
                descricao += " Administração";
                tipoMercado = (int)TiposMercados.TaxaAdm;
            }
            else if (TipoCadastroAdministracao.TaxaGestao == tipoCadastroAdmin)
            {
                descricao += " Gestão";
                tipoMercado = (int)TiposMercados.TaxaGestao;
            }

            string calculoAdminHist = "calculoAdminHist";
            string calculoAdmin = "calculoAdmin";
            string tabelaAdmin = "tabelaAdmin";
            string cadastroAmin = "cadastroAmin";
            CalculoAdministracaoHistorico calculoAdministracaoHistorico = new CalculoAdministracaoHistorico();
            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
            CalculoAdministracaoHistoricoQuery calculoAdministracaoHistoricoQuery = new CalculoAdministracaoHistoricoQuery(calculoAdminHist);
            CalculoAdministracaoQuery calculoAdministracaoQuery = new CalculoAdministracaoQuery(calculoAdmin);
            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery(tabelaAdmin);
            CadastroTaxaAdministracaoQuery cadastroTaxaAdministracaoQuery = new CadastroTaxaAdministracaoQuery(cadastroAmin);

            AnaliseResultados analiseResultados = new AnaliseResultados();

            #region Taxa Adm
            if (historico || carteiraConsolidadora)
            {
                calculoAdministracaoHistoricoQuery = new CalculoAdministracaoHistoricoQuery(calculoAdminHist);
                calculoAdministracaoQuery = new CalculoAdministracaoQuery(calculoAdmin);
                tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery(tabelaAdmin);
                cadastroTaxaAdministracaoQuery = new CadastroTaxaAdministracaoQuery(cadastroAmin);
                calculoAdministracaoHistoricoQuery.Select(calculoAdministracaoHistoricoQuery.ValorDia.Sum());
                calculoAdministracaoHistoricoQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(calculoAdministracaoHistoricoQuery.IdTabela.Equal(tabelaTaxaAdministracaoQuery.IdTabela));
                calculoAdministracaoHistoricoQuery.InnerJoin(cadastroTaxaAdministracaoQuery).On(tabelaTaxaAdministracaoQuery.IdCadastro.Equal(cadastroTaxaAdministracaoQuery.IdCadastro));

                calculoAdministracaoHistoricoQuery.Where(calculoAdministracaoHistoricoQuery.DataHistorico.LessThanOrEqual(data) &
                                                         calculoAdministracaoHistoricoQuery.DataHistorico.GreaterThan(dataHistorico) &
                                                         cadastroTaxaAdministracaoQuery.Tipo.Equal(tipo));

                if (!carteiraConsolidadora)
                    calculoAdministracaoHistoricoQuery.Where(calculoAdministracaoHistoricoQuery.IdCarteira.Equal(idCarteira));

                calculoAdministracaoHistorico = new CalculoAdministracaoHistorico();
                calculoAdministracaoHistorico.Load(calculoAdministracaoHistoricoQuery);

                if (calculoAdministracaoHistorico.ValorDia.HasValue)
                {
                    decimal valorAdm = calculoAdministracaoHistorico.ValorDia.Value * -1;

                    analiseResultados = new AnaliseResultados();
                    analiseResultados.TipoMercado = tipoMercado;
                    analiseResultados.DescricaoAtivo = "Valor apropriado no período";
                    analiseResultados.DescricaoMercado = descricao;
                    analiseResultados.PercentualPL = Math.Round(valorAdm / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = valorAdm;                   

                }
            }
            else
            {
                calculoAdministracaoHistoricoQuery = new CalculoAdministracaoHistoricoQuery(calculoAdminHist);
                calculoAdministracaoQuery = new CalculoAdministracaoQuery(calculoAdmin);
                tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery(tabelaAdmin);
                cadastroTaxaAdministracaoQuery = new CadastroTaxaAdministracaoQuery(cadastroAmin);
                calculoAdministracaoQuery.Select(calculoAdministracaoQuery.ValorDia.Sum());
                calculoAdministracaoQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(calculoAdministracaoQuery.IdTabela.Equal(tabelaTaxaAdministracaoQuery.IdTabela));
                calculoAdministracaoQuery.InnerJoin(cadastroTaxaAdministracaoQuery).On(tabelaTaxaAdministracaoQuery.IdCadastro.Equal(cadastroTaxaAdministracaoQuery.IdCadastro));
                calculoAdministracaoQuery.Where(cadastroTaxaAdministracaoQuery.Tipo.Equal(tipo) &
                                                calculoAdministracaoQuery.IdCarteira.Equal(idCarteira));

                calculoAdministracao = new CalculoAdministracao();
                calculoAdministracao.Load(calculoAdministracaoQuery);

                decimal valorAdm = 0;
                if (calculoAdministracao.ValorDia.HasValue)
                {
                    valorAdm += calculoAdministracao.ValorDia.Value;
                }

                calculoAdministracaoHistoricoQuery = new CalculoAdministracaoHistoricoQuery(calculoAdminHist);
                calculoAdministracaoQuery = new CalculoAdministracaoQuery(calculoAdmin);
                tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery(tabelaAdmin);
                cadastroTaxaAdministracaoQuery = new CadastroTaxaAdministracaoQuery(cadastroAmin);
                calculoAdministracaoHistoricoQuery.Select(calculoAdministracaoHistoricoQuery.ValorDia.Sum());
                calculoAdministracaoHistoricoQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(calculoAdministracaoHistoricoQuery.IdTabela.Equal(tabelaTaxaAdministracaoQuery.IdTabela));
                calculoAdministracaoHistoricoQuery.InnerJoin(cadastroTaxaAdministracaoQuery).On(tabelaTaxaAdministracaoQuery.IdCadastro.Equal(cadastroTaxaAdministracaoQuery.IdCadastro));

                calculoAdministracaoHistoricoQuery.Where(calculoAdministracaoHistoricoQuery.DataHistorico.LessThan(data) &
                                                         calculoAdministracaoHistoricoQuery.DataHistorico.GreaterThan(dataHistorico) &
                                                         cadastroTaxaAdministracaoQuery.Tipo.Equal(tipo) &
                                                         calculoAdministracaoHistoricoQuery.IdCarteira.Equal(idCarteira));

                calculoAdministracaoHistorico = new CalculoAdministracaoHistorico();
                calculoAdministracaoHistorico.Load(calculoAdministracaoHistoricoQuery);

                if (calculoAdministracaoHistorico.ValorDia.HasValue)
                {
                    valorAdm += calculoAdministracaoHistorico.ValorDia.Value;
                }

                if (valorAdm > 0)
                {
                    valorAdm *= -1;

                    analiseResultados = new AnaliseResultados();
                    analiseResultados.TipoMercado = tipoMercado;
                    analiseResultados.DescricaoAtivo = "Valor apropriado no período";
                    analiseResultados.DescricaoMercado = descricao;
                    analiseResultados.PercentualPL = Math.Round(valorAdm / valorPLAnterior * 100M, 4);
                    analiseResultados.ResultadoPeriodo = valorAdm;
                }
            }
            #endregion

            return analiseResultados;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="valorPLAnterior"></param>
        /// <param name="historico"></param>
        /// <param name="carteiraConsolidadora"></param>
        /// <param name="tipoCalculo"></param>
        /// <returns></returns>
        private List<AnaliseResultados> RetornaAnaliseResultadosProvisao(int idCarteira, DateTime data, DateTime dataHistorico, decimal valorPLAnterior, bool historico, bool carteiraConsolidadora, TipoCalculoProvisao tipoCalculo)
        {
            List<AnaliseResultados> lista = new List<AnaliseResultados>();
            TiposMercados tipoMercado = TipoCalculoProvisao.Provisao == tipoCalculo ? TiposMercados.Provisoes : TiposMercados.Diferimento;
            Dictionary<int, AnaliseProvisao> lstAnaliseProvisao = new Dictionary<int, AnaliseProvisao>();

            TabelaProvisaoCollection tabelaProvisaoCollection = new TabelaProvisaoCollection();

            tabelaProvisaoCollection.Query.Where(tabelaProvisaoCollection.Query.TipoCalculo.Equal(tipoCalculo) & tabelaProvisaoCollection.Query.DataFim.GreaterThan(data));

            if (!carteiraConsolidadora)
                tabelaProvisaoCollection.Query.Where(tabelaProvisaoCollection.Query.IdCarteira.Equal(idCarteira));

            tabelaProvisaoCollection.Query.Load();

            if (historico || carteiraConsolidadora)
            {
                CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
                CalculoProvisaoHistoricoQuery calculoProvisaoHistQuery = new CalculoProvisaoHistoricoQuery("calcProv");
                TabelaProvisaoQuery tabelaProvisaoQuery = new TabelaProvisaoQuery("tabProv");
                CadastroProvisaoQuery cadastroProvisaoQuery = new CadastroProvisaoQuery("cadstProv");

                calculoProvisaoHistQuery.Select(calculoProvisaoHistQuery.ValorAcumulado,
                                                calculoProvisaoHistQuery.IdTabela,
                                                calculoProvisaoHistQuery.DataHistorico);
                calculoProvisaoHistQuery.InnerJoin(tabelaProvisaoQuery).On(tabelaProvisaoQuery.IdTabela.Equal(calculoProvisaoHistQuery.IdTabela));
                calculoProvisaoHistQuery.Where(tabelaProvisaoQuery.TipoCalculo.Equal(tipoCalculo) &
                                               tabelaProvisaoQuery.DataFim.GreaterThanOrEqual(data) &
                                               calculoProvisaoHistQuery.DataHistorico.LessThanOrEqual(data) &
                                               calculoProvisaoHistQuery.DataHistorico.GreaterThanOrEqual(dataHistorico));

                if (!carteiraConsolidadora)
                    calculoProvisaoHistQuery.Where(calculoProvisaoHistQuery.IdCarteira.Equal(idCarteira));

                calculoProvisaoHistQuery.OrderBy(calculoProvisaoHistQuery.IdTabela.Ascending, calculoProvisaoHistQuery.DataHistorico.Ascending);

                List<CalculoProvisaoHistorico> lstCalculoProvisaoHistorico = new List<CalculoProvisaoHistorico>();
                if (calculoProvisaoHistoricoCollection.Load(calculoProvisaoHistQuery))
                    lstCalculoProvisaoHistorico = (List<CalculoProvisaoHistorico>)calculoProvisaoHistoricoCollection;

                foreach (TabelaProvisao tabelaProvisao in tabelaProvisaoCollection)
                {
                    int idTabela = tabelaProvisao.IdTabela.Value;

                    if (!lstAnaliseProvisao.ContainsKey(idTabela))
                    {
                        //Busca o registro mais novo e o mais antigo
                        CalculoProvisaoHistorico calculoMaior = lstCalculoProvisaoHistorico.FindLast(delegate(CalculoProvisaoHistorico x) { return x.IdTabela == idTabela && x.DataHistorico <= data; });
                        CalculoProvisaoHistorico calculoMenor = lstCalculoProvisaoHistorico.Find(delegate(CalculoProvisaoHistorico x) { return x.IdTabela == idTabela && x.DataHistorico >= dataHistorico; });

                        AnaliseProvisao analiseProvisao = new AnaliseProvisao();

                        analiseProvisao.IdTabela = idTabela;

                        if (calculoMaior != null && calculoMaior.ValorAcumulado.HasValue)
                            analiseProvisao.Valor = calculoMaior.ValorAcumulado.GetValueOrDefault(0);

                        if (calculoMenor != null && calculoMenor.ValorAcumulado.HasValue)
                        {
                            if (calculoMaior != null && calculoMenor.DataHistorico.Value != calculoMaior.DataHistorico.Value)
                                analiseProvisao.Valor -= calculoMenor.ValorAcumulado.GetValueOrDefault(0);

                        }

                        analiseProvisao.Descricao = tabelaProvisao.UpToCadastroProvisaoByIdCadastro.Descricao;

                        lstAnaliseProvisao.Add(idTabela, analiseProvisao);
                    }
                }
            }
            else
            {
                CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
                CalculoProvisaoCollection calculoProvisaoCollection = new CalculoProvisaoCollection();
                CalculoProvisaoQuery calculoProvisaoQuery = new CalculoProvisaoQuery("calcProv");
                CalculoProvisaoHistoricoQuery calculoProvisaoHistQuery = new CalculoProvisaoHistoricoQuery("calcProv");
                TabelaProvisaoQuery tabelaProvisaoQuery = new TabelaProvisaoQuery("tabProv");
                CadastroProvisaoQuery cadastroProvisaoQuery = new CadastroProvisaoQuery("cadstProv");
                List<CalculoProvisao> lstCalculoProvisao = new List<CalculoProvisao>();
                List<CalculoProvisaoHistorico> lstCalculoProvisaoHistorico = new List<CalculoProvisaoHistorico>();

                calculoProvisaoQuery.Select(calculoProvisaoQuery.ValorAcumulado,
                                            calculoProvisaoQuery.IdTabela);
                calculoProvisaoQuery.InnerJoin(tabelaProvisaoQuery).On(tabelaProvisaoQuery.IdTabela.Equal(calculoProvisaoQuery.IdTabela));                
                calculoProvisaoQuery.Where(tabelaProvisaoQuery.TipoCalculo.Equal(tipoCalculo) &
                                           calculoProvisaoQuery.DataFimApropriacao.GreaterThanOrEqual(data));

                if (!carteiraConsolidadora)
                    calculoProvisaoQuery.Where(calculoProvisaoQuery.IdCarteira.Equal(idCarteira));

                if (calculoProvisaoCollection.Load(calculoProvisaoQuery))
                    lstCalculoProvisao = (List<CalculoProvisao>)calculoProvisaoCollection;

                calculoProvisaoHistQuery = new CalculoProvisaoHistoricoQuery("calcProv");
                tabelaProvisaoQuery = new TabelaProvisaoQuery("tabProv");
                cadastroProvisaoQuery = new CadastroProvisaoQuery("cadstProv");

                calculoProvisaoHistQuery.Select(calculoProvisaoHistQuery.ValorAcumulado,
                                                calculoProvisaoHistQuery.IdTabela,
                                                calculoProvisaoHistQuery.DataHistorico);
                calculoProvisaoHistQuery.InnerJoin(tabelaProvisaoQuery).On(tabelaProvisaoQuery.IdTabela.Equal(calculoProvisaoHistQuery.IdTabela));                
                calculoProvisaoHistQuery.Where(calculoProvisaoHistQuery.DataHistorico.LessThan(data) &
                                               calculoProvisaoHistQuery.DataFimApropriacao.GreaterThanOrEqual(data) &
                                               calculoProvisaoHistQuery.DataHistorico.GreaterThanOrEqual(dataHistorico) &
                                               tabelaProvisaoQuery.TipoCalculo.Equal(tipoCalculo));

                if (!carteiraConsolidadora)
                    calculoProvisaoHistQuery.Where(calculoProvisaoHistQuery.IdCarteira.Equal(idCarteira));

                calculoProvisaoHistQuery.OrderBy(calculoProvisaoHistQuery.IdTabela.Ascending, calculoProvisaoHistQuery.DataHistorico.Ascending);

                if (calculoProvisaoHistoricoCollection.Load(calculoProvisaoHistQuery))
                    lstCalculoProvisaoHistorico = (List<CalculoProvisaoHistorico>)calculoProvisaoHistoricoCollection;

                foreach (TabelaProvisao tabelaProvisao in tabelaProvisaoCollection)
                {
                    int idTabela = tabelaProvisao.IdTabela.Value;

                    if (!lstAnaliseProvisao.ContainsKey(idTabela))
                    {
                        //Busca o registro mais novo e o mais antigo
                        CalculoProvisao calculoMaior = lstCalculoProvisao.Find(delegate(CalculoProvisao x) { return x.IdTabela == idTabela; });
                        CalculoProvisaoHistorico calculoMenor = lstCalculoProvisaoHistorico.Find(delegate(CalculoProvisaoHistorico x) { return x.IdTabela == idTabela && x.DataHistorico >= dataHistorico; });

                        AnaliseProvisao analiseProvisao = new AnaliseProvisao();

                        analiseProvisao.IdTabela = idTabela;

                        if (calculoMaior != null && calculoMaior.ValorAcumulado.HasValue)
                            analiseProvisao.Valor = calculoMaior.ValorAcumulado.GetValueOrDefault(0);

                        if (calculoMenor != null && calculoMenor.ValorAcumulado.HasValue)
                            analiseProvisao.Valor -= calculoMenor.ValorAcumulado.GetValueOrDefault(0);
                        
                        analiseProvisao.Descricao = tabelaProvisao.UpToCadastroProvisaoByIdCadastro.Descricao;

                        lstAnaliseProvisao.Add(idTabela, analiseProvisao);
                    }
                }
            }

            foreach (AnaliseProvisao analiseProvisao in lstAnaliseProvisao.Values)
            {
                decimal valorProvisao = analiseProvisao.Valor;

                string descricao = "Provisões";
                if (tipoCalculo == TipoCalculoProvisao.Diferido)
                    descricao = "Diferimentos";
                else
                    valorProvisao = Math.Abs(valorProvisao) * -1;

                AnaliseResultados analiseResultados = new AnaliseResultados();
                analiseResultados.TipoMercado = (int)tipoMercado;
                analiseResultados.DescricaoAtivo = analiseProvisao.Descricao;
                analiseResultados.DescricaoMercado = descricao;
                analiseResultados.PercentualPL = Math.Round(valorProvisao / valorPLAnterior * 100M, 4);
                analiseResultados.ResultadoPeriodo = valorProvisao;

                lista.Add(analiseResultados);
            }

            return lista;
        }

    }

}