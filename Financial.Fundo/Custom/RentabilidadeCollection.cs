/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/12/2014 15:20:07
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;

namespace Financial.Fundo
{
	public partial class RentabilidadeCollection : esRentabilidadeCollection
	{
        public void DeletaRentabilidadeMaiorIgual(int idCliente, DateTime data, TipoAtivoAuxiliar prazoMedioAtivo)
        {
            this.QueryReset();
            this.Query
                    .Select()
                    .Where(this.Query.IdCliente.Equal(idCliente) &
                           this.Query.Data.GreaterThanOrEqual(data) &
                           this.query.TipoAtivo.Equal((int)prazoMedioAtivo));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
	}
}
