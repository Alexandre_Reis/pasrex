﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;
using log4net;
using Financial.Util;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Tributo.Custom;
using Financial.Investidor;
using Financial.Fundo.Exceptions;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Fundo.Debug.HistoricoCota;
using Financial.Common;
using Financial.Common.Enums;

namespace Financial.Fundo {
    public partial class OperacaoFundo : esOperacaoFundo 
    {
        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="tipoCarteira"></param>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="idAgenteGestor"></param>
        /// <param name="idIndice"></param>
        /// <param name="idCarteira"></param>
        /// <returns></returns>
        public decimal RetornaValorEnquadra(int idCliente, DateTime data, int? tipoCarteira, int? idAgenteAdministrador,
                                                   int? idAgenteGestor, int? idIndice, int? idCarteira, int? idCategoria)
        {
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();

            operacaoFundoCollection.Query
                 .Select(operacaoFundoCollection.Query.IdCarteira, 
                         operacaoFundoCollection.Query.TipoOperacao,
                         operacaoFundoCollection.Query.ValorBruto.Sum(),
                         operacaoFundoCollection.Query.ValorLiquido.Sum(),
                         operacaoFundoCollection.Query.Quantidade.Sum())
                 .Where(operacaoFundoCollection.Query.IdCliente.Equal(idCliente),
                        operacaoFundoCollection.Query.DataConversao.GreaterThanOrEqual(data),
                        operacaoFundoCollection.Query.TipoOperacao.In((byte)TipoOperacaoFundo.Aplicacao,
                                                                      (byte)TipoOperacaoFundo.ResgateBruto,
                                                                      (byte)TipoOperacaoFundo.ResgateCotas,
                                                                      (byte)TipoOperacaoFundo.ResgateLiquido,
                                                                      (byte)TipoOperacaoFundo.ResgateTotal))
                .GroupBy(operacaoFundoCollection.Query.IdCarteira, 
                         operacaoFundoCollection.Query.TipoOperacao);

            operacaoFundoCollection.Query.Load();

            decimal totalValor = 0;

            for (int i = 0; i < operacaoFundoCollection.Count; i++)
            {
                OperacaoFundo operacaoFundo = operacaoFundoCollection[i];
                int idCarteiraCompara = operacaoFundo.IdCarteira.Value;
                byte tipoOperacao = operacaoFundo.TipoOperacao.Value;
                                

                bool passou = true;
                if (idCarteira.HasValue)
                {
                    if (idCarteiraCompara != idCarteira)
                        passou = false;
                }
                else if (tipoCarteira.HasValue || idAgenteAdministrador.HasValue || idAgenteGestor.HasValue ||
                         idIndice.HasValue || idCategoria.HasValue)
                {
                    #region Busca os campos de Carteira
                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.TipoCarteira);
                    campos.Add(carteira.Query.IdAgenteAdministrador);
                    campos.Add(carteira.Query.IdAgenteGestor);
                    campos.Add(carteira.Query.IdIndiceBenchmark);
                    campos.Add(carteira.Query.IdCategoria);
                    carteira.LoadByPrimaryKey(campos, idCarteiraCompara);

                    int tipoCarteiraCompara = carteira.TipoCarteira.Value;
                    int idAgenteAdministradorCompara = carteira.IdAgenteAdministrador.Value;
                    int idAgenteGestorCompara = carteira.IdAgenteGestor.Value;
                    int idIndiceCompara = carteira.IdIndiceBenchmark.Value;
                    int idCategoriaCompara = carteira.IdCategoria.Value;
                    #endregion

                    #region Verifica se passa pelos filtros de tipoCarteira, administrador, gestor e indice
                    if (tipoCarteira.HasValue)
                    {
                        if (tipoCarteiraCompara != tipoCarteira)
                            passou = false;
                    }

                    if (idAgenteAdministrador.HasValue)
                    {
                        if (idAgenteAdministradorCompara != idAgenteAdministrador)
                            passou = false;
                    }

                    if (idAgenteGestor.HasValue)
                    {
                        if (idAgenteGestorCompara != idAgenteGestor)
                            passou = false;
                    }


                    if (idIndice.HasValue)
                    {
                        if (idIndiceCompara != idIndice)
                            passou = false;
                    }

                    if (idCategoria.HasValue)
                    {
                        if (idCategoriaCompara != idCategoria)
                            passou = false;
                    }
                    #endregion

                }

                if (passou)
                {
                    decimal quantidadePosicao = 0;
                    decimal valorPosicao = 0;
                    if (tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas || tipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal)
                    {
                        PosicaoFundoAbertura posicaoFundoAbertura = new PosicaoFundoAbertura();
                        posicaoFundoAbertura.Query.Select(posicaoFundoAbertura.Query.Quantidade.Sum(),
                                                          posicaoFundoAbertura.Query.ValorBruto.Sum());
                        posicaoFundoAbertura.Query.Where(posicaoFundoAbertura.Query.IdCliente.Equal(idCliente),
                                                         posicaoFundoAbertura.Query.IdCarteira.Equal(idCarteiraCompara),
                                                         posicaoFundoAbertura.Query.DataHistorico.Equal(data));
                        posicaoFundoAbertura.Query.Load();

                        quantidadePosicao = posicaoFundoAbertura.Quantidade.HasValue ? posicaoFundoAbertura.Quantidade.Value : 0;
                        valorPosicao = posicaoFundoAbertura.ValorBruto.HasValue ? posicaoFundoAbertura.ValorBruto.Value : 0;
                    }

                    decimal valor = 0;
                    if (tipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
                    {
                        valor = operacaoFundo.ValorBruto.Value;
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto)
                    {
                        valor = operacaoFundo.ValorBruto.Value * -1;
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido)
                    {
                        valor = operacaoFundo.ValorLiquido.Value * -1;
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas)
                    {
                        valor = Math.Round(((operacaoFundo.Quantidade.Value / quantidadePosicao) * valorPosicao) * -1, 2);
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas)
                    {
                        valor = valorPosicao * -1;
                    }

                    totalValor += valor;
                }
                
            }

            return totalValor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="tipoOperacao"></param>
        /// <returns>soma o total operado</returns>
        public decimal RetornaTotalOperacao(int idCliente, DateTime dataOperacao, TipoOperacaoFundo tipoOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.query.TipoOperacao.Equal(tipoOperacao));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Realiza o processo de conversão do valor aplicado em quantidade de cotas (ou vice-versa)
        /// para as aplicações que estiverem cotizando na data.        
        /// Atualiza a OperacaoFundo e cria uma nova PosicaoFundo.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaAplicacao(int idCliente, DateTime data) 
        {
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            List<byte> lista = new List<byte>();
            lista.Add((byte)FonteOperacaoFundo.Manual);
            lista.Add((byte)FonteOperacaoFundo.OrdemFundo);
            lista.Add((byte)FonteOperacaoFundo.YMFComCalculo);
            operacaoFundoCollection.BuscaAplicacao(idCliente, data, lista);

            for (int i = 0; i < operacaoFundoCollection.Count; i++) {
                OperacaoFundo operacaoFundo = operacaoFundoCollection[i];
                int tipoOperacao = operacaoFundo.TipoOperacao.Value;
                int idCarteira = operacaoFundo.IdCarteira.Value;

                #region Busca tipo de cota e parâmetros de truncagem e nr de casas (quantidade e financeiro)
                Carteira carteira = new Carteira();

                List<esQueryItem> fieldsToReturn = new List<esQueryItem>();
                fieldsToReturn.Add(carteira.Query.TipoCota);
                fieldsToReturn.Add(carteira.Query.TruncaQuantidade);
                fieldsToReturn.Add(carteira.Query.TruncaFinanceiro);
                fieldsToReturn.Add(carteira.Query.CasasDecimaisQuantidade);

                carteira.LoadByPrimaryKey(fieldsToReturn, idCarteira);

                int tipoCota = carteira.TipoCota.Value;
                string truncaQuantidade = carteira.TruncaQuantidade;
                string truncaFinanceiro = carteira.TruncaFinanceiro;
                int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
                #endregion

                decimal cotaAplicacao;
                #region Busca o valor da cota na data (abertura ou fechamento)
                if (operacaoFundo.CotaInformada.HasValue)
                {
                    cotaAplicacao = operacaoFundo.CotaInformada.Value;
                }
                else
                {
                    
                    HistoricoCota historicoCota = new HistoricoCota();
                    historicoCota.BuscaValorCotaDia(idCarteira, operacaoFundo.DataConversao.Value);


                    if (tipoCota == (int)TipoCotaFundo.Abertura)
                    {
                        cotaAplicacao = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        cotaAplicacao = historicoCota.CotaFechamento.Value;
                    }

                    if (cotaAplicacao == 0)
                    {
                        throw new HistoricoCotaNaoCadastradoException("Cota zerada na data " + operacaoFundo.DataConversao + " para a carteira " + idCarteira);
                    }
                }
                #endregion

                #region Converte financeiro em quantidade ou quantidade em financeiro
                decimal valorBruto;
                decimal quantidade;
                if (tipoOperacao == (int)TipoOperacaoFundo.Aplicacao ||
                    tipoOperacao == (int)TipoOperacaoFundo.AplicacaoAcoesEspecial) {
                    valorBruto = operacaoFundo.ValorBruto.Value;

                    if (truncaQuantidade == "S") {
                        quantidade = Utilitario.Truncate(valorBruto / cotaAplicacao, casasDecimaisQuantidade);
                    }
                    else {
                        quantidade = Math.Round(valorBruto / cotaAplicacao, casasDecimaisQuantidade);
                    }
                }
                else {
                    quantidade = operacaoFundo.Quantidade.Value;

                    if (truncaFinanceiro == "S") {
                        valorBruto = Utilitario.Truncate(quantidade * cotaAplicacao, 2);
                    }
                    else {
                        valorBruto = Math.Round(quantidade * cotaAplicacao, 2);
                    }
                }
                #endregion

                #region Insert em PosicaoFundo
                PosicaoFundo posicaoFundo = new PosicaoFundo();
                posicaoFundo.IdOperacao = operacaoFundo.IdOperacao.Value;
                posicaoFundo.IdCliente = operacaoFundo.IdCliente.Value;
                posicaoFundo.IdCarteira = operacaoFundo.IdCarteira.Value;
                posicaoFundo.ValorAplicacao = valorBruto;
                posicaoFundo.DataAplicacao = operacaoFundo.DataOperacao;
                posicaoFundo.DataConversao = operacaoFundo.DataConversao;
                posicaoFundo.CotaAplicacao = cotaAplicacao;
                posicaoFundo.CotaDia = cotaAplicacao;
                posicaoFundo.ValorBruto = valorBruto;
                posicaoFundo.ValorLiquido = valorBruto;
                posicaoFundo.QuantidadeInicial = quantidade;
                posicaoFundo.Quantidade = quantidade;
                posicaoFundo.QuantidadeAntesCortes = quantidade;
                posicaoFundo.DataUltimaCobrancaIR = operacaoFundo.DataConversao; //Artificio para início de posição                
                posicaoFundo.ValorRendimento = 0;
                posicaoFundo.PosicaoIncorporada = "N";
                posicaoFundoCollection.AttachEntity(posicaoFundo);
                posicaoFundo.FieTabelaIr = operacaoFundo.FieTabelaIr;
                #endregion

                operacaoFundo.Quantidade = quantidade;
                operacaoFundo.ValorBruto = valorBruto;
                operacaoFundo.ValorLiquido = valorBruto;
                operacaoFundo.CotaOperacao = cotaAplicacao;
            }

            //Salva as collections
            operacaoFundoCollection.Save();
            posicaoFundoCollection.Save();           
        }

        /// <summary>
        /// Lança em Liquidacao todas as aplicações consolidadas liquidando no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void LancaCCAplicacao(int idCliente, DateTime data) 
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdTipo);
            cliente.LoadByPrimaryKey(campos, idCliente);

            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            if (cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaFisica ||
                cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaJuridica)
            {
                operacaoFundoCollection.BuscaAplicacaoLiquidaDiaAberto(idCliente, data);
            }
            else
            {
                operacaoFundoCollection.BuscaAplicacaoLiquidaDia(idCliente, data);
            }

            for (int i = 0; i < operacaoFundoCollection.Count; i++) 
            {
                OperacaoFundo operacaoFundo = operacaoFundoCollection[i];
                decimal valor = operacaoFundo.ValorBruto.Value;
                int idCarteira = operacaoFundo.IdCarteira.Value;

                Cliente clienteCarteira = new Cliente();
                campos = new List<esQueryItem>();
                campos.Add(clienteCarteira.Query.IdMoeda);
                campos.Add(clienteCarteira.Query.Apelido);
                clienteCarteira.LoadByPrimaryKey(campos, idCarteira);

                int idMoeda = clienteCarteira.IdMoeda.Value;
                string apelidoCarteira = clienteCarteira.Apelido;
                
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                //                 

                #region Insert em Liquidacao
                string descricao = "Aplicações do dia - " + apelidoCarteira;

                if (cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaFisica ||
                    cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaJuridica)
                {
                    if (!String.IsNullOrEmpty(operacaoFundo.Observacao) && operacaoFundo.Observacao != "")
                    {
                        descricao = descricao + " - " + operacaoFundo.Observacao;
                    }
                }

                Liquidacao liquidacao = new Liquidacao();
                liquidacao.IdCliente = idCliente;
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = data;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valor * -1;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Fundo.Aplicacao;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoFundo.IdConta.HasValue && operacaoFundo.IdConta.Value > 0 ? operacaoFundo.IdConta.Value : idContaDefault;
                liquidacaoCollection.AttachEntity(liquidacao);
                #endregion
            }


            //Checa a cota do cliente (aplicador nos fundos)
            Carteira carteiraAplicador = new Carteira();
            List<esQueryItem> camposCarteiraAplicador = new List<esQueryItem>();
            camposCarteiraAplicador.Add(carteiraAplicador.Query.TipoCota);
            if (carteiraAplicador.LoadByPrimaryKey(camposCarteiraAplicador, idCliente))
            {
                int tipoCota = carteiraAplicador.TipoCota.Value;
                //

                if (tipoCota == (byte)TipoCotaFundo.Fechamento)
                {
                    operacaoFundoCollection = new OperacaoFundoCollection();
                    operacaoFundoCollection.BuscaAplicacaoConverter(idCliente, data);

                    for (int i = 0; i < operacaoFundoCollection.Count; i++)
                    {
                        OperacaoFundo operacaoFundo = operacaoFundoCollection[i];
                        decimal valor = operacaoFundo.ValorBruto.Value;
                        DateTime dataConversao = operacaoFundo.DataConversao.Value;
                        int idCarteira = operacaoFundo.IdCarteira.Value;

                        Cliente clienteCarteira = new Cliente();
                        campos = new List<esQueryItem>();
                        campos.Add(clienteCarteira.Query.IdMoeda);
                        campos.Add(clienteCarteira.Query.Apelido);
                        clienteCarteira.LoadByPrimaryKey(campos, idCarteira);
                        int idMoeda = clienteCarteira.IdMoeda.Value;
                        string apelidoCarteira = clienteCarteira.Apelido;

                        //Busca o idContaDefault do cliente
                        Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                        int idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);

                        DateTime dataConversaoAnterior = Calendario.SubtraiDiaUtil(dataConversao, 1);

                        #region Insert em Liquidacao (Valores a converter em cotas) - contrapartida contábil
                        string descricao = "Valores a converter em cotas - " + apelidoCarteira;

                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.IdCliente = idCliente;
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataConversaoAnterior;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = valor;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Compensacao;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Fundo.AplicacaoConverter;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoFundo.IdConta.HasValue && operacaoFundo.IdConta.Value > 0 ? operacaoFundo.IdConta.Value : idContaDefault;
                        liquidacaoCollection.AttachEntity(liquidacao);
                        #endregion
                    }
                }
            }

            //Salva a collection de Liquidacao
            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Lança em Liquidacao o projetado de todos os resgates (valor resgate, IR, IOF, Pfee) com conversão na data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataConversao"></param>
        public void LancaCCResgate(int idCliente, DateTime dataConversao) 
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdTipo);
            cliente.LoadByPrimaryKey(campos, idCliente);

            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            if (cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaFisica ||
                cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaJuridica)
            {
                operacaoFundoCollection.BuscaResgateProjetaLiquidacaoAberto(idCliente, dataConversao);
            }
            else
            {
                operacaoFundoCollection.BuscaResgateProjetaLiquidacao(idCliente, dataConversao);
            }

            for (int i = 0; i < operacaoFundoCollection.Count; i++) 
            {
                OperacaoFundo operacaoFundo = operacaoFundoCollection[i];
                decimal valor = operacaoFundo.ValorBruto.Value;
                decimal valorIR = operacaoFundo.ValorIR.Value;
                decimal valorIOF = operacaoFundo.ValorIOF.Value;
                decimal valorPerformance = operacaoFundo.ValorPerformance.Value;
                                
                DateTime dataLiquidacao = operacaoFundo.DataLiquidacao.Value;
                int idCarteira = operacaoFundo.IdCarteira.Value;

                Cliente clienteCarteira = new Cliente();
                campos = new List<esQueryItem>();
                campos.Add(clienteCarteira.Query.IdMoeda);
                campos.Add(clienteCarteira.Query.Apelido);
                clienteCarteira.LoadByPrimaryKey(campos, idCarteira);

                int idMoeda = clienteCarteira.IdMoeda.Value;
                string apelidoCarteira = clienteCarteira.Apelido;

                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                //                 

                int idConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoFundo.IdConta.HasValue && operacaoFundo.IdConta.Value > 0 ? operacaoFundo.IdConta.Value : idContaDefault;

                #region Insert em Liquidacao (Valor Bruto)
                string descricao = "Resgate - " + apelidoCarteira;

                if (cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaFisica ||
                    cliente.IdTipo.Value == TipoClienteFixo.ClientePessoaJuridica)
                {
                    if (!String.IsNullOrEmpty(operacaoFundo.Observacao) && operacaoFundo.Observacao != "")
                    {
                        descricao = descricao + " - " + operacaoFundo.Observacao;
                    }
                }

                Liquidacao liquidacao = new Liquidacao();
                liquidacao.IdCliente = idCliente;
                liquidacao.DataLancamento = dataConversao;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao + " (Qtde - " + operacaoFundo.Quantidade.Value + ")";
                liquidacao.Valor = valor;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Fundo.Resgate;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdConta = idConta;
                liquidacaoCollection.AttachEntity(liquidacao);
                #endregion

                #region Insert em Liquidacao (Valor IR)
                if (valorIR != 0) {
                    descricao = "IR s/ Resgates a pagar - " + apelidoCarteira;

                    liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCliente;
                    liquidacao.DataLancamento = dataConversao;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorIR * -1;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Fundo.IRResgate;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idConta;
                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                #endregion

                #region Insert em Liquidacao (Valor IOF)
                if (valorIOF != 0) {
                    descricao = "IOF s/ Resgates a pagar - " + apelidoCarteira;

                    liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCliente;
                    liquidacao.DataLancamento = dataConversao;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorIOF * -1;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Fundo.IOFResgate;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idConta;
                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                #endregion

                #region Insert em Liquidacao (Valor Performance)
                if (valorPerformance != 0) {
                    descricao = "Performance s/ Resgates a pagar - " + apelidoCarteira;

                    liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCliente;
                    liquidacao.DataLancamento = dataConversao;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorPerformance * -1;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Fundo.PfeeResgate;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idConta;
                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                #endregion
            }

            //Salva a collection de Liquidacao
            liquidacaoCollection.Save();            
        }

        /// <summary>
        /// Método principal para processamento de resgates com conversão na data. 
        /// Calcula os tributos (IR, IOF) e eventual pfee sobre os resgates.        
        /// Baixa as posições de cotas em fundos.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaResgate(int idCliente, DateTime data) {
            OperacaoCotista operacaoCotista = new OperacaoCotista();
            operacaoCotista.ProcessaResgate(idCliente, data, TipoProcessaResgate.OperacaoFundo);            
        }
        
        /// <summary>
        /// Retorna o total de valor bruto operado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="tipoOperacao"></param>
        /// <returns>soma o total operado</returns>
        public decimal RetornaTotalAplicacao(int idCliente, int idCarteira, DateTime dataInicio, DateTime dataFim) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataConversao.Between(dataInicio, dataFim),
                        this.query.TipoOperacao.In((byte)TipoOperacaoFundo.Aplicacao,
                                                   (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna o total de valor bruto operado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="tipoOperacao"></param>
        /// <returns>soma o total operado</returns>
        public decimal RetornaTotalResgate(int idCliente, int idCarteira, DateTime dataInicio, DateTime dataFim) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataConversao.Between(dataInicio, dataFim),
                        this.query.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateBruto,
                                                   (byte)TipoOperacaoFundo.ResgateCotas,
                                                   (byte)TipoOperacaoFundo.ResgateLiquido,
                                                   (byte)TipoOperacaoFundo.ResgateTotal,
                                                   (byte)TipoOperacaoFundo.ComeCotas));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Transfere todas as operações de OperacaoFundo para OperacaoCotista.
        /// Deleta antes as operações em OperacaoCotista (filtrando IdCotista != IdCarteira).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void TransfereOperacaoCotista(int idCliente, DateTime data)
        {
            OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
            operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCotista.Equal(idCliente),
                                                         operacaoCotistaCollectionDeletar.Query.DataOperacao.Equal(data),
                                                         operacaoCotistaCollectionDeletar.Query.IdCotista.NotEqual(operacaoCotistaCollectionDeletar.Query.IdCarteira));
            operacaoCotistaCollectionDeletar.Query.Load();
            operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
            operacaoCotistaCollectionDeletar.Save();

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();

            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoFundoCollection.Query.DataOperacao.Equal(data));
            operacaoFundoCollection.Query.Load();

            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
            {
                OperacaoCotista operacaoCotista = operacaoCotistaCollection.AddNew();
                operacaoCotista.CotaOperacao = operacaoFundo.CotaOperacao;
                operacaoCotista.DataAgendamento = operacaoFundo.DataAgendamento;
                operacaoCotista.DataConversao = operacaoFundo.DataConversao;
                operacaoCotista.DataLiquidacao = operacaoFundo.DataLiquidacao;
                operacaoCotista.DataOperacao = operacaoFundo.DataOperacao;
                operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;
                operacaoCotista.IdCarteira = operacaoFundo.IdCarteira;
                operacaoCotista.IdCotista = operacaoFundo.IdCliente;
                operacaoCotista.IdFormaLiquidacao = operacaoFundo.IdFormaLiquidacao;
                //operacaoCotista.IdPosicaoResgatada = POR ORA SUPOE-SE QUE NÃO SERÃO LANCADOS RESGATES DIRECIONADOS A UMA APLICACAO!!!!
                operacaoCotista.Observacao = operacaoFundo.Observacao;
                operacaoCotista.PrejuizoUsado = operacaoFundo.PrejuizoUsado;
                operacaoCotista.Quantidade = operacaoFundo.Quantidade;
                operacaoCotista.RendimentoResgate = operacaoFundo.RendimentoResgate;
                operacaoCotista.TipoResgate = operacaoFundo.TipoOperacao;
                operacaoCotista.TipoResgate = operacaoFundo.TipoResgate;
                operacaoCotista.ValorBruto = operacaoFundo.ValorBruto;
                operacaoCotista.ValorCPMF = operacaoFundo.ValorCPMF;
                operacaoCotista.ValorIOF = operacaoFundo.ValorIOF;
                operacaoCotista.ValorIR = operacaoFundo.ValorIR;
                operacaoCotista.ValorLiquido = operacaoFundo.ValorLiquido;
                operacaoCotista.ValorPerformance = operacaoFundo.ValorPerformance;
                operacaoCotista.VariacaoResgate = operacaoFundo.VariacaoResgate;
            }

            operacaoCotistaCollection.Save();
        }
        
        /// <summary>
        /// Carga em OperacaoFundo a partir da estrutura preenchida de arquivo da SMA.
       /// </summary>
       /// <param name="movimento"></param>
       /// <param name="deferSave"></param>
       /// <param name="comCalculo"></param>
       /// <returns></returns>
        public bool ImportaMovimentoSMA(Financial.Interfaces.Import.Fundo.SMA.MovimentoPassivo movimento, bool deferSave)
        {
            this.IdOperacao = movimento.IdNota;
                        
            string codigoFundo = movimento.CdFundo.Trim();
            string codigoCliente = movimento.CdCotista.Trim();
            string cpfCnpj = movimento.CpfCnpj;
                        
            //SMA trabalha com YMF, então busca o código do fundo no sistema da YMF
            int? idFundo = null;
            
            Cliente fundoExistente = new ClienteCollection().BuscaClientePorCodigoYMF("|" + codigoFundo + "|");

            if (fundoExistente != null)
            {
                idFundo = fundoExistente.IdCliente;
            }

            else
            {
                if (Utilitario.IsInteger(codigoFundo))
                {
                    Cliente clienteFundo = new Cliente();
                    if (clienteFundo.LoadByPrimaryKey(Convert.ToInt32(codigoFundo)))
                    {
                        idFundo = clienteFundo.IdCliente.Value;
                    }
                }
            }

            if (!idFundo.HasValue)
            {
                throw new Exception("Fundo não encontrado: " + codigoFundo);
            }
                    
            int? idCliente = null;
            DateTime? dataDia = null;
            ClienteCollection clienteCollection = new ClienteCollection();
            Cliente cliente = new Cliente();
            if (!String.IsNullOrEmpty(cpfCnpj))
            {
                PessoaCollection pessoaCollection = new PessoaCollection();
                pessoaCollection.Query.Select(pessoaCollection.Query.IdPessoa);
                pessoaCollection.Query.Where(pessoaCollection.Query.Cpfcnpj.Equal(cpfCnpj));
                pessoaCollection.Query.Load();

                if (pessoaCollection.Count > 0)
                {
                    clienteCollection.Query.Select(clienteCollection.Query.IdCliente, clienteCollection.Query.DataDia);
                    clienteCollection.Query.Where(clienteCollection.Query.IdCliente.Equal(pessoaCollection[0].IdPessoa.Value));
                    clienteCollection.Query.Load();

                    if (clienteCollection.Count > 0)
                    {
                        cliente = clienteCollection[0];
                        idCliente = clienteCollection[0].IdCliente.Value;
                        dataDia = clienteCollection[0].DataDia.Value;
                    }
                }                
            }

            if (!idCliente.HasValue)
            {
                Cliente clienteExistente = new ClienteCollection().BuscaClientePorCodigoYMF("|" + codigoCliente + "|");
                if (clienteExistente != null)
                {
                    idCliente = clienteExistente.IdCliente;
                }
                else
                {

                    if (Utilitario.IsInteger(codigoCliente))
                    {
                        //Não encontramos idCliente pelo CodigoYMF... Testar pra ver se encontramos um Cliente o proprio IdCliente igual ao código passado
                        idCliente = Convert.ToInt32(codigoCliente);
                    }
                }

                if (idCliente.HasValue && cliente.LoadByPrimaryKey(idCliente.Value))
                {
                    idCliente = cliente.IdCliente.Value;
                    dataDia = cliente.DataDia.Value;
                }
                else
                {
                    idCliente = null;
                }
            }

            if (!idCliente.HasValue || cliente.StatusAtivo == (byte)StatusAtivoCliente.Inativo)
            {
                return false;
            }

            if (movimento.DtMovimento < dataDia.Value || (movimento.DtMovimento == dataDia.Value & cliente.Status == (byte)StatusCliente.Divulgado))
            {
                return false;
            }

            this.IdCarteira = idFundo.Value;
            this.IdCliente = idCliente.Value;
            
            this.DataOperacao = movimento.DtMovimento;
            this.DataConversao = movimento.DtCotizacao;
            this.DataLiquidacao = movimento.DtCotizacao; //TODO Por ora jogando na mesma data da cotização!!!!
            this.DataAgendamento = movimento.DtMovimento;

            this.TipoOperacao = MapTipoMovimentoSMA(movimento.TipoMovimentacao);

            if (this.TipoOperacao != (byte)TipoOperacaoFundo.Aplicacao)
            {
                this.TipoResgate = (byte)TipoResgateFundo.Especifico;
                this.IdPosicaoResgatada = movimento.IdNotaAplicacao;
            }            
            
            this.IdFormaLiquidacao = 1; //Checar se vai ser DOC mesmo
            this.Quantidade = movimento.QuantidadeCotas;

            this.CotaOperacao = movimento.ValorCota;
            this.ValorBruto = movimento.ValorBruto;
            this.ValorLiquido = movimento.ValorLiquido;
            this.ValorIR = movimento.ValorIR;
            this.ValorIOF = movimento.ValorIOF;
            this.ValorCPMF = 0;
            this.ValorPerformance = movimento.ValorPerformance;            
            this.PrejuizoUsado = 0;
            this.RendimentoResgate = movimento.RendimentoBruto;
            this.VariacaoResgate = 0;
            this.Observacao = string.Empty;
            this.Fonte = (byte)FonteOperacaoFundo.SMA;
            
            if (!deferSave)
            {
                this.Save();
            }

            return true;
        }

        private byte MapTipoMovimentoSMA(string tipoMovimentoSMA)
        {
            //Faz o mapeamento entro e ENUM TipoMovimentoSMA definido em Interfaces com o ENUM TipoOperacaoFundo
            byte tipoOperacaoFundo;
            if (tipoMovimentoSMA == Financial.Interfaces.Import.Fundo.Enums.TipoMovimentoSMA.Aplicacao)
            {
                tipoOperacaoFundo = (byte)TipoOperacaoFundo.Aplicacao;
            }
            else if (tipoMovimentoSMA == Financial.Interfaces.Import.Fundo.Enums.TipoMovimentoSMA.ResgateLiquido)
            {
                tipoOperacaoFundo = (byte)TipoOperacaoFundo.ResgateLiquido;
            }
            else if (tipoMovimentoSMA == Financial.Interfaces.Import.Fundo.Enums.TipoMovimentoSMA.ResgateTotal)
            {
                tipoOperacaoFundo = (byte)TipoOperacaoFundo.ResgateTotal;
            }
            else if (tipoMovimentoSMA == Financial.Interfaces.Import.Fundo.Enums.TipoMovimentoSMA.ResgatePagtoIR)
            {
                tipoOperacaoFundo = (byte)TipoOperacaoFundo.ComeCotas;
            }
            else
            {
                throw new Exception("Formato de Tipo de Operação Fundo não suportado");
            }
            return tipoOperacaoFundo;
        }

        /// <summary>
        /// Processa os lançamentos de ajuste de posição.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void AjustaPosicao(int idCliente, DateTime data)
        {
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.Quantidade,
                                                 operacaoFundoCollection.Query.IdCarteira);
            operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoFundoCollection.Query.DataOperacao.Equal(data),
                                                operacaoFundoCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.AjustePosicao));
            operacaoFundoCollection.Query.Load();

            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
            {
                decimal quantidadeAjuste = operacaoFundo.Quantidade.Value;
                int idCarteira = operacaoFundo.IdCarteira.Value;

                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira));

                posicaoFundoCollection.Query.Load();

                int cont = 0;
                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    if (cont == (posicaoFundoCollection.Count - 1))
                    {
                        posicaoFundo.DataAplicacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        posicaoFundo.DataConversao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        posicaoFundo.Quantidade = quantidadeAjuste;
                        posicaoFundo.ValorLiquido = posicaoFundo.ValorBruto = quantidadeAjuste * posicaoFundo.CotaDia.Value;
                        posicaoFundo.ValorAplicacao = quantidadeAjuste * posicaoFundo.CotaAplicacao.Value;
                        posicaoFundo.ValorIOF = 0;
                        posicaoFundo.ValorIOFVirtual = 0;
                        posicaoFundo.ValorIR = 0;
                        posicaoFundo.ValorLiquido = 0;
                        posicaoFundo.ValorPendenteLiquidacao = 0;
                        posicaoFundo.ValorPerformance = 0;
                        posicaoFundo.ValorRendimento = 0;
                    }
                    else
                    {
                        posicaoFundo.QuantidadeAntesCortes = 0;
                        posicaoFundo.Quantidade = 0;
                        posicaoFundo.ValorBruto = 0;
                        posicaoFundo.ValorAplicacao = 0;
                        posicaoFundo.ValorIOF = 0;
                        posicaoFundo.ValorIOFVirtual = 0;
                        posicaoFundo.ValorIR = 0;
                        posicaoFundo.ValorLiquido = 0;
                        posicaoFundo.ValorPendenteLiquidacao = 0;
                        posicaoFundo.ValorPerformance = 0;
                        posicaoFundo.ValorRendimento = 0;
                    }
                    cont += 1;
                }

                posicaoFundoCollection.Save();
            }
        }

        /// <summary>
        /// Processa eventos de amortização e juros para fundos FDIC.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaEventos(int idCliente, DateTime data)
        {
            #region Deleta operações de Eventos
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.IdOperacao);
            operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoFundoCollection.Query.DataOperacao.Equal(data),
                                                operacaoFundoCollection.Query.TipoOperacao.In((byte)TipoOperacaoFundo.Amortizacao,
                                                                                              (byte)TipoOperacaoFundo.Juros,
                                                                                              (byte)TipoOperacaoFundo.AmortizacaoJuros,
                                                                                              (byte)TipoOperacaoFundo.Dividendo));
            operacaoFundoCollection.Query.Load();
            operacaoFundoCollection.MarkAllAsDeleted();
            operacaoFundoCollection.Save();
            #endregion

            #region Busca Agenda Fundo
            AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
            agendaFundoCollection.Query.Select(agendaFundoCollection.Query.IdCarteira,
                                               agendaFundoCollection.Query.TipoEvento,
                                               agendaFundoCollection.Query.Valor,
                                               agendaFundoCollection.Query.Taxa,
                                               agendaFundoCollection.Query.TipoValorInput);
            agendaFundoCollection.Query.Where(agendaFundoCollection.Query.IdCarteira.NotEqual(idCliente),
                                              agendaFundoCollection.Query.DataEvento.Equal(data),
                                              agendaFundoCollection.Query.TipoEvento.In((byte)TipoEventoFundo.Amortizacao,
                                                                                        (byte)TipoEventoFundo.AmortizacaoJuros,
                                                                                        (byte)TipoEventoFundo.Juros,
                                                                                        (byte)TipoEventoFundo.Dividendo));

            agendaFundoCollection.Query.OrderBy(agendaFundoCollection.Query.IdCarteira.Ascending);

            if (!agendaFundoCollection.Query.Load())
                return;
            #endregion

            //Busca Cliente
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            int? tipo = cliente.UpToPessoaByIdPessoa.Tipo;

            ListaTipoInvestidor tipoInvestidor;
            if (tipo.HasValue)
            {
                if (tipo.Value == 1)
                    tipoInvestidor = ListaTipoInvestidor.PessoaFisica;
                else
                    tipoInvestidor = ListaTipoInvestidor.PessoaJuridica;
            }
            else
            {
                tipoInvestidor = ListaTipoInvestidor.OffShore;
            }                
                
            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);

            operacaoFundoCollection = new OperacaoFundoCollection();
            LiquidacaoCollection liquidacaoColl = new LiquidacaoCollection();
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            Dictionary<int, HistoricoCota> dicHistoricoCota = new Dictionary<int, HistoricoCota>();
            HistoricoCota historicoCota = new HistoricoCota();
            Carteira carteira = new Carteira();
            Cliente cliente_Carteira = new Cliente();

            int ultimaCarteira = 0;

            foreach (AgendaFundo agendaFundo in agendaFundoCollection)
            {
                int idCarteira = agendaFundo.IdCarteira.Value;                
                byte tipoEvento = agendaFundo.TipoEvento.Value;
                decimal taxaEvento = agendaFundo.Taxa.Value;
                decimal valorEvento = 0;
                decimal? cotaCheia = agendaFundo.CotaFechamentoCheia;

                if (agendaFundo.TipoValorInput.Equals((int)TipoValorProvento.ValorporCota))
                {
                    valorEvento = agendaFundo.Valor.Value;
                }
                else
                {
                    try
                    {
                        if (dicHistoricoCota.ContainsKey(idCarteira))
                        {
                            historicoCota = dicHistoricoCota[idCarteira];
                        }
                        else
                        {
                            historicoCota.BuscaValorCotaDia(idCarteira, data);
                            dicHistoricoCota.Add(idCarteira, historicoCota);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Evento de " + TipoEventoFundoDescricao.RetornaStringValue((int)tipoEvento) + " - " + ex.Message); ;
                    }

                    decimal cotaAux = cotaCheia.HasValue ? cotaCheia.Value : historicoCota.CotaFechamento.GetValueOrDefault(0);

                    if (agendaFundo.TipoValorInput.Equals((int)TipoValorProvento.TaxaCotaEx))
                        valorEvento = cotaAux * (taxaEvento / (100 - taxaEvento));

                    if (agendaFundo.TipoValorInput.Equals((int)TipoValorProvento.TaxaFixa))
                        valorEvento = cotaAux * taxaEvento / 100;
                }

                //Busca Posições e Caracteristicas da Carteira
                if (ultimaCarteira != idCarteira)
                {
                    posicaoFundoCollection = new PosicaoFundoCollection();
                    posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.AmortizacaoAcumuladaPorCota,
                                                        posicaoFundoCollection.Query.AmortizacaoAcumuladaPorValor,
                                                        posicaoFundoCollection.Query.JurosAcumuladoPorCota,
                                                        posicaoFundoCollection.Query.JurosAcumuladoPorValor,
                                                        posicaoFundoCollection.Query.ValorAplicacao,
                                                        posicaoFundoCollection.Query.ValorBruto,
                                                        posicaoFundoCollection.Query.IdPosicao,
                                                        posicaoFundoCollection.Query.IdCarteira,
                                                        posicaoFundoCollection.Query.Quantidade,
                                                        posicaoFundoCollection.Query.DataAplicacao,
                                                        posicaoFundoCollection.Query.DataConversao,
                                                        posicaoFundoCollection.Query.CotaAplicacao);
                    posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                       posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira));

                    if (!posicaoFundoCollection.Query.Load())
                        continue;

                    carteira.LoadByPrimaryKey(idCarteira);

                    cliente_Carteira.LoadByPrimaryKey(idCarteira);
                }

                ultimaCarteira = idCarteira;

                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    historicoCota = new HistoricoCota();

                    int idLocalFeriado = cliente_Carteira.IdLocal.Value;
                    decimal quantidade = posicaoFundo.Quantidade.Value;
                    DateTime dataAplicacao = posicaoFundo.DataAplicacao.Value;
                    DateTime dataConversao = posicaoFundo.DataConversao.Value;
                    decimal cotaAplicacao = posicaoFundo.CotaAplicacao.Value;
                    DateTime dataLiquidacao = data;
                    DateTime? dataPagamentoTributoIR = data;
                    decimal rendimento = valorEvento * quantidade;
                    decimal valorIR = 0;
                    decimal aliquotaIR = 0;
                    int idPosicao = posicaoFundo.IdPosicao.Value;
                    string codigoAtivo = posicaoFundo.IdCarteira.Value.ToString();
                    bool truncaFinanceiro = carteira.IsTruncaFinanceiro();
                    dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, carteira.DiasLiquidacaoResgate.GetValueOrDefault(0));
                                     
                    //Amortização + Juros
                    if (tipoEvento == (byte)TipoEventoFundo.AmortizacaoJuros)
                    {
                        decimal valorAplicacaoEx = posicaoFundo.ValorAplicacao.Value - posicaoFundo.AmortizacaoAcumuladaPorValor.Value;

                        try
                        {
                            if (dicHistoricoCota.ContainsKey(idCarteira))
                            {
                                historicoCota = dicHistoricoCota[idCarteira];
                            }
                            else
                            {
                                historicoCota.BuscaValorCotaDia(idCarteira, data);
                                dicHistoricoCota.Add(idCarteira, historicoCota);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Evento de " + TipoEventoFundoDescricao.RetornaStringValue((int)tipoEvento) + " - " + ex.Message); ;
                        }

                        decimal cotaDiaEx = historicoCota.CotaFechamento.GetValueOrDefault(0);
                        decimal valorDiaEx = 0;

                        if (truncaFinanceiro)
                        {
                            valorDiaEx = Utilitario.Truncate(quantidade * cotaDiaEx, 2);
                        }
                        else
                        {
                            valorDiaEx = Math.Round(quantidade * cotaDiaEx, 2);
                        }
                    

                        decimal valorAmortizado = valorAplicacaoEx * (taxaEvento / 100);
                        decimal valorJuros = (valorDiaEx - valorAplicacaoEx) * (taxaEvento / 100);

                        #region Juros
                        byte tipoOperacao = (byte)TipoOperacaoFundo.Juros;
                        if (valorJuros > 0)
                        {
                            if (aliquotaIR != 0)
                                valorIR = valorJuros * (aliquotaIR / 100);
                      
                            //Gera Operação
                            operacaoFundoCollection.AttachEntity(GeraOperacaoEvento(idCliente, idCarteira, data, dataLiquidacao, tipoOperacao, valorJuros, valorIR));

                            //Gera Liquidação
                            liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCliente, carteira.Apelido, data, dataLiquidacao, idContaDefault, (byte)TipoEventoFundo.Juros, valorJuros, -valorIR, idPosicao, codigoAtivo));

                            //Soma valor de juros por Cota 
                            posicaoFundo.JurosAcumuladoPorCota += (valorJuros / quantidade);
                            posicaoFundo.JurosAcumuladoPorValor += valorJuros;
                        }
                        #endregion

                        #region Amortização

                        dataLiquidacao = Calendario.AdicionaDiaUtil(dataLiquidacao, carteira.DiasLiquidacaoResgate.GetValueOrDefault(0));                        

                        tipoOperacao = (byte)TipoOperacaoFundo.Amortizacao;

                        //Gera Operação
                        operacaoFundoCollection.AttachEntity(GeraOperacaoEvento(idCliente, idCarteira, data, dataLiquidacao, tipoOperacao, valorAmortizado, 0));

                        //Gera Liquidação
                        liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCliente, carteira.Apelido, data, dataLiquidacao, idContaDefault, (byte)TipoEventoFundo.Amortizacao, valorAmortizado, 0, idPosicao, codigoAtivo));

                        //Soma valor de amortização por Cota 
                        posicaoFundo.AmortizacaoAcumuladaPorCota += (valorAmortizado / quantidade);
                        posicaoFundo.AmortizacaoAcumuladaPorValor += valorAmortizado;
                        #endregion
                    }
                    else
                    {
                        rendimento = valorEvento * quantidade;

                        byte tipoOperacao = (byte)TipoOperacaoFundo.Dividendo;                        

                        if (tipoEvento == (byte)TipoEventoFundo.Amortizacao)
                        {                            
                            tipoOperacao = (byte)TipoOperacaoFundo.Amortizacao;                   
                        }
                        else if (tipoEvento == (byte)TipoEventoFundo.Juros)
                        {
                            tipoOperacao = (byte)TipoOperacaoFundo.Juros;
                            if (aliquotaIR != 0)
                                valorIR = Math.Round(rendimento * aliquotaIR / 100M, 2, MidpointRounding.AwayFromZero); //Juros aplica direto o % do IR no valor dos juros                              
                        }

                        //Gera Operação
                        operacaoFundoCollection.AttachEntity(GeraOperacaoEvento(idCliente, idCarteira, data, dataLiquidacao, tipoOperacao, rendimento, valorIR));

                        //Gera Liquidação
                        liquidacaoColl.Combine(this.GeraLiquidacaoEvento(idCliente, carteira.Apelido, data, dataLiquidacao, idContaDefault, tipoEvento, rendimento, -valorIR, idPosicao, codigoAtivo));

                        if (tipoEvento == (byte)TipoEventoFundo.Amortizacao)
                        {
                            posicaoFundo.AmortizacaoAcumuladaPorCota += valorEvento;
                            posicaoFundo.AmortizacaoAcumuladaPorValor += rendimento;
                        }
                        else if (tipoEvento == (byte)TipoEventoFundo.Juros)
                        {
                            posicaoFundo.JurosAcumuladoPorCota += valorEvento;
                            posicaoFundo.JurosAcumuladoPorValor += rendimento;
                        }
                    }
                }
            }

            operacaoFundoCollection.Save();
            posicaoFundoCollection.Save();
            liquidacaoColl.Save();
        }

        private OperacaoFundo GeraOperacaoEvento(int idCliente, int idCarteira, DateTime data, DateTime dataLiquidacao, byte tipoOperacao, decimal rendimento, decimal valorIR)
        {
            OperacaoFundo operacaoFundo = new OperacaoFundo();
            operacaoFundo.TipoOperacao = tipoOperacao;
            operacaoFundo.DataAgendamento = data;
            operacaoFundo.DataConversao = data;
            operacaoFundo.DataLiquidacao = dataLiquidacao;
            operacaoFundo.DataOperacao = data;
            operacaoFundo.IdCliente = idCliente;
            operacaoFundo.IdCarteira = idCarteira;
            operacaoFundo.IdFormaLiquidacao = 1; //FORÇADO
            operacaoFundo.Quantidade = 0;
            operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;
            operacaoFundo.ValorIR = valorIR;
            operacaoFundo.ValorBruto = rendimento;
            operacaoFundo.ValorLiquido = rendimento - valorIR;
            operacaoFundo.DataRegistro = data;

            return operacaoFundo;
        }

        private LiquidacaoCollection GeraLiquidacaoEvento(int idCliente, string apelido_ContraParte, DateTime data, DateTime dataLiquidacao, int idContaDefault, byte tipoEvento, decimal valorBruto, decimal valorIR, int idPosicao, string codigoAtivo)
        {
            LiquidacaoCollection liqColl = new LiquidacaoCollection();
            int origemValorBruto = OrigemLancamentoLiquidacao.Fundo.None;
            int origemValorIR = OrigemLancamentoLiquidacao.Fundo.None;

            string descricao = "Id.Posição " + idPosicao + " (Fundo) - ";

            if (tipoEvento == (byte)TipoEventoFundo.Amortizacao)
            {
                descricao += "Amortização ";
                origemValorBruto = OrigemLancamentoLiquidacao.Fundo.Amortizacao;
            }
            else if (tipoEvento == (byte)TipoEventoFundo.Dividendo)
            {
                descricao += "Dividendo ";
                origemValorBruto = OrigemLancamentoLiquidacao.Fundo.Dividendo;
            }
            else if (tipoEvento == (byte)TipoEventoFundo.ComeCotas)
            {
                descricao += "ComeCotas ";
                origemValorBruto = OrigemLancamentoLiquidacao.Fundo.ComeCotas;
            }
            else
            {
                descricao += "Juros ";
                origemValorBruto = OrigemLancamentoLiquidacao.Fundo.Juros;
                origemValorIR = OrigemLancamentoLiquidacao.Fundo.JurosIR;
            }

            descricao = descricao + "'" + apelido_ContraParte + "'";

            if (valorBruto != 0)
            {
                Liquidacao liquidacao = liqColl.AddNew();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaDefault;
                liquidacao.Origem = origemValorBruto;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Valor = valorBruto;
                liquidacao.CodigoAtivo = codigoAtivo;
            }

            if (valorIR != 0)
            {
                Liquidacao liquidacaoIR = liqColl.AddNew();
                liquidacaoIR.DataLancamento = data;
                liquidacaoIR.DataVencimento = dataLiquidacao;
                liquidacaoIR.Descricao = descricao.Replace("Juros ", "IR / Juros ");
                liquidacaoIR.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacaoIR.IdCliente = idCliente;
                liquidacaoIR.IdConta = idContaDefault;
                liquidacaoIR.Origem = origemValorIR;
                liquidacaoIR.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacaoIR.Valor = valorIR;
                liquidacaoIR.CodigoAtivo = codigoAtivo;
            }

            return liqColl;
        }

        /// <summary>
        /// Trata aplicações vindas de arquivos de operações, no caso em que a opção escolhida de importação = SemCalculo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaAplicacaoSemCalculo(int idCliente, DateTime data) {
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();

            List<byte> listaFonte = new List<byte>();
            listaFonte.Add((byte)FonteOperacaoFundo.YMFSemCalculo);
            listaFonte.Add((byte)FonteOperacaoFundo.SMA);
            operacaoFundoCollection.BuscaAplicacao(idCliente, data, listaFonte);

            for (int i = 0; i < operacaoFundoCollection.Count; i++) {
                OperacaoFundo operacaoFundo = operacaoFundoCollection[i];

                #region Insert em PosicaoFundo
                esParameters esParams = new esParameters();
                esParams.Add("IdPosicao", operacaoFundo.IdOperacao.Value);
                esParams.Add("IdOperacao", operacaoFundo.IdOperacao.Value);
                esParams.Add("IdCliente", operacaoFundo.IdCliente.Value);
                esParams.Add("IdCarteira", operacaoFundo.IdCarteira.Value);
                esParams.Add("ValorAplicacao", operacaoFundo.ValorBruto.Value);
                esParams.Add("DataAplicacao", operacaoFundo.DataOperacao.Value);
                esParams.Add("DataConversao", operacaoFundo.DataConversao.Value);
                esParams.Add("CotaAplicacao", operacaoFundo.CotaOperacao.Value);
                esParams.Add("CotaDia", operacaoFundo.CotaOperacao.Value);
                esParams.Add("ValorBruto", operacaoFundo.ValorBruto.Value);
                esParams.Add("ValorLiquido", operacaoFundo.ValorBruto.Value);
                esParams.Add("QuantidadeInicial", operacaoFundo.Quantidade.Value);
                esParams.Add("Quantidade", operacaoFundo.Quantidade.Value);
                esParams.Add("QuantidadeAntesCortes", operacaoFundo.Quantidade.Value);
                esParams.Add("DataUltimaCobrancaIR", operacaoFundo.DataConversao.Value); //Artificio para início de posição
                decimal valorRendimento = 0.00M;
                esParams.Add("ValorRendimento", valorRendimento);
                esParams.Add("PosicaoIncorporada", "N");
                /* Defaults 0 */
                //esParams.Add("QuantidadeBloqueada", posicaoFundoHistorico.QuantidadeBloqueada.Value);                
                //esParams.Add("ValorIR", posicaoFundoHistorico.ValorIR.Value);
                //esParams.Add("ValorIOF", posicaoFundoHistorico.ValorIOF.Value);
                //esParams.Add("ValorPerformance", posicaoFundoHistorico.ValorPerformance.Value);
                //esParams.Add("ValorIOFVirtual", posicaoFundoHistorico.ValorIOFVirtual.Value);
                /**/
                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoFundo ON ");

                sqlBuilder.Append("INSERT INTO PosicaoFundo (");
                      sqlBuilder.Append(PosicaoFundoMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.IdCarteira);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.ValorAplicacao);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.DataAplicacao);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.DataConversao);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.CotaAplicacao);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.CotaDia);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.ValorBruto);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.ValorLiquido);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.QuantidadeInicial);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.Quantidade);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.QuantidadeAntesCortes);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.DataUltimaCobrancaIR);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.ValorRendimento);
                sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.PosicaoIncorporada);
                /*
                //sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.ValorIR);
                //sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.ValorIOF);
                //sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.ValorPerformance);
                //sqlBuilder.Append("," + PosicaoFundoMetadata.ColumnNames.ValorIOFVirtual);
                */
                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.IdPosicao);             sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.IdOperacao);            sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.IdCliente);             sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.IdCarteira);            sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.ValorAplicacao);        sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.DataAplicacao);         sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.DataConversao);         sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.CotaAplicacao);         sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.CotaDia);               sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.ValorBruto);            sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.ValorLiquido);          sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.QuantidadeInicial);     sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.Quantidade);            sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.QuantidadeAntesCortes); sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.DataUltimaCobrancaIR);  sqlBuilder.Append(",");
                /*
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.ValorIR);              sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.ValorIOF);             sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.ValorPerformance);     sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.ValorIOFVirtual);      sqlBuilder.Append(",");
                */
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.ValorRendimento); sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoFundoMetadata.ColumnNames.PosicaoIncorporada);
                
                sqlBuilder.Append(")");
                //
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoFundo OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                #endregion                
            }
        }

        /// <summary>
        /// Trata os resgates e comecotas vindos de arquivos de operações, no caso em que a opção escolhida de importação = SemCalculo. 
        /// Nesta situação os resgates e comecotas devem ser tratados pelo nr da nota de aplicação resgatada informada no arquivo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaResgateSemCalculo(int idCliente, DateTime data)
        {
            DetalheResgateFundoCollection detalheResgateFundoCollection = new DetalheResgateFundoCollection();

            DetalheResgateFundoQuery detalheResgateFundoQuery = new DetalheResgateFundoQuery("D");
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");

            detalheResgateFundoQuery.Select(detalheResgateFundoQuery.IdPosicaoResgatada, detalheResgateFundoQuery.Quantidade);
            detalheResgateFundoQuery.InnerJoin(operacaoFundoQuery).On(operacaoFundoQuery.IdOperacao == detalheResgateFundoQuery.IdOperacao);
            detalheResgateFundoQuery.Where(detalheResgateFundoQuery.IdCliente.Equal(idCliente),
                                           operacaoFundoQuery.DataConversao.Equal(data),
                                           operacaoFundoQuery.Fonte.In((byte)FonteOperacaoFundo.YMFSemCalculo,
                                                                       (byte)FonteOperacaoFundo.SMA,
                                                                       (byte)FonteOperacaoFundo.Ajustado));
            detalheResgateFundoCollection.Load(detalheResgateFundoQuery);

            foreach (DetalheResgateFundo detalheResgateFundo in detalheResgateFundoCollection)
            {
                int idPosicao = detalheResgateFundo.IdPosicaoResgatada.Value;
                decimal quantidade = detalheResgateFundo.Quantidade.Value;

                PosicaoFundo posicaoFundo = new PosicaoFundo();
                posicaoFundo.Query.Where(posicaoFundo.Query.IdPosicao.Equal(idPosicao));
                if (posicaoFundo.Query.Load())
                {
                    posicaoFundo.Quantidade -= quantidade;

                    if (posicaoFundo.Quantidade <= 0)
                    {
                        posicaoFundo.Quantidade = 0;
                        posicaoFundo.ValorBruto = 0;
                        posicaoFundo.ValorLiquido = 0;
                        posicaoFundo.ValorIR = 0;
                        posicaoFundo.ValorIOF = 0;
                        posicaoFundo.ValorPerformance = 0;
                    }

                    posicaoFundo.Save();
                }
            }
        }

        /// <summary>
        /// Transfere todas as operações de OperacaoCotista para OperacaoFundo.
        /// Deleta antes as operações em OperacaoFundo (filtrando IdCotista != IdCarteira).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void CarregaOperacaoCotista(int idCliente, DateTime data)
        {
            OperacaoFundoCollection operacaoFundoCollectionDeletar = new OperacaoFundoCollection();
            operacaoFundoCollectionDeletar.Query.Where(operacaoFundoCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                       operacaoFundoCollectionDeletar.Query.DataOperacao.Equal(data));
            operacaoFundoCollectionDeletar.Query.Load();
            operacaoFundoCollectionDeletar.MarkAllAsDeleted();
            operacaoFundoCollectionDeletar.Save();

            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();

            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            operacaoCotistaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == cotistaQuery.IdCotista);
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.Equal(data) &
                                        (cotistaQuery.CodigoInterface.Like("%" + idCliente.ToString() + "%")  |
                                         cotistaQuery.IdCotista.Equal(idCliente))
                                       );

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Load(operacaoCotistaQuery);

            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                OperacaoFundo operacaoFundo = operacaoFundoCollection.AddNew();

                operacaoFundo.CotaOperacao = operacaoCotista.CotaOperacao;
                operacaoFundo.DataAgendamento = operacaoCotista.DataAgendamento;
                operacaoFundo.DataConversao = operacaoCotista.DataConversao;
                operacaoFundo.DataLiquidacao = operacaoCotista.DataLiquidacao;
                operacaoFundo.DataOperacao = operacaoCotista.DataOperacao;
                operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;
                operacaoFundo.IdCarteira = operacaoCotista.IdCarteira;
                operacaoFundo.IdCliente = idCliente;
                operacaoFundo.IdFormaLiquidacao = operacaoCotista.IdFormaLiquidacao;
                //operacaoFundo.IdPosicaoResgatada = POR ORA SUPOE-SE QUE NÃO SERÃO LANCADOS RESGATES DIRECIONADOS A UMA APLICACAO!!!!
                operacaoFundo.Observacao = operacaoCotista.Observacao;
                operacaoFundo.PrejuizoUsado = operacaoCotista.PrejuizoUsado;
                operacaoFundo.Quantidade = operacaoCotista.Quantidade;
                operacaoFundo.RendimentoResgate = operacaoCotista.RendimentoResgate;
                operacaoFundo.TipoOperacao = operacaoCotista.TipoOperacao;
                operacaoFundo.TipoResgate = operacaoCotista.TipoResgate;
                operacaoFundo.ValorBruto = operacaoCotista.ValorBruto;
                operacaoFundo.ValorCPMF = operacaoCotista.ValorCPMF;
                operacaoFundo.ValorIOF = operacaoCotista.ValorIOF;
                operacaoFundo.ValorIR = operacaoCotista.ValorIR;
                operacaoFundo.ValorLiquido = operacaoCotista.ValorLiquido;
                operacaoFundo.ValorPerformance = operacaoCotista.ValorPerformance;
                operacaoFundo.VariacaoResgate = operacaoCotista.VariacaoResgate;
            }

            operacaoFundoCollection.Save();
        }
        
    }
}
