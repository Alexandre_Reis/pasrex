using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Fundo.Custom
{
    public class ComposicaoCarteira
    {
        public class posicaoSintetica
        {
            public string grupo;
            public int tipoAtivo; //Remete a ListaTipoAtivo de Financial.Fundo
            public string descricao;
            public decimal valorMercado;
            public decimal percPL;
        }
    }
}
