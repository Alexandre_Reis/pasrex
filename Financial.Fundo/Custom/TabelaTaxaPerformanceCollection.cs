using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class TabelaTaxaPerformanceCollection : esTabelaTaxaPerformanceCollection
	{
        /// <summary>
        /// Carrega o objeto TabelaTaxaPerformanceCollection com todos os campos de TabelaTaxaPerformance.
        /// Ordena decrescente por IdTabela, DataReferencia.
        /// Filtra somente com DataReferencia menor ou igual que a data, 
        /// e data menor ou igual a DataFim (ou DataFim = NULL).
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaTabelaTaxaPerformance(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.DataReferencia.LessThanOrEqual(data))
                 .OrderBy(this.Query.DataReferencia.Descending);
            
            return this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto TabelaTaxaPerformanceCollection com todos os campos de TabelaTaxaPerformance.
        /// Ordena decrescente por IdTabela, DataReferencia.
        /// Filtra somente com DataReferencia menor ou igual que a data, 
        /// e data menor ou igual a DataFim (ou DataFim = NULL).
        /// Filtra por tabelaTaxaPerformanceCollection.Query.CalculaPerformanceResgate.Equal("S").
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaTabelaTaxaPerformanceResgate(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Or(
                        this.Query.DataFim.GreaterThanOrEqual(data),
                        this.Query.DataFim.IsNull()),
                        this.Query.DataReferencia.LessThanOrEqual(data),
                        this.Query.CalculaPerformanceResgate.Equal("S"))
                 .OrderBy(this.Query.DataReferencia.Descending);

            bool retorno = this.Query.Load();

            return retorno;
        }


	}
}
