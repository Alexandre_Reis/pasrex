﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente;
using Financial.Fundo.Enums;
using Financial.Fundo.Exceptions;
using Financial.Util;
using Financial.Common.Enums;
using Financial.ContaCorrente.Enums;
using log4net;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.RendaFixa;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.InvestidorCotista;
using Financial.Common;

namespace Financial.Fundo
{
    public partial class CalculoAdministracao : esCalculoAdministracao
    {
        /// <summary>
        /// Calcula os valores (diário e acumulado) dos vários tipos de Taxa Administração, além da CPMF incidente.
        /// Lança na Liquidacao o valor acumulado da data (provisão e CPMF (somente se > 01/01/2008)).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="liquidaCC">indica se lanca ou nao em Liquidacao
        public void CalculaTaxaAdministracao(int idCarteira, DateTime data, bool liquidaCC)
        {

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataImplantacao);
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(campos, idCarteira);
            DateTime dataImplantacao = cliente.DataImplantacao.Value;

            bool isIniciando = DateTime.Compare(data, dataImplantacao) == 0;
            short idLocal = cliente.IdLocal.Value; //Local (domicilio) para avaliação dos feriados relativos ao cliente

            CalculoAdministracaoCollection calculoAdministracaoCollectionDeletar = new CalculoAdministracaoCollection();
            //CalculoAdministracaoAssociadaCollection calculoAdministracaoAssociadaCollectionDeletar = new CalculoAdministracaoAssociadaCollection();
            List<int> listaDeletar = new List<int>();

            TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();            
            tabelaTaxaAdministracaoCollection.BuscaTabelaTaxaAdministracao(idCarteira, data);

            //if (data.Day > 17) { throw new TabelaTaxaAdministracaoParametrosNulosException("Teste de mensagem de erro"); }

            for (int i = 0; i < tabelaTaxaAdministracaoCollection.Count; i++)
            {
                #region Carregamento das variáveis locais de TabelaTaxaAdministracao
                TabelaTaxaAdministracao tabelaTaxaAdministracao = (TabelaTaxaAdministracao)tabelaTaxaAdministracaoCollection[i];
                int idTabela = tabelaTaxaAdministracao.IdTabela.Value;
                int idCadastro = tabelaTaxaAdministracao.IdCadastro.Value;
                int contagemDias = tabelaTaxaAdministracao.ContagemDias.Value;
                int tipoCalculo = tabelaTaxaAdministracao.TipoCalculo.Value; //Indica se calcula p/ valor fixo ou % do PL
                int numeroMesesRenovacao = tabelaTaxaAdministracao.NumeroMesesRenovacao.Value;
                decimal valorMinimo = tabelaTaxaAdministracao.ValorMinimo.Value;
                decimal valorMaximo = tabelaTaxaAdministracao.ValorMaximo.Value;
                DateTime? dataFim = tabelaTaxaAdministracao.DataFim;
                #endregion

                //Se já tiver registro faz update, senão insert do primeiro registro                    
                DateTime dataFimApropriacao = new DateTime();
                DateTime dataPagamento = new DateTime();
                decimal valorAcumulado = 0;
                decimal valorDia = 0;

                //Se a tabela de admnistração fizer parte de taxa global, não efetua o lançamento
                if (this.CheckTaxaGlobal(idTabela))
                {
                    CalculoAdministracaoAssociada calculoAdministracao = null;

                    if (!dataFim.HasValue || data <= dataFim.Value)
                    {

                        #region Cálculo da taxa administração
                        if (tipoCalculo == (int)TipoCalculoAdministracao.ValorFixo)
                        {

                            if (!tabelaTaxaAdministracao.ValorFixoTotal.HasValue)
                            {
                                throw new TabelaTaxaAdministracaoParametrosNulosException("Valor fixo de Taxa Administração nulo.");
                            }

                            calculoAdministracao = CalculoAdministracaoAssociada.CalculaTxAdmValorFixo(tabelaTaxaAdministracao,
                                                                      idTabela,
                                                                      idCadastro,
                                                                      contagemDias,
                                                                      numeroMesesRenovacao,
                                                                      idLocal,
                                                                      data,
                                                                      idCarteira);
                        }
                        else
                        {

                            calculoAdministracao = CalculoAdministracaoAssociada.CalculaTxAdmPercPL(tabelaTaxaAdministracao,
                                                                   idTabela,
                                                                   idCadastro,
                                                                   contagemDias,
                                                                   numeroMesesRenovacao,
                                                                   idLocal,
                                                                   data,
                                                                   idCarteira,
                                                                   cliente,
                                                                   isIniciando);

                        }

                        if (calculoAdministracao != null)
                        {
                            dataFimApropriacao = calculoAdministracao.DataFimApropriacao.Value;
                            dataPagamento = calculoAdministracao.DataPagamento.Value;
                            valorAcumulado = calculoAdministracao.ValorAcumulado.Value;
                            valorDia = calculoAdministracao.ValorDia.Value;

                            calculoAdministracao.Save();

                        }

                        #region Testa valores mínimo e máximo (somente se for dia anterior ao dia de fim de apropriação)
                        if (valorMinimo != 0 || valorMaximo != 0 || tabelaTaxaAdministracao.ValorFixoTotal.HasValue)
                        {
                            DateTime dataPosterior = new DateTime();
                            if (idLocal == (short)LocalFeriadoFixo.Brasil)
                            {
                                dataPosterior = Calendario.AdicionaDiaUtil(data, 1);
                            }
                            else
                            {
                                dataPosterior = Calendario.AdicionaDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                            }

                            if (DateTime.Compare(dataPosterior, dataFimApropriacao) == 0)
                            {
                                bool atualizar = false;
                                if (valorMinimo != 0 && valorAcumulado < valorMinimo)
                                {
                                    valorAcumulado = valorMinimo;
                                    atualizar = true;
                                }

                                if (valorMaximo != 0 && valorAcumulado > valorMaximo)
                                {
                                    valorAcumulado = valorMaximo;
                                    atualizar = true;
                                }

                                if (tabelaTaxaAdministracao.ValorFixoTotal.HasValue &&
                                    tabelaTaxaAdministracao.ValorFixoTotal.Value > valorAcumulado)
                                {
                                    valorAcumulado = tabelaTaxaAdministracao.ValorFixoTotal.Value;
                                    atualizar = true;
                                }


                                //Atualiza de novo a CalculoAdministracao
                                if (atualizar)
                                {
                                    CalculoAdministracaoAssociada calculoAdministracaoUpdate = new CalculoAdministracaoAssociada();
                                    calculoAdministracaoUpdate.LoadByPrimaryKey(idTabela);
                                    calculoAdministracaoUpdate.ValorAcumulado = valorAcumulado;
                                    calculoAdministracaoUpdate.Save();
                                }
                            }
                        }
                        #endregion
                        #endregion
                    }
                }
                else
                {

                    CalculoAdministracao calculoAdministracao = null;

                    if (!dataFim.HasValue || data <= dataFim.Value)
                    {

                        #region Cálculo da taxa administração
                        if (tipoCalculo == (int)TipoCalculoAdministracao.ValorFixo)
                        {

                            if (!tabelaTaxaAdministracao.ValorFixoTotal.HasValue)
                            {
                                throw new TabelaTaxaAdministracaoParametrosNulosException("Valor fixo de Taxa Administração nulo.");
                            }

                            calculoAdministracao = CalculaTxAdmValorFixo(tabelaTaxaAdministracao,
                                                                      idTabela,
                                                                      idCadastro,
                                                                      contagemDias,
                                                                      numeroMesesRenovacao,
                                                                      idLocal,
                                                                      data,
                                                                      idCarteira);
                        }
                        else
                        {
                            calculoAdministracao = CalculaTxAdmPercPL(tabelaTaxaAdministracao,
                                                                      idLocal,
                                                                      data,
                                                                      idCarteira,
                                                                      cliente,
                                                                      isIniciando);
                        }

                        if (calculoAdministracao != null)
                        {
                            dataFimApropriacao = calculoAdministracao.DataFimApropriacao.Value;
                            dataPagamento = calculoAdministracao.DataPagamento.Value;
                            valorAcumulado = calculoAdministracao.ValorAcumulado.Value;
                            valorDia = calculoAdministracao.ValorDia.Value;

                            calculoAdministracao.Save();

                        }

                        #region Testa valores mínimo e máximo (somente se for dia anterior ao dia de fim de apropriação)
                        if (valorMinimo != 0 || valorMaximo != 0 || tabelaTaxaAdministracao.ValorFixoTotal.HasValue)
                        {
                            DateTime dataPosterior = new DateTime();
                            if (idLocal == (short)LocalFeriadoFixo.Brasil)
                            {
                                dataPosterior = Calendario.AdicionaDiaUtil(data, 1);
                            }
                            else
                            {
                                dataPosterior = Calendario.AdicionaDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                            }

                            if (DateTime.Compare(dataPosterior, dataFimApropriacao) == 0)
                            {
                                bool atualizar = false;
                                if (valorMinimo != 0 && valorAcumulado < valorMinimo)
                                {
                                    valorAcumulado = valorMinimo;
                                    atualizar = true;
                                }

                                if (valorMaximo != 0 && valorAcumulado > valorMaximo)
                                {
                                    valorAcumulado = valorMaximo;
                                    atualizar = true;
                                }

                                if (tabelaTaxaAdministracao.ValorFixoTotal.HasValue &&
                                                                   tabelaTaxaAdministracao.ValorFixoTotal.Value > valorAcumulado)
                                {
                                    valorAcumulado = tabelaTaxaAdministracao.ValorFixoTotal.Value;
                                    atualizar = true;
                                }


                                //Atualiza de novo a CalculoAdministracao
                                if (atualizar)
                                {
                                    CalculoAdministracao calculoAdministracaoUpdate = new CalculoAdministracao();
                                    calculoAdministracaoUpdate.LoadByPrimaryKey(idTabela);
                                    calculoAdministracaoUpdate.ValorAcumulado = valorAcumulado;
                                    calculoAdministracaoUpdate.Save();
                                }
                            }
                        }
                        #endregion
                        #endregion
                    }
                }

                if (dataFim.HasValue && dataFim == data)
                {
                    //Se a tabela de admnistração fizer parte de taxa global, não efetua o lançamento
                    if (this.CheckTaxaGlobal(idTabela))
                    {
                        continue;
                    }

                    if (tipoCalculo == (int)TipoCalculoAdministracao.PercentualPLOuValorFixo)
                    {
                        if (valorAcumulado < tabelaTaxaAdministracao.ValorFixoTotal)
                        {
                            valorAcumulado = tabelaTaxaAdministracao.ValorFixoTotal.Value;
                        }
                    }

                    //Se a tabela de admnistração for taxa global, seleciona as taxas filhas para efetuar pagamento
                    TaxaGlobalCollection taxaGlobalCollection = new TaxaGlobalCollection();
                    taxaGlobalCollection.Query.Select(taxaGlobalCollection.Query.IdTabelaAssociada, taxaGlobalCollection.Query.Prioridade);
                    taxaGlobalCollection.Query.Where(taxaGlobalCollection.Query.IdTabelaGlobal.Equal(idTabela));
                    taxaGlobalCollection.Query.OrderBy(taxaGlobalCollection.Query.Prioridade.Ascending);
                    taxaGlobalCollection.Query.Load();

                    if (taxaGlobalCollection.Count > 0)
                    {
                        this.geraLancamentoTaxaGlobal(taxaGlobalCollection, valorAcumulado, idTabela, idCarteira, data, dataFimApropriacao, dataPagamento);
                    }
                    else
                    {
                        this.setLancamento(idTabela, idCarteira, data, valorAcumulado);
                    }

                    continue;

                }
                else if (dataFim.HasValue && data > dataFim)
                {
                    //Deleta CalculoPerformance
                    listaDeletar.Add(idTabela);
                }                

                if (liquidaCC && valorAcumulado != 0)
                {

                    //Se a tabela de admnistração fizer parte de taxa global, não efetua o lançamento
                    if (this.CheckTaxaGlobal(idTabela))
                    {
                        continue;
                    }

                    #region Insert em Liquidacao
                    valorAcumulado = valorAcumulado * -1;

                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                    //
                    #region Insert na Liquidacao (valor da provisão)
                    string descricao = tabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Descricao + " (Vl Dia: " + valorDia.ToString() + ")";
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataPagamento;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorAcumulado;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.IdConta = idContaDefault;

                    // Taxa Administracao
                    if (tabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo == 1)
                    {
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao;
                    }
                    // Taxa Gestão
                    else if (tabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo == 2)
                    {
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaGestao;
                    }
                    // Taxa Custódia
                    else if (tabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo == 3)
                    {
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaCustodia;
                    }


                    liquidacao.Save();
                    #endregion
                    #endregion
                }
            }

            if (listaDeletar.Count > 0)
            {
                #region Deleta CalculoAdministracao vencendo
                //TaxaGlobalQuery taxaGlobalQuery = new TaxaGlobalQuery();
                //calculoAdministracaoAssociadaCollectionDeletar.Query.InnerJoin(taxaGlobalQuery).On(calculoAdministracaoAssociadaCollectionDeletar.Query.IdTabela == taxaGlobalQuery.IdTabelaAssociada);
                //calculoAdministracaoAssociadaCollectionDeletar.Query.Where(taxaGlobalQuery.IdTabelaGlobal.In(listaDeletar));
                //calculoAdministracaoAssociadaCollectionDeletar.Query.Load();
                //calculoAdministracaoAssociadaCollectionDeletar.MarkAllAsDeleted();
                //calculoAdministracaoAssociadaCollectionDeletar.Save();

                calculoAdministracaoCollectionDeletar.Query.Select(calculoAdministracaoCollectionDeletar.Query.IdTabela);
                calculoAdministracaoCollectionDeletar.Query.Where(calculoAdministracaoCollectionDeletar.Query.IdTabela.In(listaDeletar));
                calculoAdministracaoCollectionDeletar.Query.Load();
                calculoAdministracaoCollectionDeletar.MarkAllAsDeleted();
                calculoAdministracaoCollectionDeletar.Save();
                #endregion
            }
            
        }

        private void geraLancamentoTaxaGlobal(TaxaGlobalCollection taxaGlobalCollection, 
                                              decimal valorAcumulado, 
                                              int idTabela, 
                                              int idCarteira, 
                                              DateTime data,
                                              DateTime dataFimApropriacao,
                                              DateTime dataPagamento)
        {

            decimal valorLancamento = valorAcumulado;
            int index = 0;
            foreach (TaxaGlobal taxaGlobal in taxaGlobalCollection)
            {
                TaxaGlobalQuery taxaGlobalQuery = new TaxaGlobalQuery("TG");
                CalculoAdministracaoAssociadaCollection calculoAdministracaoAssociadaCollection = new CalculoAdministracaoAssociadaCollection();
                calculoAdministracaoAssociadaCollection.Query.InnerJoin(taxaGlobalQuery).On(calculoAdministracaoAssociadaCollection.Query.IdTabela.Equal(taxaGlobalQuery.IdTabelaAssociada));
                calculoAdministracaoAssociadaCollection.Query.Where(taxaGlobalQuery.IdTabelaGlobal.Equal(idTabela),
                                                         taxaGlobalQuery.Prioridade.Equal(taxaGlobal.Prioridade));
                calculoAdministracaoAssociadaCollection.Query.Load();

                decimal valorPagar = 0;

                TabelaTaxaAdministracao tabelaTxAdm = new TabelaTaxaAdministracao();

                //Efetua a soma dos valores acumulados verificando o tipo de calculo utilizado 
                foreach (CalculoAdministracaoAssociada calculoAdministracaoAssociada in calculoAdministracaoAssociadaCollection)
                {
                    tabelaTxAdm.LoadByPrimaryKey((int)calculoAdministracaoAssociada.IdTabela);
                    if (tabelaTxAdm.TipoCalculo == (int)TipoCalculoAdministracao.PercentualPLOuValorFixo && calculoAdministracaoAssociada.ValorAcumulado < tabelaTxAdm.ValorFixoTotal)
                    {
                        valorPagar = valorPagar + Convert.ToDecimal(tabelaTxAdm.ValorFixoTotal);
                    }
                    else
                    {
                        valorPagar = valorPagar + Convert.ToDecimal(calculoAdministracaoAssociada.ValorAcumulado);
                    }
                }

                if (valorLancamento > 0)
                {

                    CalculoAdministracaoAssociada calculoAdministracaoAssociada = new CalculoAdministracaoAssociada();
                    calculoAdministracaoAssociada.Query.Where(calculoAdministracaoAssociada.Query.IdTabela.Equal(taxaGlobal.IdTabelaAssociada.Value));
                    if (calculoAdministracaoAssociada.Query.Load())
                    {
                        valorLancamento = calculoAdministracaoAssociada.setLancamento(idCarteira, 
                                                                                      data, 
                                                                                      valorLancamento, 
                                                                                      index, 
                                                                                      valorPagar, 
                                                                                      (calculoAdministracaoAssociadaCollection.Count > 1));
                        index++;
                    }
                }

                //Zera os valores acumulados e diário (administração e CPMF)
                CalculoAdministracaoAssociada calcAdmAssoc = new CalculoAdministracaoAssociada();
                calcAdmAssoc.LoadByPrimaryKey(taxaGlobal.IdTabelaAssociada.Value);
                calcAdmAssoc.ValorAcumulado = 0;
                calcAdmAssoc.ValorCPMFAcumulado = 0;
                calcAdmAssoc.ValorDia = 0;
                calcAdmAssoc.DataPagamento = dataPagamento;
                calcAdmAssoc.DataFimApropriacao = dataFimApropriacao;
                calcAdmAssoc.Save();
                

            }
        }

        private void setLancamento(int idTabela, int idCarteira, DateTime data, decimal valorAcumulado)
        {
            //Na Data Fim joga em Liquidacao o valor a pagar
            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
            calculoAdministracao.Query.Where(calculoAdministracao.Query.IdTabela.Equal(idTabela));
            if (calculoAdministracao.Query.Load())
            {
                #region Insert na Liquidacao (valor da provisão) - "Pagamento de Taxa de Administração"
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                //

                //string descricao = "Pagamento de Taxa de Administração";
                string descricao = "Pagamento de " + calculoAdministracao.UpToTabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Descricao;
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = calculoAdministracao.DataPagamento.Value;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valorAcumulado * -1;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCarteira;
                liquidacao.IdConta = idContaDefault;

                // Taxa Administracao
                if (calculoAdministracao.UpToTabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo == 1)
                {
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao;
                }
                // Taxa Gestão
                else if (calculoAdministracao.UpToTabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo == 2)
                {
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao;
                }
                // Taxa Custódia
                else if (calculoAdministracao.UpToTabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo == 3)
                {
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaCustodia;
                }

                liquidacao.Save();
                #endregion
            }
        }


        private Boolean CheckTaxaGlobal(int idTabela)
        {
            TaxaGlobal taxaGlobal = new TaxaGlobal();
            taxaGlobal.Query.Where(taxaGlobal.Query.IdTabelaAssociada.Equal(idTabela));
            if(taxaGlobal.Query.Load()){
                return true;
            }
            return false;
        } 

        private CalculoAdministracao CalculaTxAdmValorFixo(TabelaTaxaAdministracao tabelaTaxaAdministracao,
                                                           int idTabela,
                                                           int idCadastro,
                                                           int contagemDias,
                                                           int numeroMesesRenovacao,
                                                           short idLocal,
                                                           DateTime data,
                                                           int idCarteira)
        {
            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();

            //Se já tiver registro faz update, senão insert do primeiro registro                    
            DateTime dataFimApropriacao = new DateTime();
            DateTime dataPagamento = new DateTime();
            decimal valorAcumulado = 0;
            decimal valorDia = 0;

            #region Cálculo da Taxa Administração por Valor Fixo
            decimal valorTotal = tabelaTaxaAdministracao.ValorFixoTotal.Value;

            if (valorTotal > 0)
            {
                if (calculoAdministracao.LoadByPrimaryKey(idTabela))
                {
                    #region Update
                    dataPagamento = calculoAdministracao.DataPagamento.Value;
                    valorAcumulado = calculoAdministracao.ValorAcumulado.Value;
                    int numeroDias;
                    int numeroDiasNaoUteis = 1;
                    if (contagemDias == (int)ContagemDiasProvisao.Uteis)
                    {

                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao,
                                                                LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                        if (idLocal == (short)LocalFeriadoFixo.Brasil)
                        {
                            numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else
                        {
                            numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, (int)idLocal, TipoFeriado.Outros);
                        }
                    }
                    else
                    {
                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaCorridoMes(data, 1);

                        //Para dias corridos, é preciso contar tb final de semana e feriado anterior
                        DateTime dataUtilAnterior = new DateTime();
                        if (idLocal == (short)LocalFeriadoFixo.Brasil)
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                        }

                        DateTime dataInicioContagem = dataUtilAnterior;
                        if (DateTime.Compare(tabelaTaxaAdministracao.DataReferencia.Value, dataUtilAnterior) > 0)
                            dataInicioContagem = tabelaTaxaAdministracao.DataReferencia.Value;
                        else
                            dataInicioContagem = dataInicioContagem.AddDays(1);

                        numeroDias = Calendario.NumeroDias(dataInicioContagem, dataFimApropriacao);
                        if (dataUtilAnterior.Month != data.Month)
                        {
                            dataUtilAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                            numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            if (DateTime.Compare(tabelaTaxaAdministracao.DataReferencia.Value, dataUtilAnterior) > 0)
                            {
                                dataUtilAnterior = tabelaTaxaAdministracao.DataReferencia.Value;
                                numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            }
                        }
                        if (DateTime.Compare(dataUtilAnterior, data) < 0) numeroDiasNaoUteis = Calendario.NumeroDias(dataUtilAnterior, data);
                    }

                    decimal valorApropriar;
                    if (numeroDias == 0)
                    {
                        valorApropriar = 0;
                        valorDia = 0;
                    }
                    else
                    {
                        //Faz os devidos cálculos para o caso de Provisão ou Diferido
                        valorApropriar = valorTotal - valorAcumulado;
                        valorDia = Math.Round(valorApropriar / numeroDias, 2);
                        valorDia = valorDia * numeroDiasNaoUteis;
                        valorAcumulado += valorDia;
                    }

                    calculoAdministracao.ValorDia = valorDia;
                    calculoAdministracao.ValorAcumulado = valorAcumulado;
                    calculoAdministracao.ValorCPMFAcumulado = 0;
                                calculoAdministracao.ValorBase = valorTotal;
                    #endregion
                }
                else
                {
                    //Cria novo registro, calculando as datas de fim de apropriação e de pagamento
                    #region Insert
                    int ano = data.Year;
                    int mes = data.Month;

                    int numeroDias;
                    int numeroDiasNaoUteis = 1;
                    if (contagemDias == (int)ContagemDiasProvisao.Uteis)
                    {
                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao,
                                                                LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                        numeroDias = Calendario.NumeroDias(data, dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    { //Para dias corridos, й preciso contar tb final de semana e feriado anterior

                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaCorridoMes(data, 1);

                        DateTime dataUtilAnterior = new DateTime();
                        if (idLocal == (short)LocalFeriadoFixo.Brasil)
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else
                        {
                            dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                        }
                        DateTime dataInicioContagem = dataUtilAnterior;
                        if (DateTime.Compare(tabelaTaxaAdministracao.DataReferencia.Value, dataUtilAnterior) > 0)
                            dataInicioContagem = tabelaTaxaAdministracao.DataReferencia.Value;
                        else
                            dataInicioContagem = dataInicioContagem.AddDays(1);

                        numeroDias = Calendario.NumeroDias(dataInicioContagem, dataFimApropriacao);

                        if (dataUtilAnterior.Month != data.Month)
                        {
                            dataUtilAnterior = Calendario.RetornaPrimeiroDiaCorridoMes(data, 0);
                            numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            if (DateTime.Compare(tabelaTaxaAdministracao.DataReferencia.Value, dataUtilAnterior) > 0)
                            {
                                dataUtilAnterior = tabelaTaxaAdministracao.DataReferencia.Value;
                                numeroDias = Calendario.NumeroDias(dataUtilAnterior, dataFimApropriacao);
                            }
                        }
                        if (DateTime.Compare(dataUtilAnterior, data) < 0) numeroDiasNaoUteis = Calendario.NumeroDias(dataUtilAnterior, data);
                    }

                    decimal valorApropriar;
                    //Faz os devidos cálculos para o caso de Provisão ou Diferido
                    valorApropriar = valorTotal;
                    valorDia = Math.Round(valorApropriar / numeroDias, 2);
                    valorDia = valorDia * numeroDiasNaoUteis;
                    valorAcumulado = valorDia;

                    //Calcula data de pagamento
                    int numeroDiasPagamento = tabelaTaxaAdministracao.NumeroDiasPagamento.Value;
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento,
                                                                        LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    #region Insert em CalculoAdministracao
                    calculoAdministracao = new CalculoAdministracao();
                    calculoAdministracao.IdTabela = idTabela;
                    calculoAdministracao.IdCarteira = idCarteira;
                    calculoAdministracao.ValorDia = valorDia;
                    calculoAdministracao.ValorAcumulado = valorAcumulado;
                    calculoAdministracao.DataFimApropriacao = dataFimApropriacao;
                    calculoAdministracao.DataPagamento = dataPagamento;
                    calculoAdministracao.ValorCPMFDia = 0;
                    calculoAdministracao.ValorCPMFAcumulado = 0;
                    calculoAdministracao.ValorBase = valorTotal;
                    #endregion
                    #endregion
                }
            }
            return calculoAdministracao;
            #endregion
        }


        public CalculoAdministracao CalculaTxAdmPercPL(TabelaTaxaAdministracao tabelaTaxaAdministracao,
                                                        short idLocal,
                                                        DateTime data,
                                                        int idCarteira,
                                                        Cliente cliente,
                                                        bool isIniciando)
        {

            int idTabela = tabelaTaxaAdministracao.IdTabela.Value;
            int idCadastro = tabelaTaxaAdministracao.IdCadastro.Value;
            int contagemDias = tabelaTaxaAdministracao.ContagemDias.Value;
            int numeroMesesRenovacao = tabelaTaxaAdministracao.NumeroMesesRenovacao.Value;

            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();

            //Se já tiver registro faz update, senão insert do primeiro registro                    
            DateTime dataFimApropriacao = new DateTime();
            DateTime dataPagamento = new DateTime();
            decimal valorAcumulado = 0;
            decimal valorDia = 0;


            #region Cálculo da Taxa Administração por % do PL
            #region Variáveis para cálculo de % sobre o PL
            if (!tabelaTaxaAdministracao.TipoApropriacao.HasValue || !tabelaTaxaAdministracao.Taxa.HasValue ||
                !tabelaTaxaAdministracao.BaseApuracao.HasValue || !tabelaTaxaAdministracao.BaseAno.HasValue ||
                !tabelaTaxaAdministracao.TipoLimitePL.HasValue)
            {
                throw new TabelaTaxaAdministracaoParametrosNulosException("Parâmetros da tabela de Taxa Administração nulos.");
            }
            int tipoApropriacao = tabelaTaxaAdministracao.TipoApropriacao.Value; //Exponencial ou Linear
            decimal taxa = tabelaTaxaAdministracao.Taxa.Value;
            List<TabelaEscalonamentoAdm> taxas = new List<TabelaEscalonamentoAdm>();
            int baseApuracao = tabelaTaxaAdministracao.BaseApuracao.Value; //Patr.Liquido/Patr.Bruto D0/D-1, 
            int baseAno = tabelaTaxaAdministracao.BaseAno.Value; //252, 360, 365, dias uteis ano
            int tipoLimitePL = tabelaTaxaAdministracao.TipoLimitePL.Value; //Total do PL, Limite Inferior
            #endregion

            DateTime dataAnterior = new DateTime();

            if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_DiaAnterior ||
                         baseApuracao == (int)BaseApuracaoAdministracao.PL_DiaAnterior)
            {
                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                }
            }

            bool historico = baseApuracao != (int)BaseApuracaoAdministracao.PBruto_Dia;
            DateTime dataAvaliacao = historico ? dataAnterior : data;

            decimal valorPL = 0;

            if (tabelaTaxaAdministracao.TipoGrupo.HasValue)
            {
                valorPL = RetornaValorMercadoSegmento(tabelaTaxaAdministracao, dataAvaliacao, historico);
            }
            else if (cliente.TipoControle.Value != (byte)TipoControleCliente.Cotista &&
                cliente.TipoControle.Value != (byte)TipoControleCliente.ApenasCotacao)
            {
                #region Busca o valor do PL (Bruto/Liquido, Abertura/Fechamento, D0/D-1)
                HistoricoCota historicoCota = new HistoricoCota();

                #region Busca o tipo de cota
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TipoCota);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                int tipoCota = carteira.TipoCota.Value;
                #endregion

                bool PLInexistente = false;
                if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_Dia)
                {
                    historicoCota.BuscaValorPatrimonioDia(idCarteira, data);
                    valorPL = historicoCota.PatrimonioBruto.Value;

                    if (cliente.IdTipo == TipoClienteFixo.FDIC)
                    {
                        PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                        decimal valorPLFilhas = Math.Abs(posicaoRendaFixa.RetornaValorNegativo(idCarteira));

                        valorPL += valorPLFilhas;
                    }
                }
                else if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_DiaAnterior ||
                         baseApuracao == (int)BaseApuracaoAdministracao.PL_DiaAnterior)
                {
                    try
                    {
                        historicoCota.BuscaValorPatrimonioDia(idCarteira, dataAnterior);
                    }
                    catch (Exception)
                    {
                        PLInexistente = true;

                        if (isIniciando)
                        {
                            valorPL = 0;
                        }
                        else
                        {
                            StringBuilder mensagem = new StringBuilder();
                            mensagem.Append("Patrimônio não cadastrado na data ")
                                    .Append(data + " para a carteira " + idCarteira);
                            throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
                        }
                    }

                    if (!PLInexistente)
                    {
                        if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_DiaAnterior)
                        {
                            valorPL = historicoCota.PatrimonioBruto.Value;
                        }
                        else if (baseApuracao == (int)BaseApuracaoAdministracao.PL_DiaAnterior)
                        {
                            if (tipoCota == (int)TipoCotaFundo.Abertura)
                            {
                                valorPL = historicoCota.PLAbertura.Value;
                            }
                            else
                            {
                                valorPL = historicoCota.PLFechamento.Value;
                            }
                        }

                        if (cliente.IdTipo == TipoClienteFixo.FDIC)
                        {
                            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                            decimal valorPLFilhas = Math.Abs(posicaoRendaFixaHistorico.RetornaValorNegativo(idCarteira, dataAnterior));

                            valorPL += valorPLFilhas;
                        }
                    }
                }
                #endregion
            }
            else
            {
                HistoricoCota historicoCota = new HistoricoCota();
                valorPL = historicoCota.RetornaPLComCotaInformada(idCarteira, data, cliente.TipoControle.Value, historico);
            }

            #region Verifica se existem Segmentos ou Ativos para serem excluidos do cálculo
            decimal valorExcluiPL = RetornaValorMercadoExcluiSegmento(tabelaTaxaAdministracao, dataAvaliacao, historico);
            valorPL -= valorExcluiPL;
            #endregion


            #region Tratamento de expurgo da Taxa Adm e faixas de escalonamento
            if (cliente.TipoControle.Value != (byte)TipoControleCliente.Cotista && cliente.TipoControle.Value != (byte)TipoControleCliente.ApenasCotacao)
            {
                //Se tiver algum expurgo (taxa adm calculada por mercado), ajusta o PL
                valorPL -= RetornaValorExpurgoAdministracao(idCarteira, idTabela, dataAvaliacao, historico);

                if (valorPL < 0)
                {
                    valorPL = 0;
                }

                #region Verifica se tem tabela de escalonamento e usa a taxa de acordo com a faixa de PL, se for o caso
                TabelaEscalonamentoAdmCollection tabelaEscalonamentoAdmCollection = new TabelaEscalonamentoAdmCollection();
                //tabelaEscalonamentoAdmCollection.Query.Select(tabelaEscalonamentoAdmCollection.Query.Percentual);
                tabelaEscalonamentoAdmCollection.Query.Where(tabelaEscalonamentoAdmCollection.Query.IdTabela.Equal(idTabela),
                                                             tabelaEscalonamentoAdmCollection.Query.FaixaPL.LessThanOrEqual(valorPL));
                tabelaEscalonamentoAdmCollection.Query.OrderBy(tabelaEscalonamentoAdmCollection.Query.FaixaPL.Descending);
                tabelaEscalonamentoAdmCollection.Query.Load();
                if (tabelaTaxaAdministracao.EscalonaProporcional == "S")
                {
                    foreach (TabelaEscalonamentoAdm tabelaEscalonamentoAdm in tabelaEscalonamentoAdmCollection)
                    {
                        taxas.Add(tabelaEscalonamentoAdm);
                    }
                }
                else
                {
                    if (tabelaEscalonamentoAdmCollection.Count > 0)
                    {
                        taxa = tabelaEscalonamentoAdmCollection[0].Percentual.Value;
                    }
                }
                #endregion

                //Aplica o limite sobre o PL
                if (tipoLimitePL == (int)TipoLimitePLAdministracao.LimiteSuperior)
                {
                    if (!tabelaTaxaAdministracao.ValorLimite.HasValue)
                    {
                        throw new TabelaTaxaAdministracaoParametrosNulosException("Valor de limite superior de Taxa Administração nulo.");
                    }
                    decimal limiteSuperiorPL = tabelaTaxaAdministracao.ValorLimite.Value;

                    if (valorPL > limiteSuperiorPL)
                    {
                        valorPL -= limiteSuperiorPL;
                    }
                }
                else if (tipoLimitePL == (int)TipoLimitePLAdministracao.LimiteInferior)
                {
                    if (!tabelaTaxaAdministracao.ValorLimite.HasValue)
                    {
                        throw new TabelaTaxaAdministracaoParametrosNulosException("Valor de limite superior de Taxa Administração nulo.");
                    }
                    decimal limiteInferiorPL = tabelaTaxaAdministracao.ValorLimite.Value;

                    if (valorPL < limiteInferiorPL)
                    {
                        valorPL = limiteInferiorPL;
                    }
                }
            }
            #endregion

            #region Dedução da base de PL de todas as demais tabelas com tipoGrupo != null
            if (!tabelaTaxaAdministracao.TipoGrupo.HasValue)
            {
                //decimal valorOutros = RetornaValorMercadoOutros(idCarteira, idTabela, dataAvaliacao, historico);
                //valorPL -= valorOutros;
            }
            #endregion
            #endregion

            #region Calcula número de dias (período e Ano)
            int numeroDias;
            if (contagemDias == (int)ContagemDiasProvisao.Corridos)
            {
                //Para dias corridos, é preciso contar tb final de semana e feriado anterior
                DateTime dataUtilAnterior = new DateTime();
                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                }

                DateTime dataInicioContagem = dataUtilAnterior;
                numeroDias = Calendario.NumeroDias(dataInicioContagem, data);
            }
            else
            {
                numeroDias = 1;
            }

            //Calcula o nr de dias úteis para base de cálculo com número exato de dias úteis no ano
            if (baseAno == (int)BaseAnoAdministracao.BaseAnoUteisExato)
            {
                DateTime primeiroDiaUtilAno = new DateTime();
                DateTime ultimoDiaUtilAno = new DateTime();

                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    primeiroDiaUtilAno = Calendario.RetornaPrimeiroDiaUtilAno(data, 0);
                    ultimoDiaUtilAno = Calendario.RetornaUltimoDiaUtilAno(data, 0);
                    baseAno = Calendario.NumeroDias(primeiroDiaUtilAno, ultimoDiaUtilAno, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    primeiroDiaUtilAno = Calendario.RetornaPrimeiroDiaUtilAno(data, 0, (int)idLocal, TipoFeriado.Outros);
                    ultimoDiaUtilAno = Calendario.RetornaUltimoDiaUtilAno(data, 0, (int)idLocal, TipoFeriado.Outros);
                    baseAno = Calendario.NumeroDias(primeiroDiaUtilAno, ultimoDiaUtilAno, (int)idLocal, TipoFeriado.Outros);
                }
            }
            #endregion

            if (calculoAdministracao.LoadByPrimaryKey(idTabela))
            {
                #region Update
                dataFimApropriacao = calculoAdministracao.DataFimApropriacao.Value;
                dataPagamento = calculoAdministracao.DataPagamento.Value;
                valorAcumulado = calculoAdministracao.ValorAcumulado.Value;

                if (tabelaTaxaAdministracao.EscalonaProporcional == "S")
                {
                    valorDia = calculaValorDia(taxas, tipoApropriacao, valorPL, baseAno);
                }
                else
                {
                    valorDia = calculaValorDia(taxa, tipoApropriacao, valorPL, baseAno);
                }

                valorAcumulado += valorDia;

                            calculoAdministracao.ValorBase = valorPL;
                calculoAdministracao.ValorDia = valorDia;
                calculoAdministracao.ValorAcumulado = valorAcumulado;
                calculoAdministracao.ValorCPMFAcumulado = 0;
                #endregion
            }
            else
            {
                //Cria novo registro, calculando as datas de fim de apropriação e de pagamento
                #region Insert
                int ano = data.Year;
                int mes = data.Month;

                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao, (int)idLocal, TipoFeriado.Outros);
                }

                if (tabelaTaxaAdministracao.EscalonaProporcional == "S")
                {
                    valorDia = calculaValorDia(taxas, tipoApropriacao, valorPL, baseAno);
                }
                else
                {
                    valorDia = calculaValorDia(taxa, tipoApropriacao, valorPL, baseAno);
                }

                //Calcula data de pagamento
                int numeroDiasPagamento = tabelaTaxaAdministracao.NumeroDiasPagamento.Value;

                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, (int)idLocal, TipoFeriado.Outros);
                }

                #region Insert em CalculoAdministracao
                calculoAdministracao = new CalculoAdministracao();
                calculoAdministracao.IdTabela = idTabela;
                calculoAdministracao.IdCarteira = idCarteira;
                calculoAdministracao.ValorDia = valorDia;
                calculoAdministracao.ValorAcumulado = valorDia;
                calculoAdministracao.DataFimApropriacao = dataFimApropriacao;
                calculoAdministracao.DataPagamento = dataPagamento;
                calculoAdministracao.ValorCPMFDia = 0;
                calculoAdministracao.ValorCPMFAcumulado = 0;
                calculoAdministracao.ValorBase = valorPL;
                #endregion
                #endregion
            }

            return calculoAdministracao;
        }

        /// <summary>
        /// Efetua calculo de TX adm para carteiras que impactam PL com Rebate
        /// </summary>
        /// <param name="tabelaTaxaAdministracao"></param>
        /// <param name="idLocal"></param>
        /// <param name="data"></param>
        /// <param name="idCarteira"></param>
        /// <param name="cliente"></param>
        /// <param name="isIniciando"></param>
        /// <returns></returns>
        public CalculoAdministracao CalculaTxAdmImpactaPL(TabelaTaxaAdministracao tabelaTaxaAdministracao,
                                                        short idLocal,
                                                        DateTime data,
                                                        int idCarteira,
                                                        Cliente cliente,
                                                        bool isIniciando)
        {

            int idTabela = tabelaTaxaAdministracao.IdTabela.Value;
            int idCadastro = tabelaTaxaAdministracao.IdCadastro.Value;
            int contagemDias = tabelaTaxaAdministracao.ContagemDias.Value;
            int numeroMesesRenovacao = tabelaTaxaAdministracao.NumeroMesesRenovacao.Value;

            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();

            //Se já tiver registro faz update, senão insert do primeiro registro                    
            DateTime dataFimApropriacao = new DateTime();
            DateTime dataPagamento = new DateTime();
            decimal valorAcumulado = 0;
            decimal valorDia = 0;


            #region Cálculo da Taxa Administração por % do PL
            #region Variáveis para cálculo de % sobre o PL
            if (!tabelaTaxaAdministracao.TipoApropriacao.HasValue || !tabelaTaxaAdministracao.Taxa.HasValue ||
                !tabelaTaxaAdministracao.BaseApuracao.HasValue || !tabelaTaxaAdministracao.BaseAno.HasValue ||
                !tabelaTaxaAdministracao.TipoLimitePL.HasValue)
            {
                throw new TabelaTaxaAdministracaoParametrosNulosException("Parâmetros da tabela de Taxa Administração nulos.");
            }
            int tipoApropriacao = tabelaTaxaAdministracao.TipoApropriacao.Value; //Exponencial ou Linear
            decimal taxa = tabelaTaxaAdministracao.Taxa.Value;
            List<TabelaEscalonamentoAdm> taxas = new List<TabelaEscalonamentoAdm>();
            int baseApuracao = tabelaTaxaAdministracao.BaseApuracao.Value; //Patr.Liquido/Patr.Bruto D0/D-1, 
            int baseAno = tabelaTaxaAdministracao.BaseAno.Value; //252, 360, 365, dias uteis ano
            int tipoLimitePL = tabelaTaxaAdministracao.TipoLimitePL.Value; //Total do PL, Limite Inferior
            #endregion

            DateTime dataAnterior = new DateTime();

            if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_DiaAnterior ||
                         baseApuracao == (int)BaseApuracaoAdministracao.PL_DiaAnterior)
            {
                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                }
            }

            bool historico = baseApuracao != (int)BaseApuracaoAdministracao.PBruto_Dia;
            DateTime dataAvaliacao = historico ? dataAnterior : data;

            decimal valorPL = 0;

            if (tabelaTaxaAdministracao.TipoGrupo.HasValue)
            {
                valorPL = RetornaValorMercadoSegmento(tabelaTaxaAdministracao, dataAvaliacao, historico);
            }
            else if (cliente.TipoControle.Value != (byte)TipoControleCliente.Cotista &&
                cliente.TipoControle.Value != (byte)TipoControleCliente.ApenasCotacao)
            {
                #region Busca o valor do PL (Bruto/Liquido, Abertura/Fechamento, D0/D-1)
                HistoricoCota historicoCota = new HistoricoCota();

                #region Busca o tipo de cota
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TipoCota);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                int tipoCota = carteira.TipoCota.Value;
                #endregion

                bool PLInexistente = false;
                if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_Dia)
                {
                    historicoCota.BuscaValorPatrimonioDia(idCarteira, data);
                    valorPL = historicoCota.PatrimonioBruto.Value;

                    if (cliente.IdTipo == TipoClienteFixo.FDIC)
                    {
                        PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                        decimal valorPLFilhas = Math.Abs(posicaoRendaFixa.RetornaValorNegativo(idCarteira));

                        valorPL += valorPLFilhas;
                    }
                }
                else if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_DiaAnterior ||
                         baseApuracao == (int)BaseApuracaoAdministracao.PL_DiaAnterior)
                {
                    try
                    {
                        historicoCota.BuscaValorPatrimonioDia(idCarteira, dataAnterior);
                    }
                    catch (Exception)
                    {
                        PLInexistente = true;

                        if (isIniciando)
                        {
                            valorPL = 0;
                        }
                        else
                        {
                            StringBuilder mensagem = new StringBuilder();
                            mensagem.Append("Patrimônio não cadastrado na data ")
                                    .Append(data + " para a carteira " + idCarteira);
                            throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
                        }
                    }

                    if (!PLInexistente)
                    {
                        if (baseApuracao == (int)BaseApuracaoAdministracao.PBruto_DiaAnterior)
                        {
                            valorPL = historicoCota.PatrimonioBruto.Value;
                        }
                        else if (baseApuracao == (int)BaseApuracaoAdministracao.PL_DiaAnterior)
                        {
                            if (tipoCota == (int)TipoCotaFundo.Abertura)
                            {
                                valorPL = historicoCota.PLAbertura.Value;
                            }
                            else
                            {
                                valorPL = historicoCota.PLFechamento.Value;
                            }
                        }

                        if (cliente.IdTipo == TipoClienteFixo.FDIC)
                        {
                            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                            decimal valorPLFilhas = Math.Abs(posicaoRendaFixaHistorico.RetornaValorNegativo(idCarteira, dataAnterior));

                            valorPL += valorPLFilhas;
                        }
                    }
                }
                #endregion
            }
            else
            {
                HistoricoCota historicoCota = new HistoricoCota();
                //valorPL = historicoCota.RetornaPLComCotaInformada(idCarteira, data, cliente.TipoControle.Value, false);
                valorPL = historicoCota.RetornaPLComCotaInformada(idCarteira, dataAvaliacao, cliente.TipoControle.Value, historico);
            }

            #region Verifica se existem Segmentos ou Ativos para serem excluidos do cálculo
            decimal valorExcluiPL = RetornaValorMercadoExcluiSegmento(tabelaTaxaAdministracao, dataAvaliacao, historico);
            valorPL -= valorExcluiPL;
            #endregion


            #region Tratamento de expurgo da Taxa Adm e faixas de escalonamento
            if (cliente.TipoControle.Value != (byte)TipoControleCliente.Cotista && cliente.TipoControle.Value != (byte)TipoControleCliente.ApenasCotacao)
            {
                //Se tiver algum expurgo (taxa adm calculada por mercado), ajusta o PL
                valorPL -= RetornaValorExpurgoAdministracao(idCarteira, idTabela, dataAvaliacao, historico);

                if (valorPL < 0)
                {
                    valorPL = 0;
                }

                #region Verifica se tem tabela de escalonamento e usa a taxa de acordo com a faixa de PL, se for o caso
                TabelaEscalonamentoAdmCollection tabelaEscalonamentoAdmCollection = new TabelaEscalonamentoAdmCollection();
                //tabelaEscalonamentoAdmCollection.Query.Select(tabelaEscalonamentoAdmCollection.Query.Percentual);
                tabelaEscalonamentoAdmCollection.Query.Where(tabelaEscalonamentoAdmCollection.Query.IdTabela.Equal(idTabela),
                                                             tabelaEscalonamentoAdmCollection.Query.FaixaPL.LessThanOrEqual(valorPL));
                tabelaEscalonamentoAdmCollection.Query.OrderBy(tabelaEscalonamentoAdmCollection.Query.FaixaPL.Descending);
                tabelaEscalonamentoAdmCollection.Query.Load();
                if (tabelaTaxaAdministracao.EscalonaProporcional == "S")
                {
                    foreach (TabelaEscalonamentoAdm tabelaEscalonamentoAdm in tabelaEscalonamentoAdmCollection)
                    {
                        taxas.Add(tabelaEscalonamentoAdm);
                    }
                }
                else
                {
                    if (tabelaEscalonamentoAdmCollection.Count > 0)
                    {
                        taxa = tabelaEscalonamentoAdmCollection[0].Percentual.Value;
                    }
                }
                #endregion

                //Aplica o limite sobre o PL
                if (tipoLimitePL == (int)TipoLimitePLAdministracao.LimiteSuperior)
                {
                    if (!tabelaTaxaAdministracao.ValorLimite.HasValue)
                    {
                        throw new TabelaTaxaAdministracaoParametrosNulosException("Valor de limite superior de Taxa Administração nulo.");
                    }
                    decimal limiteSuperiorPL = tabelaTaxaAdministracao.ValorLimite.Value;

                    if (valorPL > limiteSuperiorPL)
                    {
                        valorPL -= limiteSuperiorPL;
                    }
                }
                else if (tipoLimitePL == (int)TipoLimitePLAdministracao.LimiteInferior)
                {
                    if (!tabelaTaxaAdministracao.ValorLimite.HasValue)
                    {
                        throw new TabelaTaxaAdministracaoParametrosNulosException("Valor de limite superior de Taxa Administração nulo.");
                    }
                    decimal limiteInferiorPL = tabelaTaxaAdministracao.ValorLimite.Value;

                    if (valorPL < limiteInferiorPL)
                    {
                        valorPL = limiteInferiorPL;
                    }
                }
            }
            #endregion

            #region Dedução da base de PL de todas as demais tabelas com tipoGrupo != null
            if (!tabelaTaxaAdministracao.TipoGrupo.HasValue)
            {
                //decimal valorOutros = RetornaValorMercadoOutros(idCarteira, idTabela, dataAvaliacao, historico);
                //valorPL -= valorOutros;
            }
            #endregion
            #endregion

            #region Calcula número de dias (período e Ano)
            int numeroDias;
            if (contagemDias == (int)ContagemDiasProvisao.Corridos)
            {
                //Para dias corridos, é preciso contar tb final de semana e feriado anterior
                DateTime dataUtilAnterior = new DateTime();
                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    dataUtilAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                }

                DateTime dataInicioContagem = dataUtilAnterior;
                numeroDias = Calendario.NumeroDias(dataInicioContagem, data);
            }
            else
            {
                numeroDias = 1;
            }

            //Calcula o nr de dias úteis para base de cálculo com número exato de dias úteis no ano
            if (baseAno == (int)BaseAnoAdministracao.BaseAnoUteisExato)
            {
                DateTime primeiroDiaUtilAno = new DateTime();
                DateTime ultimoDiaUtilAno = new DateTime();

                if (idLocal == (short)LocalFeriadoFixo.Brasil)
                {
                    primeiroDiaUtilAno = Calendario.RetornaPrimeiroDiaUtilAno(data, 0);
                    ultimoDiaUtilAno = Calendario.RetornaUltimoDiaUtilAno(data, 0);
                    baseAno = Calendario.NumeroDias(primeiroDiaUtilAno, ultimoDiaUtilAno, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    primeiroDiaUtilAno = Calendario.RetornaPrimeiroDiaUtilAno(data, 0, (int)idLocal, TipoFeriado.Outros);
                    ultimoDiaUtilAno = Calendario.RetornaUltimoDiaUtilAno(data, 0, (int)idLocal, TipoFeriado.Outros);
                    baseAno = Calendario.NumeroDias(primeiroDiaUtilAno, ultimoDiaUtilAno, (int)idLocal, TipoFeriado.Outros);
                }
            }
            #endregion

            #region Atualiza Objeto de Retorno
            int ano = data.Year;
            int mes = data.Month;

            if (idLocal == (short)LocalFeriadoFixo.Brasil)
            {
                dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            else
            {
                dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao, (int)idLocal, TipoFeriado.Outros);
            }

            if (tabelaTaxaAdministracao.EscalonaProporcional == "S")
            {
                valorDia = calculaValorDia(taxas, tipoApropriacao, valorPL, baseAno);
            }
            else
            {
                valorDia = calculaValorDia(taxa, tipoApropriacao, valorPL, baseAno);
            }

            //Calcula data de pagamento
            int numeroDiasPagamento = tabelaTaxaAdministracao.NumeroDiasPagamento.Value;

            if (idLocal == (short)LocalFeriadoFixo.Brasil)
            {
                dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            else
            {
                dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, (int)idLocal, TipoFeriado.Outros);
            }

            #region Insert em CalculoAdministracao
            calculoAdministracao = new CalculoAdministracao();
            calculoAdministracao.IdTabela = idTabela;
            calculoAdministracao.IdCarteira = idCarteira;
            calculoAdministracao.ValorDia = valorDia;
            calculoAdministracao.ValorAcumulado = valorDia;
            calculoAdministracao.DataFimApropriacao = dataFimApropriacao;
            calculoAdministracao.DataPagamento = dataPagamento;
            #endregion
            #endregion

            return calculoAdministracao;
        }


        private decimal calculaValorDia(decimal taxa, int tipoApropriacao, decimal valorPl, int baseAno)
        {
            decimal taxaDia;
            if (tipoApropriacao == (int)TipoApropriacaoAdministracao.Exponencial)
            {
                taxaDia = CalculoFinanceiro.ConverteBaseTaxa(baseAno, 1, taxa);
            }
            else //Linear
            {
                taxaDia = taxa / baseAno;
            }

            //Cálculo do valor da taxa de administração diária
            return Math.Round(taxaDia / 100 * valorPl, 2);

        }

        private decimal calculaValorDia(List<TabelaEscalonamentoAdm> taxas, int tipoApropriacao, decimal valorPl, int baseAno)
        {
            decimal valorFaixa;
            decimal valorDia = 0; 
            decimal taxaDia;
            foreach (TabelaEscalonamentoAdm tabelaEscalonamentoAdm in taxas)
            {
                if (tipoApropriacao == (int)TipoApropriacaoAdministracao.Exponencial)
                {
                    taxaDia = CalculoFinanceiro.ConverteBaseTaxa(baseAno, 1, Convert.ToDecimal(tabelaEscalonamentoAdm.Percentual));
                }
                else //Linear
                {
                    taxaDia = Convert.ToDecimal(tabelaEscalonamentoAdm.Percentual) / baseAno;
                }

                valorFaixa = valorPl - Convert.ToDecimal(tabelaEscalonamentoAdm.FaixaPL);
                valorDia = valorDia + (Math.Round(taxaDia / 100 * valorFaixa, 2));

                valorPl = valorPl - valorFaixa;

            }
            return valorDia;

        }

        /// <summary>
        /// Zera a CalculoTaxaAdministracao, calculando novas datas de fim de apropriação e pagamento.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="liquidaCC">indica se lanca ou nao em Liquidacao
        public void TrataVencimentoTaxaAdministracao(int idCarteira, DateTime data, bool liquidaCC)
        {
            CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            calculoAdministracaoCollection.BuscaCalculoAdministracaoVencidas(idCarteira, data);

            Cliente cliente = new Cliente();
            short idLocal = 0; //Local (domicilio) para avaliação dos feriados relativos ao cliente
            if (calculoAdministracaoCollection.Count > 0)
            {
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.IdLocal);
                cliente.LoadByPrimaryKey(campos, idCarteira);

                idLocal = cliente.IdLocal.Value;
            }

            int idContaDefault = 0;
            if (calculoAdministracaoCollection.Count > 0)
            {
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
            }
            //

            for (int i = 0; i < calculoAdministracaoCollection.Count; i++)
            {
                #region Carregamento das variáveis locais de CalculoAdministracao
                CalculoAdministracao calculoAdministracao = calculoAdministracaoCollection[i];
                int idTabela = calculoAdministracao.IdTabela.Value;
                DateTime dataPagamento = calculoAdministracao.DataPagamento.Value;
                decimal valorAcumulado = calculoAdministracao.ValorAcumulado.Value;

                if (calculoAdministracao.UpToTabelaTaxaAdministracao.TipoCalculo == (int)TipoCalculoAdministracao.PercentualPLOuValorFixo)
                {
                    if (valorAcumulado < calculoAdministracao.UpToTabelaTaxaAdministracao.ValorFixoTotal)
                    {
                        valorAcumulado = calculoAdministracao.UpToTabelaTaxaAdministracao.ValorFixoTotal.Value;
                    }
                }

                #endregion

                #region Busca informações da TabelaTaxaAdministracao
                TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
                tabelaTaxaAdministracao.LoadByPrimaryKey(idTabela);

                int tipoCalculo = tabelaTaxaAdministracao.TipoCalculo.Value;
                int numeroMesesRenovacao = tabelaTaxaAdministracao.NumeroMesesRenovacao.Value;
                int numeroDiasPagamento = tabelaTaxaAdministracao.NumeroDiasPagamento.Value;
                decimal valorMinimo = tabelaTaxaAdministracao.ValorMinimo.Value; //Não é analisado se estiver zerado
                decimal valorMaximo = tabelaTaxaAdministracao.ValorMaximo.Value; //Não é analisado se estiver zerado

                decimal valorFixoTotal = 0;
                if (tipoCalculo == (int)TipoCalculoAdministracao.ValorFixo)
                {
                    if (!tabelaTaxaAdministracao.ValorFixoTotal.HasValue)
                    {
                        throw new TabelaTaxaAdministracaoParametrosNulosException("Valor fixo de Taxa Administração nulo.");
                    }
                    valorFixoTotal = tabelaTaxaAdministracao.ValorFixoTotal.Value;
                    valorAcumulado = valorFixoTotal; //Para cálculo por valor fixo, é forçado o valor acumulado ao final
                }
                #endregion

                #region Testa valores mínimo e máximo
                if (valorMinimo != 0 && valorAcumulado < valorMinimo)
                {
                    valorAcumulado = valorMinimo;
                }

                if (valorMaximo != 0 && valorAcumulado > valorMaximo)
                {
                    valorAcumulado = valorMaximo;
                }
                #endregion                                

                //Zera os valores acumulados e diário (administração e CPMF)
                calculoAdministracao.ValorAcumulado = 0;
                calculoAdministracao.ValorCPMFAcumulado = 0;
                calculoAdministracao.ValorDia = 0;

                #region Checa se existe dataFim para a provisão
                DateTime dataFim;
                bool terminoProvisao = false;
                if (tabelaTaxaAdministracao.DataFim.HasValue)
                {
                    dataFim = tabelaTaxaAdministracao.DataFim.Value;
                    if (dataFim <= data)
                    {
                        terminoProvisao = true;
                    }
                }
                #endregion

                if (!terminoProvisao)
                {                    
                    #region Atualiza datas de fim de apropriação e de pagamento
                    int ano = data.Year;
                    int mes = data.Month;

                    DateTime dataFimApropriacao;

                    //Calcula novas datas de fim de apropriação e de pagamento
                    if (idLocal == (short)LocalFeriadoFixo.Brasil)
                    {
                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao, (int)idLocal, TipoFeriado.Outros);
                        dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento, (int)idLocal, TipoFeriado.Outros);
                    }

                    calculoAdministracao.DataFimApropriacao = dataFimApropriacao;
                    calculoAdministracao.DataPagamento = dataPagamento;
                    #endregion

                    if (liquidaCC && valorAcumulado != 0)
                    {

                        //Se a tabela de admnistração for taxa global, seleciona as taxas filhas para efetuar pagamento
                        TaxaGlobalCollection taxaGlobalCollection = new TaxaGlobalCollection();
                        taxaGlobalCollection.Query.Select(taxaGlobalCollection.Query.IdTabelaAssociada, taxaGlobalCollection.Query.Prioridade);
                        taxaGlobalCollection.Query.Where(taxaGlobalCollection.Query.IdTabelaGlobal.Equal(idTabela));
                        taxaGlobalCollection.Query.OrderBy(taxaGlobalCollection.Query.Prioridade.Ascending);
                        taxaGlobalCollection.Query.Load();

                        if (taxaGlobalCollection.Count > 0)
                        {
                            this.geraLancamentoTaxaGlobal(taxaGlobalCollection, valorAcumulado, idTabela, idCarteira, data, dataFimApropriacao, dataPagamento);
                        }
                        else
                        {
                            this.setLancamento(idTabela, idCarteira, data, valorAcumulado);
                        }
                    }
                }                
            }

            //Save das collections
            calculoAdministracaoCollection.Save();
            //liquidacaoCollection.Save();
            
        }

        /// <summary>
        /// Função básica para copiar de CalculoAdministracao para CalculoAdministracaoHistorico e CalculoAdministracaoAssociada para CalculoAdministracaoAssociadaHistorico
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCarteira, DateTime data)
        {
            try
            {
                CalculoAdministracaoHistoricoCollection calculoAdministracaoDeletarHistoricoCollection = new CalculoAdministracaoHistoricoCollection();
                calculoAdministracaoDeletarHistoricoCollection.DeletaCalculoAdministracaoHistoricoDataHistoricoMaiorIgual(idCarteira, data);
            }
            catch { }

            CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();
            calculoAdministracaoCollection.BuscaCalculoAdministracaoCompleta(idCarteira);
            //
            CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();
            //
            #region Copia de CalculoAdministracao para CalculoAdministracaoHistorico
            for (int i = 0; i < calculoAdministracaoCollection.Count; i++)
            {
                CalculoAdministracao calculoAdministracao = calculoAdministracaoCollection[i];

                CalculoAdministracaoHistorico calculoAdministracaoHistorico = calculoAdministracaoHistoricoCollection.AddNew();

                calculoAdministracaoHistorico.DataHistorico = data;
                calculoAdministracaoHistorico.IdTabela = calculoAdministracao.IdTabela;
                calculoAdministracaoHistorico.IdCarteira = calculoAdministracao.IdCarteira;
                calculoAdministracaoHistorico.ValorDia = calculoAdministracao.ValorDia;
                calculoAdministracaoHistorico.ValorAcumulado = calculoAdministracao.ValorAcumulado;
                calculoAdministracaoHistorico.DataFimApropriacao = calculoAdministracao.DataFimApropriacao;
                calculoAdministracaoHistorico.DataPagamento = calculoAdministracao.DataPagamento;
                calculoAdministracaoHistorico.ValorCPMFDia = calculoAdministracao.ValorCPMFDia;
                calculoAdministracaoHistorico.ValorCPMFAcumulado = calculoAdministracao.ValorCPMFAcumulado;
                calculoAdministracaoHistorico.ValorBase = calculoAdministracao.ValorBase;
            }
            #endregion

            calculoAdministracaoHistoricoCollection.Save();


            try
            {
                CalculoAdministracaoAssociadaHistoricoCollection calculoAdministracaoAssociadaDeletarHistoricoCollection = new CalculoAdministracaoAssociadaHistoricoCollection();
                calculoAdministracaoAssociadaDeletarHistoricoCollection.DeletaCalculoAdministracaoHistoricoDataHistoricoMaiorIgual(idCarteira, data);
            }
            catch { }

            CalculoAdministracaoAssociadaCollection calculoAdministracaoAssociadaCollection = new CalculoAdministracaoAssociadaCollection();
            calculoAdministracaoAssociadaCollection.BuscaCalculoAdministracaoCompleta(idCarteira);
            //
            CalculoAdministracaoAssociadaHistoricoCollection calculoAdministracaoAssociadaHistoricoCollection = new CalculoAdministracaoAssociadaHistoricoCollection();
            //
            #region Copia de CalculoAdministracaoAssociada para CalculoAdministracaoAssociadaHistorico
            for (int i = 0; i < calculoAdministracaoAssociadaCollection.Count; i++)
            {
                CalculoAdministracaoAssociada calculoAdministracaoAssociada = calculoAdministracaoAssociadaCollection[i];

                CalculoAdministracaoAssociadaHistorico calculoAdministracaoAssociadaHistorico = calculoAdministracaoAssociadaHistoricoCollection.AddNew();

                calculoAdministracaoAssociadaHistorico.DataHistorico = data;
                calculoAdministracaoAssociadaHistorico.IdTabela = calculoAdministracaoAssociada.IdTabela;
                calculoAdministracaoAssociadaHistorico.IdCarteira = calculoAdministracaoAssociada.IdCarteira;
                calculoAdministracaoAssociadaHistorico.ValorDia = calculoAdministracaoAssociada.ValorDia;
                calculoAdministracaoAssociadaHistorico.ValorAcumulado = calculoAdministracaoAssociada.ValorAcumulado;
                calculoAdministracaoAssociadaHistorico.DataFimApropriacao = calculoAdministracaoAssociada.DataFimApropriacao;
                calculoAdministracaoAssociadaHistorico.DataPagamento = calculoAdministracaoAssociada.DataPagamento;
                calculoAdministracaoAssociadaHistorico.ValorCPMFDia = calculoAdministracaoAssociada.ValorCPMFDia;
                calculoAdministracaoAssociadaHistorico.ValorCPMFAcumulado = calculoAdministracaoAssociada.ValorCPMFAcumulado;
            }
            #endregion

            calculoAdministracaoAssociadaHistoricoCollection.Save();

        }

        /// <summary>
        /// Retorna o valor de administração acumulado (total) para o idCarteira.
        /// O parâmetro tipo é passado para informar se busca total de administração ou de gestão.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>        
        public decimal RetornaValorAdministracao(int idCarteira, int tipo)
        {
            CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();

            calculoAdministracaoCollection.Query
                 .Select(calculoAdministracaoCollection.Query.IdTabela, calculoAdministracaoCollection.Query.ValorAcumulado)
                 .Where(calculoAdministracaoCollection.Query.IdCarteira == idCarteira);

            calculoAdministracaoCollection.Query.Load();

            decimal valor = 0;
            for (int i = 0; i < calculoAdministracaoCollection.Count; i++)
            {
                int idTabela = calculoAdministracaoCollection[i].IdTabela.Value;

                TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
                tabelaTaxaAdministracao.LoadByPrimaryKey(idTabela);

                int tipoTaxa = tabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo.Value;

                if (tipo == tipoTaxa)
                {
                    valor += calculoAdministracaoCollection[i].ValorAcumulado.Value;
                }
            }

            return valor;
        }

        /// <summary>
        /// Retorna o valor de administração dia (total) para o idCarteira.
        /// O parâmetro tipo é passado para informar se busca total de administração ou de gestão.        
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>        
        public decimal RetornaValorAdministracaoDia(int idCarteira, byte tipo)
        {
            CalculoAdministracaoQuery calculoAdministracaoQuery = new CalculoAdministracaoQuery("C");
            CadastroTaxaAdministracaoQuery cadastroTaxaAdministracaoQuery = new CadastroTaxaAdministracaoQuery("A");
            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");

            calculoAdministracaoQuery.Select(calculoAdministracaoQuery.ValorDia.Sum());
            calculoAdministracaoQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(tabelaTaxaAdministracaoQuery.IdTabela == calculoAdministracaoQuery.IdTabela);
            calculoAdministracaoQuery.InnerJoin(cadastroTaxaAdministracaoQuery).On(cadastroTaxaAdministracaoQuery.IdCadastro == tabelaTaxaAdministracaoQuery.IdCadastro);
            calculoAdministracaoQuery.Where(tabelaTaxaAdministracaoQuery.IdCarteira.Equal(idCarteira));
            calculoAdministracaoQuery.Where(cadastroTaxaAdministracaoQuery.Tipo.Equal(tipo));

            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
            calculoAdministracao.Load(calculoAdministracaoQuery);

            decimal valor = 0;
            if (calculoAdministracao.ValorDia.HasValue)
            {
                valor = calculoAdministracao.ValorDia.Value;
            }

            return valor;
        }

        /// <summary>
        /// Expurga do cálculo da tx adm os mercados listados em TabelaExpurgoAdministracao
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idTabela"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static decimal RetornaValorExpurgoAdministracao(int idCliente, int idTabela, DateTime data, bool historico)
        {
            TabelaExpurgoAdministracaoCollection tabelaExpurgoAdministracaoCollection = new TabelaExpurgoAdministracaoCollection();
            tabelaExpurgoAdministracaoCollection.Query.Where(tabelaExpurgoAdministracaoCollection.Query.IdTabela.Equal(idTabela),
                                                             tabelaExpurgoAdministracaoCollection.Query.DataReferencia.LessThanOrEqual(data));
            tabelaExpurgoAdministracaoCollection.Query.Load();

            decimal valorTotal = 0;
            foreach (TabelaExpurgoAdministracao tabelaExpurgoAdministracao in tabelaExpurgoAdministracaoCollection)
            {
                int tipo = tabelaExpurgoAdministracao.Tipo.Value;
                string codigo = tabelaExpurgoAdministracao.Codigo;

                if (tipo == (byte)TipoGrupoAdm.Acoes)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Acoes_Ativo)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo),
                                                 posicaoBolsa.DataHistorico.Equals(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Opcoes)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Opcoes_Ativo)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Fundos)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                            posicaoFundoCollection.Query.Quantidade);
                        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                        posicaoFundoCollection.Query.Load();

                        foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                        posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                            posicaoFundoCollection.Query.Quantidade);
                        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                           posicaoFundoCollection.Query.DataHistorico.Equal(data));
                        posicaoFundoCollection.Query.Load();

                        foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Fundos_Ativo)
                {
                    int idCarteira = 0;
                    if (Utilitario.IsInteger(codigo))
                    {
                        idCarteira = Convert.ToInt32(codigo);

                        #region
                        if (!historico)
                        {
                            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                                posicaoFundoCollection.Query.Quantidade);
                            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                               posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira));
                            posicaoFundoCollection.Query.Load();

                            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                            {
                                decimal cotaDia = posicaoFundo.CotaDia.Value;
                                decimal quantidade = posicaoFundo.Quantidade.Value;

                                valorTotal += Math.Round(cotaDia * quantidade, 2);
                            }
                        }
                        else
                        {
                            PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                                posicaoFundoCollection.Query.Quantidade);
                            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                               posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                               posicaoFundoCollection.Query.DataHistorico.Equal(data));
                            posicaoFundoCollection.Query.Load();

                            foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                            {
                                decimal cotaDia = posicaoFundo.CotaDia.Value;
                                decimal quantidade = posicaoFundo.Quantidade.Value;

                                valorTotal += Math.Round(cotaDia * quantidade, 2);
                            }
                        }
                        #endregion
                    }
                }
                else if (tipo == (byte)TipoGrupoAdm.RendaFixa)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.PUMercado,
                                                                posicaoRendaFixaCollection.Query.Quantidade);
                        posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0));
                        posicaoRendaFixaCollection.Query.Load();

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.PUMercado,
                                                                posicaoRendaFixaCollection.Query.Quantidade);
                        posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0),
                                                               posicaoRendaFixaCollection.Query.DataHistorico.Equal(data));
                        posicaoRendaFixaCollection.Query.Load();

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.RendaFixa_Papel)
                {
                    #region
                    int idPapel = 0;
                    if (Utilitario.IsInteger(codigo))
                    {
                        idPapel = Convert.ToInt32(codigo);
                    }

                    if (!historico)
                    {
                        PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    papelRendaFixaQuery.IdPapel.Equal(idPapel));
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    papelRendaFixaQuery.IdPapel.Equal(idPapel),
                                                    posicaoRendaFixaQuery.DataHistorico.Equal(data));
                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.RendaFixa_Titulo)
                {
                    #region
                    int idTitulo = 0;
                    if (Utilitario.IsInteger(codigo))
                    {
                        idTitulo = Convert.ToInt32(codigo);
                    }

                    if (!historico)
                    {
                        PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo));
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo),
                                                    posicaoRendaFixaQuery.DataHistorico.Equal(data));
                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    #endregion
                }
            }

            return valorTotal;
        }

        /// <summary>
        /// Calcula o valor de mercado para as demais tabelas de taxa adminisração do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idTabela"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <returns></returns>
        public static decimal RetornaValorMercadoOutros(int idCliente, int idTabela, DateTime data, bool historico)
        {
            TabelaTaxaAdministracaoCollection tabelaTaxaAdministracaoCollection = new TabelaTaxaAdministracaoCollection();
            tabelaTaxaAdministracaoCollection.Query.Where(tabelaTaxaAdministracaoCollection.Query.IdTabela.NotEqual(idTabela),
                                                          tabelaTaxaAdministracaoCollection.Query.DataReferencia.LessThanOrEqual(data),
                                                          tabelaTaxaAdministracaoCollection.Query.IdCarteira.Equal(idCliente),
                                                          tabelaTaxaAdministracaoCollection.Query.TipoGrupo.IsNotNull());
            tabelaTaxaAdministracaoCollection.Query.Load();

            decimal valorTotal = 0;
            foreach (TabelaTaxaAdministracao tabelaTaxaAdministracao in tabelaTaxaAdministracaoCollection)
            {
                int tipo = tabelaTaxaAdministracao.TipoGrupo.Value;
                string codigo = tabelaTaxaAdministracao.CodigoAtivo;

                if (tipo == (byte)TipoGrupoAdm.Acoes)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Acoes_Ativo)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Opcoes)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Opcoes_Ativo)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Fundos)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                            posicaoFundoCollection.Query.Quantidade);
                        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                        posicaoFundoCollection.Query.Load();

                        foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                        posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                            posicaoFundoCollection.Query.Quantidade);
                        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                           posicaoFundoCollection.Query.DataHistorico.Equal(data));
                        posicaoFundoCollection.Query.Load();

                        foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.Fundos_Ativo)
                {
                    int idCarteira = 0;
                    if (Utilitario.IsInteger(codigo))
                    {
                        idCarteira = Convert.ToInt32(codigo);

                        #region
                        if (!historico)
                        {
                            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                                posicaoFundoCollection.Query.Quantidade);
                            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                               posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira));
                            posicaoFundoCollection.Query.Load();

                            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                            {
                                decimal cotaDia = posicaoFundo.CotaDia.Value;
                                decimal quantidade = posicaoFundo.Quantidade.Value;

                                valorTotal += Math.Round(cotaDia * quantidade, 2);
                            }
                        }
                        else
                        {
                            PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                                posicaoFundoCollection.Query.Quantidade);
                            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                               posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                               posicaoFundoCollection.Query.DataHistorico.Equal(data));
                            posicaoFundoCollection.Query.Load();

                            foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                            {
                                decimal cotaDia = posicaoFundo.CotaDia.Value;
                                decimal quantidade = posicaoFundo.Quantidade.Value;

                                valorTotal += Math.Round(cotaDia * quantidade, 2);
                            }
                        }
                        #endregion
                    }
                }
                else if (tipo == (byte)TipoGrupoAdm.RendaFixa)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.PUMercado,
                                                                posicaoRendaFixaCollection.Query.Quantidade);
                        posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0));
                        posicaoRendaFixaCollection.Query.Load();

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.PUMercado,
                                                                posicaoRendaFixaCollection.Query.Quantidade);
                        posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0),
                                                               posicaoRendaFixaCollection.Query.DataHistorico.Equal(data));
                        posicaoRendaFixaCollection.Query.Load();

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.RendaFixa_Papel)
                {
                    #region
                    int idPapel = 0;
                    if (Utilitario.IsInteger(codigo))
                    {
                        idPapel = Convert.ToInt32(codigo);
                    }

                    if (!historico)
                    {
                        PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    papelRendaFixaQuery.IdPapel.Equal(idPapel));
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    papelRendaFixaQuery.IdPapel.Equal(idPapel),
                                                    posicaoRendaFixaQuery.DataHistorico.Equal(data));
                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoAdm.RendaFixa_Titulo)
                {
                    #region
                    int idTitulo = 0;
                    if (Utilitario.IsInteger(codigo))
                    {
                        idTitulo = Convert.ToInt32(codigo);
                    }

                    if (!historico)
                    {
                        PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo));
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo),
                                                    posicaoRendaFixaQuery.DataHistorico.Equal(data));
                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    #endregion
                }
            }

            return valorTotal;
        }

        /// <summary>
        /// Calcula o valor de mercado para o grupo ou ativo passado.
        /// </summary>
        /// <param name="tabelaTaxaAdministracao"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <returns></returns>
        public static decimal RetornaValorMercadoSegmento(TabelaTaxaAdministracao tabelaTaxaAdministracao, DateTime data, bool historico)
        {
            int tipo = tabelaTaxaAdministracao.TipoGrupo.Value;
            string codigo = tabelaTaxaAdministracao.CodigoAtivo;
            int idCliente = tabelaTaxaAdministracao.IdCarteira.Value;
            decimal valorTotal = 0;

            if (tipo == (byte)TipoGrupoAdm.Acoes)
            {
                #region
                if (!historico)
                {
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.ValorMercado.HasValue)
                    {
                        valorTotal += posicaoBolsa.ValorMercado.Value;
                    }
                }
                else
                {
                    PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                             posicaoBolsa.Query.DataHistorico.Equal(data));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.ValorMercado.HasValue)
                    {
                        valorTotal += posicaoBolsa.ValorMercado.Value;
                    }
                }
                #endregion
            }
            else if (tipo == (byte)TipoGrupoAdm.Acoes_Ativo)
            {
                #region
                if (!historico)
                {
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                             posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.ValorMercado.HasValue)
                    {
                        valorTotal += posicaoBolsa.ValorMercado.Value;
                    }
                }
                else
                {
                    PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                             posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo),
                                             posicaoBolsa.Query.DataHistorico.Equal(data));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.ValorMercado.HasValue)
                    {
                        valorTotal += posicaoBolsa.ValorMercado.Value;
                    }
                }
                #endregion
            }
            else if (tipo == (byte)TipoGrupoAdm.Opcoes)
            {
                #region
                if (!historico)
                {
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.ValorMercado.HasValue)
                    {
                        valorTotal += posicaoBolsa.ValorMercado.Value;
                    }
                }
                else
                {
                    PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                             posicaoBolsa.Query.DataHistorico.Equal(data));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.ValorMercado.HasValue)
                    {
                        valorTotal += posicaoBolsa.ValorMercado.Value;
                    }
                }
                #endregion
            }
            else if (tipo == (byte)TipoGrupoAdm.Opcoes_Ativo)
            {
                #region
                if (!historico)
                {
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                             posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.ValorMercado.HasValue)
                    {
                        valorTotal += posicaoBolsa.ValorMercado.Value;
                    }
                }
                else
                {
                    PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                    posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                    posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                             posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo),
                                             posicaoBolsa.Query.DataHistorico.Equal(data));
                    posicaoBolsa.Query.Load();

                    if (posicaoBolsa.ValorMercado.HasValue)
                    {
                        valorTotal += posicaoBolsa.ValorMercado.Value;
                    }
                }
                #endregion
            }
            else if (tipo == (byte)TipoGrupoAdm.Fundos)
            {
                #region
                if (!historico)
                {
                    PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                    posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                        posicaoFundoCollection.Query.Quantidade);
                    posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                       posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                    posicaoFundoCollection.Query.Load();

                    foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                    {
                        decimal cotaDia = posicaoFundo.CotaDia.Value;
                        decimal quantidade = posicaoFundo.Quantidade.Value;

                        valorTotal += Math.Round(cotaDia * quantidade, 2);
                    }
                }
                else
                {
                    PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                    posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                        posicaoFundoCollection.Query.Quantidade);
                    posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                       posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                       posicaoFundoCollection.Query.DataHistorico.Equal(data));
                    posicaoFundoCollection.Query.Load();

                    foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                    {
                        decimal cotaDia = posicaoFundo.CotaDia.Value;
                        decimal quantidade = posicaoFundo.Quantidade.Value;

                        valorTotal += Math.Round(cotaDia * quantidade, 2);
                    }
                }
                #endregion
            }
            else if (tipo == (byte)TipoGrupoAdm.Fundos_Ativo)
            {
                int idCarteira = 0;
                if (Utilitario.IsInteger(codigo))
                {
                    idCarteira = Convert.ToInt32(codigo);

                    #region
                    if (!historico)
                    {
                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                            posicaoFundoCollection.Query.Quantidade);
                        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                           posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira));
                        posicaoFundoCollection.Query.Load();

                        foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                        posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                            posicaoFundoCollection.Query.Quantidade);
                        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                           posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                           posicaoFundoCollection.Query.DataHistorico.Equal(data));
                        posicaoFundoCollection.Query.Load();

                        foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        }
                    }
                    #endregion
                }
            }
            else if (tipo == (byte)TipoGrupoAdm.RendaFixa)
            {
                #region
                if (!historico)
                {
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.PUMercado,
                                                            posicaoRendaFixaCollection.Query.Quantidade);
                    posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0));
                    posicaoRendaFixaCollection.Query.Load();

                    foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                    {
                        decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                        decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                        valorTotal += Math.Round(puMercado * quantidade, 2);
                    }
                }
                else
                {
                    PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                    posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.PUMercado,
                                                            posicaoRendaFixaCollection.Query.Quantidade);
                    posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0),
                                                           posicaoRendaFixaCollection.Query.DataHistorico.Equal(data));
                    posicaoRendaFixaCollection.Query.Load();

                    foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                    {
                        decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                        decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                        valorTotal += Math.Round(puMercado * quantidade, 2);
                    }
                }
                #endregion
            }
            else if (tipo == (byte)TipoGrupoAdm.RendaFixa_Papel)
            {
                #region
                int idPapel = 0;
                if (Utilitario.IsInteger(codigo))
                {
                    idPapel = Convert.ToInt32(codigo);
                }

                if (!historico)
                {
                    PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                    TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                    PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                    posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                 posicaoRendaFixaQuery.Quantidade);
                    posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                    posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                    posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                papelRendaFixaQuery.IdPapel.Equal(idPapel));
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                    foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                    {
                        decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                        decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                        valorTotal += Math.Round(puMercado * quantidade, 2);
                    }
                }
                else
                {
                    PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                    TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                    PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                    posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                 posicaoRendaFixaQuery.Quantidade);
                    posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                    posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                    posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                papelRendaFixaQuery.IdPapel.Equal(idPapel),
                                                posicaoRendaFixaQuery.DataHistorico.Equal(data));
                    PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                    posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                    foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                    {
                        decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                        decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                        valorTotal += Math.Round(puMercado * quantidade, 2);
                    }
                }
                #endregion
            }
            else if (tipo == (byte)TipoGrupoAdm.RendaFixa_Titulo)
            {
                #region
                int idTitulo = 0;
                if (Utilitario.IsInteger(codigo))
                {
                    idTitulo = Convert.ToInt32(codigo);
                }

                if (!historico)
                {
                    PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                    TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                    posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                 posicaoRendaFixaQuery.Quantidade);
                    posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                    posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo));
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                    foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                    {
                        decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                        decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                        valorTotal += Math.Round(puMercado * quantidade, 2);
                    }
                }
                else
                {
                    PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                    TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                    posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                 posicaoRendaFixaQuery.Quantidade);
                    posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                    posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo),
                                                posicaoRendaFixaQuery.DataHistorico.Equal(data));
                    PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                    posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                    foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                    {
                        decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                        decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                        valorTotal += Math.Round(puMercado * quantidade, 2);
                    }
                }
                #endregion
            }
            
            return valorTotal;
        }

        /// <summary>
        /// Consulta valores de ativos que devem ser excluidos do cálculo
        /// </summary>
        /// <param name="tabelaTaxaAdministracao"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <returns></returns>
        public static decimal RetornaValorMercadoExcluiSegmento(TabelaTaxaAdministracao tabelaTaxaAdministracao, DateTime data, bool historico)
        {

            decimal valorTotal = 0;
            int idCliente = tabelaTaxaAdministracao.IdCarteira.Value;

            TabelaExcecaoAdmCollection tabelaExcecaoAdmCollection = new TabelaExcecaoAdmCollection();
            tabelaExcecaoAdmCollection.Query.Select(tabelaExcecaoAdmCollection.Query.TipoGrupo,
                                                    tabelaExcecaoAdmCollection.Query.CodigoAtivo);
            tabelaExcecaoAdmCollection.Query.Where(tabelaExcecaoAdmCollection.Query.IdTabela.Equal(tabelaTaxaAdministracao.IdTabela));
            tabelaExcecaoAdmCollection.Query.Load();
            foreach(TabelaExcecaoAdm tabelaExcecaoAdm in tabelaExcecaoAdmCollection)
            {
                int tipo = tabelaExcecaoAdm.TipoGrupo.Value;
                string codigo = tabelaExcecaoAdm.CodigoAtivo;

                if (tipo == (byte)TipoGrupoExcecaoAdm.Acoes)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoExcecaoAdm.Acoes_Ativo)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoExcecaoAdm.Opcoes)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoExcecaoAdm.Opcoes_Ativo)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    else
                    {
                        PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                        posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                        posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                 posicaoBolsa.Query.CdAtivoBolsa.Equal(codigo),
                                                 posicaoBolsa.Query.DataHistorico.Equal(data));
                        posicaoBolsa.Query.Load();

                        if (posicaoBolsa.ValorMercado.HasValue)
                        {
                            valorTotal += posicaoBolsa.ValorMercado.Value;
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoExcecaoAdm.Fundos)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                            posicaoFundoCollection.Query.Quantidade);
                        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                        posicaoFundoCollection.Query.Load();

                        foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                        posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                            posicaoFundoCollection.Query.Quantidade);
                        posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                           posicaoFundoCollection.Query.DataHistorico.Equal(data));
                        posicaoFundoCollection.Query.Load();

                        foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoExcecaoAdm.Fundos_Ativo)
                {
                    int idCarteira = 0;
                    if (Utilitario.IsInteger(codigo))
                    {
                        idCarteira = Convert.ToInt32(codigo);

                        #region
                        if (!historico)
                        {
                            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                                posicaoFundoCollection.Query.Quantidade);
                            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                               posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira));
                            posicaoFundoCollection.Query.Load();

                            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                            {
                                decimal cotaDia = posicaoFundo.CotaDia.Value;
                                decimal quantidade = posicaoFundo.Quantidade.Value;

                                valorTotal += Math.Round(cotaDia * quantidade, 2);
                            }
                        }
                        else
                        {
                            PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.CotaDia,
                                                                posicaoFundoCollection.Query.Quantidade);
                            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoFundoCollection.Query.Quantidade.NotEqual(0),
                                                               posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                               posicaoFundoCollection.Query.DataHistorico.Equal(data));
                            posicaoFundoCollection.Query.Load();

                            foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                            {
                                decimal cotaDia = posicaoFundo.CotaDia.Value;
                                decimal quantidade = posicaoFundo.Quantidade.Value;

                                valorTotal += Math.Round(cotaDia * quantidade, 2);
                            }
                        }
                        #endregion
                    }
                }
                else if (tipo == (byte)TipoGrupoExcecaoAdm.RendaFixa)
                {
                    #region
                    if (!historico)
                    {
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.PUMercado,
                                                                posicaoRendaFixaCollection.Query.Quantidade);
                        posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0));
                        posicaoRendaFixaCollection.Query.Load();

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.PUMercado,
                                                                posicaoRendaFixaCollection.Query.Quantidade);
                        posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0),
                                                               posicaoRendaFixaCollection.Query.DataHistorico.Equal(data));
                        posicaoRendaFixaCollection.Query.Load();

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoExcecaoAdm.RendaFixa_Papel)
                {
                    #region
                    int idPapel = 0;
                    if (Utilitario.IsInteger(codigo))
                    {
                        idPapel = Convert.ToInt32(codigo);
                    }

                    if (!historico)
                    {
                        PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    papelRendaFixaQuery.IdPapel.Equal(idPapel));
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    papelRendaFixaQuery.IdPapel.Equal(idPapel),
                                                    posicaoRendaFixaQuery.DataHistorico.Equal(data));
                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoExcecaoAdm.RendaFixa_Titulo)
                {
                    #region
                    int idTitulo = 0;
                    if (Utilitario.IsInteger(codigo))
                    {
                        idTitulo = Convert.ToInt32(codigo);
                    }

                    if (!historico)
                    {
                        PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo));
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    else
                    {
                        PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

                        posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.PUMercado,
                                                     posicaoRendaFixaQuery.Quantidade);
                        posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                        posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo),
                                                    posicaoRendaFixaQuery.DataHistorico.Equal(data));
                        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                        posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                        foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                        {
                            decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

                            valorTotal += Math.Round(puMercado * quantidade, 2);
                        }
                    }
                    #endregion
                }
                else if (tipo == (byte)TipoGrupoExcecaoAdm.Fundos_Gestor)
                { 

                    #region
                    if (!historico)
                    {
                        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("AMQ");
                        CarteiraQuery carteiraQuery = new CarteiraQuery("CQ");
                        PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("PFQ");
                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        posicaoFundoQuery.Select(posicaoFundoQuery.CotaDia, posicaoFundoQuery.Quantidade);
                        posicaoFundoQuery.InnerJoin(carteiraQuery).On(posicaoFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
                        posicaoFundoQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteGestor);
                        posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente),
                                                posicaoFundoQuery.Quantidade.NotEqual(0),
                                                agenteMercadoQuery.IdAgente == codigo,
                                                carteiraQuery.ExcecaoRegraTxAdm != "S");
                        posicaoFundoCollection.Load(posicaoFundoQuery);

                        foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        } 
                    }
                    else
                    {
                        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("AMQ");
                        CarteiraQuery carteiraQuery = new CarteiraQuery("CQ");
                        PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("PFHQ");
                        PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
                        posicaoFundoQuery.Select(posicaoFundoQuery.CotaDia, posicaoFundoQuery.Quantidade);
                        posicaoFundoQuery.InnerJoin(carteiraQuery).On(posicaoFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
                        posicaoFundoQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteGestor);
                        posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente),
                                                posicaoFundoQuery.Quantidade.NotEqual(0),
                                                agenteMercadoQuery.IdAgente == codigo,
                                                posicaoFundoQuery.DataHistorico.Equal(data),
                                                carteiraQuery.ExcecaoRegraTxAdm != "S");
                        posicaoFundoCollection.Load(posicaoFundoQuery);

                        foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
                        {
                            decimal cotaDia = posicaoFundo.CotaDia.Value;
                            decimal quantidade = posicaoFundo.Quantidade.Value;

                            valorTotal += Math.Round(cotaDia * quantidade, 2);
                        }
                    }
                    #endregion
                }
            }
            return valorTotal;
        }
    }
}
