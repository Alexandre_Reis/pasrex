﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Fundo.Enums;
using Financial.Common.Enums;
using Financial.Common;
using Financial.Investidor;

namespace Financial.Fundo
{
	public partial class PosicaoFundoHistorico : esPosicaoFundoHistorico
	{
        /// <summary>
        /// Retorna o total de valor bruto do cliente em todos as carteiras onde ele tenha posição.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorBruto(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna o total de valor bruto total da carteira em todos os clientes que tenham posição.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorBrutoCarteira(int idCarteira, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna o total de valor bruto do cliente em todos as carteiras onde ele tenha posição.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="tipoCarteira"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorBruto(int idCliente, byte tipoCarteira, DateTime dataHistorico)
        {
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");

            posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.ValorBruto.Sum());
            posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
            posicaoFundoHistoricoQuery.Where(carteiraQuery.TipoCarteira.Equal(tipoCarteira));
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente));
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataHistorico.Equal(dataHistorico));

            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

            decimal valor = 0;
            if (posicaoFundoHistoricoCollection[0].ValorBruto.HasValue)
            {
                valor = posicaoFundoHistoricoCollection[0].ValorBruto.Value;
            }
            
            return valor;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto dado o idCarteira e o idCliente
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        /// 
        public decimal RetornaSumValorBruto(int idCarteira, int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Bruto dado o idCarteira
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        /// 
        //public decimal RetornaSumValorBruto(int idCarteira, DateTime dataHistorico) {

        //    this.QueryReset();
        //    this.Query
        //         .Select(this.query.ValorBruto.Sum())
        //         .Where(this.query.IdCarteira == idCarteira,
        //                this.Query.DataHistorico.Equal(dataHistorico));

        //    this.Query.Load();

        //    #region logSql
        //    if (log.IsInfoEnabled) {
        //        string sql = this.query.es.LastQuery;
        //        sql = sql.Replace("@IdCarteira1", "'" + idCarteira + "'");
        //        sql = sql.Replace("@DataHistorico", "'" + dataHistorico.ToString("yyyy-MM-dd") + "'");
        //        log.Info(sql);
        //    }
        //    #endregion

        //    return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        //}

        /// <summary>
        /// Retorna a Soma do Valor Liquido dado o idCarteira e uma data
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaSumValorLiquido(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCarteira == idCarteira &
                        this.Query.DataHistorico == data &
                        this.Query.Quantidade != 0);

            this.Query.Load();

            return this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
        }

        /// <summary>
        /// Retorna o ValorBruto das posicoes de PosicaoFundo, filtrando a carteira e todas as posicoes de cliente cuja dataDia seja = data passada.
        /// Considera apenas fundos com nr de dias para liquidação de resgate menor ou igual que o nr de dias passado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico, int diasLiquidacao)
        {
            decimal valorTotal = 0;

            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();

            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");

            posicaoFundoHistoricoQuery.Select(carteiraQuery.IdCarteira,
                                     carteiraQuery.DiasLiquidacaoResgate,
                                     carteiraQuery.DiasCotizacaoResgate,
                                     carteiraQuery.ContagemDiasConversaoResgate,
                                     posicaoFundoHistoricoQuery.ValorBruto.Sum());
            posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente),
                                             posicaoFundoHistoricoQuery.Quantidade.GreaterThan(0),
                                             posicaoFundoHistoricoQuery.DataHistorico.Equal(dataHistorico));
            posicaoFundoHistoricoQuery.GroupBy(carteiraQuery.IdCarteira,
                                      carteiraQuery.DiasLiquidacaoResgate,
                                      carteiraQuery.DiasCotizacaoResgate,
                                      carteiraQuery.ContagemDiasConversaoResgate);
            posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

            foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
            {
                decimal valor = posicaoFundoHistorico.ValorBruto.Value;

                byte contagemDias = Convert.ToByte(posicaoFundoHistorico.GetColumn(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate));
                int diasLiquidacaoFundo = Convert.ToInt32(posicaoFundoHistorico.GetColumn(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate));
                int diasConversaoFundo = Convert.ToInt32(posicaoFundoHistorico.GetColumn(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate));

                if (contagemDias == (byte)ContagemDiasLiquidacaoResgate.DiasCorridos)
                {
                    diasLiquidacaoFundo += diasConversaoFundo;
                }

                if (diasLiquidacao >= diasLiquidacaoFundo)
                {
                    valorTotal += valor;
                }
            }

            return valorTotal;
        }


        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorBruto.Sum());
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                               posicaoFundoCollection.Query.DataHistorico.Equal(data),
                posicaoFundoCollection.Query.Quantidade.GreaterThan(0));
            posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCarteira);
            posicaoFundoCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoFundoCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
            {
                int idCarteira = posicaoFundo.IdCarteira.Value;
                decimal valor = posicaoFundo.ValorBruto.Value;

                Cliente clienteFundo = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteFundo.Query.IdMoeda);
                clienteFundo.LoadByPrimaryKey(campos, idCarteira);
                int idMoedaFundo = clienteFundo.IdMoeda.Value;

                if (idMoedaCliente != idMoedaFundo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaFundo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaFundo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }


        /// <summary>
        /// Retorna o valor de mercado liquido do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercadoLiquido(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorLiquido.Sum());
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                               posicaoFundoCollection.Query.DataHistorico.Equal(data),
                posicaoFundoCollection.Query.Quantidade.GreaterThan(0));
            posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCarteira);
            posicaoFundoCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoFundoCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoFundoHistorico posicaoFundo in posicaoFundoCollection)
            {
                int idCarteira = posicaoFundo.IdCarteira.Value;
                decimal valor = posicaoFundo.ValorLiquido.Value;

                Cliente clienteFundo = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteFundo.Query.IdMoeda);
                clienteFundo.LoadByPrimaryKey(campos, idCarteira);
                int idMoedaFundo = clienteFundo.IdMoeda.Value;

                if (idMoedaCliente != idMoedaFundo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaFundo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaFundo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }
        
	}
}
