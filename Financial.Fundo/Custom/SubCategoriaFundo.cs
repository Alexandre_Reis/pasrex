/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           22/2/2007 16:16:45
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
	public partial class SubCategoriaFundo : esSubCategoriaFundo
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idSubCategoria"></param>
        /// <param name="descricao"></param>
        /// <param name="idLista"></param>
        public void CriaSubCategoraFundo(int idCategoria, int idSubCategoria, string descricao)
        {
            StringBuilder sqlBuilder = new StringBuilder();
            esParameters esParams = new esParameters();

            esParams.Add(SubCategoriaFundoMetadata.ColumnNames.IdCategoria, idCategoria);
            esParams.Add(SubCategoriaFundoMetadata.ColumnNames.Descricao, descricao);
            esParams.Add(SubCategoriaFundoMetadata.ColumnNames.IdSubCategoria, idSubCategoria);

            sqlBuilder.Append("SET IDENTITY_INSERT SubCategoriaFundo ON ");

            sqlBuilder.Append("INSERT INTO SubCategoriaFundo (");
            sqlBuilder.Append(SubCategoriaFundoMetadata.ColumnNames.IdCategoria + ", ");
            sqlBuilder.Append(SubCategoriaFundoMetadata.ColumnNames.Descricao + ", ");
            sqlBuilder.Append(SubCategoriaFundoMetadata.ColumnNames.IdSubCategoria);
            sqlBuilder.Append(") VALUES ( ");
            sqlBuilder.Append("@" + SubCategoriaFundoMetadata.ColumnNames.IdCategoria);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + SubCategoriaFundoMetadata.ColumnNames.Descricao);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + SubCategoriaFundoMetadata.ColumnNames.IdSubCategoria);
            sqlBuilder.Append(")");

            sqlBuilder.Append("SET IDENTITY_INSERT SubCategoriaFundo OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
        }
	}
}
