﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
    public partial class CalculoPerformanceCollection : esCalculoPerformanceCollection
    {
        // Construtor
        // Cria uma nova CalculoPerformanceCollection com os dados de CalculoPerformanceHistoricoCollection
        // Todos do dados de CalculoPerformanceHistoricoCollection são copiados com excessão da dataHistorico
        public CalculoPerformanceCollection(CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection)
        {
            for (int i = 0; i < calculoPerformanceHistoricoCollection.Count; i++)
            {
                //
                CalculoPerformance p = new CalculoPerformance();

                // Para cada Coluna de CalculoPerformanceHistorico copia para CalculoPerformance
                foreach (esColumnMetadata colPosicaoHistorico in calculoPerformanceHistoricoCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != CalculoPerformanceHistoricoMetadata.ColumnNames.DataHistorico)
                    {
                        esColumnMetadata colCalculoPerformance = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (calculoPerformanceHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null)
                        {
                            p.SetColumn(colCalculoPerformance.Name, calculoPerformanceHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Carrega o objeto CalculoPerformanceCollection com todos os campos de CalculoPerformance.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="dataFimApropriacao">filtra performance já vencidas (inclusive vcto em dia não útil)</param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaCalculoPerformanceVencida(int idCarteira, DateTime dataFimApropriacao)
        {
            CalculoPerformanceQuery calculoPerformanceQuery = new CalculoPerformanceQuery("C");
            TabelaTaxaPerformanceQuery tabelaTaxaPerformanceQuery = new TabelaTaxaPerformanceQuery("T");

            calculoPerformanceQuery.Select(calculoPerformanceQuery);
            calculoPerformanceQuery.InnerJoin(tabelaTaxaPerformanceQuery).On(tabelaTaxaPerformanceQuery.IdTabela == calculoPerformanceQuery.IdTabela);
            calculoPerformanceQuery.Where(calculoPerformanceQuery.IdCarteira.Equal(idCarteira) &
                                          calculoPerformanceQuery.DataFimApropriacao.LessThanOrEqual(dataFimApropriacao)
                                          );

            bool retorno = this.Load(calculoPerformanceQuery);

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto CalculoPerformanceCollection com todos os campos de CalculoPerformance.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaCalculoPerformanceCompleta(int idCarteira)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCarteira == idCarteira);
            this.Query.Load();
        }

        /// <summary>
        /// Deleta todos os registros de cálculo da carteira passada.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaCalculoPerformance(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTabela)
                 .Where(this.Query.IdCarteira == idCarteira);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Insere novas registros na CalculoPerformance a partir do histórico
        /// (CalculoPerformanceHistorico) da data informada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void InsereCalculoPerformanceHistorico(int idCarteira, DateTime data)
        {
            this.QueryReset();
            CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();
            calculoPerformanceHistoricoCollection.BuscaCalculoPerformanceHistoricoCompleta(idCarteira, data);
            for (int i = 0; i < calculoPerformanceHistoricoCollection.Count; i++)
            {
                CalculoPerformanceHistorico calculoPerformanceHistorico = calculoPerformanceHistoricoCollection[i];

                // Copia para CalculoPerformance
                CalculoPerformance calculoPerformance = new CalculoPerformance();
                calculoPerformance.IdTabela = calculoPerformanceHistorico.IdTabela;
                calculoPerformance.IdCarteira = calculoPerformanceHistorico.IdCarteira;
                calculoPerformance.ValorAcumulado = calculoPerformanceHistorico.ValorAcumulado;
                calculoPerformance.DataFimApropriacao = calculoPerformanceHistorico.DataFimApropriacao;
                calculoPerformance.DataPagamento = calculoPerformanceHistorico.DataPagamento;
                calculoPerformance.ValorCPMFAcumulado = calculoPerformanceHistorico.ValorCPMFAcumulado;
                calculoPerformance.ValorDia = calculoPerformanceHistorico.ValorDia;
                //
                this.AttachEntity(calculoPerformance);
            }

            this.Save();
        }
    }
}
