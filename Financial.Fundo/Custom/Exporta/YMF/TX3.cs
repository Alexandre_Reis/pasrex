﻿using System;
using System.Web;
using Financial.ContaCorrente;
using Financial.Investidor;
using System.IO;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using System.Collections.Generic;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Threading;
using System.Globalization;
using System.Data.SqlClient;
using Financial.ContaCorrente.Enums;

namespace Financial.Fundo.Exportacao.YMF
{
    /// <summary>
    /// Summary description for TX3
    /// </summary>
    public class TX3
    {

        private DateTime DataExportacao;
        private class TotalOperacoesCotistas
        {
            public decimal VlAplicacoes;
            public decimal QtCotasEmitidas;
            public decimal VlCustoResgates;
            public decimal VlResgates;
            public decimal VlIR;
            public decimal QtCotasResgatadas;
            public decimal VlResgatesIR;
            public decimal VlCustoResgatesIR;
            public decimal QtCotasResgatesIR;
            public decimal VlPerformance;
            public decimal VlIOF;
            public decimal VlAmortizacao;
            public decimal VlAmortizacaoJuros;
            public decimal VlAmortizacaoPrinc;
        }

        private class TotalPosicoesCotistas
        {
            public int NumeroCotistas;
            public decimal QtCotasFechamento;
            public decimal VlCustoPatrimonio;
            public decimal VlPatrimonioFechamento;
            //public decimal VlPerformanceLDagua;
        }

        private PosicaoCotistaHistoricoCollection BuscaPosicoesCotistasHistorico(DateTime dataPosicao, int idCarteira, TipoPessoa tipoPessoa)
        {
            PosicaoCotistaHistoricoCollection posicoesCotistas = new PosicaoCotistaHistoricoCollection();

            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("O");
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            CotistaQuery cotistaQuery = new CotistaQuery("C");

            posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
            posicaoCotistaHistoricoQuery.InnerJoin(pessoaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            posicaoCotistaHistoricoQuery.Where(
                posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCarteira),
                posicaoCotistaHistoricoQuery.DataHistorico.Equal(dataPosicao),
                pessoaQuery.Tipo == (byte)tipoPessoa
                );

            posicoesCotistas.Load(posicaoCotistaHistoricoQuery);
            return posicoesCotistas;
        }

        private PosicaoCotistaCollection BuscaPosicoesCotistas(int idCarteira, TipoPessoa tipoPessoa)
        {
            PosicaoCotistaCollection posicoesCotistas = new PosicaoCotistaCollection();

            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("O");
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            posicaoCotistaQuery.InnerJoin(cotistaQuery).On(posicaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            posicaoCotistaQuery.InnerJoin(pessoaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            posicaoCotistaQuery.Where(
                posicaoCotistaQuery.IdCarteira.Equal(idCarteira),
                pessoaQuery.Tipo == (byte)tipoPessoa
                );

            posicoesCotistas.Load(posicaoCotistaQuery);
            return posicoesCotistas;
        }

        private OperacaoCotistaCollection BuscaOperacoesCotistas(DateTime dataOperacao, int idCarteira, TipoPessoa tipoPessoa)
        {
            OperacaoCotistaCollection operacoesCotistas = new OperacaoCotistaCollection();

            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            operacaoCotistaQuery.InnerJoin(cotistaQuery).On(operacaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
            operacaoCotistaQuery.InnerJoin(pessoaQuery).On(cotistaQuery.IdPessoa == pessoaQuery.IdPessoa);
            operacaoCotistaQuery.Where(
                operacaoCotistaQuery.IdCarteira.Equal(idCarteira),
                operacaoCotistaQuery.DataOperacao.Equal(dataOperacao),
                pessoaQuery.Tipo == (byte)tipoPessoa
                );

            operacoesCotistas.Load(operacaoCotistaQuery);
            return operacoesCotistas;
        }

        private List<DateTime> BuscaDatasOperacaoQueCotizaramNaDataExportacao(int idCarteira)
        {
            List<DateTime> datasOperacoes = new List<DateTime>();
            OperacaoCotistaCollection operacoesCotistas = new OperacaoCotistaCollection();
            operacoesCotistas.Query.es.Distinct = true;
            operacoesCotistas.Query.Select(operacoesCotistas.Query.DataOperacao);
            operacoesCotistas.Query.Where(operacoesCotistas.Query.DataConversao.Equal(this.DataExportacao),
                operacoesCotistas.Query.IdCarteira.Equal(idCarteira));

            operacoesCotistas.Load(operacoesCotistas.Query);

            foreach (OperacaoCotista operacaoCotista in operacoesCotistas)
            {
                datasOperacoes.Add(operacaoCotista.DataOperacao.Value);
            }
            return datasOperacoes;
        }

        private TotalPosicoesCotistas TotalizaPosicoesCotistas(DateTime dataExportacao, int idCarteira, TipoPessoa tipoPessoa)
        {
            TotalPosicoesCotistas totalPosicoesCotistas = new TotalPosicoesCotistas();
            List<int> idsCotistasLidos = new List<int>();

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);
            if (cliente.DataDia.Value.Date == dataExportacao.Date)
            {
                PosicaoCotistaCollection posicoes = this.BuscaPosicoesCotistas(idCarteira, tipoPessoa);
                foreach (PosicaoCotista posicao in posicoes)
                {
                    totalPosicoesCotistas.QtCotasFechamento += posicao.Quantidade.Value;
                    totalPosicoesCotistas.VlCustoPatrimonio += posicao.ValorAplicacao.Value;
                    totalPosicoesCotistas.VlPatrimonioFechamento += posicao.ValorBruto.Value;
                    if (!idsCotistasLidos.Contains(posicao.IdCotista.Value))
                    {
                        totalPosicoesCotistas.NumeroCotistas++;
                        if (posicao.Quantidade.Value > 0)
                        {
                            idsCotistasLidos.Add(posicao.IdCotista.Value);
                        }
                    }
                }
            }
            else
            {
                PosicaoCotistaHistoricoCollection posicoes = this.BuscaPosicoesCotistasHistorico(dataExportacao, idCarteira, tipoPessoa);
                foreach (PosicaoCotistaHistorico posicao in posicoes)
                {
                    totalPosicoesCotistas.QtCotasFechamento += posicao.Quantidade.Value;
                    totalPosicoesCotistas.VlCustoPatrimonio += posicao.ValorAplicacao.Value;
                    totalPosicoesCotistas.VlPatrimonioFechamento += posicao.ValorBruto.Value;
                    if (!idsCotistasLidos.Contains(posicao.IdCotista.Value))
                    {
                        totalPosicoesCotistas.NumeroCotistas++;
                        if (posicao.Quantidade.Value > 0)
                        {
                            idsCotistasLidos.Add(posicao.IdCotista.Value);
                        }
                    }
                }
            }
            return totalPosicoesCotistas;
        }

        private TotalOperacoesCotistas TotalizaOperacoesCotistas(DateTime dataExportacao, int idCarteira, TipoPessoa tipoPessoa)
        {
            TotalOperacoesCotistas totalOperacoesCotistas = new TotalOperacoesCotistas();
            OperacaoCotistaCollection operacoes = this.BuscaOperacoesCotistas(dataExportacao, idCarteira, tipoPessoa);

            foreach (OperacaoCotista operacao in operacoes)
            {
                if (operacao.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
                {
                    totalOperacoesCotistas.VlAplicacoes += operacao.ValorBruto.Value;
                    totalOperacoesCotistas.QtCotasEmitidas += operacao.Quantidade.Value;
                }
                else if (operacao.TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                    operacao.TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                    operacao.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
                {
                    totalOperacoesCotistas.VlCustoResgates += operacao.ValorBruto.Value - operacao.VariacaoResgate.Value;
                    totalOperacoesCotistas.VlResgates += operacao.ValorBruto.Value;
                    totalOperacoesCotistas.VlIR += operacao.ValorIR.Value;
                    totalOperacoesCotistas.QtCotasResgatadas += operacao.Quantidade.Value;
                    totalOperacoesCotistas.VlPerformance += operacao.ValorPerformance.Value;
                    totalOperacoesCotistas.VlIOF += operacao.ValorIOF.Value;
                }
                else if (operacao.TipoOperacao == (byte)TipoOperacaoCotista.ComeCotas)
                {
                    totalOperacoesCotistas.VlResgatesIR += operacao.ValorBruto.Value;
                    totalOperacoesCotistas.VlCustoResgatesIR += operacao.ValorBruto.Value - operacao.VariacaoResgate.Value;
                    totalOperacoesCotistas.QtCotasResgatesIR += operacao.Quantidade.Value;
                }
                else if (operacao.TipoOperacao == (byte)TipoOperacaoCotista.Amortizacao ||
          operacao.TipoOperacao == (byte)TipoOperacaoCotista.AmortizacaoJuros ||
          operacao.TipoOperacao == (byte)TipoOperacaoCotista.Juros)
                {
                    if (operacao.TipoOperacao == (byte)TipoOperacaoCotista.Amortizacao)
                    {
                        totalOperacoesCotistas.VlAmortizacaoPrinc += operacao.ValorBruto.Value;
                    }
                    else if (operacao.TipoOperacao == (byte)TipoOperacaoCotista.Juros)
                    {
                        totalOperacoesCotistas.VlAmortizacaoJuros += operacao.ValorBruto.Value;
                    }
                    else
                    {
                        //Amortização de Principal + Juros é mais complexo pois precisamos calcular o % de juros e % do principal
                        HistoricoCota historicoCota = new HistoricoCota();
                        List<Financial.Fundo.Debug.HistoricoCota.DebugCotaRecomposta> debugCotaAtualRecompostaList;

                        decimal cotaPreEvento = historicoCota.RetornaCotaRecompostaAtual(idCarteira, dataExportacao,
                                    out debugCotaAtualRecompostaList, (byte)Financial.Fundo.Enums.TipoEventoFundo.AmortizacaoJuros);

                        //Determinar renda pre evento
                        decimal valorPreEvento = cotaPreEvento * operacao.Quantidade.Value;
                        decimal rendaPreEvento = valorPreEvento - (operacao.CotaOperacao.Value * operacao.Quantidade.Value);
                        AgendaFundo agendaFundo = new AgendaFundo();
                        agendaFundo.LoadByPrimaryKey(operacao.IdAgenda.Value);
                        decimal valorJuros = rendaPreEvento * (agendaFundo.Taxa.Value / 100M);
                        decimal valorPrincipal = operacao.ValorBruto.Value - valorJuros;

                        totalOperacoesCotistas.VlAmortizacaoPrinc += valorPrincipal;
                        totalOperacoesCotistas.VlAmortizacaoJuros += valorJuros;
                    }

                    totalOperacoesCotistas.VlAmortizacao += operacao.ValorBruto.Value;

                }
            }

            return totalOperacoesCotistas;
        }

        public List<Financial.Interfaces.Export.YMF.TX3> MontaListaExportacao(int idCarteira)
        {
            ClienteInterface clienteInterface = new ClienteInterface();
            clienteInterface.LoadByPrimaryKey(idCarteira);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            string codAtivoSAC = carteira.RetornaCodigoAtivoSAC(clienteInterface.CodigoYMF);

            //Para cada fundo, precisamos descobrir quais as datas que serão exportadas. Vamos sempre exportar
            //a data selecionada e todas as datas em que ocorreram operações quem foram cotizadas na DataExportação
            List<DateTime> datasExportar = BuscaDatasOperacaoQueCotizaramNaDataExportacao(idCarteira);
            if (!datasExportar.Contains(DataExportacao))
            {
                datasExportar.Add(this.DataExportacao);
            }
            datasExportar.Sort();

            List<Financial.Interfaces.Export.YMF.TX3> tx3List = new List<Financial.Interfaces.Export.YMF.TX3>();

            Carteira fundo = new Carteira();
            fundo.LoadByPrimaryKey(idCarteira);

            foreach (DateTime dataExportar in datasExportar)
            {
                Financial.Interfaces.Export.YMF.TX3 tx3 = new Financial.Interfaces.Export.YMF.TX3();
                TotalOperacoesCotistas totalOperacoesPF = TotalizaOperacoesCotistas(dataExportar, fundo.IdCarteira.Value, TipoPessoa.Fisica);
                TotalOperacoesCotistas totalOperacoesPJ = TotalizaOperacoesCotistas(dataExportar, fundo.IdCarteira.Value, TipoPessoa.Juridica);
                TotalPosicoesCotistas totalPosicoesPF = TotalizaPosicoesCotistas(dataExportar, fundo.IdCarteira.Value, TipoPessoa.Fisica);
                TotalPosicoesCotistas totalPosicoesPJ = TotalizaPosicoesCotistas(dataExportar, fundo.IdCarteira.Value, TipoPessoa.Juridica);

                tx3.CdVersao = "v4";

                tx3.CdAtivo = codAtivoSAC;
                tx3.DtPosicao = dataExportar;
                tx3.VlAplicacoesPF = totalOperacoesPF.VlAplicacoes;
                tx3.QtCotasEmitidasPF = totalOperacoesPF.QtCotasEmitidas;
                tx3.VlAplicacoesPJ = totalOperacoesPJ.VlAplicacoes;
                tx3.QtCotasEmitidasPJ = totalOperacoesPJ.QtCotasEmitidas;
                tx3.VlCustoResgatesPF = totalOperacoesPF.VlCustoResgates;
                tx3.VlResgatesPF = totalOperacoesPF.VlResgates;
                tx3.VlIRPF = totalOperacoesPF.VlIR;
                tx3.QtCotasResgatadasPF = totalOperacoesPF.QtCotasResgatadas;
                tx3.VlCustoResgatesPJ = totalOperacoesPJ.VlCustoResgates;
                tx3.VlResgatesPJ = totalOperacoesPJ.VlResgates;
                tx3.VlIRPJ = totalOperacoesPJ.VlIR;
                tx3.QtCotasResgatadasPJ = totalOperacoesPJ.QtCotasResgatadas;
                tx3.VlSaqueCarencia = 0;
                tx3.VlPerformance = totalOperacoesPF.VlPerformance + totalOperacoesPJ.VlPerformance;
                tx3.NoCotistas = totalPosicoesPF.NumeroCotistas + totalPosicoesPJ.NumeroCotistas;
                tx3.VlPIP = 0;
                tx3.VlIRPIP = 0;
                tx3.VlPosicaoPFD1 = 0;
                tx3.QtCotasEmitidasPFD1 = 0;
                tx3.VlPosicaoPJD1 = 0;
                tx3.QtCotasEmitidasPJD1 = 0;
                tx3.HrAtual = System.DateTime.Now;
                tx3.VlIOF = totalOperacoesPF.VlIOF + totalOperacoesPJ.VlIOF;
                tx3.VlPerformanceLDagua = this.CalculaPerformanceLinhaDagua(idCarteira, dataExportar);

                tx3.VlIOFAplicacoes = 0;
                tx3.VlCustoResgatesIRPF = totalOperacoesPF.VlCustoResgatesIR;
                tx3.VlResgatesIRPF = totalOperacoesPF.VlResgatesIR;
                tx3.QtCotasResgatesIRPF = totalOperacoesPF.QtCotasResgatesIR;
                tx3.VlCustoResgatesIRPJ = totalOperacoesPJ.VlCustoResgatesIR;
                tx3.VlResgatesIRPJ = totalOperacoesPJ.VlResgatesIR;
                tx3.QtCotasResgatesIRPJ = totalOperacoesPJ.QtCotasResgatesIR;

                tx3.VlPatrimonioFechamentoPF = totalPosicoesPF.VlPatrimonioFechamento;
                tx3.VlCustoPatrimonioPF = totalPosicoesPF.VlCustoPatrimonio;
                tx3.QtCotasFechamentoPF = totalPosicoesPF.QtCotasFechamento;
                tx3.VlPatrimonioFechamentoPJ = totalPosicoesPJ.VlPatrimonioFechamento;
                tx3.VlCustoPatrimonioPJ = totalPosicoesPJ.VlCustoPatrimonio;
                tx3.QtCotasFechamentoPJ = totalPosicoesPJ.QtCotasFechamento;


                try
                {
                    tx3.VlCotaFechamento = BuscaValorCotaFechamento(idCarteira, dataExportar);
                }
                catch
                {
                    throw new Exception("não existe cota de fechamento para o fundo " +
                        fundo.Nome.Trim() + " na data " + dataExportar.ToString("dd/MM/yyyy"));
                }

                tx3.CdTipoMovimento = null;

                tx3.VlFDPenaltyPF = 0;
                tx3.VlFDPenaltyPJ = 0;
                tx3.VlADPenaltyPF = 0;
                tx3.VlADPenaltyPJ = 0;
                tx3.VlIngresso = 0;

                tx3.VlRepasseDiv = 0;
                tx3.VlIRRFRepasseDiv = 0;
                tx3.VlRepasseJuros = 0;
                tx3.VlIRRFRepasseJuros = 0;
                tx3.VlRepasseRend = 0;
                tx3.VlIRRFRepasseRend = 0;

                tx3.VlAmortizacaoPrincPF = totalOperacoesPF.VlAmortizacaoPrinc;
                tx3.VlAmortizacaoPrincPJ = totalOperacoesPJ.VlAmortizacaoPrinc;
                tx3.VlAmortizacaoJurosPF = totalOperacoesPF.VlAmortizacaoJuros;
                tx3.VlAmortizacaoJurosPJ = totalOperacoesPJ.VlAmortizacaoJuros;

                tx3.VlAmortizacaoPF = tx3.VlAmortizacaoPrincPF + tx3.VlAmortizacaoJurosPF;
                tx3.VlAmortizacaoPJ = tx3.VlAmortizacaoPrincPJ + tx3.VlAmortizacaoJurosPJ;
                tx3.VlAmortizacaoTotal = tx3.VlAmortizacaoPF + tx3.VlAmortizacaoPJ;

                tx3.DtFimTX3 = DateTime.Now;
                tx3List.Add(tx3);
            }
            return tx3List;
        }

        public void ExportaTabela(int idCarteira)
        {
            string linguaSelecionada = "en-us";
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(linguaSelecionada);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(linguaSelecionada);

            List<Financial.Interfaces.Export.YMF.TX3> tx3List = this.MontaListaExportacao(idCarteira);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            using (SqlConnection connCOT = carteira.CreateSqlConnectionYMFCOT())
            {
                foreach (Financial.Interfaces.Export.YMF.TX3 tx3 in tx3List)
                {
                    this.GravaTabela(connCOT, tx3);
                }
            }
        }

        private void GravaTabela(SqlConnection connCOT, Financial.Interfaces.Export.YMF.TX3 tx3)
        {

            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.AppendFormat("DELETE FROM COT_INTEGRACAO_TX3 WHERE CD_ATIVO = '{0}' and DT_POSICAO = '{1}' ", tx3.CdAtivo, tx3.DtPosicao.ToString("yyyy-MM-dd"));
            sqlBuilder.Append("INSERT INTO COT_INTEGRACAO_TX3 (CD_ATIVO,DT_POSICAO,VL_APLICACOES_PF,QT_COTAS_EMITIDAS_PF,VL_APLICACOES_PJ,QT_COTAS_EMITIDAS_PJ,VL_CUSTO_RESGATES_PF,VL_RESGATES_PF,VL_IR_PF,QT_COTAS_RESGATADAS_PF,VL_CUSTO_RESGATES_PJ,VL_RESGATES_PJ,VL_IR_PJ,QT_COTAS_RESGATADAS_PJ,VL_SAQUE_CARENCIA,VL_PERFORMANCE,VL_PERFORMANCE_LDAGUA,NO_COTISTAS,VL_PIP,VL_IR_PIP,VL_POSICAO_PF_D1,QT_COTAS_EMITIDAS_PF_D1,VL_POSICAO_PJ_D1,QT_COTAS_EMITIDAS_PJ_D1,VL_IOF,VL_IOF_APLICACOES,VL_CUSTO_RESGATES_IR_PF,VL_RESGATES_IR_PF,QT_COTAS_RESGATES_IR_PF,VL_CUSTO_RESGATES_IR_PJ,VL_RESGATES_IR_PJ,QT_COTAS_RESGATES_IR_PJ,VL_PATRIMONIO_FECHAMENTO_PF,VL_CUSTO_PATRIMONIO_PF,QT_COTAS_FECHAMENTO_PF,VL_PATRIMONIO_FECHAMENTO_PJ,VL_CUSTO_PATRIMONIO_PJ,QT_COTAS_FECHAMENTO_PJ,VL_COTA_FECHAMENTO,DT_FIM_TX3,VL_AMORTIZACAO_TOTAL,VL_AMORTIZACAO_PJ,VL_AMORTIZACAO_PF,VL_FD_PENALTY_PF,VL_FD_PENALTY_PJ,VL_AD_PENALTY_PF,VL_AD_PENALTY_PJ,VL_INGRESSO,VL_REPASSE_DIV,VL_IRRF_REPASSE_DIV,VL_REPASSE_JUROS,VL_IRRF_REPASSE_JUROS,VL_REPASSE_REND,VL_IRRF_REPASSE_REND,CD_VERSAO,HR_ATUAL,VL_PERFORMACE_LDAGUA,VL_APLICACOES_PF_TOTAL,QT_COTAS_EMITIDAS_PF_TOTAL,VL_APLICACOES_PJ_TOTAL,QT_COTAS_EMITIDAS_PJ_TOTAL,CD_TIPO_MOVIMENTO,VL_AMORTIZACAO_PRINC_PF,VL_AMORTIZACAO_PRINC_PJ,VL_AMORTIZACAO_JUROS_PF,VL_AMORTIZACAO_JUROS_PJ,VL_PROV_FINANCEIRO_SAIDA) ");
            sqlBuilder.Append("VALUES (");
            sqlBuilder.AppendFormat("'{0}',", tx3.CdAtivo);
            sqlBuilder.AppendFormat("'{0}',", tx3.DtPosicao.ToString("yyyy-MM-dd"));
            sqlBuilder.AppendFormat("{0},", tx3.VlAplicacoesPF);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasEmitidasPF);

            sqlBuilder.AppendFormat("{0},", tx3.VlAplicacoesPJ);

            sqlBuilder.AppendFormat("{0},", tx3.QtCotasEmitidasPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlCustoResgatesPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlResgatesPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlIRPF);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasResgatadasPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlCustoResgatesPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlResgatesPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlIRPJ);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasResgatadasPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlSaqueCarencia);
            sqlBuilder.AppendFormat("{0},", tx3.VlPerformance);
            sqlBuilder.AppendFormat("{0},", tx3.VlPerformanceLDagua);
            sqlBuilder.AppendFormat("{0},", tx3.NoCotistas);
            sqlBuilder.AppendFormat("{0},", tx3.VlPIP);
            sqlBuilder.AppendFormat("{0},", tx3.VlIRPIP);
            sqlBuilder.AppendFormat("{0},", tx3.VlPosicaoPFD1);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasEmitidasPFD1);
            sqlBuilder.AppendFormat("{0},", tx3.VlPosicaoPJD1);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasEmitidasPJD1);
            sqlBuilder.AppendFormat("{0},", tx3.VlIOF);
            sqlBuilder.AppendFormat("{0},", tx3.VlIOFAplicacoes);
            sqlBuilder.AppendFormat("{0},", tx3.VlCustoResgatesIRPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlResgatesIRPF);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasResgatesIRPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlCustoResgatesIRPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlResgatesIRPJ);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasResgatesIRPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlPatrimonioFechamentoPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlCustoPatrimonioPF);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasFechamentoPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlPatrimonioFechamentoPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlCustoPatrimonioPJ);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasFechamentoPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlCotaFechamento);
            sqlBuilder.AppendFormat("'{0}',", tx3.DtFimTX3.ToString("yyyy-MM-dd"));
            sqlBuilder.AppendFormat("{0},", tx3.VlAmortizacaoTotal);
            sqlBuilder.AppendFormat("{0},", tx3.VlAmortizacaoPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlAmortizacaoPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlFDPenaltyPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlFDPenaltyPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlADPenaltyPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlADPenaltyPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlIngresso);
            sqlBuilder.AppendFormat("{0},", tx3.VlRepasseDiv);
            sqlBuilder.AppendFormat("{0},", tx3.VlIRRFRepasseDiv);
            sqlBuilder.AppendFormat("{0},", tx3.VlRepasseJuros);
            sqlBuilder.AppendFormat("{0},", tx3.VlIRRFRepasseJuros);
            sqlBuilder.AppendFormat("{0},", tx3.VlRepasseRend);
            sqlBuilder.AppendFormat("{0},", tx3.VlIRRFRepasseRend);
            sqlBuilder.AppendFormat("{0},", "NULL");/*tx3.CdVersao null na view*/
            sqlBuilder.AppendFormat("{0},", "NULL");/*tx3.HrAtual null na view*/
            sqlBuilder.AppendFormat("{0},", tx3.VlPerformanceLDagua);
            sqlBuilder.AppendFormat("{0},", tx3.VlAplicacoesPFTotal);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasEmitidasPFTotal);
            sqlBuilder.AppendFormat("{0},", tx3.VlAplicacoesPJTotal);
            sqlBuilder.AppendFormat("{0},", tx3.QtCotasEmitidasPJTotal);
            sqlBuilder.AppendFormat("{0},", "NULL");/*tx3.CdTipoMovimento null na view*/
            sqlBuilder.AppendFormat("{0},", tx3.VlAmortizacaoPrincPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlAmortizacaoPrincPJ);
            sqlBuilder.AppendFormat("{0},", tx3.VlAmortizacaoJurosPF);
            sqlBuilder.AppendFormat("{0},", tx3.VlAmortizacaoJurosPJ);
            sqlBuilder.AppendFormat("{0}", tx3.VlProvFinanceiroSaida);

            sqlBuilder.Append(")");

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlBuilder.ToString(); ;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connCOT;
                cmd.ExecuteNonQuery();
            }
        }

        private decimal CalculaPerformanceLinhaDagua(int idCarteira, DateTime data)
        {
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
            liquidacao.Query.Where(liquidacao.Query.Origem.Equal(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance),
                liquidacao.Query.DataVencimento.Equal(data),
                liquidacao.Query.IdCliente.Equal(idCarteira),
                liquidacao.Query.Situacao.Equal((byte)SituacaoLancamentoLiquidacao.Normal));
            liquidacao.Query.Load();

            decimal valorPerformance = 0;
            if (liquidacao.Valor.HasValue)
            {
                valorPerformance = liquidacao.Valor.Value * -1;
            }

            return valorPerformance;
        }

        private decimal BuscaValorCotaFechamento(int idCarteira, DateTime data)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(data, idCarteira);
            return historicoCota.CotaFechamento.Value;
        }

        public TX3(DateTime dataExportacao)
        {
            this.DataExportacao = dataExportacao;
        }

    }
}
