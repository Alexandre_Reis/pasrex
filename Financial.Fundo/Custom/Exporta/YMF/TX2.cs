﻿using System;
using System.Web;
using Financial.ContaCorrente;
using Financial.Investidor;
using System.IO;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.InvestidorCotista.Enums;
using System.Collections.Generic;
using System.Text;
using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Threading;
using System.Globalization;
using System.Data.SqlClient;
using Financial.ContaCorrente.Enums;

namespace Financial.Fundo.Exportacao.YMF
{
    /// <summary>
    /// Summary description for TX2
    /// </summary>
    public class TX2
    {

        private DateTime DataExportacao;
        public class TotalPosicoesCotistas
        {
            public decimal VlPerformance;
        }

        public List<Financial.Interfaces.Export.YMF.TX2> MontaListaExportacao(int idCarteira)
        {
            List<Financial.Interfaces.Export.YMF.TX2> tx2List = new List<Financial.Interfaces.Export.YMF.TX2>();

            Carteira fundo = new Carteira();
            fundo.LoadByPrimaryKey(idCarteira);

            ClienteInterface clienteInterface = new ClienteInterface();
            clienteInterface.LoadByPrimaryKey(fundo.IdCarteira.Value);

            string codAtivoSAC = fundo.RetornaCodigoAtivoSAC(clienteInterface.CodigoYMF);
            
            Financial.Interfaces.Export.YMF.TX2 tx2 = new Financial.Interfaces.Export.YMF.TX2();
            TotalPosicoesCotistas totalPosicoes = TotalizaPosicoesCotistas(this.DataExportacao, fundo.IdCarteira.Value);

            tx2.CdAtivo = codAtivoSAC;
            tx2.DtPosicao = this.DataExportacao;
            tx2.VlPerformance = totalPosicoes.VlPerformance;
            tx2.VlCotaLiquida = BuscaValorCotaFechamento(idCarteira, this.DataExportacao);
            tx2.HrAtual = System.DateTime.Now;
            tx2.DtFimTX2 = System.DateTime.Now;

            tx2List.Add(tx2);

            return tx2List;
            
        }

        public void ExportaTabela(int idCarteira)
        {
            string linguaSelecionada = "en-us";
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(linguaSelecionada);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(linguaSelecionada);

            List<Financial.Interfaces.Export.YMF.TX2> tx2List = this.MontaListaExportacao(idCarteira);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            using (SqlConnection connCOT = carteira.CreateSqlConnectionYMFCOT())
            {
                foreach (Financial.Interfaces.Export.YMF.TX2 tx2 in tx2List)
                {
                    this.GravaTabela(connCOT, tx2);
                }
            }
        }

        private void GravaTabela(SqlConnection connCOT, Financial.Interfaces.Export.YMF.TX2 tx2)
        {
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.AppendFormat("DELETE FROM COT_INTEGRACAO_TX2 WHERE CD_ATIVO = '{0}' and DT_POSICAO = '{1}' ", tx2.CdAtivo, tx2.DtPosicao.ToString("yyyy-MM-dd"));
            sqlBuilder.Append("INSERT INTO COT_INTEGRACAO_TX2 (CD_ATIVO,DT_POSICAO,VL_COTA_LIQUIDA,VL_PERFORMANCE,DT_FIM_TX2) ");
            sqlBuilder.Append("VALUES (");
            sqlBuilder.AppendFormat("'{0}',", tx2.CdAtivo);
            sqlBuilder.AppendFormat("'{0}',", tx2.DtPosicao.ToString("yyyy-MM-dd"));
            sqlBuilder.AppendFormat("{0},", tx2.VlCotaLiquida);
            sqlBuilder.AppendFormat("{0},", tx2.VlPerformance);

            sqlBuilder.AppendFormat("'{0}'", tx2.DtFimTX2.ToString("yyyy-MM-dd"));
            
            sqlBuilder.Append(")");

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlBuilder.ToString(); ;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = connCOT;
                cmd.ExecuteNonQuery();
            }
        }
       
        public TX2(DateTime dataExportacao)
        {
            this.DataExportacao = dataExportacao;
        }
          
        public decimal BuscaValorCotaFechamento(int idCarteira, DateTime data)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.LoadByPrimaryKey(data, idCarteira);
            return historicoCota.CotaFechamento.Value;
        }
  
        private PosicaoCotistaHistoricoCollection BuscaPosicoesCotistasHistoricas(DateTime dataPosicao, int idCarteira)
        {
            PosicaoCotistaHistoricoCollection posicoesCotistas = new PosicaoCotistaHistoricoCollection();

            posicoesCotistas.Query.Where(
                posicoesCotistas.Query.IdCarteira.Equal(idCarteira),
                posicoesCotistas.Query.DataHistorico.Equal(dataPosicao)
                );

            posicoesCotistas.Load(posicoesCotistas.Query);
            return posicoesCotistas;
        }

        private PosicaoCotistaCollection BuscaPosicoesCotistas(int idCarteira)
        {
            PosicaoCotistaCollection posicoesCotistas = new PosicaoCotistaCollection();

            posicoesCotistas.Query.Where(
                posicoesCotistas.Query.IdCarteira.Equal(idCarteira)
                );

            posicoesCotistas.Load(posicoesCotistas.Query);
            return posicoesCotistas;
        }

        private TotalPosicoesCotistas TotalizaPosicoesCotistas(DateTime dataExportacao, int idCarteira)
        {
            TotalPosicoesCotistas totalPosicoesCotistas = new TotalPosicoesCotistas();

            decimal pagamentoPerformanceNaData = CalculaPerformanceLinhaDagua(idCarteira, dataExportacao);
            if (pagamentoPerformanceNaData > 0)
            {
                //Se houve pagamento de performance no dia nao podemos olhar pras posicoes dos cotistas pois no TX2 o valor exportado é o valor pago
                totalPosicoesCotistas.VlPerformance = pagamentoPerformanceNaData;
            }
            else
            {
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteira);
                if (cliente.DataDia.Value.Date == dataExportacao.Date)
                {
                    PosicaoCotistaCollection posicoes = this.BuscaPosicoesCotistas(idCarteira);

                    totalPosicoesCotistas.VlPerformance = 0;
                    foreach (PosicaoCotista posicao in posicoes)
                    {
                        totalPosicoesCotistas.VlPerformance += posicao.ValorPerformance.Value;
                    }
                }
                else
                {
                    PosicaoCotistaHistoricoCollection posicoes = this.BuscaPosicoesCotistasHistoricas(dataExportacao, idCarteira);

                    totalPosicoesCotistas.VlPerformance = 0;
                    foreach (PosicaoCotistaHistorico posicao in posicoes)
                    {
                        totalPosicoesCotistas.VlPerformance += posicao.ValorPerformance.Value;
                    }
                }
            }

            return totalPosicoesCotistas;
        }

        private decimal CalculaPerformanceLinhaDagua(int idCarteira, DateTime data)
        {
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
            liquidacao.Query.Where(liquidacao.Query.Origem.Equal(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance),
                liquidacao.Query.DataVencimento.Equal(data),
                liquidacao.Query.IdCliente.Equal(idCarteira),
                liquidacao.Query.Situacao.Equal((byte)SituacaoLancamentoLiquidacao.Normal));
            liquidacao.Query.Load();

            decimal valorPerformance = 0;
            if (liquidacao.Valor.HasValue)
            {
                valorPerformance = liquidacao.Valor.Value * -1;
            }

            return valorPerformance;
        }
        

    }
}
