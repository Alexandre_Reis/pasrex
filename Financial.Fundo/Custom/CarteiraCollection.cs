using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Investidor;
using Financial.Security;
using Financial.Investidor.Enums;
using Financial.Interfaces.Import.YMF;
using Financial.CRM;

namespace Financial.Fundo
{
    public partial class CarteiraCollection : esCarteiraCollection
    {
        /// <summary>
        /// Carrega o objeto CarteiraCollection com o campo IdCarteira.
        /// </summary>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="tipoTributacao"></param>        
        public void BuscaCarteira(int idAgenteAdministrador, int tipoTributacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.IdCarteira)
                 .Where(this.query.IdAgenteAdministrador == idAgenteAdministrador,
                        this.query.TipoTributacao == tipoTributacao);

            this.Query.Load();
        }

        public Carteira BuscaCarteiraPorNome(string nome, bool partialMatch)
        {
            this.QueryReset();

            if (partialMatch)
            {
                this.Query
                 .Where(this.Query.Nome.ToUpper().Like(nome.ToUpper()));
            }
            else
            {
                this.Query
                 .Where(this.Query.Nome.ToUpper().Equal(nome.ToUpper()));
            }

            
            this.Query.Load();

            if (this.Count == 0)
            {
                return null;
            }
            else
            {
                return this[0];
            }
        }

        public void BuscaCarteirasFundos()
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Apelido);
            carteiraQuery.Where((clienteQuery.TipoControle.Equal(TipoControleCliente.ApenasCotacao) |
                                clienteQuery.TipoControle.Equal(TipoControleCliente.Cotista)) &
                                clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &
                                clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC));
            carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);
            this.Load(carteiraQuery);
        }

        public void BuscaCarteirasFundosYMF(bool apenasTipoControleCotista)
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            ClienteInterfaceQuery clienteInterfaceQuery = new ClienteInterfaceQuery("I");

            //carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Apelido, carteiraQuery.TipoTributacao);

            if (apenasTipoControleCotista)
            {
                carteiraQuery.Where(clienteQuery.TipoControle.Equal(TipoControleCliente.Cotista) &
                                clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &
                                clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC) &
                                (clienteInterfaceQuery.CodigoYMF.IsNotNull() & clienteInterfaceQuery.CodigoYMF.NotEqual("")));
            }
            else
            {
                carteiraQuery.Where(
                                clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &
                                clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC) &
                                (clienteInterfaceQuery.CodigoYMF.IsNotNull() & clienteInterfaceQuery.CodigoYMF.NotEqual("")));
            }

            carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(clienteInterfaceQuery).On(carteiraQuery.IdCarteira == clienteInterfaceQuery.IdCliente);
            
            carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);
            this.Load(carteiraQuery);
        }

        public void BuscaCarteirasFundosAnbima()
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Apelido);
            carteiraQuery.Where((clienteQuery.TipoControle.Equal(TipoControleCliente.ApenasCotacao) |
                                clienteQuery.TipoControle.Equal(TipoControleCliente.Cotista)) &
                                clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &
                                clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC) &
                                (carteiraQuery.CodigoAnbid.IsNotNull() & carteiraQuery.CodigoAnbid.NotEqual("")));
            carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);
            this.Load(carteiraQuery);
        }

        public void BuscaCarteirasFundosClubes()
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Apelido);
            carteiraQuery.Where((clienteQuery.TipoControle.Equal(TipoControleCliente.ApenasCotacao) |
                                clienteQuery.TipoControle.Equal(TipoControleCliente.Cotista) |
                                clienteQuery.TipoControle.Equal(TipoControleCliente.Completo)) &
                                clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &
                                clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC, (int)TipoClienteFixo.Clube));
            carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);
            this.Load(carteiraQuery);
        }

        public void BuscaCarteirasFundosClubesBoletagem(string login)
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
            //
            carteiraQuery.Where(usuarioQuery.Login == login &
                                (clienteQuery.TipoControle.Equal(TipoControleCliente.Cotista) |
                                 clienteQuery.TipoControle.Equal(TipoControleCliente.Completo)) &    
                                clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao) &
                                clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC, (int)TipoClienteFixo.Clube, (int)TipoClienteFixo.OffShore_PF, (int)TipoClienteFixo.OffShore_PJ));

            this.Load(carteiraQuery);            
        }

        /// <summary>
        /// Carrega o objeto CarteiraCollection com os campos IdCarteira, Nome, Apelido.
        /// OrderBy(carteiraQuery.Apelido.Ascending);
        /// Faz um combine de collections de carteiras com acesso via PermissaoCliente 
        //  com carteiras vinculadas a clientes do TipoControle = SomenteCotacao.
        /// </summary>
        /// <param name="login"></param>          
        public void BuscaCarteirasComAcessoTodas(string login)
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
            //
            carteiraQuery.Where(usuarioQuery.Login == login,
                                clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                                clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));

            this.Load(carteiraQuery);

            carteiraQuery = new CarteiraQuery("A");
            clienteQuery = new ClienteQuery("C");
            //
            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            //
            carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                                clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
            //
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.Load(carteiraQuery);
            //
            this.Combine(carteiraCollection);
            this.Sort = CarteiraMetadata.ColumnNames.Apelido + " ASC";
        }

        /// <summary>
        /// Carrega o objeto CarteiraCollection com os campos IdCarteira, Nome, Apelido.
        /// Filtra por TipoControle.NotEqual(TipoControleCliente.ApenasCotacao)
        /// e StatusAtivo.Equal(StatusAtivoCliente.Ativo).
        /// OrderBy(carteiraQuery.Apelido.Ascending);
        /// </summary>
        /// <param name="login"></param>  
        public void BuscaCarteirasComAcesso(string login)
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
            carteiraQuery.Where(usuarioQuery.Login == login,
                               clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao),
                               clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo))
            .OrderBy(carteiraQuery.Apelido.Ascending);

            this.Load(carteiraQuery);
        }

        /// <summary>
        /// Carrega o objeto CarteiraCollection com os campos IdCarteira, Nome, Apelido.
        /// OrderBy(carteiraQuery.Apelido.Ascending);
        /// Faz um combine de collections de carteiras com acesso via PermissaoCliente 
        //  com carteiras vinculadas a clientes do TipoControle = SomenteCotacao.
        /// </summary>
        /// <param name="login"></param>          
        public void BuscaCarteirasComAcessoTotal(string login)
        {
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
            //
            carteiraQuery.Where(usuarioQuery.Login == login,
                                clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));

            this.Load(carteiraQuery);

            carteiraQuery = new CarteiraQuery("A");
            clienteQuery = new ClienteQuery("C");
            //
            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            //
            carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));
            //
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.Load(carteiraQuery);
            //
            this.Combine(carteiraCollection);
            this.Sort = CarteiraMetadata.ColumnNames.Apelido + " ASC";
        }

        public void ImportaFundoYMF(FundoYMF[] fundoYMFArray,
            FundoYMFInMemory fundoYMFInMemory)
        {

            using (esTransactionScope scope = new esTransactionScope())
            {
                foreach (FundoYMF fundoYMF in fundoYMFArray)
                {

                    Cliente clienteExistente = new ClienteCollection().BuscaClientePorCodigoYMF(fundoYMF.CdFundo);
                    Cliente cliente = new Cliente();
                    ClienteInterface clienteInterface;
                    Financial.Bolsa.ClienteBolsa clienteBolsa;
                    Financial.BMF.ClienteBMF clienteBMF;
                    Financial.RendaFixa.ClienteRendaFixa clienteRendaFixa;
                    Financial.Investidor.ContaCorrente contaCorrente;
                    PermissaoClienteCollection permissaoClienteCollection;


                    //Nao estamos fazendo atualizacoes de clientes/carteiras para nao sobrescrever as atualizacoes 
                    //manuais que tenham sido feitas nos cadastros apos a importacao
                    if (clienteExistente == null)
                    {
                        //Apenas INSERTs sao permitidos
                        cliente.ImportaFundoYMF(fundoYMF, fundoYMFInMemory, out clienteInterface,
                            out clienteBolsa, out clienteBMF, out clienteRendaFixa, out contaCorrente,
                            out permissaoClienteCollection);
                        cliente.Save();
                        clienteInterface.Save();
                        clienteBolsa.Save();
                        clienteBMF.Save();
                        clienteRendaFixa.Save();
                        if (contaCorrente != null)
                        {
                            contaCorrente.Save();
                        }
                        if (permissaoClienteCollection != null)
                        {
                            permissaoClienteCollection.Save();
                        }


                        Carteira carteira = new Carteira();

                        carteira.ImportaFundoYMF(fundoYMF, fundoYMFInMemory);
                        carteira.Save();

                        this.AttachEntity(carteira);
                    }
                    else
                    {
                        Carteira carteira = new Carteira();
                        carteira.LoadByPrimaryKey(clienteExistente.IdCliente.Value);
                        this.AttachEntity(carteira);
                    }
                }

                scope.Complete();

            }
        }

        public Carteira BuscaCarteiraPorCNPJ(string cnpj)
        {
            Carteira carteira = null;

            Pessoa pessoa = new PessoaCollection().BuscaPessoaPorCPFCNPJ(cnpj);

            if (pessoa != null)
            {
                carteira = new Carteira();
                carteira.LoadByPrimaryKey(pessoa.IdPessoa.Value);
            }            

            return carteira;
        }

        public CarteiraCollection BuscaCarteirasPorCNPJ(string cnpj)
        {
            CarteiraCollection carteiras = null;

            PessoaCollection pessoas = new PessoaCollection().BuscaPessoasPorCPFCNPJ(cnpj);

            if (pessoas != null && pessoas.Count > 0)
            {
                carteiras = new CarteiraCollection();
                foreach (Pessoa pessoa in pessoas)
                {
                    Carteira carteira = new Carteira();
                    carteira.LoadByPrimaryKey(pessoa.IdPessoa.Value);
                    carteiras.AttachEntity(carteira);
                }
            }

            return carteiras;
        }
    }
}
