﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/12/2014 15:20:07
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;
using Financial.Common.Enums;
using Financial.Common;

namespace Financial.Fundo
{
	public partial class Rentabilidade : esRentabilidade
	{
        public class RentabilidadeConversao
        {
            public ConversaoMoeda ConversaoMoeda;
            public CotacaoIndice CotacaoIndice;
        }

        public void ConverteValores(short idMoedaAtivo, short idMoedaCarteira, ref Dictionary<string, RentabilidadeConversao> dicRentConversao)
        {
            if (idMoedaAtivo == idMoedaCarteira)
                return;

            RentabilidadeConversao rentabilidadeConversao = new RentabilidadeConversao();
            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            string keyConversao = idMoedaAtivo+"-"+idMoedaCarteira;

            if (!dicRentConversao.ContainsKey(keyConversao))
            {                
                if (conversaoMoeda.LoadByPrimaryKey(idMoedaAtivo, idMoedaCarteira))
                {                    
                    if (cotacaoIndice.LoadByPrimaryKey(this.Data.Value, conversaoMoeda.IdIndice.Value))
                    {
                        rentabilidadeConversao.ConversaoMoeda = conversaoMoeda;
                        rentabilidadeConversao.CotacaoIndice = cotacaoIndice;

                        dicRentConversao.Add(keyConversao, rentabilidadeConversao);
                    }
                    else
                    {
                        Indice indice = new Indice();
                        indice.LoadByPrimaryKey(conversaoMoeda.IdIndice.Value);
                        throw new Exception("Não há cotação cadastrada para o índice " + indice.Descricao + " para o dia " + String.Format("{0:dd/MM/yyyy}", this.Data.Value) + "!");
                    }
                }
                else
                {
                    throw new Exception("Não existe Conversão de Moeda Cadastrada entre " + this.CodigoMoedaAtivo + " e " + this.CodigoMoedaPortfolio + "!");
                }
            }
            else
            {
                rentabilidadeConversao = dicRentConversao[keyConversao];
            }

            decimal cambio = rentabilidadeConversao.CotacaoIndice.Valor.Value;
            byte tipoConversao = rentabilidadeConversao.ConversaoMoeda.Tipo.Value;

            if (tipoConversao == (byte)TipoConversaoMoeda.Divide)
            {
                this.PatrimonioFinalMoedaPortfolio = this.PatrimonioFinalMoedaAtivo / cambio;
                this.PatrimonioInicialMoedaPortfolio = this.PatrimonioInicialMoedaAtivo / cambio;
                this.RentabilidadeMoedaPortfolio = this.RentabilidadeMoedaAtivo / cambio;
                this.ValorFinanceiroEntradaMoedaPortfolio = this.ValorFinanceiroEntradaMoedaAtivo / cambio;
                this.ValorFinanceiroSaidaMoedaPortfolio = this.ValorFinanceiroSaidaMoedaAtivo / cambio;
                this.ValorFinanceiroRendasMoedaPortfolio = this.ValorFinanceiroRendasMoedaAtivo / cambio;
                this.RentabilidadeBrutaDiariaMoedaPortfolio = this.RentabilidadeBrutaDiariaMoedaAtivo / cambio;
                this.RentabilidadeBrutaAcumMoedaPortfolio = this.RentabilidadeBrutaAcumMoedaAtivo / cambio;
                this.PatrimonioFinalBrutoMoedaPortfolio = this.PatrimonioFinalBrutoMoedaAtivo / cambio;
                this.PatrimonioInicialBrutoMoedaPortfolio = this.PatrimonioInicialBrutoMoedaAtivo / cambio;
                this.PatrimonioFinalGrossUpMoedaPortfolio = this.PatrimonioFinalGrossUpMoedaAtivo / cambio;
                this.PatrimonioInicialGrossUpMoedaPortfolio = this.PatrimonioInicialGrossUpMoedaAtivo / cambio;
                this.RentabilidadeGrossUpDiariaMoedaPortfolio = this.RentabilidadeGrossUpDiariaMoedaAtivo / cambio;
                this.RentabilidadeGrossUpAcumMoedaPortfolio = this.RentabilidadeGrossUpAcumMoedaAtivo / cambio;
            }
            else
            {
                this.PatrimonioFinalMoedaPortfolio = this.PatrimonioFinalMoedaAtivo * cambio;
                this.PatrimonioInicialMoedaPortfolio = this.PatrimonioInicialMoedaAtivo * cambio;
                this.RentabilidadeMoedaPortfolio = this.RentabilidadeMoedaAtivo * cambio;
                this.ValorFinanceiroEntradaMoedaPortfolio = this.ValorFinanceiroEntradaMoedaAtivo * cambio;
                this.ValorFinanceiroSaidaMoedaPortfolio = this.ValorFinanceiroSaidaMoedaAtivo * cambio;
                this.ValorFinanceiroRendasMoedaPortfolio = this.ValorFinanceiroRendasMoedaAtivo * cambio;
                this.RentabilidadeBrutaDiariaMoedaPortfolio = this.RentabilidadeBrutaDiariaMoedaAtivo * cambio;
                this.RentabilidadeBrutaAcumMoedaPortfolio = this.RentabilidadeBrutaAcumMoedaAtivo * cambio;
                this.PatrimonioFinalBrutoMoedaPortfolio = this.PatrimonioFinalBrutoMoedaAtivo * cambio;
                this.PatrimonioInicialBrutoMoedaPortfolio = this.PatrimonioInicialBrutoMoedaAtivo * cambio;
                this.PatrimonioFinalGrossUpMoedaPortfolio = this.PatrimonioFinalGrossUpMoedaAtivo * cambio;
                this.PatrimonioInicialGrossUpMoedaPortfolio = this.PatrimonioInicialGrossUpMoedaAtivo * cambio;
                this.RentabilidadeGrossUpDiariaMoedaPortfolio = this.RentabilidadeGrossUpDiariaMoedaAtivo * cambio;
                this.RentabilidadeGrossUpAcumMoedaPortfolio = this.RentabilidadeGrossUpAcumMoedaAtivo * cambio;
            }
        }
	}
}
