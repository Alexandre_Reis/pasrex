﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Util;
using Financial.Fundo;
using Financial.Investidor;
using Financial.CRM;
using Financial.Investidor.Enums;
using Financial.Security;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.CRM.Enums;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista.Enums;
using System.Data;
using Financial.Common;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.RendaFixa;

namespace Financial.Fundo
{
    public class ConsultaPrimaria
    {
        private int idAgrupamento;
        public int IdAgrupamento
        {
            get { return idAgrupamento; }
            set { idAgrupamento = value; }
        }

        private int idCliente;
        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private string descricao1;
        public string Descricao1
        {
            get { return descricao1; }
            set { descricao1 = value; }
        }

        private string descricao2;
        public string Descricao2
        {
            get { return descricao2; }
            set { descricao2 = value; }
        }

        private string descricao3;
        public string Descricao3
        {
            get { return descricao3; }
            set { descricao3 = value; }
        }
    }

    public class ConsultaDados
    {
        private int idAgrupamento;
        public int IdAgrupamento
        {
            get { return idAgrupamento; }
            set { idAgrupamento = value; }
        }

        private int idCliente;
        public int IdCliente
        {
            get { return idCliente; }
            set { idCliente = value; }
        }

        private string descricao1;
        public string Descricao1
        {
            get { return descricao1; }
            set { descricao1 = value; }
        }

        private string descricao2;
        public string Descricao2
        {
            get { return descricao2; }
            set { descricao2 = value; }
        }

        private string descricao3;
        public string Descricao3
        {
            get { return descricao3; }
            set { descricao3 = value; }
        }

        private decimal saldoBruto;
        public decimal SaldoBruto
        {
            get { return saldoBruto; }
            set { saldoBruto = value; }
        }

        private decimal tributos;
        public decimal Tributos
        {
            get { return tributos; }
            set { tributos = value; }
        }

        private decimal saldoLiquido;
        public decimal SaldoLiquido
        {
            get { return saldoLiquido; }
            set { saldoLiquido = value; }
        }

        private decimal rendimento;
        public decimal Rendimento
        {
            get { return rendimento; }
            set { rendimento = value; }
        }
    }

    public class ConsultaAtivos
    {
        #region Atributos
        public enum TipoAgrupamento
        {
            TipoCliente = 1,
            Moeda = 2,
            Instituicao = 3,
            ClasseAtivo = 4
        }

        private string login;
        private DateTime data;

        private TipoAgrupamento agrupamento;

        private bool checkRendaVariavel;
        private bool checkFundos;
        private bool checkRendaFixa;
        #endregion

        #region Construtor
        public ConsultaAtivos(DateTime data, string login, bool checkRendaVariavel, bool checkFundos, bool checkRendaFixa)
        {
            this.data = data;
            this.login = login;
            this.checkRendaVariavel = checkRendaVariavel;
            this.checkFundos = checkFundos;
            this.checkRendaFixa = checkRendaFixa;
        }
        #endregion

        public List<ConsultaDados> RetornaSaldoTipoCliente()
        {
            List<ConsultaDados> lista = new List<ConsultaDados>();

            List<int> listaMaes = new List<int>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.es.Distinct = true;
            carteiraMaeCollection.Query.Load();

            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
	        {
                listaMaes.Add(carteiraMae.IdCarteiraMae.Value);
	        }

            if (this.checkRendaVariavel)
            {
                #region Tratamento para Acoes
                ClienteQuery clienteQuery = new ClienteQuery("C");
                PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("T");
                
                clienteQuery.Select(tipoClienteQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoBolsaHistoricoQuery.ResultadoRealizar.Sum());

                clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
                clienteQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(posicaoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico == this.data,
                    posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.MercadoVista));

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(tipoClienteQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 1;//acoes
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                    string descricao2 = "Ações";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar)); ;
                    lista.Add(consultaDados);
                }
                #endregion

                #region Tratamento para Imobiliario
                clienteQuery = new ClienteQuery("C");
                posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                tipoClienteQuery = new TipoClienteQuery("T");

                clienteQuery.Select(tipoClienteQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoBolsaHistoricoQuery.ResultadoRealizar.Sum());

                clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
                clienteQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(posicaoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico == this.data,
                    posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.Imobiliario));

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(tipoClienteQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 2;//imobiliario
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                    string descricao2 = "Imobiliário";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar)); ;
                    lista.Add(consultaDados);
                }
                #endregion

                #region Tratamento para Opcoes
                clienteQuery = new ClienteQuery("C");
                posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                tipoClienteQuery = new TipoClienteQuery("T");

                clienteQuery.Select(tipoClienteQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoBolsaHistoricoQuery.ResultadoRealizar.Sum());

                clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
                clienteQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(posicaoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico == this.data,
                    posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(tipoClienteQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 3;//opcoes
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                    string descricao2 = "Opções";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar));
                    lista.Add(consultaDados);
                }
                #endregion

                #region Tratamento para PosicaoTermo
                clienteQuery = new ClienteQuery("C");
                PosicaoTermoBolsaHistoricoQuery posicaoTermoBolsaHistoricoQuery = new PosicaoTermoBolsaHistoricoQuery("P");
                tipoClienteQuery = new TipoClienteQuery("T");

                clienteQuery.Select(tipoClienteQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoTermoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoTermoBolsaHistoricoQuery.RendimentoTotal.Sum());

                clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
                clienteQuery.InnerJoin(posicaoTermoBolsaHistoricoQuery).On(posicaoTermoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.Where(posicaoTermoBolsaHistoricoQuery.DataHistorico == this.data);

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(tipoClienteQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 4;//termo
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn(TipoClienteMetadata.ColumnNames.Descricao));
                    string descricao2 = "Termo";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.RendimentoTotal));
                    lista.Add(consultaDados);
                }
                #endregion
            }

            if (checkFundos)
            {
                #region Tratamento para PosicaoFundo
                ClienteQuery clienteQuery = new ClienteQuery("C");
                CarteiraQuery carteiraQuery = new CarteiraQuery("R");
                PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
                TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("T");
                CategoriaFundoQuery categoriaFundoQuery = new CategoriaFundoQuery("A");

                clienteQuery.Select(tipoClienteQuery.Descricao.As("DescricaoTipoCliente"),
                                    categoriaFundoQuery.Descricao.As("DescricaoCategoriaFundo"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoFundoHistoricoQuery.ValorBruto.Sum(),
                                    posicaoFundoHistoricoQuery.ValorLiquido.Sum(),
                                    ((posicaoFundoHistoricoQuery.CotaDia - posicaoFundoHistoricoQuery.CotaAplicacao) * posicaoFundoHistoricoQuery.Quantidade).Sum().As("Rendimento"),
                                    (posicaoFundoHistoricoQuery.ValorIR + posicaoFundoHistoricoQuery.ValorIOF).Sum().As("Tributos")

                                    );

                clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
                clienteQuery.InnerJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);

                clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
                clienteQuery.InnerJoin(categoriaFundoQuery).On(categoriaFundoQuery.IdCategoria == carteiraQuery.IdCategoria);

                clienteQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == this.data);

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(tipoClienteQuery.Descricao,
                                    categoriaFundoQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 5;//cotas inv.
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("DescricaoTipoCliente"));
                    string descricao2 = Convert.ToString(cliente.GetColumn("DescricaoCategoriaFundo"));
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoFundoHistoricoMetadata.ColumnNames.ValorBruto));
                    consultaDados.SaldoLiquido = Convert.ToDecimal(cliente.GetColumn(PosicaoFundoHistoricoMetadata.ColumnNames.ValorLiquido));
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn("Rendimento"));
                    consultaDados.Tributos = Convert.ToDecimal(cliente.GetColumn("Tributos"));

                    lista.Add(consultaDados);
                }
                #endregion
            }

            if (checkRendaFixa)
            {
                #region Tratamento para PosicaoRendaFixa
                ClienteQuery clienteQuery = new ClienteQuery("C");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("I");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
                TipoClienteQuery tipoClienteQuery = new TipoClienteQuery("T");

                clienteQuery.Select(tipoClienteQuery.Descricao.As("DescricaoTipoCliente"),
                                    papelRendaFixaQuery.Descricao.As("DescricaoPapelRendaFixa"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoRendaFixaHistoricoQuery.ValorMercado.Sum(),
                                    ((posicaoRendaFixaHistoricoQuery.PUMercado - posicaoRendaFixaHistoricoQuery.PUOperacao) * posicaoRendaFixaHistoricoQuery.Quantidade).Sum().As("Rendimento"),
                                    (posicaoRendaFixaHistoricoQuery.ValorIR + posicaoRendaFixaHistoricoQuery.ValorIOF).Sum().As("Tributos")
                                    );

                clienteQuery.InnerJoin(tipoClienteQuery).On(tipoClienteQuery.IdTipo == clienteQuery.IdTipo);
                clienteQuery.InnerJoin(posicaoRendaFixaHistoricoQuery).On(posicaoRendaFixaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                clienteQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                clienteQuery.Where(posicaoRendaFixaHistoricoQuery.DataHistorico == this.data);

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(tipoClienteQuery.Descricao,
                                    papelRendaFixaQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 6;//rf
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("DescricaoTipoCliente"));
                    string descricao2 = Convert.ToString(cliente.GetColumn("DescricaoPapelRendaFixa"));
                    string descricao3 = cliente.Nome;

                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoRendaFixaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.Tributos = Convert.ToDecimal(cliente.GetColumn("Tributos"));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto - consultaDados.Tributos;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn("Rendimento"));

                    lista.Add(consultaDados);
                }
                #endregion
            }

            return lista;
        }

        public List<ConsultaDados> RetornaSaldoInstituicao()
        {
            List<ConsultaDados> lista = new List<ConsultaDados>();

            List<int> listaMaes = new List<int>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.es.Distinct = true;
            carteiraMaeCollection.Query.Load();

            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                listaMaes.Add(carteiraMae.IdCarteiraMae.Value);
            }

            if (checkRendaVariavel)
            {
                #region Tratamento para Acoes
                ClienteQuery clienteQuery = new ClienteQuery("C");
                PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                EmissorQuery emissorQuery = new EmissorQuery("E");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");

                clienteQuery.Select(emissorQuery.Nome.As("NomeEmissor"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoBolsaHistoricoQuery.ResultadoRealizar.Sum());

                clienteQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(posicaoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
                clienteQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == ativoBolsaQuery.IdEmissor);
                clienteQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico == this.data,
                    posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.MercadoVista));

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(emissorQuery.Nome,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 1;//acoes
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeEmissor"));
                    string descricao2 = "Ações";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar));
                    lista.Add(consultaDados);
                }
                #endregion

                #region Tratamento para Imobiliario
                clienteQuery = new ClienteQuery("C");
                posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                emissorQuery = new EmissorQuery("E");
                ativoBolsaQuery = new AtivoBolsaQuery("A");

                clienteQuery.Select(emissorQuery.Nome.As("NomeEmissor"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoBolsaHistoricoQuery.ResultadoRealizar.Sum());

                clienteQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(posicaoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
                clienteQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == ativoBolsaQuery.IdEmissor);
                clienteQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico == this.data,
                    posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.Imobiliario));

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(emissorQuery.Nome,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 2;//Imobiliario
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeEmissor"));
                    string descricao2 = "Imobiliário";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar));
                    lista.Add(consultaDados);
                }
                #endregion

                #region Tratamento para Opcoes
                clienteQuery = new ClienteQuery("C");
                posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                emissorQuery = new EmissorQuery("E");
                ativoBolsaQuery = new AtivoBolsaQuery("A");

                clienteQuery.Select(emissorQuery.Nome.As("NomeEmissor"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoBolsaHistoricoQuery.ResultadoRealizar.Sum());

                clienteQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(posicaoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
                clienteQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == ativoBolsaQuery.IdEmissor);
                clienteQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico == this.data,
                    posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(emissorQuery.Nome,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 3;//Opcoes
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeEmissor"));
                    string descricao2 = "Opções";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar));
                    lista.Add(consultaDados);
                }
                #endregion

                #region Tratamento para Termos
                clienteQuery = new ClienteQuery("C");
                PosicaoTermoBolsaHistoricoQuery posicaoTermoBolsaHistoricoQuery = new PosicaoTermoBolsaHistoricoQuery("P");
                emissorQuery = new EmissorQuery("E");
                ativoBolsaQuery = new AtivoBolsaQuery("A");

                clienteQuery.Select(emissorQuery.Nome.As("NomeEmissor"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoTermoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoTermoBolsaHistoricoQuery.RendimentoTotal.Sum());

                clienteQuery.InnerJoin(posicaoTermoBolsaHistoricoQuery).On(posicaoTermoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoTermoBolsaHistoricoQuery.CdAtivoBolsa);
                clienteQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == ativoBolsaQuery.IdEmissor);
                clienteQuery.Where(posicaoTermoBolsaHistoricoQuery.DataHistorico == this.data);

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(emissorQuery.Nome,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 4;//termo
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeEmissor"));
                    string descricao2 = "Termo";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.RendimentoTotal));
                    lista.Add(consultaDados);
                }
                #endregion
            }

            if (checkFundos)
            {
                #region Tratamento para PosicaoFundo
                ClienteQuery clienteQuery = new ClienteQuery("C");
                CarteiraQuery carteiraQuery = new CarteiraQuery("R");
                PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
                AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("A");
                CategoriaFundoQuery categoriaFundoQuery = new CategoriaFundoQuery("T");

                clienteQuery.Select(agenteMercadoQuery.Nome.As("NomeAgente"),
                                    categoriaFundoQuery.Descricao.As("DescricaoCategoriaFundo"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoFundoHistoricoQuery.ValorBruto.Sum(),
                                    posicaoFundoHistoricoQuery.ValorLiquido.Sum(),
                                    ((posicaoFundoHistoricoQuery.CotaDia - posicaoFundoHistoricoQuery.CotaAplicacao) * posicaoFundoHistoricoQuery.Quantidade).Sum().As("Rendimento"),
                                    (posicaoFundoHistoricoQuery.ValorIR + posicaoFundoHistoricoQuery.ValorIOF).Sum().As("Tributos")
                                    );

                clienteQuery.InnerJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
                clienteQuery.InnerJoin(categoriaFundoQuery).On(categoriaFundoQuery.IdCategoria == carteiraQuery.IdCategoria);
                clienteQuery.InnerJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteGestor);
                clienteQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == this.data);

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(agenteMercadoQuery.Nome,
                                    categoriaFundoQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 5;//cotas inv.
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeAgente"));
                    string descricao2 = Convert.ToString(cliente.GetColumn("DescricaoCategoriaFundo"));
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoFundoHistoricoMetadata.ColumnNames.ValorBruto));
                    consultaDados.SaldoLiquido = Convert.ToDecimal(cliente.GetColumn(PosicaoFundoHistoricoMetadata.ColumnNames.ValorLiquido));
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn("Rendimento"));
                    consultaDados.Tributos = Convert.ToDecimal(cliente.GetColumn("Tributos"));

                    lista.Add(consultaDados);
                }
                #endregion
            }

            if (checkRendaFixa)
            {
                #region Tratamento para PosicaoRendaFixa
                ClienteQuery clienteQuery = new ClienteQuery("C");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("I");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
                EmissorQuery emissorQuery = new EmissorQuery("E");

                clienteQuery.Select(emissorQuery.Nome.As("NomeEmissor"),
                                    papelRendaFixaQuery.Descricao.As("DescricaoPapelRendaFixa"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoRendaFixaHistoricoQuery.ValorMercado.Sum(),
                                    ((posicaoRendaFixaHistoricoQuery.PUMercado - posicaoRendaFixaHistoricoQuery.PUOperacao) * posicaoRendaFixaHistoricoQuery.Quantidade).Sum().As("Rendimento"),
                                    (posicaoRendaFixaHistoricoQuery.ValorIR + posicaoRendaFixaHistoricoQuery.ValorIOF).Sum().As("Tributos")
                                    );

                clienteQuery.InnerJoin(posicaoRendaFixaHistoricoQuery).On(posicaoRendaFixaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                clienteQuery.InnerJoin(emissorQuery).On(emissorQuery.IdEmissor == tituloRendaFixaQuery.IdEmissor);
                clienteQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                clienteQuery.Where(posicaoRendaFixaHistoricoQuery.DataHistorico == this.data);

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(emissorQuery.Nome,
                                    papelRendaFixaQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 6;//rf
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeEmissor"));
                    string descricao2 = Convert.ToString(cliente.GetColumn("DescricaoPapelRendaFixa"));
                    string descricao3 = cliente.Nome;

                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoRendaFixaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.Tributos = Convert.ToDecimal(cliente.GetColumn("Tributos"));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto - consultaDados.Tributos;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn("Rendimento"));

                    lista.Add(consultaDados);
                }
                #endregion
            }

            return lista;
        }

        public List<ConsultaDados> RetornaSaldoMoeda()
        {
            List<ConsultaDados> lista = new List<ConsultaDados>();

            List<int> listaMaes = new List<int>();

            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraMae);
            carteiraMaeCollection.Query.es.Distinct = true;
            carteiraMaeCollection.Query.Load();

            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                listaMaes.Add(carteiraMae.IdCarteiraMae.Value);
            }

            if (checkRendaVariavel)
            {
                #region Tratamento para Acoes
                ClienteQuery clienteQuery = new ClienteQuery("C");
                PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                MoedaQuery moedaQuery = new MoedaQuery("M");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");

                clienteQuery.Select(moedaQuery.Nome.As("NomeMoeda"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoBolsaHistoricoQuery.ResultadoRealizar.Sum());

                clienteQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(posicaoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
                clienteQuery.InnerJoin(moedaQuery).On(moedaQuery.IdMoeda == ativoBolsaQuery.IdMoeda);
                clienteQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico == this.data,
                    posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.MercadoVista));

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(moedaQuery.Nome,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 1;//acoes
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeMoeda"));
                    string descricao2 = "Ações";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar));
                    lista.Add(consultaDados);
                }
                #endregion

                #region Tratamento para Imobiliario
                clienteQuery = new ClienteQuery("C");
                posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                moedaQuery = new MoedaQuery("M");
                ativoBolsaQuery = new AtivoBolsaQuery("A");

                clienteQuery.Select(moedaQuery.Nome.As("NomeMoeda"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoBolsaHistoricoQuery.ResultadoRealizar.Sum());

                clienteQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(posicaoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
                clienteQuery.InnerJoin(moedaQuery).On(moedaQuery.IdMoeda == ativoBolsaQuery.IdMoeda);
                clienteQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico == this.data,
                    posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.Imobiliario));

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(moedaQuery.Nome,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 2;//Imobiliario
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeMoeda"));
                    string descricao2 = "Imobiliário";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar));
                    lista.Add(consultaDados);
                }
                #endregion

                #region Tratamento para Opcoes
                clienteQuery = new ClienteQuery("C");
                posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
                moedaQuery = new MoedaQuery("M");
                ativoBolsaQuery = new AtivoBolsaQuery("A");

                clienteQuery.Select(moedaQuery.Nome.As("NomeMoeda"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoBolsaHistoricoQuery.ResultadoRealizar.Sum());

                clienteQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(posicaoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
                clienteQuery.InnerJoin(moedaQuery).On(moedaQuery.IdMoeda == ativoBolsaQuery.IdMoeda);
                clienteQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico == this.data,
                    posicaoBolsaHistoricoQuery.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(moedaQuery.Nome,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 3;//Opcoes
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeMoeda"));
                    string descricao2 = "Opções";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar));
                    lista.Add(consultaDados);
                }
                #endregion

                #region Tratamento para Termos
                clienteQuery = new ClienteQuery("C");
                PosicaoTermoBolsaHistoricoQuery posicaoTermoBolsaHistoricoQuery = new PosicaoTermoBolsaHistoricoQuery("P");
                moedaQuery = new MoedaQuery("M");
                ativoBolsaQuery = new AtivoBolsaQuery("A");

                clienteQuery.Select(moedaQuery.Nome.As("NomeMoeda"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoTermoBolsaHistoricoQuery.ValorMercado.Sum(),
                                    posicaoTermoBolsaHistoricoQuery.RendimentoTotal.Sum());

                clienteQuery.InnerJoin(posicaoTermoBolsaHistoricoQuery).On(posicaoTermoBolsaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoTermoBolsaHistoricoQuery.CdAtivoBolsa);
                clienteQuery.InnerJoin(moedaQuery).On(moedaQuery.IdMoeda == ativoBolsaQuery.IdMoeda);
                clienteQuery.Where(posicaoTermoBolsaHistoricoQuery.DataHistorico == this.data);

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(moedaQuery.Nome,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 4;//termo
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeMoeda"));
                    string descricao2 = "Termo";
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto;
                    consultaDados.Tributos = 0;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.RendimentoTotal));
                    lista.Add(consultaDados);
                }
                #endregion
            }

            if (checkFundos)
            {
                #region Tratamento para PosicaoFundo
                ClienteQuery clienteQuery = new ClienteQuery("C");
                ClienteQuery carteiraMoedaQuery = new ClienteQuery("T");
                CarteiraQuery carteiraQuery = new CarteiraQuery("R");
                PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
                MoedaQuery moedaQuery = new MoedaQuery("M");
                CategoriaFundoQuery categoriaFundoQuery = new CategoriaFundoQuery("A");

                clienteQuery.Select(moedaQuery.Nome.As("NomeMoeda"),
                                    categoriaFundoQuery.Descricao.As("DescricaoCategoriaFundo"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoFundoHistoricoQuery.ValorBruto.Sum(),
                                    posicaoFundoHistoricoQuery.ValorLiquido.Sum(),
                                    ((posicaoFundoHistoricoQuery.CotaDia - posicaoFundoHistoricoQuery.CotaAplicacao) * posicaoFundoHistoricoQuery.Quantidade).Sum().As("Rendimento"),
                                    (posicaoFundoHistoricoQuery.ValorIR + posicaoFundoHistoricoQuery.ValorIOF).Sum().As("Tributos")
                                    );

                clienteQuery.InnerJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoHistoricoQuery.IdCarteira);
                clienteQuery.InnerJoin(carteiraMoedaQuery).On(carteiraMoedaQuery.IdCliente == posicaoFundoHistoricoQuery.IdCarteira);
                clienteQuery.InnerJoin(moedaQuery).On(moedaQuery.IdMoeda == carteiraMoedaQuery.IdMoeda);
                clienteQuery.InnerJoin(categoriaFundoQuery).On(categoriaFundoQuery.IdCategoria == carteiraQuery.IdCategoria);

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.Where(posicaoFundoHistoricoQuery.DataHistorico == this.data);

                clienteQuery.GroupBy(moedaQuery.Nome,
                                    categoriaFundoQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 5;//cotas inv.
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeMoeda"));
                    string descricao2 = Convert.ToString(cliente.GetColumn("DescricaoCategoriaFundo"));
                    string descricao3 = cliente.Nome;

                    //
                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoFundoHistoricoMetadata.ColumnNames.ValorBruto));
                    consultaDados.SaldoLiquido = Convert.ToDecimal(cliente.GetColumn(PosicaoFundoHistoricoMetadata.ColumnNames.ValorLiquido));
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn("Rendimento"));
                    consultaDados.Tributos = Convert.ToDecimal(cliente.GetColumn("Tributos"));

                    lista.Add(consultaDados);
                }
                #endregion
            }

            if (checkRendaFixa)
            {
                #region Tratamento para PosicaoRendaFixa
                ClienteQuery clienteQuery = new ClienteQuery("C");
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("I");
                PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("A");
                MoedaQuery moedaQuery = new MoedaQuery("M");

                clienteQuery.Select(moedaQuery.Nome.As("NomeMoeda"),
                                    papelRendaFixaQuery.Descricao.As("DescricaoPapelRendaFixa"),
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome,
                                    posicaoRendaFixaHistoricoQuery.ValorMercado.Sum(),
                                    ((posicaoRendaFixaHistoricoQuery.PUMercado - posicaoRendaFixaHistoricoQuery.PUOperacao) * posicaoRendaFixaHistoricoQuery.Quantidade).Sum().As("Rendimento"),
                                    (posicaoRendaFixaHistoricoQuery.ValorIR + posicaoRendaFixaHistoricoQuery.ValorIOF).Sum().As("Tributos")
                                    );

                clienteQuery.InnerJoin(posicaoRendaFixaHistoricoQuery).On(posicaoRendaFixaHistoricoQuery.IdCliente == clienteQuery.IdCliente);
                clienteQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                clienteQuery.InnerJoin(moedaQuery).On(moedaQuery.IdMoeda == tituloRendaFixaQuery.IdMoeda);
                clienteQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                clienteQuery.Where(posicaoRendaFixaHistoricoQuery.DataHistorico == this.data);

                if (listaMaes.Count > 0)
                {
                    clienteQuery.Where(clienteQuery.IdCliente.NotIn(listaMaes));
                }

                clienteQuery.GroupBy(moedaQuery.Nome,
                                    papelRendaFixaQuery.Descricao,
                                    clienteQuery.IdCliente,
                                    clienteQuery.Nome);

                ClienteCollection clienteCollection = new ClienteCollection();
                clienteCollection.Load(clienteQuery);

                foreach (Cliente cliente in clienteCollection)
                {
                    int idAgrupamento = 6;//rf
                    int idCliente = cliente.IdCliente.Value;
                    string descricao1 = Convert.ToString(cliente.GetColumn("NomeMoeda"));
                    string descricao2 = Convert.ToString(cliente.GetColumn("DescricaoPapelRendaFixa"));
                    string descricao3 = cliente.Nome;

                    ConsultaDados consultaDados = new ConsultaDados();
                    consultaDados.Descricao1 = descricao1;
                    consultaDados.Descricao2 = descricao2;
                    consultaDados.Descricao3 = descricao3;
                    consultaDados.IdAgrupamento = idAgrupamento;
                    consultaDados.IdCliente = idCliente;
                    consultaDados.SaldoBruto = Convert.ToDecimal(cliente.GetColumn(PosicaoRendaFixaHistoricoMetadata.ColumnNames.ValorMercado));
                    consultaDados.Tributos = Convert.ToDecimal(cliente.GetColumn("Tributos"));
                    consultaDados.SaldoLiquido = consultaDados.SaldoBruto - consultaDados.Tributos;
                    consultaDados.Rendimento = Convert.ToDecimal(cliente.GetColumn("Rendimento"));

                    lista.Add(consultaDados);
                }
                #endregion
            }

            return lista;
        }
        
        public DataTable RetornaSaldoCliente(TipoAgrupamento tipoAgrupamento)
        {
            List<ConsultaDados> lista = new List<ConsultaDados>();

            if (tipoAgrupamento == TipoAgrupamento.TipoCliente)
            {
                lista = this.RetornaSaldoTipoCliente();
            }
            else if (tipoAgrupamento == TipoAgrupamento.Moeda)
            {
                lista = this.RetornaSaldoMoeda();
            }
            else if (tipoAgrupamento == TipoAgrupamento.Instituicao)
            {
                lista = this.RetornaSaldoInstituicao();
            }

            DataTable table = this.GeraDataTable(lista);

            return table;
        }

        
        /// <summary>
        /// A partir da Classe ConsultaDados gera um DataTable no formato Id/Descricao1/Descricao2/Descricao3/IdCliente.
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        private DataTable GeraDataTable(List<ConsultaDados> c)
        {
            DataTable dt = new DataTable();
            //
            dt.Columns.Add("Id", typeof(int)); // Chave do Grid
            dt.Columns.Add("Descricao1", typeof(string));
            dt.Columns.Add("Descricao2", typeof(string));
            dt.Columns.Add("Descricao3", typeof(string));
            dt.Columns.Add("IdCliente", typeof(int));

            dt.Columns.Add("SaldoBruto", typeof(System.Decimal));
            dt.Columns.Add("Tributos", typeof(System.Decimal));
            dt.Columns.Add("SaldoLiquido", typeof(System.Decimal));
            dt.Columns.Add("Rendimento", typeof(System.Decimal));

            //Adiciona Dados
            for (int i = 0; i < c.Count; i++)
            {
                dt.Rows.Add(new object[] { i, c[i].Descricao1.Trim(), c[i].Descricao2.Trim(), c[i].Descricao3.Trim(), c[i].IdCliente });
                //
                DataRow dr = dt.Rows[i];
                //

                dr["SaldoBruto"] = c[i].SaldoBruto;
                dr["SaldoLiquido"] = c[i].SaldoLiquido;
                dr["Tributos"] = c[i].Tributos;
                dr["Rendimento"] = c[i].Rendimento;

            }

            return dt;
        }

    }
}