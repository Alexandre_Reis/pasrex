﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using System.Collections;

namespace Financial.Fundo
{
	public partial class PrejuizoFundo : esPrejuizoFundo
	{
        /// <summary>
        /// Retorna o prejuízo a compensar do cliente, levando-se em conta todos os fundos nos quais ele tem prejuízo
        /// a compensar, dados o mesmo administrador e tipo de tributação do idCarteira passado.
        /// Filtra pela DataLimiteCompensacao = null ou DataLimiteCompensacao >= data.
        /// </summary>        
        /// <param name="data"></param>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="tipoTributacao"></param>   
        /// <param name="idCliente"></param>   
        /// <returns>valor total do prejuízo a compensar nos fundos com posição pelo costista</returns>
        public decimal RetornaPrejuizoCompensar(DateTime data, int idAgenteAdministrador, int tipoTributacao,
                                                int idCliente)
        {
            #region Carrega em <ListaCarteira> os fundos que sejam do mesmo administrador e mesma categoria tributária
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.BuscaCarteira(idAgenteAdministrador, tipoTributacao);
            ArrayList listaCarteira = new ArrayList();
            for (int i = 0; i < carteiraCollection.Count; i++)
            {
                Carteira carteira = (Carteira)carteiraCollection[i];
                listaCarteira.Add(carteira.IdCarteira.Value);
            }
            #endregion

            if (carteiraCollection.Count == 0)
            {
                return 0;
            }

            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorPrejuizo.Sum())
                 .Where(this.Query.IdCarteira.In(listaCarteira.ToArray()),
                        this.Query.IdCliente == idCliente,
                        this.Query.Or(
                            this.Query.DataLimiteCompensacao.IsNull(),
                            this.Query.DataLimiteCompensacao.GreaterThanOrEqual(data))
                            );

            this.Query.Load();

            return this.ValorPrejuizo.HasValue ? (decimal)this.ValorPrejuizo : 0;
        }

        /// <summary>
        /// Trata valores de rendimento negativos (acumular como prejuízo) 
        /// e valores de rendimento positivos (a serem compensados com prejuízos em outros fundos de mesma categoria
        /// e administrador).
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="tipoTributacao"></param>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <param name="valorRendimento"></param>
        public void TrataCompensacaoPrejuizo(DateTime data, int idAgenteAdministrador, int tipoTributacao,
                                                int idCarteira, int idCliente, decimal valorRendimento,
                                                decimal prejuizoUsado)
        {
            decimal prejuizoCompensado = prejuizoUsado;
            if (valorRendimento < 0)
            {
                #region Acumula prejuízo a compensar para o idCarteira/idCliente
                PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                if (prejuizoFundo.LoadByPrimaryKey(idCliente, idCarteira, data))
                {
                    prejuizoFundo.ValorPrejuizo += Math.Abs(valorRendimento); //Acumula o prejuízo
                    prejuizoFundo.Save();
                }
                else
                {
                    //Cria nova linha de prejuízo
                    PrejuizoFundo prejuizoFundoInserir = new PrejuizoFundo();
                    prejuizoFundoInserir.AddNew();
                    prejuizoFundoInserir.Data = data;
                    prejuizoFundoInserir.IdCarteira = idCarteira;
                    prejuizoFundoInserir.IdCliente = idCliente;
                    prejuizoFundoInserir.ValorPrejuizo = Math.Abs(valorRendimento);
                    prejuizoFundoInserir.Save();
                }
                #endregion
            }

            if (prejuizoUsado > 0)
            {
                #region Baixa cada uma das linhas de prejuízo encontradas, até zerar o rendimento (ou acabarem as linhas)
                //Busca todos os valores de prejuízo a compensar por fundo
                PrejuizoFundoCollection prejuizoFundoCollection = new PrejuizoFundoCollection();
                prejuizoFundoCollection.BuscaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, idCliente);

                for (int i = 0; i < prejuizoFundoCollection.Count; i++)
                {
                    PrejuizoFundo prejuizoFundo = prejuizoFundoCollection[i];
                    int idCarteiraCompensar = prejuizoFundo.IdCarteira.Value;
                    decimal valorPrejuizoCompensar = prejuizoFundo.ValorPrejuizo.Value;

                    if (valorPrejuizoCompensar > prejuizoCompensado)
                    {
                        //Compensa todo o rendimento, baixa parcialmente a última linha de prejuízo a compensar                        
                        prejuizoFundo.ValorPrejuizo -= prejuizoCompensado;
                        prejuizoCompensado = 0;
                    }
                    else
                    {
                        //Compensa parcialmente o rendimento, zera a linha de prejuízo a compensar                        
                        prejuizoFundo.ValorPrejuizo = 0;
                        prejuizoCompensado -= valorPrejuizoCompensar;
                    }
                }

                prejuizoFundoCollection.Save();
                #endregion
            }

        }

        /// <summary>
        /// Para os casos de resgates totais, a data limite de compensação deve ser atualizad
        /// para o últmo dia útil do ano seguinte.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        public void AtualizaDataLimite(DateTime data, int idCarteira, int idCliente)
        {
            DateTime ultimoDiaAnoSeguinte = Calendario.RetornaUltimoDiaUtilAno(data, 1);
            PrejuizoFundoCollection prejuizoFundoCollection = new PrejuizoFundoCollection();
            prejuizoFundoCollection.BuscaPrejuizoCompensar(data, idCarteira, idCliente);

            for (int i = 0; i < prejuizoFundoCollection.Count; i++)
            {
                prejuizoFundoCollection[i].DataLimiteCompensacao = ultimoDiaAnoSeguinte;
            }
            prejuizoFundoCollection.Save();
        }

        /// <summary>
        /// Função básica para copiar de PrejuizoFundo para PrejuizoFundoHistorico.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            PrejuizoFundoHistoricoCollection prejuizoFundoDeletarHistoricoCollection = new PrejuizoFundoHistoricoCollection();
            prejuizoFundoDeletarHistoricoCollection.DeletaPrejuizoFundoHistoricoDataHistoricoMaiorIgual(idCliente, data);

            PrejuizoFundoCollection prejuizoFundoCollection = new PrejuizoFundoCollection();
            prejuizoFundoCollection.BuscaPrejuizoFundoCompleto(idCliente);
            //
            PrejuizoFundoHistoricoCollection prejuizoFundoHistoricoCollection = new PrejuizoFundoHistoricoCollection();
            //
            #region Copia de PrejuizoFundo para PrejuizoFundoHistorico
            for (int i = 0; i < prejuizoFundoCollection.Count; i++)
            {
                PrejuizoFundo prejuizoFundo = prejuizoFundoCollection[i];

                // Copia de PrejuizoFundo para PrejuizoFundoHistorico
                PrejuizoFundoHistorico prejuizoFundoHistorico = prejuizoFundoHistoricoCollection.AddNew();
                //
                prejuizoFundoHistorico.DataHistorico = data;
                prejuizoFundoHistorico.IdCliente = prejuizoFundo.IdCliente;
                prejuizoFundoHistorico.IdCarteira = prejuizoFundo.IdCarteira;
                prejuizoFundoHistorico.Data = prejuizoFundo.Data;
                prejuizoFundoHistorico.ValorPrejuizo = prejuizoFundo.ValorPrejuizo;
                prejuizoFundoHistorico.DataLimiteCompensacao = prejuizoFundo.DataLimiteCompensacao;
            }
            #endregion

            prejuizoFundoHistoricoCollection.Save();
        }

	}
}
