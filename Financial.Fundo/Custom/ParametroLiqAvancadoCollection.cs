﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/12/2014 17:49:05
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;

namespace Financial.Fundo
{
	public partial class ParametroLiqAvancadoCollection : esParametroLiqAvancadoCollection
	{
        public string MontaStringParametroLiqAvanc(int idCliente, DateTime dtVigencia, int enumTipoData, int TipoOperacao, string parametroValido, bool persiste)
        {
            ParametroLiqAvancado parametroLiqAvancado = new ParametroLiqAvancado();
            string parametroComposto = string.Empty;
            ParametroLiqAvancadoAnaliticoCollection parametroAvancAnaColl = new ParametroLiqAvancadoAnaliticoCollection();
            parametroAvancAnaColl.Query.Where(parametroAvancAnaColl.Query.IdCarteira.Equal(idCliente)
                                              & parametroAvancAnaColl.Query.DataVigencia.Equal(dtVigencia)
                                              & parametroAvancAnaColl.Query.TipoData.Equal(enumTipoData)
                                              & parametroAvancAnaColl.Query.TipoOperacao.Equal(TipoOperacao));
            parametroAvancAnaColl.Query.OrderBy(parametroAvancAnaColl.Query.OrdemProcessamento.Ascending);

            if (parametroAvancAnaColl.Query.Load())
            {
                foreach (ParametroLiqAvancadoAnalitico parametroAvancAna in parametroAvancAnaColl)
                {
                    if (!string.IsNullOrEmpty(parametroComposto))
                        parametroComposto = string.Concat(parametroComposto, " \\ ");

                    parametroComposto = string.Concat(parametroComposto, parametroAvancAna.ParametroComposto);
                }

                if (persiste)
                {
                    #region Deletar Registro Antigo
                    parametroLiqAvancado = new ParametroLiqAvancado();
                    parametroLiqAvancado.Query.Where(parametroLiqAvancado.Query.IdCarteira.Equal(idCliente)
                                                      & parametroLiqAvancado.Query.DataVigencia.Equal(dtVigencia)
                                                      & parametroLiqAvancado.Query.TipoData.Equal(enumTipoData)
                                                      & parametroLiqAvancado.Query.TipoOperacao.Equal(TipoOperacao));
                    if (parametroLiqAvancado.Query.Load())
                    {
                        parametroLiqAvancado.MarkAsDeleted();
                        parametroLiqAvancado.Save();
                    }
                    #endregion

                    parametroLiqAvancado = new ParametroLiqAvancado();
                    parametroLiqAvancado.IdCarteira = idCliente;
                    parametroLiqAvancado.DataVigencia = dtVigencia;
                    parametroLiqAvancado.TipoData = enumTipoData;
                    parametroLiqAvancado.TipoOperacao = TipoOperacao;
                    parametroLiqAvancado.Parametro = parametroComposto;
                    parametroLiqAvancado.ParametroValido = parametroValido;

                    parametroLiqAvancado.Save();
                    parametroAvancAnaColl.Save();
                }
            }

            #region Verifica se existe registro para data operação (Não obrigatória, porém, é usada como chave no grid da tela)            
            parametroLiqAvancado.Query.Where(parametroLiqAvancado.Query.IdCarteira.Equal(idCliente)
                                              & parametroLiqAvancado.Query.DataVigencia.Equal(dtVigencia)
                                              & parametroLiqAvancado.Query.TipoData.Equal((int)TipoParametroLiquidacao.DataOperacao)
                                              & parametroLiqAvancado.Query.TipoOperacao.Equal(TipoOperacao));

            if (!parametroLiqAvancado.Query.Load())
            {
                parametroLiqAvancado = new ParametroLiqAvancado();
                parametroLiqAvancado.IdCarteira = idCliente;
                parametroLiqAvancado.DataVigencia = dtVigencia;
                parametroLiqAvancado.TipoData = (int)TipoParametroLiquidacao.DataOperacao;
                parametroLiqAvancado.TipoOperacao = TipoOperacao;
                parametroLiqAvancado.Parametro = string.Empty;
                parametroLiqAvancado.ParametroValido = "S";

                parametroLiqAvancado.Save();
            }
            #endregion

            return parametroComposto;
        }
	}
}
