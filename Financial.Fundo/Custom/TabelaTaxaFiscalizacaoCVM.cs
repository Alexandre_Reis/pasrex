﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Fundo.Exceptions;

namespace Financial.Fundo
{
	public partial class TabelaTaxaFiscalizacaoCVM : esTabelaTaxaFiscalizacaoCVM
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaTaxaFiscalizacaoCVM));

        /// <summary>
        /// Carrega o objeto TabelaTaxaFiscalizacaoCVM com o campo ValorTaxa.
        /// Filtra FaixaPL menor ou igual a valorPL.
        /// TabelaTaxaFiscalizacaoCVMNaoCadastradoException para 0 registro.
        /// </summary>
        /// <param name="tipoCVM"></param>
        /// <param name="valorPLMedio"></param>        
        public void BuscaTaxaFiscalizacaoCVM(int tipoCVM, decimal valorPLMedio)
        {
            // TODO logar
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }

            TabelaTaxaFiscalizacaoCVMCollection tabelaTaxaFiscalizacaoCVMCollection = new TabelaTaxaFiscalizacaoCVMCollection();

            tabelaTaxaFiscalizacaoCVMCollection.Query
                 .Select(tabelaTaxaFiscalizacaoCVMCollection.Query.ValorTaxa)
                 .Where(tabelaTaxaFiscalizacaoCVMCollection.Query.TipoCVM == tipoCVM,
                        tabelaTaxaFiscalizacaoCVMCollection.Query.FaixaPL.LessThanOrEqual(valorPLMedio))
                 .OrderBy(tabelaTaxaFiscalizacaoCVMCollection.Query.FaixaPL.Descending);

            tabelaTaxaFiscalizacaoCVMCollection.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                StringBuilder sql = new StringBuilder(tabelaTaxaFiscalizacaoCVMCollection.Query.es.LastQuery);
                sql = sql.Replace("@TipoCVM1", "'" + tipoCVM + "'");
                sql = sql.Replace("@FaixaPL2", "'" + valorPLMedio + "'");
                log.Info(sql);
            }
            #endregion

            if (tabelaTaxaFiscalizacaoCVMCollection.HasData)
            {
                // Copia informações de tabelaTaxaFiscalizacaoCVMCollection para TabelaTaxaFiscalizacaoCVM
                this.AddNew();
                this.ValorTaxa = ((TabelaTaxaFiscalizacaoCVM)tabelaTaxaFiscalizacaoCVMCollection[0]).ValorTaxa;                
            }
            else
            {
                throw new TabelaTaxaFiscalizacaoCVMNaoCadastradoException("Tabela Fiscalização CVM não cadastrada para o tipoCVM " + tipoCVM + ", faixaPL " + valorPLMedio);
            }

            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nome Metodo: ");
            }
            #endregion
        }

	}
}
