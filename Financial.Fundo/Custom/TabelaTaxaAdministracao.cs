﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           22/2/2007 17:33:56
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;

namespace Financial.Fundo
{
	public partial class TabelaTaxaAdministracao : esTabelaTaxaAdministracao
	{
        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo Calculo Administracao é Percentual
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCalculoAdministracaoPercentual() {
            return this.TipoCalculo == (byte)TipoCalculoAdministracao.PercentualPL;
        }

	}
}
