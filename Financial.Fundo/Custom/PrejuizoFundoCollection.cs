﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using System.Collections;

namespace Financial.Fundo
{
	public partial class PrejuizoFundoCollection : esPrejuizoFundoCollection
	{
        /// <summary>
        /// Carrega o objeto PrejuizoCotistaCollection com os campos IdCarteira, IdCliente, Data, ValorPrejuizo.
        /// </summary>        
        /// <param name="data"></param>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="tipoTributacao"></param>   
        /// <param name="idCliente"></param>   
        public void BuscaPrejuizoCompensar(DateTime data, int idAgenteAdministrador, int tipoTributacao,
                                           int idCliente)
        {
            #region Carrega em <ListaCarteira> os fundos que sejam do mesmo administrador e mesma categoria tributária
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            carteiraCollection.BuscaCarteira(idAgenteAdministrador, tipoTributacao);
            ArrayList listaCarteira = new ArrayList();
            for (int i = 0; i < carteiraCollection.Count; i++)
            {
                Carteira carteira = (Carteira)carteiraCollection[i];
                listaCarteira.Add(carteira.IdCarteira.Value);
            }
            #endregion

            if (carteiraCollection.Count == 0)
            {
                return;
            }

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira, this.Query.IdCliente, this.Query.Data, this.Query.ValorPrejuizo)
                 .Where(this.Query.IdCarteira.In(listaCarteira.ToArray()),
                        this.Query.IdCliente == idCliente,
                        this.Query.Or(
                            this.Query.DataLimiteCompensacao.IsNull(),
                            this.Query.DataLimiteCompensacao.GreaterThanOrEqual(data))
                            );

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PrejuizoCotistaCollection com todos os campos de PrejuizoFundo.
        /// </summary>        
        /// <param name="data"></param>
        /// <param name="idCarteira"></param>
        /// <param name="idCliente"></param>   
        public void BuscaPrejuizoCompensar(DateTime data, int idCarteira, int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCarteira.Equal(idCarteira),
                        this.Query.IdCliente.Equal(idCliente));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PrejuizoFundoCollection pelo idCliente
        /// </summary>
        /// <param name="idCarteira"></param>
        public void BuscaPrejuizoFundoCompleto(int idCliente)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente);
            this.Query.Load();
        }
	}
}
