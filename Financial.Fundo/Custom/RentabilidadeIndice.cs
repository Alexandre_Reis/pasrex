﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using EntitySpaces.Interfaces;
using Financial.Fundo.Exceptions;
using Financial.Common;
using Financial.Common.Exceptions;

namespace Financial.Fundo {
        
    /// <summary>
    /// Dado um idIndice Calcula as Rentabilidades Mensais Desde o ano 2001, mês a mês
    /// Calcula as Rentabildades Anuais também.
    /// </summary>
    public class RentabilidadeIndice {
        private static readonly ILog log = LogManager.GetLogger(typeof(RentabilidadeFundo));

        private Indice indice;
        private CalculoMedida calculoMedida;
        private int idIndice;
                
        // Lista com 12 posições com as rentabilidades mensais
        private List<decimal?> rentabilidade2001 = new List<decimal?>(12);
        private List<decimal?> rentabilidade2002 = new List<decimal?>(12);
        private List<decimal?> rentabilidade2003 = new List<decimal?>(12);
        private List<decimal?> rentabilidade2004 = new List<decimal?>(12);
        private List<decimal?> rentabilidade2005 = new List<decimal?>(12);
        private List<decimal?> rentabilidade2006 = new List<decimal?>(12);
        private List<decimal?> rentabilidade2007 = new List<decimal?>(12);
        private List<decimal?> rentabilidade2008 = new List<decimal?>(12);
        private List<decimal?> rentabilidade2009 = new List<decimal?>(12);
        //
        /* Contem as rentabilidades anuais de 2001-2009*/
        private List<decimal?> rentabilidadesAnuais = new List<decimal?>(9);

        /// <summary>
        /// Use o Construtor com Parametros
        /// </summary>
        public RentabilidadeIndice() {
            throw new NotImplementedException("Use o Construtor com Parametros");
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="idIndice"></param>
        public RentabilidadeIndice(int idIndice) {
            this.idIndice = idIndice;
            //
            this.calculoMedida = new CalculoMedida((short)idIndice);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="idIndice"></param>
        /// <param name="dataInicioCarteira"></param>
        public RentabilidadeIndice(int idIndice, DateTime dataInicioCarteira) {
            this.idIndice = idIndice;
            //
            this.calculoMedida = new CalculoMedida((short)idIndice);
            this.calculoMedida.SetDataInicio(dataInicioCarteira);
        }

        // Calcula os 12 meses de Rentabilidade de 2001
        public class Rentabilidade2001 : RentabilidadeIndice {
            private decimal? rentabilidadeJaneiro, rentabilidadeFevereiro, rentabilidadeMarco,
                             rentabilidadeAbril, rentabilidadeMaio, rentabilidadeJunho,
                             rentabilidadeJulho, rentabilidadeAgosto, rentabilidadeSetembro,
                             rentabilidadeOutubro, rentabilidadeNovembro, rentabilidadeDezembro,
                             rentabilidadeAnual = null;
            
            private const int ano = 2001;

            public Rentabilidade2001() : base() {                
            }

            public Rentabilidade2001(int idIndice): base(idIndice) {}

            public Rentabilidade2001(int idIndice, DateTime dataInicioCarteira) : base(idIndice, dataInicioCarteira) { }

            public decimal? RentabilidadeJaneiro {
              get {                                     
                try {
                    this.rentabilidadeJaneiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 01, 01));
                    this.rentabilidadeJaneiro = rentabilidadeJaneiro / 100;
                }
                catch (CotacaoIndiceNaoCadastradoException) { }

                return this.rentabilidadeJaneiro;               
              }              
            }

            public decimal? RentabilidadeFevereiro {
                get {
                    try {
                        this.rentabilidadeFevereiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 02, 01));
                        this.rentabilidadeFevereiro = this.rentabilidadeFevereiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeFevereiro;
                }
            }

            public decimal? RentabilidadeMarco {
                get {
                    try {
                        this.rentabilidadeMarco = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 03, 01));
                        this.rentabilidadeMarco = this.rentabilidadeMarco / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMarco;
                }
            }

            public decimal? RentabilidadeAbril {
                get {
                    try {
                        this.rentabilidadeAbril = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 04, 01));
                        this.rentabilidadeAbril = this.rentabilidadeAbril / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAbril;
                }
            }

            public decimal? RentabilidadeMaio {
                get {
                    try {
                        this.rentabilidadeMaio = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 05, 01));
                        this.rentabilidadeMaio = this.rentabilidadeMaio / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMaio;
                }
            }

            public decimal? RentabilidadeJunho {
                get {
                    try {
                        this.rentabilidadeJunho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 06, 01));
                        this.rentabilidadeJunho = this.rentabilidadeJunho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJunho;
                }
            }

            public decimal? RentabilidadeJulho {
                get {
                    try {
                        this.rentabilidadeJulho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 07, 01));
                        this.rentabilidadeJulho = this.rentabilidadeJulho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJulho;
                }
            }

            public decimal? RentabilidadeAgosto {
                get {
                    try {
                        this.rentabilidadeAgosto = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 08, 01));
                        this.rentabilidadeAgosto = this.rentabilidadeAgosto / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAgosto;
                }
            }

            public decimal? RentabilidadeSetembro {
                get {
                    try {
                        this.rentabilidadeSetembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 09, 01));
                        this.rentabilidadeSetembro = this.rentabilidadeSetembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeSetembro;
                }
            }

            public decimal? RentabilidadeOutubro {
                get {
                    try {
                        this.rentabilidadeOutubro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 10, 01));
                        this.rentabilidadeOutubro = this.rentabilidadeOutubro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeOutubro;
                }
            }

            public decimal? RentabilidadeNovembro {
                get {
                    try {
                        this.rentabilidadeNovembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 11, 01));
                        this.rentabilidadeNovembro = this.rentabilidadeNovembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeNovembro;
                }
            }

            public decimal? RentabilidadeDezembro {
                get {
                    try {
                        this.rentabilidadeDezembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 12, 01));
                        this.rentabilidadeDezembro = this.rentabilidadeDezembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeDezembro;
                }
            }

            public decimal? RentabilidadeAnual {
                get {
                    try {
                        this.rentabilidadeAnual = this.calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(ano, 12, 31));
                        this.rentabilidadeAnual = this.rentabilidadeAnual / 100;
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    catch (Exception) { }

                    return this.rentabilidadeAnual;
                }
            }                                
        }

        // Calcula os 12 meses de rentabilidade de 2002
        public class Rentabilidade2002 : RentabilidadeIndice {
            private decimal? rentabilidadeJaneiro, rentabilidadeFevereiro, rentabilidadeMarco,
                             rentabilidadeAbril, rentabilidadeMaio, rentabilidadeJunho,
                             rentabilidadeJulho, rentabilidadeAgosto, rentabilidadeSetembro,
                             rentabilidadeOutubro, rentabilidadeNovembro, rentabilidadeDezembro,
                             rentabilidadeAnual = null;

            private const int ano = 2002;

            public Rentabilidade2002() : base() {                
            }

            public Rentabilidade2002(int idIndice) : base(idIndice) { }

            public Rentabilidade2002(int idIndice, DateTime dataInicioCarteira) : base(idIndice, dataInicioCarteira) { }

            public decimal? RentabilidadeJaneiro {
                get {
                    try {
                        this.rentabilidadeJaneiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 01, 01));
                        this.rentabilidadeJaneiro = rentabilidadeJaneiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJaneiro;
                }
            }

            public decimal? RentabilidadeFevereiro {
                get {
                    try {
                        this.rentabilidadeFevereiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 02, 01));
                        this.rentabilidadeFevereiro = this.rentabilidadeFevereiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeFevereiro;
                }
            }

            public decimal? RentabilidadeMarco {
                get {
                    try {
                        this.rentabilidadeMarco = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 03, 01));
                        this.rentabilidadeMarco = this.rentabilidadeMarco / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMarco;
                }
            }

            public decimal? RentabilidadeAbril {
                get {
                    try {
                        this.rentabilidadeAbril = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 04, 01));
                        this.rentabilidadeAbril = this.rentabilidadeAbril / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAbril;
                }
            }

            public decimal? RentabilidadeMaio {
                get {
                    try {
                        this.rentabilidadeMaio = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 05, 01));
                        this.rentabilidadeMaio = this.rentabilidadeMaio / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMaio;
                }
            }

            public decimal? RentabilidadeJunho {
                get {
                    try {
                        this.rentabilidadeJunho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 06, 01));
                        this.rentabilidadeJunho = this.rentabilidadeJunho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJunho;
                }
            }

            public decimal? RentabilidadeJulho {
                get {
                    try {
                        this.rentabilidadeJulho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 07, 01));
                        this.rentabilidadeJulho = this.rentabilidadeJulho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJulho;
                }
            }

            public decimal? RentabilidadeAgosto {
                get {
                    try {
                        this.rentabilidadeAgosto = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 08, 01));
                        this.rentabilidadeAgosto = this.rentabilidadeAgosto / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAgosto;
                }
            }

            public decimal? RentabilidadeSetembro {
                get {
                    try {
                        this.rentabilidadeSetembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 09, 01));
                        this.rentabilidadeSetembro = this.rentabilidadeSetembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeSetembro;
                }
            }

            public decimal? RentabilidadeOutubro {
                get {
                    try {
                        this.rentabilidadeOutubro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 10, 01));
                        this.rentabilidadeOutubro = this.rentabilidadeOutubro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeOutubro;
                }
            }

            public decimal? RentabilidadeNovembro {
                get {
                    try {
                        this.rentabilidadeNovembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 11, 01));
                        this.rentabilidadeNovembro = this.rentabilidadeNovembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeNovembro;
                }
            }

            public decimal? RentabilidadeDezembro {
                get {
                    try {
                        this.rentabilidadeDezembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 12, 01));
                        this.rentabilidadeDezembro = this.rentabilidadeDezembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeDezembro;
                }
            }

            public decimal? RentabilidadeAnual {
                get {
                    try {
                        this.rentabilidadeAnual = this.calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(ano, 12, 31));
                        this.rentabilidadeAnual = this.rentabilidadeAnual / 100;
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    catch (Exception) { }

                    return this.rentabilidadeAnual;
                }
            }                                
        }

        // Calcula os 12 meses de rentabilidade de 2003
        public class Rentabilidade2003 : RentabilidadeIndice {
            private decimal? rentabilidadeJaneiro, rentabilidadeFevereiro, rentabilidadeMarco,
                             rentabilidadeAbril, rentabilidadeMaio, rentabilidadeJunho,
                             rentabilidadeJulho, rentabilidadeAgosto, rentabilidadeSetembro,
                             rentabilidadeOutubro, rentabilidadeNovembro, rentabilidadeDezembro,
                             rentabilidadeAnual = null;

            private const int ano = 2003;

            public Rentabilidade2003() : base() {                
            }

            public Rentabilidade2003(int idIndice) : base(idIndice) { }

            public Rentabilidade2003(int idIndice, DateTime dataInicioCarteira) : base(idIndice, dataInicioCarteira) { }

            public decimal? RentabilidadeJaneiro {
                get {
                    try {
                        this.rentabilidadeJaneiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 01, 01));
                        this.rentabilidadeJaneiro = rentabilidadeJaneiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJaneiro;
                }
            }

            public decimal? RentabilidadeFevereiro {
                get {
                    try {
                        this.rentabilidadeFevereiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 02, 01));
                        this.rentabilidadeFevereiro = this.rentabilidadeFevereiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeFevereiro;
                }
            }

            public decimal? RentabilidadeMarco {
                get {
                    try {
                        this.rentabilidadeMarco = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 03, 01));
                        this.rentabilidadeMarco = this.rentabilidadeMarco / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMarco;
                }
            }

            public decimal? RentabilidadeAbril {
                get {
                    try {
                        this.rentabilidadeAbril = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 04, 01));
                        this.rentabilidadeAbril = this.rentabilidadeAbril / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAbril;
                }
            }

            public decimal? RentabilidadeMaio {
                get {
                    try {
                        this.rentabilidadeMaio = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 05, 01));
                        this.rentabilidadeMaio = this.rentabilidadeMaio / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMaio;
                }
            }

            public decimal? RentabilidadeJunho {
                get {
                    try {
                        this.rentabilidadeJunho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 06, 01));
                        this.rentabilidadeJunho = this.rentabilidadeJunho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJunho;
                }
            }

            public decimal? RentabilidadeJulho {
                get {
                    try {
                        this.rentabilidadeJulho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 07, 01));
                        this.rentabilidadeJulho = this.rentabilidadeJulho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJulho;
                }
            }

            public decimal? RentabilidadeAgosto {
                get {
                    try {
                        this.rentabilidadeAgosto = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 08, 01));
                        this.rentabilidadeAgosto = this.rentabilidadeAgosto / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAgosto;
                }
            }

            public decimal? RentabilidadeSetembro {
                get {
                    try {
                        this.rentabilidadeSetembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 09, 01));
                        this.rentabilidadeSetembro = this.rentabilidadeSetembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeSetembro;
                }
            }

            public decimal? RentabilidadeOutubro {
                get {
                    try {
                        this.rentabilidadeOutubro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 10, 01));
                        this.rentabilidadeOutubro = this.rentabilidadeOutubro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeOutubro;
                }
            }

            public decimal? RentabilidadeNovembro {
                get {
                    try {
                        this.rentabilidadeNovembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 11, 01));
                        this.rentabilidadeNovembro = this.rentabilidadeNovembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeNovembro;
                }
            }

            public decimal? RentabilidadeDezembro {
                get {
                    try {
                        this.rentabilidadeDezembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 12, 01));
                        this.rentabilidadeDezembro = this.rentabilidadeDezembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeDezembro;
                }
            }

            public decimal? RentabilidadeAnual {
                get {
                    try {
                        this.rentabilidadeAnual = this.calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(ano, 12, 31));
                        this.rentabilidadeAnual = this.rentabilidadeAnual / 100;
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    catch (Exception) { }

                    return this.rentabilidadeAnual;
                }
            }                                
        }

        // Calcula os 12 meses de rentabilidade de 2004
        public class Rentabilidade2004 : RentabilidadeIndice {
            private decimal? rentabilidadeJaneiro, rentabilidadeFevereiro, rentabilidadeMarco,
                             rentabilidadeAbril, rentabilidadeMaio, rentabilidadeJunho,
                             rentabilidadeJulho, rentabilidadeAgosto, rentabilidadeSetembro,
                             rentabilidadeOutubro, rentabilidadeNovembro, rentabilidadeDezembro,
                             rentabilidadeAnual = null;

            private const int ano = 2004;

            public Rentabilidade2004() : base() {                
            }

            public Rentabilidade2004(int idIndice) : base(idIndice) { }

            public Rentabilidade2004(int idIndice, DateTime dataInicioCarteira) : base(idIndice, dataInicioCarteira) { }

            public decimal? RentabilidadeJaneiro {
                get {
                    try {
                        this.rentabilidadeJaneiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 01, 01));
                        this.rentabilidadeJaneiro = rentabilidadeJaneiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJaneiro;
                }
            }

            public decimal? RentabilidadeFevereiro {
                get {
                    try {
                        this.rentabilidadeFevereiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 02, 01));
                        this.rentabilidadeFevereiro = this.rentabilidadeFevereiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeFevereiro;
                }
            }

            public decimal? RentabilidadeMarco {
                get {
                    try {
                        this.rentabilidadeMarco = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 03, 01));
                        this.rentabilidadeMarco = this.rentabilidadeMarco / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMarco;
                }
            }

            public decimal? RentabilidadeAbril {
                get {
                    try {
                        this.rentabilidadeAbril = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 04, 01));
                        this.rentabilidadeAbril = this.rentabilidadeAbril / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAbril;
                }
            }

            public decimal? RentabilidadeMaio {
                get {
                    try {
                        this.rentabilidadeMaio = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 05, 01));
                        this.rentabilidadeMaio = this.rentabilidadeMaio / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMaio;
                }
            }

            public decimal? RentabilidadeJunho {
                get {
                    try {
                        this.rentabilidadeJunho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 06, 01));
                        this.rentabilidadeJunho = this.rentabilidadeJunho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJunho;
                }
            }

            public decimal? RentabilidadeJulho {
                get {
                    try {
                        this.rentabilidadeJulho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 07, 01));
                        this.rentabilidadeJulho = this.rentabilidadeJulho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJulho;
                }
            }

            public decimal? RentabilidadeAgosto {
                get {
                    try {
                        this.rentabilidadeAgosto = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 08, 01));
                        this.rentabilidadeAgosto = this.rentabilidadeAgosto / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAgosto;
                }
            }

            public decimal? RentabilidadeSetembro {
                get {
                    try {
                        this.rentabilidadeSetembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 09, 01));
                        this.rentabilidadeSetembro = this.rentabilidadeSetembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeSetembro;
                }
            }

            public decimal? RentabilidadeOutubro {
                get {
                    try {
                        this.rentabilidadeOutubro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 10, 01));
                        this.rentabilidadeOutubro = this.rentabilidadeOutubro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeOutubro;
                }
            }

            public decimal? RentabilidadeNovembro {
                get {
                    try {
                        this.rentabilidadeNovembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 11, 01));
                        this.rentabilidadeNovembro = this.rentabilidadeNovembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeNovembro;
                }
            }

            public decimal? RentabilidadeDezembro {
                get {
                    try {
                        this.rentabilidadeDezembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 12, 01));
                        this.rentabilidadeDezembro = this.rentabilidadeDezembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeDezembro;
                }
            }

            public decimal? RentabilidadeAnual {
                get {
                    try {
                        this.rentabilidadeAnual = this.calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(ano, 12, 31));
                        this.rentabilidadeAnual = this.rentabilidadeAnual / 100;
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    catch (Exception) { }

                    return this.rentabilidadeAnual;
                }
            }                                
        }

        // Calcula os 12 meses de rentabilidade de 2005
        public class Rentabilidade2005 : RentabilidadeIndice {
            private decimal? rentabilidadeJaneiro, rentabilidadeFevereiro, rentabilidadeMarco,
                             rentabilidadeAbril, rentabilidadeMaio, rentabilidadeJunho,
                             rentabilidadeJulho, rentabilidadeAgosto, rentabilidadeSetembro,
                             rentabilidadeOutubro, rentabilidadeNovembro, rentabilidadeDezembro,
                             rentabilidadeAnual = null;

            private const int ano = 2005;

            public Rentabilidade2005() : base() {                
            }

            public Rentabilidade2005(int idIndice) : base(idIndice) { }

            public Rentabilidade2005(int idIndice, DateTime dataInicioCarteira) : base(idIndice, dataInicioCarteira) { }

            public decimal? RentabilidadeJaneiro {
                get {
                    try {
                        this.rentabilidadeJaneiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 01, 01));
                        this.rentabilidadeJaneiro = rentabilidadeJaneiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJaneiro;
                }
            }

            public decimal? RentabilidadeFevereiro {
                get {
                    try {
                        this.rentabilidadeFevereiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 02, 01));
                        this.rentabilidadeFevereiro = this.rentabilidadeFevereiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeFevereiro;
                }
            }

            public decimal? RentabilidadeMarco {
                get {
                    try {
                        this.rentabilidadeMarco = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 03, 01));
                        this.rentabilidadeMarco = this.rentabilidadeMarco / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMarco;
                }
            }

            public decimal? RentabilidadeAbril {
                get {
                    try {
                        this.rentabilidadeAbril = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 04, 01));
                        this.rentabilidadeAbril = this.rentabilidadeAbril / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAbril;
                }
            }

            public decimal? RentabilidadeMaio {
                get {
                    try {
                        this.rentabilidadeMaio = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 05, 01));
                        this.rentabilidadeMaio = this.rentabilidadeMaio / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMaio;
                }
            }

            public decimal? RentabilidadeJunho {
                get {
                    try {
                        this.rentabilidadeJunho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 06, 01));
                        this.rentabilidadeJunho = this.rentabilidadeJunho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJunho;
                }
            }

            public decimal? RentabilidadeJulho {
                get {
                    try {
                        this.rentabilidadeJulho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 07, 01));
                        this.rentabilidadeJulho = this.rentabilidadeJulho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJulho;
                }
            }

            public decimal? RentabilidadeAgosto {
                get {
                    try {
                        this.rentabilidadeAgosto = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 08, 01));
                        this.rentabilidadeAgosto = this.rentabilidadeAgosto / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAgosto;
                }
            }

            public decimal? RentabilidadeSetembro {
                get {
                    try {
                        this.rentabilidadeSetembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 09, 01));
                        this.rentabilidadeSetembro = this.rentabilidadeSetembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeSetembro;
                }
            }

            public decimal? RentabilidadeOutubro {
                get {
                    try {
                        this.rentabilidadeOutubro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 10, 01));
                        this.rentabilidadeOutubro = this.rentabilidadeOutubro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeOutubro;
                }
            }

            public decimal? RentabilidadeNovembro {
                get {
                    try {
                        this.rentabilidadeNovembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 11, 01));
                        this.rentabilidadeNovembro = this.rentabilidadeNovembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeNovembro;
                }
            }

            public decimal? RentabilidadeDezembro {
                get {
                    try {
                        this.rentabilidadeDezembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 12, 01));
                        this.rentabilidadeDezembro = this.rentabilidadeDezembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeDezembro;
                }
            }

            public decimal? RentabilidadeAnual {
                get {
                    try {
                        this.rentabilidadeAnual = this.calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(ano, 12, 31));
                        this.rentabilidadeAnual = this.rentabilidadeAnual / 100;
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    catch (Exception) { }

                    return this.rentabilidadeAnual;
                }
            }                                
        }

        // Calcula os 12 meses de rentabilidade de 2006
        public class Rentabilidade2006 : RentabilidadeIndice {
            private decimal? rentabilidadeJaneiro, rentabilidadeFevereiro, rentabilidadeMarco,
                             rentabilidadeAbril, rentabilidadeMaio, rentabilidadeJunho,
                             rentabilidadeJulho, rentabilidadeAgosto, rentabilidadeSetembro,
                             rentabilidadeOutubro, rentabilidadeNovembro, rentabilidadeDezembro,
                             rentabilidadeAnual = null;

            private const int ano = 2006;

            public Rentabilidade2006() : base() {                
            }

            public Rentabilidade2006(int idIndice) : base(idIndice) { }

            public Rentabilidade2006(int idIndice, DateTime dataInicioCarteira) : base(idIndice, dataInicioCarteira) { }

            public decimal? RentabilidadeJaneiro {
                get {
                    try {
                        this.rentabilidadeJaneiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 01, 01));
                        this.rentabilidadeJaneiro = rentabilidadeJaneiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJaneiro;
                }
            }

            public decimal? RentabilidadeFevereiro {
                get {
                    try {
                        this.rentabilidadeFevereiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 02, 01));
                        this.rentabilidadeFevereiro = this.rentabilidadeFevereiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeFevereiro;
                }
            }

            public decimal? RentabilidadeMarco {
                get {
                    try {
                        this.rentabilidadeMarco = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 03, 01));
                        this.rentabilidadeMarco = this.rentabilidadeMarco / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMarco;
                }
            }

            public decimal? RentabilidadeAbril {
                get {
                    try {
                        this.rentabilidadeAbril = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 04, 01));
                        this.rentabilidadeAbril = this.rentabilidadeAbril / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAbril;
                }
            }

            public decimal? RentabilidadeMaio {
                get {
                    try {
                        this.rentabilidadeMaio = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 05, 01));
                        this.rentabilidadeMaio = this.rentabilidadeMaio / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMaio;
                }
            }

            public decimal? RentabilidadeJunho {
                get {
                    try {
                        this.rentabilidadeJunho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 06, 01));
                        this.rentabilidadeJunho = this.rentabilidadeJunho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJunho;
                }
            }

            public decimal? RentabilidadeJulho {
                get {
                    try {
                        this.rentabilidadeJulho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 07, 01));
                        this.rentabilidadeJulho = this.rentabilidadeJulho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJulho;
                }
            }

            public decimal? RentabilidadeAgosto {
                get {
                    try {
                        this.rentabilidadeAgosto = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 08, 01));
                        this.rentabilidadeAgosto = this.rentabilidadeAgosto / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAgosto;
                }
            }

            public decimal? RentabilidadeSetembro {
                get {
                    try {
                        this.rentabilidadeSetembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 09, 01));
                        this.rentabilidadeSetembro = this.rentabilidadeSetembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeSetembro;
                }
            }

            public decimal? RentabilidadeOutubro {
                get {
                    try {
                        this.rentabilidadeOutubro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 10, 01));
                        this.rentabilidadeOutubro = this.rentabilidadeOutubro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeOutubro;
                }
            }

            public decimal? RentabilidadeNovembro {
                get {
                    try {
                        this.rentabilidadeNovembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 11, 01));
                        this.rentabilidadeNovembro = this.rentabilidadeNovembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeNovembro;
                }
            }

            public decimal? RentabilidadeDezembro {
                get {
                    try {
                        this.rentabilidadeDezembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 12, 01));
                        this.rentabilidadeDezembro = this.rentabilidadeDezembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeDezembro;
                }
            }

            public decimal? RentabilidadeAnual {
                get {
                    try {
                        this.rentabilidadeAnual = this.calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(ano, 12, 31));
                        this.rentabilidadeAnual = this.rentabilidadeAnual / 100;
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    catch (Exception) { }

                    return this.rentabilidadeAnual;
                }
            }                                
        }

        // Calcula os 12 meses de rentabilidade de 2007
        public class Rentabilidade2007 : RentabilidadeIndice {
            private decimal? rentabilidadeJaneiro, rentabilidadeFevereiro, rentabilidadeMarco,
                             rentabilidadeAbril, rentabilidadeMaio, rentabilidadeJunho,
                             rentabilidadeJulho, rentabilidadeAgosto, rentabilidadeSetembro,
                             rentabilidadeOutubro, rentabilidadeNovembro, rentabilidadeDezembro,
                             rentabilidadeAnual = null;

            private const int ano = 2007;

            public Rentabilidade2007() : base() {                
            }

            public Rentabilidade2007(int idIndice) : base(idIndice) { }

            public Rentabilidade2007(int idIndice, DateTime dataInicioCarteira) : base(idIndice, dataInicioCarteira) { }

            public decimal? RentabilidadeJaneiro {
                get {
                    try {
                        this.rentabilidadeJaneiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 01, 01));
                        this.rentabilidadeJaneiro = rentabilidadeJaneiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJaneiro;
                }
            }

            public decimal? RentabilidadeFevereiro {
                get {
                    try {
                        this.rentabilidadeFevereiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 02, 01));
                        this.rentabilidadeFevereiro = this.rentabilidadeFevereiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeFevereiro;
                }
            }

            public decimal? RentabilidadeMarco {
                get {
                    try {
                        this.rentabilidadeMarco = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 03, 01));
                        this.rentabilidadeMarco = this.rentabilidadeMarco / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMarco;
                }
            }

            public decimal? RentabilidadeAbril {
                get {
                    try {
                        this.rentabilidadeAbril = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 04, 01));
                        this.rentabilidadeAbril = this.rentabilidadeAbril / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAbril;
                }
            }

            public decimal? RentabilidadeMaio {
                get {
                    try {
                        this.rentabilidadeMaio = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 05, 01));
                        this.rentabilidadeMaio = this.rentabilidadeMaio / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMaio;
                }
            }

            public decimal? RentabilidadeJunho {
                get {
                    try {
                        this.rentabilidadeJunho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 06, 01));
                        this.rentabilidadeJunho = this.rentabilidadeJunho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJunho;
                }
            }

            public decimal? RentabilidadeJulho {
                get {
                    try {
                        this.rentabilidadeJulho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 07, 01));
                        this.rentabilidadeJulho = this.rentabilidadeJulho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJulho;
                }
            }

            public decimal? RentabilidadeAgosto {
                get {
                    try {
                        this.rentabilidadeAgosto = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 08, 01));
                        this.rentabilidadeAgosto = this.rentabilidadeAgosto / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAgosto;
                }
            }

            public decimal? RentabilidadeSetembro {
                get {
                    try {
                        this.rentabilidadeSetembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 09, 01));
                        this.rentabilidadeSetembro = this.rentabilidadeSetembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeSetembro;
                }
            }

            public decimal? RentabilidadeOutubro {
                get {
                    try {
                        this.rentabilidadeOutubro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 10, 01));
                        this.rentabilidadeOutubro = this.rentabilidadeOutubro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeOutubro;
                }
            }

            public decimal? RentabilidadeNovembro {
                get {
                    try {
                        this.rentabilidadeNovembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 11, 01));
                        this.rentabilidadeNovembro = this.rentabilidadeNovembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeNovembro;
                }
            }

            public decimal? RentabilidadeDezembro {
                get {
                    try {
                        this.rentabilidadeDezembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 12, 01));
                        this.rentabilidadeDezembro = this.rentabilidadeDezembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeDezembro;
                }
            }

            public decimal? RentabilidadeAnual {
                get {
                    try {
                        this.rentabilidadeAnual = this.calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(ano, 12, 31));
                        this.rentabilidadeAnual = this.rentabilidadeAnual / 100;
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    catch (Exception) { }

                    return this.rentabilidadeAnual;
                }
            }
        }

        // Calcula os 12 meses de rentabilidade de 2008
        public class Rentabilidade2008 : RentabilidadeIndice {
            private decimal? rentabilidadeJaneiro, rentabilidadeFevereiro, rentabilidadeMarco,
                             rentabilidadeAbril, rentabilidadeMaio, rentabilidadeJunho,
                             rentabilidadeJulho, rentabilidadeAgosto, rentabilidadeSetembro,
                             rentabilidadeOutubro, rentabilidadeNovembro, rentabilidadeDezembro,
                             rentabilidadeAnual = null;

            private const int ano = 2008;

            public Rentabilidade2008() : base() {                
            }

            public Rentabilidade2008(int idIndice): base(idIndice) {}

            public Rentabilidade2008(int idIndice, DateTime dataInicioCarteira) : base(idIndice, dataInicioCarteira) { }

            public decimal? RentabilidadeJaneiro {
                get {
                    try {
                        this.rentabilidadeJaneiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 01, 01));
                        this.rentabilidadeJaneiro = rentabilidadeJaneiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJaneiro;
                }
            }

            public decimal? RentabilidadeFevereiro {
                get {
                    try {
                        this.rentabilidadeFevereiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 02, 01));
                        this.rentabilidadeFevereiro = this.rentabilidadeFevereiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeFevereiro;
                }
            }

            public decimal? RentabilidadeMarco {
                get {
                    try {
                        this.rentabilidadeMarco = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 03, 01));
                        this.rentabilidadeMarco = this.rentabilidadeMarco / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMarco;
                }
            }

            public decimal? RentabilidadeAbril {
                get {
                    try {
                        this.rentabilidadeAbril = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 04, 01));
                        this.rentabilidadeAbril = this.rentabilidadeAbril / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAbril;
                }
            }

            public decimal? RentabilidadeMaio {
                get {
                    try {
                        this.rentabilidadeMaio = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 05, 01));
                        this.rentabilidadeMaio = this.rentabilidadeMaio / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMaio;
                }
            }

            public decimal? RentabilidadeJunho {
                get {
                    try {
                        this.rentabilidadeJunho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 06, 01));
                        this.rentabilidadeJunho = this.rentabilidadeJunho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJunho;
                }
            }

            public decimal? RentabilidadeJulho {
                get {
                    try {
                        this.rentabilidadeJulho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 07, 01));
                        this.rentabilidadeJulho = this.rentabilidadeJulho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJulho;
                }
            }

            public decimal? RentabilidadeAgosto {
                get {
                    try {
                        this.rentabilidadeAgosto = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 08, 01));
                        this.rentabilidadeAgosto = this.rentabilidadeAgosto / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAgosto;
                }
            }

            public decimal? RentabilidadeSetembro {
                get {
                    try {
                        this.rentabilidadeSetembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 09, 01));
                        this.rentabilidadeSetembro = this.rentabilidadeSetembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeSetembro;
                }
            }

            public decimal? RentabilidadeOutubro {
                get {
                    try {
                        this.rentabilidadeOutubro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 10, 01));
                        this.rentabilidadeOutubro = this.rentabilidadeOutubro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeOutubro;
                }
            }

            public decimal? RentabilidadeNovembro {
                get {
                    try {
                        this.rentabilidadeNovembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 11, 01));
                        this.rentabilidadeNovembro = this.rentabilidadeNovembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeNovembro;
                }
            }

            public decimal? RentabilidadeDezembro {
                get {
                    try {
                        this.rentabilidadeDezembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 12, 01));
                        this.rentabilidadeDezembro = this.rentabilidadeDezembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeDezembro;
                }
            }

            public decimal? RentabilidadeAnual {
                get {
                    try {
                        this.rentabilidadeAnual = this.calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(ano, 12, 31));
                        this.rentabilidadeAnual = this.rentabilidadeAnual / 100;
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    catch (Exception) { }

                    return this.rentabilidadeAnual;
                }
            }                                
        }

        // Calcula os 12 meses de rentabilidade de 2009
        // TODO: verificar rentabiidade em 2009 para o mes de abril em diante
        public class Rentabilidade2009 : RentabilidadeIndice {
            private decimal? rentabilidadeJaneiro, rentabilidadeFevereiro, rentabilidadeMarco,
                             rentabilidadeAbril, rentabilidadeMaio, rentabilidadeJunho,
                             rentabilidadeJulho, rentabilidadeAgosto, rentabilidadeSetembro,
                             rentabilidadeOutubro, rentabilidadeNovembro, rentabilidadeDezembro,
                             rentabilidadeAnual = null;

            private const int ano = 2009;

            public Rentabilidade2009()
                : base() {
            }

            public Rentabilidade2009(int idIndice) : base(idIndice) { }

            public Rentabilidade2009(int idIndice, DateTime dataInicioCarteira) : base(idIndice, dataInicioCarteira) { }

            public decimal? RentabilidadeJaneiro {
                get {
                    try {
                        this.rentabilidadeJaneiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 01, 01));
                        this.rentabilidadeJaneiro = rentabilidadeJaneiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJaneiro;
                }
            }

            public decimal? RentabilidadeFevereiro {
                get {
                    try {
                        this.rentabilidadeFevereiro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 02, 01));
                        this.rentabilidadeFevereiro = this.rentabilidadeFevereiro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeFevereiro;
                }
            }

            public decimal? RentabilidadeMarco {
                get {
                    try {
                        this.rentabilidadeMarco = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 03, 01));
                        this.rentabilidadeMarco = this.rentabilidadeMarco / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMarco;
                }
            }

            public decimal? RentabilidadeAbril {
                get {
                    try {
                        this.rentabilidadeAbril = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 04, 01));
                        this.rentabilidadeAbril = this.rentabilidadeAbril / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAbril;
                }
            }

            public decimal? RentabilidadeMaio {
                get {
                    try {
                        this.rentabilidadeMaio = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 05, 01));
                        this.rentabilidadeMaio = this.rentabilidadeMaio / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeMaio;
                }
            }

            public decimal? RentabilidadeJunho {
                get {
                    try {
                        this.rentabilidadeJunho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 06, 01));
                        this.rentabilidadeJunho = this.rentabilidadeJunho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJunho;
                }
            }

            public decimal? RentabilidadeJulho {
                get {
                    try {
                        this.rentabilidadeJulho = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 07, 01));
                        this.rentabilidadeJulho = this.rentabilidadeJulho / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeJulho;
                }
            }

            public decimal? RentabilidadeAgosto {
                get {
                    try {
                        this.rentabilidadeAgosto = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 08, 01));
                        this.rentabilidadeAgosto = this.rentabilidadeAgosto / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeAgosto;
                }
            }

            public decimal? RentabilidadeSetembro {
                get {
                    try {
                        this.rentabilidadeSetembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 09, 01));
                        this.rentabilidadeSetembro = this.rentabilidadeSetembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeSetembro;
                }
            }

            public decimal? RentabilidadeOutubro {
                get {
                    try {
                        this.rentabilidadeOutubro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 10, 01));
                        this.rentabilidadeOutubro = this.rentabilidadeOutubro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeOutubro;
                }
            }

            public decimal? RentabilidadeNovembro {
                get {
                    try {
                        this.rentabilidadeNovembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 11, 01));
                        this.rentabilidadeNovembro = this.rentabilidadeNovembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeNovembro;
                }
            }

            public decimal? RentabilidadeDezembro {
                get {
                    try {
                        this.rentabilidadeDezembro = this.calculoMedida.CalculaRetornoMesFechadoIndice(new DateTime(ano, 12, 01));
                        this.rentabilidadeDezembro = this.rentabilidadeDezembro / 100;
                    }
                    catch (CotacaoIndiceNaoCadastradoException) { }

                    return this.rentabilidadeDezembro;
                }
            }

            public decimal? RentabilidadeAnual {
                get {
                    try {
                        this.rentabilidadeAnual = this.calculoMedida.CalculaRetornoAnoFechadoIndice(new DateTime(ano, 12, 31));
                        this.rentabilidadeAnual = this.rentabilidadeAnual / 100;
                    }
                    catch (HistoricoCotaNaoCadastradoException) { }
                    catch (Exception) { }

                    return this.rentabilidadeAnual;
                }
            }
        }

        public List<decimal?> ListaRentabilidade2001 {
            get {                                              
                Rentabilidade2001 rent = new Rentabilidade2001(this.idIndice);
                //
                // Repassa a DataInicio de this
                rent.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                //
                this.rentabilidade2001 = new List<decimal?>(12);
                this.rentabilidade2001.Add(rent.RentabilidadeJaneiro);
                this.rentabilidade2001.Add(rent.RentabilidadeFevereiro);
                this.rentabilidade2001.Add(rent.RentabilidadeMarco);
                this.rentabilidade2001.Add(rent.RentabilidadeAbril);
                this.rentabilidade2001.Add(rent.RentabilidadeMaio);
                this.rentabilidade2001.Add(rent.RentabilidadeJunho);
                this.rentabilidade2001.Add(rent.RentabilidadeJulho);
                this.rentabilidade2001.Add(rent.RentabilidadeAgosto);
                this.rentabilidade2001.Add(rent.RentabilidadeSetembro);
                this.rentabilidade2001.Add(rent.RentabilidadeOutubro);
                this.rentabilidade2001.Add(rent.RentabilidadeNovembro);
                this.rentabilidade2001.Add(rent.RentabilidadeDezembro);
                //                
                return this.rentabilidade2001;                 
            }            
        }

        public List<decimal?> ListaRentabilidade2002 {
            get {
                Rentabilidade2002 rent = new Rentabilidade2002(this.idIndice);
                //
                // Repassa a DataInicio de this
                rent.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                //
                this.rentabilidade2002 = new List<decimal?>(12);
                this.rentabilidade2002.Add(rent.RentabilidadeJaneiro);
                this.rentabilidade2002.Add(rent.RentabilidadeFevereiro);
                this.rentabilidade2002.Add(rent.RentabilidadeMarco);
                this.rentabilidade2002.Add(rent.RentabilidadeAbril);
                this.rentabilidade2002.Add(rent.RentabilidadeMaio);
                this.rentabilidade2002.Add(rent.RentabilidadeJunho);
                this.rentabilidade2002.Add(rent.RentabilidadeJulho);
                this.rentabilidade2002.Add(rent.RentabilidadeAgosto);
                this.rentabilidade2002.Add(rent.RentabilidadeSetembro);
                this.rentabilidade2002.Add(rent.RentabilidadeOutubro);
                this.rentabilidade2002.Add(rent.RentabilidadeNovembro);
                this.rentabilidade2002.Add(rent.RentabilidadeDezembro);
                //                
                return this.rentabilidade2002;
            }
        }

        public List<decimal?> ListaRentabilidade2003 {
            get {
                Rentabilidade2003 rent = new Rentabilidade2003(this.idIndice);
                //
                // Repassa a DataInicio de this
                rent.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                //
                this.rentabilidade2003 = new List<decimal?>(12);
                this.rentabilidade2003.Add(rent.RentabilidadeJaneiro);
                this.rentabilidade2003.Add(rent.RentabilidadeFevereiro);
                this.rentabilidade2003.Add(rent.RentabilidadeMarco);
                this.rentabilidade2003.Add(rent.RentabilidadeAbril);
                this.rentabilidade2003.Add(rent.RentabilidadeMaio);
                this.rentabilidade2003.Add(rent.RentabilidadeJunho);
                this.rentabilidade2003.Add(rent.RentabilidadeJulho);
                this.rentabilidade2003.Add(rent.RentabilidadeAgosto);
                this.rentabilidade2003.Add(rent.RentabilidadeSetembro);
                this.rentabilidade2003.Add(rent.RentabilidadeOutubro);
                this.rentabilidade2003.Add(rent.RentabilidadeNovembro);
                this.rentabilidade2003.Add(rent.RentabilidadeDezembro);
                //                
                return this.rentabilidade2003;
            }
        }

        public List<decimal?> ListaRentabilidade2004 {
            get {
                Rentabilidade2004 rent = new Rentabilidade2004(this.idIndice);
                //
                // Repassa a DataInicio de this
                rent.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                //
                this.rentabilidade2004 = new List<decimal?>(12);
                this.rentabilidade2004.Add(rent.RentabilidadeJaneiro);
                this.rentabilidade2004.Add(rent.RentabilidadeFevereiro);
                this.rentabilidade2004.Add(rent.RentabilidadeMarco);
                this.rentabilidade2004.Add(rent.RentabilidadeAbril);
                this.rentabilidade2004.Add(rent.RentabilidadeMaio);
                this.rentabilidade2004.Add(rent.RentabilidadeJunho);
                this.rentabilidade2004.Add(rent.RentabilidadeJulho);
                this.rentabilidade2004.Add(rent.RentabilidadeAgosto);
                this.rentabilidade2004.Add(rent.RentabilidadeSetembro);
                this.rentabilidade2004.Add(rent.RentabilidadeOutubro);
                this.rentabilidade2004.Add(rent.RentabilidadeNovembro);
                this.rentabilidade2004.Add(rent.RentabilidadeDezembro);
                //                
                return this.rentabilidade2004;
            }
        }

        public List<decimal?> ListaRentabilidade2005 {
            get {
                Rentabilidade2005 rent = new Rentabilidade2005(this.idIndice);
                //
                // Repassa a DataInicio de this
                rent.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);

                this.rentabilidade2005 = new List<decimal?>(12);
                this.rentabilidade2005.Add(rent.RentabilidadeJaneiro);
                this.rentabilidade2005.Add(rent.RentabilidadeFevereiro);
                this.rentabilidade2005.Add(rent.RentabilidadeMarco);
                this.rentabilidade2005.Add(rent.RentabilidadeAbril);
                this.rentabilidade2005.Add(rent.RentabilidadeMaio);
                this.rentabilidade2005.Add(rent.RentabilidadeJunho);
                this.rentabilidade2005.Add(rent.RentabilidadeJulho);
                this.rentabilidade2005.Add(rent.RentabilidadeAgosto);
                this.rentabilidade2005.Add(rent.RentabilidadeSetembro);
                this.rentabilidade2005.Add(rent.RentabilidadeOutubro);
                this.rentabilidade2005.Add(rent.RentabilidadeNovembro);
                this.rentabilidade2005.Add(rent.RentabilidadeDezembro);
                //                
                return this.rentabilidade2005;

            }
        }

        public List<decimal?> ListaRentabilidade2006 {
            get {
                Rentabilidade2006 rent = new Rentabilidade2006(this.idIndice);
                //
                // Repassa a DataInicio de this
                rent.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);

                this.rentabilidade2006 = new List<decimal?>(12);
                this.rentabilidade2006.Add(rent.RentabilidadeJaneiro);
                this.rentabilidade2006.Add(rent.RentabilidadeFevereiro);
                this.rentabilidade2006.Add(rent.RentabilidadeMarco);
                this.rentabilidade2006.Add(rent.RentabilidadeAbril);
                this.rentabilidade2006.Add(rent.RentabilidadeMaio);
                this.rentabilidade2006.Add(rent.RentabilidadeJunho);
                this.rentabilidade2006.Add(rent.RentabilidadeJulho);
                this.rentabilidade2006.Add(rent.RentabilidadeAgosto);
                this.rentabilidade2006.Add(rent.RentabilidadeSetembro);
                this.rentabilidade2006.Add(rent.RentabilidadeOutubro);
                this.rentabilidade2006.Add(rent.RentabilidadeNovembro);
                this.rentabilidade2006.Add(rent.RentabilidadeDezembro);
                //                
                return this.rentabilidade2006;
            }
        }

        public List<decimal?> ListaRentabilidade2007 {
            get {
                Rentabilidade2007 rent = new Rentabilidade2007(this.idIndice);
                //
                //
                // Repassa a DataInicio de this
                rent.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                //
                this.rentabilidade2007 = new List<decimal?>(12);
                this.rentabilidade2007.Add(rent.RentabilidadeJaneiro);
                this.rentabilidade2007.Add(rent.RentabilidadeFevereiro);
                this.rentabilidade2007.Add(rent.RentabilidadeMarco);
                this.rentabilidade2007.Add(rent.RentabilidadeAbril);
                this.rentabilidade2007.Add(rent.RentabilidadeMaio);
                this.rentabilidade2007.Add(rent.RentabilidadeJunho);
                this.rentabilidade2007.Add(rent.RentabilidadeJulho);
                this.rentabilidade2007.Add(rent.RentabilidadeAgosto);
                this.rentabilidade2007.Add(rent.RentabilidadeSetembro);
                this.rentabilidade2007.Add(rent.RentabilidadeOutubro);
                this.rentabilidade2007.Add(rent.RentabilidadeNovembro);
                this.rentabilidade2007.Add(rent.RentabilidadeDezembro);
                //                
                return this.rentabilidade2007;
            }
        }

        public List<decimal?> ListaRentabilidade2008 {
            get {
                Rentabilidade2008 rent = new Rentabilidade2008(this.idIndice);
                //
                // Repassa a DataInicio de this
                rent.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                //
                this.rentabilidade2008 = new List<decimal?>(12);
                this.rentabilidade2008.Add(rent.RentabilidadeJaneiro);
                this.rentabilidade2008.Add(rent.RentabilidadeFevereiro);
                this.rentabilidade2008.Add(rent.RentabilidadeMarco);
                this.rentabilidade2008.Add(rent.RentabilidadeAbril);
                this.rentabilidade2008.Add(rent.RentabilidadeMaio);
                this.rentabilidade2008.Add(rent.RentabilidadeJunho);
                this.rentabilidade2008.Add(rent.RentabilidadeJulho);
                this.rentabilidade2008.Add(rent.RentabilidadeAgosto);
                this.rentabilidade2008.Add(rent.RentabilidadeSetembro);
                this.rentabilidade2008.Add(rent.RentabilidadeOutubro);
                this.rentabilidade2008.Add(rent.RentabilidadeNovembro);
                this.rentabilidade2008.Add(rent.RentabilidadeDezembro);
                //                
                return this.rentabilidade2008;
            }
        }

        public List<decimal?> ListaRentabilidade2009 {
            get {
                Rentabilidade2009 rent = new Rentabilidade2009(this.idIndice);
                //
                // Repassa a DataInicio de this
                rent.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                //
                this.rentabilidade2009 = new List<decimal?>(12);
                this.rentabilidade2009.Add(rent.RentabilidadeJaneiro);
                this.rentabilidade2009.Add(rent.RentabilidadeFevereiro);
                this.rentabilidade2009.Add(rent.RentabilidadeMarco);
                this.rentabilidade2009.Add(rent.RentabilidadeAbril);
                this.rentabilidade2009.Add(rent.RentabilidadeMaio);
                this.rentabilidade2009.Add(rent.RentabilidadeJunho);
                this.rentabilidade2009.Add(rent.RentabilidadeJulho);
                this.rentabilidade2009.Add(rent.RentabilidadeAgosto);
                this.rentabilidade2009.Add(rent.RentabilidadeSetembro);
                this.rentabilidade2009.Add(rent.RentabilidadeOutubro);
                this.rentabilidade2009.Add(rent.RentabilidadeNovembro);
                this.rentabilidade2009.Add(rent.RentabilidadeDezembro);
                //                
                return this.rentabilidade2009;
            }
        }

        /// <summary>
        /// Rentabilidades Anuais do Indice X
        /// Posicao [0] = ano 2001
        /// Posicao [1] = ano 2002
        /// Posicao [2] = ano 2003
        /// Posicao [3] = ano 2004
        /// Posicao [4] = ano 2005
        /// Posicao [5] = ano 2006
        /// Posicao [6] = ano 2007
        /// Posicao [7] = ano 2008
        /// Posicao [8] = ano 2009
        /// </summary>
        public List<decimal?> ListaRentabilidadesAnuais {
            get {
                Rentabilidade2001 rent2001 = new Rentabilidade2001(this.idIndice);
                Rentabilidade2002 rent2002 = new Rentabilidade2002(this.idIndice);
                Rentabilidade2003 rent2003 = new Rentabilidade2003(this.idIndice);
                Rentabilidade2004 rent2004 = new Rentabilidade2004(this.idIndice);
                Rentabilidade2005 rent2005 = new Rentabilidade2005(this.idIndice);
                Rentabilidade2006 rent2006 = new Rentabilidade2006(this.idIndice);
                Rentabilidade2007 rent2007 = new Rentabilidade2007(this.idIndice);
                Rentabilidade2008 rent2008 = new Rentabilidade2008(this.idIndice);
                Rentabilidade2009 rent2009 = new Rentabilidade2009(this.idIndice);
                //
                // Repassa a DataInicio de this
                rent2001.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                rent2002.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                rent2003.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                rent2004.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                rent2005.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                rent2006.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                rent2007.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                rent2008.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                rent2009.calculoMedida.SetDataInicio(this.calculoMedida.DataInicio);
                //
                this.rentabilidadesAnuais = new List<decimal?>(9);
                this.rentabilidadesAnuais.Add(rent2001.RentabilidadeAnual);
                this.rentabilidadesAnuais.Add(rent2002.RentabilidadeAnual);
                this.rentabilidadesAnuais.Add(rent2003.RentabilidadeAnual);
                this.rentabilidadesAnuais.Add(rent2004.RentabilidadeAnual);
                this.rentabilidadesAnuais.Add(rent2005.RentabilidadeAnual);
                this.rentabilidadesAnuais.Add(rent2006.RentabilidadeAnual);
                this.rentabilidadesAnuais.Add(rent2007.RentabilidadeAnual);
                this.rentabilidadesAnuais.Add(rent2008.RentabilidadeAnual);
                this.rentabilidadesAnuais.Add(rent2009.RentabilidadeAnual);
                //                
                return this.rentabilidadesAnuais;
            }
        }
    }
}
