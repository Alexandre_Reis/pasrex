﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Util.Enums;
using Financial.InvestidorCotista;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using log4net;
using Financial.Investidor;
using Financial.Fundo.Exceptions;
using Financial.Common;

namespace Financial.Fundo
{
    public partial class CalculoPerformance : esCalculoPerformance
    {
        public class MemoriaPerformance
        {
            public int idCarteira;
            public DateTime dataReferencia;
            public bool isFundo;
            public byte tipoCalculo;
            public byte baseApuracao;
            public short? idIndice;
            public decimal? percentualIndice;
            public decimal taxaJuros;
            public byte tipoApropriacaoJuros;
            public byte contagemDiasJuros;
            public int baseAnoJuros;
            public decimal taxaPerformance;
            public DateTime? dataFim;
            public string impactaPL;
            public List<ValoresPerformance> listaValores = new List<ValoresPerformance>();

            public class ValoresPerformance
            {
                public int idCotista;
                public string nome;
                public DateTime dataBase;
                public decimal fatorIndice;
                public decimal fatorJuros;
                public decimal cotaBase;
                public decimal cotaAtual;
                public decimal cotaCorrigida;
                public decimal quantidadeCotas;
                public decimal rendimento;
                public decimal valorPerformance;

                #region Properties
                public int IdCotista
                {
                    get { return idCotista; }
                    set { idCotista = value; }
                }

                public string Nome
                {
                    get { return nome; }
                    set { nome = value; }
                }

                public DateTime DataBase
                {
                    get { return dataBase; }
                    set { dataBase = value; }
                }

                public decimal FatorIndice
                {
                    get { return fatorIndice; }
                    set { fatorIndice = value; }
                }

                public decimal FatorJuros
                {
                    get { return fatorJuros; }
                    set { fatorJuros = value; }
                }

                public decimal CotaBase
                {
                    get { return cotaBase; }
                    set { cotaBase = value; }
                }

                public decimal CotaAtual
                {
                    get { return cotaAtual; }
                    set { cotaAtual = value; }
                }

                public decimal CotaCorrigida
                {
                    get { return cotaCorrigida; }
                    set { cotaCorrigida = value; }
                }

                public decimal QuantidadeCotas
                {
                    get { return quantidadeCotas; }
                    set { quantidadeCotas = value; }
                }

                public decimal Rendimento
                {
                    get { return rendimento; }
                    set { rendimento = value; }
                }

                public decimal ValorPerformance
                {
                    get { return valorPerformance; }
                    set { valorPerformance = value; }
                }
                #endregion
            }

            public MemoriaPerformance(int idTabela, DateTime data)
            {
                TabelaTaxaPerformance tabelaTaxaPerformance = new TabelaTaxaPerformance();
                tabelaTaxaPerformance.LoadByPrimaryKey(idTabela);

                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(tabelaTaxaPerformance.IdCarteira.Value);

                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(tabelaTaxaPerformance.IdCarteira.Value);

                this.idCarteira = tabelaTaxaPerformance.IdCarteira.Value;
                this.dataReferencia = tabelaTaxaPerformance.DataReferencia.Value;
                this.tipoCalculo = tabelaTaxaPerformance.TipoCalculo.Value;
                this.baseApuracao = tabelaTaxaPerformance.BaseApuracao.Value;
                this.idIndice = tabelaTaxaPerformance.IdIndice;
                this.percentualIndice = tabelaTaxaPerformance.PercentualIndice;
                this.taxaJuros = tabelaTaxaPerformance.TaxaJuros.Value;
                this.tipoApropriacaoJuros = tabelaTaxaPerformance.TipoApropriacaoJuros.Value;
                this.contagemDiasJuros = tabelaTaxaPerformance.ContagemDiasJuros.Value;
                this.baseAnoJuros = tabelaTaxaPerformance.BaseAnoJuros.Value;
                this.taxaPerformance = tabelaTaxaPerformance.TaxaPerformance.Value;
                if (tabelaTaxaPerformance.DataFim.HasValue)
                    this.dataFim = tabelaTaxaPerformance.DataFim.Value;
                this.impactaPL = tabelaTaxaPerformance.ImpactaPL;

                if (this.tipoCalculo == (byte)TipoCalculoPerformance.PorCertificado)
                {
                    #region Monta de forma analítica as posições com performance projetada
                    PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                    if (cliente.DataDia.Value == data)
                    {
                        CotistaQuery cotistaQuery = new CotistaQuery("C");
                        PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("P");

                        posicaoCotistaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == posicaoCotistaQuery.IdCotista);
                        posicaoCotistaQuery.Where(posicaoCotistaQuery.Quantidade.NotEqual(0),
                                                  posicaoCotistaQuery.IdCarteira.Equal(this.idCarteira));
                        posicaoCotistaQuery.OrderBy(cotistaQuery.Nome.Ascending);
                        posicaoCotistaCollection.Load(posicaoCotistaQuery);
                    }
                    else
                    {
                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();

                        CotistaQuery cotistaQuery = new CotistaQuery("C");
                        PosicaoCotistaHistoricoQuery posicaoCotistaQuery = new PosicaoCotistaHistoricoQuery("P");

                        posicaoCotistaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == posicaoCotistaQuery.IdCotista);
                        posicaoCotistaQuery.Where(posicaoCotistaQuery.Quantidade.NotEqual(0),
                                                  posicaoCotistaQuery.IdCarteira.Equal(this.idCarteira),
                                                  posicaoCotistaQuery.DataHistorico.Equal(data));
                        posicaoCotistaQuery.OrderBy(cotistaQuery.Nome.Ascending);
                        posicaoCotistaHistoricoCollection.Load(posicaoCotistaQuery);

                        PosicaoCotistaCollection posicaoCotistaCollectionAux = new PosicaoCotistaCollection(posicaoCotistaHistoricoCollection);
                        posicaoCotistaCollection.Combine(posicaoCotistaCollectionAux);
                    }


                    foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                    {
                        int idCotista = posicaoCotista.IdCotista.Value;

                        Cotista cotista = new Cotista();
                        cotista.LoadByPrimaryKey(idCotista);

                        string nome = cotista.Nome;

                        DateTime dataConversao = posicaoCotista.DataConversao.Value;
                        int idPosicao = posicaoCotista.IdPosicao.Value;

                        DateTime dataBase = dataConversao;
                        if (posicaoCotista.DataUltimoCortePfee.HasValue)
                        {
                            if (posicaoCotista.DataUltimoCortePfee.Value > dataBase)
                            {
                                dataBase = posicaoCotista.DataUltimoCortePfee.Value;
                            }
                        }

                        if (dataBase == data)
                        {
                            continue; //Não há performance a ser calculada na própria data base!
                        }

                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                        #region Busca as cotas base e atual (bruta ou liquida, D0 ou D-1)
                        HistoricoCota historicoCota = new HistoricoCota();

                        #region Busca a cota na dataBase (sempre a cota liquida)
                        decimal cotaBase;
                        try
                        {
                            historicoCota.BuscaValorCotaDia(this.idCarteira, dataBase);
                        }
                        catch (Exception e)
                        {

                            string msg = "Cálculo da performance. " + e.Message;
                            throw new HistoricoCotaNaoCadastradoException(msg);
                        }

                        if (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura)
                        {
                            cotaBase = historicoCota.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaBase = historicoCota.CotaFechamento.Value;
                        }
                        #endregion

                        #region Busca a cota na data atual (ou D-1)
                        decimal cotaAtual = 0;
                        DateTime dataCota = data;
                        switch (this.baseApuracao)
                        {
                            case (int)BaseApuracaoPerformance.CotaLiquida_DiaAnterior:
                                if (dataBase <= dataAnterior) //Para não ter problema no início da implantação
                                {
                                    dataCota = dataAnterior;
                                }

                                try
                                {
                                    historicoCota.BuscaValorCotaDia(this.idCarteira, dataCota);
                                }
                                catch (Exception e)
                                {

                                    string msg = "Cálculo da performance. " + e.Message;
                                    throw new HistoricoCotaNaoCadastradoException(msg);
                                }

                                if (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura)
                                {
                                    cotaAtual = historicoCota.CotaAbertura.Value;
                                }
                                else
                                {
                                    cotaAtual = historicoCota.CotaFechamento.Value;
                                }
                                break;
                            case (int)BaseApuracaoPerformance.CotaBruta_Dia:
                                try
                                {
                                    historicoCota.BuscaValorCotaDia(this.idCarteira, data);
                                }
                                catch (Exception e)
                                {

                                    string msg = "Cálculo da performance. " + e.Message;
                                    throw new HistoricoCotaNaoCadastradoException(msg);
                                }
                                cotaAtual = historicoCota.CotaBruta.Value;
                                break;
                        }
                        #endregion
                        #endregion

                        #region Calcula o fator acumulado do índice
                        decimal fatorIndice = 1;
                        if (this.idIndice.HasValue)
                        {
                            DateTime dataFinalIndice = dataCota;

                            Indice indice = new Indice();                            
                            indice.LoadByPrimaryKey((short)idIndice);

                            if (indice.Tipo.Value == (byte)TipoIndice.Percentual)
                            {
                                dataFinalIndice = Calendario.SubtraiDiaUtil(dataCota, 1);
                            }
                            if (indice.Tipo.Value == (byte)TipoIndice.Decimal &&
                                this.contagemDiasJuros == (int)ContagemDias.Uteis &&
                                (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal ||
                                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_1 ||
                                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_2 ||
                                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_3))
                            {
                                fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataBase, dataFinalIndice, dataFinalIndice.Day, indice, percentualIndice.Value, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                               
                            }
                            else
                            {
                                fatorIndice = CalculoFinanceiro.CalculaFatorIndice(dataBase, dataFinalIndice, idIndice.Value, percentualIndice.Value, true);
                            }
                        }
                        #endregion

                        #region Calcula o fator acumulado dos juros
                        decimal fatorJuros;
                        if (tipoApropriacaoJuros == (int)TipoApropriacao.Exponencial)
                        {
                            if (contagemDiasJuros == (int)ContagemDias.Corridos)
                            {   //Exponencial Dias Corridos
                                fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros);
                            }
                            else
                            {   //Exponencial Dias Úteis
                                fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                                          LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                        }
                        else
                        {
                            if (contagemDiasJuros == (int)ContagemDias.Corridos)
                            {   //Linear Dias Corridos
                                fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros);
                            }
                            else
                            {   //Linear Dias Úteis
                                fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                                     LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                        }
                        #endregion

                        #region Calcula valor da performance
                        PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollectionQuantidade = new PosicaoCotistaAberturaCollection();
                        posicaoCotistaAberturaCollectionQuantidade.Query.Select(posicaoCotistaAberturaCollectionQuantidade.Query.Quantidade);
                        posicaoCotistaAberturaCollectionQuantidade.Query.Where(posicaoCotistaAberturaCollectionQuantidade.Query.IdPosicao.Equal(idPosicao),
                                                                               posicaoCotistaAberturaCollectionQuantidade.Query.DataHistorico.Equal(data));
                        posicaoCotistaAberturaCollectionQuantidade.Query.Load();

                        decimal quantidadeCotas = 0;
                        if (posicaoCotistaAberturaCollectionQuantidade.Count > 0)
                        {
                            quantidadeCotas = posicaoCotistaAberturaCollectionQuantidade[0].Quantidade.Value;
                        }

                        decimal cotaCorrigida = 0;
                        decimal valorPerformance = 0;
                        decimal rendimento = 0;
                        if (fatorIndice < 1)
                        {
                            switch (tabelaTaxaPerformance.CalculaBenchmarkNegativo.Value)
                            {
                                case (int)CalculaBenchmarkNegativo.CalculaGanhoTotal:
                                    cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                                    valorPerformance = 0;
                                    rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                                    if (rendimento > 0)
                                    {
                                        valorPerformance = rendimento * taxaPerformance / 100M;
                                    }
                                    break;
                                case (int)CalculaBenchmarkNegativo.CalculaPartePositiva:
                                    cotaCorrigida = cotaBase * fatorJuros;
                                    valorPerformance = 0;
                                    rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                                    if (rendimento > 0)
                                    {
                                        valorPerformance = rendimento * taxaPerformance / 100M;
                                    }
                                    break;
                                case (byte)CalculaBenchmarkNegativo.CalculaGanhoTotalComRecalculo:
                                    cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                                    rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                                    if (baseApuracao == (byte)BaseApuracaoPerformance.CotaBruta_Dia)
                                    {
                                        //Calculo da performance pelo metodo tradicional
                                        decimal valorPerformance1 = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;

                                        //Vamos calcular agora a performance pelo metodo alternativo
                                        decimal cotaCorrigida2 = cotaBase * fatorJuros;
                                        decimal rendimento2 = (cotaAtual - cotaCorrigida2) * quantidadeCotas;
                                        decimal valorPerformance2 = rendimento2;

                                        if (valorPerformance1 <= valorPerformance2)
                                        {
                                            valorPerformance = valorPerformance1;
                                        }
                                        else
                                        {
                                            valorPerformance = valorPerformance2;
                                            cotaCorrigida = cotaCorrigida2;
                                            rendimento = rendimento2;
                                        }

                                    }
                                    else
                                    {
                                        if (cotaAtual > cotaCorrigida)
                                        {
                                            valorPerformance = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                                        }
                                    }
                                    break;
                                case (int)CalculaBenchmarkNegativo.NaoCalculaPerformance:
                                    valorPerformance = 0;
                                    break;
                            }
                        }
                        else
                        {
                            cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                            rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                            valorPerformance = rendimento * this.taxaPerformance / 100M;
                        }

                        //Impedir que valor performance seja negativa
                        valorPerformance = Math.Max(0, valorPerformance);
                        //Rendimento tambem nao deve ser negativo para nao ficar discrepante no relatorio
                        rendimento = Math.Max(0, rendimento);
                        
                        #endregion

                        ValoresPerformance valoresPerformance = new ValoresPerformance();
                        valoresPerformance.nome = nome;
                        valoresPerformance.idCotista = idCotista;
                        valoresPerformance.dataBase = dataBase;
                        valoresPerformance.cotaAtual = cotaAtual;
                        valoresPerformance.cotaBase = cotaBase;
                        valoresPerformance.cotaCorrigida = cotaCorrigida;
                        valoresPerformance.fatorIndice = fatorIndice;
                        valoresPerformance.fatorJuros = fatorJuros;
                        valoresPerformance.quantidadeCotas = quantidadeCotas;
                        valoresPerformance.rendimento = rendimento;
                        valoresPerformance.valorPerformance = Utilitario.Truncate(valorPerformance, 2); 
                        this.listaValores.Add(valoresPerformance);
                    }
                    #endregion
                }
                else if (this.tipoCalculo == (byte)TipoCalculoPerformance.CotaCota)
                {
                    #region Monta de forma sintética a performance projetada
                    decimal cotaAtual = 0;
                    DateTime dataBase = new DateTime();

                    if (cliente.DataDia.Value == data)
                    {
                        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                        posicaoCotistaCollection.Query.Select(posicaoCotistaCollection.Query.DataUltimoCortePfee);
                        posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                             posicaoCotistaCollection.Query.Quantidade.NotEqual(0));
                        posicaoCotistaCollection.Query.OrderBy(posicaoCotistaCollection.Query.DataUltimoCortePfee.Descending);
                        posicaoCotistaCollection.Query.Load();

                        if (posicaoCotistaCollection.Count > 0)
                        {
                            dataBase = this.dataReferencia;

                            if (posicaoCotistaCollection[0].DataUltimoCortePfee.HasValue)
                            {
                                dataBase = posicaoCotistaCollection[0].DataUltimoCortePfee.Value;
                            }
                        }
                        else
                        {
                            throw new Exception("Não há posição de cotas da carteira para cálculo da performance");
                        }
                    }
                    else
                    {
                        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                        posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.DataUltimoCortePfee,
                                                                       posicaoCotistaHistoricoCollection.Query.DataConversao);
                        posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                                      posicaoCotistaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                      posicaoCotistaHistoricoCollection.Query.DataHistorico.Equal(data));
                        posicaoCotistaHistoricoCollection.Query.OrderBy(posicaoCotistaHistoricoCollection.Query.DataUltimoCortePfee.Descending);
                        posicaoCotistaHistoricoCollection.Query.Load();

                        if (posicaoCotistaHistoricoCollection.Count > 0)
                        {
                            dataBase = this.dataReferencia;

                            if (posicaoCotistaHistoricoCollection[0].DataUltimoCortePfee.HasValue)
                            {
                                dataBase = posicaoCotistaHistoricoCollection[0].DataUltimoCortePfee.Value;
                            }
                        }
                        else
                        {
                            throw new Exception("Não há posição de cotas da carteira para cálculo da performance");
                        }
                    }

                    ValoresPerformance valoresPerformance = new ValoresPerformance();

                    // Verifica se já foi processado após a correção que grava as informações na tabela
                    CalculoPerformanceHistorico calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                    if (calculoPerformanceHistorico.LoadByPrimaryKey(data, idTabela))
                    {
                        if (calculoPerformanceHistorico.CotaAtual.HasValue)
                        {
                            valoresPerformance.nome = "";
                            valoresPerformance.idCotista = this.idCarteira;
                            valoresPerformance.dataBase = dataBase;
                            valoresPerformance.cotaAtual = calculoPerformanceHistorico.CotaAtual.Value;
                            valoresPerformance.cotaBase = calculoPerformanceHistorico.CotaBase.Value;
                            valoresPerformance.cotaCorrigida = calculoPerformanceHistorico.CotaCorrigida.Value;
                            valoresPerformance.fatorIndice = calculoPerformanceHistorico.FatorIndice.Value;
                            valoresPerformance.fatorJuros = calculoPerformanceHistorico.FatorJuros.Value;
                            valoresPerformance.quantidadeCotas = calculoPerformanceHistorico.QuantidadeCotas.Value;
                            valoresPerformance.rendimento = calculoPerformanceHistorico.Rendimento.Value;
                            valoresPerformance.valorPerformance = calculoPerformanceHistorico.ValorAcumulado.Value;
                            this.listaValores.Add(valoresPerformance);

                            return;

                        }
                    }

                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    decimal cotaBase = 0;
                    #region Busca a cota na dataBase (sempre a cota liquida)
                    HistoricoCota historicoCota = new HistoricoCota();
                    try
                    {
                        historicoCota.BuscaValorCotaDia(this.idCarteira, dataBase);
                    }
                    catch (Exception e)
                    {
                        string msg = "Cálculo da performance. " + e.Message;
                        throw new HistoricoCotaNaoCadastradoException(msg);
                    }

                    if (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura)
                    {
                        cotaBase = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        cotaBase = historicoCota.CotaFechamento.Value;
                    }
                    #endregion

                    #region Busca a cota na data atual (ou D-1)
                    DateTime dataCota = data;
                    switch (this.baseApuracao)
                    {
                        case (int)BaseApuracaoPerformance.CotaLiquida_DiaAnterior:
                            if (DateTime.Compare(dataBase, dataAnterior) <= 0) //Para não ter problema no início da implantação
                            {
                                dataCota = dataAnterior;
                            }

                            try
                            {
                                historicoCota.BuscaValorCotaDia(this.idCarteira, dataCota);
                            }
                            catch (Exception e)
                            {

                                string msg = "Cálculo da performance. " + e.Message;
                                throw new HistoricoCotaNaoCadastradoException(msg);
                            }

                            if (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura)
                            {
                                cotaAtual = historicoCota.CotaAbertura.Value;
                            }
                            else
                            {
                                cotaAtual = historicoCota.CotaFechamento.Value;
                            }
                            break;
                        case (int)BaseApuracaoPerformance.CotaBruta_Dia:
                            // Se recalcula Cota SIM, tem que recalcular o cota bruta
                            if (ParametrosConfiguracaoSistema.Fundo.RecalculaCota == "S" && this.impactaPL == "S")
                            {
                                calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                                decimal valorPfeeAcumDia = 0;
                                if (calculoPerformanceHistorico.LoadByPrimaryKey(data, idTabela))
                                {
                                    valorPfeeAcumDia = calculoPerformanceHistorico.ValorAcumulado.Value;
                                }

                                try
                                {
                                    historicoCota.BuscaValorCotaDia(this.idCarteira, data);
                                }
                                catch (Exception e)
                                {

                                    string msg = "Cálculo da performance. " + e.Message;
                                    throw new HistoricoCotaNaoCadastradoException(msg);
                                }

                                OperacaoCotista operacaoCotista = new OperacaoCotista();
                                decimal quantidadeBruta = historicoCota.QuantidadeFechamento.Value + operacaoCotista.RetornaTotalCotasResgateEspecial(idCarteira, data);

                                decimal plBruto = (historicoCota.QuantidadeFechamento.Value * historicoCota.CotaBruta.Value) + valorPfeeAcumDia + operacaoCotista.RetornaTotalValorResgateEspecial(idCarteira, data);
                                cotaAtual = plBruto / quantidadeBruta;
                            }
                            else
                            {
                                try
                                {
                                    historicoCota.BuscaValorCotaDia(this.idCarteira, data);
                                }
                                catch (Exception e)
                                {

                                    string msg = "Cálculo da performance. " + e.Message;
                                    throw new HistoricoCotaNaoCadastradoException(msg);
                                }
                                cotaAtual = historicoCota.CotaBruta.Value;
                            }
                            break;
                    }
                    #endregion

                    #region Calcula o fator acumulado do índice
                    decimal fatorIndice = 1;
                    if (this.idIndice.HasValue)
                    {
                        DateTime dataFinalIndice = data;
                        DateTime dataInicialIndice = dataBase;

                        Indice indice = new Indice();                        
                        indice.LoadByPrimaryKey(idIndice.Value);

                        if (indice.Tipo.Value == (byte)TipoIndice.Percentual)
                        {
                            dataFinalIndice = Calendario.SubtraiDiaUtil(data, 1);
                        }
                        if (indice.Tipo.Value == (byte)TipoIndice.Decimal &&
                            this.contagemDiasJuros == (int)ContagemDias.Uteis &&
                                (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal ||
                                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_1 ||
                                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_2 ||
                                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_3))
                        {
                           fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataInicialIndice, dataFinalIndice, dataFinalIndice.Day, indice, percentualIndice.Value, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else
                        {
                            fatorIndice = CalculoFinanceiro.CalculaFatorIndice(dataInicialIndice, dataFinalIndice, this.idIndice.Value, this.percentualIndice.Value, true);
                        }
                    }
                    #endregion

                    #region Calcula o fator acumulado dos juros
                    decimal fatorJuros;
                    if (tipoApropriacaoJuros == (int)TipoApropriacao.Exponencial)
                    {
                        if (contagemDiasJuros == (int)ContagemDias.Corridos)
                        {   //Exponencial Dias Corridos
                            fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, data, taxaJuros, (BaseCalculo)baseAnoJuros);
                        }
                        else
                        {   //Exponencial Dias Úteis
                            fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, data, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                                      LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                    }
                    else
                    {
                        if (contagemDiasJuros == (int)ContagemDias.Corridos)
                        {   //Linear Dias Corridos
                            fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, data, taxaJuros, (BaseCalculo)baseAnoJuros);
                        }
                        else
                        {   //Linear Dias Úteis
                            fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, data, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                                 LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                    }
                    #endregion

                    #region Calcula valor da performance
                    decimal quantidadeCotas = historicoCota.RetornaQuantidadeCotas(idCarteira, dataAnterior);
                    decimal cotaCorrigida = 0;
                    decimal rendimento = 0;
                    decimal valorPerformance = 0;
                    if (fatorIndice < 1)
                    {
                        switch (tabelaTaxaPerformance.CalculaBenchmarkNegativo.Value)
                        {
                            case (byte)CalculaBenchmarkNegativo.CalculaGanhoTotalComRecalculo:
                            case (int)CalculaBenchmarkNegativo.CalculaGanhoTotal:
                                cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                                valorPerformance = 0;
                                rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                                if (rendimento > 0)
                                {
                                    valorPerformance = rendimento * taxaPerformance / 100M;
                                }
                                break;
                            case (int)CalculaBenchmarkNegativo.CalculaPartePositiva:
                                cotaCorrigida = cotaBase * fatorJuros;
                                valorPerformance = 0;
                                rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                                if (rendimento > 0)
                                {
                                    valorPerformance = rendimento * taxaPerformance / 100M;
                                }
                                break;
                            case (int)CalculaBenchmarkNegativo.NaoCalculaPerformance:
                                valorPerformance = 0;
                                break;
                        }
                    }
                    else
                    {
                        cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                        rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                        valorPerformance = rendimento * this.taxaPerformance / 100M;
                    }

                    if (valorPerformance < 0)
                    {
                        valorPerformance = 0;
                    }
                    #endregion

                    valoresPerformance.nome = "";
                    valoresPerformance.idCotista = this.idCarteira;
                    valoresPerformance.dataBase = dataBase;
                    valoresPerformance.cotaAtual = cotaAtual;
                    valoresPerformance.cotaBase = cotaBase;
                    valoresPerformance.cotaCorrigida = cotaCorrigida;
                    valoresPerformance.fatorIndice = fatorIndice;
                    valoresPerformance.fatorJuros = fatorJuros;
                    valoresPerformance.quantidadeCotas = quantidadeCotas;
                    valoresPerformance.rendimento = rendimento;
                    valoresPerformance.valorPerformance = valorPerformance;
                    this.listaValores.Add(valoresPerformance);
                    #endregion
                }
                else if (this.tipoCalculo == (byte)TipoCalculoPerformance.CotaCotaDiario)
                {
                    #region Monta a performance cota a cota diario
                    decimal cotaAtual = 0;
                    DateTime dataBase = this.dataReferencia;

                    short idLocal = cliente.IdLocal.Value;

                    if (idLocal == LocalFeriadoFixo.Brasil)
                    {
                        dataBase = Calendario.SubtraiDiaUtil(dataBase, 1);
                    }
                    else
                    {
                        dataBase = Calendario.SubtraiDiaUtil(dataBase, 1, idLocal, TipoFeriado.Outros);
                    }

                    
                    PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
                    posicaoCotistaHistoricoCollection.Query.Select(posicaoCotistaHistoricoCollection.Query.DataUltimoCortePfee.Max());
                    posicaoCotistaHistoricoCollection.Query.Where(posicaoCotistaHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                                  posicaoCotistaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                  posicaoCotistaHistoricoCollection.Query.DataHistorico.LessThanOrEqual(data),
                                                                  posicaoCotistaHistoricoCollection.Query.DataUltimoCortePfee.NotEqual(data));
                    posicaoCotistaHistoricoCollection.Query.Load();

                    if (posicaoCotistaHistoricoCollection.Count > 0 && posicaoCotistaHistoricoCollection[0].DataUltimoCortePfee.HasValue)
                    {
                        if (posicaoCotistaHistoricoCollection[0].DataUltimoCortePfee.Value > dataBase)
                        {
                            dataBase = posicaoCotistaHistoricoCollection[0].DataUltimoCortePfee.Value;
                        }
                    }

                    DateTime dataAux = data;

                    if (this.dataFim.HasValue && dataAux > this.dataFim)
                        dataAux = this.dataFim.Value;

                    CalculoPerformanceHistorico calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                    ValoresPerformance valoresPerformance = new ValoresPerformance();
                    while (dataAux > dataBase && dataAux > cliente.DataImplantacao.Value)
                    {
                        // Verifica se já foi processado após a correção que grava as informações na tabela
                        calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                        if (calculoPerformanceHistorico.LoadByPrimaryKey(dataAux, idTabela))
                        {
                            if (calculoPerformanceHistorico.CotaAtual.HasValue)
                            {
                                valoresPerformance = new ValoresPerformance();
                                valoresPerformance.nome = "";
                                valoresPerformance.idCotista = this.idCarteira;
                                valoresPerformance.dataBase = dataAux;
                                valoresPerformance.cotaAtual = calculoPerformanceHistorico.CotaAtual.Value;
                                valoresPerformance.cotaBase = calculoPerformanceHistorico.CotaBase.Value;
                                valoresPerformance.cotaCorrigida = calculoPerformanceHistorico.CotaCorrigida.Value;
                                valoresPerformance.fatorIndice = calculoPerformanceHistorico.FatorIndice.Value;
                                valoresPerformance.fatorJuros = calculoPerformanceHistorico.FatorJuros.Value;
                                valoresPerformance.quantidadeCotas = calculoPerformanceHistorico.QuantidadeCotas.Value;
                                valoresPerformance.rendimento = calculoPerformanceHistorico.Rendimento.Value;
                                valoresPerformance.valorPerformance = calculoPerformanceHistorico.ValorDia.Value;
                                this.listaValores.Add(valoresPerformance);

                                if (idLocal == LocalFeriadoFixo.Brasil)
                                {
                                    dataAux = Calendario.SubtraiDiaUtil(dataAux, 1);
                                }
                                else
                                {
                                    dataAux = Calendario.SubtraiDiaUtil(dataAux, 1, idLocal, TipoFeriado.Outros);
                                }
                                // Vai pra próxima data
                                continue;
                            }
                        }

                        // Se continuar aqui é porque não foi reprocessado, então carrega da forma antiga calculando novamente
                        DateTime dataAuxAnterior = new DateTime();
                        if (idLocal == LocalFeriadoFixo.Brasil)
                        {
                            dataAuxAnterior = Calendario.SubtraiDiaUtil(dataAux, 1);
                        }
                        else
                        {
                            dataAuxAnterior = Calendario.SubtraiDiaUtil(dataAux, 1, idLocal, TipoFeriado.Outros);
                        }

                        #region Monta de forma sintética a performance projetada
                        HistoricoCota historicoCota = new HistoricoCota();
                        decimal quantidadeCotas = historicoCota.RetornaQuantidadeCotas(idCarteira, dataAuxAnterior);

                        decimal cotaBase = 0;
                        #region Busca a cota na data anterior (sempre a cota liquida)                        
                        try
                        {
                            historicoCota.BuscaValorCotaDia(this.idCarteira, dataAuxAnterior);
                        }
                        catch (Exception e)
                        {
                            string msg = "Cálculo da performance. " + e.Message;
                            throw new HistoricoCotaNaoCadastradoException(msg);
                        }

                        if (carteira.TipoCota.Value == (int)TipoCotaFundo.Abertura)
                        {
                            cotaBase = historicoCota.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaBase = historicoCota.CotaFechamento.Value;
                        }
                        #endregion

                        #region Busca a cota Bruta na data atual
                        decimal valorPfeeAcumDia = 0;
                        bool fimApropriacao = false;
                        calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                        if (calculoPerformanceHistorico.LoadByPrimaryKey(dataAuxAnterior, idTabela))
                        {
                            if (calculoPerformanceHistorico.DataFimApropriacao == dataAux)
                            {
                                fimApropriacao = true;
                                valorPfeeAcumDia = calculoPerformanceHistorico.ValorAcumulado.Value;
                            }
                        }

                        calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                        if (calculoPerformanceHistorico.LoadByPrimaryKey(dataAux, idTabela))
                        {
                            if (fimApropriacao)
                                valorPfeeAcumDia += calculoPerformanceHistorico.ValorDia.Value;
                            else
                                valorPfeeAcumDia = calculoPerformanceHistorico.ValorAcumulado.Value;
                        }

                        // Se recalcula Cota SIM, tem que recalcular o cota bruta
                        if (ParametrosConfiguracaoSistema.Fundo.RecalculaCota == "S" && this.impactaPL == "S")
                        {

                            try
                            {
                                historicoCota.BuscaValorCotaDia(this.idCarteira, dataAux);
                            }
                            catch (Exception e)
                            {

                                string msg = "Cálculo da performance. " + e.Message;
                                throw new HistoricoCotaNaoCadastradoException(msg);
                            }

                            OperacaoCotista operacaoCotista = new OperacaoCotista();
                            decimal quantidadeBruta = historicoCota.QuantidadeFechamento.Value + operacaoCotista.RetornaTotalCotasResgateEspecial(idCarteira, dataAux);

                            decimal plBruto = (historicoCota.QuantidadeFechamento.Value * historicoCota.CotaBruta.Value) + valorPfeeAcumDia + operacaoCotista.RetornaTotalValorResgateEspecial(idCarteira, dataAux);
                            cotaAtual = plBruto / quantidadeBruta;
                        }
                        else
                        {
                            try
                            {
                                historicoCota.BuscaValorCotaDia(this.idCarteira, dataAux);
                            }
                            catch (Exception e)
                            {

                                string msg = "Cálculo da performance. " + e.Message;
                                throw new HistoricoCotaNaoCadastradoException(msg);
                            }
                            cotaAtual = historicoCota.CotaBruta.Value;
                        }
                        #endregion

                        decimal fatorJuros = 1;
                        #region Calcula o fator acumulado dos juros
                        if (tipoApropriacaoJuros == (int)TipoApropriacao.Exponencial)
                        {
                            if (contagemDiasJuros == (int)ContagemDias.Corridos)
                            {   //Exponencial Dias Corridos
                                fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataAuxAnterior, dataAux, taxaJuros, (BaseCalculo)baseAnoJuros);
                            }
                            else
                            {   //Exponencial Dias Úteis
                                fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataAuxAnterior, dataAux, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                                          LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                        }
                        else
                        {
                            if (contagemDiasJuros == (int)ContagemDias.Corridos)
                            {   //Linear Dias Corridos
                                fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataAuxAnterior, dataAux, taxaJuros, (BaseCalculo)baseAnoJuros);
                            }
                            else
                            {   //Linear Dias Úteis
                                fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataAuxAnterior, dataAux, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                                     LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                        }
                        #endregion

                        decimal fatorIndice = 1;
                        #region Calcula o fator acumulado do índice
                        if (this.idIndice.HasValue)
                        {
                            if (cliente.DataDia.Value != dataAux && (this.idIndice.Value == (short)ListaIndiceFixo.IPCA ||
                                                                    this.idIndice.Value == (short)ListaIndiceFixo.IGPM ||
                                                                    this.idIndice.Value == (short)ListaIndiceFixo.INPC))
                            {
                                calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                                if (calculoPerformanceHistorico.LoadByPrimaryKey(dataAux, idTabela))
                                {
                                    decimal valorDiaPfee = calculoPerformanceHistorico.ValorDia.Value;

                                    fatorIndice = (cotaAtual - (valorDiaPfee / (quantidadeCotas * this.taxaPerformance / 100))) / fatorJuros / cotaBase;
                                }
                            }
                            else
                            {
                                DateTime dataFinalIndice = dataAux;
                                DateTime dataInicialIndice = dataAuxAnterior;

                                Indice indice = new Indice();                                
                                indice.LoadByPrimaryKey(idIndice.Value);

                                if (indice.Tipo.Value == (byte)TipoIndice.Percentual)
                                {
                                    dataInicialIndice = dataAux;
                                }
                                if (indice.Tipo.Value == (byte)TipoIndice.Decimal &&
                                    this.contagemDiasJuros == (int)ContagemDias.Uteis &&
                                   (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal ||
                                    indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_1 ||
                                    indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_2 ||
                                    indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_3))
                                {
                                    fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataInicialIndice, dataFinalIndice, dataFinalIndice.Day, indice, percentualIndice.Value, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                                }
                                else
                                {
                                    fatorIndice = CalculoFinanceiro.CalculaFatorIndice(dataInicialIndice, dataFinalIndice, this.idIndice.Value, this.percentualIndice.Value, true);
                                }
                            }
                        }
                        #endregion                        

                        #region Calcula valor da performance
                        decimal cotaCorrigida = 0;
                        decimal rendimento = 0;
                        decimal valorPerformance = 0;
                        if (fatorIndice < 1)
                        {
                            switch (tabelaTaxaPerformance.CalculaBenchmarkNegativo.Value)
                            {
                                case (byte)CalculaBenchmarkNegativo.CalculaGanhoTotalComRecalculo:
                                case (byte)CalculaBenchmarkNegativo.CalculaGanhoTotal:
                                    cotaCorrigida = cotaBase * fatorIndice * fatorJuros;                                                                        
                                    break;
                                case (byte)CalculaBenchmarkNegativo.CalculaPartePositiva:
                                    cotaCorrigida = cotaBase * fatorJuros;                                    
                                    break;
                                case (byte)CalculaBenchmarkNegativo.NaoCalculaPerformance:
                                    break;
                            }
                        }
                        else
                        {
                            cotaCorrigida = cotaBase * fatorIndice * fatorJuros;                            
                        }

                        calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                        if (dataAuxAnterior >= dataBase)
                        {
                            if (calculoPerformanceHistorico.LoadByPrimaryKey(dataAuxAnterior, idTabela))
                            {
                                decimal valorPerformanceAcumulado = calculoPerformanceHistorico.ValorAcumulado.HasValue ? calculoPerformanceHistorico.ValorAcumulado.Value : 0;

                                cotaAtual = cotaAtual - (valorPerformanceAcumulado / quantidadeCotas);
                            }
                        }

                        rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                        valorPerformance = Util.Utilitario.RoundMax(rendimento * this.taxaPerformance / 100M,2);
                        #endregion

                        #endregion

                        valoresPerformance = new ValoresPerformance();
                        valoresPerformance.nome = "";
                        valoresPerformance.idCotista = this.idCarteira;
                        valoresPerformance.dataBase = dataAux;
                        valoresPerformance.cotaAtual = cotaAtual;
                        valoresPerformance.cotaBase = cotaBase;
                        valoresPerformance.cotaCorrigida = cotaCorrigida;
                        valoresPerformance.fatorIndice = fatorIndice;
                        valoresPerformance.fatorJuros = fatorJuros;
                        valoresPerformance.quantidadeCotas = quantidadeCotas;
                        valoresPerformance.rendimento = rendimento;
                        valoresPerformance.valorPerformance = valorPerformance;
                        this.listaValores.Add(valoresPerformance);
                                                
                        if (idLocal == LocalFeriadoFixo.Brasil)
                        {
                            dataAux = Calendario.SubtraiDiaUtil(dataAux, 1);                            
                        }
                        else
                        {
                            dataAux = Calendario.SubtraiDiaUtil(dataAux, 1, idLocal, TipoFeriado.Outros);
                        }
                        
                    }
                    #endregion
                }
            }

        }

        /// <summary>
        /// Função básica para copiar de CalculoPerformance para CalculoPerformanceHistorico.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCarteira, DateTime data)
        {
            try
            {
                CalculoPerformanceHistoricoCollection calculoPerformanceDeletarHistoricoCollection = new CalculoPerformanceHistoricoCollection();
                calculoPerformanceDeletarHistoricoCollection.DeletaCalculoPerformanceHistoricoDataHistoricoMaiorIgual(idCarteira, data);
            }
            catch { }

            CalculoPerformanceCollection calculoPerformanceCollection = new CalculoPerformanceCollection();
            calculoPerformanceCollection.BuscaCalculoPerformanceCompleta(idCarteira);
            //
            CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();
            //
            #region Copia de CalculoPerformance para CalculoPerformanceHistorico
            for (int i = 0; i < calculoPerformanceCollection.Count; i++)
            {
                CalculoPerformance calculoPerformance = calculoPerformanceCollection[i];

                CalculoPerformanceHistorico calculoPerformanceHistorico = calculoPerformanceHistoricoCollection.AddNew();

                calculoPerformanceHistorico.DataHistorico = data;
                calculoPerformanceHistorico.IdTabela = calculoPerformance.IdTabela;
                calculoPerformanceHistorico.IdCarteira = calculoPerformance.IdCarteira;
                calculoPerformanceHistorico.ValorAcumulado = calculoPerformance.ValorAcumulado;
                calculoPerformanceHistorico.DataFimApropriacao = calculoPerformance.DataFimApropriacao;
                calculoPerformanceHistorico.DataPagamento = calculoPerformance.DataPagamento;
                calculoPerformanceHistorico.ValorCPMFAcumulado = calculoPerformance.ValorCPMFAcumulado;
                calculoPerformanceHistorico.ValorDia = calculoPerformance.ValorDia;
                calculoPerformanceHistorico.CotaAtual = calculoPerformance.CotaAtual;
                calculoPerformanceHistorico.CotaBase = calculoPerformance.CotaBase;
                calculoPerformanceHistorico.CotaCorrigida = calculoPerformance.CotaCorrigida;
                calculoPerformanceHistorico.FatorIndice = calculoPerformance.FatorIndice;
                calculoPerformanceHistorico.FatorJuros = calculoPerformance.FatorJuros;
                calculoPerformanceHistorico.QuantidadeCotas = calculoPerformance.QuantidadeCotas;
                calculoPerformanceHistorico.Rendimento = calculoPerformance.Rendimento;
            }
            #endregion

            calculoPerformanceHistoricoCollection.Save();
        }

        /// <summary>
        /// Método central para cálculo da performance fee (carteira, cotista, ambos).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void CalculaTaxaPerformance(int idCarteira, DateTime data)
        {
            TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
            tabelaTaxaPerformanceCollection.BuscaTabelaTaxaPerformance(idCarteira, data);

            CalculoPerformanceCollection calculoPerformanceCollectionDeletar = new CalculoPerformanceCollection();
            List<int> listaDeletar = new List<int>();

            for (int i = 0; i < tabelaTaxaPerformanceCollection.Count; i++)
            {
                TabelaTaxaPerformance tabelaTaxaPerformance = tabelaTaxaPerformanceCollection[i];
                int tipoCalculo = tabelaTaxaPerformance.TipoCalculo.Value;
                int idTabela = tabelaTaxaPerformance.IdTabela.Value;
                DateTime? dataFim = tabelaTaxaPerformance.DataFim;

                if (!dataFim.HasValue || data <= dataFim.Value)
                {                    
                    //Calcula na carteira (atualiza CalculoPerformance) e distribui em PosicaoCotista
                    //Total a liquidar calculado a partir do total do cálculo na carteira
                    if (tipoCalculo == (int)TipoCalculoPerformance.CotaCota)
                    {
                        this.CalculaPerformanceCota(idCarteira, data, tipoCalculo, tabelaTaxaPerformance);
                    }
                    //Calcula no cotista (PosicaoCotista) e consolida na carteira (atualiza CalculoPerformance)
                    //Total a liquidar calculado a partir do total do cálculo no cotista
                    else if (tipoCalculo == (int)TipoCalculoPerformance.PorCertificado)
                    {
                        this.CalculaPerformanceCertificado(idCarteira, data, tipoCalculo, tabelaTaxaPerformance);
                    }   
                    //Calcula no cotista -> Atualiza na PosicaoCotista
                    //Calcula na carteira -> Atualiza na CalculoPerformance
                    //Total a liquidar calculado a partir do total do cálculo na carteira
                    else if (tipoCalculo == (int)TipoCalculoPerformance.CotaCotaDiario)
                    {   
                        this.CalculaPerformanceCotaDiario(idCarteira, data, tipoCalculo, tabelaTaxaPerformance); 
                    }
                }                
                
                if (dataFim.HasValue && dataFim == data)
                {
                    //Na Data Fim joga em Liquidacao o valor a pagar
                    CalculoPerformance calculoPerformance = new CalculoPerformance();
                    calculoPerformance.Query.Where(calculoPerformance.Query.IdTabela.Equal(idTabela));
                    if (calculoPerformance.Query.Load())
                    {
                        #region Insert na Liquidacao (valor da provisão) - "Pagamento de Taxa de Performance"
                        //Busca o idContaDefault do cliente
                        Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                        int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                        //

                        string descricao = "Pagamento de Taxa de Performance";
                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = calculoPerformance.DataPagamento.Value;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = Math.Abs(calculoPerformance.ValorAcumulado.Value) * -1;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.IdConta = idContaDefault;
                        liquidacao.Save();

                        if (tabelaTaxaPerformance.ImpactaPL == "N")
                        {
                            descricao = "Estorno Pagamento de Taxa de Performance";
                            liquidacao = new Liquidacao();
                            liquidacao.DataLancamento = data;
                            liquidacao.DataVencimento = calculoPerformance.DataPagamento.Value;
                            liquidacao.Descricao = descricao;
                            liquidacao.Valor = calculoPerformance.ValorAcumulado.Value;
                            liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                            liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance;
                            liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                            liquidacao.IdCliente = idCarteira;
                            liquidacao.IdConta = idContaDefault;
                            liquidacao.Save();
                        }



                        #endregion
                    }
                }
                else if (dataFim.HasValue && data > dataFim)
                {
                    //Deleta CalculoPerformance
                    listaDeletar.Add(idTabela);
                }
            }

            if (listaDeletar.Count > 0)
            {
                #region Deleta CalculoPerformance vencendo
                calculoPerformanceCollectionDeletar.Query.Select(calculoPerformanceCollectionDeletar.Query.IdTabela);
                calculoPerformanceCollectionDeletar.Query.Where(calculoPerformanceCollectionDeletar.Query.IdTabela.In(listaDeletar));
                calculoPerformanceCollectionDeletar.Query.Load();
                calculoPerformanceCollectionDeletar.MarkAllAsDeleted();
                calculoPerformanceCollectionDeletar.Save();
                #endregion
            }
        }

        /// <summary>
        /// Calcula o valor de performance projetada para a carteira (cota contra cota, em levar em conta a data de entrada do certificado).
        /// Atualiza o valor calculado em CalculoPerformance.
        /// Atualiza o valor calculado em PosicaoCotista de forma proporcional à posição de cada certificado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="tipoCalculo">tipoCalculo = Ambos (não distribui em PosicaoCotista)</param>
        /// <param name="tabelaTaxaPerformance">objeto com os parâmetros para cálculo da performance</param>
        private void CalculaPerformanceCota(int idCarteira, DateTime data,
                                            int tipoCalculo, TabelaTaxaPerformance tabelaTaxaPerformance)
        {
            #region Carrega as variáveis de TabelaTaxaPerformance
            int idTabela = tabelaTaxaPerformance.IdTabela.Value;
            int idIndice = tabelaTaxaPerformance.IdIndice.Value;
            decimal percentualIndice = tabelaTaxaPerformance.PercentualIndice.Value;
            decimal taxaJuros = tabelaTaxaPerformance.TaxaJuros.Value;
            int tipoApropriacaoJuros = tabelaTaxaPerformance.TipoApropriacaoJuros.Value;
            int contagemDiasJuros = tabelaTaxaPerformance.ContagemDiasJuros.Value;
            int baseAnoJuros = tabelaTaxaPerformance.BaseAnoJuros.Value;
            decimal taxaPerformance = tabelaTaxaPerformance.TaxaPerformance.Value;
            int baseApuracao = tabelaTaxaPerformance.BaseApuracao.Value;
            int calculaBenchmarkNegativo = tabelaTaxaPerformance.CalculaBenchmarkNegativo.Value;
            DateTime dataReferencia = tabelaTaxaPerformance.DataReferencia.Value;
            DateTime? dataFim = tabelaTaxaPerformance.DataFim;
            string impactaPL = tabelaTaxaPerformance.ImpactaPL;
            #endregion

            #region Busca as cotas base e atual (bruta ou liquida, D0 ou D-1)
            #region Busca o tipo de cota
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            int tipoCota = carteira.TipoCota.Value;
            #endregion

            HistoricoCota historicoCota = new HistoricoCota();

            #region Busca data base de performance
            CalculoPerformance calculoPerformance = new CalculoPerformance();
            calculoPerformance.LoadByPrimaryKey(idTabela);

            //Pega a 1a posição pois todas tem a mesma data de ultimo corte
            PosicaoCotistaCollection posicaoCotistaCollectionPfee = new PosicaoCotistaCollection();
            posicaoCotistaCollectionPfee.Query.Select(posicaoCotistaCollectionPfee.Query.DataUltimoCortePfee);
            posicaoCotistaCollectionPfee.Query.Where(posicaoCotistaCollectionPfee.Query.IdCarteira.Equal(idCarteira),
                                                     posicaoCotistaCollectionPfee.Query.Quantidade.NotEqual(0));
            posicaoCotistaCollectionPfee.Query.Load();

            if (posicaoCotistaCollectionPfee.Count == 0)
            {
                return;
            }

            DateTime dataBase = dataReferencia;
            if (posicaoCotistaCollectionPfee[0].DataUltimoCortePfee.HasValue)
            {
                dataBase = posicaoCotistaCollectionPfee[0].DataUltimoCortePfee.Value;
            }
            #endregion

            if (DateTime.Compare(dataBase, data) == 0)
            {
                return; //Não há performance a ser calculada na própria data base!
            }

            #region Busca a cota na dataBase (sempre a cota liquida)
            decimal cotaBase;
            try
            {
                historicoCota.BuscaValorCotaDia(idCarteira, dataBase);
            }
            catch (Exception e)
            {

                string msg = "Cálculo da performance. " + e.Message;
                throw new HistoricoCotaNaoCadastradoException(msg);
            }

            if (tipoCota == (int)TipoCotaFundo.Abertura)
            {
                cotaBase = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaBase = historicoCota.CotaFechamento.Value;
            }
            #endregion

            #region Busca a cota na data atual (ou D-1)
            decimal cotaAtual = 0;
            DateTime dataCota = data;
            switch (baseApuracao)
            {
                case (int)BaseApuracaoPerformance.CotaLiquida_DiaAnterior:
                    DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    if (DateTime.Compare(dataBase, dataAnterior) <= 0) //Para não ter problema no início da implantação
                    {
                        dataCota = dataAnterior;
                    }

                    try
                    {
                        historicoCota.BuscaValorCotaDia(idCarteira, dataCota);
                    }
                    catch (Exception e)
                    {

                        string msg = "Cálculo da performance. " + e.Message;
                        throw new HistoricoCotaNaoCadastradoException(msg);
                    }

                    if (tipoCota == (int)TipoCotaFundo.Abertura)
                    {
                        cotaAtual = historicoCota.CotaAbertura.Value;
                    }
                    else
                    {
                        cotaAtual = historicoCota.CotaFechamento.Value;
                    }
                    break;
                case (int)BaseApuracaoPerformance.CotaBruta_Dia:
                    try
                    {
                        historicoCota.BuscaValorCotaDia(idCarteira, data);
                    }
                    catch (Exception e)
                    {

                        string msg = "Cálculo da performance. " + e.Message;
                        throw new HistoricoCotaNaoCadastradoException(msg);
                    }
                    if (impactaPL == "N")
                        if (historicoCota.CotaFechamento.Value != 0)
                            cotaAtual = historicoCota.CotaFechamento.Value;
                        else
                            cotaAtual = historicoCota.CotaAbertura.Value;
                    else
                        cotaAtual = historicoCota.CotaBruta.Value;
                    break;
            }
            #endregion
            #endregion

            #region Calcula o fator acumulado do índice
            DateTime dataFinalIndice = dataCota;
            decimal fatorIndice = 0;

            Indice indice = new Indice();
            indice.LoadByPrimaryKey((short)idIndice);

            if (indice.Tipo.Value == (byte)TipoIndice.Percentual)
            {
                dataFinalIndice = Calendario.SubtraiDiaUtil(dataCota, 1);
            }
            if (indice.Tipo.Value == (byte)TipoIndice.Decimal &&
                contagemDiasJuros == (int)ContagemDias.Uteis &&
                    (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal ||
                     indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_1 ||
                     indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_2 ||
                     indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_3))
            {
               fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataBase, dataFinalIndice, dataFinalIndice.Day, indice, percentualIndice, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            else
            {
                fatorIndice = CalculoFinanceiro.CalculaFatorIndice(dataBase, dataFinalIndice, idIndice, percentualIndice, true);
            }

            #endregion

            #region Calcula o fator acumulado dos juros
            decimal fatorJuros;
            if (tipoApropriacaoJuros == (int)TipoApropriacao.Exponencial)
            {
                if (contagemDiasJuros == (int)ContagemDias.Corridos)
                {   //Exponencial Dias Corridos
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros);
                }
                else
                {   //Exponencial Dias Úteis
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                              LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
            }
            else
            {
                if (contagemDiasJuros == (int)ContagemDias.Corridos)
                {   //Linear Dias Corridos
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros);
                }
                else
                {   //Linear Dias Úteis
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                         LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
            }
            #endregion

            #region Calcula valor da performance
            decimal quantidadeCotas = historicoCota.RetornaQuantidadeCotas(idCarteira, data);

            decimal valorPerformance = 0;
            decimal cotaCorrigida = 0;
            if (fatorIndice < 1)
            {
                switch (calculaBenchmarkNegativo)
                {
                    case (int)CalculaBenchmarkNegativo.CalculaGanhoTotal:
                    case (int)CalculaBenchmarkNegativo.CalculaGanhoTotalComRecalculo:
                        cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                        valorPerformance = 0;
                        if (cotaAtual > cotaCorrigida)
                        {
                            valorPerformance = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                        }
                        break;
                    case (int)CalculaBenchmarkNegativo.CalculaPartePositiva:
                        cotaCorrigida = cotaBase * fatorJuros;
                        valorPerformance = 0;
                        if (cotaAtual > cotaCorrigida)
                        {
                            valorPerformance = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                        }
                        break;
                    case (int)CalculaBenchmarkNegativo.NaoCalculaPerformance:
                        valorPerformance = 0;
                        break;
                }
            }
            else
            {
                cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                valorPerformance = 0;
                if (cotaAtual > cotaCorrigida)
                {
                    valorPerformance = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                }
            }

            valorPerformance = Math.Round(valorPerformance, 2);
            #endregion

            #region Salva em CalculoPerformance o valor total da performance acumulada do dia
            //Consolida o valor de performance dos cotistas em CalculoPerformance
            calculoPerformance = new CalculoPerformance();

            bool atualizaDataCorte = false;
            if (calculoPerformance.LoadByPrimaryKey(idTabela))
            {
                #region Update
                calculoPerformance.ValorAcumulado = valorPerformance;
                calculoPerformance.CotaAtual = cotaAtual;
                calculoPerformance.CotaBase = cotaBase;
                calculoPerformance.CotaCorrigida = cotaCorrigida;
                calculoPerformance.FatorIndice = fatorIndice;
                calculoPerformance.FatorJuros = fatorJuros;
                calculoPerformance.QuantidadeCotas = quantidadeCotas;
                calculoPerformance.Rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                calculoPerformance.Save();
                #endregion
            }
            else
            {
                if (tabelaTaxaPerformance.DataUltimoCortePfee.HasValue)
                {
                    atualizaDataCorte = true;
                }

                #region Insert
                DateTime dataFimApropriacao;
                DateTime dataPagamento;
                if (tabelaTaxaPerformance.DataFimApropriacao.HasValue && tabelaTaxaPerformance.DataPagamento.HasValue)
                {
                    dataFimApropriacao = tabelaTaxaPerformance.DataFimApropriacao.Value;
                    dataPagamento = tabelaTaxaPerformance.DataPagamento.Value;
                }
                else
                {
                    int numeroDiasPagamento = tabelaTaxaPerformance.NumeroDiasPagamento.Value;
                    int numeroMesesRenovacao = tabelaTaxaPerformance.NumeroMesesRenovacao.Value;
                    int diaRenovacao = tabelaTaxaPerformance.DiaRenovacao.Value;

                    int ano = data.Year;
                    int mes = data.Month;

                    if (numeroMesesRenovacao >= 1)
                    {
                        numeroMesesRenovacao -= 1; //Subtrai 1 mês, pois já está no próprio mês de início da contagem
                    }

                    //Se nada for especificado a respeito do dia de renovação, assume-se como último dia do mês
                    if (diaRenovacao == 0)
                    {
                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao,
                                                                                    LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        dataFimApropriacao = new DateTime(ano, mes, diaRenovacao);
                        dataFimApropriacao = dataFimApropriacao.AddMonths(numeroMesesRenovacao);
                        if (!Calendario.IsDiaUtil(dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                        {
                            dataFimApropriacao = Calendario.AdicionaDiaUtil(dataFimApropriacao, 1,
                                                                    LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                    }

                    //Calcula data de pagamento
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento,
                                                                        LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }

                calculoPerformance.AddNew();
                calculoPerformance.IdTabela = idTabela;
                calculoPerformance.IdCarteira = idCarteira;
                calculoPerformance.DataFimApropriacao = dataFimApropriacao;
                calculoPerformance.DataPagamento = dataPagamento;
                calculoPerformance.ValorAcumulado = valorPerformance;
                calculoPerformance.ValorCPMFAcumulado = 0;
                calculoPerformance.CotaAtual = cotaAtual;
                calculoPerformance.CotaBase = cotaBase;
                calculoPerformance.CotaCorrigida = cotaCorrigida;
                calculoPerformance.FatorIndice = fatorIndice;
                calculoPerformance.FatorJuros = fatorJuros;
                calculoPerformance.QuantidadeCotas = quantidadeCotas;
                calculoPerformance.Rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                calculoPerformance.Save();
                #endregion
            }
            #endregion

            DateTime dataVencimento = calculoPerformance.DataFimApropriacao.Value;

            if (data != calculoPerformance.DataFimApropriacao.Value && (!dataFim.HasValue || data < dataFim.Value))
            {
                #region Insert na Liquidacao (valor da provisão)
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                //
                string descricao = "Taxa de Performance";
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = calculoPerformance.DataPagamento.Value;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valorPerformance * -1;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaPerformance;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCarteira;
                liquidacao.IdConta = idContaDefault;
                liquidacao.Save();

                if (tabelaTaxaPerformance.ImpactaPL == "N")
                {
                    descricao = "Estorno Taxa de Performance";
                    liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = calculoPerformance.DataPagamento.Value;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorPerformance;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaPerformance;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Save();
                }

                #endregion
            }

            #region Salva o valor proporcional de performance em cada uma das posições de cotistas
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            decimal totalQuantidadeCotas = posicaoCotista.RetornaTotalCotas(idCarteira);

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.BuscaPosicaoCotistaPerformance(idCarteira);

            for (int i = 0; i < posicaoCotistaCollection.Count; i++)
            {
                posicaoCotista = posicaoCotistaCollection[i];
                decimal quantidade = posicaoCotista.Quantidade.Value;
                decimal performanceProporcional = Math.Round(valorPerformance * quantidade / totalQuantidadeCotas, 2);
                posicaoCotista.ValorPerformance = performanceProporcional;

                if (atualizaDataCorte)
                {
                    posicaoCotista.DataUltimoCortePfee = tabelaTaxaPerformance.DataUltimoCortePfee.Value;
                }
            }
            posicaoCotistaCollection.Save();
            #endregion
        }

        /// <summary>
        /// Calcula o valor de performance projetada para cada uma das posições de certificados.
        /// Atualiza o valor calculado em PosicaoCotista.
        /// Atualiza o valor consolidado calculado em CalculoPerformance.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="tipoCalculo">tipoCalculo = Ambos (não atualiza em CalculoAdministracao)</param>
        /// <param name="tabelaTaxaPerformance">objeto com os parâmetros para cálculo da performance</param>
        /// <param name="lancaLiquidacao">se true, lança o total de pfee do cotista em Liquidacao</param>
        private void CalculaPerformanceCertificado(int idCarteira, DateTime data,
                                                   int tipoCalculo, TabelaTaxaPerformance tabelaTaxaPerformance)
        {
            int idTabela = tabelaTaxaPerformance.IdTabela.Value;
            DateTime? dataFim = tabelaTaxaPerformance.DataFim;

            CalculoPerformance calculoPerformance = new CalculoPerformance();
            calculoPerformance.LoadByPrimaryKey(idTabela);

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.BuscaPosicaoCotista(idCarteira);

            PosicaoCotista posicaoCotistaTotalCotas = new PosicaoCotista();
            decimal totalQuantidadeCotas = posicaoCotistaTotalCotas.RetornaTotalCotas(idCarteira);

            decimal totalPerformanceAux = 0;
            for (int i = 0; i < posicaoCotistaCollection.Count; i++)
            {
                PosicaoCotista posicaoCotista = posicaoCotistaCollection[i];
                decimal quantidade = posicaoCotista.Quantidade.Value;
                DateTime dataConversao = posicaoCotista.DataConversao.Value;
                DateTime? dataUltimoCorte = posicaoCotista.DataUltimoCortePfee;

                bool atualizaDataCorte = false;
                if (!calculoPerformance.es.HasData && tabelaTaxaPerformance.DataUltimoCortePfee.HasValue && tabelaTaxaPerformance.DataUltimoCortePfee.Value >= dataConversao)
                {
                    dataUltimoCorte = tabelaTaxaPerformance.DataUltimoCortePfee.Value;
                    atualizaDataCorte = true;
                }

                tabelaTaxaPerformance.TipoCalculoResgate = (byte)TipoCalculoResgatePerformance.Calculado; //PRECISA DISTO PARA GARANTIR QUE VAI CALCULAR E NÃO ASSUMIR VALORES DA CALCULOPERFORMANCE

                decimal valorPerformance = this.RetornaValorPerformanceCotista(idCarteira, data, dataConversao, dataUltimoCorte, quantidade, tabelaTaxaPerformance);
                //
                valorPerformance = Utilitario.Truncate(valorPerformance, 2);
                posicaoCotista.ValorPerformance = valorPerformance;

                if (atualizaDataCorte)
                {
                    posicaoCotista.DataUltimoCortePfee = dataUltimoCorte.Value;
                }

                totalPerformanceAux += valorPerformance;
            }

            decimal totalPerformance = 0;
            totalPerformance = totalPerformanceAux;

            posicaoCotistaCollection.Save();

            #region Salva em CalculoPerformance o valor total da performance acumulada do dia
            if (tipoCalculo == (int)TipoCalculoPerformance.PorCertificado)
            {
                //Consolida o valor de performance dos cotistas em CalculoPerformance                
                decimal valorCPMF = 0;//Utilitario.Truncate(totalPerformance * 0.0038M, 2);

                if (calculoPerformance.es.HasData)
                {
                    #region Update
                    calculoPerformance.ValorAcumulado = totalPerformance;
                    calculoPerformance.ValorCPMFAcumulado = valorCPMF;
                    calculoPerformance.Save();
                    #endregion
                }
                else
                {
                    #region Insert
                    DateTime dataFimApropriacao;
                    DateTime dataPagamento;

                    if (tabelaTaxaPerformance.DataFimApropriacao.HasValue && tabelaTaxaPerformance.DataPagamento.HasValue)
                    {
                        dataFimApropriacao = tabelaTaxaPerformance.DataFimApropriacao.Value;
                        dataPagamento = tabelaTaxaPerformance.DataPagamento.Value;
                    }
                    else
                    {
                        int numeroDiasPagamento = tabelaTaxaPerformance.NumeroDiasPagamento.Value;
                        int numeroMesesRenovacao = tabelaTaxaPerformance.NumeroMesesRenovacao.Value;
                        int diaRenovacao = tabelaTaxaPerformance.DiaRenovacao.Value;

                        int ano = data.Year;
                        int mes = data.Month;

                        if (numeroMesesRenovacao >= 1)
                        {
                            numeroMesesRenovacao -= 1; //Subtrai 1 mês, pois já está no próprio mês de início da contagem
                        }

                        //Se nada for especificado a respeito do dia de renovação, assume-se como último dia do mês
                        if (diaRenovacao == 0)
                        {
                            dataFimApropriacao = Calendario.RetornaUltimoDiaUtilMes(data, numeroMesesRenovacao,
                                                                                    LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else
                        {
                            dataFimApropriacao = new DateTime(ano, mes, diaRenovacao);
                            dataFimApropriacao = dataFimApropriacao.AddMonths(numeroMesesRenovacao);
                            if (!Calendario.IsDiaUtil(dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                            {
                                dataFimApropriacao = Calendario.AdicionaDiaUtil(dataFimApropriacao, 1,
                                                                        LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                        }

                        //Calcula data de pagamento
                        dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento,
                                                                            LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }

                    calculoPerformance.AddNew();
                    calculoPerformance.IdTabela = idTabela;
                    calculoPerformance.IdCarteira = idCarteira;
                    calculoPerformance.DataFimApropriacao = dataFimApropriacao;
                    calculoPerformance.DataPagamento = dataPagamento;
                    calculoPerformance.ValorAcumulado = totalPerformance;
                    calculoPerformance.ValorCPMFAcumulado = valorCPMF;
                    calculoPerformance.Save();
                    #endregion
                }

                if (data != calculoPerformance.DataFimApropriacao.Value && (!dataFim.HasValue || data < dataFim.Value))
                {
                    #region Insert na Liquidacao (valor da provisão)
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                    //
                    string descricao = "Taxa de Performance";
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = calculoPerformance.DataPagamento.Value;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = totalPerformance * -1;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaPerformance;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Save();

                    if (tabelaTaxaPerformance.ImpactaPL == "N")
                    {
                        descricao = "Estorno Taxa de Performance";
                        liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = calculoPerformance.DataPagamento.Value;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = totalPerformance;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaPerformance;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.IdConta = idContaDefault;
                        liquidacao.Save();                    }
                    #endregion
                }
            }
            #endregion

        }

        /// <summary>
        /// Calcula o valor de performance projetada para a carteira (cota contra cota, em levar em conta a data de entrada do certificado).
        /// Atualiza o valor calculado em CalculoPerformance.
        /// Atualiza o valor calculado em PosicaoCotista de forma proporcional à posição de cada certificado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="tipoCalculo">tipoCalculo = Ambos (não distribui em PosicaoCotista)</param>
        /// <param name="tabelaTaxaPerformance">objeto com os parâmetros para cálculo da performance</param>
        private void CalculaPerformanceCotaDiario(int idCarteira, DateTime data,
                                                int tipoCalculo, TabelaTaxaPerformance tabelaTaxaPerformance)
        {
            #region Carrega as variáveis de TabelaTaxaPerformance
            int idTabela = tabelaTaxaPerformance.IdTabela.Value;
            int idIndice = tabelaTaxaPerformance.IdIndice.Value;
            decimal percentualIndice = tabelaTaxaPerformance.PercentualIndice.Value;
            decimal taxaJuros = tabelaTaxaPerformance.TaxaJuros.Value;
            int tipoApropriacaoJuros = tabelaTaxaPerformance.TipoApropriacaoJuros.Value;
            int contagemDiasJuros = tabelaTaxaPerformance.ContagemDiasJuros.Value;
            int baseAnoJuros = tabelaTaxaPerformance.BaseAnoJuros.Value;
            decimal taxaPerformance = tabelaTaxaPerformance.TaxaPerformance.Value;
            int baseApuracao = tabelaTaxaPerformance.BaseApuracao.Value;
            int calculaBenchmarkNegativo = tabelaTaxaPerformance.CalculaBenchmarkNegativo.Value;
            DateTime dataReferencia = tabelaTaxaPerformance.DataReferencia.Value;
            DateTime? dataFim = tabelaTaxaPerformance.DataFim;
            string impactaPL = tabelaTaxaPerformance.ImpactaPL;
            #endregion

            #region Busca o tipo de cota e data inicio cota
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.DataInicioCota);
            carteira.LoadByPrimaryKey(campos, idCarteira);
            int tipoCota = carteira.TipoCota.Value;
            DateTime dataInicioCota = carteira.DataInicioCota.Value;
            #endregion

            CalculoPerformance calculoPerformance = new CalculoPerformance();
            calculoPerformance.LoadByPrimaryKey(idTabela);

            if (dataInicioCota == data)
            {
                return; //Não há performance a ser calculada na própria data de inicio!
            }

            HistoricoCota historicoCota = new HistoricoCota();

            #region Busca local de feriado
            int idLocal = LocalFeriadoFixo.Brasil;
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);
            if (cliente.IdLocal.HasValue)
                idLocal = cliente.IdLocal.Value;
            #endregion

            DateTime dataBase = new DateTime();
            if (idLocal == LocalFeriadoFixo.Brasil)
            {
                dataBase = Calendario.SubtraiDiaUtil(data, 1);
            }
            else
            {
                dataBase = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
            }


            #region Busca a cota na dataBase (sempre a cota liquida)
            decimal cotaBase;
            try
            {
                historicoCota.BuscaValorCotaDia(idCarteira, dataBase);
            }
            catch (Exception e)
            {
                string msg = "Cálculo da performance. " + e.Message;
                throw new HistoricoCotaNaoCadastradoException(msg);
            }

            if (tipoCota == (int)TipoCotaFundo.Abertura)
            {
                cotaBase = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaBase = historicoCota.CotaFechamento.Value;
            }
            #endregion

            #region Busca a cota Bruta na data atual
            decimal cotaAtual = 0;
            try
            {
                historicoCota.BuscaValorCotaDia(idCarteira, data);
            }
            catch (Exception e)
            {

                string msg = "Cálculo da performance. " + e.Message;
                throw new HistoricoCotaNaoCadastradoException(msg);
            }
            if (impactaPL == "N")
                if (historicoCota.CotaFechamento.Value != 0)
                    cotaAtual = historicoCota.CotaFechamento.Value;
                else
                    cotaAtual = historicoCota.CotaAbertura.Value;
            else
                cotaAtual = historicoCota.CotaBruta.Value;
            #endregion

            #region Calcula o fator acumulado do índice
            DateTime dataFinalIndice = data;
            DateTime dataInicialIndice = dataBase;
            decimal fatorIndice = 0;

            Indice indice = new Indice();            
            indice.LoadByPrimaryKey((short)idIndice);

            if (indice.Tipo.Value == (byte)TipoIndice.Percentual)
            {
                dataInicialIndice = dataFinalIndice;
            }
            if (indice.Tipo.Value == (byte)TipoIndice.Decimal &&
                contagemDiasJuros == (int)ContagemDias.Uteis &&
                (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal ||
                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_1 ||
                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_2 ||
                 indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_3))
            {
                fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataInicialIndice, dataFinalIndice, dataFinalIndice.Day, indice, percentualIndice, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            else
            {
                fatorIndice = CalculoFinanceiro.CalculaFatorIndice(dataInicialIndice, dataFinalIndice, idIndice, percentualIndice, true);
            }
            #endregion

            #region Calcula o fator acumulado dos juros
            decimal fatorJuros;
            if (tipoApropriacaoJuros == (int)TipoApropriacao.Exponencial)
            {
                if (contagemDiasJuros == (int)ContagemDias.Corridos)
                {   //Exponencial Dias Corridos
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, data, taxaJuros, (BaseCalculo)baseAnoJuros);
                }
                else
                {   //Exponencial Dias Úteis
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, data, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                              LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
            }
            else
            {
                if (contagemDiasJuros == (int)ContagemDias.Corridos)
                {   //Linear Dias Corridos
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, data, taxaJuros, (BaseCalculo)baseAnoJuros);
                }
                else
                {   //Linear Dias Úteis
                    fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, data, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                         LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
            }
            #endregion

            #region Calcula valor da performance
            decimal quantidadeCotas = historicoCota.RetornaQuantidadeCotas(idCarteira, data);

            decimal valorPerformanceAcumulado = calculoPerformance.ValorAcumulado.HasValue ? calculoPerformance.ValorAcumulado.Value : 0;

            decimal valorPerformanceDia = 0;
            decimal cotaCorrigida = 0;
            if (fatorIndice < 1)
            {
                switch (calculaBenchmarkNegativo)
                {
                    case (int)CalculaBenchmarkNegativo.CalculaGanhoTotal:
                    case (int)CalculaBenchmarkNegativo.CalculaGanhoTotalComRecalculo:
                        cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                        valorPerformanceDia = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                        break;
                    case (int)CalculaBenchmarkNegativo.CalculaPartePositiva:
                        cotaCorrigida = cotaBase * fatorJuros;
                        valorPerformanceDia = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                        break;
                    case (int)CalculaBenchmarkNegativo.NaoCalculaPerformance:
                        break;
                }
            }
            else
            {
                cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                valorPerformanceDia = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
            }

            valorPerformanceAcumulado = valorPerformanceAcumulado + Math.Round(valorPerformanceDia, 2);
            #endregion

            #region Salva em CalculoPerformance o valor total da performance acumulada do dia
            //Consolida o valor de performance dos cotistas em CalculoPerformance
            decimal valorCPMF = Utilitario.Truncate(valorPerformanceAcumulado * 0.0038M, 2);
            calculoPerformance = new CalculoPerformance();
            if (calculoPerformance.LoadByPrimaryKey(idTabela))
            {
                #region Update
                calculoPerformance.ValorDia = valorPerformanceDia;
                calculoPerformance.ValorAcumulado = valorPerformanceAcumulado;
                calculoPerformance.ValorCPMFAcumulado = valorCPMF;
                calculoPerformance.CotaAtual = cotaAtual;
                calculoPerformance.CotaBase = cotaBase;
                calculoPerformance.CotaCorrigida = cotaCorrigida;
                calculoPerformance.FatorIndice = fatorIndice;
                calculoPerformance.FatorJuros = fatorJuros;
                calculoPerformance.QuantidadeCotas = quantidadeCotas;
                calculoPerformance.Rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                calculoPerformance.Save();
                #endregion
            }
            else
            {
                #region Insert
                DateTime dataFimApropriacao;
                DateTime dataPagamento;

                if (tabelaTaxaPerformance.DataFimApropriacao.HasValue && tabelaTaxaPerformance.DataPagamento.HasValue)
                {
                    dataFimApropriacao = tabelaTaxaPerformance.DataFimApropriacao.Value;
                    dataPagamento = tabelaTaxaPerformance.DataPagamento.Value;
                }
                else
                {
                    int numeroDiasPagamento = tabelaTaxaPerformance.NumeroDiasPagamento.Value;
                    int numeroMesesRenovacao = tabelaTaxaPerformance.NumeroMesesRenovacao.Value;
                    int diaRenovacao = tabelaTaxaPerformance.DiaRenovacao.Value;

                    int ano = data.Year;
                    int mes = data.Month;

                    //Se nada for especificado a respeito do dia de renovação, assume-se como último dia do mês
                    if (diaRenovacao == 0)
                    {
                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao,
                                                                                    LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        dataFimApropriacao = new DateTime(ano, mes, diaRenovacao);
                        dataFimApropriacao = dataFimApropriacao.AddMonths(numeroMesesRenovacao);
                        if (!Calendario.IsDiaUtil(dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                        {
                            dataFimApropriacao = Calendario.AdicionaDiaUtil(dataFimApropriacao, 1,
                                                                    LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                    }

                    //Calcula data de pagamento
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento,
                                                                        LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }

                calculoPerformance.AddNew();
                calculoPerformance.IdTabela = idTabela;
                calculoPerformance.IdCarteira = idCarteira;
                calculoPerformance.DataFimApropriacao = dataFimApropriacao;
                calculoPerformance.DataPagamento = dataPagamento;
                calculoPerformance.ValorAcumulado = valorPerformanceAcumulado;
                calculoPerformance.ValorDia = valorPerformanceDia;
                calculoPerformance.ValorCPMFAcumulado = valorCPMF;
                calculoPerformance.CotaAtual = cotaAtual;
                calculoPerformance.CotaBase = cotaBase;
                calculoPerformance.CotaCorrigida = cotaCorrigida;
                calculoPerformance.FatorIndice = fatorIndice;
                calculoPerformance.FatorJuros = fatorJuros;
                calculoPerformance.QuantidadeCotas = quantidadeCotas;
                calculoPerformance.Rendimento = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                calculoPerformance.Save();
                #endregion
            }
            #endregion

            DateTime dataVencimento = calculoPerformance.DataFimApropriacao.Value;

            if (data != calculoPerformance.DataFimApropriacao.Value && (!dataFim.HasValue || data < dataFim.Value))
            {
                if (valorPerformanceAcumulado > 0)
                {
                    #region Insert na Liquidacao (valor da provisão)
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                    //
                    string descricao = "Taxa de Performance";
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = calculoPerformance.DataPagamento.Value; ;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorPerformanceAcumulado * -1;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaPerformance;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Save();

                    if (impactaPL == "N")
                    {
                        descricao = "Estorno Taxa de Performance";
                        liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = calculoPerformance.DataPagamento.Value; ;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = valorPerformanceAcumulado;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.TaxaPerformance;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.IdConta = idContaDefault;
                        liquidacao.Save();
                    }
                    #endregion
                }
            }

            #region Salva o valor proporcional de performance em cada uma das posições de cotistas
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            decimal totalQuantidadeCotas = posicaoCotista.RetornaTotalCotas(idCarteira);

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            posicaoCotistaCollection.BuscaPosicaoCotistaPerformance(idCarteira);

            for (int i = 0; i < posicaoCotistaCollection.Count; i++)
            {
                posicaoCotista = posicaoCotistaCollection[i];
                decimal quantidade = posicaoCotista.Quantidade.Value;
                decimal performanceProporcional = Math.Round(valorPerformanceAcumulado * quantidade / totalQuantidadeCotas, 2);
                posicaoCotista.ValorPerformance = performanceProporcional;
            }
            posicaoCotistaCollection.Save();
            #endregion
        }

        /// <summary>
        /// Retorna o valor da performance calculado para cada uma das posições dos cotistas.
        /// Obs: Retorna valor com n casas decimais (sem truncagem);
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="dataConversao">data da conversão da aplicação</param>
        /// <param name="quantidadeCotas"></param>
        /// <param name="valorPerformance">Valor da performance calculado no 1o passo, a ser usado para desconto da Cota Bruta</param>
        /// <param name="recalcula"></param>
        /// <param name="tabelaTaxaPerformance">objeto com os parâmetros para cálculo da performance</param>
        /// <returns>valor da performance calculado</returns>
        public decimal RetornaValorPerformanceCotista(int idCarteira, DateTime data, DateTime dataConversao, DateTime? dataUltimoCorte,
                                                      decimal quantidadeCotas, TabelaTaxaPerformance tabelaTaxaPerformance)
        {
            #region Carrega as variáveis de TabelaTaxaPerformance
            int idTabela = tabelaTaxaPerformance.IdTabela.Value;
            int idIndice = tabelaTaxaPerformance.IdIndice.Value;
            decimal percentualIndice = tabelaTaxaPerformance.PercentualIndice.Value;
            decimal taxaJuros = tabelaTaxaPerformance.TaxaJuros.Value;
            int tipoApropriacaoJuros = tabelaTaxaPerformance.TipoApropriacaoJuros.Value;
            int contagemDiasJuros = tabelaTaxaPerformance.ContagemDiasJuros.Value;
            int baseAnoJuros = tabelaTaxaPerformance.BaseAnoJuros.Value;
            decimal taxaPerformance = tabelaTaxaPerformance.TaxaPerformance.Value;
            int baseApuracao = tabelaTaxaPerformance.BaseApuracao.Value;
            int calculaBenchmarkNegativo = tabelaTaxaPerformance.CalculaBenchmarkNegativo.Value;
            byte tipoCalculo = tabelaTaxaPerformance.TipoCalculo.Value;
            byte tipoCalculoResgate = tabelaTaxaPerformance.TipoCalculoResgate.Value;
            DateTime dataReferencia = tabelaTaxaPerformance.DataReferencia.Value;
            #endregion

            //Se for proporcional, calculo simplesmente o total geral de pfee pela proporção da qtde cotas da posição vs total qtde cotas da carteira
            //Caso contrário, calcula a pfee para valer da posição resgatada
            decimal valorPerformance = 0;
            if (tipoCalculoResgate == (byte)TipoCalculoResgatePerformance.ProporcionalCarteira)
            {
                valorPerformance = this.RetornaValorPerformanceProporcional(idTabela, idCarteira, quantidadeCotas);
            }
            else
            {
                HistoricoCota historicoCota = new HistoricoCota();

                #region Busca data último corte de performance em CalculoPerformance e computa a data base
                DateTime dataBase = dataConversao;
                if (dataUltimoCorte.HasValue)
                {
                    if (dataUltimoCorte.Value > dataBase)
                    {
                        dataBase = dataUltimoCorte.Value;
                    }
                }
                #endregion

                if (DateTime.Compare(dataBase, data) == 0)
                {
                    return 0; //Não há performance a ser calculada na própria data base!
                }

                #region Busca o tipo de cota
                Carteira carteira = new Carteira();
                List<esQueryItem> carteiraCampos = new List<esQueryItem>();
                carteiraCampos.Add(carteira.Query.TipoCota);
                carteira.LoadByPrimaryKey(carteiraCampos, idCarteira);
                int tipoCota = carteira.TipoCota.Value;
                #endregion

                #region Busca a cota na dataBase (sempre a cota liquida)
                decimal cotaBase;
                try
                {
                    historicoCota.BuscaValorCotaDia(idCarteira, dataBase);
                }
                catch (Exception e)
                {

                    string msg = "Cálculo da performance. " + e.Message;
                    throw new HistoricoCotaNaoCadastradoException(msg);
                }
                if (tipoCota == (int)TipoCotaFundo.Abertura)
                {
                    cotaBase = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaBase = historicoCota.CotaFechamento.Value;
                }
                #endregion

                #region Busca a cota atual (bruta ou liquida, D0 ou D-1)
                decimal cotaAtual = 0;
                DateTime dataCota = data;
                switch (baseApuracao)
                {
                    case (int)BaseApuracaoPerformance.CotaLiquida_DiaAnterior:
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                        if (DateTime.Compare(dataBase, dataAnterior) <= 0) //Para não ter problema no início da implantação
                        {
                            dataCota = dataAnterior;
                        }

                        try
                        {
                            historicoCota.BuscaValorCotaDia(idCarteira, dataCota);
                        }
                        catch (Exception e)
                        {
                            string msg = "Cálculo da performance. " + e.Message;
                            throw new HistoricoCotaNaoCadastradoException(msg);
                        }

                        if (tipoCota == (int)TipoCotaFundo.Abertura)
                        {
                            cotaAtual = historicoCota.CotaAbertura.Value;
                        }
                        else
                        {
                            cotaAtual = historicoCota.CotaFechamento.Value;
                        }
                        break;
                    case (int)BaseApuracaoPerformance.CotaBruta_Dia:
                        try
                        {
                            historicoCota.BuscaValorCotaDia(idCarteira, data);
                        }
                        catch (Exception e)
                        {

                            string msg = "Cálculo da performance. " + e.Message;
                            throw new HistoricoCotaNaoCadastradoException(msg);
                        }

                        cotaAtual = historicoCota.CotaBruta.Value;
                        break;
                }
                #endregion

                #region Calcula o fator acumulado do índice
                DateTime dataFinalIndice = dataCota;
                decimal fatorIndice = 0;

                Indice indice = new Indice();                
                indice.LoadByPrimaryKey((short)idIndice);

                if (indice.Tipo.Value == (byte)TipoIndice.Percentual)
                {
                    dataFinalIndice = Calendario.SubtraiDiaUtil(dataCota, 1);
                }
                if (indice.Tipo.Value == (byte)TipoIndice.Decimal &&
                    contagemDiasJuros == (int)ContagemDias.Uteis &&
                   (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal ||
                    indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_1 ||
                    indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_2 ||
                    indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_3))
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataBase, dataFinalIndice, dataFinalIndice.Day, indice, percentualIndice, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorIndice(dataBase, dataFinalIndice, idIndice, percentualIndice, true);
                }

                #endregion

                #region Calcula o fator acumulado dos juros
                decimal fatorJuros;
                if (tipoApropriacaoJuros == (int)TipoApropriacao.Exponencial)
                {
                    if (contagemDiasJuros == (int)ContagemDias.Corridos)
                    {   //Exponencial Dias Corridos
                        fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros);
                    }
                    else
                    {   //Exponencial Dias Úteis
                        fatorJuros = CalculoFinanceiro.CalculaFatorPreExponencial(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                                 LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                }
                else
                {
                    if (contagemDiasJuros == (int)ContagemDias.Corridos)
                    {   //Linear Dias Corridos
                        fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros);
                    }
                    else
                    {   //Linear Dias Úteis
                        fatorJuros = CalculoFinanceiro.CalculaFatorPreLinear(dataBase, dataCota, taxaJuros, (BaseCalculo)baseAnoJuros,
                                                                             LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                }
                #endregion

                #region Calcula o valor da performance
                if (fatorIndice < 1)
                {
                    decimal cotaCorrigida;
                    switch (calculaBenchmarkNegativo)
                    {
                        case (int)CalculaBenchmarkNegativo.CalculaGanhoTotal:
                            cotaCorrigida = cotaBase * fatorIndice * fatorJuros;

                            if (cotaAtual > cotaCorrigida)
                            {
                                valorPerformance = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                            }
                            break;
                        case (int)CalculaBenchmarkNegativo.CalculaPartePositiva:
                            cotaCorrigida = cotaBase * fatorJuros;
                            if (cotaAtual > cotaCorrigida)
                            {
                                valorPerformance = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                            }
                            break;
                        case (byte)CalculaBenchmarkNegativo.CalculaGanhoTotalComRecalculo:
                            cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                            if (baseApuracao == (byte)BaseApuracaoPerformance.CotaBruta_Dia)
                            {
                                if (cotaAtual > cotaBase)
                                {
                                    decimal valorPerformance1 = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                                    cotaCorrigida = cotaBase * fatorJuros;
                                    decimal valorPerformance2 = (cotaAtual - cotaCorrigida) * quantidadeCotas;
                                    valorPerformance = Math.Min(valorPerformance1, valorPerformance2);
                                }
                            }
                            else
                            {
                                if (cotaAtual > cotaCorrigida)
                                {
                                    valorPerformance = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                                }
                            }
                            break;
                        case (int)CalculaBenchmarkNegativo.NaoCalculaPerformance:
                            break;
                    }
                }
                else
                {
                    decimal cotaCorrigida = cotaBase * fatorIndice * fatorJuros;
                    if (cotaAtual > cotaCorrigida)
                    {
                        valorPerformance = (cotaAtual - cotaCorrigida) * quantidadeCotas * taxaPerformance / 100M;
                    }
                }
                #endregion
            }

            return valorPerformance;
        }

        /// <summary>
        /// Retorna o valor da pfee, considerando a ponderação da quantidade de cotas passada em relação ao total de performance já calculada e o total de quantidade de cotas da carteira.
        /// </summary>
        /// <param name="idTabela"></param>
        /// <param name="idCarteira"></param>
        /// <param name="quantidadeCotas"></param>
        /// <returns></returns>
        private decimal RetornaValorPerformanceProporcional(int idTabela, int idCarteira, decimal quantidadeCotas)
        {
            decimal valorPerformanceTotal = 0;
            CalculoPerformance calculoPerformance = new CalculoPerformance();
            calculoPerformance.Query.Select(calculoPerformance.Query.ValorAcumulado.Sum());
            calculoPerformance.Query.Where(calculoPerformance.Query.IdTabela.Equal(idTabela));
            if (calculoPerformance.Query.Load())
            {
                valorPerformanceTotal = calculoPerformance.ValorAcumulado.Value;
            }

            decimal valorPerformance = 0;
            if (valorPerformanceTotal > 0)
            {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                decimal totalQuantidadeCotas = posicaoCotista.RetornaTotalCotas(idCarteira);
                valorPerformance = valorPerformanceTotal * quantidadeCotas / totalQuantidadeCotas;
            }

            return valorPerformance;
        }

        /// <summary>
        /// Checa performance fee com fim de apropriação na data, projetando os valores a pagar 
        /// e CPMF (somente se > 01/01/2008) para a data de pagamento na Liquidacao.
        /// Zera a CalculoPerformance, calculando novas datas de fim de apropriação e pagamento.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void TrataVencimentoPerformance(int idCarteira, DateTime data)
        {
            CalculoPerformanceCollection calculoPerformanceCollection = new CalculoPerformanceCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            calculoPerformanceCollection.BuscaCalculoPerformanceVencida(idCarteira, data);

            int idContaDefault = 0;
            if (calculoPerformanceCollection.Count > 0)
            {
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                //
            }

            for (int i = 0; i < calculoPerformanceCollection.Count; i++)
            {
                #region Carregamento das variáveis locais de CalculoPerformance
                CalculoPerformance calculoPerformance = calculoPerformanceCollection[i];
                int idTabela = calculoPerformance.IdTabela.Value;
                DateTime dataPagamento = calculoPerformance.DataPagamento.Value;
                decimal valorPerformance = calculoPerformance.ValorAcumulado.Value;
                decimal valorCPMF = calculoPerformance.ValorCPMFAcumulado.Value;
                #endregion

                #region Busca informações da tabela de Performance
                TabelaTaxaPerformance tabelaTaxaPerformance = new TabelaTaxaPerformance();
                tabelaTaxaPerformance.LoadByPrimaryKey(idTabela);

                int diaRenovacao = tabelaTaxaPerformance.DiaRenovacao.Value;
                int numeroMesesRenovacao = tabelaTaxaPerformance.NumeroMesesRenovacao.Value;
                int numeroDiasPagamento = tabelaTaxaPerformance.NumeroDiasPagamento.Value;
                string zeraNegativo = tabelaTaxaPerformance.ZeraPerformanceNegativa;
                int baseApuracao = tabelaTaxaPerformance.BaseApuracao.Value;
                byte tipoCalculo = tabelaTaxaPerformance.TipoCalculo.Value;
                #endregion

                #region Checa se existe dataFim para a provisão
                DateTime dataFim = new DateTime();
                bool terminoProvisao = false;
                if (tabelaTaxaPerformance.DataFim.HasValue)
                {
                    dataFim = tabelaTaxaPerformance.DataFim.Value;
                    if (dataFim <= data)
                    {
                        terminoProvisao = true;
                    }
                }
                #endregion

                if (!terminoProvisao)
                {                    
                    if (valorPerformance > 0)
                    {
                        #region Lança em Liquidacao o valor da Performance a pagar
                        string descricao = "Pagamento de Taxa de Performance";
                        Liquidacao liquidacao = liquidacaoCollection.AddNew();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataPagamento;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = valorPerformance * -1;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.IdConta = idContaDefault;
                        #endregion
                    }                    

                    #region Atualiza datas de ultimo corte, fim de apropriação e de pagamento
                    int ano = data.Year;
                    int mes = data.Month;

                    //Calcula nova data de fim de apropriação
                    DateTime dataFimApropriacao;

                    if (diaRenovacao == 0)
                    {//Se nada for especificado a respeito do dia de renovação, assume-se como 1o dia útil do mês seguinte
                        dataFimApropriacao = Calendario.RetornaPrimeiroDiaUtilMes(data, numeroMesesRenovacao,
                                                                                LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        // Se for todo dia 31 por exemplo, e mes so tiver 30 dias, deve utilizar dia 30 desse mes
                        dataFimApropriacao = new DateTime(ano, mes, 1);
                        dataFimApropriacao = dataFimApropriacao.AddMonths(numeroMesesRenovacao);
                        dataFimApropriacao = Calendario.RetornaDataValida(diaRenovacao.ToString("00") + "/" + dataFimApropriacao.ToString("MM") + "/" + dataFimApropriacao.ToString("yyyy"));
                        if (!Calendario.IsDiaUtil(dataFimApropriacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                        {
                            dataFimApropriacao = Calendario.SubtraiDiaUtil(dataFimApropriacao, 1,
                                                                    LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                    }

                    //Calcula nova data de pagamento
                    dataPagamento = Calendario.AdicionaDiaUtil(dataFimApropriacao, numeroDiasPagamento,
                                                               LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                    calculoPerformance.DataFimApropriacao = dataFimApropriacao;
                    calculoPerformance.DataPagamento = dataPagamento;

                    //ATENÇÃO, SÓ ATUALIZA DATA BASE SE PFEE > 0 (OU ENTÃO SE PERMITE RESETAR COM PFEE ZERADA)
                    PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                    posicaoCotistaCollection.Query.Select(posicaoCotistaCollection.Query.IdPosicao,
                                                          posicaoCotistaCollection.Query.DataUltimoCortePfee);
                    posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira));

                    if (tipoCalculo == (byte)TipoCalculoPerformance.PorCertificado || tipoCalculo == (byte)TipoCalculoPerformance.CotaCota)
                    {
                        if (zeraNegativo == "N")
                        {
                            posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.ValorPerformance.GreaterThan(0));
                        }
                    }

                    posicaoCotistaCollection.Query.Load();

                    bool resetaPfee = true;
                    if (tipoCalculo != (byte)TipoCalculoPerformance.PorCertificado && tipoCalculo != (byte)TipoCalculoPerformance.CotaCota)
                    {
                        if (valorPerformance < 0 && zeraNegativo == "N")
                        {
                            resetaPfee = false;
                        }
                    }

                    if (resetaPfee)
                    {
                        calculoPerformance.ValorAcumulado = 0;

                        foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                        {
                            if (baseApuracao == (int)BaseApuracaoPerformance.CotaLiquida_DiaAnterior)
                            {
                                DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
                                posicaoCotista.DataUltimoCortePfee = dataAnterior;
                            }
                            else
                            {
                                posicaoCotista.DataUltimoCortePfee = data;
                            }
                        }
                        posicaoCotistaCollection.Save();
                    }
                    #endregion
                }                
            }

            //Save das collections
            calculoPerformanceCollection.Save();
            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Retorna o valor de performance acumulado (total) para o idCarteira.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <returns></returns>        
        public decimal RetornaValorPerformance(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorAcumulado.Sum())
                 .Where(this.Query.IdCarteira == idCarteira);

            this.Query.Load();

            return this.ValorAcumulado.HasValue ? this.ValorAcumulado.Value : 0;
        }

        /// <summary>
        /// Deduz da performance projetada, a pfee resgatada no dia.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void DeduzPerformanceResgatada(int idCarteira, DateTime data)
        {
            OperacaoCotista operacaoCotista = new OperacaoCotista();
            operacaoCotista.Query.Select(operacaoCotista.Query.ValorPerformance.Sum());
            operacaoCotista.Query.Where(operacaoCotista.Query.IdCarteira.Equal(idCarteira),
                                        operacaoCotista.Query.DataOperacao.Equal(data),
                                        operacaoCotista.Query.IdCarteira.NotEqual(operacaoCotista.Query.IdCotista));
            operacaoCotista.Query.Load();

            decimal valor = 0;
            if (operacaoCotista.ValorPerformance.HasValue)
            {
                valor = operacaoCotista.ValorPerformance.Value;

                LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                liquidacaoCollection.Query.Select(liquidacaoCollection.Query.IdLiquidacao, liquidacaoCollection.Query.Valor);
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCarteira),
                                               liquidacaoCollection.Query.DataVencimento.GreaterThanOrEqual(data),
                                               liquidacaoCollection.Query.Origem.In((int)OrigemLancamentoLiquidacao.Provisao.TaxaPerformance));
                liquidacaoCollection.Query.Load();

                if (liquidacaoCollection.Count > 0)
                {
                    decimal valorLiquidacao = liquidacaoCollection[0].Valor.Value * (-1);

                    if (liquidacaoCollection[0].Valor != 0)
                    {
                        if (valor > valorLiquidacao)
                        {
                            liquidacaoCollection[0].Valor = 0;
                        }
                        else
                        {
                            liquidacaoCollection[0].Valor += valor;
                        }
                    }
                }
                liquidacaoCollection.Save();
            }
        }

    }
}
