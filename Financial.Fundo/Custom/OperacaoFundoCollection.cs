﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;
using log4net;
using System.Collections;

namespace Financial.Fundo
{
    public partial class OperacaoFundoCollection : esOperacaoFundoCollection
    {
        /// <summary>
        /// Carrega o objeto com o campo ValorBruto.
        /// Busca operações com o TipoOperacao = "Aplicacao", "AplicacaoAcoesEspecial".
        /// Filtra por: DataConversao.GreaterThan(dataConversao),
        /// DataOperacao.Equal(data).
        /// GroupBy(this.Query.DataConversao)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void BuscaAplicacaoConverter(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira, this.Query.DataConversao, this.Query.ValorBruto.Sum())
                 .Where(this.Query.DataConversao.GreaterThan(data),
                        this.Query.DataLiquidacao.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In((int)TipoOperacaoFundo.Aplicacao,
                                                   (int)TipoOperacaoFundo.AplicacaoAcoesEspecial))
                .GroupBy(this.Query.IdCarteira, this.Query.DataConversao);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de OperacaoFundoCollection.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="dataConversao"></param>  
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaAplicacao(int idCliente, DateTime dataCalculo, List<byte> listaFonte)
        {
            List<int> lstTipoOperacao = new List<int>();
            lstTipoOperacao.Add((int)TipoOperacaoFundo.Aplicacao);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.AplicacaoAcoesEspecial);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.AplicacaoCotasEspecial);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.IngressoAtivoImpactoCota);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.IngressoAtivoImpactoQtde);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.AplicacaoCotas);

            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente,
                        //this.Query.DataConversao.Equal(dataConversao),
                        //this.query.DataRegistro.Equal(dataCalculo),
                        this.Query.TipoOperacao.In(lstTipoOperacao.ToArray()),
                        this.Query.Fonte.In(listaFonte));

            this.Query.Where((this.Query.DataConversao.Equal(dataCalculo) & this.Query.DataConversao.GreaterThanOrEqual(this.Query.DataRegistro)) | (this.Query.DataRegistro.Equal(dataCalculo) & this.Query.DataConversao.LessThanOrEqual(this.Query.DataRegistro)));
            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto OperacaoFundoCollection com os campos IdCarteira, Observacao, ValorBruto.
        /// Busca operações com o TipoOperacao = "Aplicacao", "AplicacaoAcoesEspecial".
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLiquidacao"></param>        
        public void BuscaAplicacaoLiquidaDiaAberto(int idCliente, DateTime dataLiquidacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira, this.Query.Observacao, this.Query.ValorBruto, this.Query.IdConta)
                 .Where(this.Query.DataLiquidacao.Equal(dataLiquidacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.DepositoRetirada != true,
                        this.Query.TipoOperacao.In((int)TipoOperacaoFundo.Aplicacao,
                                                   (int)TipoOperacaoFundo.AplicacaoAcoesEspecial));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoFundoCollection com os campos IdCarteira, ValorBruto.Sum().
        /// Busca operações com o TipoOperacao = "Aplicacao", "AplicacaoAcoesEspecial".
        /// GroupBy(this.Query.IdCarteira).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataLiquidacao"></param>        
        public void BuscaAplicacaoLiquidaDia(int idCliente, DateTime dataLiquidacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira, this.Query.IdConta, this.Query.ValorBruto.Sum())
                 .Where(this.Query.DataLiquidacao.Equal(dataLiquidacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.DepositoRetirada == false,
                        this.Query.TipoOperacao.In((int)TipoOperacaoFundo.Aplicacao,
                                                   (int)TipoOperacaoFundo.AplicacaoAcoesEspecial))
                .GroupBy(this.Query.IdCarteira, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as operações de ComeCotas do cliente na data.        
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="data"></param>  
        public void DeletaComeCotas(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataOperacao.Equal(data),
                        this.Query.TipoOperacao.Equal((int)TipoOperacaoFundo.ComeCotas));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto OperacaoFundoCollection com os campos ValorBruto.Sum(), ValorIR.Sum(), ValorIOF.Sum(),
        /// ValorPerformance.Sum(), IdCarteira, DataLiquidacao.
        /// TipoOperacao.In(TipoOperacaoFundo.ResgateBruto,
        ///                                        TipoOperacaoFundo.ResgateLiquido,
        ///                                        TipoOperacaoFundo.ResgateCotas,
        ///                                        TipoOperacaoFundo.ResgateTotal).
        /// GroupBy(this.Query.IdCarteira, this.Query.DataLiquidacao).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataConversao"></param>        
        public void BuscaResgateProjetaLiquidacao(int idCliente, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum(), this.Query.ValorIR.Sum(), this.Query.ValorIOF.Sum(), this.Query.Quantidade.Sum(),
                         this.Query.ValorPerformance.Sum(), this.Query.IdCarteira, this.Query.DataLiquidacao, this.Query.IdConta)
                 .Where(this.Query.DataConversao.Equal(dataConversao),
                        this.Query.IdCliente == idCliente,
                        this.Query.DepositoRetirada == false,
                        this.Query.TipoOperacao.In((int)TipoOperacaoFundo.ResgateBruto,
                                                   (int)TipoOperacaoFundo.ResgateLiquido,
                                                   (int)TipoOperacaoFundo.ResgateCotas,
                                                   (int)TipoOperacaoFundo.ResgateTotal))
                .GroupBy(this.Query.IdCarteira, this.Query.DataLiquidacao, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoFundoCollection com os campos ValorBruto, ValorIR, ValorIOF, ValorPerformance, IdCarteira, DataLiquidacao, Observacao.
        /// TipoOperacao.In(TipoOperacaoFundo.ResgateBruto,
        ///                                        TipoOperacaoFundo.ResgateLiquido,
        ///                                        TipoOperacaoFundo.ResgateCotas,
        ///                                        TipoOperacaoFundo.ResgateTotal).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataConversao"></param>        
        public void BuscaResgateProjetaLiquidacaoAberto(int idCliente, DateTime dataConversao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto, this.Query.IdCarteira, this.Query.DataLiquidacao, this.Query.Quantidade, this.Query.Observacao,
                         this.Query.ValorIR, this.Query.ValorIOF, this.Query.ValorPerformance, this.Query.IdConta)
                 .Where(this.Query.DataConversao.Equal(dataConversao),
                        this.Query.IdCliente == idCliente,
                        this.Query.DepositoRetirada != true,
                        this.Query.TipoOperacao.In((int)TipoOperacaoFundo.ResgateBruto,
                                                   (int)TipoOperacaoFundo.ResgateLiquido,
                                                   (int)TipoOperacaoFundo.ResgateCotas,
                                                   (int)TipoOperacaoFundo.ResgateTotal));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoFundoCollection com todos os campos de OperacaoFundo.
        /// Filtra por:
        /// TipoOperacao.In(TipoOperacaoFundo.ResgateBruto, 
        ///                 TipoOperacaoFundo.ResgateCotas,
        ///                 TipoOperacaoFundo.ResgateCotasEspecial,
        ///                 TipoOperacaoFundo.ResgateLiquido,
        ///                 TipoOperacaoFundo.ResgateTotal)
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="dataConversao"></param>  
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaResgate(int idCliente, DateTime dataCalculo, List<byte> listaFonte)
        {
            List<int> lstTipoOperacao = new List<int>();
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateBruto);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateCotas);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateCotasEspecial);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateLiquido);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateTotal);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.RetiradaAtivoImpactoCota);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.RetiradaAtivoImpactoQtde);

            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente.Equal(idCliente),                        
                        this.Query.ExclusaoLogica.Equal("N"),
                        this.Query.TipoOperacao.In(lstTipoOperacao.ToArray()),
                        this.Query.Fonte.In(listaFonte));

            this.Query.Where((this.Query.DataConversao.Equal(dataCalculo) & this.Query.DataConversao.GreaterThanOrEqual(this.Query.DataRegistro)) | 
                (this.Query.DataRegistro.Equal(dataCalculo) & this.Query.DataConversao.LessThanOrEqual(this.Query.DataRegistro)));
            bool retorno = this.Query.Load();

            return retorno;
        }

        public void ImportaMovimentoSMA(Financial.Interfaces.Import.Fundo.SMA.MovimentoPassivo[] movimentos)
        {
            const string TABLE_OPERACAO = "OperacaoFundo";

            StringBuilder sql = new StringBuilder();

            sql.AppendLine(String.Format("SET IDENTITY_INSERT [{0}] ON", TABLE_OPERACAO));
            sql.AppendLine("SET NOCOUNT ON");

            List<string> listColumnNamesOperacao = new List<string>();
            List<string> listColumnParametersOperacao = new List<string>();

            foreach (esColumnMetadata colOperacao in this.Meta.Columns)
            {
                listColumnNamesOperacao.Add(colOperacao.Name);
                listColumnParametersOperacao.Add("@" + colOperacao.Name);
            }

            string columnNamesOperacao = String.Join(",", listColumnNamesOperacao.ToArray());
            string columnParametersOperacao = String.Join(",", listColumnParametersOperacao.ToArray());
                        
            sql.AppendLine(String.Format("INSERT INTO {0}({1}) VALUES({2})", TABLE_OPERACAO, columnNamesOperacao, columnParametersOperacao));
            sql.AppendLine(String.Format("SET IDENTITY_INSERT [{0}] OFF", TABLE_OPERACAO));

            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    
                    List<int> listaDeletar = new List<int>();                    
                    OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
                    
                    foreach (Financial.Interfaces.Import.Fundo.SMA.MovimentoPassivo movimento in movimentos)
                    {
                        OperacaoFundo operacaoFundo = new OperacaoFundo();
                        if (operacaoFundo.ImportaMovimentoSMA(movimento, true))
                        {
                            operacaoFundoCollection.AttachEntity(operacaoFundo);
                            listaDeletar.Add(movimento.IdNota);
                        }
                    }

                    //Deleta antes todas as operações
                    if (listaDeletar.Count > 0)
                    {
                        OperacaoFundoCollection operacaoFundoCollectionDeletar = new OperacaoFundoCollection();
                        operacaoFundoCollectionDeletar.Query.Select(operacaoFundoCollectionDeletar.Query.IdOperacao);
                        operacaoFundoCollectionDeletar.Query.Where(operacaoFundoCollectionDeletar.Query.IdOperacao.In(listaDeletar));
                        operacaoFundoCollectionDeletar.Query.Load();
                        operacaoFundoCollectionDeletar.MarkAllAsDeleted();
                        operacaoFundoCollectionDeletar.Save();
                    }

                    foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
                    {
                        OperacaoFundo operacaoFundoAtualizar = new OperacaoFundo();
                        if (operacaoFundoAtualizar.LoadByPrimaryKey(operacaoFundo.IdOperacao.Value))
                        {
                            operacaoFundoAtualizar.Quantidade += operacaoFundo.Quantidade.Value;
                            operacaoFundoAtualizar.RendimentoResgate += operacaoFundo.RendimentoResgate.Value;
                            operacaoFundoAtualizar.ValorBruto += operacaoFundo.ValorBruto.Value;
                            operacaoFundoAtualizar.ValorIOF += operacaoFundo.ValorIOF.Value;
                            operacaoFundoAtualizar.ValorIR += operacaoFundo.ValorIR.Value;
                            operacaoFundoAtualizar.ValorLiquido += operacaoFundo.ValorLiquido.Value;
                            operacaoFundoAtualizar.ValorPerformance += operacaoFundo.ValorPerformance.Value;
                            operacaoFundoAtualizar.VariacaoResgate += operacaoFundo.VariacaoResgate.Value;
                            operacaoFundoAtualizar.Save();
                        }
                        else
                        {
                            #region CARGA EM (NOVO) OPERACAOFUNDO
                            esParameters esParams = new esParameters();
                            foreach (string columnName in listColumnNamesOperacao)
                            {
                                esParams.Add(columnName, operacaoFundo.GetColumn(columnName));
                            }

                            //Adicionar clausula de DELETE ao SQL Fixo
                            string sqlDynamic = String.Format(sql.ToString(), operacaoFundo.IdOperacao.Value);

                            esUtility u = new esUtility();
                            u.ExecuteNonQuery(esQueryType.Text, sqlDynamic, esParams);
                            #endregion
                        }

                        #region CARGA EM DETALHERESGATEFUNDO
                        if (operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.Aplicacao)
                        {
                            DetalheResgateFundoCollection detalheResgateFundoCollectionDeletar = new DetalheResgateFundoCollection();
                            detalheResgateFundoCollectionDeletar.Query.Select(detalheResgateFundoCollectionDeletar.Query.IdOperacao,
                                                                       detalheResgateFundoCollectionDeletar.Query.IdPosicaoResgatada);
                            detalheResgateFundoCollectionDeletar.Query.Where(detalheResgateFundoCollectionDeletar.Query.IdOperacao.Equal(operacaoFundo.IdOperacao.Value),
                                                                             detalheResgateFundoCollectionDeletar.Query.IdPosicaoResgatada.Equal(operacaoFundo.IdPosicaoResgatada.Value));
                            detalheResgateFundoCollectionDeletar.Query.Load();
                            detalheResgateFundoCollectionDeletar.MarkAllAsDeleted();
                            detalheResgateFundoCollectionDeletar.Save();

                            DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
                            detalheResgateFundo.IdCarteira = operacaoFundo.IdCarteira.Value;
                            detalheResgateFundo.IdCliente = operacaoFundo.IdCliente.Value;
                            detalheResgateFundo.IdOperacao = operacaoFundo.IdOperacao.Value;
                            detalheResgateFundo.IdPosicaoResgatada = operacaoFundo.IdPosicaoResgatada.Value;
                            detalheResgateFundo.PrejuizoUsado = 0;
                            detalheResgateFundo.Quantidade = operacaoFundo.Quantidade.Value;
                            detalheResgateFundo.RendimentoResgate = operacaoFundo.RendimentoResgate.Value;
                            detalheResgateFundo.ValorBruto = operacaoFundo.ValorBruto.Value;
                            detalheResgateFundo.ValorCPMF = 0;
                            detalheResgateFundo.ValorIOF = operacaoFundo.ValorIOF.Value;
                            detalheResgateFundo.ValorIR = operacaoFundo.ValorIR.Value;
                            detalheResgateFundo.ValorLiquido = operacaoFundo.ValorLiquido.Value;
                            detalheResgateFundo.ValorPerformance = operacaoFundo.ValorPerformance.Value;
                            detalheResgateFundo.VariacaoResgate = operacaoFundo.RendimentoResgate.Value;
                            detalheResgateFundo.Save();
                        }
                        #endregion                        
                    }

                    scope.Complete();
                }
            }

        }
    }
}
