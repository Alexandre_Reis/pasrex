﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/12/2014 17:49:13
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Fundo.Enums;

namespace Financial.Fundo
{
	public partial class ParametroLiqAvancadoAnaliticoCollection : esParametroLiqAvancadoAnaliticoCollection
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="idCarteira"></param>
        /// <param name="tipoOperacao"></param>
        /// <param name="tipoData"></param>
        /// <param name="igualVigencia"></param>
        /// <returns></returns>
        public ParametroLiqAvancadoAnaliticoCollection RetornaParametrosNaDataVigente(DateTime dataReferencia, int idCarteira, int tipoOperacao, int? tipoData)
        {
            #region Busca Parametros Analiticos
            ParametroLiqAvancadoAnaliticoQuery parametrosAnaliticosQuery = new ParametroLiqAvancadoAnaliticoQuery("parametroAnalitico");
            ParametroLiqAvancadoAnaliticoCollection parametrosAnaliticosColl = new ParametroLiqAvancadoAnaliticoCollection();

            parametrosAnaliticosQuery.Where(parametrosAnaliticosQuery.IdCarteira.Equal(idCarteira)
                                            & parametrosAnaliticosQuery.TipoOperacao.Equal(tipoOperacao)
                                            & parametrosAnaliticosQuery.DataVigencia.Equal(dataReferencia));

            if (tipoData.HasValue)
                parametrosAnaliticosQuery.Where(parametrosAnaliticosQuery.TipoData.Equal(tipoData.Value));

            parametrosAnaliticosQuery.OrderBy(parametrosAnaliticosQuery.OrdemProcessamento.Ascending);

            if (parametrosAnaliticosColl.Load(parametrosAnaliticosQuery))
                return parametrosAnaliticosColl;
            else
                return new ParametroLiqAvancadoAnaliticoCollection();

            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="idCarteira"></param>
        /// <param name="tipoOperacao"></param>
        /// <returns></returns>
        public DateTime RetornaUltDataVigente(DateTime dataReferencia, int idCarteira, int tipoOperacao)
        {
            DateTime retorno = new DateTime();
            #region Descobre último cadastro realizado
            ParametroLiqAvancadoQuery parametrosOpQuery = new ParametroLiqAvancadoQuery("parametroOperacao");
            ParametroLiqAvancadoQuery parametrosConQuery = new ParametroLiqAvancadoQuery("parametroConversao");
            ParametroLiqAvancadoQuery parametrosLiqQuery = new ParametroLiqAvancadoQuery("parametroLiquidacao");
            ParametroLiqAvancadoCollection parametrosColl = new ParametroLiqAvancadoCollection();

            parametrosOpQuery.es.Distinct = true;
            parametrosOpQuery.Select(parametrosOpQuery.DataVigencia);
            parametrosOpQuery.InnerJoin(parametrosConQuery).On(parametrosConQuery.IdCarteira.Equal(parametrosOpQuery.IdCarteira)
                                                               & parametrosConQuery.TipoOperacao.Equal(parametrosOpQuery.TipoOperacao)
                                                               & parametrosConQuery.ParametroValido.Equal(parametrosOpQuery.ParametroValido)
                                                               & parametrosConQuery.TipoData.Equal((int)TipoParametroLiquidacao.DataConversao)
                                                               & parametrosConQuery.DataVigencia.Equal(parametrosOpQuery.DataVigencia));
            parametrosOpQuery.InnerJoin(parametrosLiqQuery).On(parametrosLiqQuery.IdCarteira.Equal(parametrosOpQuery.IdCarteira)
                                                               & parametrosLiqQuery.TipoOperacao.Equal(parametrosOpQuery.TipoOperacao)
                                                               & parametrosLiqQuery.ParametroValido.Equal(parametrosOpQuery.ParametroValido)
                                                               & parametrosLiqQuery.TipoData.Equal((int)TipoParametroLiquidacao.DataLiquidacao)
                                                               & parametrosLiqQuery.DataVigencia.Equal(parametrosOpQuery.DataVigencia));
            parametrosOpQuery.Where(parametrosOpQuery.IdCarteira.Equal(idCarteira)
                                  & parametrosOpQuery.DataVigencia.LessThan(dataReferencia)
                                  & parametrosOpQuery.TipoOperacao.Equal(tipoOperacao)
                                  & parametrosOpQuery.TipoData.Equal((int)TipoParametroLiquidacao.DataOperacao)
                                  & parametrosOpQuery.ParametroValido.Equal("S"));

            parametrosOpQuery.GroupBy(parametrosOpQuery.DataVigencia);
            parametrosOpQuery.OrderBy(parametrosOpQuery.DataVigencia.Descending);
            #endregion

            if (parametrosColl.Load(parametrosOpQuery))
                retorno = parametrosColl[0].DataVigencia.Value;

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="dataBaseCalculo"></param>
        /// <param name="parametrosLiqColl"></param>
        /// <param name="tipoData"></param>
        /// <param name="inc"></param>
        /// <param name="idLocalFeriado"></param>
        /// <returns></returns>
        public DateTime RetornaDataCalculada(DateTime dataReferencia, DateTime dataBaseCalculo, ParametroLiqAvancadoAnaliticoCollection parametrosLiqColl, int tipoData, int inc, int? idLocalFeriado)
        {
            int parametroUm, parametroDois;
            bool primeiraInteracao = true;
            bool usaFeriado = (idLocalFeriado.HasValue && idLocalFeriado.Value > 0);
            DateTime dataCalculada = dataBaseCalculo;

            foreach (ParametroLiqAvancadoAnalitico parametro in parametrosLiqColl)
            {
                if (!string.IsNullOrEmpty(parametro.ParametroUm))
                    parametroUm = Convert.ToInt32(parametro.ParametroUm);
                else
                    parametroUm = 0;

                if (!string.IsNullOrEmpty(parametro.ParametroDois))
                    parametroDois = Convert.ToInt32(parametro.ParametroDois);
                else
                    parametroDois = 0;

                try
                {
                    switch (parametro.IdParametroLiqAvancado)
                    {

                        case "D":
                            {
                                #region Dias Úteis
                                int pDias = Math.Abs(parametroUm );
                                bool subtrai = (parametroUm < 0);
                                if (primeiraInteracao)
                                {
                                    if (subtrai)
                                    {
                                        if (usaFeriado)
                                            dataCalculada = Calendario.SubtraiDiaUtil(dataBaseCalculo, pDias + (inc * (-1)), idLocalFeriado.Value, TipoFeriado.Outros);
                                        else
                                            dataCalculada = Calendario.SubtraiDiaUtil(dataBaseCalculo, pDias + (inc * (-1)));
                                    }
                                    else
                                    {
                                        if (usaFeriado)
                                            dataCalculada = Calendario.AdicionaDiaUtil(dataBaseCalculo, pDias + inc, idLocalFeriado.Value, TipoFeriado.Outros);
                                        else
                                            dataCalculada = Calendario.AdicionaDiaUtil(dataBaseCalculo, pDias + inc);
                                    }
                                }
                                else
                                {
                                    if (subtrai)
                                    {
                                        if (usaFeriado)
                                            dataCalculada = Calendario.SubtraiDiaUtil(dataCalculada, pDias, idLocalFeriado.Value, TipoFeriado.Outros);
                                        else
                                            dataCalculada = Calendario.SubtraiDiaUtil(dataCalculada, pDias);
                                    }
                                    else
                                    {
                                        if (usaFeriado)
                                            dataCalculada = Calendario.AdicionaDiaUtil(dataCalculada, pDias, idLocalFeriado.Value, TipoFeriado.Outros);
                                        else
                                            dataCalculada = Calendario.AdicionaDiaUtil(dataCalculada, pDias);
                                    }
                                }
                                #endregion
                                break;
                            }
                        case "C":
                            {
                                #region Dias Corridos
                                int pDias = parametroUm;
                                if (primeiraInteracao)
                                {
                                    dataCalculada = dataBaseCalculo.AddDays(pDias + inc);
                                }
                                else
                                {
                                    dataCalculada = dataCalculada.AddDays(pDias);
                                }

                                dataCalculada = this.RetornaDataUtil(dataCalculada, false, idLocalFeriado);
                                #endregion
                                break;
                            }
                        case "W":
                            {
                                #region Semanas
                                DateTime dataTemp = new DateTime();
                                int pSemanas = parametroUm;
                                int pDias = parametroDois;
                                if (primeiraInteracao)
                                {
                                    int dias = (((pSemanas + inc) * 7) + pDias - Convert.ToInt32(dataBaseCalculo.DayOfWeek));
                                    dataTemp = dataBaseCalculo.AddDays(dias);
                                }
                                else
                                {
                                    int dias = ((pSemanas * 7) + pDias - Convert.ToInt32(dataCalculada.DayOfWeek));
                                    dataTemp = dataBaseCalculo.AddDays(dias);
                                }

                                dataCalculada = this.RetornaDataUtil(dataTemp, (tipoData != (int)TipoParametroLiquidacao.DataLiquidacao), idLocalFeriado);
                                #endregion
                                break;
                            }
                        case "F":
                            {
                                #region Quinzenal
                                int pQuinzenas = parametroUm;
                                int pDias = parametroDois;
                                int meses = 0;
                                int diaQ = 30;

                                if (primeiraInteracao)
                                    pQuinzenas += inc;

                                if (pDias > 15)
                                    pDias = 15;

                                if (dataBaseCalculo.Day <= 15)
                                    diaQ = 15;

                                for (int i = 1; i <= pQuinzenas; i++)
                                {
                                    diaQ = diaQ + 15;
                                    if (diaQ == 45)
                                    {
                                        diaQ = 15;
                                        meses = 1;
                                    }
                                }
                                if (diaQ == 30)
                                    diaQ = 31;

                                if (pDias < 15)
                                    diaQ = diaQ - (15 - pDias);

                                dataCalculada = this.incMes(dataCalculada, 0, meses, diaQ, tipoData);

                                dataCalculada = this.RetornaDataUtil(dataCalculada, (tipoData != (int)TipoParametroLiquidacao.DataLiquidacao), idLocalFeriado);
                                #endregion
                                break;
                            }
                        case "M":
                            {
                                #region Meses
                                int pMeses = parametroUm;
                                int pDia = parametroDois;

                                if (primeiraInteracao)
                                {
                                    pMeses += inc;
                                    dataCalculada = this.incMes(dataBaseCalculo, 0, pMeses, pDia, tipoData);
                                }
                                else
                                {
                                    dataCalculada = this.incMes(dataCalculada, 0, pMeses, pDia, tipoData);
                                }

                                dataCalculada = this.RetornaDataUtil(dataCalculada, (tipoData != (int)TipoParametroLiquidacao.DataLiquidacao), idLocalFeriado);
                                #endregion
                                break;
                            }
                        case "T":
                            {
                                #region Trimestre
                                int pTrimestres = parametroUm;
                                int pDias = parametroDois;
                                int novoTrim;

                                if (primeiraInteracao)
                                {
                                    pTrimestres += inc;
                                    if (dataBaseCalculo.Month % 3 == 0)
                                    {
                                        novoTrim = dataBaseCalculo.Month;
                                    }
                                    else
                                    {
                                        novoTrim = (dataBaseCalculo.Month + 3) - (dataBaseCalculo.Month % 3);
                                    }
                                }
                                else
                                {
                                    if ((int)dataBaseCalculo.Month % 3 == 0)
                                    {
                                        novoTrim = dataCalculada.Month;
                                    }
                                    else
                                    {
                                        novoTrim = (dataCalculada.Month + 3) - (dataCalculada.Month % 3);
                                    }
                                }
                                dataCalculada = this.incMes(dataBaseCalculo, 0, (novoTrim - dataBaseCalculo.Month) + pTrimestres * 3, pDias, tipoData);

                                dataCalculada = this.RetornaDataUtil(dataCalculada, (tipoData != (int)TipoParametroLiquidacao.DataLiquidacao), idLocalFeriado);
                                #endregion
                                break;
                            }
                        case "S":
                            {
                                #region Semestre
                                int pSemestres = parametroUm;
                                int pDias = parametroDois;
                                int novoSemestre;
                                int novoMes;
                                int novoDia;
                                int novoAno = (int)dataBaseCalculo.Year;
                                if (primeiraInteracao)
                                {
                                    pSemestres += inc;
                                    if (dataBaseCalculo.Month % 6 == 0)
                                    {
                                        novoSemestre = dataBaseCalculo.Month;
                                    }
                                    else
                                    {
                                        novoSemestre = (dataBaseCalculo.Month + 6) - (dataBaseCalculo.Month % 6);
                                    }
                                }
                                else
                                {
                                    if ((int)dataBaseCalculo.Month % 6 == 0)
                                    {
                                        novoSemestre = dataCalculada.Month;
                                    }
                                    else
                                    {
                                        novoSemestre = (dataCalculada.Month + 6) - (dataCalculada.Month % 6);
                                    }
                                }

                                novoMes = novoSemestre + pSemestres * 6;

                                if (novoMes > 12)
                                {
                                    novoMes = novoMes - 12;
                                    novoAno = novoAno + 1;
                                }

                                #region populaLista
                                List<int> meses30Dias = new List<int>();
                                meses30Dias.Add(4);
                                meses30Dias.Add(6);
                                meses30Dias.Add(9);
                                meses30Dias.Add(11);
                                #endregion

                                if (meses30Dias.Contains(novoMes) && pDias == 31)
                                    novoDia = 30;
                                else
                                    novoDia = pDias;

                                dataCalculada = new DateTime(novoAno, novoMes, novoDia);
                                dataCalculada = this.RetornaDataUtil(dataCalculada, (tipoData != (int)TipoParametroLiquidacao.DataLiquidacao), idLocalFeriado);

                                #endregion
                                break;
                            }
                        case "Y"://Rever
                            {
                                #region Anual
                                int pAnos = parametroUm;
                                int pDia = parametroDois;

                                DateTime dataAux = new DateTime(dataReferencia.Year, 1, 1);
                                if (primeiraInteracao)
                                    pAnos += inc;

                                dataAux.AddYears(pAnos);

                                if (usaFeriado)
                                    dataCalculada = Calendario.AdicionaDiaUtil(dataAux, pDia, idLocalFeriado.Value, TipoFeriado.Outros);
                                else
                                    dataCalculada = Calendario.AdicionaDiaUtil(dataAux, pDia);

                                #endregion
                                break;
                            }                
                        default:
                            {
                                throw new Exception("Parâmetro não implementado");
                            }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro com o parâmetro: " + parametro.ParametroComposto);
                }

                primeiraInteracao = false;
            }

            if (dataCalculada < dataReferencia)
            {
                inc += 1;

                return RetornaDataCalculada(dataReferencia, dataReferencia, parametrosLiqColl, tipoData, inc, idLocalFeriado);
            }

            return dataCalculada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataReferencia"></param>
        /// <param name="anos"></param>
        /// <param name="meses"></param>
        /// <param name="dia"></param>
        /// <param name="tipoData"></param>
        /// <returns></returns>
        private DateTime incMes(DateTime DataReferencia, int anos, int meses, int dia, int tipoData)
        {
            int novoDia, novoMes, novoAno;
            DateTime dataRetorno = DataReferencia;

            #region populaLista
            List<int> meses30Dias = new List<int>();
            meses30Dias.Add(4);
            meses30Dias.Add(6);
            meses30Dias.Add(9);
            meses30Dias.Add(11);
            #endregion

            dataRetorno = dataRetorno.AddYears(anos);
            dataRetorno = dataRetorno.AddMonths(meses);
            novoMes = dataRetorno.Month;
            novoAno = dataRetorno.Year;
            novoDia = dia;
            if (novoMes == 2)
            {
                if (dia > 28 && !DateTime.IsLeapYear(novoAno))
                    novoDia = 28;
                else if (dia > 29 && DateTime.IsLeapYear(novoAno))
                    novoDia = 29;
            }
            else if (meses30Dias.Contains(novoMes) && dia == 31)
            {
                novoDia = 30;
            }

            dataRetorno = new DateTime(novoAno, novoMes, novoDia);

            return dataRetorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="anterior"></param>
        /// <param name="idLocalFeriado"></param>
        /// <returns></returns>
        private DateTime RetornaDataUtil(DateTime data, bool anterior, int? idLocalFeriado)
        {
            bool usaFeriado = (idLocalFeriado.HasValue && idLocalFeriado.Value > 0);

            if (anterior)
            {
                if (usaFeriado)
                {
                    data = Calendario.AdicionaDiaUtil(data, 1, idLocalFeriado.Value, TipoFeriado.Outros);
                    data = Calendario.SubtraiDiaUtil(data, 1, idLocalFeriado.Value, TipoFeriado.Outros);
                        
                }
                else
                {
                    data = Calendario.AdicionaDiaUtil(data, 1);
                    data = Calendario.SubtraiDiaUtil(data, 1);
                }
            }
            else
            {
                if (usaFeriado)
                {
                    data = Calendario.SubtraiDiaUtil(data, 1, idLocalFeriado.Value, TipoFeriado.Outros);
                    data = Calendario.AdicionaDiaUtil(data, 1, idLocalFeriado.Value, TipoFeriado.Outros);
                }
                else
                {
                    data = Calendario.SubtraiDiaUtil(data, 1);
                    data = Calendario.AdicionaDiaUtil(data, 1);
                }
            }
            return data;
        }
	}
}
