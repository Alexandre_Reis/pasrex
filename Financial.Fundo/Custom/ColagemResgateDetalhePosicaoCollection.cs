/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/01/2015 11:34:56
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
	public partial class ColagemResgateDetalhePosicaoCollection : esColagemResgateDetalhePosicaoCollection
	{
        public void DeletaColagemResgateDetalhePosicaoCotista(int idCarteira, DateTime data)
        {
            #region Objetos
            ColagemResgateDetalheQuery colaResgateDtlQuery = new ColagemResgateDetalheQuery("detalhe");
            ColagemResgateDetalhePosicaoCollection colaResgateDtlPosColl = new ColagemResgateDetalhePosicaoCollection();
            ColagemResgateDetalhePosicaoQuery colaResgateDtlPosQuery = new ColagemResgateDetalhePosicaoQuery("posicao");
            #endregion

            #region Carrega Posicao dos detalhes colados
            colaResgateDtlPosQuery.InnerJoin(colaResgateDtlQuery).On(colaResgateDtlQuery.IdColagemResgateDetalhe.Equal(colaResgateDtlPosQuery.IdColagemResgateDetalhe));
            colaResgateDtlPosQuery.Where(colaResgateDtlQuery.IdCarteira.Equal(idCarteira) 
                                         & colaResgateDtlQuery.DataReferencia.Equal(data));

            if (colaResgateDtlPosColl.Load(colaResgateDtlPosQuery))
            {
                colaResgateDtlPosColl.MarkAllAsDeleted();
                colaResgateDtlPosColl.Save();
            }
            #endregion
        }

        public void DeletaColagemResgateDetalhePosicaoFundo(int idCarteira, DateTime data)
        {
            #region Objetos
            ColagemResgateDetalheQuery colaResgateDtlQuery = new ColagemResgateDetalheQuery("detalhe");
            ColagemResgateDetalhePosicaoCollection colaResgateDtlPosColl = new ColagemResgateDetalhePosicaoCollection();
            ColagemResgateDetalhePosicaoQuery colaResgateDtlPosQuery = new ColagemResgateDetalhePosicaoQuery("posicao");
            #endregion

            #region Carrega Posicao dos detalhes colados
            colaResgateDtlPosQuery.InnerJoin(colaResgateDtlQuery).On(colaResgateDtlQuery.IdColagemResgateDetalhe.Equal(colaResgateDtlPosQuery.IdColagemResgateDetalhe));
            colaResgateDtlPosQuery.Where(colaResgateDtlQuery.IdCliente.Equal(idCarteira) 
                                         & colaResgateDtlQuery.DataReferencia.Equal(data));

            if (colaResgateDtlPosColl.Load(colaResgateDtlPosQuery))
            {
                colaResgateDtlPosColl.MarkAllAsDeleted();
                colaResgateDtlPosColl.Save();
            }
            #endregion
        }
	}
}
