﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.IO;
using Financial.Investidor;
using Financial.Interfaces.Import.Fundo;
using Financial.Fundo.Enums;
using Financial.Util;

namespace Financial.Fundo
{
	public partial class TabelaCargaPassivo : esTabelaCargaPassivo
	{
        /// <summary>
        /// Carrega a TabelaCargaPassivo de uma Stream de Dados Vindos da SMA
        /// </summary>
        /// <param name="sr">Stream de dados com as informações YMF</param>
        public void CarregaTabelaCargaPassivoSMA(Stream sr)
        {
            /* Armazena IdCarteira/IdCliente para poder Deletar */
            List<string> idCarteiraDeletar = new List<string>();
            List<string> idCotistaDeletar = new List<string>();
            List<DateTime> dataDeletar = new List<DateTime>();
            //
            TabelaCargaPassivoCollection tabelaCargaPassivoCollection = new TabelaCargaPassivoCollection();
            TabelaCargaPassivoCollection tabelaCargaPassivoCollectionDeletar = new TabelaCargaPassivoCollection();
            //
            TabelaCargaPassivo tabelaCargaPassivo = new TabelaCargaPassivo();
            Cliente cliente = new Cliente();
            //

            SMA sma = new SMA();
            SMA.PosicaoDetalhadaPassivo[] posicoes = sma.ImportaPosicaoDetalhadaPassivo(sr);

            //int j = 0;
            foreach (SMA.PosicaoDetalhadaPassivo posicao in posicoes)
            {
                TabelaCargaPassivo tabela = tabelaCargaPassivoCollection.AddNew();
                //
                tabela.IdCotista = posicao.CdCotista;
                tabela.IdCarteira = posicao.CdFundo;

                tabela.Data = posicao.DtCota.Value;

                // Armazena para poder deletar
                idCotistaDeletar.Add(tabela.IdCotista);
                idCarteiraDeletar.Add(tabela.IdCarteira);
                dataDeletar.Add(tabela.Data.Value);
                //
                tabela.DataAplicacao = posicao.DtMovimento;
                tabela.IdNotaOperacao = posicao.IdNota;
                tabela.QuantidadeCotas = posicao.QuantidadeCotas;
                tabela.Tipo = (byte)TipoTabelaCargaPassivo.PosicaoData;
                tabela.ValorBruto = posicao.ValorBruto;
                tabela.ValorCota = posicao.ValorCota;
                tabela.ValorIOF = posicao.ValorIOF;
                tabela.ValorIR = posicao.ValorIR;
                tabela.ValorLiquido = posicao.ValorLiquido;
                tabela.ValorPerformance = 0;
            }

            using (esTransactionScope scope = new esTransactionScope())
            {
                #region Deleta TabelaCargaPassivo por IdCarteira/idCliente
                for (int i = 0; i < dataDeletar.Count; i++)
                {
                    tabelaCargaPassivoCollectionDeletar = new TabelaCargaPassivoCollection();

                    tabelaCargaPassivoCollectionDeletar.Query
                    .Where(tabelaCargaPassivoCollectionDeletar.Query.IdCotista == idCotistaDeletar[i],
                           tabelaCargaPassivoCollectionDeletar.Query.IdCarteira == idCarteiraDeletar[i],
                           tabelaCargaPassivoCollectionDeletar.Query.Data == dataDeletar[i]);

                    tabelaCargaPassivoCollectionDeletar.Query.Load();
                    tabelaCargaPassivoCollectionDeletar.MarkAllAsDeleted();
                    tabelaCargaPassivoCollectionDeletar.Save();
                }
                #endregion

                // Salva PosicaoFundo
                tabelaCargaPassivoCollection.Save();

                scope.Complete();
            }
        }
	}
}
