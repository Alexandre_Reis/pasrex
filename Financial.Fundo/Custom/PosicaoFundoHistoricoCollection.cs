﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class PosicaoFundoHistoricoCollection : esPosicaoFundoHistoricoCollection
	{
        // Construtor
        // Cria uma nova PosicaoFundoHistoricoCollection com os dados de PosicaoFundoCollection
        // Todos os dados de PosicaoFundoCollection são copiados, adicionando a dataHistorico
        public PosicaoFundoHistoricoCollection(PosicaoFundoCollection posicaoFundoCollection, DateTime dataHistorico)
        {
            using (esTransactionScope scope = new esTransactionScope())
            {
                int max = 0;
                PosicaoFundoHistorico posicaoFundoHistoricoMax = new PosicaoFundoHistorico();
                posicaoFundoHistoricoMax.Query.Select(posicaoFundoHistoricoMax.Query.IdPosicao.Max());
                posicaoFundoHistoricoMax.Query.Load();

                if (posicaoFundoHistoricoMax.IdPosicao.HasValue)
                {
                    max = posicaoFundoHistoricoMax.IdPosicao.Value + 1;
                }
                else
                {
                    max = 1;
                }

                for (int i = 0; i < posicaoFundoCollection.Count; i++)
                {
                    PosicaoFundoHistorico p = new PosicaoFundoHistorico();

                    // Para cada Coluna de PosicaoFundo copia para PosicaoFundoHistorico
                    foreach (esColumnMetadata colPosicao in posicaoFundoCollection.es.Meta.Columns)
                    {
                        // Copia todas as colunas 
                        esColumnMetadata colPosicaoFundoHistorico = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                        if (posicaoFundoCollection[i].GetColumn(colPosicao.Name) != null)
                        {
                            p.SetColumn(colPosicaoFundoHistorico.Name, posicaoFundoCollection[i].GetColumn(colPosicao.Name));
                        }
                    }
                    
                    p.DataHistorico = dataHistorico;
                    p.IdPosicao = max;
                    
                    max += 1;

                    this.AttachEntity(p);
                }

                scope.Complete();
            }
        }

        /// <summary>
        /// Deleta posições de fundo históricas.
        /// Filtra por DataHistorico >= dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoFundoHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta posições de fundo históricas.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoFundoHistoricoDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoFundoHistoricoCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();         
        }

        /// <summary>
        /// Carrega o objeto PosicaoFundoCollection com os campos IdCarteira, Quantidade.Sum, CotaDia.Avg, CotaAplicacao.Avg, 
        /// ValorBruto.Sum, ValorLiquido.Sum.
        /// Group By IdCarteira
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoFundoAgrupado(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira,
                         this.Query.Quantidade.Sum(),
                         this.Query.CotaDia.Avg(),
                         this.Query.CotaAplicacao.Avg(),
                         this.Query.ValorBruto.Sum(),
                         this.Query.ValorLiquido.Sum(),
                         this.Query.ValorAplicacao.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico == dataHistorico)
                 .GroupBy(this.Query.IdCarteira);

            bool retorno = this.Query.Load();

            return retorno;
        }
	}
}
