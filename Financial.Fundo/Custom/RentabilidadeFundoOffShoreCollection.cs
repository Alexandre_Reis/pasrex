﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/03/2015 16:07:06
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista;
using Financial.Investidor;
using Financial.Investidor.Enums;

namespace Financial.Fundo
{
    public partial class RentabilidadeFundoOffShoreCollection : esRentabilidadeFundoOffShoreCollection
    {
        public void CalculaRentabilidadeFundoOffShore(DateTime data, int idFundoOffShore)
        {
            #region Objetos
            RentabilidadeFundoOffShoreCollection rentabilidadeFundoColl = new RentabilidadeFundoOffShoreCollection();
            PosicaoCotistaCollection posicaoColl = new PosicaoCotistaCollection();
            PosicaoCotistaAberturaCollection posicaoAberturaColl = new PosicaoCotistaAberturaCollection();
            RentabilidadeFundoOffShore rentabilidadeFundo = new RentabilidadeFundoOffShore();
            RentabilidadeFundoOffShore rentabilidadeFundoAnterior = new RentabilidadeFundoOffShore();
            HistoricoCota historicoCotaAtual = new HistoricoCota();
            HistoricoCota historicoCotaAnterior = new HistoricoCota();
            Cliente cliente = new Cliente();
            DateTime dataCotaAnterior;
            DateTime dataCotaAtual;
            #endregion

            #region Carrega Cliente
            cliente.LoadByPrimaryKey(idFundoOffShore);

            if (cliente.IdTipo.Value != TipoClienteFixo.OffShore_PJ && cliente.IdTipo.Value != TipoClienteFixo.OffShore_PF)
                return;
            #endregion

            #region Deleta rentabilidade (Caso reprocessamento)
            rentabilidadeFundoColl.Query.Where(rentabilidadeFundoColl.Query.IdFundo.Equal(idFundoOffShore)
                                           & rentabilidadeFundoColl.Query.Data.Equal(data));

            if (rentabilidadeFundoColl.Query.Load())
            {
                rentabilidadeFundoColl.MarkAllAsDeleted();
                rentabilidadeFundoColl.Save();
            }
            #endregion

            #region Historico Cota Atual/Cópia e anterior
            historicoCotaAtual.BuscaValorCotaDia(idFundoOffShore, data);

            if (historicoCotaAtual.Data.GetValueOrDefault(new DateTime()) == new DateTime())
                return;

            historicoCotaAnterior.BuscaValorCotaAnterior(idFundoOffShore, historicoCotaAtual.Data.Value.AddDays(-1));
            #endregion

            #region Datas
            dataCotaAnterior = historicoCotaAnterior.Data.GetValueOrDefault(new DateTime());
            dataCotaAtual = historicoCotaAtual.Data.Value;
            #endregion

            #region Carrega posições / Patrimonio
            posicaoColl.Query.Select(posicaoColl.Query.ValorLiquido.Sum().As("Patrimonio"));
            posicaoColl.Query.Where(posicaoColl.Query.IdCarteira.Equal(idFundoOffShore));
            posicaoColl.Query.Load();

            #region Patrimonio/rentabilidade anterior
            if (dataCotaAnterior != new DateTime())
            {
                posicaoAberturaColl.Query.Select(posicaoAberturaColl.Query.ValorLiquido.Sum().As("PatrimonioAnterior"));
                posicaoAberturaColl.Query.Where(posicaoAberturaColl.Query.IdCarteira.Equal(idFundoOffShore)
                                                & posicaoAberturaColl.Query.DataHistorico.Equal(dataCotaAnterior));
                posicaoAberturaColl.Query.Load();

                rentabilidadeFundoAnterior.Query.Where(rentabilidadeFundoAnterior.Query.Data.Equal(dataCotaAnterior)
                       & rentabilidadeFundoAnterior.Query.IdFundo.Equal(idFundoOffShore));
                rentabilidadeFundoAnterior.Query.Load();
            }
            #endregion

            decimal patrimonio = 0, patrimonioAnterior = 0;
            if (posicaoColl.Count != 0 && !decimal.TryParse((posicaoColl[0].GetColumn("Patrimonio")).ToString(), out patrimonio))
                patrimonio = 0;

            if (posicaoAberturaColl.Count != 0 && !decimal.TryParse((posicaoAberturaColl[0].GetColumn("PatrimonioAnterior")).ToString(), out patrimonioAnterior))
                patrimonioAnterior = 0;
            #endregion

            #region Carrega objeto
            rentabilidadeFundo.Patrimonio = patrimonio;
            rentabilidadeFundo.PatrimonioAnterior = patrimonioAnterior;
            rentabilidadeFundo.IdFundo = idFundoOffShore;
            rentabilidadeFundo.Data = data;
            rentabilidadeFundo.ValorCota = historicoCotaAtual.CotaAbertura;
            rentabilidadeFundo.ValorCotaAnterior = historicoCotaAnterior.CotaAbertura;

            if (historicoCotaAnterior.CotaFechamento.GetValueOrDefault(0) != 0)
                rentabilidadeFundo.RentabilidadePercentual = (historicoCotaAtual.CotaFechamento.Value / historicoCotaAnterior.CotaFechamento.Value) - 1;
            else
                rentabilidadeFundo.RentabilidadePercentual = 0;

            rentabilidadeFundo.Rentabilidade = rentabilidadeFundo.RentabilidadePercentual.Value * rentabilidadeFundo.PatrimonioAnterior.Value;
            #endregion

            #region Rentabilidade Acumulada
            bool mesImplantacao = new DateTime(data.Year, data.Month, 1) == new DateTime(cliente.DataImplantacao.Value.Year, cliente.DataImplantacao.Value.Month, 1);

            if (mesImplantacao || data.Month == 1) // Mês de implantação do cliente, ou mês de Janeiro.           
                rentabilidadeFundo.RentabilidadeAcumuluda = 0;
            else
                rentabilidadeFundo.RentabilidadeAcumuluda = ((rentabilidadeFundo.RentabilidadePercentual.Value + 1) * (rentabilidadeFundoAnterior.RentabilidadePercentual.GetValueOrDefault(0) + 1)) - 1;
            #endregion

            rentabilidadeFundo.Save();
        }
    }
}
