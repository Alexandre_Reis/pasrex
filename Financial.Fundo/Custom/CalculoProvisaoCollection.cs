﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Fundo.Enums;

namespace Financial.Fundo {
    public partial class CalculoProvisaoCollection : esCalculoProvisaoCollection 
    {
        // Construtor
        // Cria uma nova CalculoProvisaoCollection com os dados de CalculoProvisaoHistoricoCollection
        // Todos do dados de CalculoProvisaoHistoricoCollection são copiados com excessão da dataHistorico
        public CalculoProvisaoCollection(CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection) {
            for (int i = 0; i < calculoProvisaoHistoricoCollection.Count; i++) {
                //
                CalculoProvisao p = new CalculoProvisao();

                // Para cada Coluna de CalculoProvisaoHistorico copia para CalculoProvisao
                foreach (esColumnMetadata colPosicaoHistorico in calculoProvisaoHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != CalculoProvisaoHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colCalculoProvisao = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (calculoProvisaoHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
                            p.SetColumn(colCalculoProvisao.Name, calculoProvisaoHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }
        
        /// <summary>
        /// Carrega o objeto CalculoProvisaoCollection com todos os campos de CalculoProvisao.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="dataFimApropriacao">filtra provisões já vencidas (inclusive vcto em dia não útil)</param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaCalculoProvisaoVencidas(int idCarteira, DateTime dataFimApropriacao) 
        {            
            CalculoProvisaoQuery calculoProvisaoQuery = new CalculoProvisaoQuery("C");
            TabelaProvisaoQuery tabelaProvisaoQuery = new TabelaProvisaoQuery("T");

            calculoProvisaoQuery.Where(calculoProvisaoQuery.IdCarteira.Equal(idCarteira) &
                                           (calculoProvisaoQuery.DataFimApropriacao.LessThanOrEqual(dataFimApropriacao) &
                                            calculoProvisaoQuery.ValorAcumulado.NotEqual(0) &
                                            tabelaProvisaoQuery.TipoCalculo.Equal((byte)TipoCalculoProvisao.Provisao)) |
                                           (calculoProvisaoQuery.DataFimApropriacao.Equal(dataFimApropriacao) &
                                            tabelaProvisaoQuery.TipoCalculo.Equal((byte)TipoCalculoProvisao.Diferido) &
                                            (tabelaProvisaoQuery.DataFim.GreaterThanOrEqual(dataFimApropriacao) | tabelaProvisaoQuery.DataFim.IsNull()))
                                       );
            calculoProvisaoQuery.InnerJoin(tabelaProvisaoQuery).On(calculoProvisaoQuery.IdTabela == tabelaProvisaoQuery.IdTabela);

            bool retorno = this.Load(calculoProvisaoQuery);

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto CalculoProvisaoCollection com todos os campos de CalculoProvisao.
        /// </summary>
        /// <param name="idCarteira"></param>
        public void BuscaCalculoProvisaoCompleta(int idCarteira) 
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCarteira == idCarteira);
            this.Query.Load();
        }

        /// <summary>
        /// Deleta todos os registros de cálculo da carteira passada.
        /// </summary>
        /// <param name="idCarteira"></param>
        public void DeletaCalculoProvisao(int idCarteira) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.IdTabela)
                 .Where(this.query.IdCarteira == idCarteira);
            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Insere novas registros na CalculoProvisao a partir do histórico
        /// (CalculoProvisaoHistorico) da data informada.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void InsereCalculoProvisaoHistorico(int idCarteira, DateTime data) 
        {
            this.QueryReset();
            CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
            calculoProvisaoHistoricoCollection.BuscaCalculoProvisaoHistoricoCompleta(idCarteira, data);
            for (int i = 0; i < calculoProvisaoHistoricoCollection.Count; i++) {
                CalculoProvisaoHistorico calculoProvisaoHistorico = calculoProvisaoHistoricoCollection[i];

                // Copia para CalculoProvisao
                CalculoProvisao calculoProvisao = new CalculoProvisao();
                calculoProvisao.IdTabela = calculoProvisaoHistorico.IdTabela;
                calculoProvisao.IdCarteira = calculoProvisaoHistorico.IdCarteira;
                calculoProvisao.ValorDia = calculoProvisaoHistorico.ValorDia;
                calculoProvisao.ValorAcumulado = calculoProvisaoHistorico.ValorAcumulado;
                calculoProvisao.DataFimApropriacao = calculoProvisaoHistorico.DataFimApropriacao;
                calculoProvisao.DataPagamento = calculoProvisaoHistorico.DataPagamento;
                calculoProvisao.ValorCPMFDia = calculoProvisaoHistorico.ValorCPMFDia;
                calculoProvisao.ValorCPMFAcumulado = calculoProvisaoHistorico.ValorCPMFAcumulado;
                //
                this.AttachEntity(calculoProvisao);
            }

            this.Save();
        }

    }
}
