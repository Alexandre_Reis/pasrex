﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Exceptions;
using log4net;
using Financial.Fundo.Enums;
using Financial.Util;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.Investidor.Enums;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.ContaCorrente;

namespace Financial.Fundo
{
    public class MatrizAtivos
    {
        public enum ListaTipoFixa
        {
            Acao = 1,
            TermoBolsa = 3,
            FuturoBMF = 10,            
            RendaFixa = 20,
            CotaInvestimento = 30,
            Caixa = 50
        }

        private class EstruturaAtivos
        {
            private byte tipoMercado;

            public byte TipoMercado
            {
                get { return tipoMercado; }
                set { tipoMercado = value; }
            }

            private string idAtivo;

            public string IdAtivo
            {
                get { return idAtivo; }
                set { idAtivo = value; }
            }

            private int indiceColuna;

            public int IndiceColuna
            {
                get { return indiceColuna; }
                set { indiceColuna = value; }
            }
        }
        

        /// <summary>
        /// Preenche a estrutura carteira com todas as informações de posições dos diversos mercados.
        /// </summary>
        /// <returns></returns>
        private List<EstruturaAtivos> RetornaListaEstruturaAtivos(List<int> idClientes)
        {
            List<EstruturaAtivos> lista = new List<EstruturaAtivos>();            

            int indice = 2;

            #region Acoes e Opcoes Bovespa
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            posicaoBolsaQuery.Select(posicaoBolsaQuery.CdAtivoBolsa);
            //
            posicaoBolsaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoBolsaQuery.IdCliente);
            //
            posicaoBolsaQuery.Where(posicaoBolsaQuery.Quantidade.NotEqual(0) &&
                                    clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &&
                                    clienteQuery.IdCliente.In(idClientes)
                                    );
            posicaoBolsaQuery.es.Distinct = true;

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;

                EstruturaAtivos estruturaAtivos = new EstruturaAtivos();
                estruturaAtivos.IdAtivo = cdAtivoBolsa;                
                estruturaAtivos.TipoMercado = (byte)ListaTipoFixa.Acao; //Jogando para acao as opcoes tb
                estruturaAtivos.IndiceColuna = indice;
                lista.Add(estruturaAtivos);

                indice += 1;
            }
            #endregion

            #region Termos Bovespa
            PosicaoTermoBolsaQuery posicaoTermoBolsaQuery = new PosicaoTermoBolsaQuery("P");
            clienteQuery = new ClienteQuery("C");
            posicaoTermoBolsaQuery.Select(posicaoTermoBolsaQuery.CdAtivoBolsa);
            posicaoTermoBolsaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoTermoBolsaQuery.IdCliente);
            //
            posicaoTermoBolsaQuery.Where(posicaoTermoBolsaQuery.Quantidade.NotEqual(0) &&
                                         clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &&
                                         clienteQuery.IdCliente.In(idClientes)
                                         );
            //
            posicaoTermoBolsaQuery.es.Distinct = true;

            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            posicaoTermoBolsaCollection.Load(posicaoTermoBolsaQuery);

            foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;

                EstruturaAtivos estruturaAtivos = new EstruturaAtivos();
                estruturaAtivos.IdAtivo = cdAtivoBolsa;
                estruturaAtivos.TipoMercado = (byte)ListaTipoFixa.TermoBolsa;
                estruturaAtivos.IndiceColuna = indice;
                lista.Add(estruturaAtivos);

                indice += 1;
            }
            #endregion

            #region Futuros e Opcoes BMF
            PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("P");
            clienteQuery = new ClienteQuery("C");
            posicaoBMFQuery.Select(posicaoBMFQuery.CdAtivoBMF, posicaoBMFQuery.Serie);
            posicaoBMFQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoBMFQuery.IdCliente);
            //
            posicaoBMFQuery.Where(posicaoBMFQuery.Quantidade.NotEqual(0) &&
                                  clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &&
                                  clienteQuery.IdCliente.In(idClientes)
                                  );

            posicaoBMFQuery.es.Distinct = true;

            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();
            posicaoBMFCollection.Load(posicaoBMFQuery);

            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF;
                string serie = posicaoBMF.Serie;

                EstruturaAtivos estruturaAtivos = new EstruturaAtivos();
                estruturaAtivos.IdAtivo = cdAtivoBMF + serie;
                estruturaAtivos.TipoMercado = (byte)ListaTipoFixa.FuturoBMF; //Jogando para futuro BMF opcoes tb
                estruturaAtivos.IndiceColuna = indice;
                lista.Add(estruturaAtivos);

                indice += 1;
            }
            #endregion

            #region Renda Fixa
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            clienteQuery = new ClienteQuery("C");
            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoRendaFixaQuery.IdCliente);
            //
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.Quantidade.NotEqual(0) &&
                                        clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &&
                                        clienteQuery.IdCliente.In(idClientes)
                                        );

            posicaoRendaFixaQuery.es.Distinct = true;

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;

                EstruturaAtivos estruturaAtivos = new EstruturaAtivos();
                estruturaAtivos.IdAtivo = idTitulo.ToString();
                estruturaAtivos.TipoMercado = (byte)ListaTipoFixa.RendaFixa;
                estruturaAtivos.IndiceColuna = indice;
                lista.Add(estruturaAtivos);

                indice += 1;
            }
            #endregion

            #region Cotas de Investimento
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
            clienteQuery = new ClienteQuery("C");
            posicaoFundoQuery.Select(posicaoFundoQuery.IdCarteira);
            posicaoFundoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == posicaoFundoQuery.IdCliente);
            //
            posicaoFundoQuery.Where(posicaoFundoQuery.Quantidade.NotEqual(0) &&
                                    clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &&
                                    clienteQuery.IdCliente.In(idClientes)
                                    );

            posicaoFundoQuery.es.Distinct = true;

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.Load(posicaoFundoQuery);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                int idCarteira = posicaoFundo.IdCarteira.Value;

                EstruturaAtivos estruturaAtivos = new EstruturaAtivos();
                estruturaAtivos.IdAtivo = idCarteira.ToString();
                estruturaAtivos.TipoMercado = (byte)ListaTipoFixa.CotaInvestimento;
                estruturaAtivos.IndiceColuna = indice;
                lista.Add(estruturaAtivos);

                indice += 1;
            }
            #endregion
            
            #region Caixa
            EstruturaAtivos estruturaAtivosCC = new EstruturaAtivos();
            estruturaAtivosCC.IdAtivo = "C/C";
            estruturaAtivosCC.TipoMercado = (byte)ListaTipoFixa.Caixa;
            estruturaAtivosCC.IndiceColuna = indice;
            lista.Add(estruturaAtivosCC);
            #endregion

            return lista;
        }

        /// <summary>
        /// Retorna uma matriz completa com o produto cartesiano entre ativos e clientes.
        /// </summary>
        /// <returns></returns>
        public DataTable RetornaMatrizAtivos(DateTime data, List<int> idClientes)
        {
            DataTable dt = new DataTable();

            List<EstruturaAtivos> listaEstruturaAtivos = RetornaListaEstruturaAtivos(idClientes);

            #region Monta o esqueleto da tabela
            dt.Columns.Add("Carteira", typeof(string)); //Nome da carteira
            dt.Columns.Add("Data Posição", typeof(string)); //Data dia da carteira

            foreach (EstruturaAtivos estruturaAtivos in listaEstruturaAtivos) {
                if (estruturaAtivos.TipoMercado == (byte)ListaTipoFixa.Acao) {
                    //if (!dt.Columns.Contains(estruturaAtivos.IdAtivo)) {
                        dt.Columns.Add(estruturaAtivos.IdAtivo, typeof(decimal));
                    //}
                }
                else if (estruturaAtivos.TipoMercado == (byte)ListaTipoFixa.TermoBolsa) {
                    AtivoBolsa ativoBolsa = new AtivoBolsa();                    
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(ativoBolsa.Query.DataVencimento);
                    ativoBolsa.LoadByPrimaryKey(campos, estruturaAtivos.IdAtivo);
                    DateTime dataVencimento = ativoBolsa.DataVencimento.Value;

                    string cdAtivoTermo = AtivoBolsa.RetornaCdAtivoBolsaAcao(estruturaAtivos.IdAtivo) + "T - " + dataVencimento.ToShortDateString();

                    //if (!dt.Columns.Contains(cdAtivoTermo)) {
                        dt.Columns.Add(cdAtivoTermo, typeof(decimal));
                    //}
                }
                else if (estruturaAtivos.TipoMercado == (byte)ListaTipoFixa.FuturoBMF) {
                    //if (!dt.Columns.Contains(estruturaAtivos.IdAtivo)) {
                        dt.Columns.Add(estruturaAtivos.IdAtivo, typeof(decimal));
                    //}
                }
                else if (estruturaAtivos.TipoMercado == (byte)ListaTipoFixa.RendaFixa) {
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();                    
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(tituloRendaFixa.Query.Descricao);
                    campos.Add(tituloRendaFixa.Query.DataVencimento);
                    tituloRendaFixa.LoadByPrimaryKey(campos, Convert.ToInt32(estruturaAtivos.IdAtivo));

                    string descricao = tituloRendaFixa.Descricao.Trim() + " - " + tituloRendaFixa.DataVencimento.Value.ToShortDateString();

                    if (!dt.Columns.Contains(descricao)) {
                        dt.Columns.Add(descricao, typeof(decimal));
                    }
                }
                else if (estruturaAtivos.TipoMercado == (byte)ListaTipoFixa.CotaInvestimento) {
                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.Apelido);
                    carteira.LoadByPrimaryKey(campos, Convert.ToInt32(estruturaAtivos.IdAtivo));
                    string apelido = carteira.Apelido.Trim();

                    if (apelido.Length > 22) {
                        apelido = apelido.Substring(0, 20);
                    }

                    //if (!dt.Columns.Contains(apelido)) {
                        dt.Columns.Add(apelido, typeof(decimal));
                    //}
                }
                else if (estruturaAtivos.TipoMercado == (byte)ListaTipoFixa.Caixa) {
                    //if (!dt.Columns.Contains(estruturaAtivos.IdAtivo)) {
                        dt.Columns.Add(estruturaAtivos.IdAtivo, typeof(decimal));
                    //}
                }
            }
            #endregion

            ClienteCollection clienteCollection = new ClienteCollection();
            clienteCollection.Query.Select(clienteCollection.Query.IdCliente, clienteCollection.Query.Apelido, clienteCollection.Query.DataDia);
            
            clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo == (byte)StatusAtivoCliente.Ativo &&
                                          clienteCollection.Query.TipoControle.NotIn((byte)TipoControleCliente.ApenasCotacao, (byte)TipoControleCliente.Cotista) &&
                                          clienteCollection.Query.IdCliente.In(idClientes)
                                         );
            clienteCollection.Query.Load();

            foreach (Cliente cliente in clienteCollection)
            {
                int idCliente = cliente.IdCliente.Value;
                string apelido = cliente.Apelido;
                DateTime dataDia = cliente.DataDia.Value;
                
                DataRow dr = dt.NewRow();

                dr[0] = apelido;
                dr[1] = dataDia.ToShortDateString();

                foreach (EstruturaAtivos estruturaAtivos in listaEstruturaAtivos)
                {
                    string idAtivo = estruturaAtivos.IdAtivo;
                    byte tipoMercado = estruturaAtivos.TipoMercado;
                    int indiceColuna = estruturaAtivos.IndiceColuna;

                    if (tipoMercado == (byte)ListaTipoFixa.Acao)
                    {
                        PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                        decimal quantidade = posicaoBolsa.RetornaQuantidadeAtivo(idCliente, idAtivo);
                        dr[indiceColuna] = quantidade;
                    }
                    else if (tipoMercado == (byte)ListaTipoFixa.TermoBolsa)
                    {
                        PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                        decimal quantidade = posicaoTermoBolsa.RetornaQuantidadeAtivo(idCliente, idAtivo);
                        dr[indiceColuna] = quantidade;
                    }
                    else if (tipoMercado == (byte)ListaTipoFixa.FuturoBMF)
                    {
                        PosicaoBMF posicaoBMF = new PosicaoBMF();
                        string cdAtivoBMF = idAtivo.Substring(0, 3);
                        string serie = idAtivo.Replace(cdAtivoBMF, "");
                        decimal quantidade = posicaoBMF.RetornaQuantidadeAtivo(idCliente, cdAtivoBMF, serie);
                        dr[indiceColuna] = quantidade;
                    }
                    else if (tipoMercado == (byte)ListaTipoFixa.RendaFixa)
                    {
                        PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                        decimal quantidade = posicaoRendaFixa.RetornaQuantidadeAtivo(idCliente, Convert.ToInt32(idAtivo));
                        dr[indiceColuna] = quantidade;
                    }
                    else if (tipoMercado == (byte)ListaTipoFixa.CotaInvestimento)
                    {
                        PosicaoFundo posicaoFundo = new PosicaoFundo();
                        decimal quantidade = Utilitario.Truncate(posicaoFundo.RetornaQuantidadeAtivo(idCliente, Convert.ToInt32(idAtivo)), 2);
                        dr[indiceColuna] = quantidade;
                    }
                    else if (tipoMercado == (byte)ListaTipoFixa.Caixa)
                    {
                        SaldoCaixa saldoCaixa = new SaldoCaixa();
                        saldoCaixa.BuscaSaldoCaixa(idCliente, dataDia);

                        dr[indiceColuna] = 0;
                        if (saldoCaixa.SaldoFechamento.HasValue)
                        {
                            dr[indiceColuna] = saldoCaixa.SaldoFechamento.Value;
                        }
                        else
                        {
                            DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataDia, 1);

                            saldoCaixa = new SaldoCaixa();
                            saldoCaixa.BuscaSaldoCaixa(idCliente, dataAnterior);

                            if (saldoCaixa.SaldoFechamento.HasValue)
                            {
                                dr[indiceColuna] = saldoCaixa.SaldoFechamento.Value;
                            }
                        }
                    }
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }
    }
}
