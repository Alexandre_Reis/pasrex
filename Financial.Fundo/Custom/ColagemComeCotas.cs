/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/01/2015 16:27:59
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista;

namespace Financial.Fundo
{
    public partial class ColagemComeCotas : esColagemComeCotas
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="novoStatus"></param>
        public void MudaFlagProcessado(int idCarteira, DateTime data, string tipoRegistro, string novoStatus)
        {
            ColagemComeCotasCollection colagemComeCotasColl = new ColagemComeCotasCollection();
            colagemComeCotasColl.Query.Where(colagemComeCotasColl.Query.IdCarteira.Equal(idCarteira) | colagemComeCotasColl.Query.IdCliente.Equal(idCarteira));
            colagemComeCotasColl.Query.Where(colagemComeCotasColl.Query.DataReferencia.Equal(data));

            if (!string.IsNullOrEmpty(tipoRegistro))
                colagemComeCotasColl.Query.Where(colagemComeCotasColl.Query.TipoRegistro.Equal(tipoRegistro));

            if (colagemComeCotasColl.Query.Load())
            {
                foreach (ColagemComeCotas colagem in colagemComeCotasColl)
                {
                    colagem.Processado = novoStatus;
                }

                colagemComeCotasColl.Save();
            }
        }

        public bool VerificaSeExisteValoresColadosComeCotas(int idCarteira, DateTime data)
        {
            bool passivo = VerificaSeExisteValoresColadosComeCotasPassivo(idCarteira, data);
            bool ativo = VerificaSeExisteValoresColadosComeCotasAtivo(idCarteira, data);

            return (passivo || ativo);
        }

        public bool VerificaSeExisteValoresColadosComeCotasPassivo(int idCarteira, DateTime data)
        {
            ColagemComeCotasCollection colagemComeCotasColl = new ColagemComeCotasCollection();
            colagemComeCotasColl.Query.Where((colagemComeCotasColl.Query.TipoRegistro.Equal("P") & colagemComeCotasColl.Query.IdCarteira.Equal(idCarteira)));
            colagemComeCotasColl.Query.Where(colagemComeCotasColl.Query.DataReferencia.Equal(data));

            return colagemComeCotasColl.Query.Load();
        }

        public bool VerificaSeExisteValoresColadosComeCotasAtivo(int idCarteira, DateTime data)
        {
            ColagemComeCotasCollection colagemComeCotasColl = new ColagemComeCotasCollection();
            colagemComeCotasColl.Query.Where((colagemComeCotasColl.Query.TipoRegistro.Equal("A") & colagemComeCotasColl.Query.IdCliente.Equal(idCarteira)));
            colagemComeCotasColl.Query.Where(colagemComeCotasColl.Query.DataReferencia.Equal(data));

            return colagemComeCotasColl.Query.Load();
        }

        #region Parte fundos
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="colagemComeCotasColl"></param>
        /// <param name="existemDetalhes"></param>
        public void ColaValoresFundo(int idCarteira, DateTime data, ColagemComeCotasCollection colagemComeCotasColl, bool existemDetalhes)
        {
            List<PosicaoFundo> lstPosicaoFundo = new List<PosicaoFundo>();
            PosicaoFundoCollection posicaoFundoColl = new PosicaoFundoCollection();
            ColagemComeCotasPosFundoCollection colagemComeCotasPosFundoColl = new ColagemComeCotasPosFundoCollection();

            posicaoFundoColl.Query.Where(posicaoFundoColl.Query.IdCliente.Equal(idCarteira));

            if (posicaoFundoColl.Query.Load())
            {
                lstPosicaoFundo = (List<PosicaoFundo>)posicaoFundoColl;

                foreach (ColagemComeCotas colagemFundo in colagemComeCotasColl)
                {
                    DateTime dataConversao;
                    if (colagemFundo.DataConversao != null && colagemFundo.DataConversao.HasValue)
                        dataConversao = colagemFundo.DataConversao.Value;
                    else
                        dataConversao = data;

                    List<PosicaoFundo> lstPosicaoFundoChaveada = new List<PosicaoFundo>();
                    if (existemDetalhes)
                        lstPosicaoFundoChaveada = lstPosicaoFundo.FindAll(delegate(PosicaoFundo x) { return x.IdCliente == colagemFundo.IdCliente && x.IdCarteira == colagemFundo.IdCarteira && x.DataConversao == dataConversao; });
                    else
                        lstPosicaoFundoChaveada = lstPosicaoFundo.FindAll(delegate(PosicaoFundo x) { return x.IdCliente == colagemFundo.IdCliente && x.IdCarteira == colagemFundo.IdCarteira; });

                    if (lstPosicaoFundoChaveada.Count == 0)
                        continue;

                    decimal quantidadeTotalOriginal = 0;
                    foreach (PosicaoFundo posicaoFundo in lstPosicaoFundoChaveada)
                    {
                        quantidadeTotalOriginal += posicaoFundo.Quantidade.Value;
                    }

                    foreach (PosicaoFundo posicaoFundo in lstPosicaoFundoChaveada)
                    {
                        decimal participacao = (posicaoFundo.Quantidade.Value / quantidadeTotalOriginal);

                        ColagemComeCotasPosFundo colagemComeCotasPosFundo = colagemComeCotasPosFundoColl.AddNew();
                        colagemComeCotasPosFundo.IdCarteira = colagemFundo.IdCarteira;
                        colagemComeCotasPosFundo.IdCliente = colagemFundo.IdCliente;                        
                        colagemComeCotasPosFundo.DataReferencia = data;
                        colagemComeCotasPosFundo.IdPosicaoVinculada = posicaoFundo.IdPosicao;
                        colagemComeCotasPosFundo.PrejuizoUsado = colagemFundo.PrejuizoUsado * participacao;
                        colagemComeCotasPosFundo.RendimentoComeCotas = colagemFundo.RendimentoComeCotas * participacao;
                        colagemComeCotasPosFundo.ValorBruto = colagemFundo.ValorBruto * participacao;
                        colagemComeCotasPosFundo.ValorCPMF = colagemFundo.ValorCPMF * participacao;
                        colagemComeCotasPosFundo.ValorIOF = colagemFundo.ValorIOF * participacao;
                        colagemComeCotasPosFundo.ValorIR = colagemFundo.ValorIR * participacao;
                        colagemComeCotasPosFundo.ValorLiquido = colagemFundo.ValorLiquido * participacao;
                        colagemComeCotasPosFundo.ValorPerformance = colagemFundo.ValorPerformance * participacao;
                        colagemComeCotasPosFundo.VariacaoComeCotas = colagemFundo.VariacaoComeCotas * participacao;
                        colagemComeCotasPosFundo.Quantidade = colagemFundo.Quantidade * participacao;
                        if (existemDetalhes)
                        {
                            colagemComeCotasPosFundo.DetalheImportado = "S";
                            colagemComeCotasPosFundo.DataConversao = DataConversao;
                        }
                        else
                        {
                            colagemComeCotasPosFundo.DetalheImportado = "N";
                        }
                        colagemComeCotasPosFundo.Participacao = participacao;
                    }
                }
            }
            colagemComeCotasPosFundoColl.Save();
        }
        #endregion

        #region Parte Cotistas
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="colagemComeCotasColl"></param>
        /// <param name="existemDetalhes"></param>
        public void ColaValoresCotista(int idCarteira, DateTime data, ColagemComeCotasCollection colagemComeCotasColl, bool existemDetalhes)
        {
            List<PosicaoCotista> lstPosicaoCotista = new List<PosicaoCotista>();
            PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();
            ColagemComeCotasPosCotistaCollection colagemComeCotasPosCotistaColl = new ColagemComeCotasPosCotistaCollection();

            posicaoCotistaColl.Query.Where(posicaoCotistaColl.Query.IdCarteira.Equal(idCarteira));

            if (posicaoCotistaColl.Query.Load())
            {
                lstPosicaoCotista = (List<PosicaoCotista>)posicaoCotistaColl;

                foreach (ColagemComeCotas colagemCotista in colagemComeCotasColl)
                {
                    DateTime dataConversao;
                    if (colagemCotista.DataConversao != null && colagemCotista.DataConversao.HasValue)
                        dataConversao = colagemCotista.DataConversao.Value;
                    else
                        dataConversao = data;

                    List<PosicaoCotista> lstPosicaoCotistaChaveada = new List<PosicaoCotista>();
                    if (existemDetalhes)
                        lstPosicaoCotistaChaveada = lstPosicaoCotista.FindAll(delegate(PosicaoCotista x) { return x.IdCotista == colagemCotista.IdCliente && x.IdCarteira == colagemCotista.IdCarteira && x.DataConversao == dataConversao; });
                    else
                        lstPosicaoCotistaChaveada = lstPosicaoCotista.FindAll(delegate(PosicaoCotista x) { return x.IdCotista == colagemCotista.IdCliente && x.IdCarteira == colagemCotista.IdCarteira; });

                    if (lstPosicaoCotistaChaveada.Count == 0)
                        continue;

                    decimal quantidadeTotalOriginal = 0;
                    foreach (PosicaoCotista posicaoCotista in lstPosicaoCotistaChaveada)
                    {
                        quantidadeTotalOriginal += posicaoCotista.Quantidade.Value;
                    }

                    foreach (PosicaoCotista posicaoCotista in lstPosicaoCotistaChaveada)
                    {
                        decimal participacao = (posicaoCotista.Quantidade.Value / quantidadeTotalOriginal);

                        ColagemComeCotasPosCotista colagemComeCotasPosCotista = colagemComeCotasPosCotistaColl.AddNew();
                        colagemComeCotasPosCotista.IdCarteira = colagemCotista.IdCarteira;
                        colagemComeCotasPosCotista.IdCliente = colagemCotista.IdCliente;
                        colagemComeCotasPosCotista.DataReferencia = data;
                        colagemComeCotasPosCotista.IdPosicaoVinculada = posicaoCotista.IdPosicao;
                        colagemComeCotasPosCotista.PrejuizoUsado = colagemCotista.PrejuizoUsado * participacao;
                        colagemComeCotasPosCotista.RendimentoComeCotas = colagemCotista.RendimentoComeCotas * participacao;
                        colagemComeCotasPosCotista.ValorBruto = colagemCotista.ValorBruto * participacao;
                        colagemComeCotasPosCotista.ValorCPMF = colagemCotista.ValorCPMF * participacao;
                        colagemComeCotasPosCotista.ValorIOF = colagemCotista.ValorIOF * participacao;
                        colagemComeCotasPosCotista.ValorIR = colagemCotista.ValorIR * participacao;
                        colagemComeCotasPosCotista.ValorLiquido = colagemCotista.ValorLiquido * participacao;
                        colagemComeCotasPosCotista.ValorPerformance = colagemCotista.ValorPerformance * participacao;
                        colagemComeCotasPosCotista.VariacaoComeCotas = colagemCotista.VariacaoComeCotas * participacao;
                        colagemComeCotasPosCotista.Quantidade = colagemCotista.Quantidade * participacao;
                        if (existemDetalhes)
                        {
                            colagemComeCotasPosCotista.DetalheImportado = "S";
                            colagemComeCotasPosCotista.DataConversao = DataConversao;
                        }
                        else
                        {
                            colagemComeCotasPosCotista.DetalheImportado = "N";
                        }
                        colagemComeCotasPosCotista.Participacao = participacao;
                    }
                }
            }
            colagemComeCotasPosCotistaColl.Save();
        }
        #endregion
    }
}
