/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 21/11/2014 18:38:12
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;

namespace Financial.Fundo
{
	public partial class PrazoMedioCollection : esPrazoMedioCollection
	{
        public void DeletaPrazoMedioMaiorIgual(int idCliente, DateTime data, TipoAtivoAuxiliar prazoMedioAtivo)
        {
            this.QueryReset();
            this.Query
                    .Select(this.query.IdPrazoMedio,
                            this.Query.IdCliente, 
                            this.query.DataAtual,
                            this.query.MercadoAtivo)
                    .Where(this.Query.IdCliente.Equal(idCliente) &
                           this.Query.DataAtual.GreaterThanOrEqual(data) &
                           this.query.MercadoAtivo.Equal((int)prazoMedioAtivo));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

	}
}
