﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Exceptions;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Investidor;

namespace Financial.Fundo
{
    public class CalculoTaxaFiscalizacaoCVM
    {
        /// <summary>
        /// Calcula a taxa trimestral de fiscalização da CVM, e a CPMF incidente. 
        /// Lança em Liquidacao os valores a pagar e programa o diferido para o trimestre.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void CalculaTaxaFiscalizacaoCVM(int idCarteira, DateTime data)
        {
            Carteira carteira = new Carteira();
            carteira.BuscaCalculoTaxaFiscalizacaoCVM(idCarteira);
            
            if (carteira.IsCobraTaxaFiscalizacaoCVM())
            {
                if (!carteira.TipoCVM.HasValue)
                {
                    throw new TipoCVMNaoCadastradoException("Carteira " + idCarteira + " não possui tipo CVM associado.");
                }             
                int tipoCVM = carteira.TipoCVM.Value;

                #region Calcula a data do primeiro e último dia útil do trimestre
                int primeiroMesTrimestre;
                DateTime primeiroDiaTrimestre;
                DateTime ultimoDiaTrimestre;
                if (data.Month >= 1 && data.Month <= 3)
                {
                    primeiroMesTrimestre = 01;
                }
                else if (data.Month >= 4 && data.Month <= 6)
                {
                    primeiroMesTrimestre = 04;
                }
                else if (data.Month >= 7 && data.Month <= 9)
                {
                    primeiroMesTrimestre = 07;
                }
                else 
                {
                    primeiroMesTrimestre = 10;
                }
                int anoCorrente = data.Year;
                primeiroDiaTrimestre = new DateTime(anoCorrente, primeiroMesTrimestre, 01);

                if (!Calendario.IsDiaUtil(primeiroDiaTrimestre, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    primeiroDiaTrimestre = Calendario.AdicionaDiaUtil(primeiroDiaTrimestre, 1,
                                                                      LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }

                if (data != primeiroDiaTrimestre)
                {
                    return;
                }

                ultimoDiaTrimestre = Calendario.RetornaUltimoDiaUtilMes(primeiroDiaTrimestre, 2, 
                                                                        LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                #endregion

                #region Calcula a data do primeiro e último dia útil do trimestre anterior
                DateTime primeiroDiaTrimestreAnterior;
                DateTime ultimoDiaTrimestreAnterior;

                ultimoDiaTrimestreAnterior = Calendario.SubtraiDiaUtil(primeiroDiaTrimestre, 1, 
                                                                       LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                primeiroDiaTrimestreAnterior = Calendario.RetornaPrimeiroDiaUtilMes(ultimoDiaTrimestreAnterior, -2,
                                                                       LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                #endregion

                #region Calcula o PL médio do trimestre
                
                carteira = new Carteira();                
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TipoCota);
                campos.Add(carteira.Query.DataInicioCota);
                carteira.LoadByPrimaryKey(campos, idCarteira);
                int tipoCota = carteira.TipoCota.Value;
                DateTime dataInicioCota = carteira.DataInicioCota.Value;                

                DateTime dataAuxiliar = primeiroDiaTrimestreAnterior;

                if (dataInicioCota > dataAuxiliar)
                {
                    dataAuxiliar = dataInicioCota;
                }

                int numeroDias = 0;
                decimal totalPL = 0;
                while (dataAuxiliar != ultimoDiaTrimestreAnterior)
                {                    
                    HistoricoCota historicoCota = new HistoricoCota();
                    decimal valorPL;
                    try
                    {
                        historicoCota.BuscaValorPatrimonioDia(idCarteira, dataAuxiliar);
                    }
                    catch (Exception)
                    {
                        throw new HistoricoCotaNaoCadastradoException("PL não existe na data " + dataAuxiliar + " para a carteira " + idCarteira +
                                                        ". Sugestão, desmarcar o cálculo de taxa CVM para a carteira ao rodar esta data.");
                    }
                    
                    if (tipoCota == (int)TipoCotaFundo.Abertura)
                    {
                        valorPL = historicoCota.PLAbertura.Value;
                    }
                    else {
                        valorPL = historicoCota.PLFechamento.Value;
                    }
                    totalPL += valorPL;
                    numeroDias += 1;
                    dataAuxiliar = Calendario.AdicionaDiaUtil(dataAuxiliar, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }

                decimal valorPLMedio = 0;
                if (numeroDias != 0) {                    
                    valorPLMedio = totalPL / numeroDias;
                }                
                #endregion

                if (valorPLMedio == 0)
                    return;

                #region Acha o valor da Taxa de Fiscalização, pela faixa de PL da tabela
                TabelaTaxaFiscalizacaoCVM tabelaTaxaFiscalizacaoCVM = new TabelaTaxaFiscalizacaoCVM();
                tabelaTaxaFiscalizacaoCVM.BuscaTaxaFiscalizacaoCVM(tipoCVM, valorPLMedio);

                decimal valorTaxa = tabelaTaxaFiscalizacaoCVM.ValorTaxa.Value;
                #endregion

                #region Lanca em Liquidacao o valor a pagar (e outro p/ CPMF) de Taxa de Fiscalização
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira);
                //
                DateTime dataLiquidacao = data.AddDays(10);
                if (!Calendario.IsDiaUtil(dataLiquidacao))
                {
                    dataLiquidacao = Calendario.SubtraiDiaUtil(dataLiquidacao, 1);
                }

                Liquidacao liquidacao = new Liquidacao();
                string descricao = "Taxa Fiscalização CVM";
                liquidacao.AddNew();
                liquidacao.IdCliente = idCarteira;
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataLiquidacao;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valorTaxa * -1;
                liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Provisao.TaxaFiscalizacaoCVM;
                liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdConta = idContaDefault;
                liquidacao.Save();

                //liquidacao = new Liquidacao();
                //descricao = "CPMF s/ Taxa Fiscalização CVM";
                //decimal valorCPMF = Utilitario.Truncate(valorTaxa * 0.0038M, 2);
                //liquidacao.AddNew();
                //liquidacao.IdCliente = idCarteira;
                //liquidacao.DataLancamento = data;
                //liquidacao.DataVencimento = dataPagamento;
                //liquidacao.Descricao = descricao;
                //liquidacao.Valor = valorCPMF * -1;
                //liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                //liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Provisao.CPMF;
                //liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                //liquidacao.IdConta = idContaDefault;
                //liquidacao.Save();
                #endregion
                
                #region Lanca o valor a ser diferido (taxa CVM + CPMF) na tabela de provisão
                //decimal valorDiferir = valorTaxa + valorCPMF;
                decimal valorDiferir = valorTaxa;

                #region Busca o IdCadastro em Provisao para o tipo = Taxa de Fiscalização CVM
                CadastroProvisao cadastroProvisao = new CadastroProvisao();
                int idCadastro = cadastroProvisao.RetornaIdCadastro((int)TipoCadastroProvisao.TaxaFiscalizacaoCVM);                
                #endregion

                #region Deleta qualquer provisão com o idCadastro lançado no dia
                TabelaProvisaoCollection tabelaProvisaoCollection = new TabelaProvisaoCollection();
                tabelaProvisaoCollection.DeletaTabelaProvisao(idCarteira, data, idCadastro);
                #endregion

                TabelaProvisao tabelaProvisao = new TabelaProvisao();
                tabelaProvisao.AddNew();
                tabelaProvisao.DataReferencia = data;
                tabelaProvisao.NumeroMesesRenovacao = 2;
                tabelaProvisao.NumeroDiasPagamento = 0;
                tabelaProvisao.IdCarteira = idCarteira;
                tabelaProvisao.TipoCalculo = (int)TipoCalculoProvisao.Diferido;
                tabelaProvisao.ValorTotal = valorDiferir;
                tabelaProvisao.ContagemDias = (int)ContagemDiasProvisao.Uteis;
                tabelaProvisao.IdCadastro = idCadastro;
                tabelaProvisao.DataFim = ultimoDiaTrimestre;
                tabelaProvisao.Save();
                #endregion

            }

            
        }
    }
}