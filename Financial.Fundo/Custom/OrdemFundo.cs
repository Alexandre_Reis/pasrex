using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;

namespace Financial.Fundo
{
	public partial class OrdemFundo : esOrdemFundo
	{
        /// <summary>
        /// Carrega ordens para OperacaoFundo.
        /// Altera o status de Liberado para Processado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaOrdemFundo(int idCliente, DateTime data)
        {
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            
            OrdemFundoCollection ordemFundoCollection = new OrdemFundoCollection();
            ordemFundoCollection.Query.Where(ordemFundoCollection.Query.IdCliente.Equal(idCliente),
                                             ordemFundoCollection.Query.DataOperacao.Equal(data),
                                             ordemFundoCollection.Query.Status.Equal((byte)StatusOrdemFundo.Aprovado));
            ordemFundoCollection.Query.Load();

            foreach (OrdemFundo ordemFundo in ordemFundoCollection)
            {
                OperacaoFundo operacaoFundo = operacaoFundoCollection.AddNew();
                operacaoFundo.DataAgendamento = ordemFundo.DataAgendamento;
                operacaoFundo.DataConversao = ordemFundo.DataConversao;
                operacaoFundo.DataLiquidacao = ordemFundo.DataLiquidacao;
                operacaoFundo.DataOperacao = ordemFundo.DataOperacao;
                operacaoFundo.IdCarteira = ordemFundo.IdCarteira;
                operacaoFundo.IdCliente = ordemFundo.IdCliente;
                operacaoFundo.IdFormaLiquidacao = ordemFundo.IdFormaLiquidacao;
                operacaoFundo.IdPosicaoResgatada = ordemFundo.IdPosicaoResgatada;
                operacaoFundo.Observacao = ordemFundo.Observacao;
                operacaoFundo.Quantidade = ordemFundo.Quantidade;
                operacaoFundo.TipoOperacao = ordemFundo.TipoOperacao;
                operacaoFundo.TipoResgate = ordemFundo.TipoResgate;
                operacaoFundo.ValorBruto = ordemFundo.ValorBruto;
                operacaoFundo.ValorLiquido = ordemFundo.ValorLiquido;
                operacaoFundo.IdConta = ordemFundo.IdConta;
                operacaoFundo.IdCategoriaMovimentacao = ordemFundo.IdCategoriaMovimentacao;
                operacaoFundo.Fonte = (byte)FonteOperacaoFundo.OrdemFundo;
                operacaoFundo.IdTrader = ordemFundo.IdTrader;
                
                ordemFundo.Status = (byte)StatusOrdemFundo.Processado;
            }

            ordemFundoCollection.Save();
            operacaoFundoCollection.Save();
        }
	}
}
