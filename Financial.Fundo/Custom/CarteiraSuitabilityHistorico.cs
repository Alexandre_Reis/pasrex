/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 31/08/2015 10:00:39
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Common.Enums;
using Financial.Investidor;

namespace Financial.Fundo
{
	public partial class CarteiraSuitabilityHistorico : esCarteiraSuitabilityHistorico
	{
        public static void createCarteiraSuitabilityHistorico(Carteira carteira, TipoOperacaoBanco tipoOperacaoBanco)
        {
            CarteiraSuitabilityHistorico carteiraSuitabilityHistorico = new CarteiraSuitabilityHistorico();
            carteiraSuitabilityHistorico.DataHistorico = DateTime.Now;
            carteiraSuitabilityHistorico.IdCarteira = carteira.IdCarteira;           
            carteiraSuitabilityHistorico.Tipo = tipoOperacaoBanco.ToString();

            if (carteira.PerfilRisco.HasValue)
            {
                carteiraSuitabilityHistorico.IdPerfilRisco = carteira.PerfilRisco;

                SuitabilityPerfilInvestidor suitabilityPerfilInvestidor = new SuitabilityPerfilInvestidor();
                suitabilityPerfilInvestidor.LoadByPrimaryKey(carteira.PerfilRisco.Value);
                carteiraSuitabilityHistorico.PerfilRisco = suitabilityPerfilInvestidor.Perfil;

               
            }

            carteiraSuitabilityHistorico.Save();


            
        }
	}
}
