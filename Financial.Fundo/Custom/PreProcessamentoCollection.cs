﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/01/2016 16:06:46
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Util;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Common;
using Financial.Common.Enums;
using System.Collections;
using Financial.Swap;
using System.Linq;
using Financial.RendaFixa.Enums;
using System.Threading.Tasks;

namespace Financial.Fundo
{
    public partial class PreProcessamentoCollection : esPreProcessamentoCollection
    {
        internal class CotacaoData
        {
            public int IdIndice;
            public DateTime DataCotacao;
        }

        internal class CotacaoBolsaValidar
        {
            public string CdAtivoBolsa;
            public DateTime DataCotacao;
        }

        internal class CotacaoBMFValidar
        {
            public string CdAtivoBMF;
            public string Serie;
            public DateTime DataCotacao;
        }

        internal class CotaValidar
        {
            public int IdCarteira;
            public DateTime DataCotacao;
        }

        internal class PerfilMTMValidar
        {
            public int IdPapel;
            public int? IdGrupoPerfilMTM;            
            public int IdTitulo;
            public DateTime DataSerie;
            public string TituloDescricao;            
            public string CodigoCustodia;
            public string CodigoCBLC;
            public DateTime DataVencimento;
            public DateTime DataEmissao;            
        }

        public PreProcessamentoCollection MontaPendencias(List<object> lstIdCliente, List<object> lstData, int chave, bool persiste)
        {
            List<string> lstIndices = new List<string>();
            List<CotacaoData> lstCotacaoData = new List<CotacaoData>();
            List<CotaValidar> lstCota = new List<CotaValidar>();
            List<CotacaoBolsaValidar> lstCotacaoBolsaValidar = new List<CotacaoBolsaValidar>();
            List<CotacaoBMFValidar> lstCotacaoBMFValidar = new List<CotacaoBMFValidar>();
            Hashtable hsPerfilMTMValidar = new Hashtable();
            List<string> lstAtivoBolsaCliente = new List<string>();
            List<string> lstAtivoBMFCliente = new List<string>();
            List<string> lstFundoAplicado = new List<string>();

            PreProcessamentoCollection preProcessamentoColl = new PreProcessamentoCollection();
            PreProcessamentoClienteQuery preProcessamentoClienteQuery = new PreProcessamentoClienteQuery("preCliente");

            #region Carrega Cliente na Tabela chave
            if (lstIdCliente.Count > 0)
            {
                PreProcessamentoClienteCollection preProcessamentoClienteColl = new PreProcessamentoClienteCollection();

                for (int i = 0; i < lstIdCliente.Count; i++)
                {
                    int idClienteAux = Convert.ToInt32(lstIdCliente[i]);
                    DateTime dataDiaAux = Convert.ToDateTime(lstData[i]);

                    PreProcessamentoCliente preProcessamentoCliente = preProcessamentoClienteColl.AddNew();
                    preProcessamentoCliente.Chave = chave;
                    preProcessamentoCliente.IdCliente = idClienteAux;
                    preProcessamentoCliente.DataDia = dataDiaAux;
                }

                preProcessamentoClienteColl.Save();
            }
            #endregion

            #region Fundo
            //Operação
            OperacaoFundoCollection operacaoFundoColl = new OperacaoFundoCollection();
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("operacao");
            HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("historico");
            ClienteQuery clienteQuery = new ClienteQuery("cliente");
            CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");

            operacaoFundoQuery.es.Distinct = true;
            operacaoFundoQuery.Select(operacaoFundoQuery.IdCarteira, preProcessamentoClienteQuery.DataDia);
            operacaoFundoQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(operacaoFundoQuery.IdCliente) &
                                                                          preProcessamentoClienteQuery.DataDia.Equal(operacaoFundoQuery.DataConversao));
            operacaoFundoQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            operacaoFundoColl.Load(operacaoFundoQuery);

            foreach (OperacaoFundo operacaoFundo in operacaoFundoColl)
            {
                int idCarteira = operacaoFundo.IdCarteira.Value;
                DateTime dataDia = Convert.ToDateTime(operacaoFundo.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));

                string carteiraSemCota = idCarteira + "|" + dataDia.ToShortDateString();
                if (!lstFundoAplicado.Contains(carteiraSemCota))
                {
                    lstFundoAplicado.Add(carteiraSemCota);

                    CotaValidar cota = new CotaValidar();
                    cota.IdCarteira = idCarteira;
                    cota.DataCotacao = dataDia;
                    lstCota.Add(cota);
                }
            }

            //Posicao
            PosicaoFundoCollection posicaoFundoColl = new PosicaoFundoCollection();
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("posicao");
            historicoCotaQuery = new HistoricoCotaQuery("historico");
            clienteQuery = new ClienteQuery("cliente");
            carteiraQuery = new CarteiraQuery("carteira");

            posicaoFundoQuery.es.Distinct = true;
            posicaoFundoQuery.Select(posicaoFundoQuery.IdCarteira, preProcessamentoClienteQuery.DataDia);
            posicaoFundoQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(posicaoFundoQuery.IdCliente));
            posicaoFundoQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            posicaoFundoColl.Load(posicaoFundoQuery);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoColl)
            {
                int idCarteira = posicaoFundo.IdCarteira.Value;
                DateTime dataDia = Convert.ToDateTime(posicaoFundo.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));

                string carteiraSemCota = idCarteira + "|" + dataDia.ToShortDateString();
                if (!lstFundoAplicado.Contains(carteiraSemCota))
                {
                    lstFundoAplicado.Add(carteiraSemCota);

                    CotaValidar cota = new CotaValidar();
                    cota.IdCarteira = idCarteira;
                    cota.DataCotacao = dataDia;
                    lstCota.Add(cota);
                }
            }

            #endregion

            #region Bolsa
            //Operacao
            CotacaoBolsaQuery cotacaoBolsaQuery = new CotacaoBolsaQuery("cotacaoBolsa");
            OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("operacaoBolsa");
            OperacaoBolsaCollection operacaoBolsaColl = new OperacaoBolsaCollection();
            clienteQuery = new ClienteQuery("cliente");

            operacaoBolsaQuery.es.Distinct = true;
            operacaoBolsaQuery.Select(operacaoBolsaQuery.CdAtivoBolsa, preProcessamentoClienteQuery.DataDia);
            operacaoBolsaQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(operacaoBolsaQuery.IdCliente) &
                                                                          preProcessamentoClienteQuery.DataDia.Equal(operacaoBolsaQuery.Data));
            operacaoBolsaQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            operacaoBolsaColl.Load(operacaoBolsaQuery);

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaColl)
            {
                string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                DateTime dataDia = Convert.ToDateTime(operacaoBolsa.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));

                string bolsaAtivoData = cdAtivoBolsa + "|" + dataDia.ToShortDateString();
                if (!lstAtivoBolsaCliente.Contains(bolsaAtivoData))
                {
                    lstAtivoBolsaCliente.Add(bolsaAtivoData);

                    CotacaoBolsaValidar cotacaoBolsaValidar = new CotacaoBolsaValidar();
                    cotacaoBolsaValidar.CdAtivoBolsa = cdAtivoBolsa;
                    cotacaoBolsaValidar.DataCotacao = dataDia;

                    lstCotacaoBolsaValidar.Add(cotacaoBolsaValidar);
                }
            }

            //posicao
            cotacaoBolsaQuery = new CotacaoBolsaQuery("cotacaoBolsa");
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("posicaoBolsa");
            PosicaoBolsaCollection posicaoBolsaColl = new PosicaoBolsaCollection();

            posicaoBolsaQuery.es.Distinct = true;
            posicaoBolsaQuery.Select(posicaoBolsaQuery.CdAtivoBolsa, preProcessamentoClienteQuery.DataDia);
            posicaoBolsaQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(posicaoBolsaQuery.IdCliente));
            posicaoBolsaQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            posicaoBolsaColl.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaColl)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                DateTime dataDia = Convert.ToDateTime(posicaoBolsa.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));
                string bolsaAtivoData = cdAtivoBolsa + "|" + dataDia.ToShortDateString();

                if (lstAtivoBolsaCliente.Contains(bolsaAtivoData))
                {
                    continue;
                }

                CotacaoBolsaValidar cotacaoBolsaValidar = new CotacaoBolsaValidar();
                cotacaoBolsaValidar.CdAtivoBolsa = cdAtivoBolsa;
                cotacaoBolsaValidar.DataCotacao = dataDia;

                lstCotacaoBolsaValidar.Add(cotacaoBolsaValidar);
            }
            #endregion

            #region BMF
            //Operacao
            CotacaoBMFQuery cotacaoBMFQuery = new CotacaoBMFQuery("cotacaoBMF");
            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("operacaoBMF");
            OperacaoBMFCollection operacaoBMFColl = new OperacaoBMFCollection();
            clienteQuery = new ClienteQuery("cliente");

            operacaoBMFQuery.es.Distinct = true;
            operacaoBMFQuery.Select(operacaoBMFQuery.CdAtivoBMF, operacaoBMFQuery.Serie, preProcessamentoClienteQuery.DataDia);
            operacaoBMFQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(operacaoBMFQuery.IdCliente) &
                                                                        preProcessamentoClienteQuery.DataDia.Equal(operacaoBMFQuery.Data));
            operacaoBMFQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            operacaoBMFColl.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFColl)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF + "-" + operacaoBMF.Serie;
                DateTime dataDia = Convert.ToDateTime(operacaoBMF.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));

                string BMFAtivoData = cdAtivoBMF + "|" + dataDia.ToShortDateString();
                if (!lstAtivoBMFCliente.Contains(BMFAtivoData))
                {
                    lstAtivoBMFCliente.Add(BMFAtivoData);

                    CotacaoBMFValidar cotacaoBMFValidar = new CotacaoBMFValidar();
                    cotacaoBMFValidar.CdAtivoBMF = operacaoBMF.CdAtivoBMF;
                    cotacaoBMFValidar.Serie = operacaoBMF.Serie;
                    cotacaoBMFValidar.DataCotacao = dataDia;

                    lstCotacaoBMFValidar.Add(cotacaoBMFValidar);
                }
            }

            //posicao
            cotacaoBMFQuery = new CotacaoBMFQuery("cotacaoBMF");
            PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("posicaoBMF");
            PosicaoBMFCollection posicaoBMFColl = new PosicaoBMFCollection();

            posicaoBMFQuery.es.Distinct = true;
            posicaoBMFQuery.Select(posicaoBMFQuery.CdAtivoBMF, posicaoBMFQuery.Serie, preProcessamentoClienteQuery.DataDia);
            posicaoBMFQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(posicaoBMFQuery.IdCliente));
            posicaoBMFQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            posicaoBMFColl.Load(posicaoBMFQuery);

            foreach (PosicaoBMF posicaoBMF in posicaoBMFColl)
            {
                string cdAtivoBMF = posicaoBMF.CdAtivoBMF + "-" + posicaoBMF.Serie;
                DateTime dataDia = Convert.ToDateTime(posicaoBMF.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));
                string BMFAtivoData = cdAtivoBMF + "|" + dataDia.ToShortDateString();

                if (lstAtivoBMFCliente.Contains(BMFAtivoData))
                {
                    continue;
                }

                CotacaoBMFValidar cotacaoBMFValidar = new CotacaoBMFValidar();
                cotacaoBMFValidar.CdAtivoBMF = posicaoBMF.CdAtivoBMF;
                cotacaoBMFValidar.Serie = posicaoBMF.Serie;
                cotacaoBMFValidar.DataCotacao = dataDia;

                lstCotacaoBMFValidar.Add(cotacaoBMFValidar);
            }
            #endregion

            #region Renda Fixa

            //Operação
            OperacaoRendaFixaQuery operacaoRFQuery = new OperacaoRendaFixaQuery("operacao");
            TituloRendaFixaQuery tituloRFQuery = new TituloRendaFixaQuery("titulo");
            OperacaoRendaFixaCollection operacaoRFColl = new OperacaoRendaFixaCollection();

            operacaoRFQuery.es.Distinct = true;
            operacaoRFQuery.Select(preProcessamentoClienteQuery.DataDia,
                                   operacaoRFQuery.IdIndiceVolta,
                                   carteiraQuery.IdGrupoPerfilMTM,
                                   tituloRFQuery.Descricao,
                                   tituloRFQuery.IdTitulo,
                                   tituloRFQuery.IdPapel,
                                   tituloRFQuery.IdTitulo,
                                   tituloRFQuery.CodigoCustodia,
                                   tituloRFQuery.CodigoCBLC,
                                   tituloRFQuery.DataEmissao,
                                   tituloRFQuery.DataVencimento,
                                   tituloRFQuery.IdIndice);
            operacaoRFQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira.Equal(operacaoRFQuery.IdCliente));            
            operacaoRFQuery.InnerJoin(tituloRFQuery).On(tituloRFQuery.IdTitulo.Equal(operacaoRFQuery.IdTitulo));            
            operacaoRFQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(operacaoRFQuery.IdCliente) &
                                                                       preProcessamentoClienteQuery.DataDia.Equal(operacaoRFQuery.DataRegistro));
            operacaoRFQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            operacaoRFColl.Load(operacaoRFQuery);

            foreach (OperacaoRendaFixa operacaoRF in operacaoRFColl)
            {
                #region Busca os indices para fazer a varredura
                int? idIndiceVolta = operacaoRF.IdIndiceVolta;
                DateTime dataDia = Convert.ToDateTime(operacaoRF.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));
                int idTitulo = Convert.ToInt32(operacaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdTitulo));

                string chavePerfilMTM = idTitulo + "|" + dataDia.ToShortDateString();
                if (!hsPerfilMTMValidar.Contains(chavePerfilMTM))
                {
                    int idPapel = Convert.ToInt32(operacaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                    string codigoCBLC = Convert.ToString(operacaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCBLC));
                    string codigoCustodia = Convert.ToString(operacaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia));
                    DateTime dataEmissao = Convert.ToDateTime(operacaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));
                    DateTime dataVencimento = Convert.ToDateTime(operacaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));
                    string tituloDescricao = Convert.ToString(operacaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));

                    int? idGrupoPerfilMTM = null;
                    if (!string.IsNullOrEmpty(operacaoRF.GetColumn(CarteiraMetadata.ColumnNames.IdGrupoPerfilMTM).ToString()))
                        idGrupoPerfilMTM = Convert.ToInt32(operacaoRF.GetColumn(CarteiraMetadata.ColumnNames.IdGrupoPerfilMTM));


                    PerfilMTMValidar perfil = new PerfilMTMValidar();
                    perfil.IdTitulo = idTitulo;
                    perfil.DataSerie = dataDia;
                    perfil.IdPapel = idPapel;
                    perfil.CodigoCBLC = codigoCBLC;
                    perfil.CodigoCustodia = codigoCustodia;
                    perfil.DataEmissao = dataEmissao;
                    perfil.DataVencimento = dataVencimento;
                    perfil.TituloDescricao = tituloDescricao;
                    perfil.IdGrupoPerfilMTM = idGrupoPerfilMTM;
                    hsPerfilMTMValidar.Add(chavePerfilMTM, perfil);
                }

                int idIndice;
                if (!int.TryParse(operacaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice).ToString(), out idIndice))
                    idIndice = 0;

                string indiceData = string.Empty;
                if (idIndice != 0)
                {
                    CotacaoData cotacaoData = RetornaDataCotacao(idIndice, dataDia);

                    indiceData = idIndice + "|" + cotacaoData.DataCotacao.ToShortDateString();
                    if (!lstIndices.Contains(indiceData))
                    {
                        lstCotacaoData.Add(cotacaoData);
                        lstIndices.Add(indiceData);
                    }
                    else
                        continue;
                }

                if (idIndiceVolta.HasValue)
                {
                    CotacaoData cotacaoData = RetornaDataCotacao(idIndiceVolta.Value, dataDia);

                    indiceData = idIndiceVolta.Value + "|" + cotacaoData.DataCotacao.ToShortDateString();
                    if (!lstIndices.Contains(indiceData))
                    {
                        lstCotacaoData.Add(cotacaoData);
                        lstIndices.Add(indiceData);
                    }
                    else
                        continue;
                }
                #endregion
            }

            //Posição
            PosicaoRendaFixaQuery posicaoRFQuery = new PosicaoRendaFixaQuery("posicao");
            tituloRFQuery = new TituloRendaFixaQuery("titulo");
            PosicaoRendaFixaCollection posicaoRFColl = new PosicaoRendaFixaCollection();

            posicaoRFQuery.es.Distinct = true;
            posicaoRFQuery.Select(posicaoRFQuery.IdIndiceVolta,
                                  tituloRFQuery.IdIndice,
                                  tituloRFQuery.Descricao,
                                  carteiraQuery.IdGrupoPerfilMTM,
                                  tituloRFQuery.IdTitulo,
                                  tituloRFQuery.IdPapel,
                                  tituloRFQuery.IdTitulo,
                                  tituloRFQuery.CodigoCustodia,
                                  tituloRFQuery.CodigoCBLC,
                                  tituloRFQuery.DataEmissao,
                                  tituloRFQuery.DataVencimento,
                                  tituloRFQuery.IdIndice,
                                  preProcessamentoClienteQuery.DataDia);
            posicaoRFQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira.Equal(posicaoRFQuery.IdCliente));
            posicaoRFQuery.InnerJoin(tituloRFQuery).On(tituloRFQuery.IdTitulo.Equal(posicaoRFQuery.IdTitulo));
            posicaoRFQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(posicaoRFQuery.IdCliente));
            posicaoRFQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            posicaoRFColl.Load(posicaoRFQuery);

            foreach (PosicaoRendaFixa posicaoRF in posicaoRFColl)
            {
                #region Busca os indices para fazer a varredura
                int? idIndiceVolta = posicaoRF.IdIndiceVolta;
                DateTime dataDia = Convert.ToDateTime(posicaoRF.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));

                int idTitulo = Convert.ToInt32(posicaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdTitulo));

                string chavePerfilMTM = idTitulo + "|" + dataDia.ToShortDateString();
                if (!hsPerfilMTMValidar.Contains(chavePerfilMTM))
                {
                    int idPapel = Convert.ToInt32(posicaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                    string codigoCBLC = Convert.ToString(posicaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCBLC));
                    string codigoCustodia = Convert.ToString(posicaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia));
                    DateTime dataEmissao = Convert.ToDateTime(posicaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataEmissao));
                    DateTime dataVencimento = Convert.ToDateTime(posicaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.DataVencimento));                    
                    string tituloDescricao = Convert.ToString(posicaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));
                    
                    int? idGrupoPerfilMTM = null;
                    if (!string.IsNullOrEmpty(posicaoRF.GetColumn(CarteiraMetadata.ColumnNames.IdGrupoPerfilMTM).ToString()))
                        idGrupoPerfilMTM = Convert.ToInt32(posicaoRF.GetColumn(CarteiraMetadata.ColumnNames.IdGrupoPerfilMTM));

                    PerfilMTMValidar perfil = new PerfilMTMValidar();
                    perfil.IdTitulo = idTitulo;
                    perfil.DataSerie = dataDia;
                    perfil.IdPapel = idPapel;
                    perfil.CodigoCBLC = codigoCBLC;
                    perfil.CodigoCustodia = codigoCustodia;
                    perfil.DataEmissao = dataEmissao;
                    perfil.DataVencimento = dataVencimento;
                    perfil.TituloDescricao = tituloDescricao;
                    perfil.IdGrupoPerfilMTM = idGrupoPerfilMTM;
                    hsPerfilMTMValidar.Add(chavePerfilMTM, perfil);
                }

                int idIndice;
                if (!int.TryParse(posicaoRF.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdIndice).ToString(), out idIndice))
                    idIndice = 0;

                string indiceData = string.Empty;
                if (idIndice != 0)
                {
                    CotacaoData cotacaoData = RetornaDataCotacao(idIndice, dataDia);

                    indiceData = idIndice + "|" + cotacaoData.DataCotacao.ToShortDateString();
                    if (!lstIndices.Contains(indiceData))
                    {
                        lstCotacaoData.Add(cotacaoData);
                        lstIndices.Add(indiceData);
                    }
                }

                if (idIndiceVolta.HasValue)
                {
                    CotacaoData cotacaoData = RetornaDataCotacao(idIndiceVolta.Value, dataDia);

                    indiceData = idIndiceVolta.Value + "|" + cotacaoData.DataCotacao.ToShortDateString();
                    if (!lstIndices.Contains(indiceData))
                    {
                        lstCotacaoData.Add(cotacaoData);
                        lstIndices.Add(indiceData);
                    }
                }
                #endregion
            }
            #endregion

            #region Swap

            //Operação
            OperacaoSwapQuery operacaoSwapQuery = new OperacaoSwapQuery("operacao");
            OperacaoSwapCollection operacaoSwapColl = new OperacaoSwapCollection();

            operacaoSwapQuery.es.Distinct = true;
            operacaoSwapQuery.Select(preProcessamentoClienteQuery.DataDia, operacaoSwapQuery.IdIndice, operacaoSwapQuery.IdIndiceContraParte,
                                     operacaoSwapQuery.IdAtivoCarteira, operacaoSwapQuery.IdAtivoCarteiraContraParte,
                                     operacaoSwapQuery.CdAtivoBolsa, operacaoSwapQuery.CdAtivoBolsaContraParte);
            operacaoSwapQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(operacaoSwapQuery.IdCliente) &
                                                                         preProcessamentoClienteQuery.DataDia.Equal(operacaoSwapQuery.DataRegistro));
            operacaoSwapQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            operacaoSwapColl.Load(operacaoSwapQuery);

            foreach (OperacaoSwap operacaoSwap in operacaoSwapColl)
            {
                DateTime dataDia = Convert.ToDateTime(operacaoSwap.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));

                #region Indices
                int? idIndiceParte = operacaoSwap.IdIndice;
                int? idIndiceContraParte = operacaoSwap.IdIndiceContraParte;

                string indiceData = string.Empty;
                if (idIndiceParte.HasValue)
                {
                    CotacaoData cotacaoData = RetornaDataCotacao(idIndiceParte.Value, dataDia);

                    indiceData = idIndiceParte.Value + "|" + cotacaoData.DataCotacao.ToShortDateString();
                    if (!lstIndices.Contains(indiceData))
                    {
                        lstCotacaoData.Add(cotacaoData);
                        lstIndices.Add(indiceData);
                    }
                }

                if (idIndiceContraParte.HasValue)
                {
                    CotacaoData cotacaoData = RetornaDataCotacao(idIndiceContraParte.Value, dataDia);

                    indiceData = idIndiceContraParte.Value + "|" + cotacaoData.DataCotacao.ToShortDateString();
                    if (!lstIndices.Contains(indiceData))
                    {
                        lstCotacaoData.Add(cotacaoData);
                        lstIndices.Add(indiceData);
                    }
                }
                #endregion

                #region Bolsa
                string cdAtivoBolsa = operacaoSwap.CdAtivoBolsa;
                string cdAtivoBolsaContraParte = operacaoSwap.CdAtivoBolsaContraParte;

                string bolsaAtivoData = string.Empty;
                if (!string.IsNullOrEmpty(cdAtivoBolsa))
                {
                    bolsaAtivoData = cdAtivoBolsa + "|" + dataDia.ToShortDateString();
                    if (!lstAtivoBolsaCliente.Contains(bolsaAtivoData))
                    {
                        CotacaoBolsaValidar cotacaoBolsaSwap = new CotacaoBolsaValidar();
                        cotacaoBolsaSwap.CdAtivoBolsa = cdAtivoBolsa;
                        cotacaoBolsaSwap.DataCotacao = dataDia;

                        lstCotacaoBolsaValidar.Add(cotacaoBolsaSwap);
                    }
                }

                bolsaAtivoData = string.Empty;
                if (!string.IsNullOrEmpty(cdAtivoBolsaContraParte))
                {
                    bolsaAtivoData = cdAtivoBolsaContraParte + "|" + dataDia.ToShortDateString();
                    if (!lstAtivoBolsaCliente.Contains(bolsaAtivoData))
                    {
                        CotacaoBolsaValidar cotacaoBolsaSwap = new CotacaoBolsaValidar();
                        cotacaoBolsaSwap.CdAtivoBolsa = cdAtivoBolsaContraParte;
                        cotacaoBolsaSwap.DataCotacao = dataDia;

                        lstCotacaoBolsaValidar.Add(cotacaoBolsaSwap);
                    }
                }
                #endregion

                #region Cota
                int? idAtivoCarteira = operacaoSwap.IdAtivoCarteira;
                int? idAtivoCarteiraContraParte = operacaoSwap.IdAtivoCarteiraContraParte;
                string carteiraSemCota = string.Empty;
                if (idAtivoCarteira.HasValue)
                {
                    carteiraSemCota = idAtivoCarteira + "|" + dataDia.ToShortDateString();
                    if (!lstFundoAplicado.Contains(carteiraSemCota))
                    {
                        CotaValidar cota = new CotaValidar();

                        cota.DataCotacao = dataDia;
                        cota.IdCarteira = idAtivoCarteira.Value;
                        lstCota.Add(cota);
                    }
                }

                carteiraSemCota = string.Empty;
                if (idAtivoCarteiraContraParte.HasValue)
                {
                    carteiraSemCota = idAtivoCarteiraContraParte + "|" + dataDia.ToShortDateString();
                    if (!lstFundoAplicado.Contains(carteiraSemCota))
                    {
                        CotaValidar cota = new CotaValidar();

                        cota.DataCotacao = dataDia;
                        cota.IdCarteira = idAtivoCarteiraContraParte.Value;
                        lstCota.Add(cota);
                    }
                }
                #endregion
            }


            //Posição
            PosicaoSwapQuery posicaoSwapQuery = new PosicaoSwapQuery("posicao");
            PosicaoSwapCollection posicaoSwapColl = new PosicaoSwapCollection();

            posicaoSwapQuery.es.Distinct = true;
            posicaoSwapQuery.Select(preProcessamentoClienteQuery.DataDia, posicaoSwapQuery.IdIndice, posicaoSwapQuery.IdIndiceContraParte,
                                     posicaoSwapQuery.IdAtivoCarteira, posicaoSwapQuery.IdAtivoCarteiraContraParte,
                                     posicaoSwapQuery.CdAtivoBolsa, posicaoSwapQuery.CdAtivoBolsaContraParte);
            posicaoSwapQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(posicaoSwapQuery.IdCliente));
            posicaoSwapQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            posicaoSwapColl.Load(posicaoSwapQuery);

            foreach (PosicaoSwap posicaoSwap in posicaoSwapColl)
            {
                DateTime dataDia = Convert.ToDateTime(posicaoSwap.GetColumn(PreProcessamentoClienteMetadata.ColumnNames.DataDia));

                #region Indices
                int? idIndiceParte = posicaoSwap.IdIndice;
                int? idIndiceContraParte = posicaoSwap.IdIndiceContraParte;

                string indiceData = string.Empty;
                if (idIndiceParte.HasValue)
                {
                    CotacaoData cotacaoData = RetornaDataCotacao(idIndiceParte.Value, dataDia);

                    indiceData = idIndiceParte.Value + "|" + cotacaoData.DataCotacao.ToShortDateString();
                    if (!lstIndices.Contains(indiceData))
                    {
                        lstCotacaoData.Add(cotacaoData);
                        lstIndices.Add(indiceData);
                    }
                }

                if (idIndiceContraParte.HasValue)
                {
                    CotacaoData cotacaoData = RetornaDataCotacao(idIndiceContraParte.Value, dataDia);

                    indiceData = idIndiceContraParte.Value + "|" + cotacaoData.DataCotacao.ToShortDateString();
                    if (!lstIndices.Contains(indiceData))
                    {
                        lstCotacaoData.Add(cotacaoData);
                        lstIndices.Add(indiceData);
                    }
                }
                #endregion

                #region Bolsa
                string cdAtivoBolsa = posicaoSwap.CdAtivoBolsa;
                string cdAtivoBolsaContraParte = posicaoSwap.CdAtivoBolsaContraParte;

                string bolsaAtivoData = string.Empty;
                if (!string.IsNullOrEmpty(cdAtivoBolsa))
                {
                    bolsaAtivoData = cdAtivoBolsa + "|" + dataDia.ToShortDateString();
                    if (!lstAtivoBolsaCliente.Contains(bolsaAtivoData))
                    {
                        CotacaoBolsaValidar cotacaoBolsaSwap = new CotacaoBolsaValidar();
                        cotacaoBolsaSwap.CdAtivoBolsa = cdAtivoBolsa;
                        cotacaoBolsaSwap.DataCotacao = dataDia;

                        lstCotacaoBolsaValidar.Add(cotacaoBolsaSwap);
                    }
                }

                bolsaAtivoData = string.Empty;
                if (!string.IsNullOrEmpty(cdAtivoBolsaContraParte))
                {
                    bolsaAtivoData = cdAtivoBolsaContraParte + "|" + dataDia.ToShortDateString();
                    if (!lstAtivoBolsaCliente.Contains(bolsaAtivoData))
                    {
                        CotacaoBolsaValidar cotacaoBolsaSwap = new CotacaoBolsaValidar();
                        cotacaoBolsaSwap.CdAtivoBolsa = cdAtivoBolsaContraParte;
                        cotacaoBolsaSwap.DataCotacao = dataDia;

                        lstCotacaoBolsaValidar.Add(cotacaoBolsaSwap);
                    }
                }
                #endregion

                #region Cota
                int? idAtivoCarteira = posicaoSwap.IdAtivoCarteira;
                int? idAtivoCarteiraContraParte = posicaoSwap.IdAtivoCarteiraContraParte;
                string carteiraSemCota = string.Empty;
                if (idAtivoCarteira.HasValue)
                {
                    carteiraSemCota = idAtivoCarteira + "|" + dataDia.ToShortDateString();
                    if (!lstFundoAplicado.Contains(carteiraSemCota))
                    {
                        CotaValidar cota = new CotaValidar();

                        cota.DataCotacao = dataDia;
                        cota.IdCarteira = idAtivoCarteira.Value;
                        lstCota.Add(cota);
                    }
                }

                carteiraSemCota = string.Empty;
                if (idAtivoCarteiraContraParte.HasValue)
                {
                    carteiraSemCota = idAtivoCarteiraContraParte + "|" + dataDia.ToShortDateString();
                    if (!lstFundoAplicado.Contains(carteiraSemCota))
                    {
                        CotaValidar cota = new CotaValidar();

                        cota.DataCotacao = dataDia;
                        cota.IdCarteira = idAtivoCarteiraContraParte.Value;
                        lstCota.Add(cota);
                    }
                }
                #endregion
            }
            #endregion

            #region CDI de D0 (Para cálculo do resultado)
            clienteQuery = new ClienteQuery("cliente");
            ClienteCollection clienteColl = new ClienteCollection();

            clienteQuery.es.Distinct = true;
            clienteQuery.Select(clienteQuery.DataDia);
            clienteQuery.InnerJoin(preProcessamentoClienteQuery).On(preProcessamentoClienteQuery.IdCliente.Equal(clienteQuery.IdCliente));

            clienteColl.Load(clienteQuery);

            foreach (Cliente cliente in clienteColl)
            {
                CotacaoData cotacaoData = new CotacaoData();
                cotacaoData.IdIndice = (int)ListaIndiceFixo.CDI;
                cotacaoData.DataCotacao = cliente.DataDia.Value;

                string indiceData = cotacaoData.IdIndice + "|" + cotacaoData.DataCotacao.ToShortDateString();
                if (!lstIndices.Contains(indiceData))
                {
                    lstCotacaoData.Add(cotacaoData);
                    lstIndices.Add(indiceData);
                }
            }
            #endregion

            #region Busca cotações - Indexadores
            if (lstCotacaoData.Count > 0)
            {
                lstCotacaoData.Sort(delegate(CotacaoData x, CotacaoData y) { return x.IdIndice.CompareTo(y.IdIndice); });
                foreach (CotacaoData cotacaoData in lstCotacaoData)
                {
                    CotacaoIndice cotacaoIndice = new CotacaoIndice();
                    IndiceCollection indiceColl = new IndiceCollection();
                    List<Indice> lstIndice = new List<Indice>();

                    indiceColl.LoadAll();
                    lstIndice = (List<Indice>)indiceColl;

                    if (!cotacaoIndice.LoadByPrimaryKey(cotacaoData.DataCotacao, (short)cotacaoData.IdIndice))
                    {
                        Indice indice = lstIndice.Find(delegate(Indice x) { return x.IdIndice == cotacaoData.IdIndice; });

                        PreProcessamento preProcessamento = preProcessamentoColl.AddNew();

                        preProcessamento.Descricao = indice.Descricao;
                        preProcessamento.Descricao = preProcessamento.Descricao.Trim();
                        preProcessamento.Pendencia = "Cotação Indexador";
                        preProcessamento.Data = cotacaoData.DataCotacao;
                        preProcessamento.Chave = chave;
                    }
                }
            }
            #endregion

            #region Busca Cotação Bolsa
            if (lstCotacaoBolsaValidar.Count > 0)
            {
                lstCotacaoBolsaValidar.Sort(delegate(CotacaoBolsaValidar x, CotacaoBolsaValidar y) { return x.CdAtivoBolsa.CompareTo(y.CdAtivoBolsa); });
                foreach (CotacaoBolsaValidar cotacaoBolsaSwap in lstCotacaoBolsaValidar)
                {
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();

                    DateTime data = cotacaoBolsaSwap.DataCotacao;
                    string cdAtivoBolsa = cotacaoBolsaSwap.CdAtivoBolsa;

                    if (!cotacaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa))
                    {
                        PreProcessamento preProcessamento = preProcessamentoColl.AddNew();

                        preProcessamento.Descricao = cotacaoBolsaSwap.CdAtivoBolsa;
                        preProcessamento.Descricao = preProcessamento.Descricao.Trim();
                        preProcessamento.Pendencia = "Cotação Bolsa";
                        preProcessamento.Data = cotacaoBolsaSwap.DataCotacao;
                        preProcessamento.Chave = chave;
                    }
                }
            }
            #endregion

            #region Busca Cotação BMF
            if (lstCotacaoBMFValidar.Count > 0)
            {
                lstCotacaoBMFValidar.Sort(delegate(CotacaoBMFValidar x, CotacaoBMFValidar y) { return x.CdAtivoBMF.CompareTo(y.CdAtivoBMF); });
                foreach (CotacaoBMFValidar cotacaoBMFSwap in lstCotacaoBMFValidar)
                {
                    CotacaoBMF cotacaoBMF = new CotacaoBMF();

                    DateTime data = cotacaoBMFSwap.DataCotacao;
                    string cdAtivoBMF = cotacaoBMFSwap.CdAtivoBMF;
                    string serie = cotacaoBMFSwap.Serie;

                    if (!cotacaoBMF.LoadByPrimaryKey(data, cdAtivoBMF, serie))
                    {
                        PreProcessamento preProcessamento = preProcessamentoColl.AddNew();

                        preProcessamento.Descricao = cotacaoBMFSwap.CdAtivoBMF + "-" + cotacaoBMFSwap.Serie;
                        preProcessamento.Descricao = preProcessamento.Descricao.Trim();
                        preProcessamento.Pendencia = "Cotação BMF";
                        preProcessamento.Data = cotacaoBMFSwap.DataCotacao;
                        preProcessamento.Chave = chave;
                    }
                }
            }
            #endregion

            #region Busca Cota
            Carteira carteira = new Carteira();
            HistoricoCota historicoCota = new HistoricoCota();
            Dictionary<int, Carteira> dicCarteira = new Dictionary<int, Carteira>();

            if (lstCota.Count > 0)
            {
                PreProcessamentoHistoricoCotaCollection preProcHistoricoCotaColl = new PreProcessamentoHistoricoCotaCollection();
                foreach (CotaValidar cotaValidar in lstCota)
                {
                    PreProcessamentoHistoricoCota preProcHistoricoCota = preProcHistoricoCotaColl.AddNew();

                    preProcHistoricoCota.Data = cotaValidar.DataCotacao;
                    preProcHistoricoCota.IdCarteira = cotaValidar.IdCarteira;
                    preProcHistoricoCota.Chave = chave;

                }

                preProcHistoricoCotaColl.Save();

                PreProcessamentoHistoricoCotaQuery preProcHistoricoCotaQuery = new PreProcessamentoHistoricoCotaQuery("preProcHistoricoCota");
                preProcHistoricoCotaColl = new PreProcessamentoHistoricoCotaCollection();
                historicoCotaQuery = new HistoricoCotaQuery("historicoCota");

                preProcHistoricoCotaQuery.Select(preProcHistoricoCotaQuery.IdCarteira, preProcHistoricoCotaQuery.Data);
                preProcHistoricoCotaQuery.LeftJoin(historicoCotaQuery).On(preProcHistoricoCotaQuery.IdCarteira.Equal(historicoCotaQuery.IdCarteira) &
                                                                          preProcHistoricoCotaQuery.Data.Equal(historicoCotaQuery.Data));
                preProcHistoricoCotaQuery.Where(preProcHistoricoCotaQuery.Chave.Equal(chave) &
                                                historicoCotaQuery.CotaFechamento.IsNull());
                preProcHistoricoCotaQuery.OrderBy(preProcHistoricoCotaQuery.IdCarteira.Ascending, preProcHistoricoCotaQuery.Data.Ascending);

                preProcHistoricoCotaColl.Load(preProcHistoricoCotaQuery);


                foreach (PreProcessamentoHistoricoCota preProcHistoricoCota in preProcHistoricoCotaColl)
                {
                    DateTime data = preProcHistoricoCota.Data.Value;
                    int idCarteira = preProcHistoricoCota.IdCarteira.Value;

                    try
                    {

                        historicoCota = new HistoricoCota();
                        historicoCota.BuscaValorCota(idCarteira, data);
                    }
                    catch (Exception ex)
                    {
                        //Se entrar no catch é pq não tem repetição de cota
                        PreProcessamento preProcessamento = preProcessamentoColl.AddNew();

                        carteira = new Carteira();

                        if (dicCarteira.ContainsKey(idCarteira))
                        {
                            carteira = dicCarteira[idCarteira];
                        }
                        else
                        {
                            carteira.LoadByPrimaryKey(idCarteira);
                            dicCarteira.Add(idCarteira, carteira);
                        }

                        string apelido = carteira.Apelido;

                        if (apelido.Length >= 255)
                            preProcessamento.Descricao = apelido.Substring(0, 254);
                        else
                            preProcessamento.Descricao = apelido;

                        preProcessamento.Descricao = preProcessamento.Descricao.Trim();
                        preProcessamento.Pendencia = "Valor de Cota";
                        preProcessamento.Data = data;
                        preProcessamento.Chave = chave;
                    }
                }
            }
            #endregion

            #region Busca PerfilMTM

            PerfilMTMCollection perfilMTMColl = new PerfilMTMCollection();
            PerfilMTMQuery perfilMTMQuery = new PerfilMTMQuery("perfil");
            SerieRendaFixaQuery serieRendaFixaQuery = new SerieRendaFixaQuery("serie");

            preProcessamentoClienteQuery = new PreProcessamentoClienteQuery("preProcCliente");

            preProcessamentoClienteQuery.es.Distinct = true;
            preProcessamentoClienteQuery.Select(preProcessamentoClienteQuery.DataDia);
            preProcessamentoClienteQuery.Where(preProcessamentoClienteQuery.Chave.Equal(chave));

            perfilMTMQuery.es.Distinct = true;
            perfilMTMQuery.Select(perfilMTMQuery.IdSerie, perfilMTMQuery.IdPapel, perfilMTMQuery.IdGrupoPerfilMTM, perfilMTMQuery.DtReferencia, serieRendaFixaQuery.Descricao, serieRendaFixaQuery.TipoTaxa);
            perfilMTMQuery.InnerJoin(serieRendaFixaQuery).On(serieRendaFixaQuery.IdSerie.Equal(perfilMTMQuery.IdSerie));
            perfilMTMQuery.Where(perfilMTMQuery.DtReferencia.In(preProcessamentoClienteQuery));

            perfilMTMColl.Load(perfilMTMQuery);

            List<PerfilMTMValidar> lstPerfilMTMValidar = (hsPerfilMTMValidar.Values.OfType<PerfilMTMValidar>().AsParallel().ToList());

            //Só valida o perfis dos papéis que existem em estoque
            List<PerfilMTM> lstPerfilMTM = (from PerfilMTM p in ((List<PerfilMTM>)perfilMTMColl)
                                            join PerfilMTMValidar pValidar in lstPerfilMTMValidar on p.IdPapel.Value equals pValidar.IdPapel
                                            select p).Distinct().AsParallel().ToList();
            
            var arr_papelDataGrupo = (from PerfilMTMValidar p in lstPerfilMTMValidar                      
                                      orderby p.IdPapel, p.DataSerie
                                      select new { IdPapel = p.IdPapel, DataSerie = p.DataSerie, IdGrupoPerfilMTM = p.IdGrupoPerfilMTM }).AsParallel().Distinct().ToList();

            List<int> lstSeries_CaracteristicasTitulo = new List<int>();
            lstSeries_CaracteristicasTitulo.Add((int)TipoMTMTitulo.Mercado_Debenture);
            lstSeries_CaracteristicasTitulo.Add((int)TipoMTMTitulo.Par_Debenture);
            lstSeries_CaracteristicasTitulo.Add((int)TipoMTMTitulo.Andima_TituloPublico);
            lstSeries_CaracteristicasTitulo.Add((int)TipoMTMTitulo.CotaAnbima);
            lstSeries_CaracteristicasTitulo.Add((int)TipoMTMTitulo.BovespaBMF);

            Hashtable hsPapelRF = new Hashtable();
            Hashtable hsGrupoPerfilMTM = new Hashtable();

            PreProcessamentoCollection preProcessamentoFaltaPerfilDefaultColl = new PreProcessamentoCollection();
            PreProcessamentoCollection preProcessamentoFaltaPerfilColl = new PreProcessamentoCollection();

            //Percorre todos os papéis em estoque por data e grupo mtm
            foreach (var papelDataGrupo in arr_papelDataGrupo)
            {
                int? idGrupoPerfilMTM = papelDataGrupo.IdGrupoPerfilMTM;
                int idPapel = papelDataGrupo.IdPapel;
                DateTime dtReferencia = papelDataGrupo.DataSerie;

                //Não faz MTM
                if (!idGrupoPerfilMTM.HasValue) 
                    continue;

                PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                if (hsPapelRF.Contains(idPapel))
                {
                    papelRendaFixa = (PapelRendaFixa)hsPapelRF[idPapel];
                }
                else
                {
                    papelRendaFixa.LoadByPrimaryKey(idPapel);
                    hsPapelRF.Add(idPapel, papelRendaFixa);
                }
                string papelDescricao = papelRendaFixa.Descricao;

                GrupoPerfilMTM grupoPerfilMTM = new GrupoPerfilMTM();
                if (hsGrupoPerfilMTM.Contains(idPapel))
                {
                    grupoPerfilMTM = (GrupoPerfilMTM)hsGrupoPerfilMTM[idPapel];
                }
                else
                {
                    grupoPerfilMTM.LoadByPrimaryKey(idGrupoPerfilMTM.Value);
                    hsGrupoPerfilMTM.Add(idPapel, grupoPerfilMTM);
                }

                string grupoPerfilMTMDescricao = grupoPerfilMTM.Descricao;

                //Busca todos os perfis da Data + Grupo + PApel
                List<PerfilMTM> lstPerfilMTM_Papel = lstPerfilMTM.AsParallel().Where(x => x.DtReferencia == dtReferencia && x.IdPapel == idPapel && x.IdGrupoPerfilMTM == idGrupoPerfilMTM.Value).ToList();

                if (lstPerfilMTM_Papel.Count > 0)
                {
                    //Busca se existe Perfil MTM defaul para o papel
                    List<PerfilMTM> lstPerfilMTM_Papel_Default = lstPerfilMTM_Papel.AsParallel().Where(x => !x.IdTitulo.HasValue && !x.IdOperacao.HasValue).ToList();
                    if (lstPerfilMTM_Papel_Default.Count == 0)
                    {
                        PreProcessamento preProcessamento = preProcessamentoFaltaPerfilDefaultColl.AddNew();
                        preProcessamento.Descricao = "IdPapel - " + idPapel + " - " + papelDescricao.Trim() + " - Grupo - " + grupoPerfilMTMDescricao;
                        preProcessamento.Pendencia = "Falta de Perfil MTM Default Cadastrado";
                        preProcessamento.Data = dtReferencia;
                        preProcessamento.Chave = chave;

                    }

                    //Percorre todos os perfis cadastrados e busca os preços vinculados a série
                    foreach (PerfilMTM p in lstPerfilMTM_Papel)
                    {
                        int tipoMTM = Convert.ToInt32(p.GetColumn(SerieRendaFixaMetadata.ColumnNames.TipoTaxa));

                        //Não faz MTM
                        if (tipoMTM == (int)TipoMTMTitulo.NaoFaz)
                            continue;

                        int? idSerie = p.IdSerie;
                        string descricaoSerie = Convert.ToString(p.GetColumn(SerieRendaFixaMetadata.ColumnNames.Descricao));

                        //Séries que precisam das caracteristicas do título
                        if (lstSeries_CaracteristicasTitulo.Contains(tipoMTM))
                        {
                            foreach (PerfilMTMValidar pValidar in lstPerfilMTMValidar.AsParallel().Where(x => x.IdGrupoPerfilMTM == idGrupoPerfilMTM.Value && x.IdPapel == idPapel && x.DataSerie == dtReferencia).OrderBy(x => x.IdTitulo).ToList())
                            {         
                                int idTitulo = pValidar.IdTitulo;
                                string codigoCustodia = pValidar.CodigoCustodia;
                                DateTime dataVencimento = pValidar.DataVencimento;
                                DateTime dataEmissao = pValidar.DataEmissao;
                                string codigoCBLC = pValidar.CodigoCBLC;
                                string tituloDescricao = pValidar.TituloDescricao;

                                string descricaoPreProcessamento = "Id Título - " + idTitulo + " - " + tituloDescricao.Trim() + " | Id Série - " + idSerie.Value + " - " + descricaoSerie.Trim();
                                descricaoPreProcessamento = descricaoPreProcessamento.Trim();

                                if (tipoMTM == (int)TipoMTMTitulo.Mercado_Debenture)
                                {
                                    if (!String.IsNullOrEmpty(codigoCustodia))
                                    {
                                        CotacaoMercadoDebenture cotacaoMercadoDebenture = new CotacaoMercadoDebenture();
                                        if (!cotacaoMercadoDebenture.LoadByPrimaryKey(dtReferencia, codigoCustodia, dataVencimento))
                                        {
                                            PreProcessamento preProcessamento = preProcessamentoColl.AddNew();
                                            preProcessamento.Descricao = descricaoPreProcessamento;
                                            preProcessamento.Pendencia = "Preço MTM (Cotação Mercado Debenture)";
                                            preProcessamento.Data = dtReferencia;
                                            preProcessamento.Chave = chave;
                                        }
                                    }
                                }
                                else if (tipoMTM == (int)TipoMTMTitulo.Par_Debenture)
                                {
                                    if (!String.IsNullOrEmpty(codigoCustodia))
                                    {
                                        CotacaoDebenture cotacaoDebenture = new CotacaoDebenture();
                                        if (!cotacaoDebenture.LoadByPrimaryKey(dtReferencia, codigoCustodia))
                                        {
                                            PreProcessamento preProcessamento = preProcessamentoColl.AddNew();
                                            preProcessamento.Descricao = descricaoPreProcessamento;
                                            preProcessamento.Pendencia = "Preço MTM (Cotação Debenture)";
                                            preProcessamento.Data = dtReferencia;
                                            preProcessamento.Chave = chave;
                                        }
                                    }
                                }
                                else if (tipoMTM == (int)TipoMTMTitulo.Andima_TituloPublico)
                                {
                                    if (!String.IsNullOrEmpty(codigoCustodia))
                                    {
                                        if (codigoCustodia.Contains("7601")) //Garante todas as classes de NTN-B
                                        {
                                            codigoCustodia = "760199";
                                        }

                                        if (codigoCustodia.Contains("9501")) //Garante todas as classes de NTN-F
                                        {
                                            codigoCustodia = "950199";
                                        }

                                        CotacaoMercadoAndima cotacao = new CotacaoMercadoAndima();
                                        if (!cotacao.BuscaPU(dtReferencia, codigoCustodia, dataEmissao, dataVencimento))
                                        {
                                            PreProcessamento preProcessamento = preProcessamentoColl.AddNew();
                                            preProcessamento.Descricao = descricaoPreProcessamento;
                                            preProcessamento.Pendencia = "Preço MTM (Andima Título Público)";
                                            preProcessamento.Data = dtReferencia;
                                            preProcessamento.Chave = chave;
                                        }
                                    }
                                }
                                else if (tipoMTM == (int)TipoMTMTitulo.CotaAnbima)
                                {
                                    if (!String.IsNullOrEmpty(codigoCustodia))
                                    {
                                        if (Utilitario.IsInteger(codigoCustodia))
                                        {
                                            int codigoAmbima = Convert.ToInt32(codigoCustodia);
                                            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
                                            historicoCotaCollection.Query.Select(historicoCotaCollection.Query.CotaFechamento);
                                            historicoCotaCollection.Query.Where(historicoCotaCollection.Query.Data.Equal(dtReferencia),
                                                                                historicoCotaCollection.Query.IdCarteira.Equal(codigoAmbima));

                                            if (historicoCotaCollection.Query.Load())
                                            {
                                                PreProcessamento preProcessamento = preProcessamentoColl.AddNew();
                                                preProcessamento.Descricao = descricaoPreProcessamento;
                                                preProcessamento.Pendencia = "Preço MTM (Cota Andima)";
                                                preProcessamento.Data = dtReferencia;
                                                preProcessamento.Chave = chave;
                                            }
                                        }
                                    }
                                }
                                else if (tipoMTM == (int)TipoMTMTitulo.BovespaBMF)
                                {
                                    if (!String.IsNullOrEmpty(codigoCBLC))
                                    {
                                        string cdAtivoBolsa = codigoCBLC;

                                        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                                        if (!cotacaoBolsa.LoadByPrimaryKey(dtReferencia, cdAtivoBolsa))
                                        {
                                            PreProcessamento preProcessamento = preProcessamentoColl.AddNew();
                                            preProcessamento.Descricao = descricaoPreProcessamento;
                                            preProcessamento.Pendencia = "Preço MTM (Bovespa BMF)";
                                            preProcessamento.Data = dtReferencia;
                                            preProcessamento.Chave = chave;
                                        }
                                    }
                                }
                                else if (tipoMTM == (int)TipoMTMTitulo.Tesouro && dataVencimento > dtReferencia)
                                {
                                    CotacaoMercadoTesouroCollection cotacaoMercadoTesouroCollection = new CotacaoMercadoTesouroCollection();
                                    cotacaoMercadoTesouroCollection.Query.Where(cotacaoMercadoTesouroCollection.Query.DataReferencia.Equal(dtReferencia),
                                                                                cotacaoMercadoTesouroCollection.Query.DataVencimento.Equal(dataVencimento));

                                    if (cotacaoMercadoTesouroCollection.Query.Load())
                                    {
                                        string descricao = tituloDescricao.Trim().ToUpper().Replace("-", "").Replace("PRINCIPAL", "P").Replace("PRINC", "P").Trim();

                                        CotacaoMercadoTesouro cotacao = ((List<CotacaoMercadoTesouro>)cotacaoMercadoTesouroCollection).Find(x => x.Descricao.Equals(descricao));

                                        if (!cotacao.Pu.HasValue)
                                        {
                                            PreProcessamento preProcessamento = preProcessamentoColl.AddNew();
                                            preProcessamento.Descricao = descricaoPreProcessamento;
                                            preProcessamento.Pendencia = "Preço MTM (Tesouro)";
                                            preProcessamento.Data = dtReferencia;
                                            preProcessamento.Chave = chave;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (tipoMTM == (int)TipoMTMTitulo.Serie)
                            {
                                if (idSerie.HasValue)
                                {
                                    CotacaoSerie cotacaoSerie = new CotacaoSerie();
                                    if (!cotacaoSerie.LoadByPrimaryKey(dtReferencia, idSerie.Value))
                                    {
                                        PreProcessamento preProcessamento = preProcessamentoColl.AddNew();
                                        preProcessamento.Descricao = "Id Série - " + idSerie + " - " + descricaoSerie.Trim();
                                        preProcessamento.Pendencia = "Preço MTM (Cotação Série)";
                                        preProcessamento.Data = dtReferencia;
                                        preProcessamento.Chave = chave;
                                    }
                                }
                            }
                            else if (tipoMTM == (int)TipoMTMTitulo.SerieTaxa)
                            {
                                if (idSerie.HasValue)
                                {
                                    TaxaSerie taxaSerie = new TaxaSerie();
                                    if (!taxaSerie.LoadByPrimaryKey(dtReferencia, idSerie.Value))
                                    {
                                        PreProcessamento preProcessamento = preProcessamentoColl.AddNew();
                                        preProcessamento.Descricao = "Id Série - " + idSerie + " - " + descricaoSerie.Trim();
                                        preProcessamento.Pendencia = "Preço MTM (Taxa Série)";
                                        preProcessamento.Data = dtReferencia;
                                        preProcessamento.Chave = chave;
                                    }
                                }
                            }
                            else if (tipoMTM == (int)TipoMTMTitulo.SerieTaxaPuFace)
                            {
                                if (idSerie.HasValue)
                                {
                                    TaxaSerie taxaSerie = new TaxaSerie();
                                    if (!taxaSerie.LoadByPrimaryKey(dtReferencia, idSerie.Value))
                                    {
                                        PreProcessamento preProcessamento = preProcessamentoColl.AddNew();
                                        preProcessamento.Descricao = "Id Série - " + idSerie + " - " + descricaoSerie.Trim();
                                        preProcessamento.Pendencia = "Preço MTM (% do pu de Face)";
                                        preProcessamento.Data = dtReferencia;
                                        preProcessamento.Chave = chave;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    PreProcessamento preProcessamento = preProcessamentoFaltaPerfilColl.AddNew();
                    preProcessamento.Descricao = idPapel + " - " + papelDescricao.Trim() + " - " + grupoPerfilMTMDescricao;
                    preProcessamento.Pendencia = "Falta de Perfil MTM Cadastrado";
                    preProcessamento.Data = dtReferencia;
                    preProcessamento.Chave = chave;
                }
            }

            preProcessamentoColl.Combine(preProcessamentoFaltaPerfilColl);
            preProcessamentoColl.Combine(preProcessamentoFaltaPerfilDefaultColl);
            #endregion

            if (persiste)
                preProcessamentoColl.Save();

            return preProcessamentoColl;
        }

        public void DeletaTabelasChaves(int chave)
        {
            PreProcessamentoClienteCollection preProcessamentoClienteColl = new PreProcessamentoClienteCollection();
            preProcessamentoClienteColl.Query.Where(preProcessamentoClienteColl.Query.Chave.Equal(chave));

            if (preProcessamentoClienteColl.Query.Load())
            {
                preProcessamentoClienteColl.MarkAllAsDeleted();
                preProcessamentoClienteColl.Save();
            }

            PreProcessamentoCollection preProcessamentoColl = new PreProcessamentoCollection();
            preProcessamentoColl.Query.Where(preProcessamentoColl.Query.Chave.Equal(chave));

            if (preProcessamentoColl.Query.Load())
            {
                preProcessamentoColl.MarkAllAsDeleted();
                preProcessamentoColl.Save();
            }

            PreProcessamentoHistoricoCotaCollection preProcHistoricoCotaColl = new PreProcessamentoHistoricoCotaCollection();
            preProcHistoricoCotaColl.Query.Where(preProcHistoricoCotaColl.Query.Chave.Equal(chave));

            if (preProcHistoricoCotaColl.Query.Load())
            {
                preProcHistoricoCotaColl.MarkAllAsDeleted();
                preProcHistoricoCotaColl.Save();
            }
        }

        private CotacaoData RetornaDataCotacao(int idIndice, DateTime data)
        {
            CotacaoData cotacaoData = new CotacaoData();
            DateTime dataCotacao = new DateTime();

            if (idIndice == ListaIndiceFixo.IPCA || idIndice == ListaIndiceFixo.IGPM)
                dataCotacao = new DateTime(data.Year, data.Month, 1);
            else if (idIndice == ListaIndiceFixo.CDI)
                dataCotacao = Calendario.SubtraiDiaUtil(data, 1);
            else
                dataCotacao = data;

            cotacaoData.IdIndice = idIndice;
            cotacaoData.DataCotacao = dataCotacao;

            return cotacaoData;

        }

    }
}
