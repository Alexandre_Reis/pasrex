﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class CalculoProvisaoHistoricoCollection : esCalculoProvisaoHistoricoCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(CalculoProvisaoHistoricoCollection));

        /// <summary>
        /// Carrega o objeto CalculoProvisaoHistoricoCollection com todos os campos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>bool indicando se achou registro.</returns>
        public bool BuscaCalculoProvisaoHistoricoCompleta(int idCarteira, DateTime dataHistorico)
        {

            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.QueryReset();
            this.Query
                 .Where(this.query.DataHistorico.Equal(dataHistorico),
                        this.query.IdCarteira == idCarteira);

            bool retorno = this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@DataHistorico1", "'" + dataHistorico + "'");
                sql = sql.Replace("@IdCarteira2", "'" + idCarteira + "'");
                log.Info(sql);
            }
            #endregion

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            return retorno;
        }

        /// <summary>
        /// Deleta todas as posições históricas com data >= que a data passada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaCalculoProvisaoHistoricoDataHistoricoMaiorIgual(int idCarteira, DateTime dataHistorico)
        {

            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.QueryReset();
            this.Query
                    .Select(this.Query.IdTabela, this.query.DataHistorico)
                    .Where(this.Query.IdCarteira == idCarteira,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = "Delete: " + this.Query.es.LastQuery;
                sql = sql.Replace("@IdCarteira1", "'" + idCarteira + "'");
                sql = sql.Replace("@DataHistorico2", "'" + dataHistorico + "'");
                log.Info(sql);
            }
            #endregion

            this.MarkAllAsDeleted();
            this.Save();

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }

	}
}
