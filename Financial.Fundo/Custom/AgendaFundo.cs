using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista;

namespace Financial.Fundo
{
	public partial class AgendaFundo : esAgendaFundo
	{
        /// <summary>
        /// Processa eventos de comecotas listados na tabela AgendaEventos.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaEventoComeCotas(int idCliente, DateTime data)
        {
            AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();

            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
            AgendaFundoQuery agendaFundoQuery = new AgendaFundoQuery("A");

            agendaFundoQuery.Select(agendaFundoQuery.IdCarteira);
            agendaFundoQuery.InnerJoin(posicaoFundoQuery).On(posicaoFundoQuery.IdCarteira == agendaFundoQuery.IdCarteira);
            agendaFundoQuery.Where(agendaFundoQuery.DataEvento.Equal(data),
                                   agendaFundoQuery.TipoEvento.Equal((byte)TipoEventoFundo.ComeCotas),
                                   posicaoFundoQuery.IdCliente.Equal(idCliente),
                                   posicaoFundoQuery.Quantidade.NotEqual(0));
            agendaFundoQuery.es.Distinct = true;

            agendaFundoCollection.Load(agendaFundoQuery);

            foreach (AgendaFundo agendaFundo in agendaFundoCollection)
            {
                int idCarteira = agendaFundo.IdCarteira.Value;

                PosicaoFundo posicaoFundo = new PosicaoFundo();
                posicaoFundo.ProcessaComeCotas(idCliente, idCarteira, data);
            }
        }
	}
}
