﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class PosicaoFundoAberturaCollection : esPosicaoFundoAberturaCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(PosicaoFundoAberturaCollection));

        public PosicaoFundoAberturaCollection(PosicaoFundoCollection posicaoFundoCollection, DateTime dataHistorico)
        {
            for (int i = 0; i < posicaoFundoCollection.Count; i++)
            {
                //
                PosicaoFundoAbertura p = new PosicaoFundoAbertura();

                // Para cada Coluna de PosicaoFundo copia para PosicaoFundo
                foreach (esColumnMetadata colPosicao in posicaoFundoCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas menos a Data 
                    esColumnMetadata colPosicaoFundo = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                    if (posicaoFundoCollection[i].GetColumn(colPosicao.Name) != null)
                    {
                        p.SetColumn(colPosicaoFundo.Name, posicaoFundoCollection[i].GetColumn(colPosicao.Name));
                    }
                    p.DataHistorico = dataHistorico;
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Deleta posições de fundo históricas.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoFundoAberturaDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoFundoAberturaCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();         
        }
	}
}
