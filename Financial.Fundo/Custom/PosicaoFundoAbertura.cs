﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common.Enums;
using Financial.Common;
using Financial.Investidor;

namespace Financial.Fundo
{
	public partial class PosicaoFundoAbertura : esPosicaoFundoAbertura
	{
        /// <summary>
        /// Retorna o valor de mercado liquido do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercadoLiquido(int idCliente, int idMoedaCliente, DateTime data) {
            PosicaoFundoAberturaCollection posicaoFundoCollection = new PosicaoFundoAberturaCollection();
            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorLiquido.Sum());
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente == idCliente &&
                                               posicaoFundoCollection.Query.DataHistorico == data);

            posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCarteira);
            posicaoFundoCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoFundoCollection.Count > 0) {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoFundoAbertura posicaoFundo in posicaoFundoCollection) {
                int idCarteira = posicaoFundo.IdCarteira.Value;
                decimal valor = posicaoFundo.ValorLiquido.Value;

                Cliente clienteFundo = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteFundo.Query.IdMoeda);
                clienteFundo.LoadByPrimaryKey(campos, idCarteira);
                int idMoedaFundo = clienteFundo.IdMoeda.Value;

                if (idMoedaCliente != idMoedaFundo) {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaFundo == (int)ListaMoedaFixo.Real) {
                        fatorConversao = 1 / ptax;
                    }
                    else {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaFundo));
                        if (conversaoMoeda.Query.Load()) {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide) {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime data) {
            PosicaoFundoAberturaCollection posicaoFundoCollection = new PosicaoFundoAberturaCollection();
            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorBruto.Sum());
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente == idCliente);
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.DataHistorico == data);

            posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCarteira);
            posicaoFundoCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoFundoCollection.Count > 0) {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoFundoAbertura posicaoFundo in posicaoFundoCollection) {
                int idCarteira = posicaoFundo.IdCarteira.Value;
                decimal valor = posicaoFundo.ValorBruto.Value;

                Cliente clienteFundo = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteFundo.Query.IdMoeda);
                clienteFundo.LoadByPrimaryKey(campos, idCarteira);
                int idMoedaFundo = clienteFundo.IdMoeda.Value;

                if (idMoedaCliente != idMoedaFundo) {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaFundo == (int)ListaMoedaFixo.Real) {
                        fatorConversao = 1 / ptax;
                    }
                    else {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaFundo));
                        if (conversaoMoeda.Query.Load()) {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide) {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }
	}
}
