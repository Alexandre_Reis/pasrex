﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/01/2015 11:43:23
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.InvestidorCotista;
using Financial.Common.Enums;
using Financial.Util;

namespace Financial.Fundo
{
    public partial class ColagemResgate : esColagemResgate
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="novoStatus"></param>
        public void MudaFlagProcessado(int idCarteira, DateTime data, string tipoRegistro, string novoStatus)
        {
            ColagemResgateCollection colagemResgateColl = new ColagemResgateCollection();
            colagemResgateColl.Query.Where(colagemResgateColl.Query.IdCarteira.Equal(idCarteira) | colagemResgateColl.Query.IdCliente.Equal(idCarteira));
            colagemResgateColl.Query.Where(colagemResgateColl.Query.DataReferencia.Equal(data));

            if (!string.IsNullOrEmpty(tipoRegistro))
                colagemResgateColl.Query.Where(colagemResgateColl.Query.TipoRegistro.Equal(tipoRegistro));

            if (colagemResgateColl.Query.Load())
            {
                foreach (ColagemResgate colagem in colagemResgateColl)
                {
                    colagem.Processado = novoStatus;
                }

                colagemResgateColl.Save();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool VerificaSeExisteValoresColadosResgates(int idCarteira, DateTime data)
        {
            bool passivo = VerificaSeExisteValoresColadosRegPassivo(idCarteira, data);
            bool ativo = VerificaSeExisteValoresColadosRegAtivo(idCarteira, data);

            return (passivo || ativo);
        }

        public bool VerificaSeExisteValoresColadosRegPassivo(int idCarteira, DateTime data)
        {
            ColagemResgateCollection colagemResgateColl = new ColagemResgateCollection();
            colagemResgateColl.Query.Where(colagemResgateColl.Query.TipoRegistro.Equal("P") & colagemResgateColl.Query.IdCarteira.Equal(idCarteira));
            colagemResgateColl.Query.Where(colagemResgateColl.Query.DataReferencia.Equal(data));

            return colagemResgateColl.Query.Load();
        }

        public bool VerificaSeExisteValoresColadosRegAtivo(int idCarteira, DateTime data)
        {
            ColagemResgateCollection colagemResgateColl = new ColagemResgateCollection();
            colagemResgateColl.Query.Where(colagemResgateColl.Query.TipoRegistro.Equal("A") & colagemResgateColl.Query.IdCarteira.Equal(idCarteira));
            colagemResgateColl.Query.Where(colagemResgateColl.Query.DataReferencia.Equal(data));

            return colagemResgateColl.Query.Load();
        }

        #region Parte Cotista
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        static public List<int> RetornaTipoOperacaoCotista()
        {
            List<int> lstTipoOperacao = new List<int>();
            lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateBruto);
            lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateCotas);
            lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateCotasEspecial);
            lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateLiquido);
            lstTipoOperacao.Add((int)TipoOperacaoCotista.ResgateTotal);

            return lstTipoOperacao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="colagemResgateColl"></param>
        /// <param name="existemDetalhes"></param>
        public void ColaValoresCotista(int idCarteira, DateTime data, ColagemResgateCollection colagemResgateColl, bool existemDetalhes)
        {
            Carteira carteira = new Carteira();
            List<OperacaoCotista> lstOperacaoCotista = new List<OperacaoCotista>();
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            ColagemResgateMapaCotistaCollection colagemResgateMapaColl = new ColagemResgateMapaCotistaCollection();

            #region Deleta Operações de resgate
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira)
                                            & operacaoCotistaCollection.Query.DataConversao.Equal(data)
                                            & operacaoCotistaCollection.Query.ValoresColados.Equal("N")
                                            & operacaoCotistaCollection.Query.TipoOperacao.In(RetornaTipoOperacaoCotista().ToArray()));

            if (operacaoCotistaCollection.Query.Load())
            {
                operacaoCotistaCollection.MarkAllAsDeleted();
                operacaoCotistaCollection.Save();
            }
            #endregion

            carteira.LoadByPrimaryKey(idCarteira);

            #region Busca local de Feriado
            int? idLocalFeriado = carteira.UpToCliente.IdLocalNegociacao.GetValueOrDefault(0);
            #endregion

            foreach (ColagemResgate colagemCotista in colagemResgateColl)
            {
                DateTime dataConversao = colagemCotista.DataConversao.Value;
                DateTime dataOperacao = colagemCotista.DataReferencia.Value;
                DateTime dataLiquidacao = colagemCotista.DataConversao.Value;
                OperacaoCotista operacaoCotista = new OperacaoCotista();
                operacaoCotista.CotaInformada = operacaoCotista.CotaOperacao = colagemCotista.ValorBruto.Value / colagemCotista.Quantidade.Value;
                operacaoCotista.DataAgendamento = operacaoCotista.DataConversao = dataConversao;
                operacaoCotista.DataOperacao = dataOperacao;
                operacaoCotista.DataRegistro = dataOperacao;
                operacaoCotista.IdAgenda = null;
                operacaoCotista.IdCarteira = colagemCotista.IdCarteira.Value;
                operacaoCotista.IdCotista = colagemCotista.IdCliente.Value;
                operacaoCotista.IdConta = null;
                operacaoCotista.IdFormaLiquidacao = null;
                operacaoCotista.DepositoRetirada = false;
                operacaoCotista.Observacao = null;
                operacaoCotista.RegistroEditado = "N";
                operacaoCotista.FieModalidade = null;
                operacaoCotista.IdLocalNegociacao = (int)LocalNegociacaoFixo.Brasil;
                operacaoCotista.FieTabelaIr = null;
                operacaoCotista.Observacao = string.Empty;
                operacaoCotista.IdOperacaoResgatada = null;
                operacaoCotista.IdPosicaoResgatada = null;
                operacaoCotista.IdOperacao = null;
                operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;

                ParametroLiqAvancadoAnaliticoCollection collLiquidacao = new ParametroLiqAvancadoAnaliticoCollection();
                DateTime dataVigente = collLiquidacao.RetornaUltDataVigente(operacaoCotista.DataOperacao.Value, idCarteira, (int)TipoParametroLiqOperacao.Resgate);
                if (dataVigente != new DateTime())
                {
                    collLiquidacao = new ParametroLiqAvancadoAnaliticoCollection();
                    collLiquidacao = collLiquidacao.RetornaParametrosNaDataVigente(dataVigente, idCarteira, (int)TipoParametroLiqOperacao.Resgate, (int)TipoParametroLiquidacao.DataLiquidacao);

                    if (collLiquidacao.Count > 0)
                        dataLiquidacao = collLiquidacao.RetornaDataCalculada(dataOperacao, dataConversao, collLiquidacao, (int)TipoParametroLiquidacao.DataLiquidacao, 0, idLocalFeriado);
                }
                else
                    dataLiquidacao = Calendario.AdicionaDiaUtil(dataConversao, carteira.DiasLiquidacaoResgate.Value);

                operacaoCotista.DataLiquidacao = dataLiquidacao;

                operacaoCotista.Quantidade = colagemCotista.Quantidade.Value;
                operacaoCotista.PrejuizoUsado = colagemCotista.PrejuizoUsado.Value;
                operacaoCotista.RendimentoResgate = colagemCotista.RendimentoResgate.Value;
                operacaoCotista.ValorBruto = colagemCotista.ValorBruto.Value;
                operacaoCotista.ValorCPMF = colagemCotista.ValorCPMF.Value;
                operacaoCotista.ValorIOF = colagemCotista.ValorIOF.Value;
                operacaoCotista.ValorIR = colagemCotista.ValorIR.Value;
                operacaoCotista.ValorLiquido = colagemCotista.ValorLiquido.Value;
                operacaoCotista.ValorPerformance = colagemCotista.ValorPerformance.Value;
                operacaoCotista.VariacaoResgate = colagemCotista.VariacaoResgate.Value;
                operacaoCotista.ValoresColados = "S";
                operacaoCotista.TipoOperacao = (byte)TipoOperacaoCotista.ResgateCotas;
                operacaoCotista.TipoResgate = (existemDetalhes) ? (byte)TipoResgateCotista.Colagem : (byte)TipoResgateCotista.FIFO;

                operacaoCotista.Save();

                #region Grava Mapeamento
                ColagemResgateMapaCotista colagemResgateMapaCotista = colagemResgateMapaColl.AddNew();
                colagemResgateMapaCotista.DataReferencia = data;
                colagemResgateMapaCotista.IdCarteira = colagemCotista.IdCarteira.Value;
                colagemResgateMapaCotista.IdCliente = colagemCotista.IdCliente.Value;
                colagemResgateMapaCotista.DataConversao = colagemCotista.DataConversao;
                colagemResgateMapaCotista.IdOperacaoResgateVinculada = operacaoCotista.IdOperacao.Value;
                colagemResgateMapaCotista.DetalheImportado = (existemDetalhes) ? "S" : "N";

                if (existemDetalhes)
                    colagemResgateMapaCotista.IdColagemResgateDetalhe = Convert.ToInt32(colagemCotista.GetColumn("IdColagemResgateDetalhe"));
                #endregion

                colagemCotista.Processado = "S";
            }

            colagemResgateMapaColl.Save();
            colagemResgateColl.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="idOperacaoResgate"></param>
        /// <returns></returns>
        public PosicaoCotistaCollection BuscaPosicoesParaResgateCotista(int idCarteira, DateTime data, int idOperacaoResgate)
        {
            #region Objetos
            ColagemResgateDetalheQuery colaResgateDtlQuery = new ColagemResgateDetalheQuery("detalhe");
            ColagemResgateDetalheCollection colaResgateDtlColl = new ColagemResgateDetalheCollection();

            ColagemResgateDetalhePosicaoQuery colaResgateDtlPosQuery = new ColagemResgateDetalhePosicaoQuery("posicao");
            ColagemResgateDetalhePosicaoCollection colaResgateDtlPosColl = new ColagemResgateDetalhePosicaoCollection();
            List<ColagemResgateDetalhePosicao> lstColaResgateDtlPos = new List<ColagemResgateDetalhePosicao>();

            ColagemOperResgPosCotistaCollection colaOperResgPosAfetadaColl = new ColagemOperResgPosCotistaCollection();
            ColagemResgateMapaCotistaCollection colaResgateMapaColl = new ColagemResgateMapaCotistaCollection();
            PosicaoCotistaCollection posCotistaRetornoCollection = new PosicaoCotistaCollection();
            List<PosicaoCotista> lstPosicaoCostita = new List<PosicaoCotista>();
            List<int> lstIdPosicoesDebitadas = new List<int>();
            #endregion

            #region Carrega Operação
            OperacaoCotista operacaoCotista = new OperacaoCotista();
            operacaoCotista.LoadByPrimaryKey(idOperacaoResgate);
            #endregion

            #region Carrega Mapa e Detalhe
            colaResgateMapaColl.Query.Where(colaResgateMapaColl.Query.IdOperacaoResgateVinculada.Equal(idOperacaoResgate)
                                               & colaResgateMapaColl.Query.DataReferencia.Equal(data));
            colaResgateMapaColl.Query.Load();

            int idCliente = colaResgateMapaColl[0].IdCliente.Value;
            int idColagemResgateDetalhe = colaResgateMapaColl[0].IdColagemResgateDetalhe.Value;
            DateTime dataReferencia = colaResgateMapaColl[0].DataReferencia.Value;

            colaResgateDtlColl.Query.Where(colaResgateDtlColl.Query.IdCarteira.Equal(idCarteira)
                                                & colaResgateDtlColl.Query.IdCliente.Equal(idCliente)
                                                & colaResgateDtlColl.Query.DataReferencia.Equal(dataReferencia)
                                                & colaResgateDtlColl.Query.IdColagemResgateDetalhe.Equal(idColagemResgateDetalhe));

            colaResgateDtlColl.Query.Load();
            #endregion

            #region Carrega Carteira
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            #endregion

            #region Carrega Posicao dos detalhes colados
            colaResgateDtlPosQuery.InnerJoin(colaResgateDtlQuery).On(colaResgateDtlQuery.IdColagemResgateDetalhe.Equal(colaResgateDtlPosQuery.IdColagemResgateDetalhe));
            colaResgateDtlPosQuery.Where(colaResgateDtlQuery.IdCarteira.Equal(idCarteira)
                                                    & colaResgateDtlQuery.IdCliente.Equal(idCliente)
                                                    & colaResgateDtlQuery.DataReferencia.Equal(dataReferencia));

            colaResgateDtlPosColl.Load(colaResgateDtlPosQuery);
            #endregion

            #region Carrega as posições candidatas para consumir a demanda dos detalhes
            decimal qtdeNecessaria = operacaoCotista.Quantidade.Value;
            foreach (ColagemResgateDetalhe colaResgateDtl in colaResgateDtlColl)
            {
                DateTime dataConversao = colaResgateDtl.DataConversao.Value;
                decimal qtdeColada = 0;

                #region Busca/Cria colagem resgate Posicao
                ColagemResgateDetalhePosicao colaResgatePos = ((List<ColagemResgateDetalhePosicao>)colaResgateDtlPosColl).Find((delegate(ColagemResgateDetalhePosicao x) { return x.IdColagemResgateDetalhe == colaResgateDtl.IdColagemResgateDetalhe; }));
                if (colaResgatePos == null || (colaResgatePos != null && colaResgatePos.IdColagemResgateDetalhe.HasValue && colaResgatePos.IdColagemResgateDetalhe.Value == 0))
                {
                    colaResgatePos = colaResgateDtlPosColl.AddNew();
                    colaResgatePos.QuantidadeRestante = colaResgateDtl.Quantidade.Value;
                    colaResgatePos.QuantidadeInicial = colaResgateDtl.Quantidade.Value;
                    colaResgatePos.DataReferencia = data;
                    colaResgatePos.IdColagemResgateDetalhe = colaResgateDtl.IdColagemResgateDetalhe.Value;
                }
                #endregion

                if (colaResgatePos.QuantidadeRestante.Value == 0)
                    continue;

                PosicaoCotistaCollection posicaoCotistaColl = BuscaPosicoesCotista(idCarteira, idCliente, dataConversao);
                if (posicaoCotistaColl.Count > 0)
                {
                    #region Seta quantidade
                    foreach (PosicaoCotista posicaoCotista in posicaoCotistaColl)
                    {
                        if (posicaoCotista.Quantidade > colaResgatePos.QuantidadeRestante.Value)
                            qtdeColada = colaResgatePos.QuantidadeRestante.Value;
                        else
                            qtdeColada = posicaoCotista.Quantidade.Value;

                        if (qtdeColada > qtdeNecessaria)
                            qtdeColada = qtdeNecessaria;

                        ColagemOperResgPosCotista colaOperPosicaoAfetada = colaOperResgPosAfetadaColl.AddNew();
                        colaOperPosicaoAfetada.DataReferencia = data;
                        colaOperPosicaoAfetada.IdCarteira = idCarteira;
                        colaOperPosicaoAfetada.IdCliente = operacaoCotista.IdCotista.Value;
                        colaOperPosicaoAfetada.IdOperacaoResgateVinculada = idOperacaoResgate;
                        colaOperPosicaoAfetada.IdPosicaoVinculada = posicaoCotista.IdPosicao.Value;
                        colaOperPosicaoAfetada.QuantidadeVinculada = qtdeColada;
                        colaOperPosicaoAfetada.IdColagemResgateDetalhe = colaResgateDtl.IdColagemResgateDetalhe.Value;

                        colaResgatePos.QuantidadeRestante -= qtdeColada;
                        qtdeNecessaria -= qtdeColada;
                        posCotistaRetornoCollection.AttachEntity(posicaoCotista);
                        lstIdPosicoesDebitadas.Add(posicaoCotista.IdPosicao.Value);
                        if (colaResgatePos.QuantidadeRestante == 0)
                            break;
                    }
                    #endregion
                }

                if (qtdeNecessaria != 0)
                {
                    string mensagem = "Qtde indisponível para cotizar o Resgate (Cotista - Colado) ";
                    mensagem += "Id.Operação: " + idOperacaoResgate;
                    mensagem += "Conversão Especifica: " + String.Format("{0:dd/MM/yyyy}", colaResgateDtl.DataConversao.Value);
                    mensagem += "- Ativo: " + carteira.Nome.Trim();

                    throw new Exception(mensagem);
                }
            }
            #endregion

            colaResgateDtlPosColl.Save();
            colaOperResgPosAfetadaColl.Save();

            return posCotistaRetornoCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCliente"></param>
        /// <param name="dataConversao"></param>
        /// <returns></returns>
        private PosicaoCotistaCollection BuscaPosicoesCotista(int idCarteira, int idCliente, DateTime dataConversao)
        {
            PosicaoCotistaCollection posicaoColl = new PosicaoCotistaCollection();

            posicaoColl.Query.Select(posicaoColl.Query.IdPosicao, posicaoColl.Query.IdOperacao, posicaoColl.Query.DataAplicacao, posicaoColl.Query.DataConversao,
                         posicaoColl.Query.CotaAplicacao, posicaoColl.Query.CotaDia, posicaoColl.Query.Quantidade, posicaoColl.Query.QuantidadeBloqueada,
                         posicaoColl.Query.DataUltimaCobrancaIR, posicaoColl.Query.ValorIOFVirtual,
                         posicaoColl.Query.ValorBruto, posicaoColl.Query.ValorLiquido,
                         posicaoColl.Query.ValorIOF, posicaoColl.Query.ValorIR, posicaoColl.Query.ValorPerformance,
                         posicaoColl.Query.QuantidadeAntesCortes, posicaoColl.Query.DataUltimoCortePfee,
                         posicaoColl.Query.PosicaoIncorporada);
            posicaoColl.Query.Where(posicaoColl.Query.IdCarteira.Equal(idCarteira)
                                    & posicaoColl.Query.IdCotista.Equal(idCliente)
                                    & posicaoColl.Query.DataConversao.Equal(dataConversao)
                                    & posicaoColl.Query.Quantidade.NotEqual(0));
            posicaoColl.Query.OrderBy(posicaoColl.Query.Quantidade.Descending);

            posicaoColl.Query.Load();
            
            return (posicaoColl.Count > 0 ? posicaoColl : new PosicaoCotistaCollection());
        }
        #endregion

        #region Parte Fundo
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        static public List<int> RetornaTipoOperacaoFundo()
        {
            List<int> lstTipoOperacao = new List<int>();
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateBruto);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateCotas);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateCotasEspecial);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateLiquido);
            lstTipoOperacao.Add((int)TipoOperacaoFundo.ResgateTotal);

            return lstTipoOperacao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="colagemResgateColl"></param>
        /// <param name="existemDetalhes"></param>
        public void ColaValoresFundo(int idCarteira, DateTime data, ColagemResgateCollection colagemResgateColl, bool existemDetalhes)
        {
            Carteira carteira = new Carteira();
            List<OperacaoFundo> lstOperacaoFundo = new List<OperacaoFundo>();
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            ColagemResgateMapaFundoCollection colagemResgateMapaColl = new ColagemResgateMapaFundoCollection();

            #region Deleta Operações de resgate
            operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCarteira)
                                            & operacaoFundoCollection.Query.DataConversao.Equal(data)
                                            & operacaoFundoCollection.Query.ValoresColados.Equal("N")
                                            & operacaoFundoCollection.Query.TipoOperacao.In(RetornaTipoOperacaoFundo().ToArray()));

            if (operacaoFundoCollection.Query.Load())
            {
                operacaoFundoCollection.MarkAllAsDeleted();
                operacaoFundoCollection.Save();
            }
            #endregion

            carteira.LoadByPrimaryKey(idCarteira);

            #region Busca local de Feriado
            int? idLocalFeriado = carteira.UpToCliente.IdLocalNegociacao.GetValueOrDefault(0);
            #endregion

            foreach (ColagemResgate colagemFundo in colagemResgateColl)
            {
                DateTime dataConversao = colagemFundo.DataConversao.Value;
                DateTime dataOperacao = colagemFundo.DataReferencia.Value;
                DateTime dataLiquidacao = colagemFundo.DataConversao.Value;
                OperacaoFundo operacaoFundo = new OperacaoFundo();
                operacaoFundo.CotaInformada = operacaoFundo.CotaOperacao = colagemFundo.ValorBruto.Value / colagemFundo.Quantidade.Value;
                operacaoFundo.DataAgendamento = operacaoFundo.DataConversao = dataConversao;
                operacaoFundo.DataOperacao = dataOperacao;
                operacaoFundo.DataRegistro = dataOperacao;
                operacaoFundo.IdAgenda = null;
                operacaoFundo.IdCarteira = colagemFundo.IdCarteira.Value;
                operacaoFundo.IdCliente = colagemFundo.IdCliente.Value;
                operacaoFundo.IdConta = null;
                operacaoFundo.IdFormaLiquidacao = null;
                operacaoFundo.DepositoRetirada = false;
                operacaoFundo.Observacao = null;
                operacaoFundo.RegistroEditado = "N";
                operacaoFundo.FieModalidade = null;
                operacaoFundo.IdLocalNegociacao = (int)LocalNegociacaoFixo.Brasil;
                operacaoFundo.FieTabelaIr = null;
                operacaoFundo.Observacao = string.Empty;
                operacaoFundo.IdOperacaoResgatada = null;
                operacaoFundo.IdPosicaoResgatada = null;
                operacaoFundo.IdOperacao = null;
                operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;

                ParametroLiqAvancadoAnaliticoCollection collLiquidacao = new ParametroLiqAvancadoAnaliticoCollection();
                DateTime dataVigente = collLiquidacao.RetornaUltDataVigente(operacaoFundo.DataOperacao.Value, idCarteira, (int)TipoParametroLiqOperacao.Resgate);
                if (dataVigente != new DateTime())
                {
                    collLiquidacao = new ParametroLiqAvancadoAnaliticoCollection();
                    collLiquidacao = collLiquidacao.RetornaParametrosNaDataVigente(dataVigente, idCarteira, (int)TipoParametroLiqOperacao.Resgate, (int)TipoParametroLiquidacao.DataLiquidacao);

                    if (collLiquidacao.Count > 0)
                        dataLiquidacao = collLiquidacao.RetornaDataCalculada(dataOperacao, dataConversao, collLiquidacao, (int)TipoParametroLiquidacao.DataLiquidacao, 0, idLocalFeriado);
                }
                else
                    dataLiquidacao = Calendario.AdicionaDiaUtil(dataConversao, carteira.DiasLiquidacaoResgate.Value);

                operacaoFundo.DataLiquidacao = dataLiquidacao;

                operacaoFundo.Quantidade = colagemFundo.Quantidade.Value;
                operacaoFundo.PrejuizoUsado = colagemFundo.PrejuizoUsado.Value;
                operacaoFundo.RendimentoResgate = colagemFundo.RendimentoResgate.Value;
                operacaoFundo.ValorBruto = colagemFundo.ValorBruto.Value;
                operacaoFundo.ValorCPMF = colagemFundo.ValorCPMF.Value;
                operacaoFundo.ValorIOF = colagemFundo.ValorIOF.Value;
                operacaoFundo.ValorIR = colagemFundo.ValorIR.Value;
                operacaoFundo.ValorLiquido = colagemFundo.ValorLiquido.Value;
                operacaoFundo.ValorPerformance = colagemFundo.ValorPerformance.Value;
                operacaoFundo.VariacaoResgate = colagemFundo.VariacaoResgate.Value;
                operacaoFundo.ValoresColados = "S";
                operacaoFundo.TipoOperacao = (byte)TipoOperacaoFundo.ResgateCotas;
                operacaoFundo.TipoResgate = (existemDetalhes) ? (byte)TipoResgateFundo.Colagem : (byte)TipoResgateFundo.FIFO;

                operacaoFundo.Save();

                #region Grava Mapeamento
                ColagemResgateMapaFundo colagemResgateMapaFundo = colagemResgateMapaColl.AddNew();
                colagemResgateMapaFundo.DataReferencia = data;
                colagemResgateMapaFundo.IdCarteira = colagemFundo.IdCarteira.Value;
                colagemResgateMapaFundo.IdCliente = colagemFundo.IdCliente.Value;
                colagemResgateMapaFundo.DataConversao = colagemFundo.DataConversao;
                colagemResgateMapaFundo.IdOperacaoResgateVinculada = operacaoFundo.IdOperacao.Value;
                colagemResgateMapaFundo.DetalheImportado = (existemDetalhes) ? "S" : "N";

                if (existemDetalhes)                
                    colagemResgateMapaFundo.IdColagemResgateDetalhe = Convert.ToInt32(colagemFundo.GetColumn("IdColagemResgateDetalhe"));                
                #endregion

                colagemFundo.Processado = "S";
            }

            colagemResgateMapaColl.Save();
            colagemResgateColl.Save();
        }    

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="idOperacaoResgate"></param>
        /// <returns></returns>
        public PosicaoFundoCollection BuscaPosicoesParaResgateFundo(int idCliente, DateTime data, int idOperacaoResgate)
        {
            #region Objetos
            ColagemResgateDetalheQuery colaResgateDtlQuery = new ColagemResgateDetalheQuery("detalhe");
            ColagemResgateDetalheCollection colaResgateDtlColl = new ColagemResgateDetalheCollection();

            ColagemResgateDetalhePosicaoQuery colaResgateDtlPosQuery = new ColagemResgateDetalhePosicaoQuery("posicao");
            ColagemResgateDetalhePosicaoCollection colaResgateDtlPosColl = new ColagemResgateDetalhePosicaoCollection();
            List<ColagemResgateDetalhePosicao> lstColaResgateDtlPos = new List<ColagemResgateDetalhePosicao>();

            ColagemOperResgPosFundoCollection colaOperResgPosAfetadaColl = new ColagemOperResgPosFundoCollection();
            ColagemResgateMapaFundoCollection colaResgateMapaColl = new ColagemResgateMapaFundoCollection();
            PosicaoFundoCollection posFundoRetornoCollection = new PosicaoFundoCollection();
            List<PosicaoFundo> lstPosicaoCostita = new List<PosicaoFundo>();
            List<int> lstIdPosicoesDebitadas = new List<int>();
            #endregion

            #region Carrega Operação
            OperacaoFundo operacaoFundo = new OperacaoFundo();
            operacaoFundo.LoadByPrimaryKey(idOperacaoResgate);
            #endregion

            #region Carrega Mapa e Detalhe
            colaResgateMapaColl.Query.Where(colaResgateMapaColl.Query.IdOperacaoResgateVinculada.Equal(idOperacaoResgate)
                                               & colaResgateMapaColl.Query.DataReferencia.Equal(data));
            colaResgateMapaColl.Query.Load();

            int idCarteira = colaResgateMapaColl[0].IdCarteira.Value;
            int idColagemResgateDetalhe = colaResgateMapaColl[0].IdColagemResgateDetalhe.Value;
            DateTime dataReferencia = colaResgateMapaColl[0].DataReferencia.Value;

            colaResgateDtlColl.Query.Where(colaResgateDtlColl.Query.IdCarteira.Equal(idCarteira)
                                                & colaResgateDtlColl.Query.IdCliente.Equal(idCliente)
                                                & colaResgateDtlColl.Query.DataReferencia.Equal(dataReferencia)
                                                & colaResgateDtlColl.Query.IdColagemResgateDetalhe.Equal(idColagemResgateDetalhe));

            colaResgateDtlColl.Query.Load();
            #endregion

            #region Carrega Carteira
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            #endregion

            #region Carrega Posicao dos detalhes colados
            colaResgateDtlPosQuery.InnerJoin(colaResgateDtlQuery).On(colaResgateDtlQuery.IdColagemResgateDetalhe.Equal(colaResgateDtlPosQuery.IdColagemResgateDetalhe));
            colaResgateDtlPosQuery.Where(colaResgateDtlQuery.IdCarteira.Equal(idCarteira)
                                                    & colaResgateDtlQuery.IdCliente.Equal(idCliente)
                                                    & colaResgateDtlQuery.DataReferencia.Equal(dataReferencia));

            colaResgateDtlPosColl.Load(colaResgateDtlPosQuery);
            #endregion

            #region Carrega as posições candidatas para consumir a demanda dos detalhes
            decimal qtdeNecessaria = operacaoFundo.Quantidade.Value;
            foreach (ColagemResgateDetalhe colaResgateDtl in colaResgateDtlColl)
            {
                DateTime dataConversao = colaResgateDtl.DataConversao.Value;
                decimal qtdeColada = 0;

                #region Busca/Cria colagem resgate Posicao
                ColagemResgateDetalhePosicao colaResgatePos = ((List<ColagemResgateDetalhePosicao>)colaResgateDtlPosColl).Find((delegate(ColagemResgateDetalhePosicao x) { return x.IdColagemResgateDetalhe == colaResgateDtl.IdColagemResgateDetalhe; }));
                if (colaResgatePos == null || (colaResgatePos != null && colaResgatePos.IdColagemResgateDetalhe.HasValue && colaResgatePos.IdColagemResgateDetalhe.Value == 0))
                {
                    colaResgatePos = colaResgateDtlPosColl.AddNew();
                    colaResgatePos.QuantidadeRestante = colaResgateDtl.Quantidade.Value;
                    colaResgatePos.QuantidadeInicial = colaResgateDtl.Quantidade.Value;
                    colaResgatePos.DataReferencia = data;
                    colaResgatePos.IdColagemResgateDetalhe = colaResgateDtl.IdColagemResgateDetalhe.Value;
                }
                #endregion

                if (colaResgatePos.QuantidadeRestante.Value == 0)
                    continue;

                PosicaoFundoCollection posicaoFundoColl = BuscaPosicoesFundo(idCarteira, idCliente, dataConversao);
                if (posicaoFundoColl.Count > 0)
                {
                    #region Seta quantidade
                    foreach (PosicaoFundo posicaoFundo in posicaoFundoColl)
                    {
                        if (posicaoFundo.Quantidade > colaResgatePos.QuantidadeRestante.Value)
                            qtdeColada = colaResgatePos.QuantidadeRestante.Value;
                        else
                            qtdeColada = posicaoFundo.Quantidade.Value;

                        if (qtdeColada > qtdeNecessaria)
                            qtdeColada = qtdeNecessaria;

                        ColagemOperResgPosFundo colaOperPosicaoAfetada = colaOperResgPosAfetadaColl.AddNew();
                        colaOperPosicaoAfetada.DataReferencia = data;
                        colaOperPosicaoAfetada.IdCarteira = idCarteira;
                        colaOperPosicaoAfetada.IdCliente = idCliente;
                        colaOperPosicaoAfetada.IdOperacaoResgateVinculada = idOperacaoResgate;
                        colaOperPosicaoAfetada.IdPosicaoVinculada = posicaoFundo.IdPosicao.Value;
                        colaOperPosicaoAfetada.QuantidadeVinculada = qtdeColada;
                        colaOperPosicaoAfetada.IdColagemResgateDetalhe = colaResgateDtl.IdColagemResgateDetalhe.Value;

                        colaResgatePos.QuantidadeRestante -= qtdeColada;
                        qtdeNecessaria -= qtdeColada;
                        posFundoRetornoCollection.AttachEntity(posicaoFundo);
                        lstIdPosicoesDebitadas.Add(posicaoFundo.IdPosicao.Value);
                        if (colaResgatePos.QuantidadeRestante == 0)
                            break;
                    }
                    #endregion
                }

                if (qtdeNecessaria != 0)
                {
                    string mensagem = "Qtde indisponível para cotizar o Resgate (Fundo - Colado) ";
                    mensagem += "Id.Operação: " + idOperacaoResgate;
                    mensagem += "Conversão Especifica: " + String.Format("{0:dd/MM/yyyy}", colaResgateDtl.DataConversao.Value);
                    mensagem += "- Ativo: " + carteira.Nome.Trim();

                    throw new Exception(mensagem);
                }
            }
            #endregion           

            colaResgateDtlPosColl.Save();
            colaOperResgPosAfetadaColl.Save();

            return posFundoRetornoCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCliente"></param>
        /// <param name="dataConversao"></param>
        /// <returns></returns>
        private PosicaoFundoCollection BuscaPosicoesFundo(int idCarteira, int idCliente, DateTime dataConversao)
        {
            PosicaoFundoCollection posicaoColl = new PosicaoFundoCollection();

            posicaoColl.Query.Select(posicaoColl.Query.IdPosicao, posicaoColl.Query.IdOperacao, posicaoColl.Query.DataAplicacao, posicaoColl.Query.DataConversao,
                         posicaoColl.Query.CotaAplicacao, posicaoColl.Query.CotaDia, posicaoColl.Query.Quantidade, posicaoColl.Query.QuantidadeBloqueada,
                         posicaoColl.Query.DataUltimaCobrancaIR, posicaoColl.Query.ValorIOFVirtual,
                         posicaoColl.Query.ValorBruto, posicaoColl.Query.ValorLiquido,
                         posicaoColl.Query.ValorIOF, posicaoColl.Query.ValorIR, posicaoColl.Query.ValorPerformance,
                         posicaoColl.Query.QuantidadeAntesCortes, posicaoColl.Query.DataUltimoCortePfee,
                         posicaoColl.Query.PosicaoIncorporada);
            posicaoColl.Query.Where(posicaoColl.Query.IdCarteira.Equal(idCarteira)
                                    & posicaoColl.Query.IdCliente.Equal(idCliente)
                                    & posicaoColl.Query.DataConversao.Equal(dataConversao)
                                    & posicaoColl.Query.Quantidade.NotEqual(0));
            posicaoColl.Query.OrderBy(posicaoColl.Query.Quantidade.Descending);

            posicaoColl.Query.Load();

            return (posicaoColl.Count > 0 ? posicaoColl : new PosicaoFundoCollection());
        }
        #endregion
    }
}
