﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 18/09/2014 13:17:58
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
	public partial class CalculoAdministracaoAssociadaCollection : esCalculoAdministracaoAssociadaCollection
	{
/// <summary>
        /// Construtor
        /// Cria uma nova CalculoAdministracaoAssociadaCollection com os dados de CalculoAdministracaoAssociadaHistoricoCollection
        /// Todos do dados de CalculoAdministracaoAssociadaHistoricoCollection são copiados com excessão da dataHistorico
        /// </summary>
        /// <param name="calculoAdministracaoHistoricoCollection"></param>
        public CalculoAdministracaoAssociadaCollection(CalculoAdministracaoAssociadaHistoricoCollection calculoAdministracaoAssociadaHistoricoCollection) 
        {
            for (int i = 0; i < calculoAdministracaoAssociadaHistoricoCollection.Count; i++) {
                //
                CalculoAdministracaoAssociada c = new CalculoAdministracaoAssociada();

                // Para cada Coluna de CalculoAdministracaoHistorico copia para CalculoAdministracao
                foreach (esColumnMetadata colCalculoAdministracaoAssociadaHistorico in calculoAdministracaoAssociadaHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colCalculoAdministracaoAssociadaHistorico.PropertyName != CalculoAdministracaoAssociadaHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colCalculoAdministracaoAssociada = c.es.Meta.Columns.FindByPropertyName(colCalculoAdministracaoAssociadaHistorico.PropertyName);
                        if (calculoAdministracaoAssociadaHistoricoCollection[i].GetColumn(colCalculoAdministracaoAssociadaHistorico.Name) != null) {
                            c.SetColumn(colCalculoAdministracaoAssociada.Name, calculoAdministracaoAssociadaHistoricoCollection[i].GetColumn(colCalculoAdministracaoAssociadaHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(c);
            }
        }

        /// <summary>
        /// Carrega o objeto CalculoAdministracaoAssociadaCollection com todos os campos de CalculoAdministracaoAssociada.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="dataFimApropriacao">filtra provisões já vencidas (inclusive vcto em dia não útil)</param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaCalculoAdministracaoVencidas(int idCarteira, DateTime dataFimApropriacao)
        {
            CalculoAdministracaoAssociadaQuery calculoAdministracaoAssociadaQuery = new CalculoAdministracaoAssociadaQuery("C");
            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");

            calculoAdministracaoAssociadaQuery.Where(calculoAdministracaoAssociadaQuery.IdCarteira.Equal(idCarteira),
                                       calculoAdministracaoAssociadaQuery.DataFimApropriacao.LessThanOrEqual(dataFimApropriacao),
                                       calculoAdministracaoAssociadaQuery.ValorAcumulado.GreaterThan(0));
            calculoAdministracaoAssociadaQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(calculoAdministracaoAssociadaQuery.IdTabela == tabelaTaxaAdministracaoQuery.IdTabela);

            bool retorno = this.Load(calculoAdministracaoAssociadaQuery);

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto CalculoAdministracaoAssociadaCollection com todos os campos de CalculoAdministracaoAssociada.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaCalculoAdministracaoCompleta(int idCarteira)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCarteira == idCarteira);
            this.Query.Load();
        }
        
        /// <summary>
        /// Deleta todos os registros de cálculo da carteira passada.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaCalculoAdministracao(int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTabela)
                 .Where(this.Query.IdCarteira == idCarteira);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Insere novas registros na CalculoAdministracaoAssociada a partir do histórico
        /// (CalculoAdministracaoAssociadaHistorico) da data informada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void InsereCalculoAdministracaoHistorico(int idCarteira, DateTime data)
        {
            this.QueryReset();
            CalculoAdministracaoAssociadaHistoricoCollection calculoAdministracaoAssociadaHistoricoCollection = new CalculoAdministracaoAssociadaHistoricoCollection();
            calculoAdministracaoAssociadaHistoricoCollection.BuscaCalculoAdministracaoHistoricoCompleta(idCarteira, data);
            for (int i = 0; i < calculoAdministracaoAssociadaHistoricoCollection.Count; i++)
            {
                CalculoAdministracaoAssociadaHistorico calculoAdministracaoAssociadaHistorico = calculoAdministracaoAssociadaHistoricoCollection[i];

                // Copia para CalculoAdministracao
                CalculoAdministracaoAssociada calculoAdministracaoAssociada = new CalculoAdministracaoAssociada();
                calculoAdministracaoAssociada.IdTabela = calculoAdministracaoAssociadaHistorico.IdTabela;
                calculoAdministracaoAssociada.IdCarteira = calculoAdministracaoAssociadaHistorico.IdCarteira;
                calculoAdministracaoAssociada.ValorDia = calculoAdministracaoAssociadaHistorico.ValorDia;
                calculoAdministracaoAssociada.ValorAcumulado = calculoAdministracaoAssociadaHistorico.ValorAcumulado;
                calculoAdministracaoAssociada.DataFimApropriacao = calculoAdministracaoAssociadaHistorico.DataFimApropriacao;
                calculoAdministracaoAssociada.DataPagamento = calculoAdministracaoAssociadaHistorico.DataPagamento;
                calculoAdministracaoAssociada.ValorCPMFDia = calculoAdministracaoAssociadaHistorico.ValorCPMFDia;
                calculoAdministracaoAssociada.ValorCPMFAcumulado = calculoAdministracaoAssociadaHistorico.ValorCPMFAcumulado;
                //
                this.AttachEntity(calculoAdministracaoAssociada);
            }

            this.Save();
        }
	}
}
