﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Investidor;

namespace Financial.Fundo
{
	public partial class HistoricoCotaCollection : esHistoricoCotaCollection
	{
        /// <summary>
        /// Adiciona uma Coluna Nova na Collection
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Deleta todos os registros de cálculo da carteira passada.
        /// Filtro por Data.GreaterThan(data).
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaHistoricoCota(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira, this.Query.Data)
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Data.GreaterThan(data));
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega todos os registros de HistoricoCota de uma carteira
        /// Aceita a passagem opcional de ordenacao
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="orderByColumn"></param>
        /// <param name="orderByDirection"></param>
        public bool LoadByIdCarteira(int idCarteira, string orderByColumn, esOrderByDirection? orderByDirection)
        {
            return this.LoadByIdCarteira(idCarteira, orderByColumn, orderByDirection, null, null);
        }

        public bool LoadByIdCarteira(int idCarteira, string orderByColumn, esOrderByDirection? orderByDirection, DateTime? dataInicio, DateTime? dataFim)
        {
            this.Query.Where(this.Query.IdCarteira == idCarteira);

            if (dataInicio.HasValue)
            {
                this.Query.Where(this.Query.Data >= dataInicio);
            }
            if (dataFim.HasValue)
            {
                this.Query.Where(this.Query.Data <= dataFim);
            }

            if (!string.IsNullOrEmpty(orderByColumn))
            {
                this.Query.OrderBy(orderByColumn, orderByDirection.Value);
            }
            return this.Query.Load();                
        }

        public void ImportaPosicaoFundoYMF(Financial.Interfaces.Import.YMF.PosicaoFundoYMF[] posicoesFundoYMF)
        {
            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();

            Dictionary<int, HistoricoCota> historicosCotasMaisRecentesDasCarteiras = new Dictionary<int, HistoricoCota>();
            Dictionary<int, HistoricoCota> historicosCotasMaisAntigasDasCarteiras = new Dictionary<int, HistoricoCota>();


            foreach (Financial.Interfaces.Import.YMF.PosicaoFundoYMF posicaoFundoYMF in posicoesFundoYMF)
            {
                HistoricoCota historicoCota = new HistoricoCota();
                if (historicoCota.ImportaPosicaoFundoYMF(posicaoFundoYMF))
                {
                    
                    historicoCotaCollection.AttachEntity(historicoCota);

                    if (!historicosCotasMaisRecentesDasCarteiras.ContainsKey(historicoCota.IdCarteira.Value))
                    {
                        //Se ainda não guardamos nenhuma cota da carteira atual, criar entrada no dictionary
                        historicosCotasMaisRecentesDasCarteiras.Add(historicoCota.IdCarteira.Value, historicoCota);
                        historicosCotasMaisAntigasDasCarteiras.Add(historicoCota.IdCarteira.Value, historicoCota);
                    }
                    else
                    {
                        //Checar se data é mais atual e, em caso positivo, armazená-la
                        if (historicoCota.Data.Value >= 
                            historicosCotasMaisRecentesDasCarteiras[historicoCota.IdCarteira.Value].Data.Value)
                        {
                            historicosCotasMaisRecentesDasCarteiras[historicoCota.IdCarteira.Value] = historicoCota;
                        }

                        if (historicoCota.Data.Value <
                            historicosCotasMaisAntigasDasCarteiras[historicoCota.IdCarteira.Value].Data.Value)
                        {
                            historicosCotasMaisAntigasDasCarteiras[historicoCota.IdCarteira.Value] = historicoCota;
                        }
                    }

                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {

                // Salva HistoricoCotas importados
                historicoCotaCollection.Save();


                //Atualizar clientes e carteiras com informações das cotas importadas
                foreach (KeyValuePair<int, HistoricoCota> kvpHistoricoCotaMaisRecente in 
                    historicosCotasMaisRecentesDasCarteiras)
                {
                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(kvpHistoricoCotaMaisRecente.Key);
                    cliente.GuardaInfoHistoricoCotaYMF(kvpHistoricoCotaMaisRecente.Value);
                    cliente.Save();

                    HistoricoCota historicoCotaMaisAntiga = historicosCotasMaisAntigasDasCarteiras[kvpHistoricoCotaMaisRecente.Key];
                    Carteira carteira = new Carteira();
                    carteira.LoadByPrimaryKey(kvpHistoricoCotaMaisRecente.Key);
                    carteira.GuardaInfoHistoricoCotaYMF(historicoCotaMaisAntiga);
                    carteira.Save();
                }

                scope.Complete();
            }

        }

        public bool BuscaHistoricoCotaPl(int idCarteira, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdCarteira,
                         this.Query.Data, 
                         this.Query.PLAbertura, 
                         this.query.PLFechamento, 
                         this.query.PatrimonioBruto, 
                         this.Query.CotaAbertura, 
                         this.Query.CotaFechamento, 
                         this.Query.CotaBruta)
                 .Where(this.Query.IdCarteira == idCarteira);
            this.Query.Where(this.Query.Data >= dataInicio);
            this.Query.Where(this.Query.Data <= dataFim);
            this.Query.OrderBy(this.Query.IdCarteira.Ascending, this.Query.Data.Ascending);

            bool retorno = this.Query.Load();

            return retorno;
        }
	}
}
