﻿using System;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Util;

namespace Financial.Fundo {
    public partial class DetalheResgateFundo : esDetalheResgateFundo 
    {
        /// <summary>
        /// Carga em DetalheResgateFundo a partir da estrutura preenchida de arquivo da YMF.
        /// </summary>
        /// <param name="movimento"></param>
        /// <param name="deferSave"></param>
        /// <param name="comCalculo"></param>
        /// <returns></returns>
      /*  public bool ImportaDetalheResgateYMFAnalitico(Financial.Interfaces.Import.Fundo.MovimentoAnaliticoYMF movimento, bool deferSave)
        {
            string codigoFundo = movimento.CdFundo.Trim();
            string codigoCliente = movimento.CdCotista.Trim();

            ClienteInterface clienteInterface = new ClienteInterface();
            int? idFundo = clienteInterface.BuscaClientePorCodigoYMF(codigoFundo);

            if (idFundo == null)
            {
                if (Utilitario.IsInteger(codigoFundo))
                {
                    Cliente clienteFundo = new Cliente();
                    if (clienteFundo.LoadByPrimaryKey(Convert.ToInt32(codigoFundo)))
                    {
                        idFundo = clienteFundo.IdCliente.Value;
                    }
                }
            }

            int? idCliente = clienteInterface.BuscaClientePorCodigoYMF(codigoCliente);

            Cliente cliente = new Cliente();
            DateTime? dataDia = null;

            if (idCliente == null && Utilitario.IsInteger(codigoCliente))
            {
                //Não encontramos idCliente pelo CodigoYMF... Testar pra ver se encontramos um Cliente o proprio IdCliente igual ao código passado
                idCliente = Convert.ToInt32(codigoCliente);
            }

            if (idCliente.HasValue && cliente.LoadByPrimaryKey(idCliente.Value))
            {
                idCliente = cliente.IdCliente.Value;
                dataDia = cliente.DataDia.Value;
            }

            this.IdCarteira = idFundo;
            this.IdCliente = idCliente;
            this.IdOperacao = movimento.IdNota;
            this.IdPosicaoResgatada = movimento.IdNotaAplicacao.Value;
            this.PrejuizoUsado = 0;
            this.Quantidade = movimento.QtCotas.Value;
            this.RendimentoResgate = 0;
            this.ValorBruto = movimento.VlBruto.Value;
            this.ValorCPMF = 0;
            this.ValorLiquido = movimento.VlLiquido.Value;
            this.ValorPerformance = 0;
            this.VariacaoResgate = 0;

            if (!deferSave)
            {
                this.Save();
            }
        
            return true;
        }
        */
    }
}
