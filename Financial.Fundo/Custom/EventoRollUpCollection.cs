/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 05/03/2015 16:52:09
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.InvestidorCotista;

namespace Financial.Fundo
{
	public partial class EventoRollUpCollection : esEventoRollUpCollection
	{
        public void GeraTransferenciaRollUp(int idCarteira, DateTime data)
        {
            TransferenciaSerieCollection transferenciaColl = new TransferenciaSerieCollection();
            EventoRollUpCollection eventoColl = new EventoRollUpCollection();

            #region Deleta Transferencias geradas para RollUp
            transferenciaColl.Query.Where(transferenciaColl.Query.IdCarteira.Equal(idCarteira)
                                          & transferenciaColl.Query.DataPosicao.Equal(data)
                                          & transferenciaColl.Query.IdEventoRollUp.IsNotNull());

            if (transferenciaColl.Query.Load())
            {
                transferenciaColl.MarkAllAsDeleted();
                transferenciaColl.Save();
            }
            #endregion

            transferenciaColl = new TransferenciaSerieCollection();

            #region Carrega Eventos de RollUp
            eventoColl.Query.Where(eventoColl.Query.DataExecucao.Equal(data)
                                   & eventoColl.Query.IdFundoOffShore.Equal(idCarteira));
            eventoColl.Query.Load();
            #endregion

            foreach (EventoRollUp eventoRollUp in eventoColl)
            {
                transferenciaColl.GeraTransferenciaRollUp(idCarteira, data, eventoRollUp);
                SeriesOffShore serieOrigem = eventoRollUp.UpToSeriesOffShoreByIdSerieOrigem;
                serieOrigem.DataRollUp = data;
                serieOrigem.Inativa = "S";
                serieOrigem.Save();
            }
        }
	}
}
