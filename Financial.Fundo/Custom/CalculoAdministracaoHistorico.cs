﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class CalculoAdministracaoHistorico : esCalculoAdministracaoHistorico
	{
        /// <summary>
        /// Adiciona uma coluna Virtual dinamicamente
        /// O Tipo da Coluna adicionado é considerado como sendo String
        /// </summary>
        /// <param name="column"></param>
        public void AddVirtualColumn(string column){
            //this.Table = new DataTable();
            if (this.Table != null && !this.Table.Columns.Contains(column)) {
                this.Table.Columns.Add(column, typeof(System.String));
            }
        }

        /// <summary>
        /// Retorna o valor de administração acumulado (total) histórico para o idCarteira.
        /// O parâmetro tipo é passado para informar se busca total de administração ou de gestão.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="tipo"></param>
        /// <returns></returns>        
        public decimal RetornaValorAdministracao(int idCarteira, DateTime dataHistorico, int tipo)
        {
            CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();

            calculoAdministracaoHistoricoCollection.Query
                 .Select(calculoAdministracaoHistoricoCollection.Query.IdTabela, 
                         calculoAdministracaoHistoricoCollection.Query.ValorAcumulado)
                 .Where(calculoAdministracaoHistoricoCollection.Query.IdCarteira == idCarteira,
                        calculoAdministracaoHistoricoCollection.Query.DataHistorico.Equal(dataHistorico));

            calculoAdministracaoHistoricoCollection.Query.Load();

            decimal valor = 0;
            for (int i = 0; i < calculoAdministracaoHistoricoCollection.Count; i++)
            {
                int idTabela = calculoAdministracaoHistoricoCollection[i].IdTabela.Value;

                TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
                tabelaTaxaAdministracao.LoadByPrimaryKey(idTabela);

                int tipoTaxa = tabelaTaxaAdministracao.UpToCadastroTaxaAdministracaoByIdCadastro.Tipo.Value;

                if (tipo == tipoTaxa)
                {
                    valor += calculoAdministracaoHistoricoCollection[i].ValorAcumulado.Value;
                }
            }
            
            return valor;
        }

	}
}
