/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 14/12/2015 18:15:53
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Fundo
{
	public partial class PortfolioPadrao : esPortfolioPadrao
	{
        public PortfolioPadrao RetornaPortfoliosVigentes(DateTime dataReferencia)
        {
            PortfolioPadraoCollection portfolioPadraoColl = new PortfolioPadraoCollection();
            portfolioPadraoColl.Query.Where(portfolioPadraoColl.Query.DataVigencia.LessThanOrEqual(dataReferencia));
            portfolioPadraoColl.Query.OrderBy(portfolioPadraoColl.Query.DataVigencia.Descending);

            if (portfolioPadraoColl.Query.Load())
                return portfolioPadraoColl[0];

            return new PortfolioPadrao();
        }
	}
}
