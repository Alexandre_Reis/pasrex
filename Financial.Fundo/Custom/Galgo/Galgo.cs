﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Investidor.Enums;
using Financial.Fundo.Exceptions;
using Financial.Common;
using System.Runtime.Serialization.Formatters.Binary;
using Financial.Fundo.Galgo_ServicePLCota;
using EntitySpaces.Interfaces;
using Financial.Util;

namespace Financial.Fundo.Galgo
{
    /// <summary>
    ///  Metodos de interface com o Galgo
    /// </summary>
    public class InterfaceGalgo
    {
        #region fields
        private bool _cotasPendentes;
        #endregion

        private ClienteCollection MontaListaClientes(List<int> idClientes)
        {
            ClienteQuery clienteQuery = new ClienteQuery("C");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            MoedaQuery moedaQuery = new MoedaQuery("M");
            ClienteCollection clienteCollection = new ClienteCollection();

            clienteQuery.Select(clienteQuery.IdCliente,
                                 carteiraQuery.IdCarteira.As("IdCarteira"),
                                 carteiraQuery.CodigoIsin.As("ClienteIsin"),
                                 clienteQuery.Nome.As("ClienteNome"),
                                 carteiraQuery.CodigoAnbid.As("ClienteAnbima"),
                                 carteiraQuery.CodigoSTI.As("CodigoSti"),
                                 moedaQuery.CdIso.As("MoedaIso"));
            clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
            clienteQuery.InnerJoin(moedaQuery).On(clienteQuery.IdMoeda == moedaQuery.IdMoeda);
            clienteQuery.Where(clienteQuery.IdCliente.In(idClientes));

            clienteQuery.OrderBy(clienteQuery.IdCliente.Descending);

            clienteCollection.Load(clienteQuery);

            if (clienteCollection.Count == 0)
            {
                throw new Exception("Não ha dados para exportar.");
            }

            return clienteCollection;
        }

        private Document MontaXmlPlCota(ClienteCollection clienteCollection, DateTime dataInicial, DateTime dataFinal)
        {
            Document doc = new Document();

            doc.PricRptV04 = new PriceReportV04();
            doc.PricRptV04.MsgId = new MessageIdentification1();

            doc.PricRptV04.MsgId.Id = "Envio_PLCota";
            doc.PricRptV04.MsgId.CreDtTm = DateTime.Now; // DateTime.Now.ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");

            doc.PricRptV04.MsgPgntn = new Pagination();
            doc.PricRptV04.MsgPgntn.PgNb = "1";
            doc.PricRptV04.MsgPgntn.LastPgInd = true;

            List<PriceValuation3> detailList = new List<PriceValuation3>();

            HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();

            for (int i = 0; i < clienteCollection.Count; i++)
            {
                string msgErro = string.Empty;

                int idCliente = clienteCollection[i].IdCliente.Value;
                string cdIsin = clienteCollection[i].GetColumn("ClienteIsin").ToString();
                string cdMoeda = clienteCollection[i].GetColumn("MoedaIso").ToString();
                string cdAnbima = clienteCollection[i].GetColumn("ClienteAnbima").ToString();
                string nomeCliente = clienteCollection[i].GetColumn("ClienteNome").ToString();
                string codigoSti = clienteCollection[i].GetColumn("codigoSti").ToString();

                if (string.IsNullOrEmpty(cdIsin))
                    msgErro += "Codigo ISIN não informado.\n";

                if (string.IsNullOrEmpty(cdMoeda))
                    msgErro += "Codigo Moeda não informado.\n";

                if (string.IsNullOrEmpty(codigoSti))
                    msgErro += "Codigo STI não informado.\n";

                if (!string.IsNullOrEmpty(msgErro))
                {
                    throw new Exception("Existem pendencias de cadastro para o cliente " + idCliente.ToString() + "\n\n" + msgErro);
                }

                historicoCotaCollection.BuscaHistoricoCotaPl(idCliente, dataInicial, dataFinal);

                foreach (HistoricoCota historicoCota in historicoCotaCollection)
                {
                    decimal pl = historicoCota.PLFechamento.Value;
                    decimal cota = historicoCota.CotaFechamento.Value;
                    DateTime data = historicoCota.Data.Value;

                    PriceValuation3 detail = new PriceValuation3();
                    detail.Id = string.Format("PLCotaFundo_{0}", codigoSti);
                    DateAndDateTimeChoice NAVDtTm = new DateAndDateTimeChoice();
                    NAVDtTm.Item = data;
                    NAVDtTm.ItemElementName = ItemChoiceType1.Dt;
                    detail.NAVDtTm = NAVDtTm;

                    // Identificacao - codigo isin e anbima
                    detail.FinInstrmDtls = new FinancialInstrument8();
                    SecurityIdentification3Choice FinInstrmDtlsIsin = new SecurityIdentification3Choice();
                    FinInstrmDtlsIsin.ItemElementName = ItemChoiceType3.ISIN;
                    FinInstrmDtlsIsin.Item = cdIsin;

                    SecurityIdentification3Choice FinInstrmDtlsSti = new SecurityIdentification3Choice();
                    AlternateSecurityIdentification1 sti = new AlternateSecurityIdentification1();
                    sti.ItemElementName = ItemChoiceType2.PrtryIdSrc;
                    sti.Id = codigoSti;
                    sti.Item = "SistemaGalgo";
                    FinInstrmDtlsSti.ItemElementName = ItemChoiceType3.OthrPrtryId;
                    FinInstrmDtlsSti.Item = sti;

                    List<SecurityIdentification3Choice> FinInstrmDtlsIdList = new List<SecurityIdentification3Choice>();
                    FinInstrmDtlsIdList.Add(FinInstrmDtlsIsin);
                    FinInstrmDtlsIdList.Add(FinInstrmDtlsSti);

                    detail.FinInstrmDtls.Id = FinInstrmDtlsIdList.ToArray();

                    detail.FinInstrmDtls.Nm = nomeCliente;
                    detail.FinInstrmDtls.DnmtnCcy = cdMoeda;
                    detail.FinInstrmDtls.DualFndInd = false;

                    // pl
                    List<ActiveOrHistoricCurrencyAndAmount> TtlNAVList = new List<ActiveOrHistoricCurrencyAndAmount>();
                    ActiveOrHistoricCurrencyAndAmount TtlNAV = new ActiveOrHistoricCurrencyAndAmount();
                    TtlNAV.Ccy = "BRL";
                    TtlNAV.Value = pl;
                    TtlNAVList.Add(TtlNAV);
                    detail.TtlNAV = TtlNAVList.ToArray();

                    // cota
                    List<FinancialInstrumentQuantity1> NAVList = new List<FinancialInstrumentQuantity1>();
                    FinancialInstrumentQuantity1 NAV = new FinancialInstrumentQuantity1();
                    NAV.Unit = cota;
                    NAVList.Add(NAV);

                    detail.NAV = NAVList.ToArray();
                    detail.ValtnTp = ValuationTiming1Code.USUA;
                    detail.OffclValtnInd = true;
                    detail.SspdInd = false;

                    detailList.Add(detail);
                }
            }

            doc.PricRptV04.PricValtnDtls = detailList.ToArray();

            return doc;
        }

        private HistoricoCota ConverteParaHistoricoCota(PriceValuation3 detail, string usuario)
        {
            // data da informacao
            DateTime dt = detail.NAVDtTm.Item.Date;
            FinancialInstrument8 finInstrmDtls = detail.FinInstrmDtls;
            decimal valorPl = 0;
            decimal cota = 0;
            string codigoSti = null;

            // busca codigo sti
            foreach (SecurityIdentification3Choice id in finInstrmDtls.Id)
            {
                if (id.Item is AlternateSecurityIdentification1)
                {
                    AlternateSecurityIdentification1 sti = id.Item as AlternateSecurityIdentification1;
                    codigoSti = sti.Id;
                    break;
                }
            }

            // busca PL
            foreach (ActiveOrHistoricCurrencyAndAmount pl in detail.TtlNAV)
            {
                if (pl.Value != 0)
                {
                    valorPl = pl.Value;
                    break;
                }
            }

            // busca cota
            foreach (FinancialInstrumentQuantity1 nav in detail.NAV)
            {
                if (nav.Unit != 0)
                {
                    cota = nav.Unit;
                    break;
                }
            }

            if (dt == null || string.IsNullOrEmpty(codigoSti) || valorPl == 0 || cota == 0)
            {
                return null;
            }

            CarteiraQuery carteiraQuery = new CarteiraQuery("A");
            carteiraQuery.Select(carteiraQuery.IdCarteira);
            carteiraQuery.Where(carteiraQuery.CodigoSTI.Equal(codigoSti));
            CarteiraCollection carteiraCollection = new CarteiraCollection();
            if (!carteiraCollection.Load(carteiraQuery) || !carteiraCollection.HasData)
            {
                return null;
            }
            int idCarteira = carteiraCollection[0].IdCarteira.Value;

            // busca cota dia anterior
            decimal cotaD1 = 0;
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(dt, 1, Financial.Common.Enums.LocalFeriadoFixo.Brasil, Financial.Common.Enums.TipoFeriado.Brasil);
            HistoricoCota historicoCotaD1 = new HistoricoCota();

            if (historicoCotaD1.LoadByPrimaryKey(dataAnterior, idCarteira))
            {
                cotaD1 = historicoCotaD1.CotaFechamento.Value;
            }

            HistoricoCota historicoCota = new HistoricoCota();

            if (!historicoCota.LoadByPrimaryKey(dt, idCarteira))
            {
                historicoCota = new HistoricoCota();
            }

            // se a cota informada for igual a do dia anterior, a mesma deve ser validada antes de ser assumida como boa
            if (ParametrosConfiguracaoSistema.Integracoes.IntegraGalgoPlCota == (int)Financial.Util.Enums.IntegraGalgoPlCota.IntegraValidaCotasRepetidas &&
                cotaD1 == cota)
            {
                _cotasPendentes = true;
                HistoricoCotaGalgoCollection historicoCotaGalgoCollection = new HistoricoCotaGalgoCollection();
                HistoricoCotaGalgo historicoCotaGalgo = new HistoricoCotaGalgo();
                if (!historicoCotaGalgo.LoadByPrimaryKey(dt, idCarteira))
                {
                    historicoCotaGalgo = new HistoricoCotaGalgo();
                }

                historicoCotaGalgo.Data = dt;
                historicoCotaGalgo.IdCarteira = idCarteira;
                historicoCotaGalgo.Cota = cota;
                historicoCotaGalgo.Pl = valorPl;
                historicoCotaGalgo.DataImportacao = DateTime.Now;
                historicoCotaGalgo.UsuarioImportacao = usuario;
                historicoCotaGalgo.Aprovar = (int)Financial.Util.Enums.AprovarCotaGalgo.Aprovar;
                historicoCotaGalgo.DataAprovacao = null;
                historicoCotaGalgo.UsuarioAprovacao = null;
                historicoCotaGalgoCollection.AttachEntity(historicoCotaGalgo);
                historicoCotaGalgoCollection.Save();

                return null;
            }

            // Dados do Arquivo
            historicoCota.Data = dt;
            historicoCota.IdCarteira = idCarteira;

            historicoCota.CotaAbertura = cota;
            historicoCota.CotaFechamento = cota;
            historicoCota.CotaBruta = cota;
            //
            historicoCota.PLAbertura = valorPl;
            historicoCota.PLFechamento = valorPl;
            historicoCota.PatrimonioBruto = valorPl;
            //
            historicoCota.QuantidadeFechamento = historicoCota.QuantidadeFechamento == null ? 0 : historicoCota.QuantidadeFechamento;
            historicoCota.AjustePL = historicoCota.AjustePL == null ? 0 : historicoCota.AjustePL;
            //

            return historicoCota;
        }

        #region Importa pl Cota - Arquivo
        public void ImportaPlCota(TextReader reader, string usuario)
        {
            _cotasPendentes = false;

            XmlSerializer serializer = new XmlSerializer(typeof(Document));

            Document doc = (Document)serializer.Deserialize(reader);

            if (doc != null && doc.PricRptV04 != null && doc.PricRptV04.PricValtnDtls != null)
            {
                HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();

                foreach (PriceValuation3 detail in doc.PricRptV04.PricValtnDtls)
                {
                    HistoricoCota historicoCota = ConverteParaHistoricoCota(detail, usuario);
                    if (historicoCota != null)
                        historicoCotaCollection.AttachEntity(historicoCota);
                }

                historicoCotaCollection.Save();

                if (_cotasPendentes)
                    throw new GalgoException("Foram importadas cotas pendentes para aprovação.\n\nVerifique em Mercados/Fundos/Cotas Repetidas.");
            }
        }
        #endregion

        #region Importa PL/Cota - WCF - Consumir

        public void ImportaPlCotaWcf(string url, List<int> idClientes, DateTime dtInicio, DateTime dtFim, Financial.Util.Enums.MetodoWcfGalgo metodo, long marcador, string usuario)
        {
            _cotasPendentes = false;

            try
            {
                HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
                ParametrosPLCotaComplexType parametros = new ParametrosPLCotaComplexType();
                MessageConsultaPLCotaComplexType messageConsumir = new MessageConsultaPLCotaComplexType();
                MessageBatchPendLongComplexType messageConsumirPendentes = new MessageBatchPendLongComplexType();

                MessageRetornoPLCotaComplexType response = new MessageRetornoPLCotaComplexType();

                using (ServicePLCota galgoPlCotaService = new ServicePLCota())
                {
                    galgoPlCotaService.Url = url;
                    foreach (int idCliente in idClientes)
                    {
                        Carteira carteira = new Carteira();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(carteira.Query.CodigoSTI);

                        if (!carteira.LoadByPrimaryKey(campos, idCliente))
                        {
                            continue;
                        }

                        // popula (BuscaObjeto/Objeto/CdSti) codigo STI
                        BuscaObjetoComplexType buscaObjetoComplexType = new BuscaObjetoComplexType();
                        List<ObjetoComplexType> buscaObjeto = new List<ObjetoComplexType>();
                        ObjetoComplexType obj = new ObjetoComplexType();
                        obj.CdSti = Convert.ToInt32(carteira.CodigoSTI);
                        buscaObjeto.Add(obj);

                        buscaObjetoComplexType.Objeto = buscaObjeto.ToArray();

                        // parametros
                        parametros.DtFinEnvioCota = DateTime.Now;
                        parametros.DtFinInfo = dtInicio;
                        parametros.DtInicEnvioCota = DateTime.Now;
                        parametros.DtInicInfo = dtFim;
                        parametros.Item = buscaObjetoComplexType;

                        if (metodo == Financial.Util.Enums.MetodoWcfGalgo.Consumir)
                        {
                            // consumir enviadas
                            parametros.StatusInfo = 1;
                            messageConsumir.Parametros = parametros;
                            response = galgoPlCotaService.Consumir(messageConsumir);
                        }

                        if (metodo == Financial.Util.Enums.MetodoWcfGalgo.Pendentes)
                        {
                            // consumir pendentes com base no marcador (controle gerado pelo sistema)
                            messageConsumirPendentes.Marcador = marcador;
                            response = galgoPlCotaService.ConsumirPendentes(messageConsumirPendentes);
                        }

                        if (metodo == Financial.Util.Enums.MetodoWcfGalgo.Reenviados)
                        {
                            // consumir reenviadas
                            parametros.StatusInfo = 2;
                            messageConsumir.Parametros = parametros;
                            response = galgoPlCotaService.Consumir(messageConsumir);
                        }

                        if (response == null || response.PricRptV04 == null) continue;

                        foreach (PriceValuation3 detail in response.PricRptV04.PricValtnDtls)
                        {
                            HistoricoCota historicoCota = ConverteParaHistoricoCota(detail, usuario);
                            historicoCotaCollection.AttachEntity(historicoCota);
                        }
                    }
                    historicoCotaCollection.Save();

                    if (_cotasPendentes)
                        throw new GalgoException("Foram importadas cotas pendentes para aprovação.\n\nVerifique em Mercados/Fundos/Cotas Repetidas.");

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Exporta Pl Cota - Arquivo
        public void ExportaPlCota(DateTime dataInicial, DateTime dataFinal, List<int> idClientes, out MemoryStream ms, out string nomeArquivo)
        {
            ms = new MemoryStream();

            nomeArquivo = string.Format("GalgoPlCota_{0}_{1}.xml", dataInicial.ToString("ddMMyyyy"), dataFinal.ToString("ddMMyyyy"));
            ClienteCollection clienteCollection = MontaListaClientes(idClientes);

            try
            {
                Document doc = MontaXmlPlCota(clienteCollection, dataInicial, dataFinal);

                XmlSerializer x = new XmlSerializer(doc.GetType());

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = System.Text.Encoding.GetEncoding("UTF-8");
                settings.Indent = true;
                settings.IndentChars = "\t";
                settings.NewLineChars = Environment.NewLine;
                settings.ConformanceLevel = ConformanceLevel.Document;

                //XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                //ns.Add("", "");

                using (XmlWriter writer = XmlTextWriter.Create(ms, settings))
                {
                    x.Serialize(writer, doc);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao gerar arquivo PL/Cota Galgo.\n" + ex.Message);
            }

        }
        #endregion

        #region Exporta Pl Cota - wcf
        public void ExportaPlCotaWcf(string url, DateTime dataInicial, DateTime dataFinal, List<int> idClientes)
        {
            ClienteCollection clienteCollection = MontaListaClientes(idClientes);

            try
            {
                Document doc = MontaXmlPlCota(clienteCollection, dataInicial, dataFinal);

                using (ServicePLCota galgoPlCotaService = new ServicePLCota())
                {
                    galgoPlCotaService.Url = url;
                    MessageEnviarPLCotaComplexType message = new MessageEnviarPLCotaComplexType();
                    message.PricRptV04 = doc.PricRptV04;
                    galgoPlCotaService.Enviar(message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao enviar PL/Cota Galgo.\n" + ex.Message);
            }
        }

        #endregion

    }

    #region classe Document
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "urn:iso:std:iso:20022:tech:xsd:reda.001.001.03")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "urn:iso:std:iso:20022:tech:xsd:reda.001.001.03", IsNullable = false)]
    public partial class Document
    {

        private PriceReportV04 pricRptV04Field;

        /// <remarks/>
        public PriceReportV04 PricRptV04
        {
            get
            {
                return this.pricRptV04Field;
            }
            set
            {
                this.pricRptV04Field = value;
            }
        }
    }
    #endregion
}