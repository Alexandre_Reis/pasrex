﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Fundo.Exceptions;
using Financial.Fundo.Enums;
using Financial.Util;
using Financial.Tributo.Custom;
using Financial.Investidor;
using Financial.Common.Enums;
using Financial.Common;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using System.IO;
using System.Globalization;
using Financial.Bolsa.Exceptions;
using Financial.Investidor.Exceptions;
using Financial.Investidor.Enums;
using Financial.Interfaces.Import.Fundo;
using Financial.Tributo.Debug.CalculoTributo;
using Financial.InvestidorCotista;
using Financial.Util.Enums;

namespace Financial.Fundo
{
    public partial class PosicaoFundo : esPosicaoFundo
    {
        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="tipoCarteira"></param>
        /// <param name="idAgenteAdministrador"></param>
        /// <param name="idAgenteGestor"></param>
        /// <param name="idIndice"></param>
        /// <param name="idCarteira"></param>
        /// <returns></returns>
        public decimal RetornaValorMercadoEnquadra(int idCliente, int? tipoCarteira, int? idAgenteAdministrador,
                                                   int? idAgenteGestor, int? idIndice, int? idCarteira, int? idCategoria)
        {            
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            posicaoFundoCollection.Query
                 .Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorBruto)
                 .Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente));

            posicaoFundoCollection.Query.Load();

            decimal totalValorMercado = 0;

            for (int i = 0; i < posicaoFundoCollection.Count; i++)
            {
                PosicaoFundo posicaoFundo = posicaoFundoCollection[i];
                int idCarteiraCompara = posicaoFundo.IdCarteira.Value;
                decimal valorMercado = posicaoFundo.ValorBruto.Value;

                bool passou = true;
                if (idCarteira.HasValue)
                {
                    if (idCarteiraCompara != idCarteira)
                        passou = false;
                }
                else if (tipoCarteira.HasValue || idAgenteAdministrador.HasValue || idAgenteGestor.HasValue ||
                         idIndice.HasValue || idCategoria.HasValue)
                {
                    #region Busca os campos de Carteira
                    Carteira carteira = new Carteira();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(carteira.Query.TipoCarteira);
                    campos.Add(carteira.Query.IdAgenteAdministrador);
                    campos.Add(carteira.Query.IdAgenteGestor);
                    campos.Add(carteira.Query.IdIndiceBenchmark);
                    campos.Add(carteira.Query.IdCategoria);
                    carteira.LoadByPrimaryKey(campos, idCarteiraCompara);

                    int tipoCarteiraCompara = carteira.TipoCarteira.Value;
                    int idAgenteAdministradorCompara = carteira.IdAgenteAdministrador.Value;
                    int idAgenteGestorCompara = carteira.IdAgenteGestor.Value;
                    int idIndiceCompara = carteira.IdIndiceBenchmark.Value;
                    int idCategoriaCompara = carteira.IdCategoria.Value;
                    #endregion

                    #region Verifica se passa pelos filtros de tipoCarteira, administrador, gestor e indice
                    if (tipoCarteira.HasValue)
                    {
                        if (tipoCarteiraCompara != tipoCarteira)
                            passou = false;
                    }

                    if (idAgenteAdministrador.HasValue)
                    {
                        if (idAgenteAdministradorCompara != idAgenteAdministrador)
                            passou = false;
                    }

                    if (idAgenteGestor.HasValue)
                    {
                        if (idAgenteGestorCompara != idAgenteGestor)
                            passou = false;
                    }


                    if (idIndice.HasValue)
                    {
                        if (idIndiceCompara != idIndice)
                            passou = false;
                    }

                    if (idCategoria.HasValue)
                    {
                        if (idCategoriaCompara != idCategoria)
                            passou = false;
                    }
                    #endregion

                }

                if (passou)
                    totalValorMercado += valorMercado;
            }

            return totalValorMercado;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorBruto.Sum());
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                posicaoFundoCollection.Query.Quantidade.GreaterThan(0));
            posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCarteira);
            posicaoFundoCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoFundoCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                int idCarteira = posicaoFundo.IdCarteira.Value;
                decimal valor = posicaoFundo.ValorBruto.Value;

                Cliente clienteFundo = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteFundo.Query.IdMoeda);
                clienteFundo.LoadByPrimaryKey(campos, idCarteira);
                int idMoedaFundo = clienteFundo.IdMoeda.Value;
                
                if (idMoedaCliente != idMoedaFundo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaFundo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaFundo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }
        
        /// <summary>
        /// Retorna o valor de mercado liquido do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercadoLiquido(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira, posicaoFundoCollection.Query.ValorLiquido.Sum());
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                posicaoFundoCollection.Query.Quantidade.GreaterThan(0));
            posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCarteira);
            posicaoFundoCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoFundoCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                int idCarteira = posicaoFundo.IdCarteira.Value;
                decimal valor = posicaoFundo.ValorLiquido.Value;

                Cliente clienteFundo = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteFundo.Query.IdMoeda);
                clienteFundo.LoadByPrimaryKey(campos, idCarteira);
                int idMoedaFundo = clienteFundo.IdMoeda.Value;

                if (idMoedaCliente != idMoedaFundo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaFundo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaFundo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }
        
        /// <summary>
        /// Retorna o total de valor bruto do cliente em todos as carteiras onde ele tenha posição.
        /// Query.Quantidade.GreaterThan(0).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorBruto(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.GreaterThan(0));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna o total de valor bruto do cliente dada a carteira de fundo passada.
        /// Query.Quantidade.GreaterThan(0).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorBruto(int idCliente, int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorBruto.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdCarteira == idCarteira,
                        this.Query.Quantidade.GreaterThan(0));

            this.Query.Load();

            return this.ValorBruto.HasValue ? this.ValorBruto.Value : 0;
        }

        /// <summary>
        /// Retorna o total de valor líquido "projetado" do cliente em todos as carteiras onde ele tenha posição.
        /// Query.Quantidade.GreaterThan(0).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorLiquido(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.GreaterThan(0));

            this.Query.Load();

            return this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            PosicaoFundoHistoricoCollection posicaoFundoDeletarHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoDeletarHistoricoCollection.DeletaPosicaoFundoHistoricoDataHistoricoMaiorIgual(idCliente, data);

            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoFundoHistorico (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoFundo WHERE IdCliente = " + idCliente;
            int count = 0;

            PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
            int columnCount = posicaoFundoHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoFundo in posicaoFundoHistorico.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoFundo.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoFundo.Name == PosicaoFundoHistoricoMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoFundo.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());

            
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCliente, DateTime data)
        {

            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoFundoAbertura (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoFundo WHERE IdCliente = " + idCliente;
            int count = 0;

            PosicaoFundoAbertura posicaoFundoAbertura = new PosicaoFundoAbertura();
            int columnCount = posicaoFundoAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoFundo in posicaoFundoAbertura.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoFundo.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoFundo.Name == PosicaoFundoHistoricoMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoFundo.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());


        }

        /// <summary>
        /// Atualiza a cota (abertura ou fechamento) de todas as posições de fundo da carteira passada.
        /// Atualiza o saldo bruto de todas as posições de fundos.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="data"></param>  
        public void AtualizaValoresPosicao(int idCliente, DateTime data)
        {
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.BuscaPosicaoOrdenada(idCliente);

            int i = 0;
            while (i < posicaoFundoCollection.Count)
            {
                PosicaoFundo posicaoFundo = posicaoFundoCollection[i];

                int idCarteiraAnterior = posicaoFundo.IdCarteira.Value;
                int idCarteira = posicaoFundo.IdCarteira.Value;

                HistoricoCota historicoCota = new HistoricoCota();
                if (!historicoCota.BuscaValorCota(idCarteira, data))
                {
                    Carteira carteiraMsg = new Carteira();
                    List<esQueryItem> camposMsg = new List<esQueryItem>();
                    camposMsg.Add(carteiraMsg.Query.Apelido);
                    carteiraMsg.LoadByPrimaryKey(camposMsg, idCarteira);

                    StringBuilder mensagem = new StringBuilder();
                    mensagem.Append("Cota não cadastrada na data ")
                            .Append(data + " para a carteira " + idCarteira + " - " + carteiraMsg.Apelido);
                    throw new HistoricoCotaNaoCadastradoException(mensagem.ToString());
                }


                #region Busca o tipo de cota da carteira e indicativo de Trunca Financeiro
                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TipoCota);
                campos.Add(carteira.Query.TruncaFinanceiro);
                if (!carteira.LoadByPrimaryKey(campos, idCarteira))
                    throw new CarteiraNaoCadastradaException("Carteira não cadastrada nr " + idCarteira);

                int tipoCota = carteira.TipoCota.Value;
                string truncaFinanceiro = carteira.TruncaFinanceiro;
                #endregion

                decimal cota;
                if (tipoCota == (int)TipoCotaFundo.Abertura)
                    cota = historicoCota.CotaAbertura.Value;
                else
                    cota = historicoCota.CotaFechamento.Value;

                #region verifica se carteira tem proventos
                decimal valor = 0;
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteira);
                if(cliente.TipoControle.Equals((byte)TipoControleCliente.ApenasCotacao))
                {
                    AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
                    agendaFundoCollection.Query.Select(agendaFundoCollection.Query.Valor, agendaFundoCollection.Query.ImpactarCota);
                    agendaFundoCollection.Query.Where(agendaFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                      agendaFundoCollection.Query.DataEvento.Equal(data));
                    agendaFundoCollection.Query.Load();

                    foreach (AgendaFundo agendaFundo in agendaFundoCollection)
                    {
                        if (agendaFundo.ImpactarCota.Equals("S"))
                            valor += agendaFundo.Valor.GetValueOrDefault(0);
                    }

                    cota -=valor;
                }
                #endregion

                while (idCarteira == idCarteiraAnterior)
                {
                    decimal quantidade = posicaoFundo.Quantidade.Value;
                    decimal valorBruto;
                    if (truncaFinanceiro == "S")
                    {
                        valorBruto = Utilitario.Truncate(quantidade * cota, 2);
                    }
                    else
                    {
                        valorBruto = Math.Round(quantidade * cota, 2);
                    }
                    
                    posicaoFundo.CotaDia = cota;

                    if (posicaoFundo.DataConversao.Value != data || posicaoFundo.Quantidade.Value != posicaoFundo.QuantidadeInicial.Value)
                    {
                        posicaoFundo.ValorBruto = valorBruto;
                        posicaoFundo.ValorLiquido = valorBruto; //Se não calcular tributos, mantêm o valor bruto mesmo
                    }

                    i += 1;
                    if (i < posicaoFundoCollection.Count)
                    {                        
                        posicaoFundo = posicaoFundoCollection[i];
                        idCarteira = posicaoFundo.IdCarteira.Value;
                    }
                    else
                    {
                        break;
                    }                    
                }
                                
            }

            posicaoFundoCollection.Save();
        }

        /// <summary>
        /// Atualiza os valores projetados de IR, IOF, ValorLiquido das posições de fundos.
        /// Não leva em conta o prejuízo a compensar.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="data"></param>  
        public void AtualizaProjecaoTributo(int idCliente, DateTime data)
        {            
            #region Busca isenção IR/IOF do cliente e tipo de cliente
            Cliente cliente = new Cliente();            
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IsentoIR);
            campos.Add(cliente.Query.IsentoIOF);
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.GrossUP);
            campos.Add(cliente.Query.IdPessoa);
            cliente.LoadByPrimaryKey(campos, idCliente);
            #endregion
            
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.BuscaPosicaoFundo(idCliente);

            for (int i = 0; i < posicaoFundoCollection.Count; i++)
            {
                #region Inicializa as variáveis principais
                PosicaoFundo posicaoFundo = posicaoFundoCollection[i];
                int idPosicao = posicaoFundo.IdPosicao.Value;
                int idCarteira = posicaoFundo.IdCarteira.Value;
                DateTime dataAplicacao = posicaoFundo.DataAplicacao.Value;
                DateTime dataConversao = posicaoFundo.DataConversao.Value;
                
                #region Busca os parâmetros da carteira, para cálculo dos tributos
                Carteira carteira = new Carteira();
                carteira.BuscaCarteiraCalculoTributo(idCarteira);
                int tipoCusto = carteira.TipoCusto.Value;
                int tipoCota = carteira.TipoCota.Value;
                bool calculaIOF = carteira.IsCalculaIOF();
                int tipoTributacao = carteira.TipoTributacao.Value;
                bool truncaFinanceiro = carteira.TruncaFinanceiro == "S";
                #endregion

                bool isFDIC = false;
                #region Calcula booleano indicativo se é FDIC
                Cliente clienteFDIC = new Cliente();
                campos = new List<esQueryItem>();
                campos.Add(clienteFDIC.Query.IdTipo);
                clienteFDIC.LoadByPrimaryKey(campos, idCarteira);
                isFDIC = clienteFDIC.IdTipo.Value == TipoClienteFixo.FDIC;
                #endregion

                //Avalia se deve usar a quantidade antes de cortes ou a quantidade real da posição!
                bool aliquotaDiferenciada = true;
                int diasTotal = Calendario.NumeroDias(dataConversao, data);
                if (tipoTributacao == (int)TipoTributacaoFundo.Acoes ||
                    tipoTributacao == (int)TipoTributacaoFundo.CurtoPrazo && diasTotal > 180 ||
                    tipoTributacao == (int)TipoTributacaoFundo.LongoPrazo && diasTotal > 720)
                {
                    aliquotaDiferenciada = false;
                }
                //

                decimal cotaAplicacao = 0;  
                if (tipoCusto == (int)TipoCustoFundo.MedioAplicado)
                {
                    cotaAplicacao = this.RetornaCotaMedia(idCarteira, idCliente);
                }
                else
                {
                    cotaAplicacao = posicaoFundo.CotaAplicacao.Value;
                }                

                decimal quantidadePosicao = posicaoFundo.Quantidade.Value;
                decimal quantidadeAntesCortes = posicaoFundo.QuantidadeAntesCortes.Value;
                DateTime dataUltimaCobrancaIR = posicaoFundo.DataUltimaCobrancaIR.Value;
                decimal valorIOFVirtual = posicaoFundo.ValorIOFVirtual.Value;
                #endregion

                Carteira carteiraLiquidacao = new Carteira();
                int diasLiquidacaoResgate = carteiraLiquidacao.RetornaDiasLiquidacaoResgate(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                           carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                           (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);
                int diasLiquidacaoComeCotas = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                           carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                           (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

                decimal quantidadeCalcular = 0;
                if (aliquotaDiferenciada == true)
                {
                    quantidadeCalcular = quantidadeAntesCortes;
                }
                else
                {
                    quantidadeCalcular = quantidadePosicao;
                }
                
                #region Busca o valor da cota na data
                HistoricoCota historicoCota = new HistoricoCota();                
                decimal cotaDia;
                
                historicoCota.BuscaValorCotaDia(idCarteira, data);
                if (tipoCota == (int)TipoCotaFundo.Abertura)
                {
                    cotaDia = historicoCota.CotaAbertura.Value;
                }
                else
                {
                    cotaDia = historicoCota.CotaFechamento.Value;
                }
                #endregion

                #region verifica se carteira tem proventos
                decimal valor = 0;
                Cliente clienteProventos = new Cliente();
                clienteProventos.LoadByPrimaryKey(idCarteira);
                if (clienteProventos.TipoControle.Equals((byte)TipoControleCliente.ApenasCotacao))
                {
                    AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
                    agendaFundoCollection.Query.Select(agendaFundoCollection.Query.Valor, agendaFundoCollection.Query.ImpactarCota);
                    agendaFundoCollection.Query.Where(agendaFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                      agendaFundoCollection.Query.DataEvento.Equal(data));
                    agendaFundoCollection.Query.Load();

                    foreach (AgendaFundo agendaFundo in agendaFundoCollection)
                    {
                        if (agendaFundo.ImpactarCota.Equals("S"))
                            valor += agendaFundo.Valor.GetValueOrDefault(0);
                    }

                    cotaDia -= valor;

                }
                #endregion

                #region Verifica data de ultima cobrança de IR
                decimal valorIrComeCotas = 0;
                AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
                agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.DataLancamento.Equal(data),
                                                      agendaComeCotasCollection.Query.IdCarteira.Equal(idCliente),
                                                      agendaComeCotasCollection.Query.TipoPosicao.Equal(TipoComeCotas.Fundo.GetHashCode()),
                                                      agendaComeCotasCollection.Query.TipoEvento.Equal((int)FonteOperacaoCotista.ComeCotas));

                agendaComeCotasCollection.Query.Load();

                if (agendaComeCotasCollection.Count > 0)
                {
                    dataUltimaCobrancaIR = agendaComeCotasCollection[0].DataUltimoIR.Value;
                    valorIrComeCotas = agendaComeCotasCollection[0].ValorIRPago.Value;
                }
                #endregion

                CalculoTributo calculoTributo = new CalculoTributo();
                decimal saldoBruto = saldoBruto = truncaFinanceiro ? Utilitario.Truncate(posicaoFundo.Quantidade.Value * cotaDia, 2) : Math.Round(posicaoFundo.Quantidade.Value * cotaDia, 2);
                
                #region Calcula IOF
                decimal valorIOF = 0;
                if (!cliente.IsIsentoIOF() && calculaIOF && tipoTributacao != (int)TipoTributacaoFundo.Acoes && !isFDIC)
                {
                    CalculoIOF calculoIOF = new CalculoIOF();
                    calculoIOF.IdFundo = idCarteira;
                    calculoIOF.Quantidade = quantidadeCalcular;
                    calculoIOF.CotaAplicacao = cotaAplicacao;
                    calculoIOF.CotaCalculo = cotaDia;
                    calculoIOF.DataAplicacao = dataAplicacao;
                    calculoIOF.DataCalculo = data;
                    calculoIOF.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                    calculoTributo.CalculaIOFFundo(calculoIOF);
                    valorIOF = calculoTributo.IOF;
                }
                #endregion

                bool tributaNaoResidente = cliente.IdTipo.Value == (int)TipoClienteFixo.InvestidorEstrangeiro;
                #region Calcula IR
                decimal valorIR = 0;
                if (!cliente.IsIsentoIR())
                {
                    if (carteira.AtivoFundoIsento(idCarteira, cliente.UpToPessoaByIdPessoa.Tipo, data) && cliente.GrossUP.Value != (byte)GrossupCliente.NaoFaz) // Se faz grossup
                    {
                        decimal rendimento = cotaDia - cotaAplicacao;
                        valorIOF = 0;
                        valorIR = ((rendimento / (decimal)0.85) - rendimento) * quantidadeCalcular;
                        saldoBruto += valorIR;

                        posicaoFundo.ValorBruto = saldoBruto / quantidadeCalcular;
                    }
                    else
                    {

                        if (carteira.Fie.Equals("S"))
                        {
                            calculoTributo.calculoIRFieFundo(idPosicao,
                                                             cotaDia,
                                                             quantidadeCalcular,
                                                             cliente.IsIsentoIOF(),
                                                             diasLiquidacaoResgate,
                                                             data);

                            valorIR = calculoTributo.IR;                            
                        }
                        else
                        {

                            if (tipoTributacao == (int)TipoTributacaoFundo.Acoes)
                            {
                                CalculoIRFundoAcoes calculoIRFundoAcoes = new CalculoIRFundoAcoes();
                                calculoIRFundoAcoes.Quantidade = quantidadeCalcular;
                                calculoIRFundoAcoes.PrejuizoCompensar = 0; // Não usa prejuízo a compensar na projeção
                                calculoIRFundoAcoes.CotaAplicacao = cotaAplicacao;
                                calculoIRFundoAcoes.CotaCalculo = cotaDia;
                                calculoIRFundoAcoes.TributaNaoResidente = tributaNaoResidente;
                                calculoIRFundoAcoes.DataCalculo = data;
                                calculoIRFundoAcoes.DataAplicacao = dataAplicacao;
                                calculoIRFundoAcoes.IdPosicao = idPosicao;
                                calculoTributo.CalculaIRFundoAcoes(calculoIRFundoAcoes);
                                valorIR = calculoTributo.IR;
                            }
                        else if (tipoTributacao == (int)TipoTributacaoFundo.AliquotaEspecifica)
                        {
                            //calculoTributo.calculoAliquotaEspecifica(idCarteira, dataAplicacao, data, quantidadeCalcular, cliente.IsIsentoIOF());
                            valorIR = calculoTributo.IR;
                        }
                            else if (tipoTributacao != (int)TipoTributacaoFundo.Isento)
                            {
                                CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();
                                calculoIRFundoRendaFixa.IdFundo = idCarteira;
                                calculoIRFundoRendaFixa.Quantidade = quantidadeCalcular;
                                calculoIRFundoRendaFixa.TipoCota = tipoCota;
                                calculoIRFundoRendaFixa.PrejuizoCompensar = 0; // Não usa prejuízo a compensar na projeção
                                calculoIRFundoRendaFixa.CotaAplicacao = cotaAplicacao;
                                calculoIRFundoRendaFixa.CotaCalculo = cotaDia;
                                calculoIRFundoRendaFixa.DataAplicacao = dataAplicacao;
                                calculoIRFundoRendaFixa.DataConversao = dataConversao;
                                calculoIRFundoRendaFixa.DataCalculo = data;
                                calculoIRFundoRendaFixa.DataUltimaCobrancaIR = dataUltimaCobrancaIR;
                                calculoIRFundoRendaFixa.IOFVirtual = 0; //Não usa IOF Virtual na projeção
                                calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                                calculoIRFundoRendaFixa.IdPosicao = idPosicao;
                                calculoIRFundoRendaFixa.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                                calculoIRFundoRendaFixa.DiasLiquidacaoComeCotas = diasLiquidacaoComeCotas;
                                calculoIRFundoRendaFixa.TipoProcessamentoResgate = TipoProcessaResgate.OperacaoFundo;
                                calculoIRFundoRendaFixa.IdTipo = clienteFDIC.IdTipo.Value;
                                calculoIRFundoRendaFixa.IsentoIOF = cliente.IsIsentoIOF();
                                calculoIRFundoRendaFixa.TributaNaoResidente = tributaNaoResidente;
                                calculoIRFundoRendaFixa.ProjecaoIR = true;
                                calculoIRFundoRendaFixa.QuantidadeAntesCortes = posicaoFundo.QuantidadeAntesCortes;
                                calculoIRFundoRendaFixa.QuantidadePosicao = posicaoFundo.Quantidade;

                                DebugCalculaIRFundoRendaFixa debugCalculaIRFundoRendaFixa = new DebugCalculaIRFundoRendaFixa();
                                calculoTributo.CalculaIRFundoRendaFixa(calculoIRFundoRendaFixa, true,
                                    out debugCalculaIRFundoRendaFixa);
                                valorIR = calculoTributo.IR;
                            }
                        }

                    }
                }
                #endregion

                valorIR -=  valorIrComeCotas;

                posicaoFundo.ValorIOF = valorIOF;
                posicaoFundo.ValorIR = valorIR ;   
                
                decimal saldoLiquido = saldoBruto - valorIOF - valorIR;
                posicaoFundo.ValorBruto = saldoBruto;
                posicaoFundo.ValorLiquido = saldoLiquido;
            }

            //Atualiza a collection de posicaoFundoCollection
            posicaoFundoCollection.Save();
        }

        /// <summary>
        /// Calcula a cota média de aplicação, ponderada pelas quantidades aplicadas em cada nota.
        /// Leva em conta a quantidade inicial de cada posição, mesmo que esta esteja zerada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idCarteira"></param>
        /// <returns>cota média de aplicação</returns>
        public decimal RetornaCotaMedia(int idCarteira, int idCliente)
        {
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            posicaoFundoCollection.Query
                 .Select(posicaoFundoCollection.Query.CotaAplicacao, posicaoFundoCollection.Query.QuantidadeInicial)
                 .Where(posicaoFundoCollection.Query.IdCliente == idCliente,
                        posicaoFundoCollection.Query.IdCarteira == idCarteira);

            posicaoFundoCollection.Query.Load();

            decimal cotaMedia;
            if (posicaoFundoCollection.HasData)
            {
                decimal valorAplicado = 0;
                decimal quantidadeAplicada = 0;
                for (int i = 0; i < posicaoFundoCollection.Count; i++)
                {
                    decimal cotaAplicacao = (posicaoFundoCollection[i]).CotaAplicacao.Value;
                    decimal quantidadeInicial = (posicaoFundoCollection[i]).QuantidadeInicial.Value;
                    valorAplicado += cotaAplicacao * quantidadeInicial; //Notar que não é truncado este valor
                    quantidadeAplicada += quantidadeInicial;
                }
                cotaMedia = valorAplicado / quantidadeAplicada;

            }
            else
            {
                throw new ClienteSemPosicaoException("Cliente " + idCliente + " sem posição na carteira " + idCarteira);
            }

            return cotaMedia;
        }

        /// <summary>
        /// Retorna a quantidade em posição do ativo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadeAtivo(int idCliente, int idCarteira)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdCarteira.Equal(idCarteira));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna a Soma do Valor Liquido dado o idCarteira
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCotista"></param>
        /// <returns></returns>
        /// 
        public decimal RetornaSumValorLiquido(int idCarteira) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCarteira == idCarteira &&
                        this.Query.Quantidade != 0);

            this.Query.Load();

            return this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
        }

        /// <summary>
        /// Calcula o IR e a quantidade do ComeCotas semestral.
        /// Lança operações de ComeCotas em OperacaoFundo.
        /// Lança valores dos débitos de IR (ComeCotas) em Liquidacao.
        /// Leva em conta o prejuízo a compensar em fundos de mesma categoria e administrador.
        /// </summary>
        /// <param name="idCliente"></param>  
        /// <param name="data"></param>  
        public void ProcessaComeCotas(int idCliente, DateTime data, PrioridadeOperacaoFundo prioridadeOperacaoFundo)
        {
            #region Checa se está no último dia de maio ou novembro
            DateTime ultimoDiaMaio = new DateTime(data.Year, 05, 31);
            if (!Calendario.IsDiaUtil(ultimoDiaMaio, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                ultimoDiaMaio = Calendario.SubtraiDiaUtil(ultimoDiaMaio, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            if (data != ultimoDiaMaio)
            {
                DateTime ultimoDiaNovembro = new DateTime(data.Year, 11, 30);
                if (!Calendario.IsDiaUtil(ultimoDiaNovembro, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    ultimoDiaNovembro = Calendario.SubtraiDiaUtil(ultimoDiaNovembro, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                if (data != ultimoDiaNovembro)
                {
                    return;
                }
            }
            #endregion
            
            CarteiraQuery carteiraQuery = new CarteiraQuery("C");
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            posicaoFundoQuery.Select(posicaoFundoQuery.IdCarteira);
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);

            if (prioridadeOperacaoFundo == PrioridadeOperacaoFundo.ResgateAntes)
            {
                posicaoFundoQuery.Where(carteiraQuery.PrioridadeOperacao == (byte)PrioridadeOperacaoFundo.ResgateAntes);
            }
            else
            {
                posicaoFundoQuery.Where(carteiraQuery.PrioridadeOperacao == (byte)PrioridadeOperacaoFundo.ResgateDepois);
            }

            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente),
                                    posicaoFundoQuery.Quantidade.NotEqual(0));
            posicaoFundoQuery.es.Distinct = true;
            posicaoFundoCollection.Load(posicaoFundoQuery);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                this.ProcessaComeCotas(idCliente, posicaoFundo.IdCarteira.Value, data);
            }
        }   

        /// <summary>
        /// FunçAo private chamada pela principal.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>  
        public void ProcessaComeCotas(int idCliente, int idCarteira, DateTime data)
        {
            #region Busca isenção IR/IOF do cliente e tipo de cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdCliente);
            campos.Add(cliente.Query.IsentoIR);
            campos.Add(cliente.Query.IsentoIOF);
            campos.Add(cliente.Query.IdTipo);
            cliente.LoadByPrimaryKey(campos, idCliente);
            #endregion

            if (cliente.IsIsentoIR() || cliente.IdTipo.Value == (int)TipoClienteFixo.InvestidorEstrangeiro)
            {
                return;
            }

            #region Busca os parâmetros da carteira, para cálculo dos tributos
            Carteira carteira = new Carteira();
            carteira.BuscaCarteiraCalculoTributo(idCarteira);
            int tipoTributacao = carteira.TipoTributacao.Value;
            int tipoCusto = carteira.TipoCusto.Value;
            int tipoCota = carteira.TipoCota.Value;
            bool truncaQuantidade = carteira.IsTruncaQuantidade();
            int casasDecimaisQuantidade = carteira.CasasDecimaisQuantidade.Value;
            bool calculaIOF = carteira.IsCalculaIOF();
            int idAgenteAdministrador = carteira.IdAgenteAdministrador.Value;
            #endregion

            if (tipoTributacao == (int)TipoTributacaoFundo.Acoes ||
                tipoTributacao == (int)TipoTributacaoFundo.CPrazo_SemComeCotas ||
                tipoTributacao == (int)TipoTributacaoFundo.LPrazo_SemComeCotas ||
                tipoTributacao == (int)TipoTributacaoFundo.Isento ||
                Carteira.RetornaCondominioCarteira(idCarteira, data) == (int)TipoFundo.Fechado ||
                carteira.Fie.Equals("S"))
            {
                return;
            }

            #region Deleta todas as operações de ComeCotas do dia na OperacaoFundo
            OperacaoFundoCollection operacaoFundoCollectionDeletar = new OperacaoFundoCollection();
            operacaoFundoCollectionDeletar.Query.Select(operacaoFundoCollectionDeletar.Query.IdOperacao);
            operacaoFundoCollectionDeletar.Query.Where(operacaoFundoCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                       operacaoFundoCollectionDeletar.Query.IdCarteira.Equal(idCarteira),
                                                       operacaoFundoCollectionDeletar.Query.DataOperacao.Equal(data),
                                                       operacaoFundoCollectionDeletar.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ComeCotas),
                                                       operacaoFundoCollectionDeletar.Query.Fonte.NotIn((byte)FonteOperacaoFundo.Ajustado,
                                                                                                        (byte)FonteOperacaoFundo.SMA,
                                                                                                        (byte)FonteOperacaoFundo.YMFSemCalculo));
            operacaoFundoCollectionDeletar.Query.Load();

            List<int> listaIdsOperacao = new List<int>();
            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollectionDeletar)
            {
                listaIdsOperacao.Add(operacaoFundo.IdOperacao.Value);
            }

            if (listaIdsOperacao.Count > 0)
            {
                DetalheResgateFundoCollection detalheResgateFundoCollectionDeletar = new DetalheResgateFundoCollection();
                detalheResgateFundoCollectionDeletar.Query.Where(detalheResgateFundoCollectionDeletar.Query.IdOperacao.In(listaIdsOperacao));
                detalheResgateFundoCollectionDeletar.Query.Load();
                detalheResgateFundoCollectionDeletar.MarkAllAsDeleted();
                detalheResgateFundoCollectionDeletar.Save();

                operacaoFundoCollectionDeletar.MarkAllAsDeleted();
                operacaoFundoCollectionDeletar.Save();
            }
            #endregion

            Carteira carteiraLiquidacao = new Carteira();
            int diasLiquidacaoResgate = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                       carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                       (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);
            
            #region Busca o valor da cota na data
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.BuscaValorCotaDia(idCarteira, data);

            decimal cotaDia;
            if (tipoCota == (int)TipoCotaFundo.Abertura)
            {
                cotaDia = historicoCota.CotaAbertura.Value;
            }
            else
            {
                cotaDia = historicoCota.CotaFechamento.Value;
            }
            #endregion

            //Caso 'N', o PAS não deve calcular come cotas, caso o cotista tenha algum resgate total agendado
            //Caso 'S', o PAS calcula o Come cotas normalmente
            bool ValidaResgateTotalAgendado = !carteira.ComeCotasEntreRegatesConversao.Equals("S");

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                               posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                               posicaoFundoCollection.Query.Quantidade.NotEqual(0));
            posicaoFundoCollection.Query.Load();

            int idContaDefault = 0;
            if (posicaoFundoCollection.Count > 0)
            {
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                //
            }

            //decimal totalValorIR = 0;
            List<int> lstFundoResgatesTotaisAgendados = new List<int>();
            for (int i = 0; i < posicaoFundoCollection.Count; i++)
            {
                #region Inicializa as variáveis principais
                PosicaoFundo posicaoFundo = posicaoFundoCollection[i];
                int idPosicao = posicaoFundo.IdPosicao.Value;
                DateTime dataAplicacao = posicaoFundo.DataAplicacao.Value;
                DateTime dataConversao = posicaoFundo.DataConversao.Value;

                decimal cotaAplicacao = 0;
                HistoricoCota historicoCotaAplicacao = new HistoricoCota();
                historicoCotaAplicacao.BuscaValorCotaDia(idCarteira, dataConversao, data);
                if (tipoCota == (int)TipoCotaFundo.Abertura)
                {
                    cotaAplicacao = historicoCotaAplicacao.CotaAbertura.Value;
                }
                else
                {
                    cotaAplicacao = historicoCotaAplicacao.CotaFechamento.Value;
                }

                decimal quantidade = posicaoFundo.Quantidade.Value;
                DateTime dataUltimaCobrancaIR = posicaoFundo.DataUltimaCobrancaIR.Value;                
                #endregion

                if (ValidaResgateTotalAgendado)
                {
                    if (lstFundoResgatesTotaisAgendados.Contains(idCliente))
                        continue;
                    else
                    {
                        OperacaoFundoCollection operacaoFundoColl = new OperacaoFundoCollection();
                        operacaoFundoColl.Query.Where(operacaoFundoColl.Query.IdCarteira.Equal(idCarteira) &
                                                        operacaoFundoColl.Query.IdCliente.Equal(idCliente) &
                                                        operacaoFundoColl.Query.TipoOperacao.Equal((int)TipoOperacaoFundo.ResgateTotal) &
                                                        operacaoFundoColl.Query.DataConversao.GreaterThanOrEqual(data));

                        if (operacaoFundoColl.Query.Load())
                        {
                            lstFundoResgatesTotaisAgendados.Add(idCliente);
                            continue;
                        }
                    }
                }

                decimal valorIOFVirtual = posicaoFundo.ValorIOFVirtual.Value;
                #region Calcula IOF Virtual proporcional à qtde de cotas deste comecotas

                if (valorIOFVirtual != 0)
                {
                    decimal quantidadeHistorica = 0;
                    DateTime dataPosicaoHistorica = CalculoTributo.RetornaDataComeCotasPosterior(dataConversao);
                    PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                    posicaoFundoHistorico.Query.Select(posicaoFundoHistorico.Query.Quantidade);
                    posicaoFundoHistorico.Query.Where(posicaoFundoHistorico.Query.IdPosicao.Equal(idPosicao),
                                                      posicaoFundoHistorico.Query.DataHistorico.Equal(dataPosicaoHistorica));
                    if (posicaoFundoHistorico.Query.Load())
                    {
                        quantidadeHistorica = posicaoFundoHistorico.Quantidade.Value;

                        valorIOFVirtual = Math.Round(quantidade / quantidadeHistorica * valorIOFVirtual, 2);
                    }                    
                }
                #endregion

                CalculoTributo calculoTributo = new CalculoTributo();
                
                #region Calcula rendimentos e IR (leva em conta eventual prejuízo a compensar)
                PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                decimal prejuizoCompensar = 0;
                if (carteira.RealizaCompensacaoDePrejuizo.Equals("S"))
                {                    
                    prejuizoCompensar = prejuizoFundo.RetornaPrejuizoCompensar(data, idAgenteAdministrador, tipoTributacao, idCliente);
                }
                decimal rendimento = 0;
                decimal rendimentoCompensado = 0;
                decimal prejuizoUsado = 0;
                decimal valorIR = 0;
                decimal valorIOF = 0;
                
                CalculoIRFundoRendaFixa calculoIRFundoRendaFixa = new CalculoIRFundoRendaFixa();
                calculoIRFundoRendaFixa.IdFundo = idCarteira;                
                calculoIRFundoRendaFixa.Quantidade = quantidade;
                calculoIRFundoRendaFixa.TipoCota = tipoCota;
                calculoIRFundoRendaFixa.CotaAplicacao = cotaAplicacao;
                calculoIRFundoRendaFixa.CotaCalculo = cotaDia;
                calculoIRFundoRendaFixa.DataAplicacao = dataAplicacao;
                calculoIRFundoRendaFixa.DataCalculo = data;
                calculoIRFundoRendaFixa.DataUltimaCobrancaIR = dataUltimaCobrancaIR;
                calculoIRFundoRendaFixa.IOFVirtual = valorIOFVirtual;
                calculoIRFundoRendaFixa.TipoTributacao = tipoTributacao;
                calculoIRFundoRendaFixa.PrejuizoCompensar = prejuizoCompensar;
                calculoIRFundoRendaFixa.DiasLiquidacaoResgate = diasLiquidacaoResgate;
                calculoIRFundoRendaFixa.IsentoIOF = cliente.IsIsentoIOF();
                calculoIRFundoRendaFixa.DataConversao = dataConversao;
                calculoIRFundoRendaFixa.IdPosicao = idPosicao;
                calculoIRFundoRendaFixa.QuantidadeAntesCortes = posicaoFundo.QuantidadeAntesCortes.GetValueOrDefault(0);
                calculoIRFundoRendaFixa.DiasLiquidacaoComeCotas = carteiraLiquidacao.RetornaDiasLiquidacaoComeCotas(idCarteira, data, carteira.DiasCotizacaoResgate.Value,
                                   carteira.DiasLiquidacaoResgate.Value, (ContagemDiasLiquidacaoResgate)carteira.ContagemDiasConversaoResgate.Value,
                                   (ContagemDiasPrazoIOF)carteira.ContagemPrazoIOF.Value);

                calculoTributo.CalculaIRComeCotasEvento(calculoIRFundoRendaFixa, FonteOperacaoCotista.ComeCotas, TipoComeCotas.Fundo, null);

                //Valor do IOR calculado
                valorIOF = calculoTributo.IOF;
                //Valor do IR calculado
                valorIR = calculoTributo.IR;
                //Rendimento sem abater o prejuízo 
                rendimento = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosterior;
                //Rendimento com abatimento do prejuízo 
                rendimentoCompensado = calculoTributo.RendimentoAnterior + calculoTributo.RendimentoPosteriorCompensado;
                //Valor do prejuízo usado para compensar rendimentos
                prejuizoUsado = calculoTributo.PrejuizoUsado;                
                #endregion

                #region Trata compensação de prejuízo (somente em caso de rendimento positivo)
                if (prejuizoCompensar != 0 && rendimento > 0)
                {
                    prejuizoFundo = new PrejuizoFundo();
                    prejuizoFundo.TrataCompensacaoPrejuizo(data, idAgenteAdministrador, tipoTributacao, idCarteira,
                                                           idCliente, rendimento, prejuizoUsado);
                }
                #endregion

                //#region Verifica se existem valores colados para a posicao processada
                //ColagemComeCotasPosFundo colagemComeCotasPosFundo = new ColagemComeCotasPosFundo();
                //colagemComeCotasPosFundo.Query.Where(colagemComeCotasPosFundo.Query.IdPosicaoVinculada.Equal(posicaoFundo.IdPosicao.Value)
                //                                    & colagemComeCotasPosFundo.Query.DataReferencia.Equal(data));
                //#endregion

                //if (colagemComeCotasPosFundo.Query.Load() || valorIR > 0)
                //{
                //    decimal quantidadeComeCotas;
                //    bool existeColagem = false;
                //    if (colagemComeCotasPosFundo.IdCarteira != null && colagemComeCotasPosFundo.IdCarteira.Value > 0)
                //    {
                //        ColagemComeCotas colagemComeCotas = new ColagemComeCotas();
                //        cotaDia = colagemComeCotasPosFundo.ValorBruto.Value / colagemComeCotasPosFundo.Quantidade.Value;
                //        quantidadeComeCotas = colagemComeCotasPosFundo.Quantidade.Value;
                //        valorIR = colagemComeCotasPosFundo.ValorIR.Value;
                //        valorIOF = colagemComeCotasPosFundo.ValorIOF.Value;
                //        prejuizoUsado = colagemComeCotasPosFundo.PrejuizoUsado.Value;
                //        rendimentoCompensado = colagemComeCotasPosFundo.RendimentoComeCotas.Value;
                //        rendimento = colagemComeCotasPosFundo.VariacaoComeCotas.Value;
                //        existeColagem = true;
                //    }
                //    else
                //    {
                //        #region Calcula quantidade de cotas do ComeCotas
                //        if (truncaQuantidade)
                //        {
                //            quantidadeComeCotas = Utilitario.Truncate(valorIR / cotaDia, casasDecimaisQuantidade);
                //        }
                //        else
                //        {
                //            quantidadeComeCotas = Math.Round(valorIR / cotaDia, casasDecimaisQuantidade);
                //        }
                //        #endregion                        
                //    }

                //    totalValorIR += valorIR;
                //    posicaoFundo.Quantidade -= quantidadeComeCotas;

                //    #region Lanca operacao em OperacaoFundo (tipo de operacao = ComeCotas)
                //    OperacaoFundo operacaoFundo = new OperacaoFundo();
                //    operacaoFundo.IdCliente = idCliente;
                //    operacaoFundo.IdCarteira = idCarteira;
                //    operacaoFundo.DataOperacao = data;
                //    operacaoFundo.DataConversao = data;
                //    operacaoFundo.DataLiquidacao = data;
                //    operacaoFundo.DataAgendamento = data;
                //    operacaoFundo.TipoOperacao = (int)TipoOperacaoFundo.ComeCotas;
                //    operacaoFundo.IdPosicaoResgatada = idPosicao;
                //    operacaoFundo.Quantidade = quantidadeComeCotas;
                //    operacaoFundo.CotaOperacao = cotaDia;
                //    operacaoFundo.ValorIR = valorIR;
                //    operacaoFundo.ValorIOF = valorIOF;
                //    operacaoFundo.PrejuizoUsado = prejuizoUsado;
                //    operacaoFundo.RendimentoResgate = rendimentoCompensado;
                //    operacaoFundo.VariacaoResgate = rendimento;
                //    operacaoFundo.IdFormaLiquidacao = 1; //AJUSTAR DEPOIS!!!
                //    operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Interno;
                //    operacaoFundo.ValoresColados = (existeColagem ? "S" : "N");
                    
                //    operacaoFundo.Save();
                //    #endregion

                //    #region Lanca em DetalheResgateFundo
                //    DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
                //    detalheResgateFundo.IdCarteira = idCarteira;
                //    detalheResgateFundo.IdCliente = idCliente;
                //    detalheResgateFundo.IdOperacao = operacaoFundo.IdOperacao.Value;
                //    detalheResgateFundo.IdPosicaoResgatada = idPosicao;
                //    detalheResgateFundo.Quantidade = quantidadeComeCotas;
                //    detalheResgateFundo.RendimentoResgate = rendimentoCompensado;
                //    detalheResgateFundo.ValorIR = valorIR;
                //    detalheResgateFundo.ValorIOF = valorIOF;
                //    detalheResgateFundo.VariacaoResgate = rendimento;
                //    detalheResgateFundo.ValorBruto = 0;
                //    detalheResgateFundo.ValorLiquido = 0;
                    
                //    detalheResgateFundo.Save();
                //    #endregion
                //}

                ////Este IOF abate do rendimento, portanto deve ser somado a rendimentos posteriores
                //posicaoFundo.ValorIOFVirtual = posicaoFundo.ValorIOFVirtual.Value - valorIOFVirtual + valorIOF;

                if (rendimento > 0)
                {
                    posicaoFundo.DataUltimaCobrancaIR = data;
                }
            }
            
            //Atualiza as collections
            posicaoFundoCollection.Save();            
        }

        /// <summary>
        /// Atualiza a coluna QuantidadeAntesCortes para garantir sua integridade no 1o dia de processamento do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaAtualizacaoCotasAntesCortes(int idCliente, DateTime data)
        {
            Cliente cliente = new Cliente();            
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IsentoIR);
            cliente.LoadByPrimaryKey(campos, idCliente);
            if (cliente.IsIsentoIR())
            {
                return;
            }

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira);
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                               posicaoFundoCollection.Query.Quantidade.GreaterThan(0),
                                               posicaoFundoCollection.Query.IdOperacao.IsNotNull());
            posicaoFundoCollection.Query.es.Distinct = true;
            posicaoFundoCollection.Query.Load();

            List<int> listaFundos = new List<int>();
            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                listaFundos.Add(posicaoFundo.IdCarteira.Value);
            }

            posicaoFundoCollection = new PosicaoFundoCollection();
            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                       posicaoFundoCollection.Query.Quantidade.GreaterThan(0),
                                                       posicaoFundoCollection.Query.Quantidade.Equal(posicaoFundoCollection.Query.QuantidadeAntesCortes),
                                                       posicaoFundoCollection.Query.IdOperacao.IsNull());

            if (listaFundos.Count > 0)
            {
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCarteira.NotIn(listaFundos));
            }

            posicaoFundoCollection.Query.Load();

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                decimal quantidadePosicao = posicaoFundo.Quantidade.Value;
                posicaoFundo.Quantidade = posicaoFundo.ValorAplicacao / posicaoFundo.CotaAplicacao;

                CalculoTributo calculoTributo = new CalculoTributo();
                decimal totalCotasCortes = calculoTributo.CalculaQuantidadeCortesHistoricos(data, posicaoFundo);

                posicaoFundo.Quantidade = quantidadePosicao;
                posicaoFundo.QuantidadeAntesCortes = quantidadePosicao + totalCotasCortes;
            }

            posicaoFundoCollection.Save();
        }

        /// <summary>
        /// Retorna o ValorBruto das posicoes de PosicaoFundo, filtrando a carteira e todas as posicoes de cliente cuja dataDia seja = data passada.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public decimal RetornaValorMercado(int idCarteira, DateTime data)
        {
            PosicaoFundo posicaoFundo = new PosicaoFundo();
            posicaoFundo.Query.Select(posicaoFundo.Query.ValorBruto.Sum());
            posicaoFundo.Query.Where(posicaoFundo.Query.IdCarteira.Equal(idCarteira),
                                     posicaoFundo.Query.Quantidade.GreaterThan(0));
            posicaoFundo.Query.Load();

            decimal valor = 0;
            if (posicaoFundo.ValorBruto.HasValue)
            {
                valor = posicaoFundo.ValorBruto.Value;
            }

            return valor;
        }

        /// <summary>
        /// Retorna o ValorBruto das posicoes de PosicaoFundo, filtrando a carteira e todas as posicoes de cliente cuja dataDia seja = data passada.
        /// Considera apenas fundos com nr de dias para liquidação de resgate menor ou igual que o nr de dias passado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public decimal RetornaValorMercado(int idCliente, int diasLiquidacao)
        {
            decimal valorTotal = 0;

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("P");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");

            posicaoFundoQuery.Select(carteiraQuery.IdCarteira, 
                                     carteiraQuery.DiasLiquidacaoResgate, 
                                     carteiraQuery.DiasCotizacaoResgate, 
                                     carteiraQuery.ContagemDiasConversaoResgate,
                                     posicaoFundoQuery.ValorBruto.Sum());
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == posicaoFundoQuery.IdCarteira);
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente),
                                    posicaoFundoQuery.Quantidade.GreaterThan(0));
            posicaoFundoQuery.GroupBy(carteiraQuery.IdCarteira, 
                                      carteiraQuery.DiasLiquidacaoResgate, 
                                      carteiraQuery.DiasCotizacaoResgate, 
                                      carteiraQuery.ContagemDiasConversaoResgate);
            posicaoFundoCollection.Load(posicaoFundoQuery);

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                decimal valor = posicaoFundo.ValorBruto.Value;

                byte contagemDias = Convert.ToByte(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.ContagemDiasConversaoResgate));
                int diasLiquidacaoFundo = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasLiquidacaoResgate));
                int diasConversaoFundo = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.DiasCotizacaoResgate));

                if (contagemDias == (byte)ContagemDiasLiquidacaoResgate.DiasCorridos)
                {
                    diasLiquidacaoFundo += diasConversaoFundo;
                }

                if (diasLiquidacao >= diasLiquidacaoFundo)
                {
                    valorTotal += valor;
                }                
            }

            return valorTotal;
        }
        
        /// <summary>
        /// Atualiza a PosicaoFundo com valorIR e valorIOF a partir do que foi lido do arquivo de posição da SMA.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void AtualizaTributosPosicaoSMA(int idCliente, DateTime data)
        {
            TabelaCargaPassivoCollection tabelaCargaPassivoCollection = new TabelaCargaPassivoCollection();
            tabelaCargaPassivoCollection.Query.Select(tabelaCargaPassivoCollection.Query.IdNotaOperacao,
                                                      tabelaCargaPassivoCollection.Query.ValorIR,
                                                      tabelaCargaPassivoCollection.Query.ValorIOF);
            tabelaCargaPassivoCollection.Query.Where(tabelaCargaPassivoCollection.Query.IdCotista.Equal(idCliente),
                                                     tabelaCargaPassivoCollection.Query.Data.Equal(data));
            tabelaCargaPassivoCollection.Query.Load();

            foreach (TabelaCargaPassivo tabelaCargaPassivo in tabelaCargaPassivoCollection)
            {
                decimal valorIR = tabelaCargaPassivo.ValorIR.Value;
                decimal valorIOF = tabelaCargaPassivo.ValorIOF.Value;
                int idNota = tabelaCargaPassivo.IdNotaOperacao.Value;                
                
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdPosicao,
                                                    posicaoFundoCollection.Query.ValorIR,
                                                    posicaoFundoCollection.Query.ValorIOF,
                                                    posicaoFundoCollection.Query.ValorBruto,
                                                    posicaoFundoCollection.Query.ValorLiquido);
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdPosicao.Equal(idNota));
                posicaoFundoCollection.Query.Load();

                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    posicaoFundo.ValorIR = valorIR;
                    posicaoFundo.ValorIOF = valorIOF;
                    posicaoFundo.ValorLiquido = posicaoFundo.ValorBruto.Value - posicaoFundo.ValorIR.Value - posicaoFundo.ValorIOF.Value;
                }

                posicaoFundoCollection.Save();                
            }
            
        }

        /// <summary>
        /// Retorna o saldo médio aplicado no fundo, dado o idCliente passado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idCarteira"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaSaldoMedioFundo(int idCliente, int idCarteira, DateTime dataInicio, DateTime dataFim)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            DateTime dataAux = dataInicio;

            decimal totalValor = 0;
            int cont = 0;
            while (dataAux <= dataFim)
            {
                if ((dataAux < cliente.DataDia.Value) || (cliente.Status.Value == (byte)StatusCliente.Divulgado))
                {
                    PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                    decimal valor = posicaoFundoHistorico.RetornaSumValorBruto(idCarteira, idCliente, dataAux);
                    totalValor += valor;
                }
                else
                {
                    PosicaoFundo posicaoFundo = new PosicaoFundo();
                    decimal valor = posicaoFundo.RetornaValorBruto(idCliente, idCarteira);
                    totalValor += valor;
                }

                if (totalValor != 0)
                {
                    cont++;
                }

                dataAux = Calendario.AdicionaDiaUtil(dataAux, 1);
            }

            if (cont == 0)
            {
                return 0;
            }

            return totalValor / cont;
        }

        /// <summary>
        /// Retorna uma lista única de ids de fundos alocados para a carteira do idCliente passado. Busca tanto em PosicaoFundo como em PosicaoFundoHistorico, para o periodo indicado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public List<int> RetornaListaIdsFundosEmPosicao(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.Status);
            cliente.LoadByPrimaryKey(campos, idCliente);

            List<int> lista = new List<int>();

            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira);
            posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                        posicaoFundoHistoricoCollection.Query.DataHistorico.GreaterThanOrEqual(dataInicio),
                                                        posicaoFundoHistoricoCollection.Query.DataHistorico.LessThanOrEqual(dataFim),
                                                        posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0));
            posicaoFundoHistoricoCollection.Query.es.Distinct = true;
            posicaoFundoHistoricoCollection.Query.Load();

            foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
            {
                int idCarteira = posicaoFundoHistorico.IdCarteira.Value;

                if (!lista.Contains(idCarteira))
                {
                    lista.Add(idCarteira);
                }
            }

            if (cliente.DataDia.Value == dataFim && cliente.Status.Value != (byte)StatusCliente.Divulgado)
            {
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira);
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoCollection.Query.es.Distinct = true;
                posicaoFundoCollection.Query.Load();

                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    int idCarteira = posicaoFundo.IdCarteira.Value;

                    if (!lista.Contains(idCarteira))
                    {
                        lista.Add(idCarteira);
                    }
                }
            }

            return lista;
        }

        /// <summary>
        /// Carrega posições de fundos a partir da PosicaoCotista, pela relação entre idCliente e idCotista.
        /// </summary>
        /// <param name="idCliente"></param>
        public void CarregaPosicoesCotista(int idCliente, DateTime data)
        {
            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            PosicaoFundoCollection posicaoFundoCollectionDeletar = new PosicaoFundoCollection();
            posicaoFundoCollectionDeletar.Query.Select(posicaoFundoCollectionDeletar.Query.IdPosicao);
            posicaoFundoCollectionDeletar.Query.Where(posicaoFundoCollectionDeletar.Query.IdCliente.Equal(idCliente));
            posicaoFundoCollectionDeletar.Query.Load();
            posicaoFundoCollectionDeletar.MarkAllAsDeleted();
            posicaoFundoCollectionDeletar.Save();

            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == posicaoCotistaHistoricoQuery.IdCotista);
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCotista.NotEqual(posicaoCotistaHistoricoQuery.IdCarteira) &
                                               posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0) &
                                               posicaoCotistaHistoricoQuery.DataHistorico.Equal(data) &
                                               ( cotistaQuery.IdCotista.Equal(idCliente) |
                                                 cotistaQuery.CodigoInterface.Like("%" + idCliente.ToString() + "%")));

            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

            foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
            {
                PosicaoFundo posicaoFundo = posicaoFundoCollection.AddNew();
                posicaoFundo.CotaAplicacao = posicaoCotistaHistorico.CotaAplicacao.Value;
                posicaoFundo.CotaDia = posicaoCotistaHistorico.CotaDia.Value;
                posicaoFundo.DataAplicacao = posicaoCotistaHistorico.DataAplicacao.Value;
                posicaoFundo.DataConversao = posicaoCotistaHistorico.DataConversao.Value;
                posicaoFundo.DataUltimaCobrancaIR = posicaoCotistaHistorico.DataUltimaCobrancaIR;
                posicaoFundo.DataUltimoCortePfee = posicaoCotistaHistorico.DataUltimoCortePfee;
                posicaoFundo.IdCarteira = posicaoCotistaHistorico.IdCarteira.Value;
                posicaoFundo.IdCliente = idCliente;
                posicaoFundo.PosicaoIncorporada = posicaoCotistaHistorico.PosicaoIncorporada;
                posicaoFundo.Quantidade = posicaoCotistaHistorico.Quantidade.Value;
                posicaoFundo.QuantidadeAntesCortes = posicaoCotistaHistorico.QuantidadeAntesCortes.Value;
                posicaoFundo.QuantidadeBloqueada = posicaoCotistaHistorico.QuantidadeBloqueada.Value;
                posicaoFundo.QuantidadeInicial = posicaoCotistaHistorico.QuantidadeInicial.Value;
                posicaoFundo.ValorAplicacao = posicaoCotistaHistorico.ValorAplicacao.Value;
                posicaoFundo.ValorBruto = posicaoCotistaHistorico.ValorBruto.Value;
                posicaoFundo.ValorIOF = posicaoCotistaHistorico.ValorIOF.Value;
                posicaoFundo.ValorIOFVirtual = posicaoCotistaHistorico.ValorIOFVirtual.Value;
                posicaoFundo.ValorIR = posicaoCotistaHistorico.ValorIR.Value;
                posicaoFundo.ValorLiquido = posicaoCotistaHistorico.ValorLiquido.Value;
                posicaoFundo.ValorPerformance = posicaoCotistaHistorico.ValorPerformance.Value;
                posicaoFundo.ValorRendimento = posicaoCotistaHistorico.ValorRendimento.Value;
            }

            posicaoFundoCollection.Save();
        }

        /// <summary>
        /// Checa pelos parâmetros passados se o cliente tem saldo livre suficiente dada ou a qtde ou valor passado.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="idCliente"></param>
        /// <param name="idPosicao"></param>
        /// <param name="quantidade"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public bool TemSaldoLivre(int idCarteira, int idCliente, int? idPosicao, decimal? quantidade, decimal? valor)
        {
            PosicaoFundo posicaoFundo = new PosicaoFundo();
            posicaoFundo.Query.Select(posicaoFundo.Query.Quantidade.Sum(),
                                        posicaoFundo.Query.QuantidadeBloqueada.Sum(),
                                        posicaoFundo.Query.CotaDia.Avg());
            posicaoFundo.Query.Where(posicaoFundo.Query.IdCarteira.Equal(idCarteira),
                                       posicaoFundo.Query.IdCliente.Equal(idCliente));

            if (idPosicao.HasValue)
            {
                posicaoFundo.Query.Where(posicaoFundo.Query.IdPosicao.Equal(idPosicao.Value));
            }

            posicaoFundo.Query.Load();

            decimal quantidadeLivre = 0;
            decimal valorLivre = 0;
            if (posicaoFundo.Quantidade.HasValue)
            {
                quantidadeLivre = posicaoFundo.Quantidade.Value - posicaoFundo.QuantidadeBloqueada.Value;
                valorLivre = quantidadeLivre * posicaoFundo.CotaDia.Value;
            }

            bool temSaldoLivre = true;
            if (quantidade.HasValue)
            {
                if (quantidade.Value < quantidadeLivre)
                {
                    temSaldoLivre = false;
                }
            }

            if (valor.HasValue)
            {
                if (valor.Value < valorLivre)
                {
                    temSaldoLivre = false;
                }
            }

            return temSaldoLivre;
        }
    }
}