﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class CalculoPerformanceHistorico : esCalculoPerformanceHistorico
	{
        /// <summary>
        /// Adiciona uma coluna Virtual dinamicamente
        /// O Tipo da Coluna adicionado é considerado como sendo String
        /// </summary>
        /// <param name="column"></param>
        public void AddVirtualColumn(string column)
        {
            //this.Table = new DataTable();
            if (this.Table != null && !this.Table.Columns.Contains(column))
            {
                this.Table.Columns.Add(column, typeof(System.String));
            }
        }

        /// <summary>
        /// Retorna o valor de performance acumulado (total) histórico para o idCarteira.
        /// Somente valores positivos.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>        
        public decimal RetornaValorPerformance(int idCarteira, DateTime dataHistorico)
        {
            return this.RetornaValorPerformance(idCarteira, dataHistorico, false);
        }

        /// <summary>
        /// Retorna o valor de performance acumulado (total) histórico para o idCarteira.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>        
        public decimal RetornaValorPerformance(int idCarteira, DateTime dataHistorico, bool somentePositivo)
        {
            this.QueryReset();
            this.Query.Select(this.Query.ValorAcumulado.Sum());
            this.Query.Where(this.Query.IdCarteira == idCarteira,
                             this.Query.DataHistorico.Equal(dataHistorico));

            if (somentePositivo)
            {
                this.Query.Where(this.Query.ValorAcumulado.GreaterThan(0));
            }

            this.Query.Load();
            
            return this.ValorAcumulado.HasValue ? this.ValorAcumulado.Value : 0;
        }

	}
}
