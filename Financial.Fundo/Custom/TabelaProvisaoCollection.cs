﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Fundo
{
	public partial class TabelaProvisaoCollection : esTabelaProvisaoCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaProvisaoCollection));

        public DataTable GetDataTable {
            get {
                return this.Table;
            }
        }

        /// <summary>
        /// Carrega o objeto TabelaProvisaoCollection com todos os campos de TabelaProvisao.
        /// Ordena decrescente por IdTabela, DataReferencia.
        /// Filtra somente com DataReferencia menor ou igual que a data, 
        /// e data menor ou igual a DataFim (ou DataFim = NULL).
        /// Excluem-se os registros mais antigos de IdCadastro (ficam apenas os mais recentes para cada IdCadastro).
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>  
        /// <returns>booleano para indicar se achou registro.</returns>
        public bool BuscaTabelaProvisao(int idCarteira, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.Or(
                        this.Query.DataFim.GreaterThanOrEqual(data),
                        this.Query.DataFim.IsNull()),
                        this.Query.DataReferencia.LessThanOrEqual(data))
                 .OrderBy(this.Query.IdCadastro.Descending,
                          this.Query.DataReferencia.Descending);
            
            return this.Query.Load();
            
        }

        /// <summary>
        /// Deleta todas as provisões do lançadas no dia de TabelaProvisao, pelo idCadastro e idCarteira.
        /// </summary>
        /// <param name="idCarteira"></param>  
        /// <param name="data"></param>        
        /// <param name="idCadastro"></param>
        public void DeletaTabelaProvisao(int idCarteira, DateTime data, int idCadastro)
        {
            // TODO log da funcao                              

            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCarteira == idCarteira,
                        this.Query.DataReferencia.Equal(data),
                        this.Query.IdCadastro == idCadastro);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();

            #region Log Sql
            if (log.IsInfoEnabled)
            {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@IdCarteira1", "'" + idCarteira + "'");
                sql = sql.Replace("@DataReferencia2", "'" + data + "'");
                sql = sql.Replace("@IdCadastro3", "'" + idCadastro + "'");
                log.Info(sql);
            }
            #endregion

            #region logSaida
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion            
        }
	}
}
