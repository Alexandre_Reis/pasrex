using log4net;
using Financial.Fundo.Enums;
using System.Collections.Generic;
using System;
using Financial.Util;
using Financial.Common.Enums;
using System.Text;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo.Exceptions;
using Financial.Investidor.Enums;
using Financial.ContaCorrente;
using Financial.InvestidorCotista;
using Financial.ContaCorrente.Controller;
using Financial.ContaCorrente.Enums;
using Financial.Enquadra.Controller;
using Financial.Captacao;
using Financial.Captacao.Enums;
using Financial.Investidor;
using System.Data;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Controller;
using Financial.Fundo.Exportacao.YMF;
using Financial.Common;
using Financial.Bolsa;
using Financial.Util.Enums;

namespace Financial.Fundo.Controller
{
    public class ControllerFundo
    {
        /// <summary>
        /// Reprocessa CalculoAdministracao e CalculoPerformance.
        /// Usado apenas para TipoControle = SomenteCotacao e TipoVisaoFundo != NaoTrata.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoFundo
        public void ReprocessaAdmPfee(int idCarteira, DateTime data, TipoReprocessamento tipoReprocessamento)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            int idLocal = cliente.IdLocal.Value;

            DateTime dataHistorico = data;
            if (tipoReprocessamento != TipoReprocessamento.Fechamento)
            {
                if (idLocal == LocalFeriadoFixo.Brasil)
                {
                    dataHistorico = Calendario.SubtraiDiaUtil(data, 1);
                }
                else
                {
                    dataHistorico = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                }
            }

            #region Tratamento das tabelas de cálculo (administração, performance)
            #region Delete CalculoAdministracao
            CalculoAdministracaoCollection calculoAdministracaoCollectionDeletar = new CalculoAdministracaoCollection();
            try
            {
                calculoAdministracaoCollectionDeletar.DeletaCalculoAdministracao(idCarteira);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoAdministracao");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoAdministracaoAssociada
            CalculoAdministracaoAssociadaCollection calculoAdministracaoAssociadaCollectionDeletar = new CalculoAdministracaoAssociadaCollection();
            try
            {
                calculoAdministracaoAssociadaCollectionDeletar.DeletaCalculoAdministracao(idCarteira);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoAdministracaoAssociada");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoPerformance
            CalculoPerformanceCollection calculoPerformanceCollectionDeletar = new CalculoPerformanceCollection();
            try
            {
                calculoPerformanceCollectionDeletar.DeletaCalculoPerformance(idCarteira);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoPerformance");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Insert CalculoAdministracao a partir de CalculoAdministracaoHistorico
            CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();
            try
            {
                calculoAdministracaoCollection.InsereCalculoAdministracaoHistorico(idCarteira, dataHistorico);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Insert CalculoAdministracao");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Insert CalculoAdministracaoAssociada a partir de CalculoAdministracaoCalculoAdministracaoAssociadaHistorico
            CalculoAdministracaoAssociadaCollection calculoAdministracaoAssociadaCollection = new CalculoAdministracaoAssociadaCollection();
            try
            {
                calculoAdministracaoAssociadaCollection.InsereCalculoAdministracaoHistorico(idCarteira, dataHistorico);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Insert CalculoAdministracaoAssociada");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Insert CalculoPerformance a partir de CalculoPerformanceHistorico
            CalculoPerformanceCollection calculoPerformanceCollection = new CalculoPerformanceCollection();
            try
            {
                calculoPerformanceCollection.InsereCalculoPerformanceHistorico(idCarteira, dataHistorico);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Insert CalculoPerformance");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            this.DeletaCalculoTaxasHistorico(idCarteira, data);
            #endregion
        }

        /// <summary>
        /// Deleta CalculoAdministracaoHistorico e CalculoPerformanceHistorico para idCarteira e data passados.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void DeletaCalculoTaxasHistorico(int idCarteira, DateTime data)
        {
            CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();
            calculoAdministracaoHistoricoCollection.DeletaCalculoAdministracaoHistoricoDataHistoricoMaiorIgual(idCarteira, data);

            CalculoAdministracaoAssociadaHistoricoCollection calculoAdministracaoAssociadaHistoricoCollection = new CalculoAdministracaoAssociadaHistoricoCollection();
            calculoAdministracaoAssociadaHistoricoCollection.DeletaCalculoAdministracaoHistoricoDataHistoricoMaiorIgual(idCarteira, data);

            CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();
            calculoPerformanceHistoricoCollection.DeletaCalculoPerformanceHistoricoDataHistoricoMaiorIgual(idCarteira, data);
        }

        /// <summary>
        /// Deleta PosicaoFundoHistorico e PrejuizoFundoHistorico para idCarteira e data passados.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void DeletaPosicaoFundoHistorico(int idCarteira, DateTime data)
        {
            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.DeletaPosicaoFundoHistoricoDataHistoricoMaiorIgual(idCarteira, data);

            PrejuizoFundoHistoricoCollection prejuizoFundoHistoricoCollection = new PrejuizoFundoHistoricoCollection();
            //
            prejuizoFundoHistoricoCollection.Query.Where(
                            prejuizoFundoHistoricoCollection.Query.IdCliente == idCarteira,
                            prejuizoFundoHistoricoCollection.Query.DataHistorico.GreaterThanOrEqual(data));


            prejuizoFundoHistoricoCollection.Query.Load();

            prejuizoFundoHistoricoCollection.MarkAllAsDeleted();
            prejuizoFundoHistoricoCollection.Save();

        }


        /// <summary>
        /// Reprocessa as posições por tipo = abertura ou tipo = fechamento.
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoFundo
        public void ReprocessaPosicao(int idCarteira, DateTime data, TipoReprocessamento tipoReprocessamento, bool processaMae, bool cravaCota)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            int idLocal = cliente.IdLocal.Value;

            DateTime dataHistorico = data;
            if (tipoReprocessamento != TipoReprocessamento.Fechamento)
            {
                if (idLocal == LocalFeriadoFixo.Brasil)
                {
                    dataHistorico = Calendario.SubtraiDiaUtil(data, 1);
                }
                else
                {
                    dataHistorico = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                }
            }

            #region Tratamento das tabelas de cálculo (administração, performance, provisão)
            #region Delete CalculoAdministracao
            CalculoAdministracaoCollection calculoAdministracaoCollectionDeletar = new CalculoAdministracaoCollection();
            try
            {
                calculoAdministracaoCollectionDeletar.DeletaCalculoAdministracao(idCarteira);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoAdministracao");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoAdministracaoAssociada
            CalculoAdministracaoAssociadaCollection calculoAdministracaoAssociadaCollectionDeletar = new CalculoAdministracaoAssociadaCollection();
            try
            {
                calculoAdministracaoAssociadaCollectionDeletar.DeletaCalculoAdministracao(idCarteira);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoAdministracaoAssociada");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoPerformance
            CalculoPerformanceCollection calculoPerformanceCollectionDeletar = new CalculoPerformanceCollection();
            try
            {
                calculoPerformanceCollectionDeletar.DeletaCalculoPerformance(idCarteira);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoPerformance");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoProvisao
            CalculoProvisaoCollection calculoProvisaoCollectionDeletar = new CalculoProvisaoCollection();
            try
            {
                calculoProvisaoCollectionDeletar.DeletaCalculoProvisao(idCarteira);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoPerformance");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Insert CalculoAdministracao a partir de CalculoAdministracaoHistorico
            CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();
            try
            {
                calculoAdministracaoCollection.InsereCalculoAdministracaoHistorico(idCarteira, dataHistorico);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Insert CalculoAdministracao");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Insert CalculoAdministracaoAssociada a partir de CalculoAdministracaoAssociadaHistorico
            CalculoAdministracaoAssociadaCollection calculoAdministracaoAssociadaCollection = new CalculoAdministracaoAssociadaCollection();
            try
            {
                calculoAdministracaoAssociadaCollection.InsereCalculoAdministracaoHistorico(idCarteira, dataHistorico);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Insert CalculoAdministracaoAssociada");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Insert CalculoPerformance a partir de CalculoPerformanceHistorico
            CalculoPerformanceCollection calculoPerformanceCollection = new CalculoPerformanceCollection();
            try
            {
                calculoPerformanceCollection.InsereCalculoPerformanceHistorico(idCarteira, dataHistorico);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Insert CalculoPerformance");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Insert CalculoProvisao a partir de CalculoProvisaoHistorico
            CalculoProvisaoCollection calculoProvisaoCollection = new CalculoProvisaoCollection();
            try
            {
                calculoProvisaoCollection.InsereCalculoProvisaoHistorico(idCarteira, dataHistorico);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Insert CalculoProvisao");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoAdministracaoHistorico
            CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();
            try
            {
                calculoAdministracaoHistoricoCollection.DeletaCalculoAdministracaoHistoricoDataHistoricoMaiorIgual(idCarteira, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoAdministracaoHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoAdministracaoAssociadaHistorico
            CalculoAdministracaoAssociadaHistoricoCollection calculoAdministracaoAssociadaHistoricoCollection = new CalculoAdministracaoAssociadaHistoricoCollection();
            try
            {
                calculoAdministracaoAssociadaHistoricoCollection.DeletaCalculoAdministracaoHistoricoDataHistoricoMaiorIgual(idCarteira, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoAdministracaoAssociadaHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoPerformanceHistorico
            CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();
            try
            {
                calculoPerformanceHistoricoCollection.DeletaCalculoPerformanceHistoricoDataHistoricoMaiorIgual(idCarteira, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoPerformanceHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoProvisaoHistorico
            CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
            try
            {
                calculoProvisaoHistoricoCollection.DeletaCalculoProvisaoHistoricoDataHistoricoMaiorIgual(idCarteira, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoProvisaoHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #endregion


            if (!processaMae)
            {
                this.ReprocessaPosicaoFundo(idCarteira, data, tipoReprocessamento, dataHistorico);
            }

            if (!cravaCota)
            {
                #region Tratamento do histórico da cota
                HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
                historicoCotaCollection.DeletaHistoricoCota(idCarteira, data);

                //Zera a cota, PL e quantidade de cotas do dia
                if (tipoReprocessamento != TipoReprocessamento.Fechamento)
                {
                    HistoricoCota historicoCota = new HistoricoCota();
                    if (historicoCota.LoadByPrimaryKey(data, idCarteira)) //CRAVA COTA (RETIRAR O UPDATE DA COTA DE FECHAMENTO)
                    {
                        historicoCota.CotaFechamento = 0;
                        historicoCota.QuantidadeFechamento = 0;
                        historicoCota.PLFechamento = 0;
                        historicoCota.Save();
                    }
                }
                //
                #endregion
            }

            this.ReprocessaPosicaoCotista(idCarteira, data, tipoReprocessamento);
        }

        /// <summary>
        /// Reprocessa as posições de PosicaoFundo
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// Trata tb o reprocessamento da PrejuizoFundo.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoFundo
        public void ReprocessaPosicaoFundo(int idCarteira, DateTime data, TipoReprocessamento tipoReprocessamento, DateTime dataHistorico)
        {
            #region Delete da PosicaoFundo
            PosicaoFundoCollection posicaoFundoCollectionDeletar = new PosicaoFundoCollection();
            posicaoFundoCollectionDeletar.DeletaPosicaoFundo(idCarteira);
            #endregion

            #region Inserts das Posições baseado nas posições de fechamento, ou de abertura
            //Para SqlServer precisa dar um tratamento especial para a PK Identity
            // Nome da Configuração Default SQL
            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    esUtility u = new esUtility();
                    string sql = " set identity_insert PosicaoFundo on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert PosicaoFundo a partir de PosicaoFundoHistorico
                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        try
                        {
                            posicaoFundoCollection.InserePosicaoFundoHistorico(idCarteira, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoFundo");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert PosicaoFundo a partir de PosicaoFundoAbertura
                        PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                        try
                        {
                            posicaoFundoCollection.InserePosicaoFundoAbertura(idCarteira, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoFundo");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    u = new esUtility();
                    sql = " set identity_insert PosicaoFundo off ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    scope.Complete();
                }

            }
            else
            {
                //TODO ESCREVER NA UNHA O SQL, POIS O ORACLE NÃO PERMITE DESLIGAR A SEQUENCE
                if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                {
                    #region Insert PosicaoFundo a partir de PosicaoFundoHistorico
                    PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                    try
                    {
                        posicaoFundoCollection.InserePosicaoFundoHistorico(idCarteira, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoFundo");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
                else
                {
                    #region Insert PosicaoFundo a partir de PosicaoFundoAbertura
                    PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                    try
                    {
                        posicaoFundoCollection.InserePosicaoFundoAbertura(idCarteira, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoFundo");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
            }
            #endregion

            #region Delete PosicaoFundoAbertura
            PosicaoFundoAberturaCollection posicaoFundoAberturaCollection = new PosicaoFundoAberturaCollection();
            try
            {
                posicaoFundoAberturaCollection.DeletaPosicaoFundoAberturaDataHistoricoMaior(idCarteira, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoFundoAbertura");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion


            #region Delete PrejuizoFundo
            PrejuizoFundoCollection prejuizoFundoCollectionDeletar = new PrejuizoFundoCollection();
            prejuizoFundoCollectionDeletar.Query.Select(prejuizoFundoCollectionDeletar.Query.IdCliente,
                                                      prejuizoFundoCollectionDeletar.Query.IdCarteira,
                                                      prejuizoFundoCollectionDeletar.Query.Data)
                                              .Where(prejuizoFundoCollectionDeletar.Query.IdCliente == idCarteira);
            prejuizoFundoCollectionDeletar.Query.Load();
            prejuizoFundoCollectionDeletar.MarkAllAsDeleted();
            //
            prejuizoFundoCollectionDeletar.Save();
            #endregion

            #region Insert PrejuizoFundo a partir de PrejuizoFundoHistorico
            //
            PrejuizoFundoHistoricoCollection p = new PrejuizoFundoHistoricoCollection();
            p.Query.Where(p.Query.IdCliente == idCarteira,
                          p.Query.DataHistorico == dataHistorico);
            //
            p.Query.Load();
            //
            foreach (PrejuizoFundoHistorico prejuizoFundoHistorico in p)
            {
                PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
                prejuizoFundo.Data = prejuizoFundoHistorico.Data;
                prejuizoFundo.DataLimiteCompensacao = prejuizoFundoHistorico.DataLimiteCompensacao;
                prejuizoFundo.IdCarteira = prejuizoFundoHistorico.IdCarteira;
                prejuizoFundo.IdCliente = prejuizoFundoHistorico.IdCliente;
                prejuizoFundo.ValorPrejuizo = prejuizoFundoHistorico.ValorPrejuizo;
                prejuizoFundo.Save();
            }
            #endregion

            this.DeletaPosicaoFundoHistorico(idCarteira, data); //Deleta PosicaoFundoHistorico e PrejuizoFundoHistorico

            
            #region Delete CalculoAdministracao 

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(campos, idCarteira);

            int idLocal = cliente.IdLocal.Value;
            DateTime dataHistoricoMenos1 = data;
            if (tipoReprocessamento != TipoReprocessamento.Fechamento)
            {
                if (idLocal == LocalFeriadoFixo.Brasil)
                {
                    dataHistoricoMenos1 = Calendario.SubtraiDiaUtil(data, 1);
                }
                else
                {
                    dataHistoricoMenos1 = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                }
            }
            CalculoAdministracaoCollection calculoAdministracaoCollectionDeletar = new CalculoAdministracaoCollection();
            try
            {
                calculoAdministracaoCollectionDeletar.DeletaCalculoAdministracao(idCarteira);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoAdministracao");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete CalculoPerformance
            CalculoPerformanceCollection calculoPerformanceCollectionDeletar = new CalculoPerformanceCollection();
            try
            {
                calculoPerformanceCollectionDeletar.DeletaCalculoPerformance(idCarteira);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete CalculoPerformance");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Insert CalculoAdministracao a partir de CalculoAdministracaoHistorico
            CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();
            try
            {
                calculoAdministracaoCollection.InsereCalculoAdministracaoHistorico(idCarteira, dataHistoricoMenos1);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Insert CalculoAdministracao");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Insert CalculoPerformance a partir de CalculoPerformanceHistorico
            CalculoPerformanceCollection calculoPerformanceCollection = new CalculoPerformanceCollection();
            try
            {
                calculoPerformanceCollection.InsereCalculoPerformanceHistorico(idCarteira, dataHistoricoMenos1);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Insert CalculoPerformance");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion


        }

        /// <summary>
        /// Reprocessa as posições de cotista, por tipo = abertura ou tipo = fechamento.
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// Trata tb o reprocessamento da PrejuizoCotista.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoFundo
        public void ReprocessaPosicaoCotista(int idCarteira, DateTime data, TipoReprocessamento tipoReprocessamento)
        {
            #region Verifica o tipo de cota da carteira (abertura ou fechamento)
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            if (!carteira.LoadByPrimaryKey(campos, idCarteira))
                return;

            int tipoCota = carteira.TipoCota.Value;
            #endregion

            Cliente cliente = new Cliente();
            List<esQueryItem> camposCliente = new List<esQueryItem>();
            camposCliente.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(camposCliente, idCarteira);

            int idLocal = cliente.IdLocal.Value;

            DateTime dataHistorico = data;
            if (tipoCota == (int)TipoCotaFundo.Fechamento && tipoReprocessamento != TipoReprocessamento.Fechamento)
            {
                if (idLocal == LocalFeriadoFixo.Brasil)
                {
                    dataHistorico = Calendario.SubtraiDiaUtil(data, 1);
                }
                else
                {
                    dataHistorico = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                }
            }

            #region Tratamento das posições de cotista - PosicaoCotista, PosicaoCotistaAbertura, PosicaoCotistaHistorico
            #region Delete da PosicaoCotista
            PosicaoCotistaCollection posicaoCotistaCollectionDeletar = new PosicaoCotistaCollection();
            posicaoCotistaCollectionDeletar.DeletaPosicaoCotista(idCarteira);
            #endregion

            #region Inserts das Posições baseado nas posições de fechamento, ou de abertura
            //Para SqlServer precisa dar um tratamento especial para a PK Identity
            // Nome da Configuração Default SQL
            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    esUtility u = new esUtility();
                    string sql = " set identity_insert PosicaoCotista on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert PosicaoCotista a partir de PosicaoCotistaHistorico
                        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                        try
                        {
                            posicaoCotistaCollection.InserePosicaoCotistaHistorico(idCarteira, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoCotista");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());

                            if (e.Number == 2627)
                            {
                                throw new Exception("Número de nota duplicado na tabela de Posição de Cotistas");
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert PosicaoCotista a partir de PosicaoCotistaAbertura
                        PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                        try
                        {
                            posicaoCotistaCollection.InserePosicaoCotistaAbertura(idCarteira, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoCotista");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());

                            if (e.Number == 2627)
                            {
                                throw new Exception("Número de nota duplicado na tabela de Posição de Cotistas");
                            }
                        }
                        #endregion
                    }

                    u = new esUtility();
                    sql = " set identity_insert PosicaoCotista off ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    scope.Complete();
                }

            }
            else
            {
                //TODO ESCREVER NA UNHA O SQL, POIS O ORACLE NÃO PERMITE DESLIGAR A SEQUENCE
                if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                {
                    #region Insert PosicaoCotista a partir de PosicaoCotistaHistorico
                    PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                    try
                    {
                        posicaoCotistaCollection.InserePosicaoCotistaHistorico(idCarteira, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoCotista");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());

                        if (e.Number == 2627)
                        {
                            throw new Exception("Número de nota duplicado na tabela de Posição de Cotistas");
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Insert PosicaoCotista a partir de PosicaoCotistaAbertura
                    PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                    try
                    {
                        posicaoCotistaCollection.InserePosicaoCotistaAbertura(idCarteira, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoCotista");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());

                        if (e.Number == 2627)
                        {
                            throw new Exception("Número de nota duplicado na tabela de Posição de Cotistas");
                        }
                    }
                    #endregion
                }
            }
            #endregion

            #region Delete PosicaoCotistaHistorico
            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
            try
            {
                posicaoCotistaHistoricoCollection.DeletaPosicaoCotistaHistoricoDataHistoricoMaiorIgual(idCarteira, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoCotistaHistorico");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete PosicaoCotistaAbertura
            PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
            try
            {
                posicaoCotistaAberturaCollection.DeletaPosicaoCotistaAberturaDataHistoricoMaior(idCarteira, data);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoCotistaAbertura");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #endregion

            #region Deleta Operações geradas no processamento
            this.DeletaOperacaoInterna(idCarteira, data);
            #endregion

            #region Delete PrejuizoCotistaUsado
            PrejuizoCotistaUsadoCollection prejuizoCotistaUsadoCollection = new PrejuizoCotistaUsadoCollection();
            prejuizoCotistaUsadoCollection.Query.Where(prejuizoCotistaUsadoCollection.Query.IdCarteira.Equal(idCarteira),
                                                       prejuizoCotistaUsadoCollection.Query.DataCompensacao.GreaterThanOrEqual(data));
            prejuizoCotistaUsadoCollection.Query.Load();

            if (prejuizoCotistaUsadoCollection.Count > 0)
            {
                OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                      operacaoCotistaCollection.Query.DataOperacao.GreaterThanOrEqual(data));
                if (operacaoCotistaCollection.Query.Load())
                {
                    foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
                    {
                        operacaoCotista.PrejuizoUsado = 0;
                    }
                    operacaoCotistaCollection.Save();
                }

            }

            prejuizoCotistaUsadoCollection.MarkAllAsDeleted();
            prejuizoCotistaUsadoCollection.Save();


            #endregion


            #region Tratamento de PrejuizoCotista
            if (!(tipoCota == (int)TipoCotaFundo.Abertura && tipoReprocessamento != TipoReprocessamento.CalculoDiario))
            {
                #region Delete PrejuizoCotista
                PrejuizoCotistaCollection prejuizoCotistaCollectionDeletar = new PrejuizoCotistaCollection();
                prejuizoCotistaCollectionDeletar.Query.Select(prejuizoCotistaCollectionDeletar.Query.IdCotista,
                                                              prejuizoCotistaCollectionDeletar.Query.IdCarteira,
                                                              prejuizoCotistaCollectionDeletar.Query.Data)
                                                      .Where(prejuizoCotistaCollectionDeletar.Query.IdCarteira == idCarteira);
                prejuizoCotistaCollectionDeletar.Query.Load();
                prejuizoCotistaCollectionDeletar.MarkAllAsDeleted();
                //
                prejuizoCotistaCollectionDeletar.Save();
                #endregion

                #region Insert PrejuizoCotista a partir de PrejuizoCotistaHistorico
                //
                PrejuizoCotistaHistoricoCollection p = new PrejuizoCotistaHistoricoCollection();
                p.Query.Where(p.Query.IdCarteira == idCarteira,
                              p.Query.DataHistorico == dataHistorico);
                //
                p.Query.Load();
                //
                PrejuizoCotistaCollection prejuizoCotistaCollection = new PrejuizoCotistaCollection(p);
                prejuizoCotistaCollection.Save();
                #endregion
            }

            #region Delete PrejuizoCotistaHistorico
            PrejuizoCotistaHistoricoCollection prejuizoCotistaHistoricoCollection = new PrejuizoCotistaHistoricoCollection();
            //
            prejuizoCotistaHistoricoCollection.Query.Where(
                            prejuizoCotistaHistoricoCollection.Query.IdCarteira == idCarteira,
                            prejuizoCotistaHistoricoCollection.Query.DataHistorico.GreaterThanOrEqual(data));

            prejuizoCotistaHistoricoCollection.Query.Load();

            prejuizoCotistaHistoricoCollection.MarkAllAsDeleted();
            prejuizoCotistaHistoricoCollection.Save();
            #endregion

            #endregion
        }

        /// <summary>
        /// Calcula o saldo de posições em fundo, calcula tx adm/gestão e performance.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ExecutaFechamento(int idCarteira, DateTime data, ParametrosProcessamento parametrosProcessamento)
        {
            this.ExecutaFechamento(idCarteira, data, parametrosProcessamento, false);
        }

        /// <summary>
        /// Calcula o saldo de posições em fundo, calcula tx adm/gestão e performance.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ExecutaFechamento(int idCarteira, DateTime data, ParametrosProcessamento parametrosProcessamento, bool processaMae)
        {
            this.ExecutaFechamento(idCarteira, data, parametrosProcessamento, processaMae, false);
        }

        /// <summary>
        /// Calcula o saldo de posições em fundo, calcula tx adm/gestão e performance.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ExecutaFechamento(int idCarteira, DateTime data, ParametrosProcessamento parametrosProcessamento, bool processaMae, bool processamentoValoresColados)
        {
            bool existeColagem = false;
            bool processaEventos = true;
            if (!processamentoValoresColados)
            {
                ColagemResgate colagemResgate = new ColagemResgate();
                ColagemComeCotas colagemComeCotas = new ColagemComeCotas();
                existeColagem = colagemResgate.VerificaSeExisteValoresColadosResgates(idCarteira, data) || colagemComeCotas.VerificaSeExisteValoresColadosComeCotas(idCarteira, data);
                //Se existe colagem, processa os eventos após a colagem dos valores
                processaEventos = !existeColagem;
            }
        
            #region Busca TipoCota, CalculaEnquadra, TipoVisaoFundo, TipoTributacao, PrioridadeOperacao, TipoCalculoRetorno, CodigoAnbid em Carteira
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.CalculaEnquadra);
            campos.Add(carteira.Query.TipoVisaoFundo);
            campos.Add(carteira.Query.TipoTributacao);
            campos.Add(carteira.Query.PrioridadeOperacao);
            campos.Add(carteira.Query.TipoCalculoRetorno);
            campos.Add(carteira.Query.CodigoAnbid);
            campos.Add(carteira.Query.RebateImpactaPL);
            campos.Add(carteira.Query.ValorMinimoSaldo);
            campos.Add(carteira.Query.PossuiResgateAutomatico);

            if (!carteira.LoadByPrimaryKey(campos, idCarteira))
                throw new Exception("Carteira não encontrada para o Cliente ID - " + idCarteira);
            #endregion

            #region Busca DataImplantacao, TipoControle, IdTipo, ZeraCaixa e DescontaTributoPL em Cliente
            Cliente cliente = new Cliente();
            campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataImplantacao);
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.ZeraCaixa);
            campos.Add(cliente.Query.DescontaTributoPL);
            cliente.LoadByPrimaryKey(campos, idCarteira);
            #endregion

            bool simulaYMFCot = ParametrosConfiguracaoSistema.Fundo.SimulaYMFCOT;

            int tipoCliente = cliente.IdTipo.Value;
            byte tipoControle = cliente.TipoControle.Value;
            ControllerContaCorrente controllerContaCorrente = new ControllerContaCorrente();

            if (tipoControle == (byte)TipoControleCliente.Carteira || carteira.TipoCalculoRetorno.Value == (byte)CalculoRetornoCarteira.TIR)
            {
                if (!processaMae && parametrosProcessamento.ProcessaFundo)
                {
                    this.ReprocessaPosicaoFundo(idCarteira, data, TipoReprocessamento.CalculoDiario, data);
                }
            }
            else if (tipoControle == (byte)TipoControleCliente.Completo ||
                     tipoControle == (byte)TipoControleCliente.CarteiraRentabil)
            {
                if (!processaMae && parametrosProcessamento.ProcessaFundo)
                {
                    this.ReprocessaPosicaoFundo(idCarteira, data, TipoReprocessamento.CalculoDiario, data);
                }

                this.ReprocessaPosicao(idCarteira, data, TipoReprocessamento.CalculoDiario, processaMae, parametrosProcessamento.CravaCota);
            }
            else if (tipoControle == (byte)TipoControleCliente.Cotista)
            {
                //Fazer limpeza de lançamentos de amortização na liquidação
                controllerContaCorrente.DeletaLiquidacao(idCarteira, data, true);

                this.ReprocessaPosicaoCotista(idCarteira, data, TipoReprocessamento.CalculoDiario);
                this.ReprocessaAdmPfee(idCarteira, data, TipoReprocessamento.CalculoDiario);
            }
            else if (tipoControle == (byte)TipoControleCliente.ApenasCotacao)
            {
                this.ReprocessaAdmPfee(idCarteira, data, TipoReprocessamento.CalculoDiario);
            }

            #region Restaura backup pré colagem
            if (processamentoValoresColados)
            {
                //Limpa a Liquidacao de lctos feitos pelo controllerFundo
                List<int> origens = new List<int>();
                origens.Add((int)OrigemLancamentoLiquidacao.Fundo.ComeCotas);
                origens.Add((int)OrigemLancamentoLiquidacao.Fundo.IOFResgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Fundo.IRResgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Fundo.Resgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Cotista.ComeCotas);
                origens.Add((int)OrigemLancamentoLiquidacao.Cotista.IOFResgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Cotista.IRResgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Cotista.Resgate);

                //Fontes
                List<int> fontes = new List<int>();
                fontes.Add((int)FonteLancamentoLiquidacao.Interno);

                LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                liquidacaoCollection.DeletaLiquidacao(idCarteira, origens, fontes, data);
            }
            else
            {
                this.RestauraBackupCotista(idCarteira, data);
                this.RestauraBackupFundo(idCarteira, data);
            }
            #endregion

            #region Restaura backup pré colagem
            if (processamentoValoresColados)
            {
                //Limpa a Liquidacao de lctos feitos pelo controllerFundo
                List<int> origens = new List<int>();
                origens.Add((int)OrigemLancamentoLiquidacao.Fundo.ComeCotas);
                origens.Add((int)OrigemLancamentoLiquidacao.Fundo.IOFResgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Fundo.IRResgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Fundo.Resgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Cotista.ComeCotas);
                origens.Add((int)OrigemLancamentoLiquidacao.Cotista.IOFResgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Cotista.IRResgate);
                origens.Add((int)OrigemLancamentoLiquidacao.Cotista.Resgate);

                //Fontes
                List<int> fontes = new List<int>();
                fontes.Add((int)FonteLancamentoLiquidacao.Interno);

                LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                liquidacaoCollection.DeletaLiquidacao(idCarteira, origens, fontes, data);
            }
            else
            {
                this.RestauraBackupCotista(idCarteira, data);
                this.RestauraBackupFundo(idCarteira, data);
            }
            #endregion


            //Apaga ComeCotas gerado anteriormente
            this.ExcluiComeCotasAnterior(idCarteira, data);

            OperacaoCotista operacaoCotista = new OperacaoCotista();
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            TransferenciaCota transferenciaCota = new TransferenciaCota();
            BloqueioCota bloqueioCota = new BloqueioCota();
            OperacaoFundo operacaoFundo = new OperacaoFundo();
            PosicaoFundo posicaoFundo = new PosicaoFundo();
            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
            CalculoPerformance calculoPerformance = new CalculoPerformance();
            CalculoProvisao calculoProvisao = new CalculoProvisao();
            CalculoTaxaFiscalizacaoCVM calculoTaxaFiscalizacaoCVM = new CalculoTaxaFiscalizacaoCVM();
            HistoricoCota historicoCota = new HistoricoCota();
            AgendaFundo agendaFundo = new AgendaFundo();
            PrejuizoCotista prejuizoCotista = new PrejuizoCotista();
            TransferenciaSerieCollection transferenciaSerieColl = new TransferenciaSerieCollection();
            TransferenciaEntreCotistaCollection transferenciaEntreCotistaColl = new TransferenciaEntreCotistaCollection();
            MediaMovel mediaMovel = new MediaMovel();
            ProventoBolsaCliente proventoBolsaCliente = new ProventoBolsaCliente();
            EventoRollUpCollection eventoRollIUpColl = new EventoRollUpCollection();
            RentabilidadeSerieCollection rentabilidadeSerieColl = new RentabilidadeSerieCollection();
            RentabilidadeFundoOffShoreCollection rentabilidadeFundoOffShoreColl = new RentabilidadeFundoOffShoreCollection();
            PortfolioPadraoCollection portfolioPadraoColl = new PortfolioPadraoCollection();

            if (!processaMae)
            {
                if (parametrosProcessamento.ProcessaFundo)
                {
                    OrdemFundo ordemFundo = new OrdemFundo();
                    ordemFundo.CarregaOrdemFundo(idCarteira, data);

                    if (tipoControle == (byte)TipoControleCliente.Completo ||
                        tipoControle == (byte)TipoControleCliente.Carteira ||
                        tipoControle == (byte)TipoControleCliente.Cotista ||
                        tipoControle == (byte)TipoControleCliente.CarteiraRentabil)
                    {
                        if (data == cliente.DataImplantacao.Value)
                        {
                            if ((tipoCliente == (int)TipoClienteFixo.Fundo || tipoCliente == (int)TipoClienteFixo.FDIC) &&
                                (carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.CurtoPrazo || carteira.TipoTributacao.Value == (byte)TipoTributacaoFundo.LongoPrazo))
                            {
                                //posicaoCotista.ProcessaAtualizacaoCotasPorComeCotas(idCarteira, data);
                            }
                            else if (tipoCliente != (int)TipoClienteFixo.Clube)
                            {
                                posicaoFundo.ProcessaAtualizacaoCotasAntesCortes(idCarteira, data);
                            }
                        }
                    }

                    if (parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.Fundo.FinancialFull))
                    {
                        posicaoFundo.CarregaPosicoesCotista(idCarteira, data);
                        operacaoFundo.CarregaOperacaoCotista(idCarteira, data);

                        //Eventos de Amortização e Juros (FIDC)
                        operacaoFundo.ProcessaEventos(idCarteira, data);

                        if (tipoControle != (byte)TipoControleCliente.ApenasCotacao)
                        {
                            operacaoFundo.LancaCCAplicacao(idCarteira, data);
                            operacaoFundo.LancaCCResgate(idCarteira, data);
                        }
                    }
                    else
                    {
                    #region Trata processamento de operacaoFundo e posicaoFundo
                        if (tipoControle == (byte)TipoControleCliente.Completo ||
                            tipoControle == (byte)TipoControleCliente.Carteira ||
                            tipoControle == (byte)TipoControleCliente.CarteiraRentabil)
                        {
                            if (!parametrosProcessamento.IgnoraOperacoes)
                            {
                                //Eventos de Amortização e Juros (FIDC)
                                operacaoFundo.ProcessaEventos(idCarteira, data);

                                if (!carteira.es.HasData || carteira.PrioridadeOperacao.Value == (byte)PrioridadeOperacaoFundo.ResgateDepois)
                                {
                                    //Processamentos de Posicao e OperacaoFundo a partir de operações lançadas
                                    operacaoFundo.ProcessaAplicacao(idCarteira, data);
                                }

                                if (tipoCliente != (int)TipoClienteFixo.Clube && tipoCliente != (int)TipoClienteFixo.Fundo && tipoCliente != (int)TipoClienteFixo.FDIC)
                                {
                                    posicaoFundo.ProcessaComeCotas(idCarteira, data, PrioridadeOperacaoFundo.ResgateDepois);

                                    /* calculo a projeção antes de converter os resgates, 
                                     * para ser possivel verificar se o saldo restante  
                                     * é maior que o minimo 
                                    */
                                    posicaoFundo.AtualizaValoresPosicao(idCarteira, data);
                                    posicaoFundo.AtualizaProjecaoTributo(idCarteira, data);
                                }

                                operacaoFundo.ProcessaResgate(idCarteira, data);


                                if (carteira.es.HasData && carteira.PrioridadeOperacao.Value == (byte)PrioridadeOperacaoFundo.ResgateAntes)
                                {
                                    //Processamentos de Posicao e OperacaoFundo a partir de operações lançadas
                                    operacaoFundo.ProcessaAplicacao(idCarteira, data);
                                }

                                //Processamentos de Posicao e OperacaoFundo a partir de operações carregadas da YMF 
                                // Não calcula nada, apenas sensibilza a qtde nos resgates e cria novas posições em PosicaoFundo (com o IdPosicao da YMF)
                                operacaoFundo.ProcessaAplicacaoSemCalculo(idCarteira, data);
                                operacaoFundo.ProcessaResgateSemCalculo(idCarteira, data);
                                (new OperacaoCotista()).AlteraTipoResgate(idCarteira, data, TipoProcessaResgate.OperacaoFundo);

                                if (tipoControle != (byte)TipoControleCliente.ApenasCotacao)
                                {
                                    operacaoFundo.LancaCCAplicacao(idCarteira, data);
                                    operacaoFundo.LancaCCResgate(idCarteira, data);
                                }

                                operacaoFundo.AjustaPosicao(idCarteira, data);
                            }

                            posicaoFundo.AtualizaValoresPosicao(idCarteira, data);

                            if (tipoCliente != (int)TipoClienteFixo.Clube && tipoCliente != (int)TipoClienteFixo.Fundo && tipoCliente != (int)TipoClienteFixo.FDIC)
                            {
                                if (!parametrosProcessamento.IgnoraOperacoes)
                                {
                                    posicaoFundo.ProcessaComeCotas(idCarteira, data, PrioridadeOperacaoFundo.ResgateAntes);
                                }

                                posicaoFundo.AtualizaProjecaoTributo(idCarteira, data);
                            }

                            agendaFundo.ProcessaEventoComeCotas(idCarteira, data);

                            //Atualiza a PosicaoFundo com valorIR e valorIOF a partir do que foi lido do arquivo de posição da SMA.
                            posicaoFundo.AtualizaTributosPosicaoSMA(idCarteira, data);
                        }
                    }
                    #endregion
                }

                if (!parametrosProcessamento.IgnoraOperacoes && tipoControle != (byte)TipoControleCliente.Carteira ) //&& carteira.TipoCalculoRetorno.Value != (byte)CalculoRetornoCarteira.TIR)
                {
                    if (tipoControle == (byte)TipoControleCliente.Completo ||
                        tipoControle == (byte)TipoControleCliente.CarteiraRentabil)
                    {
                        //Limpa a Liquidacao de lctos feitos pelo controllerFundo
                        List<int> origens = new List<int>();
                        origens.Add((int)OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao);
                        origens.Add((int)OrigemLancamentoLiquidacao.Provisao.TaxaCustodia);
                        origens.Add((int)OrigemLancamentoLiquidacao.Provisao.TaxaGestao);
                        origens.Add((int)OrigemLancamentoLiquidacao.Provisao.TaxaPerformance);
                        origens.Add((int)OrigemLancamentoLiquidacao.Provisao.TaxaFiscalizacaoCVM);
                        origens.Add((int)OrigemLancamentoLiquidacao.Provisao.ProvisaoOutros);

                        List<int> fontes = new List<int>();
                        fontes.Add((int)FonteLancamentoLiquidacao.Interno);

                        LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                        liquidacaoCollection.DeletaLiquidacao(idCarteira, origens, fontes);
                        //
                    }
                }

                if ((tipoControle == (byte)TipoControleCliente.Completo || tipoControle == (byte)TipoControleCliente.CarteiraRentabil)
                    && carteira.TipoCalculoRetorno.Value != (byte)CalculoRetornoCarteira.TIR)
                {
                    if (tipoCliente == (int)TipoClienteFixo.Clube || tipoCliente == (int)TipoClienteFixo.Fundo || tipoCliente == (int)TipoClienteFixo.FDIC)
                    {
                        //Distribuição de dividendos em forma de aplicação de cotistas ou geração de CPR
                        operacaoCotista.DistribuiDividendos(idCarteira, data);
                        proventoBolsaCliente.DistribuiDividendos(idCarteira, data);
                    }
                }
            }

            if (tipoControle != (byte)TipoControleCliente.Carteira && carteira.TipoCalculoRetorno.Value != (byte)CalculoRetornoCarteira.TIR)
            {
                if (!processaMae)
                {
                    if (tipoControle == (byte)TipoControleCliente.Cotista && simulaYMFCot)
                    {
                        //Usado exclusivamente para integração entre SAC e COT
                        historicoCota.AjustaCotaLiquidaYMF(idCarteira, data);
                    }
                }

                if (tipoControle == (byte)TipoControleCliente.Completo ||
                    tipoControle == (byte)TipoControleCliente.CarteiraRentabil)
                {
                    #region Calcula Provisões, Taxa CVM e a Cota Bruta de Fechamento
                    if (!processaMae)
                    {
                        //Estes cálculos só se aplicam a carteiras com cota de fechamento
                        //Calcula o caixa de fechamento do dia (para a cota bruta)
                        controllerContaCorrente.ExecutaFechamento(idCarteira, data, parametrosProcessamento);

                        #region Trata Taxa Fiscalização CVM
                        calculoTaxaFiscalizacaoCVM.CalculaTaxaFiscalizacaoCVM(idCarteira, data);
                        #endregion

                        #region Trata Provisões renováveis
                        calculoProvisao.TrataVencimentoProvisao(idCarteira, data);
                        calculoProvisao.CalculaProvisao(idCarteira, data);
                        #endregion
                    }
                    else
                    {
                        //Ajusta o caixa da mãe com eventuais pagamentos no dia de taxa de administração e performance da própria mãe
                        this.AjustaCaixaMae(idCarteira, data);
                    }

                    if (!parametrosProcessamento.CravaCota) //TRATAMENTO DE COTA CRAVADA
                    {
                        //Cota bruta embute todos os valores a receber/pagar, menos a performance calculada no dia
                        ProcessaCotaBrutaFechamento(idCarteira, data, processaMae); 
                    }
                    #endregion
                }
                
            }
            //c�lculo da taxa de administra��o 
            if (tipoControle != (byte)TipoControleCliente.Carteira)
            {
                if (tipoControle == (byte)TipoControleCliente.Completo ||
                    tipoControle == (byte)TipoControleCliente.CarteiraRentabil ||
                    tipoControle == (byte)TipoControleCliente.Cotista ||
                    tipoControle == (byte)TipoControleCliente.ApenasCotacao)
                {
                    #region Calcula Taxa Adm/Gestão e Taxa Pfee
                    #region Trata Taxa Adm, Gestão
                    bool liquidaCC = tipoControle != (byte)TipoControleCliente.ApenasCotacao;
                    calculoAdministracao.TrataVencimentoTaxaAdministracao(idCarteira, data, liquidaCC);
                    calculoAdministracao.CalculaTaxaAdministracao(idCarteira, data, liquidaCC);
                    #endregion

                    portfolioPadraoColl.ClonaLiquidacao(idCarteira, data);

                    if (!simulaYMFCot && (tipoControle == (byte)TipoControleCliente.Completo || tipoControle == (byte)TipoControleCliente.CarteiraRentabil))
                    {
                        if (!parametrosProcessamento.CravaCota) //TRATAMENTO DE COTA CRAVADA
                        {
                            //Na simulação do YMF COT a cota bruta é calculada pelo SAC e importada, portanto, nao precisamos recalcula-la
                            ProcessaRecalculoCotaBruta(idCarteira, data, cliente.DescontaTributoPL.Value);
                        }
                    }

                    if (tipoControle == (byte)TipoControleCliente.Completo ||
                        tipoControle == (byte)TipoControleCliente.CarteiraRentabil ||
                        tipoControle == (byte)TipoControleCliente.Cotista)
                    {
                        #region Trata Taxa Performance
                        calculoPerformance.CalculaTaxaPerformance(idCarteira, data);
                        calculoPerformance.TrataVencimentoPerformance(idCarteira, data);                        
                        #endregion
                    }
                    #endregion                                      
                }

            }
            //pfee
            if (tipoControle != (byte)TipoControleCliente.Carteira && carteira.TipoCalculoRetorno.Value != (byte)CalculoRetornoCarteira.TIR)
            {
                if (tipoControle == (byte)TipoControleCliente.Completo ||
                    tipoControle == (byte)TipoControleCliente.CarteiraRentabil ||
                    tipoControle == (byte)TipoControleCliente.Cotista)
                {
                    TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
                    tabelaTaxaPerformanceCollection.BuscaTabelaTaxaPerformance(idCarteira, data);

                    if (tabelaTaxaPerformanceCollection.Count > 0)
                    {
                        if (tabelaTaxaPerformanceCollection[0].ImpactaPL == "S" || tipoControle == (byte)TipoControleCliente.Cotista)
                        {
                            #region Trata Taxa Performance
                            calculoPerformance.CalculaTaxaPerformance(idCarteira, data);
                            calculoPerformance.TrataVencimentoPerformance(idCarteira, data);
                            #endregion
                        }
                    }
                }
            }           


            if (tipoControle == (byte)TipoControleCliente.Completo ||
                tipoControle == (byte)TipoControleCliente.CarteiraRentabil ||
                tipoControle == (byte)TipoControleCliente.Carteira)
            {
                //Calcula o caixa de fechamento do dia (para a cota líquida)
                ParametrosProcessamento param = new ParametrosProcessamento();
                param.IntegraCC = false; //Não precisa integrar c/c de novo pois já foi feito no cálculo da cota bruta!
                param.ListaTabelaInterfaceCliente = parametrosProcessamento.ListaTabelaInterfaceCliente;
                controllerContaCorrente.ExecutaFechamento(idCarteira, data, param);
            }

            if (tipoControle != (byte)TipoControleCliente.Carteira && carteira.TipoCalculoRetorno.Value == (byte)CalculoRetornoCarteira.TIR)
            {
                ProcessaPLFechamentoPorTIR(idCarteira, data, cliente.ZeraCaixa);

                this.CalculaCotaPorTIR(idCarteira, data, false);
            }
            else if (tipoControle != (byte)TipoControleCliente.Carteira)
            {
                if (tipoControle == (byte)TipoControleCliente.Completo ||
                    tipoControle == (byte)TipoControleCliente.CarteiraRentabil)
                {
                    if (!parametrosProcessamento.CravaCota) //TRATAMENTO DE COTA CRAVADA
                    {
                        ProcessaCotaLiquidaFechamento(idCarteira, data);
                    }

                    #region Calcula a taxa de performance se ImpactaPL = NAO
                    TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
                    tabelaTaxaPerformanceCollection.BuscaTabelaTaxaPerformance(idCarteira, data);

                    if (tabelaTaxaPerformanceCollection.Count > 0)
                    {
                        if (tabelaTaxaPerformanceCollection[0].ImpactaPL == "N")
                        {
                            #region Trata Taxa Performance
                            calculoPerformance.CalculaTaxaPerformance(idCarteira, data);
                            calculoPerformance.TrataVencimentoPerformance(idCarteira, data);
                            #endregion
                        }
                    }
                    #endregion

                    #region LancaCotasImplantacao, ZeraCaixaAutomatico e GeraResgateTributos
                    if (tipoCliente != (int)TipoClienteFixo.Clube && tipoCliente != (int)TipoClienteFixo.Fundo && tipoCliente != (int)TipoClienteFixo.FDIC)
                    {
                        //Cria o cotista relacionado a carteira, caso este não exista
                        int idCotistaAux = 1; 
                        Cotista cotista = new Cotista();  
                        cotista.Query.Select(cotista.Query.IdCotista.Max());
                        if (cotista.Query.Load())
                            idCotistaAux = cotista.IdCotista.GetValueOrDefault(0) + idCotistaAux;
                        cotista.CriaCotista(idCotistaAux, idCarteira);

                        if (data > cliente.DataImplantacao.Value && cliente.DescontaTributoPL == (byte)DescontoPLCliente.BrutoSemImpacto)
                        {
                            //Esse método serve para gerar resgates de cotista especiais para compensar o pagto de IR e IOF em carteiras administradas
                            historicoCota.GeraResgateTributos(idCarteira, data);
                        }

                        if (cliente.ZeraCaixa == "S" && !processaMae)
                        {
                            //Lança automaticamente aplicação ou resgate para zerar caixa
                            historicoCota.ZeraCaixaAutomatico(idCarteira, data);
                        }

                        if (data == cliente.DataImplantacao.Value)
                        {
                            //Lança automaticamente aplicação especial em cotas para carteiras administradas no 1o dia de implantação
                            historicoCota.LancaCotasImplantacao(idCarteira, data);
                        }
                    }
                    #endregion
                }
                //
                
                historicoCota.ProcessaIngressoRetirada(idCarteira, data);
                eventoRollIUpColl.GeraTransferenciaRollUp(idCarteira, data);
                transferenciaSerieColl.ProcessaTransferenciaSerie(idCarteira, data);
                transferenciaEntreCotistaColl.ProcessaTransferenciaEntreCotista(idCarteira, data);

                if (tipoControle == (byte)TipoControleCliente.Completo || tipoControle == (byte)TipoControleCliente.Cotista)
                {
                    #region Trata processamento de operacaoCotista, posicaoCotista, transferenciaCotista, bloqueioCota e eventos de amort/juros
                    if (!parametrosProcessamento.IgnoraOperacoes)
                    {
                        OrdemCotista ordemCotista = new OrdemCotista();
                        ordemCotista.CarregaOrdemCotista(idCarteira, data);

                        if (tipoCliente == (int)TipoClienteFixo.Clube || tipoCliente == (int)TipoClienteFixo.Fundo || tipoCliente == (int)TipoClienteFixo.FDIC)
                        {
                            //Trata bloqueio/desbloqueio de cotas
                            bloqueioCota.ProcessaBloqueioCota(idCarteira, data);
                            bloqueioCota.ProcessaDesbloqueioAutomatico(idCarteira, data);
                        }

                        //Eventos de Amortização e Juros (FIDC)
                        operacaoCotista.ProcessaEventos(idCarteira, data);

                        if (tipoControle == (byte)TipoControleCliente.Completo)
                        {
                            if (!parametrosProcessamento.CravaCota)
                            {
                                //Calcula novamente o PL para pegar os valores de resgate/aplic cotizados!
                                //Usado tb para cálculo do PL somente para carteira sem cota!
                                this.ProcessaPLFechamentoDepoisCota(idCarteira, data, false);
                            }
                        }

                        if (carteira.PossuiResgateAutomatico == "S" && carteira.ValorMinimoSaldo.GetValueOrDefault(0) > 0)
                        {
                            /* calculo a projeção antes de converter os resgates, 
                            * para ser possivel verificar se o saldo restante  
                            * é maior que o minimo 
                            */
                            posicaoCotista.AtualizaValoresPosicao(idCarteira, data);
                            posicaoCotista.AtualizaProjecaoTributo(idCarteira, data);

                            (new OperacaoCotista()).AlteraTipoResgate(idCarteira, data, TipoProcessaResgate.OperacaoCotista);
                        }


                        if (carteira.PrioridadeOperacao.Value == (byte)PrioridadeOperacaoFundo.ResgateAntes)
                        {
                            operacaoCotista.ProcessaResgate(idCarteira, data, false);
                            operacaoCotista.ProcessaAplicacao(idCarteira, data);
                        }
                        else
                        {
                            posicaoCotista.ProcessaComeCotas(idCarteira, data);
                            operacaoCotista.ProcessaAplicacao(idCarteira, data);
                            operacaoCotista.ProcessaResgate(idCarteira, data, false);
                        }
                    }

                    if (tipoCliente == (int)TipoClienteFixo.Clube || tipoCliente == (int)TipoClienteFixo.Fundo || tipoCliente == (int)TipoClienteFixo.FDIC)
                    {
                        posicaoCotista.AtualizaProjecaoTributo(idCarteira, data);

                        if (!parametrosProcessamento.IgnoraOperacoes)
                        {
                            if (carteira.PrioridadeOperacao.Value == (byte)PrioridadeOperacaoFundo.ResgateAntes)
                            {

                                posicaoCotista.ProcessaComeCotas(idCarteira, data);
                            }
                            transferenciaCota.ProcessaTransferencia(idCarteira, data);
                        }
                    }

                    if (!parametrosProcessamento.IgnoraOperacoes)
                    {
                        operacaoCotista.LancaCCAplicacao(idCarteira, data);
                        operacaoCotista.LancaCCResgate(idCarteira, data);
                    }
                    #endregion
                }
                //

                if (!processaMae)
                {
                    if (tipoControle == (byte)TipoControleCliente.Completo)
                    {
                        //Deduz da pfee projetada, todo o valor de pfee resgatado na data
                        calculoPerformance = new CalculoPerformance();
                        calculoPerformance.DeduzPerformanceResgatada(idCarteira, data);
                        //
                    }
                }

                if (tipoControle == (byte)TipoControleCliente.Completo ||
                    tipoControle == (byte)TipoControleCliente.Cotista)
                {
                    //Atualiza o total de cotas (a partir de PosicaoCotista) direto na cota
                    historicoCota.CalculaQuantidadeCotas(idCarteira, data);
                }

                if (tipoControle == (byte)TipoControleCliente.Completo ||
                    tipoControle == (byte)TipoControleCliente.CarteiraRentabil)
                {
                    if (parametrosProcessamento.CravaCota) //TRATAMENTO DE COTA CRAVADA
                    {
                        this.ProcessaAjusteCravaCota(idCarteira, data);
                    }
                    else
                    {
                        //Calcula novamente o PL para pegar os valores de resgate/aplic cotizados!
                        //Usado tb para cálculo do PL somente para carteira sem cota!
                        this.ProcessaPLFechamentoDepoisCota(idCarteira, data, false);
                    }
                }

                if (tipoControle == (byte)TipoControleCliente.Cotista && carteira.CodigoAnbid == "") //Só calcula para fundos sem cota importada, para os importados acata o PL importado
                {
                    historicoCota.CalculaPLComCotaInformada(idCarteira, data, tipoControle);
                }

                if (tipoControle == (byte)TipoControleCliente.Completo || tipoControle == (byte)TipoControleCliente.Cotista)
                {
                    //Atualiza valor de cota e projeta IR/IOF a pagar
                    posicaoCotista.AtualizaValoresPosicao(idCarteira, data);

                    if (tipoCliente == (int)TipoClienteFixo.Clube || tipoCliente == (int)TipoClienteFixo.Fundo || tipoCliente == (int)TipoClienteFixo.FDIC)
                    {
                        posicaoCotista.AtualizaProjecaoTributo(idCarteira, data);
                        prejuizoCotista.AtualizaValorPrejuizo(idCarteira, data);
                    }
                }

                //Processa Incorporação para PosicaoFundo e PosicaoCotista
                if (processaEventos)
                {
                    historicoCota.ProcessaIncorporacao(idCarteira, data);
                    historicoCota.ProcessaIncorporacaoFusaoCisao(idCarteira, data);
                }
                //Processa come-cotas de eventos de fundos
                this.ProcessaComeCotasEvento(idCarteira, data);

                //Executa come-cotas
                posicaoCotista.ProcessaComeCotasEvento(idCarteira, data);

                //Verifica se deve atualizar PL e Saldo devido a processamento de evento de fundos e come cotas
                if (tipoControle == (byte)TipoControleCliente.CarteiraRentabil || tipoControle == (byte)TipoControleCliente.Completo)
                    this.AtualizaPLeSaldo(idCarteira, data);

                if (tipoControle == (byte)TipoControleCliente.Completo)
                {
                    if (carteira.CalculaEnquadra == "S")
                    {
                        ControllerEnquadra controllerEnquadra = new ControllerEnquadra();
                        controllerEnquadra.ExecutaFechamento(idCarteira, data);
                    }
                }

                if (tipoControle == (byte)TipoControleCliente.Completo ||
                    tipoControle == (byte)TipoControleCliente.CarteiraRentabil ||
                    tipoControle == (byte)TipoControleCliente.Cotista ||
                    tipoControle == (byte)TipoControleCliente.ApenasCotacao)
                {
                    //Controle de rebates (visão gestor e/ou distribuidor)
                    if (carteira.TipoVisaoFundo.Value != (byte)TipoVisaoFundoRebate.NaoTrata)
                    {
                        CalculoRebate calculoRebate = new CalculoRebate();
                        calculoRebate.TrataRebateDiario(idCarteira, data, carteira.TipoVisaoFundo.Value);
                    }

                }


                if (tipoCliente == (int)TipoClienteFixo.Clube || tipoCliente == (int)TipoClienteFixo.Fundo || tipoCliente == (int)TipoClienteFixo.FDIC)
                {
                    FundoInvestimentoFormaCondominioCollection fundoInvestimentoFormaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();
                    fundoInvestimentoFormaCondominioCollection.Query.Where(fundoInvestimentoFormaCondominioCollection.Query.DataInicioVigencia.Equal(data),
                                                                           fundoInvestimentoFormaCondominioCollection.Query.IdCarteira.Equal(idCarteira));

                    if (fundoInvestimentoFormaCondominioCollection.Query.Load())
                        posicaoCotista.AtualizaProjecaoTributo(idCarteira, data);
                }

                if (tipoControle == (byte)TipoControleCliente.Cotista && simulaYMFCot)
                {
                    TX2 tx2 = new TX2(data);
                    tx2.ExportaTabela(idCarteira);

                    TX3 tx3 = new TX3(data);
                    tx3.ExportaTabela(idCarteira);
                }

                #region Colagem de Passivo
                if (!processamentoValoresColados)
                {
                    if (existeColagem)
                    {
                        #region Cria backup
                        this.CriaBackupFundo(idCarteira, data);
                        this.CriaBackupCotista(idCarteira, data);
                        #endregion

                        #region Aplica a Colagem dos valores
                        this.ColaValoresFundo(idCarteira, data);
                        this.ColaValoresCotista(idCarteira, data);
                        #endregion

                        this.ExecutaFechamento(idCarteira, data, parametrosProcessamento, processaMae, true);

                    }
                }
                #endregion
            }
            //

            #region MediaMovel 
            mediaMovel.CalculaMediaMovel(idCarteira, data);
            #endregion

            this.CalculaPendenciaLiquidacao(idCarteira, data);

            bool processaIndicadorCarteira = ParametrosConfiguracaoSistema.Outras.ProcessaIndicadorCarteira;
            carteira.ProcessaIndicadores(idCarteira, data, processaIndicadorCarteira);            

            #region Calcula Prazo Médio
            this.CalculaPrazoMedioFundo(data, idCarteira);
            #endregion

            #region Rentabilidade
            this.CalculaRentabilidadeFundo(data, idCarteira);

            //rentabilidadeSerieColl.CalculaRentabilidadeSerieOffShore(data, idCarteira);
            //rentabilidadeFundoOffShoreColl.CalculaRentabilidadeFundoOffShore(data, idCarteira);
            #endregion

            #region Cálculo de Resultado
            CalculoResultado calculoResultado = new CalculoResultado();
            calculoResultado.CalculaResultado(idCarteira, data);
            #endregion

            #region Colagem de Passivo
            if (!processamentoValoresColados)
            {
                if (existeColagem)
                {
                    #region Cria backup
                    this.CriaBackupFundo(idCarteira, data);
                    this.CriaBackupCotista(idCarteira, data);
                    #endregion

                    #region Aplica a Colagem dos valores
                    this.ColaValoresFundo(idCarteira, data);
                    this.ColaValoresCotista(idCarteira, data);
                    #endregion

                    this.ExecutaFechamento(idCarteira, data, parametrosProcessamento, processaMae, true);

                }
            }
            #endregion

            if (cliente.IdTipo.Value == TipoClienteFixo.Fundo)
                this.ValidaPLxQtde(idCarteira, data);

        }

        /// <summary>
        /// Se ocorreu evento de fundo (Troca de condomínio, troca de tipo de tributação, cisão/incorporação/fusão) atualiza saldo caixa e valor da cota
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void AtualizaPLeSaldo(int idCarteira, DateTime data)
        {

            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.IdCarteira.Equal(idCarteira),
                                                  agendaComeCotasCollection.Query.DataVencimento.Equal(data));
            agendaComeCotasCollection.Query.Load();
            if (agendaComeCotasCollection.Count > 0)
            {
                //Ajusta Saldo Caixa
                SaldoCaixa saldoCaixa = new SaldoCaixa();
                saldoCaixa.CalculaSaldoCaixaFechamento(idCarteira, data);

                //Ajusta PL.
                this.ProcessaCotaBrutaFechamento(idCarteira, data, false);

                //Exclui lancamento de ajuste de crava cotas
                LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCarteira),
                                                 liquidacaoCollection.Query.DataLancamento.Equal(data),
                                                 liquidacaoCollection.Query.Situacao ==(int)SituacaoLancamentoLiquidacao.Compensacao,
                                                 liquidacaoCollection.Query.Origem == (int)OrigemLancamentoLiquidacao.AjusteCompensacaoCota,
                                                 liquidacaoCollection.Query.Fonte == (int)FonteLancamentoLiquidacao.Interno);
                liquidacaoCollection.Query.Load();
                liquidacaoCollection.MarkAllAsDeleted();
                liquidacaoCollection.Save();

                this.ProcessaPLFechamentoDepoisCota(idCarteira, data, false);
            }
        }

        private void ExcluiComeCotasAnterior(int idCarteira, DateTime data)
        {
            #region exclui agendamento anterior
            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.DataLancamento.Equal(data),
                                                  agendaComeCotasCollection.Query.IdCarteira.Equal(idCarteira));
            agendaComeCotasCollection.Query.Load();
            agendaComeCotasCollection.MarkAllAsDeleted();
            agendaComeCotasCollection.Save();
            #endregion

            #region Deleta todas as operações de ComeCotas do dia na OperacaoCotista
            List<int> ListaEventos = new List<int>();
            ListaEventos.Add((int)FonteOperacaoCotista.CisaoIncorporacaoFusao);
            ListaEventos.Add((int)FonteOperacaoCotista.MudancaClassificacao);
            ListaEventos.Add((int)FonteOperacaoCotista.MudancaCondominio);

            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.DataOperacao.Equal(data),
                                                  operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                  operacaoCotistaCollection.Query.TipoOperacao.Equal((int)TipoOperacaoCotista.ComeCotas),
                                                  operacaoCotistaCollection.Query.Fonte.In(ListaEventos));
            if (operacaoCotistaCollection.Query.Load())
            {
                operacaoCotistaCollection.MarkAllAsDeleted();
                operacaoCotistaCollection.Save();
            }
            #endregion
        }

        /// <summary>
        /// Efetua come-cotas de evento de fundos
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void ProcessaComeCotasEvento(int idCarteira, DateTime data)
        {

            PosicaoCotista posicaoCotista = new PosicaoCotista();

            //Calcula come-cotas para Cisão/Incorporação/Fusão
            EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
            eventoFundoCollection.Query.Where(eventoFundoCollection.Query.DataPosicao.Equal(data),
                                              eventoFundoCollection.Query.IdCarteiraOrigem.Equal(idCarteira),
                                              eventoFundoCollection.Query.Status.Equal("Ativo"));
            eventoFundoCollection.Query.Load();

            if (eventoFundoCollection.Count > 0)
            {
                posicaoCotista.CalculaImpostoEvento(idCarteira,
                                                    data,
                                                    FonteOperacaoCotista.CisaoIncorporacaoFusao,
                                                    eventoFundoCollection[0].IdEventoFundo.Value);
            }

            //Calcula come-cotas para alteração de condomínio
            FundoInvestimentoFormaCondominioCollection fundoInvestimentoFormaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();
            fundoInvestimentoFormaCondominioCollection.Query.Where(fundoInvestimentoFormaCondominioCollection.Query.DataInicioVigencia.Equal(data),
                                                                   fundoInvestimentoFormaCondominioCollection.Query.IdCarteira.Equal(idCarteira));
            fundoInvestimentoFormaCondominioCollection.Query.Load();

            if (fundoInvestimentoFormaCondominioCollection.Count > 0)
            {
                posicaoCotista.CalculaImpostoEvento(idCarteira,
                                                    data,
                                                    FonteOperacaoCotista.MudancaCondominio,
                                                    fundoInvestimentoFormaCondominioCollection[0].IdFormaCondominio.Value);
            }

            //Calcula come-cotas para alteração de classificação tributária
            DesenquadramentoTributarioCollection desenquadramentoTributarioCollection = new DesenquadramentoTributarioCollection();
            desenquadramentoTributarioCollection.Query.Where(desenquadramentoTributarioCollection.Query.Data.Equal(data),
                                                             desenquadramentoTributarioCollection.Query.IdCarteira.Equal(idCarteira));
            desenquadramentoTributarioCollection.Query.Load();

            if (desenquadramentoTributarioCollection.Count > 0)
            {
                posicaoCotista.CalculaImpostoEvento(idCarteira,
                                                    data,
                                                    FonteOperacaoCotista.MudancaClassificacao,
                                                    desenquadramentoTributarioCollection[0].IdDesenquadramentoTributario.Value);
            }

            #region Rentabilidade
            this.CalculaRentabilidadeFundo(data, idCarteira);
            #endregion
        }

        /// <summary>
        /// Backup de CalculoAtual (Adm, Provisao, Pfee) para CalculoHistorico. Backup de PosicaoFundo e PosicaoCotista.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacao(int idCarteira, DateTime data)
        {
            this.DeletaCalculoTaxasHistorico(idCarteira, data); //Deleta de CalculoAdministracaoHistorico e CalculoPerformanceHistorico
            this.DeletaPosicaoFundoHistorico(idCarteira, data); //Deleta PosicaoFundoHistorico e PrejuizoFundoHistorico

            CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
            calculoProvisaoHistoricoCollection.DeletaCalculoProvisaoHistoricoDataHistoricoMaiorIgual(idCarteira, data);

            CalculoProvisao calculoProvisao = new CalculoProvisao();
            calculoProvisao.GeraBackup(idCarteira, data);

            this.ExecutaDivulgacaoControleFundos(idCarteira, data);

            this.ExecutaDivulgacaoControleCotista(idCarteira, data); //Inclui fechamento de posições de cotistas e de CalculoAdministracao e CalculoPerformance
        }

        /// <summary>
        /// Backup de CalculoAdministracao, CalculoPerformance, PosicaoCotista e PrejuizoCotista.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacaoControleCotista(int idCarteira, DateTime data)
        {
            this.DeletaCalculoTaxasHistorico(idCarteira, data); //Deleta de CalculoAdministracaoHistorico e CalculoPerformanceHistorico

            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
            CalculoPerformance calculoPerformance = new CalculoPerformance();

            calculoAdministracao.GeraBackup(idCarteira, data);
            calculoPerformance.GeraBackup(idCarteira, data);

            //O gerabackup já faz a remocao do historico, portanto nao precisamos fazer a delecao a priori
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.GeraBackup(idCarteira, data);

            //O gerabackup já faz a remocao do historico, portanto nao precisamos fazer a delecao a priori
            PrejuizoCotista prejuizoCotista = new PrejuizoCotista();
            prejuizoCotista.GeraBackup(idCarteira, data);
        }

        /// <summary>
        /// Backup de PosicaoFundo e PrejuizoFundo.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacaoControleFundos(int idCarteira, DateTime data)
        {
            PosicaoFundo posicaoFundo = new PosicaoFundo();
            posicaoFundo.GeraBackup(idCarteira, data);

            PrejuizoFundo prejuizoFundo = new PrejuizoFundo();
            prejuizoFundo.GeraBackup(idCarteira, data);
        }

        /// <summary>
        /// Atualização das posições de fundos com as respectivas cotas do histórico de cotas.
        /// Cálculos de provisão, tx fiscalização.        
        /// Tratamento dos respectivos vencimentos.
        /// Gera a posição de abertura na PosicaoFundoAbertura.
        /// Só executa se a carteira for cota de abertura.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void ExecutaAberturaAntesCotaBruta(int idCarteira, DateTime data)
        {
            PosicaoFundo posicaoFundo = new PosicaoFundo();

            #region Busca se a carteira é de abertura ou fechamento
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            if (!carteira.LoadByPrimaryKey(campos, idCarteira))
                throw new CarteiraNaoCadastradaException("Carteira não cadastrada. Nr " + idCarteira);
            int tipoCota = carteira.TipoCota.Value;
            #endregion

            //Estes cálculos só se aplicam a carteiras com cota de abertura
            if (tipoCota == (int)TipoCotaFundo.Abertura)
            {
                #region Atualização da cota na PosicaoFundo
                posicaoFundo.AtualizaValoresPosicao(idCarteira, data);
                #endregion

                CalculoProvisao calculoProvisao = new CalculoProvisao();
                CalculoTaxaFiscalizacaoCVM calculoTaxaFiscalizacaoCVM = new CalculoTaxaFiscalizacaoCVM();

                #region Cálculo de provisões
                calculoProvisao.TrataVencimentoProvisao(idCarteira, data);
                calculoProvisao.CalculaProvisao(idCarteira, data);
                #endregion

                #region Taxa de fiscalização CVM
                calculoTaxaFiscalizacaoCVM.CalculaTaxaFiscalizacaoCVM(idCarteira, data);
                #endregion
            }

            //Gera posição de abertura (se aplica a carteiras com cota de abertura e fechamento)
            #region posicaoFundo.GeraPosicaoAbertura
            posicaoFundo = new PosicaoFundo();
            posicaoFundo.GeraPosicaoAbertura(idCarteira, data);
            #endregion
        }

        /// <summary>
        /// Gera a posição de abertura na PosicaoFundoAbertura.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaAberturaPosicaoFundo(int idCarteira, DateTime data)
        {
            PosicaoFundo posicaoFundo = new PosicaoFundo();

            #region posicaoFundo.GeraPosicaoAbertura
            posicaoFundo = new PosicaoFundo();
            posicaoFundo.GeraPosicaoAbertura(idCarteira, data);
            #endregion
        }

        /// <summary>
        /// Cálculos de administração, performance. 
        /// Tratamento dos respectivos vencimentos.
        /// Só executa se a carteira for cota de abertura.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void ExecutaAberturaAntesCotaLiquida(int idCarteira, DateTime data)
        {
            #region Busca se a carteira é de abertura ou fechamento
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            if (!carteira.LoadByPrimaryKey(campos, idCarteira))
                throw new CarteiraNaoCadastradaException("Carteira não cadastrada. Nr " + idCarteira);
            int tipoCota = carteira.TipoCota.Value;
            #endregion

            //Estes cálculos só se aplicam a carteiras com cota de abertura
            if (tipoCota == (int)TipoCotaFundo.Abertura)
            {
                CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
                CalculoPerformance calculoPerformance = new CalculoPerformance();

                #region Cálculo de taxas de administração
                calculoAdministracao.TrataVencimentoTaxaAdministracao(idCarteira, data, true);
                calculoAdministracao.CalculaTaxaAdministracao(idCarteira, data, true);
                #endregion

                #region Cálculo de performance
                calculoPerformance.CalculaTaxaPerformance(idCarteira, data);
                calculoPerformance.TrataVencimentoPerformance(idCarteira, data);               
                #endregion
            }
        }

        /// <summary>
        /// Gera a PosicaoCotistaAbertua, para cota de abertura calcula IR e IOF projetados.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="tipoCota"></param>
        public void ExecutaAberturaPosicaoCotista(int idCarteira, DateTime data)
        {
            PosicaoCotista posicaoCotista = new PosicaoCotista();

            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            if (!carteira.LoadByPrimaryKey(campos, idCarteira))
                throw new CarteiraNaoCadastradaException("Carteira não cadastrada. Nr " + idCarteira);
            int tipoCota = carteira.TipoCota.Value;

            if (tipoCota == (int)TipoCotaFundo.Abertura)
            {
                posicaoCotista.AtualizaValoresPosicao(idCarteira, data);

                //Cálculo de IR/IOF para cota de abertura
                posicaoCotista.AtualizaProjecaoTributo(idCarteira, data);
            }

            posicaoCotista.GeraPosicaoAbertura(idCarteira, data);
        }

        /// <summary>
        /// Método central para processamento da cota bruta (sem incorporar tx adm e pfee do dia).
        /// Leva em conta todos os ativos da carteira, os valores a pagar/receber, o caixa, para compor o PL.
        /// Leva em conta a quantidade de cotas e gera a cota final.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaCotaBrutaAbertura(int idCarteira, DateTime data, bool cravaCota)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> camposCliente = new List<esQueryItem>();
            camposCliente.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(camposCliente, idCarteira);
            int idLocal = cliente.IdLocal.Value;

            DateTime dataAnterior = new DateTime();
            if (idLocal == LocalFeriadoFixo.Brasil)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
            }
            else
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
            }

            //Cálculo de provisões, taxa fiscalização CVM, atualização de cota de fundos nas posições da PosicaoFundo
            this.ExecutaAberturaAntesCotaBruta(idCarteira, data);
            //

            #region Busca parâmetros da carteira
            Carteira carteira = new Carteira();

            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaCota);
            campos.Add(carteira.Query.CasasDecimaisCota);
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.CotaInicial);

            carteira.LoadByPrimaryKey(campos, idCarteira);

            int casasDecimaisCota = carteira.CasasDecimaisCota.Value;
            int tipoCota = carteira.TipoCota.Value;
            #endregion

            #region Busca saldo de caixa de abertura
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.BuscaSaldoCaixa(idCarteira, data);

            decimal saldoCaixaAbertura = 0;
            if (saldoCaixa.SaldoAbertura.HasValue)
                saldoCaixaAbertura = saldoCaixa.SaldoAbertura.Value;
            #endregion

            #region Busca total do valor de carteira
            HistoricoCota historicoCota = new HistoricoCota();
            decimal valorCarteira = historicoCota.CalculaCarteiraTotal(idCarteira, data, (int)ListaMoedaFixo.Real);
            #endregion

            #region Busca o valor total a liquidar (recebimentos/pagamentos projetados)
            Liquidacao liquidacao = new Liquidacao();
            decimal valorLiquidar = liquidacao.RetornaLiquidacaoVencimentoMaiorIgual(idCarteira, data, data);

            //É preciso somar aos valores internos liquidados na data (ex: Taxa Adm/Pfee/Provisões...) 
            liquidacao = new Liquidacao();
            decimal valorLiquidarNaData = liquidacao.RetornaLiquidacaoLancamentoNaData(idCarteira, data, data);
            valorLiquidar += valorLiquidarNaData;
            #endregion

            #region Busca a quantidade total de cotas da carteira/fundo
            historicoCota = new HistoricoCota();
            decimal quantidadeCotas = historicoCota.RetornaQuantidadeCotas(idCarteira, dataAnterior);
            #endregion

            decimal valorAcumuladoAdm = 0;
            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
            calculoAdministracao.Query.Select(calculoAdministracao.Query.ValorAcumulado.Sum());
            calculoAdministracao.Query.Where(calculoAdministracao.Query.IdCarteira.Equal(idCarteira));
            calculoAdministracao.Query.Load();

            if (calculoAdministracao.ValorAcumulado.HasValue)
            {
                valorAcumuladoAdm = calculoAdministracao.ValorAcumulado.Value;
            }

            decimal valorPL = valorCarteira + valorLiquidar + saldoCaixaAbertura - valorAcumuladoAdm;

            decimal cotaDia;
            //Se não existe qtde de cotas (fundo liquidado), assume a última cota de D-1
            //Se não existe cota em D-1, pega a cota inicial da carteira
            if (quantidadeCotas == 0)
            {
                HistoricoCota historicoCotaAnterior = new HistoricoCota();
                if (!historicoCotaAnterior.BuscaValorCota(idCarteira, dataAnterior))
                {
                    cotaDia = carteira.CotaInicial.Value;
                }
                else
                {
                    if (!historicoCotaAnterior.CotaAbertura.HasValue)
                    {
                        cotaDia = carteira.CotaInicial.Value;
                    }
                    else
                    {
                        cotaDia = historicoCotaAnterior.CotaAbertura.Value;
                    }
                }
            }
            else
            {
                if (carteira.IsTruncaCota())
                    cotaDia = Utilitario.Truncate(valorPL / quantidadeCotas, casasDecimaisCota);
                else
                    cotaDia = Math.Round(valorPL / quantidadeCotas, casasDecimaisCota);
            }

            if (!cravaCota)
            {
                //Salva a nova cota e PL de abertura brutos na HistoricoCota
                historicoCota = new HistoricoCota();
                historicoCota.Data = data;
                historicoCota.IdCarteira = idCarteira;
                historicoCota.CotaAbertura = 0;
                historicoCota.CotaFechamento = 0;
                historicoCota.CotaBruta = cotaDia;
                historicoCota.PLAbertura = 0;
                historicoCota.PLFechamento = 0;
                historicoCota.PatrimonioBruto = valorPL;
                historicoCota.QuantidadeFechamento = quantidadeCotas;
                historicoCota.AjustePL = 0;
                historicoCota.Save();
            }
            //

        }

        /// <summary>
        /// Método central para processamento da cota líquida (incorporando tx adm e pfee do dia).
        /// Leva em conta todos os ativos da carteira, os valores a pagar/receber, o caixa, para compor o PL.
        /// Leva em conta a quantidade de cotas e gera a cota final.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaCotaLiquidaAbertura(int idCarteira, DateTime data, bool cravaCota)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> camposCliente = new List<esQueryItem>();
            camposCliente.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(camposCliente, idCarteira);
            int idLocal = cliente.IdLocal.Value;

            DateTime dataAnterior = new DateTime();
            if (idLocal == LocalFeriadoFixo.Brasil)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
            }
            else
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
            }

            //Cálculo de provisões, taxa fiscalização CVM, atualização de cota de fundos nas posições da PosicaoFundo
            this.ExecutaAberturaAntesCotaLiquida(idCarteira, data);
            //

            #region Busca parâmetros da carteira
            Carteira carteira = new Carteira();

            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaCota);
            campos.Add(carteira.Query.CasasDecimaisCota);
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.CotaInicial);

            carteira.LoadByPrimaryKey(campos, idCarteira);

            int casasDecimaisCota = carteira.CasasDecimaisCota.Value;
            int tipoCota = carteira.TipoCota.Value;
            #endregion

            if (!cravaCota)
            {
                #region Busca saldo de caixa de abertura
                SaldoCaixa saldoCaixa = new SaldoCaixa();
                saldoCaixa.BuscaSaldoCaixa(idCarteira, data);

                decimal saldoCaixaAbertura = 0;
                if (saldoCaixa.SaldoAbertura.HasValue)
                    saldoCaixaAbertura = saldoCaixa.SaldoAbertura.Value;
                #endregion

                #region Busca total do valor de carteira
                HistoricoCota historicoCota = new HistoricoCota();
                decimal valorCarteira = historicoCota.CalculaCarteiraTotal(idCarteira, data, (int)ListaMoedaFixo.Real);
                #endregion

                #region Busca o valor total a liquidar (recebimentos/pagamentos projetados)
                Liquidacao liquidacao = new Liquidacao();
                decimal valorLiquidar = liquidacao.RetornaLiquidacaoVencimentoMaiorIgual(idCarteira, data, data);

                //É preciso somar aos valores internos liquidados na data (ex: Taxa Adm/Pfee/Provisões...) 
                liquidacao = new Liquidacao();
                decimal valorLiquidarNaData = liquidacao.RetornaLiquidacaoLancamentoNaData(idCarteira, data, data);
                valorLiquidar += valorLiquidarNaData;
                #endregion

                #region Busca a quantidade total de cotas da carteira/fundo
                historicoCota = new HistoricoCota();
                decimal quantidadeCotas = historicoCota.RetornaQuantidadeCotas(idCarteira, dataAnterior);
                #endregion

                decimal valorPL = valorCarteira + valorLiquidar + saldoCaixaAbertura;

                decimal cotaDia;
                //Se não existe qtde de cotas (fundo liquidado), assume a última cota de D-1
                //Se não existe cota em D-1, pega a cota inicial da carteira
                if (quantidadeCotas == 0)
                {
                    HistoricoCota historicoCotaAnterior = new HistoricoCota();
                    if (!historicoCotaAnterior.BuscaValorCota(idCarteira, dataAnterior))
                    {
                        cotaDia = carteira.CotaInicial.Value;
                    }
                    else
                    {
                        if (!historicoCotaAnterior.CotaAbertura.HasValue)
                        {
                            cotaDia = carteira.CotaInicial.Value;
                        }
                        else
                        {
                            cotaDia = historicoCotaAnterior.CotaAbertura.Value;
                        }
                    }
                }
                else
                {
                    if (carteira.IsTruncaCota())
                        cotaDia = Utilitario.Truncate(valorPL / quantidadeCotas, casasDecimaisCota);
                    else
                        cotaDia = Math.Round(valorPL / quantidadeCotas, casasDecimaisCota);
                }

                //Atualiza a cota e PL de abertura líquidos na HistoricoCota
                historicoCota = new HistoricoCota();
                historicoCota.LoadByPrimaryKey(data, idCarteira);

                historicoCota.CotaAbertura = cotaDia;
                historicoCota.PLAbertura = valorPL;
                historicoCota.AjustePL = 0;
                historicoCota.Save();
                //
            }
        }

        /// <summary>
        /// Método central para processamento da cota bruta (sem incorporar tx adm e pfee do dia).
        /// Leva em conta todos os ativos da carteira, os valores a pagar/receber, o caixa, para compor o PL.
        /// Leva em conta a quantidade de cotas e gera a cota final.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaCotaBrutaFechamento(int idCarteira, DateTime data, bool processaMae)
        {
            #region Busca TipoControle, IdMoeda e IdLocal do cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdMoeda);
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(idCarteira);
            byte tipoControle = cliente.TipoControle.Value;
            int idMoeda = cliente.IdMoeda.Value;
            int idLocal = cliente.IdLocal.Value;
            #endregion

            #region Busca parâmetros da carteira
            Carteira carteira = new Carteira();

            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaCota);
            campos.Add(carteira.Query.CasasDecimaisCota);
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.CotaInicial);

            carteira.LoadByPrimaryKey(campos, idCarteira);

            int casasDecimaisCota = carteira.CasasDecimaisCota.Value;
            int tipoCota = carteira.TipoCota.Value;
            #endregion

            #region Busca saldo de caixa de fechamento
            SaldoCaixa saldoCaixa = new SaldoCaixa();

            saldoCaixa.BuscaSaldoCaixa(idCarteira, data, idMoeda);

            decimal saldoCaixaFechamento = 0;
            if (saldoCaixa.SaldoFechamento.HasValue)
                saldoCaixaFechamento = saldoCaixa.SaldoFechamento.Value;
            #endregion

            #region Busca total do valor de carteira
            HistoricoCota historicoCota = new HistoricoCota();
            decimal valorCarteira = historicoCota.CalculaCarteiraTotal(idCarteira, data, idMoeda);
            #endregion

            #region Busca o valor total a liquidar (recebimentos/pagamentos projetados)
            Liquidacao liquidacao = new Liquidacao();

            decimal valorLiquidar = 0;

            valorLiquidar = liquidacao.RetornaLiquidacaoVencimentoMaior(idCarteira, data, idMoeda);
            #endregion

            #region Valor de Inclusao/Retirada com impacto na quantidade de cotas
            decimal valorIngressoRetirada = historicoCota.CalculaValorIngressoRetirada(idCarteira, data);
            #endregion

            #region Busca o IR, IOF e PFEE sobre o resgate, caso seja liquidado em D0
            decimal valorImpostosResgateD0 = liquidacao.RetornaLiquidacaoImpostosResgateD0(idCarteira, data, idMoeda);
            #endregion

            decimal valorCompensacao = 0;
            if (tipoCota == (int)TipoCotaFundo.Fechamento)
            {
                #region Busca totais de valores de compensação vencendo na data (ex: valores a converter em cotas)
                liquidacao = new Liquidacao();
                valorCompensacao = liquidacao.RetornaLiquidacaoCompensacao(idCarteira, data);
                #endregion
            }

            decimal valorAcumuladoAdm = 0;
            decimal valorAcumuladoPfee = 0;
            CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
            calculoAdministracao.Query.Select(calculoAdministracao.Query.ValorAcumulado.Sum());
            calculoAdministracao.Query.Where(calculoAdministracao.Query.IdCarteira.Equal(idCarteira),
                                             calculoAdministracao.Query.DataPagamento.GreaterThanOrEqual(data));
            calculoAdministracao.Query.Load();

            if (calculoAdministracao.ValorAcumulado.HasValue)
            {
                valorAcumuladoAdm = calculoAdministracao.ValorAcumulado.Value;
            }

            #region Caso tenha taxa administracao/Gestao/Custodia calculada, estorna o valor do cálculo.
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Select(liquidacaoCollection.Query.Valor.Sum());
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente == idCarteira,
                                             liquidacaoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                             liquidacaoCollection.Query.DataVencimento.GreaterThan(data),
                                             liquidacaoCollection.Query.Origem.In(OrigemLancamentoLiquidacao.Provisao.TaxaAdministracao,
                                                                                  OrigemLancamentoLiquidacao.Provisao.TaxaGestao,
                                                                                  OrigemLancamentoLiquidacao.Provisao.TaxaCustodia));
            liquidacaoCollection.Query.Load();
            if(liquidacaoCollection[0].Valor.HasValue)
                valorLiquidar -= liquidacaoCollection[0].Valor.Value;                    

            #endregion

            //Só embute na cota bruta para quando o cálculo é cota global dia, pois neste caso precisa adicionar a pfee acumulada até d-1
            TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
            tabelaTaxaPerformanceCollection.Query.Select(tabelaTaxaPerformanceCollection.Query.IdTabela);
            tabelaTaxaPerformanceCollection.Query.Where(tabelaTaxaPerformanceCollection.Query.IdCarteira.Equal(idCarteira) &
                                                        tabelaTaxaPerformanceCollection.Query.DataReferencia.LessThanOrEqual(data) &
                                                        ( tabelaTaxaPerformanceCollection.Query.DataFim.IsNull() | tabelaTaxaPerformanceCollection.Query.DataFim.GreaterThanOrEqual(data) ) &
                                                        tabelaTaxaPerformanceCollection.Query.TipoCalculo.Equal((byte)TipoCalculoPerformance.CotaCotaDiario));
            tabelaTaxaPerformanceCollection.Query.Load();
            if (tabelaTaxaPerformanceCollection.Count > 0)
            {
                CalculoPerformance calculoPerformance = new CalculoPerformance();
                calculoPerformance.Query.Select(calculoPerformance.Query.ValorAcumulado.Sum());
                calculoPerformance.Query.Where(calculoPerformance.Query.IdCarteira.Equal(idCarteira),
                                               calculoPerformance.Query.ValorAcumulado.GreaterThan(0));
                calculoPerformance.Query.Load();

                if (calculoPerformance.ValorAcumulado.HasValue)
                {
                    valorAcumuladoPfee = calculoPerformance.ValorAcumulado.Value;
                }

            }

            decimal valorPatBruto = (valorCarteira - valorIngressoRetirada) + valorLiquidar + saldoCaixaFechamento + valorCompensacao - valorAcumuladoAdm - valorAcumuladoPfee - valorImpostosResgateD0;

            if (processaMae)
            {
                Cliente clienteFilhas = new Cliente();
                List<int> listaIdsFilhas = clienteFilhas.RetornaListaIdsFilhas(idCarteira);

                //Precisa compensar eventuais aportes e resgates que tenham sido "cotizados" no dia nas filhas, pois não devem impactar a cota bruta da mãe
                OperacaoCotista operacaoCotistaFilhasAportes = new OperacaoCotista();
                operacaoCotistaFilhasAportes.Query.Select(operacaoCotistaFilhasAportes.Query.ValorBruto.Sum());
                operacaoCotistaFilhasAportes.Query.Where(operacaoCotistaFilhasAportes.Query.IdCarteira.In(listaIdsFilhas),
                                                         operacaoCotistaFilhasAportes.Query.DataConversao.Equal(data),
                                                         operacaoCotistaFilhasAportes.Query.TipoOperacao.Equal((byte)TipoOperacaoCotista.Aplicacao));
                operacaoCotistaFilhasAportes.Query.Load();

                decimal aporteFilhas = operacaoCotistaFilhasAportes.ValorBruto.HasValue ? operacaoCotistaFilhasAportes.ValorBruto.Value : 0;

                OperacaoCotista operacaoCotistaFilhasResgates = new OperacaoCotista();
                operacaoCotistaFilhasResgates.Query.Select(operacaoCotistaFilhasResgates.Query.ValorBruto.Sum());
                operacaoCotistaFilhasResgates.Query.Where(operacaoCotistaFilhasResgates.Query.IdCarteira.In(listaIdsFilhas),
                                                         operacaoCotistaFilhasResgates.Query.DataConversao.Equal(data),
                                                         operacaoCotistaFilhasResgates.Query.TipoOperacao.In((byte)TipoOperacaoCotista.ResgateBruto,
                                                                                                             (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                                                             (byte)TipoOperacaoCotista.ResgateTotal));
                operacaoCotistaFilhasResgates.Query.Load();

                decimal resgatesFilhas = operacaoCotistaFilhasResgates.ValorBruto.HasValue ? operacaoCotistaFilhasResgates.ValorBruto.Value : 0;

                valorPatBruto = valorPatBruto - aporteFilhas + resgatesFilhas;
            }

            #region Busca a quantidade total de cotas da carteira/fundo
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            decimal quantidadeCotas = posicaoCotista.RetornaTotalCotas(idCarteira);
            #endregion

            decimal cotaDia;
            //Se não existe qtde de cotas (fundo liquidado), assume a última cota de D-1
            //Se não existe cota em D-1, pega a cota inicial da carteira
            if (quantidadeCotas == 0)
            {
                DateTime dataAnterior = new DateTime();
                if (idLocal == LocalFeriadoFixo.Brasil)
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
                }
                else
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                }

                HistoricoCota historicoCotaAnterior = new HistoricoCota();
                if (!historicoCotaAnterior.BuscaValorCota(idCarteira, dataAnterior))
                {
                    cotaDia = carteira.CotaInicial.Value;
                }
                else
                {
                    if (!historicoCotaAnterior.CotaFechamento.HasValue)
                    {
                        cotaDia = carteira.CotaInicial.Value;
                    }
                    else
                    {
                        cotaDia = historicoCotaAnterior.CotaFechamento.Value;
                    }
                }
            }
            else
            {
                if (carteira.IsTruncaCota())
                    cotaDia = Utilitario.Truncate(valorPatBruto / quantidadeCotas, casasDecimaisCota);
                else
                    cotaDia = Math.Round(valorPatBruto / quantidadeCotas, casasDecimaisCota);
            }

            //Salva a cota e PL de abertura brutos na HistoricoCota
            HistoricoCota historicoCotaSalvar = new HistoricoCota();
            if (!historicoCotaSalvar.LoadByPrimaryKey(data, idCarteira))
            {
                historicoCotaSalvar = new HistoricoCota();
                historicoCotaSalvar.PLAbertura = valorPatBruto;
                historicoCotaSalvar.CotaAbertura = cotaDia;
            }
            historicoCotaSalvar.Data = data;
            historicoCotaSalvar.IdCarteira = idCarteira;
            //historicoCotaSalvar.CotaFechamento = 0;
            historicoCotaSalvar.CotaBruta = cotaDia;
            historicoCotaSalvar.PLFechamento = 0;            
            historicoCotaSalvar.PatrimonioBruto = valorPatBruto;
            historicoCotaSalvar.QuantidadeFechamento = quantidadeCotas;
            historicoCotaSalvar.AjustePL = 0;
            historicoCotaSalvar.Save();
            //

        }

        /// <summary>
        /// Método central para processamento da cota líquida (incorporando tx adm e pfee do dia).
        /// Leva em conta todos os ativos da carteira, os valores a pagar/receber, o caixa, para compor o PL.
        /// Leva em conta a quantidade de cotas e gera a cota final.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaCotaLiquidaFechamento(int idCarteira, DateTime data)
        {
            this.ProcessaCotaLiquidaFechamento(idCarteira, data, true);
        }

        /// <summary>
        /// Método central para processamento da cota líquida (incorporando tx adm e pfee do dia).
        /// Leva em conta todos os ativos da carteira, os valores a pagar/receber, o caixa, para compor o PL.
        /// Leva em conta a quantidade de cotas e gera a cota final.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaCotaLiquidaFechamento(int idCarteira, DateTime data, bool utilizaCompensacao)
        {
            #region Busca TipoControle, IdMoeda e IdLocal do cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdMoeda);
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(idCarteira);
            byte tipoControle = cliente.TipoControle.Value;
            int idMoeda = cliente.IdMoeda.Value;
            int idLocal = cliente.IdLocal.Value;
            #endregion

            #region Busca parâmetros da carteira
            Carteira carteira = new Carteira();

            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaCota);
            campos.Add(carteira.Query.CasasDecimaisCota);
            campos.Add(carteira.Query.DataInicioCota);
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.CotaInicial);

            carteira.LoadByPrimaryKey(campos, idCarteira);

            int casasDecimaisCota = carteira.CasasDecimaisCota.Value;
            int tipoCota = carteira.TipoCota.Value;
            #endregion

            #region Busca saldo de caixa de fechamento
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.BuscaSaldoCaixa(idCarteira, data, idMoeda);

            decimal saldoCaixaFechamento = 0;
            if (saldoCaixa.SaldoFechamento.HasValue)
                saldoCaixaFechamento = saldoCaixa.SaldoFechamento.Value;
            #endregion

            #region Busca total do valor de carteira
            HistoricoCota historicoCota = new HistoricoCota();
            decimal valorCarteira = historicoCota.CalculaCarteiraTotal(idCarteira, data, idMoeda);
            #endregion

            #region Busca o valor total a liquidar (recebimentos/pagamentos projetados)
            Liquidacao liquidacao = new Liquidacao();

            decimal valorLiquidar = 0;
            valorLiquidar = liquidacao.RetornaLiquidacaoVencimentoMaior(idCarteira, data, idMoeda);
            #endregion

            #region Busca o IR, IOF e PFEE sobre o resgate, caso seja liquidado em D0
            decimal valorImpostosResgateD0 = 0;
            if (cliente.DescontaTributoPL.Value != (byte)DescontoPLCliente.PLComDesconto)
                valorImpostosResgateD0 = liquidacao.RetornaLiquidacaoImpostosResgateD0(idCarteira, data, idMoeda);
            #endregion


            #region Valor de Inclusao/Retirada com impacto na quantidade de cotas
            decimal valorIngressoRetirada = historicoCota.CalculaValorIngressoRetirada(idCarteira, data);
            #endregion

            decimal valorCompensacao = 0;
            if (tipoCota == (int)TipoCotaFundo.Fechamento && utilizaCompensacao)
            {
                #region Busca totais de valores de compensação vencendo na data (ex: valores a converter em cotas)
                liquidacao = new Liquidacao();
                valorCompensacao = liquidacao.RetornaLiquidacaoCompensacao(idCarteira, data);
                #endregion
            }
            
            decimal valorPL = (valorCarteira - valorIngressoRetirada) + valorLiquidar + saldoCaixaFechamento + valorCompensacao - valorImpostosResgateD0;

            #region Busca a quantidade total de cotas da carteira/fundo
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            decimal quantidadeCotas = posicaoCotista.RetornaTotalCotas(idCarteira);
            #endregion

            decimal cotaDia;

            #region Calcula Cota
            if (data == carteira.DataInicioCota.Value)
            {
                cotaDia = carteira.CotaInicial.Value;
            }
            else
            {
                //Se não existe qtde de cotas (fundo liquidado), assume a última cota de D-1
                //Se não existe cota em D-1, pega a cota inicial da carteira
                if (quantidadeCotas == 0 || valorPL == 0)
                {
                    DateTime dataAnterior = new DateTime();
                    if (idLocal == LocalFeriadoFixo.Brasil)
                    {
                        dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
                    }
                    else
                    {
                        dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                    }

                    HistoricoCota historicoCotaAnterior = new HistoricoCota();
                    if (!historicoCotaAnterior.BuscaValorCota(idCarteira, dataAnterior))
                    {
                        cotaDia = carteira.CotaInicial.Value;
                    }
                    else
                    {
                        cotaDia = !historicoCotaAnterior.CotaFechamento.HasValue
                                            ? carteira.CotaInicial.Value : historicoCotaAnterior.CotaFechamento.Value;
                    }
                }
                else
                {
                    cotaDia = carteira.IsTruncaCota()
                                            ? Utilitario.Truncate(valorPL / quantidadeCotas, casasDecimaisCota)
                                            : Math.Round(valorPL / quantidadeCotas, casasDecimaisCota);
                }
            }
            #endregion

            //Atualiza a cota e PL de abertura líquidos na HistoricoCota
            historicoCota = new HistoricoCota();
            if (!historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                historicoCota = new HistoricoCota();
                historicoCota.Data = data;
                historicoCota.IdCarteira = idCarteira;
                historicoCota.QuantidadeFechamento = quantidadeCotas;
                historicoCota.PLAbertura = valorPL;
                historicoCota.PatrimonioBruto = valorPL;
                historicoCota.CotaAbertura = cotaDia;
                historicoCota.CotaBruta = cotaDia;
            }

            if (historicoCota.QuantidadeFechamento == 0 && quantidadeCotas != 0)
            {
                historicoCota.QuantidadeFechamento = quantidadeCotas;
            }
            
            historicoCota.CotaFechamento = cotaDia;
            historicoCota.PLFechamento = valorPL;
            historicoCota.AjustePL = 0;
            historicoCota.Save();
            //
        }

        /// <summary>
        /// Calcula o PL e compara com PL já salvo na HistoricoCota. A diferença é jogada como pendencia para d+1 no CPR.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaAjusteCravaCota(int idCarteira, DateTime data)
        {
            #region Busca TipoControle e IdMoeda do cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdMoeda);
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(idCarteira);
            int idMoeda = cliente.IdMoeda.Value;
            short idLocal = cliente.IdLocal.Value;
            #endregion

            #region Busca parâmetros da carteira
            Carteira carteira = new Carteira();

            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaCota);
            campos.Add(carteira.Query.CasasDecimaisCota);
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.CotaInicial);
            campos.Add(carteira.Query.DataInicioCota);

            carteira.LoadByPrimaryKey(campos, idCarteira);

            int casasDecimaisCota = carteira.CasasDecimaisCota.Value;
            byte tipoCota = carteira.TipoCota.Value;
            string truncaCota = carteira.TruncaCota;
            DateTime dataInicioCota = carteira.DataInicioCota.Value;
            #endregion

            #region Calcula caixa de fechamento novamente
            ControllerContaCorrente controllerContaCorrente = new ControllerContaCorrente();
            controllerContaCorrente.ExecutaCalculoSaldoFechamento(idCarteira, data);
            #endregion

            HistoricoCota historicoCota = new HistoricoCota();
            if (historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                decimal quantidadeCotas = historicoCota.QuantidadeFechamento.Value;
                decimal PlFechamento = historicoCota.PLFechamento.Value;
                decimal cotaFechamento = historicoCota.CotaFechamento.Value;

                decimal PlCalculado = this.RetornaCalculoPLFechamento(idCarteira, data, idMoeda, tipoCota);
                //decimal cotaCalculada = 0;
                //if (truncaCota == "S")
                //{
                //    cotaCalculada = Utilitario.Truncate(PlCalculado / quantidadeCotas, casasDecimaisCota);
                //}
                //else
                //{
                //    cotaCalculada = Math.Round(PlCalculado / quantidadeCotas, casasDecimaisCota);
                //}

                //decimal diferencaCotas = cotaFechamento - cotaCalculada;
                decimal diferencaPL = PlFechamento - PlCalculado;

                if (diferencaPL != 0)
                {
                    //decimal valorAjuste = Math.Round(diferencaCotas * quantidadeCotas, 2);
                    decimal valorAjuste = diferencaPL;

                    //Busca o idContaDefault da carteira
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira, idMoeda);
                    //

                    DateTime dataProxima = new DateTime();
                    if (idLocal == (int)LocalFeriadoFixo.Brasil)
                    {
                        dataProxima = Calendario.AdicionaDiaUtil(data, 1);
                    }
                    else
                    {
                        dataProxima = Calendario.AdicionaDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                    }

                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataProxima;
                    liquidacao.Descricao = "Ajuste de Compensação de Cota";
                    liquidacao.Valor = valorAjuste;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Compensacao;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.AjusteCompensacaoCota;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Save();
                }
            }
        }

        /// <summary>
        /// Método central para processamento da cota líquida (incorporando tx adm e pfee do dia).
        /// Leva em conta todos os ativos da carteira, os valores a pagar/receber, o caixa, para compor o PL.
        /// Leva em conta a quantidade de cotas e gera a cota final.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public decimal RetornaCalculoPLFechamento(int idCarteira, DateTime data, int idMoeda, byte tipoCota)
        {
            #region Busca caixa de fechamento
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.BuscaSaldoCaixa(idCarteira, data, idMoeda);

            decimal saldoCaixaFechamento = 0;
            if (saldoCaixa.SaldoFechamento.HasValue)
                saldoCaixaFechamento = saldoCaixa.SaldoFechamento.Value;
            #endregion

            #region Busca total do valor de carteira
            HistoricoCota historicoCota = new HistoricoCota();
            decimal valorCarteira = historicoCota.CalculaCarteiraTotal(idCarteira, data, idMoeda);
            #endregion

            #region Busca o valor total a liquidar (recebimentos/pagamentos projetados)
            Liquidacao liquidacao = new Liquidacao();

            decimal valorLiquidar = 0;
            valorLiquidar = liquidacao.RetornaLiquidacaoVencimentoMaior(idCarteira, data, idMoeda);
            #endregion

            #region Busca o IR, IOF e PFEE sobre o resgate, caso seja liquidado em D0
            decimal valorImpostosResgateD0 = 0;
            if (liquidacao.VerificaJaResgatouCotistaD0(idCarteira, data) == 0 )
            {
                valorImpostosResgateD0 = liquidacao.RetornaLiquidacaoImpostosResgateD0(idCarteira, data, idMoeda);
            }
            
            #endregion

            decimal valorCompensacaoFundos = 0;
            if (tipoCota == (int)TipoCotaFundo.Fechamento)
            {
                #region Busca totais de valores a converter em cotas de Fundos
                liquidacao = new Liquidacao();
                valorCompensacaoFundos = liquidacao.RetornaLiquidacaoCompensacaoFundos(idCarteira, data);
                #endregion
            }

            decimal valorPL = valorCarteira + valorLiquidar + saldoCaixaFechamento + valorCompensacaoFundos - valorImpostosResgateD0;

            return valorPL;
        }

        /// <summary>
        /// Método central para processamento da cota líquida (incorporando tx adm e pfee do dia).
        /// Leva em conta todos os ativos da carteira, os valores a pagar/receber, o caixa, para compor o PL.
        /// Leva em conta a quantidade de cotas e gera a cota final.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaPLFechamentoDepoisCota(int idCarteira, DateTime data, bool geraHistoricoLiquidacao)
        {
            #region Busca TipoControle e IdMoeda do cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdMoeda);
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.IdLocal);
            campos.Add(cliente.Query.ZeraCaixa);
            cliente.LoadByPrimaryKey(idCarteira);
            byte tipoControle = cliente.TipoControle.Value;
            int idMoeda = cliente.IdMoeda.Value;
            int idTipo = cliente.IdTipo.Value;
            int idLocal = cliente.IdLocal.Value;
            string zeraCaixa = cliente.ZeraCaixa;
            #endregion

            #region Busca parâmetros da carteira
            Carteira carteira = new Carteira();

            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaCota);
            campos.Add(carteira.Query.CasasDecimaisCota);
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.DataInicioCota);
            campos.Add(carteira.Query.CotaInicial);

            carteira.LoadByPrimaryKey(campos, idCarteira);

            int casasDecimaisCota = carteira.CasasDecimaisCota.Value;
            byte tipoCota = carteira.TipoCota.Value;
            string truncaCota = carteira.TruncaCota;
            DateTime dataInicioCota = carteira.DataInicioCota.Value;
            #endregion

            #region Calcula caixa de fechamento novamente
            ControllerContaCorrente controllerContaCorrente = new ControllerContaCorrente();
            controllerContaCorrente.ExecutaCalculoSaldoFechamento(idCarteira, data);
            #endregion

            decimal valorPL = this.RetornaCalculoPLFechamento(idCarteira, data, idMoeda, tipoCota);

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            decimal quantidadeCotas = posicaoCotista.RetornaTotalCotas(idCarteira);

            OperacaoCotista operacaoCotista = new OperacaoCotista();
            quantidadeCotas -= operacaoCotista.RetornaTotalCotasResgateEspecial(idCarteira, data);

            //Atualiza o PL de fechamento após todos os cálculos de cota
            HistoricoCota historicoCota = new HistoricoCota();
            if (!historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                HistoricoCota historicoCotaNew = new HistoricoCota();
                historicoCotaNew.AjustePL = 0;
                historicoCotaNew.CotaAbertura = 0;
                historicoCotaNew.CotaBruta = 0;
                historicoCotaNew.CotaFechamento = 0;
                historicoCotaNew.Data = data;
                historicoCotaNew.IdCarteira = idCarteira;
                historicoCotaNew.PatrimonioBruto = valorPL;
                historicoCotaNew.PLAbertura = valorPL;
                historicoCotaNew.PLFechamento = valorPL;
                historicoCotaNew.QuantidadeFechamento = 0;
                historicoCotaNew.Save();
            }
            else
            {
                decimal cotaDia = 0;
                if (((idTipo != (int)TipoClienteFixo.Clube && idTipo != (int)TipoClienteFixo.Fundo && idTipo != (int)TipoClienteFixo.FDIC && idTipo != (int)TipoClienteFixo.ClientePessoaFisica) ||
                        ParametrosConfiguracaoSistema.Fundo.RecalculaCota == "S")
                    && dataInicioCota != data && quantidadeCotas != 0 && valorPL != 0 && historicoCota.PLAbertura.Value != 0)
                {
                    cotaDia = 0;
                    if (truncaCota == "S")
                    {
                        cotaDia = Utilitario.Truncate(valorPL / quantidadeCotas, casasDecimaisCota);
                    }
                    else
                    {
                        cotaDia = Math.Round(valorPL / quantidadeCotas, casasDecimaisCota);
                    }

                    historicoCota.CotaFechamento = cotaDia;
                    historicoCota.CotaBruta = cotaDia;
                    historicoCota.PatrimonioBruto = valorPL;
                    historicoCota.QuantidadeFechamento = quantidadeCotas;
                }

                if (dataInicioCota == data && quantidadeCotas > 0)
                {
                    historicoCota.PLAbertura = valorPL;

                    cotaDia = 0;
                    if (truncaCota == "S")
                    {
                        cotaDia = Utilitario.Truncate(valorPL / quantidadeCotas, casasDecimaisCota);
                    }
                    else
                    {
                        cotaDia = Math.Round(valorPL / quantidadeCotas, casasDecimaisCota);
                    }

                    historicoCota.CotaFechamento = cotaDia;

                    //Efetua lançamento para compensar diferença na cota
                    decimal valorLancamento = ((cotaDia - carteira.CotaInicial.Value) * quantidadeCotas) * (-1);

                    //Busca o idContaDefault da carteira
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira, idMoeda);
                    //

                    DateTime dataProxima = new DateTime();
                    if (idLocal == (int)LocalFeriadoFixo.Brasil)
                    {
                        dataProxima = Calendario.AdicionaDiaUtil(data, 1);
                    }
                    else
                    {
                        dataProxima = Calendario.AdicionaDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                    }

                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.IdCliente = idCarteira;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataProxima;
                    liquidacao.Descricao = "Ajuste de Compensação de Cota";
                    liquidacao.Valor = valorLancamento;
                    liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Compensacao;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.AjusteCompensacaoCota;
                    liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdConta = idContaDefault;
                    liquidacao.Save();

                    if (geraHistoricoLiquidacao)
                    {
                        LiquidacaoHistorico.GeraLiquidacaoHistorico(liquidacao, data);
                    }

                    historicoCota.CotaFechamento = carteira.CotaInicial.Value;
                    valorPL = Math.Round(historicoCota.CotaFechamento.Value * historicoCota.QuantidadeFechamento.Value, 2);
                    historicoCota.CotaBruta = historicoCota.CotaFechamento;

                }

                //Verifica se existe travamento de cota
                TravamentoCotasQuery travamentoCotasQuery = new TravamentoCotasQuery();
                travamentoCotasQuery.Where(travamentoCotasQuery.IdCarteira == idCarteira);
                travamentoCotasQuery.Where(travamentoCotasQuery.DataProcessamento == data);
                travamentoCotasQuery.Where(travamentoCotasQuery.TipoBloqueio.In((int)TipoTravamentoCota.CravaCota, (int)TipoTravamentoCota.Shadow));
                TravamentoCotasCollection travamentoCotasCollection = new TravamentoCotasCollection();

                if (travamentoCotasCollection.Load(travamentoCotasQuery))
                {

                    travamentoCotasCollection[0].ValorCotaCalculada = cotaDia;
                    travamentoCotasCollection.Save();

                    if (travamentoCotasCollection[0].ValorCota.Value != cotaDia && quantidadeCotas > 0)
                    {
                        //Efetua lançamento para compensar diferença na cota
                        decimal valorLancamento = (((valorPL / quantidadeCotas) - travamentoCotasCollection[0].ValorCota.Value) * quantidadeCotas) * (-1);

                        travamentoCotasCollection[0].AjusteCompensacao = valorLancamento;
                        travamentoCotasCollection.Save();

                        //Busca o idContaDefault da carteira
                        Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                        int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira, idMoeda);
                        //

                        DateTime dataProxima = new DateTime();
                        if (idLocal == (int)LocalFeriadoFixo.Brasil)
                        {
                            dataProxima = Calendario.AdicionaDiaUtil(data, 1);
                        }
                        else
                        {
                            dataProxima = Calendario.AdicionaDiaUtil(data, 1, (int)idLocal, TipoFeriado.Outros);
                        }

                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.IdCliente = idCarteira;
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataProxima;
                        liquidacao.Descricao = "Ajuste de Compensação de Cota";
                        liquidacao.Valor = valorLancamento;
                        liquidacao.Situacao = (int)SituacaoLancamentoLiquidacao.Compensacao;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.AjusteCompensacaoCota;
                        liquidacao.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdConta = idContaDefault;
                        liquidacao.Save();

                        LiquidacaoHistorico.GeraLiquidacaoHistorico(liquidacao, data);

                        Liquidacao liquidacaoPfee = new Liquidacao();
                        liquidacaoPfee.Query.Select(liquidacaoPfee.Query.Valor.Sum());
                        liquidacaoPfee.Query.Where(liquidacaoPfee.Query.Situacao.Equal((int)SituacaoLancamentoLiquidacao.Normal) &
                                               liquidacaoPfee.Query.Origem.Equal(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance) &
                                               liquidacaoPfee.Query.Fonte.Equal((int)FonteLancamentoLiquidacao.Interno) &
                                               liquidacaoPfee.Query.DataVencimento.LessThan(data) &
                                               liquidacaoPfee.Query.DataLancamento.GreaterThanOrEqual(data) &
                                               liquidacaoPfee.Query.IdCliente.Equal(idCarteira));

                        decimal? cotaBrutaCravada = null;
                        if (liquidacaoPfee.Query.Load() && liquidacaoPfee.Valor.GetValueOrDefault(0) > 0)
                        {
                            decimal valorPfee = liquidacaoPfee.Valor.Value;

                            if (valorPfee != 0)
                            {
                                decimal patrimonioCravado = travamentoCotasCollection[0].ValorCota.Value * quantidadeCotas;
                                decimal patrimonioCalculado = cotaDia * quantidadeCotas;

                                decimal patrimonioAux = patrimonioCravado * valorPfee;
                                patrimonioAux = patrimonioAux / patrimonioCalculado;

                                if (((idTipo != (int)TipoClienteFixo.Clube && idTipo != (int)TipoClienteFixo.Fundo && idTipo != (int)TipoClienteFixo.FDIC) ||
                                        ParametrosConfiguracaoSistema.Fundo.RecalculaCota == "S")
                                        && dataInicioCota != data && quantidadeCotas != 0 && patrimonioAux != 0)
                                {
                                    cotaBrutaCravada = 0;
                                    if (truncaCota == "S")
                                    {
                                        cotaBrutaCravada = Utilitario.Truncate(patrimonioAux / quantidadeCotas, casasDecimaisCota);
                                    }
                                    else
                                    {
                                        cotaBrutaCravada = Math.Round(patrimonioAux / quantidadeCotas, casasDecimaisCota);
                                    }                                    
                                }
                            }
                        }

                        historicoCota.CotaFechamento = travamentoCotasCollection[0].ValorCota.Value;
                        valorPL = historicoCota.CotaFechamento.Value * historicoCota.QuantidadeFechamento.Value;

                        if (cotaBrutaCravada.HasValue)
                        {
                            historicoCota.CotaBruta = cotaBrutaCravada.Value;
                        }
                        else
                        {
                            historicoCota.CotaBruta = travamentoCotasCollection[0].ValorCota.Value;
                        }
                    }                   
                }

                #region Atualiza Acumulado de Proventos com total calculado
                AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
                agendaFundoCollection.Query.Select(agendaFundoCollection.Query.Valor.Sum());
                agendaFundoCollection.Query.Where(agendaFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                  agendaFundoCollection.Query.DataEvento.LessThanOrEqual(data));
                if (agendaFundoCollection.Query.Load())
                {
                    historicoCota.ProventoAcumulado = agendaFundoCollection[0].Valor.GetValueOrDefault(0);
                }
                else
                {
                    historicoCota.ProventoAcumulado = 0;
                }
                #endregion

                #region Atualiza Cota-Ex
                historicoCota.CotaEx = historicoCota.CotaFechamento.Value;
                #endregion

                #region AtualizaCotaRendimento
                historicoCota.CotaRendimento = historicoCota.CotaFechamento + historicoCota.ProventoAcumulado.GetValueOrDefault(0);
                #endregion

                historicoCota.PLFechamento = valorPL;
                historicoCota.AjustePL = 0;
                historicoCota.Save();
            }
            //
        }

        /// <summary>
        /// Método central para processamento da cota líquida (incorporando tx adm e pfee do dia).
        /// Leva em conta todos os ativos da carteira, os valores a pagar/receber, o caixa, para compor o PL.
        /// Leva em conta a quantidade de cotas e gera a cota final.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaPLAberturaDepoisCota(int idCarteira, DateTime data, bool geraHistoricoLiquidacao)
        {
            #region Busca TipoControle e IdMoeda do cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdMoeda);
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.IdLocal);
            campos.Add(cliente.Query.ZeraCaixa);
            cliente.LoadByPrimaryKey(idCarteira);
            byte tipoControle = cliente.TipoControle.Value;
            int idMoeda = cliente.IdMoeda.Value;
            int idTipo = cliente.IdTipo.Value;
            int idLocal = cliente.IdLocal.Value;
            string zeraCaixa = cliente.ZeraCaixa;
            #endregion

            #region Busca parâmetros da carteira
            Carteira carteira = new Carteira();

            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TruncaCota);
            campos.Add(carteira.Query.CasasDecimaisCota);
            campos.Add(carteira.Query.TipoCota);
            campos.Add(carteira.Query.DataInicioCota);
            campos.Add(carteira.Query.CotaInicial);

            carteira.LoadByPrimaryKey(campos, idCarteira);

            int casasDecimaisCota = carteira.CasasDecimaisCota.Value;
            byte tipoCota = carteira.TipoCota.Value;
            string truncaCota = carteira.TruncaCota;
            DateTime dataInicioCota = carteira.DataInicioCota.Value;
            #endregion

            #region Calcula caixa de fechamento novamente
            ControllerContaCorrente controllerContaCorrente = new ControllerContaCorrente();
            controllerContaCorrente.ExecutaCalculoSaldoFechamento(idCarteira, data);
            #endregion

            decimal valorPL = this.RetornaCalculoPLFechamento(idCarteira, data, idMoeda, tipoCota);

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            decimal quantidadeCotas = posicaoCotista.RetornaTotalCotas(idCarteira);

            //Atualiza o PL de fechamento após todos os cálculos de cota
            HistoricoCota historicoCota = new HistoricoCota();
            if (!historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                HistoricoCota historicoCotaNew = new HistoricoCota();
                historicoCotaNew.AjustePL = 0;
                historicoCotaNew.CotaAbertura = 0;
                historicoCotaNew.CotaBruta = 0;
                historicoCotaNew.CotaFechamento = 0;
                historicoCotaNew.Data = data;
                historicoCotaNew.IdCarteira = idCarteira;
                historicoCotaNew.PatrimonioBruto = valorPL;
                historicoCotaNew.PLAbertura = valorPL;
                historicoCotaNew.PLFechamento = valorPL;
                historicoCotaNew.QuantidadeFechamento = 0;
                historicoCotaNew.Save();
            }
            else
            {
                decimal cotaDia = 0;
                if (((idTipo != (int)TipoClienteFixo.Clube && idTipo != (int)TipoClienteFixo.Fundo && idTipo != (int)TipoClienteFixo.FDIC && idTipo != (int)TipoClienteFixo.ClientePessoaFisica) ||
                        ParametrosConfiguracaoSistema.Fundo.RecalculaCota == "S")
                    && dataInicioCota != data && quantidadeCotas != 0 && valorPL != 0 && historicoCota.PLAbertura.Value != 0)
                {
                    cotaDia = 0;
                    if (truncaCota == "S")
                    {
                        cotaDia = Utilitario.Truncate(valorPL / quantidadeCotas, casasDecimaisCota);
                    }
                    else
                    {
                        cotaDia = Math.Round(valorPL / quantidadeCotas, casasDecimaisCota);
                    }

                    historicoCota.CotaAbertura = cotaDia;
                    historicoCota.CotaBruta = cotaDia;
                    historicoCota.PatrimonioBruto = valorPL;
                    historicoCota.QuantidadeFechamento = quantidadeCotas;
                }

                (new TravamentoCotas()).InsereTravamentoCotas(idCarteira, data, cotaDia, TipoTravamentoCota.CravaCota);
            }            
        }


        /// <summary>
        /// Método central para processamento do PL do dia, para cálculos de carteiras por TIR.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaPLFechamentoPorTIR(int idCarteira, DateTime data, string zeraCaixa)
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdMoeda);
            cliente.LoadByPrimaryKey(campos, idCarteira);
            int idMoeda = cliente.IdMoeda.Value;

            #region Calcula caixa de fechamento novamente
            ControllerContaCorrente controllerContaCorrente = new ControllerContaCorrente();
            controllerContaCorrente.ExecutaCalculoSaldoFechamento(idCarteira, data);

            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.BuscaSaldoCaixa(idCarteira, data, idMoeda);


            decimal saldoCaixaFechamento = 0;
            if (saldoCaixa.SaldoFechamento.HasValue)
                saldoCaixaFechamento = saldoCaixa.SaldoFechamento.Value;
            #endregion

            if (zeraCaixa == "S")
            {
                decimal valorZeragem = 0;
                if (saldoCaixaFechamento != 0)
                {
                    valorZeragem = saldoCaixaFechamento * -1;
                    saldoCaixaFechamento = 0;

                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCarteira, idMoeda);
                    //                 

                    string descricao = valorZeragem > 0 ? "Aplicações do dia" : "Resgates do dia";
                    int origem = valorZeragem > 0 ? (int)OrigemLancamentoLiquidacao.Cotista.Aplicacao : (int)OrigemLancamentoLiquidacao.Cotista.Resgate;

                    Liquidacao liquidacaoCaixa = new Liquidacao();
                    liquidacaoCaixa.IdCliente = idCarteira;
                    liquidacaoCaixa.DataLancamento = data;
                    liquidacaoCaixa.DataVencimento = data;
                    liquidacaoCaixa.Descricao = descricao;
                    liquidacaoCaixa.Valor = valorZeragem;
                    liquidacaoCaixa.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacaoCaixa.Origem = origem;
                    liquidacaoCaixa.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    liquidacaoCaixa.IdConta = idContaDefault;

                    liquidacaoCaixa.Save();

                    SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
                    saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCarteira),
                                                     saldoCaixaCollection.Query.Data.Equal(data));
                    saldoCaixaCollection.Query.Load();

                    foreach (SaldoCaixa saldoCaixaUpdate in saldoCaixaCollection)
                    {
                        saldoCaixaUpdate.SaldoFechamento = 0;
                    }
                    saldoCaixaCollection.Save();
                }
            }

            #region Busca total do valor de carteira
            HistoricoCota historicoCota = new HistoricoCota();
            decimal valorCarteira = historicoCota.CalculaCarteiraTotal(idCarteira, data, idMoeda);
            #endregion

            #region Busca o valor total a liquidar (recebimentos/pagamentos projetados)
            Liquidacao liquidacao = new Liquidacao();

            decimal valorLiquidar = 0;
            valorLiquidar = liquidacao.RetornaLiquidacaoVencimentoMaior(idCarteira, data, idMoeda);
            #endregion

            #region Busca o IR, IOF e PFEE sobre o resgate, caso seja liquidado em D0
            decimal valorImpostosResgateD0 = liquidacao.RetornaLiquidacaoImpostosResgateD0(idCarteira, data, idMoeda);
            #endregion

            decimal valorPL = valorCarteira + valorLiquidar + saldoCaixaFechamento - valorImpostosResgateD0;

            //Atualiza o PL de fechamento após todos os cálculos de cota
            historicoCota = new HistoricoCota();
            if (!historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                HistoricoCota historicoCotaNew = new HistoricoCota();
                historicoCotaNew.AjustePL = 0;
                historicoCotaNew.CotaAbertura = 0;
                historicoCotaNew.CotaBruta = 0;
                historicoCotaNew.CotaFechamento = 0;
                historicoCotaNew.Data = data;
                historicoCotaNew.IdCarteira = idCarteira;
                historicoCotaNew.PatrimonioBruto = valorPL;
                historicoCotaNew.PLAbertura = valorPL;
                historicoCotaNew.PLFechamento = valorPL;
                historicoCotaNew.QuantidadeFechamento = 0;
                historicoCotaNew.Save();
            }
            else
            {
                historicoCota.PLFechamento = valorPL;
                historicoCota.Save();
            }
            //
        }

        /// <summary>
        /// Leva em conta todos os ativos da carteira, os valores a pagar/receber, o caixa, para compor o PL.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaPLFechamentoSemCota(int idCarteira, DateTime data)
        {
            #region Busca TipoControle e IdMoeda do cliente
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdMoeda);
            campos.Add(cliente.Query.IdTipo);
            campos.Add(cliente.Query.ZeraCaixa);
            cliente.LoadByPrimaryKey(idCarteira);
            byte tipoControle = cliente.TipoControle.Value;
            int idMoeda = cliente.IdMoeda.Value;
            int idTipo = cliente.IdTipo.Value;
            string zeraCaixa = cliente.ZeraCaixa;
            #endregion

            #region Calcula caixa de fechamento novamente
            ControllerContaCorrente controllerContaCorrente = new ControllerContaCorrente();
            controllerContaCorrente.ExecutaCalculoSaldoFechamento(idCarteira, data);
            #endregion

            decimal valorPL = this.RetornaCalculoPLFechamento(idCarteira, data, idMoeda, (byte)TipoCotaFundo.Fechamento);

            //Atualiza o PL de fechamento após todos os cálculos de cota
            HistoricoCota historicoCota = new HistoricoCota();
            if (!historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                HistoricoCota historicoCotaNew = new HistoricoCota();
                historicoCotaNew.AjustePL = 0;
                historicoCotaNew.CotaAbertura = 0;
                historicoCotaNew.CotaBruta = 0;
                historicoCotaNew.CotaFechamento = 0;
                historicoCotaNew.Data = data;
                historicoCotaNew.IdCarteira = idCarteira;
                historicoCotaNew.PatrimonioBruto = valorPL;
                historicoCotaNew.PLAbertura = valorPL;
                historicoCotaNew.PLFechamento = valorPL;
                historicoCotaNew.QuantidadeFechamento = 0;
                historicoCotaNew.Save();
            }
            else
            {
                historicoCota.PLAbertura = valorPL;
                historicoCota.PLFechamento = valorPL;
                historicoCota.AjustePL = 0;
                historicoCota.Save();
            }
            //
        }

        /// <summary>
        /// Processa recálculo da cota bruta, recompondo o valor da txadm/gestao calculadas no dia.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ProcessaRecalculoCotaBruta(int idCarteira, DateTime data, byte descontaTributoPL)
        {
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Query.Select(historicoCota.Query.IdCarteira,
                                       historicoCota.Query.Data,
                                       historicoCota.Query.QuantidadeFechamento,
                                       historicoCota.Query.PatrimonioBruto,
                                       historicoCota.Query.CotaBruta);
            historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                      historicoCota.Query.Data.Equal(data));
            if (historicoCota.Query.Load())
            {
                #region Busca parâmetros da carteira
                Carteira carteira = new Carteira();

                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TruncaCota);
                campos.Add(carteira.Query.CasasDecimaisCota);
                campos.Add(carteira.Query.CotaInicial);
                campos.Add(carteira.Query.DataInicioCota);
                campos.Add(carteira.Query.TipoCota);
                carteira.LoadByPrimaryKey(campos, idCarteira);

                int tipoCota = carteira.TipoCota.Value;
                int casasDecimaisCota = carteira.CasasDecimaisCota.Value;
                #endregion

                decimal quantidadeCotas = historicoCota.QuantidadeFechamento.Value;
                decimal patrimonioBruto = historicoCota.PatrimonioBruto.Value;

                decimal valorDia = 0;
                CalculoAdministracao calculoAdministracao = new CalculoAdministracao();
                calculoAdministracao.Query.Select(calculoAdministracao.Query.ValorDia.Sum());
                calculoAdministracao.Query.Where(calculoAdministracao.Query.IdCarteira.Equal(idCarteira));
                calculoAdministracao.Query.Load();

                if (calculoAdministracao.ValorDia.HasValue)
                {
                    valorDia = calculoAdministracao.ValorDia.Value;
                }

                decimal valorCompensacao = 0;
                if (tipoCota == (int)TipoCotaFundo.Fechamento)
                {
                    #region Busca totais de valores de compensação vencendo na data (ex: valores a converter em cotas)
                    Liquidacao liquidacao = new Liquidacao();
                    valorCompensacao = liquidacao.RetornaLiquidacaoCompensacao(idCarteira, data);
                    #endregion
                }

                decimal valorIR = 0;
                if (descontaTributoPL != (byte)DescontoPLCliente.PLComDesconto)
                {
                    #region Calcula todo o valor de IR sendo resgatado na data, para virtualmente expurgar da cota Bruta (mas mantendo no Pat.Bruto)
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
                    liquidacao.Query.Where(liquidacao.Query.IdCliente.Equal(idCarteira),
                                           liquidacao.Query.DataLancamento.Equal(data),
                                           liquidacao.Query.Origem.In((int)OrigemLancamentoLiquidacao.RendaFixa.IR,
                                                                      (int)OrigemLancamentoLiquidacao.RendaFixa.IOF,
                                                                      (int)OrigemLancamentoLiquidacao.Fundo.IRResgate,
                                                                      (int)OrigemLancamentoLiquidacao.Fundo.IOFResgate,
                                                                      (int)OrigemLancamentoLiquidacao.IR.IRRendaVariavel,
                                                                      (int)OrigemLancamentoLiquidacao.IR.IRFonteDayTrade,
                                                                      (int)OrigemLancamentoLiquidacao.IR.IRFonteOperacao));
                    liquidacao.Query.Load();

                    if (liquidacao.Valor.HasValue)
                    {
                        valorIR = Math.Abs(liquidacao.Valor.Value);
                    }

                    OperacaoFundo operacaoFundo = new OperacaoFundo();
                    operacaoFundo.Query.Select(operacaoFundo.Query.ValorIR.Sum());
                    operacaoFundo.Query.Where(operacaoFundo.Query.IdCliente.Equal(idCarteira),
                                              operacaoFundo.Query.DataOperacao.Equal(data),
                                              operacaoFundo.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ComeCotas));
                    operacaoFundo.Query.Load();

                    if (operacaoFundo.ValorIR.HasValue)
                    {
                        valorIR += operacaoFundo.ValorIR.Value;
                    }
                    #endregion
                }

                patrimonioBruto -= valorDia;
                decimal patrimonioBrutoSemIR = patrimonioBruto + valorIR - valorCompensacao; 

                decimal cotaBruta = 0;
                #region Calcula Cota
                if (data == carteira.DataInicioCota.Value)
                {
                    cotaBruta = carteira.CotaInicial.Value;
                }
                else
                {
                    //Se não existe qtde de cotas (fundo liquidado), assume a última cota de D-1
                    //Se não existe cota em D-1, pega a cota inicial da carteira
                    if (quantidadeCotas == 0 || patrimonioBruto == 0)
                    {
                        Cliente cliente = new Cliente();
                        List<esQueryItem> camposCliente = new List<esQueryItem>();
                        camposCliente.Add(cliente.Query.IdLocal);
                        cliente.LoadByPrimaryKey(camposCliente, idCarteira);
                        int idLocal = cliente.IdLocal.Value;

                        DateTime dataAnterior = new DateTime();
                        if (idLocal == LocalFeriadoFixo.Brasil)
                        {
                            dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
                        }
                        else
                        {
                            dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
                        }

                        HistoricoCota historicoCotaAnterior = new HistoricoCota();
                        if (!historicoCotaAnterior.BuscaValorCota(idCarteira, dataAnterior))
                        {
                            cotaBruta = carteira.CotaInicial.Value;
                        }
                        else
                        {
                            cotaBruta = !historicoCotaAnterior.CotaFechamento.HasValue
                                                ? carteira.CotaInicial.Value : historicoCotaAnterior.CotaFechamento.Value;
                        }
                    }
                    else
                    {
                        cotaBruta = carteira.IsTruncaCota()
                                                ? Utilitario.Truncate(patrimonioBrutoSemIR / quantidadeCotas, casasDecimaisCota)
                                                : Math.Round(patrimonioBrutoSemIR / quantidadeCotas, casasDecimaisCota);
                    }
                }
                #endregion

                historicoCota.PatrimonioBruto = patrimonioBruto;
                historicoCota.CotaBruta = cotaBruta;
                historicoCota.Save();
            }
        }

        /// <summary>
        /// Ajusta o caixa da mãe com eventuais pagamentos no dia de taxa de administração e performance da própria mãe.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void AjustaCaixaMae(int idCarteira, DateTime data)
        {
            Liquidacao liquidacao = new Liquidacao();
            liquidacao.Query.Select(liquidacao.Query.Valor.Sum());
            liquidacao.Query.Where(liquidacao.Query.IdCliente.Equal(idCarteira),
                                   liquidacao.Query.DataVencimento.Equal(data),
                                   liquidacao.Query.Origem.In(OrigemLancamentoLiquidacao.Provisao.PagtoTaxaAdministracao,
                                                              OrigemLancamentoLiquidacao.Provisao.PagtoTaxaGestao,
                                                              OrigemLancamentoLiquidacao.Provisao.PagtoTaxaPerformance));
            liquidacao.Query.Load();

            decimal valor = 0;
            if (liquidacao.Valor.HasValue)
            {
                valor = liquidacao.Valor.Value;
            }

            if (valor != 0)
            {
                SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
                saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCarteira),
                                                 saldoCaixaCollection.Query.Data.Equal(data));
                saldoCaixaCollection.Query.Load();

                if (saldoCaixaCollection.Count > 0)
                {
                    saldoCaixaCollection[0].SaldoFechamento += valor;
                }
            }
        }

        /// <summary>
        /// Calcula uma cota a partir do conceito de TIR aplicando o fator do retorno diário sobre a cota de d-1.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <returns></returns>
        public void CalculaCotaPorTIR(int idCarteira, DateTime data, bool historico)
        {
            Carteira carteira = new Carteira();

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(campos, idCarteira);
            int idLocal = cliente.IdLocal.Value;

            DateTime dataAnterior = new DateTime();
            if (idLocal == LocalFeriadoFixo.Brasil)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
            }
            else
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocal, TipoFeriado.Outros);
            }

            decimal valorData = 0;

            if (historico)
            {
                valorData = carteira.RetornaValorMercadoHistorico(idCarteira, data);
            }
            else
            {
                valorData = carteira.RetornaValorMercado(idCarteira);
            }

            decimal valorHistorico = carteira.RetornaValorMercadoHistorico(idCarteira, dataAnterior);

            List<CalculoFinanceiro.CashFlow> listaCF = carteira.RetornaListaFluxoCaixa(idCarteira, dataAnterior, data, valorHistorico, valorData);

            decimal cotaDia = 0;
            decimal cotaAnterior = 0;
            HistoricoCota historicoCota = new HistoricoCota();
            historicoCota.Query.Select(historicoCota.Query.CotaFechamento);
            historicoCota.Query.Where(historicoCota.Query.IdCarteira.Equal(idCarteira),
                                      historicoCota.Query.Data.Equal(dataAnterior),
                                      historicoCota.Query.CotaFechamento.NotEqual(0),
                                      historicoCota.Query.CotaFechamento.IsNotNull());
            if (historicoCota.Query.Load())
            {
                cotaAnterior = historicoCota.CotaFechamento.Value;
            }
            else
            {
                HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
                historicoCotaCollection.Query.Select(historicoCotaCollection.Query.CotaFechamento);
                historicoCotaCollection.Query.Where(historicoCotaCollection.Query.IdCarteira.Equal(idCarteira),
                                                    historicoCotaCollection.Query.Data.LessThan(dataAnterior),
                                                    historicoCotaCollection.Query.CotaFechamento.NotEqual(0),
                                                    historicoCotaCollection.Query.CotaFechamento.IsNotNull());
                historicoCotaCollection.Query.OrderBy(historicoCotaCollection.Query.Data.Descending);
                historicoCotaCollection.Query.Load();

                if (historicoCotaCollection.Count > 0)
                {
                    cotaAnterior = historicoCotaCollection[0].CotaFechamento.Value;
                }
                else
                {
                    cotaDia = 1;
                }
            }

            if (listaCF.Count > 0)
            {
                try
                {
                    List<double> lista = new List<double>();
                    lista.Add((double)listaCF[0].valor);
                    lista.Add((double)listaCF[1].valor);
                    double[] cashFlows = lista.ToArray();

                    double tirDiaria = CalculoFinanceiro.IrrNewtonRaphson.Calculate(cashFlows);

                    cotaDia = cotaAnterior * ((decimal)tirDiaria + 1);
                }
                catch (Exception e)
                {
                }
            }

            HistoricoCota h = new HistoricoCota();
            if (h.LoadByPrimaryKey(data, idCarteira))
            {
                h.CotaFechamento = cotaDia;
            }
            else
            {
                h = new HistoricoCota();
                h.CotaAbertura = 0;
                h.CotaBruta = 0;
                h.CotaFechamento = cotaDia;
                h.Data = data;
                h.IdCarteira = idCarteira;
                h.PatrimonioBruto = 0;
                h.PLAbertura = 0;
                h.PLFechamento = 0;
                h.QuantidadeFechamento = 0;
            }

            h.Save();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        public void CalculaPrazoMedioFundo(DateTime data, int idCliente)
        {
            PrazoMedioCollection prazoMedioColl = new PrazoMedioCollection();
            prazoMedioColl.DeletaPrazoMedioMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoFundos);
            prazoMedioColl.DeletaPrazoMedioMaiorIgual(idCliente, data, TipoAtivoAuxiliar.Fundo);

            int prazo = 0;
            decimal valorBruto = 0;
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);
            string calculaPrazoMedio = string.IsNullOrEmpty(carteira.CalculaPrazoMedio) ? string.Empty : carteira.CalculaPrazoMedio;

            if (!calculaPrazoMedio.Equals("S"))
                return;

            #region Operações contra Fundo
            //Lista de Categoria que não fazem parte do PrazoMédio
            List<int> lstCategoria = new List<int>();
            lstCategoria.Add(10); //	Clube de Investimento
            lstCategoria.Add(345);//	FIDC Fomento Mercantil
            lstCategoria.Add(346);//	FIDC Financeiro
            lstCategoria.Add(347);//	FIDC Agro Indústria e Comércio
            lstCategoria.Add(348);//	FIDC Outros
            lstCategoria.Add(258);//	Fundos de Investimento Imobiliário
            lstCategoria.Add(324);//	Fundos de Participações

            CarteiraQuery carteiraQuery_CP = new CarteiraQuery("carteiraContraParte");
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("posicaoFundo");
            PosicaoFundoCollection posicaoFundoColl = new PosicaoFundoCollection();

            posicaoFundoQuery.Select(posicaoFundoQuery,
                                     carteiraQuery_CP);
            posicaoFundoQuery.InnerJoin(carteiraQuery_CP).On(posicaoFundoQuery.IdCarteira.Equal(carteiraQuery_CP.IdCarteira));
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente),
                                    carteiraQuery_CP.IdCategoria.NotIn(lstCategoria.ToArray()));
            posicaoFundoQuery.OrderBy(posicaoFundoQuery.IdCliente.Ascending);

            //TipoTributacao
            int CurtoPrazo = 1;
            int LongoPrazo = 2;
            int CP_Sem_ComeCotas = 4;
            int LP_Sem_ComeCotas = 5;

            if (posicaoFundoColl.Load(posicaoFundoQuery))
            {
                foreach (PosicaoFundo posicaoFundo in posicaoFundoColl)
                {
                    int tipoTributacao = Convert.ToInt32(posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.TipoTributacao).ToString());
                    string nomeFundo = posicaoFundo.GetColumn(CarteiraMetadata.ColumnNames.Nome).ToString();
                    if (tipoTributacao == CurtoPrazo || tipoTributacao == CP_Sem_ComeCotas)
                        prazo = 1;
                    else if (tipoTributacao == LongoPrazo || tipoTributacao == LP_Sem_ComeCotas)
                        prazo = 366;
                    else
                        continue;

                    valorBruto = posicaoFundo.ValorBruto.Value;

                    PrazoMedio prazoMedio = new PrazoMedio();
                    prazoMedio.InserePrazoMedio(data, idCliente, "Aplicação no Fundo '" + nomeFundo + "' -> ID.Posição: " + posicaoFundo.IdPosicao.Value, (int)TipoAtivoAuxiliar.OperacaoFundos, valorBruto, prazo, posicaoFundo.IdPosicao.Value);
                }
            }
            #endregion

            #region Prazo médio dos ativos
            PrazoMedioCollection prazoMedioCollAtivos = new PrazoMedioCollection();
            prazoMedioCollAtivos.Query.Where(prazoMedioCollAtivos.Query.IdCliente.Equal(idCliente) &
                                       prazoMedioCollAtivos.Query.DataAtual.Equal(data) &
                                       prazoMedioCollAtivos.Query.MercadoAtivo.NotEqual((int)TipoAtivoAuxiliar.Fundo));

            if (prazoMedioCollAtivos.Query.Load())
            {
                List<int> lstPrazos = new List<int>();
                List<decimal> lstValores = new List<decimal>();

                foreach (PrazoMedio prazoMedioAux in prazoMedioCollAtivos)
                {
                    lstPrazos.Add(Convert.ToInt32(prazoMedioAux.PrazoMedio.Value));
                    lstValores.Add(prazoMedioAux.ValorPosicao.Value);
                }

                PrazoMedio prazoMedio = new PrazoMedio();
                prazoMedio.InserePrazoMedioPonderado(data, idCliente, "Prazo médio do Fundo '" + carteira.Apelido + "'", lstPrazos, lstValores, (int)TipoAtivoAuxiliar.Fundo, null);
            }
            #endregion

            prazoMedioColl.Save();
        }

        /// <summary>
        /// Calcula rentabilidade de Fundo
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <param name="enumTipoProcessamento"></param>
        public void CalculaRentabilidadeFundo(DateTime data, int idCliente)
        {
            #region Se carteira mãe não calcula rentabilidade ( precisa desenvolver )
            CarteiraMae carteiraMae = new CarteiraMae();
            if (carteiraMae.IsCarteiraMae(idCliente))
                return;
            #endregion

            #region EntitySpaces
            string tablePosicaoFechamento = "PosicaoFechamento";
            string tableRentabilidade = "Rentabilidade";
            string tablePosicaoAbertura = "PosicaoAbertura";
            string tablePosicaoHistorico = "PosicaoHistorico";
            string tableOperacaoFundoCredito = "OperacaoFundoCredito";
            string tableCarteira = "Carteira";
            string tableOperacaoFundoDebito = "OperacaoFundoDebito";
            string fieldValorFechamento = "ValorFechamento";
            string fieldValorAbertura = "ValorAbertura";
            string fieldQtdeFechamento = "QtdeFechamento";
            string fieldQtdeAbertura = "QtdeAbertura";
            string fieldDescricaoCarteira = "DescricaoCarteira";
            string fieldValorBrutoAbertura = "ValorBrutoAbertura";
            string fieldValorBrutoFechamento = "ValorBrutoFechamento";
            string fieldValorAplicacao = "ValorAplicacao";
            #endregion

            #region Objetos
            Dictionary<int, string> dicMoedasAtivo = new Dictionary<int, string>();
            Dictionary<string, decimal> dicCotacao = new Dictionary<string, decimal>();
            Dictionary<string, Rentabilidade.RentabilidadeConversao> dicRentConversao = new Dictionary<string, Rentabilidade.RentabilidadeConversao>();
            List<int> lstTipoOperacaoEntrada = new List<int>();
            List<int> lstTipoOperacaoSaida = new List<int>();
            List<int> lstTipoOperacaoRenda = new List<int>();
            List<OperacaoFundo> lstOperacoesDebito = new List<OperacaoFundo>();
            List<OperacaoFundo> lstOperacoesCredito = new List<OperacaoFundo>();
            List<OperacaoFundo> lstOperacoesRendas = new List<OperacaoFundo>();
            List<int> lstAtivoIncentivado = new List<int>();
            List<AgendaFundo> lstAgendaFundos = new List<AgendaFundo>();
            List<Rentabilidade> lstRentabilidadeAnterior = new List<Rentabilidade>();
            //Querys
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery(tablePosicaoFechamento);
            PosicaoFundoAberturaQuery posicaoFundoAberturaQuery = new PosicaoFundoAberturaQuery(tablePosicaoAbertura);
            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery(tablePosicaoHistorico);
            OperacaoFundoQuery operacaoFundoCreditoQuery = new OperacaoFundoQuery(tableOperacaoFundoCredito);
            OperacaoFundoQuery operacaoFundoDebitoQuery = new OperacaoFundoQuery(tableOperacaoFundoDebito);
            RentabilidadeQuery rentabilidadeQuery = new RentabilidadeQuery(tableRentabilidade);
            CarteiraQuery carteiraQuery = new CarteiraQuery(tableCarteira);

            //Collection
            PosicaoFundoCollection posicaoFundoColl = new PosicaoFundoCollection();
            RentabilidadeCollection rentabilidadeColl = new RentabilidadeCollection();
            OperacaoFundoCollection operacaoFundoDebitoColl = new OperacaoFundoCollection();
            OperacaoFundoCollection operacaoFundoCreditoColl = new OperacaoFundoCollection();
            AgendaFundoCollection agendaFundoColl= new AgendaFundoCollection();

            //Comuns        
            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            Cliente cliente = new Cliente();
            Carteira carteira = new Carteira();
            Moeda moeda = new Moeda();
            OperacaoFundo operacaoAux;
            Rentabilidade rentabilidadeAnt;

            cliente.LoadByPrimaryKey(idCliente);
            int? tipoInvestidor = cliente.UpToPessoaByIdPessoa.Tipo;

            DateTime dataAnterior = new DateTime();
            if (cliente.IdLocal.Value != (short)LocalFeriadoFixo.Brasil)
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, cliente.IdLocal.Value, TipoFeriado.Outros);
            else
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            #endregion

            #region Variaveis
            decimal valorFechamento = 0;
            decimal valorAbertura = 0;
            decimal valorEntrada = 0;
            decimal valorSaidaLiquido = 0;
            decimal valorSaidaBruto = 0;
            decimal valorRendaLiquida = 0;
            decimal valorRendaBruta = 0;
            decimal valorRentabilidadeLiquida = 0;
            decimal valorRentabilidadeBruta = 0;
            decimal cambio = 0;
            decimal qtdeEntrada = 0;
            decimal qtdeSaida = 0;
            decimal qtdeAbertura = 0;
            decimal qtdeFechamento = 0;
            string descricaoCompleta = string.Empty;
            string descricaoMoedaCarteira = string.Empty;
            string descricaoMoedaAtivo = string.Empty;
            string keyConversao = string.Empty;
            int idCarteira = 0;
            short idMoedaAtivo = 0;
            short idMoedaCarteira = 0;
            byte tipoConversao;
            decimal cotaGerencialAnterior = 0;
            decimal rentabilidadeGrupoIncentivado = 0;
            decimal rentabilidadeGrupoTributado = 0;
            decimal divisorIncentivado = 0;
            decimal dividendoIncentivado = 0;
            decimal divisorTributado = 0;
            decimal dividendoTributado = 0;            
            decimal valorGrossUp = 0;
            decimal valorBrutoFechamento = 0;
            decimal valorBrutoAbertura = 0;
            decimal valorAplicacao = 0;
            decimal valorRentabilidadeBrutaAcumulada = 0;             
            decimal valorRentabilidadeBrutaDiaria = 0;
            decimal valorGrossUpAnterior = 0;
            decimal valorBrutoAnterior = 0;
            decimal valorRentabilidadeGrossUpAcumulada = 0;
            decimal valorRentabilidadeGrossUpDiaria = 0;
            decimal valorGrossUpFechamento = 0;
            decimal  valorBruto = 0;
            decimal valorGrossUpAbertura = 0;
            #endregion

            #region Deleta Rentabilidade
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoFundos);
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoFundosIncentivado);
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.Fundo);
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.RentAtivosIncentivados);
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.RentAtivoTributados);
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.RentBrutaTotal);
            #endregion

            #region Popula Listas
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoFundo.Aplicacao);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoFundo.AplicacaoAcoesEspecial);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoFundo.AplicacaoCotasEspecial);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoFundo.IngressoAtivoImpactoCota);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoFundo.IngressoAtivoImpactoQtde);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoFundo.AplicacaoCotas);

            lstTipoOperacaoSaida.Add((int)TipoOperacaoFundo.ResgateBruto);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoFundo.ResgateCotas);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoFundo.ResgateCotasEspecial);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoFundo.ResgateLiquido);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoFundo.ResgateTotal);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoFundo.RetiradaAtivoImpactoCota);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoFundo.RetiradaAtivoImpactoQtde);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoFundo.ComeCotas);

            lstTipoOperacaoRenda.Add((byte)TipoOperacaoFundo.Amortizacao);
            lstTipoOperacaoRenda.Add((byte)TipoOperacaoFundo.Juros);
            lstTipoOperacaoRenda.Add((byte)TipoOperacaoFundo.AmortizacaoJuros);
            lstTipoOperacaoRenda.Add((byte)TipoOperacaoFundo.Dividendo);

            lstAtivoIncentivado.Add((int)TipoAtivoAuxiliar.RendaFixaIncentivado);
            lstAtivoIncentivado.Add((int)TipoAtivoAuxiliar.OperacaoFundosIncentivado);
            #endregion

            #region Carrega as Posições
            posicaoFundoQuery.Select(posicaoFundoQuery.IdCliente,
                                    posicaoFundoQuery.IdCarteira,
                                    carteiraQuery.Nome.As(fieldDescricaoCarteira),
                                    posicaoFundoQuery.ValorLiquido.Sum().As(fieldValorFechamento),
                                    posicaoFundoHistoricoQuery.ValorLiquido.Sum().Coalesce("0").As(fieldValorAbertura),
                                    posicaoFundoQuery.Quantidade.Sum().As(fieldQtdeFechamento),
                                    posicaoFundoHistoricoQuery.Quantidade.Sum().Coalesce("0").As(fieldQtdeAbertura),
                                    posicaoFundoQuery.ValorBruto.Sum().As(fieldValorBrutoFechamento),
                                    posicaoFundoHistoricoQuery.ValorBruto.Sum().As(fieldValorBrutoAbertura),
                                    posicaoFundoQuery.ValorAplicacao.Sum().As(fieldValorAplicacao));
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira.Equal(posicaoFundoQuery.IdCarteira));
            posicaoFundoQuery.LeftJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdPosicao.Equal(posicaoFundoQuery.IdPosicao)
                                                                      & posicaoFundoHistoricoQuery.DataHistorico.Equal("'" + dataAnterior.ToString("yyyyMMdd") + "'"));
            posicaoFundoQuery.Where(posicaoFundoQuery.IdCliente.Equal(idCliente));
            posicaoFundoQuery.GroupBy(posicaoFundoQuery.IdCliente,
                                      posicaoFundoQuery.IdCarteira,
                                      carteiraQuery.Nome);
            posicaoFundoQuery.OrderBy(posicaoFundoQuery.IdCliente.Ascending,
                                      posicaoFundoQuery.IdCarteira.Ascending);
            #endregion

            #region Carrega Moeda e Carteira
            cliente.LoadByPrimaryKey(idCliente);

            if (moeda.LoadByPrimaryKey(cliente.IdMoeda.Value))
            {
                descricaoMoedaCarteira = moeda.Nome;
                idMoedaCarteira = moeda.IdMoeda.Value;
            }
            #endregion

            #region Carrega Rentabilidade do dia anterior
            RentabilidadeCollection rentabilidadeAntCollection = new RentabilidadeCollection();
            rentabilidadeAntCollection.Query.Where(rentabilidadeAntCollection.Query.IdCliente.Equal(idCliente) &
                                                   rentabilidadeAntCollection.Query.TipoAtivo.In((int)TipoAtivoAuxiliar.OperacaoFundos) &
                                                   rentabilidadeAntCollection.Query.Data.Equal(dataAnterior));

            if (rentabilidadeAntCollection.Query.Load())
                lstRentabilidadeAnterior = (List<Rentabilidade>)rentabilidadeAntCollection;
            #endregion

            #region Operações de Fundos
            if (posicaoFundoColl.Load(posicaoFundoQuery))
            {
                #region Carrega Rendas dos Ativos
                OperacaoFundoCollection operacaoFundoRendasCollection = new OperacaoFundoCollection();
                operacaoFundoRendasCollection.Query.Select(operacaoFundoRendasCollection.Query.ValorBruto.Sum(),
                                                           operacaoFundoRendasCollection.Query.ValorLiquido.Sum(),
                                                           operacaoFundoRendasCollection.Query.IdCarteira);
                operacaoFundoRendasCollection.Query.Where(operacaoFundoRendasCollection.Query.IdCliente.Equal(idCliente),
                                                          operacaoFundoRendasCollection.Query.DataOperacao.Equal(data),
                                                          operacaoFundoRendasCollection.Query.TipoOperacao.In(lstTipoOperacaoRenda.ToArray()));
                operacaoFundoRendasCollection.Query.GroupBy(operacaoFundoRendasCollection.Query.IdCarteira);

                if (operacaoFundoRendasCollection.Query.Load())
                    lstOperacoesRendas = (List<OperacaoFundo>)operacaoFundoRendasCollection;
                #endregion

                #region Carrega as Operações que movimentaram a posição na data de processamento (Crédito)
                operacaoFundoCreditoQuery.Select(operacaoFundoCreditoQuery.IdCliente,
                                                 operacaoFundoCreditoQuery.IdCarteira,
                                                 operacaoFundoCreditoQuery.ValorBruto.Sum(),
                                                 operacaoFundoCreditoQuery.Quantidade.Sum());
                operacaoFundoCreditoQuery.Where(operacaoFundoCreditoQuery.DataConversao.Equal(data)
                                                & operacaoFundoCreditoQuery.IdCliente.Equal(idCliente)
                                                & operacaoFundoCreditoQuery.TipoOperacao.In(lstTipoOperacaoEntrada.ToArray()));
                operacaoFundoCreditoQuery.GroupBy(operacaoFundoCreditoQuery.IdCliente,
                                                  operacaoFundoCreditoQuery.IdCarteira);
                operacaoFundoCreditoQuery.OrderBy(operacaoFundoCreditoQuery.IdCliente.Ascending,
                                                  operacaoFundoCreditoQuery.IdCarteira.Ascending);

                if (operacaoFundoCreditoColl.Load(operacaoFundoCreditoQuery))
                    lstOperacoesCredito = (List<OperacaoFundo>)operacaoFundoCreditoColl;
                #endregion

                #region Carrega as Operações que movimentaram a posição na data de processamento (Débito)
                operacaoFundoDebitoQuery.Select(operacaoFundoDebitoQuery.IdCliente,
                                                 operacaoFundoDebitoQuery.IdCarteira,
                                                 operacaoFundoDebitoQuery.ValorBruto.Sum(),
                                                 operacaoFundoDebitoQuery.Quantidade.Sum(),
                                                 operacaoFundoDebitoQuery.ValorLiquido.Sum());
                operacaoFundoDebitoQuery.Where(operacaoFundoDebitoQuery.DataConversao.Equal(data)
                                                & operacaoFundoDebitoQuery.IdCliente.Equal(idCliente)
                                                & operacaoFundoDebitoQuery.TipoOperacao.In(lstTipoOperacaoSaida.ToArray()));
                operacaoFundoDebitoQuery.GroupBy(operacaoFundoDebitoQuery.IdCliente,
                                                  operacaoFundoDebitoQuery.IdCarteira);
                operacaoFundoDebitoQuery.OrderBy(operacaoFundoDebitoQuery.IdCliente.Ascending,
                                                  operacaoFundoDebitoQuery.IdCarteira.Ascending);

                if (operacaoFundoDebitoColl.Load(operacaoFundoDebitoQuery))
                    lstOperacoesDebito = (List<OperacaoFundo>)operacaoFundoDebitoColl;
                #endregion

                foreach (PosicaoFundo posicaoFundo in posicaoFundoColl)
                {
                    idCarteira = posicaoFundo.IdCarteira.Value;
                    idMoedaAtivo = 1; //Real
                    valorFechamento = 0;
                    valorAbertura = 0;
                    valorEntrada = 0;
                    valorSaidaLiquido = 0;
                    valorRendaLiquida = 0;
                    valorRendaBruta = 0;
                    valorRentabilidadeLiquida = 0;
                    valorRentabilidadeBruta = 0;
                    qtdeEntrada = 0;
                    qtdeSaida = 0;
                    qtdeAbertura = 0;
                    qtdeFechamento = 0;
                    valorRentabilidadeGrossUpAcumulada = 0;
                    valorRentabilidadeGrossUpDiaria = 0;
                    valorGrossUpFechamento = 0;
                    valorBruto = 0;
                    valorGrossUpAbertura = 0;                    

                    #region Carrega moeda do Ativo
                    if (!dicMoedasAtivo.ContainsKey(idMoedaAtivo))
                    {
                        moeda = new Moeda();
                        moeda.LoadByPrimaryKey(idMoedaAtivo);
                        dicMoedasAtivo.Add(moeda.IdMoeda.Value, moeda.Nome);
                    }
                    descricaoMoedaAtivo = dicMoedasAtivo[idMoedaAtivo];
                    #endregion

                    #region Operações Entrada
                    operacaoAux = new OperacaoFundo();
                    operacaoAux = lstOperacoesCredito.Find(delegate(OperacaoFundo x) { return x.IdCarteira == idCarteira; });
                    if (operacaoAux != null && operacaoAux.IdCarteira.Value > 0)
                    {
                        valorEntrada = Convert.ToDecimal(operacaoAux.ValorBruto.Value);
                        qtdeEntrada = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                    }
                    else
                    {
                        valorEntrada = 0;
                        qtdeEntrada = 0;
                    }
                    #endregion

                    #region Operações Saida
                    operacaoAux = new OperacaoFundo();
                    operacaoAux = lstOperacoesDebito.Find(delegate(OperacaoFundo x) { return x.IdCarteira == idCarteira; });
                    if (operacaoAux != null && operacaoAux.IdCarteira.Value > 0)
                    {
                        valorSaidaLiquido = Convert.ToDecimal(operacaoAux.ValorLiquido.Value);
                        valorSaidaBruto = Convert.ToDecimal(operacaoAux.ValorBruto.Value);
                        qtdeSaida = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                    }
                    else
                    {
                        valorSaidaLiquido = 0;
                        valorSaidaBruto = 0;
                        qtdeSaida = 0;
                    }
                    #endregion

                    #region Rendas
                    operacaoAux = new OperacaoFundo();
                    operacaoAux = lstOperacoesRendas.Find(delegate(OperacaoFundo x) { return x.IdCarteira == idCarteira; });
                    if (operacaoAux != null && operacaoAux.IdCarteira.Value > 0)
                    {
                        valorRendaLiquida = Convert.ToDecimal(operacaoAux.ValorLiquido.Value);
                        valorRendaBruta = Convert.ToDecimal(operacaoAux.ValorBruto.Value);
                    }
                    else
                    {
                        valorRendaLiquida = 0;
                        valorRendaBruta = 0;
                    }
                    #endregion

                    int tipoAtivo = (int)TipoAtivoAuxiliar.OperacaoFundos;
                    if (carteira.AtivoFundoIsento(idCarteira, tipoInvestidor, data))
                        tipoAtivo = (int)TipoAtivoAuxiliar.OperacaoFundosIncentivado;

                    if (!Decimal.TryParse(posicaoFundo.GetColumn(fieldValorFechamento).ToString(), out valorFechamento))
                        valorFechamento = 0;

                    if (!Decimal.TryParse(posicaoFundo.GetColumn(fieldValorAbertura).ToString(), out valorAbertura))
                        valorAbertura = 0;

                    if (!Decimal.TryParse(posicaoFundo.GetColumn(fieldQtdeAbertura).ToString(), out qtdeAbertura))
                        qtdeAbertura = 0;

                    if (!Decimal.TryParse(posicaoFundo.GetColumn(fieldQtdeFechamento).ToString(), out qtdeFechamento))
                        qtdeFechamento = 0;

                    if (!Decimal.TryParse(posicaoFundo.GetColumn(fieldValorBrutoFechamento).ToString(), out valorBrutoFechamento))
                        valorBrutoFechamento = 0;

                    if (!Decimal.TryParse(posicaoFundo.GetColumn(fieldValorBrutoAbertura).ToString(), out valorBrutoAbertura))
                        valorBrutoAbertura = 0;

                    if (!Decimal.TryParse(posicaoFundo.GetColumn(fieldValorAplicacao).ToString(), out valorAplicacao))
                        valorAplicacao = 0;                 

                    #region Rentabilidade Liquida
                    try
                    {
                        valorRentabilidadeLiquida = ((valorFechamento + valorSaidaLiquido) / (valorAbertura + valorEntrada - valorRendaLiquida)) - 1;
                    }
                    catch (Exception ex)
                    {
                        valorRentabilidadeLiquida = 0;
                    }
                    #endregion

                    #region Rentabilidade Bruta

                    #region Rentabilidade com GrossUp
                    if (tipoAtivo == (int)TipoAtivoAuxiliar.OperacaoFundosIncentivado)
                    {
                        valorGrossUpFechamento = valorBrutoFechamento;

                        //Valor Bruto é igual ao valor liquido, quando o título é grossup
                        valorBrutoFechamento = valorFechamento;
                        valorBrutoAbertura = valorAbertura;

                        if (qtdeFechamento == 0)
                        {
                            valorGrossUpFechamento = qtdeFechamento == 0 ? 0 : valorGrossUpFechamento;                            
                        }

                        //Diária GrossUP
                        try
                        {
                            valorRentabilidadeGrossUpDiaria = valorRentabilidadeLiquida / (decimal)0.85;
                        }
                        catch (Exception ex)
                        {
                            valorRentabilidadeGrossUpDiaria = 0;
                        }

                    }
                    #endregion

                    #region Rentabilidade sem GrossUp
                    //Rentabilidade Diaria
                    try
                    {
                        valorRentabilidadeBrutaDiaria = ((valorBrutoFechamento + valorSaidaBruto) / (valorBrutoAbertura + valorEntrada - valorRendaBruta)) - 1;
                    }
                    catch (Exception ex)
                    {
                        valorRentabilidadeBrutaDiaria = 0;
                    }
                    #endregion

                    #endregion

                    #region Rentabilidade Acumulada
                    valorRentabilidadeBrutaAcumulada = valorRentabilidadeBrutaDiaria;
                    Rentabilidade rentabilidadeAux = new Rentabilidade();
                    rentabilidadeAux = lstRentabilidadeAnterior.Find(delegate(Rentabilidade x) { return x.IdCarteira == posicaoFundo.IdCarteira.Value; });
                    if (rentabilidadeAux != null && rentabilidadeAux.IdCarteira.Value > 0)
                    {
                        if (rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.GetValueOrDefault(0) > 0)
                            valorRentabilidadeBrutaAcumulada = ((1 + rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.Value) / 100) * ((1 + valorRentabilidadeBrutaDiaria) / 100);

                        if (rentabilidadeAux.RentabilidadeGrossUpAcumMoedaAtivo.GetValueOrDefault(0) > 0)
                            valorRentabilidadeGrossUpAcumulada = ((1 + rentabilidadeAux.RentabilidadeGrossUpAcumMoedaAtivo.Value) / 100) * ((1 + valorRentabilidadeGrossUpDiaria) / 100);
                    }
                    #endregion

                    descricaoCompleta = "Aplicação no Fundo '" + posicaoFundo.GetColumn(fieldDescricaoCarteira) + "'";

                    #region Popula objeto
                    Rentabilidade rentabilidade = rentabilidadeColl.AddNew();
                    rentabilidade.CodigoAtivo = descricaoCompleta;
                    rentabilidade.TipoAtivo = tipoAtivo;
                    rentabilidade.Data = data;
                    rentabilidade.IdCliente = idCliente;
                    rentabilidade.IdCarteira = posicaoFundo.IdCarteira.Value;
                    rentabilidade.QuantidadeFinalAtivo = qtdeFechamento;
                    rentabilidade.QuantidadeInicialAtivo = qtdeAbertura;
                    rentabilidade.QuantidadeTotalEntradaAtivo = qtdeEntrada;
                    rentabilidade.QuantidadeTotalSaidaAtivo = qtdeSaida;

                    //Ativo
                    rentabilidade.CodigoMoedaAtivo = descricaoMoedaAtivo;
                    rentabilidade.PatrimonioFinalMoedaPortfolio = rentabilidade.PatrimonioFinalMoedaAtivo = valorFechamento;
                    rentabilidade.PatrimonioInicialMoedaPortfolio = rentabilidade.PatrimonioInicialMoedaAtivo = valorAbertura;
                    rentabilidade.RentabilidadeMoedaPortfolio = rentabilidade.RentabilidadeMoedaAtivo = valorRentabilidadeLiquida;
                    rentabilidade.ValorFinanceiroEntradaMoedaPortfolio = rentabilidade.ValorFinanceiroEntradaMoedaAtivo = valorEntrada;
                    rentabilidade.ValorFinanceiroSaidaMoedaPortfolio = rentabilidade.ValorFinanceiroSaidaMoedaAtivo = valorSaidaLiquido;
                    rentabilidade.ValorFinanceiroRendasMoedaPortfolio = rentabilidade.ValorFinanceiroRendasMoedaAtivo = valorRendaLiquida;
                    rentabilidade.RentabilidadeBrutaDiariaMoedaAtivo = rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio = valorRentabilidadeBrutaDiaria;
                    rentabilidade.RentabilidadeBrutaAcumMoedaAtivo = rentabilidade.RentabilidadeBrutaAcumMoedaPortfolio = valorRentabilidadeBrutaAcumulada;
                    rentabilidade.RentabilidadeGrossUpDiariaMoedaAtivo = rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio = valorRentabilidadeGrossUpDiaria;
                    rentabilidade.RentabilidadeGrossUpAcumMoedaAtivo = rentabilidade.RentabilidadeGrossUpAcumMoedaPortfolio = valorRentabilidadeGrossUpAcumulada;
                    rentabilidade.RentabilidadeGrossUpAcumMoedaAtivo = rentabilidade.RentabilidadeGrossUpAcumMoedaPortfolio = 0;
                    rentabilidade.PatrimonioFinalBrutoMoedaAtivo = rentabilidade.PatrimonioFinalBrutoMoedaPortfolio = valorBrutoFechamento;
                    rentabilidade.PatrimonioInicialBrutoMoedaPortfolio = rentabilidade.PatrimonioInicialBrutoMoedaAtivo = valorBrutoAbertura;

                    //Soma o patrimonio bruto de um título, mesmo que não seja grossup
                    rentabilidade.PatrimonioFinalGrossUpMoedaAtivo = rentabilidade.PatrimonioFinalGrossUpMoedaPortfolio = (valorGrossUpFechamento == 0 ? valorBrutoFechamento : valorGrossUpFechamento);
                    rentabilidade.PatrimonioInicialGrossUpMoedaPortfolio = rentabilidade.PatrimonioInicialGrossUpMoedaAtivo = (valorGrossUpAbertura == 0 ? valorBrutoAbertura : valorGrossUpAbertura);

                    //Portfolio
                    rentabilidade.CodigoMoedaPortfolio = descricaoMoedaCarteira;

                    rentabilidade.ConverteValores(idMoedaAtivo, idMoedaCarteira, ref dicRentConversao);
                    #endregion

                }

                rentabilidadeColl.Save();

            }
            #endregion

            #region Rentabilidade da Carteira
            rentabilidadeColl = new RentabilidadeCollection();            

            rentabilidadeQuery.Select(rentabilidadeQuery.TipoAtivo,
                                      rentabilidadeQuery.PatrimonioInicialMoedaPortfolio,
                                      rentabilidadeQuery.PatrimonioFinalMoedaPortfolio,
                                      rentabilidadeQuery.ValorFinanceiroEntradaMoedaPortfolio,
                                      rentabilidadeQuery.ValorFinanceiroSaidaMoedaPortfolio,
                                      rentabilidadeQuery.ValorFinanceiroRendasMoedaPortfolio,
                                      rentabilidadeQuery.PatrimonioFinalGrossUpMoedaPortfolio.Coalesce("0"),
                                      rentabilidadeQuery.PatrimonioFinalBrutoMoedaPortfolio.Coalesce("0"),
                                      rentabilidadeQuery.PatrimonioInicialBrutoMoedaPortfolio.Coalesce("0"),
                                      rentabilidadeQuery.PatrimonioInicialGrossUpMoedaPortfolio.Coalesce("0"),
                                      rentabilidadeQuery.RentabilidadeBrutaDiariaMoedaPortfolio.Coalesce("0"),
                                      rentabilidadeQuery.RentabilidadeGrossUpDiariaMoedaPortfolio.Coalesce("0"));
            rentabilidadeQuery.Where(rentabilidadeQuery.IdCliente.Equal(idCliente)
                                    & rentabilidadeQuery.Data.Equal(data));

            if (rentabilidadeColl.Load(rentabilidadeQuery))
            {
                valorGrossUpAnterior = 0;
                valorBrutoAnterior = 0;
                valorBrutoFechamento = 0;
                valorRentabilidadeBruta = 0;
                rentabilidadeGrupoIncentivado = 0;
                rentabilidadeGrupoTributado = 0;
                divisorIncentivado = 0;
                dividendoIncentivado = 0;
                divisorTributado = 0;
                dividendoTributado = 0;
                valorFechamento = 0;
                valorSaidaLiquido = 0;
                valorAbertura = 0;
                valorEntrada = 0;
                valorRendaLiquida = 0;
                valorGrossUp = 0;
                valorBrutoFechamento = 0;
                cotaGerencialAnterior = 0;

                rentabilidadeAnt = new Rentabilidade();
                rentabilidadeAnt.Query.Select(rentabilidadeAnt.Query.PatrimonioFinalBrutoMoedaPortfolio.Coalesce("0"),
                                              rentabilidadeAnt.Query.PatrimonioFinalGrossUpMoedaPortfolio.Coalesce("0"),
                                              rentabilidadeAnt.Query.CotaGerencial.Coalesce("0"));
                rentabilidadeAnt.Query.Where(rentabilidadeAnt.Query.IdCliente.Equal(idCliente) &
                                             rentabilidadeAnt.Query.TipoAtivo.Equal((int)TipoAtivoAuxiliar.Fundo) &
                                             rentabilidadeAnt.Query.Data.Equal(dataAnterior));

                if(rentabilidadeAnt.Query.Load())
                {
                    valorBrutoAnterior = rentabilidadeAnt.PatrimonioFinalBrutoMoedaPortfolio.Value;
                    valorGrossUpAnterior = rentabilidadeAnt.PatrimonioFinalGrossUpMoedaPortfolio.Value;
                    cotaGerencialAnterior = rentabilidadeAnt.CotaGerencial.Value;
                }

                //Rentabilidade Bruta
                decimal rentabilidadeBrutaAtivo = 100;
                decimal valorInicialBrutoAtivo = 0;

                foreach (Rentabilidade rentabilidade in rentabilidadeColl)
                {
                    //Liquido
                    valorFechamento += rentabilidade.PatrimonioFinalMoedaPortfolio.Value;
                    valorSaidaLiquido += rentabilidade.ValorFinanceiroSaidaMoedaPortfolio.Value;
                    valorAbertura += rentabilidade.PatrimonioInicialMoedaPortfolio.Value;
                    valorEntrada += rentabilidade.ValorFinanceiroEntradaMoedaPortfolio.Value;
                    valorRendaLiquida += rentabilidade.ValorFinanceiroRendasMoedaPortfolio.Value;

                    //Bruto
                    valorGrossUp += rentabilidade.PatrimonioFinalGrossUpMoedaPortfolio.GetValueOrDefault(0);
                    valorBrutoFechamento += rentabilidade.PatrimonioFinalBrutoMoedaPortfolio.GetValueOrDefault(0);

                    if (lstAtivoIncentivado.Contains(rentabilidade.TipoAtivo.Value))
                    {
                        valorInicialBrutoAtivo += rentabilidade.PatrimonioInicialGrossUpMoedaPortfolio.Value;
                        rentabilidadeBrutaAtivo *= rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio.Value;
                    }
                    else
                    {
                        valorInicialBrutoAtivo += rentabilidade.PatrimonioInicialBrutoMoedaPortfolio.Value;
                        rentabilidadeBrutaAtivo *= rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio.Value;
                    }

                    if (valorInicialBrutoAtivo != 0)
                    {
                        if (valorGrossUpAnterior != 0)
                            valorRentabilidadeBruta += rentabilidadeBrutaAtivo * (valorInicialBrutoAtivo / valorGrossUpAnterior);
                        else if (valorBrutoAnterior != 0)
                            valorRentabilidadeBruta += rentabilidadeBrutaAtivo * (valorInicialBrutoAtivo / valorBrutoAnterior);
                    }

                    if (lstAtivoIncentivado.Contains(rentabilidade.TipoAtivo.Value) && valorGrossUp != 0)
                    {
                        decimal patrimonio = rentabilidade.PatrimonioFinalGrossUpMoedaPortfolio.Value;
                        divisorTributado += patrimonio;
                        dividendoTributado += patrimonio * rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio.Value;
                    }
                    else //Tributado
                    {
                        decimal patrimonio = rentabilidade.PatrimonioFinalBrutoMoedaPortfolio.Value;
                        divisorIncentivado += patrimonio;
                        dividendoIncentivado += patrimonio * rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio.Value;
                    }
                }
                
                #region Rentabilidade dos grupos incentivados e tributados
                if (dividendoTributado > 0)
                {
                    rentabilidadeGrupoTributado = dividendoTributado / divisorTributado;
                }

                if (dividendoIncentivado > 0)
                {
                    rentabilidadeGrupoIncentivado = dividendoIncentivado / divisorIncentivado;
                }

                #endregion

                #region Cota Gerencial
                decimal cotaGerencial = cotaGerencialAnterior == 0 ? 100 : cotaGerencialAnterior;
                if (valorRentabilidadeBruta != 0)
                {
                    cotaGerencial = cotaGerencialAnterior * (1 + valorRentabilidadeBruta / 100);
                }
                #endregion

                #region Rentabilidade Liquida
                try
                {
                    valorRentabilidadeLiquida = ((valorFechamento + valorSaidaLiquido) / (valorAbertura + valorEntrada - valorRendaLiquida)) - 1;
                }
                catch (Exception ex)
                {
                    valorRentabilidadeLiquida = 0;
                }
                #endregion

                Rentabilidade rentabilidadeAux = new Rentabilidade();
                rentabilidadeAux.PatrimonioFinalMoedaPortfolio = valorFechamento;
                rentabilidadeAux.ValorFinanceiroSaidaMoedaPortfolio = valorSaidaLiquido;
                rentabilidadeAux.PatrimonioInicialMoedaPortfolio = valorAbertura;
                rentabilidadeAux.ValorFinanceiroEntradaMoedaPortfolio = valorEntrada;
                rentabilidadeAux.ValorFinanceiroRendasMoedaPortfolio = valorRendaLiquida;
                rentabilidadeAux.RentabilidadeMoedaPortfolio = valorRentabilidadeLiquida;
                rentabilidadeAux.CodigoMoedaPortfolio = descricaoMoedaCarteira;
                rentabilidadeAux.CotaGerencial = cotaGerencial;
                rentabilidadeAux.PatrimonioFinalGrossUpMoedaPortfolio = valorGrossUp;
                rentabilidadeAux.PatrimonioFinalBrutoMoedaPortfolio = valorBrutoFechamento;
                rentabilidadeAux.RentabilidadeBrutaDiariaMoedaPortfolio = valorRentabilidadeBruta / 100;
                rentabilidadeAux.RentabilidadeGrossUpDiariaMoedaAtivo = valorRentabilidadeBruta / 100;
                rentabilidadeAux.RentabilidadeAtivosTributados = rentabilidadeGrupoTributado;
                rentabilidadeAux.RentabilidadeAtivosIcentivados = rentabilidadeGrupoIncentivado;
                rentabilidadeAux.Data = data;
                rentabilidadeAux.IdCliente = idCliente;
                rentabilidadeAux.TipoAtivo = (int)TipoAtivoAuxiliar.Fundo;
                rentabilidadeAux.CodigoAtivo = string.Empty;
                rentabilidadeAux.CodigoMoedaAtivo = string.Empty;

                rentabilidadeAux.Save();

            }
            #endregion
        }
		
        #region Colagem Cotista
        private void CriaBackupCotista(int idCarteira, DateTime data)
        {
            #region Declara Objetos
            OperacaoCotistaAuxCollection operacaoCotistaAuxColl = new OperacaoCotistaAuxCollection();
            DetalheResgateCotistaAuxCollection detalheResgateAuxColl = new DetalheResgateCotistaAuxCollection();

            OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
            DetalheResgateCotistaCollection detalheResgateColl = new DetalheResgateCotistaCollection();
            OperacaoCotistaQuery operacaoCotistaQuery;
            DetalheResgateCotistaQuery detalheResgateQuery;

            List<DetalheResgateCotista> lstDetalheResgateCotista = new List<DetalheResgateCotista>();
            List<int> lstTipoOperacao = ColagemResgate.RetornaTipoOperacaoCotista();

            PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
            PosicaoCotistaAuxCollection posicaoCotistaAuxCollection = new PosicaoCotistaAuxCollection();
            #endregion

            #region ComeCotas
            ColagemComeCotas colagemComeCotas = new ColagemComeCotas();

            if (colagemComeCotas.VerificaSeExisteValoresColadosComeCotasPassivo(idCarteira, data))
            {
                #region Deleta Posições da Tabela PosicaoAux
                posicaoCotistaAuxCollection.Query.Where(posicaoCotistaAuxCollection.Query.IdCarteira.Equal(idCarteira)
                                                      & posicaoCotistaAuxCollection.Query.DataReferencia.Equal(data));
                if (posicaoCotistaAuxCollection.Query.Load())
                {
                    posicaoCotistaAuxCollection.MarkAllAsDeleted();
                    posicaoCotistaAuxCollection.Save();
                }
                #endregion

                #region Carrega Posicoes do Cotista
                posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira));
                posicaoCotistaCollection.Query.Load();
                #endregion

                #region Percorre posições
                posicaoCotistaAuxCollection = new PosicaoCotistaAuxCollection();
                foreach (PosicaoCotista posicaoCotista in posicaoCotistaCollection)
                {
                    PosicaoCotistaAux posicaoCotistaAux = posicaoCotistaAuxCollection.AddNew();
                    foreach (esColumnMetadata colPosicao in posicaoCotistaAux.es.Meta.Columns)
                    {
                        if (colPosicao.Name.ToUpper().Trim().Equals("DATAREFERENCIA"))
                            posicaoCotistaAux.DataReferencia = data;
                        else if (posicaoCotistaAux.es.Meta.Columns.FindByColumnName(colPosicao.Name) != null && posicaoCotista.es.Meta.Columns.FindByColumnName(colPosicao.Name) != null)
                            posicaoCotistaAux.SetColumn(colPosicao.Name, posicaoCotista.GetColumn(colPosicao.Name));
                    }
                }

                posicaoCotistaAuxCollection.Save();
                #endregion
            }
            #endregion

            #region Resgate

            #region Deleta Operações da Tabela Aux
            operacaoCotistaAuxColl.Query.Where(operacaoCotistaAuxColl.Query.IdCarteira.Equal(idCarteira)
                                          & operacaoCotistaAuxColl.Query.DataConversao.Equal(data)
                                          & operacaoCotistaAuxColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

            if (operacaoCotistaAuxColl.Query.Load())
            {
                operacaoCotistaAuxColl.MarkAllAsDeleted();
                operacaoCotistaAuxColl.Save();
            }
            #endregion

            ColagemResgate colagemResgate = new ColagemResgate();
            if (colagemResgate.VerificaSeExisteValoresColadosRegPassivo(idCarteira, data))
            {
                #region Deleta Detalhes das Tabelas Aux
                detalheResgateAuxColl.Query.Where(detalheResgateAuxColl.Query.IdCarteira.Equal(idCarteira)
                                                  & detalheResgateAuxColl.Query.DataOperacao.Equal(data)
                                                  & detalheResgateAuxColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

                if (detalheResgateAuxColl.Query.Load())
                {
                    detalheResgateAuxColl.MarkAllAsDeleted();
                    detalheResgateAuxColl.Save();
                }
                #endregion



                #region Carrega Operações de Cotista
                operacaoCotistaColl = new OperacaoCotistaCollection();
                operacaoCotistaColl.Query.Where(operacaoCotistaColl.Query.IdCarteira.Equal(idCarteira)
                                              & operacaoCotistaColl.Query.DataConversao.Equal(data)
                                              & operacaoCotistaColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

                operacaoCotistaColl.Query.Load();
                #endregion

                #region Carrega Detalhes
                detalheResgateQuery = new DetalheResgateCotistaQuery("detalheResgate");
                operacaoCotistaQuery = new OperacaoCotistaQuery("operacaoCotista");
                detalheResgateQuery.InnerJoin(operacaoCotistaQuery).On(operacaoCotistaQuery.IdOperacao.Equal(detalheResgateQuery.IdOperacao));
                detalheResgateQuery.Where(operacaoCotistaQuery.IdCarteira.Equal(idCarteira)
                                              & operacaoCotistaQuery.DataConversao.Equal(data)
                                              & operacaoCotistaQuery.TipoOperacao.In(lstTipoOperacao.ToArray()));

                if (detalheResgateColl.Load(detalheResgateQuery))
                    lstDetalheResgateCotista = (List<DetalheResgateCotista>)detalheResgateColl;
                #endregion

                if (operacaoCotistaColl.Count == 0)
                    return;

                foreach (OperacaoCotista operacaoCotista in operacaoCotistaColl)
                {
                    #region Operação Cotista
                    OperacaoCotistaAux operacaoCotistaAux = operacaoCotistaAuxColl.AddNew();
                    foreach (esColumnMetadata colOperacao in operacaoCotista.es.Meta.Columns)
                    {
                        if (operacaoCotistaAux.es.Meta.Columns.FindByColumnName(colOperacao.Name) != null && operacaoCotista.es.Meta.Columns.FindByColumnName(colOperacao.Name) != null)
                            operacaoCotistaAux.SetColumn(colOperacao.Name, operacaoCotista.GetColumn(colOperacao.Name));
                    }
                    #endregion

                    #region Detalhe Resgate
                    foreach (DetalheResgateCotista detalheCotista in lstDetalheResgateCotista.FindAll(delegate(DetalheResgateCotista x) { return x.IdOperacao == operacaoCotista.IdOperacao.Value; }))
                    {
                        DetalheResgateCotistaAux detalheCotistaAux = detalheResgateAuxColl.AddNew();
                        foreach (esColumnMetadata colDetalhe in detalheCotistaAux.es.Meta.Columns)
                        {
                            if (colDetalhe.Name.ToUpper().Trim().Equals("TIPOOPERACAO"))
                            {
                                detalheCotistaAux.TipoOperacao = operacaoCotista.TipoOperacao.Value;
                            }
                            else if (colDetalhe.Name.ToUpper().Trim().Equals("DATAOPERACAO"))
                            {
                                detalheCotistaAux.DataOperacao = operacaoCotista.DataConversao.Value;
                            }
                            else if (detalheCotistaAux.es.Meta.Columns.FindByColumnName(colDetalhe.Name) != null && detalheCotista.es.Meta.Columns.FindByColumnName(colDetalhe.Name) != null)
                            {
                                detalheCotistaAux.SetColumn(colDetalhe.Name, detalheCotista.GetColumn(colDetalhe.Name));
                            }
                        }
                    }
                    #endregion
                }

                operacaoCotistaAuxColl.Save();
                detalheResgateAuxColl.Save();
            }
            #endregion
        }

        /// <summary>
        /// Restaura operações coladas e deleta o bkp - Cotista
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void RestauraBackupCotista(int idCarteira, DateTime data)
        {
            #region Declara objetos
            OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
            OperacaoCotistaAuxCollection operacaoCotistaAuxColl = new OperacaoCotistaAuxCollection();
            List<OperacaoCotistaAux> lstOperacaoCotistaAux = new List<OperacaoCotistaAux>();
            List<int> lstTipoOperacao = ColagemResgate.RetornaTipoOperacaoCotista();
            #endregion

            ColagemOperResgPosCotistaCollection colagemOperResgPosAfetadaColl = new ColagemOperResgPosCotistaCollection();
            colagemOperResgPosAfetadaColl.DeletaColagemOperResgPosAfetada(idCarteira, data);

            ColagemResgateDetalhePosicaoCollection colagemResgateDetalhePosicaoColl = new ColagemResgateDetalhePosicaoCollection();
            colagemResgateDetalhePosicaoColl.DeletaColagemResgateDetalhePosicaoCotista(idCarteira, data);

            #region Deleta Operações de ComeCotas - Operacao
            operacaoCotistaColl.Query.Where(operacaoCotistaColl.Query.IdCarteira.Equal(idCarteira)
                                          & operacaoCotistaColl.Query.DataOperacao.Equal(data)
                                          & operacaoCotistaColl.Query.ValoresColados.Equal("S")
                                          & operacaoCotistaColl.Query.TipoOperacao.Equal((int)TipoOperacaoCotista.ComeCotas));

            if (operacaoCotistaColl.Query.Load())
            {
                operacaoCotistaColl.MarkAllAsDeleted();
                operacaoCotistaColl.Save();
            }
            #endregion

            #region Deleta ColagemComeCotasPosCotista
            ColagemComeCotasPosCotistaCollection colagemComeCotasPosCotistaColl = new ColagemComeCotasPosCotistaCollection();
            colagemComeCotasPosCotistaColl.Query.Where(colagemComeCotasPosCotistaColl.Query.IdCarteira.Equal(idCarteira)
                                                      & colagemComeCotasPosCotistaColl.Query.DataReferencia.Equal(data));

            if (colagemComeCotasPosCotistaColl.Query.Load())
            {
                colagemComeCotasPosCotistaColl.MarkAllAsDeleted();
                colagemComeCotasPosCotistaColl.Save();
            }
            #endregion

            #region Deleta Operações de ComeCotas - OperacaoAux
            operacaoCotistaAuxColl.Query.Where(operacaoCotistaAuxColl.Query.IdCarteira.Equal(idCarteira)
                                             & operacaoCotistaAuxColl.Query.DataOperacao.Equal(data)
                                             & operacaoCotistaAuxColl.Query.TipoOperacao.Equal((int)TipoOperacaoCotista.ComeCotas));

            if (operacaoCotistaAuxColl.Query.Load())
            {
                operacaoCotistaAuxColl.MarkAllAsDeleted();
                operacaoCotistaAuxColl.Save();
            }
            #endregion

            #region Deleta Operações coladas
            operacaoCotistaColl = new OperacaoCotistaCollection();
            operacaoCotistaColl.Query.Where(operacaoCotistaColl.Query.IdCarteira.Equal(idCarteira)
                                          & operacaoCotistaColl.Query.DataConversao.Equal(data)
                                          & operacaoCotistaColl.Query.ValoresColados.Equal("S")
                                          & operacaoCotistaColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

            if (operacaoCotistaColl.Query.Load())
            {
                operacaoCotistaColl.MarkAllAsDeleted();
                operacaoCotistaColl.Save();
            }
            #endregion

            #region Carrega Operações de BKP
            operacaoCotistaAuxColl = new OperacaoCotistaAuxCollection();
            operacaoCotistaAuxColl.Query.Where(operacaoCotistaAuxColl.Query.IdCarteira.Equal(idCarteira)
                                          & operacaoCotistaAuxColl.Query.DataOperacao.Equal(data)
                                          & operacaoCotistaAuxColl.Query.ValoresColados.Equal("N")
                                          & operacaoCotistaAuxColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

            if (!operacaoCotistaAuxColl.Query.Load())
            {
                return;
            }
            #endregion

            operacaoCotistaColl = new OperacaoCotistaCollection();

            #region Restaurando Backup
            foreach (OperacaoCotistaAux operacaoCotistaAux in operacaoCotistaAuxColl)
            {
                OperacaoCotista operacaoCotista = operacaoCotistaColl.AddNew();
                #region Operação Cotista
                foreach (esColumnMetadata colOperacao in operacaoCotistaAux.es.Meta.Columns)
                {
                    if (colOperacao.Name.ToUpper().Trim().Equals("IDOPERACAO")) //Não copia o ID, será usado o identity
                        continue;

                    if (operacaoCotista.es.Meta.Columns.FindByColumnName(colOperacao.Name) != null && operacaoCotistaAux.es.Meta.Columns.FindByColumnName(colOperacao.Name) != null)
                        operacaoCotista.SetColumn(colOperacao.Name, operacaoCotistaAux.GetColumn(colOperacao.Name));
                }
                #endregion
            }
            #endregion

            operacaoCotistaColl.Save();
        }

        /// <summary>
        /// Cola os valores importados
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void ColaValoresCotista(int idCarteira, DateTime data)
        {
            #region Declara objetos
            ColagemResgateDetalheQuery colagemResgateDetalheQuery;
            ColagemResgateQuery colagemResgateQuery;
            ColagemResgate colagemResgate = new ColagemResgate();
            ColagemResgateCollection colagemResgateColl;
            ColagemComeCotas colagemComeCotas = new ColagemComeCotas();
            ColagemComeCotasDetalheQuery colagemComeCotasDetalheQuery;
            ColagemComeCotasQuery colagemComeCotasQuery;
            ColagemComeCotasCollection colagemComeCotasColl;
            string tipoRegistro = "P";
            #endregion

            #region Deleta ColagemResgateMapa
            ColagemResgateMapaCotistaCollection colagemResgateMapaColl = new ColagemResgateMapaCotistaCollection();
            colagemResgateMapaColl.Query.Where(colagemResgateMapaColl.Query.IdCarteira.Equal(idCarteira)
                                               & colagemResgateMapaColl.Query.DataReferencia.Equal(data));

            if (colagemResgateMapaColl.Query.Load())
            {
                colagemResgateMapaColl.MarkAllAsDeleted();
                colagemResgateMapaColl.Save();
            }
            #endregion

            #region Volta status Processado
            colagemResgate.MudaFlagProcessado(idCarteira, data, tipoRegistro, "N");
            colagemComeCotas.MudaFlagProcessado(idCarteira, data, tipoRegistro, "N");
            #endregion

            #region Resgates Colados - Analitico
            colagemResgateDetalheQuery = new ColagemResgateDetalheQuery("colagemResgateDestalhe");
            colagemResgateQuery = new ColagemResgateQuery("colagemResgate");
            colagemResgateColl = new ColagemResgateCollection();
            colagemResgateQuery.Select(colagemResgateDetalheQuery.Quantidade,
                                       colagemResgateDetalheQuery.PrejuizoUsado,
                                       colagemResgateDetalheQuery.RendimentoResgate,
                                       colagemResgateDetalheQuery.ValorBruto,
                                       colagemResgateDetalheQuery.ValorCPMF,
                                       colagemResgateDetalheQuery.ValorIOF,
                                       colagemResgateDetalheQuery.ValorIR,
                                       colagemResgateDetalheQuery.ValorLiquido,
                                       colagemResgateDetalheQuery.ValorPerformance,
                                       colagemResgateDetalheQuery.VariacaoResgate,
                                       colagemResgateQuery.IdCarteira,
                                       colagemResgateQuery.IdCliente,
                                       colagemResgateQuery.DataConversao,
                                       colagemResgateQuery.DataReferencia,
                                       colagemResgateQuery.IdColagemResgate,
                                       colagemResgateQuery.Processado,
                                       colagemResgateDetalheQuery.IdColagemResgateDetalhe);
            colagemResgateQuery.InnerJoin(colagemResgateDetalheQuery).On(colagemResgateDetalheQuery.CodigoExterno.Equal(colagemResgateQuery.CodigoExterno)
                                                                         & colagemResgateDetalheQuery.IdCarteira.Equal(colagemResgateQuery.IdCarteira)
                                                                         & colagemResgateDetalheQuery.IdCliente.Equal(colagemResgateQuery.IdCliente)
                                                                         & colagemResgateDetalheQuery.DataReferencia.Equal(colagemResgateQuery.DataReferencia));
            colagemResgateQuery.Where(colagemResgateQuery.IdCarteira.Equal(idCarteira)
                                       & colagemResgateQuery.Processado.Equal("N")
                                       & colagemResgateQuery.DataReferencia.Equal(data)
                                       & colagemResgateQuery.DataConversao.IsNotNull()
                                       & colagemResgateQuery.TipoRegistro.Equal(tipoRegistro));

            if (colagemResgateColl.Load(colagemResgateQuery))
            {
                colagemResgate.ColaValoresCotista(idCarteira, data, colagemResgateColl, true);
            }
            #endregion

            #region Resgates Colados - Consolidado
            colagemResgateDetalheQuery = new ColagemResgateDetalheQuery("colagemResgateDestalhe");
            colagemResgateQuery = new ColagemResgateQuery("colagemResgate");
            colagemResgateColl = new ColagemResgateCollection();
            colagemResgateQuery.Select(colagemResgateQuery.Quantidade,
                                       colagemResgateQuery.PrejuizoUsado,
                                       colagemResgateQuery.RendimentoResgate,
                                       colagemResgateQuery.ValorBruto,
                                       colagemResgateQuery.ValorCPMF,
                                       colagemResgateQuery.ValorIOF,
                                       colagemResgateQuery.ValorIR,
                                       colagemResgateQuery.ValorLiquido,
                                       colagemResgateQuery.ValorPerformance,
                                       colagemResgateQuery.VariacaoResgate,
                                       colagemResgateQuery.IdCarteira,
                                       colagemResgateQuery.IdCliente,
                                       colagemResgateQuery.DataConversao,
                                       colagemResgateQuery.DataReferencia,
                                       colagemResgateQuery.IdColagemResgate,
                                       colagemResgateQuery.Processado);
            colagemResgateQuery.LeftJoin(colagemResgateDetalheQuery).On(colagemResgateDetalheQuery.CodigoExterno.Equal(colagemResgateQuery.CodigoExterno)
                                                                        & colagemResgateDetalheQuery.IdCarteira.Equal(colagemResgateQuery.IdCarteira)
                                                                        & colagemResgateDetalheQuery.IdCliente.Equal(colagemResgateQuery.IdCliente)
                                                                        & colagemResgateDetalheQuery.DataReferencia.Equal(colagemResgateQuery.DataReferencia));
            colagemResgateQuery.Where(colagemResgateQuery.IdCarteira.Equal(idCarteira)
                                       & colagemResgateQuery.Processado.Equal("N")
                                       & colagemResgateQuery.DataReferencia.Equal(data)
                                       & colagemResgateDetalheQuery.CodigoExterno.IsNull()
                                       & colagemResgateQuery.TipoRegistro.Equal(tipoRegistro));

            if (colagemResgateColl.Load(colagemResgateQuery))
            {
                colagemResgate.ColaValoresCotista(idCarteira, data, colagemResgateColl, false);
            }
            #endregion

            #region ComeCotas - Analitico
            colagemComeCotasDetalheQuery = new ColagemComeCotasDetalheQuery("colagemComeCotasDestalhe");
            colagemComeCotasQuery = new ColagemComeCotasQuery("colagemComeCotas");
            colagemComeCotasColl = new ColagemComeCotasCollection();
            colagemComeCotasQuery.Select(colagemComeCotasDetalheQuery.Quantidade.Sum(),
                                       colagemComeCotasDetalheQuery.PrejuizoUsado.Sum(),
                                       colagemComeCotasDetalheQuery.RendimentoComeCotas.Sum(),
                                       colagemComeCotasDetalheQuery.ValorBruto.Sum(),
                                       colagemComeCotasDetalheQuery.ValorCPMF.Sum(),
                                       colagemComeCotasDetalheQuery.ValorIOF.Sum(),
                                       colagemComeCotasDetalheQuery.ValorIR.Sum(),
                                       colagemComeCotasDetalheQuery.ValorLiquido.Sum(),
                                       colagemComeCotasDetalheQuery.ValorPerformance.Sum(),
                                       colagemComeCotasDetalheQuery.VariacaoComeCotas.Sum(),
                                       colagemComeCotasQuery.IdCarteira,
                                       colagemComeCotasQuery.IdCliente,
                                       colagemComeCotasDetalheQuery.DataConversao);
            colagemComeCotasQuery.InnerJoin(colagemComeCotasDetalheQuery).On(colagemComeCotasDetalheQuery.CodigoExterno.Equal(colagemComeCotasQuery.CodigoExterno)
                                                                             & colagemComeCotasDetalheQuery.IdCarteira.Equal(colagemComeCotasQuery.IdCarteira)
                                                                             & colagemComeCotasDetalheQuery.IdCliente.Equal(colagemComeCotasQuery.IdCliente)
                                                                             & colagemComeCotasDetalheQuery.DataReferencia.Equal(colagemComeCotasQuery.DataReferencia));
            colagemComeCotasQuery.Where(colagemComeCotasQuery.IdCarteira.Equal(idCarteira)
                                       & colagemComeCotasQuery.Processado.Equal("N")
                                       & colagemComeCotasQuery.DataReferencia.Equal(data)
                                       & colagemComeCotasQuery.TipoRegistro.Equal(tipoRegistro));
            colagemComeCotasQuery.GroupBy(colagemComeCotasQuery.IdCarteira,
                                        colagemComeCotasQuery.IdCliente,
                                        colagemComeCotasDetalheQuery.DataConversao);

            if (colagemComeCotasColl.Load(colagemComeCotasQuery))
            {
                colagemComeCotas.ColaValoresCotista(idCarteira, data, colagemComeCotasColl, true);
                colagemComeCotas.MudaFlagProcessado(idCarteira, data, tipoRegistro, "S");
            }
            #endregion

            #region ComeCotas - Consolidado
            colagemComeCotasDetalheQuery = new ColagemComeCotasDetalheQuery("colagemComeCotasDestalhe");
            colagemComeCotasQuery = new ColagemComeCotasQuery("colagemComeCotas");
            colagemComeCotasColl = new ColagemComeCotasCollection();
            colagemComeCotasQuery.Select(colagemComeCotasQuery.Quantidade.Sum(),
                                       colagemComeCotasQuery.PrejuizoUsado.Sum(),
                                       colagemComeCotasQuery.RendimentoComeCotas.Sum(),
                                       colagemComeCotasQuery.ValorBruto.Sum(),
                                       colagemComeCotasQuery.ValorCPMF.Sum(),
                                       colagemComeCotasQuery.ValorIOF.Sum(),
                                       colagemComeCotasQuery.ValorIR.Sum(),
                                       colagemComeCotasQuery.ValorLiquido.Sum(),
                                       colagemComeCotasQuery.ValorPerformance.Sum(),
                                       colagemComeCotasQuery.VariacaoComeCotas.Sum(),
                                       colagemComeCotasQuery.IdCarteira,
                                       colagemComeCotasQuery.IdCliente,
                                       colagemComeCotasQuery.DataConversao);
            colagemComeCotasQuery.LeftJoin(colagemComeCotasDetalheQuery).On(colagemComeCotasDetalheQuery.CodigoExterno.Equal(colagemComeCotasQuery.CodigoExterno)
                                                                            & colagemComeCotasDetalheQuery.IdCarteira.Equal(colagemComeCotasQuery.IdCarteira)
                                                                            & colagemComeCotasDetalheQuery.IdCliente.Equal(colagemComeCotasQuery.IdCliente)
                                                                            & colagemComeCotasDetalheQuery.DataReferencia.Equal(colagemComeCotasQuery.DataReferencia));
            colagemComeCotasQuery.Where(colagemComeCotasQuery.IdCarteira.Equal(idCarteira)
                                       & colagemComeCotasQuery.DataReferencia.Equal(data)
                                       & colagemComeCotasQuery.Processado.Equal("N")
                                       & colagemComeCotasDetalheQuery.CodigoExterno.IsNull()
                                       & colagemComeCotasQuery.TipoRegistro.Equal(tipoRegistro));
            colagemComeCotasQuery.GroupBy(colagemComeCotasQuery.IdCarteira,
                                        colagemComeCotasQuery.IdCliente,
                                        colagemComeCotasQuery.DataConversao);

            if (colagemComeCotasColl.Load(colagemComeCotasQuery))
            {
                colagemComeCotas.ColaValoresCotista(idCarteira, data, colagemComeCotasColl, false);
                colagemComeCotas.MudaFlagProcessado(idCarteira, data, tipoRegistro, "S");
            }
            #endregion
        }
        #endregion

        #region Colagem Fundo

        /// <summary>
        /// Método harmoniza conteúdo das tabelas de Operação/Detalhe e Posicao com suas duplicatas
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        private void CriaBackupFundo(int idCliente, DateTime data)
        {
            #region Declara Objetos
            OperacaoFundoAuxCollection operacaoFundoAuxColl = new OperacaoFundoAuxCollection();
            DetalheResgateFundoAuxCollection detalheResgateAuxColl = new DetalheResgateFundoAuxCollection();

            OperacaoFundoCollection operacaoFundoColl = new OperacaoFundoCollection();
            DetalheResgateFundoCollection detalheResgateColl = new DetalheResgateFundoCollection();
            OperacaoFundoQuery operacaoFundoQuery;
            DetalheResgateFundoQuery detalheResgateQuery;

            List<DetalheResgateFundo> lstDetalheResgateFundo = new List<DetalheResgateFundo>();
            List<int> lstTipoOperacao = ColagemResgate.RetornaTipoOperacaoFundo();

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
            PosicaoFundoAuxCollection posicaoFundoAuxCollection = new PosicaoFundoAuxCollection();
            #endregion

            #region ComeCotas
            ColagemComeCotas colagemComeCotas = new ColagemComeCotas();
            if (colagemComeCotas.VerificaSeExisteValoresColadosComeCotasAtivo(idCliente, data))
            {
                #region Deleta Posições da Tabela PosicaoAux
                posicaoFundoAuxCollection.Query.Where(posicaoFundoAuxCollection.Query.IdCliente.Equal(idCliente)
                                                      & posicaoFundoAuxCollection.Query.DataReferencia.Equal(data));
                if (posicaoFundoAuxCollection.Query.Load())
                {
                    posicaoFundoAuxCollection.MarkAllAsDeleted();
                    posicaoFundoAuxCollection.Save();
                }
                #endregion

                #region Carrega Posicoes do Fundo
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente));
                posicaoFundoCollection.Query.Load();
                #endregion

                #region Percorre posições
                posicaoFundoAuxCollection = new PosicaoFundoAuxCollection();
                foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
                {
                    PosicaoFundoAux posicaoFundoAux = posicaoFundoAuxCollection.AddNew();
                    foreach (esColumnMetadata colPosicao in posicaoFundoAux.es.Meta.Columns)
                    {
                        if (colPosicao.Name.ToUpper().Trim().Equals("DATAREFERENCIA"))
                            posicaoFundoAux.DataReferencia = data;
                        else if (posicaoFundoAux.es.Meta.Columns.FindByColumnName(colPosicao.Name) != null && posicaoFundo.es.Meta.Columns.FindByColumnName(colPosicao.Name) != null)
                            posicaoFundoAux.SetColumn(colPosicao.Name, posicaoFundo.GetColumn(colPosicao.Name));
                    }
                }

                posicaoFundoAuxCollection.Save();
                #endregion
            }
            #endregion

            #region Resgate

            #region Deleta Operações da Tabela Aux
            operacaoFundoAuxColl.Query.Where(operacaoFundoAuxColl.Query.IdCliente.Equal(idCliente)
                                          & operacaoFundoAuxColl.Query.DataConversao.Equal(data)
                                          & operacaoFundoAuxColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

            if (operacaoFundoAuxColl.Query.Load())
            {
                operacaoFundoAuxColl.MarkAllAsDeleted();
                operacaoFundoAuxColl.Save();
            }
            #endregion

            ColagemResgate colagemResgate = new ColagemResgate();
            if (colagemResgate.VerificaSeExisteValoresColadosRegAtivo(idCliente, data))
            {

                #region Deleta Detalhes das Tabelas Aux
                detalheResgateAuxColl.Query.Where(detalheResgateAuxColl.Query.IdCliente.Equal(idCliente)
                                                  & detalheResgateAuxColl.Query.DataOperacao.Equal(data)
                                                  & detalheResgateAuxColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

                if (detalheResgateAuxColl.Query.Load())
                {
                    detalheResgateAuxColl.MarkAllAsDeleted();
                    detalheResgateAuxColl.Save();
                }
                #endregion

                #region Carrega Operações de Fundo
                operacaoFundoColl = new OperacaoFundoCollection();
                operacaoFundoColl.Query.Where(operacaoFundoColl.Query.IdCliente.Equal(idCliente)
                                              & operacaoFundoColl.Query.DataConversao.Equal(data)
                                              & operacaoFundoColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

                operacaoFundoColl.Query.Load();
                #endregion

                #region Carrega Detalhes
                detalheResgateQuery = new DetalheResgateFundoQuery("detalheResgate");
                operacaoFundoQuery = new OperacaoFundoQuery("operacaoFundo");
                detalheResgateQuery.InnerJoin(operacaoFundoQuery).On(operacaoFundoQuery.IdOperacao.Equal(detalheResgateQuery.IdOperacao));
                detalheResgateQuery.Where(operacaoFundoQuery.IdCliente.Equal(idCliente)
                                              & operacaoFundoQuery.DataConversao.Equal(data)
                                              & operacaoFundoQuery.TipoOperacao.In(lstTipoOperacao.ToArray()));

                if (detalheResgateColl.Load(detalheResgateQuery))
                    lstDetalheResgateFundo = (List<DetalheResgateFundo>)detalheResgateColl;
                #endregion

                if (operacaoFundoColl.Count == 0)
                    return;

                foreach (OperacaoFundo operacaoFundo in operacaoFundoColl)
                {
                    #region Operação Fundo
                    OperacaoFundoAux operacaoFundoAux = operacaoFundoAuxColl.AddNew();
                    foreach (esColumnMetadata colOperacao in operacaoFundo.es.Meta.Columns)
                    {
                        if (operacaoFundoAux.es.Meta.Columns.FindByColumnName(colOperacao.Name) != null && operacaoFundo.es.Meta.Columns.FindByColumnName(colOperacao.Name) != null)
                            operacaoFundoAux.SetColumn(colOperacao.Name, operacaoFundo.GetColumn(colOperacao.Name));
                    }
                    #endregion

                    #region Detalhe Resgate
                    foreach (DetalheResgateFundo detalheFundo in lstDetalheResgateFundo.FindAll(delegate(DetalheResgateFundo x) { return x.IdOperacao == operacaoFundo.IdOperacao.Value; }))
                    {
                        DetalheResgateFundoAux detalheFundoAux = detalheResgateAuxColl.AddNew();
                        foreach (esColumnMetadata colDetalhe in detalheFundoAux.es.Meta.Columns)
                        {
                            if (colDetalhe.Name.ToUpper().Trim().Equals("TIPOOPERACAO"))
                            {
                                detalheFundoAux.TipoOperacao = operacaoFundo.TipoOperacao.Value;
                            }
                            else if (colDetalhe.Name.ToUpper().Trim().Equals("DATAOPERACAO"))
                            {
                                detalheFundoAux.DataOperacao = operacaoFundo.DataConversao.Value;
                            }
                            else if (detalheFundoAux.es.Meta.Columns.FindByColumnName(colDetalhe.Name) != null && detalheFundo.es.Meta.Columns.FindByColumnName(colDetalhe.Name) != null)
                            {
                                detalheFundoAux.SetColumn(colDetalhe.Name, detalheFundo.GetColumn(colDetalhe.Name));
                            }
                        }
                    }
                    #endregion
                }

                operacaoFundoAuxColl.Save();
                detalheResgateAuxColl.Save();
            }
            #endregion
        }

        /// <summary>
        /// Restaura operações coladas e deleta o bkp - Fundo
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void RestauraBackupFundo(int idCliente, DateTime data)
        {
            #region Declara objetos
            OperacaoFundoCollection operacaoFundoColl = new OperacaoFundoCollection();
            OperacaoFundoAuxCollection operacaoFundoAuxColl = new OperacaoFundoAuxCollection();
            List<OperacaoFundoAux> lstOperacaoFundoAux = new List<OperacaoFundoAux>();
            List<int> lstTipoOperacao = ColagemResgate.RetornaTipoOperacaoFundo();
            #endregion

            ColagemOperResgPosFundoCollection colagemOperResgPosAfetadaColl = new ColagemOperResgPosFundoCollection();
            colagemOperResgPosAfetadaColl.DeletaColagemOperResgPosAfetada(idCliente, data);

            ColagemResgateDetalhePosicaoCollection colagemResgateDetalhePosicaoColl = new ColagemResgateDetalhePosicaoCollection();
            colagemResgateDetalhePosicaoColl.DeletaColagemResgateDetalhePosicaoFundo(idCliente, data);

            #region Deleta ColagemComeCotasPosFundo
            ColagemComeCotasPosFundoCollection colagemComeCotasPosFundoColl = new ColagemComeCotasPosFundoCollection();
            colagemComeCotasPosFundoColl.Query.Where(colagemComeCotasPosFundoColl.Query.IdCliente.Equal(idCliente)
                                                    & colagemComeCotasPosFundoColl.Query.DataReferencia.Equal(data));

            if (colagemComeCotasPosFundoColl.Query.Load())
            {
                colagemComeCotasPosFundoColl.MarkAllAsDeleted();
                colagemComeCotasPosFundoColl.Save();
            }
            #endregion

            #region Deleta Operações de ComeCotas - Operacao
            operacaoFundoColl.Query.Where(operacaoFundoColl.Query.IdCliente.Equal(idCliente)
                                          & operacaoFundoColl.Query.DataOperacao.Equal(data)
                                          & operacaoFundoColl.Query.ValoresColados.Equal("S")
                                          & operacaoFundoColl.Query.TipoOperacao.Equal((int)TipoOperacaoFundo.ComeCotas));

            if (operacaoFundoColl.Query.Load())
            {
                operacaoFundoColl.MarkAllAsDeleted();
                operacaoFundoColl.Save();
            }
            #endregion

            #region Deleta Operações de ComeCotas - OperacaoAux
            operacaoFundoAuxColl.Query.Where(operacaoFundoAuxColl.Query.IdCliente.Equal(idCliente)
                                             & operacaoFundoAuxColl.Query.DataOperacao.Equal(data)
                                             & operacaoFundoAuxColl.Query.ValoresColados.NotEqual("S")
                                             & operacaoFundoAuxColl.Query.TipoOperacao.Equal((int)TipoOperacaoFundo.ComeCotas));

            if (operacaoFundoAuxColl.Query.Load())
            {
                operacaoFundoAuxColl.MarkAllAsDeleted();
                operacaoFundoAuxColl.Save();
            }
            #endregion

            #region Deleta Operações coladas
            operacaoFundoColl = new OperacaoFundoCollection();
            operacaoFundoColl.Query.Where(operacaoFundoColl.Query.IdCliente.Equal(idCliente)
                                          & operacaoFundoColl.Query.DataConversao.Equal(data)
                                          & operacaoFundoColl.Query.ValoresColados.Equal("S")
                                          & operacaoFundoColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

            if (operacaoFundoColl.Query.Load())
            {
                operacaoFundoColl.MarkAllAsDeleted();
                operacaoFundoColl.Save();
            }
            #endregion

            #region Carrega Operação de BKP
            operacaoFundoAuxColl = new OperacaoFundoAuxCollection();
            operacaoFundoAuxColl.Query.Where(operacaoFundoAuxColl.Query.IdCliente.Equal(idCliente)
                                          & operacaoFundoAuxColl.Query.DataOperacao.Equal(data)
                                          & operacaoFundoAuxColl.Query.ValoresColados.Equal("N")
                                          & operacaoFundoAuxColl.Query.TipoOperacao.In(lstTipoOperacao.ToArray()));

            if (!operacaoFundoAuxColl.Query.Load())
            {
                return;
            }
            #endregion

            operacaoFundoColl = new OperacaoFundoCollection();

            #region Restaurando Backup
            foreach (OperacaoFundoAux operacaoFundoAux in operacaoFundoAuxColl)
            {
                OperacaoFundo operacaoFundo = operacaoFundoColl.AddNew();
                #region Operação Fundo
                foreach (esColumnMetadata colOperacao in operacaoFundoAux.es.Meta.Columns)
                {
                    if (colOperacao.Name.ToUpper().Trim().Equals("IDOPERACAO")) //Não copia o ID, será usado o identity
                        continue;

                    if (operacaoFundo.es.Meta.Columns.FindByColumnName(colOperacao.Name) != null && operacaoFundoAux.es.Meta.Columns.FindByColumnName(colOperacao.Name) != null)
                        operacaoFundo.SetColumn(colOperacao.Name, operacaoFundoAux.GetColumn(colOperacao.Name));
                }
                #endregion
            }
            #endregion

            operacaoFundoColl.Save();
        }

        /// <summary>
        /// Cola os valores importados
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        private void ColaValoresFundo(int idCarteira, DateTime data)
        {
            #region Declara objetos
            ColagemResgateDetalheQuery colagemResgateDetalheQuery;
            ColagemResgateQuery colagemResgateQuery;
            ColagemResgate colagemResgate = new ColagemResgate();
            ColagemResgateCollection colagemResgateColl;
            ColagemComeCotasDetalheQuery colagemComeCotasDetalheQuery;
            ColagemComeCotasQuery colagemComeCotasQuery;
            ColagemComeCotas colagemComeCotas = new ColagemComeCotas();
            ColagemComeCotasCollection colagemComeCotasColl;
            string tipoRegistro = "A";
            #endregion

            #region Deleta ColagemResgateMapa
            ColagemResgateMapaFundoCollection colagemResgateMapaColl = new ColagemResgateMapaFundoCollection();
            colagemResgateMapaColl.Query.Where(colagemResgateMapaColl.Query.IdCliente.Equal(idCarteira)
                                               & colagemResgateMapaColl.Query.DataReferencia.Equal(data));

            if (colagemResgateMapaColl.Query.Load())
            {
                colagemResgateMapaColl.MarkAllAsDeleted();
                colagemResgateMapaColl.Save();
            }
            #endregion

            #region Deleta ColagemComeCotasPosFundo
            ColagemComeCotasPosFundoCollection colagemComeCotasPosFundoColl = new ColagemComeCotasPosFundoCollection();
            colagemComeCotasPosFundoColl.Query.Where(colagemComeCotasPosFundoColl.Query.IdCliente.Equal(idCarteira)
                                               & colagemComeCotasPosFundoColl.Query.DataReferencia.Equal(data));

            if (colagemComeCotasPosFundoColl.Query.Load())
            {
                colagemComeCotasPosFundoColl.MarkAllAsDeleted();
                colagemComeCotasPosFundoColl.Save();
            }
            #endregion

            #region Volta status Processado
            colagemResgate.MudaFlagProcessado(idCarteira, data, tipoRegistro, "N");
            colagemComeCotas.MudaFlagProcessado(idCarteira, data, tipoRegistro, "N");
            #endregion

            #region Resgates Colados - Analitico
            colagemResgateDetalheQuery = new ColagemResgateDetalheQuery("colagemResgateDestalhe");
            colagemResgateQuery = new ColagemResgateQuery("colagemResgate");
            colagemResgateColl = new ColagemResgateCollection();
            colagemResgateQuery.Select(colagemResgateDetalheQuery.Quantidade,
                                       colagemResgateDetalheQuery.PrejuizoUsado,
                                       colagemResgateDetalheQuery.RendimentoResgate,
                                       colagemResgateDetalheQuery.ValorBruto,
                                       colagemResgateDetalheQuery.ValorCPMF,
                                       colagemResgateDetalheQuery.ValorIOF,
                                       colagemResgateDetalheQuery.ValorIR,
                                       colagemResgateDetalheQuery.ValorLiquido,
                                       colagemResgateDetalheQuery.ValorPerformance,
                                       colagemResgateDetalheQuery.VariacaoResgate,
                                       colagemResgateQuery.IdCarteira,
                                       colagemResgateQuery.IdCliente,
                                       colagemResgateQuery.DataConversao,
                                       colagemResgateQuery.DataReferencia,
                                       colagemResgateQuery.IdColagemResgate,
                                       colagemResgateQuery.Processado,
                                       colagemResgateDetalheQuery.IdColagemResgateDetalhe);
            colagemResgateQuery.InnerJoin(colagemResgateDetalheQuery).On(colagemResgateDetalheQuery.CodigoExterno.Equal(colagemResgateQuery.CodigoExterno)
                                                                        & colagemResgateDetalheQuery.IdCarteira.Equal(colagemResgateQuery.IdCarteira)
                                                                        & colagemResgateDetalheQuery.IdCliente.Equal(colagemResgateQuery.IdCliente)
                                                                        & colagemResgateDetalheQuery.DataReferencia.Equal(colagemResgateQuery.DataReferencia));
            colagemResgateQuery.Where(colagemResgateQuery.IdCliente.Equal(idCarteira)
                                       & colagemResgateQuery.DataReferencia.Equal(data)
                                       & colagemResgateQuery.Processado.Equal("N")
                                       & colagemResgateQuery.TipoRegistro.Equal(tipoRegistro));           

            if (colagemResgateColl.Load(colagemResgateQuery))
            {
                colagemResgate.ColaValoresFundo(idCarteira, data, colagemResgateColl, true);
            }
            #endregion

            #region Resgates Colados - Consolidado
            colagemResgateDetalheQuery = new ColagemResgateDetalheQuery("colagemResgateDestalhe");
            colagemResgateQuery = new ColagemResgateQuery("colagemResgate");
            colagemResgateColl = new ColagemResgateCollection();
            colagemResgateQuery.Select(colagemResgateQuery.Quantidade,
                                       colagemResgateQuery.PrejuizoUsado,
                                       colagemResgateQuery.RendimentoResgate,
                                       colagemResgateQuery.ValorBruto,
                                       colagemResgateQuery.ValorCPMF,
                                       colagemResgateQuery.ValorIOF,
                                       colagemResgateQuery.ValorIR,
                                       colagemResgateQuery.ValorLiquido,
                                       colagemResgateQuery.ValorPerformance,
                                       colagemResgateQuery.VariacaoResgate,
                                       colagemResgateQuery.IdCarteira,
                                       colagemResgateQuery.IdCliente,
                                       colagemResgateQuery.DataConversao,
                                       colagemResgateQuery.DataReferencia,
                                       colagemResgateQuery.IdColagemResgate,
                                       colagemResgateQuery.Processado);
            colagemResgateQuery.LeftJoin(colagemResgateDetalheQuery).On(colagemResgateDetalheQuery.CodigoExterno.Equal(colagemResgateQuery.CodigoExterno)
                                                                        & colagemResgateDetalheQuery.IdCarteira.Equal(colagemResgateQuery.IdCarteira)
                                                                        & colagemResgateDetalheQuery.IdCliente.Equal(colagemResgateQuery.IdCliente)
                                                                        & colagemResgateDetalheQuery.DataReferencia.Equal(colagemResgateQuery.DataReferencia));
            colagemResgateQuery.Where(colagemResgateQuery.IdCliente.Equal(idCarteira)
                                       & colagemResgateQuery.DataReferencia.Equal(data)
                                       & colagemResgateDetalheQuery.CodigoExterno.IsNull()
                                       & colagemResgateQuery.Processado.Equal("N")
                                       & colagemResgateQuery.TipoRegistro.Equal(tipoRegistro));

            if (colagemResgateColl.Load(colagemResgateQuery))
            {
                colagemResgate.ColaValoresFundo(idCarteira, data, colagemResgateColl, false);
            }
            #endregion
            
            #region ComeCotas - Analitico
            colagemComeCotasDetalheQuery = new ColagemComeCotasDetalheQuery("colagemComeCotasDestalhe");
            colagemComeCotasQuery = new ColagemComeCotasQuery("colagemComeCotas");
            colagemComeCotasColl = new ColagemComeCotasCollection();
            colagemComeCotasQuery.Select(colagemComeCotasDetalheQuery.Quantidade.Sum(),
                                       colagemComeCotasDetalheQuery.PrejuizoUsado.Sum(),
                                       colagemComeCotasDetalheQuery.RendimentoComeCotas.Sum(),
                                       colagemComeCotasDetalheQuery.ValorBruto.Sum(),
                                       colagemComeCotasDetalheQuery.ValorCPMF.Sum(),
                                       colagemComeCotasDetalheQuery.ValorIOF.Sum(),
                                       colagemComeCotasDetalheQuery.ValorIR.Sum(),
                                       colagemComeCotasDetalheQuery.ValorLiquido.Sum(),
                                       colagemComeCotasDetalheQuery.ValorPerformance.Sum(),
                                       colagemComeCotasDetalheQuery.VariacaoComeCotas.Sum(),
                                       colagemComeCotasQuery.IdCarteira,
                                       colagemComeCotasQuery.IdCliente,
                                       colagemComeCotasDetalheQuery.DataConversao);
            colagemComeCotasQuery.InnerJoin(colagemComeCotasDetalheQuery).On(colagemComeCotasDetalheQuery.CodigoExterno.Equal(colagemComeCotasQuery.CodigoExterno)
                                                                             & colagemComeCotasDetalheQuery.IdCarteira.Equal(colagemComeCotasQuery.IdCarteira)
                                                                             & colagemComeCotasDetalheQuery.IdCliente.Equal(colagemComeCotasQuery.IdCliente)
                                                                             & colagemComeCotasDetalheQuery.DataReferencia.Equal(colagemComeCotasQuery.DataReferencia));
            colagemComeCotasQuery.Where(colagemComeCotasQuery.IdCliente.Equal(idCarteira)
                                       & colagemComeCotasQuery.DataReferencia.Equal(data)
                                       & colagemComeCotasQuery.Processado.Equal("N")
                                       & colagemComeCotasQuery.TipoRegistro.Equal(tipoRegistro));
            colagemComeCotasQuery.GroupBy(colagemComeCotasQuery.IdCarteira,
                                        colagemComeCotasQuery.IdCliente,
                                        colagemComeCotasDetalheQuery.DataConversao);

            if (colagemComeCotasColl.Load(colagemComeCotasQuery))
            {
                colagemComeCotas.ColaValoresFundo(idCarteira, data, colagemComeCotasColl, true);
            }
            #endregion

            #region ComeCotas - Consolidado
            colagemComeCotasDetalheQuery = new ColagemComeCotasDetalheQuery("colagemComeCotasDestalhe");
            colagemComeCotasQuery = new ColagemComeCotasQuery("colagemComeCotas");
            colagemComeCotasColl = new ColagemComeCotasCollection();
            colagemComeCotasQuery.Select(colagemComeCotasQuery.Quantidade.Sum(),
                                       colagemComeCotasQuery.PrejuizoUsado.Sum(),
                                       colagemComeCotasQuery.RendimentoComeCotas.Sum(),
                                       colagemComeCotasQuery.ValorBruto.Sum(),
                                       colagemComeCotasQuery.ValorCPMF.Sum(),
                                       colagemComeCotasQuery.ValorIOF.Sum(),
                                       colagemComeCotasQuery.ValorIR.Sum(),
                                       colagemComeCotasQuery.ValorLiquido.Sum(),
                                       colagemComeCotasQuery.ValorPerformance.Sum(),
                                       colagemComeCotasQuery.VariacaoComeCotas.Sum(),
                                       colagemComeCotasQuery.IdCarteira,
                                       colagemComeCotasQuery.IdCliente,
                                       colagemComeCotasQuery.DataConversao);
            colagemComeCotasQuery.LeftJoin(colagemComeCotasDetalheQuery).On(colagemComeCotasDetalheQuery.CodigoExterno.Equal(colagemComeCotasQuery.CodigoExterno)
                                                                             & colagemComeCotasDetalheQuery.IdCarteira.Equal(colagemComeCotasQuery.IdCarteira)
                                                                             & colagemComeCotasDetalheQuery.IdCliente.Equal(colagemComeCotasQuery.IdCliente)
                                                                             & colagemComeCotasDetalheQuery.DataReferencia.Equal(colagemComeCotasQuery.DataReferencia));
            colagemComeCotasQuery.Where(colagemComeCotasQuery.IdCliente.Equal(idCarteira)
                                       & colagemComeCotasQuery.DataReferencia.Equal(data)
                                       & colagemComeCotasDetalheQuery.CodigoExterno.IsNull()
                                       & colagemComeCotasQuery.Processado.Equal("N")
                                       & colagemComeCotasQuery.TipoRegistro.Equal(tipoRegistro));
            colagemComeCotasQuery.GroupBy(colagemComeCotasQuery.IdCarteira,
                                        colagemComeCotasQuery.IdCliente,
                                        colagemComeCotasQuery.DataConversao);

            if (colagemComeCotasColl.Load(colagemComeCotasQuery))
            {
                colagemComeCotas.ColaValoresFundo(idCarteira, data, colagemComeCotasColl, false);
                colagemComeCotas.MudaFlagProcessado(idCarteira, data, tipoRegistro, "S");
            }
            #endregion            
        }
        #endregion

        private void ExcluiComeCotas(int idCarteira, DateTime data)
        {
            #region exclui agendamento anterior
            AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
            agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.DataLancamento.GreaterThanOrEqual(data),
                                                  agendaComeCotasCollection.Query.IdCarteira.Equal(idCarteira));
            agendaComeCotasCollection.Query.Load();
            agendaComeCotasCollection.MarkAllAsDeleted();
            agendaComeCotasCollection.Save();
            #endregion
        }

        public void DeletaOperacaoInterna(int idCarteira, DateTime data)
        {
            TransferenciaSerieQuery transferenciaSerieQuery = new TransferenciaSerieQuery("transferencia");
            OperacaoCotistaQuery operacaoQuery = new OperacaoCotistaQuery("operacao");
            OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();

            #region Transferencia de Série
            operacaoQuery.Select(operacaoQuery);
            operacaoQuery.InnerJoin(transferenciaSerieQuery).On(operacaoQuery.IdTransferenciaSerie.Equal(transferenciaSerieQuery.IdTransferenciaSerie));
            operacaoQuery.Where(transferenciaSerieQuery.IdCarteira.Equal(idCarteira) 
                                & transferenciaSerieQuery.DataExecucao.Equal(data)
                                & operacaoQuery.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateCotasEspecial, (byte)TipoOperacaoFundo.AplicacaoCotasEspecial));

            if (operacaoCotistaColl.Load(operacaoQuery))
            {
                operacaoCotistaColl.MarkAllAsDeleted();
                operacaoCotistaColl.Save();
            }
            #endregion

            #region Transferencia entre Cotistas
            operacaoQuery = new OperacaoCotistaQuery("operacao");
            operacaoCotistaColl = new OperacaoCotistaCollection();
            operacaoQuery.Select(operacaoQuery);
            operacaoQuery.Where(operacaoQuery.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateCotasEspecial, (byte)TipoOperacaoFundo.AplicacaoCotasEspecial)
                                & operacaoQuery.Fonte.Equal((int)FonteOperacaoCotista.TransferenciaEntreCotista)
                                & operacaoQuery.IdCarteira.Equal(idCarteira)
                                & operacaoQuery.DataRegistro.Equal(data));

            if (operacaoCotistaColl.Load(operacaoQuery))
            {
                operacaoCotistaColl.MarkAllAsDeleted();
                operacaoCotistaColl.Save();
            }
            #endregion

            #region Deleta operações/posições de zera caixa de Zera Caixa
            OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
            operacaoCotistaCollectionDeletar.Query.Select(operacaoCotistaCollectionDeletar.Query.IdOperacao,
                                                          operacaoCotistaCollectionDeletar.Query.TipoOperacao,
                                                          operacaoCotistaCollectionDeletar.Query.DataRegistro);
            operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCarteira.Equal(idCarteira) &
                                                  operacaoCotistaCollectionDeletar.Query.DataRegistro.GreaterThanOrEqual(data) &
                                                  operacaoCotistaCollectionDeletar.Query.Fonte.Equal((byte)FonteOperacaoCotista.ZeraCaixa));

            if (operacaoCotistaCollectionDeletar.Query.Load())
            {
                PosicaoCotistaAberturaCollection posicaoCotistaAberturaColl = new PosicaoCotistaAberturaCollection();
                PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoColl = new PosicaoCotistaHistoricoCollection();
                PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();  

                foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollectionDeletar)
                {                   
                    int idOperacao = operacaoCotista.IdOperacao.Value;
                    byte tipoOperacao = operacaoCotista.TipoOperacao.Value;
                    DateTime dataRegistro = operacaoCotista.DataRegistro.Value;

                    if (tipoOperacao != (byte)TipoOperacaoCotista.Aplicacao)
                        continue;

                    //Fechamento
                    posicaoCotistaColl.Query.Where(posicaoCotistaColl.Query.IdOperacao.Equal(idOperacao) &
                                                   posicaoCotistaColl.Query.IdCarteira.Equal(idCarteira));
                    if (posicaoCotistaColl.Query.Load())
                    {
                        posicaoCotistaColl.MarkAllAsDeleted();
                        posicaoCotistaColl.Save();
                    }

                    //Historico
                    posicaoCotistaHistoricoColl.Query.Where(posicaoCotistaHistoricoColl.Query.IdOperacao.Equal(idOperacao) &
                                                            posicaoCotistaHistoricoColl.Query.IdCarteira.Equal(idCarteira) &
                                                            posicaoCotistaHistoricoColl.Query.DataHistorico.GreaterThanOrEqual(dataRegistro));
                    if (posicaoCotistaHistoricoColl.Query.Load())
                    {
                        posicaoCotistaHistoricoColl.MarkAllAsDeleted();
                        posicaoCotistaHistoricoColl.Save();
                    }

                    //Abertura
                    posicaoCotistaAberturaColl.Query.Where(posicaoCotistaAberturaColl.Query.IdOperacao.Equal(idOperacao) &
                                                           posicaoCotistaAberturaColl.Query.IdCarteira.Equal(idCarteira) &
                                                           posicaoCotistaAberturaColl.Query.DataHistorico.GreaterThanOrEqual(dataRegistro));
                    if (posicaoCotistaAberturaColl.Query.Load())
                    {
                        posicaoCotistaAberturaColl.MarkAllAsDeleted();
                        posicaoCotistaAberturaColl.Save();
                    }
                }

            }
            operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
            operacaoCotistaCollectionDeletar.Save();
            #endregion
        }


        public void CalculaPendenciaLiquidacao(int idCliente, DateTime data)
        {
            PosicaoFundoCollection posicaoFundoColl = new PosicaoFundoCollection();
            PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();

            posicaoFundoColl.CalculaPendenciaLiquidacao(idCliente, data);
            posicaoCotistaColl.CalculaPendenciaLiquidacao(idCliente, data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ValidaPLxQtde(int idCliente, DateTime data)
        {
            HistoricoCota historicoCota = new HistoricoCota();

            if (!historicoCota.LoadByPrimaryKey(data, idCliente))
            {
                throw new Exception("Não existe registro de Histórico Cota!");
            }
            else
            {
                //Verifica se ocorreu evento de cisao/incorporacao/fusão total (100%)
                EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
                eventoFundoCollection.Query.Where(eventoFundoCollection.Query.DataPosicao.Equal(data));
                eventoFundoCollection.Query.Where(eventoFundoCollection.Query.Status.Equal("Ativo"));
                eventoFundoCollection.Query.Where(eventoFundoCollection.Query.IdCarteiraOrigem.Equal(idCliente));
                eventoFundoCollection.Query.Where(eventoFundoCollection.Query.Percentual.Equal(100));
                if (eventoFundoCollection.Query.Load())
                {
                    if (eventoFundoCollection.Count > 0)
                        return;
                }

                if (historicoCota.PLFechamento.Value + historicoCota.QuantidadeFechamento.Value == 0)
                    throw new Exception("Quantidade e PL zerados!");
            }

            return;
        }
    }
}
