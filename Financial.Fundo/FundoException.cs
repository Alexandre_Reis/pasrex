﻿using System;

namespace Financial.Fundo.Exceptions {
    /// <summary>
    /// Classe base de Exceção do componente de Fundo
    /// </summary>
    public class FundoException : Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public FundoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public FundoException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public FundoException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de Carteira
    /// </summary>
    public class CarteiraNaoCadastradaException : FundoException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CarteiraNaoCadastradaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CarteiraNaoCadastradaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de HistoricoCota
    /// </summary>
    public class HistoricoCotaNaoCadastradoException : FundoException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public HistoricoCotaNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public HistoricoCotaNaoCadastradoException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public HistoricoCotaNaoCadastradoException(string mensagem, Exception inner) : base(mensagem, inner) { }
    }

    /// <summary>
    /// Exceção de TabelaTaxaAdministracao
    /// </summary>
    public class TabelaTaxaAdministracaoParametrosNulosException : FundoException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TabelaTaxaAdministracaoParametrosNulosException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TabelaTaxaAdministracaoParametrosNulosException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de TipoCVM
    /// </summary>
    public class TipoCVMNaoCadastradoException : FundoException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TipoCVMNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TipoCVMNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de TabelaTaxaFiscalizacaoCVM
    /// </summary>
    public class TabelaTaxaFiscalizacaoCVMNaoCadastradoException : FundoException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TabelaTaxaFiscalizacaoCVMNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TabelaTaxaFiscalizacaoCVMNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de IdCadastroProvisao
    /// </summary>
    public class IdCadastroProvisaoNaoCadastradoException : FundoException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public IdCadastroProvisaoNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public IdCadastroProvisaoNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de Cliente
    /// </summary>
    public class ClienteSemPosicaoException : FundoException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ClienteSemPosicaoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ClienteSemPosicaoException(string mensagem) : base(mensagem) { }
    }

}


