SELECT DISTINCT
	'16'+ ';' +
	ISNULL(substring(CONVERT(varchar,SAC_CL_PATRSINT.Data,20),1,10),'') 	+ ';' +
	LTRIM(RTRIM(ISNULL(CLI_BAS.IdCarteira,''))) + ';' +
	LTRIM(RTRIM(ISNULL(CLI_BAS.Nome,''))) + ';' +
	ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI_BAS.Cpfcnpj,''),'.',''),'-',''),'/','')))	+ ';'  +
	LTRIM(RTRIM(ISNULL(CLI_BAS.CodigoIsin,''))) + ';' +
	LTRIM(RTRIM(ISNULL(Moeda.Nome,''))) + ';' +
	CONVERT(varchar, ISNULL(CLI_BAS.DiasCotizacaoResgate, 0))	+ ';' +
	CONVERT(varchar, ISNULL(CLI_BAS.DiasLiquidacaoResgate, 0))	+ ';' +
	CONVERT(varchar, ISNULL(SAC_CL_PATRSINT.QuantidadeFechamento, 0))	+ ';' +
	CONVERT(varchar, ISNULL(SAC_CL_PATRSINT.CotaFechamento, 0))	+ ';' +
	'0'+ ';' + -- PATRSINT.VL_COTA1 RENTAB.DT_DIA_INI
	'0'+ ';' + -- PATRSINT.VL_COTA1 RENTAB.DT_MES_INI
	'0'+ ';' + -- PATRSINT.VL_COTA1 RENTAB.DT_ANO_INI
	'0'+ ';' + -- PATRSINT.VL_COTA1 RENTAB.DT_12M_INI
	
	''+ ';' + -- SG_CLASSIFICACAO_CVM
	''+ ';' + -- SG_CLASSIFICACAO_SRF
	''+ ';'  -- SG_CLASSIFICACAO_ANBID
	 AS LINHA
FROM (SELECT	Pessoa.Nome, carteira.CodigoIsin, 
				Pessoa.IdPessoa, Cliente.IdMoeda, Cliente.IdCliente, 
				Carteira.IdCarteira, Pessoa.Cpfcnpj,
				Carteira.DiasCotizacaoResgate, carteira.DiasLiquidacaoResgate
		FROM Pessoa 
		INNER JOIN Cliente
			ON Pessoa.IdPessoa = Cliente.IdCliente
		INNER JOIN Carteira
			ON Cliente.IdCliente = Carteira.IdCarteira ) CLI_BAS
	
		INNER JOIN Moeda
			ON Moeda.IdMoeda = CLI_BAS.IdMoeda
			
	INNER JOIN HistoricoCota SAC_CL_PATRSINT
		ON CLI_BAS.IdCarteira = SAC_CL_PATRSINT.IdCarteira 
		
WHERE 	CLI_BAS.IdCarteira in (	'{1}') 
AND		SAC_CL_PATRSINT.Data = '{0}'
