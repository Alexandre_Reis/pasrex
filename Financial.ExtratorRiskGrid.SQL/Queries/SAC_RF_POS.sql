﻿SELECT	DISTINCT 
		'01' + ';' + 
		substring(CONVERT(varchar,POS.DT,20),1,10) 	+ ';' +
		LTRIM(RTRIM(ISNULL(CLI.NOME,''))) + ';' +
		LTRIM(RTRIM(ISNULL(TIPO.BACEN_CD,''))) + ';' +
		ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.CGC,''),'.',''),'-',''),'/','')))	+ ';' +
		LTRIM(RTRIM(ISNULL(LASTRO.DS_DESCRICAO,'')))  + ';' 		+
		CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
								 FROM	SAC_BA_EQUIVALENCIAS 				EQUIV 
								WHERE	EQUIV.CD_CONTRAPARTE		 = 'SPC' 
								AND		EQUIV.CD_SISTEMA_DESTINO = LASTRO.BAMDA_CD 
								AND		EQUIV.CD_EQUIVALENCIA	 = 'MDA' ) IS NULL THEN LTRIM(RTRIM(ISNULL(LASTRO.BAMDA_CD,'')))
		ELSE (SELECT	LTRIM(RTRIM(EQUIV.CD_SISTEMA_ORIGEM)) 
					FROM		SAC_BA_EQUIVALENCIAS 				EQUIV
					WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
					AND		EQUIV.CD_SISTEMA_DESTINO	= LASTRO.BAMDA_CD 
					AND		EQUIV.CD_EQUIVALENCIA			= 'MDA') END + ';' + 
		LTRIM(RTRIM(ISNULL(LASTRO.CD_CETIP_SELIC,'')))+ ';' +
		LTRIM(RTRIM(ISNULL(LASTRO.CD_ISIN,'')))+ ';' +
		CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
								 FROM	SAC_BA_EQUIVALENCIAS				EQUIV 
								WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
								AND		EQUIV.CD_SISTEMA_DESTINO	= LASTRO.BAINS_CD 
								AND		EQUIV.CD_EQUIVALENCIA			= 'BEP') IS NULL THEN RTRIM(LTRIM(ISNULL(LASTRO.BAINS_CD,'')))
		ELSE (SELECT	LTRIM(RTRIM(EQUIV.CD_SISTEMA_ORIGEM)) 
					FROM		SAC_BA_EQUIVALENCIAS				EQUIV
					WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
					AND		EQUIV.CD_SISTEMA_DESTINO	= LASTRO.BAINS_CD 
					AND		EQUIV.CD_EQUIVALENCIA			= 'BEP') END + ';' + 
		substring(CONVERT(varchar,LASTRO.DT_VENCIMENTO,20),1,10) 	+ ';' +
		substring(CONVERT(varchar,LASTRO.DT_EMISSAO,20),1,10) 	+ ';' +
		
		
		ISNULL(substring(CONVERT(varchar,LASTRO.DT_DATA_BASE,20),1,10),'') 	+ ';' +
		
		substring(CONVERT(varchar,(SELECT MIN(MOV.DT) 
				 		FROM	SAC_RF_MOV MOV 
						WHERE MOV.RFOP_CD = OPER.CD 
						AND	MOV.SG_OPERACAO = 'C'),20),1,10) 	+ ';' +
		convert(varchar,convert(decimal(25,12),ISNULL(POS.VL_PU_NOMINAL, 0.0))) + ';' +
		convert(varchar,convert(decimal(25,12),ISNULL(POS.VL_PUEM_PRINC, 0))		) + ';' +
		convert(varchar,convert(decimal(25,12),ISNULL(POS.VL_PU_AO_PAR, 0.0))	) + ';' +
		convert(varchar,convert(decimal(25,12),ISNULL(LASTRO.VL_PU_EMISSAO, 0.0))	) + ';' +
		LTRIM(RTRIM(ISNULL(LASTRO.IC_ADLIC,'')))+ ';' +
		LTRIM(RTRIM(ISNULL(OPER.IC_NEG_VENC,'')))+ ';' +
		convert(varchar,convert(decimal(25,12),	CASE WHEN (LASTRO.IC_ATIVO_PASSIVO = 'A') 
							THEN (ISNULL(POS.QT_DISPONIVEL, 0) + ISNULL(POS.QT_BLOQUEADA, 0)) * 1 
							ELSE (ISNULL(POS.QT_DISPONIVEL, 0) + ISNULL(POS.QT_BLOQUEADA, 0)) * -1 END) ) + ';' +
		convert(varchar,ISNULL(POS.VL_TIR, 0.0))+ ';' +
		ISNULL(TIPO.IC_JUROS	,'') + ';' +
		ISNULL(LASTRO.IC_FORCA_CALCULO_PADRONIZADO	,'') + ';' +
		ISNULL(TIPO.IC_CALCULO_PADRONIZADO,'') + ';' +
		convert(varchar,convert(decimal(25,12), ISNULL(LASTRO.IDDIR_PC, 0.0))	)+ ';' +
		CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
				FROM	SAC_BA_EQUIVALENCIAS 				EQUIV 
				WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
				AND		EQUIV.CD_SISTEMA_DESTINO	= LASTRO.IDDIR_CD 
				AND		EQUIV.CD_EQUIVALENCIA			= 'IDX') IS NULL THEN RTRIM(LTRIM(ISNULL(LASTRO.IDDIR_CD ,'')))
		ELSE (SELECT	LTRIM(RTRIM(EQUIV.CD_SISTEMA_ORIGEM ))
					FROM		SAC_BA_EQUIVALENCIAS 				EQUIV
					WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
					AND		EQUIV.CD_SISTEMA_DESTINO	= LASTRO.IDDIR_CD 
					AND		EQUIV.CD_EQUIVALENCIA			= 'IDX') END + ';' + 
		LTRIM(RTRIM(ISNULL(LASTRO.IC_TAXA_CDI	,''))) + ';' +
		convert(varchar,ISNULL(LASTRO.PC_MTM, 0.0))	 + ';' +
		convert(varchar,ISNULL(POS.VL_COUPON,0.0))	 + ';' +
		convert(varchar,(ISNULL(POS.QT_DISPONIVEL, 0) + ISNULL(POS.QT_BLOQUEADA, 0)) * ISNULL(POS.VL_PU_MERCADO, 0)) + ';' +
		CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
					 FROM	SAC_BA_EQUIVALENCIAS 				EQUIV
					WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
					AND		EQUIV.CD_SISTEMA_DESTINO	= TERMO.RVPAP_CD 
					AND		EQUIV.CD_EQUIVALENCIA			= 'PRV') IS NULL THEN LTRIM(RTRIM(ISNULL(TERMO.RVPAP_CD ,'')))
		ELSE (SELECT	LTRIM(RTRIM(EQUIV.CD_SISTEMA_ORIGEM ))
						FROM	SAC_BA_EQUIVALENCIAS 				EQUIV 
					 WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
						 AND	EQUIV.CD_SISTEMA_DESTINO	= TERMO.RVPAP_CD 
						 AND	EQUIV.CD_EQUIVALENCIA			= 'PRV') END + ';' + 
		ISNULL(LTRIM(RTRIM(LASTRO.CD)) ,'')+ ';' +
		ISNULL(LTRIM(RTRIM(OPER.CLCLI_CD)) ,'')+ ';' AS LINHA 
FROM	SAC_RF_POS POS
			INNER JOIN SAC_RF_OPERACAO 		OPER
				ON OPER.CD	=	POS.RFOP_CD
			INNER JOIN SAC_RF_LASTRO LASTRO
				ON OPER.RFLAS_CD		=	LASTRO.CD
			LEFT OUTER JOIN SAC_RF_LASTRO_TERMO TERMO 
				ON LASTRO.CD = TERMO.RFLAS_CD
			INNER JOIN SAC_RF_TIPO	TIPO
				ON TIPO.CD					=	LASTRO.RFTP_CD
			INNER JOIN CLI_BAS CLI
				ON OPER.CLCLI_CD		=	CLI.CODCLI 
				
WHERE	POS.DT = '{0}'
AND CLI.CODCLI IN (	'{1}') 
AND CLI.FLAGDEL			=	0
AND (ISNULL(POS.QT_DISPONIVEL, 0) + ISNULL(POS.QT_BLOQUEADA, 0)) <> 0
