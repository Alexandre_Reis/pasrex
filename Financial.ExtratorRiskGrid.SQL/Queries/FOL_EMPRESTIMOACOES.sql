SELECT DISTINCT 
		'15' + ';' +
		LTRIM(RTRIM(ISNULL(CLI.Nome,''))) + ';' +
		ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.Cpfcnpj,''),'.',''),'-',''),'/','')))	+ ';'  +
		ISNULL(substring(CONVERT(varchar,POS.DataHistorico,20),1,10),'') 	+ ';' +
		LTRIM(RTRIM(ISNULL(POS.CdAtivoBolsa,''))) + ';' +

		 LTRIM(RTRIM(ISNULL(POS.TipoEmprestimo,''))) + ';' +

		convert(varchar,ISNULL(POS.Quantidade, 0.0))+ ';' +
		convert(varchar,ISNULL(POS.ValorMercado, 0.0))+ ';' +

		 ISNULL(substring(CONVERT(varchar,pos.DataVencimento,20),1,10),'') 	+ ';' AS LINHA
FROM  (SELECT Pessoa.Nome, Pessoa.IdPessoa, Cliente.IdMoeda, Cliente.IdCliente, Carteira.IdCarteira, Pessoa.Cpfcnpj
		FROM Pessoa 
		INNER JOIN Cliente
			ON Pessoa.IdPessoa = Cliente.IdCliente
		INNER JOIN Carteira
			ON Cliente.IdCliente = Carteira.IdCarteira ) CLI
	
	INNER JOIN PosicaoEmprestimoBolsaHistorico POS
		ON CLI.IdCliente = POS.IdCliente 

WHERE   CLI.IdCarteira IN (	'{1}') 
and		POS.DataHistorico = '{0}'