﻿SELECT	'04' + ';' + 
				substring(CONVERT(varchar,POS.DT,20),1,10) 	+ ';' +
				ltrim(rtrim(CLI.CODCLI))+ ';' + 
				ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.CGC,''),'.',''),'-',''),'/','')))	+ ';' +
				ltrim(rtrim(CLI.NOME))+ ';' + 

				CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
										 FROM	SAC_BA_EQUIVALENCIAS				EQUIV
										WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
										AND		EQUIV.CD_SISTEMA_DESTINO	= CAD.RVCOR_CD_CONTRA 
										AND		EQUIV.CD_EQUIVALENCIA			= 'BEP') IS NULL THEN LTRIM(RTRIM(ISNULL(CAD.RVCOR_CD_CONTRA ,'')))
				ELSE (SELECT	LTRIM(RTRIM(EQUIV.CD_SISTEMA_ORIGEM))
							FROM		SAC_BA_EQUIVALENCIAS 				EQUIV
							WHERE		EQUIV.CD_CONTRAPARTE			= 'SPC' 
							AND			EQUIV.CD_SISTEMA_DESTINO	= CAD.RVCOR_CD_CONTRA 
							AND			EQUIV.CD_EQUIVALENCIA			= 'BEP') END + ';' + 

				substring(CONVERT(varchar,MOV.DT,20),1,10) 	+ ';' +
				ltrim(rtrim(isnull(CAD.IC_GARANTIA,''))) + ';' +
				CONVERT(varchar, ISNULL(POS.QT, 0))	+ ';' +
				ltrim(rtrim(isnull(MOV.RFOP_CD,''))) + ';' +
				CONVERT(varchar, ISNULL(CAD.PC_TAXA_ATIVO, 0))+ ';' +
				ltrim(rtrim(isnull(CAD.IC_JUROS_ATIVO,''))) + ';' +
				CONVERT(varchar, ISNULL(CAD.DD_BASE_JUROS_ATIVO, 0))+ ';' +
				CONVERT(varchar, ISNULL(CAD.PC_TAXA_PASSIVO, 0))+ ';' +
				ltrim(rtrim(isnull(CAD.IC_JUROS_PASSIVO,''))) + ';' +
				CONVERT(varchar, ISNULL(CAD.DD_BASE_JUROS_ATIVO, 0))+ ';' +
				isnull(substring(CONVERT(varchar,CAD.DT_VENCIMENTO,20),1,10),'') 	+ ';' +
				CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
										 FROM	SAC_BA_EQUIVALENCIAS				EQUIV 
										WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
										AND		EQUIV.CD_SISTEMA_DESTINO	= CAD.IDDIR_CD_ATIVO 
										AND		EQUIV.CD_EQUIVALENCIA			= 'IDX') IS NULL THEN ltrim(rtrim(isnull(CAD.IDDIR_CD_ATIVO ,'')))
				ELSE (SELECT	ltrim(rtrim(EQUIV.CD_SISTEMA_ORIGEM ))
							FROM		SAC_BA_EQUIVALENCIAS 				EQUIV
							WHERE		EQUIV.CD_CONTRAPARTE			= 'SPC' 
							AND			EQUIV.CD_SISTEMA_DESTINO	= CAD.IDDIR_CD_ATIVO 
							AND			EQUIV.CD_EQUIVALENCIA			= 'IDX') END + ';' + 

				ltrim(rtrim(isnull(IC_INDICE_ATIVO,''))) + ';' +
				ltrim(rtrim(isnull(CAD.IC_TAXA_CDI_ATIVO,''))) + ';' +
				CONVERT(varchar, ISNULL(CAD.VL_COTACAO_INICIAL_A, 0)) + ';' +
				CONVERT(varchar, ISNULL(CAD.PC_MTM_ATIVO, 0 )) + ';' +
				CONVERT(varchar, ISNULL(POS.VL_ATIVO_MTM, 0)) + ';' +

				CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
										 FROM	SAC_BA_EQUIVALENCIAS				EQUIV
										WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
										AND		EQUIV.CD_SISTEMA_DESTINO	= CAD.IDDIR_CD_PASSIVO 
										AND		EQUIV.CD_EQUIVALENCIA			= 'IDX') IS NULL THEN ltrim(rtrim(isnull(CAD.IDDIR_CD_PASSIVO,'')))
				ELSE (SELECT	ltrim(rtrim(EQUIV.CD_SISTEMA_ORIGEM ))
							FROM		SAC_BA_EQUIVALENCIAS 				EQUIV 
							WHERE		EQUIV.CD_CONTRAPARTE			= 'SPC' 
							AND			EQUIV.CD_SISTEMA_DESTINO	= CAD.IDDIR_CD_PASSIVO 
							AND			EQUIV.CD_EQUIVALENCIA			= 'IDX') END	+ ';' + 

				ltrim(rtrim(isnull(IC_INDICE_PASSIVO,''))) + ';' +
				ltrim(rtrim(isnull(CAD.IC_TAXA_CDI_PASSIVO,''))) + ';' +
				CONVERT(varchar, ISNULL(CAD.VL_COTACAO_INICIAL_P, 0)) + ';' +
				CONVERT(varchar, ISNULL(CAD.PC_MTM_PASSIVO, 0))	+ ';' +
				CONVERT(varchar, ISNULL(POS.VL_PASSIVO_MTM, 0))	+ ';' +

				CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
										 FROM	SAC_BA_EQUIVALENCIAS				EQUIV 
										WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
										AND		EQUIV.CD_SISTEMA_DESTINO	= CAD.BAMDA_CD 
										AND		EQUIV.CD_EQUIVALENCIA			= 'MDA') IS NULL THEN ltrim(rtrim(isnull(CAD.BAMDA_CD ,'')))
				ELSE (SELECT	ltrim(rtrim(EQUIV.CD_SISTEMA_ORIGEM ))
							FROM		SAC_BA_EQUIVALENCIAS 				EQUIV
							WHERE		EQUIV.CD_CONTRAPARTE			= 'SPC' 
							AND			EQUIV.CD_SISTEMA_DESTINO	= CAD.BAMDA_CD 
							AND			EQUIV.CD_EQUIVALENCIA			= 'MDA') END + ';' + 

				ltrim(rtrim(isnull(MOV.IC_NEG_VENC,''))) + ';' +
				isnull(substring(CONVERT(varchar,CAD.DT_INI_VALORIZACAO,20),1,10),'')	+ ';' AS LINHA

FROM		SAC_SW2_POS		POS
			INNER JOIN SAC_SW2_MOV	MOV
				ON MOV.SWCAD_CD	= POS.SWCAD_CD AND	MOV.CLCLI_CD = POS.CLCLI_CD 
			INNER JOIN SAC_SW_CAD	CAD
				ON CAD.CD = MOV.SWCAD_CD AND MOV.IC_TIPO_MOV IN ('C', 'D') 
			INNER JOIN CLI_BAS CLI
				ON  CLI.CODCLI	=	POS.CLCLI_CD 

WHERE POS.DT = '{0}'
AND			CLI.CODCLI IN (	'{1}') 
AND			CLI.FLAGDEL	=	0 
AND			ISNULL(POS.QT, 0)	<> 0