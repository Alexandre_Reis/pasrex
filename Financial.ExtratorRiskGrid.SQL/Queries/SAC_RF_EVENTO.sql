﻿SELECT	DISTINCT 
				'02' + ';' + 
				LTRIM(RTRIM(EVE.RFLAS_CD))+ ';' + 
				ISNULL(substring(CONVERT(varchar,EVE.DT,20),1,10),'')+ ';' + 
				LTRIM(RTRIM(EVE.SG))+ ';' + 
				convert(varchar,ISNULL(EVE.VL_JUROS, 0))+ ';' AS LINHA
FROM		SAC_RF_POS POS
			INNER JOIN SAC_RF_OPERACAO	OPERACAO
				ON POS.RFOP_CD = OPERACAO.CD 
			INNER JOIN SAC_RF_LASTRO LASTRO
				ON OPERACAO.RFLAS_CD = LASTRO.CD 
			INNER JOIN SAC_RF_EVENTO EVE
				ON LASTRO.CD = EVE.RFLAS_CD 
WHERE		OPERACAO.CLCLI_CD IN ('{1}') 
AND			EVE.DT	>= '{0}'
AND			LASTRO.IC_INATIVO				<> 'S' 
AND			POS.DT = '{0}'
AND			ISNULL(EVE.VL_JUROS, 0)	<> 0