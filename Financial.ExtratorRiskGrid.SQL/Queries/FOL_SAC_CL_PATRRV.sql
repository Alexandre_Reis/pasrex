SELECT	DISTINCT 
				'03' + ';' + 
				CLI.Nome + ';' + 
				ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.Cpfcnpj,''),'.',''),'-',''),'/','')))	+ ';' +
				substring(CONVERT(varchar,PATRRV.DataHistorico,20),1,10) 	+ ';' +
				RTRIM(LTRIM(ISNULL(PATRRV.CdAtivoBolsa,'')))+ ';' + 
				CONVERT(varchar,ISNULL(PATRRV.Quantidade, 0)) + ';' + 
				RTRIM(LTRIM(ISNULL(PATRRV.TipoMercado,'')))+ ';' + 
				'' + ';' +  -- FLEX.IC_TIPO_OPCAO
				RTRIM(LTRIM(ISNULL(Moeda.Nome,'')))+ ';' + 
				'N' + ';' +  -- CASE WHEN (CADPAP.TIPO = 'C') THEN 'S' ELSE 'N' END	+ ';' + 
				CONVERT(VARCHAR,ISNULL(CADPAP.PUExercicio, 0))	+ ';' + 
				RTRIM(LTRIM(ISNULL(CADPAP.CdAtivoBolsaObjeto,'')))+ ';' +
				ISNULL(substring(CONVERT(varchar,CADPAP.DataVencimento,20),1,10),'') 	+ ';' +
				LTRIM(RTRIM(ISNULL('SP',''))) + ';' + 
				CONVERT(VARCHAR, ISNULL(0, 0))+ ';' +  -- CADPAP.TAMANHO_CONTRATO
				'' + ';' +  -- ISNULL(substring(CONVERT(varchar,FLEX.DT_PGTO_PREMIO,20),1,10),'') + ';' + 
				'' + ';' +  -- CONVERT(VARCHAR, ISNULL(FLEX.VL_KNOCK_IN, 0))+ ';' + 
				'' + ';' +  -- LTRIM(RTRIM(ISNULL(FLEX.IC_PGTO_KNOCK_IN,''))) + ';' + 
				'' + ';' +  -- LTRIM(RTRIM(ISNULL(FLEX.IC_KNOCK_IN,''))) + ';' + 
				'' + ';' +  -- CONVERT(VARCHAR, ISNULL(FLEX.VL_PREMIO_KNOCK_IN, 0))+ ';' + 
				'' + ';' +  -- CONVERT(VARCHAR, ISNULL(FLEX.VL_KNOCK_OUT, 0))	+ ';' + 
				'' + ';' +  -- LTRIM(RTRIM(ISNULL(FLEX.IC_KNOCK_OUT,''))) + ';' + 
				'' + ';' +  -- LTRIM(RTRIM(ISNULL(FLEX.IC_PREMIO_KNOCK_OUT,''))) + ';' + 
				'' + ';' +  -- CONVERT(VARCHAR, ISNULL(FLEX.VL_PREMIO_KNOCK_OUT, 0))	+ ';' + 
				'' + ';' +  -- LTRIM(RTRIM(ISNULL(FLEX.IC_PGTO_KNOCK_OUT,''))) + ';' + 
				CONVERT(VARCHAR, ISNULL(PATRRV.PUMercado, 0))+ ';' + 
				CONVERT(VARCHAR, ISNULL(PATRRV.ValorMercado, 0))	+ ';' + 
				LTRIM(RTRIM(ISNULL(CADPAP.CodigoIsin,'')))	+ ';' + 
				LTRIM(RTRIM(ISNULL(cli.IdCarteira,''))) + ';'  as LINHA
FROM		(SELECT Pessoa.Nome, Pessoa.IdPessoa, Cliente.IdMoeda, Cliente.IdCliente, Carteira.IdCarteira, Pessoa.Cpfcnpj
		FROM Pessoa 
		INNER JOIN Cliente
			ON Pessoa.IdPessoa = Cliente.IdCliente
		INNER JOIN Carteira
			ON Cliente.IdCliente = Carteira.IdCarteira ) CLI
	
	INNER JOIN PosicaoBolsaHistorico PATRRV
		ON CLI.IdCliente = PATRRV.IdCliente 
		
	INNER JOIN AtivoBolsa CADPAP
		ON PATRRV.CdAtivoBolsa = CADPAP.CdAtivoBolsa
	
	INNER JOIN Moeda
		on CADPAP.IdMoeda = Moeda.IdMoeda
		
WHERE		CLI.IdCarteira IN (	'{1}') 
AND			PATRRV.DataHistorico =	'{0}'