SELECT	DISTINCT 
		'01' + ';' + 
		substring(CONVERT(varchar,POS.DataHistorico,20),1,10) 	+ ';' +
		LTRIM(RTRIM(ISNULL(CLI.Nome,''))) + ';' +
		'' + ';' + -- BACEN_CD
		ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.Cpfcnpj,''),'.',''),'-',''),'/','')))	+ ';' +
		LTRIM(RTRIM(ISNULL(LASTRO.DescricaoCompleta,'')))  + ';' 		+
		ltrim(rtrim(ISNULL(Moeda.Nome,'')))		+ ';' +
		LTRIM(RTRIM(ISNULL(LASTRO.CodigoCetip,'')))+ ';' +
		LTRIM(RTRIM(ISNULL(LASTRO.CodigoIsin,'')))+ ';' +
		LTRIM(RTRIM(ISNULL(Emissor.Nome,'')))+ ';' +
		
		substring(CONVERT(varchar,LASTRO.DataVencimento,20),1,10) 	+ ';' +
		substring(CONVERT(varchar,LASTRO.DataEmissao,20),1,10) 	+ ';' +
		
		
		ISNULL(substring(CONVERT(varchar,LASTRO.DataEmissao,20),1,10),'') 	+ ';' +
		ISNULL(substring(CONVERT(varchar,POS.DataOperacao,20),1,10),'') 	+ ';' +
		
		convert(varchar,convert(decimal(25,12),ISNULL(POS.PUOperacao, 0.0))) + ';' +
		convert(varchar,convert(decimal(25,12),ISNULL(POS.PUJuros, 0))		) + ';' +
		convert(varchar,convert(decimal(25,12),ISNULL(POS.PUJuros, 0.0))	) + ';' +
		convert(varchar,convert(decimal(25,12),ISNULL(LASTRO.PUNominal, 0.0))	) + ';' +
		
		case  POS.TipoOperacao 
			when 1 then 'N'
			else 'S'
		end + ';' +
			
		'N' + ';' +
		
		
		convert(varchar,convert(decimal(25,12),	ISNULL(POS.Quantidade, 0)) ) + ';' +
		convert(varchar,ISNULL(POS.TaxaOperacao, 0.0))+ ';' +
		'' + ';' + --ISNULL(TIPO.IC_JUROS	,'') + ';' +
		'' + ';' + --ISNULL(LASTRO.IC_FORCA_CALCULO_PADRONIZADO	,'') + ';' +
		'' + ';' + --ISNULL(TIPO.IC_CALCULO_PADRONIZADO,'') + ';' +
		convert(varchar,convert(decimal(25,12), ISNULL(100, 0.0))	)+ ';' + --LASTRO.IDDIR_PC
		convert(varchar,ISNULL(Indice.Descricao, 0.0))	 + ';' +
		
		LTRIM(RTRIM(ISNULL('N'	,''))) + ';' +
		convert(varchar,ISNULL(0, 0.0))	 + ';' + --LASTRO.PC_MTM
		convert(varchar,ISNULL(0,0.0))	 + ';' + -- POS.VL_COUPON
		convert(varchar,pos.ValorMercado) + ';' +
		'' + ';' + --rvpap_cd
		ISNULL(LTRIM(RTRIM(LASTRO.IdTitulo)) ,'')+ ';' +
		ISNULL(LTRIM(RTRIM(cli.IdCarteira)) ,'')+ ';' AS LINHA 
FROM	(SELECT Pessoa.Nome, Pessoa.IdPessoa, Cliente.IdMoeda, Cliente.IdCliente, Carteira.IdCarteira, Pessoa.Cpfcnpj
		FROM Pessoa 
		INNER JOIN Cliente
			ON Pessoa.IdPessoa = Cliente.IdCliente
		INNER JOIN Carteira
			ON Cliente.IdCliente = Carteira.IdCarteira ) CLI
	
	INNER JOIN PosicaoRendaFixaHistorico POS
		ON CLI.IdCliente = POS.IdCliente 
		
	INNER JOIN TituloRendaFixa LASTRO
		on POS.IdTitulo = LASTRO.IdTitulo
	
	LEFT OUTER JOIN Indice
		on LASTRO.IdIndice = Indice.IdIndice
		
	INNER JOIN Moeda
		ON Moeda.IdMoeda = LASTRO.IdMoeda
		
	INNER JOIN Emissor
		ON Emissor.IdEmissor = LASTRO.IdEmissor
			
				
WHERE	POS.DataHistorico = '{0}'
AND CLI.IdCarteira IN (	'{1}') 
