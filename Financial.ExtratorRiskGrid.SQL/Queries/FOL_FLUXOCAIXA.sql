SELECT	DISTINCT 
				'10'+ ';' +
				ISNULL(substring(CONVERT(varchar,CAIXA.DataHistorico,20),1,10),'') 	+ ';' +
				LTRIM(RTRIM(ISNULL(CLI.Nome	,''))) + ';' +
				ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.Cpfcnpj,''),'.',''),'-',''),'/','')))	+ ';'  +
				ltrim(rtrim(ISNULL(CONTA.Numero, ''))) + ltrim(rtrim(ISNULL(CONTA.DigitoConta, '')))	+ ';'  +
				ISNULL(substring(CONVERT(varchar,CAIXA.DataVencimento,20),1,10),'') 	+ ';' +

				LTRIM(RTRIM(ISNULL(Moeda.Nome	,''))) + ';' +
				convert(varchar,ISNULL(CONVERT(MONEY,sum(CAIXA.Valor)), 0.0))+ ';' +
			LTRIM(RTRIM(ISNULL(CLI.IdCarteira,''))) + ';' AS LINHA

FROM	(SELECT Pessoa.Nome, Pessoa.IdPessoa, Cliente.IdMoeda, Cliente.IdCliente, Carteira.IdCarteira, Pessoa.Cpfcnpj
		FROM Pessoa 
		INNER JOIN Cliente
			ON Pessoa.IdPessoa = Cliente.IdCliente
		INNER JOIN Carteira
			ON Cliente.IdCliente = Carteira.IdCarteira ) CLI
	
	INNER JOIN 	LiquidacaoHistorico	CAIXA
		ON CLI.IdCliente = CAIXA.IdCliente 
	
	LEFT OUTER JOIN ContaCorrente CONTA
		ON CAIXA.IdConta = CONTA.IdConta
		
	INNER JOIN Moeda
		ON CLI.IdMoeda = Moeda.IdMoeda
WHERE		CLI.IdCarteira IN (	'{1}') 
AND			CAIXA.DataHistorico =	'{0}'
GROUP
BY			CAIXA.DataHistorico, CLI.Nome, CLI.Cpfcnpj, CONTA.Numero, CONTA.DigitoConta, CAIXA.DataVencimento, Moeda.Nome, CLI.IdCarteira


--select * from ContaCorrente