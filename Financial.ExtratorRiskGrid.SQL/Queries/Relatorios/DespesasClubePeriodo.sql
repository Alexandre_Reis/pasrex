﻿SELECT 
CASE TIPO WHEN '041' THEN 'Taxa de Administração' 
	  WHEN '042' THEN 'Taxa de Performance' 
	  WHEN '052' THEN 'Auditoria' 
	  WHEN '048' THEN 'Outras Despesas' 
	  WHEN '156' THEN 'Taxa de Custódia' 
	  WHEN '698' THEN 'Taxa de Controladoria s/tx de Admin. Bruta' 
END 					DESCRICAO,
SUM(VALOR)				VALOR,
SUM(VALOR) / MAX(PATR.VL_PATRLIQTOT) 	PERC_PL
FROM MT_CAIXA, SAC_CL_PATR PATR
WHERE CODCLI = '{0}'
AND DATALIQ BETWEEN '{1}' AND '{2}'
AND TIPO IN ('041','042', '052', '048', '156', '698')
AND PATR.CLCLI_CD = CODCLI
AND PATR.DT = '{2}'
GROUP BY TIPO

UNION

SELECT 
'Corretagem' 				DESCRICAO,
SUM(CORCLI+VL_EMOLUMENTO)*-1 		VALOR,
SUM(VALOR)*-1 / MAX(PATR.VL_PATRLIQTOT) PERC_PL
FROM RV_MOV, SAC_CL_PATR PATR
WHERE CODCLI = '{0}'
AND DATAMOV BETWEEN '{1}' AND '{2}'
AND PATR.CLCLI_CD = CODCLI
AND PATR.DT = '{2}'
