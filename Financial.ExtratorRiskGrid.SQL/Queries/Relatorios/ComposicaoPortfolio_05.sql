--Analista filtros desta query
SELECT top 1 '05'                                                         TIPO_REG,   
         COT_FUNDO.CD_EMPRESA                                         CD_EMPRESA,   
         RENT_FUNDO.ID_SEQ                                            ID_SEQ,   
         0                                                            ORDEM,   
         COT_FUNDO.CD_FUNDO                                           CD_FUNDO,   
         ISNULL(COT_CLIENTE.NM_CLIENTE, ' ')                          NOME_FUNDO_INDEXADOR,   
         ISNULL(RENT_FUNDO.PC_MES, 0)                                 RENT_MES_ATUAL,   
         ISNULL(RENT_FUNDO.PC_MES_ANTERIOR, 0)                        RENT_MES_ANTERIOR,   
         ISNULL(RENT_FUNDO.PC_ANO, 0)                                 RENT_ANO_ATUAL,   
         ISNULL(RENT_FUNDO.PC_12_MESES, 0)                            RENT_12M,   
         RENT_FUNDO.IC_UP_RENTABILIDADE                               IC_UP_RENTABILIDADE,   
         RENT_FUNDO.IC_IF_TIPO                                        IC_IF_TIPO   
  FROM   DBO.SAT_RENTAB_FUNDO_INDEX  RENT_FUNDO,   
         BRD_ACT_YMF_COT..COT_FUNDO        COT_FUNDO,   
         BRD_ACT_YMF_COT..COT_CLIENTE      COT_CLIENTE    
  WHERE  RENT_FUNDO.IC_IF_TIPO = 'F'   
  AND    COT_FUNDO.CD_FUNDO    = RENT_FUNDO.CD_FUNDO_INDEX   
  AND    COT_FUNDO.CD_CLIENTE  = COT_CLIENTE.CD_CLIENTE   
  AND	 COT_FUNDO.CD_FUNDO	   = '{0}'
  UNION   
  SELECT DISTINCT   
         '05'                                                         TIPO_REG,   
         COT_FUNDO.CD_EMPRESA                                         CD_EMPRESA,   
         RENT_INDEX.ID_SEQ                                            ID_SEQ,   
         MIN(INDEXADOR.ID_ORDEM_INDEXADOR)                            ORDEM,   
         ''                                                           CD_FUNDO,   
         CONVERT(CHAR(50),   
         CASE WHEN '' = RTRIM(LTRIM(ISNULL(IDX.NOME, IDX.CODINDEX)))   
              THEN IDX.CODINDEX   
              ELSE ISNULL(IDX.NOME, IDX.CODINDEX) END)                NOME_FUNDO_INDEXADOR,   
         MAX(ISNULL(RENT_INDEX.PC_MES, 0))                            RENT_MES_ATUAL,   
         MAX(ISNULL(RENT_INDEX.PC_MES_ANTERIOR, 0))                   RENT_MES_ANTERIOR,   
         MAX(ISNULL(RENT_INDEX.PC_ANO, 0))                            RENT_ANO_ATUAL,   
         MAX(ISNULL(RENT_INDEX.PC_12_MESES, 0))                       RENT_12M,   
         MAX(RENT_INDEX.IC_UP_RENTABILIDADE)                          IC_UP_RENTABILIDADE,   
         MAX(RENT_INDEX.IC_IF_TIPO)                                   IC_IF_TIPO   
  FROM   DBO.SAT_RENTAB_FUNDO_INDEX             RENT_INDEX,   
         IDX_INDEXDIR                           IDX,   
         BRD_ACT_YMF_COT..COT_RENTAB_INDEXADOR_FUNDO  INDEXADOR,   
         BRD_ACT_YMF_COT..COT_FUNDO                   COT_FUNDO,   
         SAT_EXTRATO_SEQ                        EXTRATO   
  WHERE  RENT_INDEX.IC_IF_TIPO         = 'I'   
  AND    IDX.CODINDEX                  = RENT_INDEX.CD_FUNDO_INDEX   
  AND    INDEXADOR.CD_INDEXADOR        = IDX.CODINDEX   
  AND    INDEXADOR.CD_FUNDO            = COT_FUNDO.CD_FUNDO   
  AND    INDEXADOR.IC_UP_RENTABILIDADE = RENT_INDEX.IC_UP_RENTABILIDADE   
  AND    COT_FUNDO.CD_FUNDO            = EXTRATO.CD_VALOR   
  AND    EXTRATO.CD_COLUNA             = 'CD_FUNDO'   
  AND    EXTRATO.ID_SEQ                = RENT_INDEX.ID_SEQ
  GROUP BY COT_FUNDO.CD_EMPRESA,   
           RENT_INDEX.ID_SEQ,   
           IDX.CODINDEX,   
           IDX.NOME 