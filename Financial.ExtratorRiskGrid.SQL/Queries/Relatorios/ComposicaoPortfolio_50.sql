  SELECT DISTINCT '50'                                                     TIPO_REG, 
         COT_FUNDO.CD_EMPRESA                                              CD_EMPRESA, 
         EXTRATO.ID_SEQ                                                    ID_SEQ, 
         EXTRATO.CD_COTISTA                                                CD_COTISTA, 
         COT_FUNDO.CD_FUNDO                                                CD_FUNDO, 
         ISNULL(COT_CLIENTE_FUNDO.NM_ABREVIADO, ' ')                       APELIDO_FUNDO, 
         ISNULL(COT_CLIENTE_FUNDO.NM_CLIENTE, ' ')                         NOME_FUNDO, 
         CASE COT_FUNDO_IR.IC_FC_ALIQUOTA 
              WHEN 'F' THEN '6' 
              ELSE CASE COT_FUNDO_IR.IC_LC_CARTEIRA_FUNDO 
                   WHEN 'L' THEN '5' 
                   WHEN 'C' THEN '4' 
                   ELSE ' ' END END                                        TIPO_FUNDO, 
         RIGHT('00000000000000' + RTRIM(LTRIM(CONVERT(CHAR(14), 
         ISNULL(CONVERT(CHAR(14), COT_CLIENTE_FUNDO.NO_CGC), ' ')))), 14)  CNPJ_FUNDO, 
         COT_FUNDO_IR.DT_INICIO_IR                                         DT_INICIO_IR, 
         COT_FUNDO_IR.DT_FIM_IR                                            DT_FIM_IR 
  FROM   BRD_ACT_YMF_COT..COT_FUNDO_IR  COT_FUNDO_IR, 
         BRD_ACT_YMF_COT..COT_FUNDO     COT_FUNDO, 
         BRD_ACT_YMF_COT..COT_CLIENTE   COT_CLIENTE_FUNDO, 
         DBO.SAT_COT_EXTRATO_ARQ  EXTRATO 
  WHERE  COT_CLIENTE_FUNDO.CD_CLIENTE	 = COT_FUNDO.CD_CLIENTE 
		AND COT_FUNDO_IR.CD_FUNDO        = COT_FUNDO.CD_FUNDO 
		AND EXTRATO.CD_FUNDO             = COT_FUNDO.CD_FUNDO
		AND EXTRATO.CD_COTISTA			 = '{0}'
		--AND EXTRATO.DT_INI = '20110831'
		--AND EXTRATO.DT_FIM = '20110930'