﻿
  SELECT DISTINCT TOP 1 
         '70'                                                       TIP_REG, 
         RENT_FUNDO.ID_SEQ                                          ID_SEQ, 
         COT_FUNDO.CD_FUNDO                                         CD_FUNDO, 
         ISNULL(EXTRATO.CD_COTISTA, ' ')                            CD_COTISTA, 
         ISNULL(COT_CLIENTE_FUNDO.NM_ABREVIADO, ' ')                APELIDO_FUNDO, 
         'Rentabilidade do fundo no mês :'                          LABEL_RENT_MES_ATUAL, 
         ISNULL(RENT_FUNDO.PC_MES, CONVERT(FLOAT, 0.00))            RENT_MES_ATUAL, 
         'Últ. 6 meses :'                                           LABEL_RENT_6M, 
         ISNULL(RENT_FUNDO.PC_6_MESES, CONVERT(FLOAT, 0.00))        RENT_6M, 
         '2Calculado com base nas cotas do último dia útil do mês'  LABEL_CALC, 
         '3Resgate por Ordem Judicial'                              LABEL_RESG_JUDICIAL, 
         '4Curto Prazo para fins tributários'                       LABEL_CP, 
         '5Longo Prazo para fins tributários'                       LABEL_LP, 
         '6Ações'                                                   LABEL_ACOES 
  FROM   DBO.SAT_RENTAB_FUNDO_INDEX  RENT_FUNDO, 
         BRD_ACT_YMF_COT..COT_FUNDO        COT_FUNDO, 
         BRD_ACT_YMF_COT..COT_CLIENTE      COT_CLIENTE_FUNDO, 
         DBO.SAT_COT_EXTRATO_ARQ     EXTRATO 
  WHERE  RENT_FUNDO.IC_IF_TIPO = 'F' 
  AND    COT_FUNDO.CD_FUNDO    = RENT_FUNDO.CD_FUNDO_INDEX 
  AND    COT_FUNDO.CD_CLIENTE  = COT_CLIENTE_FUNDO.CD_CLIENTE 
  AND    EXTRATO.CD_FUNDO      = COT_FUNDO.CD_FUNDO

  AND    EXTRATO.CD_COTISTA = '{0}'       
  AND    EXTRATO.DT_MOVIMENTO BETWEEN '{1}' AND '{2}'
  --AND    EXTRATO.CD_FUNDO = '2708' 