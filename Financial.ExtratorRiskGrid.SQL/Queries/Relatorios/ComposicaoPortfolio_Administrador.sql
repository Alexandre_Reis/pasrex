
SELECT      TOP 1     
            C.NO_CGC, 
            CA.NM_CLIENTE AS NM_ADM, 
            CA.NO_CGC CNPJ_ADM, 
            rtrim(ISNULL( E.DS_LOGRADOURO, '' ))  + '    ' + 
            rtrim(ISNULL(SUBSTRING(e.NR_CEP,1,5) + '-' +  SUBSTRING(E.NR_CEP,6,3), '' ))  + '    ' + 
            rtrim(ISNULL( E.NM_CIDADE, '' )) + '    ' + 
            rtrim(ISNULL( E.SG_UF, '' )) ENDERECO_ADM 
            
            FROM              
            
				COT_POSICAO_NOTA_HISTORICO PNH,  
				COT_FUNDO F, 
				COT_CLIENTE C,  
				COT_MOVIMENTO M, 
				COT_ADMINISTRADOR A, 
				COT_CLIENTE CA LEFT OUTER JOIN COT_ENDERECO E ON CA.CD_CLIENTE = E.CD_CLIENTE 
	            
            WHERE                       
			F.CD_ADMINISTRADOR = A.CD_ADMINISTRADOR AND 
            A.CD_CLIENTE = CA.CD_CLIENTE AND 
            E.CD_TIPO_ENDERECO = 'P' AND 
            PNH.ID_NOTA = M.ID_NOTA AND 
            M.CD_PADRAO <> 'V' AND  
            M.DT_LIQUIDACAO_FISICA <= PNH.DT_POSICAO AND 
            PNH.CD_FUNDO = F.CD_FUNDO AND 
            F.CD_CLIENTE = C.CD_CLIENTE  
			--and 
	        --F.CD_FUNDO  in ('{0}')