SELECT '16'                                    TIP_REG, 
         EXTRATO_RESUMO.CD_EMPRESA               CD_EMPRESA, 
         ISNULL(EXTRATO_RESUMO.CD_COTISTA, ' ')  CD_COTISTA, 
		 ISNULL(EXTRATO_RESUMO.DS_TIPO_FUNDO, ' ')      CLASSIFICACAO, 
		 ISNULL(EXTRATO_RESUMO.CD_FUNDO, ' ')	CD_FUNDO,
		 ISNULL(EXTRATO_RESUMO.APELIDO_FUNDO, ' ')NM_FUNDO,
         EXTRATO_RESUMO.ID_SEQ                   ID_SEQ, 
         CONVERT(CHAR(38), ' ')                  FILLER, 
         EXTRATO_RESUMO.SALDO_FIM_BRUTO          SALDO_FIM_BRUTO, 
         EXTRATO_RESUMO.SALDO_FIM_LIQUIDO        SALDO_FIM_LIQUIDO 
  FROM   DBO.SAT_COT_EXTRATO_RESUMO  EXTRATO_RESUMO,
		 BRD_ACT_YMF_COT..COT_FUNDO          COT_FUNDO, 
		 BRD_ACT_YMF_COT..COT_ADMINISTRADOR  COT_ADMINISTRADOR 
		WHERE COT_FUNDO.CD_FUNDO         = EXTRATO_RESUMO.CD_FUNDO 
			AND    COT_FUNDO.CD_ADMINISTRADOR = COT_ADMINISTRADOR.CD_ADMINISTRADOR 
			AND EXTRATO_RESUMO.CD_COTISTA = '{0}'
			AND EXTRATO_RESUMO.DT_INI = '{1}'
			AND EXTRATO_RESUMO.DT_FIM = '{2}'