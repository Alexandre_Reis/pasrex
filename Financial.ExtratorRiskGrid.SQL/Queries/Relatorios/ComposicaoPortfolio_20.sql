  SELECT TOP 1
		 '20'                                      TIP_REG, 
         EXTRATO_RESUMO.CD_EMPRESA                 CD_EMPRESA, 
         EXTRATO_RESUMO.CD_COTISTA                 CD_COTISTA, 
         EXTRATO_RESUMO.ID_SEQ                     ID_SEQ, 
         EXTRATO_RESUMO.DT_INI                     DT_INI, 
         EXTRATO_RESUMO.SALDO_INI                  SALDO_INI, 
         EXTRATO_RESUMO.APLICACOES                 APLICACOES, 
         EXTRATO_RESUMO.RESGATES_FINANCEIRO        RESGATES, 
         EXTRATO_RESUMO.REND_BRUTO                 REND_BRUTO, 
         EXTRATO_RESUMO.DT_FIM                     DT_FIM, 
         EXTRATO_RESUMO.SALDO_FIM_BRUTO            SALDO_FIM_BRUTO, 
         EXTRATO_RESUMO.SALDO_FIM_LIQUIDO          SALDO_FIM_LIQUIDO, 
         EXTRATO_RESUMO.SALDO_BLOQ_JUD_FINANCEIRO  SALDO_BLOQ_JUD, 
         EXTRATO_RESUMO.BLOQ_JUD_FINANCEIRO        BLOQ_JUD, 
         EXTRATO_RESUMO.DESBLOQ_JUD_FINANCEIRO     DESBLOQ_JUD, 
         EXTRATO_RESUMO.TRANSF_JUD_FINANCEIRO      TRANSF_JUD 
  FROM   DBO.SAT_COT_EXTRATO_RESUMO  EXTRATO_RESUMO 
		WHERE EXTRATO_RESUMO.CD_COTISTA = '{0}'
		--AND EXTRATO_RESUMO.DT_INI = '{1}'
		--AND EXTRATO_RESUMO.DT_FIM = '{2}'
