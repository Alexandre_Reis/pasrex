﻿SELECT 
CASE C.TIPO 
	WHEN 'V' THEN 'Ação'
	WHEN 'P' THEN 'Opção'
	WHEN 'C' THEN 'Opção'
	WHEN 'F' THEN 'Futuro de Ações'
END TIPO_ATIVO,
P.CODPAP COD_NEGOCIACAO,
E.CODFANT EMISSOR,
CASE C.TIPO 
	WHEN 'P' THEN C.RVPAP_CD_OPCAO
	WHEN 'C' THEN C.RVPAP_CD_OPCAO
	WHEN 'F' THEN C.RVPAP_CD_OPCAO
	ELSE NULL
END ATIVO_SUBJACENTE,
CASE C.TIPO 
	WHEN 'P' THEN C.DATAVCTO
	WHEN 'C' THEN C.DATAVCTO
	WHEN 'F' THEN C.DATAVCTO
	ELSE NULL
END VENCIMENTO,
P.QTDBLOQ + P.QTDDISP QUANTIDADE,
P.VL_MERC VALOR_MERCADO,
P.VL_MERC / PATR.VL_PATRLIQTOT PERC_PL
FROM SAC_CL_PATR PATR, RV_POS P, RV_CADPAP C, BAS_CAD_EMP E
WHERE PATR.CLCLI_CD = '{0}'
AND PATR.DT = '{2}'
AND PATR.CLCLI_CD = P.CODCLI
AND PATR.DT = P.DATAPOS
AND P.CODPAP = C.CODPAP
AND C.CODEMP = E.CODEMP

UNION

-- Posição RF
SELECT 
CASE T.IC_PUB_PRIV 
	WHEN 'U' THEN 'Título Público'
	WHEN 'I' THEN 'Título Privado'
END TIPO_ATIVO,
T.CD COD_NEGOCIACAO,
I.NOMEINST EMISSOR,
NULL ATIVO_SUBJACENTE,
L.DT_VENCIMENTO VENCIMENTO,
P.QT_BLOQUEADA + P.QT_DISPONIVEL QUANTIDADE,
P.VL_LIQ_POS VALOR_MERCADO,
P.VL_LIQ_POS / PATR.VL_PATRLIQTOT PERC_PL
FROM SAC_CL_PATR PATR, SAC_RF_POS P, SAC_RF_OPERACAO O, SAC_RF_TIPO T, SAC_RF_LASTRO L, BAS_CAD_IF I
WHERE PATR.CLCLI_CD = '{0}'
AND PATR.DT = '{2}'
AND PATR.CLCLI_CD = O.CLCLI_CD
AND PATR.DT = P.DT
AND P.RFOP_CD = O.CD 
AND O.RFLAS_CD = L.CD
AND L.RFTP_CD = T.CD
AND L.BAINS_CD = I.CODINST

UNION

-- Fundos
SELECT 
'Fundo' 			TIPO_ATIVO,
C.CD 				COD_NEGOCIACAO,
I.NOMEINST 			EMISSOR,
NULL 				ATIVO_SUBJACENTE,
NULL 				VENCIMENTO,
P.QT_COTAS 			QUANTIDADE,
P.VL_LIQ 			VALOR_MERCADO,
P.VL_LIQ / PATR.VL_PATRLIQTOT 	PERC_PL
FROM SAC_CL_PATR PATR, SAC_FI_POS P, SAC_FI_CADASTRO C, BAS_CAD_IF I
WHERE PATR.CLCLI_CD = '{0}'
AND PATR.DT = '{2}'
AND PATR.CLCLI_CD = P.CLCLI_CD
AND PATR.DT = P.DT
AND P.FICAD_CD = C.CD
AND C.BAINS_CD = I.CODINST