SELECT	DISTINCT 
convert(varchar,IdCliente)		 			AS CODIGO, 
Nome   	 					AS CLIENTE, 
ISNULL(CONVERT(CHAR(10),DataDia, 103),'')	AS DT_PROC_CL, 
 ''			AS IC_COTA_LIBERADA, 
 ''			AS DT_COTA_LIBERADA, 
 ''			AS IC_CTB_LIBERADO, 
 ''			AS DT_CTB_LIBERADO 
FROM	Cliente	
			INNER JOIN GrupoProcessamento
				on Cliente.IdGrupoProcessamento = GrupoProcessamento.IdGrupoProcessamento
where GrupoProcessamento.IdGrupoProcessamento = '{0}'
order by 2
			