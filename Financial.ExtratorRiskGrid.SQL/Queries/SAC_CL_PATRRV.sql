﻿SELECT	DISTINCT 
				'03' + ';' + 
				CLI.NOME + ';' + 
				ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.CGC,''),'.',''),'-',''),'/','')))	+ ';' +
				substring(CONVERT(varchar,PATRRV.DT,20),1,10) 	+ ';' +
				CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
										 FROM	SAC_BA_EQUIVALENCIAS				EQUIV
										WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
										AND		EQUIV.CD_SISTEMA_DESTINO	= PATRRV.RVPAP_CD 
										AND		EQUIV.CD_EQUIVALENCIA			= 'PRV') IS NULL THEN LTRIM(RTRIM(ISNULL(PATRRV.RVPAP_CD,'')))
				ELSE (SELECT	LTRIM(RTRIM(EQUIV.CD_SISTEMA_ORIGEM ))
							FROM		SAC_BA_EQUIVALENCIAS 				EQUIV 
							WHERE		EQUIV.CD_CONTRAPARTE			= 'SPC' 
							AND			EQUIV.CD_SISTEMA_DESTINO	= PATRRV.RVPAP_CD 
							AND			EQUIV.CD_EQUIVALENCIA			= 'PRV') END + ';' + 
				CONVERT(varchar,ISNULL(QT_TOTAL, 0)) + ';' + 
				RTRIM(LTRIM(ISNULL(CADPAP.TIPO,'')))+ ';' + 
				RTRIM(LTRIM(ISNULL(FLEX.IC_TIPO_OPCAO,'')))+ ';' + 
				CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
										 FROM	SAC_BA_EQUIVALENCIAS 				EQUIV
										WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
										AND		EQUIV.CD_SISTEMA_DESTINO	= CADPAP.CODMOEDA 
										AND		EQUIV.CD_EQUIVALENCIA			= 'MDA') IS NULL THEN RTRIM(LTRIM(ISNULL(CADPAP.CODMOEDA ,'')))
				ELSE (SELECT	RTRIM(LTRIM(EQUIV.CD_SISTEMA_ORIGEM ))
							FROM		SAC_BA_EQUIVALENCIAS 				EQUIV
							WHERE		EQUIV.CD_CONTRAPARTE			= 'SPC' 
							AND			EQUIV.CD_SISTEMA_DESTINO	= CADPAP.CODMOEDA 
							AND			EQUIV.CD_EQUIVALENCIA			= 'MDA') END + ';' + 
				CASE WHEN (CADPAP.TIPO = 'C') THEN 'S' ELSE 'N' END	+ ';' + 
				CONVERT(VARCHAR,ISNULL(CADPAP.EXERC, 0))	+ ';' + 
				CASE WHEN (SELECT	EQUIV.CD_SISTEMA_ORIGEM 
										 FROM	SAC_BA_EQUIVALENCIAS				EQUIV
										WHERE	EQUIV.CD_CONTRAPARTE			= 'SPC' 
										AND		EQUIV.CD_SISTEMA_DESTINO	= CADPAP.RVPAP_CD_OPCAO 
										AND		EQUIV.CD_EQUIVALENCIA			= 'PRV') IS NULL THEN LTRIM(RTRIM(ISNULL(CADPAP.RVPAP_CD_OPCAO ,'')))
				ELSE (SELECT	LTRIM(RTRIM(EQUIV.CD_SISTEMA_ORIGEM ))
							FROM		SAC_BA_EQUIVALENCIAS 				EQUIV
							WHERE		EQUIV.CD_CONTRAPARTE			= 'SPC' 
							AND			EQUIV.CD_SISTEMA_DESTINO	= CADPAP.RVPAP_CD_OPCAO 
							AND			EQUIV.CD_EQUIVALENCIA			= 'PRV') END + ';' + 
				ISNULL(substring(CONVERT(varchar,CADPAP.DATAVCTO,20),1,10),'') 	+ ';' +
				LTRIM(RTRIM(ISNULL(PATRRV.CD_PRACA,''))) + ';' + 
				CONVERT(VARCHAR, ISNULL(CADPAP.TAMANHO_CONTRATO, 0))+ ';' + 
				ISNULL(substring(CONVERT(varchar,FLEX.DT_PGTO_PREMIO,20),1,10),'') + ';' + 
				CONVERT(VARCHAR, ISNULL(FLEX.VL_KNOCK_IN, 0))+ ';' + 
				LTRIM(RTRIM(ISNULL(FLEX.IC_PGTO_KNOCK_IN,''))) + ';' + 
				LTRIM(RTRIM(ISNULL(FLEX.IC_KNOCK_IN,''))) + ';' + 
				CONVERT(VARCHAR, ISNULL(FLEX.VL_PREMIO_KNOCK_IN, 0))+ ';' + 
				CONVERT(VARCHAR, ISNULL(FLEX.VL_KNOCK_OUT, 0))	+ ';' + 
				LTRIM(RTRIM(ISNULL(FLEX.IC_KNOCK_OUT,''))) + ';' + 
				LTRIM(RTRIM(ISNULL(FLEX.IC_PREMIO_KNOCK_OUT,''))) + ';' + 
				CONVERT(VARCHAR, ISNULL(FLEX.VL_PREMIO_KNOCK_OUT, 0))	+ ';' + 
				LTRIM(RTRIM(ISNULL(FLEX.IC_PGTO_KNOCK_OUT,''))) + ';' + 
				CONVERT(VARCHAR, ISNULL(PATRRV.VL_COTACAO, 0))+ ';' + 
				CONVERT(VARCHAR, ISNULL(PATRRV.VL_MERC, 0))	+ ';' + 
				LTRIM(RTRIM(ISNULL(CADPAP.ISIN,'')))	+ ';' + 
				LTRIM(RTRIM(ISNULL(PATRRV.CLCLI_CD,''))) + ';'  as LINHA
FROM		SAC_CL_PATRRV       PATRRV
			INNER JOIN  RV_CADPAP CADPAP
				ON CADPAP.CODPAP =	PATRRV.RVPAP_CD 
			INNER JOIN CLI_BAS CLI
				ON CLI.CODCLI =	PATRRV.CLCLI_CD 
			LEFT OUTER JOIN SAC_RV_CADPAP_FLEX  FLEX 
				ON PATRRV.RVPAP_CD	= FLEX.RVPAP_CD
WHERE		PATRRV.DT =	'{0}'
AND			CLI.CODCLI IN (	'{1}') 
AND			ISNULL(QT_TOTAL, 0)	<> 0
