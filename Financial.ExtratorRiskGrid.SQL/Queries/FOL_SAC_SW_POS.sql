SELECT	'04' + ';' + 
				substring(CONVERT(varchar,POS.DataHistorico,20),1,10) 	+ ';' +
				ltrim(rtrim(CLI.IdCarteira))+ ';' + 
				ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.Cpfcnpj,''),'.',''),'-',''),'/','')))	+ ';' +
				ltrim(rtrim(CLI.Nome))+ ';' + 

				''+ ';' + -- contraparte

				substring(CONVERT(varchar,POS.DataEmissao,20),1,10) 	+ ';' +
				''+ ';' + --ltrim(rtrim(isnull(CAD.IC_GARANTIA,''))) + ';' +
				CONVERT(varchar, ISNULL(POS.ValorBase, 0))	+ ';' +
				''+ ';' + --ltrim(rtrim(isnull(MOV.RFOP_CD,''))) + ';' +
				CONVERT(varchar, ISNULL(POS.Percentual, 0))+ ';' +
				''+ ';' + --ltrim(rtrim(isnull(CAD.IC_JUROS_ATIVO,''))) + ';' +
				CONVERT(varchar, ISNULL(POS.BaseAno, 0))+ ';' +
				CONVERT(varchar, ISNULL(POS.PercentualContraParte, 0))+ ';' +
				''+ ';' + --ltrim(rtrim(isnull(CAD.IC_JUROS_PASSIVO,''))) + ';' +
				CONVERT(varchar, ISNULL(POS.BaseAnoContraParte, 0))+ ';' +
				isnull(substring(CONVERT(varchar,POS.DataVencimento,20),1,10),'') 	+ ';' +
				ltrim(rtrim(IndiceAtivo.Descricao))+ ';' + 
				
				''+ ';' + --ltrim(rtrim(isnull(IC_INDICE_ATIVO,''))) + ';' +
				''+ ';' + --ltrim(rtrim(isnull(CAD.IC_TAXA_CDI_ATIVO,''))) + ';' +
				'0'+ ';' + --CONVERT(varchar, ISNULL(CAD.VL_COTACAO_INICIAL_A, 0)) + ';' +
				'0'+ ';' + --CONVERT(varchar, ISNULL(CAD.PC_MTM_ATIVO, 0 )) + ';' +
				'0'+ ';' + --CONVERT(varchar, ISNULL(POS.VL_ATIVO_MTM, 0)) + ';' +

				ltrim(rtrim(IndicePassivo.Descricao))+ ';' + 	+ ';' + 

				''+ ';' + --ltrim(rtrim(isnull(IC_INDICE_PASSIVO,''))) + ';' +
				''+ ';' + --ltrim(rtrim(isnull(CAD.IC_TAXA_CDI_PASSIVO,''))) + ';' +
				'0'+ ';' + --CONVERT(varchar, ISNULL(CAD.VL_COTACAO_INICIAL_P, 0)) + ';' +
				'0'+ ';' + --CONVERT(varchar, ISNULL(CAD.PC_MTM_PASSIVO, 0))	+ ';' +
				'0'+ ';' + --CONVERT(varchar, ISNULL(POS.VL_PASSIVO_MTM, 0))	+ ';' +

				''+ ';' + -- moeda

				'N'+ ';' + --ltrim(rtrim(isnull(MOV.IC_NEG_VENC,''))) + ';' +
				isnull(substring(CONVERT(varchar,POS.DataEmissao,20),1,10),'')	+ ';' AS LINHA

FROM	(SELECT Pessoa.Nome, Pessoa.IdPessoa, Cliente.IdMoeda, Cliente.IdCliente, Carteira.IdCarteira, Pessoa.Cpfcnpj
		FROM Pessoa 
		INNER JOIN Cliente
			ON Pessoa.IdPessoa = Cliente.IdCliente
		INNER JOIN Carteira
			ON Cliente.IdCliente = Carteira.IdCarteira ) CLI
	
	INNER JOIN PosicaoSwapHistorico		POS
		ON CLI.IdCliente = POS.IdCliente 
		
	INNER JOIN Indice IndiceAtivo
		ON IndiceAtivo.IdIndice = POS.IdIndice
		
	INNER JOIN Indice IndicePassivo
		ON IndicePassivo.IdIndice = POS.IdIndiceContraParte
		

WHERE	POS.DataHistorico = '{0}'
AND CLI.IdCarteira IN (	'{1}') 

