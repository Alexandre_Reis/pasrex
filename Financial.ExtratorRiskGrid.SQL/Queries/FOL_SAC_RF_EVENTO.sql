SELECT	DISTINCT 
				'02' + ';' + 
				LTRIM(RTRIM(EventoRendaFixa.IdTitulo))+ ';' + 
				ISNULL(substring(CONVERT(varchar,EventoRendaFixa.DataPagamento,20),1,10),'')+ ';' + 
				LTRIM(RTRIM(EventoRendaFixa.TipoEvento))+ ';' + 
				convert(varchar,ISNULL(EventoRendaFixa.Valor, 0))+ ';' AS LINHA
FROM	(SELECT Pessoa.Nome, Pessoa.IdPessoa, Cliente.IdMoeda, Cliente.IdCliente, Carteira.IdCarteira, Pessoa.Cpfcnpj
		FROM Pessoa 
		INNER JOIN Cliente
			ON Pessoa.IdPessoa = Cliente.IdCliente
		INNER JOIN Carteira
			ON Cliente.IdCliente = Carteira.IdCarteira ) CLI
	
	INNER JOIN PosicaoRendaFixaHistorico POS
		ON CLI.IdCliente = POS.IdCliente 
		
	INNER JOIN TituloRendaFixa LASTRO
		on POS.IdTitulo = LASTRO.IdTitulo
		
	INNER JOIN EventoRendaFixa
		on LASTRO.IdTitulo = EventoRendaFixa.IdTitulo

	WHERE	POS.DataHistorico = '{0}'
	AND CLI.IdCarteira IN (	'{1}') 
