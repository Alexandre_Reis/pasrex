SELECT	DISTINCT 
				'06' + ';' +
				substring(CONVERT(varchar,PATRFI.DataHistorico,20),1,10) 	+ ';' +
				ltrim(rtrim(CLI.IdCarteira))+ ';' +
				ltrim(rtrim(CLI.Nome))+ ';' +
				ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.Cpfcnpj,''),'.',''),'-',''),'/','')))	+ ';' +
				ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(FI_CAD.Cpfcnpj,''),'.',''),'-',''),'/',''))) + ';' +
				ltrim(rtrim(ISNULL(FI_CAD.CodigoIsin,'')))	+ ';' +
				ltrim(rtrim(ISNULL(FI_CAD.Nome,'')))			+ ';' +
				convert(varchar,convert(decimal(25,8), PATRFI.Quantidade)) + ';' +
				Moeda.Nome		+ ';' +
				convert(varchar,convert(decimal(25,8), isnull(PATRFI.CotaDia,0))) + ';' +
			LTRIM(RTRIM(isnull(FI_CAD.IdCarteira,''))) + ';' AS LINHA
FROM	(SELECT Pessoa.Nome, Pessoa.IdPessoa, Cliente.IdMoeda, Cliente.IdCliente, Carteira.IdCarteira, Pessoa.Cpfcnpj
		FROM Pessoa 
		INNER JOIN Cliente
			ON Pessoa.IdPessoa = Cliente.IdCliente
		INNER JOIN Carteira
			ON Cliente.IdCliente = Carteira.IdCarteira ) CLI
	
	INNER JOIN PosicaoFundoHistorico		PATRFI
		ON CLI.IdCliente = PATRFI.IdCliente
			INNER JOIN  (SELECT Pessoa.Nome, Carteira.CodigoIsin, Pessoa.IdPessoa, Cliente.IdCliente, Carteira.IdCarteira, Pessoa.Cpfcnpj
						FROM Pessoa 
						INNER JOIN Cliente
							ON Pessoa.IdPessoa = Cliente.IdCliente
						INNER JOIN Carteira
							ON Cliente.IdCliente = Carteira.IdCarteira )	FI_CAD 
				ON FI_CAD.IdCarteira = PATRFI.IdCarteira 
	INNER JOIN Moeda
		ON Moeda.IdMoeda = CLI.IdMoeda
WHERE		CLI.IdCarteira IN (	'{1}') 
AND			PATRFI.DataHistorico =	'{0}'
