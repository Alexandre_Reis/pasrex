SELECT	DISTINCT 
				'05'	+ ';' + 									
				substring(CONVERT(varchar,PATR.DataHistorico,20),1,10) 	+ ';' +
				LTRIM(RTRIM(ISNULL(CLI.IdCarteira,''))) + ';' +
				LTRIM(RTRIM(ISNULL(CLI.Nome,''))) + ';' +
				ltrim(rtrim(REPLACE(REPLACE(REPLACE(ISNULL(CLI.Cpfcnpj,''),'.',''),'-',''),'/','')))	+ ';'  +
				LTRIM(RTRIM(ISNULL(PATR.CdAtivoBMF,''))) + ';' + 
				ISNULL(substring(CONVERT(varchar,PATR.DataVencimento,20),1,10),'') 	+ ';' +
				LTRIM(RTRIM(ISNULL(PATR.Serie,'')))+ ';' +
				LTRIM(RTRIM(ISNULL(AtivoBMF.CodigoIsin,'')))+ ';' +
				'' + ';' + -- LTRIM(RTRIM(ISNULL(ATIVO.IC,'')))+ ';' +
				'' + ';' + -- ATIVO.CD_OBJETO_OPCAO
				
				convert(varchar,ISNULL(PATR.Quantidade, 0.0))+ ';' +
				LTRIM(RTRIM(ISNULL(AtivoBMF.TipoSerie,'')))+ ';' +
				'' + ';' + --LTRIM(RTRIM(ISNULL(PATR.RVCOR_CD,'')))+ ';' +
				'' + ';' + --LTRIM(RTRIM(ISNULL(VCTO.IC_MODELO_OPCAO,'')))+ ';' +
				convert(varchar,ISNULL(AtivoBMF.PrecoExercicio, 0.0))+ ';' +
				convert(varchar,ISNULL(PATR.PUMercado, 0.0))+ ';' +
				CASE AtivoBMF.TipoMercado
                                  when '1' then 'DIS'
                                  when '2' then 'FUT'
                                  when '3' then 'OPD'
                                  when '4' then 'OPF'
                                  when '5' then 'TMO'
                                  ELSE ''
                END + ';' +
				
				convert(VARCHAR,ISNULL(CONVERT(MONEY,PATR.ValorMercado), 0.0))+ ';' AS LINHA 
FROM	(SELECT Pessoa.Nome, Pessoa.IdPessoa, Cliente.IdMoeda, Cliente.IdCliente, Carteira.IdCarteira, Pessoa.Cpfcnpj
		FROM Pessoa 
		INNER JOIN Cliente
			ON Pessoa.IdPessoa = Cliente.IdCliente
		INNER JOIN Carteira
			ON Cliente.IdCliente = Carteira.IdCarteira ) CLI
	
	INNER JOIN PosicaoBMFHistorico		PATR
		ON CLI.IdCliente = PATR.IdCliente 
			
	INNER JOIN AtivoBMF
		ON PATR.CdAtivoBMF = AtivoBMF.CdAtivoBMF AND PATR.Serie = AtivoBMF.Serie

	INNER JOIN Moeda
		ON AtivoBMF.IdMoeda = Moeda.IdMoeda

WHERE		CLI.IdCarteira IN (	'{1}') 
AND			PATR.DataHistorico =	'{0}'
