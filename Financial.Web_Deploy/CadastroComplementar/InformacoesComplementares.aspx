﻿<%@ page language="C#" inherits="CadastrosBasicos_InformacoesComplementares, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }            
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    
                var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textNome');            
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);                        
                }        
            " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackClonar" runat="server" OnCallback="callBackClonar_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanelClonar.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.') {
                     
                    popupClonar.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Informações Complementares"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupClonar" AllowDragging="true" PopupElementID="popupLote"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="500" Left="150" Top="10" HeaderText="Clonar"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                                            <table border="0" cellspacing="2" cellpadding="2">
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelInfo" runat="server" CssClass="labelRequired" Text="Carteira a Clonar:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dx:ASPxGridLookup ID="dropInfoComplementares" ClientInstanceName="dropInfoComplementares"
                                                            runat="server" KeyFieldName="CompositeKey" DataSourceID="EsDSInfoComplementarClonar"
                                                            Width="350px" TextFormatString="{0} - {1} - {3}" Font-Size="11px" AllowUserInput="false">
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                                                <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small" />
                                                                <dxwgv:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small" Width="90%"
                                                                    CellStyle-Wrap="false" />
                                                                <dxwgv:GridViewDataDateColumn FieldName="DataInicioVigencia" Width="10%" />
                                                                <dxwgv:GridViewDataColumn FieldName="DataInicioVigenciaString" Visible="false" />
                                                                <dxwgv:GridViewDataColumn FieldName="CompositeKey" Visible="false" />
                                                            </Columns>
                                                            <GridViewProperties>
                                                                <Settings ShowFilterRow="True" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Nova Carteira:" />
                                                    </td>
                                                    <td class="td_Label">
                                                        <dx:ASPxGridLookup ID="dropCarteiraDestino" ClientInstanceName="dropCarteiraDestino"
                                                            runat="server" KeyFieldName="IdCarteira" DataSourceID="EsDSCarteiraClonar" Width="250px"
                                                            TextFormatString="{0}" Font-Size="11px" AllowUserInput="false">
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                                                <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small" />
                                                                <dxwgv:GridViewDataColumn FieldName="Apelido" CellStyle-Font-Size="X-Small" Width="89%"
                                                                    CellStyle-Wrap="false" />
                                                            </Columns>
                                                            <GridViewProperties>
                                                                <Settings ShowFilterRow="True" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Início Vigência:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataClonar" runat="server" ClientInstanceName="textDataClonar" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnProcessaClonar" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                                                   LoadingPanelClonar.Show();
                                                                   callBackClonar.SendCallback();                                                                        
                                                                   
                                                                   return false;
                                                                   ">
                                                    <asp:Literal ID="Literal15" runat="server" Text="Clonar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnClonar" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="
                                                                                
                                            dropInfoComplementares.SetText('');
                                            dropInfoComplementares.SetValue('');
                                            
                                            dropCarteiraDestino.SetText('');
                                            dropCarteiraDestino.SetValue('');
                                            
                                            textDataClonar.SetValue(null);
                                        
                                        popupClonar.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal8" runat="server" Text="Clonar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="CompositeKey"
                                        DataSourceID="EsDSInfoComplementar" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="2" Width="9%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="3" Width="35%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataInicioVigencia" Caption="Data Vigência"
                                                VisibleIndex="4" Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="Periodicidade" VisibleIndex="5" Width="20%" />
                                            <dxwgv:GridViewDataColumn FieldName="LocalFormaDivulgacao" Caption="Local Forma Divulgação"
                                                VisibleIndex="6" Width="20%" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <dxtc:ASPxPageControl ID="tabCadastro" runat="server" ClientInstanceName="tabCadastro"
                                                            EnableHierarchyRecreation="True" ActiveTabIndex="0" Height="100%" Width="100%"
                                                            TabSpacing="0px" EnableClientSideAPI="true">
                                                            <TabPages>
                                                                <dxtc:TabPage Text="Informações Complementares I" Name="tab1">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Data Inicial:" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxDateEdit ID="textDataInicioVigencia" runat="server" ClientInstanceName="textDataInicioVigencia"
                                                                                            Value='<%#Eval("DataInicioVigencia")%>' />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit1"
                                                                                            ClientInstanceName="btnEditCodigoCarteira" Text='<%# Eval("IdCarteira") %>' MaxLength="10"
                                                                                            NumberType="Integer">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} "
                                                                                                ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <dxrp:ASPxRoundPanel ID="Panel1" runat="server" HeaderStyle-Font-Bold="true" HeaderText="Divulgação da Composição da Carteira do Fundo"
                                                                                Width="800px" HeaderStyle-Font-Size="9px" ContentPaddings-PaddingBottom="1" ContentPaddings-PaddingTop="1">
                                                                                <PanelCollection>
                                                                                    <dxp:PanelContent runat="server">
                                                                                        <table border="0">
                                                                                            <tr>
                                                                                                <td class="td_Label" style="text-align: left; padding-left: 5px">
                                                                                                    <asp:Label ID="label24" runat="server" CssClass="labelRequired" Text="I-Peridiocidade" />
                                                                                                </td>
                                                                                                <td class="td_Label" style="text-align: left; padding-left: 5px">
                                                                                                    <asp:Label ID="label33324" runat="server" CssClass="labelRequired" Text="II-Local, Meio e Forma de Divulgação" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <dxe:ASPxTextBox ID="textPeriodicidade" ClientInstanceName="textPeriodicidade" runat="server"
                                                                                                        Width="400px" MaxLength="250" Text='<%#Eval("Periodicidade")%>' TextMode="MultiLine"
                                                                                                        Rows="1" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <dxe:ASPxTextBox ID="textLocalFormaDivulgacao" ClientInstanceName="textLocalFormaDivulgacao"
                                                                                                        runat="server" Width="400px" MaxLength="300" Text='<%#Eval("LocalFormaDivulgacao")%>'
                                                                                                        TextMode="MultiLine" Rows="1" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </dxp:PanelContent>
                                                                                </PanelCollection>
                                                                            </dxrp:ASPxRoundPanel>
                                                                            <dxrp:ASPxRoundPanel ID="Panel2" runat="server" HeaderStyle-Font-Bold="true" HeaderText="Atendimento ao Cotista"
                                                                                Width="850px" HeaderStyle-Font-Size="9px" ContentPaddings-PaddingBottom="1" ContentPaddings-PaddingTop="1">
                                                                                <PanelCollection>
                                                                                    <dxp:PanelContent runat="server">
                                                                                        <table border="0">
                                                                                            <tr>
                                                                                                <td class="td_Label" colspan="2" style="text-align: left; padding-left: 5px; padding-top: 1px;
                                                                                                    padding-bottom: 1px">
                                                                                                    <asp:Label ID="label2443" runat="server" CssClass="labelRequired" Text="III-Local, meio e forma de solicitação de informações pelo cotista" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <dxe:ASPxTextBox ID="textLocalFormaSolicitacaoCotista" ClientInstanceName="textLocalFormaSolicitacaoCotista"
                                                                                                        runat="server" Width="400px" MaxLength="250" Text='<%#Eval("LocalFormaSolicitacaoCotista")%>'
                                                                                                        TextMode="MultiLine" Rows="1" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </dxp:PanelContent>
                                                                                </PanelCollection>
                                                                            </dxrp:ASPxRoundPanel>
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="label24343" runat="server" CssClass="labelRequired" Text="IV-Exposição, em ordem de relevância, dos fatores de riscos inerentes <br> à composição da carteira do fundo" />
                                                                                    </td>
                                                                                    <td align="left" class="td_Label" style="text-align: left; padding-left: 5px">
                                                                                        <asp:Label ID="labe3l33324" runat="server" CssClass="labelRequired" Text="V-Política relativa ao exercício de direito do voto" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textFatoresRisco" ClientInstanceName="textFatoresRisco" runat="server"
                                                                                            Width="400px" MaxLength="2000" Text='<%#Eval("FatoresRisco")%>' TextMode="MultiLine"
                                                                                            Rows="1" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textPoliticaExercicioVoto" ClientInstanceName="textPoliticaExercicioVoto"
                                                                                            runat="server" Width="400px" MaxLength="1000" Text='<%#Eval("PoliticaExercicioVoto")%>'
                                                                                            TextMode="MultiLine" Rows="1" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="label243483" runat="server" CssClass="labelRequired" Text="VI- Tributação aplicável ao fundo e a seus cotistas, contemplando a politica a ser <br> adotada pelo administrador quanto ao tratamento tributário perseguido" />
                                                                                    </td>
                                                                                    <td align="left" class="td_Label" style="text-align: left; padding-left: 5px">
                                                                                        <asp:Label ID="labe37l338324" runat="server" CssClass="labelRequired" Text="VII - Política de administração de risco (métodos do administrador para gerenciar os riscos)" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textTributacaoAplicavel" ClientInstanceName="textTributacaoAplicavel"
                                                                                            runat="server" Width="400px" MaxLength="2500" Text='<%#Eval("TributacaoAplicavel")%>'
                                                                                            TextMode="MultiLine" Rows="1" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textPoliticaAdministracaoRisco" ClientInstanceName="textPoliticaAdministracaoRisco"
                                                                                            runat="server" Width="400px" MaxLength="2500" Text='<%#Eval("PoliticaAdministracaoRisco")%>'
                                                                                            TextMode="MultiLine" Rows="1" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="label243543" runat="server" CssClass="labelRequired" Text="VIII – Agência Class. Risco e classificação obtida" />
                                                                                    </td>
                                                                                    <td align="left" class="td_Label" style="text-align: left; padding-left: 5px">
                                                                                        <asp:Label ID="labe36l33324" runat="server" CssClass="labelRequired" Text="IX.	Apresentação detalhada do administrador e do gestor, departamento técnico e <br> demais recursos e serviços utilizados pelo gestor" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textAgenciaClassificacaoRisco" ClientInstanceName="textAgenciaClassificacaoRisco"
                                                                                            runat="server" Width="400px" MaxLength="50" Text='<%#Eval("AgenciaClassificacaoRisco")%>'
                                                                                            TextMode="MultiLine" Rows="1" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textRecursosServicosGestor" ClientInstanceName="textRecursosServicosGestor"
                                                                                            runat="server" Width="400px" MaxLength="100" Text='<%#Eval("RecursosServicosGestor")%>'
                                                                                            TextMode="MultiLine" Rows="1" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="label24s343" runat="server" CssClass="labelRequired" Text="X–Demais prestadores de serviços do fundo" />
                                                                                    </td>
                                                                                    <td align="left" class="td_Label" style="text-align: left; padding-left: 5px">
                                                                                        <asp:Label ID="labe3sl33324" runat="server" CssClass="labelRequired" Text="XI-Política de distribuição de cotas" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textPrestadoresServicos" ClientInstanceName="textPrestadoresServicos"
                                                                                            runat="server" Width="400px" MaxLength="100" Text='<%#Eval("PrestadoresServicos")%>'
                                                                                            TextMode="MultiLine" Rows="1" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textPoliticaDistribuicaoCotas" ClientInstanceName="textPoliticaDistribuicaoCotas"
                                                                                            runat="server" Width="400px" MaxLength="2500" Text='<%#Eval("PoliticaDistribuicaoCotas")%>'
                                                                                            TextMode="MultiLine" Rows="1" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="label2434s3" runat="server" CssClass="labelRequired" Text="XII- Observações" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxTextBox ID="textObservacoes" ClientInstanceName="textObservacoes" runat="server"
                                                                                            Width="800px" MaxLength="2500" Text='<%#Eval("Observacoes")%>' TextMode="singleline"
                                                                                            Rows="1" />
                                                                                    </td>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Informações Complementares II" Name="tab2">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblGestorVotaAssembleia" runat="server" CssClass="labelRequired" Text="Campo especifico para informar se o gestor vota ou não <br> em assembléias dos ativos que compõem a carteira:" />
                                                                                    </td>
                                                                                    <td align="left" class="td_Label" style="text-align: left; padding-left: 5px">
                                                                                        <asp:Label ID="lblAGENC_CLASSIF_RATIN" runat="server" CssClass="labelRequired" Text="Informação se existe ou não  <br> agência de classificação de rating:" />
                                                                                    </td>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblCOD_DISTR_OFERTA_PUB" runat="server" CssClass="labelRequired" Text="Indicação se o distribuidor oferta para o público alvo do fundo,  <br> preponderantemente, fundos geridos por um único gestor  <br> ou por gestoras ligadas a um mesmo grupo econômico:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropGestorVotaAssembleia" ClientInstanceName="dropGestorVotaAssembleia"
                                                                                            runat="server" Text='<%#Eval("CodVotoGestAssemb")%>' ShowShadow="true" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownListCurto_2">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="2" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropAGENC_CLASSIF_RATIN" ClientInstanceName="dropAGENC_CLASSIF_RATIN"
                                                                                            runat="server" Text='<%#Eval("AgencClassifRatin")%>' ShowShadow="true" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownListCurto_2">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="2" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCOD_DISTR_OFERTA_PUB" ClientInstanceName="dropCOD_DISTR_OFERTA_PUB"
                                                                                            runat="server" Text='<%#Eval("CodDistrOfertaPub")%>' ShowShadow="true" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownListCurto_2">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="2" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblDescricaoLocalDivulgacao" runat="server" CssClass="labelRequired"
                                                                                            Text="Descrição do local de divulgação:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textDescricaoLocalDivulgacao" ClientInstanceName="textDescricaoLocalDivulgacao"
                                                                                            runat="server" Width="800px" MaxLength="300" Text='<%#Eval("DsLocalDivulg")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblDescricaoResponsavel" runat="server" CssClass="labelRequired" Text="Descrição do responsável (administrador, gestor, distribuidor, etc) descrição de texto enviada pela instituição:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textDescricaoResponsavel" ClientInstanceName="textDescricaoResponsavel"
                                                                                            runat="server" Width="800px" MaxLength="250" Text='<%#Eval("DsResp")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblApresentavaoAdministrador" runat="server" CssClass="labelRequired"
                                                                                            Text="Apresentação do administrador:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textApresentavaoAdministrador" ClientInstanceName="textApresentavaoAdministrador"
                                                                                            runat="server" Width="800px" MaxLength="2500" Text='<%#Eval("ApresDetalheAdm")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblINFORM_AUTOREGUL_ANBIMA" runat="server" CssClass="labelRequired"
                                                                                            Text="Informações sobre autorregulação anbima. exemplo: classificação anbima, selo anbima, referência a adesão ao código anbima (administrador e gestor) e disclaimers <br> (avisos obrigatórios previstos em código de autorregulação e diretrizes aplicadas a fundos de investimentos):" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textINFORM_AUTOREGUL_ANBIMA" ClientInstanceName="textINFORM_AUTOREGUL_ANBIMA"
                                                                                            runat="server" Width="800px" MaxLength="2500" Text='<%#Eval("InformAutoregulAnbima")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblDsServicoPrestado" runat="server" CssClass="labelRequired" Text="Descrição do serviço prestado nos termos do artigo X, inciso 41:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textDsServicoPrestado" ClientInstanceName="textDsServicoPrestado"
                                                                                            runat="server" Width="800px" MaxLength="100" Text='<%#Eval("DsServicoPrestado")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblDsLocal" runat="server" CssClass="labelRequired" Text="Descrição do local. Texto da instituição:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textDsLocal" ClientInstanceName="textDsLocal" runat="server"
                                                                                            Width="800px" MaxLength="100" Text='<%#Eval("DsLocal")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblNmPrest" runat="server" CssClass="labelRequired" Text="Nome do prestador:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textNmPrest" ClientInstanceName="textDsLocal" runat="server"
                                                                                            Width="800px" MaxLength="100" Text='<%#Eval("NmPrest")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblNrCnpj" runat="server" CssClass="labelRequired" Text="Nr.CNPJ:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textNrCnpj" ClientInstanceName="textNrCnpj" runat="server" Width="800px"
                                                                                            MaxLength="30" Text='<%#Eval("NrCnpj")%>'>
                                                                                            <ClientSideEvents Init="function(s,e) 
                                                                                            {
                                                                                                var cpfcnpj = textNrCnpj.GetText();
                                                                                                
                                                                                                textNrCnpj.GetInputElement().setAttribute('maxlength', 18);
                                                                                                cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
                                                                                                cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')                
                                                                                                textNrCnpj.SetValue(cpfcnpj);
                                                                                               
	                                                                                        }" KeyPress="function(s,e) 
                                                                                            {
                                                                                                var cpfcnpj = textNrCnpj.GetText();
                                                                                                
                                                                                                textNrCnpj.GetInputElement().setAttribute('maxlength', 18);
                                                                                                cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
                                                                                                cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')                
                                                                                                textNrCnpj.SetValue(cpfcnpj);
                                                                                               
	                                                                                        }" />
                                                                                        </dxe:ASPxTextBox>
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblDisclAdvert" runat="server" CssClass="labelRequired" Text="Padronização do disclaimer relativo a advertência da manutenção do serviço pela cvm:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textDisclAdvert" ClientInstanceName="textDisclAdvert" runat="server"
                                                                                            Width="800px" MaxLength="100" Text='<%#Eval("DisclAdvert")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td style="text-align: left; padding-left: 5px" class="td_Label">
                                                                                        <asp:Label ID="lblCodMeioDivulg" runat="server" CssClass="labelRequired" Text="Código do meio de divulgação:" />
                                                                                    </td>
                                                                                    <td align="left" class="td_Label" style="text-align: left; padding-left: 5px">
                                                                                        <asp:Label ID="lblCodMeio" runat="server" CssClass="labelRequired" Text="Código do meio de solicitação:" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCodMeioDivulg" ClientInstanceName="dropCodMeioDivulg" runat="server"
                                                                                            Text='<%#Eval("CodMeioDivulg")%>' ShowShadow="true" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownListCurto_2">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Eletrônico" />
                                                                                                <dxe:ListEditItem Value="2" Text="Físico" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCodMeio" ClientInstanceName="dropCodMeio" runat="server"
                                                                                            Text='<%#Eval("CodMeio")%>' ShowShadow="true" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Eletrônico" />
                                                                                                <dxe:ListEditItem Value="2" Text="Físico" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                            </TabPages>
                                                            <Paddings PaddingLeft="0px" />
                                                            <TabStyle HorizontalAlign="Center" />
                                                            <ContentStyle>
                                                                <Border BorderColor="#4986A2" />
                                                            </ContentStyle>
                                                        </dxtc:ASPxPageControl>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="890px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxlp:ASPxLoadingPanel ID="LoadingPanelClonar" runat="server" Text="Processando, aguarde..."
            ClientInstanceName="LoadingPanelClonar" Modal="True" />
        <cc1:esDataSource ID="EsDSInfoComplementarClonar" runat="server" OnesSelect="EsDSnfoComplementarClonar_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCarteiraClonar" runat="server" OnesSelect="EsDSCarteiraClonar_esSelect"
            LowLevelBind="true" />
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSInfoComplementar" runat="server" OnesSelect="EsDSInfoComplementar_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>
