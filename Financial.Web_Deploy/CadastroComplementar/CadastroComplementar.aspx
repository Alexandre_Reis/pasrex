﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastroComplementar_CadastroComplementar, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;        
    var operacao = '';
    
    function CarregaMercadoTipoPessoa(dropCadastro)
    {
        dropMercadoTipoPessoa.PerformCallback(dropCadastro.GetSelectedItem().text);
    }
    
    function LimpaCampos()
    {
        dropCadastro.SetValue(null);
        dropMercado.GetGridView().PerformCallback(); 
        var div = document.getElementById('popupDinamico_panelDinamico');
        if(div != null)
            div.innerHTML = "";
    }
    
    function HabilitaCampo(TipoCampo)
    {

	    var visible = false;

	    dropValorCampo.SetClientVisible(visible); 
	    lblDropValorCampo.SetClientVisible(visible); 
	    spinValorCampo.SetClientVisible(visible); 
	    lblSpinValorCampo.SetClientVisible(visible); 
	    document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textValorCampo').style.display = "none";
	    lblTextValorCampo.SetClientVisible(visible);
	    dataValorCampo.SetClientVisible(visible); 
	    lblDataValorCampo.SetClientVisible(visible); 
    		
	    visible = true;	
	    if(TipoCampo == '-1')
	    {
		    spinValorCampo.SetClientVisible(visible); 
		    spinValorCampo.SetWidth(textNomeCampo.GetWidth());
		    lblSpinValorCampo.SetClientVisible(visible); 
	    }
	    else if (TipoCampo == '-2')
	    {		    
		    callbackTextCampo.SendCallback(); 
	    }
	    else if(TipoCampo == '-3')
	    {
		    dataValorCampo.SetClientVisible(visible); 
		    dataValorCampo.SetWidth(textNomeCampo.GetWidth());
		    lblDataValorCampo.SetClientVisible(visible); 
	    }
	    else if(TipoCampo != '-1' && TipoCampo != '-2' && TipoCampo != '-3')
	    {
		    dropValorCampo.SetClientVisible(visible); 
		    dropValorCampo.SetWidth(textNomeCampo.GetWidth());
		    lblDropValorCampo.SetClientVisible(visible); 
	    }

    }
    
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackPanel" runat="server" OnCallback="callbackPanel_Callback">
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackTextCampo" runat="server" OnCallback="callbackTextCampo_Callback">
        <ClientSideEvents CallbackComplete="function(s,e) 
        {
        	var textValorCampo = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textValorCampo');
		    textValorCampo.style.display = '';
		    textValorCampo.maxLength = e.result;
		    lblTextValorCampo.SetClientVisible(true);  
        
        }" />
        </dxcb:ASPxCallback>        
        <dxcb:ASPxCallback ID="callbackCadastro" runat="server" OnCallback="callbackCadastro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            popupDinamico.Hide();
            gridCadastro.PerformCallback('btnRefresh'); 
            alert('Cadastro efetuado com sucesso!');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table>
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro Complementar"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupDinamico" AllowDragging="true" PopupElementID="popupDinamico"
                                    ClientInstanceName="popupDinamico" EnableClientSideAPI="True" PopupVerticalAlign="Middle"
                                    PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" Width="400" Left="250"
                                    Top="70" HeaderText="Cadastro Complementar" runat="server" HeaderStyle-BackColor="#EBECEE"
                                    HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl runat="server">
                                            <div class="editForm">
                                                <table>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelCadastro" runat="server" CssClass="labelRequired" Text="Cadastro:">
                                                            </asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropCadastro" runat="server" ClientInstanceName="dropCadastro"
                                                                ShowShadow="false" DropDownStyle="DropDownList" OnLoad="dropCadastro_OnLoad"
                                                                CssClass="dropDownListLongo">
                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                { 
                                                                    dropMercado.GetGridView().PerformCallback(); 
                                                                }" />
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelMercadoTipoPessoa" runat="server" CssClass="labelRequired" Text="Mercado/Tipo Pessoa:" />
                                                        </td>
                                                        <td>
                                                            <dx:ASPxGridLookup ID="dropMercado" ClientInstanceName="dropMercado" runat="server"
                                                                SelectionMode="Single" KeyFieldName="Id" TextFormatString="{1}" CssClass="dropDownListLongo"
                                                                Font-Size="11px" DropDownWindowStyle-HorizontalAlign="Center" AllowUserInput="False"
                                                                OnLoad="dropMercado_OnLoad">
                                                                <Columns>
                                                                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                                                    <dxwgv:GridViewDataColumn FieldName="Id" Caption="Id" Width="20%" CellStyle-Font-Size="XX-Small"
                                                                        CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                                                    <dxwgv:GridViewDataColumn FieldName="Nome" Caption="Nome" Width="80%" CellStyle-Font-Size="XX-Small"
                                                                        CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                                                                </Columns>
                                                                <GridViewProperties>
                                                                    <Settings ShowFilterRow="True" />
                                                                </GridViewProperties>
                                                            </dx:ASPxGridLookup>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="linhaH">
                                            </div>
                                            <asp:Panel ID="panelDinamico" runat="server">
                                            </asp:Panel>
                                            <div class="linhaH">
                                            </div>
                                            <div class="linkButton linkButtonNoBorder popupFooter">
                                                <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                    <asp:LinkButton ID="btnGerarCadastro" runat="server" Font-Overline="False" ForeColor="Black"
                                                        CssClass="btnLote" OnClick="btnGerarCadastro_Click">
                                                        <asp:Literal ID="Literal9" runat="server" Text="Gerar Cadastro" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnCadastro" runat="server" Font-Overline="False" ForeColor="Black"
                                                        OnClientClick="callbackCadastro.SendCallback(); return false;" CssClass="btnOK">
                                                        <asp:Literal ID="Literal11" runat="server" Text="Cadastrar" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                    <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px" />
                                    <ClientSideEvents Closing="function(s, e) { LimpaCampos(); } " />    
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="popupDinamico.ShowWindow(); return false; ">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" />
                                        <div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" />
                                        <div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" />
                                        <div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting" KeyFieldName="IdCadastroComplementares"
                                        OnCustomCallback="gridCadastro_CustomCallback" DataSourceID="EsDSCadastroComplementar"
                                        OnCustomColumnDisplayText="gridCadastro_CustomColumnDisplayText" OnCancelRowEditing="gridCadastro_CancelRowEditing"
                                        OnPreRender="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" HeaderStyle-HorizontalAlign="Center" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCadastroComplementares" Visible="false" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCadastro" Caption="Cadastro" Width="10%"
                                                HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left" ExportWidth="100" />
                                            <dxwgv:GridViewDataColumn FieldName="NomeCampo" Caption="Nome do Campo" Width="10%"
                                                HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left" ExportWidth="100" />
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCampo" Caption="Descrição" Width="20%"
                                                HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left" ExportWidth="100" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCampo" Caption="Tipo" Width="10%"
                                                HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left" ExportWidth="100" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="CampoObrigatorio" Caption="Campo Obrigatório"
                                                Width="10%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" >
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>                                                                                                                                             
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoMercadoTipoPessoa" Caption="Mercado/Tipo Pessoa"
                                                Width="20%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                            <dxwgv:GridViewDataColumn FieldName="ValorCampo" Caption="Valor" Width="10%" HeaderStyle-HorizontalAlign="Left"
                                                CellStyle-HorizontalAlign="Left" ExportWidth="100" />
                                        </Columns>
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenTipoCampo" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenTipoCampo" Text='<%#Eval("TipoCampo")%>' />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <dxe:ASPxLabel ID="lblTipoCadastro" runat="server" CssClass="labelNormal" Text="Tipo Cadastro:" ClientVisible="false"></dxe:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="dropTipoCadastro" runat="server" ClientInstanceName="dropTipoCadastro" ClientVisible="false"
                                                                    ClientEnabled="false" Value='<%#Eval("TipoCadastro")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblNomeCampo" runat="server" CssClass="labelNormal" Text="Nome Campo:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textNomeCampo" runat="server" ClientInstanceName="textNomeCampo" Width="400px"
                                                                    ClientEnabled="false" Value='<%#Eval("NomeCampo")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblDescricaoCampo" runat="server" CssClass="labelNormal" Text="Descrição Campo:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textDescricaoCampo" runat="server" ClientInstanceName="textDescricaoCampo" Width="400px"
                                                                    ClientEnabled="false" Value='<%#Eval("DescricaoCampo")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblTipoCampo" runat="server" CssClass="labelNormal" Text="Tipo Campo:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textTipoCampo" runat="server" ClientInstanceName="textTipoCampo" Width="400px" ClientEnabled="false" OnInit="textTipoCampo_Init">
                                                                    <ClientSideEvents Init="function(s,e){ HabilitaCampo(hiddenTipoCampo.GetValue()); }" />  
                                                                </dxe:ASPxTextBox>
                                                                    
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblDescricaoMercadoTipoPessoa" runat="server" CssClass="labelNormal"
                                                                    Text="Descrição Mercado Tipo Pessoa:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textDescricaoMercadoTipoPessoaa" runat="server" ClientInstanceName="textDescricaoMercadoTipoPessoaa"
                                                                    ClientEnabled="false" Value='<%#Eval("DescricaoMercadoTipoPessoa")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblCampoObrigatorio" runat="server" CssClass="labelNormal" Text="Campo Obrigatório:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textCampoObrigatorio" runat="server" ClientInstanceName="textCampoObrigatorio"
                                                                    ClientEnabled="false" Value='<%#Eval("CampoObrigatorio")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <dxe:ASPxLabel ID="lblTextValorCampo" ClientInstanceName="lblTextValorCampo" runat="server" CssClass="labelNormal" Text="Valor Campo:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textValorCampo" runat="server" ClientInstanceName="textValorCampo"
                                                                    ClientVisible="false" Value='<%#Eval("ValorCampo")%>'>
                                                                <ClientSideEvents Init="function(s , e) { TipoCampo = textTipoCampo.GetValue(); 
                                                                                                          var visible = TipoCampo == '-2';
                                                                                                          textValorCampo.SetClientVisible(visible);  
                                                                                                          lblTextValorCampo.SetClientVisible(visible);  
                                                                                                        }" />
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td class="td_Label_Curto">
                                                                <dxe:ASPxLabel ID="lblDropValorCampo" ClientInstanceName="lblDropValorCampo" runat="server" CssClass="labelNormal" Text="Valor Campo:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropValorCampo" ClientInstanceName="dropValorCampo" runat="server" DataSourceID="EsDSCadastroComplementarItemLista"
                                                                    ClientVisible="false" ValueField="IdLista" TextField="ItemLista" CssClass="dropDownListLongo"
                                                                    Text='<%#Eval("ValorCampo")%>'>
                                                                    <ClientSideEvents Init="function(s , e) { TipoCampo = textTipoCampo.GetValue(); 
                                                                                                              var visible = TipoCampo != '-1' && TipoCampo != '-2' && TipoCampo != '-3';
                                                                                                              dropValorCampo.SetClientVisible(visible); 
                                                                                                              lblDropValorCampo.SetClientVisible(visible); 
                                                                                                            }" />
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <dxe:ASPxLabel ID="lblDataValorCampo" ClientInstanceName="lblDataValorCampo" runat="server" CssClass="labelNormal" Text="Valor Campo:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="dataValorCampo" ClientInstanceName="dataValorCampo" runat="server"
                                                                    ClientVisible="false" CssClass="dropDownListLongo"
                                                                    Text='<%#Eval("ValorCampo")%>'>
                                                                    <ClientSideEvents Init="function(s , e) { TipoCampo = textTipoCampo.GetValue(); 
                                                                                                              var visible = TipoCampo == '-3';
                                                                                                              dataValorCampo.SetClientVisible(visible); 
                                                                                                              lblDataValorCampo.SetClientVisible(visible); 
                                                                                                            }" />
                                                                </dxe:ASPxDateEdit>
                                                            </td>
                                                        </tr>   
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <dxe:ASPxLabel ID="lblSpinValorCampo" ClientInstanceName="lblSpinValorCampo" runat="server" CssClass="labelNormal" Text="Valor Campo:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="spinValorCampo" ClientInstanceName="spinValorCampo" runat="server"
                                                                    ClientVisible="false" CssClass="dropDownListLongo" NumberType="Float"
                                                                    Text='<%#Eval("ValorCampo")%>'>
                                                                    <ClientSideEvents Init="function(s , e) { TipoCampo = textTipoCampo.GetValue(); 
                                                                                                              var visible = TipoCampo == '-1';
                                                                                                              spinValorCampo.SetClientVisible(visible); 
                                                                                                              lblSpinValorCampo.SetClientVisible(visible); 
                                                                                                            }" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>                                                                                                              
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                            CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal6" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton></div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="600px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
                Modal="True">
            </dx:ASPxLoadingPanel>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCadastroComplementar" runat="server" OnesSelect="EsDSCadastroComplementar_esSelect" />
        <cc1:esDataSource ID="EsDSCadastroComplementarItemLista" runat="server" OnesSelect="EsDSCadastroComplementarItemLista_esSelect" />
    </form>
</body>
</html>
