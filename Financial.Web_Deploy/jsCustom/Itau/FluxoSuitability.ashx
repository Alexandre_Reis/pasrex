<%@ WebHandler Language="C#" Class="FluxoSuitability" %>

using System;
using System.Web;
using Newtonsoft.Json;

public class FluxoSuitability : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {   
        
        try
        {
            string action = context.Request["action"];
            DadosOperacao dadosOperacao = JsonConvert.DeserializeObject<DadosOperacao>(context.Request["dadosOperacao"]);

            //MOCK
            /*DadosOperacao dadosOperacao = new DadosOperacao();
            dadosOperacao.IdCliente = 6005;
            dadosOperacao.DataOperacao = DateTime.Today;
            dadosOperacao.ValorOperacao = 1000;*/

            string json = "";

            Financial.Investidor.Cliente cliente = new Financial.Investidor.Cliente();
            cliente.LoadByPrimaryKey(dadosOperacao.IdCliente);

            string cci = dadosOperacao.IdCliente.ToString();
            string papelDescricao = dadosOperacao.PapelDescricao;
            bool operacaoAgendada = dadosOperacao.DataOperacao.Date > cliente.DataDia.Value.Date;
            decimal valorOperacao = dadosOperacao.ValorOperacao;

            WSSuitabilityItau2.Suitability suitability = new WSSuitabilityItau2.Suitability();

            string usuarioIntegracao = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.UsuarioIntegracao;
            string senhaIntegracao = Financial.Util.ParametrosConfiguracaoSistema.Integracoes.SenhaIntegracao;

            if (!String.IsNullOrEmpty(usuarioIntegracao) && !String.IsNullOrEmpty(senhaIntegracao))
            {
                System.Net.CredentialCache cache = new System.Net.CredentialCache();
                cache.Add(new Uri(suitability.Url), "Basic", new System.Net.NetworkCredential(usuarioIntegracao, senhaIntegracao));
                suitability.Credentials = cache;
            }

            if (action == "ConsultarPerfilCliente" || action == "AssinarTCQ")
            {

                WSSuitabilityItau2.RespostaOfSuitabilityEntidadeGa_SAZF5R suitabilityEntidade = suitability.ConsultarPerfilCliente(WSSuitabilityItau2.CanalSuitabilityEnum.Financial,
                true, cci, papelDescricao, valorOperacao, true, operacaoAgendada, true);

                //MOCK:
                /*WSSuitabilityItau2.RespostaOfSuitabilityEntidadeGa_SAZF5R suitabilityEntidade = new WSSuitabilityItau2.RespostaOfSuitabilityEntidadeGa_SAZF5R();

                suitabilityEntidade.Dados = new WSSuitabilityItau2.SuitabilityEntidade();
                suitabilityEntidade.Dados.SolicitarTCQ = false;
                suitabilityEntidade.Dados.MensagemTermo = "N�o desejo conhecer meu perfil de investidor e declaro estar ciente dos riscos envolvidos ao investir sem saber meu perfil. Ao escolher essa op��o voc� n�o poder� contar com a assessoria da Ita� Corretora com rela��o ao envio de recomenda��es de investimentos";
                suitabilityEntidade.Dados.MensagemAlerta = "Para prosseguir, voc� deve antes preencher o question�rio de perfil de investidor no site da Ita� Corretora. www.itaucorretora.com.br >> Perfil.";
                */
                //fim

                string mensagemTermoOriginal = suitabilityEntidade.Dados.MensagemTermo;
                string mensagemTermoNova = suitabilityEntidade.Dados.MensagemAlerta;
                mensagemTermoNova += ("\n\n" + mensagemTermoOriginal);

                suitabilityEntidade.Dados.MensagemTermo = mensagemTermoNova;


                if (action == "ConsultarPerfilCliente")
                {
                    json = JsonConvert.SerializeObject(suitabilityEntidade.Dados, Formatting.None);
                }
                else
                {
                    //Assinar TCQ
                    WSSuitabilityItau2.RespostaOfboolean resultadoAssinatura = suitability.AssinarTCQ(WSSuitabilityItau2.CanalSuitabilityEnum.Financial, true, cci, "", papelDescricao,
                    valorOperacao, true, operacaoAgendada, true, suitabilityEntidade.Dados.PerfilCliente, true);

                    //MOCK:
                    /*WSSuitabilityItau2.RespostaOfboolean resultadoAssinatura = new WSSuitabilityItau2.RespostaOfboolean();
                    resultadoAssinatura.Dados = true;*/


                    json = JsonConvert.SerializeObject(resultadoAssinatura.Dados, Formatting.None);
                }

            }
            else if (action == "ConsultaEnquadramentoSuitability" || action == "AssinarTCD")
            {
                WSSuitabilityItau2.RespostaOfSuitabilityEntidadeGa_SAZF5R suitabilityEntidade = suitability.ConsultarEnquadramentoCliente(WSSuitabilityItau2.CanalSuitabilityEnum.Financial,
                true, cci, "", papelDescricao, valorOperacao, true, operacaoAgendada, true);

                //MOCK
                /*WSSuitabilityItau2.RespostaOfSuitabilityEntidadeGa_SAZF5R suitabilityEntidade = new WSSuitabilityItau2.RespostaOfSuitabilityEntidadeGa_SAZF5R();

                suitabilityEntidade.Dados = new WSSuitabilityItau2.SuitabilityEntidade();
                suitabilityEntidade.Dados.SolicitarTCD = false;
                suitabilityEntidade.Dados.ApresentarAlerta = false;
                suitabilityEntidade.Dados.MensagemTermo = "<p>Caso voc� tenha ordens ainda n�o executadas, elas n�o est�o sendo consideradas para determinar o perfil dos seus investimentos</p><p>Ao configrmar a opera��o voc� declara que conhece a import�ncia de manter seus investimentos adequados ao seu perfil de investidor</p>";
                suitabilityEntidade.Dados.MensagemAlerta = "Com base na sua posi��o atual, seu perfil de investidor est� alinhado com seus investimentos. Lembrando que opera��es futuras poder�o alterar o perfil de seus investimentos.";
                suitabilityEntidade.Dados.TextoExibicaoPerfilCliente = "Seu perfil de investidor";
                suitabilityEntidade.Dados.TextoExibicaoPerfilCarteira = "Seus investimentos nessa conta ap�s essa aplica��o";
                */
                //fim mock

                string mensagemTermoOriginal = suitabilityEntidade.Dados.MensagemTermo;
                string mensagemTermoNova = (suitabilityEntidade.Dados.MensagemAlerta + "\n\n");
                mensagemTermoNova += (suitabilityEntidade.Dados.TextoExibicaoPerfilCliente + ": " + TraduzSuitabilityEnum(suitabilityEntidade.Dados.PerfilCliente) + "\n");
                mensagemTermoNova += (suitabilityEntidade.Dados.TextoExibicaoPerfilCarteira + ": " + TraduzSuitabilityEnum(suitabilityEntidade.Dados.PerfilCarteiraDepois) + "\n\n");
                mensagemTermoNova += mensagemTermoOriginal;
                mensagemTermoNova = mensagemTermoNova.Replace("<p>", "");
                mensagemTermoNova = mensagemTermoNova.Replace("</p>", "\n");

                suitabilityEntidade.Dados.MensagemTermo = mensagemTermoNova;

                if (action == "ConsultaEnquadramentoSuitability")
                {
                    json = JsonConvert.SerializeObject(suitabilityEntidade.Dados, Formatting.None);
                }
                else
                {
                    //AssinarTCD
                    WSSuitabilityItau2.RespostaOfboolean resultadoAssinatura = suitability.AssinarTCD(WSSuitabilityItau2.CanalSuitabilityEnum.Financial, true, cci, "", papelDescricao,
                   valorOperacao, true, operacaoAgendada, true, suitabilityEntidade.Dados.PerfilCliente, true, suitabilityEntidade.Dados.PerfilCarteiraDepois.Value, true);


                    //MOCK:
                    /*WSSuitabilityItau2.RespostaOfboolean resultadoAssinatura = new WSSuitabilityItau2.RespostaOfboolean();
                    resultadoAssinatura.Dados = true;*/


                    json = JsonConvert.SerializeObject(resultadoAssinatura.Dados, Formatting.None);
                    
                }
            }

            context.Response.ContentType = "text/javascript";
            context.Response.Write(json);

        }
        catch (Exception ex)
        {
               
            JSONResponse jsonResponse = new JSONResponse();
            //context.Response.Write("Message: " + ex.Message + Environment.NewLine +
            //    "InnerException: " + (ex.InnerException != null ? ex.InnerException.Message : "") + Environment.NewLine + 
            //    "Stacktrace: " + ex.StackTrace + "");
            
            
            
            
            jsonResponse.success = false;
            jsonResponse.errorMessage = ex.Message;
            jsonResponse.errorDetail = ex.StackTrace;
            
            string serializedText = JsonConvert.SerializeObject(jsonResponse, Formatting.None);
            context.Response.ContentType = "text/html";
            context.Response.Write(serializedText);
        }
        
    }
    
    public class JSONResponse
    {
        public bool success;
        public string errorMessage;
        public string errorDetail;
        
        
        public void Response()
        {
        }

    }
    

    private string TraduzSuitabilityEnum(WSSuitabilityItau2.PerfilSuitabilityEnum? suitabilityEnum){
        string suitabilityString = "";

        if (!suitabilityEnum.HasValue || suitabilityEnum.Value == WSSuitabilityItau2.PerfilSuitabilityEnum.ClienteSemPerfil)
        {
            suitabilityString = "N�o definido";
        }
        else if (suitabilityEnum.Value == WSSuitabilityItau2.PerfilSuitabilityEnum.Agressivo)
        {
            suitabilityString = "Agressivo";
        }
        else if (suitabilityEnum.Value == WSSuitabilityItau2.PerfilSuitabilityEnum.Arrojado)
        {
            suitabilityString = "Arrojado";
        }
        else if (suitabilityEnum.Value == WSSuitabilityItau2.PerfilSuitabilityEnum.Conservador)
        {
            suitabilityString = "Conservador";
        }
        else if (suitabilityEnum.Value == WSSuitabilityItau2.PerfilSuitabilityEnum.Moderado)
        {
            suitabilityString = "Moderado";
        }

        return suitabilityString;
    }
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public class DadosOperacao
    {
        public int IdCliente;
        public string PapelDescricao;
        public DateTime DataOperacao;
        public decimal ValorOperacao;
        public DadosOperacao()
        {
        }
    }

}