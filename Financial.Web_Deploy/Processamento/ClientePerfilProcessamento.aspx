﻿<%@ page language="C#" autoeventwireup="true" inherits="Processamento_ClientePerfilProcessamento, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    
    function OnGetDataPerfil(data) {        
        hiddenIdPerfil.SetValue(data);
        ASPxCallbackPerfil.SendCallback(data);
        popupPerfil.HideWindow();
        btnEditPerfil.Focus();
        gridPerfil.PerformCallback(data);
    }
    
    function OnGetDataCliente(data) {
        hiddenIdCliente.SetValue(data);
        ASPxCallbackCliente.SendCallback(data);
        popupCliente.HideWindow();
        btnEditCliente.Focus();
    }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="ASPxCallbackPerfil" runat="server" OnCallback="ASPxCallback_Callback_Perfil">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditPerfil.SetValue(e.result); } "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallbackCliente" runat="server" OnCallback="ASPxCallback2_Callback_Cliente">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCliente.SetValue(e.result); } "/>
    </dxcb:ASPxCallback>

    <dxpc:ASPxPopupControl ID="popupPerfil" ClientInstanceName="popupPerfil" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridPerfil" runat="server" Width="100%"
                    ClientInstanceName="gridPerfil"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSPerfil" KeyFieldName="IdPerfil"
                    OnCustomDataCallback="gridPerfil_CustomDataCallback" 
                    OnCustomCallback="gridPerfil_CustomCallback"
                    OnHtmlRowCreated="gridPerfil_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="Descricao" VisibleIndex="0" Width="100%"/>
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
                gridPerfil.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataPerfil); 
                }" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Perfil" />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridPerfil.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupCliente" ClientInstanceName="popupCliente" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridCliente" runat="server" Width="100%"
                    ClientInstanceName="gridCliente"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSCliente" KeyFieldName="IdCliente"
                    OnCustomDataCallback="gridCliente_CustomDataCallback" 
                    OnCustomCallback="gridCliente_CustomCallback"
                    OnHtmlRowCreated="gridCliente_HtmlRowCreated"
                    >               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdCliente" VisibleIndex="0" Width="20%"/>
                <dxwgv:GridViewDataTextColumn FieldName="Nome" VisibleIndex="0" Width="100%"/>
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) {
                gridCliente.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataCliente);}" 
                              Init="function(s, e) {e.cancel = true;}"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Cliente" />
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCliente.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Relaciona Perfil de Processamento x Cliente"/>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="CompositeKey" DataSourceID="EsDSPerfilCliente"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="7%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
                <dxwgv:GridViewDataColumn FieldName="IdPerfil" Visible="false"></dxwgv:GridViewDataColumn>
                <dxwgv:GridViewDataColumn FieldName="IdCliente" Visible="false"></dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataColumn FieldName="DescricaoPerfil" Caption="Perfil" UnboundType="String" VisibleIndex="1" Width="20%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Id.Cliente" UnboundType="String" VisibleIndex="1" Width="20%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                <dxwgv:GridViewDataColumn FieldName="DescricaoCliente" Caption="Cliente" UnboundType="String" VisibleIndex="1" Width="40%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataColumn FieldName="Sequencia" Caption="Sequência" UnboundType="String" VisibleIndex="3" Width="8%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                                              
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <dxe:ASPxTextBox ID="hiddenIdPerfil" runat="server" CssClass="hiddenField" Text='<%#Eval("IdPerfil")%>' ClientInstanceName="hiddenIdPerfil" />
                        <dxe:ASPxTextBox ID="hiddenIdCliente" runat="server" CssClass="hiddenField" Text='<%#Eval("IdCliente")%>' ClientInstanceName="hiddenIdCliente" />
                        
                        <table>
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelPerfil" runat="server" CssClass="labelRequired" Text="Perfil:" />
                                </td>                                     
                                
                                <td colspan="3">
                                                                                     
                                    <dxe:ASPxButtonEdit ID="btnEditPerfil" runat="server" CssClass="textButtonEdit" 
                                                        EnableClientSideAPI="True" ClientInstanceName="btnEditPerfil" ReadOnly="true" Width="380px"
                                                        Text='<%#Eval("DescricaoPerfil")%>' OnLoad="btnEditPerfil_Load"> 
                                        <Buttons><dxe:EditButton/></Buttons>        
                                    <ClientSideEvents ButtonClick="function(s, e) {popupPerfil.ShowAtElementByID(s.name);}" />                            
                                    </dxe:ASPxButtonEdit>
                                </td>
                                
                            </tr>  
                            
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:" />
                                </td>                                     
                                
                                <td colspan="3">
                                                                                     
                                    <dxe:ASPxButtonEdit ID="btnEditCliente" runat="server" CssClass="textButtonEdit" 
                                                        EnableClientSideAPI="True" ClientInstanceName="btnEditCliente" ReadOnly="true" Width="380px"
                                                        Text='<%#Eval("DescricaoCliente")%>' OnLoad="btnEditCliente_Load"> 
                                        <Buttons><dxe:EditButton/></Buttons>        
                                    <ClientSideEvents ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" />                            
                                    </dxe:ASPxButtonEdit>
                                </td>
                                
                            </tr>  
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelSequencia" runat="server" CssClass="labelRequired" Text="Sequência:"></asp:Label>                    
                                </td>                    
                                <td colspan="3"> 
                                    <dxe:ASPxSpinEdit ID="textSequencia" runat="server" CssClass="textNormal_5" MaxLength="2" NumberType="integer"
                                    ClientInstanceName="textSequencia" Text='<%#Eval("Sequencia")%>' />
                                </td>                                                      
                            </tr>
                            
                        </table>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                            OnClientClick="operacao='salvarAdd';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal6" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="operacao='salvar';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSPerfilCliente" runat="server" OnesSelect="EsDSPerfilCliente_esSelect" />
    <cc1:esDataSource ID="EsDSPerfil" runat="server" OnesSelect="EsDSPerfil_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    </form>
</body>
</html>