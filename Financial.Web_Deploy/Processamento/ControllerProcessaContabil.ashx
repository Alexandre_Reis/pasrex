<%@ WebHandler Language="C#" Class="ControllerProcessaContabil" %>

using System;
using System.Web;
using Newtonsoft.Json;
using Financial.Util;
using Financial.Investidor;
using System.Collections.Generic;
using EntitySpaces.Interfaces;
using Financial.Processamento;
using Financial.Investidor.Controller;
using Financial.Web.Enums;
using Financial.Investidor.Enums;
using Financial.Contabil.Controller;

public class ControllerProcessaContabil : IHttpHandler {

    public void ProcessRequest(HttpContext context)
        {
            JSONResponse jsonResponse = new JSONResponse();

            Int32 idCliente = Convert.ToInt32(context.Request["idCliente"]);

            DateTime dataInicio = new DateTime();
            dataInicio = Convert.ToDateTime(context.Request["dataInicio"]);
        
            DateTime dataFinal = new DateTime();
            dataFinal = Convert.ToDateTime(context.Request["dataFinal"]);

            bool zeragem = Convert.ToBoolean(context.Request["zeragemContabil"]);

            try
            {
                ControllerContabil controllerContabil = new ControllerContabil();
                controllerContabil.ExecutaProcessoEventosPeriodo(idCliente, dataInicio, dataFinal, zeragem);
                
                jsonResponse.success = true;
            }
            catch (Exception e)
            {
                jsonResponse.success = false;
                jsonResponse.errorMessage = e.Message;
                
            }
            string serializedText = JsonConvert.SerializeObject(jsonResponse, Formatting.None);
            context.Response.ContentType = "text/html";
            context.Response.Write(serializedText);
        }

    public class JSONResponse
    {
        public bool success;
        public string errorMessage;
        public void Response()
        {
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}