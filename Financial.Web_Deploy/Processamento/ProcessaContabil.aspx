﻿<%@ page language="C#" autoeventwireup="true" inherits="Processamento_ProcessaContabil, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" src="../js/jquery.js"></script>

    <script type="text/javascript" src="../js/jquery.progressbar.js"></script>

    <script type="text/javascript" src="../js/process.js?v=3"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown=onDocumentKeyDown;
        var tipoProcesso = 0;    
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '')
            {                   
                alert(e.result);   
                window.processing = false;                              
            }
            else
            {
                zeragemContabil = document.getElementById('checkZeragem').checked;
                Financial.Util.ProgressBar.PrepareProcess({Grid: gridConsulta, 
                                                        ProcessaContabil: true, 
                                                        DataInicio: textDataInicio.GetText(), 
                                                        DataFinal: textDataFim.GetText(),
                                                        ZeragemContabil: zeragemContabil});
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dx:ASPxPopupControl ID="progressWindow" ClientInstanceName="progressWindow" Width="300"
            Height="120" HeaderText="Processamento de Clientes" Modal="true" CloseAction="None"
            ShowCloseButton="false" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
            runat="server">
            <ContentCollection>
                <dx:PopupControlContentControl>
                    <div id="progressBarWrapper">
                        <div id="client-info">
                        </div>
                        <div id="resumo-processamento">
                            <p style="color: green">
                                Processados com sucesso: <span id="total-sucessos">0</span></p>
                            <p id="wrapper-erros" style="color: red; display: none;">
                                Processados com erro: <span id="total-erros">0</span></p>
                        </div>
                        <div class="progressBar" id="spaceused1">
                            0%
                        </div>
                        <div class="linkButton linkButtonNoBorder">
                            <asp:LinkButton ID="btnStop" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                CssClass="btnStop" OnClientClick="Financial.Util.ProgressBar.StopProcess();return false;">
                                <asp:Literal ID="Literal7" runat="server" Text="Parar" /><div>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnClose" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                CssClass="btnOK" OnClientClick="gridConsulta.PerformCallback('btnRefresh'); progressWindow.Hide(); return false;">
                                <asp:Literal ID="Literal1" runat="server" Text="Fechar" /><div>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnErrors" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                CssClass="btnStop" OnClientClick="outputProcessamento.Show();return false;">
                                <asp:Literal ID="Literal9" runat="server" Text="Exibir Erros" /><div>
                                </div>
                            </asp:LinkButton>
                        </div>
                    </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
        <dx:ASPxPopupControl ID="outputProcessamento" ClientInstanceName="outputProcessamento"
            Width="600" Height="420" HeaderText="Resultado do Processamento" Modal="true"
            CloseAction="CloseButton" ScrollBars="Vertical" ShowCloseButton="true" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" runat="server">
            <ContentCollection>
                <dx:PopupControlContentControl>
                    <div id="output-info">
                    </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
        
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Processamento Contábil" />
                            </div>
                            <div id="mainContent">
                                <table class="dropDownInlineWrapper">
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelGrupoProcessamento" runat="server" CssClass="labelNormal" Text="Grupo:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropGrupoProcessamento" runat="server" ClientInstanceName="dropGrupoProcessamento"
                                                DataSourceID="EsDSGrupoProcessamento" ShowShadow="true" DropDownStyle="DropDownList"
                                                CssClass="dropDownListCurto1 dropDownInline" TextField="Descricao" ValueField="IdGrupoProcessamento">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Status:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropStatus" ClientInstanceName="dropStatus" runat="server"
                                                ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto1 dropDownInline">
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Abertos Ñ Calculados" />
                                                    <dxe:ListEditItem Value="2" Text="Calculados" />
                                                    <dxe:ListEditItem Value="3" Text="Fechados" />
                                                    <dxe:ListEditItem Value="4" Text="Último Status Não OK" />
                                                </Items>
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                        
                                        <td>
                                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Data Inicial:"></asp:Label>
                                        </td>                                                                            
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                        </td>
                                        
                                        <td>
                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Data Final:"></asp:Label>                        
                                        </td>                                                                            
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="checkZeragem" runat="server" Text="Zeragem de ano" Checked="false" />
                                        </td>
                                    </tr>
                                </table>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnRun" OnClientClick="
                                        if(window.processing === true){
                                                alert('Processamento já foi iniciado. Por favor, aguarde.');
                                        }else{
                                             window.processing = true; 
                                            callbackErro.SendCallback();
                                        }
                                        return false;
                                        ">
                                        <asp:Literal ID="Literal2" runat="server" Text="Processar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridConsulta.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" KeyFieldName="IdCliente"
                                        DataSourceID="EsDSCliente" OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData"
                                        OnCustomCallback="gridConsulta_CustomCallback">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderStyle HorizontalAlign="Center"/>
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="2" Width="40%" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="IdTipo" VisibleIndex="3"
                                                Width="20%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="4"
                                                Width="8%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Sim" />
                                                        <dxe:ListEditItem Value="2" Text="Não" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Status" Visible="False" />
                                            <dxwgv:GridViewDataTextColumn FieldName="DataContabil" Caption="Data Contábil" UnboundType="String"
                                                VisibleIndex="4" Width="10%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="6" Width="10%" />
                                        </Columns>
                                        <SettingsPopup EditForm-Width="500px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoProcessamento" runat="server" OnesSelect="EsDSGrupoProcessamento_esSelect" />
    </form>
</body>
</html>
