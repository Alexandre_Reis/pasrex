﻿<%@ page language="C#" autoeventwireup="true" inherits="Batimento_BatimentoCC, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    </script>
</head>
<body>
    <form id="form1" runat="server">
                                             
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Batimento de C/C (Sinacor)" />
                            </div>
                            <div id="mainContent">
                                
                                <table class="dropDownInlineWrapper" border="0" width="350">
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelGrupoProcessamento" runat="server" CssClass="labelNormal" Text="Grupo:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropGrupoProcessamento" runat="server" ClientInstanceName="dropGrupoProcessamento"
                                                DataSourceID="EsDSGrupoProcessamento" ShowShadow="true" DropDownStyle="DropDownList"
                                                CssClass="dropDownListCurto1 dropDownInline" TextField="Descricao" ValueField="IdGrupoProcessamento">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxComboBox>
                                        </td>                                            
                                        <td>    
                                            <dxe:ASPxTextBox ID="textLista" ClientInstanceName="textLista" runat="server" CssClass="textNormal" MaxLength="100" OnPreRender="ListaNumeros_OnPreRender" ClientEnabled="false" DisabledStyle-BackColor="#EBEBEB" Width="200"/>
                                        </td>
                                    </tr>
                                </table>
                                
                                <div class="linkButton">                                                                                                                                                                                                                                                                                
                                    <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" CssClass="btnRun" OnClientClick=" gridConsulta.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal2" runat="server" Text="Consultar"/><div></div></asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Exportar"/><div></div></asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Exportar"/><div></div></asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridConsulta.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>                                                                                                            
                                </div>
                                
                                <div class="divDataGrid">
                                                                
                                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" KeyFieldName="IdCliente" AutoGenerateColumns="False"
                                    DataSourceID="DS_Consulta" ClientInstanceName="gridConsulta" OnCustomCallback="gridConsulta_CustomCallback">
                                        
                                        <Columns>
                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Código" VisibleIndex="1" Width="10%" >
                                                <CellStyle HorizontalAlign="Left"/>
                                            </dxwgv:GridViewDataColumn>
                                                                                
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="40%" />                                            
                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" Caption="Data Cliente" VisibleIndex="3" Width="10%" />
                                            
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="SaldoFinancial" VisibleIndex="4" Width="15%" >
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="SaldoSinacor" VisibleIndex="5" Width="15%" >
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="Diferenca" Caption="Diferença" VisibleIndex="6" Width="15%" >
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                                            </dxwgv:GridViewDataSpinEditColumn>
                                        
                                        </Columns>
                                        
                                        <SettingsPopup EditForm-Width="500px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta" Landscape="true"/>
        <cc1:esDataSource ID="EsDSGrupoProcessamento" runat="server" OnesSelect="EsDSGrupoProcessamento_esSelect" />

        <asp:ObjectDataSource ID="DS_Consulta" runat="server" SelectMethod="CarregaGridCCSinacor" TypeName="Financial.Batimentos.BatimentoCC">
            <SelectParameters>
                <asp:ControlParameter ControlID="dropGrupoProcessamento" Name="idGrupo" PropertyName="Value" Type="Int32" />
            </SelectParameters>                    
        </asp:ObjectDataSource>    
        
    </form>
       
</body>

</html>