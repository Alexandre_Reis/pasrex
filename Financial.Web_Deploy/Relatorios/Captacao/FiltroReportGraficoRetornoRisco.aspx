﻿<%@ page language="C#" autoeventwireup="true" inherits="FiltroReportGraficoRetornoRisco, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
</head>

<body>
    <form id="form1" runat="server">    
        
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
                    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
                       
    <div id="header"><asp:Label ID="lblHeader" runat="server" Text="Gráfico de Retorno X Risco"/></div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <div class="reportFilter">
                                        
        <div style="height:20px"></div>
        
        <table cellpadding="2" cellspacing="2">          
        <tr>
            <td class="td_Label">
                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteiras:" />
            </td>
            <td>
                <dx:ASPxGridLookup ID="dropCarteira" ClientInstanceName="dropCarteira" runat="server" SelectionMode="Multiple" KeyFieldName="IdCarteira" DataSourceID="EsDSCarteira"
                                   Width="350px" TextFormatString="{0}" MultiTextSeparator=", " Font-Size="11px" IncrementalFilteringMode="StartsWith">
                     <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />                        
                         <dx:GridViewDataColumn FieldName="IdCarteira" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>                         
                         <dx:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small"/>                            
                    </Columns>
                    
                    <GridViewProperties>
                        <Settings ShowFilterRow="True" />
                    </GridViewProperties>
                    
                </dx:ASPxGridLookup>
            </td>
        </tr>
        
        <tr>
        
        <tr>
            <td class="td_Label">
                <asp:Label ID="labelTipoRelatorio" runat="server" CssClass="labelRequired" Text="Coluna X: " />
            </td>
            
            <td colspan="3">
                <dxe:ASPxComboBox ID="dropTipoRetorno" runat="server" CssClass="dropDownListCurto" ClientInstanceName="dropTipoRetorno">                    
                <Items>
                    <dxe:ListEditItem Value="0" Text="Retorno Efetivo" />
                    <dxe:ListEditItem Value="1" Text="Retorno Anualizado" />
                </Items>
                </dxe:ASPxComboBox>
            </td>
        </tr>
        
        <td class="td_Label"><asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:"/></td>
        
        <td>
           <table cellpadding="0" cellspacing="0">                
            <tr>
            <td><dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"/></td>
            <td class="labelCurto"><asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:"/></td>
            <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td>
            </tr>                                                            
           </table>
                     
         </td></tr>

        </table>
        
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>
    
    </ContentTemplate>
    </asp:UpdatePanel>            
                        
    </div>
    
    </div>
    </td></tr>
    </table>
    
    </div>    
        
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        
    </form>
</body>
</html>