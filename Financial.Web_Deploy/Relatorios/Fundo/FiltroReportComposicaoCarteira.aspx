﻿<%@ page language="C#" autoeventwireup="true" inherits="FiltroReportComposicaoCarteira, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxnb" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="JavaScript">
function OnGetDataCliente(values) {
    btnEditCodigo.SetValue(values);                    
    popupCliente.HideWindow();
    ASPxCallback1.SendCallback(btnEditCodigo.GetValue());        
    btnEditCodigo.Focus();    
}
</script>
<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body >
    <form id="form1" runat="server"> 
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            var resultSplit = e.result.split('|');
            e.result = resultSplit[0] + '|' + resultSplit[1] + '|' + resultSplit[3];
            
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigo, document.getElementById('textNome'), textData);
                        
            tipoCota = resultSplit[2];
            if (tipoCota == 'Fech') {
                dropTipoRelatorio.SetEnabled(false);
            }
            else {
                dropTipoRelatorio.SetEnabled(true);
            }    
                         
            //var newDate = LocalizedData(resultSplit[1], resultSplit[3]);                                          
            //textData.SetValue(newDate);                               
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
                       
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Composição da Carteira"></asp:Label>
    </div>
        
    <div id="mainContentSpace">
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        
        <div class="reportFilter">
        
        <div style="height:20px"></div>                                  
                                        
        <table cellpadding="2" cellspacing="2">
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigo" runat="server" CssClass="textButtonEdit" 
                                            EnableClientSideAPI="True" ClientInstanceName="btnEditCodigo"
                                            MaxLength="15" NumberType="Integer" AllowMouseWheel="false">  
                <Buttons>
                    <dxe:EditButton></dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents                        
                         KeyPress="function(s, e) {document.getElementById('textNome').value = '';}" 
                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}"                           
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigo);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td colspan="2" width="300">
                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
            </td>
            </tr>
                        
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
            </td>
            
            <td colspan="3">
                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" CssClass="textData" />                
            </td>                                                                
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelTipoRelatorio" runat="server" CssClass="labelNormal" Text="Tipo: " />
            </td>
            
            <td colspan="3">
                <dxe:ASPxComboBox ID="dropTipoRelatorio" runat="server" CssClass="dropDownListCurto" ClientInstanceName="dropTipoRelatorio">                    
                <Items>
                    <dxe:ListEditItem Value="0" Text="Abertura" />
                    <dxe:ListEditItem Value="1" Text="Fechamento" />
                </Items>
                </dxe:ASPxComboBox>
            </td>
            </tr>
            
            <tr>
                <td class="td_Label">
                    <asp:Label ID="labelMoeda" runat="server" CssClass="labelNormal" Text="Moeda:"/>
                </td>                
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropMoeda" runat="server" ClientInstanceName="dropMoeda"
                                        DataSourceID="EsDSMoeda" IncrementalFilteringMode="StartsWith" 
                                        ShowShadow="false" DropDownStyle="DropDownList"
                                        CssClass="dropDownListCurto" TextField="Nome" ValueField="IdMoeda">                                        
                    </dxe:ASPxComboBox>         
                </td>
            </tr>            
                        
            <tr>                                   
                <td class="td_Label">
                    <asp:Label ID="labelLingua" runat="server" CssClass="labelNormal" Text="Lingua: " />
                </td>
                    
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropLingua" runat="server" CssClass="dropDownListCurto" IncrementalFilteringMode="StartsWith"
                            ClientInstanceName="dropLingua" SelectedIndex="0" DropDownStyle="DropDownList">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Português" />
                        <dxe:ListEditItem Value="2" Text="Inglês" />
                    </Items>
                    </dxe:ASPxComboBox>
                </td>            
            </tr>
            
            <tr>                                   
                <td class="td_Label">
                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Separa Custódia: " />
                </td>
                    
                <td colspan="3">
                    <dxe:ASPxComboBox ID="dropCustodia" runat="server" CssClass="dropDownListCurto" ClientInstanceName="dropCustodia" SelectedIndex="0" >
                    <Items>
                        <dxe:ListEditItem Value="S" Text="Sim" />
                        <dxe:ListEditItem Value="N" Text="Não" />
                    </Items>
                    </dxe:ASPxComboBox>
                </td>            
            </tr>
            
        </table>                                                                                 
        
        </div>
                                                                
        <div id="reportLinkButton" class="linkButton linkButtonNoBorder">
           <asp:LinkButton ID="btnVisualiza" runat="server" Font-Overline="false" CssClass="btnReport" OnClick="btnVisualiza_Click"><asp:Literal ID="Literal1" runat="server" Text="Visualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"  CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
        </div>
        
    </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
    
    </div>
    </td></tr></table>
    </div>
            
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />        
    <cc1:esDataSource ID="EsDSMoeda" runat="server" OnesSelect="EsDSMoeda_esSelect" />
    
   </form>
</body>
</html>