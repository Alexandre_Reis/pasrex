﻿<%@ page language="C#" autoeventwireup="true" inherits="FiltroReportDesempenhoFundo, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxnb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>


<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../../js/global.js"></script>

    <script type="text/javascript">
        function btnExportaClick() {
            LoadingPanel.Show();
            callbackExporta.SendCallback();
        }
    </script>

</head>
<body>
    <dxcb:ASPxCallback ID="callbackExporta" runat="server" OnCallback="callbackExporta_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanel.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe =
                        document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                }else{
                    alert(e.result);
                }
            }
        }" />
    </dxcb:ASPxCallback>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" AsyncPostBackTimeout="360000" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="divPanel divPanelNew">
                    <div id="container_small">
                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Desempenho Fundo"></asp:Label>
                        </div>
                        <div id="mainContent" style="width: 850px">
                            <div class="reportFilter">
                                <div class="dataMessage">
                                </div>
                                <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" ShowHeader="False" BackColor="White" Border-BorderColor="#AECAF0">
                                    <Border BorderColor="#AECAF0"></Border>
                                    <PanelCollection>
                                        <dxp:PanelContent runat="server">
                                            <dxtc:ASPxPageControl ID="tabArquivos" runat="server"
                                                ClientInstanceName="tabArquivos" Height="100%" Width="100%" TabSpacing="0px" ActiveTabIndex="1">
                                                <TabPages>
                                                    <dxtc:TabPage Text="Desempenho Fundo">
                                                        <ContentCollection>
                                                            <dxw:ContentControl runat="server">
                                                                <div class="divDataGrid">
                                                                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" ClientInstanceName="gridConsulta"
                                                                        AutoGenerateColumns="False" DataSourceID="EsDSCliente" KeyFieldName="IdCliente"
                                                                        OnHtmlRowCreated="gridConsulta_HtmlRowCreated" Width="850px" OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData"
                                                                        OnCustomCallback="gridConsulta_CustomCallback">
                                                                        <Columns>
                                                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="10%" ButtonType="Image" ShowClearFilterButton="True">
                                                                                <HeaderTemplate>
                                                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll" />
                                                                                </HeaderTemplate>
                                                                            </dxwgv:GridViewCommandColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left">
                                                                                <CellStyle HorizontalAlign="Left">
                                                                                </CellStyle>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="2" Width="25%"
                                                                                Settings-AutoFilterCondition="Contains">
                                                                                <Settings AutoFilterCondition="Contains"></Settings>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="3" Width="9%" />
                                                                            <dxwgv:GridViewDataTextColumn FieldName="StatusDescricao" Caption="Status"
                                                                                UnboundType="String" VisibleIndex="4" Width="12%"
                                                                                Settings-AutoFilterCondition="Contains">
                                                                                <Settings AutoFilterCondition="Contains"></Settings>
                                                                            </dxwgv:GridViewDataTextColumn>
                                                                            <dxwgv:GridViewDataTextColumn FieldName="Status" Visible="False" />
                                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Cliente" FieldName="IdTipo" VisibleIndex="5" Width="15%">
                                                                                <EditFormSettings Visible="False" />
                                                                                <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" />
                                                                            </dxwgv:GridViewDataComboBoxColumn>
                                                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="8"
                                                                                Width="5%" ExportWidth="100">
                                                                                <EditFormSettings Visible="False" />
                                                                                <PropertiesComboBox EncodeHtml="false">
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="1" Text="<div title='Sim'>Sim</div>" />
                                                                                        <dxe:ListEditItem Value="2" Text="<div title='Não'>Não</div>" />
                                                                                    </Items>
                                                                                </PropertiesComboBox>
                                                                            </dxwgv:GridViewDataComboBoxColumn>

                                                                        </Columns>
                                                                        <SettingsPager Mode="ShowAllRecords">
                                                                        </SettingsPager>
                                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" ShowVerticalScrollBar="True" />
                                                                        <SettingsText EmptyDataRow="0 registros" />
                                                                        <Images />
                                                                        <Styles>
                                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                                            <AlternatingRow Enabled="True" />
                                                                            <Cell Wrap="False" />
                                                                        </Styles>
                                                                    </dxwgv:ASPxGridView>
                                                                </div>
                                                                <table border="0">
                                                                    <tr>
                                                                        <td>

                                                                            <table cellpadding="2" cellspacing="2" border="0">
                                                                                <tr>
                                                                                    <td>

                                                                                        <dxrp:ASPxRoundPanel ID="pnlPeriodo" runat="server" HeaderText="Período" HeaderStyle-Font-Size="11px" Width="610px">
                                                                                            <PanelCollection>
                                                                                                <dxp:PanelContent runat="server">
                                                                                                    <table border="0" width="100%">
                                                                                                        <tr>
                                                                                                            <td style="width: 400px">
                                                                                                                <dxe:ASPxRadioButtonList ID="periodo" runat="server" ClientInstanceName="periodo" ValueType="System.Int32" RepeatDirection="Vertical" Width="100%">
                                                                                                                    <ValidationSettings>
                                                                                                                        <RequiredField IsRequired="true" />
                                                                                                                    </ValidationSettings>
                                                                                                                    <Items>
                                                                                                                        <dxe:ListEditItem Value="1" Text="12 meses findos em 31 de Dezembro" Selected="true" />
                                                                                                                        <dxe:ListEditItem Value="2" Text="12 meses findos em 30 de Junho" />
                                                                                                                    </Items>
                                                                                                                </dxe:ASPxRadioButtonList>
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <asp:Label ID="label5" runat="server" CssClass="labelRequired labelFloat" Text="Ano:" />
                                                                                                            </td>
                                                                                                            <td>
                                                                                                                <dxe:ASPxComboBox ID="dropAno" runat="server" ClientInstanceName="dropAno"
                                                                                                                    OnInit="dropAno_Init" CssClass="dropDownListCurto_2" DropDownRows="10" />
                                                                                                            </td>

                                                                                                            <td>
                                                                                                                <div id="Div3" class="linkButton linkButtonNoBorder  linkButtonNoMargin">
                                                                                                                    <asp:LinkButton ID="btnTipo_yyyy" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                                                                        OnClientClick="btnExportaClick(); return false;">
                                                                                                                        <asp:Literal ID="Literal3" runat="server" Text="Exportar" /><div>
                                                                                                                        </div>
                                                                                                                    </asp:LinkButton>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>

                                                                                                    </table>
                                                                                                </dxp:PanelContent>
                                                                                            </PanelCollection>
                                                                                        </dxrp:ASPxRoundPanel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dxw:ContentControl>
                                                        </ContentCollection>
                                                    </dxtc:TabPage>
                                                </TabPages>
                                            </dxtc:ASPxPageControl>
                                        </dxp:PanelContent>
                                    </PanelCollection>
                                </dxrp:ASPxRoundPanel>
                            </div>
                        </div>
                    </div>
                </div>
                
            </ContentTemplate>
        </asp:UpdatePanel>

        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />
        <dxwgv:ASPxGridView ID="gridExportacao1" runat="server" Visible="false" />
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridExportacao1" />

        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Exportando arquivo, aguarde..." ClientInstanceName="LoadingPanel" Modal="true" />
    </form>
</body>
</html>
