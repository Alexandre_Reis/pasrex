﻿<%@ page language="C#" autoeventwireup="true" inherits="Relatorios_AgendaFluxoAtivosDeCreditos, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Agenda e Fluxo de Ativos de Creditos</title>
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraFiltro.Focus();
    }        
    </script>

</head>
<body>
<dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else
            {                
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira);
                
                if (gridCadastro.cp_EditVisibleIndex == 'new') {
                                
                    if ( resultSplit[1] != '') {
                                                                                
                        if (resultSplit[1] != null && resultSplit[1] != '') {                        
                            var newDate = LocalizedData(resultSplit[1], resultSplit[2]);
                            textDataOperacao.SetValue(newDate);
                        }                    
                        else {
                            textDataOperacao.SetValue(null);
                        }
                    }
                }                                                                
            }
        }        
        " />
        </dxcb:ASPxCallback>
    <form id="form1" runat="server">
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Agenda e Fluxo de Ativos de Creditos"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelCarteiraFiltro" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoCarteiraFiltro" EnableClientSideAPI="true" CssFilePath="../../css/forms.css"
                                                            SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCarteiraFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteiraFiltro);}" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2" width="450">
                                                        <asp:TextBox ID="textNomeCarteiraFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter"
                                        OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal2" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" DataSourceID="EsDSAgendaEFluxo"
                                        AutoGenerateColumns="False" OnCustomCallback="gridCadastro_CustomCallback">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn Visible="false" ShowClearFilterButton="True"/>
                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="1">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="2">
                                            </dxwgv:GridViewDataDateColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="3">
                                            </dxwgv:GridViewDataDateColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="PLFechamento" VisibleIndex="4">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" VisibleIndex="5">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Nome" VisibleIndex="6">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="RiscoFinal" VisibleIndex="7">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="8">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorMercado" VisibleIndex="9">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataEvento" VisibleIndex="10">
                                            </dxwgv:GridViewDataDateColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="TipoEvento" VisibleIndex="11">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Taxa" VisibleIndex="12">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" VisibleIndex="13">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Comentario" VisibleIndex="14">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorIR" VisibleIndex="15">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorIOF" VisibleIndex="16">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorLiquido" VisibleIndex="17">
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Settings ShowFilterRow="True" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSAgendaEFluxo" runat="server" OnesSelect="EsDSAgendaEFluxo_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>
