﻿<%@ page language="C#" autoeventwireup="true" inherits="Login_TrocarSenha, Financial.Web_Deploy" title="Trocar Senha" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<html>
<head id="Head1" runat="server">
    <link href="~/css/login.css?v2" rel="stylesheet" type="text/css" />
    <link href="~/cssCustom/login.css" rel="stylesheet" type="text/css" />
    <link href="~/css/keyboard.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />

    <script type="text/javascript" language="Javascript" src="../js/ext-core.js"></script>
    <script type="text/javascript" language="Javascript" src="../js/login/login.js"></script>
    <script type="text/javascript" language="Javascript" src="../jsCustom/login/login.js"></script>

</head>
<body onresize="window.location.reload();">
    <form id="Form1" class="trocar_senha" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>

        <dxcb:ASPxCallback ID="callbackSenha" runat="server" OnCallback="callbackSenha_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
                var returnURL = e.result.split('=');
                if(returnURL && returnURL.length && returnURL[0] == 'ReturnURL'){
                    window.location.href = returnURL[1];
                } else {
                    alert(e.result);
                }
        }
        " />
        </dxcb:ASPxCallback>

        <script type="text/javascript" language="JavaScript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            function InitializeRequest(sender, args) {
                $get('Form1').style.cursor = 'wait'; 

                // Get a reference to the element that raised the postback,
                //   and disables it.
                $get(args._postBackElement.id).disabled = true;
            }

            function EndRequest(sender, args) {
                $get('Form1').style.cursor = 'auto';

                // Get a reference to the element that raised the postback
                //   which is completing, and enable it.
                $get(sender._postBackSettings.sourceElement.id).disabled = false;
                
                Financial.Login.updateHTML();
            }            
        </script>
        <table id="Login1">
            <tr>
                <td>
		            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
			            <ContentTemplate>
				            <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="input_text"/>
            				 
				            <asp:TextBox ID="PasswordConfirmation" TextMode="Password" runat="server" CssClass="input_text"></asp:TextBox>

				            <asp:LinkButton ID="LoginButton" runat="server" Font-Underline="false" CommandName="Login"
					             Text="{text}" CssClass="btn_login" OnClientClick="callbackSenha.SendCallback(); return false;">
				            </asp:LinkButton>
				            <div id="FailureText" class="failure-text">
					            <asp:Literal ID="Literal1" runat="server" EnableViewState="False"></asp:Literal>
				            </div>
			            </ContentTemplate>
		            </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>