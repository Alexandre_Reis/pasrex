<%@ WebHandler Language="C#" Class="LoginAjax" %>

using System;
using System.Web;
using Financial.Security;
using System.Web.Security;
using System.Collections.Specialized;

public class LoginAjax : IHttpHandler, System.Web.SessionState.IRequiresSessionState {
    
    public void ProcessRequest (HttpContext context) {

        bool valid = false;
        
        string username = context.Request["login"];
        string password = context.Request["password"];

        FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
        NameValueCollection listaValores = new NameValueCollection();
        listaValores.Add("passwordFormat", "Encrypted");
        financialMembershipProvider.Initialize(null, listaValores);
        
        if (financialMembershipProvider.ValidateUser(username, password))
        {
            valid = true;
            bool rememberMe = false;

            if (financialMembershipProvider.IsUserMaster(username.ToLower()))
            {
                GrupoUsuarioCollection grupoUsuarioCollection = new GrupoUsuarioCollection();

                grupoUsuarioCollection.Query.Where(grupoUsuarioCollection.Query.TipoPerfil == (byte)Financial.Security.Enums.TipoPerfilGrupo.Administrador,
                                                   grupoUsuarioCollection.Query.IdGrupo != 0)
                                            .OrderBy(grupoUsuarioCollection.Query.IdGrupo.Ascending);

                grupoUsuarioCollection.Query.Load();

                // Sempre existe
                int idGrupo = grupoUsuarioCollection[0].IdGrupo.Value;

                UsuarioCollection usuarioCollection = new UsuarioCollection();
                usuarioCollection.Query.Where(usuarioCollection.Query.IdGrupo == idGrupo)
                                 .OrderBy(usuarioCollection.Query.IdUsuario.Ascending);

                usuarioCollection.Query.Load(); // Sempre existe
                //
                string login = usuarioCollection[0].Login.Trim().ToLower();

                context.Session["IsUserMaster"] = true; // Indica que � usuario master
                //
                FormsAuthentication.SetAuthCookie(login, rememberMe);
            }
            else
            {
                context.Session["IsUserMaster"] = false;
                FormsAuthentication.SetAuthCookie(username, rememberMe);
            }
        }
        
        //Usuario usuario = new Usuario();
        //usuario.BuscaUsuario(Username.Text.ToString());

        context.Response.ContentType = "text/javascript";
        context.Response.Write("{success: " + valid.ToString().ToLower() + "}");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}