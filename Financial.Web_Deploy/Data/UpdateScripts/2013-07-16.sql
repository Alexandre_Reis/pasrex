declare @data_versao char(10)
set @data_versao = '2013-07-16'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table enquadraregra add RealTime tinyint null
go

update enquadraregra set RealTime = 2 --Nao faz
go

alter table enquadraregra alter column RealTime tinyint not null
go