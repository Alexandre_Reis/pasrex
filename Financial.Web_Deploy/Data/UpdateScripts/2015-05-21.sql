declare @data_versao char(10)
set @data_versao = '2015-05-21'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


IF NOT EXISTS(SELECT * FROM sys.columns WHERE NAME = N'ProventoBolsaCliente' and Object_ID = Object_ID(N'TipoPosicaoBolsa'))
BEGIN
	ALTER TABLE ProventoBolsaCliente add TipoPosicaoBolsa smallint 
END
GO	
