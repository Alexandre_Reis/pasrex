declare @data_versao char(10)
set @data_versao = '2014-02-05'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table cotacaoindice add FatorDiario decimal(38,28)
go
alter table cotacaoindice add FatorAcumulado decimal(38,28)
go

 