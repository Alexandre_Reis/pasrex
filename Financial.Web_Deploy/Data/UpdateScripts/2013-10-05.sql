declare @data_versao char(10)
set @data_versao = '2013-10-05'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_Normal  DEFAULT ((0)) FOR ResultadoNormal
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_DT  DEFAULT ((0)) FOR ResultadoDT
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_ResultadoLiquidoMes  DEFAULT ((0)) FOR ResultadoLiquidoMes
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_ResultadoNegativoAnterior  DEFAULT ((0)) FOR ResultadoNegativoAnterior
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_BaseCalculo  DEFAULT ((0)) FOR BaseCalculo
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_PrejuizoCompensar  DEFAULT ((0)) FOR PrejuizoCompensar
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_Imposto  DEFAULT ((0)) FOR ImpostoCalculado
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_ResultadoLiquidoMesDT  DEFAULT ((0)) FOR ResultadoLiquidoMesDT
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_ResultadoNegativoAnteriorDT  DEFAULT ((0)) FOR ResultadoNegativoAnteriorDT
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_BaseCalculoDT  DEFAULT ((0)) FOR BaseCalculoDT
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_PrejuizoCompensarDT  DEFAULT ((0)) FOR PrejuizoCompensarDT
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_ImpostoDT  DEFAULT ((0)) FOR ImpostoCalculadoDT
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_TotalImposto  DEFAULT ((0)) FOR TotalImposto
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_IRDayTradeMes  DEFAULT ((0)) FOR IRDayTradeMes
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_IRDayTradeMesAnterior  DEFAULT ((0)) FOR IRDayTradeMesAnterior
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_IRDayTradeCompensar  DEFAULT ((0)) FOR IRDayTradeCompensar
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_IRFonteNormal  DEFAULT ((0)) FOR IRFonteNormal
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_ImpostoPagar  DEFAULT ((0)) FOR ImpostoPagar
GO

ALTER TABLE ApuracaoIRImobiliario ADD  CONSTRAINT DF_ApuracaoIRImobiliario_ImpostoPago  DEFAULT ((0)) FOR ImpostoPago
GO


