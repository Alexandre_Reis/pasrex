declare @data_versao char(10)
set @data_versao = '2013-06-11'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Setar Tempo Expiracao de Senha para 0 de forma a nao solicitar a todos os clientes a alteracao de senha
update configuracao set ValorNumerico = 0 where Id = 2010