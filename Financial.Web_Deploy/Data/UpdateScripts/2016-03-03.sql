﻿declare @data_versao char(10)
set @data_versao = '2016-03-03'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

begin transaction

	-- não permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)

	--DDL
	IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'MediaMovel_IDX')
	BEGIN
		CREATE NONCLUSTERED INDEX MediaMovel_IDX
		ON [dbo].[MediaMovel] ([DataAtual],[TipoMediaMovel])
		INCLUDE ([IdMediaMovel],[IdCliente],[MediaMovel],[idPosicao],[ChaveComposta])
	END

	IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoRendaFixaAbertura_IDX')
	BEGIN
		CREATE NONCLUSTERED INDEX PosicaoRendaFixaAbertura_IDX
		ON [dbo].[PosicaoRendaFixaAbertura] ([IdCliente],[DataHistorico])
		INCLUDE ([IdPosicao],[IdTitulo],[TipoOperacao],[DataVencimento],[Quantidade],[QuantidadeBloqueada],[DataOperacao],[DataLiquidacao],[PUOperacao],[PUCurva],[ValorCurva],[PUMercado],[ValorMercado],[PUJuros],[ValorJuros],[DataVolta],[TaxaVolta],[PUVolta],[ValorVolta],[QuantidadeInicial],[ValorIR],[ValorIOF],[TipoNegociacao],[PUCorrecao],[ValorCorrecao],[TaxaOperacao],[IdAgente],[IdCustodia],[CustoCustodia],[IdIndiceVolta],[IdOperacao],[OperacaoTermo],[ValorBrutoGrossUp])
	END
	--

	--DML
	if not exists (select 1 from PermissaoMenu where IdGrupo = 1 and IdMenu = 1295) 
	begin
		insert into PermissaoMenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) values (1, 1295, 'S','S','S','S')
	end
	--

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off