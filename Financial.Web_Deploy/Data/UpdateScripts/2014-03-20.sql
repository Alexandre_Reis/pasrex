declare @data_versao char(10)
set @data_versao = '2014-03-20'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE [dbo].[SuitabilityQuestao](
	[IdQuestao] [int] IDENTITY(1,1) NOT NULL,
	[Descricao] [varchar](2000) NOT NULL,
	[Fator] [int] NOT NULL,
 CONSTRAINT [PK_SuitabilityQuestao] PRIMARY KEY CLUSTERED 
(
	[IdQuestao] ASC
)
)
GO

CREATE TABLE [dbo].[SuitabilityOpcao](
	[IdOpcao] [int] IDENTITY(1,1) NOT NULL,
	[IdQuestao] [int] NOT NULL,
	[Descricao] [varchar](2000) NOT NULL,
	[Pontos] [int] NOT NULL,
 CONSTRAINT [PK_SuitabilityOpcao] PRIMARY KEY CLUSTERED 
(
	[IdOpcao] ASC
)
)
GO

CREATE TABLE [dbo].[SuitabilityResposta](
	[IdResposta] [int] IDENTITY(1,1) NOT NULL,
	[Data] [datetime] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdQuestao] [int] NOT NULL,
	[IdOpcaoEscolhida] [int] NOT NULL,
	[RiskPoints] [int] NOT NULL,
 CONSTRAINT [PK_SuitabilityResposta] PRIMARY KEY CLUSTERED 
(
	[IdResposta] ASC
)
)
GO

CREATE TABLE [dbo].[SuitabilityPerfil](
	[IdPerfilInvestidor] [tinyint] NOT NULL,
	[Data] [datetime] NOT NULL,
	[Pontos] [int] NOT NULL,
 CONSTRAINT [PK_SuitabilityPerfil] PRIMARY KEY CLUSTERED 
(
	[IdPerfilInvestidor] ASC,
	[Data] ASC
)
)
GO

/****** Object:  ForeignKey [FK_SuitabilityOpcao_SuitabilityQuestao]    Script Date: 03/20/2014 13:32:38 ******/
ALTER TABLE [dbo].[SuitabilityOpcao]  WITH CHECK ADD  CONSTRAINT [FK_SuitabilityOpcao_SuitabilityQuestao] FOREIGN KEY([IdQuestao])
REFERENCES [dbo].[SuitabilityQuestao] ([IdQuestao])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SuitabilityOpcao] CHECK CONSTRAINT [FK_SuitabilityOpcao_SuitabilityQuestao]
GO
/****** Object:  ForeignKey [FK_SuitabilityPerfil_PerfilInvestidor]    Script Date: 03/20/2014 13:32:38 ******/
ALTER TABLE [dbo].[SuitabilityPerfil]  WITH CHECK ADD  CONSTRAINT [FK_SuitabilityPerfil_PerfilInvestidor] FOREIGN KEY([IdPerfilInvestidor])
REFERENCES [dbo].[PerfilInvestidor] ([IdPerfilInvestidor])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SuitabilityPerfil] CHECK CONSTRAINT [FK_SuitabilityPerfil_PerfilInvestidor]
GO
/****** Object:  ForeignKey [FK_SuitabilityResposta_SuitabilityOpcao]    Script Date: 03/20/2014 13:32:38 ******/
ALTER TABLE [dbo].[SuitabilityResposta]  WITH CHECK ADD  CONSTRAINT [FK_SuitabilityResposta_SuitabilityOpcao] FOREIGN KEY([IdOpcaoEscolhida])
REFERENCES [dbo].[SuitabilityOpcao] ([IdOpcao])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SuitabilityResposta] CHECK CONSTRAINT [FK_SuitabilityResposta_SuitabilityOpcao]
GO
/****** Object:  ForeignKey [FK_SuitabilityResposta_SuitabilityQuestao]    Script Date: 03/20/2014 13:32:38 ******/
ALTER TABLE [dbo].[SuitabilityResposta]  WITH CHECK ADD  CONSTRAINT [FK_SuitabilityResposta_SuitabilityQuestao] FOREIGN KEY([IdQuestao])
REFERENCES [dbo].[SuitabilityQuestao] ([IdQuestao])
GO
ALTER TABLE [dbo].[SuitabilityResposta] CHECK CONSTRAINT [FK_SuitabilityResposta_SuitabilityQuestao]
GO
/****** Object:  ForeignKey [FK_SuitabilityResposta_Usuario]    Script Date: 03/20/2014 13:32:38 ******/
ALTER TABLE [dbo].[SuitabilityResposta]  WITH CHECK ADD  CONSTRAINT [FK_SuitabilityResposta_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuario] ([IdUsuario])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SuitabilityResposta] CHECK CONSTRAINT [FK_SuitabilityResposta_Usuario]
GO



--Menu Tabela Processamento
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1200,'S','S','S','S' from grupousuario where idgrupo <> 0

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1205,'S','S','S','S' from grupousuario where idgrupo <> 0

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1210,'S','S','S','S' from grupousuario where idgrupo <> 0

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1215,'S','S','S','S' from grupousuario where idgrupo <> 0
