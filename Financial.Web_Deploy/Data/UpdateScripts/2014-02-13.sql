declare @data_versao char(10)
set @data_versao = '2014-02-13'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu Posicao Geral de Cotistas
begin try
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,10245,'S','S','S','S' from grupousuario where idgrupo <> 0
end try
begin catch
end catch

