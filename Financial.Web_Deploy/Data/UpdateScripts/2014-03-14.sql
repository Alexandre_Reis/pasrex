declare @data_versao char(10)
set @data_versao = '2014-03-14'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaoswap add DataRegistro datetime null
go

update operacaoswap set DataRegistro = DataEmissao
go

alter table operacaoswap alter column DataRegistro datetime not null
go



alter table operacaorendafixa add DataRegistro datetime null
go

update operacaorendafixa set DataRegistro = DataOperacao
go

alter table operacaorendafixa alter column DataRegistro datetime not null
go


alter table operacaoswap alter column NumeroContrato varchar(20) not null
go
alter table posicaoswap alter column NumeroContrato varchar(20) not null
go
alter table posicaoswaphistorico alter column NumeroContrato varchar(20) not null
go
alter table posicaoswapabertura alter column NumeroContrato varchar(20) not null
go