﻿
select distinct
	o.idcliente as CCI,
	p.cpfcnpj as CPF,
	a.codigo as Agencia,
	RTRIM(LTRIM(c.numero))as Conta,
	o.dataoperacao as DataOperacao,
	t.descricao as Mercadoria,
	o.taxaoperacao as Taxa,
	i.descricao as Indice,
	t.dataemissao as Emissao,
	t.datavencimento as Vencimento,
	e.nome as Emissor,
	case 
		when o.tipooperacao=1 then 'Compra Final'
		when o.tipooperacao=2 then 'Venda Final'
		when o.tipooperacao=3 then 'Compra Revenda'
		when o.tipooperacao=4 then 'Venda Recompra'
		when o.tipooperacao=6 then 'Venda Total'
		when o.tipooperacao=10 then 'Compra Casada'
		when o.tipooperacao=11 then 'Venda Casada'
		when o.tipooperacao=12 then 'Antecipacao Revenda'
		when o.tipooperacao=13 then 'Antecipacao Recompra'
		when o.tipooperacao=20 then 'Depósito' 
		when o.tipooperacao=21 then 'Retirada' 
	end as TipoOperacao,
	o.quantidade as Quantidade,
	o.puoperacao as PU,
	o.valor as Valor,
	o.rendimento as Rendimento,
	o.valorir as IR,
	o.valoriof as IOF,
	o.valorcorretagem as Corretagem,
	case 
		when o.idcustodia= 2 then 'CETIP' 
		when o.idcustodia= 3 then 'CBLC' 
		else 'CBLC'
	end depositaria,
	s.IdAssessor assessor,
	case 
		when o.IdAgenteContraparte is null then 
			o.Rendimento
		else 0
	end as Receita,
	pp.Descricao as Papel
from 
operacaorendafixa o (NOLOCK)
join titulorendafixa t (NOLOCK)on o.idtitulo= t.idtitulo
left join emissor e (NOLOCK)on t.idemissor= e.idemissor
left join pessoa p (NOLOCK)on o.idcliente= p.idpessoa
left join contacorrente c (NOLOCK)on p.idpessoa= c.idpessoa
left join agencia a (NOLOCK)on c.idagencia= a.idagencia
left join indice i (NOLOCK)on t.idindice= i.idindice
left join clienteRendaFixa b (NOLOCK)on o.idcliente= b.idcliente
left join assessor s (NOLOCK)on b.idassessor= s.idassessor
left join Carteira ca (NOLOCK)on ca.IdCarteira = o.IdCliente
left join OperacaoRendaFixa opEsp (NOLOCK)on o.IdOperacaoEspelho = opEsp.IdOperacao
left join cliente cliEsp (NOLOCK)on cliEsp.IdCliente = opEsp.IdCliente
left join PosicaoRendaFixa posEsp (NOLOCK)on posEsp.IdCliente = opEsp.IdCliente and posEsp.IdTitulo = opEsp.IdTitulo and opEsp.DataOperacao = cliEsp.DataDia
left join PosicaoRendaFixaHistorico posEspH (NOLOCK)on posEspH.IdCliente = opEsp.IdCliente and posEspH.IdTitulo = opEsp.IdTitulo and posEspH.DataHistorico = opEsp.DataOperacao
left join PapelRendaFixa pp (NOLOCK)on pp.IdPapel = t.IdPapel
where 
	c.contadefault='S' and 
	(o.IdAgenteContraParte is not null or ca.ProcAutomatico = 'N')
