declare @data_versao char(10)
set @data_versao = '2015-08-28'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)
go 


-- Criacao da tabela Lamina
CREATE TABLE [Lamina](
	[IdCarteira] [int] NOT NULL,
	[InicioVigencia] [datetime] NOT NULL,
	[EnderecoEletronico] [varchar](200) NULL,
	[DescricaoPublicoAlvo] [varchar](500) NULL,
	[ObjetivoFundo] [varchar](500) NULL,
	[DescricaoPoliticaInvestimento] [varchar](500) NULL,
	[LimiteAplicacaoExterior] [decimal](16, 2) NULL,
	[LimiteCreditoPrivado] [decimal](16, 2) NULL,
	[DerivativosProtecaoCarteira] [char](1) NULL,
	[LimiteAlavancagem] [decimal](16, 2) NULL,
	[PrazoCarencia] [varchar](500) NULL,
	[TaxaEntrada] [varchar](500) NULL,
	[TaxaSaida] [varchar](500) NULL,
	[Risco] [int] NULL,
	[FundoEstruturado] [char](1) NULL,
	[CenariosApuracaoRentabilidade] [varchar](500) NULL,
	[DesempenhoFundo] [varchar](500) NULL,
	[PoliticaDistribuicao] [varchar](500) NULL,
	[Telefone] [varchar](20) NULL,
	[UrlAtendimento] [varchar](100) NULL,
	[Reclamacoes] [varchar](500) NULL,
	[FundosInvestimento] [varchar](500) NULL,
	[FundosInvestimentoCotas] [varchar](500) NULL,
	[EstrategiaPerdas] [char](1) NULL,
	[RegulamentoPerdasPatrimoniais] [varchar](500) NULL,
	[RegulamentoPatrimonioNegativo] [varchar](500) NULL,
 CONSTRAINT [PK_Lamina] PRIMARY KEY CLUSTERED 
(
	[IdCarteira] ASC,
	[InicioVigencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Lamina]  WITH CHECK ADD  CONSTRAINT [FK_Lamina_Carteira] FOREIGN KEY([IdCarteira])
REFERENCES [Carteira] ([IdCarteira])
GO

ALTER TABLE [Lamina] CHECK CONSTRAINT [FK_Lamina_Carteira]
GO



--Menu Lamina
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,7165,'S','S','S','S' from grupousuario where idgrupo <> 0
