declare @data_versao char(10)
set @data_versao = '2013-06-10'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE HistoricoIncorporacaoPosicao(
	IdPosicao int NOT NULL,
	DataIncorporacao datetime NOT NULL,
	IdCarteiraOrigem int NOT NULL,
	IdCarteiraDestino int NOT NULL,		
	DataConversao datetime NOT NULL		
 CONSTRAINT PK_HistoricoIncorporacaoPosicao PRIMARY KEY CLUSTERED 
(
	IdPosicao ASC,
	DataIncorporacao ASC,
	IdCarteiraOrigem ASC,
	IdCarteiraDestino ASC,
	DataConversao ASC
)
)
GO

