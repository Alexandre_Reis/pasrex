declare @data_versao char(10)
set @data_versao = '2015-01-12'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu Pai - ContaCorrente
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1150,'S','S','S','S' from grupousuario where idgrupo <> 0

--Menu TipoConta
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1155,'S','S','S','S' from grupousuario where idgrupo <> 0


--Menu GrupoConta
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1160,'S','S','S','S' from grupousuario where idgrupo <> 0

