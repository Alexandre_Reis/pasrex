﻿-- O código abaixo deve ser inserido em todos os novos scripts, atualizando-se a data_versao:
declare @data_versao char(10)
set @data_versao = '2013-02-07'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
--Ao final sempre atualizar a tabela de VersaoSchema com o script abaixo
insert into VersaoSchema VALUES(@data_versao)

alter table ordemrendafixa alter column quantidade decimal(16,2) null
go
alter table ordemrendafixa alter column puoperacao decimal(25,12) null
go