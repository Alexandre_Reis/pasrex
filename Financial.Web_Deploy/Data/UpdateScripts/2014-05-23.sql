declare @data_versao char(10)
set @data_versao = '2014-05-23'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


CREATE TABLE PessoaVinculo(
	IdPessoa int NOT NULL,
	IdPessoaVinculada int NOT NULL,
	TipoVinculo tinyint NOT NULL,
 CONSTRAINT PK_PessoaVinculo PRIMARY KEY CLUSTERED 
(
	IdPessoa ASC,
	IdPessoaVinculada ASC
)
)
GO


