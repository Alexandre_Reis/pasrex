﻿declare @data_versao char(10)
set @data_versao = '2015-09-10'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION


	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);

if not exists(select valortexto from configuracao where id = 5057)
begin
insert into configuracao (id, descricao, valornumerico, valortexto) 
values(5057, 'Descontar Rendimento Cupom', null, 'S')
end

	ALTER TABLE Carteira
	ADD ProcAutomatico char(1) NOT NULL DEFAULT 'N'

	ALTER TABLE OrdemRendaFixa
	ADD IdAgenteContraParte int NULL foreign key references AgenteMercado(IdAgente),
		IdCarteiraContraParte int NULL foreign key references Carteira(IdCarteira)

	ALTER TABLE OperacaoRendaFixa
	ADD IdAgenteContraParte int NULL foreign key references AgenteMercado(IdAgente),
		IdOperacaoEspelho int NULL,
		DataModificacao datetime not null default GetDAte()



COMMIT TRANSACTION

GO	

