declare @data_versao char(10)
set @data_versao = '2014-09-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE dbo.Pessoa ADD
	NaturezaJuridica smallint NULL,
	TipoRenda char(1) NULL,
	IndicadorFatca char(1) NULL,
	GiinTin varchar(16) NULL,
	JustificativaGiin char(2) NULL,
	CnaeDivisao tinyint NULL,
	CnaeGrupo tinyint NULL,
	CnaeClasse tinyint NULL,
	CnaeSubClasse tinyint NULL,
	Nacionalidade smallint NULL
go
