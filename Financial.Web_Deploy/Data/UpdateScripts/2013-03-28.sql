declare @data_versao char(10)
set @data_versao = '2013-03-28'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


drop table ContabSaldo
go
CREATE TABLE ContabSaldo(
	IdConta int NOT NULL,
	Data datetime NOT NULL,
	SaldoAnterior decimal(16, 2) NOT NULL,
	TotalDebito decimal(16, 2) NOT NULL,
	TotalCredito decimal(16, 2) NOT NULL,
	SaldoFinal decimal(16, 2) NOT NULL,
 CONSTRAINT PK_ContabSaldo PRIMARY KEY CLUSTERED 
(
	IdConta ASC,
	Data ASC
)
)
GO


alter table ContabLancamento drop constraint ContabLancamento_ContabilPlanoContas_FK1
go
alter table ContabLancamento drop constraint ContabLancamento_ContabilPlanoContas_FK2
go
drop table ContabilPlanoContas
go
CREATE TABLE ContabPlanoContas(
	IdConta int IDENTITY(1,1) NOT NULL,
	IdPlano int NOT NULL,
	TipoConta char(1) NOT NULL,
	Descricao varchar(max) NOT NULL,
	IdContaMae int NULL,
	Codigo varchar(50) NULL,
	CodigoReduzida varchar(8) NULL,
 CONSTRAINT PK_ContabPlanoContas PRIMARY KEY CLUSTERED 
(
	IdConta ASC
)
)
go

ALTER TABLE ContabPlanoContas  WITH CHECK ADD  CONSTRAINT ContabPlanoContas_ContabPlanoContas_FK1 FOREIGN KEY(IdContaMae)
REFERENCES ContabPlanoContas (IdConta)
GO

ALTER TABLE ContabPlanoContas  WITH CHECK ADD  CONSTRAINT ContabPlanoContas_ContabPlano_FK1 FOREIGN KEY(IdPlano)
REFERENCES ContabPlano (IdPlano)
GO

ALTER TABLE ContabSaldo  WITH CHECK ADD  CONSTRAINT ContabSaldo_ContabPlanoContas_FK1 FOREIGN KEY(IdConta)
REFERENCES ContabPlanoContas (IdConta)
GO

ALTER TABLE ContabLancamento  WITH CHECK ADD  CONSTRAINT ContabLancamento_ContabPlanoContas_FK1 FOREIGN KEY(IdContaCredito)
REFERENCES ContabPlanoContas (IdConta)
GO

ALTER TABLE ContabLancamento  WITH CHECK ADD  CONSTRAINT ContabLancamento_ContabPlanoContas_FK2 FOREIGN KEY(IdContaDebito)
REFERENCES ContabPlanoContas (IdConta)
GO

