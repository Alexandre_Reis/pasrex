declare @data_versao char(10)
set @data_versao = '2015-05-28'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


UPDATE OperacaoRendaFixa set Status = 1 where Status is NULL or Status = 0

ALTER TABLE [dbo].[LiquidacaoRendaFixa] DROP  CONSTRAINT [DF_LiquidacaoRendaFixa_Status]
GO
ALTER TABLE [dbo].[LiquidacaoRendaFixa] ADD CONSTRAINT [DF_LiquidacaoRendaFixa_Status] DEFAULT ((1)) FOR [Status]

UPDATE LiquidacaoRendaFixa set Status = 1 where Status is NULL or Status = 0