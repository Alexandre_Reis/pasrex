﻿declare @data_versao char(10)
set @data_versao = '2013-07-09'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu de Implantação de Saldos Contabeis
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,13160,'N','S','S','S' from grupousuario where idgrupo <> 0
go


CREATE NONCLUSTERED INDEX IDX_DataHistorico_IdCliente ON PosicaoFundoHistorico 
(
	DataHistorico ASC,
	IdCliente ASC
)
GO


CREATE NONCLUSTERED INDEX IDX_DataHistorico_IdCliente ON PosicaoBolsaHistorico 
(
	DataHistorico ASC,
	IdCliente ASC
)
GO

CREATE NONCLUSTERED INDEX IDX_DataHistorico_IdCliente ON PosicaoRendaFixaHistorico 
(
	DataHistorico ASC,
	IdCliente ASC
)
GO

CREATE NONCLUSTERED INDEX IDX_DataHistorico_IdCliente ON PosicaoFundoAbertura
(
	DataHistorico ASC,
	IdCliente ASC
)
GO


CREATE NONCLUSTERED INDEX IDX_DataHistorico_IdCliente ON PosicaoBolsaAbertura 
(
	DataHistorico ASC,
	IdCliente ASC
)
GO

CREATE NONCLUSTERED INDEX IDX_DataHistorico_IdCliente ON PosicaoRendaFixaAbertura
(
	DataHistorico ASC,
	IdCliente ASC
)
GO


CREATE NONCLUSTERED INDEX [IDX_IdCliente_DataConversao] ON OperacaoFundo
(
	[IdCliente] ASC,
	[DataConversao] ASC
)
GO

