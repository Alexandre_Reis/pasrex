declare @data_versao char(10)
set @data_versao = '2014-02-17'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE [PessoaTelefone](
	[IdTelefone] [int] IDENTITY(1,1) NOT NULL,
	[IdPessoa] [int] NOT NULL,
	[DDI] [varchar](5) NOT NULL,
	[DDD] [varchar](5) NOT NULL,
	[Numero] [varchar](60) NOT NULL,
	[Ramal] [varchar](5) NULL,
	[TipoTelefone] [tinyint] NOT NULL,
 CONSTRAINT [PK_PessoaTelefone_1] PRIMARY KEY CLUSTERED 
(
	[IdTelefone] ASC
)
) ON [PRIMARY]

GO


ALTER TABLE [PessoaTelefone]  WITH CHECK ADD  CONSTRAINT [FK_PessoaTelefone_Pessoa] FOREIGN KEY([IdPessoa])
REFERENCES [Pessoa] ([IdPessoa])
ON DELETE CASCADE
GO

ALTER TABLE [PessoaTelefone] CHECK CONSTRAINT [FK_PessoaTelefone_Pessoa]
GO



CREATE TABLE [PessoaEmail](
	[IdEmail] [int] IDENTITY(1,1) NOT NULL,
	[IdPessoa] [int] NOT NULL,
	[Email] [varchar](200) NOT NULL,
 CONSTRAINT [PK_PessoaEmail_1] PRIMARY KEY CLUSTERED 
(
	[IdEmail] ASC
)
) ON [PRIMARY]

GO

ALTER TABLE [PessoaEmail]  WITH CHECK ADD  CONSTRAINT [FK_PessoaEmail_Pessoa] FOREIGN KEY([IdPessoa])
REFERENCES [Pessoa] ([IdPessoa])
ON DELETE CASCADE
GO

ALTER TABLE [PessoaEmail] CHECK CONSTRAINT [FK_PessoaEmail_Pessoa]
GO

