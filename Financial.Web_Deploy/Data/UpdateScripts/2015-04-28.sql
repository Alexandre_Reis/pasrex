declare @data_versao char(10)
set @data_versao = '2015-04-28'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)
go 

ALTER TABLE [dbo].[OperacaoRendaFixa] DROP  CONSTRAINT [DF_OperacaoRendaFixa_Status]
GO
ALTER TABLE [dbo].[OperacaoRendaFixa] ADD CONSTRAINT [DF_OperacaoRendaFixa_Status] DEFAULT ((1)) FOR [Status]
