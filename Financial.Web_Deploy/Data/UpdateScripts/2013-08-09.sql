declare @data_versao char(10)
set @data_versao = '2013-08-09'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


alter table cliente add ContabilDaytrade tinyint null
go
update cliente set ContabilDaytrade = 1 where CalculaContabil = 'S' --Net
go

--Menu do Report de Cliente - Contabil
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,13135,'S','S','S','S' from grupousuario where idgrupo <> 0
go

