declare @data_versao char(10)
set @data_versao = '2013-09-17'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

insert into [dbo].configuracao values (45, 'Aliquota Unica Grossup', null, 'S')
go

alter table operacaobolsa add NumeroNota int null
go
alter table ordembolsa add NumeroNota int null
go
alter table operacaorendafixa add NumeroNota int null
go
