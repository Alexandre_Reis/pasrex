declare @data_versao char(10)
set @data_versao = '2013-08-16'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


alter table estrategia add IdIndiceBenchmark smallint null
go

ALTER TABLE Estrategia  WITH CHECK ADD  CONSTRAINT FK_Estrategia_Indice FOREIGN KEY(IdIndiceBenchmark)
REFERENCES Indice (IdIndice)
GO






