﻿--- Query montada para a INDUSVAL
---
---
---


UPDATE LiquidacaoHistorico 
SET descricao = 'Ajuste de Distribuição de Div'
FROM LiquidacaoHistorico
 WHERE idcliente = 17449 AND descricao = 'Ajuste de Distribuição de Dividendos';


if exists(select * from sys.all_objects where name = 'v_Proventos' and type = 'v')
	drop view v_Proventos 
go 

Create view v_Proventos as

	-- Retorna todas as taxas da CPR que não sejam:
	--     Taxa de Adm ou Custodia (
	--     Dividendos ou Juros sob Capital Proprio  (Bvdivid)
	-- Estes já estão descriminados em outras consultas.

  select DataHistorico as DatMov , idcliente as Codprod, ValorCorrigidoJuros - ValorBase as Prov, Case PontaEmprestimo when 1 then 'Ativo' else 'Passivo' end as Tipo
	from PosicaoEmprestimoBolsaHistorico

   union all

    select a.DataHistorico as DatMov, a.IdCLiente as Codprod, a.valor as Prov, iif (a.valor > 0 ,'Ativo','Passivo') as Tipo
		from LiquidacaoHistorico a
		where a.datalancamento <= a.DataHistorico and 
		      a.dataVencimento > a.DataHistorico and 
			   not ((a.descricao like '%TAXA%' and a.origem in (801,802,810,811,820,821)) or a.descricao like '%DIVIDENDO%' or (a.descricao like '%JURO%' and a.descricao not like '%BTC%'))
go

  


if exists(select * from sys.all_objects where name = 'v_InterfacePosicaoPatrimonial' and type = 'v')
	drop view v_InterfacePosicaoPatrimonial 
go 
create VIEW v_InterfacePosicaoPatrimonial as 

Select a.Data as DatMov, 
       a.idcarteira as Codprod,
	   b.nome as Nomeprod,
	   a.plfechamento as Patrliquido,
	   a.cotabruta as Valcotaneg, 
       coalesce((select sum(RVvista.ValorMercado) 
	      from PosicaoBolsaHistorico RVvista,
			   AtivoBolsa
		  where RVvista.datahistorico = a.data and
				RVvista.idCliente     = a.idCarteira and 
				RVvista.cdAtivoBolsa  = ativoBolsa.cdAtivoBolsa and 
				ativoBolsa.tipomercado = 'VIS'),0) as BvVista,
       coalesce((select sum(RVvista.ValorMercado) 
	      from PosicaoBolsaHistorico RVvista,
			   AtivoBolsa
		  where RVvista.datahistorico = a.data and
				RVvista.idCliente     = a.idCarteira and 
				RVvista.cdAtivoBolsa  = ativoBolsa.cdAtivoBolsa and 
				ativoBolsa.tipomercado in ('OPC','OPV')),0) as Bvopcoes,
       coalesce((select sum(RVTermo.ValorMercado) 
	      from PosicaoTermoBolsaHistorico RVTermo
		  where RVTermo.datahistorico = a.data and
				RVTermo.idCliente     = a.idCarteira ),0) as Bvtermo,
		coalesce((select sum(valor) 
		from Liquidacaohistorico  
		where Liquidacaohistorico.datalancamento <= Liquidacaohistorico.datahistorico and 
		      Liquidacaohistorico.dataVencimento > Liquidacaohistorico.datahistorico and 
			  Liquidacaohistorico.datahistorico = a.data and 
			  Liquidacaohistorico.idcliente = a.idCarteira and 
			  ((Liquidacaohistorico.descricao like '%TAXA%' and Liquidacaohistorico.origem in (801,802,810,811,820,821)) or Liquidacaohistorico.descricao like '%DIVIDENDO%' or (Liquidacaohistorico.descricao like '%JURO%' and Liquidacaohistorico.descricao not like '%BTC%')) 
			   ) ,0) as Bvdivid,
		coalesce((select sum(RVTermo.ValorMercado) 
	      from PosicaoTermoBolsaHistorico RVTermo
		  where RVTermo.datahistorico = a.data and
				RVTermo.idCliente     = a.idCarteira and 
				RVTermo.quantidade > 0),0) as Bvcttermocpa, 
		coalesce((select sum(RVTermo.ValorMercado) 
	      from PosicaoTermoBolsaHistorico RVTermo
		  where RVTermo.datahistorico = a.data and
				RVTermo.idCliente     = a.idCarteira and 
				RVTermo.quantidade < 0),0) as Bvcttermovda, 
		coalesce((select sum(PosicaoBMFHistorico.ajusteDiario) 
		from PosicaoBMFHistorico 
		where PosicaoBMFHistorico.datahistorico = a.Data and 
		      PosicaoBMFHistorico.idcliente = a.idCarteira ) ,0) as Bmajuste,
		coalesce((select sum(RF.ValorMercado) 
	      from PosicaoRendaFixaHistorico RF			   
		  where RF.datahistorico = a.data and
				RF.idCliente     = a.idCarteira),0) as Rftotal,
		coalesce((select sum(SW.Saldo) 
	      from PosicaoSWAPHistorico SW			   
		  where SW.datahistorico = a.data and
				SW.idCliente     = a.idCarteira),0) as Rfdifswapp,
		coalesce((select sum(FDO.valorliquido) 
	      from PosicaoFUNDOHistorico FDO, 
			   Carteira 			   
		  where FDO.datahistorico = a.data and
				FDO.idcliente     = a.idCarteira and 
				Carteira.idcarteira = a.idcarteira and 
				Carteira.TipoCarteira = 2),0) as Fdorvar,
		 coalesce((select sum(FDO.valorliquido) 
	      from PosicaoFUNDOHistorico FDO, 
			   Carteira 			   
		  where FDO.datahistorico = a.data and
				FDO.idcliente     = a.idCarteira and 
				Carteira.idcarteira = a.idcarteira and 
				Carteira.TipoCarteira = 1),0) as Fdorfix,

		 coalesce((select sum(prov) 
		    from v_proventos 
			  where datmov = a.data and 
			        codprod = a.idCarteira and 
					tipo = 'Ativo')  ,0) as Provativa,  

		 coalesce((select sum(prov) 
		    from v_proventos 
			  where datmov = a.data and 
			        codprod = a.idCarteira and 
					tipo = 'Passivo')  ,0) as ProvPassiva,  

	      coalesce((select -sum(TxAdm.ValorAcumulado) 
	      from CalculoAdministracaoHistorico TxAdm
		  where TxAdm.datahistorico = a.data and
				TxAdm.idCarteira     = a.idCarteira ),0) as Txadacum,
	      coalesce((select -sum(Pfee.ValorAcumulado) 
	      from CalculoPerformanceHistorico Pfee
		  where Pfee.datahistorico = a.data and
				Pfee.idCarteira     = a.idCarteira ),0) as Txpfeeprev,
		  coalesce((select sum(cxa.SaldoFechamento) 
	      from SaldoCaixa cxa
		  where cxa.data  = a.data and
				cxa.idcliente     = a.idCarteira ),0) as Cctotal
	    
 from historicoCota a
 left join Carteira as b on b.idcarteira = a.idcarteira ;

 GO

 -- exemplo de utilização

  select * 
  from v_InterfacePosicaoPatrimonial 
  where Codprod  = 13352 and DatMov = '2015-11-09'

    select * 
  from v_InterfacePosicaoPatrimonial 
  where Codprod  = 17449 and DatMov = '2015-11-09'

  