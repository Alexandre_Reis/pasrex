﻿---- SCRIPT para INTEGRAÇÃO com o sistema VALEMOBI
----       INDUSVAL
----


if exists(select * from sys.all_objects where name = 'v_PosCotProdSint' and type = 'v')
	drop view v_PosCotProdSint 
go 

create view v_PosCotProdSint 
as
	select 
	    A.Datahistorico as dt_mov,
		A.IdCotista as cod_cot,
		A.IdCarteira as cod_prod,
		C.Nome as nome_prod,
		B.idAgente as cod_ges,
		B.Nome as nome_ges,
		SUM(A.Quantidade) as tt_qtde
	from 
		PosicaoCotistaHistorico A
	    left join Carteira as C on A.idCarteira = C.idCarteira
		left join AgenteMercado as B on C.idAgenteGestor = b.idAgente
	where A.Quantidade > 0
	group by 
	    A.Datahistorico ,
		A.IdCotista,
		A.IdCarteira,
		C.Nome,
		B.idAgente,
		B.Nome
GO

if exists(select * from sys.all_objects where name = 'v_posgeralcottc' and type = 'v')
	drop view v_posgeralcottc 
go 

  CREATE VIEW  v_posgeralcottc
AS
select 
	CotaAplicacao as vr_cota_aplc, 
	ValorAplicacao as vr_aplic, 
    ValorBruto as vr_bruto, 
	valorIr + coalesce((select Op.ValorIR 
	 from OperacaoCotista Op 
	 where Op.IdPosicaoResgatada = PosicaoCotistaHistorico.IdPosicao and 
	       Op.TipoOperacao = 20 and 
		   Op.DataOperacao = PosicaoCotistaHistorico.DataUltimaCobrancaIR and 
		   Op.DataOperacao = DataHistorico),0) as ir_rf,
	coalesce((select Op.ValorIR 
	 from OperacaoCotista Op 
	 where Op.IdPosicaoResgatada = PosicaoCotistaHistorico.IdPosicao and 
	       Op.TipoOperacao = 20 and 
		   Op.DataOperacao = PosicaoCotistaHistorico.DataUltimaCobrancaIR and 
		   Op.DataOperacao = DataHistorico),0)  as ir_rf_cc, 
	valoriof as iof_30, 
	valorliquido as vr_liq_final, 
	Quantidade as qt_final, 
	IdCotista as cod_cot, 
	IdCarteira as cod_prod, 
	IdPosicao as cert, 
	DataConversao as dt_aplic, 
	CotaDia as val_cota, 
	DataHistorico as dt_movto 
from 
	PosicaoCotistaHistorico
where Quantidade > 0 
GO

if exists(select * from sys.all_objects where name = 'vod_PosGeralCotTc' and type = 'v')
	drop view vod_PosGeralCotTc 
go 
create view vod_PosGeralCotTc
as
SELECT a.IdPosicao cert
	  ,a.Datahistorico dt_movto
      ,a.IdCotista cod_cot
      ,a.IdCarteira cod_prod
	  ,C.Nome as nome_prod
	  ,D.idAgente as cod_ges
	  ,D.Nome as nome_ges
      ,a.ValorAplicacao vr_aplic
      ,a.DataConversao dt_aplic 
      ,a.CotaAplicacao vr_cota_aplc
      ,a.CotaDia val_cota
      ,a.ValorBruto vr_bruto
      ,a.ValorLiquido vr_liq_final
      ,a.Quantidade qt_final
      ,a.ValorIR + coalesce((select Op.ValorIR 
	     from OperacaoCotista Op 
	     where Op.IdPosicaoResgatada = A.IdPosicao and 
		       Op.TipoOperacao = 20 and 
			   Op.DataOperacao = a.DataUltimaCobrancaIR and 
			   Op.DataOperacao = a.Datahistorico),0) as  ir_rf
	  ,coalesce((select Op.ValorIR 
	     from OperacaoCotista Op 
	     where Op.IdPosicaoResgatada = A.IdPosicao and 
		       Op.TipoOperacao = 20 and 
			   Op.DataOperacao = a.DataUltimaCobrancaIR and 
			   Op.DataOperacao = a.Datahistorico),0) as ir_rf_cc
      ,a.ValorIOF  iof_30
	  ,b.Quantidade qtde
  FROM PosicaoCotistaHistorico A 
	left join PosicaoCotistaAbertura B on a.datahistorico = b.DataHistorico and a.IdPosicao = b.IdPosicao 
    left join Carteira as C on A.idCarteira = C.idCarteira
	left join AgenteMercado as D on C.idAgenteGestor = D.idAgente
  where a.Quantidade > 0
GO



-- Exemplos de utilização e testes -----------------------

--select *
-- from OperacaoCotista Op 
--where Op.IdPosicaoResgatada = 59053 and 
--      Op.TipoOperacao = 20 and 
--	  Op.DataOperacao = '2015-5-29'

--select * from v_posgeralcottc 
--where cod_cot = 2000006155 and 
--     cod_prod = 21341 and dt_movto in ('2015-6-1', '2015-5-29', '2015-5-28') and cert = 41132
-- order by dt_movto desc, cert desc

 select * from v_posgeralcottc 
where cod_prod = 21341 and dt_movto in ('2015-10-30') 
 order by dt_movto desc, cert desc

-- select top 1000 * 
--  from PosicaoCotistaHistorico
--  where DataHistorico in ('2015-5-29') and IdCotista = 2000006155 and IdCarteira = 5071

--   select top 1000 * 
--  from PosicaoCotistaHistorico
--  where DataHistorico in ('2015-5-29') and idPOsicao = 59053

--idPOsicao = 41132
--3731.335339960000
--select 3760.798659500000 - 3048.92 / 103.481890300000





