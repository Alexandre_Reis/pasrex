﻿

if exists(select * from sys.all_objects where name = 'v_DadosCliente' and type = 'v')
	drop view v_DadosCliente 
go 

create VIEW  v_DadosCliente  as 

  select Pessoa.cpfcnpj as CpfCnpj,
		 Cotista.idCotista as CodigoCliente,
		 Cotista.Nome as NomeCliente,
		 case Pessoa.Tipo 
		  when 1 then 'PF' 
		  when 2 then 'PJ' 
		 else 'NA'
		 end as TipoPessoa,
		 PessoaEndereco.idEndereco as IdEndereco,
		 PessoaEndereco.Endereco as Logradouro, 
		 PessoaEndereco.Numero as Numero,
		 PessoaEndereco.Complemento as Complemento, 
		 PessoaEndereco.Bairro as Bairro,
		 PessoaEndereco.Cidade as Cidade,
		 PessoaEndereco.UF as UFEstado,
		 PessoaEndereco.CEP as CEP		 
   from Cotista
   left join Pessoa on Cotista.IdPessoa = Pessoa.IdPessoa 
   left join PessoaEndereco on Pessoa.idPessoa = PessoaEndereco.IdPessoa 
go


if exists(select * from sys.all_objects where name = 'v_DadosClubeFundo' and type = 'v')
	drop view v_DadosClubeFundo 
go 

create VIEW  v_DadosClubeFundo  as 

  select Carteira.idCarteira as CodigoProduto,
		 Carteira.Nome as NomeProduto,
		 Case Carteira.TipoTributacao 
		  when 1 then 'Sim'
		  when 2 then 'Sim'
		 else 'Não'
		 end as ComeCotas,
		 AgenteMercado.nome as Empresa
   from Carteira
   left join AgenteMercado on AgenteMercado.idAgente = Carteira.idAgenteGestor
go

if exists(select * from sys.all_objects where name = 'v_MovimentoDirfConsulta' and type = 'v')
	drop view v_MovimentoDirfConsulta 
go 

create VIEW  v_MovimentoDirfConsulta  as 

  select idCotista as IdentificaoCliente, 
         idCarteira as IdentificaoClubeFundo,
		 DataOperacao as DataMovimento,
		 Quantidade as QuantidadeQuotas,
		 ValorIR as ValorIRRF,
		 RendimentoResgate as ValorRendimentosTributaveis,
		 'Fundo de Investimento' as TipoDeProduto, 
		 Month(DataOperacao) as MesVigencia
   from OperacaoCotista
go

if exists(select * from sys.all_objects where name = 'v_MovimentoInforme' and type = 'v')
	drop view v_MovimentoInforme 
go 

create VIEW  v_MovimentoInforme  as 

  select idCotista as IdentificaoCliente, 
         idCarteira as IdentificaoClubeFundo,
		 DataOperacao as DataMovimento,
		 Quantidade as QuantidadeQuotas,
		 ValorIR as ValorIRRF,
		 ValorBruto as ValorBruto,
		 ValorLiquido as ValorLiquido,
		 0 as ValorBase
   from OperacaoCotista
go


