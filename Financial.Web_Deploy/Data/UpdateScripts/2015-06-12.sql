﻿declare @data_versao char(10)
set @data_versao = '2015-06-12'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) 
BEGIN
	return
END
ELSE
BEGIN
	insert into VersaoSchema VALUES(@data_versao)
	BEGIN TRANSACTION DeParaTransaction
	delete depara where IdTipoDePara = -24	
	delete TipoDePara where IdTipoDePara = -24 
	set identity_insert TipoDePara on				
	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-24,1,'TipoPapelAtivo','Tipo papel Ativo',	null)		
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-24,	1,	'Normal,',	'Normal')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-24,	2,	'Bonus de Subscrição',	'BonusSubscricao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-24,	3,	'Recibo de Subscrição',	'ReciboSubscricao')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-24,	4,	'ETF',	'ETF')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-24,	5,	'Certificado de Deposito em Ações',	'CertificadoDepositoAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-24,	6,	'Recibo de Deposito em Ações',	'ReciboDepositoAcoes')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-24,	7,	'BDR Nível I',	'BDRNivelI')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-24,	8,	'BDR Nível II',	'BDRNivelII')
	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-24,	9,	'BDR Nível III',	'BDRNivelIII')
	COMMIT
END


