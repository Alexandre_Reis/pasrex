declare @data_versao char(10)
set @data_versao = '2015-10-07'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

ALTER TABLE GerOperacaoBolsa ALTER COLUMN CdAtivoBolsaOpcao VARCHAR(25) NULL;
GO