declare @data_versao char(10)
set @data_versao = '2014-09-02'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE CotacaoMercadoFiduciario(
	IdTitulo int NOT NULL,
	Data datetime NOT NULL,
	Cotacao decimal(16, 8) NOT NULL,
 CONSTRAINT PK_CotacaoMercadoFiduciario PRIMARY KEY CLUSTERED 
(
	IdTitulo ASC,
	Data ASC
)
)
GO