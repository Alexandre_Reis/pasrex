﻿declare @data_versao char(10)
set @data_versao = '2013-10-21'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Acesso à nova entrada Posições a Liquidar RF
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,22270,'N','S','S','S' from grupousuario where idgrupo <> 0
go
--Acesso à nova entrada Mapa de Liquidação RF
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,22280,'N','S','S','S' from grupousuario where idgrupo <> 0
go

