declare @data_versao char(10)
set @data_versao = '2015-03-10'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaocotista add IdOrdem int null