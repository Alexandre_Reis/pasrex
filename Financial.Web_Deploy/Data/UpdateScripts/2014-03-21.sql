declare @data_versao char(10)
set @data_versao = '2014-03-21'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table tabelataxaadministracao alter column Taxa decimal (16,12) null
go
