declare @data_versao char(10)
set @data_versao = '2014-03-31'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu Tabela Processamento
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,40190,'S','S','S','S' from grupousuario where idgrupo <> 0


alter table posicaoswap add ValorIR decimal (16,2) null
go
alter table posicaoswaphistorico add ValorIR decimal (16,2) null
go
alter table posicaoswapabertura add ValorIR decimal (16,2) null
go
update posicaoswap set ValorIR  = 0
go
update posicaoswaphistorico set ValorIR  = 0
go
update posicaoswapabertura set ValorIR  = 0
go
ALTER TABLE posicaoswap ADD  CONSTRAINT DF_PosicaoSwap_ValorIR  DEFAULT ((0)) FOR ValorIR
GO
ALTER TABLE posicaoswaphistorico ADD  CONSTRAINT DF_PosicaoSwapHistorico_ValorIR  DEFAULT ((0)) FOR ValorIR
GO
ALTER TABLE posicaoswapabertura ADD  CONSTRAINT DF_PosicaoSwapAbertura_ValorIR  DEFAULT ((0)) FOR ValorIR
GO
alter table posicaoswap alter column ValorIR decimal (16,2) not null
go
alter table posicaoswaphistorico alter column ValorIR decimal (16,2) not null
go
alter table posicaoswapabertura alter column ValorIR decimal (16,2) not null
go

IF EXISTS (SELECT * 
  FROM sys.foreign_keys 
   WHERE object_id = OBJECT_ID(N'dbo.PosicaoRendaFixa_BloqueioRendaFixa_FK1')
   AND parent_object_id = OBJECT_ID(N'dbo.BloqueioRendaFixa')
)
  ALTER TABLE BloqueioRendaFixa DROP CONSTRAINT PosicaoRendaFixa_BloqueioRendaFixa_FK1
  

