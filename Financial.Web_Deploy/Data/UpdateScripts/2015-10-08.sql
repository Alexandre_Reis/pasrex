﻿declare @data_versao char(10)
set @data_versao = '2015-10-08'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION

	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);

	if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'ComeCotasEntreRegatesConversao')
	BEGIN
		ALTER TABLE dbo.Carteira ADD ComeCotasEntreRegatesConversao char(1) NOT NULL DEFAULT 'S'
	END
	GO
		
COMMIT TRANSACTION

GO	



