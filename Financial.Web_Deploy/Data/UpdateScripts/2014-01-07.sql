declare @data_versao char(10)
set @data_versao = '2014-01-07'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table tabelataxaperformance add DataFimApropriacao datetime null
go
alter table tabelataxaperformance add DataPagamento datetime null
go
alter table tabelataxaperformance add DataUltimoCortePfee datetime null
go



