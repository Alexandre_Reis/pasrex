declare @data_versao char(10)
set @data_versao = '2014-05-28'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaorendafixa add OperacaoTermo char(1) null
go
update operacaorendafixa set OperacaoTermo = 'N'
go
alter table operacaorendafixa alter column OperacaoTermo char(1) not null
go

ALTER TABLE operacaorendafixa ADD CONSTRAINT [DF_OperacaoRendaFixa_OperacaoTermo]  DEFAULT (('N')) FOR OperacaoTermo
GO

alter table posicaorendafixa add OperacaoTermo char(1) null
go
update posicaorendafixa set OperacaoTermo = 'N'
go
alter table posicaorendafixa alter column OperacaoTermo char(1) not null
go

ALTER TABLE posicaorendafixa ADD CONSTRAINT [DF_PosicaoRendaFixa_OperacaoTermo]  DEFAULT (('N')) FOR OperacaoTermo
GO

alter table posicaorendafixahistorico add OperacaoTermo char(1) null
go
update posicaorendafixahistorico set OperacaoTermo = 'N'
go
alter table posicaorendafixahistorico alter column OperacaoTermo char(1) not null
go

ALTER TABLE posicaorendafixahistorico ADD CONSTRAINT [DF_PosicaoRendaFixaHistorico_OperacaoTermo]  DEFAULT (('N')) FOR OperacaoTermo
GO

alter table posicaorendafixaabertura add OperacaoTermo char(1) null
go
update posicaorendafixaabertura set OperacaoTermo = 'N'
go
alter table posicaorendafixaabertura alter column OperacaoTermo char(1) not null
go

ALTER TABLE posicaorendafixaabertura ADD CONSTRAINT [DF_PosicaoRendaFixaAbertura_OperacaoTermo]  DEFAULT (('N')) FOR OperacaoTermo
GO