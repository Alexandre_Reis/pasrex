declare @data_versao char(10)
set @data_versao = '2014-01-10'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu de Detalhe de Carteira de Bolsa
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) SELECT idgrupo,2345,'S','S','S','S' from grupousuario where idgrupo <> 0

