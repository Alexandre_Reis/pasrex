declare @data_versao char(10)
set @data_versao = '2015-04-30'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

if not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'AberturaIndexada')
BEGIN
	ALTER TABLE Cliente ADD AberturaIndexada smallint NOT NULL DEFAULT '0'
END
GO

if Not exists(select 1 from syscolumns where id = object_id('Cliente') and name = 'IdIndiceAbertura')
BEGIN
	ALTER TABLE dbo.Cliente ADD IdIndiceAbertura smallint NULL;
	ALTER TABLE Cliente  WITH CHECK ADD  CONSTRAINT Cliente_Indice_FK1 FOREIGN KEY(IdIndiceAbertura)
	REFERENCES Indice (IdIndice) ON DELETE SET NULL;
END
GO	