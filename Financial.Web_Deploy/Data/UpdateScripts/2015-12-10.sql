﻿declare @data_versao char(10)
set @data_versao = '2015-12-10'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

begin transaction

	-- não permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)

	--DDL
	if (not exists (select * from sys.all_columns where name = 'CodigoSti' and OBJECT_NAME(object_id) = 'Carteira'))
	begin
		ALTER TABLE Carteira ADD  CodigoSti varchar(35) null
	end
	
	
	IF EXISTS(select * FROM sys.views where name = 'ViewStatusInvestidores')
	begin
		DROP VIEW ViewStatusInvestidores;
	end	
	GO

	CREATE VIEW ViewStatusInvestidores AS
	  select Cotista.IdCotista, 
			ltrim(rtrim(CONVERT(char(20),Cotista.IdCotista))) + ' - ' + Cotista.Apelido as Cotista,
			pessoaCotista.CPFCNPJ as DocumentoCotista,
			'' as Assessor,
			perfilcotista.Perfil as PerfilRiscoInvestidor, 
			CASE WHEN pessoaCotista.Tipo = 1 
				THEN 'Física'
			ELSE
				'Juridica'
			END AS TipoPessoa,
			OperacaoCotista.DataOperacao as DataUltimaMovimentacao, 
			Carteira.Apelido as FundoQueInveste, 		
			perfilfundo.Perfil as PerfilRiscoFundo, 
			'' AS PerfilRiscoPortifolioCliente, --Calculado em tempo de extração do relatório
			case 
				when perfilcotista.nivel < perfilfundo.nivel then 'Sim' else 'Não' 
			end as CotistaDesenquadrado, 
			case 
				when (select count(1) 
					  from SuitabilityEventos,  
					  SuitabilityLogMensagens 
					  where SuitabilityEventos.IdMensagem = SuitabilityLogMensagens.IdMensagem 
						and SuitabilityEventos.IdSuitabilityEventos in (18, 19) 
						and SuitabilityLogMensagens.Resposta = 'OK') > 0 then 'Sim' else 'Não' 
			end as AssinouIncompatibilidadeDePerfilRisco, 
			case 
				when (	select count(1) 
						from SuitabilityEventos,  
							 SuitabilityLogMensagens 
						where SuitabilityEventos.IdMensagem = SuitabilityLogMensagens.IdMensagem 
							and SuitabilityEventos.IdSuitabilityEventos in (16,17) 
							and SuitabilityLogMensagens.Resposta = 'OK') > 0 then 'Sim' else 'Não' 
			end as AssinouTermoAdesao, 
			PessoaSuitability.UltimaAlteracao AS DataUltimaAlteracao, 
			case suitabilityParametrosWorkflow.GrandezaPeriodo			 
				when 1 then DATEADD(day, suitabilityParametrosWorkflow.ValorPeriodo,PessoaSuitability.UltimaAlteracao) 
				when 2 then DATEADD(day, (suitabilityParametrosWorkflow.ValorPeriodo * 30),PessoaSuitability.UltimaAlteracao) 
				when 3 then DATEADD(day, (suitabilityParametrosWorkflow.ValorPeriodo * 360),PessoaSuitability.UltimaAlteracao) 
			end as DataProximaRenovacao,
			Carteira.IdCarteira,
			pessoaCotista.Tipo
	 from OperacaoCotista,  
	 PessoaSuitability,  
	 SuitabilityPerfilInvestidor as perfilcotista,  
	 SuitabilityPerfilInvestidor as perfilfundo, 
	 Carteira, 
	 suitabilityParametrosWorkflow,
	 Cotista,
	 Pessoa pessoaCotista
	 where OperacaoCotista.IdCotista = PessoaSuitability.IdPessoa 
		and PessoaSuitability.Perfil = perfilcotista.IdPerfilInvestidor 
		and OperacaoCotista.IdCarteira = Carteira.IdCarteira 
		and Carteira.PerfilRisco = perfilfundo.IdPerfilInvestidor 
		and Cotista.IdCotista = OperacaoCotista.IdCotista 
		and cotista.IdCotista = pessoaCotista.IdPessoa
		and operacaoCotista.TipoOperacao in (1,10,11,12)
	GO
	--

	--DML
	delete from PermissaoMenu where idMEnu = 27000
	insert into [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
		  ,27000
		  ,[PermissaoLeitura]
		  ,[PermissaoAlteracao]
		  ,[PermissaoExclusao]
		  ,[PermissaoInclusao]
	  FROM [dbo].[PermissaoMenu]
	  where IdMenu = 15110;

	delete from PermissaoMenu where idMEnu = 27001
	insert into [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
		  ,27001
		  ,[PermissaoLeitura]
		  ,[PermissaoAlteracao]
		  ,[PermissaoExclusao]
		  ,[PermissaoInclusao]
	  FROM [dbo].[PermissaoMenu]
	  where IdMenu = 15110;
	
	delete from PermissaoMenu where idMEnu = 27002
	insert into [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
		  ,27002
		  ,[PermissaoLeitura]
		  ,[PermissaoAlteracao]
		  ,[PermissaoExclusao]
		  ,[PermissaoInclusao]
	  FROM [dbo].[PermissaoMenu]
	  where IdMenu = 15110;
	
	delete from PermissaoMenu where idMEnu = 27003
	insert into [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
		  ,27003
		  ,[PermissaoLeitura]
		  ,[PermissaoAlteracao]
		  ,[PermissaoExclusao]
		  ,[PermissaoInclusao]
	  FROM [dbo].[PermissaoMenu]
	  where IdMenu = 15110;
	
	if (select count(1) from CadastroComplementarCampos where TipoCadastro = -9 and NomeCampo = 'ASSESSOR') = 0	
	BEGIN
		INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais) 
		VALUES ( -9, 'ASSESSOR', 'ASSESSOR DO COTISTA', null, -2, 'N', 0,0);
	END
	--

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off


