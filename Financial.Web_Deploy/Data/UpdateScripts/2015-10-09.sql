﻿declare @data_versao char(10)
set @data_versao = '2015-10-09'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION
	
	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);
	
	
	ALTER TABLE OperacaoRendaFixa
	add StatusBatimento int NULL


COMMIT TRANSACTION

GO