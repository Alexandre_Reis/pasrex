declare @data_versao char(10)
set @data_versao = '2013-11-05'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)



alter table operacaorendafixa add Emolumento decimal (16, 2) null
go
update operacaorendafixa set Emolumento = 0
go
alter table operacaorendafixa alter column Emolumento decimal (16, 2) not null
go
ALTER TABLE operacaorendafixa ADD  CONSTRAINT DF_OperacaoRendaFixa_Emolumento  DEFAULT ((0)) FOR Emolumento
go
