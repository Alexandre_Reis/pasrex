﻿declare @data_versao char(10)
set @data_versao = '2015-12-17'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

IF NOT EXISTS (SELECT * FROM configuracao WHERE ID = 6101)
BEGIN
	insert into configuracao (ID, Descricao, ValorTexto) Values(6101, 'Calcula COF usando pl de D-0', 'N');
END
GO

delete from PermissaoMenu where idMenu = 7841
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7841
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 10260;
GO

delete from PermissaoMenu where idMenu = 14561
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14561
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 10260;
GO

delete from PermissaoMenu where idMEnu = 15270
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,15270
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 10260;
GO

--Merge 1.0 -> 1.1 - 2016/01/12

--Menu Lamina
delete from PermissaoMenu where idMEnu = 7165;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,7165,'S','S','S','S' from grupousuario where idgrupo <> 0;

if not exists(select valortexto from configuracao where id = 5057)
begin
	insert into configuracao (id, descricao, valornumerico, valortexto) 
	values(5057, 'Descontar Rendimento Cupom', null, 'S');
end

if not exists(select 1 from [indice] where [IdIndice] = 110)
begin
	INSERT INTO [dbo].[indice] ([IdIndice],[Descricao],[Tipo],[TipoDivulgacao])VALUES(110,'IMA_GERAL_EX_C',2,1)
end

--Menu Integracoes / Informacoes Complementares
delete from PermissaoMenu where idMEnu = 14260;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,14260,'S','S','S','S' from grupousuario where idgrupo <> 0;

if(select count(1) from SuitabilityValidacao) = 0
begin
	insert into SuitabilityValidacao values ('Pessoa Física');
	insert into SuitabilityValidacao values ('Pessoa Jurídica');
	insert into SuitabilityValidacao values ('Fundos');
	insert into SuitabilityValidacao values ('Clubes de Investimento');
end

delete from PermissaoMenu where idMEnu = 1280
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1280
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1230;

--Acesso a nova tela de tipo de dispensa
delete from PermissaoMenu where idMEnu = 1230
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1230
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1205;

if(select count(1) from SuitabilityTipoDispensa) = 0
begin
	insert into SuitabilityTipoDispensa (Descricao) values ('Pessoas habilitadas a atuar como integrantes do sistema de distribuição');
end

--Acesso a nova tela de tipo de dispensa
delete from PermissaoMenu where idMEnu = 1235
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1235
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1205;

delete from PermissaoMenu where idMEnu = 4880
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4880
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 4870;

delete from PermissaoMenu where idMEnu = 1240
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1240
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1215;

delete from PermissaoMenu where idMEnu = 1250
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1250
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1240;

if (select count(1) from SuitabilityEventos) = 0
begin
	insert into SuitabilityEventos values (1, 'Apuração de novo perfil de Investidor (Portal)', null);
	insert into SuitabilityEventos values (2, 'Declaração sobre as informações prestadas (Portal)', null);
	insert into SuitabilityEventos values (3, 'Aceite do perfil apurado', null);
	insert into SuitabilityEventos values (4, 'Aceite do perfil apurado (Portal)', null);
	insert into SuitabilityEventos values (5, 'Aceite do tipo de dispensa', null);
	insert into SuitabilityEventos values (6, 'Aceite do tipo de dispensa (Portal)', null);
	insert into SuitabilityEventos values (7, 'Recusa do preenchimento do formulário', null);
	insert into SuitabilityEventos values (8, 'Recusa do preenchimento do formulário (Portal)', null);
	insert into SuitabilityEventos values (9, 'Questionário não preenchido (Portal)', null);
	insert into SuitabilityEventos values (10, 'Atualizar perfil (Portal)', null);
	insert into SuitabilityEventos values (11, 'Inexistência de Perfil (Portal)', null);
	insert into SuitabilityEventos values (12, 'Perfil desatualizado (Portal)', null);
	insert into SuitabilityEventos values (13, 'Desenquadramento - Alteração de perfil', null);
	insert into SuitabilityEventos values (14, 'Desenquadramento - Alteração de perfil (Portal)', null);
	insert into SuitabilityEventos values (15, 'Termo de Ciência (Portal)', null);
	insert into SuitabilityEventos values (16, 'Termo de Ciência de Risco e Adesão ao Fundo', null);
	insert into SuitabilityEventos values (17, 'Termo de Ciência de Risco e Adesão ao Fundo (Portal)', null);
	insert into SuitabilityEventos values (18, 'Termo de Inadequação', null);
	insert into SuitabilityEventos values (19, 'Termo de Inadequação (Portal)', null);
end

if (select count(1) from SuitabilityEventosHistorico) = 0
begin
	insert into SuitabilityEventosHistorico values (getdate(),1, 'Apuração de novo perfil de Investidor (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),2, 'Declaração sobre as informações prestadas (Portal)', null,null,  'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),3, 'Aceite do perfil apurado', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),4, 'Aceite do perfil apurado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),5, 'Aceite do tipo de dispensa', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),6, 'Aceite do tipo de dispensa (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),7, 'Recusa do preenchimento do formulário”', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),8, 'Recusa do preenchimento do formulário (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),9, 'Questionário não preenchido (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),10, 'Atualizar perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),11, 'Inexistência de Perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),12, 'Perfil desatualizado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),13, 'Desenquadramento - Alteração de perfil', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),14, 'Desenquadramento - Alteração de perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),15, 'Termo de Ciência (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),16, 'Termo de Ciência de Risco e Adesão ao Fundo', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),17, 'Termo de Ciência de Risco e Adesão ao Fundo (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),18, 'Termo de Inadequação', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),19, 'Termo de Inadequação (Portal)', null, null, 'Insert');
end

delete from PermissaoMenu where idMEnu = 1260
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1260
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1250;
GO

delete from PermissaoMenu where idMEnu = 1270
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1270
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1250;
GO


delete from PermissaoMenu where idMEnu = 1290
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1290
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1250;
GO

--insere dados de configuração para que esses possam ser salvos posteriormente na tela
if not exists(select 1 from Configuracao where Id = 1050)
begin
	insert into Configuracao(Id,Descricao,ValorTexto) values(1050,'NomeInstituicao','');
end

if not exists(select 1 from Configuracao where Id = 1051)
begin
	insert into Configuracao(Id,Descricao,ValorTexto) values(1051,'CodInstituicao','');
end

if not exists(select 1 from Configuracao where Id = 1052)
begin
	insert into Configuracao(Id,Descricao,ValorTexto) values(1052,'ResponsavelInstituicao','');
end
	
if not exists(select 1 from Configuracao where Id = 1053)
begin
	insert into Configuracao(Id,Descricao,ValorTexto) values(1053,'TelefoneInstituicao','');
end
	
if not exists(select 1 from Configuracao where Id = 1054)
begin
	insert into Configuracao(Id,Descricao,ValorTexto) values(1054,'EmailInstituicao','');
end

if (select count(1) from SuitabilityEventosHistorico) = 0
begin
	insert into SuitabilityEventosHistorico values (getdate(),1, 'Apuração de novo perfil de Investidor (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),2, 'Declaração sobre as informações prestadas (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),3, 'Aceite do perfil apurado', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),4, 'Aceite do perfil apurado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),5, 'Aceite do tipo de dispensa', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),6, 'Aceite do tipo de dispensa (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),7, 'Recusa do preenchimento do formulário', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),8, 'Recusa do preenchimento do formulário (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),9, 'Questionário não preenchido (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),10, 'Atualizar perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),11, 'Inexistência de Perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),12, 'Perfil desatualizado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),13, 'Desenquadramento - Alteração de perfil', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),14, 'Desenquadramento - Alteração de perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),15, 'Termo de Ciência (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),16, 'Termo de Ciência de Risco e Adesão ao Fundo', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),17, 'Termo de Ciência de Risco e Adesão ao Fundo (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),18, 'Termo de Inadequação', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),19, 'Termo de Inadequação (Portal)', null, null, 'Insert');
end

--Implantação Suitability no cadastro de pessoas.
--TODO alterar processo para versão 1.1.N (GPS) devido relação 1xn de pessoa para cliente
insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select distinct IdPessoa, 'N', 'N', 1, getdate()
from Cliente
where IdTipo = 1
and not exists (select 1 from PessoaSuitability where cliente.IdPessoa = PessoaSuitability.IdPessoa);

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select distinct IdPessoa, 'N', 'N', 2, getdate()
from Cliente
where IdTipo = 10
and not exists (select 1 from PessoaSuitability where cliente.IdPessoa = PessoaSuitability.IdPessoa);

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao, Dispensa)
select distinct IdPessoa, 'S', 'N', 3, getdate(), 1
from Cliente
where Cliente.IdTipo in (300,310,320,500,510)
and not exists (select 1 from PessoaSuitability where cliente.IdPessoa = PessoaSuitability.IdPessoa);

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao, Dispensa)
select distinct IdPessoa,'S', 'N', 4, getdate(), 1
from Cliente
where Cliente.IdTipo in (200)
and not exists (select 1 from PessoaSuitability where cliente.IdPessoa = PessoaSuitability.IdPessoa);

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select distinct IdPessoa, 'N', 'N', 1, getdate()
from Cotista
where TipoTributacao = 1
and not exists (select 1 from PessoaSuitability where cotista.IdPessoa = PessoaSuitability.IdPessoa);

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select distinct IdPessoa, 'N', 'N', 2, getdate()
from Cotista
where TipoTributacao = 2
and not exists (select 1 from PessoaSuitability where cotista.IdPessoa = PessoaSuitability.IdPessoa);

if not exists (select 1 from configuracao where ID = 5022)
begin
	INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(5022,'ContaIntegracao',null,'202');
end

if(select count(1) from SuitabilityParametrosWorkflow) = 0
begin
	insert into SuitabilityParametrosWorkflow (AtivaWorkFlow) Values('N')
end

if(select count(1) from SuitabilityParametrosWorkflowHistorico) = 0
begin
	insert into SuitabilityParametrosWorkflowHistorico (DataHistorico, AtivaWorkFlow, Tipo) Values (getdate(), 'N', 'Insert')
end

if (select count(1) from SuitabilityEventosHistorico) = 0
begin
	insert into SuitabilityEventosHistorico values (getdate(),1, 'Apuração de novo perfil de Investidor (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),2, 'Declaração sobre as informações prestadas (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),3, 'Aceite do perfil apurado', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),4, 'Aceite do perfil apurado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),5, 'Aceite do tipo de dispensa', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),6, 'Aceite do tipo de dispensa (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),7, 'Recusa do preenchimento do formulário', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),8, 'Recusa do preenchimento do formulário (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),9, 'Questionário não preenchido (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),10, 'Atualizar perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),11, 'Inexistência de Perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),12, 'Perfil desatualizado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),13, 'Desenquadramento - Alteração de perfil', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),14, 'Desenquadramento - Alteração de perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),15, 'Termo de Ciência (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),16, 'Termo de Ciência de Risco e Adesão ao Fundo', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),17, 'Termo de Ciência de Risco e Adesão ao Fundo (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),18, 'Termo de Inadequação', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),19, 'Termo de Inadequação (Portal)', null, null, 'Insert');
end

--Implantação Suitability no cadastro de pessoas.
--TODO alterar processo para versão 1.1.N (GPS) devido relação 1xn de pessoa para cliente
insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select IdPessoa, 'N', 'N', 1, getdate()
from Cliente
where IdTipo = 1
and not exists (select 1 from PessoaSuitability where cliente.IdPessoa = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select IdPessoa, 'N', 'N', 2, getdate()
from Cliente
where IdTipo = 10
and not exists (select 1 from PessoaSuitability where cliente.IdPessoa = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao, Dispensa)
select IdPessoa, 'S', 'N', 3, getdate(), 1
from Cliente
where Cliente.IdTipo in (300,310,320,500,510)
and not exists (select 1 from PessoaSuitability where cliente.IdPessoa = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao, Dispensa)
select IdPessoa, 'S', 'N', 4, getdate(), 1
from Cliente
where Cliente.IdTipo in (200)
and not exists (select 1 from PessoaSuitability where cliente.IdPessoa = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select IdPessoa, 'N', 'N', 1, getdate()
from Cotista
where TipoTributacao = 1
and not exists (select 1 from PessoaSuitability where cotista.IdPessoa = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select IdPessoa, 'N', 'N', 2, getdate()
from Cotista
where TipoTributacao = 2
and not exists (select 1 from PessoaSuitability where cotista.IdPessoa = PessoaSuitability.IdPessoa);
go

IF NOT EXISTS(SELECT ID FROM CONFIGURACAO WHERE ID = 5060 )
BEGIN
	INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(5060,'ExibePizzaTopN',99,'')
END  

update Carteira set Corretora = 'N' where Corretora is null;

delete from PermissaoMenu where idMEnu = 7716
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,7716
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 7160;

DELETE FROM PermissaoMenu WHERE IdMenu = 530
	INSERT INTO [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
	  ,530
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
	FROM [dbo].[PermissaoMenu]
 WHERE IdMenu = 100

DELETE FROM PermissaoMenu WHERE IdMenu = 10280
	INSERT INTO [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
	  ,10280
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
	FROM [dbo].[PermissaoMenu]
 WHERE IdMenu = 10270;
 
 
delete from PermissaoMenu where idMEnu = 27000
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,27000
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 15110;

delete from PermissaoMenu where idMEnu = 27001
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,27001
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 15110;

delete from PermissaoMenu where idMEnu = 27002
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,27002
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 15110;

delete from PermissaoMenu where idMEnu = 27003
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,27003
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 15110;

if (select count(1) from CadastroComplementarCampos where TipoCadastro = -9 and NomeCampo = 'ASSESSOR') = 0	
BEGIN
	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais) 
	VALUES ( -9, 'ASSESSOR', 'ASSESSOR DO COTISTA', null, -2, 'N', 0,0);
END

if not exists (select 1 from PermissaoMenu where IdGrupo = 1 and IdMenu = 15030) 
begin
	insert into PermissaoMenu values (1, 15030, 'S','S','S','S')
end

if not exists (select 1 from PermissaoMenu where IdGrupo = 1 and IdMenu = 14135) 
begin
	insert into PermissaoMenu values (1, 14135, 'S','S','S','S')
end

UPDATE Configuracao SET ValorTexto = 'S' where id = 5001;

IF NOT EXISTS(SELECT ID FROM CONFIGURACAO WHERE ID = 1060 )
BEGIN
	INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(1060,'IntegraGalgoPlCota',0,'')
END  

UPDATE pessoa set CPFCNPJ = SUBSTRING(CPFCNPJ, len(CPFCNPJ) - 10, 11) where tipo =1 and len(CPFCNPJ) > 11
UPDATE pessoa set CPFCNPJ = SUBSTRING(CPFCNPJ, len(CPFCNPJ) - 13, 14) where tipo =2 and len(CPFCNPJ) > 14	

--Apaga o conteúdo do campo de CarteiraFundo
DELETE FROM CadastroComplementar
where  exists (select 'X' 
                from CadastroComplementarCampos A
				 where a.IdCamposComplementares = CadastroComplementar.IdCamposComplementares 
				       and a.NomeCampo in ('NomeFundo') 
					   and a.TipoCadastro = -8 --CarteiraFundo
			   )

-- Apaga a definição do Campo de CarteiraFundo
delete CadastroComplementarCampos 
 where TipoCadastro = -8 AND --CarteiraFundo
       NomeCampo in ('NomeFundo')

-- Cria um novo campo de CarteiraFundo
INSERT INTO [CadastroComplementarCampos] 
   ([TipoCadastro],[NomeCampo] ,[DescricaoCampo] ,[ValorDefault] ,[TipoCampo] ,[CampoObrigatorio],[Tamanho]  ,[CasasDecimais]) 
 VALUES  
   (-8 , 'NomeFundo','Nome Fundo' ,NULL ,-2 ,'N' ,50  ,0) 

-- INSERE Conteúdo default nos campos criados
INSERT INTO [CadastroComplementar]
           ([IdCamposComplementares]
           ,[IdMercadoTipoPessoa]
           ,[DescricaoMercadoTipoPessoa]
           ,[ValorCampo])
	select (select IdCamposComplementares  from [CadastroComplementarCampos] where NomeCampo = 'NomeFundo' and TipoCadastro = -8),
           Carteira.idCarteira,
           Convert(Varchar(30),Carteira.Apelido), 
           Convert(Varchar(30),Carteira.Nome)  -- Preenche o nome reduzido com os primeiros 10 caracteres do Apelido
	 from Carteira
	 -- where Carteira.idCarteira in (743259, 746295, 736745, 736744)  --- Indicar aqui a lista de carteiras cujo Status do Fundo seja FC
	 
DECLARE @idCadastro INT;
DECLARE @maxChar INT;
DECLARE @NomeCampoChar varchar(255);
DECLARE @ValorCampoChar varchar(500)
DECLARE @DescricaoCampoChar varchar(255);

SET @NomeCampoChar = 'Objetivo';
SET @DescricaoCampoChar = 'Objetivo do Fundo';
SET @ValorCampoChar = 'Proporcionar rentabilidade aos seus cotistas através de oportunidades oferecidas pelos mercados domésticos de taxas de juros pós-fixadas, prefixadas e de índices de preços.'
SET @maxChar = 175;
if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar) = 0
begin

	begin transaction;
	
 -- nгo permite que a transaction seja commitada em caso de erros
    set xact_abort on;

	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
	VALUES(-8, @NomeCampoChar, @DescricaoCampoChar, null, -2, 'N', @maxChar, 0)

	SET @idCadastro = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar)

	--Inserir carteira
	INSERT INTO CadastroComplementar (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
	SELECT 	@idCadastro,
			IdCarteira,
			LTRIM(RTRIM(SUBSTRING(cart.Apelido, 1, 50))) ,		   		   
			LTRIM(RTRIM(SUBSTRING(@ValorCampoChar, 1, @maxChar))) 
    FROM Carteira cart;

	commit transaction;
END

SET @NomeCampoChar = 'Publico Alvo';
SET @DescricaoCampoChar = 'Define o público alvo do fundo';
SET @ValorCampoChar = 'Destina-se a receber aplicações de recursos provenientes de investidores pessoas físicas e jurídicas em geral, com perfil de risco moderado.'
SET @maxChar = 175;
if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar) = 0
begin

	begin transaction;
	
 -- nгo permite que a transaction seja commitada em caso de erros
    set xact_abort on;

	if exists(select 1 from syscolumns where id = object_id('CadastroComplementar') and name = 'ValorCampo')
	begin
		ALTER TABLE CadastroComplementar ALTER COLUMN ValorCampo VARCHAR(500) NOT NULL
	END

	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
	VALUES(-8, @NomeCampoChar, @DescricaoCampoChar, null, -2, 'N', @maxChar, 0)

	SET @idCadastro = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar)

	--Inserir carteira
	INSERT INTO CadastroComplementar (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
	SELECT 	@idCadastro,
			IdCarteira,
			LTRIM(RTRIM(SUBSTRING(cart.Apelido, 1, 50))) ,		   		   
			LTRIM(RTRIM(SUBSTRING(@ValorCampoChar, 1, @maxChar))) 
    FROM Carteira cart;

	commit transaction;
END

SET @NomeCampoChar = 'Politica de Investimento';
SET @DescricaoCampoChar = 'Define a Política de Investimento do Fundo';
SET @ValorCampoChar = 'O fundo pretende atingir seu objetivo investindo no mínimo 80% de seu patrimônio em títulos públicos e/ou privados de renda fixa pré ou pós fixadas de médio e longo prazo, sendo permitido o uso de derivativos а taxa de juros doméstica e/ou índices de preços, com objetivo exclusivo de proteger a carteira do fundo.';
SET @maxChar = 500;
if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar) = 0
begin

	begin transaction;
	
 -- nгo permite que a transaction seja commitada em caso de erros
    set xact_abort on;

	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
	VALUES(-8, @NomeCampoChar, @DescricaoCampoChar, null, -2, 'N', @maxChar, 0)

	SET @idCadastro = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar)

	--Inserir carteira
	INSERT INTO CadastroComplementar (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
	SELECT 	@idCadastro,
			IdCarteira,
			LTRIM(RTRIM(SUBSTRING(cart.Apelido, 1, 50))) ,		   		   
			LTRIM(RTRIM(SUBSTRING(@ValorCampoChar, 1, @maxChar))) 
    FROM Carteira cart;

	commit transaction;
END

---- Exemplos de consulta--------------------------

select ValorCampo
   from CadastroComplementar 
   where IdCamposComplementares = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'Objetivo') 
         and CadastroComplementar.IdMercadoTipoPessoa = '5114'

select ValorCampo from CadastroComplementar where
   IdCamposComplementares = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'Publico Alvo')
            and CadastroComplementar.IdMercadoTipoPessoa = '5114'


select ValorCampo from CadastroComplementar where
   IdCamposComplementares = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'Politica de Investimento')
            and CadastroComplementar.IdMercadoTipoPessoa = '5114'
            
if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'NomeFundoLamina') > 0
begin
	UPDATE CadastroComplementarCampos SET DescricaoCampo = 'Nome do fundo que será apresentado na lâmina' where TipoCadastro = -8 and NomeCampo = 'NomeFundoLamina'
END	

update CadastroComplementar
set ValorCampo = 'Proporcionar rentabilidade aos seus cotistas através de oportunidades oferecidas pelos mercados domésticos de taxas de juros pós-fixadas, prefixadas e de índices de preços.'
where IdCamposComplementares = (select IdCamposComplementares from CadastroComplementarCampos where nomecampo = 'objetivo');

update CadastroComplementar
set ValorCampo = 'Destina-se a receber aplicações de recursos provenientes de investidores pessoas físicas e jurídicas em geral, com perfil de risco moderado.'
where IdCamposComplementares = (select IdCamposComplementares from CadastroComplementarCampos where nomecampo = 'Publico Alvo');

update CadastroComplementar
set ValorCampo = 'O fundo pretende atingir seu objetivo investindo no mínimo 80% de seu patrimônio em títulos públicos e/ou privados de renda fixa pré ou pós fixadas de médio e longo prazo, sendo permitido o uso de derivativos à taxa de juros doméstica e/ou índices de preços, com objetivo exclusivo de proteger a carteira do fundo.'
where IdCamposComplementares = (select IdCamposComplementares from CadastroComplementarCampos where nomecampo = 'Politica de Investimento');

update CadastroComplementarCampos set DescricaoCampo = 'Define o público alvo do fundo' where TipoCadastro = -8 and nomecampo = 'Publico Alvo';
update CadastroComplementarCampos set DescricaoCampo = 'Define a Política de Investimento do Fundo' where TipoCadastro = -8 and nomecampo = 'Politica de Investimento';	

if not exists (select 1 from PermissaoMenu where IdGrupo = 1 and IdMenu = 1295) 
begin
	insert into PermissaoMenu (IdGrupo, IdMenu, PermissaoLeitura, PermissaoAlteracao, PermissaoExclusao, PermissaoInclusao) values (1, 1295, 'S','S','S','S')
end

if(select count(1) from EntidadePerfilFundo) = 0
begin
	DECLARE @IdEntidade INT

	INSERT INTO EntidadePerfilFundo VALUES ('CVM', 'Comissão de Valores Mobiliários')
	SET @IdEntidade = IDENT_CURRENT('EntidadePerfilFundo')

	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Renda Fixa','Fundo de Renda Fixa','2015-10-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Ações','Fundo de Ações','2015-10-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Multimercado','Fundo Multimercado','2015-10-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Cambial','Fundo Cambial','2015-10-01')

	INSERT INTO EntidadePerfilFundo VALUES ('ANBIMA', 'Associação Brasileira das Entidades dos Mercados Financeiro e de Capitais')
	SET @IdEntidade = IDENT_CURRENT('EntidadePerfilFundo')

	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Simples','Fundo de Renda Fixa - Simples','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'RF - Índices','Fundo de Renda Fixa - Índices','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Soberano','Fundo de Renda Fixa - Soberano','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Grau de Investimento','Fundo de Renda Fixa - Grau de Investimento','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Crédito Livre','Fundo de Renda Fixa -  Crédito Livre','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'RF - Investimento no Exterior','Fundo de Renda Fixa - Investimento no Exterior','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Dívida Externa','Fundo de Renda Fixa - Dívida Externa','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Balanceados ','Fundo Multimercados - Balanceados','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Flexível','Fundo Multimercados - Flexível','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Macro','Fundo Multimercados - Macro','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Trading','Fundo Multimercados - Trading','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Long and Short - Direcional','Fundo Multimercados - Long and Short - Direcional','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Long and Short - Neutro','Fundo Multimercados - Long and Short - Neutro','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Juros e Moedas','Fundo Multimercados - Juros e Moedas','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Multimercado Livre','Fundo Multimercados - Livre','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Capital Protegido','Fundo Multimercados - Capital Protegido','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Estratégia Específica','Fundo Multimercados - Estratégia Específica','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Multimercado - Investimento no Exterior','Fundo Multimercados - Investimentos','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Ações - Índices','Fundo de Ações - Índices','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Valor Crescimento','Fundo de Ações - Valor Crescimento','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Setoriais','Fundo de Ações - Setoriais','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Dividendos','Fundo de Ações - Dividendos','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Small Cap','Fundo de Ações - Small Cap','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Sustentabilidade / Governança','Fundo de Ações - Sustentabilidade / Governança','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Índice Ativo','Fundo de Ações - Índice Ativo','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Ações Livre','Fundo de Ações - Livre','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Ações - Investimento no Exterior','Fundo de Ações - Investimento no Exterior','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Fundos Fechados de Ações','Fundo de Ações - Fechado','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Fundos de Ações FMP/FGTS','Fundo de Ações - FMP/FGTS','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Fundos de Mono Ação','Fundo de Ações - Mono','2015-07-01')
	INSERT INTO EntidadeClassificacaoPerfilFundo VALUES (@IdEntidade, 'Cambial','Cambial','2015-07-01')
end