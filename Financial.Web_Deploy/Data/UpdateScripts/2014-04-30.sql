declare @data_versao char(10)
set @data_versao = '2014-04-30'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

drop table PermissaoOperacaoFundo
go

CREATE TABLE PermissaoOperacaoFundo(
	IdCotista int NOT NULL,
	IdFundo int NOT NULL,
	Email varchar(200) NULL,
 CONSTRAINT PermissaoOperacaoFundo_PK PRIMARY KEY CLUSTERED 
(
	IdCotista ASC,
	IdFundo ASC
)
)
GO

ALTER TABLE PermissaoOperacaoFundo ADD  CONSTRAINT Cliente_PermissaoOperacaoFundo_FK1 FOREIGN KEY(IdFundo)
REFERENCES Cliente (IdCliente)
ON DELETE CASCADE
GO


ALTER TABLE Agencia ADD	DigitoAgencia char(1) NULL
ALTER TABLE ContaCorrente ADD DigitoConta char(2) NULL