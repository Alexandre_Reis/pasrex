declare @data_versao char(10)
set @data_versao = '2013-11-27'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table titulorendafixa add DebentureConversivel char(1) null
go
update titulorendafixa set DebentureConversivel = 'N'
go
alter table titulorendafixa alter column DebentureConversivel char(1) not null
go
ALTER TABLE titulorendafixa ADD  CONSTRAINT DF_TituloRendaFixa_DebentureConversivel  DEFAULT (('N')) FOR DebentureConversivel
go

alter table carteira add CodigoIsin varchar(12) null
go
