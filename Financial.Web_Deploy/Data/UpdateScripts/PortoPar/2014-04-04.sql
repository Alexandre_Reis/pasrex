CREATE TABLE [TabelaOrdemTesouraria](
	[IdOperacao] [int] NOT NULL,
	[IdOperacaoTesouraria] [int] NOT NULL,
 CONSTRAINT [PK_TabelaOrdemTesouraria] PRIMARY KEY CLUSTERED 
(
	[IdOperacao] ASC,
	[IdOperacaoTesouraria] ASC
)
)

CREATE TABLE [OrdemTesouraria](
	[IdOperacao] [int] NOT NULL,
	[IdCotista] [int] NOT NULL,
	[DataOperacao] [datetime] NOT NULL,
	[TipoOperacao] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[Valor] [decimal](16, 2) NOT NULL,
 CONSTRAINT [PK_OrdemTesouraria] PRIMARY KEY CLUSTERED 
(
	[IdOperacao] ASC
)
)
