CREATE TABLE dbo.PermissaoPortalCliente
(
IdGrupo int NOT NULL,
PodeBoletar char(1) NOT NULL,
PodeDistribuir char(1) NOT NULL,
PodeAprovar char(1) NOT NULL,
AcessoSeguranca char(1) NOT NULL
)  ON [PRIMARY]

GO

ALTER TABLE dbo.PermissaoPortalCliente ADD CONSTRAINT PK_Table_1 PRIMARY KEY CLUSTERED 
( 
IdGrupo
) 




