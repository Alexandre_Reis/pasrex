/****** Object:  StoredProcedure [dbo].[EXTRACT_BSC_CLIENTE]    Script Date: 10/02/2015 18:35:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EXTRACT_BSC_CLIENTE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EXTRACT_BSC_CLIENTE]
GO
/****** Object:  StoredProcedure [dbo].[EXTRACT_BSC_CLIENTE]    Script Date: 10/02/2015 17:29:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[EXTRACT_BSC_CLIENTE]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

SELECT 
	CONVERT(CHAR(15),p.IdPessoa) as BSC_CD_CLIENTE,--idcliente
	CONVERT(CHAR(1),CASE WHEN p.IdPessoa = 1 THEN 'F' ELSE 'J' END) as BSC_IC_FJ_PESSOA,--idcliente
	CONVERT(decimal(11),CASE WHEN  p.Tipo = 1 THEN CONVERT(FLOAT,LEFT(RTRIM(LTRIM(replace(replace(replace(p.Cpfcnpj,'-',''),'.',''),'/',''))),11))  ELSE NULL END ) as BSC_NO_CPF,
	CONVERT(DECIMAL(14),CASE WHEN p.Tipo = 2 THEN CONVERT(FLOAT,LEFT(RTRIM(LTRIM(replace(replace(replace(p.Cpfcnpj,'-',''),'.',''),'/',''))),11)) ELSE NULL END ) as BSC_NO_CGC,
	CONVERT(VARCHAR(50),p.Apelido) COLLATE SQL_Latin1_General_Cp1251_CS_AS as BSC_NM_CLIENTE
	,@DataRef as DataRef
FROM
	Pessoa p 
INNER JOIN Cotista c on c.IdCotista = p.IdPessoa

END
GO