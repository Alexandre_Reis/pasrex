﻿--faz com que o script só seja commitado caso todas as batches seja executadas com exit
SET XACT_ABORT ON; 
GO

--garante isolação completa em relação a outros scripts
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION;
GO 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

USE [FIN_PORTOPAR]
GO

IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'EXTRACT_BS_RV_CADCOR') 
    AND xtype IN (N'P', N'PC')
)
DROP PROCEDURE EXTRACT_BS_RV_CADCOR
GO

CREATE PROCEDURE[dbo].[EXTRACT_BS_RV_CADCOR]
      @DataRef date --Data em que a procedure vai rodar
AS
BEGIN

SELECT 
	convert(char(8)	   , ag.IdAgente) as BS_CODCOR,
	convert(float	   , null) as BS_PERCDEVCOR,
	convert(varchar(30), null) as BS_FAXADM,
	convert(char(15)   , null) as BS_CONTATOADM,
	convert(varchar(30), null) as BS_FAXOPER,
	convert(char(15)   , null) as BS_CONTATOOPER,
	convert(varchar(20), ag.Nome) as BS_NOME,
	convert(char(15)   , null) as BS_CODCETIP,
	convert(char(15)   , null) as BS_BOVESPA,
	convert(varchar(50), null) as BS_ENDERECO,
	convert(float	   , null) as BS_PERCDEVFUT,
	convert(char(1)	   , null) as BS_TIPO_CORRETORA,
	convert(char(1)	   , null) as BS_IC_LIGADA,
	convert(char(1)	   , null) as BS_IC_AGENTE_DE_COMPENSACAO,
	convert(varchar(30), null) as BS_NO_TEL_ADM,
	convert(varchar(30), null) as BS_NO_TEL_OPER,
	convert(char(4)	   , null) as BS_CD_AG,
	convert(char(10)   , null) as BS_CD_CC,
	convert(char(4)	   , null) as BS_CD_BCO,
	convert(char(12)   , null) as BS_CD_SWIFT,
	convert(char(20)   , null) as BS_DS_TIPO_LIQ_FIN_RV,
	convert(char(20)   , null) as BS_DS_TIPO_LIQ_FIN_FU,
	convert(char(20)   , null) as BS_DS_TIPO_LIQ_FIN_SW,
	convert(char(15)   , null) as BS_CD_BMF,
	convert(char(18)   , null) as BS_NO_CNPJ,
	convert(char(6)	   , null) as BS_CD_CONTROLE_FINANC,
	convert(char(15)   , null) as BS_CONTATOOPERBVSP,
	convert(char(15)   , null) as BS_CONTATOADMBVSP,
	convert(varchar(30), null) as BS_NO_TEL_ADMBVSP,
	convert(varchar(30), null) as BS_NO_TEL_OPERBVSP,
	convert(varchar(30), null) as BS_FAXOPERBVSP,
	convert(varchar(30), null) as BS_FAXADMBVSP,
	convert(char(1)	   , null) as BS_IC_LOG_LIBERADO,
	convert(int		   , null) as BS_ID_LOG_LIBERADO,
	convert(char(8)	   , null) as BS_BASIF_CODINST,
	convert(char(1)	   , null) as BS_SG_TIPO_SERVICO,
	convert(char(9)	   , null) as BS_CD_ISPB,
	convert(char(15)   , null) as BS_CD_ADMIN,
	convert(char(20)   , null) as BS_NM_MEM_COMPENS,
	convert(char(9)	   , null) as BS_CD_ISPB_BMF,
	convert(char(4)	   , null) as BS_CD_BCO_BMF,
	convert(char(10)   , null) as BS_CD_CC_BMF,
	convert(char(4)	   , null) as BS_CD_AG_BMF,
	convert(char(8)	   , null) as BS_CD_IF,
	convert(char(8)	   , null) as BS_CD_IF_BMF,
	convert(char(1)	   , null) as BS_IC_PNA,
	convert(char(1)	   , null) as BS_CD_DAC,
	convert(char(1)	   , null) as BS_CD_DAC_BMF,
	convert(char(8)	   , null) as BS_CD_GRUPO_ECONOMICO,
	convert(char(1)	   , null) as BS_IC_CONT_DIAS_LIQ_FIN_CORTG,
	convert(int		   , null) as BS_NO_DIAS_LIQ_FIN_CORTG,
	@DataRef as Dataref
from  AgenteMercado ag
--left join PerfilCorretagemBolsa pfC on  ag.IdAgente = pfc.IdAgente and pfc.DataReferencia < (select min(datadia) from cliente where datadia > @DataRef)


END
GO


USE [ATLAS_BS_MAPPING]
GO

IF EXISTS(select * from sys.all_objects WHERE name = 'BS_RV_CADCOR')
	BEGIN
		drop table BS_RV_CADCOR 
	END

CREATE TABLE BS_RV_CADCOR(                                       
		BS_CODCOR                              char(8)		NOT NULL,
		BS_PERCDEVCOR                          float		NULL,
		BS_FAXADM                              varchar(30)	NULL,
		BS_CONTATOADM                          char(15)		NULL,
		BS_FAXOPER                             varchar(30)	NULL,
		BS_CONTATOOPER                         char(15)		NULL,
		BS_NOME                                varchar(20)	NULL,
		BS_CODCETIP                            char(15)		NULL,
		BS_BOVESPA                             char(15)		NULL,
		BS_ENDERECO                            varchar(50)	NULL,
		BS_PERCDEVFUT                          float		NULL,
		BS_TIPO_CORRETORA                      char(1)		NULL,
		BS_IC_LIGADA                           char(1)		NULL,
		BS_IC_AGENTE_DE_COMPENSACAO            char(1)		NULL,
		BS_NO_TEL_ADM                          varchar(30)	NULL,
		BS_NO_TEL_OPER                         varchar(30)	NULL,
		BS_CD_AG                               char(4)		NULL,
		BS_CD_CC                               char(10)		NULL,
		BS_CD_BCO                              char(4)		NULL,
		BS_CD_SWIFT                            char(12)		NULL,
		BS_DS_TIPO_LIQ_FIN_RV                  char(20)		NULL,
		BS_DS_TIPO_LIQ_FIN_FU                  char(20)		NULL,
		BS_DS_TIPO_LIQ_FIN_SW                  char(20)		NULL,
		BS_CD_BMF                              char(15)		NULL,
		BS_NO_CNPJ                             char(18)		NULL,
		BS_CD_CONTROLE_FINANC                  char(6)		NULL,
		BS_CONTATOOPERBVSP                     char(15)		NULL,
		BS_CONTATOADMBVSP                      char(15)		NULL,
		BS_NO_TEL_ADMBVSP                      varchar(30)	NULL,
		BS_NO_TEL_OPERBVSP                     varchar(30)	NULL,
		BS_FAXOPERBVSP                         varchar(30)	NULL,
		BS_FAXADMBVSP                          varchar(30)	NULL,
		BS_IC_LOG_LIBERADO                     char(1)		NULL,
		BS_ID_LOG_LIBERADO                     int			NULL,
		BS_BASIF_CODINST                       char(8)		NULL,
		BS_SG_TIPO_SERVICO                     char(1)		NULL,
		BS_CD_ISPB                             char(9)		NULL,
		BS_CD_ADMIN                            char(15)		NULL,
		BS_NM_MEM_COMPENS                      char(20)		NULL,
		BS_CD_ISPB_BMF                         char(9)		NULL,
		BS_CD_BCO_BMF                          char(4)		NULL,
		BS_CD_CC_BMF                           char(10)		NULL,
		BS_CD_AG_BMF                           char(4)		NULL,
		BS_CD_IF                               char(8)		NULL,
		BS_CD_IF_BMF                           char(8)		NULL,
		BS_IC_PNA                              char(1)		NULL,
		BS_CD_DAC                              char(1)		NULL,
		BS_CD_DAC_BMF                          char(1)		NULL,
		BS_CD_GRUPO_ECONOMICO                  char(8)		NULL,
		BS_IC_CONT_DIAS_LIQ_FIN_CORTG          char(1)		NULL,
		BS_NO_DIAS_LIQ_FIN_CORTG			   int			NULL,	
		DataRef								   DateTime		NOT NULL,
 CONSTRAINT [XPKBS_RV_CADCOR] PRIMARY KEY CLUSTERED 
	(
		[BS_CODCOR] ASC,
		[DataRef]
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
	) ON [PRIMARY]
GO

IF EXISTS(select * from sys.all_objects WHERE name = 'BS_PARAM')
	BEGIN
		drop table BS_PARAM 
	END

	CREATE TABLE [dbo].[BS_PARAM](
		[BS_DATAHOJE] [datetime] NOT NULL,
		[DataRef] [DateTime] NOT NULL,
	 CONSTRAINT [XPKBS_PARAM] PRIMARY KEY NONCLUSTERED 
	(
		[BS_DATAHOJE] ASC,
		[DataRef] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [PRIMARY]
	) ON [PRIMARY]

GO


IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'PAS_LOAD_BS_RV_CADCOR') AND xtype IN (N'P', N'PC') ) 
BEGIN 
	DROP PROCEDURE PAS_LOAD_BS_RV_CADCOR;
END  
GO
CREATE PROCEDURE[dbo].[PAS_LOAD_BS_RV_CADCOR] @DataRef date --Data em que a procedure vai rodar
	, @BS_RV_CADCOR_message varchar(max) OUTPUT
	, @IdExec int = NULL
AS 
BEGIN  
	SET NOCOUNT ON
	BEGIN TRANSACTION;	
	DECLARE @DataInicioProcesso datetime = GETDATE()
	BEGIN /* BS_RV_CADCOR */ 
		BEGIN TRY
			DECLARE @BS_RV_CADCOR_Count int
			SET @BS_RV_CADCOR_message = 'BS_RV_CADCOR'
			SET @BS_RV_CADCOR_Count = (SELECT count(*) from BS_RV_CADCOR where DataRef = @DataRef)
			SET @BS_RV_CADCOR_message += CHAR(10) + CHAR(9) + CONVERT(VARCHAR(100),@BS_RV_CADCOR_Count) + ' registro(s) em conflito encontrado(s).'
			IF(@BS_RV_CADCOR_Count > 0)
			BEGIN
			DELETE BS_RV_CADCOR WHERE DataRef = @DataRef
			SET @BS_RV_CADCOR_message += CHAR(10) + CHAR(9) + CONVERT(VARCHAR(100),@@ROWCOUNT) + ' registro(s) em conflito apagado(s).'
		END
		BEGIN
			INSERT INTO BS_RV_CADCOR(
				BS_CODCOR,BS_PERCDEVCOR,BS_FAXADM,BS_CONTATOADM,BS_FAXOPER,BS_CONTATOOPER,
				BS_NOME,BS_CODCETIP,BS_BOVESPA,BS_ENDERECO,BS_PERCDEVFUT,
				BS_TIPO_CORRETORA,BS_IC_LIGADA,BS_IC_AGENTE_DE_COMPENSACAO,BS_NO_TEL_ADM,BS_NO_TEL_OPER,
				BS_CD_AG,BS_CD_CC,BS_CD_BCO,BS_CD_SWIFT,BS_DS_TIPO_LIQ_FIN_RV,
				BS_DS_TIPO_LIQ_FIN_FU,BS_DS_TIPO_LIQ_FIN_SW,BS_CD_BMF,BS_NO_CNPJ,BS_CD_CONTROLE_FINANC,
				BS_CONTATOOPERBVSP,BS_CONTATOADMBVSP,BS_NO_TEL_ADMBVSP,BS_NO_TEL_OPERBVSP,BS_FAXOPERBVSP,
				BS_FAXADMBVSP,BS_IC_LOG_LIBERADO,BS_ID_LOG_LIBERADO,BS_BASIF_CODINST,BS_SG_TIPO_SERVICO,
				BS_CD_ISPB,BS_CD_ADMIN,BS_NM_MEM_COMPENS,BS_CD_ISPB_BMF,BS_CD_BCO_BMF,
				BS_CD_CC_BMF,BS_CD_AG_BMF,BS_CD_IF,BS_CD_IF_BMF,BS_IC_PNA,
				BS_CD_DAC,BS_CD_DAC_BMF,BS_CD_GRUPO_ECONOMICO,BS_IC_CONT_DIAS_LIQ_FIN_CORTG,BS_NO_DIAS_LIQ_FIN_CORTG,
				Dataref			
			) exec FIN_PORTOPAR.dbo.EXTRACT_BS_RV_CADCOR @DataRef
			SET @BS_RV_CADCOR_message += CHAR(10) + CHAR(9) + CONVERT(VARCHAR(100),@@ROWCOUNT) + ' registro(s) inserido(s).'
			
			INSERT INTO LOAD_LOG VALUES(@IdExec,(select current_user),'PAS_LOAD_BS_RV_CADCOR',@DataInicioProcesso,GETDATE(),1,@DataRef,@BS_RV_CADCOR_message)
			SET NOCOUNT OFF		
			RAISERROR(@BS_RV_CADCOR_message,0,1) WITH NOWAIT	
			SET NOCOUNT ON
			
			SELECT LEFT('PAS_LOAD_BS_RV_CADCOR'+' >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',30) + '>' AS 'PROCEDURE', 'OK' AS 'STATUS'
		END
		END TRY
		BEGIN CATCH
			ROLLBACK;			
			SET @BS_RV_CADCOR_message += CHAR(10) + CHAR(9) + '########## ERRO ##########'
			SET @BS_RV_CADCOR_message += CHAR(10) + CHAR(9) + 
			'Msg ' + CONVERT(VARCHAR(100),ERROR_NUMBER()) + 
			', Level ' + CONVERT(VARCHAR(100),ERROR_SEVERITY()) + 
			', State ' + CONVERT(VARCHAR(100),ERROR_STATE()) + 
			', Line ' + CONVERT(VARCHAR(100),ERROR_LINE()) +
			CHAR(10) + CHAR(9) + ERROR_MESSAGE()

			INSERT INTO LOAD_LOG VALUES(@IdExec,(select current_user),'PAS_LOAD_BS_RV_CADCOR',@DataInicioProcesso,GETDATE(),0,@DataRef,@BS_RV_CADCOR_message)
			SET NOCOUNT OFF					
			SELECT LEFT('PAS_LOAD_BS_RV_CADCOR'+' >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>',30) + '>' AS 'PROCEDURE', 'ERRO' AS 'STATUS';
			RAISERROR(@BS_RV_CADCOR_message,16,2) WITH NOWAIT;
			THROW;				
		END CATCH
		
		
		
		COMMIT;
	END
END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'PAS_LOAD') AND xtype IN (N'P', N'PC') ) 
BEGIN 
	DROP PROCEDURE PAS_LOAD;
END  
GO

/****** Object:  StoredProcedure [dbo].[PAS_LOAD]    Script Date: 10/02/2015 18:25:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE[dbo].[PAS_LOAD]       
	@DataRef date --Data em que a procedure vai rodar 
AS 
BEGIN    
	SET NOCOUNT ON   
	DECLARE @DataInicioProcesso DATETIME = GETDATE() 
	DECLARE @Principal_message VARCHAR(MAX) = ''  
	DECLARE @TempMessage VARCHAR(MAX) = ''

	DECLARE @IdExec int  = (select ISNULL((SELECT MAX(IDEXEC) FROM LOAD_LOG),0))
	
	SET @IdExec += 1
	
	
	
	BEGIN TRY
		EXEC PAS_LOAD_BS_CL_PATRSINT				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_CLIENTE				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BSC_MOVIMENTO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_CL_PATR				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_CAIXA				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_MOEDA				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_BOLSA				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_XRATE				@DataRef, @TempMessage OUTPUT, @IdExec
		
		EXEC PAS_LOAD_BS_RV_EM_POS				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_EMPRESTIMO_ACOES				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_CONTA_CORRENTE				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_CL_PATRRV				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_CL_PATRRF				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_DIVIDENDO_PAG_POS				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_DIVIDENDO_QTD_POS				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_INDEX_COT				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_INDEX_CAD				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RF_LASTRO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BSC_RESGATE				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_BA_ADMINISTRACAO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RF_OPER				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RF_TIPO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_EM_POS_TAXA_REGISTRO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_BA_EQUIVALENCIAS				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BSC_POSICAO_NOTA				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_CL_CPR				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_GANHO_LIQ_MOV				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_FI_POS				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_FI_MOV				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_FI_CAD				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_FU_CAD				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_FU_COT				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_FU_MOV				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_FU_POS				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_ENQUADRAMENTO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_SW_POS				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_SW_CAD				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_SW_MOV				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_EM_POS_REMUNERACAO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_POS				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_CAD				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_COT				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_MOV				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RF_MOV				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RF_POS				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_EM_POS_COMISSAO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_CLI_CON				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RF_MEMORIA_IRRF				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_FU_VCTO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_BAS_CAD_IF				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BSC_COTISTA				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BSC_CLIENTE				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_DIVIDENDO				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_CL_PATRFUT				@DataRef, @TempMessage OUTPUT, @IdExec
		EXEC PAS_LOAD_BS_RV_CADCOR				@DataRef,@TempMessage OUTPUT, @IdExec
		
		DELETE BS_PARAM
		INSERT INTO BS_PARAM VALUES(@DataRef,@DataRef)
		

		INSERT INTO LOAD_LOG VALUES(@IdExec,(select current_user),'PAS_LOAD',@DataInicioProcesso,GETDATE(),1,@DataRef,@TempMessage)		
		PRINT( CHAR(10) +'Procedure executada com sucesso !');
	END TRY
	BEGIN CATCH
		INSERT INTO LOAD_LOG VALUES(@IdExec,(select current_user),'PAS_LOAD',@DataInicioProcesso,GETDATE(),0,@DataRef,@TempMessage)
		RAISERROR('Atenção, ocorreram erros na execução desta procedure.',15,1) WITH NOWAIT;
	END CATCH	

	
 
	--logging...
	
END
GO



COMMIT TRANSACTION

--pas_load_bs_rv_cadcor '20141212',null,null

--select * from bs_rv_cadcor

--pas_load '20141212'


