/****** Object:  Table [dbo].[BS_CONTA_CORRENTE]    Script Date: 10/02/2015 18:17:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BS_CONTA_CORRENTE]') AND type in (N'U'))
DROP TABLE [dbo].[BS_CONTA_CORRENTE]
GO
/****** Object:  Table [dbo].[BS_CONTA_CORRENTE]    Script Date: 10/02/2015 18:26:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BS_CONTA_CORRENTE](
	[BS_ID_CONTA] [numeric](7, 0) NOT NULL,
	[BS_CLCLI_CD] [char](15) NULL,
	[BS_CD_BANCO] [char](3)  NULL,
	[BS_CD_AGENCIA] [char](4)  NULL,
	[BS_CD_CONTA_CORRENTE] [char](15) NULL,
	[BS_CD_DAC] [char](2) NULL,
	[BS_SG_TIPO_CONTA] [char](1) NULL,
	[BS_IC_LOG_LIBERADO] [char](1) NULL,
	[BS_ID_LOG_LIBERADO] [int] NULL,
	[BS_IC_CONTA_INVESTIMENTO] [char](1) NULL,
	[BS_IC_ID_INT_EXT] [char](2) NULL,
	[DataRef] [datetime] NOT NULL,
 CONSTRAINT [XPKBS_CL_CONTA_CORRENTE] PRIMARY KEY CLUSTERED 
(
	[BS_ID_CONTA] ASC,
	[DataRef] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO