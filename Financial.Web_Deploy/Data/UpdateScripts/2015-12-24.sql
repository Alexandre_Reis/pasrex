﻿declare @data_versao char(10)
set @data_versao = '2015-12-24'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

begin transaction

	-- não permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)

	--DDL
	alter table IRProventos alter column ValorDistribuido decimal(18,2) not null;
	alter table IRProventos alter column ValorIR decimal(18,2) not null;

	if not exists(select 1 from syscolumns where id = object_id('AgendaFundo') and name = 'TipoValorInput')
	BEGIN
		ALTER TABLE AgendaFundo ADD TipoValorInput int NOT NULL default 1;
	END
	

	if not exists(select 1 from syscolumns where id = object_id('Carteira') and name = 'Rendimento')
	BEGIN
		ALTER TABLE Carteira ADD Rendimento int NOT NULL default 1
	END
	

	if not exists(select 1 from syscolumns where id = object_id('HistoricoCota') and name = 'CotaEx')
	BEGIN
		ALTER TABLE HistoricoCota ADD CotaEx decimal(28,10) NOT NULL default 0
	END
	

	if not exists(select 1 from syscolumns where id = object_id('HistoricoCota') and name = 'CotaRendimento')
	BEGIN
		ALTER TABLE HistoricoCota ADD CotaRendimento decimal(28,10) NOT NULL default 0
	END
	

	ALTER TABLE IRProventos alter column Descricao varchar(200) not null	

	if not exists(select 1 from syscolumns where id = object_id('IRProventos') and name = 'ValorLiquido')
	BEGIN
		ALTER TABLE IRProventos ADD ValorLiquido decimal(18,2) NOT NULL default 0
	END
	

	if not exists(select 1 from syscolumns where id = object_id('ExcecoesTributacaoIR') and name = 'RegimeEspecialTributacao')
	BEGIN
		ALTER TABLE ExcecoesTributacaoIR ADD RegimeEspecialTributacao char(1) NOT NULL default 'N'
	END
	
	--

	--DML
	--

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off



