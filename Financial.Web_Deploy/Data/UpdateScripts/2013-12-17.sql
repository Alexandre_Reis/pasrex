declare @data_versao char(10)
set @data_versao = '2013-12-17'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu de Tabela de Custos RF
INSERT INTO [dbo].[permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])VALUES(1,3900,'S','S','S','S')

update papelrendafixa set classe = 1105 where classe = 1600 --DPGE
go
update papelrendafixa set classe = 1110 where classe = 1800 --LF
go
update papelrendafixa set classe = 1200 where classe = 1201 --CRI Cetip
go

alter table operacaorendafixa add StatusExportacao int null
go

CREATE TABLE TabelaCustosRendaFixa(
	IdTabela int IDENTITY(1,1) NOT NULL,
	DataReferencia datetime NOT NULL,
	IdAgente int NOT NULL,
	IdCliente int NULL,	
	ValorMensal decimal(6, 2) NOT NULL,
	TaxaAno decimal(10, 4) NOT NULL,
	Classe decimal(16, 2) NULL,
	Canal tinyint NULL,
 CONSTRAINT PK_TabelaCustosRendaFixa PRIMARY KEY CLUSTERED 
(
	IdTabela ASC
)
)
GO

ALTER TABLE TabelaCustosRendaFixa  WITH CHECK ADD  CONSTRAINT FK_TabelaCustosRendaFixa_Cliente FOREIGN KEY(IdCliente)
REFERENCES Cliente (IdCliente)
GO

ALTER TABLE TabelaCustosRendaFixa CHECK CONSTRAINT FK_TabelaCustosRendaFixa_Cliente
GO

alter table posicaorendafixa drop constraint DF_PosicaoRendaFixa_TaxaCustodia
go
alter table posicaorendafixaabertura drop constraint DF_PosicaoRendaFixaAbertura_TaxaCustodia
go
alter table posicaorendafixahistorico drop constraint DF_PosicaoRendaFixaHistorico_TaxaCustodia
go

alter table posicaorendafixa drop column TaxaCustodia
go
alter table posicaorendafixaabertura drop column TaxaCustodia
go
alter table posicaorendafixahistorico drop column TaxaCustodia
go

alter table posicaorendafixa add CustoCustodia decimal (16, 2) null
go
alter table posicaorendafixaabertura add CustoCustodia decimal (16, 2) null
go
alter table posicaorendafixahistorico add CustoCustodia decimal (16, 2) null
go

ALTER TABLE posicaorendafixa ADD  CONSTRAINT DF_PosicaoRendaFixa_CustoCustodia  DEFAULT ((0)) FOR CustoCustodia
go
ALTER TABLE posicaorendafixaabertura ADD  CONSTRAINT DF_PosicaoRendaFixaAbertura_CustoCustodia  DEFAULT ((0)) FOR CustoCustodia
go
ALTER TABLE posicaorendafixahistorico ADD  CONSTRAINT DF_PosicaoRendaFixaHistorico_CustoCustodia  DEFAULT ((0)) FOR CustoCustodia
go
