declare @data_versao char(10)
set @data_versao = '2013-04-02'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table ClienteInterface alter column CodigoYMF varchar (200) null
alter table Cotista alter column CodigoInterface varchar (200) null
alter table Pessoa alter column CodigoInterface varchar (200) null

go