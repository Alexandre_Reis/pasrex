﻿declare @data_versao char(10)
set @data_versao = '2015-11-27'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin

    -- avisa o usuario em caso de erro
    raiserror('Atenção este script ja foi executado anteriormente.',16,1)

    -- não mostra a execução do script
    set nocount on

    -- não roda o script
    set noexec on 

end

begin transaction

     -- não permite que a transaction seja commitada em caso de erros
    set xact_abort on

    insert into VersaoSchema values(@data_versao)

    -- DDL	

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'IRProventos') = 0
	Begin
		CREATE TABLE IRProventos 
		(
			IdIRProventos					INT PRIMARY KEY identity NOT NULL, 
			IdCotista						INT                 NOT NULL,
			ValorDistribuido				decimal				NOT NULL,
			ValorIR							decimal				NOT NULL,
			IdCarteira						INT					NOT NULL,
			DataLancamento   				DateTime			NOT NULL,
			DataVencimento					DateTime			NOT NULL,
			Descricao						varchar(50)         NOT NULL,
			IsentoIR						varchar(01)			NOT NULL
		)
		
		ALTER TABLE IRProventos ADD CONSTRAINT IRProventos_Carteira_FK1 FOREIGN KEY (IdCarteira) REFERENCES Carteira(IdCarteira) on delete cascade
		ALTER TABLE IRProventos ADD CONSTRAINT IRProventos_Cotista_FK1 FOREIGN KEY (IdCotista) REFERENCES Cotista(IdCotista) on delete cascade
		
	end 
	
    --

    -- DML
	DELETE FROM PermissaoMenu WHERE IdMenu = 10280
		INSERT INTO [dbo].[PermissaoMenu]
		SELECT [IdGrupo]
		  ,10280
		  ,[PermissaoLeitura]
		  ,[PermissaoAlteracao]
		  ,[PermissaoExclusao]
		  ,[PermissaoInclusao]
		FROM [dbo].[PermissaoMenu]
	 WHERE IdMenu = 10270;
	 --

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off
