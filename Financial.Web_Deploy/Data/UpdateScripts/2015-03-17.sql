declare @data_versao char(10)
set @data_versao = '2015-03-17'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,19200,'S','S','S','S' from grupousuario where idgrupo <> 0
go
