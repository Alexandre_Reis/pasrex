﻿declare @data_versao char(10)
set @data_versao = '2013-05-24'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


--Acesso à nova entrada Contabil
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,25300,'N','S','S','S' from grupousuario where idgrupo <> 0
go

--Acesso à nova entrada RazaoContabil
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,25310,'N','S','S','S' from grupousuario where idgrupo <> 0
go

alter table transferenciacota add Taxa decimal (20, 12) null
go