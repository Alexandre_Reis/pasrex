declare @data_versao char(10)
set @data_versao = '2013-10-09'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table agentemercado add CartaPatente varchar(20) null
go
alter table clientebolsa add PessoaVinculada char(1) null
go
update clientebolsa set PessoaVinculada = 'N'
go
ALTER TABLE clientebolsa ADD  CONSTRAINT DF_ClienteBolsa_PessoaVinculada  DEFAULT (('N')) FOR PessoaVinculada
GO
