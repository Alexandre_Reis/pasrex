declare @data_versao char(10)
set @data_versao = '2015-06-05'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoFundoHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoFundoHistorico_Idx ON PosicaoFundoHistorico (DataHistorico, IdCliente);
END
GO	

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoFundoAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoFundoAbertura_Idx ON PosicaoFundoAbertura (DataHistorico, IdCliente);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoCotistaHistorico_Idx')
BEGIN
	CREATE INDEX PosicaoCotistaHistorico_Idx ON PosicaoCotistaHistorico (DataHistorico, IdCarteira);
END
GO		

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'PosicaoCotistaAbertura_Idx')
BEGIN
	CREATE INDEX PosicaoCotistaAbertura_Idx ON PosicaoCotistaAbertura (DataHistorico, IdCarteira);
END
GO	