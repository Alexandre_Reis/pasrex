declare @data_versao char(10)
set @data_versao = '2015-04-13'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)
go 

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'CopyTables')
BEGIN
	DROP PROCEDURE CopyTables
end
GO

	CREATE PROCEDURE CopyTables
		@Banco_Origem varchar(200), @Banco_Destino nvarchar(200),
		@idClienteOrigem int, @idClienteDestino int,
		@mercadoBolsa bit, @mercadoBMF bit,
		@mercadoRendaFixa bit, @mercadoLiquidacao bit,
		@mercadoFundo bit, @mercadoHistoricoCota bit
	AS

	DECLARE @sql nvarchar(2000)

	BEGIN TRY
		BEGIN TRANSACTION

	--#region Cria Pessoa Destino se não Existir
	-- /*************************  Cria Pessoa Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.Pessoa where idpessoa = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.Pessoa where IdPessoa = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idPessoa = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.Pessoa SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'Pessoa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria Cliente Destino se não Existir 
	-- /*************************  Cria Cliente Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.Cliente where idcliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.Cliente where IdCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.Cliente SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'cliente'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	-----------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria Carteira Destino se não Existir
	-- /*************************  Cria Carteira Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.Carteira where idCarteira = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.Carteira where IdCarteira = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCarteira = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.Carteira SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'Carteira'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	----------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria ClienteBolsa Destino se não Existir
	-- /*************************  Cria ClienteBolsa Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.ClienteBolsa where idCliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ClienteBolsa where idCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.ClienteBolsa SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'ClienteBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	----------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria ClienteBMF Destino se não Existir
	-- /*************************  Cria ClienteBMF Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.ClienteBMF where idCliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ClienteBMF where idCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.ClienteBMF SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'ClienteBMF'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	----------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region  Cria ClienteRendaFixa Destino se não Existir
	-- /*************************  Cria ClienteRendaFixa Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.ClienteRendaFixa where idCliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ClienteRendaFixa where idCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.ClienteRendaFixa SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'ClienteRendaFixa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Cria ClienteInterface Destino se não Existir
	-- /*************************  Cria ClienteInterface Destino se não Existir ******************************************************************/
	SET @sql = ' IF NOT EXISTS (SELECT * FROM [' + @Banco_Destino + '].dbo.ClienteInterface where idCliente = @idClienteDestino) ' + CHAR(13) +
			   ' BEGIN ' + CHAR(13) +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ClienteInterface where idCliente = @idClienteOrigem ' + CHAR(13) +
				   ' Update #TempTable set idCliente = @idClienteDestino ' + CHAR(13) +
				   ' INSERT INTO [' + @Banco_Destino + '].dbo.ClienteInterface SELECT * FROM #TempTable ' + CHAR(13) +
				   ' DROP TABLE #TempTable ' + CHAR(13) +
			   ' END '
	print 'ClienteInterface'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--#endregion
	--#region Data Dia e Status do Cliente Destino
	-- /*************************  Data Dia e Status do Cliente Destino ******************************************************************/
	-- Status:
	-- 1 - AbertoNaoCalculado
	-- 2 - AbertoCalculado
	-- 3 - Fechado
	-- 4 - UltimoCalculoNaoOK      
	---------------------------------------------------------------------------------------
	DECLARE @dataDiaCliente DateTime, @statusCliente int

	SET @sql = ' SELECT @dataDiaClienteOut = datadia, ' + CHAR(13) +
			   '		@statusClienteOut = status ' + CHAR(13) +
			   ' FROM [' + @Banco_Destino + '].dbo.Cliente where idCliente = @idClienteDestino ' + CHAR(13)
			   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteDestino int, @dataDiaClienteOut DateTime OUTPUT, @statusClienteOut int OUTPUT',
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaClienteOut=@dataDiaCliente OUTPUT, @statusClienteOut=@statusCliente OUTPUT
											  
	set @dataDiaCliente=CONVERT(varchar(8), @dataDiaCliente, 112) -- Tira o Time
	print @dataDiaCliente
	print @statusCliente
	--#endregion
	---------------------------------------------------------------------------------------------------------------------------------------------------

	--#region Mercado Bolsa
	-- /************************* Bolsa	******************************************************************/
	IF @mercadoBolsa = 1
	BEGIN

	-- /*************************  PosicaoBolsa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBolsa SELECT * FROM #TempTable ' +		   
			   ' DROP TABLE #TempTable '
	print 'PosicaoBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	-- /********************************************************************************************************************/
	-- /************************* PosicaoBolsaAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoBolsaAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoBolsaAbertura P, [' + @Banco_Origem + '].dbo.PosicaoBolsaAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 
	 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBolsaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBolsaAbertura'  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	-- /********************************************************************************************************************/
	-- /************************* PosicaoBolsaHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoBolsaHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoBolsaHistorico P, [' + @Banco_Origem + '].dbo.PosicaoBolsaHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBolsaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBolsaHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	-- /********************************************************************************************************************


	--/*************************  PosicaoEmprestimoBolsa ******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoEmprestimoBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	--/*************************  PosicaoEmprestimoBolsaAbertura  *****************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsaAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsaAbertura P, [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsaAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'

	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 

			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoEmprestimoBolsaAbertura'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/
	--/*************************    PosicaoEmprestimoBolsaHistorico	*****************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsaHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsaHistorico P, [' + @Banco_Origem + '].dbo.PosicaoEmprestimoBolsaHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente =  @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 
							 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoEmprestimoBolsaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoEmprestimoBolsaHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************


	--//*************************    OperacaoBolsa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************    OperacaoEmprestimoBolsa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoEmprestimoBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoEmprestimoBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoEmprestimoBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************     OrdemBolsa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OrdemBolsa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOrdem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OrdemBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OrdemBolsa'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************
	--//*************************     PerfilCorretagemBolsa	******************************************************************/
	SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.PerfilCorretagemBolsa where idCliente = @idClienteDestino ' +
			   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PerfilCorretagemBolsa where idCliente = @idClienteOrigem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PerfilCorretagemBolsa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PerfilCorretagemBolsa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************     ProventoBolsaCliente	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.ProventoBolsaCliente where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idProvento ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.ProventoBolsaCliente SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'ProventoBolsaCliente'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	END
	-- /*******************************************************************************************/

	--#endregion
	--#region Mercado BMF
	-- /************************* BMF	******************************************************************/
	IF @mercadoBMF = 1
	BEGIN

	-- /************************* PosicaoBMF	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoBMF where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBMF SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBMF'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	-- /********************************************************************************************************************/
	-- /************************* PosicaoBMFAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoBMFAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoBMFAbertura P, [' + @Banco_Origem + '].dbo.PosicaoBMFAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
							 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBMFAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBMFAbertura'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	-- /********************************************************************************************************************/
	-- /************************* PosicaoBMFHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoBMFHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoBMFHistorico P, [' + @Banco_Origem + '].dbo.PosicaoBMFHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
						
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoBMFHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoBMFHistorico'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	-- /********************************************************************************************************************/

	--/*************************    OperacaoBMF	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoBMF where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoBMF SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoBMF'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************     PerfilCorretagemBMF	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PerfilCorretagemBMF where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPerfilCorretagem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PerfilCorretagemBMF SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PerfilCorretagemBMF'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************     OrdemBMF	 ******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OrdemBMF where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOrdem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OrdemBMF SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OrdemBMF'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************
	END
	-- /*******************************************************************************************/

	--#endregion
	--#region Mercado RendaFixa
	-- /************************* RendaFixa	******************************************************************/
	IF @mercadoRendaFixa = 1
	BEGIN

	--/*************************  PosicaoRendaFixa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoRendaFixa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoRendaFixa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoRendaFixa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	--/*************************  PosicaoRendaFixaAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoRendaFixaAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoRendaFixaAbertura P, [' + @Banco_Origem + '].dbo.PosicaoRendaFixaAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoRendaFixaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoRendaFixaAbertura'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/
	--/*************************  PosicaoRendaFixaHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoRendaFixaHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoRendaFixaHistorico P, [' + @Banco_Origem + '].dbo.PosicaoRendaFixaHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 
										 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoRendaFixaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoRendaFixaHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/

	--//*************************    OperacaoRendaFixa	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoRendaFixa where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoRendaFixa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoRendaFixa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	END
	-- /*******************************************************************************************/

	--#endregion
	--#region Liquidacao
	-- /************************* Liquidacao	******************************************************************/
	IF @mercadoLiquidacao = 1
	BEGIN

	--#region Cria ContaCorrente ClienteDestino se não houver e retorna o IdConta do cliente Destino
	DECLARE @idConta int

	--INSERT INTO contacorrente
	--SELECT null, null, 110, 110, 1, 'S', 1 
	--WHERE NOT EXISTS (select top 1 1 from contacorrente where idpessoa = 110)
	--SELECT top 1 idConta FROM contacorrente WHERE idpessoa = 110 order by contadefault desc

	SET @sql = ' INSERT INTO [' + @Banco_Destino + '].dbo.ContaCorrente ' + CHAR(13) +
			   ' SELECT null, null, @idClienteDestino, @idClienteDestino, ''S'', 1, null, null, null, null, null ' + CHAR(13) +
			   ' WHERE NOT EXISTS (SELECT top 1 1 FROM [' + @Banco_Destino + '].dbo.ContaCorrente where idpessoa = @idClienteDestino) ' + CHAR(13) +
			   ' SELECT TOP 1 @idContaOut = idConta ' + CHAR(13) +	
			   ' FROM [' + @Banco_Destino + '].dbo.ContaCorrente where idPessoa = @idClienteDestino ORDER BY ContaDefault DESC' + CHAR(13)
			   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteDestino int, @idContaOut int OUTPUT',
						  @idClienteDestino = @idClienteDestino,
						  @idContaOut=@idConta OUTPUT
											  
	print @idConta
	--#endregion

	--#region Liquidacao
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.Liquidacao where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.Liquidacao SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'Liquidacao'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @idConta int', 
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @idConta = @idConta
	--#endregion

	--#region LiquidacaoAbertura
	--/*************************    LiquidacaoAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idliquidacao),0) from [' + @Banco_Destino + '].dbo.LiquidacaoAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
				 from [' + @Banco_Origem + '].dbo.LiquidacaoAbertura L, [' + @Banco_Origem + '].dbo.LiquidacaoAbertura L1
				 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
							 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.LiquidacaoAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
			   
	print 'LiquidacaoAbertura'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime, @idConta int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente,
						  @idConta = @idConta
																  
	--/********************************************************************************************************************/

	--#endregion

	--#region LiquidacaoHistorico
	--/*************************    LiquidacaoHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idliquidacao),0) from [' + @Banco_Destino + '].dbo.LiquidacaoHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
				 from [' + @Banco_Origem + '].dbo.LiquidacaoHistorico L, [' + @Banco_Origem + '].dbo.LiquidacaoHistorico L1
				 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.LiquidacaoHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '

	print 'LiquidacaoHistorico'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime, @idConta int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente,
						  @idConta = @idConta                     
	--/********************************************************************************************************************/

	--#endregion

	--#region LiquidacaoFuturo
	--/*************************      LiquidacaoFuturo	*****************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idliquidacao),0) from [' + @Banco_Destino + '].dbo.LiquidacaoFuturo' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY L.idliquidacao ASC)) + @i as Row, L.* 
				 from [' + @Banco_Origem + '].dbo.LiquidacaoFuturo L, [' + @Banco_Origem + '].dbo.LiquidacaoFuturo L1
				 where L.idliquidacao = L1.idliquidacao and L.datahistorico = L1.datahistorico and L.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), L.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idLiquidacao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdLiquidacao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.LiquidacaoFuturo SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '

	print 'LiquidacaoFuturo'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime, @idConta int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente,
						  @idConta = @idConta                    
	--/********************************************************************************************************************

	--#endregion

	--#region SaldoCaixa
	--/*************************     SaldoCaixa	******************************************************************/
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.SaldoCaixa where idCliente = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) <= @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.SaldoCaixa where idCliente = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.SaldoCaixa where idCliente = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) < @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.SaldoCaixa where idCliente = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 		   
					   
			   ' Update #TempTable set idCliente = @idClienteDestino, IdConta = @idConta ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.SaldoCaixa SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '

	print 'SaldoCaixa'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime, @idConta int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente,
						  @idConta = @idConta
	--/********************************************************************************************************************/
	--#endregion

	END
	-- /*******************************************************************************************/

	--#endregion
	--#region Mercado Fundo
	-- /************************* Fundo	******************************************************************/
	IF @mercadoFundo = 1
	BEGIN

	--//*************************  PosicaoFundo	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoFundo where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set IdCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoFundo SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoFundo'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	--/*************************  PosicaoFundoAbertura	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoFundoAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoFundoAbertura P, [' + @Banco_Origem + '].dbo.PosicaoFundoAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set IdCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoFundoAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoFundoAbertura'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/
	--/*************************  PosicaoFundoHistorico	******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoFundoHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoFundoHistorico P, [' + @Banco_Origem + '].dbo.PosicaoFundoHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCliente = @idClienteOrigem'

	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set IdCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoFundoHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoFundoHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************

	--//*************************    OperacaoFundo	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoFundo where IdCliente = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoFundo SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoFundo'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************

	--//*************************      PrejuizoFundo	******************************************************************/
	SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.PrejuizoFundo where idCliente = @idClienteDestino ' +
			   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PrejuizoFundo where idCliente = @idClienteOrigem ' +
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PrejuizoFundo SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PrejuizoFundo'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************
	--//*************************      PrejuizoFundoHistorico	******************************************************************/
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.PrejuizoFundoHistorico where idCliente = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), datahistorico, 112) AS Datetime) <= @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PrejuizoFundoHistorico where idCliente = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.PrejuizoFundoHistorico where idCliente = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), datahistorico, 112) AS Datetime) < @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PrejuizoFundoHistorico where idCliente = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 		   
					   
			   ' Update #TempTable set idCliente = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PrejuizoFundoHistorico SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '

	print 'PrejuizoFundoHistorico'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************
	END
	-- /*******************************************************************************************/

	--#endregion
	--#region HistoricoCota
	-- /************************* HistoricoCota	******************************************************************/
	IF @mercadoHistoricoCota = 1
	BEGIN

	--/*************************      HistoricoCota	******************************************************************/
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.HistoricoCota where idcarteira = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) <= @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.HistoricoCota where IdCarteira = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = ' DELETE FROM [' + @Banco_Destino + '].dbo.HistoricoCota where idcarteira = @idClienteDestino ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) < @dataDiaCliente ' +
				   ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.HistoricoCota where IdCarteira = @idClienteOrigem ' +
				   ' and cast(CONVERT(varchar(8), data, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 		   
					   
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.HistoricoCota SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'HistoricoCota'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************

	-- /************************* PosicaoCotista ******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.PosicaoCotista where IdCarteira = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' +
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoCotista SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'PosicaoCotista'		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************/
	--/*************************  PosicaoCotistaAbertura ******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoCotistaAbertura' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoCotistaAbertura P, [' + @Banco_Origem + '].dbo.PosicaoCotistaAbertura P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idClienteOrigem'
				 
	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 			 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoCotistaAbertura SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoCotistaAbertura'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************/
	--/*************************  PosicaoCotistaHistorico ******************************************************************/
	SET @sql = ' DECLARE @i int ' + CHAR(13) +
			   ' SELECT @i = isnull( max(idposicao),0) from [' + @Banco_Destino + '].dbo.PosicaoCotistaHistorico' + CHAR(13) +
							   
			   ' SELECT * INTO #TempTable FROM ( ' + CHAR(13) +
			   
			   ' SELECT ( ROW_NUMBER() OVER(ORDER BY P.idPosicao ASC)) + @i as Row, P.* 
				 from [' + @Banco_Origem + '].dbo.PosicaoCotistaHistorico P, [' + @Banco_Origem + '].dbo.PosicaoCotistaHistorico P1
				 where P.idPosicao = P1.idPosicao and P.datahistorico = P1.datahistorico and P.IdCarteira = @idClienteOrigem'

	IF @statusCliente = 3 -- Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) <= @dataDiaCliente '
	END

	ELSE	-- Nao Fechado
	BEGIN
		SET @sql = @sql + ' and cast(CONVERT(varchar(8), P.datahistorico, 112) AS Datetime) < @dataDiaCliente '
	END

	SET @sql = @sql + 
				 
			   ') as T' + CHAR(13) +
			   
			   ' ALTER TABLE #TempTable DROP COLUMN idPosicao ' + CHAR(13) +
			   ' EXEC tempdb..sp_rename ''#TempTable.Row'', ''IdPosicao'', ''COLUMN'' ' + CHAR(13) +
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.PosicaoCotistaHistorico SELECT * FROM #TempTable ' + CHAR(13) +
			   ' DROP TABLE #TempTable '
	print 'PosicaoCotistaHistorico'		  		   
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int, @dataDiaCliente DateTime',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino,
						  @dataDiaCliente = @dataDiaCliente
	--/********************************************************************************************************************

	--//*************************    OperacaoCotista	******************************************************************/
	SET @sql = ' SELECT * INTO #TempTable FROM [' + @Banco_Origem + '].dbo.OperacaoCotista where IdCarteira = @idClienteOrigem ' +
			   ' ALTER TABLE #TempTable DROP COLUMN idOperacao ' +
			   ' Update #TempTable set idCarteira = @idClienteDestino ' +
			   ' INSERT INTO [' + @Banco_Destino + '].dbo.OperacaoCotista SELECT * FROM #TempTable ' +
			   ' DROP TABLE #TempTable '
	print 'OperacaoCotista'
	EXECUTE sp_executesql @sql, 
						  N'@idClienteOrigem int, @idClienteDestino int',
						  @idClienteOrigem = @idClienteOrigem, 
						  @idClienteDestino = @idClienteDestino
	--/********************************************************************************************************************
	END
	-- /*******************************************************************************************/

	--#endregion
	print 'Commit'
	COMMIT TRAN

	END TRY
	BEGIN CATCH		
		IF @@TRANCOUNT > 0
		begin
			print '------- ROLLBACK Transaction pois houve ERRO'		
			print ERROR_MESSAGE()
			ROLLBACK TRAN --RollBack if error occured
		end        
	END CATCH
GO
