declare @data_versao char(10)
set @data_versao = '2014-11-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Menu Ajuste Operacao Renda Fixa
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,3895,'S','S','S','S' from grupousuario where idgrupo <> 0