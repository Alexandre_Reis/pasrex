﻿declare @data_versao char(10)
set @data_versao = '2013-10-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Acesso à nova entrada Nota Corretagem
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])
SELECT idgrupo,20180,'N','S','S','S' from grupousuario where idgrupo <> 0
go