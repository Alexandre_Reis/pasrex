﻿declare @data_versao char(10)
set @data_versao = '2014-09-12'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


-- O código abaixo deve ser inserido em todos os novos scripts, atualizando-se a data_versao:
if not exists(select 1 from syscolumns where id = object_id('IndicadoresCarteira') and name = 'Sharpe3Meses')   alter table IndicadoresCarteira add Sharpe3Meses decimal(20,12)   default 0 null;

if not exists(select 1 from syscolumns where id = object_id('IndicadoresCarteira') and name = 'Sharpe6Meses')  alter table IndicadoresCarteira add Sharpe6Meses decimal(20,12)   default 0 null;

if not exists(select 1 from syscolumns where id = object_id('IndicadoresCarteira') and name = 'Sharpe12Meses')  alter table IndicadoresCarteira add Sharpe12Meses decimal(20,12)  default 0 null;