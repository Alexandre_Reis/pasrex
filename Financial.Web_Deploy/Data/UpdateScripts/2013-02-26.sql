declare @data_versao char(10)
set @data_versao = '2013-02-26'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE ContabilPlanoContas(
	IdConta int IDENTITY(1,1) NOT NULL,
	IdPlano int NOT NULL,
	TipoConta char(1) NOT NULL,
	Descricao varchar(max) NOT NULL,
	IdContaMae int NULL,
	Codigo varchar(50) NULL,
	CodigoResumida varchar(8) NULL,
 CONSTRAINT PK_ContabilPlanoContas PRIMARY KEY CLUSTERED 
(
	IdConta ASC
)
)
GO

ALTER TABLE ContabilPlanoContas  WITH CHECK ADD  CONSTRAINT ContabilPlanoContas_ContabPlano_FK1 FOREIGN KEY(IdPlano)
REFERENCES ContabPlano (IdPlano)
GO

ALTER TABLE ContabilPlanoContas  WITH CHECK ADD  CONSTRAINT ContabilPlanoContas_ContabilPlanoContas_FK1 FOREIGN KEY(IdContaMae)
REFERENCES ContabilPlanoContas (IdConta)
GO
CREATE TABLE ContabSaldo(
	IdConta int NOT NULL,
	Data datetime NOT NULL,
	SaldoAnterior decimal(16, 2) NOT NULL,
	TotalDebito decimal(16, 2) NOT NULL,
	TotalCredito decimal(16, 2) NOT NULL,
	SaldoFinal decimal(16, 2) NOT NULL,
 CONSTRAINT PK_ContabSaldo PRIMARY KEY CLUSTERED 
(
	IdConta ASC,
	Data ASC
)
)
GO

ALTER TABLE ContabSaldo  WITH CHECK ADD  CONSTRAINT ContabSaldo_ContabilPlanoContas_FK1 FOREIGN KEY(IdConta)
REFERENCES ContabilPlanoContas (IdConta)
GO

alter table contablancamento add IdContaDebito int null
go
alter table contablancamento add IdContaCredito int null
go

ALTER TABLE ContabLancamento  WITH CHECK ADD  CONSTRAINT ContabLancamento_ContabilPlanoContas_FK1 FOREIGN KEY(IdContaCredito)
REFERENCES ContabilPlanoContas (IdConta)
go
ALTER TABLE ContabLancamento  WITH CHECK ADD  CONSTRAINT ContabLancamento_ContabilPlanoContas_FK2 FOREIGN KEY(IdContaDebito)
REFERENCES ContabilPlanoContas (IdConta)
go

alter table operacaobolsa add Observacao varchar(200)
go
alter table bloqueiobolsa add Observacao varchar(200)
go

