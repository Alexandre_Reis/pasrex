﻿declare @data_versao char(10)
set @data_versao = '2015-09-28'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return

BEGIN TRANSACTION


	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
			insert into VersaoSchema VALUES(@data_versao)

			/*
				Drop Constraint Feferenciadas na Ativobolsa (CdAtivoBolsa) 
			*/
			IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaDetalhe_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoBolsaDetalhe] DROP CONSTRAINT [AtivoBolsa_PosicaoBolsaDetalhe_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_ProventoBolsa_FK2', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[ProventoBolsa] DROP CONSTRAINT [AtivoBolsa_ProventoBolsa_FK2];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_ProventoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[ProventoBolsaCliente] DROP CONSTRAINT [AtivoBolsa_ProventoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_SubscricaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[SubscricaoBolsa] DROP CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoEmprestimoBolsa] DROP CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_SubscricaoBolsa_FK2', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[SubscricaoBolsa] DROP CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK2];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[GerPosicaoBolsa] DROP CONSTRAINT [AtivoBolsa_GerPosicaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsaAbertura_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_GerPosicaoBolsaAbertura_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_TabelaCONR_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[TabelaCONR] DROP CONSTRAINT [AtivoBolsa_TabelaCONR_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsaHistorico_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_GerPosicaoBolsaHistorico_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsaAbertura_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoEmprestimoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsaAbertura_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_Grupamento_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[GrupamentoBolsa] DROP CONSTRAINT [AtivoBolsa_Grupamento_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_OrdemBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[OrdemBolsa] DROP CONSTRAINT [AtivoBolsa_OrdemBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_OrdemBolsa_FK2', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[OrdemBolsa] DROP CONSTRAINT [AtivoBolsa_OrdemBolsa_FK2];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoEmprestimoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[LiquidacaoEmprestimoBolsa] DROP CONSTRAINT [AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_ConversaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[ConversaoBolsa] DROP CONSTRAINT [AtivoBolsa_ConversaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_ConversaoBolsa_FK2', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[ConversaoBolsa] DROP CONSTRAINT [AtivoBolsa_ConversaoBolsa_FK2];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_CotacaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[CotacaoBolsa] DROP CONSTRAINT [AtivoBolsa_CotacaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_LiquidacaoTermoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[LiquidacaoTermoBolsa] DROP CONSTRAINT [AtivoBolsa_LiquidacaoTermoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_DistribuicaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[DistribuicaoBolsa] DROP CONSTRAINT [AtivoBolsa_DistribuicaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_TransferenciaBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[TransferenciaBolsa] DROP CONSTRAINT [AtivoBolsa_TransferenciaBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_OperacaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[OperacaoBolsa] DROP CONSTRAINT [AtivoBolsa_OperacaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_OperacaoBolsa_FK2', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[OperacaoBolsa] DROP CONSTRAINT [AtivoBolsa_OperacaoBolsa_FK2];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PosicaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoBolsa] DROP CONSTRAINT [AtivoBolsa_PosicaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoTermoBolsa] DROP CONSTRAINT [AtivoBolsa_PosicaoTermoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaAbertura_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_PosicaoBolsaAbertura_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_AtivoBolsaHistorico_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[AtivoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_AtivoBolsaHistorico_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_BloqueioBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[BloqueioBolsa] DROP CONSTRAINT [AtivoBolsa_BloqueioBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_EventoFisicoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[EventoFisicoBolsa] DROP CONSTRAINT [AtivoBolsa_EventoFisicoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsaAbertura_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoTermoBolsaAbertura] DROP CONSTRAINT [AtivoBolsa_PosicaoTermoBolsaAbertura_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_BonificacaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[BonificacaoBolsa] DROP CONSTRAINT [AtivoBolsa_BonificacaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('FK_EvolucaoCapitalSocial_AtivoBolsa', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[EvolucaoCapitalSocial] DROP CONSTRAINT [FK_EvolucaoCapitalSocial_AtivoBolsa];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_BonificacaoBolsa_FK2', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[BonificacaoBolsa] DROP CONSTRAINT [AtivoBolsa_BonificacaoBolsa_FK2];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaHistorico_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_PosicaoBolsaHistorico_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_FatorCotacaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[FatorCotacaoBolsa] DROP CONSTRAINT [AtivoBolsa_FatorCotacaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsaHistorico_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoTermoBolsaHistorico] DROP CONSTRAINT [AtivoBolsa_PosicaoTermoBolsaHistorico_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsa_GerOperacaoBolsa_FK1', 'F') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[GerOperacaoBolsa] DROP CONSTRAINT [AtivoBolsa_GerOperacaoBolsa_FK1];
			END
			 
			IF (OBJECT_ID('AtivoBolsaHistorico_PK', 'PK') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[AtivoBolsaHistorico] DROP CONSTRAINT [AtivoBolsaHistorico_PK];
			END
			 
			IF (OBJECT_ID('CotacaoBolsa_PK', 'PK') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[CotacaoBolsa] DROP CONSTRAINT [CotacaoBolsa_PK];
			END
			 
			IF (OBJECT_ID('DistribuicaoBolsa_PK', 'PK') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[DistribuicaoBolsa] DROP CONSTRAINT [DistribuicaoBolsa_PK] ;
			END
			 
			IF (OBJECT_ID('PK_EvolucaoCapitalSocial', 'PK') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[EvolucaoCapitalSocial] DROP CONSTRAINT [PK_EvolucaoCapitalSocial] ;
			END
			 
			IF (OBJECT_ID('FatorCotacaoBolsa_PK', 'PK') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[FatorCotacaoBolsa] DROP CONSTRAINT [FatorCotacaoBolsa_PK] ;
			END
			 
			IF (OBJECT_ID('PosicaoBolsaDetalhe_PK', 'PK') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[PosicaoBolsaDetalhe] DROP CONSTRAINT [PosicaoBolsaDetalhe_PK]; 
			END
			 
			IF (OBJECT_ID('AtivoBolsa_PK', 'PK') IS NOT NULL)
			BEGIN
				 ALTER TABLE [dbo].[AtivoBolsa] DROP CONSTRAINT [AtivoBolsa_PK] ;
			END
			 

			/*
				altera tamanho da coluna CdAtivoBolsa para 25
			*/

			ALTER TABLE AtivoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE AtivoBolsa ALTER COLUMN CdAtivoBolsaObjeto VARCHAR(25) ;
			 
			ALTER TABLE PosicaoBolsaDetalhe ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE AtivoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE BloqueioBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE PosicaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE PosicaoBolsaAbertura ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE EventoFisicoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE BonificacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE BonificacaoBolsa ALTER COLUMN CdAtivoBolsaDestino VARCHAR(25) NOT NULL;
			 
			ALTER TABLE PosicaoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE EvolucaoCapitalSocial ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE FatorCotacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE PosicaoEmprestimoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE PosicaoEmprestimoBolsaAbertura ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE GerOperacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE GerOperacaoBolsa ALTER COLUMN CdAtivoBolsaOpcao VARCHAR(25) NULL;
			 
			ALTER TABLE PosicaoEmprestimoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE TransferenciaBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE GerPosicaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE GerPosicaoBolsaAbertura ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE GerPosicaoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE GrupamentoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE PosicaoTermoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE PosicaoTermoBolsaAbertura ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE PosicaoTermoBolsaHistorico ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE ProventoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE ProventoBolsaCliente ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE LiquidacaoEmprestimoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE SubscricaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE SubscricaoBolsa ALTER COLUMN CdAtivoBolsaDireito VARCHAR(25) NOT NULL;
			 
			ALTER TABLE ConversaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE ConversaoBolsa ALTER COLUMN CdAtivoBolsaDestino VARCHAR(25) NOT NULL;
			 
			ALTER TABLE LiquidacaoTermoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE TabelaCONR ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE CotacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE OperacaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE OperacaoBolsa ALTER COLUMN CdAtivoBolsaOpcao VARCHAR(25) ;
			 
			ALTER TABLE OperacaoEmprestimoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE OrdemBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 
			ALTER TABLE OrdemBolsa ALTER COLUMN CdAtivoBolsaOpcao VARCHAR(25);
			 
			ALTER TABLE DistribuicaoBolsa ALTER COLUMN CdAtivoBolsa VARCHAR(25) NOT NULL;
			 

			/*
				Recria as constraints com AtivoBolsa (CdAtivoBolsa) 
			*/
			IF (OBJECT_ID('AtivoBolsa_PK', 'PK') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[AtivoBolsa]  ADD CONSTRAINT [AtivoBolsa_PK] PRIMARY KEY (CdAtivoBolsa);
			END
			 
			IF (OBJECT_ID('AtivoBolsaHistorico_PK', 'PK') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[AtivoBolsaHistorico]  ADD CONSTRAINT [AtivoBolsaHistorico_PK] PRIMARY KEY (DataReferencia,CdAtivoBolsa);
			END
			  
			IF (OBJECT_ID('CotacaoBolsa_PK', 'PK') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[CotacaoBolsa]	ADD CONSTRAINT	[CotacaoBolsa_PK] PRIMARY KEY 	(Data,CdAtivoBolsa);
			END
			  
			IF (OBJECT_ID('DistribuicaoBolsa_PK', 'PK') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[DistribuicaoBolsa]	ADD CONSTRAINT	[DistribuicaoBolsa_PK] PRIMARY KEY (DataReferencia,CdAtivoBolsa);
			END
			  
			IF (OBJECT_ID('PK_EvolucaoCapitalSocial', 'PK') IS NULL)
			BEGIN
			   ALTER TABLE [dbo].[EvolucaoCapitalSocial]	ADD CONSTRAINT	[PK_EvolucaoCapitalSocial] PRIMARY KEY CLUSTERED	(CdAtivoBolsa,Data);
			END
			 
			IF (OBJECT_ID('FatorCotacaoBolsa_PK', 'PK') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[FatorCotacaoBolsa]	ADD CONSTRAINT	[FatorCotacaoBolsa_PK]	 PRIMARY KEY (DataReferencia,CdAtivoBolsa);
			END
			 
			IF (OBJECT_ID('PosicaoBolsaDetalhe_PK', 'PK') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoBolsaDetalhe]	ADD CONSTRAINT	[PosicaoBolsaDetalhe_PK] PRIMARY KEY CLUSTERED (DataHistorico, IdCliente, IdAgente, CdAtivoBolsa, TipoCarteira);
			END
			 
			IF (OBJECT_ID('AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[LiquidacaoEmprestimoBolsa] ADD CONSTRAINT [AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_AtivoBolsaHistorico_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[AtivoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_AtivoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_BloqueioBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[BloqueioBolsa] ADD CONSTRAINT [AtivoBolsa_BloqueioBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_CotacaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[CotacaoBolsa] ADD CONSTRAINT [AtivoBolsa_CotacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_DistribuicaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[DistribuicaoBolsa] ADD CONSTRAINT [AtivoBolsa_DistribuicaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_EventoFisicoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[EventoFisicoBolsa] ADD CONSTRAINT [AtivoBolsa_EventoFisicoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_FatorCotacaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[FatorCotacaoBolsa] ADD CONSTRAINT [AtivoBolsa_FatorCotacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_Grupamento_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[GrupamentoBolsa] ADD CONSTRAINT [AtivoBolsa_Grupamento_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_LiquidacaoTermoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[LiquidacaoTermoBolsa] ADD CONSTRAINT [AtivoBolsa_LiquidacaoTermoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_OperacaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[OperacaoBolsa] ADD CONSTRAINT [AtivoBolsa_OperacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_OrdemBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[OrdemBolsa] ADD CONSTRAINT [AtivoBolsa_OrdemBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoBolsa] ADD CONSTRAINT [AtivoBolsa_PosicaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaAbertura_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoBolsaAbertura] ADD CONSTRAINT [AtivoBolsa_PosicaoBolsaAbertura_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaDetalhe_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoBolsaDetalhe] ADD CONSTRAINT [AtivoBolsa_PosicaoBolsaDetalhe_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoBolsaHistorico_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_PosicaoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsaAbertura_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoEmprestimoBolsaAbertura] ADD CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsaAbertura_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoEmprestimoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsaAbertura_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoTermoBolsaAbertura] ADD CONSTRAINT [AtivoBolsa_PosicaoTermoBolsaAbertura_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsaHistorico_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoTermoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_PosicaoTermoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_ProventoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[ProventoBolsaCliente] ADD CONSTRAINT [AtivoBolsa_ProventoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_ProventoBolsa_FK2', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[ProventoBolsa] ADD CONSTRAINT [AtivoBolsa_ProventoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_SubscricaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[SubscricaoBolsa] ADD CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_TransferenciaBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[TransferenciaBolsa] ADD CONSTRAINT [AtivoBolsa_TransferenciaBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]) ON DELETE CASCADE;
			END
			  
			IF (OBJECT_ID('AtivoBolsa_BonificacaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[BonificacaoBolsa] ADD CONSTRAINT [AtivoBolsa_BonificacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_BonificacaoBolsa_FK2', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[BonificacaoBolsa] ADD CONSTRAINT [AtivoBolsa_BonificacaoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaDestino]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_ConversaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[ConversaoBolsa] ADD CONSTRAINT [AtivoBolsa_ConversaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_ConversaoBolsa_FK2', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[ConversaoBolsa] ADD CONSTRAINT [AtivoBolsa_ConversaoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaDestino]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_GerOperacaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[GerOperacaoBolsa] ADD CONSTRAINT [AtivoBolsa_GerOperacaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[GerPosicaoBolsa] ADD CONSTRAINT [AtivoBolsa_GerPosicaoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsaAbertura_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[GerPosicaoBolsaAbertura] ADD CONSTRAINT [AtivoBolsa_GerPosicaoBolsaAbertura_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_GerPosicaoBolsaHistorico_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[GerPosicaoBolsaHistorico] ADD CONSTRAINT [AtivoBolsa_GerPosicaoBolsaHistorico_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_OperacaoBolsa_FK2', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[OperacaoBolsa] ADD CONSTRAINT [AtivoBolsa_OperacaoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaOpcao]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_OrdemBolsa_FK2', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[OrdemBolsa] ADD CONSTRAINT [AtivoBolsa_OrdemBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaOpcao]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoEmprestimoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoEmprestimoBolsa] ADD CONSTRAINT [AtivoBolsa_PosicaoEmprestimoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_PosicaoTermoBolsa_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[PosicaoTermoBolsa] ADD CONSTRAINT [AtivoBolsa_PosicaoTermoBolsa_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_SubscricaoBolsa_FK2', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[SubscricaoBolsa] ADD CONSTRAINT [AtivoBolsa_SubscricaoBolsa_FK2] FOREIGN KEY ([CdAtivoBolsaDireito]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('AtivoBolsa_TabelaCONR_FK1', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[TabelaCONR] ADD CONSTRAINT [AtivoBolsa_TabelaCONR_FK1] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  
			IF (OBJECT_ID('FK_EvolucaoCapitalSocial_AtivoBolsa', 'F') IS NULL)
			BEGIN
				ALTER TABLE [dbo].[EvolucaoCapitalSocial] ADD CONSTRAINT [FK_EvolucaoCapitalSocial_AtivoBolsa] FOREIGN KEY ([CdAtivoBolsa]) REFERENCES [dbo].[AtivoBolsa]([CdAtivoBolsa]);
			END
			  

COMMIT TRANSACTION

GO


