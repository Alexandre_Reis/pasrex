declare @data_versao char(10)
set @data_versao = '2015-08-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

SET IDENTITY_INSERT ListaCategoriaFundo ON
if (select count(1) from ListaCategoriaFundo where IdLista = 10000) = 0	
BEGIN
	insert into ListaCategoriaFundo (IdLista, Descricao) values (10000, 'PadrÃo');
END 
SET IDENTITY_INSERT ListaCategoriaFundo OFF
