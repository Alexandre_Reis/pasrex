﻿declare @data_versao char(10)
set @data_versao = '2015-10-21'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION
	
	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on
	insert into VersaoSchema values(@data_versao);
	
	
	





IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PrazoMedio') = 0
begin
	CREATE TABLE PrazoMedio
	(
		IdPrazoMedio			 INT 			NOT NULL PRIMARY KEY identity,
		DataAtual 		 		 DateTime		NOT NULL,
		IdCliente		 		 INT 			NOT NULL,
		Descricao 		 		 VARCHAR(255) 	NOT NULL,
		PrazoMedioAtivo		 	 INT 			NOT NULL,
		PrazoMedio		 		 Decimal(10,0)	NOT NULL,
		ValorPosicao	 		 Decimal(25,2)	NOT NULL
	)
END

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'IdMercadoTipoPessoa' and Object_ID = Object_ID(N'CadastroComplementar'))
BEGIN
 ALTER TABLE CadastroComplementar ALTER COLUMN IdMercadoTipoPessoa varchar(20)
END
GO

IF EXISTS(SELECT * FROM sys.columns WHERE NAME = N'PrazoMedioAtivo' and Object_ID = Object_ID(N'PrazoMedio'))
BEGIN
 exec sp_rename 'PrazoMedio.PrazoMedioAtivo', 'MercadoAtivo'
END
GO

IF (select count(*) from SYSCOLUMNS WHERE ID = (SELECT ID FROM SYSOBJECTS WHERE  NAME = 'PrazoMedio' ) and name = 'IdPosicao') = 0
begin
 alter table PrazoMedio add IdPosicao int NULL;
END 
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflow') = 0
begin
	CREATE TABLE SuitabilityParametrosWorkflow
	(
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflow primary key(IdParametrosWorkflow)
	)
	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') = 0
begin
	CREATE TABLE SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] int NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end
GO

if(select count(1) from SuitabilityParametrosWorkflow) = 0
begin
	insert into SuitabilityParametrosWorkflow (AtivaWorkFlow) Values('N')
end
go

if(select count(1) from SuitabilityParametrosWorkflowHistorico) = 0
begin
	insert into SuitabilityParametrosWorkflowHistorico (DataHistorico, AtivaWorkFlow, Tipo) Values (getdate(), 'N', 'Insert')
end
go


if not exists(select count(1) from syscolumns where id = object_id('ControladoriaAtivo'))
begin
alter table carteira
add ControladoriaAtivo char(1)
end
go

if not exists(select count(1) from syscolumns where id = object_id('ControladoriaPassivo'))
begin
alter table carteira
add	ControladoriaPassivo char(1)
end
go
if not exists(select count(1) from syscolumns where id = object_id('MesmoConglomerado'))
begin
alter table carteira
add MesmoConglomerado char(1)
end
go

if not exists(select count(1) from syscolumns where id = object_id('CategoriaAnbima'))
begin
alter table carteira
add CategoriaAnbima int null
end
go

if not exists(select count(1) from syscolumns where id = object_id('Contratante'))
begin
alter table carteira
add Contratante int references AgenteMercado(IdAgente)
end
go


--Adicionar atributo
if not exists(select 1 from syscolumns where id = object_id('SuitabilityQuestaoHistorico') and name = 'Validacao')
BEGIN
	ALTER TABLE SuitabilityQuestaoHistorico ADD Validacao varchar(100) null
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityOpcaoHistorico') > 0
begin
	DROP TABLE SuitabilityOpcaoHistorico;

	CREATE TABLE SuitabilityOpcaoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdQuestao] [int] NOT NULL,
		[Questao] [varchar](2000) NULL,
		[IdOpcao] [int] NOT NULL,
		[Opcao] [varchar](2000) NULL,
		[Pontos] [int] NOT NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityOpcaoHistorico primary key(DataHistorico, IdQuestao, IdOpcao)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityResposta') > 0
begin
	DROP TABLE SuitabilityResposta;
end
go

if exists(select 1 from sysconstraints where object_name(id) = 'SuitabilityRespostaCotistaDetalhe' and object_name(constid) = 'SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK')
begin
	ALTER TABLE SuitabilityRespostaCotistaDetalhe DROP CONSTRAINT SuitabilityRespostaCotistaDetalhe_SuitabilityRespostaCotista_FK
end
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametroPerfilHistorico') > 0
begin

	drop table SuitabilityParametroPerfilHistorico;

	CREATE TABLE SuitabilityParametroPerfilHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		IdParametroPerfil int NOT NULL,
		IdValidacao int NOT NULL,
		Validacao varchar(100) NULL,
		IdPerfilInvestidor int NOT NULL,
		PerfilInvestidor varchar(20) NOT NULL,
		Definicao text NULL,
		FaixaPontuacao int NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametroPerfilHistorico primary key(DataHistorico, IdParametroPerfil)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'CarteiraSuitabilityHistorico') > 0
begin

	drop TABLE CarteiraSuitabilityHistorico

	CREATE TABLE CarteiraSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdCarteira] int NOT NULL,
		[IdPerfilRisco] int NULL,
		[PerfilRisco] varchar(100) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_CarteiraSuitabilityHistorico primary key(DataHistorico, IdCarteira)
	)	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityApuracaoRiscoHistorico') > 0
begin

	drop TABLE SuitabilityApuracaoRiscoHistorico

	CREATE TABLE SuitabilityApuracaoRiscoHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdApuracaoRisco] int NOT NULL,
		[IdValidacao] int NOT NULL,
		[Validacao] varchar(100) NULL,
		[IdPerfilCarteira] int NOT NULL,
		[PerfilCarteira] varchar(100) NULL,
		[PerfilRiscoFundo] varchar(100) NOT NULL,
		[Criterio] varchar(50) NOT NULL,
		[Percentual] int NOT NULL default 0,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityApuracaoRiscoHistorico primary key(DataHistorico, IdApuracaoRisco)
	)
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityParametrosWorkflowHistorico') > 0
begin

	drop TABLE SuitabilityParametrosWorkflowHistorico

	CREATE TABLE SuitabilityParametrosWorkflowHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdParametrosWorkflow] int NOT NULL default 1,
		[AtivaWorkFlow] char(1) NOT NULL default 'N',
		[VerificaEnquadramento] varchar(20) NULL,
		[ResponsavelNome] varchar(50) NULL,
		[ResponsavelEmail] varchar(100) NULL,
		[ValorPeriodo] int NULL,
		[GrandezaPeriodo] varchar(20) NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_SuitabilityParametrosWorkflowHistorico primary key(DataHistorico, IdParametrosWorkflow)
	)
	
end
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'SuitabilityEventosHistorico') > 0
begin

	drop TABLE SuitabilityEventosHistorico;

	CREATE TABLE SuitabilityEventosHistorico(
		[DataHistorico] [datetime] NOT NULL,
		[IdSuitabilityEventos] [int] NOT NULL,
		[Descricao] [varchar](100) NOT NULL,
		[IdMensagem] [int] NULL,
		[Mesagem] text NULL,
		[Tipo] [varchar](10) NOT NULL,
	 CONSTRAINT [PK_SuitabilityEventosHistorico] PRIMARY KEY (DataHistorico, IdSuitabilityEventos))
end
go

if (select count(1) from SuitabilityEventosHistorico) = 0
begin
	insert into SuitabilityEventosHistorico values (getdate(),1, 'Apuraзгo de novo perfil de Investidor (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),2, 'Declaraзгo sobre as informaзхes prestadas (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),3, 'Aceite do perfil apurado', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),4, 'Aceite do perfil apurado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),5, 'Aceite do tipo de dispensa', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),6, 'Aceite do tipo de dispensa (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),7, 'Recusa do preenchimento do formulбrio”', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),8, 'Recusa do preenchimento do formulбrio (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),9, 'Questionбrio nгo preenchido (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),10, 'Atualizar perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),11, 'Inexistкncia de Perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),12, 'Perfil desatualizado (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),13, 'Desenquadramento - Alteraзгo de perfil', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),14, 'Desenquadramento - Alteraзгo de perfil (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),15, 'Termo de Ciкncia (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),16, 'Termo de Ciкncia de Risco e Adesгo ao Fundo', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),17, 'Termo de Ciкncia de Risco e Adesгo ao Fundo (Portal)', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),18, 'Termo de Inadequaзгo', null, null, 'Insert');
	insert into SuitabilityEventosHistorico values (getdate(),19, 'Termo de Inadequaзгo (Portal)', null, null, 'Insert');
end
go

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PessoaSuitabilityHistorico') > 0
begin

	drop table PessoaSuitabilityHistorico

	CREATE TABLE PessoaSuitabilityHistorico
	(
		[DataHistorico] [datetime] NOT NULL,
		[IdPessoa] int NOT NULL,
		[IdValidacao] int NULL,
		[Validacao] varchar(100) NULL,
		[IdPerfil] int NULL,
		[Perfil] varchar(100) NULL,
		[Recusa] char(1) NOT NULL default 'N',
		[Dispensado] char(1) NOT NULL default 'N',
		[IdDispensa] int NULL,
		[Dispensa] varchar(100) NULL,
		[UltimaAlteracao] datetime NULL,
		[Tipo] [varchar](10) NOT NULL,
		CONSTRAINT PK_PessoaSuitabilityHistorico primary key(DataHistorico, IdPessoa)
	)	
end
GO

--Implantaзгo Suitability no cadastro de pessoas.
--TODO alterar processo para versгo 1.1.N (GPS) devido relaзгo 1xn de pessoa para cliente
insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select IdCliente, 'N', 'N', 1, getdate()
from Cliente
where IdTipo = 1
and not exists (select 1 from PessoaSuitability where cliente.IdCliente = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select IdCliente, 'N', 'N', 2, getdate()
from Cliente
where IdTipo = 10
and not exists (select 1 from PessoaSuitability where cliente.IdCliente = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao, Dispensa)
select IdCliente, 'S', 'N', 3, getdate(), 1
from Cliente
where Cliente.IdTipo in (300,310,320,500,510)
and not exists (select 1 from PessoaSuitability where cliente.IdCliente = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao, Dispensa)
select IdCliente, 'S', 'N', 4, getdate(), 1
from Cliente
where Cliente.IdTipo in (200)
and not exists (select 1 from PessoaSuitability where cliente.IdCliente = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select idCotista, 'N', 'N', 1, getdate()
from Cotista
where TipoTributacao = 1
and not exists (select 1 from PessoaSuitability where cotista.IdCotista = PessoaSuitability.IdPessoa);
go

insert into PessoaSuitability (IdPessoa, Dispensado, Recusa, IdValidacao, UltimaAlteracao)
select idCotista, 'N', 'N', 2, getdate()
from Cotista
where TipoTributacao = 2
and not exists (select 1 from PessoaSuitability where cotista.IdCotista = PessoaSuitability.IdPessoa);
go


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'TabelaPerfilMensalCVM') > 0
begin
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente1 varchar(50) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente2 varchar(50) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_CnpjComitente3 varchar(50) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada1 char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada2 char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ParteRelacionada3 char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal1 decimal (16, 2) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal2 decimal (16, 2) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorTotal3 decimal (16, 2) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_VedadaCobrancaTaxa char(1) null
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_DataUltimaCobranca datetime NULL
if not exists(select * from sys.all_columns where name = 'Secao18_CnpjComitente1' and object_id = object_id('TabelaPerfilMensalCVM'))
	alter table TabelaPerfilMensalCVM add Secao18_ValorUltimaCota decimal (16, 2) null
end




IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoCotistaAux]
	(
		[IdOperacaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[Observacao] [varchar](400) NOT NULL,
		[DadosBancarios] [varchar](500) NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdConta] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdAgenda] [int] NULL,
		[IdOperacaoResgatada] [int] NULL,
		[IdOperacaoAuxiliar] [int] NULL,
		[IdOrdem] [int] NULL
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[DetalheResgateCotistaAux]
	(
		[IdOperacao] [int] NOT NULL,
		[IdPosicaoResgatada] [int] NOT NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[TipoOperacao] [TinyInt] NOT NULL,
	 CONSTRAINT [PK_DetalheResgateCotistaAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoCotistaAux') = 0
Begin
	CREATE TABLE [dbo].[PosicaoCotistaAux]
	(
		[IdPosicaoCotistaAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCotista] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NOT NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'OperacaoFundoAux') = 0
Begin
	CREATE TABLE [dbo].[OperacaoFundoAux](
		[IdOperacaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[IdOperacao] [int] NOT NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[DataOperacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[DataLiquidacao] [datetime] NOT NULL,
		[DataAgendamento] [datetime] NOT NULL,
		[TipoOperacao] [tinyint] NOT NULL,
		[TipoResgate] [tinyint] NULL,
		[IdPosicaoResgatada] [int] NULL,
		[IdFormaLiquidacao] [tinyint] NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[CotaOperacao] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorCPMF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[PrejuizoUsado] [decimal](16, 2) NOT NULL,
		[RendimentoResgate] [decimal](16, 2) NOT NULL,
		[VariacaoResgate] [decimal](16, 2) NOT NULL,
		[IdConta] [int] NULL,
		[Observacao] [varchar](400) NOT NULL,
		[Fonte] [tinyint] NOT NULL,
		[IdAgenda] [int] NULL,
		[CotaInformada] [decimal](28, 12) NULL,
		[IdOperacaoResgatada] [int] NULL
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'PosicaoFundoAux') = 0
Begin	
	CREATE TABLE [dbo].[PosicaoFundoAux]
	(
		[IdPosicaoFundoAux] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
		[DataReferencia] [datetime] NOT NULL,
		[IdPosicao] [int] NOT NULL,
		[IdOperacao] [int] NULL,
		[IdCliente] [int] NOT NULL,
		[IdCarteira] [int] NOT NULL,
		[ValorAplicacao] [decimal](16, 2) NOT NULL,
		[DataAplicacao] [datetime] NOT NULL,
		[DataConversao] [datetime] NOT NULL,
		[CotaAplicacao] [decimal](28, 12) NOT NULL,
		[CotaDia] [decimal](28, 12) NOT NULL,
		[ValorBruto] [decimal](16, 2) NOT NULL,
		[ValorLiquido] [decimal](16, 2) NOT NULL,
		[QuantidadeInicial] [decimal](28, 12) NOT NULL,
		[Quantidade] [decimal](28, 12) NOT NULL,
		[QuantidadeBloqueada] [decimal](28, 12) NULL,
		[DataUltimaCobrancaIR] [datetime] NOT NULL,
		[ValorIR] [decimal](16, 2) NOT NULL,
		[ValorIOF] [decimal](16, 2) NOT NULL,
		[ValorPerformance] [decimal](16, 2) NOT NULL,
		[ValorIOFVirtual] [decimal](16, 2) NOT NULL,
		[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
		[ValorRendimento] [decimal](16, 2) NOT NULL,
		[DataUltimoCortePfee] [datetime] NULL,
		[PosicaoIncorporada] [char](1) NOT NULL
	)
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'DetalheResgateFundoAux') = 0
Begin
CREATE TABLE [dbo].[DetalheResgateFundoAux]
(
	[IdOperacao] [int] NOT NULL,
	[IdPosicaoResgatada] [int] NOT NULL,
	[IdCliente] [int] NOT NULL,
	[IdCarteira] [int] NOT NULL,
	[Quantidade] [decimal](28, 12) NOT NULL,
	[ValorBruto] [decimal](16, 2) NOT NULL,
	[ValorLiquido] [decimal](16, 2) NOT NULL,
	[ValorCPMF] [decimal](16, 2) NOT NULL,
	[ValorPerformance] [decimal](16, 2) NOT NULL,
	[PrejuizoUsado] [decimal](16, 2) NOT NULL,
	[RendimentoResgate] [decimal](16, 2) NOT NULL,
	[VariacaoResgate] [decimal](16, 2) NOT NULL,
	[ValorIR] [decimal](16, 2) NOT NULL,
	[ValorIOF] [decimal](16, 2) NOT NULL,
	[DataOperacao] [datetime] NOT NULL,
	[TipoOperacao] [TinyInt] NOT NULL,
	CONSTRAINT [PK_DetalheResgateFundoAux] PRIMARY KEY CLUSTERED 
	(
		[IdOperacao] ASC,
		[IdPosicaoResgatada] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
	) ON [PRIMARY]
END
GO

if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundo') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundo ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END
GO
	
if not exists(select 1 from syscolumns where id = object_id('OperacaoFundoAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoFundoAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N' 
END
GO	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotista') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotista ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'RegistroEditado')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD RegistroEditado char(1) NOT NULL DEFAULT 'N'
END	

if not exists(select 1 from syscolumns where id = object_id('OperacaoCotistaAux') and name = 'ValoresColados')
BEGIN
	ALTER TABLE dbo.OperacaoCotistaAux ADD ValoresColados char(1) NOT NULL DEFAULT 'N'
END	

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotas') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotas]
	(
		IdColagemComeCotas [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhe]
	(
		IdColagemComeCotasDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoComeCotas [decimal](28, 12) NOT NULL,
		VariacaoComeCotas [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasDetalhePosicao]
	(
		IdColagemComeCotasDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemComeCotasDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasMapaFundo]
	(
		IdColagemComeCotasMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoComeCotasVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosFundo]
	(
		IdColagemComeCotasPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaFundo]
	(
		IdColagemResgateMapaFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosFundo') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosFundo]
	(
		IdColagemOperResgPosFundo  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	
GO


IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgate') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgate]
	(
		IdColagemResgate [int] PRIMARY KEY NOT NULL IDENTITY,
		TipoRegistro char(1) NOT NULL ,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL
	)	
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhe') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhe]
	(
		IdColagemResgateDetalhe  [int] PRIMARY KEY NOT NULL IDENTITY,
		CodigoExterno char(15) NOT NULL ,
		DataReferencia [datetime] NOT NULL,
		DataOperacao [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		Quantidade [decimal](28, 12) NOT NULL,
		CotaOperacao [decimal](28, 12) NOT NULL,
		ValorBruto [decimal](28, 12) NOT NULL,
		ValorLiquido [decimal](28, 12) NOT NULL,
		ValorCPMF [decimal](28, 12) NOT NULL,
		ValorPerformance [decimal](28, 12) NOT NULL,
		PrejuizoUsado [decimal](28, 12) NOT NULL,
		RendimentoResgate [decimal](28, 12) NOT NULL,
		VariacaoResgate [decimal](28, 12) NOT NULL,
		ValorIR [decimal](28, 12) NOT NULL,
		ValorIOF [decimal](28, 12) NOT NULL,
	)	
END	
GO

 IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateDetalhePosicao') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateDetalhePosicao]
	(
		IdColagemResgateDetalhePosicao [int] PRIMARY KEY NOT NULL IDENTITY,
		IdColagemResgateDetalhe [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		QuantidadeInicial [decimal](28, 12) NOT NULL,
		QuantidadeRestante [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemResgateMapaCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemResgateMapaCotista]
	(
		IdColagemResgateMapaCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		DataReferencia [datetime] NOT NULL,
		IdCliente  [int] NOT NULL,
		IdCarteira  [int] NOT NULL,
		DataConversao [datetime] NULL,
		DetalheImportado char(1) NOT NULL DEFAULT 'N',
		IdOperacaoResgateVinculada  [int] NOT NULL,	
		Participacao [decimal](28, 12) NOT NULL
	)		
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemOperResgPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemOperResgPosCotista]
	(
		IdColagemOperResgPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  [int] NOT NULL,
		IdCliente  [int] NOT NULL,
		DataReferencia [datetime] NOT NULL,
		IdColagemResgateDetalhe  [int] NOT NULL,
		IdOperacaoResgateVinculada  [int] NOT NULL,
		IdPosicaoVinculada  [int] NOT NULL,
		QuantidadeVinculada [decimal](28, 12) NOT NULL
	)		
END	
GO

if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] DROP COLUMN Participacao;
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaFundo') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaFundo] ADD IdColagemResgateDetalhe int NULL;
END
GO

if exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] DROP COLUMN DataOperacao;
END
GO

if exists(select 1 from syscolumns where id = object_id('ColagemResgateDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateDetalhe] DROP COLUMN DataOperacao;
END
GO

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] DROP COLUMN DataOperacao;
END
GO

if exists(select 1 from syscolumns where id = object_id('ColagemComeCotasDetalhe') and name = 'DataOperacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotasDetalhe] DROP COLUMN DataOperacao;
END
GO


if exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'Participacao')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] DROP COLUMN Participacao;
END
GO

if NOT exists(select 1 from syscolumns where id = object_id('ColagemResgateMapaCotista') and name = 'IdColagemResgateDetalhe')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgateMapaCotista] ADD IdColagemResgateDetalhe int NULL;
END
GO

if not exists(select 1 from syscolumns where id = object_id('ColagemResgate') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemResgate] ADD Processado char(1) NOT NULL DEFAULT 'N';
END
GO		

if not exists(select 1 from syscolumns where id = object_id('ColagemComeCotas') and name = 'Processado')
BEGIN	
	ALTER TABLE [dbo].[ColagemComeCotas] ADD Processado char(1) NOT NULL DEFAULT 'N';
END
GO		

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'ColagemComeCotasPosCotista') = 0
Begin
	CREATE TABLE [dbo].[ColagemComeCotasPosCotista]
	(
		IdColagemComeCotasPosCotista  [int] PRIMARY KEY NOT NULL IDENTITY,
		IdCarteira  				[int] NOT NULL,
		IdCliente  					[int] NOT NULL,
		DataConversao 				[datetime] NULL,
		DataReferencia 				[datetime] NOT NULL,
		IdPosicaoVinculada  		[int] NOT NULL,
		PrejuizoUsado 				[decimal](28, 12) NOT NULL,
		RendimentoComeCotas 		[decimal](28, 12) NOT NULL,
		ValorBruto 					[decimal](28, 12) NOT NULL,
		ValorCPMF 					[decimal](28, 12) NOT NULL,
		ValorIOF 					[decimal](28, 12) NOT NULL,
		ValorIR 					[decimal](28, 12) NOT NULL,
		ValorLiquido 				[decimal](28, 12) NOT NULL,
		ValorPerformance 			[decimal](28, 12) NOT NULL,
		VariacaoComeCotas 			[decimal](28, 12) NOT NULL,
		Quantidade					[decimal](28, 12) NOT NULL,
		Participacao				[decimal](28, 12) NOT NULL,
		DetalheImportado 			[char](1) NOT NULL DEFAULT 'N'
	)		
END	
GO 


UPDATE InformacoesComplementaresFundo SET Periodicidade = SUBSTRING(Periodicidade,1, 250);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN Periodicidade varchar(250) null;

UPDATE InformacoesComplementaresFundo SET LocalFormaDivulgacao = SUBSTRING(LocalFormaDivulgacao,1, 300);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaDivulgacao varchar(300) null;

UPDATE InformacoesComplementaresFundo SET LocalFormaSolicitacaoCotista = SUBSTRING(LocalFormaSolicitacaoCotista,1, 250);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN LocalFormaSolicitacaoCotista varchar(250) null;


ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN FatoresRisco varchar(2000) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaExercicioVoto varchar(1000) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN TributacaoAplicavel varchar(2500) null;
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PoliticaAdministracaoRisco varchar(2500) null;

UPDATE InformacoesComplementaresFundo SET AgenciaClassificacaoRisco = SUBSTRING(AgenciaClassificacaoRisco,1, 50);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN AgenciaClassificacaoRisco varchar(50) null;

UPDATE InformacoesComplementaresFundo SET RecursosServicosGestor = SUBSTRING(RecursosServicosGestor,1, 100);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN RecursosServicosGestor varchar(100) null;

UPDATE InformacoesComplementaresFundo SET PrestadoresServicos = SUBSTRING(PrestadoresServicos,1, 100);
ALTER TABLE InformacoesComplementaresFundo ALTER COLUMN PrestadoresServicos varchar(100) null;		

COMMIT TRANSACTION

GO