declare @data_versao char(10)
set @data_versao = '2014-10-21'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table cliente add MultiMoeda char(1) null
go

update cliente set MultiMoeda = 'N'
go

alter table cliente alter column MultiMoeda char(1) not null
go

ALTER TABLE cliente ADD  CONSTRAINT [DF_Cliente_MultiMoeda]  DEFAULT (('N')) FOR MultiMoeda