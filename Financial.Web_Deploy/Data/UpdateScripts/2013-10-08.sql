declare @data_versao char(10)
set @data_versao = '2013-10-08'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table agentemercado add DigitoCodigoBovespa tinyint null
go
alter table agentemercado add DigitoCodigoBMF tinyint null
go

alter table posicaotermobolsa add TipoTermo tinyint null
go
alter table posicaotermobolsahistorico add TipoTermo tinyint null
go
alter table posicaotermobolsaabertura add TipoTermo tinyint null
go

ALTER TABLE posicaotermobolsa ADD  CONSTRAINT [DF_PosicaoTermoBolsa_TipoTermo]  DEFAULT ((1)) FOR TipoTermo
GO
ALTER TABLE posicaotermobolsahistorico ADD  CONSTRAINT [DF_PosicaoTermoBolsaHistorico_TipoTermo]  DEFAULT ((1)) FOR TipoTermo
GO
ALTER TABLE posicaotermobolsaabertura ADD  CONSTRAINT [DF_PosicaoTermoBolsaAbertura_TipoTermo]  DEFAULT ((1)) FOR TipoTermo
GO

update posicaotermobolsa set TipoTermo = 1
GO
update PosicaoTermoBolsaHistorico set TipoTermo = 1
GO
update PosicaoTermoBolsaAbertura set TipoTermo = 1
GO
