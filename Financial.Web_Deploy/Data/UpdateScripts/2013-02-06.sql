﻿-- O código abaixo deve ser inserido em todos os novos scripts, atualizando-se a data_versao:
declare @data_versao char(10)
set @data_versao = '2013-02-06'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return

set identity_insert CategoriaFundo on
insert into CategoriaFundo (IdCategoria,Descricao,IdLista) values (1000, 'CBLC Curto Prazo', 100)
insert into CategoriaFundo (IdCategoria,Descricao,IdLista) values (1001, 'CBLC Renda Fixa', 100)
insert into CategoriaFundo (IdCategoria,Descricao,IdLista) values (1002, 'CBLC Referenciado', 100)
set identity_insert CategoriaFundo off

--Ao final sempre atualizar a tabela de VersaoSchema com o script abaixo
insert into VersaoSchema VALUES(@data_versao)

if exists (select 1 from TabelaFundoCategoria where IdLista = 100 and IdCategoriaFundo in (1000, 1001, 1002)) return

insert into TabelaFundoCategoria
(IdCarteira,
 IdLista,
 IdCategoriaFundo)
 select idcliente, 100, 1001 from cliente
 where idtipo = 500





