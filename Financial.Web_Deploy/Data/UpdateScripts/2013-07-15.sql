declare @data_versao char(10)
set @data_versao = '2013-07-15'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table contabconta add FuncaoConta int null
go

update contabconta set FuncaoConta = 1 where left(codigo, 1) = '1' or left(codigo, 1) = '2' or left(codigo, 1) = '4' 
go

update contabconta set FuncaoConta = 20 where left(codigo, 1) = '7'
go

update contabconta set FuncaoConta = 21 where left(codigo, 1) = '8'
go

update contabconta set FuncaoConta = 30 where left(codigo, 1) = '3' or left(codigo, 1) = '9'
go

update contabconta set FuncaoConta = 40 where left(codigo, 1) = '6'
go

update contabconta set FuncaoConta = 50 where left(codigo, 5) = '6.1.8'
go

alter table contabconta alter column FuncaoConta int not null
go

