declare @data_versao char(10)
set @data_versao = '2015-06-01'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaoswap add CodigoIsin varchar(12) null
alter table operacaoswap add CpfcnpjContraParte varchar(30) null
