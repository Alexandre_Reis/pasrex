﻿declare @data_versao char(10)
set @data_versao = '2015-12-16'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenção este script ja foi executado anteriormente.',16,1)

	
	-- não mostra a execução do script
	set nocount on 
	
	-- não roda o script
	set noexec on 
	

end

begin transaction

	-- não permite que a transaction seja commitada em caso de erros
	set xact_abort on

	insert into VersaoSchema values(@data_versao)

	if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorAdministracaoPaga')
	BEGIN
		ALTER TABLE TabelaInformeDesempenho add ValorAdministracaoPaga decimal (16,2) not null default 0.0
	END

	if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorPfeePaga')
	BEGIN
		ALTER TABLE TabelaInformeDesempenho add ValorPfeePaga decimal (16,2) not null default 0.0
	END
	
	if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorCustodiaPaga')
	BEGIN
		ALTER TABLE TabelaInformeDesempenho add ValorCustodiaPaga decimal (16,2) not null default 0.0
	END
	
	if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorOutrasDespesasPagas')
	BEGIN
		ALTER TABLE TabelaInformeDesempenho add ValorOutrasDespesasPagas decimal (16,2) not null default 0.0
	END
	
	if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorAdministracaoPagaGrupoEconomicoADM')
	BEGIN
		ALTER TABLE TabelaInformeDesempenho add ValorAdministracaoPagaGrupoEconomicoADM decimal (16,2) not null default 0.0
	END
	
	if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorDespesasOperacionaisGrupoEconomicoADM')
	BEGIN
		ALTER TABLE TabelaInformeDesempenho add ValorDespesasOperacionaisGrupoEconomicoADM decimal (16,2) not null default 0.0
	END

	if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorAdministracaoPagaGrupoEconomicoGestor')
	BEGIN
		ALTER TABLE TabelaInformeDesempenho add ValorAdministracaoPagaGrupoEconomicoGestor decimal (16,2) not null default 0.0
	END
	
	if not exists(select 1 from syscolumns where id = object_id('TabelaInformeDesempenho') and name = 'ValorDespesasOperacionaisGrupoEconomicoGestor')
	BEGIN
		ALTER TABLE TabelaInformeDesempenho add ValorDespesasOperacionaisGrupoEconomicoGestor decimal (16,2) not null default 0.0
	END	
	
	delete from PermissaoMenu where idMEnu = 25120
	insert into [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
		  ,25120
		  ,[PermissaoLeitura]
		  ,[PermissaoAlteracao]
		  ,[PermissaoExclusao]
		  ,[PermissaoInclusao]
	  FROM [dbo].[PermissaoMenu]
	  where IdMenu = 15110;
		
-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off
