﻿BEGIN TRANSACTION


	--garante que só sera commitado caso não aconteça nenhum erro
	set xact_abort on

	--insere dados de configuração para que esses possam ser salvos posteriormente na tela
	insert into Configuracao(Id,Descricao,ValorTexto, ValorNumerico) values(4004,'FiltaTopNPieChart','', 5);
	insert into Configuracao(Id,Descricao,ValorTexto) values(4005,'JanelaModelSubReportRetornoCarteira','S');

COMMIT TRANSACTION

GO	
