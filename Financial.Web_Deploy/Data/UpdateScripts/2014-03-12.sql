declare @data_versao char(10)
set @data_versao = '2014-03-12'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE [TabelaBandaCarteira](
	[IdCarteira] [int] NOT NULL,
	[DataReferencia] [datetime] NOT NULL,
	[LimiteSuperior] [decimal](16, 8) NOT NULL,
	[LimiteInferior] [decimal](16, 8) NOT NULL,
 CONSTRAINT [PK_TabelaBandaCarteira] PRIMARY KEY CLUSTERED 
(
	[IdCarteira] ASC,
	[DataReferencia] ASC
)
)

GO


CREATE TABLE PerfilInvestidor(
 IdPerfilInvestidor tinyint NOT NULL,
 Descricao varchar(50) NOT NULL,
 CONSTRAINT PK_PerfilInvestidor PRIMARY KEY CLUSTERED 
(
 IdPerfilInvestidor ASC
)
) 

alter table pessoa add IdPerfilInvestidor tinyint null
go


ALTER TABLE Pessoa  WITH CHECK ADD  CONSTRAINT PerfilInvestidor_Pessoa_FK1 FOREIGN KEY(IdPerfilInvestidor)
REFERENCES PerfilInvestidor (IdPerfilInvestidor)
GO