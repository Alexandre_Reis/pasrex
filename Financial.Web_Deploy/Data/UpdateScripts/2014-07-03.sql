declare @data_versao char(10)
set @data_versao = '2014-07-03'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


ALTER TABLE Cliente ADD DataBloqueioProcessamento datetime NULL