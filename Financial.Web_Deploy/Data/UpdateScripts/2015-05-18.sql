declare @data_versao char(10)
set @data_versao = '2015-05-18'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MediaMovelRV') > 0
begin
	DROP TABLE MediaMovelRV;
END	
GO

IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MediaMovel') = 0
begin
	CREATE TABLE MediaMovel
	(
		IdMediaMovel			 INT 			NOT NULL PRIMARY KEY identity,
		DataAtual 		 		 DateTime		NOT NULL,
		IdCliente		 		 INT 			NOT NULL,
		TipoMediaMovel		 	 INT 			NOT NULL,
		MediaMovel		 		 Decimal(25,8)	NOT NULL,
		idPosicao				 int            NULL,
		ChaveComposta			 varchar(100) 	NULL				
	)
END	
GO

if Not exists(select 1 from syscolumns where id = object_id('MediaMovel') and name = 'IdCliente')
BEGIN
	ALTER TABLE dbo.MediaMovel ADD IdCliente smallint NULL;
	ALTER TABLE Cliente  WITH CHECK ADD  CONSTRAINT MediaMovel_Cliente_FK1 FOREIGN KEY(IdCliente)
	REFERENCES Cliente (IdCliente) ON DELETE Cascade;
END
GO	