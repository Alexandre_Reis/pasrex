declare @data_versao char(10)
set @data_versao = '2014-04-16'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaocotista add IdOperacaoAuxiliar int null
go

ALTER TABLE AtivoBolsa drop CONSTRAINT AtivoBolsa_GrupoEconomico_FK1
go

drop table GrupoEconomico
go

alter table AtivoBolsa add IdGrupoEconomico int null
go

update AtivoBolsa set IdGrupoEconomico = IdGrupo
go

alter table AtivoBolsa drop column IdGrupo
go

drop TABLE GrupoEconomico

CREATE TABLE GrupoEconomico(
	IdGrupo int IDENTITY(1,1) NOT NULL,
	Nome varchar(500) NOT NULL,
 CONSTRAINT GrupoEconomico_PK PRIMARY KEY CLUSTERED 
(
	IdGrupo ASC
)
)
GO

ALTER TABLE AtivoBolsa ADD CONSTRAINT AtivoBolsa_GrupoEconomico_FK1 FOREIGN KEY(IdGrupoEconomico)
REFERENCES GrupoEconomico (IdGrupo)
GO

alter table carteira add IdGrupoEconomico int null
go


if exists (select idtipo from tipocliente where idtipo = 510) return
insert into tipocliente VALUES(510, 'FDIC')

--Menu Tabela Grupo Economico
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,450,'S','S','S','S' from grupousuario where idgrupo <> 0


