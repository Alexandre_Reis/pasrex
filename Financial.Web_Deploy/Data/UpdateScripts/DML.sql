declare @data_versao char(10)
set @data_versao = '2015-10-29'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

delete from PermissaoMenu where idMEnu = 310
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,310
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 10260;
GO

delete from PermissaoMenu where idMEnu = 10245
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,10245
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 10260;
GO

delete from PermissaoMenu where idMEnu = 3960;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3960
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 3940;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3940
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 3770;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3770
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 3980;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3980
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 3970;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3970
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 3990;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3990
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 400;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,400
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 14260;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14260
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14200;
GO

delete from PermissaoMenu where idMEnu = 14261;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14261
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 14262;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14262
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 3950;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3950
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14200;
GO

delete from PermissaoMenu where idMEnu = 15250;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,15250
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 15260;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,15260
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 7725;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7725
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
	 
delete from PermissaoMenu where idMEnu = 630;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,630
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 640;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,640
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 10270;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,10270
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 7735;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,7735
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;
GO
	
delete from PermissaoMenu where idMEnu = 7730;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,7730
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 7740;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,7740
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 10280;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,10280
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 3990;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,3990
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 22280;
GO

if exists(select 1 from OperacaoRendaFixa where IdLiquidacao = 0)
BEGIN
	EXEC('UPDATE op SET op.IdLiquidacao = (CASE WHEN pap.TipoPapel <> 1 THEN 2 ELSE 1 END)
	FROM OperacaoRendaFixa op
	INNER JOIN TituloRendaFixa tit ON op.IdTitulo = tit.IdTitulo
	INNER JOIN PapelRendafixa pap ON pap.idPapel = tit.idPapel')
END
GO

if exists(select 1 from OperacaoRendaFixa where IdCustodia = 0)
BEGIN
	EXEC('UPDATE op SET op.IdCustodia = (CASE WHEN pap.TipoPapel <> 1 THEN 2 ELSE 1 END)
	FROM OperacaoRendaFixa op
	INNER JOIN TituloRendaFixa tit ON op.IdTitulo = tit.IdTitulo
	INNER JOIN PapelRendafixa pap ON pap.idPapel = tit.idPapel')
END
GO

delete from PermissaoMenu where idMenu = 7440;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7440
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 7430
GO

delete from PermissaoMenu where idMenu = 7450;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7450
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 7430
GO

delete from PermissaoMenu where idMenu = 14590;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14590
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14520
GO

delete from PermissaoMenu where idMenu = 14591;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14591
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14520
GO

delete from PermissaoMenu where idMenu = 14592;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14592
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14520
GO

delete from PermissaoMenu where idMenu = 14610;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14610
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14600
GO

delete from PermissaoMenu where idMenu = 14630;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14630
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14600
GO

delete from PermissaoMenu where idMenu = 4880;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4880
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 4850
GO

delete from PermissaoMenu where idMenu = 4881;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4881
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 4850
GO

delete from PermissaoMenu where idMenu = 4882;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4882
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 4850
GO

delete from permissaoMenu where idMenu in (480, 490, 500, 510, 520)
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,480
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,490
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,500
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,510
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,520
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

DELETE FROM PermissaoMenu WHERE IdMenu = 530
    INSERT INTO [dbo].[PermissaoMenu]
	SELECT [IdGrupo]
      ,530
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
	FROM [dbo].[PermissaoMenu]
 WHERE IdMenu = 100
 GO
	
delete from PermissaoMenu where idMenu = 480;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,480
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 490;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,490
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 500;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,500
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 510;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,510
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 520;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,520
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO	

DELETE FROM PermissaoMenu WHERE idMenu = 7720;
INSERT INTO [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7720
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 7430
 GO

delete from PermissaoMenu where idMenu = 4890;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4890
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 4880  
 GO
 
delete from PermissaoMenu where idMenu = 4900;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4900
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 4880
GO

delete from PermissaoMenu where idMenu = 4910;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4910
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 4880
GO

delete from PermissaoMenu where idMenu = 540;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,540
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 550;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,550
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 560;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,560
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 570;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,570
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 580;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,580
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 590;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,590
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 600;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,600
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 610;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,610
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  WHERE IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 4890;
delete from PermissaoMenu where idMenu = 4900;
delete from PermissaoMenu where idMenu = 4870;
delete from PermissaoMenu where idMenu = 7800;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7800
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 7810;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7810
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 7820;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7820
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 7830;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7830
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

delete from PermissaoMenu where idMenu = 7840;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7840
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 100
GO

IF NOT EXISTS (SELECT * FROM configuracao WHERE ID = 5014)
BEGIN
	insert into configuracao (ID, Descricao, ValorTexto) Values(5014, 'Retroagir Carteiras Dependentes', 'N');
END
GO

IF NOT EXISTS (SELECT * FROM configuracao WHERE ID = 5015)
BEGIN
	insert into configuracao (ID, Descricao, ValorTexto) Values(5015, 'Permitir Operações Retroativas', 'N');
END
GO

IF NOT EXISTS (SELECT * FROM configuracao WHERE ID = 5016)
BEGIN
	insert into configuracao (ID, Descricao, ValorTexto) Values(5016, 'Data Base', CONVERT(VARCHAR(10), GETDATE(), 103));
END
GO

BEGIN 
	DECLARE @IdCotista	INT, 
			@IdCarteira	INT,
			@ValorAplicacao DECIMAL(16,2),
			@DataAplicacao DATETIME,
			@DataConversao DATETIME,
			@Quantidade	DECIMAL(28,12),
			@CotaAplicacao DECIMAL(28,12),
			@IdOperacao INT,
			@IdPosicao INT,
			@DataDia DATETIME
	
	DECLARE CotistaCursor CURSOR FOR
	SELECT PosicaoCotista.IdCotista,
			PosicaoCotista.IdCarteira,
			PosicaoCotista.ValorAplicacao,
			PosicaoCotista.DataAplicacao,
			PosicaoCotista.DataConversao,
			PosicaoCotista.Quantidade,
			PosicaoCotista.CotaAplicacao,
			PosicaoCotista.IdOperacao,
			PosicaoCotista.IdPosicao,
			Cliente.DataDia
	FROM PosicaoCotista, Cliente
	WHERE PosicaoCotista.IdCarteira = Cliente.IdCliente
	and  PosicaoCotista.IdOperacao not in (select OperacaoCotista.IdOperacao from OperacaoCotista);

	OPEN CotistaCursor;
	-- Armazena os valores nas variáveis
	FETCH NEXT FROM CotistaCursor
	INTO @IdCotista, 
		@IdCarteira,
		@ValorAplicacao,
		@DataAplicacao,
		@DataConversao,
		@Quantidade,
		@CotaAplicacao,
		@IdOperacao,
		@IdPosicao,
		@DataDia 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO OperacaoCotista(IdCotista, IdCarteira, ValorBruto, Quantidade, DataOperacao, DataConversao, CotaOperacao, DataLiquidacao, DataAgendamento, TipoOperacao, IdFormaLiquidacao, Fonte, DataRegistro) 
		VALUES(@IdCotista, @IdCarteira, @ValorAplicacao, @Quantidade, @DataDia, @DataConversao,@CotaAplicacao, GetDate(),@DataAplicacao, 1, 1, 2, getdate());  

		UPDATE PosicaoCotista
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;

		UPDATE PosicaoCotistaAbertura
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;
		
		UPDATE PosicaoCotistaHistorico
		SET IdOperacao = (select max(OperacaoCotista.IdOperacao) FROM OperacaoCotista)
		WHERE IdPosicao = @IdPosicao;
		
		FETCH NEXT FROM CotistaCursor
		INTO @IdCotista, 
			@IdCarteira,
			@ValorAplicacao,
			@DataAplicacao,
			@DataConversao,
			@Quantidade,
			@CotaAplicacao,
			@IdOperacao,
			@IdPosicao,
			@DataDia 
	END

	CLOSE CotistaCursor;
	DEALLOCATE CotistaCursor;
END 
GO

IF NOT EXISTS (select 1 from TipoCliente where IdTipo = 801)
BEGIN
	insert into TipoCliente (IdTipo, Descricao) Values (801, 'Off Shore');
END
GO
	  
delete from PermissaoMenu where IdMEnu =  15040
insert into [dbo]. [PermissaoMenu]
SELECT [IdGrupo]
      ,15040
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 30000;
GO	  

--Criar processo de cadastro Travamento de Cotas
delete from PermissaoMenu where idMenu = 14640;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14640
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14630
GO

--Cria novo item de menu Consulta Ajuste de Cotas
delete from PermissaoMenu where idMenu = 14650;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14650
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14640
GO

delete from PermissaoMenu where idMenu = 10290;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,10290
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14630

delete from PermissaoMenu where idMEnu = 650;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,650
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 100 and IdGrupo = 1
GO

delete from PermissaoMenu where idMEnu = 660;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,660
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 100 and IdGrupo = 1
GO

delete from PermissaoMenu where idMEnu = 670;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,670
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 100 and IdGrupo = 1
GO

/* Diferencial - Ini */ 
DELETE FROM permissaomenu WHERE IdMenu = 19200;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,19200,'S','S','S','S' from grupousuario where idgrupo <> 0
go

DELETE FROM permissaomenu WHERE IdMenu = 390;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,390,'S','S','S','S' from grupousuario where idgrupo <> 0
go

DELETE FROM permissaomenu WHERE IdMenu = 6140;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,6140,'S','S','S','S' from grupousuario where idgrupo <> 0
go

DELETE FROM permissaomenu WHERE IdMenu = 25215;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,25215,'S','S','S','S' from grupousuario where idgrupo <> 0;
go

DELETE FROM permissaomenu WHERE IdMenu = 10270;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,10270,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

DELETE FROM permissaomenu WHERE IdMenu = 1150;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1150,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

DELETE FROM permissaomenu WHERE IdMenu = 1155;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1155,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

DELETE FROM permissaomenu WHERE IdMenu = 1160;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,1160,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

DELETE FROM CONFIGURACAO WHERE Id = 109;
INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(109,'PfeeCotasFundos',null,'N')
GO

DELETE FROM CONFIGURACAO WHERE Id = 1040;
INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(1040,'UsuarioIntegracao',null,'')
GO

DELETE FROM CONFIGURACAO WHERE Id = 1041;
INSERT INTO CONFIGURACAO(Id,Descricao,ValorNumerico,ValorTexto)VALUES(1041,'SenhaIntegracao',null,'')
GO

DELETE FROM permissaomenu WHERE IdMenu = 3895;
INSERT INTO [permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao]) 
SELECT idgrupo,3895,'S','S','S','S' from grupousuario where idgrupo <> 0
GO

/* Diferencial - Fim */ 

delete from PermissaoMenu where idMEnu = 680;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,680
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 100 and IdGrupo = 1
GO

delete from PermissaoMenu where idMEnu = 690;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
	  ,690
	  ,[PermissaoLeitura]
	  ,[PermissaoAlteracao]
	  ,[PermissaoExclusao]
	  ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
where IdMenu = 100 and IdGrupo = 1
GO


--Diferencial
set identity_insert tipodepara on 
BEGIN
	delete from depara where idtipodepara < 0;
	delete TipoDePara where IdTipoDePara < 0
END
GO

--BEGIN
--	set identity_insert TipoDePara on	
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-2, 1,	'SituacaoLancamentoLiquidacao','Tabela Liquidacao',	null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-3, 1,	'FonteLancamentoLiquidacao','Tabela Liquidacao',	null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-4, 1,	'Origem MT_CAIXA','Tabela Liquidacao',	null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-5, 1,	'Tipo MT_CAIXA','Tabela Liquidacao',	null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-6, 1,	'Tipo Operacao Fundo','Tabela OperacaoFundo', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-7, 1,	'TipoTributacaoFundo','Tabela Carteira', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-8, 1,	'TipoCarteiraFundo','Tabela Carteira', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-9, 1,	'TipoOperacaoTitulo','Tabela OperacaoRendaFixa', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-10,1,	'PontaSwap','Tabela OperacaoSwap', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-11,1,	'TipoMercadoAtivoBolsa','Tabela AtivoBolsa', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-12,1,	'PontaEmprestimoBolsa','Tabela  PosicaoEmprestimoBolsaHistorico', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-14,1,	'TipoApropriacaoSwap',	'Tipo de curva usada (exponencial / Linear)', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-15,1,	'ContagemDiasSwap',	'Dias úteis ou corridos', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-16,1,	'TipoAtivoAuxiliar',	'Tabela de Prazo Médio', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-17,1,	'TipoOperacaoCotista',	'Tipo de Operações do Cotista', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-18,1,	'TipoOperacaoCotistaNota',	'Tipo de Operações do Cotista resgate por nota', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-19,1,	'TipoResgateCotista',	'Tipo de Resgate de Cotista', null)
--	insert into TipoDePara(IdTipoDePara,TipoCampo,Descricao,Observacao,EnumTipoDePara) values(-20,1,	'TipoMercadoAtivoBMF','Tabela AtivoBMF', null)
--	set identity_insert TipoDePara off
--END
--GO
--
--BEGIN
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-2,1,1,'Normal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-2,2,2,'Pendente')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-2,3,3,'Compensacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-3,1,1,'Interno')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-3,2,2,'Manual')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-3,3,3,'Sinacor')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-3,4,4,'ArquivoCMDF')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,0,0,'Bolsa-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1,1,'Bolsa-CompraAcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,2,2,'Bolsa-VendaAcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,3,3,'Bolsa-CompraOpcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,4,4,'Bolsa-VendaOpcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,5,5,'Bolsa-ExercicioCompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,6,6,'Bolsa-ExercicioVenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,7,7,'Bolsa-CompraTermo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,8,8,'Bolsa-VendaTermo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,9,9,'Bolsa-AntecipacaoTermo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,10,10,'Bolsa-EmprestimoDoado')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,11,11,'Bolsa-EmprestimoTomado')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,12,12,'Bolsa-AntecipacaoEmprestimo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,13,13,'Bolsa-AjusteOperacaoFuturo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,14,14,'Bolsa-AjustePosicaoFuturo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,15,15,'Bolsa-LiquidacaoFinalFuturo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,16,16,'Bolsa-DespesasTaxas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,17,17,'Bolsa-Corretagem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,18,18,'Bolsa-Dividendo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,19,19,'Bolsa-JurosCapital')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,20,20,'Bolsa-Rendimento')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,21,21,'Bolsa-RestituicaoCapital')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,22,22,'Bolsa-CreditoFracoesAcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,23,23,'Bolsa-IRProventos')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,24,24,'Bolsa-OutrosProventos')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,25,25,'Bolsa-Amortizacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,40,40,'Bolsa-TaxaCustodia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,100,100,'Bolsa-Outros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,200,200,'BMF-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,201,201,'BMF-CompraVista')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,202,202,'BMF-VendaVista')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,203,203,'BMF-CompraOpcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,204,204,'BMF-VendaOpcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,205,205,'BMF-ExercicioCompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,206,206,'BMF-ExercicioVenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,220,220,'BMF-DespesasTaxas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,221,221,'BMF-Corretagem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,222,222,'BMF-Emolumento')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,223,223,'BMF-Registro')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,224,224,'BMF-OutrosCustos')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,250,250,'BMF-AjustePosicao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,251,251,'BMF-AjusteOperacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,260,260,'BMF-TaxaPermanencia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,300,300,'BMF-Outros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,500,500,'RendaFixa-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,501,501,'RendaFixa-CompraFinal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,502,502,'RendaFixa-VendaFinal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,503,503,'RendaFixa-CompraRevenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,504,504,'RendaFixa-VendaRecompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,505,505,'RendaFixa-NetOperacaoCasada')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,510,510,'RendaFixa-Vencimento')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,511,511,'RendaFixa-Revenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,512,512,'RendaFixa-Recompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,520,520,'RendaFixa-Juros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,521,521,'RendaFixa-Amortizacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,530,530,'RendaFixa-IR')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,531,531,'RendaFixa-IOF')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,540,540,'RendaFixa-Corretagem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,550,550,'RendaFixa-TaxaCustodia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,600,600,'RendaFixa-Outros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,701,701,'Swap-LiquidacaoVencimento')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,702,702,'Swap-LiquidacaoAntecipacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,703,703,'Swap-DespesasTaxas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,801,801,'Provisao-TaxaAdministracao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,802,802,'Provisao-PagtoTaxaAdministracao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,810,810,'Provisao-TaxaGestao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,811,811,'Provisao-PagtoTaxaGestao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,820,820,'Provisao-TaxaPerformance')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,821,821,'Provisao-PagtoTaxaPerformance')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,830,830,'Provisao-CPMF')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,840,840,'Provisao-TaxaFiscalizacaoCVM')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,841,841,'Provisao-PagtoTaxaFiscalizacaoCVM')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,890,890,'Provisao-ProvisaoOutros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,891,891,'Provisao-PagtoProvisaoOutros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,851,851,'Provisao-TaxaCustodia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,852,852,'Provisao-PagtoTaxaCustodia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1000,1000,'Fundo-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1001,1001,'Fundo-Aplicacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1002,1002,'Fundo-Resgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1003,1003,'Fundo-IRResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1004,1004,'Fundo-IOFResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1005,1005,'Fundo-PfeeResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1006,1006,'Fundo-ComeCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1010,1010,'Fundo-AplicacaoConverter')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1020,1020,'Fundo-TransferenciaSaldo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1100,1100,'Cotista-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1101,1101,'Cotista-Aplicacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1102,1102,'Cotista-Resgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1103,1103,'Cotista-IRResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1104,1104,'Cotista-IOFResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1105,1105,'Cotista-PfeeResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1106,1106,'Cotista-ComeCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,1110,1110,'Cotista-AplicacaoConverter')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,2000,2000,'IR-IRFonteOperacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,2001,2001,'IR-IRFonteDayTrade')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,2002,2002,'IR-IRRendaVariavel')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,3000,3000,'Margem-ChamadaMargem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,3001,3001,'Margem-DevolucaoMargem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,5000,5000,'-AjusteCompensacaoCota')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-4,100000,100000,'-Outros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,0,'RV','Bolsa-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1,'RV','Bolsa-CompraAcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,2,'RV','Bolsa-VendaAcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,3,'RV','Bolsa-CompraOpcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,4,'RV','Bolsa-VendaOpcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,5,'RV','Bolsa-ExercicioCompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,6,'RV','Bolsa-ExercicioVenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,7,'RV','Bolsa-CompraTermo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,8,'RV','Bolsa-VendaTermo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,9,'RV','Bolsa-AntecipacaoTermo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,10,'RV','Bolsa-EmprestimoDoado')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,11,'RV','Bolsa-EmprestimoTomado')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,12,'RV','Bolsa-AntecipacaoEmprestimo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,13,'RV','Bolsa-AjusteOperacaoFuturo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,14,'RV','Bolsa-AjustePosicaoFuturo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,15,'RV','Bolsa-LiquidacaoFinalFuturo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,16,'RV','Bolsa-DespesasTaxas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,17,'RV','Bolsa-Corretagem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,18,'RV','Bolsa-Dividendo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,19,'RV','Bolsa-JurosCapital')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,20,'RV','Bolsa-Rendimento')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,21,'RV','Bolsa-RestituicaoCapital')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,22,'RV','Bolsa-CreditoFracoesAcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,23,'RV','Bolsa-IRProventos')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,24,'RV','Bolsa-OutrosProventos')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,25,'RV','Bolsa-Amortizacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,40,'RV','Bolsa-TaxaCustodia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,100,'RV','Bolsa-Outros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,200,'FU','BMF-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,201,'FU','BMF-CompraVista')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,202,'FU','BMF-VendaVista')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,203,'FU','BMF-CompraOpcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,204,'FU','BMF-VendaOpcoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,205,'FU','BMF-ExercicioCompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,206,'FU','BMF-ExercicioVenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,220,'FU','BMF-DespesasTaxas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,221,'FU','BMF-Corretagem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,222,'FU','BMF-Emolumento')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,223,'FU','BMF-Registro')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,224,'FU','BMF-OutrosCustos')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,250,'FU','BMF-AjustePosicao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,251,'FU','BMF-AjusteOperacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,260,'FU','BMF-TaxaPermanencia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,300,'FU','BMF-Outros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,500,'RF','RendaFixa-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,501,'RF','RendaFixa-CompraFinal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,502,'RF','RendaFixa-VendaFinal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,503,'RF','RendaFixa-CompraRevenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,504,'RF','RendaFixa-VendaRecompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,505,'RF','RendaFixa-NetOperacaoCasada')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,510,'RF','RendaFixa-Vencimento')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,511,'RF','RendaFixa-Revenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,512,'RF','RendaFixa-Recompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,520,'RF','RendaFixa-Juros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,521,'RF','RendaFixa-Amortizacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,530,'RF','RendaFixa-IR')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,531,'RF','RendaFixa-IOF')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,540,'RF','RendaFixa-Corretagem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,550,'RF','RendaFixa-TaxaCustodia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,600,'RF','RendaFixa-Outros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,701,'SW','Swap-LiquidacaoVencimento')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,702,'SW','Swap-LiquidacaoAntecipacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,703,'SW','Swap-DespesasTaxas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,801,'CL','Provisao-TaxaAdministracao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,802,'CL','Provisao-PagtoTaxaAdministracao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,810,'CL','Provisao-TaxaGestao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,811,'CL','Provisao-PagtoTaxaGestao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,820,'CL','Provisao-TaxaPerformance')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,821,'CL','Provisao-PagtoTaxaPerformance')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,830,'CL','Provisao-CPMF')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,840,'CL','Provisao-TaxaFiscalizacaoCVM')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,841,'CL','Provisao-PagtoTaxaFiscalizacaoCVM')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,890,'CL','Provisao-ProvisaoOutros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,891,'CL','Provisao-PagtoProvisaoOutros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,851,'CL','Provisao-TaxaCustodia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,852,'CL','Provisao-PagtoTaxaCustodia')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1000,'FI','Fundo-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1001,'FI','Fundo-Aplicacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1002,'FI','Fundo-Resgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1003,'FI','Fundo-IRResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1004,'FI','Fundo-IOFResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1005,'FI','Fundo-PfeeResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1006,'FI','Fundo-ComeCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1010,'FI','Fundo-AplicacaoConverter')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1020,'FI','Fundo-TransferenciaSaldo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1100,'COT','Cotista-None')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1101,'COT','Cotista-Aplicacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1102,'COT','Cotista-Resgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1103,'COT','Cotista-IRResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1104,'COT','Cotista-IOFResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1105,'COT','Cotista-PfeeResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1106,'COT','Cotista-ComeCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,1110,'COT','Cotista-AplicacaoConverter')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,2000,'CL','IR-IRFonteOperacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,2001,'CL','IR-IRFonteDayTrade')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,2002,'CL','IR-IRRendaVariavel')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,3000,'FU','Margem-ChamadaMargem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,3001,'FU','Margem-DevolucaoMargem')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,5000,'CL','AjusteCompensacaoCota')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-5,100000,'MT','Outros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,1,'A','Aplicacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,2,'R','ResgateBruto')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,3,'R','ResgateLiquido')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,4,'R','ResgateCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,5,'R','ResgateTotal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,10,'A','AplicacaoCotasEspecial')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,11,'A','AplicacaoAcoesEspecial')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,12,'R','ResgateCotasEspecial')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,20,'','ComeCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,40,'','AjustePosicao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,80,'','Amortizacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,82,'','Juros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,84,'','AmortizacaoJuros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,100,'','IncorporacaoResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-6,101,'','IncorporacaoAplicacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,1,'RF','CurtoPrazo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,2,'RF','LongoPrazo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,3,'RV','Acoes')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,4,'RF','CPrazo_SemComeCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,5,'RF','LPrazo_SemComeCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-7,20,'Isento','Isento')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-8,1,'RF','Renda Fixa')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-8,2,'RV','Renda Variável')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,1,'C','CompraFinal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,2,'V','VendaFinal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,3,'C','CompraRevenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,4,'V','VendaRecompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,6,'V','VendaTotal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,10,'C','CompraCasada')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,11,'V','VendaCasada')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,12,'V','AntecipacaoRevenda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,13,'C','AntecipacaoRecompra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,20,'D','Deposito')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,21,'R','Retirada')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,22,'D','IngressoAtivoImpactoQtde')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,23,'D','IngressoAtivoImpactoCota')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,24,'R','RetiradaAtivoImpactoQtde')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-9,25,'R','RetiradaAtivoImpactoCota')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-10,1,'C','Parte')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-10,2,'V','ContraParte')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-11,'OPV','P','Opção de Venda')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-11,'OPC','O','Opção de Compra')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-11,'VIS','V','Papel a Vista')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-12,1,'D','Doador')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-12,2,'T','Tomador')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-14,1,'E','Exponencial')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-14,2,'L','Linear')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-15,1,'U','Uteis')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-15,2,'C','Corridos')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,1,'OperacaoBolsa','')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,2,'OperacaoRendaFixa','')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,3,'OperacaoFundos','')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,4,'ContaCorrente','')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,5,'OperacaoBMF','')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,6,'OperacaoSwap','')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-16,7,'Fundo','')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,1,'A','Aplicacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,2,'RB ','ResgateBruto')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,3,'RL ','ResgateLiquido')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,4,'RC ','ResgateCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,5,'RT ','ResgateTotal')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,10,'não mapeada','AplicacaoCotasEspecial')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,11,'não mapeada','AplicacaoAcoesEspecial')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,12,'não mapeada','ResgateCotasEspecial')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,20,'RL','ComeCotas')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,80,'não mapeada','Amortizacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,82,'não mapeada','Juros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,84,'não mapeada','AmortizacaoJuros')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,100,'não mapeada','IncorporacaoResgate')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-17,101,'não mapeada','IncorporacaoAplicacao')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-18,2,'NB','Resgate por NOTA - Valor Bruto')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-18,3,'NL','Resgate por NOTA -  Valor Líquido')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-18,5,'NT','Resgate por NOTA - Total')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-19,1,'não existe','Especifico')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-19,2,'F','FIFO')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-19,3,'não existe','LIFO')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-19,4,'J','MenorImposto')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,1,'DIS','Disponivel')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,2,'FUT','Futuro')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,3,'OPD','OpcaoDisponivel')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,4,'OPF','OpcaoFuturo')
--	insert into DePara (IdTipoDePara,CodigoInterno,CodigoExterno,DescricaoOrigem) values(-20,5,'TER','Termo')
--
set identity_insert tipodepara off
--		
--END
--GO

UPDATE OperacaoRendaFixa set Status = 1 where Status is NULL or Status = 0
UPDATE LiquidacaoRendaFixa set Status = 1 where Status is NULL or Status = 0
GO

if (select count(1) from TabelaCustosBolsa) = 0
begin
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 1, N'MercadoVista_Final', CAST(0.0285 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 2, N'MercadoVista_DayTrade', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 3, N'MercadoVista_CarteiraPropria', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 4, N'MercadoVista_ExercicioOpcaoLancadaOPC', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 5, N'MercadoVista_Fundo', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 6, N'MercadoVista_Clube', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 7, N'MercadoVista_ExercicioOpcaoIndice', CAST(0.0270 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0080 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 15, N'Leilao', CAST(0.3500 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.1500 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(70.00 AS Decimal(8, 2)), CAST(30.00 AS Decimal(8, 2)));
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 20, N'MercadoTermo', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0120 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0280 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 30, N'MercadoFuturo_Final', CAST(0.0055 AS Decimal(8, 4)), CAST(0.0032 AS Decimal(8, 4)), CAST(0.0017 AS Decimal(8, 4)), CAST(0.0076 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 31, N'MercadoFuturo_DayTrade', CAST(0.0055 AS Decimal(8, 4)), CAST(0.0006 AS Decimal(8, 4)), CAST(0.0017 AS Decimal(8, 4)), CAST(0.0015 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 40, N'MercadoOpcao_Final', CAST(0.0580 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0700 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 41, N'MercadoOpcao_DayTrade', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0140 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 42, N'MercadoOpcao_CarteiraPropria', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0200 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 43, N'MercadoOpcao_Fundo', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0210 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0490 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 44, N'MercadoOpcao_Clube', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0210 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0490 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 50, N'MercadoOpcaoIndice_Final', CAST(0.0270 AS Decimal(8, 4)), CAST(0.0050 AS Decimal(8, 4)), CAST(0.0080 AS Decimal(8, 4)), CAST(0.0450 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 51, N'MercadoOpcaoIndice_DayTrade', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0140 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 52, N'MercadoOpcaoIndice_CarteiraPropria', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0200 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 53, N'MercadoOpcaoIndice_Fundo', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0050 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0300 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 54, N'MercadoOpcaoIndice_Clube', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0050 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0300 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 60, N'Box_Final', CAST(0.0270 AS Decimal(8, 4)), CAST(0.0015 AS Decimal(8, 4)), CAST(0.0080 AS Decimal(8, 4)), CAST(0.0035 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 61, N'Box_CarteiraPropria', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0015 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0035 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 62, N'Box_Fundo', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0015 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0035 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 63, N'Box_Clube', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0015 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0035 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 70, N'Soma (Balcão)', CAST(0.0680 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, NULL);
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 100, N'EmprestimoBolsa_Voluntario', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.2500 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, CAST(10.00 AS Decimal(8, 2)));
	INSERT [dbo].[TabelaCustosBolsa] ([DataReferencia], [IdTabela], [Descricao], [PercentualEmolumentoBolsa], [PercentualRegistroBolsa], [PercentualLiquidacaoCBLC], [PercentualRegistroCBLC], [MinimoBovespa], [MinimoCBLC]) VALUES (CAST(0x0000000000000000 AS DateTime), 101, N'EmprestimoBolsa_Compulsorio', CAST(0.0190 AS Decimal(8, 4)), CAST(0.0060 AS Decimal(8, 4)), CAST(0.5000 AS Decimal(8, 4)), CAST(0.0000 AS Decimal(8, 4)), NULL, NULL);
end
GO

if (select count(1) from TipoInvestidorCVM) = 0
begin
	INSERT [dbo].[TipoInvestidorCVM] ([TipoCVM], [Descricao]) VALUES (1, N'CompanhiaAberta');
	INSERT [dbo].[TipoInvestidorCVM] ([TipoCVM], [Descricao]) VALUES (2, N'SociedadeBeneficiariaIncentivoFiscal');
	INSERT [dbo].[TipoInvestidorCVM] ([TipoCVM], [Descricao]) VALUES (3, N'InstituicaoFinanceira');
	INSERT [dbo].[TipoInvestidorCVM] ([TipoCVM], [Descricao]) VALUES (4, N'InvestidorEstrangeiro');
	INSERT [dbo].[TipoInvestidorCVM] ([TipoCVM], [Descricao]) VALUES (5, N'FundoInvestimento');
	INSERT [dbo].[TipoInvestidorCVM] ([TipoCVM], [Descricao]) VALUES (6, N'FundoAplicacaoCotas');
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(0.00 AS Decimal(16, 2)), CAST(300.00 AS Decimal(16, 2)));
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(2500000.00 AS Decimal(16, 2)), CAST(450.00 AS Decimal(16, 2)));
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(5000000.00 AS Decimal(16, 2)), CAST(675.00 AS Decimal(16, 2)));
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(10000000.00 AS Decimal(16, 2)), CAST(900.00 AS Decimal(16, 2)));
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(20000000.00 AS Decimal(16, 2)), CAST(1200.00 AS Decimal(16, 2)));
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(40000000.00 AS Decimal(16, 2)), CAST(1920.00 AS Decimal(16, 2)));
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(80000000.00 AS Decimal(16, 2)), CAST(2880.00 AS Decimal(16, 2)));
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(160000000.00 AS Decimal(16, 2)), CAST(3840.00 AS Decimal(16, 2)));
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(320000000.00 AS Decimal(16, 2)), CAST(4800.00 AS Decimal(16, 2)));
	INSERT [dbo].[TabelaTaxaFiscalizacaoCVM] ([TipoCVM], [FaixaPL], [ValorTaxa]) VALUES (6, CAST(640000000.00 AS Decimal(16, 2)), CAST(5400.00 AS Decimal(16, 2)));
end
GO

if (select count(1) from TabelaIOF) = 0
begin
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (1,96.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (2,93.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (3,90.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (4,86.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (5,83.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (6,80.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (7,76.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (8,73.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (9,70.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (10,66.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (11,63.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (12,60.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (13,56.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (14,53.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (15,50.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (16,46.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (17,43.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (18,40.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (19,36.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (20,33.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (21,30.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (22,26.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (23,23.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (24,20.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (25,16.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (26,13.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (27,10.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (28,6.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (29,3.00);
	insert into TabelaIOF (Prazo, AliquotaIOF) Values (30,0.00);
end
GO

if (select count(1) from TabelaTOB) = 0
begin
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'AUD', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'CAD', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A2EE00000000 AS DateTime), N'DI1', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'DOL', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'DOL', 3, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'DOL', 4, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'EUR', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A30000000000 AS DateTime), N'IDI', 3, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A2C400000000 AS DateTime), N'ind', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A2DC00000000 AS DateTime), N'IND', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A2C400000000 AS DateTime), N'IND', 4, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'ISP', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'ISP', 4, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'JPY', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x00008FE700000000 AS DateTime), N'oz1', 1, CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x00008FE700000000 AS DateTime), N'oz1', 3, CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)), CAST(0.00000000 AS Decimal(12, 8)))
	INSERT [dbo].[TabelaTOB] ([DataReferencia], [CdAtivoBMF], [TipoMercado], [TOB], [TOBDayTrade], [TOBMinima], [TOBDayTradeMinima], [TOBExercicio], [TOBExercicioCasado]) VALUES (CAST(0x0000A33B00000000 AS DateTime), N'T10', 2, CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)), CAST(1.00000000 AS Decimal(12, 8)))
end
GO

delete from PermissaoMenu where idMEnu = 10121;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,10121
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 1141;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1141
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

if (select count(1) from TipoCliente where idTipo = 801) > 0	
begin
	update TipoCliente set Descricao = 'Off Shore - PF' where idTipo = 801;
end
go

if (select count(1) from TipoCliente where idTipo = 802) = 0	
begin	
	insert into TipoCliente (IdTipo, Descricao) values ( 802 ,'Off Shore - PJ');
end
go	

delete from PermissaoMenu where idMEnu = 325;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,325
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 10300;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,10300
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 10260;
GO

if (select count(1) from Configuracao where ID = 5055) = 0	
begin
	INSERT INTO Configuracao VALUES (5055, 'Valida Qtde na data de Conversão (Processamento)', null, 'S');
end
go

UPDATE Cotista SET IdPessoa = IdCotista WHERE IdPessoa is null;
UPDATE Cliente SET IdPessoa = IdCliente WHERE IdPessoa is null;

if (select count(1) from Configuracao where ID = 5050) = 0	
begin
	INSERT INTO Configuracao VALUES (5050, 'IdAutomaticoPessoa', null, 'S');
end
go

if (select count(1) from Configuracao where ID = 5051) = 0	
begin
	INSERT INTO Configuracao VALUES (5051, 'PermiteDuplDocumentoPessoa', null, 'S');
end
go

if (select count(1) from Configuracao where ID = 5052) = 0	
begin
	INSERT INTO Configuracao VALUES (5052, 'PropagaAlterNomeCliente', null, 'S');
end
go

if (select count(1) from Configuracao where ID = 5053) = 0	
begin
	INSERT INTO Configuracao VALUES (5053, 'EstendeConceitoPessoaAgente', null, 'S');
end
go

if (select count(1) from Configuracao where ID = 5054) = 0	
begin
	INSERT INTO Configuracao VALUES (5054, 'PropagaAlterNomeAgente', null, 'S');
end
go

if (select count(1) from Configuracao where ID = 5055) = 0	
begin
	INSERT INTO Configuracao VALUES (5055, 'Valida Qtde na data de Conversão (Processamento)', null, 'S');
end
go

if (select count(1) from Configuracao where ID = 5056) = 0	
begin
	INSERT INTO Configuracao VALUES (5056, 'Tipo Visualização Fundos (Resgates de Cotistas)', null, 'A');
end
go

UPDATE operacao SET operacao.dataRegistro = operacao.dataConversao
FROM operacaoCotista operacao
INNER JOIN Cliente ON Cliente.IdCliente = operacao.IdCarteira
where cliente.DataImplantacao > operacao.DataConversao and cliente.DataImplantacao = operacao.dataRegistro;

UPDATE operacao SET operacao.dataRegistro = operacao.dataConversao
FROM operacaoFundo operacao
INNER JOIN Cliente ON Cliente.IdCliente = operacao.IdCliente
where cliente.DataImplantacao > operacao.DataConversao and cliente.DataImplantacao = operacao.dataRegistro;

if (select count(1) from Configuracao where ID = 5050) = 0	
begin
	INSERT INTO [dbo].[Configuracao] ([Id], [Descricao], [ValorNumerico], [ValorTexto]) VALUES (5050, N'IdAutomaticoPessoa', NULL, N'S')
end
go

if (select count(1) from Configuracao where ID = 5051) = 0	
begin	
	INSERT INTO [dbo].[Configuracao] ([Id], [Descricao], [ValorNumerico], [ValorTexto]) VALUES (5051, N'PermiteDuplDocumentoPessoa', NULL, N'S')
end
go

if (select count(1) from Configuracao where ID = 5052) = 0	
begin	
	INSERT INTO [dbo].[Configuracao] ([Id], [Descricao], [ValorNumerico], [ValorTexto]) VALUES (5052, N'PropagaAlterNomeCliente', NULL, N'S')
end
go	
	
if (select count(1) from Configuracao where ID = 5053) = 0	
begin	
	INSERT INTO [dbo].[Configuracao] ([Id], [Descricao], [ValorNumerico], [ValorTexto]) VALUES (5053, N'EstendeConceitoPessoaAgente', NULL, N'S')
end
go	
	
if (select count(1) from Configuracao where ID = 5054) = 0	
begin	
	INSERT INTO [dbo].[Configuracao] ([Id], [Descricao], [ValorNumerico], [ValorTexto]) VALUES (5054, N'PropagaAlterNomeAgente', NULL, N'S')
end
go	
	
if (select count(1) from Configuracao where ID = 5055) = 0	
begin	
	INSERT INTO [dbo].[Configuracao] ([Id], [Descricao], [ValorNumerico], [ValorTexto]) VALUES (5055, N'Valida Qtde na data de Conversão (Processamento)', NULL, N'S')
end
go	

delete from PermissaoMenu where idMEnu = 7750;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7750
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 7810;
GO

delete from PermissaoMenu where idMEnu = 1230;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,1230
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 1120;
GO

delete from PermissaoMenu where idMEnu = 10310;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,10310
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 10120;
GO

delete from PermissaoMenu where idMEnu = 7760;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7760
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 7810;
GO

DECLARE @IdAgenda int,
	 @IdCarteira int,
	 @TipoEvento int,
	 @DataEvento datetime,
	 @Valor decimal(30,20),
	 @Taxa decimal(30,20),
	 @TipoValorInput int;

--Cursor para atualizacao de AgendaFundo
DECLARE @cotaFechamento decimal(28,12);

DECLARE AgendaFundoCursor CURSOR FOR
SELECT IdAgenda,
	   IdCarteira,
	   TipoEvento,
	   DataEvento,
	   Valor,
	   Taxa,
	   TipoValorInput
FROM AgendaFundo;

OPEN AgendaFundoCursor;
FETCH NEXT FROM AgendaFundoCursor
INTO @IdAgenda,
	 @IdCarteira,
	 @TipoEvento,
	 @DataEvento,
	 @Valor,
	 @Taxa,
	 @TipoValorInput;
	 
--Verifica se há mais linhas a serem percorridas
WHILE @@FETCH_STATUS = 0
BEGIN
	if(@Valor <> 0)
	Begin
		select @TipoValorInput = 1;
	End

	if(@Taxa <> 0)
	Begin
		select @TipoValorInput = 2;

		select @cotaFechamento = 0;
		if(select count(1) from  HistoricoCota where IdCarteira = @IdCarteira and Data = @DataEvento) > 0
		Begin
			select @cotaFechamento = cotaFechamento from HistoricoCota where IdCarteira = @IdCarteira and Data = @DataEvento;
		end

		if(@cotaFechamento <> 0)
		begin
			select @Valor = @cotaFechamento * (@Taxa / (100 - @Taxa));
		end
	end

	update AgendaFundo set valor = @Valor, taxa = @Taxa, TipoValorInput = @TipoValorInput where IdAgenda = @IdAgenda;


	FETCH NEXT FROM AgendaFundoCursor
	INTO @IdAgenda,
		 @IdCarteira,
		 @TipoEvento,
		 @DataEvento,
		 @Valor,
		 @Taxa,
		 @TipoValorInput;
END
CLOSE AgendaFundoCursor;
DEALLOCATE AgendaFundoCursor;
GO

SET IDENTITY_INSERT ListaCategoriaFundo ON
if (select count(1) from ListaCategoriaFundo where IdLista = 10000) = 0	
BEGIN
	insert into ListaCategoriaFundo (IdLista, Descricao) values (10000, 'Padrão');
END 
SET IDENTITY_INSERT ListaCategoriaFundo OFF

delete from PermissaoMenu where idMEnu = 10301;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,10301
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 7810 and idGrupo = 1;
GO

delete from PermissaoMenu where idMEnu = 7770;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,7770
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 7810;
GO

if (select count(1) from ClasseCota) = 0	
begin	
	INSERT INTO ClasseCota (Descricao)
	values ('Não definido');
	INSERT INTO ClasseCota (Descricao)
	values ('Classe Única');   
	INSERT INTO ClasseCota (Descricao)
	values ('Sênior');
	INSERT INTO ClasseCota (Descricao)
	values ('Mezzanino');
	INSERT INTO ClasseCota (Descricao)
	values ('Subordinada');
	INSERT INTO ClasseCota (Descricao)
	values ('Subordinada Júnior');	
end
go

delete from PermissaoMenu where idMEnu = 10320;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,10320
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 10160;
GO

UPDATE Rentabilidade SET TipoAtivo = 20 where TipoAtivo = 2;
UPDATE Rentabilidade SET TipoAtivo = 30 where TipoAtivo = 3;
UPDATE Rentabilidade SET TipoAtivo = 40 where TipoAtivo = 4;
UPDATE Rentabilidade SET TipoAtivo = 50 where TipoAtivo = 5;
UPDATE Rentabilidade SET TipoAtivo = 60 where TipoAtivo = 6;
UPDATE Rentabilidade SET TipoAtivo = 200 where TipoAtivo = 7;

delete from PermissaoMenu where idMEnu = 3965;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3965
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 10160;
GO

if (select count(1) from TipoDePara where EnumTipoDePara = 1 and Descricao = 'Fincs - Indexador') = 0	
BEGIN
	declare @IdTipoDePara INT
	Insert into TipoDePara values( 1, 'Fincs - Indexador', null, 1);

	set @IdTipoDePara = (select IdTipoDePara from TipoDePara where EnumTipoDePara = 1 and Descricao = 'Fincs - Indexador')
	
	INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipoDePara, 1, 123); --CDI
	INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipoDePara, 10, 130); --SELIC
	INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipoDePara, 61,127); --IPCA)
	INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipoDePara, 60, 126); --IGPM)
	INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipoDePara, 62, 4); --INPC)
	INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipoDePara, 63, 3); --IGPDI)
end
go	

if (select count(1) from GrupoPerfilMTM ) = 0	
BEGIN
	declare @IdGrupoPerfilMTM INT
	
	INSERT INTO GrupoPerfilMTM (Descricao) VALUES ('Grupo Default'); 
	set @IdGrupoPerfilMTM = (select IdGrupoPerfilMTM from GrupoPerfilMTM where Descricao = 'Grupo Default')
	UPDATE PerfilMTM SET IdGrupoPerfilMTM = @IdGrupoPerfilMTM WHERE IdGrupoPerfilMTM IS NULL
end
go		

delete from PermissaoMenu where idMEnu = 4130;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,4130
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

IF NOT EXISTS (SELECT * FROM configuracao WHERE ID = 6100)
BEGIN
	insert into configuracao (ID, Descricao, ValorTexto) Values(6100, 'Aborta proc por erro cadastral no título de RF', 'S');
END
GO

if (select count(1) from TipoDePara where Descricao = 'Fincs - Indexador') = 0
begin
	Insert into TipoDePara values( 1, 'Fincs - Indexador', null, 1);
	declare @IdTipodepara int
	select @IdTipodepara = IdTipoDePara from TipoDePara where Descricao = 'Fincs - Indexador'
		INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipodepara, 1, 123); --CDI
		INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipodepara, 10, 130); --SELIC
		INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipodepara, 61,127); --IPCA)
		INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipodepara, 60, 126); --IGPM)
		INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipodepara, 62, 4); --INPC)
		INSERT INTO DePara (IdTipoDePara, CodigoInterno, CodigoExterno) VALUES ( @IdTipodepara, 63, 3); --IGPDI)
end
go