﻿declare @data_versao char(10)
set @data_versao = '2014-10-23'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

-- Tabela Feeder Inicio
CREATE TABLE Feeder 
( 	IdFeeder INT PRIMARY KEY NOT NULL IDENTITY , 
	Descricao VARCHAR(50) NOT NULL, 
	Url VARCHAR(255) NULL,
	LocalJarFile VARCHAR(255) NULL,
	LocalDados VARCHAR(255) NULL
) 
GO

INSERT INTO Feeder (descricao,url) VALUES ('BVMF'	,'http://www.bmfbovespa.com.br/shared/iframeBoletim.aspx?altura=3800&idioma=pt-br&url=www2.bmf.com.br/pages/portal/bmfbovespa/boletim1/TxRef1.asp');
GO
INSERT INTO Feeder (descricao) VALUES ('ANBIMA');
GO
INSERT INTO Feeder (descricao) VALUES ('SND'	);
GO
INSERT INTO Feeder (descricao) VALUES ('CETIP'	);
GO
INSERT INTO Feeder (descricao) VALUES ('Area Interna'	);
GO
INSERT INTO Feeder (descricao) VALUES ('BACEN'	);
GO

delete from PermissaoMenu where idMEnu = 310;
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,310
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
-- Tabela Feeder Fim

-- Tabela Curva RF INICIO
CREATE TABLE CurvaRendaFixa 
( 	IdCurvaRendaFixa 					INT PRIMARY KEY NOT NULL, 
	Descricao 							VARCHAR(100) NOT NULL, 
	CriterioInterpolacao 				INT NOT NULL, 
	ExpressaoTaxaZero 					INT NOT NULL, 	
	PermiteInterpolacao			 		INT NOT NULL, 
	PermiteExtrapolacao		 			INT NOT NULL,
	TipoCurva				 			INT NOT NULL,
	IdCurvaBase  						INT NULL,
	IdCurvaSpread  						INT NULL,
	TipoComposicao						INT NULL,
	IdIndice							smallint NULL
) 
GO

alter table CurvaRendaFixa add constraint CurvaRF_CurvaBase_FK FOREIGN KEY ( IdCurvaBase ) references CurvaRendaFixa(IdCurvaRendaFixa)
GO

alter table CurvaRendaFixa add constraint CurvaRF_CurvaSpread_FK FOREIGN KEY ( IdCurvaSpread ) references CurvaRendaFixa(IdCurvaRendaFixa)
GO

alter table CurvaRendaFixa add constraint Curva_Indice_FK FOREIGN KEY ( IdIndice ) references Indice(IdIndice);
GO

delete from PermissaoMenu where idMEnu = 3920;
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3920
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
-- Tabela Curva RF FIM

-- TABELA SERIERENDAFIXA Inicio
ALTER TABLE SerieRendaFixa ADD TipoTaxa	INT NOT NULL default '0';
GO
ALTER TABLE SerieRendaFixa ADD IdFeeder	INT NOT NULL default '5';
GO
alter table SerieRendaFixa add constraint SerieRF_Feeder_FK FOREIGN KEY ( IdFeeder ) references Feeder(IdFeeder)
GO

UPDATE SerieRendaFixa
SET TipoTaxa = isnull(titulo.TipoMTM, 1)
FROM SerieRendaFixa serie
INNER JOIN TituloRendaFixa titulo ON serie.IdSerie = titulo.IdSerie;
GO

ALTER TABLE TituloRendaFixa Drop Column TipoMTM;
GO
-- TABELA SERIERENDAFIXA Fim


--Tabela PerfilMTM INICIO
CREATE TABLE PerfilMTM 
( 	IdPerfilMTM 					INT 	PRIMARY KEY identity NOT NULL, 
	DtVigencia						DateTime			NOT NULL, 
	IdPapel							INT 				NULL, 
	IdSerie							INT 				NULL, 
	IdTitulo 						INT 				NULL, 	
	IdOperacao 						INT 				NULL
) 
GO

alter table PerfilMTM add constraint PerfilMTM_Papel_FK FOREIGN KEY ( IdPapel ) references PapelRendaFixa(IdPapel);
GO
alter table PerfilMTM add constraint PerfilMTM_Serie_FK FOREIGN KEY ( IdSerie ) references SerieRendaFixa(IdSerie);
GO
alter table PerfilMTM add constraint PerfilMTM_Titulo_FK FOREIGN KEY ( IdTitulo ) references TituloRendaFixa(IdTitulo);
GO
alter table PerfilMTM add constraint PerfilMTM_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao)
GO

create table DataAux
(
	idDataAux 					INT 	PRIMARY KEY identity NOT NULL, 
	dataAtual						DateTime			NOT NULL, 
)
GO

insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/11/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '31/12/2013', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '31/01/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/02/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '31/03/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/04/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/05/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/06/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '31/07/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '28/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/08/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '04/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '05/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '11/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '12/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '18/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '19/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '25/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '26/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '29/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '30/09/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '01/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '02/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '03/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '06/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '07/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '08/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '09/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '10/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '13/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '14/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '15/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '16/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '17/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '20/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '21/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '22/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '23/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '24/10/2014', 103));
go 
insert into dataAux ( dataAtual ) values (Convert(DateTime, '27/10/2014', 103));
go 


Insert into PerfilMTM ( DtVigencia, IdPapel, IdTitulo, IdOperacao,IdSerie)
select distinct dataRef.dataAtual,
    titulo.IdPapel,
    titulo.IdTitulo,
     null,
    titulo.idSerie  
From TituloRendaFixa titulo, dataAux dataRef
where titulo.idSerie is not null;
GO

drop table DataAux;
GO

delete from PermissaoMenu where idMEnu = 3960;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3960
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

IF OBJECT_ID('SerieRendaFixa_TituloRendaFixa_FK1') Is Not Null
Begin
 ALTER TABLE TituloRendaFixa 
 DROP CONSTRAINT SerieRendaFixa_TituloRendaFixa_FK1;
End

ALTER TABLE TituloRendaFixa Drop Column IdSerie;
GO
--Tabela PErfilMTM FIM

--Tabela MTMManual INICIO
CREATE TABLE MTMManual 
( 	
	IdOperacao						INT 				NOT NULL,
	DtVigencia						DateTime 				NOT NULL, 
	Taxa252 						Decimal(25,12)		NULL, 
	PuMTM							Decimal(25,12)		NULL	
	CONSTRAINT PK_MTMManual 		primary key(DtVigencia, IdOperacao)	
) 
GO

delete from PermissaoMenu where idMEnu = 3940;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3940
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
--Tabela MTMManual FIM

--Tabela TaxaSerie INICIO
CREATE TABLE TaxaSerie 
( 	
	IdSerie							INT 				NOT NULL,
	Data							DateTime 			NOT NULL, 
	Taxa	 						Decimal(25,12)		NOT NULL,  
	DigitadoImportado 				INT					NOT NULL	
	CONSTRAINT PK_TaxaSerie 		primary key(Data, IdSerie)	
) 
GO

delete from PermissaoMenu where idMEnu = 3770;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3770
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
--Tabela TaxaSerie Fim

-- Tabela CotacaoSerie INICIO
ALTER TABLE CotacaoSerie ADD DigitadoImportado INT NOT NULL default '2';
GO
-- Tabela CotacaoSerie FIM

-- Tabela VigenciaCategoria INICIO
CREATE TABLE VigenciaCategoria 
( 	
	IdVigenciaCategoria				 INT 	PRIMARY KEY identity NOT NULL, 
	DataVigencia					 DateTime 			NOT NULL, 
	IdOperacao						 INT 				NOT NULL, 
	TipoVigenciaCategoria 			 INT				NOT NULL	
) 
GO

delete from PermissaoMenu where idMEnu = 3980;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3980
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
-- Tabela VigenciaCategoria FIM

-- Tabela Cliente INICIO
ALTER TABLE Carteira ADD CalculaMTM INT NULL;
GO
-- Tabela Cliente FIM

--Tabela TaxaCurva INICIO
CREATE TABLE TaxaCurva 
( 	
	IdCurvaRendaFixa				INT 				NOT NULL,
	"DataBase"						DateTime 			NOT NULL, 
	DataVertice						DateTime 			NOT NULL, 
	CodigoVertice					VARCHAR(100) 		NULL, 
	PrazoDU							INT 				NOT NULL, 
	PrazoDC							INT 				NOT NULL, 
	Taxa	 						Decimal(25,12)		NOT NULL	
	CONSTRAINT PK_TaxaCurva 		primary key("DataBase", IdCurvaRendaFixa,DataVertice)	
) 
GO

alter table TaxaCurva add constraint TaxaCurva_CurvaRF_FK FOREIGN KEY ( IdCurvaRendaFixa ) references CurvaRendaFixa(IdCurvaRendaFixa);
GO

delete from PermissaoMenu where idMEnu = 3970;
GO
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3970
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
--Tabela TaxaCurva Fim

--Tabela Indice INICIO
ALTER TABLE Indice ADD IdFeeder INT not NULL default '5';
GO
alter table Indice add constraint Indice_Feeder_FK FOREIGN KEY ( IdFeeder ) references Feeder(IdFeeder);
GO
--Tabela Indice Fim

--Tabela MemoriaCalculoRendaFixa INICIO
CREATE TABLE MemoriaCalculoRendaFixa
(
	DataAtual 		DateTime		NOT NULL,
	IdOperacao		INT 			NOT NULL,
	TipoPreco 		INT 			NOT NULL,
	DataFluxo		DateTime 		NOT NULL,
	ValorFluxo		Decimal(25,8)	NOT NULL,
	ValorPresente	Decimal(25,8)	NOT NULL,	
	Taxa	 		Decimal(25,12)	NOT NULL,	
	NumeroDias		INT				NOT NULL
	CONSTRAINT PK_MemoriaCalculoRendaFixa 		primary key(DataAtual, IdOperacao, TipoPreco, DataFluxo)	
)
GO

alter table MemoriaCalculoRendaFixa add constraint MemCalRF_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao);
GO
--Tabela MemoriaCalculoRendaFixa FIM

--Tabela Local Custodia Inicio
delete from PermissaoMenu where idMEnu = 390;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,390
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

CREATE TABLE [dbo].[LocalCustodia]
(
 [IdLocalCustodia] [int] PRIMARY KEY  IDENTITY(1,1) NOT NULL,
 [Descricao] [varchar](60) NOT NULL,
 [Codigo] [varchar](250) NOT NULL,
 [Cnpj] [varchar](250) NOT NULL
)
GO

ALTER TABLE [dbo].[LocalCustodia] ADD  DEFAULT ('') FOR [Codigo]
GO
ALTER TABLE [dbo].[LocalCustodia] ADD  DEFAULT ('') FOR [Cnpj]
GO

--Tabela Local Custodia Fim


--Tabela PosicaoRendaFixa Inicio
ALTER TABLE PosicaoRendaFixa ADD ValorCurvaVencimento Decimal(25,2) NULL default '0';
ALTER TABLE PosicaoRendaFixa ADD PUCurvaVencimento Decimal(25,8) NULL default '0';
ALTER TABLE PosicaoRendaFixa ADD AjusteMTM Decimal(25,8) NULL default '0';
ALTER TABLE PosicaoRendaFixa ADD AjusteVencimento Decimal(25,8) NULL default '0';
ALTER TABLE PosicaoRendaFixa ADD TaxaMTM Decimal(25,8) NULL default '0';
--Tabela PosicaoRendaFixa Fim

--Tabela PosicaoRendaFixaAbertura Inicio
ALTER TABLE PosicaoRendaFixaAbertura ADD ValorCurvaVencimento Decimal(25,2) NULL default '0';
ALTER TABLE PosicaoRendaFixaAbertura ADD PUCurvaVencimento Decimal(25,8) NULL default '0';
ALTER TABLE PosicaoRendaFixaAbertura ADD AjusteMTM Decimal(25,8) NULL default '0';
ALTER TABLE PosicaoRendaFixaAbertura ADD AjusteVencimento Decimal(25,8) NULL default '0';
ALTER TABLE PosicaoRendaFixaAbertura ADD TaxaMTM Decimal(25,8) NULL default '0';
--Tabela PosicaoRendaFixaAbertura Fim

--Tabela PosicaoRendaFixaHistorico Inicio
ALTER TABLE PosicaoRendaFixaHistorico ADD ValorCurvaVencimento Decimal(25,2) NULL default '0';
ALTER TABLE PosicaoRendaFixaHistorico ADD PUCurvaVencimento Decimal(25,8) NULL default '0';
ALTER TABLE PosicaoRendaFixaHistorico ADD AjusteMTM Decimal(25,8) NULL default '0';
ALTER TABLE PosicaoRendaFixaHistorico ADD AjusteVencimento Decimal(25,8) NULL default '0';
ALTER TABLE PosicaoRendaFixaHistorico ADD TaxaMTM Decimal(25,8) NULL default '0';
--Tabela PosicaoRendaFixaHistorico Fim


--Tabela TipoDePara Inicio
CREATE TABLE TipoDePara 
( 	IdTipoDePara 					INT 				PRIMARY KEY identity NOT NULL, 
	TipoCampo						INT 				NOT NULL, 
	Descricao 						VARCHAR(100) 		NOT NULL, 
	Observacao 						VARCHAR(255) 		NULL,
	EnumTipoDePara					INT 				NULL	
) 
GO
--Tabela TipoDePara Fim

--Tabela DePara Inicio
CREATE TABLE DePara 
( 	IdDePara	 					INT 				PRIMARY KEY identity NOT NULL, 
	IdTipoDePara 					INT 				NOT NULL, 
	CodigoInterno	 				VARCHAR(100) 		NOT NULL, 
	CodigoExterno					VARCHAR(100) 		NOT NULL	
) 
GO
alter table DePara add constraint DePara_TipoDePara_FK FOREIGN KEY ( IdTipoDePara ) references TipoDePara(IdTipoDePara)

delete from PermissaoMenu where idMEnu = 14260;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14260
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14200;
GO

delete from PermissaoMenu where idMEnu = 14261;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14261
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMEnu = 14262;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14262
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO
--Tabela DePara Fim


--Tabela TítuloRendaFixa Inicio (pt 2)
ALTER TABLE TituloRendaFixa
ADD PremioRebate decimal(25,8)

ALTER TABLE TituloRendaFixa
ADD ExpTaxaEmissao int

ALTER TABLE TituloRendaFixa
ADD Periodicidade int

ALTER TABLE TituloRendaFixa
ADD CriterioAmortizacao INT NOT NULL default '2';
--Tabela TituloRendaFixa Fim (pt 2)

--Tabela MemoriaCalculoRendaFixa INICIO
IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'MemoriaCalculoRendaFixa') > 0
Begin
	drop TABLE MemoriaCalculoRendaFixa 
End

CREATE TABLE MemoriaCalculoRendaFixa
(
	DataAtual 		 		 DateTime		NOT NULL,
	IdOperacao		 		 INT 			NOT NULL,
	TipoPreco 		 		 INT 			NOT NULL,
	TipoProcessamento		 INT 			NOT NULL,
	DataFluxo		 		 DateTime 		NOT NULL,
	TaxaDesconto	 		 Decimal(25,12)	NULL,
	ValorVencimento	 		 Decimal(25,8)	NULL,
	ValorPresente	 		 Decimal(25,8)	NULL,		
	ValorNominalAjustado	 Decimal(25,8)	NULL,
	ValorNominal			 Decimal(25,8)	NULL
	CONSTRAINT PK_MemoriaCalculoRendaFixa 		primary key(DataAtual, IdOperacao, TipoPreco, TipoProcessamento, DataFluxo)	
)
alter table MemoriaCalculoRendaFixa add constraint MemCalRF_Operacao_FK FOREIGN KEY ( IdOperacao ) references OperacaoRendaFixa(IdOperacao);

delete from PermissaoMenu where idMEnu = 3990;
insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,3990
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

--Tabela MemoriaCalculoRendaFixa FIM
