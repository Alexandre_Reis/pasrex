﻿declare @data_versao char(10)
set @data_versao = '2014-05-20'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

if not exists(select valortexto from configuracao where id = 5012)
begin
insert into configuracao (id, descricao, valornumerico, valortexto) 
values(5012, 'Calcula Fator Índice', null, 'N')
end



ALTER TABLE PessoaDocumento DROP CONSTRAINT FK_PessoaDocumento_Pessoa
GO
ALTER TABLE PessoaDocumento DROP CONSTRAINT FK_PessoaDocumento_PessoaDocumento
GO
ALTER TABLE PessoaDocumento ADD CONSTRAINT FK_PessoaDocumento_Pessoa FOREIGN KEY(IdPessoa) 
REFERENCES Pessoa(IdPessoa) ON UPDATE  NO ACTION  ON DELETE  CASCADE 	