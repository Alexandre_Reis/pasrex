declare @data_versao char(10)
set @data_versao = '2013-09-10'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

CREATE TABLE GrupoAfinidade(
	IdGrupo int IDENTITY(1,1) NOT NULL,
	Descricao varchar(200) NOT NULL,
 CONSTRAINT GrupoAfinidade_PK PRIMARY KEY CLUSTERED 
(
	IdGrupo ASC
)
)
 
 alter table Cliente add IdGrupoAfinidade int null
 go
 
 ALTER TABLE Cliente  WITH CHECK ADD  CONSTRAINT GrupoAfinidade_Cliente_FK1 FOREIGN KEY(IdGrupoAfinidade)
 REFERENCES GrupoAfinidade (IdGrupo)
 ON DELETE CASCADE
 go
  
 alter table ClienteRendaFixa add IdAssessor int null
 go
 
 ALTER TABLE ClienteRendaFixa WITH CHECK ADD  CONSTRAINT Assessor_ClienteRendaFixa_FK1 FOREIGN KEY(IdAssessor)
 REFERENCES Assessor (IdAssessor)
 ON DELETE CASCADE
 go
 
 alter table ClienteRendaFixa add CodigoCetip varchar(14) null
 go
 alter table TituloRendaFixa add CodigoCetip varchar(14) null
 go
 alter table TituloRendaFixa add CodigoCBLC varchar(10) null
 go
 
 --Menu de Grupo de Afinidade
 INSERT INTO [dbo].[permissaomenu] ([IdGrupo],[IdMenu],[PermissaoLeitura],[PermissaoAlteracao],[PermissaoExclusao],[PermissaoInclusao])VALUES(1,470,'S','S','S','S')