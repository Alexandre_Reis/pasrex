declare @data_versao char(10)
set @data_versao = '2013-06-12'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table posicaorendafixa add IdAgente int null
go
alter table posicaorendafixahistorico add IdAgente int null
go
alter table posicaorendafixaabertura add IdAgente int null
go

alter table operacaorendafixa add IdAgenteCorretora int null
go
alter table ordemrendafixa add IdAgenteCorretora int null
go

alter table posicaorendafixa add IdCustodia tinyint null
go
alter table posicaorendafixahistorico add IdCustodia tinyint null
go
alter table posicaorendafixaabertura add IdCustodia tinyint null
go

update posicaorendafixa set IdCustodia = 0
go
update posicaorendafixahistorico set IdCustodia = 0
go
update posicaorendafixaabertura set IdCustodia = 0
go

alter table posicaorendafixa alter column IdCustodia tinyint not null
go
alter table posicaorendafixahistorico alter column IdCustodia tinyint not null
go
alter table posicaorendafixaabertura alter column IdCustodia tinyint not null
go

ALTER TABLE PosicaoRendaFixa ADD  CONSTRAINT DF_PosicaoRendaFixa_IdCustodia DEFAULT ((0)) FOR IdCustodia
GO

ALTER TABLE PosicaoRendaFixaHistorico ADD  CONSTRAINT DF_PosicaoRendaFixaHistorico_IdCustodia DEFAULT ((0)) FOR IdCustodia
GO

ALTER TABLE PosicaoRendaFixaAbertura ADD  CONSTRAINT DF_PosicaoRendaFixaAbertura_IdCustodia DEFAULT ((0)) FOR IdCustodia
GO

alter table operacaorendafixa add IdCustodia tinyint null
go
alter table ordemrendafixa add IdCustodia tinyint null
go
update operacaorendafixa set IdCustodia = localcustodia
go
update ordemrendafixa set IdCustodia = localcustodia
go
alter table operacaorendafixa alter column IdCustodia tinyint not null
go
alter table ordemrendafixa alter column IdCustodia tinyint not null
go


alter table operacaorendafixa add IdLiquidacao tinyint null
go
alter table ordemrendafixa add IdLiquidacao tinyint null
go
update operacaorendafixa set IdLiquidacao = idformaliquidacao
go
update ordemrendafixa set IdLiquidacao = idformaliquidacao
go
alter table operacaorendafixa alter column IdLiquidacao tinyint not null
go
alter table ordemrendafixa alter column IdLiquidacao tinyint not null
go

alter table operacaorendafixa drop column idformaliquidacao
go
alter table ordemrendafixa drop column idformaliquidacao
go

alter table operacaorendafixa drop column localcustodia
go
alter table ordemrendafixa drop column localcustodia
go