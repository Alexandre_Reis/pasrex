declare @data_versao char(10)
set @data_versao = '2014-09-11'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

delete from PermissaoMenu where idMenu = 22290;
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,22290
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO

delete from PermissaoMenu where idMenu = 15250;

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,15250
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 22280;
GO  