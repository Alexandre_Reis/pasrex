declare @data_versao char(10)
set @data_versao = '2014-03-10'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)


INSERT INTO pessoaEmail ( idPessoa, email )
select idpessoa, email from pessoaendereco where email != null or email != ''
order by idpessoa, recebecorrespondencia desc 

go

INSERT INTO pessoatelefone ( idPessoa, DDI, DDD, Numero, Ramal, tipotelefone )
select idpessoa, '', '', fone, '', 1 from pessoaendereco where fone != null or fone != ''
order by idpessoa, recebecorrespondencia desc

go

alter table PessoaEndereco drop column Fone
go
alter table PessoaEndereco drop column Email
go

