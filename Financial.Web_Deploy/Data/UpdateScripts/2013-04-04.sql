declare @data_versao char(10)
set @data_versao = '2013-04-04'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table cliente add Aux int null
go
update cliente set Aux = 1 where Grossup = 'N'
go
update cliente set Aux = 2 where Grossup = 'S'
go
update cliente set Grossup = Aux
go
alter table cliente alter column Grossup tinyint not null
go
alter table cliente drop column Aux
go


