declare @data_versao char(10)
set @data_versao = '2013-04-11'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table TabelaCarteiraSimulada add DataFim datetime null
go
alter table TabelaCarteiraSimulada add IdIndice smallint null
go
alter table TabelaCarteiraSimulada add Taxa decimal (12,8)
go
