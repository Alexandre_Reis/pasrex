declare @data_versao char(10)
set @data_versao = '2013-10-16'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaorendafixa add ValorISS decimal (16,2) null
go
update operacaorendafixa set ValorISS = 0
go
alter table operacaorendafixa alter column ValorISS decimal (16,2) not null
go

ALTER TABLE operacaorendafixa ADD  CONSTRAINT DF_OperacaoRendaFixa_ValorISS  DEFAULT ((0)) FOR ValorISS
GO


