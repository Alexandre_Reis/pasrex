declare @data_versao char(10)
set @data_versao = '2014-12-12'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table liquidacaorendafixa add IdOperacaoVenda int null
go

alter table liquidacaorendafixa alter column Quantidade decimal (25, 12) not null
go