declare @data_versao char(10)
set @data_versao = '2013-10-24'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table agentemercado add Apelido varchar(20) null
go

alter table operacaorendafixa add CodigoContraParte varchar(10) null
go

CREATE TABLE GrauRisco(
	IdGrauRisco int IDENTITY(1,1) NOT NULL,
	Descricao varchar(50) NOT NULL,
 CONSTRAINT GrauRisco_PK PRIMARY KEY CLUSTERED 
(
	IdGrauRisco ASC
)
)

alter table carteira add IdGrauRisco int null
go

ALTER TABLE Carteira  WITH CHECK ADD  CONSTRAINT GrauRisco_Carteira_FK1 FOREIGN KEY(IdGrauRisco)
REFERENCES GrauRisco (IdGrauRisco)
ON DELETE SET NULL
GO


