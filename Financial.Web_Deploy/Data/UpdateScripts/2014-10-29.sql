declare @data_versao char(10)
set @data_versao = '2014-10-29'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

--Criar processo de cadastro Travamento de Cotas
delete from PermissaoMenu where idMenu = 14640;
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14640
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14630
GO

--Cria novo item de menu Consulta Ajuste de Cotas
delete from PermissaoMenu where idMenu = 14650;
GO

insert into [dbo].[PermissaoMenu]
SELECT [IdGrupo]
      ,14650
      ,[PermissaoLeitura]
      ,[PermissaoAlteracao]
      ,[PermissaoExclusao]
      ,[PermissaoInclusao]
  FROM [dbo].[PermissaoMenu]
  where IdMenu = 14640
GO

CREATE TABLE TravamentoCotas(
	IdTravamentoCota int IDENTITY(1,1) NOT NULL,
	IdCarteira int NOT NULL,
	DataProcessamento datetime NOT NULL,
	TipoBloqueio int NOT NULL,
	ValorCota decimal (28,12) NOT NULL,
	ValorCotaCalculada decimal (28,12) NOT NULL,
	AjusteCompensacao decimal (16,2) NULL,
 CONSTRAINT TravamentoCotas_PK PRIMARY KEY CLUSTERED 
(
	IdTravamentoCota ASC
)
)
GO

ALTER TABLE TravamentoCotas  WITH CHECK ADD  CONSTRAINT Carteira_TravamentoCotas_FK1 FOREIGN KEY(IdCarteira)
REFERENCES Carteira (IdCarteira)
GO


