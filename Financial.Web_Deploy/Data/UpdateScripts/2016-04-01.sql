declare @data_versao char(10)
set @data_versao = '2016-04-01'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  n�o permite que seja executado novamente, avisa o usuario e 
*  n�o exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Aten��o este script ja foi executado anteriormente.',16,1)

	
	-- n�o mostra a execu��o do script
	set nocount on 
	
	-- n�o roda o script
	set noexec on 
	

end

begin transaction

	-- n�o permite que a transaction seja commitada em caso de erros
	set xact_abort on	
	
	insert into VersaoSchema values(@data_versao)

	IF (SELECT count(*) FROM SYSOBJECTS WHERE XTYPE = 'U' AND NAME = 'AgendaComeCotas') = 0
	begin
		CREATE TABLE [dbo].[AgendaComeCotas](
			[IdAgendamentoComeCotas] [int] IDENTITY(1,1) NOT NULL,
			[IdCarteira] [int] NOT NULL,
			[DataLancamento] [datetime] NOT NULL,
			[DataVencimento] [datetime] NOT NULL,
			[TipoEvento] [int] NOT NULL,
			[IdPosicao] [int] NOT NULL,
			[DataUltimoIR] [datetime] NOT NULL,
			[Quantidade] [decimal](28, 12) NOT NULL,
			[QuantidadeAntesCortes] [decimal](28, 12) NOT NULL,
			[ValorCotaAplic] [decimal](28, 12) NOT NULL,
			[ValorCotaUltimoPagamentoIR] [decimal](28, 12) NOT NULL,
			[ValorCota] [decimal](28, 12) NOT NULL,
			[AliquotaCC] [decimal](5, 2) NOT NULL,
			[RendimentoBrutoDesdeAplicacao] [decimal](16, 2) NOT NULL,
			[RendimentoDesdeUltimoPagamentoIR] [decimal](16, 2) NOT NULL,
			[NumDiasCorridosDesdeAquisicao] [int] NOT NULL,
			[PrazoIOF] [int] NOT NULL,
			[AliquotaIOF] [decimal](5, 2) NOT NULL,
			[ValorIOF] [decimal](28, 12) NOT NULL,
			[ValorIOFVirtual] [decimal](28, 12) NOT NULL,
			[PrejuizoUsado] [decimal](16, 2) NOT NULL,
			[RendimentoCompensado] [decimal](16, 2) NOT NULL,
			[ValorIRAgendado] [decimal](28, 12) NOT NULL,
			[ValorIRPago] [decimal](16, 2) NOT NULL,
			[Residuo15] [decimal](28, 12) NOT NULL,
			[Residuo175] [decimal](28, 12) NOT NULL,
			[Residuo20] [decimal](28, 12) NOT NULL,
			[Residuo225] [decimal](28, 12) NOT NULL,
			[QuantidadeComida] [decimal](28, 12) NOT NULL,
			[QuantidadeFinal] [decimal](28, 12) NOT NULL,
			[ExecucaoRecolhimento] [int] NOT NULL,
			[TipoAliquotaIR] [int] NOT NULL,
			[AliquotaIR] [int] NOT NULL,
			[TipoPosicao] [int] NULL,
		PRIMARY KEY CLUSTERED 
		(
			[IdAgendamentoComeCotas] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
		
		ALTER TABLE [dbo].[AgendaComeCotas] ADD  DEFAULT ((0)) FOR [AliquotaIR]
		
		ALTER TABLE [dbo].[AgendaComeCotas]  WITH CHECK ADD FOREIGN KEY([IdCarteira]) REFERENCES [dbo].[Carteira] ([IdCarteira])
		
		CREATE NONCLUSTERED INDEX [AgendaComeCotas_IdCarteira] ON [dbo].AgendaComeCotas([IdCarteira] DESC)
	END	

-- commita a transaction e volta as variaveis do sql server para o estado inicial
commit transaction
set noexec off
set nocount off