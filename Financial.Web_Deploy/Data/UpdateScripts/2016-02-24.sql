﻿declare @data_versao char(10)
set @data_versao = '2016-02-24'

/* 
*  caso este script ja tenha sido executado anteriormente 
*  não permite que seja executado novamente, avisa o usuario e 
*  não exibe nenhuma outra saida
*/
if exists(select * from VersaoSchema where DataVersao = @data_versao)
begin
	
	-- avisa o usuario em caso de erro
	raiserror('Atenзгo este script ja foi executado anteriormente.',16,1)

	
	-- nгo mostra a execuзгo do script
	set nocount on 
	
	-- nгo roda o script
	set noexec on 
	

end

/****** Criação dos campos de cadastro complementar  ******/

begin transaction 

 -- nгo permite que a transaction seja commitada em caso de erros
    set xact_abort on
    
    insert into VersaoSchema values(@data_versao)

DECLARE @idCadastro INT;
DECLARE @maxChar INT;
DECLARE @NomeCampoChar varchar(255);
DECLARE @ValorCampoChar varchar(500)
DECLARE @DescricaoCampoChar varchar(255);

SET @NomeCampoChar = 'Objetivo';
SET @DescricaoCampoChar = 'Objetivo do Fundo';
SET @ValorCampoChar = 'Proporcionar rentabilidade aos seus cotistas através de oportunidades oferecidas pelos mercados domésticos de taxas de juros pós-fixadas, prefixadas e de índices de preços.'
SET @maxChar = 175;
if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar) = 0
begin

	begin transaction;
	
 -- nгo permite que a transaction seja commitada em caso de erros
    set xact_abort on;

	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
	VALUES(-8, @NomeCampoChar, @DescricaoCampoChar, null, -2, 'N', @maxChar, 0)

	SET @idCadastro = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar)

	--Inserir carteira
	INSERT INTO CadastroComplementar (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
	SELECT 	@idCadastro,
			IdCarteira,
			LTRIM(RTRIM(SUBSTRING(cart.Apelido, 1, 50))) ,		   		   
			LTRIM(RTRIM(SUBSTRING(@ValorCampoChar, 1, @maxChar))) 
    FROM Carteira cart;

	commit transaction;
END

SET @NomeCampoChar = 'Publico Alvo';
SET @DescricaoCampoChar = 'Define o público alvo do fundo';
SET @ValorCampoChar = 'Destina-se a receber aplicações de recursos provenientes de investidores pessoas físicas e jurídicas em geral, com perfil de risco moderado.'
SET @maxChar = 175;
if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar) = 0
begin

	begin transaction;
	
 -- nгo permite que a transaction seja commitada em caso de erros
    set xact_abort on;

	if exists(select 1 from syscolumns where id = object_id('CadastroComplementar') and name = 'ValorCampo')
	begin
		ALTER TABLE CadastroComplementar ALTER COLUMN ValorCampo VARCHAR(500) NOT NULL
	END

	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
	VALUES(-8, @NomeCampoChar, @DescricaoCampoChar, null, -2, 'N', @maxChar, 0)

	SET @idCadastro = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar)

	--Inserir carteira
	INSERT INTO CadastroComplementar (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
	SELECT 	@idCadastro,
			IdCarteira,
			LTRIM(RTRIM(SUBSTRING(cart.Apelido, 1, 50))) ,		   		   
			LTRIM(RTRIM(SUBSTRING(@ValorCampoChar, 1, @maxChar))) 
    FROM Carteira cart;

	commit transaction;
END

SET @NomeCampoChar = 'Politica de Investimento';
SET @DescricaoCampoChar = 'Define a Política de Investimento do Fundo';
SET @ValorCampoChar = 'O fundo pretende atingir seu objetivo investindo no mínimo 80% de seu patrimônio em títulos públicos e/ou privados de renda fixa pré ou pós fixadas de médio e longo prazo, sendo permitido o uso de derivativos а taxa de juros doméstica e/ou índices de preços, com objetivo exclusivo de proteger a carteira do fundo.';
SET @maxChar = 500;
if (select count(1) from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar) = 0
begin

	begin transaction;
	
 -- nгo permite que a transaction seja commitada em caso de erros
    set xact_abort on;

	INSERT INTO CadastroComplementarCampos (TipoCadastro, NomeCampo, DescricaoCampo, ValorDefault, TipoCampo, CampoObrigatorio, Tamanho, CasasDecimais)
	VALUES(-8, @NomeCampoChar, @DescricaoCampoChar, null, -2, 'N', @maxChar, 0)

	SET @idCadastro = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = @NomeCampoChar)

	--Inserir carteira
	INSERT INTO CadastroComplementar (IdCamposComplementares, IdMercadoTipoPessoa, DescricaoMercadoTipoPessoa, ValorCampo) 
	SELECT 	@idCadastro,
			IdCarteira,
			LTRIM(RTRIM(SUBSTRING(cart.Apelido, 1, 50))) ,		   		   
			LTRIM(RTRIM(SUBSTRING(@ValorCampoChar, 1, @maxChar))) 
    FROM Carteira cart;

	commit transaction;
END

---- Exemplos de consulta--------------------------

select ValorCampo
   from CadastroComplementar 
   where IdCamposComplementares = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'Objetivo') 
         and CadastroComplementar.IdMercadoTipoPessoa = '5114'

select ValorCampo from CadastroComplementar where
   IdCamposComplementares = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'Publico Alvo')
            and CadastroComplementar.IdMercadoTipoPessoa = '5114'


select ValorCampo from CadastroComplementar where
   IdCamposComplementares = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'Politica de Investimento')
            and CadastroComplementar.IdMercadoTipoPessoa = '5114'


---- Exclusгo das informaзхes--------------------------

--delete CadastroComplementar 
--where IdCamposComplementares = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 
--      and NomeCampo = 'Objetivo');
--delete CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'Objetivo';

--delete CadastroComplementar 
--where IdCamposComplementares = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 
--      and NomeCampo = 'Publico Alvo');
--delete CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'Publico Alvo';

--delete CadastroComplementar 
--where IdCamposComplementares = (select IdCamposComplementares  from CadastroComplementarCampos where TipoCadastro = -8 
--      and NomeCampo = 'Politica de Investimento');
--delete CadastroComplementarCampos where TipoCadastro = -8 and NomeCampo = 'Politica de Investimento';




commit transaction
go
