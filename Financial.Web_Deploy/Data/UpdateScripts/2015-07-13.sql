declare @data_versao char(10)
set @data_versao = '2015-07-13'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
BEGIN TRANSACTION
IF EXISTS(select 1 from syscolumns where id = object_id('CadastroComplementarCampos') and name = 'DescricaoCampo')
BEGIN
	alter table CadastroComplementarCampos alter column DescricaoCampo varchar(160);
END
GO	
COMMIT TRANSACTION

