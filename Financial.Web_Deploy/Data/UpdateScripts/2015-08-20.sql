declare @data_versao char(10)
set @data_versao = '2015-08-20'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

update titulorendafixa set descricao = 'NTN-C' where descricao = 'NTNC'