declare @data_versao char(10)
set @data_versao = '2013-12-27'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table operacaorendafixa add Observacao varchar(200) null
go
alter table operacaorendafixa add IdIndiceVolta smallint null
go

