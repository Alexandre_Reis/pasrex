declare @data_versao char(10)
set @data_versao = '2014-05-09'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table carteira add FundoExclusivo char(1) null
go
update carteira set FundoExclusivo  = 'N'
go
alter table carteira alter column FundoExclusivo char(1) not null
go

ALTER TABLE Carteira ADD  CONSTRAINT DF_Carteira_FundoExclusivo  DEFAULT (('N')) FOR FundoExclusivo
GO
