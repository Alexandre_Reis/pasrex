declare @data_versao char(10)
set @data_versao = '2013-09-20'
if exists (select 1 from VersaoSchema where DataVersao = @data_versao) return
insert into VersaoSchema VALUES(@data_versao)

alter table tabelacarteirasimulada drop column Percentual
go