﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_TabelaRebateCorretagem, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
                       
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');
                      
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                  
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de Rebate de Corretagem" />
    </div>
           
    <div id="mainContent">
                                
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
        </div>
    
        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaRebateCorretagem"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                >        
                    
            <Columns>           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                
                <dxwgv:GridViewDataTextColumn FieldName="IdCliente" Caption="Id Cliente" Width="17%" VisibleIndex="1" CellStyle-HorizontalAlign="left">
                <PropertiesTextEdit MaxLength="100" />
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Nome" Width="30%" VisibleIndex="2">
                <PropertiesTextEdit MaxLength="100" />
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataComboBoxColumn Caption="Agente Mercado" FieldName="IdAgenteCorretora" VisibleIndex="3" Width="20%">                        
                    <EditFormSettings Visible="False" />
                    <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente" />
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data" VisibleIndex="4" Width="10%"/>
                                    
                <dxwgv:GridViewDataTextColumn FieldName="PercentualRebate" Caption="% Rebate" VisibleIndex="5" Width="17%" 
                        HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                        GroupFooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                <PropertiesTextEdit DisplayFormatString="{0:N2}" />
                </dxwgv:GridViewDataTextColumn>                    
            </Columns>                
            <Templates>
            <EditForm>
            
                <div class="editForm">    
                        
                    <table>
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:" />
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" ClientInstanceName="btnEditCodigoCliente"
                                        Text='<%# Eval("IdCliente") %>' OnLoad="btnEditCodigoCliente_Load"
                                        MaxLength="10" NumberType="Integer">            
                                <Buttons>
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>  
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>                                        
                            
                            <td colspan="2">
                                <asp:TextBox ID="textNome" runat="server" CssClass="textLongo" Enabled="False" Text='<%# Eval("Apelido") %>' />
                            </td>
                        </tr>

                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelAgenteMercado" runat="server" CssClass="labelRequired" Text="Corretora:" />
                            </td>                
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropAgenteMercado" runat="server" ClientInstanceName="dropAgenteMercado"
                                                    DataSourceID="EsDSAgenteMercado" IncrementalFilteringMode="Contains"  
                                                    ShowShadow="false" DropDownStyle="DropDown"
                                                    CssClass="dropDownList" TextField="Nome" ValueField="IdAgente" 
                                                    OnLoad="dropAgenteMercado_Load" Text='<%#Eval("IdAgenteCorretora")%>'>                                                        
                                  <ClientSideEvents LostFocus="function(s, e) { 
                                                                    if(s.GetSelectedIndex() == -1)
                                                                        s.SetText(null); } "/>                  
                                </dxe:ASPxComboBox>         
                            </td>
                        </tr>  
                        
                        <tr>
                            <td class="td_Label">
                                 <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Data Ref:"  />
                            </td> 
                            <td colspan="2">
                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>' OnLoad="textDataReferencia_Load"/>                            
                            </td>                              
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelPercentualRebate" runat="server" CssClass="labelRequired" Text="% Rebate:" />
                            </td>
                            <td colspan="2">
                                <dxe:ASPxSpinEdit ID="textPercentualRebate" runat="server" ClientInstanceName="textValor" 
                                        Text='<%# Eval("PercentualRebate") %>' NumberType="Float" MaxLength="10" DecimalPlaces="2"
                                        CssClass="textValor_5">
                                </dxe:ASPxSpinEdit>
                            </td>                             
                        </tr>                            
                        
                    </table>
                    
                    <div class="linhaH"></div>    
                                            
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal4" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>
                    
                </div>
                                         
            </EditForm>
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
                       
        </dxwgv:ASPxGridView>            
        </div>
                   
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTabelaRebateCorretagem" runat="server" OnesSelect="EsDSTabelaRebateCorretagem_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
        
    </form>
</body>
</html>