﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_FormaCondominio, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    popup=true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function HideData() {
        labelDataRecolhimento.SetVisible(false);
        textDataRecolhimento.SetVisible(false);
    }
    function ShowData() {
        labelDataRecolhimento.SetVisible(true);
        textDataRecolhimento.SetVisible(true);
    }
    
    function OnGetDataCarteira(data) {
        
        //cbPanel.PerformCallback(data);
        
        if(textDataInicioVigencia.GetValue() == null)
        {   
            popupCarteira.HideWindow();
            alert('A Data Vigência deve ser preenchida');                              
        }
        else
        {   
            hiddenIdCarteira.SetValue(data);
            ASPxCallback1.SendCallback(data);
            popupCarteira.HideWindow();
            btnEditCodigoCarteira.Focus();        
            cbPanel.PerformCallback(data);
            hiddenTipoFundoAtual.SetVisible(true);
        }
    }
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCodigoCarteira.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Fundo de Investimento - Forma de Condomínio"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" OnRowUpdating="gridCadastro_RowUpdating"
                                    DataSourceID="EsDSFundoInvestimentoFormaCondominio" KeyFieldName="IdFormaCondominio"
                                    OnRowInserting="gridCadastro_RowInserting" OnCustomCallback="gridCadastro_CustomCallback"
                                    OnCancelRowEditing="gridCadastro_CancelRowEditing" OnCustomColumnDisplayText="gridCadastro_CustomColumnDisplayText">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Fundo de Investimento" FieldName="IdCarteira">
                                            <PropertiesComboBox DataSourceID="EsDSCarteira" TextField="Nome" ValueField="IdCarteira" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Forma de Condomínio" FieldName="FormaCondominio">
                                            <PropertiesComboBox>
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Aberto" />
                                                    <dxe:ListEditItem Value="2" Text="Fechado" />
                                                </Items>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Execução de Recolhimento" FieldName="ExecucaoRecolhimento"
                                            Width="20%">
                                            <PropertiesComboBox>
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Não Recolher" />
                                                    <dxe:ListEditItem Value="2" Text="Recolher no momento do evento/transformação/desenquadramento" />
                                                    <dxe:ListEditItem Value="3" Text="Agendar o Recolhimento para uma data pré-definida" />
                                                </Items>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Alíquota IR" FieldName="AliquotaIR">
                                            <PropertiesComboBox>
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Utilizar alíquota IR padrão do Come-Cota" />
                                                    <dxe:ListEditItem Value="2" Text="Utilizar alíquota IR vigente de cada cautela" />
                                                </Items>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataDateColumn FieldName="DataInicioVigencia" Caption="Data Inicio de Vigência"
                                            CellStyle-HorizontalAlign="left" Width="10%" />
                                        <dxwgv:GridViewDataDateColumn FieldName="DataRecolhimento" Caption="Data Recolhimento"
                                            CellStyle-HorizontalAlign="left" Width="10%" />
                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                            <asp:Panel ID="panelEdicao" runat="server">
                                                <fieldset>
                                                    <legend>Fundo de Investimento - Forma de Condomínio</legend>
                                                    <table border="0">
                                                        <tr>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ID="labelDataInicioVigencia" runat="server" ClientInstanceName="labelDataInicioVigencia"
                                                                    CssClass="labelRequired" Text="Data Inicio Vigência:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataInicioVigencia" runat="server" ClientInstanceName="textDataInicioVigencia"
                                                                    Value='<%#Eval("DataInicioVigencia")%>'>
                                                                </dxe:ASPxDateEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <dxe:ASPxTextBox ID="hiddenIdCarteira" runat="server" CssClass="hiddenField" Text='<%#Eval("IdCarteira")%>'
                                                                ClientInstanceName="hiddenIdCarteira" />
                                                            <td>
                                                                <asp:Label ID="labelFundoInvestimento" runat="server" CssClass="labelRequired" Text="Fundo de Investimento:"> </asp:Label>
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCarteira" ReadOnly="true"
                                                                    Width="380px" Text='<%#Eval("IdCarteira")%>' OnLoad="btnEditCarteira_Load">
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                            <%--<dxe:ASPxComboBox ID="dropFundoInvestimento" runat="server" ClientInstanceName="dropFundoInvestimento"
                                                                    Value='<%#Eval("IdCarteira")%>' ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo"
                                                                    DataSourceID="EsDSCarteira" TextField="Nome" ValueType="System.Int32" ValueField="IdCarteira">
                                                                    <ClientSideEvents ValueChanged="function(s, e) 
                                                                    {
                                                                        if(textDataInicioVigencia.GetValue() == null)
                                                                        {
                                                                            dropFundoInvestimento.SetValue(null);
                                                                            alert('A Data Vigência deve ser preenchida');                              
                                                                        }
                                                                        else
                                                                        {
                                                                            cbPanel.PerformCallback(s.GetValue());
                                                                        }
                                                                    }" />
                                                                </dxe:ASPxComboBox>--%>
                                                            <td>
                                                                <dxe:ASPxLabel ID="labelTipoFundo" ClientInstanceName="labelTipoFundo" runat="server"
                                                                    Text="Classificação Atual:">
                                                                    <ClientSideEvents Init="function(s, e) 
                                                                    {
                                                                        s.SetVisible(false);
                                                                    }" />
                                                                </dxe:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxLabel ID="labelTipoFundoAtual" ClientInstanceName="labelTipoFundoAtual"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                                <dxcp:ASPxCallbackPanel runat="server" ClientInstanceName="cbPanel" ID="cbPanel"
                                                    OnCallback="cbPanel_Callback">
                                                    <PanelCollection>
                                                        <dxp:PanelContent runat="server">
                                                            <dxe:ASPxLabel ID="hiddenTipoFundoAtual" runat="server" ClientInstanceName="hiddenTipoFundoAtual"
                                                                CssClass="hiddenField" />
                                                            <fieldset>
                                                                <legend>Padrão de Recolhimento de IR(Come-Cotas) para Eventos Corporativos, Transformações
                                                                    e Desenquadramentos de Fundos</legend>
                                                                <table>
                                                                    <tr>
                                                                        <td class="td_Label">
                                                                            <fieldset>
                                                                                <legend style="text-align: left">Forma de Condominio</legend>
                                                                                <dxe:ASPxRadioButtonList ID="rblFormaCondominio" runat="server" Value='<%#Eval("FormaCondominio")%>'
                                                                                    RepeatDirection="Horizontal" ValueType="System.Int32" ReadOnly="true">
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="1" Text="Aberto" />
                                                                                        <dxe:ListEditItem Value="2" Text="Fechado" />
                                                                                    </Items>
                                                                                </dxe:ASPxRadioButtonList>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_Label">
                                                                            <fieldset>
                                                                                <legend style="text-align: left">Execução do Recolhimento</legend>
                                                                                <dxe:ASPxRadioButtonList ID="rblExecucaoRecolhimento" runat="server" ClientInstanceName="rblExecucaoRecolhimento"
                                                                                    ValueType="System.Int32" Value='<%#Eval("ExecucaoRecolhimento")%>'>
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="1" Text="Não Recolher" />
                                                                                        <dxe:ListEditItem Value="2" Text="Recolher no momento do evento/transformação/desenquadramento" />
                                                                                        <dxe:ListEditItem Value="3" Text="Agendar o Recolhimento para uma data pré-definida" />
                                                                                    </Items>
                                                                                    <ClientSideEvents Init="function(s, e) 
                                                                                    {
                                                                                        if(rblExecucaoRecolhimento.GetSelectedItem() != null){
                                                                                            if(rblExecucaoRecolhimento.GetSelectedItem().value != '3')
                                                                                            { 
                                                                                                HideData();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                ShowData();
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            HideData();
                                                                                        }
                                                                                    }" />
                                                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                                    {
                                                                                        if(rblExecucaoRecolhimento.GetSelectedItem().value == '3')
                                                                                        {
                                                                                            ShowData();
                                                                                        } 
                                                                                        else 
                                                                                        {
                                                                                            HideData();
                                                                                            textDataRecolhimento.SetDate(null);
                                                                                        }
                                                                                    }" />
                                                                                </dxe:ASPxRadioButtonList>
                                                                            </fieldset>
                                                                        </td>
                                                                        <td class="td_Label">
                                                                            <fieldset>
                                                                                <legend style="text-align: left">Alíquota IR</legend>
                                                                                <dxe:ASPxRadioButtonList ID="rblAliquotaIR" ClientInstanceName="rblAliquotaIR" runat="server"
                                                                                    Value='<%#Eval("AliquotaIR")%>' ValueType="System.Int32">
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Value="1" Text="Utilizar alíquota IR padrão do Come-Cota" />
                                                                                        <dxe:ListEditItem Value="2" Text="Utilizar alíquota IR vigente de cada cautela" />
                                                                                    </Items>
                                                                                </dxe:ASPxRadioButtonList>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_Label">
                                                                            <dxe:ASPxLabel ID="labelDataRecolhimento" runat="server" ClientInstanceName="labelDataRecolhimento"
                                                                                CssClass="labelNormal" Text="Data Recolhimento:" />
                                                                            <dxe:ASPxDateEdit ID="textDataRecolhimento" runat="server" ClientInstanceName="textDataRecolhimento"
                                                                                Value='<%#Eval("DataRecolhimento")%>'>
                                                                            </dxe:ASPxDateEdit>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </dxp:PanelContent>
                                                    </PanelCollection>
                                                    <ClientSideEvents EndCallback="function(s, e) 
                                                    {
                                                        labelTipoFundoAtual.SetText(hiddenTipoFundoAtual.GetValue());
                                                        labelTipoFundo.SetVisible(true);
                                                        labelTipoFundoAtual.SetVisible(true);
                                                    }" />
                                                </dxcp:ASPxCallbackPanel>
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                        </div>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                        OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                        <asp:Literal ID="Literal11" runat="server" Text="Cancelar" /><div>
                                                        </div>
                                                    </asp:LinkButton>
                                                </div>
                                            </asp:Panel>
                                        </EditForm>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="40%" />
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSFundoInvestimentoFormaCondominio" runat="server" OnesSelect="EsDSFundoInvestimentoFormaCondominio_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>
