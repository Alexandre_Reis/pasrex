﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_CotaFundo, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '' && e.result != 'IdItau')
            {                   
                alert(e.result);
            }
            else
            {
                if (e.result == 'IdItau')
                {
                    if (confirm('Cota com variação acima de 2%, deseja continuar?')==false) 
                    { 
                        operacao = '';
                        return false;
                    }
                }                
                
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackImportarCotas" runat="server" OnCallback="callbackImportarCotas_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            LoadingPanel.Hide();
            if (e.result != '') {            
                alert(e.result);                              
            }else{
                popupImportarCotas.Hide();
                alert('Cotas importadas com sucesso!');
                gridCadastro.PerformCallback('btnRefresh');
            }
        }
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Cotas de Fundos"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupImportarCotas" AllowDragging="true" PopupElementID="popupImportarCotas"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="350" Left="350" Top="50" HeaderText="Importar Histórico de Cotas"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table border="0" width="350">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelFundo" runat="server" CssClass="labelNormal" Text="Fundo:" />
                                                        <dxe:ASPxComboBox ClientInstanceName="dropCarteiraImportarCotas" ID="dropCarteiraImportarCotas"
                                                            runat="server" DropDownStyle="DropDown" IncrementalFilteringMode="StartsWith"
                                                            DataSourceID="EsDSCarteira" ValueField="IdCarteira" TextField="Apelido" CssClass="dropDownListLongo"
                                                            Width="338px">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <dxe:ASPxCheckBox ID="checkTodasCotas" ClientInstanceName="checkTodasCotas" runat="server"
                                                            Text="Todos os Fundos">
                                                            <ClientSideEvents CheckedChanged="function(s, e) {if(checkTodasCotas.GetChecked())
                                                                                                 {dropCarteiraImportarCotas.SetEnabled(false);
                                                                                                  dropCarteiraImportarCotas.SetSelectedIndex(-1);}
                                                                                                else
                                                                                                 {dropCarteiraImportarCotas.SetEnabled(true);}
                                                                                                    }" />
                                                        </dxe:ASPxCheckBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="margin-top: 10px;"></div>
                                                        <asp:Label ID="labelDataInicioImportarCotas" runat="server" CssClass="labelNormal" Text="Data de Início para Atualização: <div style='margin: 3px 0 3px 0; color:#999'>* Atenção: preencher apenas se desejar corrigir cotas já importadas</div>"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="DataInicioImportarCotas" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnRunImportarCotas" runat="server" Font-Overline="false" CssClass="btnRun"
                                                    OnClientClick="
                                    {
                                        if(!checkTodasCotas.GetChecked() && dropCarteiraImportarCotas.GetText() == '') {
                                            alert('Escolha o fundo cujas cotas devem ser importadas!');                                                                            
                                        }
                                        else {                                                                    
                                            LoadingPanel.Show();
                                            callbackImportarCotas.SendCallback();                                                                             
                                        }
                                        return false;
                                    }
                                    ">
                                                    <asp:Literal ID="Literal11" runat="server" Text="Importar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Fundo:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropCarteiraFiltro" runat="server" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                            DataSourceID="EsDSCarteira" ValueField="IdCarteira" TextField="Apelido" CssClass="dropDownListLongo">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal8" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick="if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter"
                                        OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnImportarCotas" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="popupImportarCotas.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Importar Cotas" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="CompositeKey"
                                        DataSourceID="EsDSHistoricoCota" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                        OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnPreRender="gridCadastro_PreRender"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="10%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="1" Width="10%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" Width="10%" VisibleIndex="2"/>
                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Fundo" Width="45%" VisibleIndex="3"/>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="CotaFechamento" Caption="Valor Cota"
                                                VisibleIndex="4" Width="30%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000000000;(#,##0.00);0.0000000000}" />
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />                                            
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label_Curto">
                                                                    <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>'
                                                                        OnInit="textData_Init" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label_Curto">
                                                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" AssociatedControlID="dropCarteira"
                                                                        Text="Fundo:">
                                                                    </asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropCarteira" runat="server" DataSourceID="EsDSCarteira" ValueField="IdCarteira"
                                                                        TextField="Apelido" DropDownStyle="DropDown" IncrementalFilteringMode="Contains" CssClass="dropDownListLongo" Text='<%#Eval("IdCarteira")%>'
                                                                        OnLoad="dropCarteira_Load">
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label_Curto">
                                                                    <asp:Label ID="labelCota" runat="server" CssClass="labelRequired" Text="Cota:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textCota" runat="server" ClientInstanceName="textCota" Text='<%# Eval("CotaFechamento") %>'
                                                                        NumberType="Float" MaxLength="16" DecimalPlaces="12">
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal9" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="350px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSHistoricoCota" runat="server" OnesSelect="EsDSHistoricoCota_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Importando cotas, aguarde..."
            ClientInstanceName="LoadingPanel" Modal="True" />
    </form>
</body>
</html>
