﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_DetalheResgateFundo, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">           
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraFiltro.Focus();
    }        
    function OnClearFilterClick_Filtro()  {
        textIdPosicaoResgatadaFiltro.SetValue(null);
        textIdOperacaoFiltro.SetValue(null);    
    }            
    // Ocorre quando combo OperacaoFundo muda de valor
    // values[]= IdCliente;IdCarteira;ApelidoCliente;ApelidoCarteira
    function gvCallback(values) {
        //alert("Values: " + values);
        
        if (values[0]!= null && values[0]!= '') {
            btnEditCodigoCliente.SetValue(values[0]);
            btnEditCodigoCarteira.SetValue(values[1]);
            textNomeCliente.SetValue(values[2]);
            textNomeCarteira.SetValue(values[3]);
        }
        else {
            btnEditCodigoCliente.SetValue(null);
            btnEditCodigoCarteira.SetValue(null);
            textNomeCliente.SetValue(null);
            textNomeCarteira.SetValue(null);
        }
        //
        // Chama o CallBack para Carregar Combo de Posição baseado no idCliente e IdCarteira
        var filtro = values[0] + ";"+values[1];
        dropPosicao.PerformCallback(filtro);
        //dropPosicao.SetSelectedIndex(-1);                
        //dropPosicao.Value = "";
        //var t=setTimeout("SetIndice()",3000);
    }    
        
    function SetIndice() {
        //dropPosicao..SetSelectedIndex(-1);
    }    
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            }
            else    
            {
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            if (gridCadastro.cp_EditVisibleIndex == -1) {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else {                                                                               
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira, textData);
            }
        }        
        "/>
    </dxcb:ASPxCallback>            
        
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else
                {      
                    if (operacao == 'salvar')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }                       
                }                  
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>  
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>  
                                                                
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Ajuste de Resgates em Fundos"/>
    </div>
        
    <div id="mainContent">   
            
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table border="0">        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"/>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {popupMensagemCliente.HideWindow();
                                     				                ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
                                                                    }"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td width="450">
                                <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" />
                            </td>
                        </tr>        

                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCarteiraFiltro" runat="server" CssClass="labelNormal" Text="Carteira:"/>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCarteiraFiltro" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton />
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCarteiraFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {popupMensagemCarteira.HideWindow();
                                     				                ASPxCallback2.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
                                                                    }"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td width="450">
                                <asp:TextBox ID="textNomeCarteiraFiltro" runat="server" CssClass="textNome" Enabled="false" />
                            </td>
                        </tr>        
                        
                        <tr>
                            <td>
                                <asp:Label ID="labelIdOperacao" runat="server" CssClass="labelNormal" Text="Id Oper.:" ToolTip="Id da Operação Resgatada" />
                            </td>
                                           
                            <td colspan="3" width="450">
                                <dxe:ASPxSpinEdit ID="textIdOperacaoFiltro" runat="server" CssClass="textValor" ClientInstanceName="textIdOperacaoFiltro" MaxLength="8" NumberType="Integer"  />
                            </td>    
                        </tr>
                        
                        <tr>
                            <td>
                                <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Pos. Resgatada:" ToolTip="Id da Posição Resgatada" />
                            </td>
                                           
                            <td colspan="3" width="450">
                                  <dxe:ASPxSpinEdit ID="textIdPosicaoResgatadaFiltro" runat="server" CssClass="textValor" ClientInstanceName="textIdPosicaoResgatadaFiltro" MaxLength="8" NumberType="Integer" />
                            </td>    
                        </tr>                        
                                                
                        <tr>
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" Visible="false" />
                                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" Visible="false"/>
                            </td>
                        </tr>
                                                                                                        
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_Filtro(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>                
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) { operacao='deletar'; callbackErro.SendCallback();} return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal5" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
            </div>
            
            <div class="divDataGrid">
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="CompositeKey" DataSourceID="EsDSDetalheResgateFundo"
                        OnRowUpdating="gridCadastro_RowUpdating"
                        OnRowInserting="gridCadastro_RowInserting"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                        OnPreRender="gridCadastro_PreRender"
                        OnBeforeGetCallbackResult="gridCadastro_PreRender"                                          
                        >
                                            
                    <Columns>                    
                                                                                             
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"/>
                        <dxwgv:GridViewDataTextColumn FieldName="ApelidoCliente" UnboundType="String" Visible="False"/>
                        <dxwgv:GridViewDataTextColumn FieldName="ApelidoCarteira" UnboundType="String" Visible="False"/>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Nota Original" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left"/>
                        <dxwgv:GridViewDataColumn FieldName="IdPosicaoResgatada" Caption="Nota Resgat." VisibleIndex="2" Width="7%" CellStyle-HorizontalAlign="left"/>
                        <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Dt. Operação" VisibleIndex="3" Width="10%"/>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Cliente" VisibleIndex="4" Width="8%" CellStyle-HorizontalAlign="left" />
                        <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Carteira" VisibleIndex="5" Width="8%" CellStyle-HorizontalAlign="left"/>
        
                        <dxwgv:GridViewDataComboBoxColumn FieldName="Fonte" VisibleIndex="6" Width="7%">
                            <PropertiesComboBox EncodeHtml="false">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="<div title='Interno'>Interno</div>" />
                                    <dxe:ListEditItem Value="2" Text="<div title='Manual'>Manual</div>" />
                                    <dxe:ListEditItem Value="5" Text="<div title='Importado YMF'>Importado YMF</div>" />
                                    <dxe:ListEditItem Value="10" Text="<div title='Importado SMA'>Importado SMA</div>" />
                                    <dxe:ListEditItem Value="50" Text="<div title='Ajustado'>Ajustado</div>" />
                                    <dxe:ListEditItem Value="100" Text="<div title='Ordem Fundo'>Ordem Fundo</div>" />                                    
                                </Items>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>                       
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="8" Width="10%" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"/>
                        </dxwgv:GridViewDataTextColumn>

                        <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" Caption="Vl. Bruto" VisibleIndex="10" Width="10%" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>
                        </dxwgv:GridViewDataTextColumn>
    
                        <dxwgv:GridViewDataTextColumn FieldName="ValorIR" Caption="Vl. IR" VisibleIndex="12" Width="9%" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>
                        </dxwgv:GridViewDataTextColumn>

                        <dxwgv:GridViewDataTextColumn FieldName="ValorIOF" Caption="Vl. IOF" VisibleIndex="14" Width="9%" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>
                        </dxwgv:GridViewDataTextColumn>

                        <dxwgv:GridViewDataTextColumn FieldName="ValorLiquido" Caption="Vl. Liquido" VisibleIndex="16" Width="9%" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"/>
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorPerformance" Visible="false"/>
                                                
                    </Columns>
                    
                    <Templates>
                    <EditForm>
                        
                        <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">                        
                        <div class="editForm">                            
                            
                            <table border="0">

<!-- Mostra no Modo Insert -->
<div id="divInsert" runat="server" visible="false">                                                        
    <tr>
        <td class="td_Label">
            <asp:Label ID="label7" runat="server" CssClass="labelRequired" Text="Oper. Fundos:" ToolTip="Escolha uma OperaçãoFundo"/>
        </td>        

        <td colspan="3">                                
            <dxwgv:ASPxGridLookup ID="dropOperacaoFundo" ClientInstanceName="dropOperacaoFundo" runat="server" SelectionMode="Single" KeyFieldName="IdOperacao" DataSourceID="EsDSOperacaoFundo"
            Width="400px" TextFormatString="{0} - {2} - {3} - {5:d} - {7:#,##0.00;(#,##0.00);0.00}" Font-Size="11px" IncrementalFilteringMode="StartsWith" DropDownWindowStyle-HorizontalAlign="Center"
            DropDownWindowStyle-Wrap="false" AllowUserInput="False">
                 <Columns>
                     <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                     <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Id Nota" Width="6%" CellStyle-Font-Size="XX-Small" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                     <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Id Cliente" Width="6%" CellStyle-Font-Size="XX-Small" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                     <dxwgv:GridViewDataColumn FieldName="ApelidoCliente" Caption="Nome" Width="12%" CellStyle-Font-Size="XX-Small" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                     <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id Carteira" Width="6%" CellStyle-Font-Size="XX-Small" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                     <dxwgv:GridViewDataColumn FieldName="ApelidoCarteira" Caption="Nome" Width="12%" CellStyle-Font-Size="XX-Small" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                     <dxwgv:GridViewDataColumn FieldName="DataOperacao" Caption="Operação" Width="7%" CellStyle-Font-Size="XX-Small" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                     <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" Caption="Quantidade" Width="7%" CellStyle-Font-Size="XX-Small" CellStyle-HorizontalAlign="right" HeaderStyle-HorizontalAlign="right">
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"/>
                     </dxwgv:GridViewDataSpinEditColumn>
                     
                     <dxwgv:GridViewDataSpinEditColumn FieldName="ValorBruto" Caption="Valor" Width="7%" CellStyle-Font-Size="XX-Small" CellStyle-HorizontalAlign="right" HeaderStyle-HorizontalAlign="right">
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                     </dxwgv:GridViewDataSpinEditColumn>
                </Columns>
                
                <GridViewProperties>
                    <Settings ShowFilterRow="True"/>
                </GridViewProperties>
                
                <ClientSideEvents TextChanged="function(s, e) {
                     // textIdOperacaoGrid.SetText(s.GetText());                
                     var gv = ASPxClientGridView.Cast(dropOperacaoFundo.GetGridView());
                     gv.GetRowValues(gv.GetFocusedRowIndex(), 'IdCliente;IdCarteira;ApelidoCliente;ApelidoCarteira', gvCallback);                
                }" />
            </dxwgv:ASPxGridLookup>                                                
        </td>
     </tr>
     <tr>
        <td class="td_Label">
            <asp:Label ID="label10" runat="server" CssClass="labelRequired" Text="Posição Fundo:" ToolTip="Escolha Primeiraimente uma Operação Fundo"/>
        </td>
        <td colspan="3">
            <dxe:ASPxComboBox runat="server" ID="dropPosicao" ClientInstanceName="dropPosicao" OnCallback="dropPosicao_Callback"
                DropDownStyle="DropDownList" DataSourceID="EsDSPosicaoFundo" TextField="IdPosicao" Width="400px"
                ValueField="IdPosicao" ValueType="System.Int32" 
                TextFormatString="{0} - {2} - {5} - {4:#,##0.00;(#,##0.00);0.00}"
                >
               
               <Columns>                    
                  <dxe:ListBoxColumn FieldName="IdPosicao" Width="60px" Caption="Id Posição" />
                  <dxe:ListBoxColumn FieldName="ValorAplicacao" Width="100px" Caption="Valor Aplicação" />
                  <dxe:ListBoxColumn FieldName="DataAplicacaoFormatada" Width="70px" Caption="Dt. Aplicação"/>
                  <dxe:ListBoxColumn FieldName="CotaAplicacao" Width="100px" Caption="Cota Aplicação" />                  
                  <dxe:ListBoxColumn FieldName="Quantidade" Width="100px" Caption="Quantidade" />
                  <dxe:ListBoxColumn FieldName="ValorLiquido" Width="100px" Caption="Valor Líquido" />
               </Columns>

            </dxe:ASPxComboBox>                        
                            
        </td>
    </tr>
</div>

<!-- Mostra no Modo Update -->
<div id="divUpdate" runat="server" visible="false">
    <tr>
        <td class="td_Label">
            <asp:Label ID="label8" runat="server" CssClass="labelNormal" Text="Id Oper. Fundo:"/>
        </td>        
        <td>
            <dxe:ASPxSpinEdit ID="textIdOperacao" runat="server" ClientInstanceName="textIdOperacaoGrid" 
                CssClass="textButtonEdit" ClientEnabled="false" Text='<%#Eval("IdOperacao")%>' >                                        
            </dxe:ASPxSpinEdit>
       </td>
        <td class="td_Label">
            <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Id Pos. Resgatada:"/>
        </td>               
        <td>                                
            <dxe:ASPxSpinEdit ID="textIdPosicao" runat="server" ClientInstanceName="textIdPosicaoGrid" 
                CssClass="textButtonEdit" ClientEnabled="false" Text='<%#Eval("IdPosicaoResgatada")%>'>                                        
            </dxe:ASPxSpinEdit>
        </td>
    </tr>                                
</div>

                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCliente" runat="server" CssClass="labelNormal" Text="Cliente:"/>
                                    </td>        
                                    
                                     <td>
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" ClientInstanceName="btnEditCodigoCliente" CssClass="textButtonEdit" ClientEnabled="false" Text='<%#Eval("IdCliente")%>'>
                                        <Buttons><dxe:EditButton></dxe:EditButton></Buttons>
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                    
                                    <td colspan="2">
                                        <dxe:ASPxTextBox ID="textNomeCliente" runat="server" CssClass="textNome" ClientInstanceName="textNomeCliente" 
                                            ClientEnabled="false" BackColor="#EBEBEB" Text='<%#Eval("ApelidoCliente")%>' />
                                    </td>                                        
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelNormal" Text="Carteira:"/>
                                    </td>        
                                    
                                     <td>
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" ClientInstanceName="btnEditCodigoCarteira" CssClass="textButtonEdit" ClientEnabled="false" Text='<%#Eval("IdCarteira")%>'>
                                        <Buttons><dxe:EditButton></dxe:EditButton></Buttons>
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                    
                                    <td colspan="2">
                                        <dxe:ASPxTextBox ID="textNomeCarteira" runat="server" CssClass="textNome" ClientInstanceName="textNomeCarteira" 
                                            ClientEnabled="false" BackColor="#EBEBEB" Text='<%#Eval("ApelidoCarteira")%>' />
                                    </td>                                        
                                </tr>
                                        
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Quantidade:"/>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor_5" ClientInstanceName="textQuantidade" 
                                            MaxLength="28" NumberType="Float" DecimalPlaces="12" Text='<%#Eval("Quantidade")%>' />
                                    </td>                                  
                                    
                                     <td class="td_Label">
                                        <asp:Label ID="label5" runat="server" CssClass="labelRequired" Text="Performance:"/>
                                    </td>
                                    <td>
                                         <dxe:ASPxSpinEdit ID="textPerformance" runat="server" CssClass="textValor_5" ClientInstanceName="textPerformance" 
                                            MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorPerformance")%>' />
                                    </td>          
                                    
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Valor Bruto:"/>
                                    </td>
                                    <td>
                                         <dxe:ASPxSpinEdit ID="textValorBruto" runat="server" CssClass="textValor_5" ClientInstanceName="textValorBruto" 
                                            MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorBruto")%>' />
                                    </td>
                                    
                                    <td class="td_Label">
                                        <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Valor Líquido:"/>
                                    </td>
                                    <td>
                                         <dxe:ASPxSpinEdit ID="textValorLiquido" runat="server" CssClass="textValor_5" ClientInstanceName="textValorLiquido" 
                                            MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorLiquido")%>' />
                                    </td>                                               
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Valor IR:"/>
                                    </td>
                                    <td>
                                         <dxe:ASPxSpinEdit ID="textValorIR" runat="server" CssClass="textValor_5" ClientInstanceName="textValorIR" 
                                            MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorIR")%>' />
                                    </td>
                                    
                                    <td class="td_Label">
                                        <asp:Label ID="label4" runat="server" CssClass="labelRequired" Text="Valor IOF:"/>
                                    </td>
                                    <td>
                                         <dxe:ASPxSpinEdit ID="textValorIOF" runat="server" CssClass="textValor_5" ClientInstanceName="textValorIOF" 
                                            MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorIOF")%>' />
                                    </td>                                               
                                </tr>                                                                                                
                            </table>
                            
                            <div class="linhaH"></div>
                                        
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"  OnInit="btnOKAdd_Init"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal2" runat="server" Text="OK+"/><div></div></asp:LinkButton>

                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK"/><div></div></asp:LinkButton>

                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal13" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                            </div>        
                        </div>                        
                        </asp:Panel>
                    </EditForm>
                        
                    <StatusBar>
                        <div>
                            <div style="float:left">
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                            </div>                    
                        </div>
                    </StatusBar>
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="600px"  />
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>
                    
                </dxwgv:ASPxGridView>            
            </div>        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"/>
            
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSDetalheResgateFundo" runat="server" OnesSelect="EsDSDetalheResgateFundo_esSelect" />
    <cc1:esDataSource ID="EsDSOperacaoFundo" runat="server" OnesSelect="EsDSOperacaoFundo_esSelect" LowLevelBind="true"/>
    <cc1:esDataSource ID="EsDSPosicaoFundo" runat="server" OnesSelect="EsDSPosicaoFundo_esSelect" LowLevelBind="true" />
    
    </form>
</body>
</html>