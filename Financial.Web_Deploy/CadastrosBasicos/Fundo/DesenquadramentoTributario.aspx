﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_DesenquadramentoTributario, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <link href="~/css/teste.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    popup=true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function HideData() {
        labelDataRecolhimento.SetVisible(false);
        textDataRecolhimento.SetVisible(false);
    }
    function ShowData() {
        labelDataRecolhimento.SetVisible(true);
        textDataRecolhimento.SetVisible(true);
    }
    function textMotivoMudancaOcorrencia_OnKeyDown(s, e) {
        if (s.GetText().length >= 200)
            if (e.htmlEvent.keyCode > 47 && e.htmlEvent.keyCode < 90)
                ASPxClientUtils.PreventEventAndBubble(e.htmlEvent);
        // Ctrl + V
        if (e.htmlEvent.keyCode === 86 && e.htmlEvent.ctrlKey)
            CorrigeTexto();
    }
    function CorrigeTexto() {
            var maxLength = 200;
            setTimeout(function () { memo.SetText(memo.GetText().substr(0, maxLength)); }, 0);
    }
    function OnGetDataCarteira(data) 
    {
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();    
    }    
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Fundo de Investimento - Desenquadramento Tributário"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" OnRowUpdating="gridCadastro_RowUpdating"
                                    DataSourceID="EsDSDesenquadramentoTributario" KeyFieldName="IdDesenquadramentoTributario"
                                    OnRowInserting="gridCadastro_RowInserting" OnCustomCallback="gridCadastro_CustomCallback"
                                    OnCancelRowEditing="gridCadastro_CancelRowEditing"
                                    OnCustomColumnDisplayText="gridCadastro_CustomColumnDisplayText" OnPreRender="gridCadastro_PreRender">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Tipo de Ocorrência" FieldName="TipoOcorrencia"
                                            Width="30%">
                                            <PropertiesComboBox>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Data" CellStyle-HorizontalAlign="left"
                                            Width="10%" />
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Fundo de Investimento" FieldName="IdCarteira">
                                            <PropertiesComboBox DataSourceID="EsDSCarteira" TextField="Nome" ValueField="IdCarteira" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Classificação Tributária Atual" FieldName="ClassificacaoTributariaAtual">
                                            <PropertiesComboBox>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="MotivoMudancaOcorrencia" Caption="Motivo da Mudança/Ocorrência" />
                                        <dxwgv:GridViewDataTextColumn FieldName="Apelido" Visible="false" UnboundType="String"/>
                                        <dxwgv:GridViewDataTextColumn FieldName="ClassificacaoTributariaCarteira" Visible="false" UnboundType="String"/>                                        
                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                            <asp:Panel ID="panelEdicao" runat="server">
                                                <fieldset>
                                                    <legend>Fundo de Investimento - Desenquadramento Tributário</legend>
                                                    <table border="0">
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTipoOcorrencia" runat="server" CssClass="labelRequired" Text="Tipo Ocorrência:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxRadioButtonList ID="rblTipoOcorrencia" ClientInstanceName="rblTipoOcorrencia"
                                                                    runat="server" Value='<%#Eval("TipoOcorrencia")%>' Width="450px" ValueType="System.Int32"
                                                                    OnLoad="rblTipoOcorrencia_OnLoad">
                                                                    <ClientSideEvents Init="function(s, e) 
                                                                    {
                                                                        if(rblTipoOcorrencia.Value == 2)
                                                                            cbPanel.SetClientVisible(true);
                                                                        else     
                                                                            cbPanel.SetClientVisible(false);
                                                                    }" />
                                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                    {
                                                                    debugger;
                                                                        if(rblTipoOcorrencia.GetSelectedItem().value == '1')
                                                                        {
                                                                            cbPanel.SetClientVisible(false);
                                                                        } 
                                                                        else 
                                                                        {
                                                                            cbPanel.SetClientVisible(true);
                                                                            cbPanel.PerformCallback(dropFundoInvestimento.GetValue());
                                                                        }
                                                                    }" />
                                                                </dxe:ASPxRadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data Ocorrência/Data Inicio Vigência:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                                                    ClientInstanceName="btnEditCodigoCarteira" Text='<%# Eval("IdCarteira") %>' OnLoad="btnEditCodigo_Load"
                                                                    MaxLength="10" NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {popupMensagemCarteira.HideWindow();                                                                                        
                                                                        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
                                                                        cbPanel.PerformCallback(s.GetValue());
                                                                         }" />
                                                                </dxe:ASPxSpinEdit>
                                                                <asp:TextBox ID="textNome" runat="server" CssClass="textLongo" Enabled="False" Text='<%# Eval("Apelido") %>' BackColor="lightGray"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <dxe:ASPxLabel ID="labelClassificacaoTributariaBaseDados" ClientInstanceName="labelClassificacaoTributariaBaseDados"
                                                                    runat="server" Text="Classificação Tributaria Atual:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textClassificacaoTributariaBaseDados" ClientInstanceName="textClassificacaoTributariaBaseDados"
                                                                    runat="server" OnDataBound="textClassificacaoTributariaBaseDados_OnDataBound" BackColor="lightGray"
                                                                    Value='<%#Eval("ClassificacaoTributariaCarteira")%>' Width="450px" Enabled="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelClassificacaoTributariaAtual" runat="server" CssClass="labelRequired"
                                                                    Text="Nova Classificação Tributária:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxRadioButtonList ID="rblClassificacaoTributariaAtual" ClientInstanceName="rblClassificacaoTributariaAtual"
                                                                    runat="server" RepeatDirection="Horizontal" ValueType="System.Int32" Width="450px" 
                                                                    Value='<%#Eval("ClassificacaoTributariaAtual")%>' OnLoad="rblClassificacaoTributariaAtual_OnLoad" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="labelMotivoMudancaOcorrencia" runat="server" Text="Motivo da Mudança/Ocorrência:"> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxMemo ID="textMotivoMudancaOcorrencia" runat="server" ClientInstanceName="textMotivoMudancaOcorrencia"
                                                                    Value='<%#Eval("MotivoMudancaOcorrencia")%>' Width="450px" Height="100px">
                                                                    <ClientSideEvents KeyDown="textMotivoMudancaOcorrencia_OnKeyDown" />
                                                                </dxe:ASPxMemo>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </fieldset>
                                                <dxcp:ASPxCallbackPanel runat="server" ClientInstanceName="cbPanel" ID="cbPanel"
                                                    OnCallback="cbPanel_Callback">
                                                    <PanelCollection>
                                                        <dxp:PanelContent runat="server">
                                                            <asp:Panel ID="panelRecolhimentoIR" runat="server">
                                                                <fieldset>
                                                                    <legend>Padrão de Recolhimento de IR(Come-Cotas) para Eventos Corporativos, Transformações
                                                                        e Desenquadramentos de Fundos</legend>
                                                                    <table>
                                                                        <tr>
                                                                            <td class="td_Label">
                                                                                <fieldset>
                                                                                    <legend style="text-align: left">Execução do Recolhimento</legend>
                                                                                    <dxe:ASPxRadioButtonList ID="rblExecucaoRecolhimento" runat="server" ClientInstanceName="rblExecucaoRecolhimento"
                                                                                        ValueType="System.Int32" Value='<%#Eval("ExecucaoRecolhimento")%>' Height="100px">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Não Recolher" />
                                                                                            <dxe:ListEditItem Value="2" Text="Recolher no momento do evento/transformação/desenquadramento" />
                                                                                            <dxe:ListEditItem Value="3" Text="Agendar o Recolhimento para uma data pré-definida" />
                                                                                        </Items>
                                                                                        <ClientSideEvents Init="function(s, e) 
                                                                                        {
                                                                                            if(rblExecucaoRecolhimento.GetSelectedItem() != null){
                                                                                                if(rblExecucaoRecolhimento.GetSelectedItem().value != '3')
                                                                                                { 
                                                                                                    HideData();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    ShowData();
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                HideData();
                                                                                            }
                                                                                        }" />
                                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                                        {
                                                                                            if(rblExecucaoRecolhimento.GetSelectedItem().value == '3')
                                                                                            {
                                                                                                ShowData();
                                                                                            } 
                                                                                            else 
                                                                                            {
                                                                                                HideData();
                                                                                                textDataRecolhimento.SetDate(null);
                                                                                            }
                                                                                        }" />
                                                                                    </dxe:ASPxRadioButtonList>
                                                                                </fieldset>
                                                                            </td>
                                                                            <td class="td_Label">
                                                                                <fieldset>
                                                                                    <legend style="text-align: left">Alíquota IR</legend>
                                                                                    <dxe:ASPxRadioButtonList ID="rblAliquotaIR" ClientInstanceName="rblAliquotaIR" runat="server"
                                                                                        Height="100px" Value='<%#Eval("AliquotaIR")%>' ValueType="System.Int32">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Utilizar alíquota IR padrão do Come-Cota" />
                                                                                            <dxe:ListEditItem Value="2" Text="Utilizar alíquota IR vigente de cada cautela" />
                                                                                        </Items>
                                                                                    </dxe:ASPxRadioButtonList>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td_Label">
                                                                                <dxe:ASPxLabel ID="labelDataRecolhimento" runat="server" ClientInstanceName="labelDataRecolhimento"
                                                                                    CssClass="labelNormal" Text="Data Recolhimento:" />
                                                                                <dxe:ASPxDateEdit ID="textDataRecolhimento" runat="server" ClientInstanceName="textDataRecolhimento"
                                                                                    Value='<%#Eval("DataRecolhimento")%>' />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </asp:Panel>
                                                        </dxp:PanelContent>
                                                    </PanelCollection>
                                                </dxcp:ASPxCallbackPanel>
                                            </asp:Panel>
                                            <div class="linhaH">
                                            </div>
                                            <div class="editForm">
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                        </div>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                        OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                        <asp:Literal ID="Literal11" runat="server" Text="Cancelar" /><div>
                                                        </div>
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </EditForm>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="45%" />
                                    <ClientSideEvents EndCallback="function(s, e) 
                                     {
                                       if(rblTipoOcorrencia.GetValue() == 2)
                                           cbPanel.SetClientVisible(true);
                                       else     
                                           cbPanel.SetClientVisible(false);
                                     }" />
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSDesenquadramentoTributario" runat="server" OnesSelect="EsDSDesenquadramentoTributario_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>
