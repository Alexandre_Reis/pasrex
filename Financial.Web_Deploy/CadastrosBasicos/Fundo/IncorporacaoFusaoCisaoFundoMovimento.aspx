﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_IncorporacaoFusaoCisaoFundoMovimento, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web"
    TagPrefix="dxuc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';

    function CheckedChanged(s, e, grid) {

        if (s.GetChecked())
            grid.SelectRows();
        else
            grid.UnselectRows();
    }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                              
            }
            else {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Movimentos de Incorporação/Fusão/Cisão de Fundo"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdEventoFundo"
                                        DataSourceID="EsDSIncorporacaoFusaoCisaoFundo"  OnCustomCallback="gridCadastro_CustomCallback"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnRowUpdating="gridCadastro_RowUpdating"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="7%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="CarteiraOrigem" Caption="Carteira Origem" VisibleIndex="1"
                                                UnboundType="String" Width="40%" />
                                            <dxwgv:GridViewDataColumn FieldName="CarteiraDestino" Caption="Carteira Destino"
                                                VisibleIndex="2" UnboundType="String" Width="40%" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="hiddenIdEventoFundo" runat="server" CssClass="hiddenField" Text='<%#Eval("IdEventoFundo")%>'
                                                                    ClientInstanceName="hiddenIdEventoFundo" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <b>
                                                                    <asp:Label ID="label5" runat="server" Text='Carteira Origem:' />
                                                                </b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text='<%#Eval("CarteiraOrigem")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <b>
                                                                    <asp:Label ID="label7" runat="server" Text='Carteira Destino:' />
                                                                </b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="label8" runat="server" CssClass="labelNormal" Text='<%#Eval("CarteiraDestino")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <b>
                                                                    <asp:Label ID="label1" runat="server" Text='Percentual:' />
                                                                </b>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text='<%#Eval("Percentual")%>' />
                                                            </td>
                                                        </tr>
                                                        <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                            <div class="editForm">
                                                                <dxtc:ASPxPageControl ID="tabCadastro" runat="server" ActiveTabIndex="0" TabSpacing="0px"
                                                                    TabAlign="Center" TabPosition="Top" Height="210px">
                                                                    <TabPages>
                                                                        <dxtc:TabPage Text="Cautelas">
                                                                            <ContentCollection>
                                                                                <dxw:ContentControl runat="server">
                                                                                    <div class="divPanel" id="Div1">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="divDataGrid">
                                                                                                        <dxwgv:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" KeyFieldName="CompositeKeyCautelas"
                                                                                                            DataSourceID="EsDSCautelas" OnCustomUnboundColumnData="gridCautelas_CustomUnboundColumnData" Settings-ShowHeaderFilterButton="true"
                                                                                                            OnRowUpdating="gridCautelas_RowUpdating" AutoGenerateColumns="False" Width="1200px" SettingsText-CommandCancel="Cancelar" SettingsText-CommandUpdate="Atualizar"
                                                                                                            OnCustomSummaryCalculate="grid_CustomSummaryCalculate" OnSelectionChanged="grid_SelectionChanged" OnHtmlDataCellPrepared="gridCautela_HtmlDataCellPrepared">
                                                                                                            <Columns>
                                                                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" >
                                                                                                                    <HeaderTemplate>
                                                                                                                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas"
                                                                                                                            BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, ASPxGridView1);}"
                                                                                                                            OnInit="CheckBoxSelectAll" />
                                                                                                                    </HeaderTemplate>
                                                                                                                </dxwgv:GridViewCommandColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="Processar" ReadOnly="True" VisibleIndex="1" Visible="true">
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="CompositeKeyCautelas" ReadOnly="True" VisibleIndex="2" UnboundType="String" Visible="False">
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn ReadOnly="True" FieldName="Apelido" Caption="Cotista"
                                                                                                                    VisibleIndex="3" Width="35%">
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn ReadOnly="True" FieldName="IdOperacao" Caption="Cautela"
                                                                                                                    VisibleIndex="4" UnboundType="String" Width="5%">
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataDateColumn ReadOnly="True" FieldName="DataAplicacao" Caption="Dt. Aplic."
                                                                                                                    VisibleIndex="5" UnboundType="String" Width="10%">
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataDateColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn ReadOnly="True" FieldName="Quantidade" Caption="Qtd. Cotas"
                                                                                                                    VisibleIndex="6" UnboundType="String" Width="10%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" Caption="Vl. Bruto" VisibleIndex="7"
                                                                                                                    UnboundType="String" Width="10%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="ValorIOF" Caption="Vl. IOF" VisibleIndex="8"
                                                                                                                    UnboundType="String" Width="5%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="ValorIR" Caption="Vl. IR" VisibleIndex="9"
                                                                                                                    UnboundType="String" Width="5%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="ValorLiquido" Caption="Vl. Líquido" VisibleIndex="10"
                                                                                                                    UnboundType="String" Width="10%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="QtdEvento" Caption="Qtd. Evento (Duplo click p/ editar)" VisibleIndex="11"
                                                                                                                    UnboundType="String" Width="10%" CellStyle-Font-Bold="true" EditFormSettings-Caption="Quantidade do Evento:" EditCellStyle-HorizontalAlign="Left" EditFormCaptionStyle-HorizontalAlign="Right">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="NovoValor" Caption="Valor Bruto Cisão" VisibleIndex="12"
                                                                                                                    UnboundType="String" Width="10%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="PercentualCisao" Caption="Percentual" VisibleIndex="13"
                                                                                                                    UnboundType="Decimal" Width="10%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                            </Columns>
                                                                                                            <SettingsBehavior ProcessSelectionChangedOnServer="True" />
                                                                                                            <Settings ShowFooter="True" />
                                                                                                            <TotalSummary>
                                                                                                                <dxwgv:ASPxSummaryItem FieldName="ValorBruto" SummaryType="Sum" />
                                                                                                                <dxwgv:ASPxSummaryItem FieldName="ValorLiquido" SummaryType="Sum" />
                                                                                                                <dxwgv:ASPxSummaryItem FieldName="NovoValor" SummaryType="Custom"/>
                                                                                                            </TotalSummary>
                                                                                                            <SettingsPopup EditForm-Width="1000px" />
                                                                                                            <ClientSideEvents RowDblClick="function(s, e) {s.StartEditRow(e.visibleIndex);}" />
                                                                                                        </dxwgv:ASPxGridView>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </dxw:ContentControl>
                                                                            </ContentCollection>
                                                                        </dxtc:TabPage>
                                                                        <dxtc:TabPage Text="Selecão de Ativos">
                                                                            <ContentCollection>
                                                                                <dxw:ContentControl runat="server">
                                                                                    <div class="divPanel" id="Div2">
                                                                                        <table width="100%">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="divDataGrid">
                                                                                                        <dxwgv:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2" runat="server" KeyFieldName="CompositeKeyAtivos"
                                                                                                            DataSourceID="EsDSAtivos" OnCustomUnboundColumnData="gridAtivos_CustomUnboundColumnData" Settings-ShowHeaderFilterButton="true"
                                                                                                            OnRowUpdating="gridAtivos_RowUpdating" AutoGenerateColumns="False" Width="1200px" SettingsText-CommandCancel="Cancelar" SettingsText-CommandUpdate="Atualizar"
                                                                                                            OnCustomSummaryCalculate="grid_CustomSummaryCalculate" OnSelectionChanged="grid_SelectionChanged" OnHtmlDataCellPrepared="gridCautela_HtmlDataCellPrepared">
                                                                                                            <Columns>
                                                                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" >
                                                                                                                    <HeaderTemplate>
                                                                                                                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas"
                                                                                                                            BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, ASPxGridView2);}"
                                                                                                                            OnInit="CheckBoxSelectAll" />
                                                                                                                    </HeaderTemplate>
                                                                                                                </dxwgv:GridViewCommandColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="Processar" ReadOnly="True" VisibleIndex="1" Visible="true">
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="CompositeKeyAtivos" ReadOnly="True" VisibleIndex="2"
                                                                                                                    UnboundType="String" Visible="False">
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn ReadOnly="True" FieldName="DescricaoMercado" Caption="Mercado"
                                                                                                                    VisibleIndex="3" UnboundType="String" Width="10%">
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn ReadOnly="True" FieldName="Ativo" Caption="Ativo" VisibleIndex="4"
                                                                                                                    UnboundType="String" Width="40%">
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn ReadOnly="True" FieldName="QtdAtivo" Caption="Quantidade"
                                                                                                                    VisibleIndex="5" UnboundType="String" Width="10%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn ReadOnly="True" FieldName="ValorLiquido" Caption="Vl. Líquido"
                                                                                                                    VisibleIndex="6" UnboundType="String" Width="10%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="QtdEvento" Caption="Qtd. Evento (Duplo click p/ editar)" VisibleIndex="7" 
                                                                                                                    UnboundType="String" Width="10%" CellStyle-Font-Bold="true" EditFormSettings-Caption="Quantidade do Evento:" EditCellStyle-HorizontalAlign="Left" EditFormCaptionStyle-HorizontalAlign="Right">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn FieldName="NovoValor" Caption="Valor Bruto Cisão" VisibleIndex="8"
                                                                                                                    UnboundType="String" Width="10%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                                <dxwgv:GridViewDataTextColumn ReadOnly="True" FieldName="PercentualCisaoAtivos" Caption="Percentual" VisibleIndex="9" CellStyle-HorizontalAlign="Right"
                                                                                                                    UnboundType="Decimal" Width="10%">
                                                                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>
                                                                                                                    <EditFormSettings Visible="False" />
                                                                                                                </dxwgv:GridViewDataTextColumn>
                                                                                                            </Columns>
                                                                                                            <SettingsBehavior ProcessSelectionChangedOnServer="True" />
                                                                                                            <Settings ShowFooter="True" />
                                                                                                            <TotalSummary>
                                                                                                                <dxwgv:ASPxSummaryItem FieldName="ValorLiquido" SummaryType="Sum" />
                                                                                                                <dxwgv:ASPxSummaryItem FieldName="NovoValor" SummaryType="Custom" />
                                                                                                            </TotalSummary>
                                                                                                            <SettingsPopup EditForm-Width="1000px" />
                                                                                                            <ClientSideEvents RowDblClick="function(s, e) {s.StartEditRow(e.visibleIndex);}" />
                                                                                                        </dxwgv:ASPxGridView>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </dxw:ContentControl>
                                                                            </ContentCollection>
                                                                        </dxtc:TabPage>
                                                                    </TabPages>
                                                                </dxtc:ASPxPageControl>
                                                                <div class="linhaH"></div>
                                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                        <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                        OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                        <asp:Literal ID="Literal13" runat="server" Text="Cancelar" />
                                                                        <div></div>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>

                                                    </table>
                                                </div>
                                            </EditForm>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="500px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSIncorporacaoFusaoCisaoFundo" runat="server" OnesSelect="EsDSIncorporacaoFundo_esSelect" />
        <cc1:esDataSource ID="EsDSCautelas" runat="server" OnesSelect="EsDSCautelas_esSelect" />
        <cc1:esDataSource ID="EsDSAtivos" runat="server" OnesSelect="EsDSAtivos_esSelect" />
    </form>
</body>
</html>
