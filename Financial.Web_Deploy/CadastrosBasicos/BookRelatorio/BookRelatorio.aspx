﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_BookRelatorio, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
        
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    var tipoRelatorio;
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callBack1" runat="server" OnCallback="callBack1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                tipoRelatorio = e.result;
            }
            
            if (tipoRelatorio == 'Geral') {
                gridTipoRelatorio.AddItem('Informe Consolidado de Carteiras', 200);
                gridTipoRelatorio.AddItem('Enquadramento (Desenquadrados)', 500);
                gridTipoRelatorio.AddItem('Enquadramento (Todos)', 501);
            }
            else if (tipoRelatorio == 'Carteira') {
                gridTipoRelatorio.AddItem('Composição da Carteira', 1);
                gridTipoRelatorio.AddItem('Extrato Mensal de Cliente', 5);
                gridTipoRelatorio.AddItem('Extrato Mensal de Cliente (sem C/C)', 6);
                gridTipoRelatorio.AddItem('Fluxo de Caixa Sintético', 10);
                gridTipoRelatorio.AddItem('Mapa de Resultados', 30);
                gridTipoRelatorio.AddItem('DARF', 40);
                gridTipoRelatorio.AddItem('Saldos de Cotistas Analítico (Ordena por Código)', 100);
                gridTipoRelatorio.AddItem('Saldos de Cotistas Analítico (Ordena por Nome)', 101);
                gridTipoRelatorio.AddItem('Saldos de Cotistas Consolidado (Ordena por Código)', 102);
                gridTipoRelatorio.AddItem('Saldos de Cotistas Consolidado (Ordena por Nome)', 103);
                gridTipoRelatorio.AddItem('Extrato de Cotista', 104);
                gridTipoRelatorio.AddItem('Extrato de Cotista (com Carteira)', 105);
                gridTipoRelatorio.AddItem('Movimento de Aplic/Resg Cotista', 110);
                gridTipoRelatorio.AddItem('Notas de Aplic/Resg Cotista', 115);
                gridTipoRelatorio.AddItem('Extrato de Conta-Corrente', 150);
                gridTipoRelatorio.AddItem('Apuração de Ganho de RV', 170);
                gridTipoRelatorio.AddItem('Operações de Bolsa', 300);
                gridTipoRelatorio.AddItem('Operações de BMF', 310);
                gridTipoRelatorio.AddItem('Operações de Renda Fixa', 320);
                gridTipoRelatorio.AddItem('Operações de Fundos', 330);
                gridTipoRelatorio.AddItem('Enquadramento Carteira Individual', 502);                                                    
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
       
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Book x Relatórios"></asp:Label>
    </div>
        
    <div id="mainContent">
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="CompositeKey" DataSourceID="EsDSBookRelatorio"
                    OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdBook" Caption="Book" VisibleIndex="1" Width="30%">
                    <PropertiesComboBox DataSourceID="EsDSBook" TextField="Descricao" ValueField="IdBook"
                            ClientSideEvents-SelectedIndexChanged="function(s, e) 
                                                { gridTipoRelatorio.ClearItems();
                                                  callBack1.SendCallback(s.GetValue());
                                                  return false;                                                  
                                                    }">
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdRelatorio" VisibleIndex="2" Width="55%">
                        <PropertiesComboBox EncodeHtml="false" ClientInstanceName="gridTipoRelatorio">
                            <Items>
                                <dxe:ListEditItem Value="1" Text="<div title='Composição da Carteira'>Composição da Carteira</div>" />
                                <dxe:ListEditItem Value="5" Text="<div title='Extrato Mensal de Cliente'>Extrato Mensal de Cliente</div>" />
                                <dxe:ListEditItem Value="6" Text="<div title='Extrato Mensal de Cliente (sem C/C)'>Extrato Mensal de Cliente (sem C/C)</div>" />
                                <dxe:ListEditItem Value="10" Text="<div title='Fluxo de Caixa Sintético'>Fluxo de Caixa Sintético</div>" />
                                <dxe:ListEditItem Value="30" Text="<div title='Mapa de Resultados'>Mapa de Resultados</div>" />
                                <dxe:ListEditItem Value="40" Text="<div title='DARF'>DARF</div>" />
                                <dxe:ListEditItem Value="100" Text="<div title='Saldos de Cotistas Analítico (Ordena por Código)'>Saldos de Cotistas Analítico (Ordena por Código)</div>" />
                                <dxe:ListEditItem Value="101" Text="<div title='Saldos de Cotistas Analítico (Ordena por Nome)'>Saldos de Cotistas Analítico (Ordena por Nome)</div>" />
                                <dxe:ListEditItem Value="102" Text="<div title='Saldos de Cotistas Consolidado (Ordena por Código)'>Saldos de Cotistas Consolidado (Ordena por Código)</div>" />
                                <dxe:ListEditItem Value="103" Text="<div title='Saldos de Cotistas Consolidado (Ordena por Nome)'>Saldos de Cotistas Consolidado (Ordena por Nome)</div>" />
                                <dxe:ListEditItem Value="104" Text="<div title='Extrato de Cotista'>Extrato de Cotista</div>" />
                                <dxe:ListEditItem Value="104" Text="<div title='Extrato de Cotista (com Carteira)'>Extrato de Cotista (com Carteira)</div>" />
                                <dxe:ListEditItem Value="110" Text="<div title='Movimento de Aplic/Resg Cotista'>Movimento de Aplic/Resg Cotista</div>" />
                                <dxe:ListEditItem Value="115" Text="<div title='Notas de Aplic/Resg Cotista'>Notas de Aplic/Resg Cotista</div>" />
                                <dxe:ListEditItem Value="150" Text="<div title='Extrato de Conta-Corrente'>Extrato de Conta-Corrente</div>" />
                                <dxe:ListEditItem Value="170" Text="<div title='Apuração de Ganho de RV'>Apuração de Ganho de RV</div>" />
                                <dxe:ListEditItem Value="200" Text="<div title='Informe Consolidado de Carteiras'>Informe Consolidado de Carteiras</div>" />
                                <dxe:ListEditItem Value="300" Text="<div title='Operações de Bolsa'>Operações de Bolsa</div>" />
                                <dxe:ListEditItem Value="310" Text="<div title='Operações de BMF'>Operações de BMF</div>" />
                                <dxe:ListEditItem Value="320" Text="<div title='Operações de Bolsa'>Operações de Renda Fixa</div>" />
                                <dxe:ListEditItem Value="330" Text="<div title='Operações de Bolsa'>Operações de Fundos</div>" />
                                <dxe:ListEditItem Value="500" Text="<div title='Enquadramento (Desenquadrados)'>Enquadramento (Desenquadrados)</div>" />
                                <dxe:ListEditItem Value="501" Text="<div title='Enquadramento (Todos)'>Enquadramento (Todos)</div>" />
                                <dxe:ListEditItem Value="502" Text="<div title='Enquadramento Carteira Individual'>Enquadramento Carteira Individual</div>" />
                            </Items>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False">
                    </dxwgv:GridViewDataTextColumn >
                </Columns>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                    <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
                </SettingsCommandButton>
                                
            </dxwgv:ASPxGridView>            
            </div>

      
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSBookRelatorio" runat="server" OnesSelect="EsDSBookRelatorio_esSelect" />
    <cc1:esDataSource ID="EsDSBook" runat="server" OnesSelect="EsDSBook_esSelect" />        
        
    </form>
</body>
</html>