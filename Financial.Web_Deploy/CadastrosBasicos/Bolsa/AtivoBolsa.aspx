﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_AtivoBolsa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataEmissor(data) {
        document.getElementById(gridCadastro.cpHiddenIdEmissor).value = data;
        ASPxCallback2.SendCallback(data);
        popupEmissor.HideWindow();
        btnEditEmissor.Focus();
    }        
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditEmissor.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Ativos de Bolsa"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="CdAtivoBolsa"
                                        DataSourceID="EsDSAtivoBolsa" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" OnCustomJSProperties="gridCadastro_CustomJSProperties">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBolsa" Caption="Código" Width="15%"
                                                VisibleIndex="1" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Especificacao" Caption="Espec." Width="7%"
                                                VisibleIndex="2" Settings-AutoFilterCondition="Contains" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="25%"
                                                VisibleIndex="3" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoIsin" Caption="ISIN" Width="10%" VisibleIndex="4"
                                                Settings-AutoFilterCondition="Contains" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoCusip" Caption="COSIP" Width="10%"
                                                VisibleIndex="5" Settings-AutoFilterCondition="Contains" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoMercado" Caption="Mercado" VisibleIndex="6"
                                                Width="10%" ExportWidth="130">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="VIS" Text="<div title='Mercado Vista'>Merc. Vista</div>" />
                                                        <dxe:ListEditItem Value="OPC" Text="<div title='Opção Compra'>Opção Compra</div>" />
                                                        <dxe:ListEditItem Value="OPV" Text="<div title='Opção Venda'>Opção Venda</div>" />
                                                        <dxe:ListEditItem Value="TER" Text="<div title='Termo'>Termo</div>" />
                                                        <dxe:ListEditItem Value="FUT" Text="<div title='Futuro'>Futuro</div>" />
                                                        <dxe:ListEditItem Value="IMO" Text="<div title='Imobiliário'>Imobiliário</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" Width="10%"
                                                VisibleIndex="7" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Moeda" FieldName="IdMoeda" VisibleIndex="8"
                                                Width="10%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSMoeda" TextField="Nome" ValueField="IdMoeda" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="PUExercicio" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBolsaObjeto" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoIsin" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdEmissor" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="NomeEmissor" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdMoeda" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="OpcaoIndice" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdEstrategia" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdLocalCustodia" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdClearing" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdLocalNegociacao" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="Participacao" Visible="false" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCdAtivoBolsa" runat="server" AssociatedControlID="textCdAtivoBolsa"
                                                                    CssClass="labelRequired" Text="Código:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textCdAtivoBolsa" ClientInstanceName="textCdAtivoBolsa" runat="server"
                                                                    CssClass="textNormal_5" MaxLength="20" Text='<%# Eval("CdAtivoBolsa") %>' OnInit="textCdAtivoBolsa_Init" />                                  
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelEspecificacao" runat="server" AssociatedControlID="textEspecificacao"
                                                                    CssClass="labelRequired" Text="Espec.:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textEspecificacao" ClientInstanceName="textEspecificacao" runat="server"
                                                                    CssClass="textCurto" MaxLength="3" Text='<%#Eval("Especificacao")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDescricao" runat="server" AssociatedControlID="textDescricao"
                                                                    CssClass="labelNormal" Text="Descrição:" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textDescricao" runat="server" CssClass="textNormal" MaxLength="200"
                                                                    Text='<%#Eval("Descricao")%>'></asp:TextBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTipoMercado" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoMercado"
                                                                    Text="Mercado:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoMercado" runat="server" CssFilePath="../../css/forms.css"
                                                                    CssClass="dropDownListCurto" Text='<%#Eval("TipoMercado")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="VIS" Text="Merc. Vista" />
                                                                        <dxe:ListEditItem Value="OPC" Text="Opção Compra" />
                                                                        <dxe:ListEditItem Value="OPV" Text="Opção Venda" />
                                                                        <dxe:ListEditItem Value="TER" Text="Termo" />
                                                                        <dxe:ListEditItem Value="FUT" Text="Futuro" />
                                                                        <dxe:ListEditItem Value="IMO" Text="Imobiliário" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataVencimento" runat="server" CssClass="labelNormal" Text="Vencimento:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento"
                                                                    Value='<%#Eval("DataVencimento")%>' />
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPUExercicio" runat="server" CssClass="labelNormal" Text="PU Exerc:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textPUExercicio" runat="server" ClientInstanceName="textPUExercicio"
                                                                    SpinButtons-ShowIncrementButtons="false" CssFilePath="../../css/forms.css" CssClass="textValor_5"
                                                                    Text='<%# Eval("PUExercicio") %>' NumberType="Float" MaxLength="9" DecimalPlaces="2">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelEmissor" runat="server" CssClass="labelRequired" Text="Emissor:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="hiddenIdEmissor" runat="server" CssClass="hiddenField" Text='<%#Eval("IdEmissor")%>' />
                                                                <dxe:ASPxButtonEdit ID="btnEditEmissor" runat="server" CssClass="textButtonEdit5"
                                                                    ClientInstanceName="btnEditEmissor" Text='<%#Eval("NomeEmissor")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupEmissor.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelMoeda" runat="server" CssClass="labelRequired" Text="Moeda:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropMoeda" runat="server" DataSourceID="EsDSMoeda" ValueField="IdMoeda"
                                                                    TextField="Nome" CssFilePath="../../css/forms.css" CssClass="dropDownListCurto"
                                                                    Text='<%#Eval("IdMoeda")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCdAtivoBolsaObjeto" runat="server" AssociatedControlID="textCdAtivoBolsaObjeto"
                                                                    CssClass="labelNormal" Text="Cód. Objeto:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textCdAtivoBolsaObjeto" runat="server" CssClass="textNormal_5" MaxLength="14"
                                                                    Text='<%#Eval("CdAtivoBolsaObjeto")%>'></asp:TextBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCodigoIsin" runat="server" AssociatedControlID="textCodigoIsin"
                                                                    CssClass="labelNormal" Text="Cód. Isin:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textCodigoIsin" runat="server" CssClass="textNormal_5" MaxLength="12"
                                                                    Text='<%#Eval("CodigoIsin")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Estratégia:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropEstrategia" runat="server" ClientInstanceName="dropEstrategia"
                                                                    DataSourceID="EsDSEstrategia" ShowShadow="False" DropDownStyle="DropDown" CssClass="dropDownListCurto"
                                                                    TextField="Descricao" ValueField="IdEstrategia" Text='<%# Eval("IdEstrategia") %>'
                                                                    ValueType="System.String">
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label2" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoPapel"
                                                                    Text="Tipo Papel:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoPapel" runat="server" CssFilePath="../../css/forms.css"
                                                                    CssClass="dropDownList" Text='<%#Eval("TipoPapel")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Normal" />
                                                                        <dxe:ListEditItem Value="2" Text="Bônus Subscrição" />
                                                                        <dxe:ListEditItem Value="3" Text="Recibo Subscrição" />
                                                                        <dxe:ListEditItem Value="4" Text="Fundo ETF" />
                                                                        <dxe:ListEditItem Value="5" Text="Certif. Depósito Ações" />
                                                                        <dxe:ListEditItem Value="6" Text="Recibo Depósito Ações" />
                                                                        <dxe:ListEditItem Value="7" Text="BDR Nível I" />
                                                                        <dxe:ListEditItem Value="8" Text="BDR Nível II" />
                                                                        <dxe:ListEditItem Value="9" Text="BDR Nível III" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label3" runat="server" AssociatedControlID="textNumeroSerie" CssClass="labelNormal"
                                                                    Text="Nr Série:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textNumeroSerie" runat="server" CssClass="textNormal_5" MaxLength="50"
                                                                    Text='<%#Eval("NumeroSerie")%>'></asp:TextBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Cód CUSIP:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textCodigoCusip" runat="server" CssClass="textNormal_5" MaxLength="9"
                                                                    Text='<%#Eval("CodigoCusip")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblLocalCustodia" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalCustodia"
                                                                    Text="Local Custódia:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropLocalCustodia" runat="server" DataSourceID="EsDSLocalCustodia"
                                                                    ValueField="IdLocalCustodia" TextField="Descricao" CssClass="dropDownList" Text='<%#Eval("IdLocalCustodia")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblLocalNegociacao" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalNegociacao"
                                                                    Text="Local Negociação:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropLocalNegociacao" runat="server" DataSourceID="EsDSLocalNegociacao"
                                                                    ValueField="IdLocalNegociacao" TextField="Descricao" CssClass="dropDownList"
                                                                    Text='<%#Eval("IdLocalNegociacao")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblClearing" runat="server" CssClass="labelRequired" AssociatedControlID="dropClearing"
                                                                    Text="Clearing:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropClearing" runat="server" DataSourceID="EsDSClearing" ValueField="IdClearing"
                                                                    TextField="Descricao" CssClass="dropDownList" Text='<%#Eval("IdClearing")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblCodigoCDA" runat="server" CssClass="labelNormal" Text="Codigo CDA:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textCodigoCDA" runat="server" CssClass="textValor_4" ClientInstanceName="textCodigoCDA"
                                                                    MaxLength="16" NumberType="Integer" Text='<%#Eval("CodigoCDA")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblCodigoBDS" runat="server" CssClass="labelNormal" Text="Codigo BDS:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textCodigoBDS" runat="server" CssClass="textValor_4" ClientInstanceName="textCodigoBDS"
                                                                    MaxLength="16" NumberType="Integer" Text='<%#Eval("CodigoBDS")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCheck" runat="server" CssClass="labelNormal" Text="Participação:"></asp:Label>
                                                            </td>                                                            
                                                            <td>
                                                                <dxe:ASPxCheckBox ID="chkParticipacao" runat="server" Text="" />
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="Label36" runat="server" CssClass="labelNormal" Text="Investimento Coletivo – CVM:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropInvestimentoColetivoCvm" runat="server" ClientInstanceName="dropInvestimentoColetivoCvm"
                                                                    ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("InvestimentoColetivoCvm") %>'
                                                                    ValueType="System.String">
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal12" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="500px" />
                                        <ClientSideEvents BeginCallback="function(s, e) {
                                            if (e.command == 'CUSTOMCALLBACK') {                            
                                                isCustomCallback = true;
                                            }
                                        }" EndCallback="function(s, e) {
                                            if (isCustomCallback) {
                                                isCustomCallback = false;
                                                s.Refresh();
                                            }
                                        }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSEmissor" runat="server" OnesSelect="EsDSEmissor_esSelect" />
        <cc1:esDataSource ID="EsDSMoeda" runat="server" OnesSelect="EsDSMoeda_esSelect" />
        <cc1:esDataSource ID="EsDSEstrategia" runat="server" OnesSelect="EsDSEstrategia_esSelect" />
        <cc1:esDataSource ID="EsDSLocalCustodia" runat="server" OnesSelect="EsDSLocalCustodia_esSelect" />
        <cc1:esDataSource ID="EsDSClearing" runat="server" OnesSelect="EsDSClearing_esSelect" />
        <cc1:esDataSource ID="EsDSLocalNegociacao" runat="server" OnesSelect="EsDSLocalNegociacao_esSelect" />
    </form>
</body>
</html>
