﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_AjustePosicaoEmprestimoBolsa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="Atatika.Web.UI" Namespace="Atatika.Web.UI" TagPrefix="atk" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown; 
           
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }    
        
    function FechaPopupEmprestimoBolsa()
    {
        textQuantidade.SetEnabled(false); 
        textQuantidade.SetText(''); 
        textNrContrato.SetEnabled(false); 
        textNrContrato.SetText('');
        textTaxa.SetEnabled(false); 
        textTaxa.SetText(''); 
        textDataRegistro.SetEnabled(false); 
        textDataRegistro.SetText(''); 
        textDataVencimento.SetEnabled(false); 
        textDataVencimento.SetText('');         
        popupPosicaoEmprestimoBolsa.HideWindow();         
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            var textNomeCliente = document.getElementById('textNomeCliente');
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <asp:TextBox ID="textMsgNaoExiste" runat="server" style="display:none" Text="Inexistente"/>
    <asp:TextBox ID="textMsgInativo" runat="server" style="display:none" Text="Inativo"/>
    <asp:TextBox ID="textMsgUsuarioSemAcesso" runat="server" style="display:none" Text="Usuário sem acesso!"/>
    
    <dxpc:ASPxPopupControl ID="popupMensagemCliente" ShowHeader="false" PopupElementID="btnEditCodigoCliente" CloseAction="OuterMouseClick" 
            ForeColor="Red" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" Left="100"
            PopupAction="None" HeaderText="" runat="server">
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupCliente" runat="server" Width="500px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridCliente" runat="server" Width="100%"
                    ClientInstanceName="gridCliente"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSCliente" KeyFieldName="IdCliente"
                    OnCustomDataCallback="gridCliente_CustomDataCallback" 
                    OnHtmlRowCreated="gridCliente_HtmlRowCreated">               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdCliente" ReadOnly="True" VisibleIndex="0" Width="20%">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="1" Width="80%">                    
                </dxwgv:GridViewDataTextColumn>                                
            </Columns>            
            <Settings ShowFilterRow="True" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            <ClientSideEvents RowDblClick="function(s, e) {
            gridCliente.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCliente);}" Init="function(s, e) {
	        e.cancel = true;
	        }"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>                    
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Cliente" />
            </dxwgv:ASPxGridView>    
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCliente.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupPosicaoEmprestimoBolsa" runat="server" Width="500px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="Center" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridPosicaoEmprestimoBolsa" runat="server" Width="100%"
                    ClientInstanceName="gridPosicaoEmprestimoBolsa"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSPosicaoEmprestimoBolsa" KeyFieldName="IdPosicao"                    
                    OnCustomCallback="gridPosicaoEmprestimoBolsa_CustomCallback"
                    OnHtmlRowCreated="gridPosicaoEmprestimoBolsa_HtmlRowCreated"
                    OnCustomUnboundColumnData="gridPosicaoEmprestimoBolsa_CustomUnboundColumnData">               
            <Columns>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="IdPosicao" Visible="false">
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="IdOperacao" Visible="false">
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="PontaEmprestimo" Visible="false">
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" ReadOnly="true" VisibleIndex="8" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataColumn FieldName="PontaEmprestimoDescricao" UnboundType="String" ReadOnly="true" Caption="Tipo" VisibleIndex="4" Width="12%">
                </dxwgv:GridViewDataColumn>
                <dxwgv:GridViewDataColumn FieldName="CdAtivoBolsa" ReadOnly="true" Caption="Código" VisibleIndex="5" Width="10%">                    
                </dxwgv:GridViewDataColumn>
                <dxwgv:GridViewDataDateColumn FieldName="DataRegistro" ReadOnly="true" Caption="Registro" VisibleIndex="6" Width="11%">
                </dxwgv:GridViewDataDateColumn>
                <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" ReadOnly="true" Caption="Vencimento" VisibleIndex="7" Width="11%">
                </dxwgv:GridViewDataDateColumn>                
                <dxwgv:GridViewDataSpinEditColumn FieldName="TaxaOperacao" ReadOnly="true" Caption="Taxa" VisibleIndex="9" Width="8%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataColumn FieldName="NumeroContrato" ReadOnly="true" Caption="Nr Contrato" VisibleIndex="10" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                </dxwgv:GridViewDataColumn>
                
            </Columns>            
            <Settings ShowTitlePanel="True" ShowFilterRow="True" VerticalScrollBarMode="Visible" VerticalScrollableHeight="230" />
            <SettingsPager PageSize="1000"></SettingsPager>
            <SettingsBehavior ColumnResizeMode="Disabled" AllowFocusedRow="true" />            
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posições de Empréstimo" />
            
            <ClientSideEvents RowDblClick="function(s, e) {textQuantidade.SetEnabled(true); 
                                                           textQuantidade.SetText('');
                                                           textQuantidade.Focus();
                                                           textNrContrato.SetEnabled(true); 
                                                           textNrContrato.SetText('');
                                                           textTaxa.SetEnabled(true); 
                                                           textTaxa.SetText('');
                                                           textDataRegistro.SetEnabled(true); 
                                                           textDataRegistro.SetText('');
                                                           textDataVencimento.SetEnabled(true); 
                                                           textDataVencimento.SetText('');
                                                           selectedIndex = e.visibleIndex; }" />            
            
            </dxwgv:ASPxGridView>
        
            <div>
                <table>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="labelAjustar" runat="server" CssClass="labelNormal" Text="(Deixe em branco os campos que não for ajustar)" Style="color: #58607C" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="labelQtde" runat="server" CssClass="labelNormal" Text="Quantidade:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor" ClientInstanceName="textQuantidade"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="20" NumberType="Float" DecimalPlaces="8" >
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Taxa:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textTaxa" runat="server" CssClass="textValor" ClientInstanceName="textTaxa"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="8" NumberType="Float" DecimalPlaces="2" >
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Registro:"></asp:Label>
                        </td>
                        
                        <td>
                            <dxe:ASPxDateEdit ID="textDataRegistro" runat="server" ClientInstanceName="textDataRegistro" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Vencimento:"></asp:Label>
                        </td>
                        
                        <td>
                            <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Nr Contrato:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textNrContrato" runat="server" CssClass="textValor" ClientInstanceName="textNrContrato"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="10" NumberType="Integer" DecimalPlaces="0" >
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    
                    
                    <tr>                    
                    </tr>
                </table>
                <div class="linkButton" style="background:none; border: none">
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"  
                                                   OnClientClick="gridPosicaoEmprestimoBolsa.PerformCallback(selectedIndex); return false;"><asp:Literal ID="Literal1" runat="server" Text="Salvar"/><div></div></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                CssClass="btnCancel" OnClientClick="FechaPopupEmprestimoBolsa(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Fechar"/><div></div></asp:LinkButton>
                            </div>
                    
            </div>
            
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents PopUp="function(s, e) {gridPosicaoEmprestimoBolsa.PerformCallback(); 
                                                 textQuantidade.SetEnabled(false);
                                                 textNrContrato.SetEnabled(false);
                                                 textTaxa.SetEnabled(false);
                                                 textDataRegistro.SetEnabled(false);
                                                 textDataVencimento.SetEnabled(false);}"
                          CloseUp="function(s, e) {FechaPopupEmprestimoBolsa(); return false;}" />   
    </dxpc:ASPxPopupControl>
                                        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Ajuste de Posições de Empréstimo de Bolsa"></asp:Label>
    </div>
        
    <div id="mainContent">
            
        <atk:AKRequiredValidator ID="RequiredValidator1" runat="server" ValidationGroup="ATK" >            
        </atk:AKRequiredValidator>
        
        <div class="reportFilter">
        
        <div class="dataMessage">
        <asp:ValidationSummary ID="validationSummary" runat="server" 
                         HeaderText="Campos com * são obrigatórios." EnableClientScript="true"
                         DisplayMode="SingleParagraph" ShowSummary="true" ValidationGroup="ATK"/>
        </div>
        
        <table cellpadding="3">
        
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" 
                                    ClientInstanceName="btnEditCodigoCliente">  
                <Buttons>
                    <dxe:EditButton>
                    </dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCliente').value = '';}" 
                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td>
                <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
            </td>                        
            <td>
                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" />              
            </td>         
            
            <td class="tdButton" width="170">
                <div class="linkButton linkButtonNoBorder">
                <asp:LinkButton ID="btnPosicaoEmprestimoBolsa" ForeColor="black" runat="server" CssClass="btnPopup" ValidationGroup="ATK" OnClientClick="if(Page_ClientValidate()) popupPosicaoEmprestimoBolsa.ShowAtElementByID(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Ver Posições"/><div></div></asp:LinkButton>
                </div>
            </td>         
                                                                                        
            </tr>            
        </table>
        
        </div>
        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <cc1:esDataSource ID="EsDSPosicaoEmprestimoBolsa" runat="server" OnesSelect="EsDSPosicaoEmprestimoBolsa_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />    
    
    </form>
</body>
</html>