﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_PerfilCorretagemBolsa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    var operacao = '';
          
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        btnEditCodigoCliente.Focus();
    }      
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome);                        
            }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackClone" runat="server" OnCallback="callbackClone_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {    
            alert('Clonagem executada');
            gridCadastro.UpdateEdit();
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Perfis de Corretagem (Bolsa)"></asp:Label>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" 
                KeyFieldName="CompositeKey" DataSourceID="EsDSPerfilCorretagemBolsa"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                OnCustomJSProperties="gridCadastro_CustomJSProperties" >        
                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataColumn FieldName="CompositeKey" UnboundType="String" Visible="false">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="15%">                    
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgente" Caption="Corretora" VisibleIndex="3" Width="15%">
                    <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente">
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="4" Width="10%"/>
                
                <dxwgv:GridViewDataComboBoxColumn Caption="Mercado" FieldName="TipoMercado" VisibleIndex="4" Width="10%">
                        <PropertiesComboBox EncodeHtml="false">
                        <Items>                        
                            <dxe:ListEditItem Value="" Text="" />
                            <dxe:ListEditItem Value="VIS" Text="<div title='Ações'>Ações</div>" />
                            <dxe:ListEditItem Value="OPC" Text="<div title='Opção de Compra'>Opção de Compra</div>" />
                            <dxe:ListEditItem Value="OPV" Text="<div title='Opção de Venda'>Opção de Venda</div>" />
                            <dxe:ListEditItem Value="TER" Text="<div title='Termo'>Termo</div>" />          
                        </Items>
                        </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="IdTemplate" Caption="Template" VisibleIndex="5" Width="15%">
                    <PropertiesComboBox DataSourceID="EsDSTemplateCorretagemBolsa" TextField="Descricao" ValueField="IdTemplate">
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="PercentualDesconto" Caption="% Desconto" VisibleIndex="6" Width="8%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                                                
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="ISS" Caption="% ISS" VisibleIndex="8" Width="8%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                                                
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                </td>        
                                                            
                                <td>                        
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" ClientInstanceName="btnEditCodigoCliente"
                                                                Text='<%# Eval("IdCliente") %>' MaxLength="10" NumberType="Integer" 
                                                                OnLoad="btnEditCodigoCliente_Load">            
                                    <Buttons>
                                        <dxe:EditButton>
                                        </dxe:EditButton>                                
                                    </Buttons>  
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                             ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                            />               
                                    </dxe:ASPxSpinEdit>
                                </td> 
                                    
                                <td>
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                                </td>                        
                            </tr>
                            
                            <tr>                                        
                                <td class="td_Label">
                                    <asp:Label ID="labelAgente" runat="server" CssClass="labelRequired" AssociatedControlID="dropAgente" Text="Corretora:"/>                                    
                                </td>                                                    
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropAgente" runat="server" DataSourceID="EsDSAgenteMercado"
                                                     ValueField="IdAgente" TextField="Nome"
                                                     CssClass="dropDownList" Text='<%#Eval("IdAgente")%>' OnLoad="dropAgente_Load">
                                    </dxe:ASPxComboBox>                                                                               
                                </td>    
                            </tr>    
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Dt Referência:"  /></td> 
                                <td colspan="2">
                                    <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>' OnInit="textDataReferencia_Init"/>
                                </td>  
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTipoMercado" runat="server" CssClass="labelNormal" Text="Mercado:"/>
                                </td>                    
                                <td colspan="2">      
                                    <dxe:ASPxComboBox ID="dropTipoMercado" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                        CssClass="dropDownListCurto" Text='<%#Eval("TipoMercado")%>' OnLoad="dropTipoMercado_Load">
                                    <Items>
                                    <dxe:ListEditItem Value="" Text="" />
                                    <dxe:ListEditItem Value="VIS" Text="Ações" />
                                    <dxe:ListEditItem Value="OPC" Text="Opção de Compra" />
                                    <dxe:ListEditItem Value="OPV" Text="Opção de Venda" />
                                    <dxe:ListEditItem Value="TER" Text="Termo" />                                        
                                    </Items>                                               
                                    <ClientSideEvents KeyDown="function(s, e) {if (e.htmlEvent.keyCode == 46) {s.SetSelectedIndex(-1); } }" />                             
                                    </dxe:ASPxComboBox>                 
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTemplate" runat="server" CssClass="labelRequired" AssociatedControlID="dropTemplate" Text="Template:">
                                    </asp:Label>
                                </td>                                                    
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropTemplate" runat="server" DataSourceID="EsDSTemplateCorretagemBolsa"
                                                     ValueField="IdTemplate" TextField="Descricao" CssClass="dropDownList" Text='<%#Eval("IdTemplate")%>'>
                                    </dxe:ASPxComboBox>                                                                               
                                </td>    
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelPercentualDesconto" runat="server" CssClass="labelRequired" Text="% Desconto:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <dxe:ASPxSpinEdit ID="textPercentualDesconto" runat="server" ClientInstanceName="textPercentualDesconto"
                                        Text='<%# Eval("PercentualDesconto") %>' NumberType="Float" MaxLength="5" DecimalPlaces="2"
                                        CssClass="textPercCurto">
                                    </dxe:ASPxSpinEdit>                                
                                </td>                                                        
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelISS" runat="server" CssClass="labelRequired" Text="% ISS:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <dxe:ASPxSpinEdit ID="textISS" runat="server" ClientInstanceName="textISS"
                                        Text='<%# Eval("ISS") %>' NumberType="Float" MaxLength="4" DecimalPlaces="2"
                                        CssClass="textPercCurto">
                                    </dxe:ASPxSpinEdit>                                
                                </td>                                                        
                            </tr>
                            
                        </table>
                        
                        <div class="linkButton linkButtonNoBorder" style="margin-left:80px;">
                        <asp:LinkButton ID="btnClone" runat="server" Font-Overline="false" CssClass="btnCopy" OnInit="btnClone_Init"
                                                OnClientClick="callbackClone.SendCallback(); return false;">
                                <asp:Literal ID="Literal8" runat="server" Text="Clonar p/ Clientes Ativos"/><div></div></asp:LinkButton>
                        </div>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                        
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>

                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal6" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="250px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSPerfilCorretagemBolsa" runat="server" OnesSelect="EsDSPerfilCorretagemBolsa_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSTemplateCorretagemBolsa" runat="server" OnesSelect="EsDSTemplateCorretagemBolsa_esSelect" />
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
    </form>
</body>
</html>