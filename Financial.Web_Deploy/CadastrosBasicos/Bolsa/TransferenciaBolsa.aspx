﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_TransferenciaBolsa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;        
    var operacao = '';
    
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        btnEditCodigoCliente.Focus();
    }  
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }   
    function OnGetDataAtivoBolsa(data) {
        btnEditAtivoBolsa.SetValue(data);
        popupAtivoBolsa.HideWindow();
        ASPxCallback2.SendCallback(btnEditAtivoBolsa.GetValue());
        btnEditAtivoBolsa.Focus();
    }        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else                
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }          
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (gridCadastro.cp_EditVisibleIndex == -1) {  
                var resultSplit = e.result.split('|');                        
                e.result = resultSplit[0];    
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            }
            else {  
                var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');                               
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome, textData);                        
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result == '' && btnEditAtivoBolsa.GetValue() != null)
            {
                popupMensagemAtivoBolsa.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBolsa.ShowAtElementByID(btnEditAtivoBolsa.name);
                btnEditAtivoBolsa.SetValue(''); 
                btnEditAtivoBolsa.Focus();                 
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Transferência de Bolsa"/>
    </div>
        
    <div id="mainContent">
                   
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                        Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                        HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                
                <table>  
                    <tr>
                        <td class="td_Label_Longo">
                            <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                        </td>        
                        
                        <td>                                                                                                                
                            <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                        ClientInstanceName="btnEditCodigoClienteFiltro" EnableClientSideAPI="true"
                                        MaxLength="10" NumberType="Integer">            
                            <Buttons>                                           
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </Buttons>       
                            <ClientSideEvents                                                           
                                     KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                     ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                    />               
                            </dxe:ASPxSpinEdit>
                        </td>
                    
                        <td  colspan="2" width="450">
                            <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>
                          
                    <tr>
                        <td>                
                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                        </td>    
                                            
                        <td>
                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />                                      
                        </td>  
                        
                        <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                        </td>                                                                      
                    </tr>
                </table>
                                               
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>        
        
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal5" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
        </div>
        
        <div class="divDataGrid">    
            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdTransferencia" DataSourceID="EsDSTransferenciaBolsa"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnPreRender="gridCadastro_PreRender"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties">
                                         
                <Columns>                    
                                         
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="5%" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="25%"/>                    
                    
                    <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="3" Width="10%"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="CdAtivoBolsa" Caption="Código" VisibleIndex="4" Width="10%"/>                    
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgenteOrigem" Caption="Origem" VisibleIndex="5" Width="14%">
                        <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgenteDestino" Caption="Destino" VisibleIndex="6" Width="14%">
                        <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="7" Width="12%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            GroupFooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdAgente" Visible="False"/>                    
                    
                    <dxwgv:GridViewDataColumn FieldName="PU" Visible="False"/>                    
                                    
                </Columns>
                
                <Templates>            
                <EditForm>
                    <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                    
                    <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
            
                    <div class="editForm">

                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                </td>                            
                                
                                <td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" ClientInstanceName="btnEditCodigoCliente" EnableClientSideAPI="true"
                                                      CssClass="textButtonEdit" Text='<%# Eval("IdCliente") %>' MaxLength="10" NumberType="Integer">
                                    <Buttons>
                                        <dxe:EditButton></dxe:EditButton>                                
                                    </Buttons>  
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                             ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                            />               
                                    </dxe:ASPxSpinEdit>
                                </td>
                        
                                <td colspan="2">
                                  <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                                </td>                                        
                            </tr>

                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelData" runat="server" CssClass="labelNormal" Text="Lançamento:"  />
                                </td> 
                                <td>
                                   <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" ClientEnabled="false" Value='<%#Eval("Data")%>' CssClass="textData" ValidationSettings-ValidationGroup="ATK" />
                                </td>  
                                
                                <td align="right">
                                    <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>                        
                                </td>
                                <td>
                                    <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa" runat="server" Width="100%" CssClass="textAtivoCurto" MaxLength="25" 
                                                        EnableClientSideAPI="True" ClientInstanceName="btnEditAtivoBolsa" Text="<%#Bind('CdAtivoBolsa')%>" >            
                                    <Buttons>
                                        <dxe:EditButton></dxe:EditButton>                
                                    </Buttons>           
                                    <ClientSideEvents                              
                                         ButtonClick="function(s, e) {popupAtivoBolsa.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {popupMensagemAtivoBolsa.HideWindow(); 
                                                                    if (btnEditAtivoBolsa.GetValue() != null)
                                                                    {
                                                                        ASPxCallback2.SendCallback(btnEditAtivoBolsa.GetValue());
                                                                    }
                                                                    }"
                                    />                    
                                    </dxe:ASPxButtonEdit>                
                                </td>                                                                                                       
                            </tr>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelAgenteOrigem" runat="server" CssClass="labelRequired" Text="Origem:"></asp:Label>                    </td>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                                 </td>                 
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropAgenteOrigem" runat="server" DataSourceID="EsDSAgenteMercado" 
                                                        ShowShadow="false" IncrementalFilteringMode="Contains"  DropDownStyle="DropDown"
                                                        CssClass="dropDownList" TextField="Nome" ValueField="IdAgente"
                                                        Text='<%#Eval("IdAgenteOrigem")%>'>
                                            
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>                              
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelAgenteDestino" runat="server" CssClass="labelRequired" Text="Destino:"></asp:Label>                    </td>
                                </td>
                                <td colspan="3">                    
                                
                                    <dxe:ASPxComboBox ID="dropAgenteDestino" runat="server" DataSourceID="EsDSAgenteMercado" 
                                                        ShowShadow="false" IncrementalFilteringMode="Contains"  DropDownStyle="DropDown"
                                                        CssClass="dropDownList"                                                 
                                                        TextField="Nome" ValueField="IdAgente"
                                                        Text='<%#Eval("IdAgenteDestino")%>'>
                                            <ClientSideEvents LostFocus="function(s, e) { 
                                                                                        if(s.GetSelectedIndex() == -1)
                                                                                            s.SetText(null); } "/>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Quantidade:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" ClientInstanceName="textQuantidade" 
                                    CssClass="textValor_5"
                                    Text='<%# Eval("Quantidade") %>' NumberType="Integer"  
                                    MaxLength="10" DecimalPlaces="0">
                                    </dxe:ASPxSpinEdit>
                                </td>                                  
                                
                                <td align="right">
                                    <asp:Label ID="labelPU" runat="server" CssClass="labelRequired" Text="PU:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textPU" runat="server" ClientInstanceName="textPU"                                    
                                    Text='<%# Eval("PU") %>' NumberType="Float" CssClass="textValor_5"
                                    MaxLength="16" DecimalPlaces="10">
                                    </dxe:ASPxSpinEdit>
                                </td>                                  
                            </tr>
                        </table>
                        
                        <div class="linhaH"></div>
                        
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init" 
                                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK+"/><div></div></asp:LinkButton>        
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                               OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal13" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                        
                    </div>     
                    
                    </asp:Panel>                                   
                </EditForm>
                
                <StatusBar>
                    <div>
                        <div style="float:left">
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                        </div>                    
                    </div>
                </StatusBar>            
                
                </Templates>
                
                <SettingsPopup EditForm-Width="550" />                        
                
            </dxwgv:ASPxGridView>
            
        </div>                      
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSTransferenciaBolsa" runat="server" OnesSelect="EsDSTransferenciaBolsa_esSelect" LowLevelBind="true" />    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />    
    <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />    
    
    </form>
</body>
</html>