﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_BloqueioBolsa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;        
    var operacao = '';
    
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        btnEditCodigoCliente.Focus();
    }  
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }   
    function OnGetDataAtivoBolsa(data) {
        btnEditAtivoBolsa.SetValue(data);
        popupAtivoBolsa.HideWindow();
        ASPxCallback2.SendCallback(btnEditAtivoBolsa.GetValue());
        btnEditAtivoBolsa.Focus();
    }        
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else
                {                
                    gridCadastro.UpdateEdit();                      
                }                     
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var resultSplit = e.result.split('|');                        
                e.result = resultSplit[0];    
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            }
            else 
            {  
                var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');                
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome, textData);
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result == '' && btnEditAtivoBolsa.GetValue() != null)
            {
                popupMensagemAtivoBolsa.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBolsa.ShowAtElementByID(btnEditAtivoBolsa.name);
                btnEditAtivoBolsa.SetValue(''); 
                btnEditAtivoBolsa.Focus();                 
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Bloqueio/Desbloqueio de Bolsa"></asp:Label>
    </div>
        
    <div id="mainContent">
                       
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                        Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                        HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                
                <table>  
                    <tr>
                        <td class="td_Label_Longo">
                            <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                        </td>        
                        
                        <td>                                                                                                                
                            <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                        ClientInstanceName="btnEditCodigoClienteFiltro" EnableClientSideAPI="true"
                                        MaxLength="10" NumberType="Integer">            
                            <Buttons>                                           
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </Buttons>       
                            <ClientSideEvents                                                           
                                     KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                     ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                    />               
                            </dxe:ASPxSpinEdit>
                        </td>
                    
                        <td  colspan="2" width="450">
                            <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>
                          
                    <tr>
                        <td>                
                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                        </td>    
                                            
                        <td>
                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />                                      
                        </td>  
                        
                        <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                        </td>                                                                   
                    </tr>
                </table>        
                
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>        
        
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;"><asp:Literal ID="Literal10" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal6" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal5" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
        
        <div class="divDataGrid">    
            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdBloqueio" DataSourceID="EsDSBloqueioBolsa"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnPreRender="gridCadastro_PreRender"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    >     
                
                <Columns>                    
                                         
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="5%" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="7%" CellStyle-HorizontalAlign="left"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="22%"/>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" VisibleIndex="3" Width="10%"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo" VisibleIndex="4" Width="8%">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="<div title='Bloqueio'>Bloqueio</div>" />
                        <dxe:ListEditItem Value="2" Text="<div title='Desbloqueio'>Desbloqueio</div>" />                            
                    </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="CdAtivoBolsa" Caption="Código" VisibleIndex="5" Width="6%">                    
                    </dxwgv:GridViewDataColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgente" Caption="Agente" VisibleIndex="6" Width="15%">
                        <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="7" Width="8%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            GroupFooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="Observacao" Caption="Observação" VisibleIndex="8" Width="20%" Settings-AutoFilterCondition="Contains" >
                    </dxwgv:GridViewDataColumn>
                                    
                </Columns>
                
                <Templates>            
                <EditForm>
                    <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                    
                    <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
            
                    <div class="editForm">
                        
                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                </td>        
                                
                                <td>                                        
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" ClientInstanceName="btnEditCodigoCliente" EnableClientSideAPI="true"
                                                                            Text='<%# Eval("IdCliente") %>' MaxLength="10" NumberType="Integer">            
                                <Buttons>
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>  
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                        />               
                                </dxe:ASPxSpinEdit>  
                                </td>
                            
                                <td colspan="2">
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textLongo5" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                                </td>
                            </tr>
                            
                            <tr>       
                                <td class="td_Label">
                                    <asp:Label ID="labelData" runat="server" CssClass="labelNormal" Text="Lançamento:"  />
                                </td> 
                                <td>
                                    <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("DataOperacao")%>' ClientEnabled="false" />                                
                                </td> 
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>                    
                                    </td>                    
                                    <td colspan="3">      
                                        <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" 
                                                            ShowShadow="false" ClientInstanceName="dropTipoOperacao"
                                                            CssClass="dropDownListCurto"                                                 
                                                            Text='<%#Eval("TipoOperacao")%>'>                                        
                                        <Items>
                                        <dxe:ListEditItem Value="1" Text="Bloqueio" />
                                        <dxe:ListEditItem Value="2" Text="Desbloqueio" />                                                                
                                        </Items>
                                        </dxe:ASPxComboBox>                                                                 
                                    </td>      
                                </tr> 
                                
                                <td class="td_Label">
                                    <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>                        
                                </td>
                                <td colspan="3">
                                    <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa" runat="server" Width="45%" CssClass="textAtivoCurto" MaxLength="25" 
                                                        ClientInstanceName="btnEditAtivoBolsa" Text="<%#Bind('CdAtivoBolsa')%>" >            
                                    <Buttons>
                                        <dxe:EditButton></dxe:EditButton>                
                                    </Buttons>           
                                    <ClientSideEvents                              
                                         ButtonClick="function(s, e) {popupAtivoBolsa.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {popupMensagemAtivoBolsa.HideWindow(); 
                                                                    if (btnEditAtivoBolsa.GetValue() != null)
                                                                    {
                                                                        ASPxCallback2.SendCallback(btnEditAtivoBolsa.GetValue());
                                                                    }
                                                                    }"
                                    />                    
                                    </dxe:ASPxButtonEdit>                        
                                </td>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>                                                                                                       
                            </tr>                                
                                    
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelAgente" runat="server" CssClass="labelRequired" Text="Agente:"></asp:Label>
                                </td>                 
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropAgente" runat="server" DataSourceID="EsDSAgenteMercado" 
                                                        ShowShadow="false" IncrementalFilteringMode="Contains" DropDownStyle="DropDown"
                                                        CssClass="dropDownList" TextField="Nome" ValueField="IdAgente"
                                                        Text='<%#Eval("IdAgente")%>'>
                                            
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            
                            <tr>
                                <td  class="td_Label">
                                    <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Quantidade:"></asp:Label>
                                </td>
                                <td colspan="3">                                    
                                    <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor_4" ClientInstanceName="textQuantidade"
		                            MaxLength="10" NumberType="integer" Text="<%#Bind('Quantidade')%>">            
                                    </dxe:ASPxSpinEdit>                                    
                                </td>                                  
                            </tr>
                            
                            <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelTipoCarteira" runat="server" CssClass="labelNormal" Text="Tipo Carteira:"></asp:Label>                    
                            </td>                    
                            <td colspan="3"> 
                                <dxe:ASPxComboBox ID="dropTipoCarteira" runat="server" DataSourceID="EsDSTipoCarteira" ClientInstanceName="dropTipoCarteira"
                                                        ShowShadow="false" IncrementalFilteringMode="Contains" DropDownStyle="DropDown"
                                                        CssClass="dropDownListLongo" Text='<%#Eval("TipoCarteiraBloqueada")%>'
                                                        TextField="Descricao" ValueField="IdCarteiraBolsa"      
                                                        >                                              
                                            <ClientSideEvents LostFocus="function(s, e) { 
                                                                                        if(s.GetSelectedIndex() == -1)
                                                                                            s.SetText(null); } "/>
                                </dxe:ASPxComboBox>                                                        
                            </td>      
                        </tr>
                            
                        <tr>
                            <td  class="td_Label">
                                <asp:Label ID="labelObservacao" runat="server" CssClass="labelNormal" Text="Observação:"> </asp:Label>
                            </td>
                            <td colspan="5">
                                <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" Rows="2" CssClass="textLongo5" MaxLength="200" Text='<%#Eval("Observacao")%>'/>
                            </td>                    
                        </tr>
                        
                        </table>
                        
                        
                        <div class="linhaH"></div>
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal2" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" 
                                                        OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                        
                    </div>     
                    
                    </asp:Panel>                                   
                </EditForm>
                
                <StatusBar>
                    <div>
                        <div style="float:left">
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                        </div>                    
                    </div>
                </StatusBar>            
                
                </Templates>
                
                <SettingsPopup EditForm-Width="500" />                        
                
            </dxwgv:ASPxGridView>
            
        </div>
                      
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSBloqueioBolsa" runat="server" OnesSelect="EsDSBloqueioBolsa_esSelect" LowLevelBind="true" />    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />    
    <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />    
    <cc1:esDataSource ID="EsDSTipoCarteira" runat="server" OnesSelect="EsDSTipoCarteira_esSelect" />    
    
    </form>
</body>
</html>