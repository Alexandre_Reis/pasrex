﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_PessoaVinculo, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head2" runat="server">    
       
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>
        
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="Div1">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Cotitular"></asp:Label>
    </div>
           
    <div id="mainContent">
                                 
            <div class="linkButton" >         
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"  CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>               
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="CompositeKey" DataSourceID="EsDSPessoaVinculo"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnPreRender="gridCadastro_PreRender"
                    OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                    
                    <dxwgv:GridViewDataColumn FieldName="IdPessoa" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="Nome" Caption="Nome Pessoa" VisibleIndex="2" Width="30%"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdPessoaVinculada" VisibleIndex="3" Width="10%" CellStyle-HorizontalAlign="left"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="NomeVinculada" Caption="Nome Pessoa Cotitular" VisibleIndex="4" Width="30%"/>
                                    
                </Columns>
                
                <Templates>
            <EditForm>
                                
                <div class="editForm">
                    
                    <table border="0">
                            
                    <tr>
                        <td class="td_Label">
                            <asp:Label ID="labelPessoa" runat="server" CssClass="labelRequired" Text="Pessoa:" />
                        </td>
                        <td>
                           
                           <dx:ASPxGridLookup ID="dropPessoa" ClientInstanceName="dropPessoa" runat="server" SelectionMode="Single" KeyFieldName="IdPessoa" DataSourceID="EsDSPessoa" Value='<%#Eval("IdPessoa")%>'
                                               Width="350px" TextFormatString="{1}" Font-Size="11px" IncrementalFilteringMode="StartsWith" OnInit="dropPessoaOnInit" MaxLength="12" AllowUserInput="false" >
                                 <Columns>
                                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                    <dxwgv:GridViewDataColumn FieldName="IdPessoa" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>
                                    <dxwgv:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small"/>
                                </Columns>
                                
                                <GridViewProperties>
                                    <Settings ShowFilterRow="True" />
                                </GridViewProperties>
                                
                            </dx:ASPxGridLookup>
                        </td>
                    </tr>
                                        
                    <tr>
                        <td class="td_Label">
                            <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Pessoa Cotitular:" />
                        </td>
                        <td>
                           
                           <dx:ASPxGridLookup ID="dropPessoaVinculada" ClientInstanceName="dropPessoaVinculada" runat="server" SelectionMode="Single" KeyFieldName="IdPessoa" DataSourceID="EsDSPessoa" Value='<%#Eval("IdPessoaVinculada")%>'
                                               Width="350px" TextFormatString="{1}" Font-Size="11px" IncrementalFilteringMode="StartsWith" OnInit="dropPessoaVinculadaOnInit" MaxLength="12" AllowUserInput="false">
                                 <Columns>
                                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                    <dxwgv:GridViewDataColumn FieldName="IdPessoa" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small"/>
                                    <dxwgv:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small"/>
                                </Columns>
                                
                                <GridViewProperties>
                                    <Settings ShowFilterRow="True" />
                                </GridViewProperties>
                                
                            </dx:ASPxGridLookup>
                        </td>
                    </tr>
                                        
                    </table>
                
                    <div class="linhaH"></div>
                    
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>

                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                        OnClientClick="operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal3" runat="server" Text="OK"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal7" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>                        
                </div>       
                                
            </EditForm>
            
            <StatusBar>
                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
            </StatusBar>      
            
            </Templates>
            
            <SettingsPopup EditForm-Width="450px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>
            
            </dxwgv:ASPxGridView>
            </div>     
    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape= "true" LeftMargin="40" RightMargin="40" />
    
    <cc1:esDataSource ID="EsDSPessoaVinculo" runat="server" OnesSelect="EsDSPessoaVinculo_esSelect" />
    <cc1:esDataSource ID="EsDSPessoa" runat="server" OnesSelect="EsPessoa_esSelect"  />
        
    </form>
</body>
</html>



