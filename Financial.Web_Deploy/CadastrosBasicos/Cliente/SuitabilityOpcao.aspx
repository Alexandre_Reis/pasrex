﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Cliente_SuitabilityOpcao, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
           
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    
    function showHistorico()
    {
        gridHistorico.PerformCallback();
        popupHistorico.Show();
    }
    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
               
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Suitability Opção"></asp:Label>
    </div>

    <div id="mainContent">
    
            <dxpc:ASPxPopupControl ID="popupHistorico" runat="server" Width="500px" HeaderText=""
                ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                AllowDragging="True">
                <ContentCollection>
                    <dxpc:PopupControlContentControl runat="server">
                        <div>
                            <dxwgv:ASPxGridView ID="gridHistorico" runat="server" Width="100%" ClientInstanceName="gridHistorico" EnableCallBacks="true"
                                AutoGenerateColumns="true" DataSourceID="EsDSSuitabilityHistorico" KeyFieldName="DataHistorico"
                                OnCustomCallback="gridHistorico_CustomCallback">

                                <Settings ShowTitlePanel="True" />
                                <SettingsBehavior ColumnResizeMode="Disabled" />
                                <SettingsDetail ShowDetailButtons="False" />
                                <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                                    <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                </Styles>
                                <Images>
                                    <PopupEditFormWindowClose Height="17px" Width="17px" />
                                </Images>
                                <SettingsText EmptyDataRow="0 Registros" Title="Histórico" />
                            </dxwgv:ASPxGridView>
                        </div>
                    </dxpc:PopupControlContentControl>
                </ContentCollection>
           </dxpc:ASPxPopupControl>
    
           <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnHistorico" runat="server" Font-Overline="false" CssClass="btnCustomFields" OnClientClick="showHistorico(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Histórico"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdOpcao" DataSourceID="EsDSSuitabilityOpcao"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    SettingsBehavior-ColumnResizeMode="Control"
                    >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdQuestao" Caption="Questão" VisibleIndex="2" Width="40%">
                        <PropertiesComboBox DataSourceID="EsDSSuitabilityQuestao" TextField="Descricao" ValueField="IdQuestao" EncodeHtml="false" Width="100%">
                            <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                            <ValidationSettings ErrorText=""></ValidationSettings>
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Opção" Width="30%" VisibleIndex="4">                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataSpinEditColumn FieldName="Pontos" VisibleIndex="6" Width="5%">                                                                    
                        <PropertiesSpinEdit SpinButtons-ShowIncrementButtons="false" DisplayFormatString="n0" MaxLength="10" MaxValue="9999999999">                                
                        </PropertiesSpinEdit>                
                    </dxwgv:GridViewDataSpinEditColumn>                
                    
                </Columns>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                    <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
                </SettingsCommandButton>                
                                
            </dxwgv:ASPxGridView>            
            </div>    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSSuitabilityOpcao" runat="server" OnesSelect="EsDSSuitabilityOpcao_esSelect" />
    <cc1:esDataSource ID="EsDSSuitabilityQuestao" runat="server" OnesSelect="EsDSSuitabilityQuestao_esSelect" />
    <cc1:esDataSource ID="EsDSSuitabilityHistorico" runat="server" OnesSelect="EsDSSuitabilityHistorico_esSelect" />
    </form>
</body>
</html>