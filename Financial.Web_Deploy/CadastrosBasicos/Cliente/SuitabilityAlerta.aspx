﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Cliente_SuitabilityAlerta, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
	Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
	Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
		   
	<link href="~/css/forms.css" rel="stylesheet" type="text/css" />     
	<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
	<script type="text/javascript" language="Javascript">
	var popup = false;
	document.onkeydown=onDocumentKeyDown;
	
    function OnGetDataCotistaFiltro(data) {
        btnEditCodigoCotistaFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCotistaFiltro.GetValue());
        popupCotista.HideWindow();
        btnEditCodigoCotistaFiltro.Focus();
    }
	
	</script>
</head>

<body>
	<form id="form1" runat="server">
	
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                        
        if (gridCadastro.cp_EditVisibleIndex == -1)
        {
            var resultSplit = e.result.split('|');
            e.result = resultSplit[0];
            var textNomeCotistaFiltro = document.getElementById('popupFiltro_textNomeCotistaFiltro');
            OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotistaFiltro, textNomeCotistaFiltro);
        }
        else    
        {
            var textNomeCotista = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotista');
            OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotista, textNomeCotista);
        }
    }        
    " />
    </dxcb:ASPxCallback>
        
	<div class="divPanel">
	<table width="100%"><tr><td>
	<div id="container">
			   
	<div id="header">
		<asp:Label ID="lblHeader" runat="server" Text="Suitability Alertas"></asp:Label>
	</div>

	<div id="mainContent">
	
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
            EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
            runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <table>
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCotistaFiltro" runat="server" CssClass="labelNormal" Text="Cotista:"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCotistaFiltro" runat="server" CssClass="textButtonEdit"
                                    ClientInstanceName="btnEditCodigoCotistaFiltro" EnableClientSideAPI="true" CssFilePath="../../css/forms.css"
                                    SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">
                                    <Buttons>
                                        <dxe:EditButton>
                                        </dxe:EditButton>
                                    </Buttons>
                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCotistaFiltro').value = '';} "
                                        ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback1, btnEditCodigoCotistaFiltro);}" />
                                </dxe:ASPxSpinEdit>
                            </td>
                            <td colspan="2" width="450">
                                <asp:TextBox ID="textNomeCotistaFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:" Visible="false"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" Visible="false"/>
                            </td>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" Visible="false"/></td>
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" Visible="false"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="labelTipo" runat="server" CssClass="labelNormal" Text="Tipo:"></asp:Label>
                            </td>
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropTipoAlertaFiltro" runat="server" ClientInstanceName="dropTipoAlertaFiltro"
                                    ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                    CssClass="dropDownListCurto">
                                    <Items>
                                        <dxe:ListEditItem Value="0" Text="Todos" />
                                        <dxe:ListEditItem Value="1" Text="Perfil expira em até 30 dias" />
                                        <dxe:ListEditItem Value="2" Text="Perfil expirado" />
                                        <dxe:ListEditItem Value="3" Text="Perfil não informado" />
                                        <dxe:ListEditItem Value="4" Text="Desenquadramento" />
                                    </Items>
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                    <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                        <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                            CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                            <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                            </div>
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                            CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                            <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                            </div>
                        </asp:LinkButton>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
	
	
		<div class="linkButton" >
            <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal2" runat="server" Text="Filtro" /><div></div></asp:LinkButton>            
            <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
            <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
            <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
		</div>

		<div class="divDataGrid">            
			<dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
			    OnCustomCallback="gridCadastro_CustomCallback"  OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
				KeyFieldName="IdCotista" DataSourceID="EsDSSuitabilityAlertas"
				SettingsBehavior-ColumnResizeMode="Control">        
					
			<Columns>           
			   
			    <dxwgv:GridViewDataColumn FieldName="IdCotista" Caption="Id Cotista" VisibleIndex="1" Width="5%" ExportWidth="100" CellStyle-HorizontalAlign="Left"/>
				<dxwgv:GridViewDataTextColumn FieldName="Nome" Width="30%" Caption="Cotista" VisibleIndex="2"/>
				<dxwgv:GridViewDataTextColumn FieldName="Cpfcnpj" Width="10%" Caption="CPF/CNPJ" VisibleIndex="3" UnboundType="String"/>
				<dxwgv:GridViewDataTextColumn FieldName="PerfilCotista" Width="10%" Caption="Perfil" VisibleIndex="4" UnboundType="String"/>
				<dxwgv:GridViewDataTextColumn FieldName="UltimaAlteracao" Width="10%" Caption="Data Atualização" VisibleIndex="5"  UnboundType="DateTime"/>
				<dxwgv:GridViewDataTextColumn FieldName="TipoAlerta" Width="35%" Caption="Descrição Alerta" VisibleIndex="6" UnboundType="String"/>
									
			</Columns>                
							
		</dxwgv:ASPxGridView>            
		</div>    
	</div>
	</div>
	</td></tr></table>
	</div>        
	
	<dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
		
	<cc1:esDataSource ID="EsDSSuitabilityAlertas" runat="server" OnesSelect="EsDSSuitabilityAlertas_esSelect" />
    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
	</form>
</body>
</html>