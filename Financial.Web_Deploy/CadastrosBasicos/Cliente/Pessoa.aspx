﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Pessoa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        var divisao = null;
        var grupo = null;
        var classe = null;
        document.onkeydown=onDocumentKeyDownWithCallback;    
        var operacao = '';    
        
        function OnEndCallbackGrupo(s,e){
            if (divisao){
                dropCnaeDivisao.SetValue(divisao);
                divisao = null;
            }
            grupo = null;
            dropCnaeGrupo.SetValue(grupo);
            dropCnaeClasse.PerformCallback(grupo);
        }     
        
        function OnEndCallbackClasse(s,e){
            if (grupo){
                dropCnaeGrupo.SetValue(grupo);
                grupo = null;                
            }
            classe = null;
            dropCnaeClasse.SetValue(classe);
            dropCnaeSubClasse.PerformCallback(classe);
        }     
        
        function OnEndCallbackSubClasse(s,e){
            if (classe){
                dropCnaeClasse.SetValue(classe);
                classe = null;
            }
            dropCnaeSubClasse.SetValue(null);
        } 
        
        function HabilitaDispensa()
        {
            if (dropDispensado.GetSelectedIndex() != -1 && dropDispensado.GetSelectedItem().value == "N") { 
                dropTipoDispensa.SetSelectedIndex(-1); 
                dropTipoDispensa.SetEnabled(false); 
            }
            else {
                dropTipoDispensa.SetEnabled(true);
            }          
        }
        
        function showHistorico()
        {
            gridHistorico.PerformCallback();
            popupHistorico.Show();
        }
        
        function ValidacaoDocumento(offshore)
        {
            if(offshore)
            {
                textCPFCNPJ.SetEnabled(false);
                dropTipo.SetEnabled(false);
                labelTipo.GetMainElement().className = 'labelNormal';
                labelCPFCNPJ.GetMainElement().className = 'labelNormal';
            }
            else            
            {
                textCPFCNPJ.SetEnabled(true);
                dropTipo.SetEnabled(true);
                labelTipo.GetMainElement().className = 'labelRequired';
                labelCPFCNPJ.GetMainElement().className = 'labelRequired';
            }
        
        }
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
		var isUpdateEdit = false;   // usada para controlar a msg de impacto após edição do nome/documento da pessoa
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>                        
        <dxcb:ASPxCallback ID="textIdPessoa_CallBack" runat="server" OnCallback="callbackIdPessoa_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { if(e.result != '') {textIdPessoa.SetValue(e.result); textIdPessoa.SetEnabled(false); } }" />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ChkOffShore_Callback" runat="server" OnCallback="callbackChkOffShore_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { if(e.result != '') { var offshore = (e.result == 'S'); chkOffShore.SetChecked(offshore); ValidacaoDocumento(offshore);} }" />
        </dxcb:ASPxCallback>        
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                    
                alert(e.result);                                             
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                          
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        
        
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAddSinacor" runat="server" OnCallback="callbackAddSinacor_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
            popupAddSinacor.Hide();
        }        
        " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupTemplate" AllowDragging="true" PopupElementID="chkTemplate"
            EnableClientSideAPI="True" PopupAction="LeftMouseClick" CloseAction="OuterMouseClick"
            PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" Width="400" Left="250"
            Top="200" HeaderText="Templates" runat="server" HeaderStyle-BackColor="#EBECEE"
            HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="labelTemplateCarteira" runat="server" CssClass="labelNormal" Text="Carteira :"> </asp:Label>
                                <dxe:ASPxComboBox ID="dropTemplateCarteira" runat="server" ClientInstanceName="dropTemplateCarteira"
                                    DataSourceID="EsDSTemplateCarteira" ShowShadow="false" CssClass="dropDownListLongo"
                                    TextField="Descricao" ValueField="IdCarteira" EnableCallbackMode="true">
                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { if (s.GetSelectedIndex() != -1) dropTemplateCotista.SetSelectedIndex(-1);  }" />
                                </dxe:ASPxComboBox>
                            </td>
                            <td>
                                <asp:Label ID="labelTemplateCotista" runat="server" CssClass="labelNormal" Text="Cotista :"> </asp:Label>
                                <dxe:ASPxComboBox ID="dropTemplateCotista" runat="server" ClientInstanceName="dropTemplateCotista"
                                    DataSourceID="EsDSTemplateCotista" ShowShadow="false" CssClass="dropDownListLongo"
                                    TextField="Descricao" ValueField="IdCotista" EnableCallbackMode="true">
                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { if (s.GetSelectedIndex() != -1) dropTemplateCarteira.SetSelectedIndex(-1);  }" />
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="popupHistorico" runat="server" Width="500px" HeaderText=""
            ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            AllowDragging="True">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridHistorico" runat="server" Width="100%" ClientInstanceName="gridHistorico"
                            EnableCallBacks="true" AutoGenerateColumns="true" DataSourceID="EsDSSuitabilityHistorico"
                            KeyFieldName="DataHistorico" OnCustomCallback="gridHistorico_CustomCallback">
                            <Settings ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Histórico" />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="popupAddSinacor" AllowDragging="true" PopupElementID="popupAddSinacor"
            EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Importar Cadastros Sinacor"
            runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupAddSinacorControlContentControl" runat="server">
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="labelListaIdsImportaSinacor" runat="server" CssClass="labelRequired"
                                    Text="Lista de códigos Sinacor a serem importados (um por linha):"> </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="textListaIdsImportaSinacor" runat="server" TextMode="MultiLine"
                                    MaxLength="400" Rows="8" CssClass="textLongo5" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="labelTipo" runat="server" CssClass="labelNormal" Text="Tipo de Importação"> </asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxComboBox ID="dropTipoImport" runat="server" ClientInstanceName="dropTipoImport"
                                    ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto" SelectedIndex="0">
                                    <Items>
                                        <dxe:ListEditItem Value="1" Text="Cliente/Carteira" />
                                        <dxe:ListEditItem Value="2" Text="Cotista de Fundo" />
                                    </Items>
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                    <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                        <asp:LinkButton ID="btnImportaCadastroSinacor" runat="server" Font-Overline="false"
                            ForeColor="Black" CssClass="btnOK" OnClientClick="callbackAddSinacor.PerformCallback(); return false;">
                            <asp:Literal ID="btnLabelBuscaFundo" runat="server" Text="Importar" /><div>
                            </div>
                        </asp:LinkButton>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Pessoas"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnAddSinacor" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnPreRender="btnSinacorOnPreRender" OnClientClick="popupAddSinacor.Show(); return false;"
                                        ToolTip="Informe os Ids das pessoas que deseja importar do Sinacor">
                                        <asp:Literal ID="Literal9" runat="server" Text="Novo (Sinacor)" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que Deseja Excluir? Se Confirmado todo o Histórico de Posições/Operações do Cliente será Deletado.')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal2" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnSinacor" runat="server" Font-Overline="false" CssClass="btnCard"
                                        OnPreRender="btnSinacorOnPreRender" OnClientClick="if (confirm('Tem certeza que deseja atualizar o cadastro? Ele será inteiramente substituído pelo cadastro do Sinacor.')==true) {gridCadastro.PerformCallback('btnSinacor');return false;}"
                                        ToolTip="Escolha os Ids das pessoas que deseja importar do Sinacor">
                                        <asp:Literal ID="Literal8" runat="server" Text="Atualiza Sinacor" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal5" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnHistorico" runat="server" Font-Overline="false" CssClass="btnCustomFields"
                                        OnClientClick="showHistorico(); return false;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Histórico Suitability" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdPessoa"
                                        DataSourceID="EsDSPessoa" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting" 
                                        OnRowUpdated="gridCadastro_RowUpdated"    
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnStartRowEditing="gridCadastro_StartRowEditing"
                                        OnCancelRowEditing="gridCadastro_CancelRowEditing">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdPessoa" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="2" Width="20%" />
                                            <dxwgv:GridViewDataColumn FieldName="Nome" VisibleIndex="2" Width="30%" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="Tipo" VisibleIndex="3"
                                                Width="5%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Física'>Física</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Jurídica'>Jurídica</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Cpfcnpj" Caption="CPF/Cnpj" VisibleIndex="4"
                                                Width="8%" />
                                            <dxwgv:GridViewDataColumn FieldName="DataCadatro" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataUltimaAlteracao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="OffShore" Visible="false">                                            
                                            </dxwgv:GridViewDataColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxtc:ASPxPageControl ID="tabCadastro" runat="server" ClientInstanceName="tabCadastro"
                                                        Height="100%" Width="100%" ActiveTabIndex="0" TabSpacing="0px" OnLoad="tabCadastroOnLoad">
                                                        <TabPages>
                                                            <dxtc:TabPage Text="Informações Básicas">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <dxe:ASPxCheckBox ID="chkOffShore" runat="server" ClientInstanceName="chkOffShore" >
                                                                                        <ClientSideEvents CheckedChanged="function(s, e) { ValidacaoDocumento(s.GetChecked()); }" 
                                                                                         Init="function(s, e) { if(!gridCadastro.IsNewRowEditing()) ChkOffShore_Callback.SendCallback(); else ValidacaoDocumento(s.GetChecked()); }"/>
                                                                                    </dxe:ASPxCheckBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblOffShore" runat="server" CssClass="labelNormal" Text="OffShore"> </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelIdPessoa" runat="server" CssClass="labelRequired" Text="Id :"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textIdPessoa" runat="server" ClientInstanceName="textIdPessoa" 
                                                                                        CssClass="textNormal_5" Text='<%# Eval("IdPessoa") %>' ClientSideEvents-Init="function(s ,e) { if(gridCadastro.IsNewRowEditing()) textIdPessoa_CallBack.SendCallback(); else textIdPessoa.SetEnabled(false); }"
                                                                                        MaxLength="8" NumberType="Integer" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelApelido" runat="server" CssClass="labelRequired" Text="Apelido:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textApelido" ClientInstanceName="textApelido" runat="server"
                                                                                        CssClass="textLongo" MaxLength="100" Text='<%#Eval("Apelido")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelNome" runat="server" CssClass="labelRequired" Text="Nome:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" MaxLength="255" Text='<%#Eval("Nome")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <dxe:ASPxLabel ID="labelTipo" ClientInstanceName="labelTipo" runat="server" Text="Tipo:"> </dxe:ASPxLabel>                                                                                    
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipo" runat="server" ClientInstanceName="dropTipo" ShowShadow="false" 
                                                                                        DropDownStyle="DropDown" CssClass="dropDownListCurto" Text='<%#Eval("Tipo")%>'>
                                                                                        <ClientSideEvents SelectedIndexChanged="function(s,e)
                                                                                        {
                                                                                            debugger
                                                                                            if(dropTipo.GetValue() == '1')
                                                                                            {
                                                                                                textCPFCNPJ.GetInputElement().setAttribute('maxlength', 14);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                textCPFCNPJ.GetInputElement().setAttribute('maxlength', 18);
                                                                                            }
                                                                                        }" />
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Física" />
                                                                                            <dxe:ListEditItem Value="2" Text="Jurídica" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <dxe:ASPxLabel ID="labelCPFCNPJ" ClientInstanceName="labelCPFCNPJ" runat="server" Text="CPF/CNPJ:"> </dxe:ASPxLabel>
                                                                                </td>
                                                                                <td>
                                                                                   <dxe:ASPxTextBox ID="textCPFCNPJ" ClientInstanceName="textCPFCNPJ" runat="server"
                                                                                        CssClass="textLongo" Text='<%#Eval("Cpfcnpj")%>'>
                                                                                        <ClientSideEvents KeyPress="function(s,e) 
                                                                                        {
                                                                                            var TipoCliente = dropTipo.GetText();
                                                                                            var cpfcnpj = textCPFCNPJ.GetText();
                                                                                            
                                                                                            if(TipoCliente == 'Jurídica')
                                                                                            {
                                                                                                cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
                                                                                                cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')                
                                                                                                textCPFCNPJ.SetValue(cpfcnpj);
                                                                                            }
                                                                                            if(TipoCliente == 'Física')
                                                                                            {
                                                                                                cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{3})(\d)/,'$1.$2')
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{3})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpfcnpj = cpfcnpj.replace(/(\d{3})(\d)/,'$1-$2')                
                                                                                                textCPFCNPJ.SetValue(cpfcnpj);
                                                                                            }
	                                                                                    }" />
                                                                                        <ClientSideEvents Init="function(s,e) 
                                                                                        {
                                                                                            var TipoCliente = dropTipo.GetText();
                                                                                            var cpfcnpj = textCPFCNPJ.GetText();
                                                                                            
                                                                                            if(TipoCliente == 'Jurídica')
                                                                                            {
                                                                                                textCPFCNPJ.GetInputElement().setAttribute('maxlength', 18);
                                                                                                cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{2})(\d)/,'$1.$2')
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpfcnpj = cpfcnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
                                                                                                cpfcnpj = cpfcnpj.replace(/(\d{4})(\d)/,'$1-$2')                
                                                                                                textCPFCNPJ.SetValue(cpfcnpj);
                                                                                            }
                                                                                            if(TipoCliente == 'Física')
                                                                                            {
                                                                                                textCPFCNPJ.GetInputElement().setAttribute('maxlength', 14);
                                                                                                cpfcnpj = cpfcnpj.replace(/\D/g,'');                     
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{3})(\d)/,'$1.$2')
                                                                                                cpfcnpj = cpfcnpj.replace(/^(\d{3})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpfcnpj = cpfcnpj.replace(/(\d{3})(\d)/,'$1-$2')                
                                                                                                textCPFCNPJ.SetValue(cpfcnpj);
                                                                                            }
	                                                                                    }" />
                                                                                    </dxe:ASPxTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <dxe:ASPxCheckBox ID="chkTemplate" runat="server" Text="" ClientInstanceName="chkTemplate"
                                                                                        OnLoad="chkTemplate_Load">
                                                                                        <ClientSideEvents CheckedChanged="function(s, e) { if (s.GetChecked()) popupTemplate.Show(); }" />
                                                                                    </dxe:ASPxCheckBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="labelTemplate" runat="server" CssClass="labelNormal" Text="Usa Template"
                                                                                        OnInit="labelTemplate_Init"> </asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                            <dxtc:TabPage Text="Endereços">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <asp:DropDownList Visible="false" ID="dropEndereco" runat="server" DataSourceID="EsDSPessoaEndereco" />
                                                                        <asp:HiddenField ID="hiddenIdEndereco1" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hiddenIdEndereco2" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hiddenTelefone1" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hiddenTelefone2" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hiddenTelefone3" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hiddenEmail1" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hiddenEmail2" runat="server" Value="" />
                                                                        <div class="dataMessage">
                                                                            <asp:ValidationSummary ID="validationSummary1" runat="server" HeaderText="Campos com * são obrigatórios."
                                                                                EnableClientScript="true" DisplayMode="SingleParagraph" ShowSummary="true" ValidationGroup="ATK" />
                                                                        </div>
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelEndereco" runat="server" CssClass="labelNormal" Text="Logradouro:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textEndereco" runat="server" CssClass="textLongo" MaxLength="200" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelNumero" runat="server" CssClass="labelNormal" Text="Número:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textNumero" runat="server" CssClass="textCurto_5" MaxLength="5" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelComplemento" runat="server" CssClass="labelNormal" Text="Complemento:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <asp:TextBox ID="textComplemento" runat="server" CssClass="textNormal" MaxLength="50" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelBairro" runat="server" CssClass="labelNormal" Text="Bairro:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textBairro" runat="server" CssClass="textLongo" MaxLength="100" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCidade" runat="server" CssClass="labelNormal" Text="Cidade:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCidade" runat="server" CssClass="textNormal" MaxLength="100" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelUF" runat="server" CssClass="labelNormal" Text="UF:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <asp:TextBox ID="textUF" runat="server" CssClass="textEstado" MaxLength="2" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCEP" runat="server" CssClass="labelNormal" Text="CEP:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCEP" runat="server" CssClass="textNormal" MaxLength="10" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelPais" runat="server" CssClass="labelNormal" Text="País:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textPais" runat="server" CssClass="textNormal" MaxLength="100" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Tipo:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoEndereco" runat="server" ClientInstanceName="dropTipoEndereco"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_5">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="C" Text="Comercial" />
                                                                                            <dxe:ListEditItem Value="R" Text="Residencial" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelRecebeCorrespondencia" runat="server" CssClass="labelNormal"
                                                                                        Text="Correspondência:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropRecebeCorrespondencia" runat="server" ClientInstanceName="dropRecebeCorrespondencia"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_6">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <hr class="linhaH" />
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelEndereco2" runat="server" CssClass="labelNormal" Text="Logradouro:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textEndereco2" runat="server" CssClass="textLongo" MaxLength="200" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelNumero2" runat="server" CssClass="labelNormal" Text="Número:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textNumero2" runat="server" CssClass="textCurto_5" MaxLength="5" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelComplemento2" runat="server" CssClass="labelNormal" Text="Complemento:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <asp:TextBox ID="textComplemento2" runat="server" CssClass="textNormal" MaxLength="50" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelBairro2" runat="server" CssClass="labelNormal" Text="Bairro:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textBairro2" runat="server" CssClass="textLongo" MaxLength="100" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCidade2" runat="server" CssClass="labelNormal" Text="Cidade:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCidade2" runat="server" CssClass="textNormal" MaxLength="100" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelUF2" runat="server" CssClass="labelNormal" Text="UF:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <asp:TextBox ID="textUF2" runat="server" CssClass="textEstado" MaxLength="2" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCEP2" runat="server" CssClass="labelNormal" Text="CEP:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCEP2" runat="server" CssClass="textNormal" MaxLength="10" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelPais2" runat="server" CssClass="labelNormal" Text="País:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textPais2" runat="server" CssClass="textNormal" MaxLength="100" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Tipo:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoEndereco2" runat="server" ClientInstanceName="dropTipoEndereco2"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_5">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="C" Text="Comercial" />
                                                                                            <dxe:ListEditItem Value="R" Text="Residencial" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelRecebeCorrespondencia2" runat="server" CssClass="labelNormal"
                                                                                        Text="Correspondência:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropRecebeCorrespondencia2" runat="server" ClientInstanceName="dropRecebeCorrespondencia2"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_6">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <hr class="linhaH" />
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <u><b>Telefones: </b></u>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- Telefones -->
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Tipo:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoTelefone" runat="server" ClientInstanceName="dropTipoTelefone"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_2">
                                                                                        <ClientSideEvents ValueChanged="function(s, e) { 
                                                                                                                if(s.GetText()==''){
                                                                                                                    txtDDI.SetValue('');
                                                                                                                    txtDDD.SetValue('');
                                                                                                                    txtNumero.SetValue('');
                                                                                                                    txtRamal.SetValue('');}
                                                                                                                    }" />
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Residencial" />
                                                                                            <dxe:ListEditItem Value="2" Text="Celular" />
                                                                                            <dxe:ListEditItem Value="3" Text="Comercial" />
                                                                                            <dxe:ListEditItem Value="4" Text="Outros" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelDDI" runat="server" CssClass="labelNormal" Text="DDI:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtDDI" ClientInstanceName="txtDDI" runat="server" CssClass="textCurto_5"
                                                                                        MaxLength="5" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelDDD" runat="server" CssClass="labelNormal" Text="DDD:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtDDD" ClientInstanceName="txtDDD" runat="server" CssClass="textCurto_5"
                                                                                        MaxLength="5" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelNumeroTelefone" runat="server" CssClass="labelNormal" Text="Número:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtNumero" ClientInstanceName="txtNumero" runat="server" CssClass="textNormal"
                                                                                        MaxLength="12" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelRamal" runat="server" CssClass="labelNormal" Text="Ramal:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtRamal" ClientInstanceName="txtRamal" runat="server" CssClass="textCurto_5"
                                                                                        MaxLength="5" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label17" runat="server" CssClass="labelNormal" Text="Tipo:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoTelefone1" runat="server" ClientInstanceName="dropTipoTelefone1"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_2">
                                                                                        <ClientSideEvents ValueChanged="function(s, e) { 
                                                                                                                if(s.GetText()==''){
                                                                                                                    txtDDI1.SetValue('');
                                                                                                                    txtDDD1.SetValue('');
                                                                                                                    txtNumero1.SetValue('');
                                                                                                                    txtRamal1.SetValue('');}
                                                                                                                    }" />
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Residencial" />
                                                                                            <dxe:ListEditItem Value="2" Text="Celular" />
                                                                                            <dxe:ListEditItem Value="3" Text="Comercial" />
                                                                                            <dxe:ListEditItem Value="4" Text="Outros" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="DDI:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtDDI1" ClientInstanceName="txtDDI1" runat="server" CssClass="textCurto_5"
                                                                                        MaxLength="5" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label14" runat="server" CssClass="labelNormal" Text="DDD:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtDDD1" ClientInstanceName="txtDDD1" runat="server" CssClass="textCurto_5"
                                                                                        MaxLength="5" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label15" runat="server" CssClass="labelNormal" Text="Número:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtNumero1" ClientInstanceName="txtNumero1" runat="server" CssClass="textNormal"
                                                                                        MaxLength="12" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label16" runat="server" CssClass="labelNormal" Text="Ramal:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtRamal1" ClientInstanceName="txtRamal1" runat="server" CssClass="textCurto_5"
                                                                                        MaxLength="5" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label22" runat="server" CssClass="labelNormal" Text="Tipo:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoTelefone2" runat="server" ClientInstanceName="dropTipoTelefone2"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_2">
                                                                                        <ClientSideEvents ValueChanged="function(s, e) { 
                                                                                                                if(s.GetText()==''){
                                                                                                                    txtDDI2.SetValue('');
                                                                                                                    txtDDD2.SetValue('');
                                                                                                                    txtNumero2.SetValue('');
                                                                                                                    txtRamal2.SetValue('');}
                                                                                                                    }" />
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Residencial" />
                                                                                            <dxe:ListEditItem Value="2" Text="Celular" />
                                                                                            <dxe:ListEditItem Value="3" Text="Comercial" />
                                                                                            <dxe:ListEditItem Value="4" Text="Outros" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label18" runat="server" CssClass="labelNormal" Text="DDI:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtDDI2" ClientInstanceName="txtDDI2" runat="server" CssClass="textCurto_5"
                                                                                        MaxLength="5" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label19" runat="server" CssClass="labelNormal" Text="DDD:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtDDD2" ClientInstanceName="txtDDD2" runat="server" CssClass="textCurto_5"
                                                                                        MaxLength="5" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label20" runat="server" CssClass="labelNormal" Text="Número:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtNumero2" ClientInstanceName="txtNumero2" runat="server" CssClass="textNormal"
                                                                                        MaxLength="12" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label21" runat="server" CssClass="labelNormal" Text="Ramal:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtRamal2" ClientInstanceName="txtRamal2" runat="server" CssClass="textCurto_5"
                                                                                        MaxLength="5" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <hr class="linhaH" />
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <u><b>Emails: </b></u>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- Emails -->
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label23" runat="server" CssClass="labelNormal" Text="Email1:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <asp:TextBox ID="txtEmail1" runat="server" CssClass="textLongo" MaxLength="50" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label24" runat="server" CssClass="labelNormal" Text="Email2:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtEmail2" runat="server" CssClass="textLongo" MaxLength="50" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <hr class="linhaH" />
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                            <dxtc:TabPage Text="Informações Adicionais">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <asp:HiddenField ID="hiddenDocumento1" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hiddenDocumento2" runat="server" Value="" />
                                                                        <asp:HiddenField ID="hiddenDocumento3" runat="server" Value="" />
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelSexo" runat="server" Text="Sexo:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropSexo" runat="server" ClientInstanceName="dropSexo" ShowShadow="false"
                                                                                        DropDownStyle="DropDown" CssClass="dropDownListCurto" Text='<%#Eval("Sexo")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="M" Text="Masculino" />
                                                                                            <dxe:ListEditItem Value="F" Text="Feminino" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelEstadoCivil" runat="server" Text="Estado Civil:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropEstadoCivil" runat="server" ClientInstanceName="dropEstadoCivil"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto" Text='<%#Eval("EstadoCivil")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Casado" />
                                                                                            <dxe:ListEditItem Value="2" Text="Solteiro" />
                                                                                            <dxe:ListEditItem Value="3" Text="Viúvo" />
                                                                                            <dxe:ListEditItem Value="4" Text="Divorciado" />
                                                                                            <dxe:ListEditItem Value="5" Text="Outros" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelDataNascimento" runat="server" Text="Data Nascimento:" />
                                                                                </td>
                                                                                <td colspan="5">
                                                                                    <dxe:ASPxDateEdit ID="textDataNascimento" runat="server" ClientInstanceName="textDataNascimento"
                                                                                        Value='<%#Eval("DataNascimento")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelProfissao" runat="server" Text="Profissão:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="1">
                                                                                    <asp:TextBox ID="textProfissao" runat="server" CssClass="textNormal" MaxLength="50"
                                                                                        Text='<%#Eval("Profissao")%>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label45" runat="server" CssClass="labelNormal" Text="País Nacionalidade:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="7">
                                                                                    <dxe:ASPxComboBox ID="dropNacionalidade" runat="server" ClientInstanceName="dropNacionalidade"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" Text='<%#Eval("Nacionalidade")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="13" Text="AFEGANISTAO" />
                                                                                            <dxe:ListEditItem Value="47" Text="ANTILHAS HOLANDESAS" />
                                                                                            <dxe:ListEditItem Value="63" Text="ARGENTINA" />
                                                                                            <dxe:ListEditItem Value="72" Text="AUSTRIA" />
                                                                                            <dxe:ListEditItem Value="31" Text="BURUNDI" />
                                                                                            <dxe:ListEditItem Value="87" Text="BELGICA" />
                                                                                            <dxe:ListEditItem Value="229" Text="BENIN" />
                                                                                            <dxe:ListEditItem Value="81" Text="BANGLADESH" />
                                                                                            <dxe:ListEditItem Value="80" Text="BAHREIN, ILHAS" />
                                                                                            <dxe:ListEditItem Value="88" Text="BELIZE" />
                                                                                            <dxe:ListEditItem Value="90" Text="BERMUDAS" />
                                                                                            <dxe:ListEditItem Value="83" Text="BARBADOS" />
                                                                                            <dxe:ListEditItem Value="101" Text="BOTSUANA" />
                                                                                            <dxe:ListEditItem Value="158" Text="CHILE" />
                                                                                            <dxe:ListEditItem Value="160" Text="CHINA, REPUBLICA POPULAR" />
                                                                                            <dxe:ListEditItem Value="831" Text="UCRANIA" />
                                                                                            <dxe:ListEditItem Value="193" Text="COSTA DO MARFIM" />
                                                                                            <dxe:ListEditItem Value="145" Text="CAMAROES" />
                                                                                            <dxe:ListEditItem Value="177" Text="CONGO" />
                                                                                            <dxe:ListEditItem Value="169" Text="COLOMBIA" />
                                                                                            <dxe:ListEditItem Value="685" Text="SAARA OCIDENTAL" />
                                                                                            <dxe:ListEditItem Value="199" Text="CUBA" />
                                                                                            <dxe:ListEditItem Value="163" Text="CHIPRE" />
                                                                                            <dxe:ListEditItem Value="232" Text="DINAMARCA" />
                                                                                            <dxe:ListEditItem Value="235" Text="DOMINICA, ILHA" />
                                                                                            <dxe:ListEditItem Value="647" Text="REPUBLICA DOMINICANA" />
                                                                                            <dxe:ListEditItem Value="59" Text="ARGELIA" />
                                                                                            <dxe:ListEditItem Value="239" Text="EQUADOR" />
                                                                                            <dxe:ListEditItem Value="196" Text="COSTA RICA" />
                                                                                            <dxe:ListEditItem Value="240" Text="EGITO" />
                                                                                            <dxe:ListEditItem Value="783" Text="DJIBOUTI" />
                                                                                            <dxe:ListEditItem Value="245" Text="ESPANHA" />
                                                                                            <dxe:ListEditItem Value="253" Text="ETIOPIA" />
                                                                                            <dxe:ListEditItem Value="244" Text="EMIRADOS ARABES UNIDOS" />
                                                                                            <dxe:ListEditItem Value="870" Text="FIDJI" />
                                                                                            <dxe:ListEditItem Value="275" Text="FRANCA" />
                                                                                            <dxe:ListEditItem Value="289" Text="GANA" />
                                                                                            <dxe:ListEditItem Value="285" Text="GAMBIA" />
                                                                                            <dxe:ListEditItem Value="301" Text="GRECIA" />
                                                                                            <dxe:ListEditItem Value="317" Text="GUATEMALA" />
                                                                                            <dxe:ListEditItem Value="337" Text="GUIANA" />
                                                                                            <dxe:ListEditItem Value="351" Text="HONG KONG" />
                                                                                            <dxe:ListEditItem Value="161" Text="FORMOSA (TAIWAN)" />
                                                                                            <dxe:ListEditItem Value="115" Text="BURKINA FASO" />
                                                                                            <dxe:ListEditItem Value="151" Text="CANARIAS, ILHAS" />
                                                                                            <dxe:ListEditItem Value="291" Text="GEORGIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="297" Text="GRANADA" />
                                                                                            <dxe:ListEditItem Value="365" Text="INDONESIA" />
                                                                                            <dxe:ListEditItem Value="361" Text="INDIA" />
                                                                                            <dxe:ListEditItem Value="372" Text="IRA, REPUBLICA ISLAMICA DO" />
                                                                                            <dxe:ListEditItem Value="375" Text="IRLANDA" />
                                                                                            <dxe:ListEditItem Value="690" Text="SAMOA OCIDENTAL" />
                                                                                            <dxe:ListEditItem Value="379" Text="ISLANDIA" />
                                                                                            <dxe:ListEditItem Value="386" Text="ITALIA" />
                                                                                            <dxe:ListEditItem Value="251" Text="ESTONIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="866" Text="VIRGENS AMERICANAS, ILHAS" />
                                                                                            <dxe:ListEditItem Value="391" Text="JAMAICA" />
                                                                                            <dxe:ListEditItem Value="399" Text="JAPAO" />
                                                                                            <dxe:ListEditItem Value="271" Text="FINLANDIA" />
                                                                                            <dxe:ListEditItem Value="190" Text="COREIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="420" Text="LAOS, REPUBLICA POPULAR DEMOCRATICA" />
                                                                                            <dxe:ListEditItem Value="434" Text="LIBERIA" />
                                                                                            <dxe:ListEditItem Value="438" Text="LIBIA" />
                                                                                            <dxe:ListEditItem Value="442" Text="LITUANIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="750" Text="SRI LANKA" />
                                                                                            <dxe:ListEditItem Value="447" Text="MACAU" />
                                                                                            <dxe:ListEditItem Value="474" Text="MARROCOS" />
                                                                                            <dxe:ListEditItem Value="450" Text="MADAGASCAR" />
                                                                                            <dxe:ListEditItem Value="467" Text="MALTA" />
                                                                                            <dxe:ListEditItem Value="495" Text="MONACO" />
                                                                                            <dxe:ListEditItem Value="485" Text="MAURICIO" />
                                                                                            <dxe:ListEditItem Value="458" Text="MALAVI" />
                                                                                            <dxe:ListEditItem Value="508" Text="NAURU" />
                                                                                            <dxe:ListEditItem Value="525" Text="NIGER" />
                                                                                            <dxe:ListEditItem Value="521" Text="NICARAGUA" />
                                                                                            <dxe:ListEditItem Value="305" Text="GROELANDIA" />
                                                                                            <dxe:ListEditItem Value="538" Text="NORUEGA" />
                                                                                            <dxe:ListEditItem Value="580" Text="PANAMA" />
                                                                                            <dxe:ListEditItem Value="586" Text="PARAGUAI" />
                                                                                            <dxe:ListEditItem Value="589" Text="PERU" />
                                                                                            <dxe:ListEditItem Value="267" Text="FILIPINAS" />
                                                                                            <dxe:ListEditItem Value="603" Text="POLONIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="607" Text="PORTUGAL" />
                                                                                            <dxe:ListEditItem Value="281" Text="GABAO" />
                                                                                            <dxe:ListEditItem Value="660" Text="REUNIAO, ILHA" />
                                                                                            <dxe:ListEditItem Value="675" Text="RUANDA" />
                                                                                            <dxe:ListEditItem Value="53" Text="ARABIA SAUDITA" />
                                                                                            <dxe:ListEditItem Value="85" Text="BELARUS, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="728" Text="SENEGAL" />
                                                                                            <dxe:ListEditItem Value="741" Text="CINGAPURA" />
                                                                                            <dxe:ListEditItem Value="744" Text="SIRIA, REPUBLICA ARABE DA" />
                                                                                            <dxe:ListEditItem Value="735" Text="SERRA LEOA" />
                                                                                            <dxe:ListEditItem Value="687" Text="EL SALVADOR" />
                                                                                            <dxe:ListEditItem Value="697" Text="SAN MARINO" />
                                                                                            <dxe:ListEditItem Value="715" Text="SANTA LUCIA" />
                                                                                            <dxe:ListEditItem Value="764" Text="SUECIA" />
                                                                                            <dxe:ListEditItem Value="800" Text="TOGO" />
                                                                                            <dxe:ListEditItem Value="776" Text="TAILANDIA" />
                                                                                            <dxe:ListEditItem Value="810" Text="TONGA" />
                                                                                            <dxe:ListEditItem Value="815" Text="TRINIDAD E TOBAGO" />
                                                                                            <dxe:ListEditItem Value="827" Text="TURQUIA" />
                                                                                            <dxe:ListEditItem Value="780" Text="TANZANIA, REPUBLICA UNIDA DA" />
                                                                                            <dxe:ListEditItem Value="845" Text="URUGUAI" />
                                                                                            <dxe:ListEditItem Value="249" Text="ESTADOS UNIDOS" />
                                                                                            <dxe:ListEditItem Value="850" Text="VENEZUELA" />
                                                                                            <dxe:ListEditItem Value="863" Text="VIRGENS BRITANICAS, ILHAS" />
                                                                                            <dxe:ListEditItem Value="890" Text="ZAMBIA" />
                                                                                            <dxe:ListEditItem Value="23" Text="ALEMANHA" />
                                                                                            <dxe:ListEditItem Value="69" Text="AUSTRALIA" />
                                                                                            <dxe:ListEditItem Value="77" Text="BAHAMAS, ILHAS" />
                                                                                            <dxe:ListEditItem Value="97" Text="BOLIVIA" />
                                                                                            <dxe:ListEditItem Value="105" Text="BRASIL" />
                                                                                            <dxe:ListEditItem Value="149" Text="CANADA" />
                                                                                            <dxe:ListEditItem Value="293" Text="GIBRALTAR" />
                                                                                            <dxe:ListEditItem Value="309" Text="GUADALUPE" />
                                                                                            <dxe:ListEditItem Value="341" Text="HAITI" />
                                                                                            <dxe:ListEditItem Value="355" Text="HUNGRIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="367" Text="INGLATERRA" />
                                                                                            <dxe:ListEditItem Value="369" Text="IRAQUE" />
                                                                                            <dxe:ListEditItem Value="383" Text="ISRAEL" />
                                                                                            <dxe:ListEditItem Value="388" Text="IUGOSLAVIA, REPUBLICA FEDERATIVA" />
                                                                                            <dxe:ListEditItem Value="403" Text="JORDANIA" />
                                                                                            <dxe:ListEditItem Value="431" Text="LIBANO" />
                                                                                            <dxe:ListEditItem Value="440" Text="LIECHTENSTEIN" />
                                                                                            <dxe:ListEditItem Value="445" Text="LUXEMBURGO" />
                                                                                            <dxe:ListEditItem Value="455" Text="MALASIA" />
                                                                                            <dxe:ListEditItem Value="464" Text="MALI" />
                                                                                            <dxe:ListEditItem Value="477" Text="MARTINICA" />
                                                                                            <dxe:ListEditItem Value="493" Text="MEXICO" />
                                                                                            <dxe:ListEditItem Value="517" Text="NEPAL" />
                                                                                            <dxe:ListEditItem Value="531" Text="NIUE, ILHA" />
                                                                                            <dxe:ListEditItem Value="548" Text="NOVA ZELANDIA" />
                                                                                            <dxe:ListEditItem Value="576" Text="PAQUISTAO" />
                                                                                            <dxe:ListEditItem Value="611" Text="PORTO RICO" />
                                                                                            <dxe:ListEditItem Value="623" Text="QUENIA" />
                                                                                            <dxe:ListEditItem Value="670" Text="ROMENIA" />
                                                                                            <dxe:ListEditItem Value="676" Text="RUSSIA, FEDERACAO DA" />
                                                                                            <dxe:ListEditItem Value="759" Text="SUDAO" />
                                                                                            <dxe:ListEditItem Value="767" Text="SUICA" />
                                                                                            <dxe:ListEditItem Value="791" Text="TCHECA, REPUBLICA" />
                                                                                            <dxe:ListEditItem Value="820" Text="TUNISIA" />
                                                                                            <dxe:ListEditItem Value="833" Text="UGANDA" />
                                                                                            <dxe:ListEditItem Value="551" Text="VANUATU  PACIFIC" />
                                                                                            <dxe:ListEditItem Value="888" Text="ZAIRE" />
                                                                                            <dxe:ListEditItem Value="665" Text="ZIMBABUE" />
                                                                                            <dxe:ListEditItem Value="640" Text="REPUBLICA CENTRO AFRICANA" />
                                                                                            <dxe:ListEditItem Value="756" Text="AFRICA DO SUL" />
                                                                                            <dxe:ListEditItem Value="17" Text="ALBANIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="37" Text="ANDORRA" />
                                                                                            <dxe:ListEditItem Value="40" Text="ANGOLA" />
                                                                                            <dxe:ListEditItem Value="41" Text="ANGUILLA" />
                                                                                            <dxe:ListEditItem Value="43" Text="ANTIGUA BARBUDA" />
                                                                                            <dxe:ListEditItem Value="64" Text="ARMENIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="65" Text="ARUBA" />
                                                                                            <dxe:ListEditItem Value="73" Text="ARZEBAIJAO, REPUBLICA DO" />
                                                                                            <dxe:ListEditItem Value="98" Text="BOSNIA-HERZEGOVINIA" />
                                                                                            <dxe:ListEditItem Value="108" Text="BRUNEI" />
                                                                                            <dxe:ListEditItem Value="111" Text="BULGARIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="119" Text="BUTAO" />
                                                                                            <dxe:ListEditItem Value="127" Text="CABO VERDE, REPUBLICA DE" />
                                                                                            <dxe:ListEditItem Value="141" Text="CAMBOJA" />
                                                                                            <dxe:ListEditItem Value="153" Text="CASAQUISTAO, REPUBLICA DO" />
                                                                                            <dxe:ListEditItem Value="154" Text="CATAR" />
                                                                                            <dxe:ListEditItem Value="137" Text="CAYMAN, ILHAS" />
                                                                                            <dxe:ListEditItem Value="788" Text="CHADE" />
                                                                                            <dxe:ListEditItem Value="511" Text="CHRISTMAS, ILHAS" />
                                                                                            <dxe:ListEditItem Value="165" Text="COCOS-KEELING, ILHAS" />
                                                                                            <dxe:ListEditItem Value="173" Text="COMORES, ILHAS" />
                                                                                            <dxe:ListEditItem Value="183" Text="COOK, ILHAS" />
                                                                                            <dxe:ListEditItem Value="187" Text="COREIA, REPUBLICA POPULAR DEMOCRATICA" />
                                                                                            <dxe:ListEditItem Value="198" Text="COVEITE" />
                                                                                            <dxe:ListEditItem Value="195" Text="CROACIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="237" Text="DUBAI" />
                                                                                            <dxe:ListEditItem Value="247" Text="ESLOVACA, REPUBLICA" />
                                                                                            <dxe:ListEditItem Value="246" Text="ESLOVENIA, REPUBLICA" />
                                                                                            <dxe:ListEditItem Value="255" Text="FALKLAND (MALVINAS)" />
                                                                                            <dxe:ListEditItem Value="259" Text="FEROE, ILHAS" />
                                                                                            <dxe:ListEditItem Value="263" Text="FEZZAN" />
                                                                                            <dxe:ListEditItem Value="313" Text="GUAM" />
                                                                                            <dxe:ListEditItem Value="325" Text="GUIANA FRANCESA" />
                                                                                            <dxe:ListEditItem Value="329" Text="GUINE" />
                                                                                            <dxe:ListEditItem Value="334" Text="GUINE-BISSAU" />
                                                                                            <dxe:ListEditItem Value="331" Text="GUINE-EQUATORIAL" />
                                                                                            <dxe:ListEditItem Value="345" Text="HONDURAS" />
                                                                                            <dxe:ListEditItem Value="357" Text="IEMEN" />
                                                                                            <dxe:ListEditItem Value="396" Text="JOHNSTON, ILHAS" />
                                                                                            <dxe:ListEditItem Value="411" Text="KIRIBATI" />
                                                                                            <dxe:ListEditItem Value="423" Text="LEBUAN, ILHAS" />
                                                                                            <dxe:ListEditItem Value="426" Text="LESOTO" />
                                                                                            <dxe:ListEditItem Value="427" Text="LETONIA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="449" Text="MACEDONIA" />
                                                                                            <dxe:ListEditItem Value="461" Text="MALDIVAS" />
                                                                                            <dxe:ListEditItem Value="472" Text="MARIANAS DO NORTE" />
                                                                                            <dxe:ListEditItem Value="476" Text="MARSHALL, ILHAS" />
                                                                                            <dxe:ListEditItem Value="488" Text="MAURITANIA" />
                                                                                            <dxe:ListEditItem Value="93" Text="MIANMAR (BIRMANIA)" />
                                                                                            <dxe:ListEditItem Value="499" Text="MICRONESIA" />
                                                                                            <dxe:ListEditItem Value="490" Text="MIDWAY, ILHAS" />
                                                                                            <dxe:ListEditItem Value="505" Text="MOCAMBIQUE" />
                                                                                            <dxe:ListEditItem Value="494" Text="MOLDOVA, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="497" Text="MONGOLIA" />
                                                                                            <dxe:ListEditItem Value="501" Text="MONTSERRAT, ILHAS" />
                                                                                            <dxe:ListEditItem Value="507" Text="NAMIBIA" />
                                                                                            <dxe:ListEditItem Value="535" Text="NORFOLK, ILHA" />
                                                                                            <dxe:ListEditItem Value="528" Text="NIGERIA" />
                                                                                            <dxe:ListEditItem Value="542" Text="NOVA CALEDONIA" />
                                                                                            <dxe:ListEditItem Value="556" Text="OMA" />
                                                                                            <dxe:ListEditItem Value="563" Text="PACIFICO(ADM EUA), ILHAS DO" />
                                                                                            <dxe:ListEditItem Value="566" Text="PACIFICO(POSSE EUA), ILHAS DO" />
                                                                                            <dxe:ListEditItem Value="573" Text="PAISES BAIXOS(HOLANDA)" />
                                                                                            <dxe:ListEditItem Value="575" Text="PALAU" />
                                                                                            <dxe:ListEditItem Value="545" Text="PAPUA NOVA GUINE" />
                                                                                            <dxe:ListEditItem Value="593" Text="PITCAIM, ILHA DE" />
                                                                                            <dxe:ListEditItem Value="599" Text="POLINESIA FRANCESA" />
                                                                                            <dxe:ListEditItem Value="625" Text="QUIRGUIZ, REPUBLICA DA" />
                                                                                            <dxe:ListEditItem Value="628" Text="REINO UNIDO" />
                                                                                            <dxe:ListEditItem Value="677" Text="SALOMAO, ILHAS" />
                                                                                            <dxe:ListEditItem Value="691" Text="SAMOA AMERICANA" />
                                                                                            <dxe:ListEditItem Value="710" Text="SANTA HELENA" />
                                                                                            <dxe:ListEditItem Value="695" Text="SAO CRISTOVAO E NEVES, ILHAS" />
                                                                                            <dxe:ListEditItem Value="700" Text="SAO PEDRO E MIQUELON" />
                                                                                            <dxe:ListEditItem Value="720" Text="SAO TOME E PRINCIPE, ILHAS" />
                                                                                            <dxe:ListEditItem Value="705" Text="SAO VICENTE E GRANADINAS" />
                                                                                            <dxe:ListEditItem Value="731" Text="SEYCHELLES" />
                                                                                            <dxe:ListEditItem Value="748" Text="SOMALIA" />
                                                                                            <dxe:ListEditItem Value="754" Text="SUAZILANDIA" />
                                                                                            <dxe:ListEditItem Value="770" Text="SURINAME" />
                                                                                            <dxe:ListEditItem Value="772" Text="TADJIQUISTAO" />
                                                                                            <dxe:ListEditItem Value="782" Text="TERRITORIO BRITANICO OCEANO INDICO" />
                                                                                            <dxe:ListEditItem Value="795" Text="TIMOR LESTE" />
                                                                                            <dxe:ListEditItem Value="805" Text="TOQUELAU, ILHAS" />
                                                                                            <dxe:ListEditItem Value="823" Text="TURCAS E CAICOS" />
                                                                                            <dxe:ListEditItem Value="824" Text="TURCOMENISTAO, REPUBLICA DO" />
                                                                                            <dxe:ListEditItem Value="828" Text="TUVALU" />
                                                                                            <dxe:ListEditItem Value="847" Text="UZBEQUISTAO, REPUBLICA DO" />
                                                                                            <dxe:ListEditItem Value="848" Text="VATICANO, ESTADO DA CIDADE DO" />
                                                                                            <dxe:ListEditItem Value="858" Text="VIETNA" />
                                                                                            <dxe:ListEditItem Value="875" Text="WALLIS E FUTUNA, ILHAS" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label5" runat="server" Text="Cód.Interface:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="8">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                    <asp:TextBox ID="textCodigoInterface" runat="server" CssClass="textNormal" MaxLength="40"
                                                                                                    Text='<%#Eval("CodigoInterface")%>' /></td>
                                                                                            <td width="12px">
                                                                                            </td>
                                                                                            <td class="td_Label">
                                                                                                <asp:Label ID="labelPerfilInvestidor" runat="server" Text="Perfil Investidor:"> </asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <dxe:ASPxComboBox ID="dropPerfilInvestidor" runat="server" ClientInstanceName="dropPerfilInvestidor"
                                                                                                    DataSourceID="EsDSPerfilInvestidor" ShowShadow="false" CssClass="dropDownListCurto"
                                                                                                    DropDownStyle="DropDown" TextField="Descricao" Text='<%#Eval("IdPerfilInvestidor")%>'
                                                                                                    ValueField="IdPerfilInvestidor">
                                                                                                </dxe:ASPxComboBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label8" runat="server" Text="Politicamente Exposta:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <dxe:ASPxComboBox ID="dropPessoaPoliticamenteExposta" runat="server" ClientInstanceName="dropPessoaPoliticamenteExposta"
                                                                                                    ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_6" Text='<%#Eval("PessoaPoliticamenteExposta")%>'>
                                                                                                    <Items>
                                                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                                                    </Items>
                                                                                                </dxe:ASPxComboBox>
                                                                                            </td>
                                                                                            <td width="25px">
                                                                                            </td>
                                                                                            <td class="td_Label">
                                                                                                <asp:Label ID="label35" runat="server" CssClass="labelNormal" Text="Pessoa Vinculada:"> </asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <dxe:ASPxComboBox ID="dropPessoaVinculada" runat="server" ClientInstanceName="dropPessoaVinculada"
                                                                                                    ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_6" Text='<%#Eval("PessoaVinculada")%>'>
                                                                                                    <Items>
                                                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                                                    </Items>
                                                                                                </dxe:ASPxComboBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Naturalidade:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="estado" runat="server" ClientInstanceName="estado" ShowShadow="false"
                                                                                        DropDownStyle="DropDown" CssClass="dropDownListCurto_6" Text='<%#Eval("UFNaturalidade")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="AC" Text="AC" />
                                                                                            <dxe:ListEditItem Value="AL" Text="AL" />
                                                                                            <dxe:ListEditItem Value="AP" Text="AP" />
                                                                                            <dxe:ListEditItem Value="AM" Text="AM" />
                                                                                            <dxe:ListEditItem Value="BA" Text="BA" />
                                                                                            <dxe:ListEditItem Value="CE" Text="CE" />
                                                                                            <dxe:ListEditItem Value="DF" Text="DF" />
                                                                                            <dxe:ListEditItem Value="ES" Text="ES" />
                                                                                            <dxe:ListEditItem Value="GO" Text="GO" />
                                                                                            <dxe:ListEditItem Value="MA" Text="MA" />
                                                                                            <dxe:ListEditItem Value="MT" Text="MT" />
                                                                                            <dxe:ListEditItem Value="MS" Text="MS" />
                                                                                            <dxe:ListEditItem Value="MG" Text="MG" />
                                                                                            <dxe:ListEditItem Value="PA" Text="PA" />
                                                                                            <dxe:ListEditItem Value="PB" Text="PB" />
                                                                                            <dxe:ListEditItem Value="PR" Text="PR" />
                                                                                            <dxe:ListEditItem Value="PE" Text="PE" />
                                                                                            <dxe:ListEditItem Value="PI" Text="PI" />
                                                                                            <dxe:ListEditItem Value="RJ" Text="RJ" />
                                                                                            <dxe:ListEditItem Value="RN" Text="RN" />
                                                                                            <dxe:ListEditItem Value="RS" Text="RS" />
                                                                                            <dxe:ListEditItem Value="RO" Text="RO" />
                                                                                            <dxe:ListEditItem Value="RR" Text="RR" />
                                                                                            <dxe:ListEditItem Value="SC" Text="SC" />
                                                                                            <dxe:ListEditItem Value="SP" Text="SP" />
                                                                                            <dxe:ListEditItem Value="SE" Text="SE" />
                                                                                            <dxe:ListEditItem Value="TO" Text="TO" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelSituacaoLegal" runat="server" Text="Situação Legal:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropSituacaoLegal" runat="server" ClientInstanceName="dropSituacaoLegal"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto" Text='<%#Eval("SituacaoLegal")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Espolio" />
                                                                                            <dxe:ListEditItem Value="2" Text="Interdito" />
                                                                                            <dxe:ListEditItem Value="3" Text="Maior" />
                                                                                            <dxe:ListEditItem Value="4" Text="Menor" />
                                                                                            <dxe:ListEditItem Value="5" Text="Emancipado" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label10" runat="server" Text="Filiação Nome Pai:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="filiacaoNomePai" runat="server" CssClass="textLongo" MaxLength="200"
                                                                                        Text='<%#Eval("FiliacaoNomePai")%>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Filiação Nome Mãe:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="7">
                                                                                    <asp:TextBox ID="filiacaoNomeMae" runat="server" CssClass="textLongo" MaxLength="200"
                                                                                        Text='<%#Eval("FiliacaoNomeMae")%>' />
                                                                                </td>
                                                                            </tr>                                                                                                                                                        
                                                                        </table>
                                                                        <hr class="linhaH" />
                                                                        
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label50" runat="server" Text="Dt. Vencimento Cadastro:" />
                                                                                </td>
                                                                                <td colspan="5">
                                                                                    <dxe:ASPxDateEdit ID="textDataVencimentoCadastro" runat="server" ClientInstanceName="textDataVencimentoCadastro"
                                                                                        Value='<%#Eval("DataVencimentoCadastro")%>' />
                                                                                </td>
                                                                                
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label51" runat="server" Text="Alerta?" />
                                                                                </td>
                                                                                <td colspan="5">
                                                                                    <dxe:ASPxComboBox ID="dropAlertaCadastro" runat="server" ClientInstanceName="dropAlertaCadastro"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("AlertaCadastro")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label52" runat="server" Text="Email:" />
                                                                                </td>
                                                                                
                                                                                <td colspan="5">
                                                                                    <asp:TextBox ID="textEmailAlerta" runat="server" CssClass="textLongo" MaxLength="50" Value='<%#Eval("EmailAlerta")%>' />
                                                                                </td>
                                                                        </table>
                                                                        <hr class="linhaH" />
                                                                        <!-- Documentos -->
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <u><b>Documentos: </b></u>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Tipo:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoDocumento" runat="server" ClientInstanceName="dropTipoDocumento"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo">
                                                                                        <ClientSideEvents ValueChanged="function(s, e) { 
                                                                                                                if(s.GetText()==''){
                                                                                                                    txtNumDocumento.SetValue('');
                                                                                                                    dropOrgaoEmissor.SetValue('');
                                                                                                                    textDataExpedicao.SetValue(null);
                                                                                                                    dropUFEmissor.SetValue('');}
                                                                                                                    }" />
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Cédula Identidade (CI)" />
                                                                                            <dxe:ListEditItem Value="2" Text="Cédula Identidade Estrangeiro (CIE)" />
                                                                                            <dxe:ListEditItem Value="3" Text="Carteira Militar" />
                                                                                            <dxe:ListEditItem Value="4" Text="Identidade Profissional (CIP)" />
                                                                                            <dxe:ListEditItem Value="5" Text="Cédula Identidade Profissional (Público)" />
                                                                                            <dxe:ListEditItem Value="6" Text="Certidão Nascimento" />
                                                                                            <dxe:ListEditItem Value="7" Text="Carteira Habilitação" />
                                                                                            <dxe:ListEditItem Value="8" Text="Carteira Trabalho" />
                                                                                            <dxe:ListEditItem Value="9" Text="Identificação Prisional" />
                                                                                            <dxe:ListEditItem Value="10" Text="Laissez Passer" />
                                                                                            <dxe:ListEditItem Value="11" Text="Passaporte" />
                                                                                            <dxe:ListEditItem Value="12" Text="Cédula Identidade (RG)" />
                                                                                            <dxe:ListEditItem Value="13" Text="Documento Identidade (RI)" />
                                                                                            <dxe:ListEditItem Value="14" Text="Cédula Identidade Estrangeiro (RNE)" />
                                                                                            <dxe:ListEditItem Value="15" Text="Inscrição Estadual" />
                                                                                            <dxe:ListEditItem Value="16" Text="NIRE" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Doc.:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtNumDocumento" ClientInstanceName="txtNumDocumento" runat="server"
                                                                                        CssClass="textNormal_5" MaxLength="13" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Orgão Emissor:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropOrgaoEmissor" runat="server" ClientInstanceName="dropOrgaoEmissor"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_2">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="ABEXCON" Text="ABEXCON" />
                                                                                            <dxe:ListEditItem Value="AERON" Text="AERON" />
                                                                                            <dxe:ListEditItem Value="ASLEGIS" Text="ASLEGIS" />
                                                                                            <dxe:ListEditItem Value="CBM" Text="CBM" />
                                                                                            <dxe:ListEditItem Value="CFP" Text="CFP" />
                                                                                            <dxe:ListEditItem Value="CGJ" Text="CGJ" />
                                                                                            <dxe:ListEditItem Value="CMRJ" Text="CMRJ" />
                                                                                            <dxe:ListEditItem Value="CNDL" Text="CNDL" />
                                                                                            <dxe:ListEditItem Value="CNE" Text="CNE" />
                                                                                            <dxe:ListEditItem Value="CNEN" Text="CNEN" />
                                                                                            <dxe:ListEditItem Value="CONDEBR" Text="CONDEBR" />
                                                                                            <dxe:ListEditItem Value="CONFEV" Text="CONFEV" />
                                                                                            <dxe:ListEditItem Value="CONFIPA" Text="CONFIPA" />
                                                                                            <dxe:ListEditItem Value="CONRE" Text="CONRE" />
                                                                                            <dxe:ListEditItem Value="CONRERP" Text="CONRERP" />
                                                                                            <dxe:ListEditItem Value="CORE" Text="CORE" />
                                                                                            <dxe:ListEditItem Value="CORECON" Text="CORECON" />
                                                                                            <dxe:ListEditItem Value="COREM" Text="COREM" />
                                                                                            <dxe:ListEditItem Value="COREN" Text="COREN" />
                                                                                            <dxe:ListEditItem Value="CRA" Text="CRA" />
                                                                                            <dxe:ListEditItem Value="CRAS" Text="CRAS" />
                                                                                            <dxe:ListEditItem Value="CRB" Text="CRB" />
                                                                                            <dxe:ListEditItem Value="CRBM" Text="CRBM" />
                                                                                            <dxe:ListEditItem Value="CRC" Text="CRC" />
                                                                                            <dxe:ListEditItem Value="CRCPN" Text="CRCPN" />
                                                                                            <dxe:ListEditItem Value="CREA" Text="CREA" />
                                                                                            <dxe:ListEditItem Value="CRECI" Text="CRECI" />
                                                                                            <dxe:ListEditItem Value="CREF" Text="CREF" />
                                                                                            <dxe:ListEditItem Value="CRESS" Text="CRESS" />
                                                                                            <dxe:ListEditItem Value="CRF" Text="CRF" />
                                                                                            <dxe:ListEditItem Value="CRFA" Text="CRFA" />
                                                                                            <dxe:ListEditItem Value="CRFTO" Text="CRFTO" />
                                                                                            <dxe:ListEditItem Value="CRM" Text="CRM" />
                                                                                            <dxe:ListEditItem Value="CRMV" Text="CRMV" />
                                                                                            <dxe:ListEditItem Value="CRN" Text="CRN" />
                                                                                            <dxe:ListEditItem Value="CRO" Text="CRO" />
                                                                                            <dxe:ListEditItem Value="CRP" Text="CRP" />
                                                                                            <dxe:ListEditItem Value="CRQ" Text="CRQ" />
                                                                                            <dxe:ListEditItem Value="CRTA" Text="CRTA" />
                                                                                            <dxe:ListEditItem Value="CRTR" Text="CRTR" />
                                                                                            <dxe:ListEditItem Value="CTMTB" Text="CTMTB" />
                                                                                            <dxe:ListEditItem Value="DEF" Text="DEF" />
                                                                                            <dxe:ListEditItem Value="DEPCT" Text="DEPCT" />
                                                                                            <dxe:ListEditItem Value="DESIPE" Text="DESIPE" />
                                                                                            <dxe:ListEditItem Value="DESP" Text="DESP" />
                                                                                            <dxe:ListEditItem Value="DETRAN" Text="DETRAN" />
                                                                                            <dxe:ListEditItem Value="DFSP" Text="DFSP" />
                                                                                            <dxe:ListEditItem Value="DGPC" Text="DGPC" />
                                                                                            <dxe:ListEditItem Value="DI" Text="DI" />
                                                                                            <dxe:ListEditItem Value="DPF" Text="DPF" />
                                                                                            <dxe:ListEditItem Value="DPGE" Text="DPGE" />
                                                                                            <dxe:ListEditItem Value="DPTC" Text="DPTC" />
                                                                                            <dxe:ListEditItem Value="DSG" Text="DSG" />
                                                                                            <dxe:ListEditItem Value="EAPJU" Text="EAPJU" />
                                                                                            <dxe:ListEditItem Value="ESG" Text="ESG" />
                                                                                            <dxe:ListEditItem Value="EXERCIT" Text="EXERCIT" />
                                                                                            <dxe:ListEditItem Value="FENAJ" Text="FENAJ" />
                                                                                            <dxe:ListEditItem Value="FUNAI" Text="FUNAI" />
                                                                                            <dxe:ListEditItem Value="GEJ" Text="GEJ" />
                                                                                            <dxe:ListEditItem Value="GISI" Text="GISI" />
                                                                                            <dxe:ListEditItem Value="IC" Text="IC" />
                                                                                            <dxe:ListEditItem Value="IFP" Text="IFP" />
                                                                                            <dxe:ListEditItem Value="IIDAMP" Text="IIDAMP" />
                                                                                            <dxe:ListEditItem Value="IIDML" Text="IIDML" />
                                                                                            <dxe:ListEditItem Value="IIEP" Text="IIEP" />
                                                                                            <dxe:ListEditItem Value="IITP" Text="IITP" />
                                                                                            <dxe:ListEditItem Value="IMLC" Text="IMLC" />
                                                                                            <dxe:ListEditItem Value="INI" Text="INI" />
                                                                                            <dxe:ListEditItem Value="IPF" Text="IPF" />
                                                                                            <dxe:ListEditItem Value="IPT" Text="IPT" />
                                                                                            <dxe:ListEditItem Value="ITB" Text="ITB" />
                                                                                            <dxe:ListEditItem Value="JUIJ" Text="JUIJ" />
                                                                                            <dxe:ListEditItem Value="LAISSEZ" Text="LAISSEZ" />
                                                                                            <dxe:ListEditItem Value="MARINHA" Text="MARINHA" />
                                                                                            <dxe:ListEditItem Value="MC" Text="MC" />
                                                                                            <dxe:ListEditItem Value="MCT" Text="MCT" />
                                                                                            <dxe:ListEditItem Value="MCU" Text="MCU" />
                                                                                            <dxe:ListEditItem Value="MED" Text="MED" />
                                                                                            <dxe:ListEditItem Value="MEFP" Text="MEFP" />
                                                                                            <dxe:ListEditItem Value="MEUF" Text="MEUF" />
                                                                                            <dxe:ListEditItem Value="MF" Text="MF" />
                                                                                            <dxe:ListEditItem Value="MGUERRA" Text="MGUERRA" />
                                                                                            <dxe:ListEditItem Value="MI" Text="MI" />
                                                                                            <dxe:ListEditItem Value="MIC" Text="MIC" />
                                                                                            <dxe:ListEditItem Value="MJ" Text="MJ" />
                                                                                            <dxe:ListEditItem Value="MPAS" Text="MPAS" />
                                                                                            <dxe:ListEditItem Value="MPDFT" Text="MPDFT" />
                                                                                            <dxe:ListEditItem Value="MPEMG" Text="MPEMG" />
                                                                                            <dxe:ListEditItem Value="MPEMT" Text="MPEMT" />
                                                                                            <dxe:ListEditItem Value="MPEPR" Text="MPEPR" />
                                                                                            <dxe:ListEditItem Value="MPERJ" Text="MPERJ" />
                                                                                            <dxe:ListEditItem Value="MPESP" Text="MPESP" />
                                                                                            <dxe:ListEditItem Value="MPF" Text="MPF" />
                                                                                            <dxe:ListEditItem Value="MPM" Text="MPM" />
                                                                                            <dxe:ListEditItem Value="MPT" Text="MPT" />
                                                                                            <dxe:ListEditItem Value="MRE" Text="MRE" />
                                                                                            <dxe:ListEditItem Value="MS" Text="MS" />
                                                                                            <dxe:ListEditItem Value="MT" Text="MT" />
                                                                                            <dxe:ListEditItem Value="MTRANSP" Text="MTRANSP" />
                                                                                            <dxe:ListEditItem Value="OAB" Text="OAB" />
                                                                                            <dxe:ListEditItem Value="OMBCR" Text="OMBCR" />
                                                                                            <dxe:ListEditItem Value="P_JURIC" Text="P_JURIC" />
                                                                                            <dxe:ListEditItem Value="PASSPOR" Text="PASSPOR" />
                                                                                            <dxe:ListEditItem Value="PCIID" Text="PCIID" />
                                                                                            <dxe:ListEditItem Value="PCRJ" Text="PCRJ" />
                                                                                            <dxe:ListEditItem Value="PDFII" Text="PDFII" />
                                                                                            <dxe:ListEditItem Value="PFF" Text="PFF" />
                                                                                            <dxe:ListEditItem Value="PGE" Text="PGE" />
                                                                                            <dxe:ListEditItem Value="PGJ" Text="PGJ" />
                                                                                            <dxe:ListEditItem Value="PIG" Text="PIG" />
                                                                                            <dxe:ListEditItem Value="PJEG" Text="PJEG" />
                                                                                            <dxe:ListEditItem Value="PM" Text="PM" />
                                                                                            <dxe:ListEditItem Value="PMP" Text="PMP" />
                                                                                            <dxe:ListEditItem Value="RBR" Text="RBR" />
                                                                                            <dxe:ListEditItem Value="RFBR" Text="RFBR" />
                                                                                            <dxe:ListEditItem Value="RFF" Text="RFF" />
                                                                                            <dxe:ListEditItem Value="SADGP" Text="SADGP" />
                                                                                            <dxe:ListEditItem Value="SAPESS" Text="SAPESS" />
                                                                                            <dxe:ListEditItem Value="SC" Text="SC" />
                                                                                            <dxe:ListEditItem Value="SDPPES" Text="SDPPES" />
                                                                                            <dxe:ListEditItem Value="SEA" Text="SEA" />
                                                                                            <dxe:ListEditItem Value="SECT" Text="SECT" />
                                                                                            <dxe:ListEditItem Value="SEDPF" Text="SEDPF" />
                                                                                            <dxe:ListEditItem Value="SEDS" Text="SEDS" />
                                                                                            <dxe:ListEditItem Value="SEEC" Text="SEEC" />
                                                                                            <dxe:ListEditItem Value="SEF" Text="SEF" />
                                                                                            <dxe:ListEditItem Value="SEJSP" Text="SEJSP" />
                                                                                            <dxe:ListEditItem Value="SEJU" Text="SEJU" />
                                                                                            <dxe:ListEditItem Value="SEOMA" Text="SEOMA" />
                                                                                            <dxe:ListEditItem Value="SEPC" Text="SEPC" />
                                                                                            <dxe:ListEditItem Value="SES" Text="SES" />
                                                                                            <dxe:ListEditItem Value="SESP" Text="SESP" />
                                                                                            <dxe:ListEditItem Value="SF" Text="SF" />
                                                                                            <dxe:ListEditItem Value="SGPJ" Text="SGPJ" />
                                                                                            <dxe:ListEditItem Value="SIC" Text="SIC" />
                                                                                            <dxe:ListEditItem Value="SIJ" Text="SIJ" />
                                                                                            <dxe:ListEditItem Value="SJP" Text="SJP" />
                                                                                            <dxe:ListEditItem Value="SJS" Text="SJS" />
                                                                                            <dxe:ListEditItem Value="SJSP" Text="SJSP" />
                                                                                            <dxe:ListEditItem Value="SJTC" Text="SJTC" />
                                                                                            <dxe:ListEditItem Value="SPC" Text="SPC" />
                                                                                            <dxe:ListEditItem Value="SPCII" Text="SPCII" />
                                                                                            <dxe:ListEditItem Value="SPSP" Text="SPSP" />
                                                                                            <dxe:ListEditItem Value="SPTC" Text="SPTC" />
                                                                                            <dxe:ListEditItem Value="SRF" Text="SRF" />
                                                                                            <dxe:ListEditItem Value="SSI" Text="SSI" />
                                                                                            <dxe:ListEditItem Value="SSP" Text="SSP" />
                                                                                            <dxe:ListEditItem Value="SSPDC" Text="SSPDC" />
                                                                                            <dxe:ListEditItem Value="STM" Text="STM" />
                                                                                            <dxe:ListEditItem Value="TC" Text="TC" />
                                                                                            <dxe:ListEditItem Value="TJ" Text="TJ" />
                                                                                            <dxe:ListEditItem Value="TRE" Text="TRE" />
                                                                                            <dxe:ListEditItem Value="TRF" Text="TRF" />
                                                                                            <dxe:ListEditItem Value="TRIBALC" Text="TRIBALC" />
                                                                                            <dxe:ListEditItem Value="TRT" Text="TRT" />
                                                                                            <dxe:ListEditItem Value="TST" Text="TST" />
                                                                                            <dxe:ListEditItem Value="SEFAZ" Text="SEFAZ" />
                                                                                            <dxe:ListEditItem Value="JCDE" Text="JCDE" />
                                                                                            <dxe:ListEditItem Value="JUCAP" Text="JUCAP" />
                                                                                            <dxe:ListEditItem Value="JUCEA" Text="JUCEA" />
                                                                                            <dxe:ListEditItem Value="JUCEAC" Text="JUCEAC" />
                                                                                            <dxe:ListEditItem Value="JUCEAL" Text="JUCEAL" />
                                                                                            <dxe:ListEditItem Value="JUCEB" Text="JUCEB" />
                                                                                            <dxe:ListEditItem Value="JUCEC" Text="JUCEC" />
                                                                                            <dxe:ListEditItem Value="JUCEES" Text="JUCEES" />
                                                                                            <dxe:ListEditItem Value="JUCEG" Text="JUCEG" />
                                                                                            <dxe:ListEditItem Value="JUCEMA" Text="JUCEMA" />
                                                                                            <dxe:ListEditItem Value="JUCEMAT" Text="JUCEMAT" />
                                                                                            <dxe:ListEditItem Value="JUCEMG" Text="JUCEMG" />
                                                                                            <dxe:ListEditItem Value="JUCEMS" Text="JUCEMS" />
                                                                                            <dxe:ListEditItem Value="JUCEP" Text="JUCEP" />
                                                                                            <dxe:ListEditItem Value="JUCEPA" Text="JUCEPA" />
                                                                                            <dxe:ListEditItem Value="JUCEPAR" Text="JUCEPAR" />
                                                                                            <dxe:ListEditItem Value="JUCEPE" Text="JUCEPE" />
                                                                                            <dxe:ListEditItem Value="JUCEPI" Text="JUCEPI" />
                                                                                            <dxe:ListEditItem Value="JUCER" Text="JUCER" />
                                                                                            <dxe:ListEditItem Value="JUCERGS" Text="JUCERGS" />
                                                                                            <dxe:ListEditItem Value="JUCERJA" Text="JUCERJA" />
                                                                                            <dxe:ListEditItem Value="JUCERN" Text="JUCERN" />
                                                                                            <dxe:ListEditItem Value="JUCERR" Text="JUCERR" />
                                                                                            <dxe:ListEditItem Value="JUCESC" Text="JUCESC" />
                                                                                            <dxe:ListEditItem Value="JUCESE" Text="JUCESE" />
                                                                                            <dxe:ListEditItem Value="JUCESP" Text="JUCESP" />
                                                                                            <dxe:ListEditItem Value="JUCETIN" Text="JUCETIN" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelDataExpedicao" runat="server" CssClass="labelNormal" Text="Dt. Expedição:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataExpedicao" runat="server" ClientInstanceName="textDataExpedicao" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="UF Emissor:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropUFEmissor" runat="server" ClientInstanceName="dropUFEmissor"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_6">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="AC" Text="AC" />
                                                                                            <dxe:ListEditItem Value="AL" Text="AL" />
                                                                                            <dxe:ListEditItem Value="AP" Text="AP" />
                                                                                            <dxe:ListEditItem Value="AM" Text="AM" />
                                                                                            <dxe:ListEditItem Value="BA" Text="BA" />
                                                                                            <dxe:ListEditItem Value="CE" Text="CE" />
                                                                                            <dxe:ListEditItem Value="DF" Text="DF" />
                                                                                            <dxe:ListEditItem Value="ES" Text="ES" />
                                                                                            <dxe:ListEditItem Value="GO" Text="GO" />
                                                                                            <dxe:ListEditItem Value="MA" Text="MA" />
                                                                                            <dxe:ListEditItem Value="MT" Text="MT" />
                                                                                            <dxe:ListEditItem Value="MS" Text="MS" />
                                                                                            <dxe:ListEditItem Value="MG" Text="MG" />
                                                                                            <dxe:ListEditItem Value="PA" Text="PA" />
                                                                                            <dxe:ListEditItem Value="PB" Text="PB" />
                                                                                            <dxe:ListEditItem Value="PR" Text="PR" />
                                                                                            <dxe:ListEditItem Value="PE" Text="PE" />
                                                                                            <dxe:ListEditItem Value="PI" Text="PI" />
                                                                                            <dxe:ListEditItem Value="RJ" Text="RJ" />
                                                                                            <dxe:ListEditItem Value="RN" Text="RN" />
                                                                                            <dxe:ListEditItem Value="RS" Text="RS" />
                                                                                            <dxe:ListEditItem Value="RO" Text="RO" />
                                                                                            <dxe:ListEditItem Value="RR" Text="RR" />
                                                                                            <dxe:ListEditItem Value="SC" Text="SC" />
                                                                                            <dxe:ListEditItem Value="SP" Text="SP" />
                                                                                            <dxe:ListEditItem Value="SE" Text="SE" />
                                                                                            <dxe:ListEditItem Value="TO" Text="TO" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label49" runat="server" CssClass="labelNormal" Text="Dt. Vencimento:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label25" runat="server" CssClass="labelNormal" Text="Tipo:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoDocumento1" runat="server" ClientInstanceName="dropTipoDocumento1"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo">
                                                                                        <ClientSideEvents ValueChanged="function(s, e) { 
                                                                                                                if(s.GetText()==''){
                                                                                                                    txtNumDocumento1.SetValue('');
                                                                                                                    dropOrgaoEmissor1.SetValue('');
                                                                                                                    textDataExpedicao1.SetValue(null);
                                                                                                                    dropUFEmissor1.SetValue('');}
                                                                                                                    }" />
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Cédula Identidade (CI)" />
                                                                                            <dxe:ListEditItem Value="2" Text="Cédula Identidade Estrangeiro (CIE)" />
                                                                                            <dxe:ListEditItem Value="3" Text="Carteira Militar" />
                                                                                            <dxe:ListEditItem Value="4" Text="Identidade Profissional (CIP)" />
                                                                                            <dxe:ListEditItem Value="5" Text="Cédula Identidade Profissional (Público)" />
                                                                                            <dxe:ListEditItem Value="6" Text="Certidão Nascimento" />
                                                                                            <dxe:ListEditItem Value="7" Text="Carteira Habilitação" />
                                                                                            <dxe:ListEditItem Value="8" Text="Carteira Trabalho" />
                                                                                            <dxe:ListEditItem Value="9" Text="Identificação Prisional" />
                                                                                            <dxe:ListEditItem Value="10" Text="Laissez Passer" />
                                                                                            <dxe:ListEditItem Value="11" Text="Passaporte" />
                                                                                            <dxe:ListEditItem Value="12" Text="Cédula Identidade (RG)" />
                                                                                            <dxe:ListEditItem Value="13" Text="Documento Identidade (RI)" />
                                                                                            <dxe:ListEditItem Value="14" Text="Cédula Identidade Estrangeiro (RNE)" />
                                                                                            <dxe:ListEditItem Value="15" Text="Inscrição Estadual" />
                                                                                            <dxe:ListEditItem Value="16" Text="NIRE" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label26" runat="server" CssClass="labelNormal" Text="Doc.:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtNumDocumento1" ClientInstanceName="txtNumDocumento1" runat="server"
                                                                                        CssClass="textNormal_5" MaxLength="13" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label27" runat="server" CssClass="labelNormal" Text="Orgão Emissor:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropOrgaoEmissor1" runat="server" ClientInstanceName="dropOrgaoEmissor1"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_2">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="ABEXCON" Text="ABEXCON" />
                                                                                            <dxe:ListEditItem Value="AERON" Text="AERON" />
                                                                                            <dxe:ListEditItem Value="ASLEGIS" Text="ASLEGIS" />
                                                                                            <dxe:ListEditItem Value="CBM" Text="CBM" />
                                                                                            <dxe:ListEditItem Value="CFP" Text="CFP" />
                                                                                            <dxe:ListEditItem Value="CGJ" Text="CGJ" />
                                                                                            <dxe:ListEditItem Value="CMRJ" Text="CMRJ" />
                                                                                            <dxe:ListEditItem Value="CNDL" Text="CNDL" />
                                                                                            <dxe:ListEditItem Value="CNE" Text="CNE" />
                                                                                            <dxe:ListEditItem Value="CNEN" Text="CNEN" />
                                                                                            <dxe:ListEditItem Value="CONDEBR" Text="CONDEBR" />
                                                                                            <dxe:ListEditItem Value="CONFEV" Text="CONFEV" />
                                                                                            <dxe:ListEditItem Value="CONFIPA" Text="CONFIPA" />
                                                                                            <dxe:ListEditItem Value="CONRE" Text="CONRE" />
                                                                                            <dxe:ListEditItem Value="CONRERP" Text="CONRERP" />
                                                                                            <dxe:ListEditItem Value="CORE" Text="CORE" />
                                                                                            <dxe:ListEditItem Value="CORECON" Text="CORECON" />
                                                                                            <dxe:ListEditItem Value="COREM" Text="COREM" />
                                                                                            <dxe:ListEditItem Value="COREN" Text="COREN" />
                                                                                            <dxe:ListEditItem Value="CRA" Text="CRA" />
                                                                                            <dxe:ListEditItem Value="CRAS" Text="CRAS" />
                                                                                            <dxe:ListEditItem Value="CRB" Text="CRB" />
                                                                                            <dxe:ListEditItem Value="CRBM" Text="CRBM" />
                                                                                            <dxe:ListEditItem Value="CRC" Text="CRC" />
                                                                                            <dxe:ListEditItem Value="CRCPN" Text="CRCPN" />
                                                                                            <dxe:ListEditItem Value="CREA" Text="CREA" />
                                                                                            <dxe:ListEditItem Value="CRECI" Text="CRECI" />
                                                                                            <dxe:ListEditItem Value="CREF" Text="CREF" />
                                                                                            <dxe:ListEditItem Value="CRESS" Text="CRESS" />
                                                                                            <dxe:ListEditItem Value="CRF" Text="CRF" />
                                                                                            <dxe:ListEditItem Value="CRFA" Text="CRFA" />
                                                                                            <dxe:ListEditItem Value="CRFTO" Text="CRFTO" />
                                                                                            <dxe:ListEditItem Value="CRM" Text="CRM" />
                                                                                            <dxe:ListEditItem Value="CRMV" Text="CRMV" />
                                                                                            <dxe:ListEditItem Value="CRN" Text="CRN" />
                                                                                            <dxe:ListEditItem Value="CRO" Text="CRO" />
                                                                                            <dxe:ListEditItem Value="CRP" Text="CRP" />
                                                                                            <dxe:ListEditItem Value="CRQ" Text="CRQ" />
                                                                                            <dxe:ListEditItem Value="CRTA" Text="CRTA" />
                                                                                            <dxe:ListEditItem Value="CRTR" Text="CRTR" />
                                                                                            <dxe:ListEditItem Value="CTMTB" Text="CTMTB" />
                                                                                            <dxe:ListEditItem Value="DEF" Text="DEF" />
                                                                                            <dxe:ListEditItem Value="DEPCT" Text="DEPCT" />
                                                                                            <dxe:ListEditItem Value="DESIPE" Text="DESIPE" />
                                                                                            <dxe:ListEditItem Value="DESP" Text="DESP" />
                                                                                            <dxe:ListEditItem Value="DETRAN" Text="DETRAN" />
                                                                                            <dxe:ListEditItem Value="DFSP" Text="DFSP" />
                                                                                            <dxe:ListEditItem Value="DGPC" Text="DGPC" />
                                                                                            <dxe:ListEditItem Value="DI" Text="DI" />
                                                                                            <dxe:ListEditItem Value="DPF" Text="DPF" />
                                                                                            <dxe:ListEditItem Value="DPGE" Text="DPGE" />
                                                                                            <dxe:ListEditItem Value="DPTC" Text="DPTC" />
                                                                                            <dxe:ListEditItem Value="DSG" Text="DSG" />
                                                                                            <dxe:ListEditItem Value="EAPJU" Text="EAPJU" />
                                                                                            <dxe:ListEditItem Value="ESG" Text="ESG" />
                                                                                            <dxe:ListEditItem Value="EXERCIT" Text="EXERCIT" />
                                                                                            <dxe:ListEditItem Value="FENAJ" Text="FENAJ" />
                                                                                            <dxe:ListEditItem Value="FUNAI" Text="FUNAI" />
                                                                                            <dxe:ListEditItem Value="GEJ" Text="GEJ" />
                                                                                            <dxe:ListEditItem Value="GISI" Text="GISI" />
                                                                                            <dxe:ListEditItem Value="IC" Text="IC" />
                                                                                            <dxe:ListEditItem Value="IFP" Text="IFP" />
                                                                                            <dxe:ListEditItem Value="IIDAMP" Text="IIDAMP" />
                                                                                            <dxe:ListEditItem Value="IIDML" Text="IIDML" />
                                                                                            <dxe:ListEditItem Value="IIEP" Text="IIEP" />
                                                                                            <dxe:ListEditItem Value="IITP" Text="IITP" />
                                                                                            <dxe:ListEditItem Value="IMLC" Text="IMLC" />
                                                                                            <dxe:ListEditItem Value="INI" Text="INI" />
                                                                                            <dxe:ListEditItem Value="IPF" Text="IPF" />
                                                                                            <dxe:ListEditItem Value="IPT" Text="IPT" />
                                                                                            <dxe:ListEditItem Value="ITB" Text="ITB" />
                                                                                            <dxe:ListEditItem Value="JUIJ" Text="JUIJ" />
                                                                                            <dxe:ListEditItem Value="LAISSEZ" Text="LAISSEZ" />
                                                                                            <dxe:ListEditItem Value="MARINHA" Text="MARINHA" />
                                                                                            <dxe:ListEditItem Value="MC" Text="MC" />
                                                                                            <dxe:ListEditItem Value="MCT" Text="MCT" />
                                                                                            <dxe:ListEditItem Value="MCU" Text="MCU" />
                                                                                            <dxe:ListEditItem Value="MED" Text="MED" />
                                                                                            <dxe:ListEditItem Value="MEFP" Text="MEFP" />
                                                                                            <dxe:ListEditItem Value="MEUF" Text="MEUF" />
                                                                                            <dxe:ListEditItem Value="MF" Text="MF" />
                                                                                            <dxe:ListEditItem Value="MGUERRA" Text="MGUERRA" />
                                                                                            <dxe:ListEditItem Value="MI" Text="MI" />
                                                                                            <dxe:ListEditItem Value="MIC" Text="MIC" />
                                                                                            <dxe:ListEditItem Value="MJ" Text="MJ" />
                                                                                            <dxe:ListEditItem Value="MPAS" Text="MPAS" />
                                                                                            <dxe:ListEditItem Value="MPDFT" Text="MPDFT" />
                                                                                            <dxe:ListEditItem Value="MPEMG" Text="MPEMG" />
                                                                                            <dxe:ListEditItem Value="MPEMT" Text="MPEMT" />
                                                                                            <dxe:ListEditItem Value="MPEPR" Text="MPEPR" />
                                                                                            <dxe:ListEditItem Value="MPERJ" Text="MPERJ" />
                                                                                            <dxe:ListEditItem Value="MPESP" Text="MPESP" />
                                                                                            <dxe:ListEditItem Value="MPF" Text="MPF" />
                                                                                            <dxe:ListEditItem Value="MPM" Text="MPM" />
                                                                                            <dxe:ListEditItem Value="MPT" Text="MPT" />
                                                                                            <dxe:ListEditItem Value="MRE" Text="MRE" />
                                                                                            <dxe:ListEditItem Value="MS" Text="MS" />
                                                                                            <dxe:ListEditItem Value="MT" Text="MT" />
                                                                                            <dxe:ListEditItem Value="MTRANSP" Text="MTRANSP" />
                                                                                            <dxe:ListEditItem Value="OAB" Text="OAB" />
                                                                                            <dxe:ListEditItem Value="OMBCR" Text="OMBCR" />
                                                                                            <dxe:ListEditItem Value="P_JURIC" Text="P_JURIC" />
                                                                                            <dxe:ListEditItem Value="PASSPOR" Text="PASSPOR" />
                                                                                            <dxe:ListEditItem Value="PCIID" Text="PCIID" />
                                                                                            <dxe:ListEditItem Value="PCRJ" Text="PCRJ" />
                                                                                            <dxe:ListEditItem Value="PDFII" Text="PDFII" />
                                                                                            <dxe:ListEditItem Value="PFF" Text="PFF" />
                                                                                            <dxe:ListEditItem Value="PGE" Text="PGE" />
                                                                                            <dxe:ListEditItem Value="PGJ" Text="PGJ" />
                                                                                            <dxe:ListEditItem Value="PIG" Text="PIG" />
                                                                                            <dxe:ListEditItem Value="PJEG" Text="PJEG" />
                                                                                            <dxe:ListEditItem Value="PM" Text="PM" />
                                                                                            <dxe:ListEditItem Value="PMP" Text="PMP" />
                                                                                            <dxe:ListEditItem Value="RBR" Text="RBR" />
                                                                                            <dxe:ListEditItem Value="RFBR" Text="RFBR" />
                                                                                            <dxe:ListEditItem Value="RFF" Text="RFF" />
                                                                                            <dxe:ListEditItem Value="SADGP" Text="SADGP" />
                                                                                            <dxe:ListEditItem Value="SAPESS" Text="SAPESS" />
                                                                                            <dxe:ListEditItem Value="SC" Text="SC" />
                                                                                            <dxe:ListEditItem Value="SDPPES" Text="SDPPES" />
                                                                                            <dxe:ListEditItem Value="SEA" Text="SEA" />
                                                                                            <dxe:ListEditItem Value="SECT" Text="SECT" />
                                                                                            <dxe:ListEditItem Value="SEDPF" Text="SEDPF" />
                                                                                            <dxe:ListEditItem Value="SEDS" Text="SEDS" />
                                                                                            <dxe:ListEditItem Value="SEEC" Text="SEEC" />
                                                                                            <dxe:ListEditItem Value="SEF" Text="SEF" />
                                                                                            <dxe:ListEditItem Value="SEJSP" Text="SEJSP" />
                                                                                            <dxe:ListEditItem Value="SEJU" Text="SEJU" />
                                                                                            <dxe:ListEditItem Value="SEOMA" Text="SEOMA" />
                                                                                            <dxe:ListEditItem Value="SEPC" Text="SEPC" />
                                                                                            <dxe:ListEditItem Value="SES" Text="SES" />
                                                                                            <dxe:ListEditItem Value="SESP" Text="SESP" />
                                                                                            <dxe:ListEditItem Value="SF" Text="SF" />
                                                                                            <dxe:ListEditItem Value="SGPJ" Text="SGPJ" />
                                                                                            <dxe:ListEditItem Value="SIC" Text="SIC" />
                                                                                            <dxe:ListEditItem Value="SIJ" Text="SIJ" />
                                                                                            <dxe:ListEditItem Value="SJP" Text="SJP" />
                                                                                            <dxe:ListEditItem Value="SJS" Text="SJS" />
                                                                                            <dxe:ListEditItem Value="SJSP" Text="SJSP" />
                                                                                            <dxe:ListEditItem Value="SJTC" Text="SJTC" />
                                                                                            <dxe:ListEditItem Value="SPC" Text="SPC" />
                                                                                            <dxe:ListEditItem Value="SPCII" Text="SPCII" />
                                                                                            <dxe:ListEditItem Value="SPSP" Text="SPSP" />
                                                                                            <dxe:ListEditItem Value="SPTC" Text="SPTC" />
                                                                                            <dxe:ListEditItem Value="SRF" Text="SRF" />
                                                                                            <dxe:ListEditItem Value="SSI" Text="SSI" />
                                                                                            <dxe:ListEditItem Value="SSP" Text="SSP" />
                                                                                            <dxe:ListEditItem Value="SSPDC" Text="SSPDC" />
                                                                                            <dxe:ListEditItem Value="STM" Text="STM" />
                                                                                            <dxe:ListEditItem Value="TC" Text="TC" />
                                                                                            <dxe:ListEditItem Value="TJ" Text="TJ" />
                                                                                            <dxe:ListEditItem Value="TRE" Text="TRE" />
                                                                                            <dxe:ListEditItem Value="TRF" Text="TRF" />
                                                                                            <dxe:ListEditItem Value="TRIBALC" Text="TRIBALC" />
                                                                                            <dxe:ListEditItem Value="TRT" Text="TRT" />
                                                                                            <dxe:ListEditItem Value="TST" Text="TST" />
                                                                                            <dxe:ListEditItem Value="SEFAZ" Text="SEFAZ" />
                                                                                            <dxe:ListEditItem Value="JCDE" Text="JCDE" />
                                                                                            <dxe:ListEditItem Value="JUCAP" Text="JUCAP" />
                                                                                            <dxe:ListEditItem Value="JUCEA" Text="JUCEA" />
                                                                                            <dxe:ListEditItem Value="JUCEAC" Text="JUCEAC" />
                                                                                            <dxe:ListEditItem Value="JUCEAL" Text="JUCEAL" />
                                                                                            <dxe:ListEditItem Value="JUCEB" Text="JUCEB" />
                                                                                            <dxe:ListEditItem Value="JUCEC" Text="JUCEC" />
                                                                                            <dxe:ListEditItem Value="JUCEES" Text="JUCEES" />
                                                                                            <dxe:ListEditItem Value="JUCEG" Text="JUCEG" />
                                                                                            <dxe:ListEditItem Value="JUCEMA" Text="JUCEMA" />
                                                                                            <dxe:ListEditItem Value="JUCEMAT" Text="JUCEMAT" />
                                                                                            <dxe:ListEditItem Value="JUCEMG" Text="JUCEMG" />
                                                                                            <dxe:ListEditItem Value="JUCEMS" Text="JUCEMS" />
                                                                                            <dxe:ListEditItem Value="JUCEP" Text="JUCEP" />
                                                                                            <dxe:ListEditItem Value="JUCEPA" Text="JUCEPA" />
                                                                                            <dxe:ListEditItem Value="JUCEPAR" Text="JUCEPAR" />
                                                                                            <dxe:ListEditItem Value="JUCEPE" Text="JUCEPE" />
                                                                                            <dxe:ListEditItem Value="JUCEPI" Text="JUCEPI" />
                                                                                            <dxe:ListEditItem Value="JUCER" Text="JUCER" />
                                                                                            <dxe:ListEditItem Value="JUCERGS" Text="JUCERGS" />
                                                                                            <dxe:ListEditItem Value="JUCERJA" Text="JUCERJA" />
                                                                                            <dxe:ListEditItem Value="JUCERN" Text="JUCERN" />
                                                                                            <dxe:ListEditItem Value="JUCERR" Text="JUCERR" />
                                                                                            <dxe:ListEditItem Value="JUCESC" Text="JUCESC" />
                                                                                            <dxe:ListEditItem Value="JUCESE" Text="JUCESE" />
                                                                                            <dxe:ListEditItem Value="JUCESP" Text="JUCESP" />
                                                                                            <dxe:ListEditItem Value="JUCETIN" Text="JUCETIN" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label28" runat="server" CssClass="labelNormal" Text="Dt. Expedição:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataExpedicao1" runat="server" ClientInstanceName="textDataExpedicao1" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label29" runat="server" CssClass="labelNormal" Text="UF Emissor:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropUFEmissor1" runat="server" ClientInstanceName="dropUFEmissor1"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_6">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="AC" Text="AC" />
                                                                                            <dxe:ListEditItem Value="AL" Text="AL" />
                                                                                            <dxe:ListEditItem Value="AP" Text="AP" />
                                                                                            <dxe:ListEditItem Value="AM" Text="AM" />
                                                                                            <dxe:ListEditItem Value="BA" Text="BA" />
                                                                                            <dxe:ListEditItem Value="CE" Text="CE" />
                                                                                            <dxe:ListEditItem Value="DF" Text="DF" />
                                                                                            <dxe:ListEditItem Value="ES" Text="ES" />
                                                                                            <dxe:ListEditItem Value="GO" Text="GO" />
                                                                                            <dxe:ListEditItem Value="MA" Text="MA" />
                                                                                            <dxe:ListEditItem Value="MT" Text="MT" />
                                                                                            <dxe:ListEditItem Value="MS" Text="MS" />
                                                                                            <dxe:ListEditItem Value="MG" Text="MG" />
                                                                                            <dxe:ListEditItem Value="PA" Text="PA" />
                                                                                            <dxe:ListEditItem Value="PB" Text="PB" />
                                                                                            <dxe:ListEditItem Value="PR" Text="PR" />
                                                                                            <dxe:ListEditItem Value="PE" Text="PE" />
                                                                                            <dxe:ListEditItem Value="PI" Text="PI" />
                                                                                            <dxe:ListEditItem Value="RJ" Text="RJ" />
                                                                                            <dxe:ListEditItem Value="RN" Text="RN" />
                                                                                            <dxe:ListEditItem Value="RS" Text="RS" />
                                                                                            <dxe:ListEditItem Value="RO" Text="RO" />
                                                                                            <dxe:ListEditItem Value="RR" Text="RR" />
                                                                                            <dxe:ListEditItem Value="SC" Text="SC" />
                                                                                            <dxe:ListEditItem Value="SP" Text="SP" />
                                                                                            <dxe:ListEditItem Value="SE" Text="SE" />
                                                                                            <dxe:ListEditItem Value="TO" Text="TO" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label47" runat="server" CssClass="labelNormal" Text="Dt. Vencimento:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataVencimento1" runat="server" ClientInstanceName="textDataVencimento1" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label30" runat="server" CssClass="labelNormal" Text="Tipo:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoDocumento2" runat="server" ClientInstanceName="dropTipoDocumento2"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo">
                                                                                        <ClientSideEvents ValueChanged="function(s, e) { 
                                                                                                                if(s.GetText()==''){
                                                                                                                    txtNumDocumento2.SetValue('');
                                                                                                                    dropOrgaoEmissor2.SetValue('');
                                                                                                                    textDataExpedicao2.SetValue(null);
                                                                                                                    dropUFEmissor2.SetValue('');}
                                                                                                                    }" />
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Cédula Identidade (CI)" />
                                                                                            <dxe:ListEditItem Value="2" Text="Cédula Identidade Estrangeiro (CIE)" />
                                                                                            <dxe:ListEditItem Value="3" Text="Carteira Militar" />
                                                                                            <dxe:ListEditItem Value="4" Text="Identidade Profissional (CIP)" />
                                                                                            <dxe:ListEditItem Value="5" Text="Cédula Identidade Profissional (Público)" />
                                                                                            <dxe:ListEditItem Value="6" Text="Certidão Nascimento" />
                                                                                            <dxe:ListEditItem Value="7" Text="Carteira Habilitação" />
                                                                                            <dxe:ListEditItem Value="8" Text="Carteira Trabalho" />
                                                                                            <dxe:ListEditItem Value="9" Text="Identificação Prisional" />
                                                                                            <dxe:ListEditItem Value="10" Text="Laissez Passer" />
                                                                                            <dxe:ListEditItem Value="11" Text="Passaporte" />
                                                                                            <dxe:ListEditItem Value="12" Text="Cédula Identidade (RG)" />
                                                                                            <dxe:ListEditItem Value="13" Text="Documento Identidade (RI)" />
                                                                                            <dxe:ListEditItem Value="14" Text="Cédula Identidade Estrangeiro (RNE)" />
                                                                                            <dxe:ListEditItem Value="15" Text="Inscrição Estadual" />
                                                                                            <dxe:ListEditItem Value="16" Text="NIRE" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label31" runat="server" CssClass="labelNormal" Text="Doc.:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="txtNumDocumento2" ClientInstanceName="txtNumDocumento2" runat="server"
                                                                                        CssClass="textNormal_5" MaxLength="13" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label32" runat="server" CssClass="labelNormal" Text="Orgão Emissor:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropOrgaoEmissor2" runat="server" ClientInstanceName="dropOrgaoEmissor2"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_2">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="ABEXCON" Text="ABEXCON" />
                                                                                            <dxe:ListEditItem Value="AERON" Text="AERON" />
                                                                                            <dxe:ListEditItem Value="ASLEGIS" Text="ASLEGIS" />
                                                                                            <dxe:ListEditItem Value="CBM" Text="CBM" />
                                                                                            <dxe:ListEditItem Value="CFP" Text="CFP" />
                                                                                            <dxe:ListEditItem Value="CGJ" Text="CGJ" />
                                                                                            <dxe:ListEditItem Value="CMRJ" Text="CMRJ" />
                                                                                            <dxe:ListEditItem Value="CNDL" Text="CNDL" />
                                                                                            <dxe:ListEditItem Value="CNE" Text="CNE" />
                                                                                            <dxe:ListEditItem Value="CNEN" Text="CNEN" />
                                                                                            <dxe:ListEditItem Value="CONDEBR" Text="CONDEBR" />
                                                                                            <dxe:ListEditItem Value="CONFEV" Text="CONFEV" />
                                                                                            <dxe:ListEditItem Value="CONFIPA" Text="CONFIPA" />
                                                                                            <dxe:ListEditItem Value="CONRE" Text="CONRE" />
                                                                                            <dxe:ListEditItem Value="CONRERP" Text="CONRERP" />
                                                                                            <dxe:ListEditItem Value="CORE" Text="CORE" />
                                                                                            <dxe:ListEditItem Value="CORECON" Text="CORECON" />
                                                                                            <dxe:ListEditItem Value="COREM" Text="COREM" />
                                                                                            <dxe:ListEditItem Value="COREN" Text="COREN" />
                                                                                            <dxe:ListEditItem Value="CRA" Text="CRA" />
                                                                                            <dxe:ListEditItem Value="CRAS" Text="CRAS" />
                                                                                            <dxe:ListEditItem Value="CRB" Text="CRB" />
                                                                                            <dxe:ListEditItem Value="CRBM" Text="CRBM" />
                                                                                            <dxe:ListEditItem Value="CRC" Text="CRC" />
                                                                                            <dxe:ListEditItem Value="CRCPN" Text="CRCPN" />
                                                                                            <dxe:ListEditItem Value="CREA" Text="CREA" />
                                                                                            <dxe:ListEditItem Value="CRECI" Text="CRECI" />
                                                                                            <dxe:ListEditItem Value="CREF" Text="CREF" />
                                                                                            <dxe:ListEditItem Value="CRESS" Text="CRESS" />
                                                                                            <dxe:ListEditItem Value="CRF" Text="CRF" />
                                                                                            <dxe:ListEditItem Value="CRFA" Text="CRFA" />
                                                                                            <dxe:ListEditItem Value="CRFTO" Text="CRFTO" />
                                                                                            <dxe:ListEditItem Value="CRM" Text="CRM" />
                                                                                            <dxe:ListEditItem Value="CRMV" Text="CRMV" />
                                                                                            <dxe:ListEditItem Value="CRN" Text="CRN" />
                                                                                            <dxe:ListEditItem Value="CRO" Text="CRO" />
                                                                                            <dxe:ListEditItem Value="CRP" Text="CRP" />
                                                                                            <dxe:ListEditItem Value="CRQ" Text="CRQ" />
                                                                                            <dxe:ListEditItem Value="CRTA" Text="CRTA" />
                                                                                            <dxe:ListEditItem Value="CRTR" Text="CRTR" />
                                                                                            <dxe:ListEditItem Value="CTMTB" Text="CTMTB" />
                                                                                            <dxe:ListEditItem Value="DEF" Text="DEF" />
                                                                                            <dxe:ListEditItem Value="DEPCT" Text="DEPCT" />
                                                                                            <dxe:ListEditItem Value="DESIPE" Text="DESIPE" />
                                                                                            <dxe:ListEditItem Value="DESP" Text="DESP" />
                                                                                            <dxe:ListEditItem Value="DETRAN" Text="DETRAN" />
                                                                                            <dxe:ListEditItem Value="DFSP" Text="DFSP" />
                                                                                            <dxe:ListEditItem Value="DGPC" Text="DGPC" />
                                                                                            <dxe:ListEditItem Value="DI" Text="DI" />
                                                                                            <dxe:ListEditItem Value="DPF" Text="DPF" />
                                                                                            <dxe:ListEditItem Value="DPGE" Text="DPGE" />
                                                                                            <dxe:ListEditItem Value="DPTC" Text="DPTC" />
                                                                                            <dxe:ListEditItem Value="DSG" Text="DSG" />
                                                                                            <dxe:ListEditItem Value="EAPJU" Text="EAPJU" />
                                                                                            <dxe:ListEditItem Value="ESG" Text="ESG" />
                                                                                            <dxe:ListEditItem Value="EXERCIT" Text="EXERCIT" />
                                                                                            <dxe:ListEditItem Value="FENAJ" Text="FENAJ" />
                                                                                            <dxe:ListEditItem Value="FUNAI" Text="FUNAI" />
                                                                                            <dxe:ListEditItem Value="GEJ" Text="GEJ" />
                                                                                            <dxe:ListEditItem Value="GISI" Text="GISI" />
                                                                                            <dxe:ListEditItem Value="IC" Text="IC" />
                                                                                            <dxe:ListEditItem Value="IFP" Text="IFP" />
                                                                                            <dxe:ListEditItem Value="IIDAMP" Text="IIDAMP" />
                                                                                            <dxe:ListEditItem Value="IIDML" Text="IIDML" />
                                                                                            <dxe:ListEditItem Value="IIEP" Text="IIEP" />
                                                                                            <dxe:ListEditItem Value="IITP" Text="IITP" />
                                                                                            <dxe:ListEditItem Value="IMLC" Text="IMLC" />
                                                                                            <dxe:ListEditItem Value="INI" Text="INI" />
                                                                                            <dxe:ListEditItem Value="IPF" Text="IPF" />
                                                                                            <dxe:ListEditItem Value="IPT" Text="IPT" />
                                                                                            <dxe:ListEditItem Value="ITB" Text="ITB" />
                                                                                            <dxe:ListEditItem Value="JUIJ" Text="JUIJ" />
                                                                                            <dxe:ListEditItem Value="LAISSEZ" Text="LAISSEZ" />
                                                                                            <dxe:ListEditItem Value="MARINHA" Text="MARINHA" />
                                                                                            <dxe:ListEditItem Value="MC" Text="MC" />
                                                                                            <dxe:ListEditItem Value="MCT" Text="MCT" />
                                                                                            <dxe:ListEditItem Value="MCU" Text="MCU" />
                                                                                            <dxe:ListEditItem Value="MED" Text="MED" />
                                                                                            <dxe:ListEditItem Value="MEFP" Text="MEFP" />
                                                                                            <dxe:ListEditItem Value="MEUF" Text="MEUF" />
                                                                                            <dxe:ListEditItem Value="MF" Text="MF" />
                                                                                            <dxe:ListEditItem Value="MGUERRA" Text="MGUERRA" />
                                                                                            <dxe:ListEditItem Value="MI" Text="MI" />
                                                                                            <dxe:ListEditItem Value="MIC" Text="MIC" />
                                                                                            <dxe:ListEditItem Value="MJ" Text="MJ" />
                                                                                            <dxe:ListEditItem Value="MPAS" Text="MPAS" />
                                                                                            <dxe:ListEditItem Value="MPDFT" Text="MPDFT" />
                                                                                            <dxe:ListEditItem Value="MPEMG" Text="MPEMG" />
                                                                                            <dxe:ListEditItem Value="MPEMT" Text="MPEMT" />
                                                                                            <dxe:ListEditItem Value="MPEPR" Text="MPEPR" />
                                                                                            <dxe:ListEditItem Value="MPERJ" Text="MPERJ" />
                                                                                            <dxe:ListEditItem Value="MPESP" Text="MPESP" />
                                                                                            <dxe:ListEditItem Value="MPF" Text="MPF" />
                                                                                            <dxe:ListEditItem Value="MPM" Text="MPM" />
                                                                                            <dxe:ListEditItem Value="MPT" Text="MPT" />
                                                                                            <dxe:ListEditItem Value="MRE" Text="MRE" />
                                                                                            <dxe:ListEditItem Value="MS" Text="MS" />
                                                                                            <dxe:ListEditItem Value="MT" Text="MT" />
                                                                                            <dxe:ListEditItem Value="MTRANSP" Text="MTRANSP" />
                                                                                            <dxe:ListEditItem Value="OAB" Text="OAB" />
                                                                                            <dxe:ListEditItem Value="OMBCR" Text="OMBCR" />
                                                                                            <dxe:ListEditItem Value="P_JURIC" Text="P_JURIC" />
                                                                                            <dxe:ListEditItem Value="PASSPOR" Text="PASSPOR" />
                                                                                            <dxe:ListEditItem Value="PCIID" Text="PCIID" />
                                                                                            <dxe:ListEditItem Value="PCRJ" Text="PCRJ" />
                                                                                            <dxe:ListEditItem Value="PDFII" Text="PDFII" />
                                                                                            <dxe:ListEditItem Value="PFF" Text="PFF" />
                                                                                            <dxe:ListEditItem Value="PGE" Text="PGE" />
                                                                                            <dxe:ListEditItem Value="PGJ" Text="PGJ" />
                                                                                            <dxe:ListEditItem Value="PIG" Text="PIG" />
                                                                                            <dxe:ListEditItem Value="PJEG" Text="PJEG" />
                                                                                            <dxe:ListEditItem Value="PM" Text="PM" />
                                                                                            <dxe:ListEditItem Value="PMP" Text="PMP" />
                                                                                            <dxe:ListEditItem Value="RBR" Text="RBR" />
                                                                                            <dxe:ListEditItem Value="RFBR" Text="RFBR" />
                                                                                            <dxe:ListEditItem Value="RFF" Text="RFF" />
                                                                                            <dxe:ListEditItem Value="SADGP" Text="SADGP" />
                                                                                            <dxe:ListEditItem Value="SAPESS" Text="SAPESS" />
                                                                                            <dxe:ListEditItem Value="SC" Text="SC" />
                                                                                            <dxe:ListEditItem Value="SDPPES" Text="SDPPES" />
                                                                                            <dxe:ListEditItem Value="SEA" Text="SEA" />
                                                                                            <dxe:ListEditItem Value="SECT" Text="SECT" />
                                                                                            <dxe:ListEditItem Value="SEDPF" Text="SEDPF" />
                                                                                            <dxe:ListEditItem Value="SEDS" Text="SEDS" />
                                                                                            <dxe:ListEditItem Value="SEEC" Text="SEEC" />
                                                                                            <dxe:ListEditItem Value="SEF" Text="SEF" />
                                                                                            <dxe:ListEditItem Value="SEJSP" Text="SEJSP" />
                                                                                            <dxe:ListEditItem Value="SEJU" Text="SEJU" />
                                                                                            <dxe:ListEditItem Value="SEOMA" Text="SEOMA" />
                                                                                            <dxe:ListEditItem Value="SEPC" Text="SEPC" />
                                                                                            <dxe:ListEditItem Value="SES" Text="SES" />
                                                                                            <dxe:ListEditItem Value="SESP" Text="SESP" />
                                                                                            <dxe:ListEditItem Value="SF" Text="SF" />
                                                                                            <dxe:ListEditItem Value="SGPJ" Text="SGPJ" />
                                                                                            <dxe:ListEditItem Value="SIC" Text="SIC" />
                                                                                            <dxe:ListEditItem Value="SIJ" Text="SIJ" />
                                                                                            <dxe:ListEditItem Value="SJP" Text="SJP" />
                                                                                            <dxe:ListEditItem Value="SJS" Text="SJS" />
                                                                                            <dxe:ListEditItem Value="SJSP" Text="SJSP" />
                                                                                            <dxe:ListEditItem Value="SJTC" Text="SJTC" />
                                                                                            <dxe:ListEditItem Value="SPC" Text="SPC" />
                                                                                            <dxe:ListEditItem Value="SPCII" Text="SPCII" />
                                                                                            <dxe:ListEditItem Value="SPSP" Text="SPSP" />
                                                                                            <dxe:ListEditItem Value="SPTC" Text="SPTC" />
                                                                                            <dxe:ListEditItem Value="SRF" Text="SRF" />
                                                                                            <dxe:ListEditItem Value="SSI" Text="SSI" />
                                                                                            <dxe:ListEditItem Value="SSP" Text="SSP" />
                                                                                            <dxe:ListEditItem Value="SSPDC" Text="SSPDC" />
                                                                                            <dxe:ListEditItem Value="STM" Text="STM" />
                                                                                            <dxe:ListEditItem Value="TC" Text="TC" />
                                                                                            <dxe:ListEditItem Value="TJ" Text="TJ" />
                                                                                            <dxe:ListEditItem Value="TRE" Text="TRE" />
                                                                                            <dxe:ListEditItem Value="TRF" Text="TRF" />
                                                                                            <dxe:ListEditItem Value="TRIBALC" Text="TRIBALC" />
                                                                                            <dxe:ListEditItem Value="TRT" Text="TRT" />
                                                                                            <dxe:ListEditItem Value="TST" Text="TST" />
                                                                                            <dxe:ListEditItem Value="SEFAZ" Text="SEFAZ" />
                                                                                            <dxe:ListEditItem Value="JCDE" Text="JCDE" />
                                                                                            <dxe:ListEditItem Value="JUCAP" Text="JUCAP" />
                                                                                            <dxe:ListEditItem Value="JUCEA" Text="JUCEA" />
                                                                                            <dxe:ListEditItem Value="JUCEAC" Text="JUCEAC" />
                                                                                            <dxe:ListEditItem Value="JUCEAL" Text="JUCEAL" />
                                                                                            <dxe:ListEditItem Value="JUCEB" Text="JUCEB" />
                                                                                            <dxe:ListEditItem Value="JUCEC" Text="JUCEC" />
                                                                                            <dxe:ListEditItem Value="JUCEES" Text="JUCEES" />
                                                                                            <dxe:ListEditItem Value="JUCEG" Text="JUCEG" />
                                                                                            <dxe:ListEditItem Value="JUCEMA" Text="JUCEMA" />
                                                                                            <dxe:ListEditItem Value="JUCEMAT" Text="JUCEMAT" />
                                                                                            <dxe:ListEditItem Value="JUCEMG" Text="JUCEMG" />
                                                                                            <dxe:ListEditItem Value="JUCEMS" Text="JUCEMS" />
                                                                                            <dxe:ListEditItem Value="JUCEP" Text="JUCEP" />
                                                                                            <dxe:ListEditItem Value="JUCEPA" Text="JUCEPA" />
                                                                                            <dxe:ListEditItem Value="JUCEPAR" Text="JUCEPAR" />
                                                                                            <dxe:ListEditItem Value="JUCEPE" Text="JUCEPE" />
                                                                                            <dxe:ListEditItem Value="JUCEPI" Text="JUCEPI" />
                                                                                            <dxe:ListEditItem Value="JUCER" Text="JUCER" />
                                                                                            <dxe:ListEditItem Value="JUCERGS" Text="JUCERGS" />
                                                                                            <dxe:ListEditItem Value="JUCERJA" Text="JUCERJA" />
                                                                                            <dxe:ListEditItem Value="JUCERN" Text="JUCERN" />
                                                                                            <dxe:ListEditItem Value="JUCERR" Text="JUCERR" />
                                                                                            <dxe:ListEditItem Value="JUCESC" Text="JUCESC" />
                                                                                            <dxe:ListEditItem Value="JUCESE" Text="JUCESE" />
                                                                                            <dxe:ListEditItem Value="JUCESP" Text="JUCESP" />
                                                                                            <dxe:ListEditItem Value="JUCETIN" Text="JUCETIN" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label33" runat="server" CssClass="labelNormal" Text="Dt. Expedição:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataExpedicao2" runat="server" ClientInstanceName="textDataExpedicao2" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label34" runat="server" CssClass="labelNormal" Text="UF Emissor:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropUFEmissor2" runat="server" ClientInstanceName="dropUFEmissor2"
                                                                                        ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_6">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="AC" Text="AC" />
                                                                                            <dxe:ListEditItem Value="AL" Text="AL" />
                                                                                            <dxe:ListEditItem Value="AP" Text="AP" />
                                                                                            <dxe:ListEditItem Value="AM" Text="AM" />
                                                                                            <dxe:ListEditItem Value="BA" Text="BA" />
                                                                                            <dxe:ListEditItem Value="CE" Text="CE" />
                                                                                            <dxe:ListEditItem Value="DF" Text="DF" />
                                                                                            <dxe:ListEditItem Value="ES" Text="ES" />
                                                                                            <dxe:ListEditItem Value="GO" Text="GO" />
                                                                                            <dxe:ListEditItem Value="MA" Text="MA" />
                                                                                            <dxe:ListEditItem Value="MT" Text="MT" />
                                                                                            <dxe:ListEditItem Value="MS" Text="MS" />
                                                                                            <dxe:ListEditItem Value="MG" Text="MG" />
                                                                                            <dxe:ListEditItem Value="PA" Text="PA" />
                                                                                            <dxe:ListEditItem Value="PB" Text="PB" />
                                                                                            <dxe:ListEditItem Value="PR" Text="PR" />
                                                                                            <dxe:ListEditItem Value="PE" Text="PE" />
                                                                                            <dxe:ListEditItem Value="PI" Text="PI" />
                                                                                            <dxe:ListEditItem Value="RJ" Text="RJ" />
                                                                                            <dxe:ListEditItem Value="RN" Text="RN" />
                                                                                            <dxe:ListEditItem Value="RS" Text="RS" />
                                                                                            <dxe:ListEditItem Value="RO" Text="RO" />
                                                                                            <dxe:ListEditItem Value="RR" Text="RR" />
                                                                                            <dxe:ListEditItem Value="SC" Text="SC" />
                                                                                            <dxe:ListEditItem Value="SP" Text="SP" />
                                                                                            <dxe:ListEditItem Value="SE" Text="SE" />
                                                                                            <dxe:ListEditItem Value="TO" Text="TO" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label48" runat="server" CssClass="labelNormal" Text="Dt. Vencimento:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataVencimento2" runat="server" ClientInstanceName="textDataVencimento2" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                            <dxtc:TabPage Text="FATCA">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                                            <ContentTemplate>
                                                                                <table>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="label37" runat="server" CssClass="labelNormal" Text="Natureza Jurídica:"> </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropNaturezaJuridica" runat="server" ClientInstanceName="dropNaturezaJuridica"
                                                                                                ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" Text='<%# Eval("NaturezaJuridica") %>'>
                                                                                                <Items>
                                                                                                    <dxe:ListEditItem Value="1" Text="Crédito" />
                                                                                                    <dxe:ListEditItem Value="2" Text="Débito" />
                                                                                                    <dxe:ListEditItem Value="101" Text="Poder Executivo Federal" />
                                                                                                    <dxe:ListEditItem Value="102" Text="Poder Executivo Estadual ou do Distrito Federal" />
                                                                                                    <dxe:ListEditItem Value="103" Text="Poder Executivo Municipal" />
                                                                                                    <dxe:ListEditItem Value="104" Text="Poder Legislativo Federal" />
                                                                                                    <dxe:ListEditItem Value="105" Text="Poder Legislativo Estadual ou do Distrito Federal" />
                                                                                                    <dxe:ListEditItem Value="106" Text="Poder Legislativo Municipal" />
                                                                                                    <dxe:ListEditItem Value="107" Text="Poder Judiciário Federal" />
                                                                                                    <dxe:ListEditItem Value="108" Text="Poder Judiciário Estadual ou do Distrito Federal" />
                                                                                                    <dxe:ListEditItem Value="109" Text="Órgão Autonomo de Direito Público" />
                                                                                                    <dxe:ListEditItem Value="110" Text="Autarquia Federal" />
                                                                                                    <dxe:ListEditItem Value="111" Text="Autarquia Estadual ou do Distrito Federal" />
                                                                                                    <dxe:ListEditItem Value="112" Text="Autarquia Municipal" />
                                                                                                    <dxe:ListEditItem Value="113" Text="Fundação Federal" />
                                                                                                    <dxe:ListEditItem Value="114" Text="Fundação Estadual ou do Distrito Federal" />
                                                                                                    <dxe:ListEditItem Value="115" Text="Fundação Municipal" />
                                                                                                    <dxe:ListEditItem Value="116" Text="Órgão Público Autonomo Federal" />
                                                                                                    <dxe:ListEditItem Value="117" Text="Órgão Público Autonomo Estadual ou do Distrito Federal" />
                                                                                                    <dxe:ListEditItem Value="118" Text="Órgão Público Autonomo Municipal" />
                                                                                                    <dxe:ListEditItem Value="201" Text="Empresa Pública" />
                                                                                                    <dxe:ListEditItem Value="202" Text="Sociedade Anônima Fechada - Empresa Pública" />
                                                                                                    <dxe:ListEditItem Value="203" Text="Sociedade de Economia Mista" />
                                                                                                    <dxe:ListEditItem Value="204" Text="Sociedade Anônima Aberta" />
                                                                                                    <dxe:ListEditItem Value="205" Text="Sociedade Anônima Fechada" />
                                                                                                    <dxe:ListEditItem Value="206" Text="Sociedade Empresaria Limitada" />
                                                                                                    <dxe:ListEditItem Value="207" Text="Sociedade Empresaria em Nome Coletivo" />
                                                                                                    <dxe:ListEditItem Value="208" Text="Sociedade Empresaria em Comandita Simples" />
                                                                                                    <dxe:ListEditItem Value="209" Text="Sociedade Empresaria em Comandita por Ações" />
                                                                                                    <dxe:ListEditItem Value="210" Text="Sociedade Mercantil de Capital e Indústria" />
                                                                                                    <dxe:ListEditItem Value="211" Text="Sociedade Civil Com Fins Lucrativos" />
                                                                                                    <dxe:ListEditItem Value="212" Text="Sociedade em Conta de Participação" />
                                                                                                    <dxe:ListEditItem Value="213" Text="Empresário (Individual)" />
                                                                                                    <dxe:ListEditItem Value="214" Text="Cooperativa" />
                                                                                                    <dxe:ListEditItem Value="215" Text="Consórcio de Sociedades" />
                                                                                                    <dxe:ListEditItem Value="216" Text="Grupo de Sociedades" />
                                                                                                    <dxe:ListEditItem Value="217" Text="Estabelecimento, no Brasil, de Sociedade Estrangeira" />
                                                                                                    <dxe:ListEditItem Value="218" Text="Sociedade Anônima em Garantia Solidária" />
                                                                                                    <dxe:ListEditItem Value="219" Text="Estabelecimento Empr Binacional Argentino-Brasileira" />
                                                                                                    <dxe:ListEditItem Value="220" Text="Entidade Binacional Itaipu" />
                                                                                                    <dxe:ListEditItem Value="221" Text="Empresa Domiciliada no Exterior" />
                                                                                                    <dxe:ListEditItem Value="222" Text="Clube/Fundo de Investimento" />
                                                                                                    <dxe:ListEditItem Value="223" Text="Sociedade Simples Pura" />
                                                                                                    <dxe:ListEditItem Value="224" Text="Sociedade Simples Limitada" />
                                                                                                    <dxe:ListEditItem Value="225" Text="Sociedade Simples em Nome Coletivo" />
                                                                                                    <dxe:ListEditItem Value="226" Text="Sociedade Simples em Comandita Simples" />
                                                                                                    <dxe:ListEditItem Value="227" Text="Empresa Binacional" />
                                                                                                    <dxe:ListEditItem Value="301" Text="Fundação Mantida com Recursos Privados" />
                                                                                                    <dxe:ListEditItem Value="302" Text="Associação" />
                                                                                                    <dxe:ListEditItem Value="303" Text="Serviço Notarial e Registral (Cartório)" />
                                                                                                    <dxe:ListEditItem Value="304" Text="Organização Social" />
                                                                                                    <dxe:ListEditItem Value="305" Text="Organiz da Sociedade Civil Interesse Público (OSCIP)" />
                                                                                                    <dxe:ListEditItem Value="306" Text="Outras Formas Fundações Mantidas Com Recursos Privado" />
                                                                                                    <dxe:ListEditItem Value="307" Text="Serviço Social Autônomo" />
                                                                                                    <dxe:ListEditItem Value="308" Text="Condomínio Edifício" />
                                                                                                    <dxe:ListEditItem Value="309" Text="Unidade Executora (Programa Dinheiro Direto na Escola" />
                                                                                                    <dxe:ListEditItem Value="310" Text="Comissão de Conciliação Prévia" />
                                                                                                    <dxe:ListEditItem Value="311" Text="Entidade de Mediação e Arbitragem" />
                                                                                                    <dxe:ListEditItem Value="312" Text="Partido Político" />
                                                                                                    <dxe:ListEditItem Value="313" Text="Entidade Sindical" />
                                                                                                    <dxe:ListEditItem Value="320" Text="Estab, no Brasil, de Fundação ou Assoc. Estrangeiras" />
                                                                                                    <dxe:ListEditItem Value="321" Text="Fundação ou Associação Domiciliada no Exterior" />
                                                                                                    <dxe:ListEditItem Value="399" Text="Associação Privada" />
                                                                                                    <dxe:ListEditItem Value="401" Text="Empresa Individual Imobiliária" />
                                                                                                    <dxe:ListEditItem Value="408" Text="Contribuinte Individual" />
                                                                                                    <dxe:ListEditItem Value="409" Text="Candidato a Cargo Político Eletivo" />
                                                                                                    <dxe:ListEditItem Value="450" Text="Organismos Internac. e Outras Inst. Extraterritoriais" />
                                                                                                    <dxe:ListEditItem Value="500" Text="Organização Internac. e Outras Inst. Extraterritoriais" />
                                                                                                    <dxe:ListEditItem Value="119" Text="Comissão Polinacional" />
                                                                                                    <dxe:ListEditItem Value="120" Text="Fundo Público" />
                                                                                                    <dxe:ListEditItem Value="121" Text="Associação Pública" />
                                                                                                    <dxe:ListEditItem Value="228" Text="Consórcio de Empregadores" />
                                                                                                    <dxe:ListEditItem Value="229" Text="Consórcio Simples" />
                                                                                                    <dxe:ListEditItem Value="230" Text="Empresa Individual de Responsabilidade Limitada (de Natureza Empresária)" />
                                                                                                    <dxe:ListEditItem Value="231" Text="Empresa Individual de Responsabilidade Limitada (de Natureza Simples)" />
                                                                                                    <dxe:ListEditItem Value="322" Text="Organização Religiosa" />
                                                                                                    <dxe:ListEditItem Value="323" Text="Comunidade Indígena" />
                                                                                                    <dxe:ListEditItem Value="324" Text="Fundo Privado" />
                                                                                                    <dxe:ListEditItem Value="501" Text="Organização Internacional" />
                                                                                                    <dxe:ListEditItem Value="502" Text="Representação Diplomática Estrangeira" />
                                                                                                    <dxe:ListEditItem Value="503" Text="Outras Instituições Extraterritoriais" />
                                                                                                </Items>
                                                                                            </dxe:ASPxComboBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="label38" runat="server" CssClass="labelNormal" Text="Tipo de Renda:"> </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropTipoRenda" runat="server" ClientInstanceName="dropTipoRenda"
                                                                                                ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto" Text='<%#Eval("TipoRenda")%>'>
                                                                                                <Items>
                                                                                                    <dxe:ListEditItem Value="A" Text="Ativa" />
                                                                                                    <dxe:ListEditItem Value="P" Text="Passiva" />
                                                                                                </Items>
                                                                                            </dxe:ASPxComboBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="label39" runat="server" CssClass="labelNormal" Text="Tem FATCA:"> </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropIndicadorFatca" runat="server" ClientInstanceName="dropIndicadorFatca"
                                                                                                ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto" Text='<%#Eval("IndicadorFatca")%>'>
                                                                                                <Items>
                                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                </Items>
                                                                                            </dxe:ASPxComboBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="label36" runat="server" CssClass="labelNormal" Text="GIIN / TIN:"> </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:TextBox ID="textGiinTin" runat="server" CssClass="textNormal" MaxLength="30"
                                                                                                Text='<%#Eval("GiinTin")%>' />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="label40" runat="server" CssClass="labelNormal" Text="Justificativa Ausência GIIN:"> </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropJustificativaGiin" runat="server" ClientInstanceName="dropJustificativaGiin"
                                                                                                ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" Text='<%#Eval("JustificativaGiin")%>'>
                                                                                                <Items>
                                                                                                    <dxe:ListEditItem Value="01" Text="Estamos em processo de obtenção do GIIN" />
                                                                                                    <dxe:ListEditItem Value="02" Text="Não possuimos nem estamos em processo de obtenção" />
                                                                                                </Items>
                                                                                            </dxe:ASPxComboBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="label41" runat="server" CssClass="labelNormal" Text="CNAE Divisão:"> </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropCnaeDivisao" runat="server" ClientInstanceName="dropCnaeDivisao"
                                                                                                DataSourceID="EsDSDivisaoCnae" ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo"
                                                                                                TextField="NomeDivisao" ValueField="CdDivisaoCnae" Text='<%#Session["cnaeDivisao"]%>'>
                                                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { divisao=s.GetValue();dropCnaeGrupo.PerformCallback(divisao); }" />
                                                                                            </dxe:ASPxComboBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="label42" runat="server" CssClass="labelNormal" Text="CNAE Grupo:"> </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropCnaeGrupo" runat="server" ClientInstanceName="dropCnaeGrupo"
                                                                                                DataSourceID="EsDSGrupoCnae" ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo"
                                                                                                Text='<%#Session["cnaeGrupo"]%>' TextField="NomeGrupo" ValueField="CdGrupo" OnCallback="dropCnaeGrupo_Callback">
                                                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { grupo=s.GetValue();dropCnaeClasse.PerformCallback(grupo); }"
                                                                                                    EndCallback="OnEndCallbackGrupo" />
                                                                                            </dxe:ASPxComboBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="label43" runat="server" CssClass="labelNormal" Text="CNAE Classe:"> </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropCnaeClasse" runat="server" ClientInstanceName="dropCnaeClasse"
                                                                                                DataSourceID="EsDSClasseCnae" ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo"
                                                                                                Text='<%#Session["cnaeClasse"]%>' TextField="NomeClasse" ValueField="CdClasse"
                                                                                                OnCallback="dropCnaeClasse_Callback">
                                                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { classe=s.GetValue();dropCnaeSubClasse.PerformCallback(classe); }"
                                                                                                    EndCallback="OnEndCallbackClasse" />
                                                                                            </dxe:ASPxComboBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="td_Label">
                                                                                            <asp:Label ID="label44" runat="server" CssClass="labelNormal" Text="CNAE SubClasse:"> </asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropCnaeSubClasse" runat="server" ClientInstanceName="dropCnaeSubClasse"
                                                                                                DataSourceID="EsDSSubClasseCnae" ShowShadow="false" DropDownStyle="DropDown"
                                                                                                CssClass="dropDownListLongo" TextField="NomeSubClasse" ValueField="CdSubClasse"
                                                                                                OnCallback="dropCnaeSubClasse_Callback" Text='<%#Session["cnaeSubClasse"]%>'>
                                                                                                <ClientSideEvents EndCallback="OnEndCallbackSubClasse" />
                                                                                            </dxe:ASPxComboBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                            <dxtc:TabPage Text="Suitability">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelPerfil" runat="server" CssClass="labelNormal" Text="Perfil:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropPerfil" runat="server" ClientInstanceName="dropPerfil"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_1"
                                                                                        Text='<%#Eval("Perfil")%>' DataSourceID="EsDSSuitabilityPerfilInvestidor" ValueField="IdPerfilInvestidor"
                                                                                        TextField="Perfil">
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelNaoInformadoRecusa" runat="server" CssClass="labelNormal" Text="Não Informado (Recusa):"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropNaoInformadoRecusa" runat="server" ClientInstanceName="dropNaoInformadoRecusa"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("Recusa")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label46" runat="server" CssClass="labelNormal" Text="Tipo de Validação:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropValidacao" runat="server" ClientInstanceName="dropValidacao"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_1"
                                                                                        Text='<%#Eval("IdValidacao")%>' DataSourceID="EsDSValidacao" ValueField="IdValidacao"
                                                                                        TextField="Descricao">
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelUltimaAtualizacao" runat="server" CssClass="labelNormal" Text="Ult.Alt.:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="ASPxDateEdit1" runat="server" ClientInstanceName="textDataFim"
                                                                                        Value='<%#Eval("UltimaAlteracao")%>' Enabled="false" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelDispensado" runat="server" CssClass="labelNormal" Text="Dispensado:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropDispensado" runat="server" ClientInstanceName="dropDispensado"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("Dispensado")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                        <ClientSideEvents Init="HabilitaDispensa" SelectedIndexChanged="HabilitaDispensa" />
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelTipoDispensa" runat="server" CssClass="labelNormal" Text="Tipo de Dispensa:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoDispensa" runat="server" ClientInstanceName="dropTipoDispensa"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_1"
                                                                                        Text='<%#Eval("Dispensa")%>' DataSourceID="EsDSTipoDispensa" ValueField="IdDispensa"
                                                                                        TextField="Descricao">
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                        </TabPages>
                                                    </dxtc:ASPxPageControl>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal3" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal6" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal7" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="420px" />
                                        <ClientSideEvents BeginCallback="function(s, e) {
						if (e.command == 'CUSTOMCALLBACK') {                            
                            isCustomCallback = true;                            
                        }
                        else if (e.command == 'UPDATEEDIT')
                        {
                            isUpdateEdit = true;      
                        } 
                        						
                    }" EndCallback="function(s, e) {
						if (isCustomCallback) 
						{
                            isCustomCallback = false;
                            s.Refresh();
                        }
                        else if (isUpdateEdit)
                        {
                            isUpdateEdit = false;
                            if(gridCadastro.cpMessage != '' && gridCadastro.cpMessage != null)
                            {
                                alert(gridCadastro.cpMessage);
                                gridCadastro.cpMessage = '';
                            }
                        }
                    }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSDivisaoCnae" runat="server" OnesSelect="EsDSDivisaoCnae_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoCnae" runat="server" OnesSelect="EsDSGrupoCnae_esSelect" />
        <cc1:esDataSource ID="EsDSClasseCnae" runat="server" OnesSelect="EsDSClasseCnae_esSelect" />
        <cc1:esDataSource ID="EsDSSubClasseCnae" runat="server" OnesSelect="EsDSSubClasseCnae_esSelect" />
        <cc1:esDataSource ID="EsDSPessoa" runat="server" OnesSelect="EsDSPessoa_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSPessoaEndereco" runat="server" OnesSelect="EsDSPessoaEndereco_esSelect" />
        <cc1:esDataSource ID="EsDSTemplateCarteira" runat="server" OnesSelect="EsDSTemplateCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSTemplateCotista" runat="server" OnesSelect="EsDSTemplateCotista_esSelect" />
        <cc1:esDataSource ID="EsDSPerfilInvestidor" runat="server" OnesSelect="EsDSPerfilInvestidor_esSelect" />
        <cc1:esDataSource ID="EsDSTipoDispensa" runat="server" OnesSelect="EsDSTipoDispensa_esSelect" />
        <cc1:esDataSource ID="EsDSSuitabilityPerfilInvestidor" runat="server" OnesSelect="EsDSSuitabilityPerfilInvestidor_esSelect" />
        <cc1:esDataSource ID="EsDSValidacao" runat="server" OnesSelect="EsDSValidacao_esSelect" />
        <cc1:esDataSource ID="EsDSSuitabilityHistorico" runat="server" OnesSelect="EsDSSuitabilityHistorico_esSelect" />
    </form>
</body>
</html>
