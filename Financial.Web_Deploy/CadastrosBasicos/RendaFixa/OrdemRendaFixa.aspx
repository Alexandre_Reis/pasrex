﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_OrdemRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;        
    var operacao = '';
    
    function OnGetDataCliente(data) {          
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }
    function OnGetDataTituloRF(data) {
        document.getElementById(gridCadastro.cpHiddenIdTitulo).value = data;
        ASPxCallback2.SendCallback(data);
        popupTituloRF.HideWindow();
        btnEditTituloRF.Focus();
    }
    function OnGetDataCarteira(data) {          
        btnCarteiraContraparte.SetValue(data);        
        CallbackCarteira.SendCallback(btnCarteiraContraparte.GetValue());
        
        popupCarteira.HideWindow();
        btnCarteiraContraparte.Focus();
    }
    
    function OnGetDataAgenteMercado(data) {          
        btnAgenteContraParte.SetValue(data);        
        CallbackAgenteMercado.SendCallback(btnAgenteContraParte.GetValue());
        
        popupAgenteMercado.HideWindow();
        btnAgenteContraParte.Focus();
    }
    
    function TrataCompromisso(enable)
    {   
        textDataVolta.SetEnabled(enable);
        textTaxaVolta.SetEnabled(enable);
        textPUVolta.SetEnabled(enable);
        textValorVolta.SetEnabled(enable);        
        
        if (!enable)
        {
            textDataVolta.SetText('');
            textTaxaVolta.SetText('');
            textPUVolta.SetText('');
            textValorVolta.SetText('');
        }
    }
    
    function CallbackCarteiraComplete(s,e)
    {
        var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
        textNomeCarteira.value = e.result;    
    }
    
    function CallbackAgenteMercadoComplete(s,e)
    {
        var textNomeAgente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeAgente');
        textNomeAgente.value = e.result;        
    }
    
    function dropMercadoClienteSelectedIndexChanged(s,e)
    {
        var trAgenteContraParte = document.getElementById('trAgenteContraParte');
        var trCarteiraContraParte = document.getElementById('trCarteiraContraParte');
        
        var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
        var textNomeAgente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeAgente');
        
                
        if(s.GetValue() === "1")
        {
            trCarteiraContraParte.style.display = "none";
            trAgenteContraParte.style.display = "table-row";
        }
        else if(s.GetValue() ==="2")
        {
            trAgenteContraParte.style.display = "none";            
            trCarteiraContraParte.style.display = "table-row";                        
        }        
        
    }
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else if (operacao == 'aprovar')
                {
                    gridCadastro.PerformCallback('btnAprova');
                }
                else if (operacao == 'cancelar')
                {
                    gridCadastro.PerformCallback('btnCancela');
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            textQuantidade.SetValue(null);
            textPUOperacao.SetValue(null);
            textValor.SetValue(null);
            alert('Operação feita com sucesso.');
            textQuantidade.Focus();
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            }
            else
            {
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);            
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                                    if (e.result != null) btnEditTituloRF.SetValue(e.result);                                    
                                    } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback3" runat="server" OnCallback="ASPxCallback3_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                                if (resultSplit != '')
                                {
                                    var resultSplit = e.result.split('|');
                                    textPUVolta.SetValue(resultSplit[0]);
                                    textValorVolta.SetValue(resultSplit[1]);
                                }    
                                    } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackTaxa" runat="server" OnCallback="callbackTaxa_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '')
            {
                textTaxaOperacao.SetValue(e.result);
            }
                
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="CallbackCarteira" runat="server" OnCallback="CallbackCarteira_Callback">
            <ClientSideEvents CallbackComplete="CallbackCarteiraComplete" />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="CallbackAgenteMercado" runat="server" OnCallback="CallbackAgenteMercado_Callback">
            <ClientSideEvents CallbackComplete="CallbackAgenteMercadoComplete" />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Ordens de Renda Fixa"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2" width="450">
                                                        <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal11" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback(operacao);} return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal14" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnAprova" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnTick" OnClientClick="if (confirm('Tem certeza que quer aprovar as ordens?')==true) {operacao='aprovar'; callbackErro.SendCallback(operacao);} return false;">
                                        <asp:Literal ID="Literal5" runat="server" Text="Aprovar Ordens" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCancela" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnCross" OnClientClick="if (confirm('Tem certeza que quer cancelar as ordens?')==true) {operacao='cancelar'; callbackErro.SendCallback(operacao);} return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Cancelar Ordens" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal8" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao"
                                        DataSourceID="EsDSOrdemRendaFixa" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnPreRender="gridCadastro_PreRender"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnInitNewRow="gridCadastro_InitNewRow"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData" OnCustomColumnDisplayText="gridCadastro_CustomColumnDisplayText">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Id" VisibleIndex="1" Width="5%"
                                                CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="2" Width="5%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="15%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" VisibleIndex="4"
                                                Width="10%" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo" VisibleIndex="5"
                                                Width="8%" ExportWidth="130">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Compra Final'>Compra Final</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Venda Final'>Venda Final</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Compra c/ Rev.'>Compra c/ Rev.</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Venda c/ Rec.'>Venda c/ Rec.</div>" />
                                                        <dxe:ListEditItem Value="6" Text="<div title='Venda Total'>Venda Total</div>" />
                                                        <dxe:ListEditItem Value="10" Text="<div title='Compra Casada'>Compra Casada</div>" />
                                                        <dxe:ListEditItem Value="11" Text="<div title='Venda Casada'>Venda Casada</div>" />
                                                        <dxe:ListEditItem Value="12" Text="<div title='Antecipação Revenda'>Antecip. Rev.</div>" />
                                                        <dxe:ListEditItem Value="13" Text="<div title='Antecipação Recompra'>Antecip. Rec.</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Observacao" UnboundType="String" Caption="Informações Gestor"
                                                CellStyle-Wrap="True" VisibleIndex="6" Width="15%" Settings-AutoFilterCondition="Contains">
                                                <PropertiesTextEdit EncodeHtml="false">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="7" Width="10%"
                                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="PUOperacao" Caption="PU" VisibleIndex="8"
                                                Width="10%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="9" Width="10%" HeaderStyle-HorizontalAlign="Right"
                                                FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataColumn FieldName="StatusDescricao" Caption="Status" UnboundType="String"
                                                VisibleIndex="18" Width="7%" HeaderStyle-HorizontalAlign="Center" FooterCellStyle-HorizontalAlign="Center"
                                                CellStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdTitulo" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataLiquidacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="TaxaOperacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="TipoNegociacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataVolta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="TaxaVolta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="PUVolta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ValorVolta" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Status" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdLiquidacao" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCustodia" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="TipoContraparte" UnboundType="String" Visible="false"/>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>' MaxLength="10"
                                                                        NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelMercadoCliente" runat="server" CssClass="labelRequired" Text="Mercado/Cliente:" OnLoad="labelMercadoCliente_OnInit"></asp:Label>
                                                                </td>
                                                                <td>
                                                                <dxe:ASPxComboBox ID="dropMercadoCliente" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoContraparte")%>' OnInit="dropMercadoCliente_OnInit" >
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Mercado" />
                                                                            <dxe:ListEditItem Value="2" Text="Cliente" />
                                                                        </Items>
                                                                        <ClientSideEvents SelectedIndexChanged="dropMercadoClienteSelectedIndexChanged" Init="dropMercadoClienteSelectedIndexChanged"/>
                                                                        </dxe:ASPxComboBox>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr id="trAgenteContraParte" style="display:none">
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelIdAgenteContraParte" runat="server" CssClass="labelRequired" OnLoad="labelMercadoCliente_OnInit"
                                                                        Text="Contraparte:" ></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnAgenteContraParte" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnAgenteContraParte" Text='<%#Eval("IdAgenteContraParte")%>' OnLoad="btnAgenteContraParte_OnInit"
                                                                        MaxLength="10" NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeAgente').value = '';} "
                                                                            ButtonClick="function(s, e) {popupAgenteMercado.ShowAtElementByID(s.name);}"
                                                                            LostFocus="function(s, e) {OnLostFocus(popupMensagemAgenteMercado,CallbackAgenteMercado, btnAgenteContraParte);}" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNomeAgente" runat="server" CssClass="textNomeAgente" Enabled="false" Text='<%#Eval("AgenteMercadoContraparte")%>'></asp:TextBox>
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr id="trCarteiraContraParte" style="display:none">
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelIdCarteiraContraparte" runat="server" CssClass="labelRequired" OnLoad="labelMercadoCliente_OnInit"
                                                                        Text="Contraparte:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnCarteiraContraparte" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnCarteiraContraparte" Text='<%#Eval("IdCarteiraContraparte")%>' OnLoad="btnAgenteContraParte_OnInit"
                                                                        MaxLength="10" NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira,CallbackCarteira, btnCarteiraContraparte);}" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="2">
                                                                    <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNomeCarteira" Enabled="false"
                                                                    Text='<%#Eval("CarteiraContraparte")%>'></asp:TextBox>
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <asp:Label ID="labelMensagem" runat="server" Text="  (Clique no botão...)" CssClass="labelNormal"
                                                                    Visible="false"></asp:Label>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTitulo" runat="server" CssClass="labelRequired" Text="Título:" />
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="hiddenIdTitulo" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTitulo")%>' />
                                                                    <dxe:ASPxButtonEdit ID="btnEditTituloRF" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditTituloRF" ReadOnly="true" Width="380px" Text='<%#Eval("DescricaoCompleta")%>'>
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents ButtonClick="function(s, e) {popupTituloRF.ShowAtElementByID(s.name);}" />
                                                                    </dxe:ASPxButtonEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoOperacao")%>' >
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Compra Final" />
                                                                            <dxe:ListEditItem Value="2" Text="Venda Final" />
                                                                            <dxe:ListEditItem Value="3" Text="Compra c/ Rev." />
                                                                            <dxe:ListEditItem Value="4" Text="Venda c/ Rec." />
                                                                            <dxe:ListEditItem Value="6" Text="Venda Total" />
                                                                            <dxe:ListEditItem Value="10" Text="Compra Casada" />
                                                                            <dxe:ListEditItem Value="11" Text="Venda Casada" />
                                                                            <dxe:ListEditItem Value="12" Text="Antecip. Rev." />
                                                                            <dxe:ListEditItem Value="13" Text="Antecip. Rec." />
                                                                        </Items>
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { if (s.GetSelectedIndex() == 2 || s.GetSelectedIndex() == 3)
                                                                                {TrataCompromisso(true);}
                                                                                else
                                                                                {TrataCompromisso(false);}                                                                                    
                                                                                
                                                                                if (s.GetSelectedIndex() == 2)
                                                                                { dropFormaLiquidacao.SetSelectedIndex(1); } 
                                                                                
                                                                                }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="labelTaxaOperacao" runat="server" CssClass="labelRequired" Text="Tx Operação:"></asp:Label>
                                                                </td>
                                                                <td style="width: 200px;">
                                                                    <dxe:ASPxSpinEdit ID="textTaxaOperacao" runat="server" CssClass="textValor_5" Style="float: left;"
                                                                        ClientInstanceName="textTaxaOperacao" MaxLength="25" NumberType="Float" DecimalPlaces="16"
                                                                        Text='<%#Eval("TaxaOperacao")%>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                    <div class="linkButton linkButtonNoBorder" style="display: inline">
                                                                        <asp:LinkButton ID="btnCalculaTaxa" runat="server" CssClass="btnCalc" OnClientClick="callbackTaxa.SendCallback(); return false;">
                                                                            <asp:Literal ID="Literal2" runat="server" Text="Calcula" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoLiquidacao" runat="server" CssClass="labelRequired" Text="Liquida:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropFormaLiquidacao" runat="server" ClientInstanceName="dropFormaLiquidacao"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_5"
                                                                        Text='<%# Eval("IdLiquidacao") %>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="0" Text="-" />
                                                                            <dxe:ListEditItem Value="1" Text="Selic" />
                                                                            <dxe:ListEditItem Value="2" Text="Cetip" />
                                                                            <dxe:ListEditItem Value="3" Text="CBLC" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="labelLocalCustodia" runat="server" CssClass="labelNormal" Text="Custódia:"
                                                                        Visible="false"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropLocalCustodia" runat="server" ShowShadow="false" Visible="false"
                                                                        DropDownStyle="DropDownList" CssClass="dropDownListCurto_2" Text='<%#Eval("IdCustodia")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="0" Text="-" />
                                                                            <dxe:ListEditItem Value="1" Text="Selic" />
                                                                            <dxe:ListEditItem Value="2" Text="Cetip" />
                                                                            <dxe:ListEditItem Value="3" Text="CBLC" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                <tr>                                
                                    <td class="td_Label">
                                        <asp:Label ID="labelTrader" runat="server" CssClass="labelNormal" Text="Trader:"></asp:Label>                        
                                    </td>
                                    <td colspan="2">
                                        <dxe:ASPxComboBox ID="dropTrader" runat="server" ClientInstanceName="dropTrader"
                                                            DataSourceID="EsDSTrader" IncrementalFilteringMode="Contains"  
                                                            ShowShadow="false" DropDownStyle="DropDown" 
                                                            CssClass="dropDownList" TextField="Nome" ValueField="IdTrader"
                                                            Text='<%#Eval("IdTrader")%>'>
                                          <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); } " />
                                        </dxe:ASPxComboBox>      
                                    </td>
                                </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoNegociacao" runat="server" CssClass="labelNormal" Text="Categoria:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoNegociacao" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoNegociacao")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Negociação" />
                                                                            <dxe:ListEditItem Value="2" Text="Disp. Venda" />
                                                                            <dxe:ListEditItem Value="3" Text="Vencimento" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="labelDataVolta" runat="server" CssClass="labelNormal" Text="Dt Volta:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataVolta" runat="server" ClientInstanceName="textDataVolta"
                                                                        Value='<%#Eval("DataVolta")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelQuantidade" runat="server" CssClass="labelNormal" Text="Qtde:"></asp:Label>
                                                                </td>
                                                                <td>
                                        <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor_5" ClientInstanceName="textQuantidade" DisplayFormatString="N"
                                                                        MaxLength="16" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Quantidade")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) {CalculoTriplo(textPUOperacao, textQuantidade, textValor, 1);
                                                                         }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="labelTaxaVolta" runat="server" CssClass="labelNormal" Text="Tx Volta:"></asp:Label>
                                                                </td>
                                                                <td>
                                        <dxe:ASPxSpinEdit ID="textTaxaVolta" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxaVolta" DisplayFormatString="N"
                                                                        MaxLength="8" NumberType="Float" DecimalPlaces="4" Text='<%#Eval("TaxaVolta")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) {ASPxCallback3.SendCallback(); }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelPUOperacao" runat="server" CssClass="labelNormal" Text="PU:"></asp:Label>
                                                                </td>
                                                                <td>
                                        <dxe:ASPxSpinEdit ID="textPUOperacao" runat="server" CssClass="textValor_5" ClientInstanceName="textPUOperacao" DisplayFormatString="N"
                                                                        MaxLength="25" NumberType="Float" DecimalPlaces="12" Text='<%#Eval("PUOperacao")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) {CalculoTriplo(textPUOperacao, textQuantidade, textValor, 1);
                                                                         }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="labelPUVolta" runat="server" CssClass="labelNormal" Text="PU Volta:"></asp:Label>
                                                                </td>
                                                                <td>
                                        <dxe:ASPxSpinEdit ID="textPUVolta" runat="server" CssClass="textValor_5" ClientInstanceName="textPUVolta" DisplayFormatString="N"
                                                                        MaxLength="25" NumberType="Float" DecimalPlaces="12" Text='<%#Eval("PUVolta")%>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor:"></asp:Label>
                                                                </td>
                                                                <td>
                                        <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor" DisplayFormatString="N"
                                                                        MaxLength="12" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Valor")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) {CalculoTriplo(textPUOperacao, textQuantidade, textValor, 1);
                                                                         }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Label ID="labelValorVolta" runat="server" CssClass="labelNormal" Text="Valor Volta:"></asp:Label>
                                                                </td>
                                                                <td>
                                        <dxe:ASPxSpinEdit ID="textValorVolta" runat="server" CssClass="textValor_5" ClientInstanceName="textValorVolta" DisplayFormatString="N"
                                                                        MaxLength="14" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorVolta")%>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="lblCategoriaMovimentacao" runat="server" CssClass="labelNormal" Text="Categoria Mov:"> </asp:Label>
                                                                </td>                
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropCategoriaMovimentacao" runat="server" ClientInstanceName="dropCategoriaMovimentacao"
                                                                                        DataSourceID="EsDSCategoriaMovimentacao" IncrementalFilteringMode="Contains"  
                                                                                        ShowShadow="false" DropDownStyle="DropDown"
                                                                                        CssClass="dropDownListLongo" TextField="CodigoCategoria" ValueField="IdCategoriaMovimentacao"
                                                                                        Text='<%#Eval("IdCategoriaMovimentacao")%>'>
                                                                    </dxe:ASPxComboBox>         
                                                                </td> 
                                                            </tr>                                                            
                                                            <tr>
                                                                <td>
                                                                    &nbsp</td>
                                                                <td colspan="2">
                                                                    <asp:Label ID="labelObservacao" runat="server" CssClass="labelNormal" Text="INFORMADO PELO GESTOR"> </asp:Label>
                                                                </td>
                                                                <td colspan="2">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    &nbsp</td>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" Rows="8" CssClass="textLongo5"
                                                                        Text='<%#Eval("Observacao")%>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal10" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="550px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSOrdemRendaFixa" runat="server" OnesSelect="EsDSOrdemRendaFixa_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRF" runat="server" OnesSelect="EsDSTituloRF_esSelect" />
    <cc1:esDataSource ID="EsDSTrader" runat="server" OnesSelect="EsDSTrader_esSelect" />        
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
        <cc1:esDataSource ID="EsDSCategoriaMovimentacao" runat="server" OnesSelect="EsDSCategoriaMovimentacao_esSelect"/>    
    </form>
</body>
</html>
