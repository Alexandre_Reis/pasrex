﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_BloqueioRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html"; charset="utf-8">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    var selectedIndex; 
           
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }   
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }    
    function FechaPopupPosicaoRendaFixa()
    {
        textQuantidade.SetEnabled(false); 
        textQuantidade.SetText(''); 
        popupPosicaoRendaFixa.HideWindow();    
        gridCadastro.CancelEdit();
        return false;     
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }                
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="callbackErroPosicao" runat="server" OnCallback="callbackErroPosicao_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridPosicaoRendaFixa.PerformCallback(selectedIndex);                
            }
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                  
                var resultSplit = e.result.split('|');                        
                e.result = resultSplit[0];            
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            } 
            else
            {                                        
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');                
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente, textData);                                
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxpc:ASPxPopupControl ID="popupPosicaoRendaFixa" runat="server" Width="700px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True"
                        ShowCloseButton="false" >
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridPosicaoRendaFixa" runat="server" Width="100%"
                    ClientInstanceName="gridPosicaoRendaFixa"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSPosicaoRendaFixa" KeyFieldName="IdPosicao"
                    OnHtmlRowCreated="gridPosicaoRendaFixa_HtmlRowCreated"       
                    OnCustomCallback="gridPosicaoRendaFixa_CustomCallback">               
            <Columns>
                <dxwgv:GridViewDataSpinEditColumn FieldName="IdPosicao" Visible="false">
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="IdTitulo" Visible="false">
                </dxwgv:GridViewDataSpinEditColumn>                
                <dxwgv:GridViewDataSpinEditColumn FieldName="QuantidadeBloqueada" Caption="Qtde Bloqueada" VisibleIndex="3" Width="13%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="Descricao" ReadOnly="true" Caption="Título" VisibleIndex="4" Width="15%">
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" ReadOnly="true" Caption="Operação" VisibleIndex="6" Width="12%">
                </dxwgv:GridViewDataDateColumn>
                <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" ReadOnly="true" Caption="Vencimento" VisibleIndex="7" Width="12%">
                </dxwgv:GridViewDataDateColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" ReadOnly="true" VisibleIndex="8" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="PUMercado" ReadOnly="true" VisibleIndex="10" Width="20%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorMercado" ReadOnly="true" VisibleIndex="12" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                
            </Columns>            
            
            <SettingsBehavior ColumnResizeMode="Disabled" AllowFocusedRow="true" />
            <SettingsPager PageSize="1000"></SettingsPager>
            <Settings ShowTitlePanel="True"  ShowFilterRow="True" VerticalScrollBarMode="Visible" VerticalScrollableHeight="280" />
            
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posições de Renda Fixa" />
            
            <ClientSideEvents RowDblClick="function(s, e) {textQuantidade.SetEnabled(true); textQuantidade.SetText('');
                                                           textQuantidade.Focus(); selectedIndex = e.visibleIndex; }" />            
            </dxwgv:ASPxGridView>                              
                    
            <div>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="labelQuantidade" runat="server" CssClass="labelNormal" Text="Quantidade:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor" ClientInstanceName="textQuantidade"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="12" MinValue="0" MaxValue="9999999" NumberType="Integer" DecimalPlaces="0" >
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                        <td >
                            <div class="linkButton linkButtonNoBorder" style="text-align:right;">
                                
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" OnClientClick="callbackErroPosicao.SendCallback(selectedIndex); return false;">
                                <asp:Literal ID="Literal9" runat="server" Text="Salvar"/><div></div></asp:LinkButton>
                                                        
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="FechaPopupPosicaoRendaFixa(); return false;">
                                <asp:Literal ID="Literal11" runat="server" Text="Fechar"/><div></div></asp:LinkButton>
                            </div>        
                        </td>
                    </tr>
                </table>
                
                
            </div>                
            
        </div>    
        </dxpc:PopupControlContentControl></ContentCollection>     
        <ClientSideEvents PopUp="function(s, e) {gridPosicaoRendaFixa.PerformCallback(); 
                                                 textQuantidade.SetEnabled(false);}"
                          CloseUp="function(s, e) {FechaPopupPosicaoRendaFixa(); return false;}" />   
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Bloqueio/Desbloqueio de Posições de Renda Fixa"></asp:Label>
    </div>
        
    <div id="mainContent">
    
            
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoClienteFiltro" EnableClientSideAPI="true"
                                            CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td  colspan="2" width="450">
                                <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                            </td>
                        </tr>        
                        
                        <tr>
                            <td>                
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                            </td>    
                                                
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                            </td>  
                            
                            <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                            </td>                                                                    
                        </tr>                                                                       
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
    
            <div class="linkButton">
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) operacao='deletar'; callbackErro.SendCallback(); return false;" ><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()" ><asp:Literal ID="Literal2" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click" ><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click" ><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="IdBloqueio" DataSourceID="EsDSBloqueioRendaFixa"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnPreRender="gridCadastro_PreRender"
                        OnCustomJSProperties="gridCadastro_CustomJSProperties"                        
                        OnCancelRowEditing="gridCadastro_CancelRowEditing"
                        OnBeforeGetCallbackResult="gridCadastro_PreRender" >
                    
                    <Columns>
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                       <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="2" Width="10%" CellStyle-HorizontalAlign="left"/>                                            
                        <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="35%"/>                                            
                        <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" VisibleIndex="4" Width="10%"/>
                        
                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo" VisibleIndex="5" Width="15%">
                            <PropertiesComboBox EncodeHtml="false">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="<div title='Bloqueio'>Bloqueio</div>" />
                                    <dxe:ListEditItem Value="2" Text="<div title='Desbloqueio'>Desbloqueio</div>" />
                                </Items>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="9" Width="25%"                                 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}"></PropertiesTextEdit>        
                        </dxwgv:GridViewDataTextColumn>
                                                                        
                        <dxwgv:GridViewDataColumn FieldName="IdPosicao" Visible="false"/>
                    </Columns>
                    
                    <Templates>            
                    <EditForm>                          
                        <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                        
                        <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                        
                        <div class="editForm">                            
                            
                            <table border="0">
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                    </td>        
                                    
                                    <td>
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" 
                                                    ClientInstanceName="btnEditCodigoCliente" EnableClientSideAPI="true"
                                                    CssFilePath="../../css/forms.css" Text='<%#Eval("IdCliente")%>'                                                    
                                                    SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">            
                                        <Buttons>                                           
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>       
                                        <ClientSideEvents                                                           
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} " 
                                                 ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                                />                           
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                    
                                    <td>
                                        <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>                                        
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:" />
                                    </td>                                     
                                    <td>
                                        <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" ClientEnabled="false" Value='<%#Eval("DataOperacao")%>'/>                                    
                                    </td> 
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Motivo:" />
                                    </td>   
                                    <td colspan="3">
                                      <dxe:ASPxMemo ID="memoMotivo" runat="server" Height="70px" Width="400px" 
                                      ClientInstanceName="memoMotivo" HorizontalAlign="left" Text='<%#Eval("Motivo")%>'></dxe:ASPxMemo>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipo" runat="server" CssClass="labelRequired" Text="Tipo:" />
                                    </td>                                     
                                    <td>
                                        <dxe:ASPxComboBox ID="dropTipo" runat="server" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2" Text='<%#Eval("TipoOperacao")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Bloqueio" />
                                            <dxe:ListEditItem Value="2" Text="Desbloqueio" />
                                        </Items>                                                            
                                        </dxe:ASPxComboBox>           
                                    </td>
                                    
                                    <td class="linkButton" colspan="2">
                                        <asp:LinkButton ID="btnPosicaoRendaFixa" ForeColor="black" runat="server" CssClass="btnPopup" OnClientClick="popupPosicaoRendaFixa.ShowAtElementByID(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Ver Posições"/><div></div></asp:LinkButton>
                                    </td>                                    
                                </tr>             
                                
                                <tr>
                                                                                                   
                            </table>
                            
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="labelInfo" runat="server" CssClass="labelNormal" Text="Atenção: todas as operações de bloqueio e/ou desbloqueio na data serão deletadas." style="color:#58607C" />                            
                                    </td>
                                </tr>                                
                            </table>
                            
                            <div class="linhaH"></div>                                        
                            
                        </div>                        
                        </asp:Panel>
                    </EditForm>
                        
                    <StatusBar>
                        <div>
                            <div style="float:left">
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                            </div>                    
                        </div>
                    </StatusBar>
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="500px"  />
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>
                    
                </dxwgv:ASPxGridView>            
            </div>        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSBloqueioRendaFixa" runat="server" OnesSelect="EsDSBloqueioRendaFixa_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSPosicaoRendaFixa" runat="server" OnesSelect="EsDSPosicaoRendaFixa_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    
    </form>
</body>
</html>