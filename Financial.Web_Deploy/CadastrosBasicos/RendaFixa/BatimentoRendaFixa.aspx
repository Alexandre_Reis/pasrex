﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_BatimentoRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxuc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Batimento Renda Fixa</title>
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    function Uploader_OnUploadComplete(args) {                
            gridCadastro.Refresh();
            gridCadastro1.Refresh();
            
            if (args.callbackData != '') { 
                alert(args.callbackData);
            }
            
        }
    function dropMercadoClienteSelectedIndexChanged(s,e)
    {
    
        var trAgenteContraParte = document.getElementById('trAgenteContraParte');
        var trCarteiraContraParte = document.getElementById('trCarteiraContraParte');
    
        var textNomeCarteira = document.getElementById('tabBatimento_gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
        var textNomeAgente = document.getElementById('tabBatimento_gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeAgente');
        
                
        if(s.GetValue() === "1")
        {
            trCarteiraContraParte.style.display = "none";
            trAgenteContraParte.style.display = "table-row";
        }
        else if(s.GetValue() ==="2")
        {
            trAgenteContraParte.style.display = "none";            
            trCarteiraContraParte.style.display = "table-row";                        
        }       
        
        
        
    }
    
    function callbackErroComplete(s,e)
    {
        if (e.result != '')
        {                   
            alert(e.result);                                              
        }
        else
        {
            gridCadastro.UpdateEdit()
        }
    }
    
    function callbackOpCetipComplete(s,e)
    {
    
        if(e.result != '')
        {
            alert(e.result);
        }
        else
        {
        
            var dataExportacao = new Date();
            var curr_date = dataExportacao.getDate().toString();
            var curr_month = (dataExportacao.getMonth() + 1).toString(); //Months are zero based
            var curr_year = dataExportacao.getFullYear().toString();
            var dataExportacaoString = curr_year + '-' + (curr_month[1]?curr_month:'0'+curr_month[0]) + 
                '-' + (curr_date[1]? curr_date :'0' + curr_date[0]);
            
            var newLocation = window.location.href.toLowerCase().replace('/cadastrosbasicos/rendafixa/batimentorendafixa.aspx', 
                '/interfaces/exibeexportacao.aspx?Tipo=12&DataExportacao=' + dataExportacaoString);
            
            var iframeId = 'iframe_download';
            var iframe = document.getElementById(iframeId);

            if (!iframe && document.createElement && (iframe =
                document.createElement('iframe'))) {
            
                iframe.name = iframe.id = iframeId;
                iframe.width = 0;
                iframe.height = 0;
                iframe.src = 'about:blank';
                document.body.appendChild(iframe);                                                                    
            }
            iframe.src = newLocation;
            
        }
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackLiberarParaExportacao" runat="server" OnCallback="callbackLiberarParaExportacao_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
                gridCadastro.Refresh();
                gridCadastro1.Refresh();
                alert(e.result);                                              
            
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackPreencherIdCetip" runat="server" OnCallback="callbackPreencherIdCetip_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
                gridIPOCorretora.Refresh();
                alert(e.result);                                              
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackExportarIPOCorretora" runat="server" OnCallback="callbackExportarIPOCorretora_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    
               gridIPOCorretora.Refresh();
                if(e && e.result.length){
                    if(e.result.indexOf('redirect') === 0){
                        var newLocation = (e.result.split(':'))[1];
                        var iframeId = 'iframe_download';
                        var iframe = document.getElementById(iframeId);

                        if (!iframe && document.createElement && (iframe =
                            document.createElement('iframe'))) {
               
                            iframe.name = iframe.id = iframeId;
                            iframe.width = 0;
                            iframe.height = 0;
                            iframe.src = 'about:blank';
                            document.body.appendChild(iframe);                                                                    
                        }
                        iframe.src = newLocation;
                   
                    }else{
                        alert(e.result);
                    }
                }
                
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackExportarIPOClientes" runat="server" OnCallback="callbackExportarIPOClientes_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    
               gridIPOCorretora.Refresh();
               gridIPOClientes.Refresh();
               
                if(e && e.result.length){
                    if(e.result.indexOf('redirect') === 0){
                        var newLocation = (e.result.split(':'))[1];
                        var iframeId = 'iframe_download';
                        var iframe = document.getElementById(iframeId);

                        if (!iframe && document.createElement && (iframe =
                            document.createElement('iframe'))) {
               
                            iframe.name = iframe.id = iframeId;
                            iframe.width = 0;
                            iframe.height = 0;
                            iframe.src = 'about:blank';
                            document.body.appendChild(iframe);                                                                    
                        }
                        iframe.src = newLocation;
                        
                        gridIPOCorretora.Refresh();
                        gridIPOClientes.Refresh();
               
                   
                    }else{
                        alert(e.result);
                    }
                }
                
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackDelete" runat="server" OnCallback="callbackDelete_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
                gridCadastro.Refresh();
                gridCadastro1.Refresh();
                alert(e.result);                                              
            
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackUpdate" runat="server" OnCallback="callbackUpdate_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '') {                   
                alert(e.result);                              
            }    
            else {
                //LoadingPanel.Show();
                                
                uplUpdateFinancial.UploadFile();                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="callbackErroComplete" />
        </dxcb:ASPxCallback>
                <dxcb:ASPxCallback ID="callbackOpCetip" runat="server" OnCallback="callbackOpCetip_Callback">
            <ClientSideEvents CallbackComplete="callbackOpCetipComplete"/>
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Batimento de Renda Fixa"></asp:Label>
                            </div>
                            <dxtc:ASPxPageControl ID="tabBatimento" runat="server" ClientInstanceName="tabBatimento"
                                Height="100%" Width="100%" TabSpacing="0px" ActiveTabIndex="0">
                                <TabPages>
                                    <dxtc:TabPage Text="Batimento">
                                        <ContentCollection>
                                            <dxw:ContentControl runat="server">
                                                <div id="mainContent">
                                                    <div class="linkButton">
                                                        <asp:LinkButton ID="btnLiberarParaExportacao" runat="server" Font-Overline="false"
                                                            ValidationGroup="ATK" CssClass="btnOK" OnClientClick=" if (confirm('Tem certeza que deseja liberar para exportação?')==true) {callbackLiberarParaExportacao.SendCallback();} return false;">
                                                            <asp:Literal ID="Literal6" runat="server" Text="Liberar p/ Exportação" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                                            CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackDelete.SendCallback();} return false;">
                                                            <asp:Literal ID="Literal4" runat="server" Text="Excluir" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                                            OnClientClick="gridCadastro.Refresh(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                    <div class="divDataGrid">
                                                        <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao"
                                                            DataSourceID="EsDSOperacaoRendaFixa" OnPreRender="gridCadastro_PreRender" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                                            OnBeforeGetCallbackResult="gridCadastro_PreRender" OnCustomUnboundColumnData="gridCadastro1_CustomUnboundColumnData"
                                                            OnRowUpdating="gridCadastro_RowUpdating">
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                                    <HeaderTemplate>
                                                                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                                    </HeaderTemplate>
                                                                </dxwgv:GridViewCommandColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="StatusExportacaoDesc" UnboundType="String"
                                                                    Caption="Status" Width="7%">
                                                                    <PropertiesComboBox EncodeHtml="false" DropDownStyle="DropDown">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="Exportação Pendente" Text="Exportação Pendente" />
                                                                            <dxe:ListEditItem Value="Liberado para Exportação" Text="Liberado para Exportação" />
                                                                            <dxe:ListEditItem Value="Exportado" Text="Exportado" />
                                                                            <dxe:ListEditItem Value="Processado c/ Sucesso" Text="Processado c/ Sucesso" />
                                                                            <dxe:ListEditItem Value="Falta Liquidação" Text="Falta Liquidação" />
                                                                            <dxe:ListEditItem Value="Falta Papel" Text="Falta Papel" />
                                                                            <dxe:ListEditItem Value="Infos. Divergentes" Text="Infos. Divergentes" />
                                                                            <dxe:ListEditItem Value="Erro no Bloco" Text="Erro no Bloco" />
                                                                            <dxe:ListEditItem Value="Erro no Serviço" Text="Erro no Serviço" />
                                                                            <dxe:ListEditItem Value="Erro no Domínio" Text="Erro no Domínio" />
                                                                            <dxe:ListEditItem Value="Outros Erros" Text="Outros Erros" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="StatusBatimento" Caption="Status Batimento" Width="7%">
                                                                    <PropertiesComboBox EncodeHtml="false" DropDownStyle="DropDown">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Pendente" />
                                                                            <dxe:ListEditItem Value="2" Text="Batido" />
                                                                            <dxe:ListEditItem Value="3" Text="Em Análise" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Título" Width="14%"
                                                                    Settings-AutoFilterCondition="Contains">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn Caption="Emissão" FieldName="DataEmissao" Visible="true"
                                                                    Width="5%">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdOperacao" Visible="true" Width="5%">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo" Width="7%"
                                                                    ExportWidth="120">
                                                                    <PropertiesComboBox EncodeHtml="false" DropDownStyle="DropDown">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="<div title='Compra Final'>Compra Final</div>" />
                                                                            <dxe:ListEditItem Value="2" Text="<div title='Venda Final'>Venda Final</div>" />
                                                                            <dxe:ListEditItem Value="3" Text="<div title='Compra c/ Rev.'>Compra c/ Rev.</div>" />
                                                                            <dxe:ListEditItem Value="4" Text="<div title='Venda c/ Rec.'>Venda c/ Rec.</div>" />
                                                                            <dxe:ListEditItem Value="6" Text="<div title='Venda Total'>Venda Total</div>" />
                                                                            <dxe:ListEditItem Value="10" Text="<div title='Compra Casada'>Compra Casada</div>" />
                                                                            <dxe:ListEditItem Value="11" Text="<div title='Venda Casada'>Venda Casada</div>" />
                                                                            <dxe:ListEditItem Value="12" Text="<div title='Antecipação Revenda'>Antecip. Rev.</div>" />
                                                                            <dxe:ListEditItem Value="13" Text="<div title='Antecipação Recompra'>Antecip. Rec.</div>" />
                                                                            <dxe:ListEditItem Value="20" Text="<div title='Depósito'>Depósito</div>" />
                                                                            <dxe:ListEditItem Value="21" Text="<div title='Retirada'>Retirada</div>" />
                                                                            <dxe:ListEditItem Value="22" Text="<div title='Bloqueio'>Bloqueio</div>" />
                                                                            <dxe:ListEditItem Value="23" Text="<div title='Desbloqueio'>Desbloqueio</div>" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Quantidade" Width="8%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="PUOperacao" Caption="PU" Width="10%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Valor" Width="9%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdCliente" Width="8%" CellStyle-HorizontalAlign="left">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="Apelido" Width="5%" ExportWidth="200">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoContraparte" UnboundType="String"
                                                                    Caption="Mercado/Cliente" Width="6%" ExportWidth="120">
                                                                    <PropertiesComboBox EncodeHtml="false" DropDownStyle="DropDown">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="<div title='Mercado'>Mercado</div>" />
                                                                            <dxe:ListEditItem Value="2" Text="<div title='Cliente'>Cliente</div>" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataColumn Caption="Contraparte" FieldName="NomeContraparte" UnboundType="string"
                                                                    Width="8%" ExportWidth="200">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" Width="10%" />
                                                                <dxwgv:GridViewDataColumn Caption="Id Vinculado" FieldName="IdOperacaoVinculo" ExportWidth="200" />
                                                                <dxwgv:GridViewDataColumn FieldName="IdTitulo" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DataLiquidacao" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="TaxaOperacao" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="TipoNegociacao" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DataVolta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="TaxaVolta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="PUVolta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="ValorVolta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdLiquidacao" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdCustodia" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="textoPosicao" Visible="false" UnboundType="String">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DataModificacao" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                            </Columns>                                                            
                                                            <Templates>
                                                                <EditForm>
                                                                    <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                                    <asp:Panel ID="panelEdicao" runat="server">
                                                                        <div class="editForm">
                                                                            
                                                                            
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                                            ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>' MaxLength="10"
                                                                                            NumberType="Integer" ClientEnabled="false">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} "
                                                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox ID="textNomeCliente" Style="width: 272px;" runat="server" CssClass="textNome"
                                                                                            Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelMercadoCliente" runat="server" CssClass="labelRequired" Text="Mercado/Cliente:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropMercadoCliente" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownListCurto_2" Text='<%#Eval("TipoContraparte")%>' ClientEnabled="false">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Mercado" />
                                                                                                <dxe:ListEditItem Value="2" Text="Cliente" />
                                                                                            </Items>
                                                                                            <ClientSideEvents SelectedIndexChanged="dropMercadoClienteSelectedIndexChanged" Init="dropMercadoClienteSelectedIndexChanged" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trAgenteContraParte" style="display: none">
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIdAgenteContraParte" runat="server" CssClass="labelRequired"
                                                                                            Text="Contraparte:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="btnAgenteContraParte" runat="server" CssClass="textButtonEdit"
                                                                                            ClientInstanceName="btnAgenteContraParte" Text='<%#Eval("IdAgenteContraParte")%>'
                                                                                            MaxLength="10" NumberType="Integer" ClientEnabled="false">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeAgente').value = '';} "
                                                                                                ButtonClick="function(s, e) {popupAgenteMercado.ShowAtElementByID(s.name);}"
                                                                                                LostFocus="function(s, e) {OnLostFocus(popupMensagemAgenteMercado,CallbackAgenteMercado, btnAgenteContraParte);}" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox ID="textNomeAgente" runat="server" CssClass="textNomeAgente" Enabled="false"
                                                                                            Text='<%#Eval("AgenteMercadoContraparte")%>'></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="trCarteiraContraParte" style="display: none">
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIdCarteiraContraparte" runat="server" CssClass="labelRequired"
                                                                                            Text="Contraparte:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="btnCarteiraContraparte" runat="server" CssClass="textButtonEdit"
                                                                                            ClientInstanceName="btnCarteiraContraparte" Text='<%#Eval("IdClienteContraparte")%>'
                                                                                            MaxLength="10" NumberType="Integer" ClientEnabled="false">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                                                ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira,CallbackCarteira, btnCarteiraContraparte);}" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNomeCarteira" Enabled="false"
                                                                                            Text='<%#Eval("ApelidoClienteContraparte")%>'></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoOperacao" ClientInstanceName="dropTipoOperacao" runat="server"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                                            Text='<%#Eval("TipoOperacao")%>' ClientEnabled="false">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Compra Final" />
                                                                                                <dxe:ListEditItem Value="2" Text="Venda Final" />
                                                                                                <dxe:ListEditItem Value="3" Text="Compra c/ Rev." />
                                                                                                <dxe:ListEditItem Value="4" Text="Venda c/ Rec." />
                                                                                                <dxe:ListEditItem Value="6" Text="Venda Total" />
                                                                                                <dxe:ListEditItem Value="10" Text="Compra Casada" />
                                                                                                <dxe:ListEditItem Value="11" Text="Venda Casada" />
                                                                                                <dxe:ListEditItem Value="12" Text="Antecip. Rev." />
                                                                                                <dxe:ListEditItem Value="13" Text="Antecip. Rec." />
                                                                                                <dxe:ListEditItem Value="20" Text="Depósito" />
                                                                                                <dxe:ListEditItem Value="21" Text="Retirada" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoNegociacao" runat="server" CssClass="labelNormal" Text="Categoria:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoNegociacao" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                                            CssClass="dropDownListCurto_2" Text='<%#Eval("TipoNegociacao")%>' ClientEnabled="false">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Negociação" />
                                                                                                <dxe:ListEditItem Value="2" Text="Disp. Venda" />
                                                                                                <dxe:ListEditItem Value="3" Text="Vencimento" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table border="0">
                                                                                <tr>
                                                                                    <asp:Label ID="labelMensagem" runat="server" Text="  (Clique no botão...)" CssClass="labelNormal"
                                                                                        Visible="false"></asp:Label>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTitulo" runat="server" CssClass="labelRequired" Text="Título:" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <asp:TextBox ID="hiddenIdOperacao" runat="server" CssClass="hiddenField" Text='<%#Eval("IdOperacao")%>' />
                                                                                        <asp:TextBox ID="hiddenIdTitulo" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTitulo")%>' />
                                                                                        <asp:TextBox ID="hiddenDataModificacao" runat="server" CssClass="hiddenField" Text='<%#Eval("DataModificacao")%>' />
                                                                                        <dxe:ASPxButtonEdit ID="btnEditTituloRF" runat="server" CssClass="textButtonEdit"
                                                                                            ClientInstanceName="btnEditTituloRF" ReadOnly="true" Width="380px" Text='<%#Eval("DescricaoCompleta")%>'
                                                                                            ClientEnabled="false">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents ButtonClick="function(s, e) {popupTituloRF.ShowAtElementByID(s.name);}" />
                                                                                        </dxe:ASPxButtonEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <asp:Label ID="labelMensagem2" runat="server" Text="  (Clique no botão...)" CssClass="labelNormal"
                                                                                        Visible="false"></asp:Label>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelPosicao" runat="server" CssClass="labelNormal" Text="Id Posição:" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <asp:TextBox ID="hiddenIdPosicao" runat="server" CssClass="hiddenField" />
                                                                                        <dxe:ASPxButtonEdit ID="btnEditPosicao" runat="server" CssClass="textButtonEdit"
                                                                                            ClientInstanceName="btnEditPosicao" ReadOnly="true" Width="380px" ClientEnabled="false"
                                                                                            Text='<%#Eval("textoPosicao")%>'>
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents ButtonClick="function(s, e) {popupPosicao.ShowAtElementByID(s.name);}" />
                                                                                        </dxe:ASPxButtonEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Registro: " />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataRegistro" runat="server" ClientInstanceName="textDataRegistro"
                                                                                            Value='<%#Eval("DataRegistro")%>' ClientEnabled="false" />
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:Label ID="labelTaxaOperacao" runat="server" CssClass="labelRequired" Text="Tx Operação:"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 165px;">
                                                                                        <dxe:ASPxSpinEdit ID="textTaxaOperacao" runat="server" CssClass="textValor_5" Style="float: left;"
                                                                                            ClientInstanceName="textTaxaOperacao" MaxLength="25" NumberType="Float" DecimalPlaces="16"
                                                                                            Text='<%#Eval("TaxaOperacao")%>' ClientEnabled="false">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                        <div class="linkButton linkButtonNoBorder" style="display: inline">
                                                                                            <asp:LinkButton ID="btnCalculaTaxa" runat="server" CssClass="btnCalc iconOnly" OnClientClick="callbackTaxa.SendCallback(); return false;">
                                                                                                <asp:Literal ID="Literal2" runat="server" Text="&nbsp" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelDataOperacao" runat="server" CssClass="labelRequired" Text="Operação:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataOperacao" runat="server" ClientInstanceName="textDataOperacao"
                                                                                            Value='<%#Eval("DataOperacao")%>' ClientEnabled="false" />
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:Label ID="labelDataVolta" runat="server" CssClass="labelNormal" Text="Data Volta:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataVolta" runat="server" ClientInstanceName="textDataVolta"
                                                                                            ClientEnabled="false" Value='<%#Eval("DataVolta")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <!-- Opcional Forma Liquidacao/Custodia -->
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelFormaLiquidacao" runat="server" CssClass="labelRequired" Text="Liquida:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropFormaLiquidacao" runat="server" ClientInstanceName="dropFormaLiquidacao"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_5"
                                                                                            Text='<%# Eval("IdLiquidacao") %>' ClientEnabled="false">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="0" Text="-" />
                                                                                                <dxe:ListEditItem Value="1" Text="Selic" />
                                                                                                <dxe:ListEditItem Value="2" Text="Cetip" />
                                                                                                <dxe:ListEditItem Value="3" Text="CBLC" />
                                                                                            </Items>
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { dropLocalCustodia.SetSelectedIndex(s.GetSelectedIndex());  }" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:Label ID="labelLocalCustodia" runat="server" CssClass="labelNormal" Text="Custódia:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropLocalCustodia" ClientInstanceName="dropLocalCustodia" runat="server"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                                            Text='<%#Eval("IdCustodia")%>' ClientEnabled="false">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="0" Text="-" />
                                                                                                <dxe:ListEditItem Value="1" Text="Selic" />
                                                                                                <dxe:ListEditItem Value="2" Text="Cetip" />
                                                                                                <dxe:ListEditItem Value="3" Text="CBLC" />
                                                                                            </Items>
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { dropFormaLiquidacao.SetSelectedIndex(s.GetSelectedIndex());  }" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        <asp:Label ID="labelDataLiquidacao" runat="server" CssClass="labelRequired" Text="Liquidação:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataLiquidacao" runat="server" ClientInstanceName="textDataLiquidacao"
                                                                                            Value='<%#Eval("DataLiquidacao")%>' ClientEnabled="false" />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Índice:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropIndiceVolta" runat="server" ClientInstanceName="dropIndiceVolta"
                                                                                            ClientEnabled="false" CssClass="dropDownList" IncrementalFilteringMode="StartsWith"
                                                                                            ShowShadow="false" DropDownStyle="DropDown" DataSourceID="EsDSIndice" ValueField="IdIndice"
                                                                                            TextField="Descricao" Text='<%#Eval("IdIndiceVolta")%>'>
                                                                                            <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); }
                                                                                " />
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { 
                                                                            if(s.GetSelectedIndex() != -1)
                                                                                {
                                                                                    textPUVolta.SetEnabled(false);
                                                                                    textValorVolta.SetEnabled(false);
                                                                                    textPUVolta.SetText('');
                                                                                    textValorVolta.SetText('');    
                                                                                }
                                                                                else
                                                                                {
                                                                                    textPUVolta.SetEnabled(true);
                                                                                    textValorVolta.SetEnabled(true);
                                                                                }
                                                                                
                                                                             }
                                                                                " />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Qtde:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor_5" ClientInstanceName="textQuantidade"
                                                                                            MaxLength="25" NumberType="Float" DecimalPlaces="12" Text='<%#Eval("Quantidade")%>'
                                                                                            ClientEnabled="false">
                                                                                            <ClientSideEvents LostFocus="function(s, e) {CalculoTriplo(textPUOperacao, textQuantidade, textValor, 1);
                                                                         }" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:Label ID="labelTaxaVolta" runat="server" CssClass="labelNormal" Text="Taxa Volta:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textTaxaVolta" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxaVolta"
                                                                                            ClientEnabled="false" MaxLength="8" NumberType="Float" DecimalPlaces="4" Text='<%#Eval("TaxaVolta")%>'>
                                                                                            <ClientSideEvents LostFocus="function(s, e) {if(dropIndiceVolta.GetSelectedIndex() == -1) ASPxCallback3.SendCallback(); }" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelPUOperacao" runat="server" CssClass="labelRequired" Text="PU:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textPUOperacao" runat="server" Style="float: left;" CssClass="textValor_5"
                                                                                            ClientInstanceName="textPUOperacao" MaxLength="25" NumberType="Float" DecimalPlaces="12"
                                                                                            Text='<%#Eval("PUOperacao")%>' ClientEnabled="false">
                                                                                            <ClientSideEvents LostFocus="function(s, e) {CalculoTriplo(textPUOperacao, textQuantidade, textValor, 1);
                                                                         }" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                        <div class="linkButton linkButtonNoBorder" style="display: inline">
                                                                                            <asp:LinkButton ID="btnCalculaPU" runat="server" CssClass="btnCalc iconOnly" OnClientClick="callbackPU.SendCallback(); return false;">
                                                                                                <asp:Literal ID="Literal11" runat="server" Text="&nbsp" /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:Label ID="labelPUVolta" runat="server" CssClass="labelNormal" Text="PU Volta:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textPUVolta" runat="server" CssClass="textValor_5" ClientInstanceName="textPUVolta"
                                                                                            ClientEnabled="false" MaxLength="25" NumberType="Float" DecimalPlaces="12" Text='<%#Eval("PUVolta")%>'>
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor"
                                                                                            MaxLength="12" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("Valor")%>'
                                                                                            ClientEnabled="false">
                                                                                            <ClientSideEvents LostFocus="function(s, e) {CalculoTriplo(textPUOperacao, textQuantidade, textValor, 1);
                                                                         }" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <asp:Label ID="labelValorVolta" runat="server" CssClass="labelNormal" Text="Valor Volta:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textValorVolta" runat="server" CssClass="textValor_5" ClientInstanceName="textValorVolta"
                                                                                            ClientEnabled="false" MaxLength="14" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("ValorVolta")%>'>
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCheck" runat="server" CssClass="labelNormal" Text="Termo de RF:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkOperacaoTermo" runat="server" Text="" />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Valor Corretagem:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textValorCorretagem" runat="server" CssClass="textValor_5"
                                                                                            ClientInstanceName="textValorCorretagem" MaxLength="19" NumberType="Float" DecimalPlaces="2"
                                                                                            Text='<%#Eval("ValorCorretagem")%>' ClientEnabled="false">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIdVinculo" runat="server" CssClass="labelNormal" Text="Id Vinculado:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textIdVinculo" runat="server" CssClass="textValor_5" ClientInstanceName="textIdVinculo"
                                                                                            MaxLength="12" NumberType="Integer" ClientEnabled="false" Text='<%#Eval("IdOperacaoVinculo")%>' />
                                                                                    </td>
                                                                                    <%--<td class="td_Label">
                                                                                        <asp:Label ID="labelCodigoContraParte" runat="server" CssClass="labelNormal" Text="Contraparte:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="textCodigoContraParte" runat="server" Text='<%# Eval("CodigoContraParte") %>'></asp:TextBox>
                                                                                    </td>--%>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="IPO:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkIsIPO" runat="server" Text="" />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelStatus" runat="server" CssClass="labelNormal" Text="Status:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropStatusExportacao" ClientInstanceName="dropTipoOperacao" runat="server"
                                                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                                            Text='<%#Eval("StatusBatimento")%>' >
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Pendente" />
                                                                                                <dxe:ListEditItem Value="2" Text="Batido" />
                                                                                                <dxe:ListEditItem Value="3" Text="Em Análise" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                        
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelObservacao" runat="server" CssClass="labelNormal" Text="Observação:"> </asp:Label>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" Rows="4" Style="width: 324px;"
                                                                                            CssClass="textLongo5" Text='<%#Eval("Observacao")%>' />
                                                                                        <div class="linkButton linkButtonNoBorder" style="display: inline-table">
                                                                                            <asp:LinkButton ID="btnObservacao" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                                Visible="false" OnClientClick="operacao='salvarObservacao'; gridCadastro.PerformCallback('btnObservacao'); return false;">
                                                                                                <asp:Literal ID="Literal15" runat="server" Text="Salva Obs." /><div>
                                                                                                </div>
                                                                                            </asp:LinkButton>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="display: none">
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataModificacao" runat="server" ClientInstanceName="textDataModificacao"
                                                                                            Value='<%#Eval("DataModificacao")%>' ClientEnabled="false" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <div class="linhaH">
                                                                            </div>
                                                                            <div class="linkButton linkButtonNoBorder popupFooter">
                                                                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                                    OnClientClick="callbackErro.SendCallback(); return false;">
                                                                                    <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                                                    </div>
                                                                                </asp:LinkButton>
                                                                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                                    OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                                    <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                                                                                    </div>
                                                                                </asp:LinkButton>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </EditForm>
                                                                <StatusBar>
                                                                    <div>
                                                                        <div style="float: left">
                                                                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                </StatusBar>
                                                            </Templates>
                                                            <SettingsPopup EditForm-Width="550px" />
                                                            <SettingsCommandButton>
                                                                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                                            </SettingsCommandButton>
                                                        </dxwgv:ASPxGridView>
                                                    </div>
                                                </div>
                                            </dxw:ContentControl>
                                        </ContentCollection>
                                    </dxtc:TabPage>
                                    <dxtc:TabPage Text="Acompanhamento">
                                        <ContentCollection>
                                            <dxw:ContentControl runat="server">
                                                <div id="Div1">
                                                    <div class="divDataGrid">
                                                        <div class="linkButton">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                OnClientClick="callbackOpCetip.SendCallback(); return false;                                                                             ">
                                                                <asp:Literal ID="Literal1" runat="server" Text="Exportar p/ Cetip" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton5" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                                                OnClientClick="gridCadastro1.Refresh(); return false;">
                                                                <asp:Literal ID="Literal10" runat="server" Text="Atualizar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                        <dxwgv:ASPxGridView ID="gridCadastro1" OnCustomUnboundColumnData="gridCadastro1_CustomUnboundColumnData"
                                                            runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao" DataSourceID="EsDSOperacaoRendaFixaAcompanhamento"
                                                            ClientInstanceName="gridCadastro1">
                                                            <Columns>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="StatusExportacaoDesc" UnboundType="String"
                                                                    Caption="Status" Width="8%">
                                                                    <PropertiesComboBox EncodeHtml="false" DropDownStyle="DropDown">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="Exportação Pendente" Text="Exportação Pendente" />
                                                                            <dxe:ListEditItem Value="Liberado para Exportação" Text="Liberado para Exportação" />
                                                                            <dxe:ListEditItem Value="Exportado" Text="Exportado" />
                                                                            <dxe:ListEditItem Value="Processado c/ Sucesso" Text="Processado c/ Sucesso" />
                                                                            <dxe:ListEditItem Value="Falta Liquidação" Text="Falta Liquidação" />
                                                                            <dxe:ListEditItem Value="Falta Papel" Text="Falta Papel" />
                                                                            <dxe:ListEditItem Value="Infos. Divergentes" Text="Infos. Divergentes" />
                                                                            <dxe:ListEditItem Value="Erro no Bloco" Text="Erro no Bloco" />
                                                                            <dxe:ListEditItem Value="Erro no Serviço" Text="Erro no Serviço" />
                                                                            <dxe:ListEditItem Value="Erro no Domínio" Text="Erro no Domínio" />
                                                                            <dxe:ListEditItem Value="Outros Erros" Text="Outros Erros" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Título" Width="28%"
                                                                    Settings-AutoFilterCondition="Contains">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn Caption="Emissão" FieldName="DataEmissao" Visible="true"
                                                                    Width="5%">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdOperacao" Visible="true">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo" Width="12%"
                                                                    ExportWidth="120">
                                                                    <PropertiesComboBox EncodeHtml="false">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="<div title='Compra Final'>Compra Final</div>" />
                                                                            <dxe:ListEditItem Value="2" Text="<div title='Venda Final'>Venda Final</div>" />
                                                                            <dxe:ListEditItem Value="3" Text="<div title='Compra c/ Rev.'>Compra c/ Rev.</div>" />
                                                                            <dxe:ListEditItem Value="4" Text="<div title='Venda c/ Rec.'>Venda c/ Rec.</div>" />
                                                                            <dxe:ListEditItem Value="6" Text="<div title='Venda Total'>Venda Total</div>" />
                                                                            <dxe:ListEditItem Value="10" Text="<div title='Compra Casada'>Compra Casada</div>" />
                                                                            <dxe:ListEditItem Value="11" Text="<div title='Venda Casada'>Venda Casada</div>" />
                                                                            <dxe:ListEditItem Value="12" Text="<div title='Antecipação Revenda'>Antecip. Rev.</div>" />
                                                                            <dxe:ListEditItem Value="13" Text="<div title='Antecipação Recompra'>Antecip. Rec.</div>" />
                                                                            <dxe:ListEditItem Value="20" Text="<div title='Depósito'>Depósito</div>" />
                                                                            <dxe:ListEditItem Value="21" Text="<div title='Retirada'>Retirada</div>" />
                                                                            <dxe:ListEditItem Value="22" Text="<div title='Bloqueio'>Bloqueio</div>" />
                                                                            <dxe:ListEditItem Value="23" Text="<div title='Desbloqueio'>Desbloqueio</div>" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Quantidade" Width="10%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="PUOperacao" Caption="PU" Width="10%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Valor" Width="12%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdCliente" Width="8%" CellStyle-HorizontalAlign="left">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="Apelido" Width="17%" ExportWidth="200">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoContraparte" UnboundType="String"
                                                                    Caption="Mercado/Cliente" Width="6%" ExportWidth="120">
                                                                    <PropertiesComboBox EncodeHtml="false" DropDownStyle="DropDown">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="<div title='Mercado'>Mercado</div>" />
                                                                            <dxe:ListEditItem Value="2" Text="<div title='Cliente'>Cliente</div>" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataColumn Caption="Contraparte" FieldName="NomeContraparte" UnboundType="string"
                                                                    Width="8%" ExportWidth="200">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" Width="8%" />
                                                                <dxwgv:GridViewDataColumn Caption="Id Vinculado" FieldName="IdOperacaoVinculo" ExportWidth="200" />
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="IdLiquidacao" Caption="Clearing" Width="6%"
                                                                    ExportWidth="120">
                                                                    <PropertiesComboBox EncodeHtml="false" DropDownStyle="DropDown">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="0" Text="<div title='-'>'-'</div>" />
                                                                            <dxe:ListEditItem Value="1" Text="<div title='Selic'>Selic</div>" />
                                                                            <dxe:ListEditItem Value="2" Text="<div title='Cetip'>Cetip</div>" />
                                                                            <dxe:ListEditItem Value="3" Text="<div title='CBLC'>CBLC</div>" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdTitulo" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DataLiquidacao" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="TaxaOperacao" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="TipoNegociacao" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DataVolta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="TaxaVolta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="PUVolta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="ValorVolta" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdCustodia" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                            </Columns>
                                                        </dxwgv:ASPxGridView>
                                                    </div>
                                                </div>
                                                <div style="margin-top: 20px;" class="linkButton linkButtonNoBorder">
                                                    <asp:LinkButton ID="btnUpdate" runat="server" Font-Overline="false" CssClass="btnRun"
                                                        OnClientClick="
                                                                    {
                                                                        if(uplUpdateFinancial.GetText()== '') {
                                                                            alert('Escolha o arquivo com o retorno do processamento da Cetip');                                                                            
                                                                        }
                                                                        else {                                                                    
                                                                            callbackUpdate.SendCallback();                                                                             
                                                                        }
                                                                        return false;
                                                                    }
                                                                    ">
                                                        <asp:Literal ID="Literal5" runat="server" Text="Processar Retorno Cetip" /><div>
                                                        </div>
                                                    </asp:LinkButton>
                                                    <dxuc:ASPxUploadControl ID="uplUpdateFinancial" runat="server" ClientInstanceName="uplUpdateFinancial"
                                                        OnFileUploadComplete="uplUpdateFinancial_FileUploadComplete" CssClass="labelNormal"
                                                        EnableDefaultAppearance="False" FileUploadMode="OnPageLoad">
                                                        <ValidationSettings MaxFileSize="100000000" />
                                                        <ClientSideEvents FileUploadComplete="function(s, e) {Uploader_OnUploadComplete(e); }">
                                                        </ClientSideEvents>
                                                    </dxuc:ASPxUploadControl>
                                                </div>
                                            </dxw:ContentControl>
                                        </ContentCollection>
                                    </dxtc:TabPage>
                                    <dxtc:TabPage Text="Clientes">
                                        <ContentCollection>
                                            <dxw:ContentControl runat="server">
                                                <div class="linkButton linkButtonNoBorder">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label" style="width: 50px;">
                                                                <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Data:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textData_ddmmyyyy" runat="server" ClientInstanceName="textData_ddmmyyyy" />
                                                            </td>
                                                            <td>
                                                                <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                    OnClientClick="
                                                        var dataExportacao = textData_ddmmyyyy.GetDate();
                                                        
                                                        var curr_date = dataExportacao.getDate().toString();
                                                        var curr_month = (dataExportacao.getMonth() + 1).toString(); //Months are zero based
                                                        var curr_year = dataExportacao.getFullYear().toString();
                                                        var dataExportacaoString = curr_year + '-' + (curr_month[1]?curr_month:'0'+curr_month[0]) + 
                                                            '-' + (curr_date[1]? curr_date :'0' + curr_date[0]);
                                                        
                                                        var newLocation = window.location.href.toLowerCase().replace('/cadastrosbasicos/rendafixa/batimentorendafixa.aspx', 
                                                            '/interfaces/exibeexportacao.aspx?Tipo=11&DataExportacao=' + dataExportacaoString);
                                                        
                                                        var iframeId = 'iframe_download';
                                                        var iframe = document.getElementById(iframeId);

                                                        if (!iframe && document.createElement && (iframe =
                                                            document.createElement('iframe'))) {
               
                                                            iframe.name = iframe.id = iframeId;
                                                            iframe.width = 0;
                                                            iframe.height = 0;
                                                            iframe.src = 'about:blank';
                                                            document.body.appendChild(iframe);                                                                    
                                                        }
                                                        iframe.src = newLocation;
                                                        return false;
                                                        ">
                                                                    <asp:Literal ID="Literal2" runat="server" Text="Exportar p/ Cetip" /><div>
                                                                    </div>
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </dxw:ContentControl>
                                        </ContentCollection>
                                    </dxtc:TabPage>
                                    <dxtc:TabPage Text="IPO">
                                        <ContentCollection>
                                            <dxw:ContentControl runat="server">
                                                <div id="Div2">
                                                    <div class="divDataGrid">
                                                        Compras Corretora:
                                                        <div class="linkButton">
                                                            <asp:LinkButton ID="LinkButton8" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                                                CssClass="btnOK" OnClientClick=" if (confirm('Tem certeza que deseja exportar?')==true) {callbackExportarIPOCorretora.SendCallback();} return false;">
                                                                <asp:Literal ID="Literal12" runat="server" Text="Exportar Compras Corretora" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton9" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                                                CssClass="btnOK" OnClientClick="var idCetip=prompt('Identificador Cetip:'); if(idCetip != null){callbackPreencherIdCetip.SendCallback(idCetip);} return false;">
                                                                <asp:Literal ID="Literal13" runat="server" Text="Informar Id Cetip" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                                                CssClass="btnOK" OnClientClick=" if (confirm('Tem certeza que deseja exportar?')==true) {callbackExportarIPOClientes.SendCallback();} return false;">
                                                                <asp:Literal ID="Literal3" runat="server" Text="Exportar Compras Clientes" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                                                OnClientClick="gridIPOCorretora.Refresh(); gridIPOClientes.Refresh(); return false;">
                                                                <asp:Literal ID="Literal7" runat="server" Text="Atualizar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                        <dxwgv:ASPxGridView ID="gridIPOCorretora" OnCustomUnboundColumnData="gridIPOCorretora_CustomUnboundColumnData"
                                                            runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao" DataSourceID="EsDSOperacaoRendaFixaIPOCorretora"
                                                            ClientInstanceName="gridIPOCorretora">
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                                    <HeaderTemplate>
                                                                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridIPOCorretora);}" OnInit="CheckBoxSelectAll"/>
                                                                    </HeaderTemplate>
                                                                </dxwgv:GridViewCommandColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="StatusExportacaoDesc" UnboundType="String"
                                                                    Caption="Status" Width="8%">
                                                                    <PropertiesComboBox EncodeHtml="false" DropDownStyle="DropDown">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="Não Exportado" Text="Não Exportado" />
                                                                            <dxe:ListEditItem Value="Exportado" Text="Exportado" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Título" Width="28%"
                                                                    Settings-AutoFilterCondition="Contains">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Quantidade" Width="12%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="PUOperacao" Caption="PU" Width="13%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Valor" Width="12%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" Width="10%" />
                                                                <dxwgv:GridViewDataColumn Caption="Id Cetip" FieldName="IdOperacaoExterna" ExportWidth="200" />
                                                                <dxwgv:GridViewDataColumn FieldName="IdTitulo" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdCustodia" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdOperacao" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                            </Columns>
                                                            <SettingsCommandButton>
                                                                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                                            </SettingsCommandButton>
                                                        </dxwgv:ASPxGridView>
                                                    </div>
                                                    <div class="divDataGrid" style="margin-top: 30px;">
                                                        Compras Clientes:
                                                        <dxwgv:ASPxGridView ID="gridIPOClientes" OnCustomUnboundColumnData="gridIPOClientes_CustomUnboundColumnData"
                                                            runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao" DataSourceID="EsDSOperacaoRendaFixaIPOClientes"
                                                            ClientInstanceName="gridIPOClientes">
                                                            <Columns>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="StatusExportacaoDesc" UnboundType="String"
                                                                    Caption="Status" Width="8%">
                                                                    <PropertiesComboBox EncodeHtml="false" DropDownStyle="DropDown">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="Exportação Pendente" Text="Exportação Pendente" />
                                                                            <dxe:ListEditItem Value="Liberado para Exportação" Text="Liberado para Exportação" />
                                                                            <dxe:ListEditItem Value="Exportado" Text="Exportado" />
                                                                            <dxe:ListEditItem Value="Processado c/ Sucesso" Text="Processado c/ Sucesso" />
                                                                            <dxe:ListEditItem Value="Falta Liquidação" Text="Falta Liquidação" />
                                                                            <dxe:ListEditItem Value="Falta Papel" Text="Falta Papel" />
                                                                            <dxe:ListEditItem Value="Infos. Divergentes" Text="Infos. Divergentes" />
                                                                            <dxe:ListEditItem Value="Erro no Bloco" Text="Erro no Bloco" />
                                                                            <dxe:ListEditItem Value="Erro no Serviço" Text="Erro no Serviço" />
                                                                            <dxe:ListEditItem Value="Erro no Domínio" Text="Erro no Domínio" />
                                                                            <dxe:ListEditItem Value="Outros Erros" Text="Outros Erros" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Caption="Título" Width="28%"
                                                                    Settings-AutoFilterCondition="Contains">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn Caption="Emissão" FieldName="DataEmissao" Visible="true"
                                                                    Width="5%">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdOperacao" Visible="true">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo" Width="12%"
                                                                    ExportWidth="120">
                                                                    <PropertiesComboBox EncodeHtml="false">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="<div title='Compra Final'>Compra Final</div>" />
                                                                            <dxe:ListEditItem Value="2" Text="<div title='Venda Final'>Venda Final</div>" />
                                                                            <dxe:ListEditItem Value="3" Text="<div title='Compra c/ Rev.'>Compra c/ Rev.</div>" />
                                                                            <dxe:ListEditItem Value="4" Text="<div title='Venda c/ Rec.'>Venda c/ Rec.</div>" />
                                                                            <dxe:ListEditItem Value="6" Text="<div title='Venda Total'>Venda Total</div>" />
                                                                            <dxe:ListEditItem Value="10" Text="<div title='Compra Casada'>Compra Casada</div>" />
                                                                            <dxe:ListEditItem Value="11" Text="<div title='Venda Casada'>Venda Casada</div>" />
                                                                            <dxe:ListEditItem Value="12" Text="<div title='Antecipação Revenda'>Antecip. Rev.</div>" />
                                                                            <dxe:ListEditItem Value="13" Text="<div title='Antecipação Recompra'>Antecip. Rec.</div>" />
                                                                            <dxe:ListEditItem Value="20" Text="<div title='Depósito'>Depósito</div>" />
                                                                            <dxe:ListEditItem Value="21" Text="<div title='Retirada'>Retirada</div>" />
                                                                            <dxe:ListEditItem Value="22" Text="<div title='Bloqueio'>Bloqueio</div>" />
                                                                            <dxe:ListEditItem Value="23" Text="<div title='Desbloqueio'>Desbloqueio</div>" />
                                                                        </Items>
                                                                    </PropertiesComboBox>
                                                                </dxwgv:GridViewDataComboBoxColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Quantidade" Width="12%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="PUOperacao" Caption="PU" Width="13%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn FieldName="Valor" Width="12%" HeaderStyle-HorizontalAlign="Right"
                                                                    FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                    </PropertiesTextEdit>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdCliente" Width="8%" CellStyle-HorizontalAlign="left">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="Apelido" Width="17%" ExportWidth="200">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" Width="10%" />
                                                                <dxwgv:GridViewDataColumn FieldName="IdTitulo" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                                <dxwgv:GridViewDataColumn FieldName="IdCustodia" Visible="false">
                                                                </dxwgv:GridViewDataColumn>
                                                            </Columns>
                                                        </dxwgv:ASPxGridView>
                                                    </div>
                                                </div>
                                            </dxw:ContentControl>
                                        </ContentCollection>
                                    </dxtc:TabPage>
                                </TabPages>
                            </dxtc:ASPxPageControl>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" LeftMargin="30" RightMargin="30">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSOperacaoRendaFixa" runat="server" OnesSelect="EsDSOperacaoRendaFixa_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSOperacaoRendaFixaAcompanhamento" runat="server" OnesSelect="EsDSOperacaoRendaFixaAcompanhamento_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSOperacaoRendaFixaIPOCorretora" runat="server" OnesSelect="EsDSOperacaoRendaFixaIPOCorretora_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSOperacaoRendaFixaIPOClientes" runat="server" OnesSelect="EsDSOperacaoRendaFixaIPOClientes_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRF" runat="server" OnesSelect="EsDSTituloRF_esSelect" />
        <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteMercadoCorretora" runat="server" OnesSelect="EsDSAgenteMercadoCorretora_esSelect" />
    </form>
</body>
</html>
