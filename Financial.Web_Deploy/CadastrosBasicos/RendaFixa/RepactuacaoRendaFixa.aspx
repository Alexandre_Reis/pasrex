﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_RepactuacaoRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;
        var operacao = '';

        function OnGetDataTituloRendaFixaNovo(data) {
            hiddenIdTituloNovo.SetValue(data);
            callBackPopupTituloNovo.SendCallback(data);
            popupTituloRendaFixaNovo.HideWindow();
            btnEditTituloRendaFixaNovo.Focus();
        }

        function OnGetDataTituloRendaFixaOrig(data) {
            hiddenIdTituloOrig.SetValue(data);
            callBackPopupTituloOrig.SendCallback(data);
            popupTituloRendaFixaOrig.HideWindow();
            btnEditTituloRendaFixaOrig.Focus();
        }

              
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {        
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPopupTituloOrig" runat="server" OnCallback="callBackPopupTituloOrig_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditTituloRendaFixaOrig.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupTituloRendaFixaOrig" ClientInstanceName="popupTituloRendaFixaOrig"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupCCC_TituloRendaFixaOrig" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridTituloRendaFixaOrig" runat="server" Width="100%" ClientInstanceName="gridTituloRendaFixaOrig"
                            AutoGenerateColumns="False" DataSourceID="EsDSTituloRendaFixaOrig" KeyFieldName="IdTitulo"
                            OnCustomDataCallback="gridTituloRendaFixaOrig_CustomDataCallback" OnCustomCallback="gridTituloRendaFixaOrig_CustomCallback"
                            OnHtmlRowCreated="gridTituloRendaFixaOrig_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" Caption="Título" VisibleIndex="0"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                    Width="60%" />
                                <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Dt.Vencimento"
                                    VisibleIndex="2" Width="5%" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                gridTituloRendaFixaOrig.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataTituloRendaFixaOrig);}"
                                Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Título Original." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridTituloRendaFixaOrig.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <dxcb:ASPxCallback ID="callBackPopupTituloNovo" runat="server" OnCallback="callBackPopupTituloNovo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditTituloRendaFixaNovo.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupTituloRendaFixaNovo" ClientInstanceName="popupTituloRendaFixaNovo"
            runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupCCC_TituloRendaFixaNovo" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridTituloRendaFixaNovo" runat="server" Width="100%" ClientInstanceName="gridTituloRendaFixaNovo"
                            AutoGenerateColumns="False" DataSourceID="EsDSTituloRendaFixaNovo" KeyFieldName="IdTitulo"
                            OnCustomDataCallback="gridTituloRendaFixaNovo_CustomDataCallback" OnCustomCallback="gridTituloRendaFixaNovo_CustomCallback"
                            OnHtmlRowCreated="gridTituloRendaFixaNovo_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" Caption="Título" VisibleIndex="0"
                                    Width="14%" />
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                    Width="60%" />
                                <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Dt.Vencimento"
                                    VisibleIndex="2" Width="5%" />
                            </Columns>
                            <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                gridTituloRendaFixaNovo.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataTituloRendaFixaNovo);}"
                                Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Título Novo." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridTituloRendaFixaNovo.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Repactuação de Título Renda Fixa"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px"></HeaderStyle>
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupCCC_Filtro" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" />
                                                    <div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal8" runat="server" Text="Limpar" />
                                                    <div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton></div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdRepactuacao"
                                        DataSourceID="EsDSRepactuacaoRendaFixa" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdRepactuacao" Caption="Id.Repactuação" ReadOnly="True" VisibleIndex="0" Width="6%">
                                                <EditFormSettings Visible="False" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdTituloOriginal" VisibleIndex="1" Visible="false">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoTituloOriginal" Caption="Título Original"
                                                VisibleIndex="2">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdTituloNovo" VisibleIndex="3" Visible="false">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoTituloNovo" Caption="Título Novo"
                                                VisibleIndex="4">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataEvento" Caption="Dt.Evento" VisibleIndex="5">
                                            </dxwgv:GridViewDataDateColumn>
                                        </Columns>
                                        <SettingsPopup EditForm-Width="250px" />
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenIdTituloNovo" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTituloNovo")%>'
                                                        ClientInstanceName="hiddenIdTituloNovo" />
                                                    <dxe:ASPxTextBox ID="hiddenIdTituloOrig" runat="server" CssClass="hiddenField" Text='<%#Eval("IdTituloOriginal")%>'
                                                        ClientInstanceName="hiddenIdTituloOrig" />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Título Original:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditTituloRendaFixaOrig" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditTituloRendaFixaOrig" ReadOnly="true"
                                                                    Width="380px" Text='<%#(Eval("IdTituloOriginal") != null ? Eval("IdTituloOriginal") + " - " + Eval("DescricaoTituloOriginal") : " ")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupTituloRendaFixaOrig.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Título: Novo" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditTituloRendaFixaNovo" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditTituloRendaFixaNovo" ReadOnly="true"
                                                                    Width="380px" Text='<%#(Eval("IdTituloNovo") != null ? Eval("IdTituloNovo") + " - " + Eval("DescricaoTituloNovo") : " ")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupTituloRendaFixaNovo.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDtEvento" runat="server" CssClass="labelRequired" Text="Dt.Evento:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDtEvento" runat="server" ClientInstanceName="textDtVigencia"
                                                                    Width="150px" Value='<%#Eval("DataEvento")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK+" />
                                                            <div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                            CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" />
                                                            <div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                            CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" />
                                                            <div>
                                                            </div>
                                                        </asp:LinkButton></div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSRepactuacaoRendaFixa" runat="server" OnesSelect="EsDSRepactuacaoRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRendaFixaNovo" runat="server" OnesSelect="EsDSTituloRendaFixaNovo_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRendaFixaOrig" runat="server" OnesSelect="EsDSTituloRendaFixaOrig_esSelect" />
    </form>
</body>
</html>
