﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_TituloRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web"
    TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <title></title>

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;
        var operacao = '';
        var classe = '';

        function OnGetDataEmissor(data) {
            document.getElementById(gridCadastro.cpHiddenIdEmissor).value = data;
            ASPxCallback2.SendCallback(data);
            popupEmissor.HideWindow();
            btnEditEmissor.Focus();
        }
    </script>

    <script type="text/javascript" language="javascript">
        function Uploader_OnUploadComplete(args) {
            if (args.callbackData != '') {
                alert(args.callbackData);
            }
            else {
                alert('Arquivo(s) importado(s) com sucesso.');
                popupAgendaRendaFixa.Hide();
            }
        }

        function CloseGridLookup() {
            debugger;
            gridLookupEmpresaSecuritizada.ConfirmCurrentSelection();
            gridLookupEmpresaSecuritizada.HideDropDown();
            gridLookupEmpresaSecuritizada.Focus();
        }

        function DesabilitaCamposConformeClasse() {
            if (classe == '2000') //Debenture
            {
                dropDebentureConversivel.SetEnabled(true);
                dropDebentureInfra.SetEnabled(true);
            }
            else {
                dropDebentureConversivel.SetEnabled(false);
                dropDebentureInfra.SetEnabled(false);
            }
        }

        function HabilitaAbaOpcaoEmbutida(HabilitaOpcaoEmbutida) {
            tabCadastro.GetTab(2).SetVisible(HabilitaOpcaoEmbutida == 'S');
        }
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }    
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '' && e.result != 'Operação feita com sucesso.')
            {
                document.getElementById('hiddenIdClone').value = e.result;
                alert('Clonagem feita com sucesso. O novo título já foi criado, caso não queira realizar mais clonagens, basta clicar no botão Cancelar.');                
            }
            else
            {
                //document.getElementById('hiddenIdClone').value = '';
                alert('Operação realizada com sucesso.');
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditEmissor.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackExcelImplantacao" runat="server" OnCallback="callbackExcelImplantacao_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                uplAgendaEventos.UploadFile();
            }
        }
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackLote" runat="server" OnCallback="callBackLote_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanelLote.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.') {
                     
                    popupLote.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPapel" runat="server" OnCallback="callBackPapel_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                        
            if (e.result != '') 
            {
                var resultSplit = e.result.split('|');
                
                drop_eJuros.SetValue(resultSplit[0]);
                drop_eJurosTipo.SetValue(resultSplit[1]);
                dropIsentoIR.SetValue(resultSplit[2]);
                classe = resultSplit[3];
                DesabilitaCamposConformeClasse();
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackDesabilitaCamposPorClasse" runat="server" OnCallback="callBackPapel_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                        
            if (e.result != '') 
            {
                var resultSplit = e.result.split('|');
                classe = resultSplit[3];
                DesabilitaCamposConformeClasse();
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackDesabilitaCamposPorClasseLote" runat="server" OnCallback="callBackPapelLote_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                        
            if (e.result != '') 
            {
                var classeLote = e.result;
                
                if(classeLote == '2000') //Debenture
                {
                    dropDebentureConversivelLote.SetEnabled(true);
                    dropDebentureInfraLote.SetEnabled(true);
                }
                else
                {
                    dropDebentureConversivelLote.SetEnabled(false);
                    dropDebentureInfraLote.SetEnabled(false);
                    dropDebentureConversivelLote.SetSelectedIndex(1);
                    dropDebentureInfraLote.SetSelectedIndex(1);
                }      
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <asp:HiddenField ID="hiddenIdClone" runat="server" />
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Títulos de Renda Fixa"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupAgendaRendaFixa" AllowDragging="true" PopupElementID="popupAgendaRendaFixa"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="350" Left="350" Top="50" HeaderText="Carregar Agenda Renda Fixa"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table border="0" width="350">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="label37" runat="server" CssClass="labelBold" Text="Agenda de Eventos (Renda Fixa)" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="line-height: 32px;">
                                                            <dxuc:ASPxUploadControl ID="uplAgendaEventos" runat="server" ClientInstanceName="uplAgendaEventos"
                                                                OnFileUploadComplete="uplAgendaEventos_FileUploadComplete" Width="200px" Size="25"
                                                                FileUploadMode="OnPageLoad" EnableDefaultAppearance="False">
                                                                <ValidationSettings MaxFileSize="100000000" />
                                                                <ClientSideEvents FileUploadComplete="function(s, e) { Uploader_OnUploadComplete(e);  }" />
                                                            </dxuc:ASPxUploadControl>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-left: 40px; margin-top: 20px">
                                                <asp:LinkButton ID="btnRunExcelImplantacao" runat="server" Font-Overline="false"
                                                    CssClass="btnRun" OnClientClick="
                                    {
                                        if(uplAgendaEventos.GetText()== '') {
                                            alert('Entre com o arquivo de Agenda Renda Fixa!');                                                                            
                                        }
                                        else {                                                                    
                                            callbackExcelImplantacao.SendCallback();                                                                             
                                        }
                                        return false;
                                    }
                                    ">
                                                    <asp:Literal ID="Literal11" runat="server" Text="Importar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnLimparExcelImplantacao" runat="server" Font-Overline="false"
                                                    CssClass="btnClear" OnClientClick="
                                    {
                                        uplAgendaEventos.ClearText();
                                        return false;
                                    }
                                    ">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupLote" AllowDragging="true" PopupElementID="popupLote"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="500" Left="250" Top="70" HeaderText="Alteração em Lote"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                                            <table border="0" cellspacing="2" cellpadding="2">
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelPapelLote" runat="server" CssClass="labelNormal" Text="Papel:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropPapelLote" runat="server" DataSourceID="EsDSPapelRendaFixa"
                                                            ClientInstanceName="dropPapelLote" ValueField="IdPapel" TextField="Descricao"
                                                            EnableClientSideAPI="true" CssClass="dropDownListCurto">
                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) {callBackDesabilitaCamposPorClasseLote.SendCallback(); }" />
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelEstrategiaLote" runat="server" CssClass="labelNormal" Text="Estratégia:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropEstrategiaLote" runat="server" ClientInstanceName="dropEstrategiaLote"
                                                            DataSourceID="EsDSEstrategia" ShowShadow="False" DropDownStyle="DropDown" CssClass="dropDownListCurto"
                                                            TextField="Descricao" ValueField="IdEstrategia">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelIsentoIRLote" runat="server" CssClass="labelNormal" Text="Isento IR:" />
                                                    </td>
                                                    <td class="td_Label">
                                                        <dxe:ASPxComboBox ID="dropIsentoIRLote" runat="server" CssClass="dropDownListCurto"
                                                            ClientInstanceName="dropIsentoIRLote">
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Não Isento" />
                                                                <dxe:ListEditItem Value="2" Text="Isento PF" />
                                                                <dxe:ListEditItem Value="3" Text="Isento" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelIsentoIOFLote" runat="server" CssClass="labelNormal" Text="Isento IOF:" />
                                                    </td>
                                                    <td class="td_Label">
                                                        <dxe:ASPxComboBox ID="dropIsentoIOFLote" runat="server" CssClass="dropDownListCurto"
                                                            ClientInstanceName="dropIsentoIOFLote">
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Não Isento" />
                                                                <dxe:ListEditItem Value="2" Text="Isento PF" />
                                                                <dxe:ListEditItem Value="3" Text="Isento" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelEmissorLote" runat="server" CssClass="labelNormal" Text="Emissor:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dx:ASPxGridLookup ID="dropEmissorLote" ClientInstanceName="dropEmissorLote" runat="server"
                                                            KeyFieldName="IdEmissor" DataSourceID="EsDSEmissor" Width="250px" TextFormatString="{0} - {1}"
                                                            Font-Size="11px" AllowUserInput="false">
                                                            <Columns>
                                                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="4%" />
                                                                <dxwgv:GridViewDataColumn FieldName="IdEmissor" Caption="Id" Width="7%" CellStyle-Font-Size="X-Small" />
                                                                <dxwgv:GridViewDataColumn FieldName="Nome" CellStyle-Font-Size="X-Small" Width="90%"
                                                                    CellStyle-Wrap="false" />
                                                            </Columns>
                                                            <GridViewProperties>
                                                                <Settings ShowFilterRow="True" />
                                                            </GridViewProperties>
                                                        </dx:ASPxGridLookup>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelTaxaLote" runat="server" AssociatedControlID="textTaxaLote" CssClass="labelNormal"
                                                            Text="Tx Juros:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="textTaxaLote" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxaLote"
                                                            MaxLength="8" NumberType="Float" DecimalPlaces="4">
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelIndiceLote" runat="server" AssociatedControlID="dropIndiceLote"
                                                            CssClass="labelNormal" Text="Índice:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropIndiceLote" ClientInstanceName="dropIndiceLote" runat="server"
                                                            DataSourceID="EsDSIndice" ValueField="IdIndice" TextField="Descricao" CssClass="dropDownList">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelPercentualLote" runat="server" AssociatedControlID="textPercentualLote"
                                                            CssClass="labelNormal" Text="% Índice:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="textPercentualLote" runat="server" CssClass="textPercCurto"
                                                            ClientInstanceName="textPercentualLote" MaxLength="8" NumberType="Float" DecimalPlaces="4">
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelPUNominalLote" runat="server" AssociatedControlID="textPUNominalLote"
                                                            CssClass="labelNormal" Text="PU Nominal:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="textPUNominalLote" runat="server" CssClass="textValor_5" ClientInstanceName="textPUNominalLote"
                                                            MaxLength="16" NumberType="Float" DecimalPlaces="8">
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelDebConversivelLote" runat="server" CssClass="labelNormal" Text="Deb.Conversível:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <table border="0">
                                                            <tr>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropDebentureConversivelLote" ClientInstanceName="dropDebentureConversivelLote"
                                                                        ClientEnabled="false" runat="server" CssClass="dropDownListCurto_6">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDebentureInfraLote" runat="server" CssClass="labelNormal" Text="Deb.Infra:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropDebentureInfraLote" ClientInstanceName="dropDebentureInfraLote"
                                                                        ClientEnabled="false" runat="server" CssClass="dropDownListCurto_6">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelMoedaLote" runat="server" CssClass="labelNormal" Text="Moeda:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropMoedaLote" ClientInstanceName="dropMoedaLote" runat="server"
                                                            DataSourceID="EsDSMoeda" ValueField="IdMoeda" TextField="Nome" CssClass="dropDownListCurto">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelPeriodicidadeLote" runat="server" CssClass="labelNormal" Text="Periodicidade:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropPeriodicidadeLote" ClientInstanceName="dropPeriodicidadeLote"
                                                            runat="server" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_1"
                                                            OnLoad="dropPeriodicidade_OnLoad" />
                                                    </td>


                                                    <td class="td_Label">
                                                        <asp:Label ID="labelCriterioAmortizacaoLote" runat="server" CssClass="labelNormal"
                                                            Text="Critério Amortização:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropCriterioAmortizacaoLote" ClientInstanceName="dropCriterioAmortizacaoLote"
                                                            runat="server" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_1"
                                                            OnLoad="dropCriterioAmortizacao_OnLoad" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="lblExecutaProRataEmissaoLote" runat="server" CssClass="labelNormal"
                                                            Text="Executa Pro Rata Emissão:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropExecutaProRataEmissaoLote" ClientInstanceName="dropExecutaProRataEmissaoLote"
                                                            runat="server" CssClass="dropDownListCurto_1">
                                                            <Items>
                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="lblTipoProRataLote" runat="server" CssClass="labelNormal" Text="Tipo Pro Rata:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropTipoProRataLote" ClientInstanceName="dropTipoProRataLote"
                                                            runat="server" CssClass="dropDownListCurto_1">
                                                            <Items>
                                                                <dxe:ListEditItem Value="0" Text="Dias úteis" />
                                                                <dxe:ListEditItem Value="1" Text="Dias corridos" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelPermiteRepactuacaoLote" runat="server" CssClass="labelNormal"
                                                            Text="Permite Repactuação:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropPermiteRepactuacaoLote" ClientInstanceName="dropPermiteRepactuacaoLote"
                                                            runat="server" CssClass="dropDownListCurto_6">
                                                            <Items>
                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelExecutaProRataLiquidacaoLote" runat="server" CssClass="labelNormal"
                                                            Text="Executa Pro rata Liquidação:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropExecutaProRataLiquidacaoLote" ClientInstanceName="dropExecutaProRataLiquidacaoLote"
                                                            runat="server" CssClass="dropDownListCurto_6">
                                                            <Items>
                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="lblProRataEmissaoDiaLote" runat="server" CssClass="labelNormal" Text="Pro Rata Emissão Dia:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="textProRataEmissaoDiaLote" ClientInstanceName="textProRataEmissaoDiaLote"
                                                            runat="server" CssClass="textCurto" MaxLength="6" NumberType="Integer" />
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="lblAtivoRegraLote" runat="server" CssClass="labelNormal" Text="Ativo Regra:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropAtivoRegraLote" ClientInstanceName="dropAtivoRegraLote"
                                                            runat="server" CssClass="dropDownListCurto_6">
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Sem regra específica" />
                                                                <dxe:ListEditItem Value="2000" Text="Tipo 1 (2000)" />
                                                                <dxe:ListEditItem Value="3000" Text="Tipo 2 (3000)" />
                                                                <dxe:ListEditItem Value="3001" Text="Tipo 3 (3001)" />
                                                                <dxe:ListEditItem Value="1000" Text="Tipo 4 (1000)" />
                                                                <dxe:ListEditItem Value="4000" Text="Tipo 5 (4000)" />
                                                                <dxe:ListEditItem Value="4001" Text="Tipo 6 (4001)" />
                                                                <dxe:ListEditItem Value="6000" Text="Tipo 7 (6000)" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="lblDefasagemLiquidacaoLote" runat="server" CssClass="labelNormal"
                                                            Text="Defasagem Liquidação:" />
                                                    </td>
                                                    <td colspan="2">
                                                        <dxe:ASPxSpinEdit ID="textDefasagemLiquidacaoLote" runat="server" Width="150px" ClientInstanceName="textDefasagemLiquidacaoLote"
                                                            NumberType="Integer">
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="lbl_eJurosLote" runat="server" CssClass="labelNormal" Text="Tipo de Calendário e cálculo de Juros:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="drop_eJurosLote" ClientInstanceName="drop_eJurosLote" runat="server"
                                                            ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_1">
                                                            <Items>
                                                                <dxe:ListEditItem Text="Sem Juros" Value="0" />
                                                                <dxe:ListEditItem Text="Fixo" Value="1" />
                                                                <dxe:ListEditItem Text="Dias Corridos" Value="2" />
                                                                <dxe:ListEditItem Text="Dias Corridos 360" Value="4" />
                                                                <dxe:ListEditItem Text="Dias Úteis" Value="5" />
                                                                <dxe:ListEditItem Text="Dias Úteis 252" Value="6" />
                                                                <dxe:ListEditItem Text="N12" Value="7" />
                                                                <dxe:ListEditItem Text="Meses 21/252" Value="8" />
                                                                <dxe:ListEditItem Text="Meses 30/360" Value="9" />
                                                                <dxe:ListEditItem Text="Meses 30/365" Value="10" />
                                                                <dxe:ListEditItem Text="USA 30/360" Value="11" />
                                                                <dxe:ListEditItem Text="EUR 30/360" Value="12" />
                                                                <dxe:ListEditItem Text="Libor" Value="13" />
                                                                <dxe:ListEditItem Text="Ano Pro Rata Mês" Value="14" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="lblDefasagemMesesLote" runat="server" CssClass="labelNormal" Text="Defasagem Meses:" />
                                                    </td>
                                                    <td colspan="2">
                                                        <dxe:ASPxSpinEdit ID="textDefasagemMesesLote" runat="server" Width="150px" ClientInstanceName="textDefasagemMesesLote"
                                                            NumberType="Integer">
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="lblJurosTipoLote" runat="server" CssClass="labelNormal" Text="Juros Tipo:" />
                                                    </td>
                                                    <td colspan="2">
                                                        <dxe:ASPxComboBox ID="drop_eJurosTipoLote" ClientInstanceName="drop_eJurosTipoLote"
                                                            runat="server" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_1">
                                                            <Items>
                                                                <dxe:ListEditItem Text="Simples" Value="-1" />
                                                                <dxe:ListEditItem Text="Composto" Value="0" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnProcessaLote" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="if (confirm('Tem certeza que quer realizar a atualização em lote?')==true) {                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                                                        LoadingPanelLote.Show();
                                                                        callBackLote.SendCallback();                                                                        
                                                                   }
                                                                   return false;
                                                                   ">
                                                    <asp:Literal ID="Literal15" runat="server" Text="Processa Atualização" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnLote" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="
                                            dropPapelLote.SetSelectedIndex(-1);
                                            dropEstrategiaLote.SetSelectedIndex(-1);
                                            dropIsentoIRLote.SetSelectedIndex(-1);
                                            dropIsentoIOFLote.SetSelectedIndex(-1);
                                            dropEmissorLote.SetText('');
                                            dropEmissorLote.SetValue('');
                                            textTaxaLote.SetText('');
                                            dropIndiceLote.SetSelectedIndex(-1);
                                            textPercentualLote.SetText('');
                                            textPUNominalLote.SetText('');
                                            dropDebentureConversivelLote.SetSelectedIndex(1);
                                            dropDebentureInfraLote.SetSelectedIndex(1);
                                            dropDebentureConversivelLote.SetEnabled(false);
                                            dropDebentureInfraLote.SetEnabled(false);
                                            dropMoedaLote.SetSelectedIndex(-1);
                                            dropPeriodicidadeLote.SetSelectedIndex(-1);
                                            dropCriterioAmortizacaoLote.SetSelectedIndex(-1);
                                            dropExecutaProRataEmissaoLote.SetSelectedIndex(-1);
                                            dropTipoProRataLote.SetSelectedIndex(-1);
                                            dropPermiteRepactuacaoLote.SetSelectedIndex(-1);
                                            dropExecutaProRataLiquidacaoLote.SetSelectedIndex(-1);
                                            textProRataEmissaoDiaLote.SetText('');
                                            dropAtivoRegraLote.SetSelectedIndex(-1);
                                            textDefasagemLiquidacaoLote.SetText('');
                                            drop_eJurosLote.SetSelectedIndex(-1);
                                            textDefasagemMesesLote.SetText('');
                                            drop_eJurosTipoLote.SetSelectedIndex(-1);
                                        popupLote.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal8" runat="server" Text="Altera em Lote" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnAgendaRendaFixa" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="popupAgendaRendaFixa.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Agenda Renda Fixa" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" ClientInstanceName="gridCadastro" runat="server"
                                        EnableCallBacks="true" KeyFieldName="IdTitulo" DataSourceID="EsDSTituloRendaFixa"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnInitNewRow="gridCadastro_InitNewRow" OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll" />
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdTitulo" Caption="Id Título" Width="7%"
                                                VisibleIndex="1" CellStyle-HorizontalAlign="Left" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdPapel" Caption="Papel" VisibleIndex="2"
                                                Width="12%">
                                                <PropertiesComboBox DataSourceID="EsDSPapelRendaFixa" TextField="Descricao" ValueField="IdPapel">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="15%"
                                                VisibleIndex="3" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdEmissor" Caption="Emissor" VisibleIndex="4"
                                                Width="10%">
                                                <PropertiesComboBox DataSourceID="EsDSEmissorInner" TextField="Nome" ValueField="IdEmissor">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IdIndice" Caption="Índice" VisibleIndex="6"
                                                Width="8%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice">
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Percentual" Caption="% Índice" VisibleIndex="8"
                                                Width="6%">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Taxa" VisibleIndex="10" Width="6%">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="PUNominal" VisibleIndex="12" Width="10%">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}" />
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataEmissao" Caption="Emissão" VisibleIndex="14"
                                                Width="10%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" VisibleIndex="16"
                                                Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="NomeEmissor" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorNominal" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoCustodia" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdMoeda" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdEstrategia" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoInterface" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="PermiteRepactuacao" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="PapelIsentoIR" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IsentoIR" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DefasagemLiquidacao" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="Participacao" Visible="false" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxtc:ASPxPageControl ClientInstanceName="tabCadastro" ID="tabCadastro" runat="server"
                                                        ActiveTabIndex="0" TabSpacing="0px" TabAlign="Center" TabPosition="Top" Height="210px">
                                                        <TabPages>
                                                            <dxtc:TabPage Text="Informações Básicas">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelIdTitulo" runat="server" AssociatedControlID="textIdTitulo" OnLoad="labelIdTitulo_Load"
                                                                                        CssClass="labelNormal" Text="Id Título:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textIdTitulo" runat="server" CssClass="textValor_5" NumberType="Integer"
                                                                                        MaxLength="10" OnLoad="textIdTitulo_Load" Text='<%#Eval("IdTitulo")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelDescricao" runat="server" AssociatedControlID="textDescricao"
                                                                                        CssClass="labelRequired" Text="Descrição:">
                                                                                    </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textDescricao" ClientInstanceName="textDescricao" runat="server"
                                                                                        CssClass="textNormal5" MaxLength="100" Text='<%# Eval("Descricao") %>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelPapel" runat="server" AssociatedControlID="dropPapel" CssClass="labelRequired"
                                                                                        Text="Papel:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropPapel" runat="server" DataSourceID="EsDSPapelRendaFixa"
                                                                                        ValueField="IdPapel" TextField="Descricao" CssClass="dropDownListCurto" Text='<%#Eval("IdPapel")%>'>
                                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {callBackPapel.SendCallback(); }"
                                                                                            Init="function(s, e) {callBackDesabilitaCamposPorClasse.SendCallback(); }" />
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelDataEmissao" runat="server" CssClass="labelRequired" Text="Emissão:" /></td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataEmissao" runat="server" ClientInstanceName="textDataEmissao"
                                                                                        Value='<%#Eval("DataEmissao")%>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelDataVencimento" runat="server" CssClass="labelRequired" Text="Vencimento:" /></td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento"
                                                                                        Value='<%#Eval("DataVencimento")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelIsentoIR" runat="server" Text="Isento IR:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropIsentoIR" runat="server" ClientInstanceName="dropIsentoIR"
                                                                                        CssClass="dropDownListCurto" OnDataBound="dropIsentoIR_OnLoad" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Isento IOF:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropIsentoIOF" runat="server" CssClass="dropDownListCurto"
                                                                                        Text='<%#Eval("IsentoIOF")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Não Isento" />
                                                                                            <dxe:ListEditItem Value="2" Text="Isento PF" />
                                                                                            <dxe:ListEditItem Value="3" Text="Isento" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelEmissor" runat="server" CssClass="labelRequired" Text="Emissor:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="hiddenIdEmissor" runat="server" CssClass="hiddenField" Text='<%#Eval("IdEmissor")%>' />
                                                                                    <dxe:ASPxButtonEdit ID="btnEditEmissor" runat="server" CssClass="textButtonEdit5"
                                                                                        ClientInstanceName="btnEditEmissor" ReadOnly="true" Text='<%#Eval("NomeEmissor")%>'>
                                                                                        <Buttons>
                                                                                            <dxe:EditButton />
                                                                                        </Buttons>
                                                                                        <ClientSideEvents ButtonClick="function(s, e) {popupEmissor.ShowAtElementByID(s.name);}" />
                                                                                    </dxe:ASPxButtonEdit>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelTaxa" runat="server" AssociatedControlID="textTaxa" CssClass="labelNormal"
                                                                                        Text="Tx Juros:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textTaxa" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxa"
                                                                                        MaxLength="8" NumberType="Float" DecimalPlaces="4" Text='<%#Eval("Taxa")%>'>
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelIndice" runat="server" AssociatedControlID="dropIndice" CssClass="labelNormal"
                                                                                        Text="Índice:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropIndice" runat="server" DataSourceID="EsDSIndice" ValueField="IdIndice"
                                                                                        TextField="Descricao" CssClass="dropDownList" Text='<%#Eval("IdIndice")%>'>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelPercentual" runat="server" AssociatedControlID="textPercentual"
                                                                                        CssClass="labelNormal" Text="% Índice:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textPercentual" runat="server" CssClass="textPercCurto" ClientInstanceName="textPercentual"
                                                                                        MaxLength="8" NumberType="Float" DecimalPlaces="4" Text='<%#Eval("Percentual")%>'>
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelPUNominal" runat="server" AssociatedControlID="textPUNominal"
                                                                                        CssClass="labelRequired" Text="PU Nominal:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textPUNominal" runat="server" CssClass="textValor_5" ClientInstanceName="textPUNominal"
                                                                                        MaxLength="16" NumberType="Float" DecimalPlaces="8" Text='<%#Eval("PUNominal")%>'>
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label8" runat="server" CssClass="labelRequired" Text="Deb.Conversível:" />
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <table border="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <dxe:ASPxComboBox ID="dropDebentureConversivel" ClientInstanceName="dropDebentureConversivel" ClientEnabled="false"
                                                                                                    runat="server" CssClass="dropDownListCurto_6" Text='<%#Eval("DebentureConversivel")%>'>
                                                                                                    <Items>
                                                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                                                    </Items>
                                                                                                </dxe:ASPxComboBox>
                                                                                            </td>
                                                                                            <td class="td_Label">
                                                                                                <asp:Label ID="label10" runat="server" CssClass="labelRequired" Text="Deb.Infra:" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <dxe:ASPxComboBox ID="dropDebentureInfra" ClientInstanceName="dropDebentureInfra" ClientEnabled="false"
                                                                                                    runat="server" CssClass="dropDownListCurto_6" Text='<%#Eval("DebentureInfra")%>'>
                                                                                                    <Items>
                                                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                                                    </Items>
                                                                                                </dxe:ASPxComboBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelMoeda" runat="server" CssClass="labelRequired" Text="Moeda:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropMoeda" runat="server" DataSourceID="EsDSMoeda" ValueField="IdMoeda"
                                                                                        TextField="Nome" CssClass="dropDownListCurto" Text='<%#Eval("IdMoeda")%>'>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCodigoCustodia" runat="server" AssociatedControlID="textCodigoCustodia"
                                                                                        CssClass="labelNormal" Text="Cód. Anbima:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCodigoCustodia" runat="server" CssClass="textValor_5" MaxLength="10"
                                                                                        Text='<%#Eval("CodigoCustodia")%>'></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <div class="linkButton linkButtonNoBorder">
                                                                                        <asp:LinkButton ID="btnClone" runat="server" Font-Overline="false" CssClass="btnCopy"
                                                                                            OnClientClick="callbackAdd.SendCallback('clone'); return false;" OnInit="btnClone_Init">
                                                                                            <asp:Literal ID="Literal8" runat="server" Text="Clonar" /><div>
                                                                                            </div>
                                                                                        </asp:LinkButton>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label6" runat="server" AssociatedControlID="textCodigoCBLC" CssClass="labelNormal"
                                                                                        Text="Cód. CBLC:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCodigoCBLC" runat="server" CssClass="textValor_5" MaxLength="10"
                                                                                        Text='<%#Eval("CodigoCBLC")%>'></asp:TextBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label7" runat="server" AssociatedControlID="textCodigoCetip" CssClass="labelNormal"
                                                                                        Text="Cód. Cetip:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCodigoCetip" runat="server" CssClass="textValor_5" MaxLength="14"
                                                                                        Text='<%#Eval("CodigoCetip")%>'></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label5" runat="server" AssociatedControlID="textCodigoISIN" CssClass="labelNormal"
                                                                                        Text="Cód. ISIN:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCodigoISIN" runat="server" CssClass="textValor_5" MaxLength="12"
                                                                                        Text='<%#Eval("CodigoISIN")%>'></asp:TextBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Estratégia:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropEstrategia" runat="server" ClientInstanceName="dropEstrategia"
                                                                                        DataSourceID="EsDSEstrategia" ShowShadow="False" DropDownStyle="DropDown" CssClass="dropDownListCurto"
                                                                                        TextField="Descricao" ValueField="IdEstrategia" Text='<%# Eval("IdEstrategia") %>'
                                                                                        ValueType="System.String">
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCodigoCusip" runat="server" CssClass="labelNormal" Text="Cód. CUSIP:" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCodigoCusip" runat="server" CssClass="textValor_5" MaxLength="9"
                                                                                        Text='<%#Eval("CodigoCusip")%>'></asp:TextBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelPeriodicidade" runat="server" CssClass="labelNormal" Text="Periodicidade:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropPeriodicidade" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                                        Text='<%#Eval("Periodicidade")%>' CssClass="dropDownListCurto_1" OnLoad="dropPeriodicidade_OnLoad" />
                                                                                </td>



                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCriterioAmortizacao" runat="server" CssClass="labelRequired"
                                                                                        Text="Critério Amortização:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropCriterioAmortizacao" runat="server" ShowShadow="false"
                                                                                        DropDownStyle="DropDownList" Text='<%#Eval("CriterioAmortizacao")%>' CssClass="dropDownListCurto_1"
                                                                                        OnLoad="dropCriterioAmortizacao_OnLoad" />
                                                                                </td>

                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelParticipacao" runat="server" CssClass="labelNormal" Text="Participação:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxCheckBox ID="chkParticipacao" runat="server" Text="" />
                                                                                </td>


                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Prêmio Rebate:" Visible="false" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textPremioRebate" ClientInstanceName="textPremioRebate" runat="server" Visible="false"
                                                                                        CssClass="textValor_5" MaxLength="6" Text='<%#Eval("PremioRebate")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblOpcaoEmbutida" runat="server" CssClass="labelRequired" Text="Opção Embutida:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropOpcaoEmbutida" ClientInstanceName="dropOpcaoEmbutida" runat="server"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" Text='<%#Eval("OpcaoEmbutida")%>'
                                                                                        CssClass="dropDownListCurto_1">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Text="Sim" Value="S" />
                                                                                            <dxe:ListEditItem Text="Não" Value="N" />
                                                                                        </Items>
                                                                                        <ClientSideEvents Init="function(s, e) { if(s.GetSelectedItem() != null) HabilitaAbaOpcaoEmbutida(s.GetSelectedItem().value); }"
                                                                                            SelectedIndexChanged="function(s, e) { HabilitaAbaOpcaoEmbutida(s.GetSelectedItem().value); }" />
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                            <dxtc:TabPage Text="Informações Adicionais">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblExecutaProRataEmissao" runat="server" CssClass="labelNormal" Text="Executa Pro Rata Emissão:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropExecutaProRataEmissao" runat="server" CssClass="dropDownListCurto_1"
                                                                                        Text='<%#Eval("ExecutaProRataEmissao")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblTipoProRata" runat="server" CssClass="labelNormal" Text="Tipo Pro Rata:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoProRata" runat="server" CssClass="dropDownListCurto_1"
                                                                                        Text='<%#Eval("TipoProRata")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="0" Text="Dias úteis" />
                                                                                            <dxe:ListEditItem Value="1" Text="Dias corridos" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Permite Repactuação:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropPermiteRepactuacao" runat="server" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("PermiteRepactuacao")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label9" runat="server" AssociatedControlID="textCodigoInterface" CssClass="labelNormal"
                                                                                        Text="Cód.Interface:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textCodigoInterface" ClientInstanceName="textCodigoInterface"
                                                                                        runat="server" MaxLength="40" Text='<%#Eval("CodigoInterface")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Data Inicio Correção:" /></td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataInicioCorrecao" runat="server" ClientInstanceName="textDataInicioCorrecao"
                                                                                        Value='<%#Eval("DataInicioCorrecao")%>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblExecutaProRataLiquidacao" runat="server" CssClass="labelNormal"
                                                                                        Text="Executa Pro Rata Liquidação:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropExecutaProRataLiquidacao" runat="server" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("ProRataLiquidacao")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblProRataEmissaoDia" runat="server" CssClass="labelNormal" Text="Pro Rata Emissão Dia:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textProRataEmissaoDia" ClientInstanceName="textProRataEmissaoDia"
                                                                                        runat="server" CssClass="textCurto" MaxLength="6" Text='<%#Eval("ProRataEmissaoDia")%>'
                                                                                        NumberType="Integer" />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblAtivoRegra" runat="server" CssClass="labelNormal" Text="Ativo Regra:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropAtivoRegra" runat="server" CssClass="dropDownList"
                                                                                        Text='<%#Eval("AtivoRegra")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Sem regra específica" />
                                                                                            <dxe:ListEditItem Value="2000" Text="Tipo 1 (2000)" />
                                                                                            <dxe:ListEditItem Value="3000" Text="Tipo 2 (3000)" />
                                                                                            <dxe:ListEditItem Value="3001" Text="Tipo 3 (3001)" />
                                                                                            <dxe:ListEditItem Value="1000" Text="Tipo 4 (1000)" />
                                                                                            <dxe:ListEditItem Value="4000" Text="Tipo 5 (4000)" />
                                                                                            <dxe:ListEditItem Value="4001" Text="Tipo 6 (4001)" />
                                                                                            <dxe:ListEditItem Value="6000" Text="Tipo 7 (6000)" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelEmpresaSecuritizada" runat="server" CssClass="labelNormal" Text="Emp. Securitizada:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dx:ASPxGridLookup ID="gridLookupEmpresaSecuritizada" runat="server" SelectionMode="Multiple"
                                                                                        DataSourceID="EsDSAgenteMercado" ClientInstanceName="gridLookupEmpresaSecuritizada"
                                                                                        KeyFieldName="IdAgente" Width="200px" TextFormatString="{1}" MultiTextSeparator="| "
                                                                                        AllowUserInput="false" OnDataBound="gridLookupEmpresaSecuritizada_OnDataBound">
                                                                                        <Columns>
                                                                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                                                            <dxwgv:GridViewDataColumn FieldName="IdAgente" />
                                                                                            <dxwgv:GridViewDataColumn FieldName="Nome" />
                                                                                        </Columns>
                                                                                        <GridViewProperties>
                                                                                            <Templates>
                                                                                                <StatusBar>
                                                                                                    <table class="OptionsTable" style="float: right">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <dxe:ASPxButton ID="Close" runat="server" AutoPostBack="false" Text="Close" ClientSideEvents-Click="CloseGridLookup" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </StatusBar>
                                                                                            </Templates>
                                                                                            <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                                        </GridViewProperties>
                                                                                    </dx:ASPxGridLookup>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblDefasagemLiquidacao" runat="server" CssClass="labelNormal" Text="Defasagem Liquidação:" />
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <dxe:ASPxSpinEdit ID="textDefasagemLiquidacao" runat="server" Width="150px" ClientInstanceName="textDefasagemLiquidacao"
                                                                                        NumberType="Integer" Text='<%#Eval("DefasagemLiquidacao")%>'>
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lbl_eJuros" runat="server" CssClass="labelNormal" Text="Tipo de Calendário e cálculo de Juros:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="drop_eJuros" ClientInstanceName="drop_eJuros" runat="server"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" Text='<%#Eval("EJuros")%>' CssClass="dropDownListCurto_1">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Text="Sem Juros" Value="0" />
                                                                                            <dxe:ListEditItem Text="Fixo" Value="1" />
                                                                                            <dxe:ListEditItem Text="Dias Corridos" Value="2" />
                                                                                            <dxe:ListEditItem Text="Dias Corridos 360" Value="4" />
                                                                                            <dxe:ListEditItem Text="Dias Úteis" Value="5" />
                                                                                            <dxe:ListEditItem Text="Dias Úteis 252" Value="6" />
                                                                                            <dxe:ListEditItem Text="N12" Value="7" />
                                                                                            <dxe:ListEditItem Text="Meses 21/252" Value="8" />
                                                                                            <dxe:ListEditItem Text="Meses 30/360" Value="9" />
                                                                                            <dxe:ListEditItem Text="Meses 30/365" Value="10" />
                                                                                            <dxe:ListEditItem Text="USA 30/360" Value="11" />
                                                                                            <dxe:ListEditItem Text="EUR 30/360" Value="12" />
                                                                                            <dxe:ListEditItem Text="Libor" Value="13" />
                                                                                            <dxe:ListEditItem Text="Ano Pro Rata Mês" Value="14" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblDefasagemMeses" runat="server" CssClass="labelNormal" Text="Defasagem Meses:" />
                                                                                </td>
                                                                                <td colspan="2">
                                                                                    <dxe:ASPxSpinEdit ID="textDefasagemMeses" runat="server" Width="150px" ClientInstanceName="textDefasagemMeses"
                                                                                        NumberType="Integer" Text='<%#Eval("DefasagemMeses")%>'>
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblJurosTipo" runat="server" CssClass="labelNormal" Text="Juros Tipo:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="drop_eJurosTipo" ClientInstanceName="drop_eJurosTipo" runat="server"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" Text='<%#Eval("EJurosTipo")%>'
                                                                                        CssClass="dropDownListCurto_1">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Text="Simples" Value="-1" />
                                                                                            <dxe:ListEditItem Text="Composto" Value="0" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lbl_eTaxa" runat="server" CssClass="labelNormal" Text="Tipo Taxa MTM:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="drop_eTaxa" ClientInstanceName="drop_eTaxa" runat="server"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" Text='<%#Eval("ETaxa")%>'
                                                                                        CssClass="dropDownListCurto_1">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Text="Ano360" Value="1" />
                                                                                            <dxe:ListEditItem Text="Ano365" Value="2" />
                                                                                            <dxe:ListEditItem Text="Efetiva" Value="3" />
                                                                                            <dxe:ListEditItem Text="Linear360" Value="4" />
                                                                                            <dxe:ListEditItem Text="OverAno" Value="5" />
                                                                                            <dxe:ListEditItem Text="OverMes" Value="6" />
                                                                                            <dxe:ListEditItem Text="YTM_EUR" Value="7" />
                                                                                            <dxe:ListEditItem Text="YTM_USA" Value="8" />
                                                                                            <dxe:ListEditItem Text="Libor" Value="9" />
                                                                                            <dxe:ListEditItem Text="Calendar360" Value="10" />
                                                                                            <dxe:ListEditItem Text="Calendar365" Value="11" />
                                                                                            <dxe:ListEditItem Text="Meses30360" Value="12" />
                                                                                            <dxe:ListEditItem Text="Ano360BR" Value="13" />
                                                                                            <dxe:ListEditItem Text="Ano365BR" Value="14" />
                                                                                            <dxe:ListEditItem Text="YTM_365" Value="15" />
                                                                                            <dxe:ListEditItem Text="Continua365" Value="16" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblCodigoCDA" runat="server" CssClass="labelNormal" Text="Código CDA:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textCodigoCDA" runat="server" CssClass="textValor_4" ClientInstanceName="textCodigoCDA"
                                                                                        MaxLength="16" NumberType="Integer" Text="<%#Bind('CodigoCDA')%> ">
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblCodigoBDS" runat="server" CssClass="labelNormal" Text="Código BDS:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textCodigoBDS" runat="server" CssClass="textValor_4" ClientInstanceName="textCodigoBDS"
                                                                                        MaxLength="16" NumberType="Integer" Text="<%#Bind('CodigoBDS')%> ">
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                            <dxtc:TabPage Text="Opções embutidas" ClientVisible="false">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <table border="0">
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblTipoOpcao" runat="server" CssClass="labelRequired" Text="Tipo Opção:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoOpcao" ClientInstanceName="dropTipoOpcao" runat="server"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" Text='<%#Eval("TipoOpcao")%>'
                                                                                        CssClass="dropDownListCurto_1">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Text="Call" Value="1" />
                                                                                            <dxe:ListEditItem Text="Put" Value="2" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelTipoExecicio" runat="server" CssClass="labelRequired" Text="Tipo Exercício:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoExercicio" ClientInstanceName="dropTipoExercicio" runat="server"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" Text='<%#Eval("TipoExercicio")%>'
                                                                                        CssClass="dropDownListCurto_1">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Text="Europeia" Value="1" />
                                                                                            <dxe:ListEditItem Text="Americana" Value="2" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblDtInicioExercicio" runat="server" CssClass="labelRequired" Text="Data Inicio Exercício:" /></td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataInicioExercicio" runat="server" ClientInstanceName="textDataInicioExercicio"
                                                                                        Value='<%#Eval("DataInicioExercicio")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblDataVencimentoOpcao" runat="server" CssClass="labelRequired" Text="Data Vencimento Opção:" /></td>
                                                                                <td>
                                                                                    <dxe:ASPxDateEdit ID="textDataVencimentoOpcao" runat="server" ClientInstanceName="textDataVencimentoOpcao"
                                                                                        Value='<%#Eval("DataVencimentoOpcao")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblTipoPremio" runat="server" CssClass="labelRequired" Text="Tipo Prêmio:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropTipoPremio" ClientInstanceName="dropTipoPremio" runat="server"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" Text='<%#Eval("TipoPremio")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Text="Valor Fixo" Value="1" />
                                                                                            <dxe:ListEditItem Text="Percentual Valor Face" Value="2" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblValorPremio" runat="server" AssociatedControlID="textValorPremio"
                                                                                        CssClass="labelRequired" Text="Valor Prêmio:" />
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="textValorPremio" runat="server" CssClass="textValor_5" ClientInstanceName="textValorPremio"
                                                                                        MaxLength="16" NumberType="Float" DecimalPlaces="8" Text='<%#Eval("ValorPremio")%>'>
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                        </TabPages>
                                                    </dxtc:ASPxPageControl>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; document.getElementById('hiddenIdClone').value = ''; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal7" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="500px" />
                                        <ClientSideEvents BeginCallback="function(s, e) {
		                                        if (e.command == 'CUSTOMCALLBACK') {
                                                    isCustomCallback = true;
                                                }						
                                            }"
                                            EndCallback="function(s, e) {
			                                    if (isCustomCallback) {
                                                    isCustomCallback = false;
                                                    s.Refresh();
                                                }
                                            }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png" />
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxlp:ASPxLoadingPanel ID="LoadingPanelLote" runat="server" Text="Processando, aguarde..."
            ClientInstanceName="LoadingPanelLote" Modal="True" />
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            LeftMargin="50" />
        <cc1:esDataSource ID="EsDSTituloRendaFixa" runat="server" OnesSelect="EsDSTituloRendaFixa_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSPapelRendaFixa" runat="server" OnesSelect="EsDSPapelRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
        <cc1:esDataSource ID="EsDSEmissor" runat="server" OnesSelect="EsDSEmissor_esSelect" />
        <cc1:esDataSource ID="EsDSEmissorInner" runat="server" OnesSelect="EsDSEmissorInner_esSelect" />
        <cc1:esDataSource ID="EsDSSerieRendaFixa" runat="server" OnesSelect="EsDSSerieRendaFixa_esSelect" />
        <cc1:esDataSource ID="EsDSMoeda" runat="server" OnesSelect="EsDSMoeda_esSelect" />
        <cc1:esDataSource ID="EsDSEstrategia" runat="server" OnesSelect="EsDSEstrategia_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
    </form>
</body>
</html>