﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_CotacaoMercadoAndima, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />    
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Cotações de Títulos Públicos (Mercado Anbima)"></asp:Label>
    </div>
        
    <div id="mainContent">
    
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                            PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                            Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                            HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>        
                    <tr>
                    <td>                
                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                    </td>    
                                        
                    <td>
                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                    </td>  
                    
                    <td>
                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                    </td>
                    <td>
                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />                        
                    </td>                                                                            
                    </tr>
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
    
            <div class="linkButton">               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                            KeyFieldName="CompositeKey" DataSourceID="EsDSCotacaoMercadoAndima"
                            OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"                
                            OnRowUpdating="gridCadastro_RowUpdating"
                            OnRowInserting="gridCadastro_RowInserting"
                            OnCustomCallback="gridCadastro_CustomCallback"
                            OnPreRender="gridCadastro_PreRender"   
                            OnCustomJSProperties="gridCadastro_CustomJSProperties"
                            OnBeforeGetCallbackResult="gridCadastro_PreRender" >        
                                
                    <Columns>           
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                                                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Data" VisibleIndex="1" Width="10%"/>                            
                        <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="20%" VisibleIndex="2"/>                        
                        <dxwgv:GridViewDataTextColumn FieldName="CodigoSELIC" Caption="Cód. SELIC" Width="9%" VisibleIndex="3"/>                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataEmissao" Caption="Emissão" Width="10%" VisibleIndex="4"/>                            
                        <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" Width="10%" VisibleIndex="5"/>    
                        
                        <dxwgv:GridViewDataSpinEditColumn FieldName="TaxaIndicativa" Caption="Taxa" Width="18%" VisibleIndex="6" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                        </dxwgv:GridViewDataSpinEditColumn>
                        
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Pu" Caption="PU" Width="18%" VisibleIndex="7" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesSpinEdit>
                        </dxwgv:GridViewDataSpinEditColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"/>
                    </Columns>
                    
                    <Templates>
                        <EditForm>
                                                                                   
                            <div class="editForm">   
                                
                                <table>                    
                                    <tr>                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                        </td>
                                                                
                                        <td>                        
                                            <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>' OnInit="textDataReferencia_Init"/>                                            
                                        </td>  
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">                                        
                                            <asp:Label ID="labelDescricao" runat="server" CssClass="labelRequired" Text="Descrição:"></asp:Label>
                                        </td>
                                        <td >
                                            <asp:TextBox ID="textDescricao" runat="server" CssClass="textNormal" MaxLength="20" Text='<%#Eval("Descricao")%>' OnInit="textDescricao_Init" />
                                        </td>                                                                        
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">                                        
                                            <asp:Label ID="labelCodigoSELIC" runat="server" CssClass="labelRequired" Text="Código:"></asp:Label>
                                        </td>
                                        <td >
                                            <asp:TextBox ID="textCodigoSELIC" runat="server" CssClass="textNormal_5" MaxLength="10" Text='<%#Eval("CodigoSELIC")%>' OnInit="textCodigoSELIC_Init" />
                                        </td>                                                                        
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="labelDataEmissao" runat="server" CssClass="labelRequired" Text="Emissão:"></asp:Label>
                                        </td>
                                                                
                                        <td>                       
                                            <dxe:ASPxDateEdit ID="textDataEmissao" runat="server" ClientInstanceName="textDataEmissao" Value='<%#Eval("DataEmissao")%>' OnInit="textDataEmissao_Init"/>                                         
                                        </td>  
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="labelDataVencimento" runat="server" CssClass="labelRequired" Text="Vcto:"></asp:Label>
                                        </td>
                                                                
                                        <td>                
                                            <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento" Value='<%#Eval("DataVencimento")%>' OnInit="textDataVencimento_Init"/>
                                        </td>  
                                    </tr>
                                    
                                    <tr>
                                        <td  class="td_Label_Curto">
                                            <asp:Label ID="labelTaxaIndicativa" runat="server" CssClass="labelRequired" Text="Taxa:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textTaxaIndicativa" runat="server" CssClass="textValor_5" ClientInstanceName="textTaxaIndicativa"
                                                                             MaxLength="8" NumberType="Float" DecimalPlaces="4" Text="<%#Bind('TaxaIndicativa')%>">                                                                                                        
                                            </dxe:ASPxSpinEdit>
                                        </td>                                         
                                    </tr>      
                                    
                                    <tr>
                                        <td  class="td_Label_Curto">
                                            <asp:Label ID="labelPU" runat="server" CssClass="labelRequired" Text="PU:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textPU" runat="server" CssClass="textValor_5" ClientInstanceName="textPU"
                                                                             MaxLength="16" NumberType="Float" DecimalPlaces="8" Text='<%#Bind("Pu")%>'>                                                                                                        
                                            </dxe:ASPxSpinEdit>
                                        </td>                                         
                                    </tr>
                                </table>
                                
                                <div class="linhaH"></div>
                        
                                <div class="linkButton linkButtonNoBorder popupFooter">
                                    <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"   OnInit="btnOKAdd_Init"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                                    <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                                </div>    
                            </div>
                                                        
                        </EditForm>
                        <StatusBar>
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                        </StatusBar>            
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="230px" />
                                        
                    <Templates>
                    <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                    </StatusBar>            
                    </Templates>
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>                        
                </dxwgv:ASPxGridView>            
            </div>            
    </div>
    </div>
    </td></tr></table>
    </div>
                       
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSCotacaoMercadoAndima" runat="server" OnesSelect="EsDSCotacaoMercadoAndima_esSelect" />
        
    </form>
</body>
</html>