﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_CurvaRendaFixa, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var operacao = '';
        var popup = true; // usada para controlar o refresh após um delete feito no grid
        var isCustomCallback = false;
        document.onkeydown = onDocumentKeyDown;

        function OnGetDataSerieRendaFixa(data) {
            hiddenIdSerie.SetValue(data);
            callBackPopupSerie.SendCallback(data);
            popupSerieRendaFixa.HideWindow();
            btnEditSerieRendaFixa.Focus();
        }

        function OnGetDataCurvaBase(data) {
            hiddenIdCurvaBase.SetValue(data);
            callBackPopupCurvaBase.SendCallback(data);
            popupCurvaBase.HideWindow();
            btnEditCurvaBase.Focus();
        }

        function OnGetDataCurvaSpread(data) {
            hiddenIdCurvaSpread.SetValue(data);
            callBackPopupCurvaSpread.SendCallback(data);
            popupCurvaSpread.HideWindow();
            btnEditCurvaSpread.Focus();
        }

        function OnGetDataIndice(data) {
            hiddenIdIndice.SetValue(data);
            callBackPopupIndice.SendCallback(data);
            popupIndice.HideWindow();
            btnEditIndice.Focus();
        }             
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            
            operacao = '';
        }                
         
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callBackPopupSerie" runat="server" OnCallback="callBackPopupSerie_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditSerieRendaFixa.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupSerieRendaFixa" ClientInstanceName="popupSerieRendaFixa"
        runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridSerieRendaFixa" runat="server" Width="100%" ClientInstanceName="gridSerieRendaFixa"
                        AutoGenerateColumns="False" DataSourceID="EsDSSerieRendaFixa" KeyFieldName="IdSerie"
                        OnCustomDataCallback="gridSerieRendaFixa_CustomDataCallback" OnCustomCallback="gridSerieRendaFixa_CustomCallback"
                        OnHtmlRowCreated="gridSerieRendaFixa_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdSerie" Caption="Série" VisibleIndex="0"
                                Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                Width="60%" />
                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoFeeder" Caption="Feeder" VisibleIndex="2" />
                        </Columns>
                        <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridSerieRendaFixa.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataSerieRendaFixa);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Série." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridSerieRendaFixa.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <dxcb:ASPxCallback ID="callBackPopupIndice" runat="server" OnCallback="callBackPopupIndice_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditIndice.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupIndice" ClientInstanceName="popupIndice" runat="server"
        Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControlIndice" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridIndice" runat="server" Width="100%" ClientInstanceName="gridIndice"
                        AutoGenerateColumns="False" DataSourceID="EsDSIndice" KeyFieldName="IdIndice"
                        OnCustomDataCallback="gridIndice_CustomDataCallback" OnCustomCallback="gridIndice_CustomCallback"
                        OnHtmlRowCreated="gridIndice_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdIndice" Caption="Índice" VisibleIndex="0"
                                Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                Width="60%" />
                        </Columns>
                        <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridIndice.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataIndice);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Índice." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridIndice.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <dxcb:ASPxCallback ID="callBackPopupCurvaBase" runat="server" OnCallback="callBackPopupCurvaBase_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCurvaBase.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupCurvaBase" ClientInstanceName="popupCurvaBase" runat="server"
        Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControlCurvaBase" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridCurvaBase" runat="server" Width="100%" ClientInstanceName="gridCurvaBase"
                        AutoGenerateColumns="False" DataSourceID="EsDSCurvaBase" KeyFieldName="IdCurvaBasePopup"
                        OnCustomDataCallback="gridCurvaBase_CustomDataCallback" OnCustomCallback="gridCurvaBase_CustomCallback"
                        OnHtmlRowCreated="gridCurvaBase_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdCurvaRendaFixa" Caption="Curva Base" VisibleIndex="0"
                                Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                Width="60%" />
                        </Columns>
                        <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridCurvaBase.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataCurvaBase);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Tipo Curva." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCurvaBase.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <dxcb:ASPxCallback ID="callBackPopupCurvaSpread" runat="server" OnCallback="callBackPopupCurvaSpread_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCurvaSpread.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupCurvaSpread" ClientInstanceName="popupCurvaSpread"
        runat="server" Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControlCurvaSpread" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridCurvaSpread" runat="server" Width="100%" ClientInstanceName="gridCurvaSpread"
                        AutoGenerateColumns="False" DataSourceID="EsDSCurvaSpread" KeyFieldName="IdCurvaSpreadPopup"
                        OnCustomDataCallback="gridCurvaSpread_CustomDataCallback" OnCustomCallback="gridCurvaSpread_CustomCallback"
                        OnHtmlRowCreated="gridCurvaSpread_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdCurvaRendaFixa" Caption="Curva Spread"
                                VisibleIndex="0" Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                Width="60%" />
                        </Columns>
                        <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridCurvaSpread.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataCurvaSpread);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Curva Spread." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCurvaSpread.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <div class="divPanel">
        <table width="100%">
            <tr>
                <td>
                    <div id="container">
                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Curvas de Renda Fixa"></asp:Label>
                        </div>
                        <div id="mainContent">
                            <div class="linkButton">
                                <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                    <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true)  
                                                                                               {callbackErro.SendCallback('btnDelete'); operacao='deletar';}
                                                                                               return false;">
                                    <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                    <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                    ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                    <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                    </div>
                                </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                    CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;">
                                    <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                    </div>
                                </asp:LinkButton></div>
                            <div class="divDataGrid">
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdCurvaRendaFixa"
                                    DataSourceID="EsDSCurvaRendaFixa" OnCustomCallback="gridCadastro_CustomCallback"
                                    OnLoad="gridCadastro_Load" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                    OnCustomJSProperties="gridCadastro_CustomJSProperties" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                    AutoGenerateColumns="False">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdCurvaRendaFixa" Caption="ID" VisibleIndex="0"
                                            ReadOnly="True" Width="5%">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="Descricao" VisibleIndex="1" Caption="Descrição"
                                            Width="20%">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Critério de Interpolação" FieldName="CriterioInterpolacao"
                                            VisibleIndex="2" Width="10%">
                                            <PropertiesComboBox ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Expressão Taxa Zero" FieldName="ExpressaoTaxaZero"
                                            VisibleIndex="3">
                                            <PropertiesComboBox ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Tipo de Curva" FieldName="TipoCurva" VisibleIndex="4">
                                            <PropertiesComboBox ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoCurvaBase" VisibleIndex="5" Caption="Curva Base"
                                            Width="20%">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoCurvaSpread" Caption="Curva Spread"
                                            VisibleIndex="6" Width="20%">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="PermiteInterpolacao" Visible="false" VisibleIndex="7">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="PermiteExtrapolacao" Visible="false" VisibleIndex="8">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdTipoCurvaBase" Visible="false" VisibleIndex="9">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdCurvaSpread" Visible="false" VisibleIndex="10">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="IdIndice" Visible="false" VisibleIndex="11">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="TipoComposicao" Visible="false" VisibleIndex="12">
                                        </dxwgv:GridViewDataTextColumn>
                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <div class="editForm">
                                                <dxe:ASPxTextBox ID="hiddenIdSerie" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdSerie"
                                                    Text='<%#Eval("IdCurvaRendaFixa")%>' />
                                                <dxe:ASPxTextBox ID="hiddenIdCurvaBase" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdCurvaBase"
                                                    Text='<%#Eval("IdCurvaBase")%>' />
                                                <dxe:ASPxTextBox ID="hiddenIdCurvaSpread" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdCurvaSpread"
                                                    Text='<%#Eval("IdCurvaSpread")%>' />
                                                <dxe:ASPxTextBox ID="hiddenIdIndice" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdIndice"
                                                    Text='<%#Eval("IdIndice")%>' />
                                                <table>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelTabela" runat="server" CssClass="labelRequired" Text="Série:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButtonEdit ID="btnEditSerieRendaFixa" runat="server" CssClass="textButtonEdit"
                                                                EnableClientSideAPI="True" ClientInstanceName="btnEditSerieRendaFixa" ReadOnly="true"
                                                                Width="340px" Text='<%# Eval("IdCurvaRendaFixa") != null ? Eval("IdCurvaRendaFixa") + " - " + Eval("Descricao") : " "%>' OnLoad="btnEditSerieRendaFixa_Load">
                                                                <Buttons>
                                                                    <dxe:EditButton />
                                                                </Buttons>
                                                                <ClientSideEvents ButtonClick="function(s, e) {popupSerieRendaFixa.ShowAtElementByID(s.name);}" />
                                                            </dxe:ASPxButtonEdit>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelPermiteInterpolacao" runat="server" CssClass="labelRequired"
                                                                AssociatedControlID="dropPermiteInterpolacao" Text="Permite Interpolação:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropPermiteInterpolacao" runat="server" CssClass="dropDownListLongo"
                                                                OnLoad="dropPermiteInterpolacao_Load" Text='<%#Eval("PermiteInterpolacao")%>'>
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelPermiteExtrapolacao" runat="server" CssClass="labelRequired"
                                                                AssociatedControlID="dropPermiteExtrapolacao" Text="Permite Extrapolação:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropPermiteExtrapolacao" runat="server" CssClass="dropDownListLongo"
                                                                OnLoad="dropPermiteExtrapolacao_Load" Text='<%#Eval("PermiteExtrapolacao")%>'>
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelCriterioInterpolacao" runat="server" CssClass="labelRequired"
                                                                AssociatedControlID="dropCriterioInterpolacao" Text="Criterio Interpolação:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropCriterioInterpolacao" runat="server" CssClass="dropDownListLongo"
                                                                OnLoad="dropCriterioInterpolacao_Load" Text='<%#Eval("CriterioInterpolacao")%>'>
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelTipoCurva" runat="server" CssClass="labelRequired" AssociatedControlID="dropTipoCurva"
                                                                Text="Tipo Curva:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropTipoCurva" runat="server" CssClass="dropDownListLongo"
                                                                EnableClientSideAPI="True" OnLoad="dropTipoCurva_Load" Text='<%#Eval("TipoCurva")%>'>
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelExpressaoTaxaZero" runat="server" CssClass="labelRequired" AssociatedControlID="dropExpressaoTaxaZero"
                                                                Text="Expressão Taxa Zero:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropExpressaoTaxaZero" runat="server" CssClass="dropDownListLongo"
                                                                OnLoad="dropExpressaoTaxaZero_Load" Text='<%#Eval("ExpressaoTaxaZero")%>'>
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelCurvaBase" runat="server" CssClass="labelNormal" Text="Curva Base:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButtonEdit ID="btnEditCurvaBase" runat="server" CssClass="textButtonEdit"
                                                                Text='<%# (Eval("IdCurvaBase") != null ? Eval("IdCurvaBase") + "-" + Eval("DescricaoCurvaBase") : " ") %>'
                                                                EnableClientSideAPI="True" ClientInstanceName="btnEditCurvaBase" ReadOnly="True"
                                                                Width="340px">
                                                                <Buttons>
                                                                    <dxe:EditButton />
                                                                </Buttons>
                                                                <ClientSideEvents ButtonClick="function(s, e) {popupCurvaBase.ShowAtElementByID(s.name); gridCurvaBase.PerformCallback('btnRefresh');}" />
                                                            </dxe:ASPxButtonEdit>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButton ID="btnLimparCurvaBase" runat="server" AutoPostBack="false" BackgroundImage-ImageUrl="~\imagens\delete.png"
                                                                BackgroundImage-HorizontalPosition="center" BackgroundImage-VerticalPosition="center"
                                                                BackgroundImage-Repeat="NoRepeat" ClientSideEvents-Click="function(s, e) { hiddenIdCurvaBase.SetValue(null); btnEditCurvaBase.SetValue(null); }">
                                                            </dxe:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelIndice" runat="server" CssClass="labelNormal" Text="Indexador Curva Base:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButtonEdit ID="btnEditIndice" runat="server" CssClass="textButtonEdit" Text='<%#(Eval("IdIndice") != null ? Eval("IdIndice") + "-" + Eval("DescricaoIndice") : " ")%>'
                                                                EnableClientSideAPI="True" ClientInstanceName="btnEditIndice" ReadOnly="true"
                                                                Width="340px">
                                                                <Buttons>
                                                                    <dxe:EditButton />
                                                                </Buttons>
                                                                <ClientSideEvents ButtonClick="function(s, e) {popupIndice.ShowAtElementByID(s.name); gridIndice.PerformCallback('btnRefresh');}" />
                                                            </dxe:ASPxButtonEdit>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButton ID="btnLimparIndice" runat="server" AutoPostBack="false" BackgroundImage-ImageUrl="~\imagens\delete.png"
                                                                BackgroundImage-HorizontalPosition="center" BackgroundImage-VerticalPosition="center"
                                                                BackgroundImage-Repeat="NoRepeat" ClientSideEvents-Click="function(s, e) { hiddenIdIndice.SetValue(null); btnEditIndice.SetValue(null); } ">
                                                            </dxe:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelCurvaSpread" runat="server" CssClass="labelNormal" Text="Curva Spread:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButtonEdit ID="btnEditCurvaSpread" runat="server" CssClass="textButtonEdit"
                                                                Text='<%#(Eval("IdCurvaSpread") != null ? Eval("IdCurvaSpread") + "-" + Eval("DescricaoCurvaSpread") : " ")%>'
                                                                EnableClientSideAPI="True" ClientInstanceName="btnEditCurvaSpread" ReadOnly="true"
                                                                Width="340px">
                                                                <Buttons>
                                                                    <dxe:EditButton />
                                                                </Buttons>
                                                                <ClientSideEvents ButtonClick="function(s, e) {popupCurvaSpread.ShowAtElementByID(s.name); gridCurvaSpread.PerformCallback('btnRefresh');}" />
                                                            </dxe:ASPxButtonEdit>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButton ID="btnLimparCurvaSpread" runat="server" AutoPostBack="false" BackgroundImage-ImageUrl="~\imagens\delete.png"
                                                                BackgroundImage-HorizontalPosition="center" BackgroundImage-VerticalPosition="center"
                                                                BackgroundImage-Repeat="NoRepeat" ClientSideEvents-Click="function(s, e) { hiddenIdCurvaSpread.SetValue(null); btnEditCurvaSpread.SetValue(null); } ">
                                                            </dxe:ASPxButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labeldTipoComposicao" runat="server" CssClass="labelNormal" AssociatedControlID="dropTipoComposicao"
                                                                Text="Tipo Composição:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropTipoComposicao" runat="server" CssClass="dropDownListLongo"
                                                                OnLoad="dropTipoComposicao_Load" Text='<%#Eval("TipoComposicao")%>'>
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="linhaH">
                                                </div>
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                        OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal5" runat="server" Text="OK+" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                        CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal1" runat="server" Text="OK" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                        CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                        <asp:Literal ID="Literal2" runat="server" Text="Cancelar" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton></div>
                                            </div>
                                        </EditForm>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="600px" />
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                            <Image Url="../../imagens/funnel--minus.png">
                                            </Image>
                                        </ClearFilterButton>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
        LeftMargin="50" RightMargin="50" />
    <cc1:esDataSource ID="EsDSCurvaRendaFixa" runat="server" OnesSelect="EsDSCurvaRendaFixa_esSelect" />
    <cc1:esDataSource ID="EsDSSerieRendaFixa" runat="server" OnesSelect="EsDSSerieRendaFixa_esSelect" />
    <cc1:esDataSource ID="EsDSCurvaBase" runat="server" OnesSelect="EsDSCurvaBase_esSelect" />
    <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
    <cc1:esDataSource ID="EsDSCurvaSpread" runat="server" OnesSelect="EsDSCurvaSpread_esSelect" />
    </form>
</body>
</html>
