﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_EnquadraRegra, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown; 
    var operacao = '';
           
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);
        popupCarteira.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        btnEditCodigoCarteira.Focus();
    }      
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);                        
            }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Enquadramento - Cadastro de Regras"></asp:Label>
    </div>
        
    <div id="mainContent">
    
    <div class="linkButton" >               
       <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
       <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir? Possíveis resultados vinculados a esta regra também serão excluídos.')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
       <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
       <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
       <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
    </div>
    
    <div class="divDataGrid">    
        
        <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="IdRegra" DataSourceID="EsDSEnquadraRegra"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnRowInserting="gridCadastro_RowInserting"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnCustomJSProperties="gridCadastro_CustomJSProperties"
                OnInitNewRow="gridCadastro_InitNewRow" 
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >     
                                                                                              
            <Columns>                    
                                     
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="1" Width="6%" CellStyle-HorizontalAlign="left"/>
                
                <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="18%"/>                    
                
                <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="4" Width="25%"/>                    
                
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="5" Width="10%"/>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoRegra" VisibleIndex="7" Width="18%" ExportWidth = "150">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="100" Text="<div title='Limite Absoluto'>Limite Absoluto</div>" />
                            <dxe:ListEditItem Value="200" Text="<div title='Concentração Cotista'>Concentração Cotista</div>" />
                            <dxe:ListEditItem Value="300" Text="<div title='Número de Cotistas'>Número de Cotistas</div>" />
                            <dxe:ListEditItem Value="400" Text="<div title='Posições a Descoberto'>Posições a Descoberto</div>" />
                            <dxe:ListEditItem Value="500" Text="<div title='Média Móvel'>Média Móvel</div>" />
                            <dxe:ListEditItem Value="600" Text="<div title='Concentração Ativo'>Concentração Ativo</div>" />                            
                            <dxe:ListEditItem Value="800" Text="<div title='Limite Oscilação'>Limite Oscilação</div>" />
                            <dxe:ListEditItem Value="810" Text="<div title='Limite Desvio (Mensal)'>Limite Desvio (Mensal)</div>" />
                            <dxe:ListEditItem Value="811" Text="<div title='Limite Desvio (Anual)'>Limite Desvio (Anual)</div>" />
                            <dxe:ListEditItem Value="900" Text="<div title='Participação PL Fundo'>Participação PL Fundo</div>" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="SubTipoRegra" VisibleIndex="8" Width="25%" ExportWidth = "150">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="401" Text="<div title='Descoberto - Termo Ação'>Descoberto - Termo Ação</div>" />
                            <dxe:ListEditItem Value="402" Text="<div title='Descoberto - Opções'>Descoberto - Opções</div>" />
                            <dxe:ListEditItem Value="601" Text="<div title='Concentração - Cotas Inv. /Ativo'>Concentração - Cotas Inv. /Ativo</div>" />
                            <dxe:ListEditItem Value="602" Text="<div title='Concentração - Cotas Inv. /Admin.'>Concentração - Cotas Inv. /Admin.</div>" />
                            <dxe:ListEditItem Value="603" Text="<div title='Concentração - Cotas Inv. /Gestor'>Concentração - Cotas Inv. /Gestor</div>" />
                            <dxe:ListEditItem Value="610" Text="<div title='Concentração - Ação'>Concentração - Ação</div>" />
                            <dxe:ListEditItem Value="620" Text="<div title='Concentração - Renda Fixa /Título'>Concentração - Renda Fixa /Título</div>" />
                            <dxe:ListEditItem Value="630" Text="<div title='Concentração - Emissor'>Concentração - Emissor</div>" />
                            <dxe:ListEditItem Value="801" Text="<div title='Cota da Carteira'>Oscilação - Cota da Carteira</div>" />
                            <dxe:ListEditItem Value="802" Text="<div title='Cotas de Fundos'>Oscilação - Cotas de Fundos</div>" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
            
                <dxwgv:GridViewDataColumn FieldName="TipoEnquadramento" Visible="False"/>                    
                <dxwgv:GridViewDataColumn FieldName="DataFim" Visible="False"/>                    
                <dxwgv:GridViewDataColumn FieldName="IdGrupoBase" Visible="False"/>                    
                <dxwgv:GridViewDataColumn FieldName="IdGrupoCriterio" Visible="False"/>                    
                <dxwgv:GridViewDataColumn FieldName="Minimo" Visible="False"/>                    
                <dxwgv:GridViewDataColumn FieldName="Maximo" Visible="False"/>                    
                <dxwgv:GridViewDataColumn FieldName="RealTime" Visible="False"/>   
            </Columns>
            
            <Templates>            
            <EditForm>                
                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                               
                <div class="editForm">
                    
                    <table>
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                            </td>        
                            
                            <td>
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                                        ClientInstanceName="btnEditCodigoCarteira"
                                                        Text='<%# Eval("IdCarteira") %>' MaxLength="10" NumberType="Integer"
                                                        OnLoad="btnEditCodigoCarteira_Load">            
                                <Buttons>                                           
                                    <dxe:EditButton />
                                </Buttons>                                                    
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                            
                            <td colspan="4">
                                <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                            </td>
                        </tr>
                        
                        <tr>       
                            <td class="td_Label">
                                <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Referência:"  />
                            </td> 
                            <td>
                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>'/>                            
                            </td>  
                        
                            <td class="td_Label">
                                <asp:Label ID="labelDataFim" runat="server" Text="Data Fim:"></asp:Label>
                            </td>
                            <td>
                                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" Value='<%#Eval("DataFim")%>'/>
                            </td>  
                        </tr>                                
                    
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelDescricao" runat="server" CssClass="labelRequired" Text="Descrição:"> </asp:Label>
                            </td>
                            <td colspan="3">
                                <dxe:ASPxTextBox ID="textDescricao" runat="server" CssClass="textDescricao_5" MaxLength="200" Text='<%#Eval("Descricao")%>'/>
                            </td>                    
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelTipoEnquadramento" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>                    
                            </td>                    
                            <td colspan="3">      
                                <dxe:ASPxComboBox ID="dropTipoEnquadramento" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto" Text='<%#Eval("TipoEnquadramento")%>'
                                                    OnLoad="dropTipoEnquadramento_Load">                                        
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Legal" />
                                <dxe:ListEditItem Value="2" Text="Gerencial" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>                                                                 
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Real-Time:"></asp:Label>                    
                            </td>                    
                            <td colspan="3">      
                                <dxe:ASPxComboBox ID="dropRealTime" runat="server" 
                                                    ShowShadow="false" CssClass="dropDownListCurto" Text='<%#Eval("RealTime")%>'
                                                    OnLoad="dropRealTime_Load">                                        
                                <Items>
                                <dxe:ListEditItem Value="1" Text="Calcula" />
                                <dxe:ListEditItem Value="2" Text="Não Calcula" />
                                </Items>                                                            
                                </dxe:ASPxComboBox>                                                                 
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelTipoRegra" runat="server" CssClass="labelRequired" Text="Tipo Regra:"></asp:Label>                    
                            </td>                    
                            <td colspan="3">      
                                <dxe:ASPxComboBox ID="dropTipoRegra" runat="server" ShowShadow="false" CssClass="dropDownListLongo" Text='<%#Eval("TipoRegra")%>'>                                        
                                <Items>
                                <dxe:ListEditItem Value="100" Text="Limite Absoluto" />
                                <dxe:ListEditItem Value="200" Text="Concentração Cotista" />                    
                                <dxe:ListEditItem Value="300" Text="Número de Cotistas" />                    
                                <dxe:ListEditItem Value="400" Text="Posições a Descoberto" />                    
                                <dxe:ListEditItem Value="500" Text="Média Móvel" />                    
                                <dxe:ListEditItem Value="600" Text="Concentração" />                    
                                <dxe:ListEditItem Value="800" Text="Limite Oscilação" />
                                <dxe:ListEditItem Value="810" Text="Limite Desvio (Mensal)" />
                                <dxe:ListEditItem Value="811" Text="Limite Desvio (Anual)" />
                                <dxe:ListEditItem Value="900" Text="Participação PL Fundo" />
                                </Items>             
                                <ClientSideEvents
                                                 SelectedIndexChanged="function(s, e) {dropSubTipoRegra.ClearItems();
                                                            if (s.GetSelectedIndex() == 3 || s.GetSelectedIndex() == 5 || s.GetSelectedIndex() == 6 || s.GetSelectedIndex() == 7 || s.GetSelectedIndex() == 8)
                                                                { dropSubTipoRegra.SetEnabled(true);
                                                                  if (s.GetSelectedIndex() == 3)
                                                                  {
                                                                    dropSubTipoRegra.InsertItem(0, 'Termo Ação', 401);
                                                                    dropSubTipoRegra.InsertItem(1, 'Opções', 402);
                                                                  }
                                                                  else if (s.GetSelectedIndex() == 5)
                                                                  {
                                                                    dropSubTipoRegra.InsertItem(0, 'Cotas Inv. - Ativo', 601);
                                                                    dropSubTipoRegra.InsertItem(1, 'Cotas Inv. - Administrador', 602);
                                                                    dropSubTipoRegra.InsertItem(2, 'Cotas Inv. - Gestor', 603);
                                                                    dropSubTipoRegra.InsertItem(3, 'Ação', 610);
                                                                    dropSubTipoRegra.InsertItem(4, 'Título Renda Fixa', 620);
                                                                    dropSubTipoRegra.InsertItem(5, 'Emissor', 630);
                                                                  }
                                                                  else if (s.GetSelectedIndex() == 6 || s.GetSelectedIndex() == 7 || s.GetSelectedIndex() == 8)
                                                                  {
                                                                    dropSubTipoRegra.InsertItem(0, 'Cota da Carteira', 801);
                                                                    dropSubTipoRegra.InsertItem(1, 'Cotas de Fundos', 802);                                                                    
                                                                  }
                                                                }
                                                            else
                                                                {dropSubTipoRegra.SetSelectedIndex(-1); dropSubTipoRegra.SetEnabled(false); }   
                                                                
                                                            if (s.GetSelectedIndex() == 0 || s.GetSelectedIndex() == 5)
                                                                { dropGrupoBase.SetEnabled(true); }
                                                            else
                                                                { dropGrupoBase.SetSelectedIndex(-1); dropGrupoBase.SetEnabled(false); }
                                                                
                                                            if (s.GetSelectedIndex() == 0 || s.GetSelectedIndex() == 5)
                                                                { dropGrupoCriterio.SetEnabled(true); }
                                                            else
                                                                { dropGrupoCriterio.SetSelectedIndex(-1); dropGrupoCriterio.SetEnabled(false); }
                                                                  
                                                            if (s.GetSelectedIndex() != 3 && s.GetSelectedIndex() != 4)
                                                                { textMinimo.SetEnabled(true); textMaximo.SetEnabled(true); }
                                                            else
                                                                { textMinimo.SetText(''); textMinimo.SetEnabled(false);
                                                                  textMaximo.SetText(''); textMaximo.SetEnabled(false); }
                                                            }"
                                                />                                                                                                                                     
                                </dxe:ASPxComboBox>                                                                 
                            </td>      
                        </tr>                
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelSubTipoRegra" runat="server" CssClass="labelNormal" Text="Sub Tipo Regra:"></asp:Label>                    
                            </td>                    
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropSubTipoRegra" runat="server" 
                                                    ShowShadow="false" ClientInstanceName="dropSubTipoRegra"
                                                    CssClass="dropDownListLongo" Text='<%#Eval("SubTipoRegra")%>'>
                                                    <ClientSideEvents
                                                 SelectedIndexChanged="function(s, e) {
                                                            if (s.GetSelectedIndex() == 3 || s.GetSelectedIndex() == 5)
                                                                { dropGrupoCriterio.SetEnabled(true); }
                                                            else
                                                                { dropGrupoCriterio.SetSelectedIndex(-1); dropGrupoCriterio.SetEnabled(false); }
                                                                  
                                                            }"
                                                />
                                </dxe:ASPxComboBox>                                                                 
                            </td>      
                        </tr>                
                    
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelGrupoCriterio" runat="server" CssClass="labelNormal" Text="Grupo Critério:"></asp:Label>      
                            </td>
                            
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropGrupoCriterio" runat="server" DataSourceID="EsDSEnquadraGrupo" 
                                                    ClientInstanceName="dropGrupoCriterio" ShowShadow="false" CssClass="dropDownListLongo"                                                 
                                                    TextField="Descricao" ValueField="IdGrupo"
                                                    Text='<%#Eval("IdGrupoCriterio")%>'>
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                        
                        <tr>                            
                            <td class="td_Label">
                                <asp:Label ID="labelGrupoBase" runat="server" CssClass="labelNormal" Text="Grupo Base:"></asp:Label>                    
                            </td>
                            
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropGrupoBase" runat="server" DataSourceID="EsDSEnquadraGrupo" ClientInstanceName="dropGrupoBase"
                                                    ShowShadow="false" CssClass="dropDownListLongo"                                                 
                                                    TextField="Descricao" ValueField="IdGrupo"
                                                    Text='<%#Eval("IdGrupoBase")%>'>
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelMinimo" runat="server" CssClass="labelNormal" Text="Mínimo:"></asp:Label>
                            </td>
                            <td colspan="3">
                                <dxe:ASPxSpinEdit ID="textMinimo" runat="server" ClientInstanceName="textMinimo" CssClass="textNormal"
                                                    Text='<%# Eval("Minimo") %>' NumberType="Float" MaxLength="10" DecimalPlaces="2">
                                </dxe:ASPxSpinEdit>
                            </td>    
                        </tr>
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelMaximo" runat="server" CssClass="labelNormal" Text="Máximo:"></asp:Label>
                            </td>
                            <td colspan="3">
                                <dxe:ASPxSpinEdit ID="textMaximo" runat="server" ClientInstanceName="textMaximo" CssClass="textNormal" 
                                                    Text='<%# Eval("Maximo") %>' NumberType="Float" MaxLength="10" DecimalPlaces="2">
                                </dxe:ASPxSpinEdit>
                            </td>                                                                                            
                        </tr>
                        
                        <tr>
                            <td colspan="3">
                                <asp:CheckBox ID="chkReplica" runat="server" Text="Replica Regra" ToolTip="Replica regra para todos as carteiras de mesmo tipo e controle" />
                            </td>
                        </tr>
                    </table>
                    
                    <div class="linhaH"></div>
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                                       
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                                                            
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>
                
                </div>      
                                                  
            </EditForm>
            
            <StatusBar>
                <div>
                    <div style="float:left">
                    <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                    </div>                    
                </div>
            </StatusBar>            
            
            </Templates>
            
            <SettingsPopup EditForm-Width="550" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>                        
            
        </dxwgv:ASPxGridView>
        
    </div>
     
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" 
    LeftMargin = "35" RightMargin = "35"
    ></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSEnquadraRegra" runat="server" OnesSelect="EsDSEnquadraRegra_esSelect" LowLevelBind="True" />    
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSEnquadraGrupo" runat="server" OnesSelect="EsDSEnquadraGrupo_esSelect" />        
    
    </form>
</body>
</html>