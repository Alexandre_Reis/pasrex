﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Cotista, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    popup=true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataPessoa(data) {
        btnEditCodigoPessoa.SetValue(data);
        popupPessoa.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoPessoa.GetValue());
        btnEditCodigoPessoa.Focus();
    }
    
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        popupCliente.HideWindow();
        ASPxCallback2.SendCallback(btnEditCodigoCliente.GetValue());
        btnEditCodigoCliente.Focus();
    }
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
            textApelido = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textApelido');
            OnCallBackCompleteCliente(s, e, popupMensagemPessoa, btnEditCodigoPessoa, textApelido);
                
                textNomeCotista.SetValue(textApelido.value);
                textApelidoCotista.SetValue(textApelido.value);
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ChkOffShore_Callback" runat="server" OnCallback="callbackChkOffShore_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { if(e.result != '') { var offshore = (e.result == 'S'); chkOffShore.SetChecked(offshore); } }" />
        </dxcb:ASPxCallback>                
        <dxcb:ASPxCallback ID="callBackIdCotista" runat="server" OnCallback="callBackIdCotista_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                textIdCotista.SetValue(e.result);
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            textApelidoClienteEspelho = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textApelidoClienteEspelho');
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textApelidoClienteEspelho);
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                var resultSplit = e.result.split('|');
                  
                if(resultSplit[1] != null && resultSplit[1] == 'updateIdCotista')
                {
                    alert(resultSplit[0]);  
                    callBackIdCotista.SendCallback();
                }  
                else
                {                         
                alert(e.result);                              
            }
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Cotistas"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnAtiva" runat="server" Font-Overline="false" CssClass="btnEdit"
                                        OnClientClick="gridCadastro.PerformCallback('btnAtivar'); return false;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Ativa" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDesativa" runat="server" Font-Overline="false" CssClass="btnPageDelete"
                                        OnClientClick="gridCadastro.PerformCallback('btnDesativar'); return false;">
                                        <asp:Literal ID="Literal7" runat="server" Text="Desativa" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdCotista"
                                        DataSourceID="EsDSCotista" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="13%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCotista" VisibleIndex="1" Width="5%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="IdPessoa" VisibleIndex="1" Width="5%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="2" Width="42%" />                                            
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="4"
                                                Width="10%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Cotista" FieldName="TipoCotistaCVM"
                                                VisibleIndex="5" Width="12%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="pessoa física private banking" />
                                                        <dxe:ListEditItem Value="2" Text="pessoa física varejo" />
                                                        <dxe:ListEditItem Value="3" Text="pessoa jurídica não-financeira private banking" />
                                                        <dxe:ListEditItem Value="4" Text="pessoa jurídica não-financeira varejo" />
                                                        <dxe:ListEditItem Value="5" Text="banco comercial" />
                                                        <dxe:ListEditItem Value="6" Text="corretora ou distribuidora" />
                                                        <dxe:ListEditItem Value="7" Text="outras pessoas jurídicas financeiras" />
                                                        <dxe:ListEditItem Value="8" Text="Investidores não residentes" />
                                                        <dxe:ListEditItem Value="9" Text="entidade aberta de previdência complementar" />
                                                        <dxe:ListEditItem Value="10" Text="entidade fechada de previdência complementar" />
                                                        <dxe:ListEditItem Value="11" Text="regime próprio de previdência dos servidores públicos" />
                                                        <dxe:ListEditItem Value="12" Text="sociedade seguradora ou resseguradora" />
                                                        <dxe:ListEditItem Value="13" Text="sociedade de capitalização e de arrendamento mercantil" />
                                                        <dxe:ListEditItem Value="14" Text="fundos e clubes de Investimento" />
                                                        <dxe:ListEditItem Value="15" Text="cotistas de distribuidores do fundo (distribuição por conta e ordem)" />
                                                        <dxe:ListEditItem Value="16" Text="outros tipos de cotistas não relacionados" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IsentoIR" Visible="false" />                                            
                                            <dxwgv:GridViewDataColumn FieldName="IsentoIOF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdAgenteDistribuidor" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdClienteEspelho" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoClienteEspelho" Visible="false" UnboundType="String" />
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoPessoa" Visible="false" UnboundType="String" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoCotistaAnbima" Visible="false" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ClientInstanceName="lblIdCotista" CssClass="labelNormal" runat="server"  ID="lblIdCotista" Text="Id Cotista">
                                                                </dxe:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxTextBox ID="textIdCotista" ClientInstanceName="textIdCotista" runat="server" CssClass="textButtonEdit" BackColor="#e9ebe8"
                                                                    Text='<%#Eval("IdCotista")%>' ClientEnabled="false">
                                                                    <ClientSideEvents Init="function(s,e)
                                                                                            {
                                                                                                if(gridCadastro.IsNewRowEditing())
                                                                                                    callBackIdCotista.SendCallback();
                                                                                            }" /> 
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxCheckBox ID="chkOffShore" runat="server" ClientInstanceName="chkOffShore" >
                                                                    <ClientSideEvents Init="function(s, e) { if(!gridCadastro.IsNewRowEditing()) ChkOffShore_Callback.SendCallback(); }"/>
                                                                </dxe:ASPxCheckBox>                      
                                                                <asp:Label ID="lblOffShore" runat="server" CssClass="labelNormal" Text="OffShore"> </asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPessoa" runat="server" CssClass="labelRequired" Text="Pessoa:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoPessoa" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoPessoa" Text='<%# Eval("IdPessoa") %>' OnLoad="btnEditCodigoPessoa_Load"
                                                                    MaxLength="10" NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textApelido').value = '';} "
                                                                        ButtonClick="function(s, e) {popupPessoa.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemPessoa, ASPxCallback1, btnEditCodigoPessoa);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textApelido" runat="server" CssClass="textLongo" Enabled="False"
                                                                    Text='<%# Eval("ApelidoPessoa") %>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelNomeCotista" runat="server" CssClass="labelRequired" Text="Nome Cotista:"> </asp:Label>                                                                                    
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxTextBox ID="textNomeCotista" runat="server" CssClass="textLongo" ClientInstanceName="textNomeCotista"
                                                                    Text='<%#Eval("Nome")%>' MaxLength="255">
                                                                </dxe:ASPxTextBox>
                                                            </td>                                                        
                                                        </tr>                                                        
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelApelidoCotista" runat="server" CssClass="labelRequired" Text="Apelido Cotista:"> </asp:Label>                                                                                    
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxTextBox ID="textApelidoCotista" runat="server" CssClass="textLongo" ClientInstanceName="textApelidoCotista"
                                                                    Text='<%#Eval("Apelido")%>' MaxLength="255">
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelStatusAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropStatusAtivo" runat="server" ClientInstanceName="dropStatusAtivo"
                                                                    ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                    Text='<%#Eval("StatusAtivo")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Sim" />
                                                                        <dxe:ListEditItem Value="2" Text="Não" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelIsentoIR" runat="server" CssClass="labelRequired" Text="Isento IR:"> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropIsentoIR" runat="server" ClientInstanceName="dropIsentoIR"
                                                                    ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                    Text='<%#Eval("IsentoIR")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelIsentoIOF" runat="server" CssClass="labelRequired" Text="Isento IOF: "> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropIsentoIOF" runat="server" ClientInstanceName="dropIsentoIOF"
                                                                    ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                    Text='<%#Eval("IsentoIOF")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Tipo Tributação: "> </asp:Label>
                                                            </td>
                                                            <td colspan="4">
                                                                <dxe:ASPxComboBox ID="dropTipoTributacao" runat="server" ClientInstanceName="dropTipoTributacao"
                                                                    ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                    Text='<%#Eval("TipoTributacao")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Residente" />
                                                                        <dxe:ListEditItem Value="2" Text="Não Residente" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Tipo Cotista: "> </asp:Label>
                                                            </td>
                                                            <td colspan="4">
                                                                <dxe:ASPxComboBox ID="dropTipoCotista" runat="server" ClientInstanceName="dropTipoCotista"
                                                                    ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownList" Text='<%#Eval("TipoCotistaCVM")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="pessoa física private banking" />
                                                                        <dxe:ListEditItem Value="2" Text="pessoa física varejo" />
                                                                        <dxe:ListEditItem Value="3" Text="pessoa jurídica não-financeira private banking" />
                                                                        <dxe:ListEditItem Value="4" Text="pessoa jurídica não-financeira varejo" />
                                                                        <dxe:ListEditItem Value="5" Text="banco comercial" />
                                                                        <dxe:ListEditItem Value="6" Text="corretora ou distribuidora" />
                                                                        <dxe:ListEditItem Value="7" Text="outras pessoas jurídicas financeiras" />
                                                                        <dxe:ListEditItem Value="8" Text="Investidores não residentes" />
                                                                        <dxe:ListEditItem Value="9" Text="entidade aberta de previdência complementar" />
                                                                        <dxe:ListEditItem Value="10" Text="entidade fechada de previdência complementar" />
                                                                        <dxe:ListEditItem Value="11" Text="regime próprio de previdência dos servidores públicos" />
                                                                        <dxe:ListEditItem Value="12" Text="sociedade seguradora ou resseguradora" />
                                                                        <dxe:ListEditItem Value="13" Text="sociedade de capitalização e de arrendamento mercantil" />
                                                                        <dxe:ListEditItem Value="14" Text="fundos e clubes de Investimento" />
                                                                        <dxe:ListEditItem Value="15" Text="cotistas de distribuidores do fundo (distribuição por conta e ordem)" />
                                                                        <dxe:ListEditItem Value="16" Text="outros tipos de cotistas não relacionados" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Tipo Cotista Anbima: "> </asp:Label>
                                                            </td>
                                                            <td colspan="4">
                                                                <dxe:ASPxComboBox ID="dropTipoCotistaAnbima" runat="server" ClientInstanceName="dropTipoCotistaAnbima"
                                                                    ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownList" Text='<%#Eval("TipoCotistaAnbima")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="EFPC Emp. Pública" />
                                                                        <dxe:ListEditItem Value="2" Text="EFPC Emp. Privada" />
                                                                        <dxe:ListEditItem Value="3" Text="Seguradora" />
                                                                        <dxe:ListEditItem Value="4" Text="Entidade Aberta de Previdência Complementar" />
                                                                        <dxe:ListEditItem Value="5" Text="Capitalização" />
                                                                        <dxe:ListEditItem Value="6" Text="Corporate" />
                                                                        <dxe:ListEditItem Value="7" Text="Middle Market" />
                                                                        <dxe:ListEditItem Value="8" Text="Private" />
                                                                        <dxe:ListEditItem Value="9" Text="Varejo Alta Renda" />
                                                                        <dxe:ListEditItem Value="10" Text="Varejo" />
                                                                        <dxe:ListEditItem Value="11" Text="Poder Público" />
                                                                        <dxe:ListEditItem Value="12" Text="Regime Próprio de Previdência Social" />
                                                                        <dxe:ListEditItem Value="13" Text="Fundos de Investimento" />
                                                                        <dxe:ListEditItem Value="14" Text="Estrangeiros" />
                                                                        <dxe:ListEditItem Value="15" Text="Outros" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteiraAssociada" runat="server" CssClass="labelNormal" Text="Carteira:"> </asp:Label>
                                                            </td>
                                                            <td colspan="4">
                                                                <dxe:ASPxComboBox ID="dropCarteiraAssociada" runat="server" ClientInstanceName="dropCarteiraAssociada"
                                                                    DataSourceID="EsDSCarteira" ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownList"
                                                                    TextField="Apelido" ValueField="IdCarteira"  Text='<%#Eval("IdCarteira")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelLogin" runat="server" CssClass="labelNormal" Text="Login:"> </asp:Label>
                                                            </td>
                                                            <td colspan="4">
                                                                <asp:TextBox ID="textLogin" runat="server" CssClass="textMaisLongo"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCodigoInterface" runat="server" Text="Cód.Interface:"> </asp:Label>
                                                            </td>
                                                            <td colspan="4">
                                                                <asp:TextBox ID="textCodigoInterface" runat="server" CssClass="textMaisLongo" MaxLength="40"
                                                                    Text='<%#Eval("CodigoInterface")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataExpiracao" runat="server" Text="Data de Expiração:"> </asp:Label>
                                                            </td>
                                                            <td colspan="4">
                                                                <dxe:ASPxDateEdit ID="textDataExpiracao" runat="server" ClientInstanceName="textDataExpiracao"
                                                                   Value='<%#Eval("DataExpiracao")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPendenciaCadastral" runat="server" Text="Pendência Cadastral:"> </asp:Label>
                                                            </td>
                                                            <td colspan="4">
                                                                <asp:TextBox ID="textPendenciaCadastral" runat="server" TextMode="MultiLine" Rows="2" CssClass="textLongo" MaxLength="200"
                                                                    Text='<%#Eval("PendenciaCadastral")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelIdClienteEspelho" runat="server" CssClass="labelNormal" Text="Cliente Espelho:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCliente" Text='<%# Eval("IdClienteEspelho") %>' 
                                                                    MaxLength="10" NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textApelidoClienteEspelho').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback2, btnEditCodigoCliente);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textApelidoClienteEspelho" runat="server" CssClass="textLongo" Enabled="False" 
                                                                Text='<%#Eval("ApelidoClienteEspelho")%>'/>
                                                            </td>
                                                        </tr>
                                                        <tr>                                                            
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblCodigoConsolidacaoExterno" runat="server" Text="Código de Consolidação Externo:"> </asp:Label>
                                                            </td>
                                                            <td colspan="4">
                                                                <dxe:ASPxTextBox ID="textCodigoConsolidacaoExterno" runat="server" CssClass="textLongo" MaxLength="20"
                                                                    Text='<%#Eval("CodigoConsolidacaoExterno")%>' />
                                                            </td>
                                                        </tr>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="450px" />
                                        <ClientSideEvents BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }" EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteDistribudor" runat="server" OnesSelect="EsDSAgenteDistribudor_esSelect" />
        <cc1:esDataSource ID="EsDSPessoa" runat="server" OnesSelect="EsDSPessoa_esSelect" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    </form>
</body>
</html>
