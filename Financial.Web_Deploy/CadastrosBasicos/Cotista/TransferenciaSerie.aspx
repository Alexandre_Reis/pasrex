﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_TransferenciaSerie, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    {
        var popup = true;    
        document.onkeydown=onDocumentKeyDown;
        var operacao = '';
        var btnSerie = '';
            
        function OnGetDataCotista(data) 
        {        
            btnEditCodigoCotista.SetValue(data);        
            callBackCotista.SendCallback(btnEditCodigoCotista.GetValue());
            popupCotista.HideWindow();
            btnEditCodigoCotista.Focus();
            hiddenCotista.SetValue(data);   
        }   
             
        function OnGetDataCarteira(data) 
        {
            btnEditCodigoCarteira.SetValue(data);        
            callBackCarteira.SendCallback(btnEditCodigoCarteira.GetValue());
            popupCarteira.HideWindow();
            btnEditCodigoCarteira.Focus();
            hiddenCarteira.SetValue(data);  
        }         
        
        function OnGetDataCarteiraFiltro(data) 
        {        
            btnEditCodigoCarteiraFiltro.SetValue(data);        
            callBackCarteira.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
            popupCarteira.HideWindow();
            btnEditCodigoCarteiraFiltro.Focus();
        }
        
        function OnGetDataSerieOffShore(data) 
        {
            if(btnSerie == 'Origem')
            {
                hiddenSerieOrigem.SetValue(data);
                btnEditSerieOrigem.Focus();
            }
            else
            {
                hiddenSerieDestino.SetValue(data);
                btnEditSerieDestino.Focus();
            }
            
            callBackPopupSerieOffShore.SendCallback(data);
            popupSerieOffShore.HideWindow();                                    
        }
        
        function CloseGridLookup() 
        {
            dropPosicaoCotista.ConfirmCurrentSelection();
            dropPosicaoCotista.HideDropDown();
            dropPosicaoCotista.Focus();
        }      

        function AtualizaGridLookup() 
        {            
            callBackAtualizaFiltro.PerformCallback();   
            dropPosicaoCotista.GetGridView().UnselectAllRowsOnPage(); 
            dropPosicaoCotista.GetGridView().PerformCallback();    
        }      

        function LimpaTodosCampos()
        {               
            LimpaCotista();
            LimpaSeries();                       
        } 
        
        function LimpaCotista()
        {
            btnEditCodigoCotista.SetValue(null);            
            hiddenCotista.SetValue(null);  
            textNomeCotista = '';
            dropPosicaoCotista.GetGridView().UnselectAllRowsOnPage();
        }
        
        function LimpaSeries()
        {               
            btnEditSerieOrigem.SetValue(null);
            hiddenSerieOrigem.SetValue(null); 
            btnEditSerieDestino.SetValue(null);
            hiddenSerieDestino.SetValue(null);                                   
            dropPosicaoCotista.GetGridView().UnselectAllRowsOnPage();
        } 
    }     
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackCotista" runat="server" OnCallback="callBackCotista_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                        
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];
                var textNomeCotistaFiltro = document.getElementById('popupFiltro_textNomeCotistaFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotistaFiltro, textNomeCotistaFiltro);
            }
            else    
            {
                var resultSplit = e.result.split('|');
                hiddenCotista.SetValue(resultSplit[1]);
                LimpaSeries();
                var textNomeCotista = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotista');
                OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotista, textNomeCotista);                                                     
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackCarteira" runat="server" OnCallback="callBackCarteira_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else
            {
                LimpaTodosCampos();
                hiddenCarteira.SetValue(resultSplit[3]);    
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira, textDataExecucao);
                
                if (gridCadastro.cp_EditVisibleIndex == 'new') 
                {                                                                                        
                    var newDate = LocalizedData(resultSplit[1], resultSplit[2]);
                    textDataExecucao.SetValue(newDate);                                                                                                                                                                                                 
                }                                                                             
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackPopupSerieOffShore" runat="server" OnCallback="callBackPopupSerieOffShore_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
                                        {
                                            if (e.result != null)
                                            { 
                                                if(btnSerie == 'Origem')
                                                {
                                                    btnEditSerieOrigem.SetValue(e.result);                                                                                                             
                                                    btnEditSerieDestino.SetValue(null);
                                                    hiddenSerieDestino.SetValue(null);   
                                                    AtualizaGridLookup(); 
                                                }
                                                else
                                                {
                                                    btnEditSerieDestino.SetValue(e.result);           
                                                }                                                                                                                                                                                                         
                                            }
                                            btnSerie = '';                                                
                                        } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackAtualizaFiltro" runat="server" OnCallback="callBackAtualizaFiltro_Callback">
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupSerieOffShore" ClientInstanceName="popupSerieOffShore"
            runat="server" Width="450px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
            PopupHorizontalAlign="OutsideRight" AllowDragging="True">
            <ContentStyle VerticalAlign="Top">
            </ContentStyle>
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentSerieOffShore" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridSerieOffShore" runat="server" Width="100%" ClientInstanceName="gridSerieOffShore"
                            AutoGenerateColumns="False" DataSourceID="EsDSSerieOffShore" KeyFieldName="IdSeriesOffShore"
                            OnCustomDataCallback="gridSerieOffShore_CustomDataCallback" OnCustomCallback="gridSerieOffShore_CustomCallback"
                            OnHtmlRowCreated="gridSerieOffShore_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="IdSeriesOffShore" VisibleIndex="0" ReadOnly="True"
                                    Width="8%">
                                    <EditFormSettings Visible="False" Caption="Id.Série OffShore" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="Descricao" VisibleIndex="1" Caption="Descrição">
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) {
                    gridSerieOffShore.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataSerieOffShore);}" Init="function(s, e) {e.cancel = true;}" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True">
                                <AlternatingRow Enabled="True">
                                </AlternatingRow>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Série - OffShore." />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents CloseUp="function(s, e) {gridSerieOffShore.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Transferência de Séries - Off Shore"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton></div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdTransferenciaSerie"
                                        DataSourceID="EsDSTransferenciaSerie" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnCellEditorInitialize="gridCadastro_CellEditorInitialize" OnPreRender="gridCadastro_PreRender"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdTransferenciaSerie" ReadOnly="True" VisibleIndex="0" Caption="Id.Transferência Série" Width="8%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataExecucao" VisibleIndex="1" Caption="Data Execução" Width="6%">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" VisibleIndex="2" UnboundType="String" Width="5%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCarteira" VisibleIndex="3" UnboundType="String" Caption="Carteira">
                                            </dxwgv:GridViewDataColumn>                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCotista" VisibleIndex="4" UnboundType="String" Width="5%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCotista" VisibleIndex="5" UnboundType="String" Caption="Cotista">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="NomeSerieOrigem" VisibleIndex="6" UnboundType="String" Caption="Série Origem">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="NomeSerieDestino" VisibleIndex="7" UnboundType="String" Caption="Série Destino">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdPosicao" VisibleIndex="8" UnboundType="String" Caption="Id.Posição" Width="8%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdSerieOrigem" Visible="false" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdSerieDestino" Visible="false" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenCarteira" runat="server" CssClass="hiddenField" Text='<%#Eval("IdCarteira")%>'
                                                        ClientInstanceName="hiddenCarteira" />
                                                    <dxe:ASPxTextBox ID="hiddenCotista" runat="server" CssClass="hiddenField" Text='<%#Eval("IdCotista")%>'
                                                        ClientInstanceName="hiddenCotista" />
                                                    <dxe:ASPxTextBox ID="hiddenSerieOrigem" runat="server" CssClass="hiddenField" Text='<%#Eval("IdSerieOrigem")%>'
                                                        ClientInstanceName="hiddenSerieOrigem" />
                                                    <dxe:ASPxTextBox ID="hiddenSerieDestino" runat="server" CssClass="hiddenField" Text='<%#Eval("IdSerieDestino")%>'
                                                        ClientInstanceName="hiddenSerieDestino" />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira OffShore:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit"
                                                                    OnLoad="btnEditCodigoCarteira_Load" ClientInstanceName="btnEditCodigoCarteira"
                                                                    Text='<%#Eval("IdCarteira")%>' MaxLength="10" NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) { OnLostFocus(popupMensagemCarteira, callBackCarteira, btnEditCodigoCarteira);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false"
                                                                    Text='<%#Eval("DescricaoCarteira")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCotista" runat="server" CssClass="labelNormal" Text="Cotista:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCotista" runat="server" ClientInstanceName="btnEditCodigoCotista"
                                                                    OnLoad="btnEditCodigoCotista_Load" CssClass="textButtonEdit" Text='<%#Eval("IdCotista")%>'
                                                                    MaxLength="10" NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) { document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotista').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" LostFocus="function(s, e) { OnLostFocus(popupMensagemCotista, callBackCotista, btnEditCodigoCotista);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textNomeCotista" runat="server" CssClass="textNome" Enabled="false"
                                                                    Text='<%#Eval("DescricaoCotista")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataExecucao" runat="server" CssClass="labelRequired" Text="Data Execução:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataExecucao" runat="server" ClientInstanceName="textDataExecucao"
                                                                    OnLoad="textDataExecucao_Load" ClientSideEvents-ValueChanged="LimpaSeries" Value='<%#Eval("DataExecucao")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblSerieOrigem" runat="server" CssClass="labelRequired" Text="Série Origem:" />
                                                            </td>
                                                            <td colspan="3">
                                                                <dxe:ASPxButtonEdit ID="btnEditSerieOrigem" runat="server" CssClass="textButtonEdit"
                                                                    OnLoad="btnEditSerieOrigem_Load" Text='<%# Eval("IdSerieOrigem") + " - " + Eval("NomeSerieOrigem") %>'
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditSerieOrigem" ReadOnly="True"
                                                                    Width="340px">
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {btnSerie = 'Origem'; popupSerieOffShore.ShowAtElementByID(s.name); gridSerieOffShore.PerformCallback(btnSerie);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPosicoes" runat="server" CssClass="labelRequired" Text="Posições:"
                                                                    OnLoad="labelPosicoes_Load"></asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <dxgv:ASPxGridLookup ID="dropPosicaoCotista" runat="server" SelectionMode="Multiple"
                                                                    ClientInstanceName="dropPosicaoCotista" Width="340px" TextFormatString="{0}" 
                                                                    OnLoad="dropPosicaoCotista_Load" MultiTextSeparator=", " EnableClientSideAPI="true"
                                                                    KeyFieldName="IdPosicao" DataSourceID="EsDSPosicaoCotista">
                                                                    <Columns>
                                                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                                        <dxwgv:GridViewDataColumn FieldName="IdPosicao" Caption="Id.Posição" />
                                                                        <dxwgv:GridViewDataColumn FieldName="IdCotista" Caption="Id.Cotista" />
                                                                        <dxwgv:GridViewDataColumn FieldName="Nome" Caption="Nome" Width="40%" />
                                                                        <dxwgv:GridViewDataColumn FieldName="DataAplicacao" Caption="Data Aplicação" />
                                                                        <dxwgv:GridViewDataColumn FieldName="DataConversao" Caption="Data Aplicação" />
                                                                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" 
                                                                            VisibleIndex="9" Width="12%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                                            CellStyle-HorizontalAlign="Right">                                                                      
                                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                            </PropertiesTextEdit>
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorLiquido" Caption="Valor Líquido" 
                                                                            VisibleIndex="9" Width="12%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                                            CellStyle-HorizontalAlign="Right">                                                                      
                                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                            </PropertiesTextEdit>
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                        <dxwgv:GridViewDataTextColumn FieldName="CotaAplicacao" Caption="Cota Aplicação" 
                                                                            VisibleIndex="9" Width="12%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                                            CellStyle-HorizontalAlign="Right">                                                                      
                                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                            </PropertiesTextEdit>
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <GridViewProperties>
                                                                        <Templates>
                                                                            <StatusBar>
                                                                                <table class="OptionsTable" style="float: right">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <dxe:ASPxButton ID="Fechar" runat="server" AutoPostBack="false" Text="Fechar" ClientSideEvents-Click="CloseGridLookup" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </StatusBar>
                                                                        </Templates>
                                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                    </GridViewProperties>
                                                                    <ClientSideEvents DropDown="function(s, e) { }" />
                                                                </dxgv:ASPxGridLookup>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPosicao" runat="server" CssClass="labelRequired" Text="Posição:"
                                                                    OnLoad="labelPosicao_Load"></asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <asp:TextBox ID="textPosicao" runat="server" CssClass="textNome" OnLoad="textPosicao_Load"
                                                                    Enabled="false" Text='<%#Eval("IdPosicao")%>'></asp:TextBox>
                                                            </td>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="lblSerieDestino" runat="server" CssClass="labelRequired" Text="Série Destino:" />
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxButtonEdit ID="btnEditSerieDestino" runat="server" CssClass="textButtonEdit"
                                                                        Text='<%# Eval("IdSerieDestino") + " - " + Eval("NomeSerieDestino") %>' EnableClientSideAPI="True"
                                                                        ClientInstanceName="btnEditSerieDestino" ReadOnly="True" Width="340px">
                                                                        <Buttons>
                                                                            <dxe:EditButton />
                                                                        </Buttons>
                                                                        <ClientSideEvents ButtonClick="function(s, e) {btnSerie = 'Destino'; popupSerieOffShore.ShowAtElementByID(s.name); gridSerieOffShore.PerformCallback(btnSerie);}" />
                                                                    </dxe:ASPxButtonEdit>
                                                                </td>
                                                            </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton></div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="550px" />
                                        <ClientSideEvents Init="function(s, e) { FocusField(gridCadastro.cpTextDescricao);}"
                                            BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }" EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                            <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif">
                                                <Image Url="../../imagens/ico_form_ok_inline.gif">
                                                </Image>
                                            </UpdateButton>
                                            <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif">
                                                <Image Url="../../imagens/ico_form_back_inline.gif">
                                                </Image>
                                            </CancelButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSTransferenciaSerie" runat="server" OnesSelect="EsDSTransferenciaSerie_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
        <cc1:esDataSource ID="EsDSSerieOffShore" runat="server" OnesSelect="EsDSSerieOffShore_esSelect" />
        <cc1:esDataSource ID="EsDSPosicaoCotista" runat="server" OnesSelect="EsDSPosicaoCotista_esSelect" />
    </form>
</body>
</html>
