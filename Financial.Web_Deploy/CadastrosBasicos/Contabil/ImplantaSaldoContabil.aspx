﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_ImplantaSaldoContabil, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    var operacao = '';
          
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        btnEditCodigoCliente.Focus();
    }
    
    function OnGetDataClienteFiltro(data){
        if (popupFiltro.IsVisible()) {
            btnEditCodigoClienteFiltro.SetValue(data);        
            ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoClienteFiltro.Focus();
            }
    }
    
    function OnGetData(data) 
    {
        document.getElementById(gridCadastro.cpHiddenIdConta).value = data;
        ASPxCallbackConta.SendCallback(data);
        btnEditConta.Focus();        
        
        popupContabConta.HideWindow();
    }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, document.getElementById('popupFiltro_textNomeClienteFiltro'));                        
            }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callBackAtualizacao" runat="server" OnCallback="callBackAtualizacao_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { alert(e.result); }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallbackConta" runat="server" OnCallback="ASPxCallbackConta_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    if (e.result != null) 
                                                                {
                                                                    var resultSplit = e.result.split('|');
                                                                    codigo = resultSplit[0];
                                                                    codigoReduzida = resultSplit[1];
                                                                    
                                                                    codigoFinal = '';
                                                                    if (codigo != '' && codigo != null)
                                                                    {
                                                                        codigoFinal = codigo;
                                                                    }
                                                                    else if (codigoReduzida != '' && codigoReduzida != null)
                                                                    {
                                                                        codigoFinal = codigoReduzida;
                                                                    }
                                                                    
                                                                    btnEditConta.SetValue(codigoFinal);
                                                                }
                                                            } "/>
    </dxcb:ASPxCallback>
    
    <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">                                                                        
        <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
            
            <table>        
                <tr>
                    <td class="td_Label_Longo">
                        <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                    </td>        
                    
                    <td>                                                                                                                
                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                    ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">            
                        <Buttons>                                           
                            <dxe:EditButton>
                            </dxe:EditButton>                                
                        </Buttons>       
                        <ClientSideEvents                                                           
                                 KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                 ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                />               
                        </dxe:ASPxSpinEdit>
                    </td>
                
                    <td  colspan="2" width="450">
                        <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                    </td>
                </tr>        
                
                <tr>
                    <td>                
                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                    </td>    
                                        
                    <td>
                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                    </td>  
                    
                    <td colspan="2">
                    <table>
                        <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                        <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                    </table>
                    </td>                                                                            
                </tr>
                
            </table>        
            
            <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal8" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
            </div>                   
        </dxpc:PopupControlContentControl></ContentCollection>                             
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupAtualiza" AllowDragging="true" PopupElementID="popupAtualiza"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Atualização de Saldos Contábeis"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                <table border="0">
                    
                    <tr>
                        <td class="td_Label">
                            <asp:Label ID="labelListaClientes" runat="server" CssClass="labelNormal" Text="Ids de clientes (separador ; )" > </asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="textListaClientes" runat="server" TextMode="MultiLine" Rows="4" CssClass="textLongo5"></asp:TextBox>
                        </td> 
                    </tr>
                    
                    <tr>
                        <td class="td_Label">
                            <asp:Label ID="labelDataAtualizacao" runat="server" CssClass="labelNormal" Text="Data:"></asp:Label>
                        </td>
                        <td>
                            <dxe:ASPxDateEdit ID="textDataAtualizacao" runat="server" ClientInstanceName="textDataAtualizacao" />
                        </td>
                    </tr>
                </table>
                
                <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                    <asp:LinkButton ID="btnProcessaAtualizacao" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK"
                        
                        OnClientClick="if (confirm('Tem certeza que quer realizar a atualização dos saldos na data?')==true) {
                                            callBackAtualizacao.SendCallback();
                                       }
                                       return false;
                                      ">                                                    
                        
                        <asp:Literal ID="Literal15" runat="server" Text="Processa Atualização" /><div></div>
                    </asp:LinkButton>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Implantação de Saldos Contábeis"></asp:Label>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal5" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnAtualizaSaldo" runat="server" Font-Overline="false" CssClass="btnLote" OnClientClick="popupAtualiza.ShowWindow(); return false;"><asp:Literal ID="Literal7" runat="server" Text="Atualiza Saldos"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>               
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" 
                KeyFieldName="CompositeKey" DataSourceID="EsDSContabSaldo"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                OnCustomJSProperties="gridCadastro_CustomJSProperties" >        
                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="13%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataColumn FieldName="CompositeKey" UnboundType="String" Visible="false">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="5%" CellStyle-HorizontalAlign="left">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="20%">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="30%" VisibleIndex="3">
                    <PropertiesTextEdit MaxLength="22"/>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Codigo" Caption="Conta Contábil" Width="10%" VisibleIndex="4">
                    <PropertiesTextEdit MaxLength="22"/>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="CodigoReduzida" Caption="Reduzida" Width="5%" VisibleIndex="5">
                    <PropertiesTextEdit MaxLength="22"/>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Data" VisibleIndex="7" Width="8%"/>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="SaldoFinal" Caption="Saldo Fechamento" VisibleIndex="9" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                                                
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>                
                
                <dxwgv:GridViewDataColumn FieldName="IdConta" Visible="false">
                </dxwgv:GridViewDataColumn>
                
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                </td>        
                                                            
                                <td>                        
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" ClientInstanceName="btnEditCodigoCliente"
                                                                Text='<%# Eval("IdCliente") %>' MaxLength="10" NumberType="Integer" 
                                                                OnLoad="btnEditCodigoCliente_Load">            
                                    <Buttons>
                                        <dxe:EditButton>
                                        </dxe:EditButton>                                
                                    </Buttons>  
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                             ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                            />               
                                    </dxe:ASPxSpinEdit>
                                </td> 
                                    
                                <td>
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                                </td>                        
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"  />
                                </td> 
                                
                                <td>
                                    <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>' OnInit="textData_Init"/>
                                </td>  
                            </tr>
                            
                            <tr>
                            <asp:Label ID="labelMensagem" runat="server" Text="  (Clique no botão...)" CssClass="labelNormal" Visible="false"></asp:Label>
                            
                            <td  class="td_Label">
                                <asp:Label ID="labelConta" runat="server" CssClass="labelRequired" Text="Conta Contábil:" />
                            </td>                                     
                            
                            <td colspan="3">
                                <asp:TextBox ID="hiddenIdConta" runat="server" CssClass="hiddenField" Text='<%#Eval("IdConta")%>'    />
                                <dxe:ASPxButtonEdit ID="btnEditConta" runat="server" CssClass="textButtonEdit" 
                                                    ClientInstanceName="btnEditConta" ReadOnly="true" Width="150px"
                                                    Text='<%#Eval("Codigo")%>' OnInit="btnEditConta_Init" > 
                                <Buttons>
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>        
                                <ClientSideEvents
                                         ButtonClick="function(s, e) {popupContabConta.ShowAtElementByID(s.name);}"                          
                                />                            
                                </dxe:ASPxButtonEdit>                                
                            </td>
                        </tr>
                            
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Saldo Fechamento:"></asp:Label>
                            </td>
                            <td colspan="2">
                                <dxe:ASPxSpinEdit ID="textValor" runat="server" ClientInstanceName="textValor"
                                    Text='<%# Eval("SaldoFinal") %>' NumberType="Float" MaxLength="16" DecimalPlaces="2"
                                    CssClass="textValor">
                                </dxe:ASPxSpinEdit>                                
                            </td>                                                        
                        </tr>
                            
                        </table>
                        
                        <div class="linkButton linkButtonNoBorder popupFooter">
                        
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>

                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal6" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
                <StatusBar>
                    <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                </StatusBar>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="250px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
    
    <cc1:esDataSource ID="EsDSContabSaldo" runat="server" OnesSelect="EsDSContabSaldo_esSelect" LowLevelBind="true" />    
    <cc1:esDataSource ID="EsDSPlano" runat="server" OnesSelect="EsDSPlano_esSelect" />    
    <cc1:esDataSource ID="EsDSContabConta" runat="server" OnesSelect="EsDSContabConta_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
    </form>
</body>
</html>