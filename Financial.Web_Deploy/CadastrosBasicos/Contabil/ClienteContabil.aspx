﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_ClienteContabil, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    

        
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    document.onkeydown=onDocumentKeyDown;    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                    
            }
            else 
            {
                gridCadastro.UpdateEdit();                
            }
        }        
        "/>        
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callBackLote" runat="server" OnCallback="callBackLote_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.' ||
                    e.result == 'Processo executado parcialmente.') {
                     
                    popupLote.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }                               
            }                                             
        }        
        " />
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Cliente - Parâmetros para Contábil"></asp:Label>
    </div>
           
    <div id="mainContent">
        
        <dxpc:ASPxPopupControl ID="popupLote" AllowDragging="true" PopupElementID="popupLote"
                        EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                        CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Alteração em Lote"
                        runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                        <ContentCollection>
                            <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                                <table>
                                    <tr>
                                        <td class="td_Label">
                                            <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Calcula Contábil:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropCalculaContabilLote" runat="server" ClientInstanceName="dropCalculaContabilLote"
                                                ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                Text='<%#Eval("CalculaContabil")%>'>
                                                <Items>
                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                </Items>
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td class="td_Label">
                                            <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Plano:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropContabilPlanoLote" runat="server" ClientInstanceName="dropContabilPlanoLote"
                                                DataSourceID="EsDSContabilPlano" ShowShadow="false" DropDownStyle="DropDownList"
                                                CssClass="dropDownListCurto" TextField="Descricao" ValueField="IdPlano" >
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td class="td_Label">
                                            <asp:Label ID="label21" runat="server" CssClass="labelNormal" Text="Liquidação:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropContabilLiquidacaoLote" runat="server" ClientInstanceName="dropContabilLiquidacaoLote"
                                                ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto">
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Net" />
                                                    <dxe:ListEditItem Value="2" Text="Segregado" />
                                                </Items>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_Label">
                                            <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Ações:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropContabilAcoesLote" runat="server" ClientInstanceName="dropContabilAcoesLote"
                                                ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto">
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Consolidado" />
                                                    <dxe:ListEditItem Value="2" Text="Analítico" />
                                                </Items>
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td class="td_Label">
                                            <asp:Label ID="label14" runat="server" CssClass="labelNormal" Text="Fundos:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropContabilFundosLote" runat="server" ClientInstanceName="dropContabilFundosLote"
                                                ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto">
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Consolidado" />
                                                    <dxe:ListEditItem Value="2" Text="Analítico" />
                                                </Items>
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td class="td_Label">
                                            <asp:Label ID="label20" runat="server" CssClass="labelNormal" Text="BMF:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropContabilBMFLote" runat="server" ClientInstanceName="dropContabilBMFLote"
                                                ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto">
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Consolidado" />
                                                    <dxe:ListEditItem Value="2" Text="Analítico" />
                                                </Items>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="td_Label">
                                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="DayTrade:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropContabilDaytradeLote" runat="server" ClientInstanceName="dropContabilDaytradeLote"
                                                ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto">
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Net" />
                                                    <dxe:ListEditItem Value="2" Text="Segregado" />
                                                </Items>
                                            </dxe:ASPxComboBox>
                                        </td>
                                        
                                        <td class="td_Label">
                                            <asp:Label ID="label15" runat="server" CssClass="labelNormal" Text="Categoria:" />
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textCategoriaContabilLote" runat="server" ClientInstanceName="textCategoriaContabilLote"
                                                NumberType="Integer" MaxLength="10" CssClass="textValor_5" />
                                        </td>
                                    </tr>
                                </table>
                                
                                <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                    <asp:LinkButton ID="btnProcessaLote" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK"
                                        
                                        OnClientClick="if (confirm('Tem certeza que quer realizar a atualização em lote?')==true) 
                                                       {
                                                            callBackLote.SendCallback();
                                                       }
                                                       return false;
                                                      ">                                                    
                                        
                                        <asp:Literal ID="Literal15" runat="server" Text="Processa Atualização" /><div></div>
                                    </asp:LinkButton>
                                </div>
                            </dxpc:PopupControlContentControl>
                        </ContentCollection>
            </dxpc:ASPxPopupControl>
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnLote" runat="server" Font-Overline="false" CssClass="btnLote" OnClientClick="popupLote.ShowWindow(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Altera em Lote" /><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdCliente" DataSourceID="EsDSCliente"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnCustomCallback="gridCadastro_CustomCallback" >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>  
                    
                    <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="IdTipo" VisibleIndex="1" Width="10%" ExportWidth="60">
                        <EditFormSettings Visible="False" />
                        <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo"/>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="2" Width="5%" CellStyle-HorizontalAlign="left"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="CalculaContabil" Caption="Contábil" VisibleIndex="4" Width="4%" ExportWidth="40">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="S" Text="Sim"/>
                        <dxe:ListEditItem Value="N" Text="Não"/>
                    </Items>
                    </PropertiesComboBox>            
                    </dxwgv:GridViewDataComboBoxColumn>        
                    
                    <dxwgv:GridViewDataComboBoxColumn Caption="Plano" FieldName="IdPlano" VisibleIndex="6" Width="8%" ExportWidth="60">
                        <EditFormSettings Visible="False" />
                        <PropertiesComboBox DataSourceID="EsDSContabilPlano" TextField="Descricao" ValueField="IdPlano"/>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="ContabilLiquidacao" Caption="Liquidação" VisibleIndex="8" Width="6%" ExportWidth="40">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Net"/>
                        <dxe:ListEditItem Value="2" Text="Segregado"/>
                    </Items>
                    </PropertiesComboBox>            
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="ContabilAcoes" Caption="Ações" VisibleIndex="10" Width="6%" ExportWidth="40">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Consolidado"/>
                        <dxe:ListEditItem Value="2" Text="Analítico"/>
                    </Items>
                    </PropertiesComboBox>            
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="ContabilFundos" Caption="Fundos" VisibleIndex="12" Width="6%" ExportWidth="40">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Consolidado"/>
                        <dxe:ListEditItem Value="2" Text="Analítico"/>
                    </Items>
                    </PropertiesComboBox>            
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="ContabilBMF" Caption="BMF" VisibleIndex="14" Width="6%" ExportWidth="40">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Consolidado"/>
                        <dxe:ListEditItem Value="2" Text="Analítico"/>
                    </Items>
                    </PropertiesComboBox>            
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="ContabilDaytrade" Caption="Daytrade" VisibleIndex="16" Width="6%" ExportWidth="40">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="Net"/>
                        <dxe:ListEditItem Value="2" Text="Segregado"/>
                    </Items>
                    </PropertiesComboBox>            
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="ContabilDataTrava" Caption="Trava" VisibleIndex="18" Width="8%" ExportWidth="90"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="CodigoContabil" Caption="Código" VisibleIndex="20" Width="5%" CellStyle-HorizontalAlign="left"/>
                    <dxwgv:GridViewDataColumn FieldName="CategoriaContabil" Caption="Categoria" VisibleIndex="21" Width="5%" CellStyle-HorizontalAlign="left"/>
                    
                </Columns>
                
                <Templates>
                <EditForm>
                     
                    <div class="editForm">    
                            
                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" Width="150px" ClientInstanceName="btnEditCodigoCliente"
                                            Text='<%# Eval("IdCliente") %>' MaxLength="10" NumberType="Integer" Enabled="false"/>                                    
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textLongo5" Enabled="False" Text='<%# Eval("Apelido") %>'></asp:TextBox>
                                </td>
                            </tr>                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Calcula Contábil:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropCalculaContabil" runat="server" ClientInstanceName="dropCalculaContabil"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                        Text='<%#Eval("CalculaContabil")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                            <dxe:ListEditItem Value="N" Text="Não" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                                <td class="td_Label">
                                    <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Plano:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropContabilPlano" runat="server" ClientInstanceName="dropContabilPlano"
                                        DataSourceID="EsDSContabilPlano" ShowShadow="false" DropDownStyle="DropDownList"
                                        CssClass="dropDownListCurto" TextField="Descricao" ValueField="IdPlano" Text='<%#Eval("IdPlano") %>'>
                                    </dxe:ASPxComboBox>
                                </td>
                                <td class="td_Label">
                                    <asp:Label ID="label21" runat="server" CssClass="labelNormal" Text="Contábil Liquidação:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropContabilLiquidacao" runat="server" ClientInstanceName="dropContabilLiquidacao"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                        Text='<%#Eval("ContabilLiquidacao")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Net" />
                                            <dxe:ListEditItem Value="2" Text="Segregado" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Contábil Ações:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropContabilAcoes" runat="server" ClientInstanceName="dropContabilAcoes"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                        Text='<%#Eval("ContabilAcoes")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Consolidado" />
                                            <dxe:ListEditItem Value="2" Text="Analítico" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                                <td class="td_Label">
                                    <asp:Label ID="label14" runat="server" CssClass="labelNormal" Text="Contábil Fundos:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropContabilFundos" runat="server" ClientInstanceName="dropContabilFundos"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                        Text='<%#Eval("ContabilFundos")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Consolidado" />
                                            <dxe:ListEditItem Value="2" Text="Analítico" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                                <td class="td_Label">
                                    <asp:Label ID="label20" runat="server" CssClass="labelNormal" Text="Contábil BMF:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropContabilBMF" runat="server" ClientInstanceName="dropContabilBMF"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                        Text='<%#Eval("ContabilBMF")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Consolidado" />
                                            <dxe:ListEditItem Value="2" Text="Analítico" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Contábil DayTrade:" />
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropContabilDaytrade" runat="server" ClientInstanceName="dropContabilDaytrade"
                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                        Text='<%#Eval("ContabilDaytrade")%>'>
                                        <Items>
                                            <dxe:ListEditItem Value="1" Text="Net" />
                                            <dxe:ListEditItem Value="2" Text="Segregado" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>
                                <td  class="td_Label">
                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Data Trava:" />
                                </td>                                     
                                <td>
                                    <dxe:ASPxDateEdit ID="textDataTrava" runat="server" ClientInstanceName="textDataTrava" Value='<%#Eval("ContabilDataTrava")%>' />
                                </td> 
                            </tr>
                            <tr>                                
                                <td class="td_Label">
                                    <asp:Label ID="label17" runat="server" CssClass="labelNormal" Text="Código Contábil:" />
                                </td>
                                <td>
                                    <dxe:ASPxTextBox ID="textCodigoContabil" runat="server" ClientInstanceName="textCodigoContabil"
                                            MaxLength="10" CssClass="textNormal" Text='<%#Eval("CodigoContabil") %>' />
                                </td>
                                
                                <td class="td_Label">
                                    <asp:Label ID="label15" runat="server" CssClass="labelNormal" Text="Id. Categoria:" />
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textCategoriaContabil" runat="server" ClientInstanceName="textCategoriaContabil"
                                        NumberType="Integer" MaxLength="10" CssClass="textValor_5" Text='<%#Eval("CategoriaContabil") %>' />
                                </td>
                            </tr>                                                                            
                            
                        </table>
                        
                        <div class="linhaH"></div>    
                                                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                            OnClientClick="callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                        
                    </div>                                                  
                   
                </EditForm>
                </Templates>
                
                <SettingsPopup EditForm-Width="500px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                                
            </dxwgv:ASPxGridView>            
            </div>       
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSContabilPlano" runat="server" OnesSelect="EsDSContabilPlano_esSelect" />
    <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />
        
    </form>
</body>
</html>