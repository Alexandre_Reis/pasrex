﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_LiquidacaoContabil, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;    
    var operacao = '';
    var evento = '';
    
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }
    
    function OnGetDataClienteFiltro(data){                   
        if (popupFiltro.IsVisible()) {
            btnEditCodigoClienteFiltro.SetValue(data);        
            ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
            popupCliente.HideWindow();
            btnEditCodigoClienteFiltro.Focus();
        }        
    }    
    
    // PopContabRoteiro
    function OnGetDataContabRoteiro(data) 
    {        
        var resultSplit = data.split('|');        
        //                      
        if (evento == 'evento')
        {
            textIdEvento.SetValue(resultSplit[0]);          
            btnEditEvento.SetValue(resultSplit[1]);
            popupContabRoteiro.HideWindow();
            btnEditEvento.Focus();
        }
        if (evento == 'vencimento')
        {
            textIdEventoVencimento.SetValue(resultSplit[0]);          
            btnEditEventoVencimento.SetValue(resultSplit[1]);
            popupContabRoteiro.HideWindow();
            btnEditEventoVencimento.Focus();
        }
        //
        
    }
    
    function LimpaCampos()
    {
        textIdEvento.SetValue(null);          
        btnEditEvento.SetValue(null);
        textIdEventoVencimento.SetValue(null);          
        btnEditEventoVencimento.SetValue(null);
    }
    
    </script>
        
</head>
<body>

    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" AsyncPostBackTimeout="360000"/>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {            
            var resultSplit = e.result.split('|');                        
            e.result = resultSplit[0];            
            var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');                
            OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callBackLote" runat="server" OnCallback="callBackLote_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {   
                        
            LoadingPanel.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.') {
                     
                    popupLote.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Lançamentos - Liquidação Financeira (Ajuste de Evento)"></asp:Label>
    </div>
        
    <div id="mainContent">
        <dxe:ASPxTextBox ID="textIdEvento" ClientInstanceName="textIdEvento" runat="server" ClientVisible="false" Text='<%#Eval("IdEvento")%>' />
        <dxe:ASPxTextBox ID="textIdEventoVencimento" ClientInstanceName="textIdEventoVencimento" runat="server" ClientVisible="false" Text='<%#Eval("IdEventoVencimento")%>' />
        
        <dxpc:ASPxPopupControl ID="popupContabRoteiro" ClientInstanceName="popupContabRoteiro" runat="server" Width="550px" HeaderText="" 
                        ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridContabRoteiro" runat="server" Width="100%"
                    ClientInstanceName="gridContabRoteiro"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSContabRoteiro" KeyFieldName="IdEvento"
                    OnCustomDataCallback="gridContabRoteiro_CustomDataCallback" 
                    OnCustomCallback="gridContabRoteiro_CustomCallback"
                    OnHtmlRowCreated="gridContabRoteiro_HtmlRowCreated">               
            <Columns>
            
                    <dxwgv:GridViewDataTextColumn FieldName="IdConta" Visible="false"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" Width="20%" VisibleIndex="0" Settings-AutoFilterCondition="Contains">                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ContaDebito" Caption="Conta Débito" Width="9%" VisibleIndex="1" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">                    
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ContaCredito" Caption="Conta Crédito" Width="9%" VisibleIndex="2" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Identificador" Caption="Ativo" Width="8%" VisibleIndex="3"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Origem" VisibleIndex="4" Width="25%" ExportWidth="200">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="100" Text="<div title='Bolsa - Valorização Ação'>Bolsa - Valorização Ação</div>" />
                            <dxe:ListEditItem Value="101" Text="<div title='Bolsa - Reversão Valorização Ação'>Bolsa - Reversão Valorização Ação</div>" />
                            <dxe:ListEditItem Value="102" Text="<div title='Bolsa - Desvalorização Ação'>Bolsa - Desvalorização Ação</div>" />
                            <dxe:ListEditItem Value="103" Text="<div title='Bolsa - Reversão Desvalorização Ação'>Bolsa - Reversão Desvalorização Ação</div>" />
                            <dxe:ListEditItem Value="200" Text="<div title='Bolsa - Valorização Ação Short'>Bolsa - Valorização Ação Short</div>" />
                            <dxe:ListEditItem Value="201" Text="<div title='Bolsa - Reversão Valorização Ação Short'>Bolsa - Reversão Valorização Ação Short</div>" />
                            <dxe:ListEditItem Value="202" Text="<div title='Bolsa - Desvalorização Ação Short'>Bolsa - Desvalorização Ação Short</div>" />
                            <dxe:ListEditItem Value="203" Text="<div title='Bolsa - Reversão Desvalorização Ação Short'>Bolsa - Reversão Desvalorização Ação Short</div>" />
                            <dxe:ListEditItem Value="500" Text="<div title='Bolsa - Valorização OpçãoCompra Comprada'>Bolsa - Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="501" Text="<div title='Bolsa - Reversão Valorização OpçãoCompra Comprada'>Bolsa - Reversão Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="502" Text="<div title='Bolsa - Desvalorização OpçãoCompra Comprada'>Bolsa - Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="503" Text="<div title='Bolsa - Reversão Desvalorização OpçãoCompra Comprada'>Bolsa - Reversão Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="600" Text="<div title='Bolsa - Valorização OpçãoCompra Vendida'>Bolsa - Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="601" Text="<div title='Bolsa - Reversão Valorização OpçãoCompra Vendida'>Bolsa - Reversão Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="602" Text="<div title='Bolsa - Desvalorização OpçãoCompra Vendida'>Bolsa - Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="603" Text="<div title='Bolsa - Reversão Desvalorização OpçãoCompra Vendida'>Bolsa - Reversão Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="1000" Text="<div title='Bolsa - Valorização OpçãoVenda Comprada'>Bolsa - Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1001" Text="<div title='Bolsa - Reversão Valorização OpçãoVenda Comprada'>Bolsa - Reversão Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1002" Text="<div title='Bolsa - Desvalorização OpçãoVenda Comprada'>Bolsa - Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1003" Text="<div title='Bolsa - Reversão Desvalorização OpçãoVenda Comprada'>Bolsa - Reversão Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="1100" Text="<div title='Bolsa - Valorização OpçãoVenda Vendida'>Bolsa - Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1101" Text="<div title='Bolsa - Reversão Valorização OpçãoVenda Vendida'>Bolsa - Reversão Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1102" Text="<div title='Bolsa - Desvalorização OpçãoVenda Vendida'>Bolsa - Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1103" Text="<div title='Bolsa - Reversão Desvalorização OpçãoVenda Vendida'>Bolsa - Reversão Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="1500" Text="<div title='Bolsa - Compra Ação'>Bolsa - Compra Ação</div>" />
                            <dxe:ListEditItem Value="1501" Text="<div title='Bolsa - Compra Ação (Corretagem)'>Bolsa - Compra Ação (Corretagem)</div>" />
                            <dxe:ListEditItem Value="1502" Text="<div title='Bolsa - Compra Ação (Emolumento)'>Bolsa - Compra Ação (Emolumento)</div>" />
                            <dxe:ListEditItem Value="1503" Text="<div title='Bolsa - Compra Ação (Taxas CBLC)'>Bolsa - Compra Ação (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="2000" Text="<div title='Bolsa - Venda Ação'>Bolsa - Venda Ação</div>" />
                            <dxe:ListEditItem Value="2001" Text="<div title='Bolsa - Venda Ação (Corretagem)'>Bolsa - Venda Ação (Corretagem)</div>" />
                            <dxe:ListEditItem Value="2002" Text="<div title='Bolsa - Venda Ação (Emolumento)'>Bolsa - Venda Ação (Emolumento)</div>" />
                            <dxe:ListEditItem Value="2003" Text="<div title='Bolsa - Venda Ação (Taxas CBLC)'>Bolsa - Venda Ação (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="2500" Text="<div title='Bolsa - Compra OpçãoCompra'>Bolsa - Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="2501" Text="<div title='Bolsa - Compra OpçãoCompra (Corretagem)'>Bolsa - Compra OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="2502" Text="<div title='Bolsa - Compra OpçãoCompra (Emolumento)'>Bolsa - Compra OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="2503" Text="<div title='Bolsa - Compra OpçãoCompra (Taxas CBLC)'>Bolsa - Compra OpçãoCompra (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="3000" Text="<div title='Bolsa - Venda OpçãoCompra'>Bolsa - Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="3001" Text="<div title='Bolsa - Venda OpçãoCompra (Corretagem)'>Bolsa - Venda OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="3002" Text="<div title='Bolsa - Venda OpçãoCompra (Emolumento)'>Bolsa - Venda OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="3003" Text="<div title='Bolsa - Venda OpçãoCompra (Taxas CBLC)'>Bolsa - Venda OpçãoCompra (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="3500" Text="<div title='Bolsa - Compra OpçãoVenda'>Bolsa - Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="3501" Text="<div title='Bolsa - Compra OpçãoVenda (Corretagem)'>Bolsa - Compra OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="3502" Text="<div title='Bolsa - Compra OpçãoVenda (Emolumento)'>Bolsa - Compra OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="3503" Text="<div title='Bolsa - Compra OpçãoVenda (Taxas CBLC)'>Bolsa - Compra OpçãoVenda (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="4000" Text="<div title='Bolsa - Venda OpçãoVenda'>Bolsa - Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4001" Text="<div title='Bolsa - Venda OpçãoVenda (Corretagem)'>Bolsa - Venda OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="4002" Text="<div title='Bolsa - Venda OpçãoVenda (Emolumento)'>Bolsa - Venda OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="4003" Text="<div title='Bolsa - Venda OpçãoVenda (Taxas CBLC)'>Bolsa - Venda OpçãoVenda (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="4500" Text="<div title='Bolsa - Lucro Ação'>Bolsa - Lucro Ação</div>" />
                            <dxe:ListEditItem Value="4501" Text="<div title='Bolsa - Prejuízo Ação'>Bolsa - Prejuízo Ação</div>" />
                            <dxe:ListEditItem Value="4600" Text="<div title='Bolsa - Lucro Compra OpçãoCompra'>Bolsa - Lucro Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4601" Text="<div title='Bolsa - Prejuízo Compra OpçãoCompra'>Bolsa - Prejuízo Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4650" Text="<div title='Bolsa - Lucro Venda OpçãoCompra'>Bolsa - Lucro Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4651" Text="<div title='Bolsa - Prejuízo Venda OpçãoCompra'>Bolsa - Prejuízo Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="4700" Text="<div title='Bolsa - Lucro Compra OpçãoVenda'>Bolsa - Lucro Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4701" Text="<div title='Bolsa - Prejuízo Compra OpçãoVenda'>Bolsa - Prejuízo Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4750" Text="<div title='Bolsa - Lucro Venda OpçãoVenda'>Bolsa - Lucro Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="4751" Text="<div title='Bolsa - Prejuízo Venda OpçãoVenda'>Bolsa - Prejuízo Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="5000" Text="<div title='Bolsa - Lucro Ação DayTrade'>Bolsa - Lucro Ação DayTrade</div>" />
                            <dxe:ListEditItem Value="5001" Text="<div title='Bolsa - Prejuízo Ação DayTrade'>Bolsa - Prejuízo Ação DayTrade</div>" />
                            <dxe:ListEditItem Value="5100" Text="<div title='Bolsa - Lucro Opção DayTrade'>Bolsa - Lucro Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="5101" Text="<div title='Bolsa - Prejuízo Opção DayTrade'>Bolsa - Prejuízo Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="5500" Text="<div title='Bolsa - Expiração OpçãoCompra Comprada'>Bolsa - Expiração OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="5600" Text="<div title='Bolsa - Expiração OpçãoCompra Vendida'>Bolsa - Expiração OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="5700" Text="<div title='Bolsa - Expiração OpçãoVenda Comprada'>Bolsa - Expiração OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="5800" Text="<div title='Bolsa - Expiração OpçãoVenda Vendida'>Bolsa - Expiração OpçãoVenda Vendida</div>" />                            
                            <dxe:ListEditItem Value="6000" Text="<div title='Bolsa - Exercício OpçãoCompra Comprada'>Bolsa - Exercício OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="6100" Text="<div title='Bolsa - Exercício OpçãoCompra Vendida'>Bolsa - Exercício OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="6200" Text="<div title='Bolsa - Exercício OpçãoVenda Comprada'>Bolsa - Exercício OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="6300" Text="<div title='Bolsa - Expiração OpçãoVenda Vendida'>Bolsa - Expiração OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="7000" Text="<div title='Bolsa - Liquidação a Pagar'>Bolsa - Liquidação a Pagar</div>" />
                            <dxe:ListEditItem Value="7001" Text="<div title='Bolsa - Liquidação a Receber'>Bolsa - Liquidação a Receber</div>" />
                            <dxe:ListEditItem Value="8000" Text="<div title='Bolsa - Provisão Dividendos Receber'>Bolsa - Provisão Dividendos Receber</div>" />
                            <dxe:ListEditItem Value="8100" Text="<div title='Bolsa - Pagamento Dividendos Receber'>Bolsa - Pagamento Dividendos Receber</div>" />
                            <dxe:ListEditItem Value="8200" Text="<div title='Bolsa - Provisão Dividendos Pagar'>Bolsa - Provisão Dividendos Pagar</div>" />
                            <dxe:ListEditItem Value="8300" Text="<div title='Bolsa - Pagamento Dividendos Pagar'>Bolsa - Pagamento Dividendos Pagar</div>" />
                            <dxe:ListEditItem Value="8400" Text="<div title='Bolsa - Provisão Juros s/ Capital Receber'>Bolsa - Provisão Juros s/ Capital Receber</div>" />
                            <dxe:ListEditItem Value="8500" Text="<div title='Bolsa - Pagamento Juros s/ Capital Receber'>Bolsa - Pagamento Juros s/ Capital Receber</div>" />
                            <dxe:ListEditItem Value="8600" Text="<div title='Bolsa - Provisão Juros s/ Capital Pagar'>Bolsa - Provisão Juros s/ Capital Pagar</div>" />
                            <dxe:ListEditItem Value="8700" Text="<div title='Bolsa - Pagamento Juros s/ Capital Pagar'>Bolsa - Pagamento Juros s/ Capital Pagar</div>" />                            
                            <dxe:ListEditItem Value="8800" Text="<div title='Bolsa - Provisão Rendimentos Receber'>Bolsa - Provisão Rendimentos Receber</div>" />
                            <dxe:ListEditItem Value="8900" Text="<div title='Bolsa - Pagamento Rendimentos Receber'>Bolsa - Pagamento Rendimentos Receber</div>" />
                            <dxe:ListEditItem Value="9000" Text="<div title='Bolsa - Provisão Rendimentos Pagar'>Bolsa - Provisão Rendimentos Pagar</div>" />
                            <dxe:ListEditItem Value="9100" Text="<div title='Bolsa - Pagamento Rendimentos Pagar'>Bolsa - Pagamento Rendimentos Pagar</div>" />                                                        
                            <dxe:ListEditItem Value="10000" Text="<div title='Bolsa - BTC Tomado'>Bolsa - BTC Tomado</div>" />
                            <dxe:ListEditItem Value="10100" Text="<div title='Bolsa - BTC Tomado Devolução'>Bolsa - BTC Tomado Devolução</div>" />
                            <dxe:ListEditItem Value="10200" Text="<div title='Bolsa - BTC Doado'>Bolsa - BTC Doado</div>" />
                            <dxe:ListEditItem Value="10300" Text="<div title='Bolsa - BTC Doado Devolução'>Bolsa - BTC Doado Devolução</div>" />
                            <dxe:ListEditItem Value="10400" Text="<div title='Bolsa - BTC Tomado Variação Positiva'>Bolsa - BTC Tomado Variação Positiva</div>" />
                            <dxe:ListEditItem Value="10401" Text="<div title='Bolsa - BTC Tomado Variação Positiva (Reversão)'>Bolsa - BTC Tomado Variação Positiva (Reversão)</div>" />
                            <dxe:ListEditItem Value="10500" Text="<div title='Bolsa - BTC Tomado Variação Negativa'>Bolsa - BTC Tomado Variação Negativa</div>" />
                            <dxe:ListEditItem Value="10501" Text="<div title='Bolsa - BTC Tomado Variação Negativa (Reversão)'>Bolsa - BTC Tomado Variação Negativa (Reversão)</div>" />
                            <dxe:ListEditItem Value="10600" Text="<div title='Bolsa - BTC Doado Variação Positiva'>Bolsa - BTC Doado Variação Positiva</div>" />
                            <dxe:ListEditItem Value="10601" Text="<div title='Bolsa - BTC Doado Variação Positiva (Reversão)'>Bolsa - BTC Doado Variação Positiva (Reversão)</div>" />
                            <dxe:ListEditItem Value="10700" Text="<div title='Bolsa - BTC Doado Variação Negativa'>Bolsa - BTC Doado Variação Negativa</div>" />
                            <dxe:ListEditItem Value="10701" Text="<div title='Bolsa - BTC Doado Variação Negativa (Reversão)'>Bolsa - BTC Doado Variação Negativa (Reversão)</div>" />
                            <dxe:ListEditItem Value="10800" Text="<div title='Bolsa - BTC Tomado Provisão Taxas'>Bolsa - BTC Tomado Provisão Taxas</div>" />
                            <dxe:ListEditItem Value="10900" Text="<div title='Bolsa - BTC Tomado Pagto Taxas'>Bolsa - BTC Tomado Pagto Taxas</div>" />
                            <dxe:ListEditItem Value="11000" Text="<div title='Bolsa - BTC Doado Provisão Taxas'>Bolsa - BTC Doado Provisão Taxas</div>" />
                            <dxe:ListEditItem Value="11100" Text="<div title='Bolsa - BTC Doado Pagto Taxas'>Bolsa - BTC Doado Pagto Taxas</div>" />                            
                            <dxe:ListEditItem Value="14000" Text="<div title='Bolsa - Compra Termo'>Bolsa - Compra Termo</div>" />
                            <dxe:ListEditItem Value="14001" Text="<div title='Bolsa - Compra Termo (Corretagem)'>Bolsa - Compra Termo (Corretagem)</div>" />
                            <dxe:ListEditItem Value="14002" Text="<div title='Bolsa - Compra Termo (Emolumento)'>Bolsa - Compra Termo (Emolumento)</div>" />
                            <dxe:ListEditItem Value="14003" Text="<div title='Bolsa - Compra Termo (Taxas CBLC)'>Bolsa - Compra Termo (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="14500" Text="<div title='Bolsa - Venda Termo'>Bolsa - Venda Termo</div>"/>
                            <dxe:ListEditItem Value="14501" Text="<div title='Bolsa - Venda Termo (Corretagem)'>Bolsa - Venda Termo (Corretagem)</div>" />
                            <dxe:ListEditItem Value="14502" Text="<div title='Bolsa - Venda Termo (Emolumento)'>Bolsa - Venda Termo (Emolumento)</div>" />
                            <dxe:ListEditItem Value="14503" Text="<div title='Bolsa - Venda Termo (Taxas CBLC)'>Bolsa - Venda Termo (Taxas CBLC)</div>" />
                            <dxe:ListEditItem Value="14700" Text="<div title='Bolsa - Liquidação Física Termo Comprado'>Bolsa - Liquidação Física Termo Comprado</div>" />
                            <dxe:ListEditItem Value="14750" Text="<div title='Bolsa - Liquidação Física Termo Vendido'>Bolsa - Liquidação Física Termo Vendido</div>" />
                            <dxe:ListEditItem Value="14800" Text="<div title='Bolsa - Liquidação Financeira Termo Comprado'>Bolsa - Liquidação Financeira Termo Comprado</div>" />
                            <dxe:ListEditItem Value="14850" Text="<div title='Bolsa - Liquidação Financeira Termo Vendido'>Bolsa - Liquidação Financeira Termo Vendido</div>" />
                            <dxe:ListEditItem Value="15000" Text="<div title='Bolsa - Valorização Termo Comprado'>Bolsa - Valorização Termo Comprado</div>" />
                            <dxe:ListEditItem Value="15010" Text="<div title='Bolsa - Desvalorização Termo Comprado'>Bolsa - Desvalorização Termo Comprado</div>" />
                            <dxe:ListEditItem Value="15050" Text="<div title='Bolsa - Valorização Termo Vendido'>Bolsa - Valorização Termo Vendido</div>" />
                            <dxe:ListEditItem Value="15060" Text="<div title='Bolsa - Desvalorização Termo Vendido'>Bolsa - Desvalorização Termo Vendido</div>" />                            
                            <dxe:ListEditItem Value="15100" Text="<div title='Bolsa - Juros Termo Comprado'>Bolsa - Juros Termo Comprado</div>" />
                            <dxe:ListEditItem Value="15110" Text="<div title='Bolsa - Juros Termo Vendido'>Bolsa - Juros Termo Vendido</div>" />                            
                            <dxe:ListEditItem Value="20000" Text="<div title='BMF - Valorização OpçãoCompra Comprada'>BMF - Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20001" Text="<div title='BMF - Reversão Valorização OpçãoCompra Comprada'>BMF - Reversão Valorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20002" Text="<div title='BMF - Desvalorização OpçãoCompra Comprada'>BMF - Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20003" Text="<div title='BMF - Reversão Desvalorização OpçãoCompra Comprada'>BMF - Reversão Desvalorização OpçãoCompra Comprada</div>" />
                            <dxe:ListEditItem Value="20100" Text="<div title='BMF - Valorização OpçãoCompra Vendida'>BMF - Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20101" Text="<div title='BMF - Reversão Valorização OpçãoCompra Vendida'>BMF - Reversão Valorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20102" Text="<div title='BMF - Desvalorização OpçãoCompra Vendida'>BMF - Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20103" Text="<div title='BMF - Reversão Desvalorização OpçãoCompra Vendida'>BMF - Reversão Desvalorização OpçãoCompra Vendida</div>" />
                            <dxe:ListEditItem Value="20500" Text="<div title='BMF - Valorização OpçãoVenda Comprada'>BMF - Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20501" Text="<div title='BMF - Reversão Valorização OpçãoVenda Comprada'>BMF - Reversão Valorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20502" Text="<div title='BMF - Desvalorização OpçãoVenda Comprada'>BMF - Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20503" Text="<div title='BMF - Reversão Desvalorização OpçãoVenda Comprada'>BMF - Reversão Desvalorização OpçãoVenda Comprada</div>" />
                            <dxe:ListEditItem Value="20600" Text="<div title='BMF - Valorização OpçãoVenda Vendida'>BMF - Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="20601" Text="<div title='BMF - Reversão Valorização OpçãoVenda Vendida'>BMF - Reversão Valorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="20602" Text="<div title='BMF - Desvalorização OpçãoVenda Vendida'>BMF - Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="20603" Text="<div title='BMF - Reversão Desvalorização OpçãoVenda Vendida'>BMF - Reversão Desvalorização OpçãoVenda Vendida</div>" />
                            <dxe:ListEditItem Value="21000" Text="<div title='BMF - Ajuste Positivo Futuros'>BMF - Ajuste Positivo Futuros</div>" />
                            <dxe:ListEditItem Value="21001" Text="<div title='BMF - Ajuste Negativo Futuros'>BMF - Ajuste Negativo Futuros</div>" />
                            <dxe:ListEditItem Value="21100" Text="<div title='BMF - Futuros (Corretagem)'>BMF - Futuros (Corretagem)</div>" />
                            <dxe:ListEditItem Value="21101" Text="<div title='BMF - Futuros (Emolumento)'>BMF - Futuros (Emolumento)</div>" />
                            <dxe:ListEditItem Value="21102" Text="<div title='BMF - Futuros (Registro)'>BMF - Futuros (Registro)</div>" />
                            <dxe:ListEditItem Value="21500" Text="<div title='BMF - Compra OpçãoCompra'>BMF - Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="21600" Text="<div title='BMF - Compra OpçãoCompra (Corretagem)'>BMF - Compra OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="21601" Text="<div title='BMF - Compra OpçãoCompra (Emolumento)'>BMF - Compra OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="21602" Text="<div title='BMF - Compra OpçãoCompra (Registro)'>BMF - Compra OpçãoCompra (Registro)</div>" />
                            <dxe:ListEditItem Value="22000" Text="<div title='BMF - Venda OpçãoCompra'>BMF - Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="22100" Text="<div title='BMF - Venda OpçãoCompra (Corretagem)'>BMF - Venda OpçãoCompra (Corretagem)</div>" />
                            <dxe:ListEditItem Value="22101" Text="<div title='BMF - Venda OpçãoCompra (Emolumento)'>BMF - Venda OpçãoCompra (Emolumento)</div>" />
                            <dxe:ListEditItem Value="22102" Text="<div title='BMF - Venda OpçãoCompra (Registro)'>BMF - Venda OpçãoCompra (Registro)</div>" />
                            <dxe:ListEditItem Value="22500" Text="<div title='BMF - Compra OpçãoVenda'>BMF - Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="22600" Text="<div title='BMF - Compra OpçãoVenda (Corretagem)'>BMF - Compra OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="22601" Text="<div title='BMF - Compra OpçãoVenda (Emolumento)'>BMF - Compra OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="22602" Text="<div title='BMF - Compra OpçãoVenda (Registro)'>BMF - Compra OpçãoVenda (Registro)</div>" />
                            <dxe:ListEditItem Value="23000" Text="<div title='BMF - Venda OpçãoVenda'>BMF - Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="23100" Text="<div title='BMF - Venda OpçãoVenda (Corretagem)'>BMF - Venda OpçãoVenda (Corretagem)</div>" />
                            <dxe:ListEditItem Value="23101" Text="<div title='BMF - Venda OpçãoVenda (Emolumento)'>BMF - Venda OpçãoVenda (Emolumento)</div>" />
                            <dxe:ListEditItem Value="23102" Text="<div title='BMF - Venda OpçãoVenda (Registro)'>BMF - Venda OpçãoVenda (Registro)</div>" />                            
                            <dxe:ListEditItem Value="23500" Text="<div title='BMF - Lucro Compra OpçãoCompra'>BMF - Lucro Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="23501" Text="<div title='BMF - Prejuízo Compra OpçãoCompra'>BMF - Prejuízo Compra OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="23550" Text="<div title='BMF - Lucro Venda OpçãoCompra'>BMF - Lucro Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="23551" Text="<div title='BMF - Prejuízo Venda OpçãoCompra'>BMF - Prejuízo Venda OpçãoCompra</div>" />
                            <dxe:ListEditItem Value="24000" Text="<div title='BMF - Lucro Compra OpçãoVenda'>BMF - Lucro Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="24001" Text="<div title='BMF - Prejuízo Compra OpçãoVenda'>BMF - Prejuízo Compra OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="24050" Text="<div title='BMF - Lucro Venda OpçãoVenda'>BMF - Lucro Venda OpçãoVenda</div>" />
                            <dxe:ListEditItem Value="24051" Text="<div title='BMF - Prejuízo Venda OpçãoVenda'>BMF - Prejuízo Venda OpçãoVenda</div>" />                                    
                            <dxe:ListEditItem Value="25000" Text="<div title='BMF - Lucro Futuro DayTrade'>BMF - Lucro Futuro DayTrade</div>" />
                            <dxe:ListEditItem Value="25001" Text="<div title='BMF - Prejuízo Futuro DayTrade'>BMF - Prejuízo Futuro DayTrade</div>" />
                            <dxe:ListEditItem Value="25100" Text="<div title='BMF - Lucro Opção DayTrade'>BMF - Lucro Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="25101" Text="<div title='BMF - Prejuízo Opção DayTrade'>BMF - Prejuízo Opção DayTrade</div>" />
                            <dxe:ListEditItem Value="26000" Text="<div title='BMF - Liquidação a Pagar'>BMF - Liquidação a Pagar</div>" />
                            <dxe:ListEditItem Value="26001" Text="<div title='BMF - Liquidação a Receber'>BMF - Liquidação a Receber</div>" />                            
                            <dxe:ListEditItem Value="27000" Text="<div title='BMF - Provisão Taxa de Permanência'>BMF - Provisão Taxa de Permanência</div>" />
                            <dxe:ListEditItem Value="27001" Text="<div title='BMF - Pagamento Taxa de Permanência'>BMF - Pagamento Taxa de Permanência</div>" />                            
                            <dxe:ListEditItem Value="30000" Text="<div title='Renda Fixa - Compra Final'>Renda Fixa - Compra Final</div>" />
                            <dxe:ListEditItem Value="30500" Text="<div title='Renda Fixa - Venda Final'>Renda Fixa - Venda Final</div>" />
                            <dxe:ListEditItem Value="30600" Text="<div title='Renda Fixa - Lucro Venda Final'>Renda Fixa - Lucro Venda Final</div>" />
                            <dxe:ListEditItem Value="30601" Text="<div title='Renda Fixa - Prejuízo Venda Final'>Renda Fixa - Prejuízo Venda Final</div>" />
                            <dxe:ListEditItem Value="31000" Text="<div title='Renda Fixa - Compra Compromissada'>Renda Fixa - Compra Compromissada</div>" />
                            <dxe:ListEditItem Value="31100" Text="<div title='Renda Fixa - Revenda'>Renda Fixa - Revenda</div>" />
                            <dxe:ListEditItem Value="31200" Text="<div title='Renda Fixa - Lucro Revenda'>Renda Fixa - Lucro Revenda</div>" />
                            <dxe:ListEditItem Value="32000" Text="<div title='Renda Fixa - Ajuste MTM Positivo'>Renda Fixa - Ajuste MTM Positivo</div>" />
                            <dxe:ListEditItem Value="32001" Text="<div title='Renda Fixa - Ajuste MTM Negativo'>Renda Fixa - Ajuste MTM Negativo</div>" />
                            <dxe:ListEditItem Value="32002" Text="<div title='Renda Fixa - Reversão Ajuste MTM Positivo'>Renda Fixa - Reversão Ajuste MTM Positivo</div>" />
                            <dxe:ListEditItem Value="32003" Text="<div title='Renda Fixa - Reversão Ajuste MTM Negativo'>Renda Fixa - Reversão Ajuste MTM Negativo</div>" />
                            <dxe:ListEditItem Value="32500" Text="<div title='Renda Fixa - Renda Positiva'>Renda Fixa - Renda Positiva</div>" />
                            <dxe:ListEditItem Value="32501" Text="<div title='Renda Fixa - Renda Negativa'>Renda Fixa - Renda Negativa</div>" />
                            <dxe:ListEditItem Value="33000" Text="<div title='Renda Fixa - Vencimento'>Renda Fixa - Vencimento</div>" />
                            <dxe:ListEditItem Value="34000" Text="<div title='Renda Fixa - Pagamento Juros'>Renda Fixa - Pagamento Juros</div>" />
                            <dxe:ListEditItem Value="34100" Text="<div title='Renda Fixa - Pagamento Amortização'>Renda Fixa - Pagamento Amortização</div>" />
                            <dxe:ListEditItem Value="40000" Text="<div title='Swap - Ajuste Positivo'>Swap - Ajuste Positivo</div>" />
                            <dxe:ListEditItem Value="40001" Text="<div title='Swap - Reversão Ajuste Positivo'>Swap - Reversão Ajuste Positivo</div>" />
                            <dxe:ListEditItem Value="40100" Text="<div title='Swap - Ajuste Negativo'>Swap - Ajuste Negativo</div>" />
                            <dxe:ListEditItem Value="40101" Text="<div title='Swap - Reversão Ajuste Negativo'>Swap - Reversão Ajuste Negativo</div>" />
                            <dxe:ListEditItem Value="41000" Text="<div title='Swap - Liquidação Pagar'>Swap - Liquidação Pagar</div>" />
                            <dxe:ListEditItem Value="41001" Text="<div title='Swap - Liquidação Receber'>Swap - Liquidação Receber</div>" />
                            <dxe:ListEditItem Value="50000" Text="<div title='Fundos - Aplicação'>Fundos - Aplicação</div>" />
                            <dxe:ListEditItem Value="51000" Text="<div title='Fundos - Resgate'>Fundos - Resgate</div>" />
                            <dxe:ListEditItem Value="51100" Text="<div title='Fundos - Liquidação Resgate'>Fundos - Liquidação Resgate</div>" />
                            <dxe:ListEditItem Value="52000" Text="<div title='Fundos - Valorização'>Fundos - Valorização</div>" />
                            <dxe:ListEditItem Value="52001" Text="<div title='Fundos - Desvalorização'>Fundos - Desvalorização</div>" />
                            <dxe:ListEditItem Value="60000" Text="<div title='Cotista - Aplicação Pessoa Física'>Cotista - Aplicação Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60500" Text="<div title='Cotista - Resgate (Custo) Pessoa Física'>Cotista - Resgate (Custo) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60600" Text="<div title='Cotista - Resgate (Variação Positiva) Pessoa Física'>Cotista - Resgate (Variação Positiva) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60601" Text="<div title='Cotista - Resgate (Variação Negativa) Pessoa Física'>Cotista - Resgate (Variação Negativa) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60700" Text="<div title='Cotista - Resgate (Provisão IR) Pessoa Física'>Cotista - Resgate (Provisão IR) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="60800" Text="<div title='Cotista - Resgate (Provisão IOF) Pessoa Física'>Cotista - Resgate (Provisão IOF) Pessoa Física</div>" />
                            <dxe:ListEditItem Value="62000" Text="<div title='Cotista - Aplicação Pessoa Jurídica'>Cotista - Aplicação Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62500" Text="<div title='Cotista - Resgate (Custo) Pessoa Jurídica'>Cotista - Resgate (Custo) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62600" Text="<div title='Cotista - Resgate (Variação Positiva) Pessoa Jurídica'>Cotista - Resgate (Variação Positiva) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62601" Text="<div title='Cotista - Resgate (Variação Negativa) Pessoa Jurídica'>Cotista - Resgate (Variação Negativa) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62700" Text="<div title='Cotista - Resgate (Provisão IR) Pessoa Jurídica'>Cotista - Resgate (Provisão IR) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="62800" Text="<div title='Cotista - Resgate (Provisão IOF) Pessoa Jurídica'>Cotista - Resgate (Provisão IOF) Pessoa Jurídica</div>" />
                            <dxe:ListEditItem Value="64000" Text="<div title='Cotista - Liquidação Resgate'>Cotista - Liquidação Resgate</div>" />
                            <dxe:ListEditItem Value="64100" Text="<div title='Cotista - Liquidação Resgate IR'>Cotista - Liquidação Resgate IR</div>" />
                            <dxe:ListEditItem Value="64200" Text="<div title='Cotista - Liquidação Resgate IOF'>Cotista - Liquidação Resgate IOF</div>" />
                            <dxe:ListEditItem Value="80000" Text="<div title='Despesas - Provisão'>Despesas - Provisão</div>" />
                            <dxe:ListEditItem Value="80100" Text="<div title='Despesas - Pagamento'>Despesas - Pagamento</div>" />
                            <dxe:ListEditItem Value="999999" Text="<div title='Outros'>Outros</div>" />
                        </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdPlano" Caption="Plano" VisibleIndex="5" Width="10%">
                    <PropertiesComboBox DataSourceID="EsDSPlano" TextField="Descricao" ValueField="IdPlano"/>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="ContaDebitoReduzida" Caption="Conta Débito Reduzida" Width="9%" VisibleIndex="7" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                    <dxwgv:GridViewDataTextColumn FieldName="ContaCreditoReduzida" Caption="Conta Crédito Reduzida" Width="9%" VisibleIndex="8" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"/>
                                                                    
            </Columns>
            
            <Settings ShowFilterRow="true" ShowTitlePanel="True"/>            
            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            
            <ClientSideEvents RowDblClick="function(s, e) { 
                                                gridContabRoteiro.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataContabRoteiro);
                              }" 
                              Init="function(s, e) {e.cancel = true;}"
            />
	        
            <SettingsDetail ShowDetailButtons="False" />
            
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>
            
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Roteiro Contábil" />
            
            </dxwgv:ASPxGridView>
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridContabRoteiro.ClearFilter(); }" />
        </dxpc:ASPxPopupControl>
                 
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                
                <table>        
                    <tr>
                        <td class="td_Label_Longo">
                            <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                        </td>        
                        
                        <td>                                                                                                                
                            <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                        ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">            
                            <Buttons>                                           
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </Buttons>       
                            <ClientSideEvents                                                           
                                     KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                     ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                    />               
                            </dxe:ASPxSpinEdit>
                        </td>
                    
                        <td  colspan="2" width="450">
                            <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>        
                    
                    <tr>
                        <td>
                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Vcto Início:"></asp:Label>
                        </td>    
                                            
                        <td>
                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                        </td>  
                        
                        <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Vcto Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                        </td>                                                                   
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Lcto Início:"></asp:Label>
                        </td>    
                                            
                        <td>
                            <dxe:ASPxDateEdit ID="textDataInicioLancamento" runat="server" ClientInstanceName="textDataInicioLancamento" />
                        </td>  
                        
                        <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Lcto Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFimLancamento" runat="server" ClientInstanceName="textDataFimLancamento"/></td></tr>
                            </table>
                        </td>                                                                   
                    </tr>
                                        
                    </table>
                    
                    <table>
                    <tr>
                    <td>
                        <asp:Label ID="labelDescricao" runat="server" CssClass="labelNormal" Text="Descric:"></asp:Label>
                    </td>                
                    <td colspan="3">
                        <asp:TextBox ID="textDescricaoFiltro" runat="server" CssClass="textNormal"></asp:TextBox>
                    </td>    
                    </tr>
                </table>        
                
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>
        
        <dxpc:ASPxPopupControl ID="popupLote" AllowDragging="true" PopupElementID="popupLote" EnableClientSideAPI="True"
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="450" Left="250" Top="70" HeaderText="Alteração em Lote de Evento" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                    
                    <table>
                        <tr>
                            <td class="td_Label_Curto">
                                <asp:Label ID="labelEvento" runat="server" CssClass="labelRequired" Text="Evento:" />
                            </td>
                            <td colspan="3">
                                <dxe:ASPxButtonEdit ID="btnEditEvento" runat="server" ReadOnly="true"
                                                    Text='<%#Eval("Descricao")%>' ClientInstanceName="btnEditEvento" 
                                                    CssClass="textButtonEditLongo3">
                                <Buttons>
                                    <dxe:EditButton></dxe:EditButton>                
                                </Buttons>
                                        <ClientSideEvents                              
                                             ButtonClick="function(s, e) {popupContabRoteiro.ShowAtElementByID(s.name);evento = 'evento'}"                                                  
                                        />                                                                   
                                </dxe:ASPxButtonEdit>
                                                                                                                                                                
                            </td>                                                               
                        </tr>
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelEventoVencimento" runat="server" CssClass="labelNormal" Text="Evento Vencimento:" />
                            </td>
                            <td colspan="3">
                                <dxe:ASPxButtonEdit ID="btnEditEventoVencimento" runat="server" ReadOnly="true"
                                                    Text='<%#Eval("Descricao")%>' ClientInstanceName="btnEditEventoVencimento" 
                                                    CssClass="textButtonEditLongo3">
                                <Buttons>
                                    <dxe:EditButton></dxe:EditButton>                
                                </Buttons>
                                        <ClientSideEvents                              
                                             ButtonClick="function(s, e) {popupContabRoteiro.ShowAtElementByID(s.name);evento = 'vencimento'}"                                                  
                                        />                                                                   
                                </dxe:ASPxButtonEdit>
                                                                                                                                                                
                            </td>                                                               
                        </tr>    
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnProcessaLote" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" 
                         OnClientClick="LoadingPanel.Show();
                                        callBackLote.SendCallback();
                                        return false;
                                        ">
                        
                        <asp:Literal ID="Literal15" runat="server" Text="Processar"/><div></div>
                        </asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
        
        <div class="linkButton" >
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnLote" runat="server" Font-Overline="false" CssClass="btnLote" OnClientClick="LimpaCampos(); popupLote.ShowWindow(); return false;"><asp:Literal ID="Literal2" runat="server" Text="Altera Evento"/><div></div></asp:LinkButton>           
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
        
        <div class="divDataGrid"> 
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdLiquidacao" DataSourceID="EsDSLiquidacao"
                    OnHtmlDataCellPrepared="gridCadastro_HtmlDataCellPrepared"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    OnPreRender="gridCadastro_PreRender"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    >                     
                <Columns>                    
                                         
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="3%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="6%" CellStyle-HorizontalAlign="left"/>
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="15%"/>                    
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataLancamento" Caption="Lançamento" VisibleIndex="3" Width="10%"/>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Vencimento" VisibleIndex="4" Width="10%"/>
                    
                    <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="5" Width="20%" />                    
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Fonte" VisibleIndex="7" Width="5%" ExportWidth="110">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="2" Text="<div title='Manual'>Manual</div>" />
                        <dxe:ListEditItem Value="3" Text="<div title='Sinacor'>Sinacor</div>" />
                        <dxe:ListEditItem Value="4" Text="<div title='CMDF'>CMDF</div>" />
                    </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdEvento" Caption="Evento" VisibleIndex="8" Width="22%">
                    <PropertiesComboBox DataSourceID="EsDSContabRoteiro" TextField="Descricao" ValueField="IdEvento"/>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="9" Width="8%" 
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            GroupFooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                    
                    </dxwgv:GridViewDataTextColumn>                    
                    
                </Columns>
                
                <Templates>
                    <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                    </StatusBar>
                </Templates>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                                                
            </dxwgv:ASPxGridView>                
        </div>
                      
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" LeftMargin = "30" RightMargin = "30"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSLiquidacao" runat="server" OnesSelect="EsDSLiquidacao_esSelect" LowLevelBind="True" />
    <cc1:esDataSource ID="EsDSContabRoteiro" runat="server" OnesSelect="EsDSContabRoteiro_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />     
    <cc1:esDataSource ID="EsDSPlano" runat="server" OnesSelect="EsDSPlano_esSelect" />        
    
    <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Processando, aguarde..." ClientInstanceName="LoadingPanel" Modal="True"/>
    
    </form>
</body>
</html>