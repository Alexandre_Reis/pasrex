﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_CadastroTaxaAdministracao, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
           
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
        var popup = false;
        document.onkeydown=onDocumentKeyDown;
    </script>
    
    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Taxa Adm/Gestão/Custódia"></asp:Label>
    </div>
        
    <div id="mainContent">
        
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false; "><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
    
        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="IdCadastro" DataSourceID="EsDSCadastroTaxaAdministracao"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                >        
                    
            <Columns>           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Descricao" Width="50%" VisibleIndex="1">                    
                <PropertiesTextEdit MaxLength="255"></PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="Tipo" VisibleIndex="2" Width="40%">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="1" Text="Taxa Administração" />
                            <dxe:ListEditItem Value="2" Text="Taxa Gestão" />
                            <dxe:ListEditItem Value="3" Text="Taxa Custódia" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
            </Columns>
            
            <ClientSideEvents

                BeginCallback="function(s, e) {
		            if (e.command == 'CUSTOMCALLBACK') {
                        isCustomCallback = true;
                    }						
                }"

                EndCallback="function(s, e) {
			        if (isCustomCallback) {
                        isCustomCallback = false;
                        s.Refresh();
                    }
                }"
            />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>            
            
        </dxwgv:ASPxGridView>            
        </div>
    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSCadastroTaxaAdministracao" runat="server" OnesSelect="EsDSCadastroTaxaAdministracao_esSelect" />
        
    </form>
</body>
</html>