﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_EventoRollUp, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    {
        var popup = true;    
        document.onkeydown=onDocumentKeyDown;
        var operacao = '';

        function OnGetDataCarteira(data) 
        {
            btnEditCodigoCarteira.SetValue(data);        
            callBackCarteira.SendCallback(btnEditCodigoCarteira.GetValue());
            popupCarteira.HideWindow();
            btnEditCodigoCarteira.Focus();
            hiddenCarteira.SetValue(data);  
        }         
        
        function CloseGridLookup() 
        {
            dropSerieOrigem.ConfirmCurrentSelection();
            dropSerieOrigem.HideDropDown();
            dropSerieOrigem.Focus();
        }      

        function AtualizaGridLookup() 
        {
            dropSerieOrigem.GetGridView().PerformCallback();    
        }      
    }     
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackCarteira" runat="server" OnCallback="callBackCarteira_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else
            {
                hiddenCarteira.SetValue(resultSplit[3]);    
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira, textDataExecucao);
                
                if (gridCadastro.cp_EditVisibleIndex == 'new') 
                {                                                                                        
                    var newDate = LocalizedData(resultSplit[1], resultSplit[2]);
                    textDataExecucao.SetValue(newDate);                                                                                                                                                                                                 
                    textDataNAV.SetValue(newDate);  
                    callBackSerieBase.PerformCallback();
                }                                                                             
            }
        }        
        " />
        </dxcb:ASPxCallback>
                <dxcb:ASPxCallback ID="callBackSerieBase" runat="server" OnCallback="callBackSerieBase_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if(e.result != null)
            {
                var resultSplit = e.result.split('|');
                hiddenSerieDestino.SetValue(resultSplit[0]);
                textSerieBase.SetValue(resultSplit[1]);                
            }
            else
            {
                hiddenSerieDestino.SetValue(null);
                textSerieBase.SetValue(null);
            }
            AtualizaGridLookup();
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de RollUP - Off Shore"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton></div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdEventoRollUp"
                                        DataSourceID="EsDSEventoRollUp" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnCellEditorInitialize="gridCadastro_CellEditorInitialize" OnPreRender="gridCadastro_PreRender"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdEventoRollUp" ReadOnly="True" VisibleIndex="0"
                                                Caption="Id.Evento RollUP" Width="8%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataExecucao" VisibleIndex="1" Caption="Data Execução"
                                                Width="6%">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataNav" VisibleIndex="2" Caption="Data NAV"
                                                Width="6%">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdFundoOffShore" VisibleIndex="3" UnboundType="String" Caption="Id.Fundo"
                                                Width="5%">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCarteira" VisibleIndex="4" UnboundType="String"
                                                Caption="Fundo OffShore">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="NomeSerieOrigem" VisibleIndex="5" UnboundType="String"
                                                Caption="Série Origem">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="NomeSerieDestino" VisibleIndex="6" UnboundType="String"
                                                Caption="Série Base/Destino">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdSerieOrigem" Visible="false" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdSerieDestino" Visible="false" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenCarteira" runat="server" CssClass="hiddenField" Text='<%#Eval("IdFundoOffShore")%>'
                                                        ClientInstanceName="hiddenCarteira" />
                                                    <dxe:ASPxTextBox ID="hiddenSerieOrigem" runat="server" CssClass="hiddenField" Text='<%#Eval("IdSerieOrigem")%>'
                                                        ClientInstanceName="hiddenSerieOrigem" />
                                                    <dxe:ASPxTextBox ID="hiddenSerieDestino" runat="server" CssClass="hiddenField" Text='<%#Eval("IdSerieDestino")%>'
                                                        ClientInstanceName="hiddenSerieDestino" />                                                        
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Fundo Off Shore:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" OnLoad="btnEditCodigoCarteira_Load"
                                                                    ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdFundoOffShore")%>'
                                                                    MaxLength="10" NumberType="Integer">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) { OnLostFocus(popupMensagemCarteira, callBackCarteira, btnEditCodigoCarteira);}" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false"
                                                                    Text='<%#Eval("DescricaoCarteira")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataExecucao" runat="server" CssClass="labelRequired" Text="Data Execução:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataExecucao" runat="server" ClientInstanceName="textDataExecucao"
                                                                    Value='<%#Eval("DataExecucao")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataNAV" runat="server" CssClass="labelRequired" Text="Data NAV:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataNAV" runat="server" ClientInstanceName="textDataNAV"
                                                                    Value='<%#Eval("DataNAV")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblSerieBase" runat="server" CssClass="labelRequired" Text="Série Base:" />
                                                            </td>
                                                            <td colspan="3">
                                                                <dxe:ASPxTextBox ID="textSerieBase" runat="server" CssClass="textButtonEdit" ClientEnabled="false"
                                                                    Text='<%# Eval("IdSerieDestino") + " - " + Eval("NomeSerieDestino") %>' EnableClientSideAPI="True"
                                                                    ClientInstanceName="textSerieBase" ReadOnly="True" Width="340px">
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblSerieOrigem" runat="server" CssClass="labelRequired" Text="Série Origem:" OnLoad="lblSerieOrigem_Load" />
                                                            </td>
                                                            <td colspan="3">
                                                                <dxe:ASPxTextBox ID="textSerieOrigem" runat="server" CssClass="textButtonEdit" ClientEnabled="false"
                                                                    Text='<%# Eval("IdSerieOrigem") + " - " + Eval("NomeSerieOrigem") %>' EnableClientSideAPI="True"
                                                                    ClientInstanceName="textSerieBase" ReadOnly="True" Width="340px" OnLoad="textSerieOrigem_Load">
                                                                </dxe:ASPxTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelSeries" runat="server" CssClass="labelRequired" Text="Séries:" OnLoad="labelSeries_Load"></asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <dxgv:ASPxGridLookup ID="dropSerieOrigem" runat="server" SelectionMode="Multiple"  OnLoad="dropSerieOrigem_Load"
                                                                    ClientInstanceName="dropSerieOrigem" Width="340px" TextFormatString="{0}" MultiTextSeparator=", "
                                                                    EnableClientSideAPI="true" DataSourceID="EsDSSerieOffShore" KeyFieldName="IdSeriesOffShore">
                                                                    <Columns>
                                                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0"  />
                                                                        <dxwgv:GridViewDataTextColumn FieldName="IdSeriesOffShore" VisibleIndex="1" ReadOnly="True"
                                                                            Width="8%">
                                                                            <editformsettings visible="False" caption="Id.Série OffShore" />
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                        <dxwgv:GridViewDataTextColumn FieldName="Descricao" VisibleIndex="2" Caption="Descrição">
                                                                        </dxwgv:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <GridViewProperties>
                                                                        <Templates>
                                                                            <StatusBar>
                                                                                <table class="OptionsTable" style="float: right">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <dxe:ASPxButton ID="Atualizar" runat="server" AutoPostBack="false" Text="Atualizar"
                                                                                                ClientSideEvents-Click="AtualizaGridLookup" />
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxButton ID="Fechar" runat="server" AutoPostBack="false" Text="Fechar" ClientSideEvents-Click="CloseGridLookup" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </StatusBar>
                                                                        </Templates>
                                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                    </GridViewProperties>
                                                                    <ClientSideEvents DropDown="function(s, e) { }" />
                                                                </dxgv:ASPxGridLookup>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton></div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="550px" />
                                        <ClientSideEvents Init="function(s, e) { FocusField(gridCadastro.cpTextDescricao);}"
                                            BeginCallback="function(s, e) {
		                                                                       if (e.command == 'CUSTOMCALLBACK') 
		                                                                       {
                                                                                   isCustomCallback = true;
                                                                               }						
                                                                           }" EndCallback="function(s, e) 
                                                                                {
			                                                                       if (isCustomCallback) {
                                                                                       isCustomCallback = false;
                                                                                       s.Refresh();
                                                                                }
                                                                           }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                            <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif">
                                                <Image Url="../../imagens/ico_form_ok_inline.gif">
                                                </Image>
                                            </UpdateButton>
                                            <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif">
                                                <Image Url="../../imagens/ico_form_back_inline.gif">
                                                </Image>
                                            </CancelButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSEventoRollUp" runat="server" OnesSelect="EsDSEventoRollUp_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSSerieOffShore" runat="server" OnesSelect="EsDSSerieOffShore_esSelect" />
    </form>
</body>
</html>
