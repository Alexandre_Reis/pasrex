﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Carteira, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDownWithCallback;
        var operacao = '';

        var Financial = Financial || {};
        Financial.Carteira = Financial.Carteira || {};
        Financial.Carteira.UpdateFieldsFundo = function (p) {
            var fundo = eval('(' + p + ')');
            var idPessoa;

            if (utilizaMaxID == false) {
                fundo.IdFinancial = prompt("Digite no campo abaixo o Id.Cliente/Carteira que este fundo deverá assumir no PAS. Por padrão, o Id no PAS é o próprio Código Anbima:", fundo.CodigoFundo);
                idPessoa = prompt("Digite no campo abaixo o Id.Pessoa que este fundo deverá assumir no PAS. Por padrão, o Id no PAS (Cliente/Fundo) é o próprio Código Anbima:", fundo.CodigoFundo);
            }
            else {
                if (!confirm("O PAS escolherá os IDs de Pessoa/Fundo automaticamente!"))
                    return;

                fundo.IdFinancial = '';
                idPessoa = '';
            }

            //inputCodigoAnbid.GetInputElement().value = p;
            if (popupBuscaFundo.importaFundo === true) {
                LoadingPanel.Show();
                callBackImportaFundo.SendCallback(fundo.CodigoFundo + "|" + fundo.IdFinancial + "|" + fundo.CNPJ + "|" + idPessoa);
            } else {
                inputCodigoAnbid.SetText(fundo.CodigoFundo);
            }

            popupBuscaFundo.HideWindow();
        }

        function OnGetDataCliente(data) {
            btnEditCodigoCliente.SetValue(data);
            popupCliente.HideWindow();
            ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
            btnEditCodigoCliente.Focus();
        }

        function OnGetDataCarteira(data) {
            btnEditCodigoCarteira.SetValue(data);
            ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
            popupCarteira.HideWindow();
            btnEditCodigoCarteira.Focus();
        }

        function CamposVisiveis(e) {
            var visivel = true;
            if (dropTipoCalculoRetorno.GetSelectedIndex() == 1) visivel = false;
            {
                labelCotaInicial.SetVisible(visivel);
                textCotaInicial.SetVisible(visivel);
                labelCasasDecimaisCota.SetVisible(visivel);
                textCasasDecimaisCota.SetVisible(visivel);
                labelCasasDecimaisQuantidade.SetVisible(visivel);
                textCasasDecimaisQuantidade.SetVisible(visivel);
                labelTipoCota.SetVisible(visivel);
                dropTipoCota.SetVisible(visivel);
                labelTruncaQuantidade.SetVisible(visivel);
                dropTruncaQuantidade.SetVisible(visivel);
                labelTruncaCota.SetVisible(visivel);
                dropTruncaCota.SetVisible(visivel);
                labelTruncaFinanceiro.SetVisible(visivel);
                dropTruncaFinanceiro.SetVisible(visivel);
            }

            if (dropProjecaoIRResgate.GetSelectedIndex() == 3) {
                lblDiasAposResgateIR.SetVisible(true);
                textDiasAposResgateIR.SetVisible(true);
            }
            else {
                lblDiasAposResgateIR.SetVisible(false);
                textDiasAposResgateIR.SetVisible(false);
            }

            if (dropProjecaoIRComeCotas.GetSelectedIndex() == 3) {
                lblDiasAposComeCotasIR.SetVisible(true);
                textDiasAposComeCotasIR.SetVisible(true);
            }
            else {
                lblDiasAposComeCotasIR.SetVisible(false);
                textDiasAposComeCotasIR.SetVisible(false);
            }

            if (dropProjecaoIOFResgate.GetSelectedIndex() == 3) {
                lblDiasAposResgateIOF.SetVisible(true);
                textDiasAposResgateIOF.SetVisible(true);
            }
            else {
                lblDiasAposResgateIOF.SetVisible(false);
                textDiasAposResgateIOF.SetVisible(false);
            }

            if (dropContagemPrazoIOF.GetSelectedItem() != null && dropContagemPrazoIOF.GetSelectedItem().value == 4) {
                lblDataFimContIOF.SetVisible(true);
                lblDataInicioContIOF.SetVisible(true);
                dropDataFimContIOF.SetVisible(true);
                dropDataInicioContIOF.SetVisible(true);
            }
            else {
                lblDataFimContIOF.SetVisible(false);
                lblDataInicioContIOF.SetVisible(false);
                dropDataFimContIOF.SetVisible(false);
                dropDataInicioContIOF.SetVisible(false);
            }
        }

        function HabilitaGrupoMTM(calculaMTM) {
            var habilita = (calculaMTM == '1'); //0 = Não; 1 = Sim

            labelGrupoPerfilMTM.SetVisible(habilita);
            dropGrupoPerfilMTM.SetVisible(habilita);
        }

        function showHistorico() {
            gridHistorico.PerformCallback();
            popupHistorico.Show();
        }

        function DropMesmoConglomerado_ValueChanged(s, e) {

            if (s.GetSelectedItem().value === "S") {
                dropCategoriaAnbima.SelectIndex(-1)
                if (dropCategoriaAnbima.FindItemByValue("8") != null)
                    dropCategoriaAnbima.RemoveItem(dropCategoriaAnbima.FindItemByValue("8").index);
            }
            else {
                if (dropCategoriaAnbima.FindItemByValue("8") == null)
                    dropCategoriaAnbima.AddItem("Pessoas Físicas", 8);
            }
        }




    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360000" />
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];
                
                textApelido = document.getElementById('gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textApelido');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textApelido);
                                                
                              
                    var tipoCliente = resultSplit[1];
                    var amortizacaoRendimentoJuros = resultSplit[2];
                    
                    if (tipoCliente != null && tipoCliente != '') {
                        if(tipoCliente.toLowerCase() == 'clube') {
                            tabCadastro.GetTab(2).SetVisible(true);
                            tabCadastro.GetTab(3).SetVisible(true);
                            tabCadastro.GetTab(4).SetVisible(false);
                            tabCadastro.GetTab(5).SetVisible(false);
                            
                        }
                        else if(tipoCliente.toLowerCase() == 'offshore') {
                            tabCadastro.GetTab(2).SetVisible(false);
                            tabCadastro.GetTab(3).SetVisible(false);
                            tabCadastro.GetTab(4).SetVisible(true);
                            tabCadastro.GetTab(5).SetVisible(true);
                            textCotaInicial.SetEnabled(false);
                        }
                    }
                    else {
                        tabCadastro.GetTab(2).SetVisible(false);
                        tabCadastro.GetTab(3).SetVisible(false);
                        tabCadastro.GetTab(4).SetVisible(false);
                        tabCadastro.GetTab(5).SetVisible(false);
                    }                    
                    
            }        
        " />
        </dxcb:ASPxCallback>

        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            
                var resultSplit = e.result.split('|');
             
                var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textNome');            
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);
    
                }        
            " />
        </dxcb:ASPxCallback>

        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackLote" runat="server" OnCallback="callBackLote_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanelLote.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.' ||
                    e.result == 'Processo executado parcialmente.') {
                     
                    popupLote.Hide();
                    gridCadastro.PerformCallback('btnRefresh');
                }                               
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackImportaFundo" runat="server" OnCallback="callBackImportaFundo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            LoadingPanel.Hide();
            if (e.result != '') {
                alert(e.result);
                gridCadastro.PerformCallback('btnRefresh');
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackImportaListaFundos" runat="server" OnCallback="callBackImportaListaFundos_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            LoadingPanelFundos.Hide();
            if (e.result != '') {
                alert(e.result);
                gridCadastro.PerformCallback('btnRefresh');
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Carteiras"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupHistorico" runat="server" Width="500px" HeaderText=""
                                    ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    AllowDragging="True">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                                            <div>
                                                <dxwgv:ASPxGridView ID="gridHistorico" runat="server" Width="100%" ClientInstanceName="gridHistorico"
                                                    EnableCallBacks="true" AutoGenerateColumns="true" DataSourceID="EsDSSuitabilityHistorico"
                                                    KeyFieldName="DataHistorico" OnCustomCallback="gridHistorico_CustomCallback">
                                                    <Settings ShowTitlePanel="True" />
                                                    <SettingsBehavior ColumnResizeMode="Disabled" />
                                                    <SettingsDetail ShowDetailButtons="False" />
                                                    <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                                                        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                    </Styles>
                                                    <Images>
                                                        <PopupEditFormWindowClose Height="17px" Width="17px" />
                                                    </Images>
                                                    <SettingsText EmptyDataRow="0 Registros" Title="Histórico" />
                                                </dxwgv:ASPxGridView>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupBuscaFundo" AllowDragging="true" PopupElementID="popupBuscaFundo"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Pesquisar Fundos"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupBuscaFundoControlContentControl" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelBuscaFundoCodigoAnbid" runat="server" CssClass="labelNormal"
                                                            Text="Código Anbima:" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="textBuscaFundoCodigoAnbid" runat="server" CssClass="textValor_4"
                                                            MaxLength="30" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelBuscaFundoCNPJ" runat="server" CssClass="labelNormal" Text="CNPJ:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="textBuscaFundoCNPJ" runat="server" CssClass="textValor_4" MaxLength="30" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelBuscaFundoNomeFantasia" runat="server" CssClass="labelNormal"
                                                            Text="Nome Fundo:" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="textBuscaFundoNomeFantasia" runat="server" CssClass="textValor_4"
                                                            MaxLength="30" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnBuscaFundo" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="document.getElementById('gridBuscaFundoWrapper').style.display='block';gridBuscaFundo.PerformCallback(); return false;">
                                                    <asp:Literal ID="btnLabelBuscaFundo" runat="server" Text="Pesquisar Fundos" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                            <div id="gridBuscaFundoWrapper" style="margin-top: 20px; display: none;" class="divDataGrid">
                                                <p>
                                                    Selecione o fundo utilizando a lista abaixo:
                                                </p>
                                                <dxwgv:ASPxGridView ID="gridBuscaFundo" OnDataBinding="gridBuscaFundo_DataBinding"
                                                    runat="server" EnableCallBacks="true" ClientInstanceName="gridBuscaFundo" KeyFieldName="CodigoFundo"
                                                    Width="100%" OnCustomDataCallback="gridBuscaFundo_CustomDataCallback" OnCustomCallback="gridBuscaFundo_CustomCallback"
                                                    OnHtmlRowCreated="gridBuscaFundo_HtmlRowCreated">
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn FieldName="CodigoFundo" VisibleIndex="1" Width="20%"
                                                            CellStyle-HorizontalAlign="left" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="CNPJ" VisibleIndex="2" Width="20%" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="NomeFantasia" Caption="Nome" VisibleIndex="3"
                                                            Width="60%" CellStyle-HorizontalAlign="center" />
                                                    </Columns>
                                                    <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                                                    <SettingsBehavior ColumnResizeMode="Disabled" />
                                                    <ClientSideEvents RowDblClick="function(s, e) {
                                                    gridEvento.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataEvento);}"
                                                        Init="function(s, e) {e.cancel = true;}" />
                                                    <SettingsDetail ShowDetailButtons="False" />
                                                    <Styles AlternatingRow-Enabled="True">
                                                        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                    </Styles>
                                                    <Images>
                                                        <PopupEditFormWindowClose Height="17px" Width="17px" />
                                                    </Images>
                                                    <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Fundos" />
                                                    <ClientSideEvents RowDblClick="function(s, e) {
                                                            gridBuscaFundo.GetValuesOnCustomCallback(e.visibleIndex, Financial.Carteira.UpdateFieldsFundo)
                                                        }" />
                                                </dxwgv:ASPxGridView>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupLote" AllowDragging="true" PopupElementID="popupLote"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Alteração em Lote"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                                            <table border="0">
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:CheckBox ID="chkResetData" runat="server" Text="Reset Data Início pela Cota Inicial" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelDataInicioCota" runat="server" CssClass="labelNormal" Text="Dt. Inicio Cota:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxDateEdit ID="dataInicioCota" runat="server" ClientInstanceName="dataInicioCota" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelTruncaCota" runat="server" CssClass="labelNormal" Text="Trunca Cota:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropTruncaCota" runat="server" ClientInstanceName="dropTruncaCota"
                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String">
                                                            <Items>
                                                                <dxe:ListEditItem Value="" Text="" />
                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Trunca Qtde:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="dropTruncaQuantidade" runat="server" ClientInstanceName="dropTruncaQuantidade"
                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String">
                                                            <Items>
                                                                <dxe:ListEditItem Value="" Text="" />
                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Nr Casas Cota:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="casasDecimaisCota" runat="server" CssClass="textCurto" ClientInstanceName="casasDecimaisCota"
                                                            MaxLength="2" NumberType="integer" />
                                                    </td>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Nr Casas Qtde:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="casasDecimaisQuantidade" runat="server" CssClass="textCurto"
                                                            ClientInstanceName="casasDecimaisQuantidade" MaxLength="2" NumberType="integer" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelIndiceBenchmarkLote" runat="server" CssClass="labelNormal" Text="Benchmark:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropIndiceBenchmarkLote" runat="server" ClientInstanceName="dropIndiceBenchmarkLote"
                                                            DataSourceID="EsDSIndice" ShowShadow="False" CssClass="dropDownList" TextField="Descricao"
                                                            ValueField="IdIndice" ValueType="System.String">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelCategoriaFundoLote" runat="server" CssClass="labelNormal" Text="Categoria:"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropCategoriaFundoLote" runat="server" ClientInstanceName="dropCategoriaFundoLote"
                                                            DataSourceID="EsDSCategoriaFundo" ShowShadow="False" CssClass="dropDownListLongo"
                                                            TextField="Descricao" ValueField="IdCategoria" ValueType="System.String">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelSubCategoriaFundoLote" runat="server" CssClass="labelNormal"
                                                            Text="SubCategoria:"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropSubCategoriaFundoLote" runat="server" ClientInstanceName="dropSubCategoriaFundoLote"
                                                            DataSourceID="EsDSSubCategoriaFundo" ShowShadow="False" CssClass="dropDownListLongo"
                                                            TextField="Descricao" ValueField="IdSubCategoria" ValueType="System.String">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelAgenteAdministrador" runat="server" CssClass="labelNormal" Text="Administrador:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropAgenteAdministradorLote" runat="server" ClientInstanceName="dropAgenteAdministradorLote"
                                                            DataSourceID="EsDSAgenteMercado" ShowShadow="False" CssClass="dropDownListLongo"
                                                            TextField="Nome" ValueField="IdAgente" ValueType="System.String">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="labelAgenteGestor" runat="server" CssClass="labelNormal" Text="Gestor:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropAgenteGestorLote" runat="server" ClientInstanceName="dropAgenteGestorLote"
                                                            DataSourceID="EsDSAgenteMercado" ShowShadow="False" CssClass="dropDownListLongo"
                                                            TextField="Nome" ValueField="IdAgente" ValueType="System.String">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnProcessaLote" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="if (confirm('Tem certeza que quer realizar a atualização em lote?')==true) {
                                                                        LoadingPanelLote.Show();
                                                                        callBackLote.SendCallback();                                                                        
                                                                   }
                                                                   return false;
                                                                   ">
                                                    <asp:Literal ID="Literal15" runat="server" Text="Processa Atualização" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupBuscaFundos" AllowDragging="true" PopupElementID="popupBuscaFundos"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="290" Left="250" Top="70" HeaderText="Importar Lista de Fundos"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table border="0">
                                                <tr>
                                                    <td class="td_Label">
                                                        <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Entre com os Códigos Anbima ou o CNPJ <br>(não deve conter o ponto e ter 14 caracteres) dos fundos. <br>Cada linha representa um Fundo." />
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="margin-left: 30px">
                                                        <dxe:ASPxMemo ID="ASPxMemo1" runat="server" Height="300px" Width="260px" ClientInstanceName="ASPxMemo1"
                                                            HorizontalAlign="Center">
                                                        </dxe:ASPxMemo>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnImportacaoFundos" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="
                                                                   LoadingPanelFundos.Show();
                                                                   callBackImportaListaFundos.SendCallback();                                                                        
                                                                   
                                                                   return false;
                                                                   ">
                                                    <asp:Literal ID="Literal13" runat="server" Text="Importar Fundos" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnLimpar" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="ASPxMemo1.SetText('');
                                                                   return false;">
                                                    <asp:Literal ID="Literal14" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?  Se Confirmado todo o Histórico de Posições/Operações da Carteira será Deletado.')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnAtiva" runat="server" Font-Overline="false" CssClass="btnEdit"
                                        OnClientClick="if (confirm('Tem certeza que quer ativar os clientes?')==true) gridCadastro.PerformCallback('btnAtivar');return false;">
                                        <asp:Literal ID="Literal10" runat="server" Text="Ativa" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDesativa" runat="server" Font-Overline="false" CssClass="btnPageDelete"
                                        OnClientClick="if (confirm('Tem certeza que quer desativar os clientes?')==true) gridCadastro.PerformCallback('btnDesativar');return false;">
                                        <asp:Literal ID="Literal7" runat="server" Text="Desativa" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnLote" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="popupLote.ShowWindow(); return false;">
                                        <asp:Literal ID="Literal8" runat="server" Text="Altera em Lote" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnImportaFundo" runat="server" Font-Overline="false" CssClass="btnLote"
                                        OnClientClick="popupBuscaFundo.importaFundo = true; popupBuscaFundo.Show(); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Importar Fundo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnImportaListaFundo" runat="server" Font-Overline="false" CssClass="btnLote"
                                        Visible="true" OnClientClick="popupBuscaFundos.Show(); return false;">
                                        <asp:Literal ID="Literal12" runat="server" Text="Importar Lista Fundos" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCustomFields" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnCustomFields" OnClientClick="gridCadastro.ShowCustomizationWindow(); return false;">
                                        <asp:Literal ID="Literal11" runat="server" Text="Mais Campos" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnHistorico" runat="server" Font-Overline="false" CssClass="btnCustomFields"
                                        OnClientClick="showHistorico(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Histórico Suitability" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" ClientInstanceName="gridCadastro" runat="server"
                                        EnableCallBacks="true" KeyFieldName="IdCarteira" DataSourceID="EsDSCarteira"
                                        OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                        OnCustomButtonInitialize="gridCadastro_CustomButtonInitialize" OnCustomButtonCallback="gridCadastro_CustomButtonCallback"
                                        OnInitNewRow="gridCadastro_InitNewRow">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll" />
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <%--<dxwgv:GridViewDataColumn FieldName="IdTipo" VisibleIndex="-1" Width="8%" CellStyle-HorizontalAlign="left" />--%>
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="2" Width="8%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="4" Width="30%" />
                                            <dxwgv:GridViewDataColumn FieldName="Nome" VisibleIndex="5" Width="30%" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Categoria" FieldName="IdCategoria" VisibleIndex="6"
                                                Width="20%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSCategoriaFundo" TextField="Descricao" ValueField="IdCategoria"
                                                    DropDownStyle="DropDown" IncrementalFilteringMode="Contains" />
                                            </dxwgv:GridViewDataComboBoxColumn>

                                            <dxwgv:GridViewDataComboBoxColumn Caption="Off Shore" FieldName="FundoOffShore" ReadOnly="True"
                                                VisibleIndex="7" UnboundType="String">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewCommandColumn ButtonType="Image" Caption="Nova Classe" VisibleIndex="8"
                                                Name="NovaClasse">
                                                <CustomButtons>
                                                    <dxwgv:GridViewCommandColumnCustomButton ID="Classe">
                                                        <Image ToolTip="Criar nova classe" Url="../../imagens/ico_form_copy.gif" />
                                                    </dxwgv:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Benchmark" FieldName="IdIndiceBenchmark"
                                                VisibleIndex="9" Width="15%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="10"
                                                Width="5%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataInicioCota" Caption="Início Cota" VisibleIndex="12"
                                                Width="8%" />

                                            <dxwgv:GridViewDataColumn FieldName="TipoCarteira" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CasasDecimaisCota" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CasasDecimaisQuantidade" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TruncaCota" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TruncaQuantidade" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TruncaFinanceiro" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CalculaEnquadra" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CotaInicial" Caption="Cota Inicial" Visible="False" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo Cota" FieldName="TipoCota" Visible="False">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Abertura'>Abertura</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Fechamento'>Fechamento</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Administrador" FieldName="IdAgenteAdministrador"
                                                Visible="false">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente"
                                                    DropDownStyle="DropDown" IncrementalFilteringMode="Contains" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Gestor" FieldName="IdAgenteGestor" Visible="false">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente"
                                                    DropDownStyle="DropDown" IncrementalFilteringMode="Contains" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Rentab." FieldName="TipoRentabilidade"
                                                Visible="False">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='1o a 1o'>1o a 1o</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='30 a 30'>30 a 30</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ValorMinimoAplicacao" Caption="Mínimo Aplic."
                                                Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="ValorMinimoResgate" Caption="Mínimo Resgate"
                                                Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="ValorMinimoSaldo" Caption="Mínimo Saldo" Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="ValorMaximoAplicacao" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ValorMinimoInicial" Caption="Mínimo Inicial"
                                                Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="DiasCotizacaoAplicacao" Caption="Conversão Aplic."
                                                Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="DiasCotizacaoResgate" Caption="Conversão Resgate"
                                                Visible="False" />
                                            <dxwgv:GridViewDataColumn FieldName="DiasLiquidacaoAplicacao" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DiasLiquidacaoResgate" Caption="Liquid. Resgate"
                                                Visible="False" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Custo" FieldName="TipoCusto" Visible="False">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Aplicação'>Aplicação</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Médio'>Médio</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tributa" FieldName="TipoTributacao" Visible="False">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Curto Prazo" />
                                                        <dxe:ListEditItem Value="2" Text="Longo Prazo" />
                                                        <dxe:ListEditItem Value="3" Text="Ações" />
                                                        <dxe:ListEditItem Value="4" Text="CP (s/ ComeCotas)" />
                                                        <dxe:ListEditItem Value="5" Text="LP (s/ ComeCotas)" />
                                                        <dxe:ListEditItem Value="20" Text="Isento" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Calcula IOF" FieldName="CalculaIOF" Visible="False">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Contagem Resgate" FieldName="ContagemDiasConversaoResgate"
                                                Visible="False">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Úteis" />
                                                        <dxe:ListEditItem Value="2" Text="Corridos" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Contagem IOF" FieldName="ContagemPrazoIOF"
                                                Visible="False">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Mellon" />
                                                        <dxe:ListEditItem Value="2" Text="Prazo Corrido" />
                                                        <dxe:ListEditItem Value="3" Text="Direta" />
                                                        <dxe:ListEditItem Value="4" Text="Personalizado" />
                                                        <dxe:ListEditItem Value="5" Text="Santander" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Compensa Prejuízo" FieldName="CompensacaoPrejuizo"
                                                Visible="False">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Padrão'>Padrão</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Direto'>Direto</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DiasAniversario" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoAniversario" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="HorarioInicio" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="HorarioFim" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="HorarioLimiteCotizacao" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdSubCategoria" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CobraTaxaFiscalizacaoCVM" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoCVM" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ProjecaoIRResgate" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoVisaoFundo" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DistribuicaoDividendo" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdEstrategia" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoCalculoRetorno" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CalculaMTM" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CalculaPrazoMedio" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DiasAposResgateIR" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ProjecaoIRComeCotas" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DiasAposComeCotasIR" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="RealizaCompensacaoDePrejuizo" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdClassesOffShore" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="Nome1" Visible="False" ShowInCustomizationForm="false" UnboundType="String" />
                                            <dxwgv:GridViewDataColumn FieldName="DataInicio" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="SerieUnica" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="PoliticaInvestimentos" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="Moeda" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="Domicilio" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="TaxaAdm" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="TaxaCustodia" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="TaxaPerformance" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="IndexadorPerf" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="FreqCalcPerformance" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="LockUp" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="HardSoft" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="PenalidadeResgate" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="VlMinimoAplicacao" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="VlMinimoResgate" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="VlMinimoPermanencia" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="QtdDiasConvResgate" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="QtdDiasLiqFin" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="HoldBack" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="AnivHoldBack" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="SidePocket" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="QtdDiasConvAplicacao" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="QtdDiasLiqFinAplic" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="FrequenciaSerie" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="PrazoValidadePrevia" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="IdIndicePrevia" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="PrazoRepeticaoCotas" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="IdTipo" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="NomeEmissaoSerie" Visible="False" ShowInCustomizationForm="false" UnboundType="string" />
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteiraFundoPai" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoTituloBovespa" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="MultiplasEmissoes" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="MultiplasSeries" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="NumeroEmissao" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="NumeroSerie" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DataBaseInicioEmissaoSerie" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="EmissaoEncerravel" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DataFimEmissao" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdClasseCota" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="QuantidadeCotasAportadas" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="PermiteRendimento" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="PermiteAmortizacao" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="PeriodicidadeCota" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="PeriodicidadeRendimento" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="InicioPeriodicidadeRendimento" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="PeriodicidadeAmortizacao" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="InicioPeriodicidadeAmortizacao" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="RetemIRFonteRendimento" Visible="False" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="ExecutaResgateFimEmissao" Visible="False" ShowInCustomizationForm="false" />

                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <dxtc:ASPxPageControl ID="tabCadastro" runat="server" ClientInstanceName="tabCadastro"
                                                            EnableHierarchyRecreation="True" ActiveTabIndex="0" Height="100%" Width="100%"
                                                            TabSpacing="0px" EnableClientSideAPI="true" OnLoad="tabCadastroOnLoad">
                                                            <TabPages>
                                                                <dxtc:TabPage Text="Informações Básicas I" Name="tab1">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit1"
                                                                                            ClientInstanceName="btnEditCodigoCliente" Text='<%# Eval("IdCarteira") %>' OnLoad="btnEditCodigoCliente_Load"
                                                                                            MaxLength="15" NumberType="Integer">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents KeyPress="function(s, e) {textApelido = 'gridCadastro_DXPEForm_ef'+ gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textApelido';
		                                                                                                      document.getElementById(textApelido).value = '';} "
                                                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}"
                                                                                                LostFocus="function(s, e) { 
			                                                                                                OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);                                                                                                			                                                                                                						                                                                                                                                                                                       			                                                                                        		                                                                                        			
		                                                                                              }" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox ID="textApelido" runat="server" CssClass="textNome" Enabled="false"
                                                                                            Width="240" Text='<%# Eval("Apelido") %>'>
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelStatusAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxComboBox ID="dropStatusAtivo" runat="server" ClientInstanceName="dropStatusAtivo"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("StatusAtivo") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="2" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelProcAutomatico" runat="server" CssClass="labelRequired" Text="Proc Calculo RF:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropProcCalculoRF" runat="server" ClientInstanceName="dropProcCalculoRF"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueField="System.String"
                                                                                            Text='<%# Eval("ProcAutomatico") %>'>
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label11" runat="server" CssClass="labelRequired" Text="Retorno:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoCalculoRetorno" runat="server" ClientInstanceName="dropTipoCalculoRetorno"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_1" Text='<%# Eval("TipoCalculoRetorno") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Por Cota" />
                                                                                                <dxe:ListEditItem Value="2" Text="Por TIR" />
                                                                                            </Items>
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) { CamposVisiveis(); } " Init="function(s, e) { CamposVisiveis(); } " />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoCarteira" runat="server" CssClass="labelRequired" Text="Tipo Carteira:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoCarteira" runat="server" ClientInstanceName="dropTipoCarteira"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("TipoCarteira") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Renda Fixa" />
                                                                                                <dxe:ListEditItem Value="2" Text="Renda Variável" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIndiceBenchmark" runat="server" CssClass="labelRequired" Text="Benchmark:"></asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxComboBox ID="dropIndiceBenchmark" runat="server" ClientInstanceName="dropIndiceBenchmark"
                                                                                            DataSourceID="EsDSIndice" ShowShadow="False" CssClass="dropDownList" TextField="Descricao"
                                                                                            ValueField="IdIndice" Text='<%# Eval("IdIndiceBenchmark") %>' ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>

                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCorretora" runat="server" CssClass="labelRequired" Text="Corretora:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCorretora" runat="server" ClientInstanceName="dropCorretora"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueField="System.String"
                                                                                            Text='<%# Eval("Corretora") %>'>
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Início:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"
                                                                                            Value='<%#Eval("DataInicioCota")%>' />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelCotaInicial" runat="server" ClientInstanceName="labelCotaInicial"
                                                                                            CssClass="labelRequired" Text="Cota Inicial:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textCotaInicial" runat="server" ClientInstanceName="textCotaInicial"
                                                                                            CssClass="textValor_5" Text='<%# Eval("CotaInicial") %>' NumberType="Float" MaxLength="28"
                                                                                            DecimalPlaces="12">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelCasasDecimaisCota" runat="server" ClientInstanceName="labelCasasDecimaisCota"
                                                                                            CssClass="labelRequired" Text="Nr Casas Cota:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textCasasDecimaisCota" runat="server" CssClass="textCurto"
                                                                                            ClientInstanceName="textCasasDecimaisCota" MaxLength="2" NumberType="integer"
                                                                                            Text="<%#Bind('CasasDecimaisCota')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelCasasDecimaisQuantidade" runat="server" ClientInstanceName="labelCasasDecimaisQuantidade"
                                                                                            CssClass="labelRequired" Text="Nr Casas Qtde:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textCasasDecimaisQuantidade" runat="server" CssClass="textCurto"
                                                                                            ClientInstanceName="textCasasDecimaisQuantidade" MaxLength="2" NumberType="integer"
                                                                                            Text="<%#Bind('CasasDecimaisQuantidade')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelTipoCota" runat="server" ClientInstanceName="labelTipoCota"
                                                                                            CssClass="labelRequired" Text="Tipo Cota:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoCota" runat="server" ClientInstanceName="dropTipoCota"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_1" Text='<%# Eval("TipoCota") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Abertura" />
                                                                                                <dxe:ListEditItem Value="2" Text="Fechamento" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelTruncaCota" runat="server" ClientInstanceName="labelTruncaCota"
                                                                                            CssClass="labelRequired" Text="Trunca Cota:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTruncaCota" runat="server" ClientInstanceName="dropTruncaCota"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("TruncaCota") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelTruncaQuantidade" runat="server" ClientInstanceName="labelTruncaQuantidade"
                                                                                            CssClass="labelRequired" Text="Trunca Qtde:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTruncaQuantidade" runat="server" ClientInstanceName="dropTruncaQuantidade"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("TruncaQuantidade") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelTruncaFinanceiro" runat="server" ClientInstanceName="labelTruncaFinanceiro"
                                                                                            CssClass="labelRequired" Text="Trunca Financ.:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTruncaFinanceiro" runat="server" ClientInstanceName="dropTruncaFinanceiro"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("TruncaFinanceiro") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelBuscaCotaAnterior" runat="server" ClientInstanceName="labelBuscaCotaAnterior"
                                                                                            CssClass="labelRequired" Text="Busca Cota Anterior:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropBuscaCotaAnterior" runat="server" ClientInstanceName="dropBuscaCotaAnterior"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("BuscaCotaAnterior") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelNumeroDiasBuscaCota" runat="server" ClientInstanceName="labelNumeroDiasBuscaCota"
                                                                                            CssClass="labelNormal" Text="Nro.Dias Busca Cota:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textNumeroDiasBuscaCota" runat="server" CssClass="textCurto"
                                                                                            ClientInstanceName="textNumeroDiasBuscaCota" MaxLength="2" NumberType="integer"
                                                                                            Text="<%#Bind('NumeroDiasBuscaCota')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelBandaVariacao" runat="server" ClientInstanceName="labelBandaVariacao"
                                                                                            CssClass="labelNormal" Text="Banda Variação % Ajuste:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textBandaVariacao" runat="server" CssClass="textCurto" ClientInstanceName="textBandaVariacao"
                                                                                            NumberType="Float" MaxLength="10" DecimalPlaces="2" Text="<%#Bind('BandaVariacao')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelRebateImpactaPL" runat="server" ClientInstanceName="labelRebateImpactaPL"
                                                                                            CssClass="labelNormal" Text="Impacta PL c/ rebate:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropRebateImpactaPL" runat="server" ClientInstanceName="dropRebateImpactaPL"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("RebateImpactaPL") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelCodigoBDS" runat="server" ClientInstanceName="labelCodigoBDS"
                                                                                            CssClass="labelNormal" Text="Codigo BDS:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textCodigoBDS" runat="server" CssClass="textValor_4" ClientInstanceName="textCodigoBDS"
                                                                                            MaxLength="16" NumberType="Integer" Text="<%#Bind('CodigoBDS')%> ">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelPerfilClassificacao" runat="server" ClientInstanceName="labelPerfilClassificacao"
                                                                                            CssClass="labelNormal" Text="Perfil de Classificação:">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td colspan="5">
                                                                                        <asp:TextBox ID="textPerfilClassificacao" runat="server" CssClass="aspNetDisabled textNome"
                                                                                            Enabled="false" Text='<%# Eval("CodigoPerfil") + " - " + Eval("DescricaoPerfil") %>'>
                                                                                        </asp:TextBox>

                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblCodigoConsolidacaoExterno" runat="server" AssociatedControlID="textCodigoConsolidacaoExterno" CssClass="labelNormal"
                                                                                            Text="Código de Consolidação Externo:" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxTextBox ID="textCodigoConsolidacaoExterno" runat="server" CssClass="textLongo" MaxLength="60"
                                                                                            Text='<%#Eval("CodigoConsolidacaoExterno")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Informações Básicas II" Name="tab2">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCategoriaFundo" runat="server" CssClass="labelRequired" Text="Categoria:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCategoriaFundo" runat="server" ClientInstanceName="dropCategoriaFundo"
                                                                                            DataSourceID="EsDSCategoriaFundo" ShowShadow="False" CssClass="dropDownListLongo"
                                                                                            DropDownStyle="DropDown" IncrementalFilteringMode="Contains" TextField="Descricao"
                                                                                            ValueField="IdCategoria" Text='<%# Eval("IdCategoria") %>' ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelSubCategoriaFundo" runat="server" CssClass="labelRequired" Text="SubCategoria:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropSubCategoriaFundo" runat="server" ClientInstanceName="dropSubCategoriaFundo"
                                                                                            DataSourceID="EsDSSubCategoriaFundo" ShowShadow="False" CssClass="dropDownListLongo"
                                                                                            DropDownStyle="DropDown" IncrementalFilteringMode="Contains" TextField="Descricao"
                                                                                            ValueField="IdSubCategoria" Text='<%# Eval("IdSubCategoria") %>' ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelAgenteAdministrador" runat="server" CssClass="labelRequired"
                                                                                            Text="Administrador:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropAgenteAdministrador" runat="server" ClientInstanceName="dropAgenteAdministrador"
                                                                                            DataSourceID="EsDSAgenteMercado" ShowShadow="False" CssClass="dropDownListLongo"
                                                                                            TextField="Nome" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                                            ValueField="IdAgente" Text='<%# Eval("IdAgenteAdministrador") %>' ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelAgenteGestor" runat="server" CssClass="labelRequired" Text="Gestor:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropAgenteGestor" runat="server" ClientInstanceName="dropAgenteGestor"
                                                                                            DataSourceID="EsDSAgenteMercado" ShowShadow="False" CssClass="dropDownListLongo"
                                                                                            TextField="Nome" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                                            ValueField="IdAgente" Text='<%# Eval("IdAgenteGestor") %>' ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelAgenteCustodiante" runat="server" CssClass="labelRequired" Text="Custodiante:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropAgenteCustodiante" runat="server" ClientInstanceName="dropAgenteCustodiante"
                                                                                            DataSourceID="EsDSAgenteMercado" ShowShadow="False" CssClass="dropDownListLongo"
                                                                                            TextField="Nome" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                                            ValueField="IdAgente" Text='<%# Eval("IdAgenteCustodiante") %>' ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Grupo Econômico:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropGrupoEconomico" runat="server" ClientInstanceName="dropGrupoEconomico"
                                                                                            DataSourceID="EsDSGrupoEconomico" ShowShadow="False" CssClass="dropDownListLongo"
                                                                                            TextField="Nome" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                                            ValueField="IdGrupo" Text='<%# Eval("IdGrupoEconomico") %>' ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoRentabilidade" runat="server" CssClass="labelRequired" Text="Rentabilidade:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoRentabilidade" runat="server" ClientInstanceName="dropTipoRentabilidade"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("TipoRentabilidade") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="1o a 1o" />
                                                                                                <dxe:ListEditItem Value="2" Text="30 a 30" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelCalculaEnquadra" runat="server" CssClass="labelRequired" Text="Enquadramento:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCalculaEnquadra" runat="server" ClientInstanceName="dropCalculaEnquadra"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("CalculaEnquadra") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelCobraTaxaFiscalizacaoCVM" runat="server" CssClass="labelRequired"
                                                                                            Text="Cobra Taxa CVM:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCobraTaxaFiscalizacaoCVM" runat="server" ClientInstanceName="dropCobraTaxaFiscalizacaoCVM"
                                                                                            ShowShadow="False" SelectedIndex="0" CssClass="dropDownListCurto_6" Text='<%# Eval("CobraTaxaFiscalizacaoCVM") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) {
                                                                                                            if (s.GetSelectedIndex() == 1)
                                                                                                                { dropTipoCVM.SetSelectedIndex(-1); dropTipoCVM.SetEnabled(false); }
                                                                                                            else
                                                                                                                {dropTipoCVM.SetEnabled(true);}    
                                                                                                            }" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelTipoCVM" runat="server" CssClass="labelNormal" Text="Tipo CVM:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoCVM" runat="server" ClientInstanceName="dropTipoCVM"
                                                                                            ShowShadow="False" DropDownStyle="DropDown" CssClass="dropDownList" Text='<%# Eval("TipoCVM") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Companhia Aberta" />
                                                                                                <dxe:ListEditItem Value="2" Text="Sociedade Beneficiária" />
                                                                                                <dxe:ListEditItem Value="3" Text="Instituição Financeira" />
                                                                                                <dxe:ListEditItem Value="4" Text="Investidor Estrangeiro" />
                                                                                                <dxe:ListEditItem Value="5" Text="Fundo Investimento" />
                                                                                                <dxe:ListEditItem Value="6" Text="Fundo Aplicação Cotas" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>

                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelFundoExclusivo" runat="server" CssClass="labelNormal" Text="Fundo Exclusivo:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropFundoExclusivo" runat="server" ClientInstanceName="dropFundoExclusivo"
                                                                                            ShowShadow="False" SelectedIndex="0" CssClass="dropDownListCurto_6" Text='<%# Eval("FundoExclusivo") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                        <td class="td_Label_Longo">
                                                                                            <asp:Label ID="labelExcecaoRegraTxAdm" runat="server" CssClass="labelNormal" Text="Exceção Tx. Adm.:"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <dxe:ASPxComboBox ID="dropExcecaoRegraTxAdm" runat="server" ClientInstanceName="dropExcecaoRegraTxAdm"
                                                                                                ShowShadow="False" SelectedIndex="0" CssClass="dropDownListCurto_6" Text='<%# Eval("ExcecaoRegraTxAdm") %>'
                                                                                                ValueType="System.String">
                                                                                                <Items>
                                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                                </Items>
                                                                                            </dxe:ASPxComboBox>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelFIE" runat="server" CssClass="labelNormal" Text="FIE:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropFIE" runat="server" ClientInstanceName="dropFIE"
                                                                                            ShowShadow="False" SelectedIndex="0" CssClass="dropDownListCurto_6" Text='<%# Eval("FIE") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>

                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelExplodeCotadeFundos" runat="server" CssClass="labelRequired" Text="Explode Cota de Fundos:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropExplodeCotaDeFundo" runat="server" ClientInstanceName="dropExplodeCotaDeFundo"
                                                                                            ShowShadow="False" SelectedIndex="0" CssClass="dropDownListCurto_6" Text='<%# Eval("ExplodeCotasDeFundos") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelMesmoConglomerado" runat="server" CssClass="labelNormal" Text="Mesmo Conglomerado:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropMesmoConglomerado" runat="server" ClientInstanceName="dropMesmoConglomerado"
                                                                                            ShowShadow="False" SelectedIndex="0" CssClass="dropDownListCurto_6" Text='<%# Eval("MesmoConglomerado") %>'
                                                                                            ValueType="System.String" ClientSideEvents-ValueChanged="DropMesmoConglomerado_ValueChanged">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelCalculaMTM" runat="server" CssClass="labelRequired" Text="Calcula MTM:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCalculaMTM" runat="server" ClientInstanceName="dropCalculaMTM"
                                                                                            OnLoad="dropCalculaMTM_Load" ShowShadow="False" SelectedIndex="0" CssClass="dropDownListCurto_6"
                                                                                            Text='<%# Eval("CalculaMTM") %>' ValueType="System.String">
                                                                                            <ClientSideEvents Init="function(s, e) { if(gridCadastro.IsNewRowEditing() || s.GetSelectedItem() == null) dropCalculaMTM.SetValue(0); HabilitaGrupoMTM(s.GetSelectedItem().value); }" SelectedIndexChanged="function(s, e) { HabilitaGrupoMTM(s.GetSelectedItem().value); }" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelGrupoPerfilMTM" ClientInstanceName="labelGrupoPerfilMTM" runat="server" CssClass="labelRequired" Text="Grupo Perfil MTM:"></dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropGrupoPerfilMTM" runat="server" ClientInstanceName="dropGrupoPerfilMTM"
                                                                                            DataSourceID="EsDSGrupoPerfilMTM" ShowShadow="False" DropDownStyle="DropDown" CssClass="dropDownListLongo"
                                                                                            TextField="Descricao" ValueField="IdGrupoPerfilMTM" Text='<%# Eval("IdGrupoPerfilMTM") %>'
                                                                                            ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>

                                                                                </tr>
                                                                                <td class="td_Label_Longo">
                                                                                    <asp:Label ID="labelControladoriaAtivo" runat="server" CssClass="labelNormal" Text="Controladoria Ativo:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropControladoriaAtivo" runat="server" ClientInstanceName="dropControladoriaAtivo"
                                                                                        ShowShadow="False" SelectedIndex="0" CssClass="dropDownListCurto_6" Text='<%# Eval("ControladoriaAtivo") %>'
                                                                                        ValueType="System.String">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label_Longo">
                                                                                    <asp:Label ID="labelControladoriaPassivo" runat="server" CssClass="labelNormal" Text="Controladoria Passivo:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropControladoriaPassivo" runat="server" ClientInstanceName="dropControladoriaPassivo"
                                                                                        ShowShadow="False" SelectedIndex="0" CssClass="dropDownListCurto_6" Text='<%# Eval("ControladoriaPassivo") %>'
                                                                                        ValueType="System.String">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelCategoriaAnbima" runat="server" CssClass="labelNormal" Text="Categoria Anbima:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCategoriaAnbima" runat="server" ClientInstanceName="dropCategoriaAnbima"
                                                                                            ShowShadow="False" SelectedIndex="0" CssClass="dropDownList" Text='<%# Eval("CategoriaAnbima") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Administradores/Gestores" />
                                                                                                <dxe:ListEditItem Value="2" Text="EFPC" />
                                                                                                <dxe:ListEditItem Value="3" Text="Empresas" />
                                                                                                <dxe:ListEditItem Value="4" Text="Seguradoras" />
                                                                                                <dxe:ListEditItem Value="5" Text="FIDC" />
                                                                                                <dxe:ListEditItem Value="6" Text="Clubes de Investimento" />
                                                                                                <dxe:ListEditItem Value="7" Text="Outros Investidores Institucionais" />
                                                                                                <dxe:ListEditItem Value="8" Text="Pessoas Físicas" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelContratante" runat="server" CssClass="labelNormal" Text="Contratante:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropContratante" runat="server" ClientInstanceName="dropContratante"
                                                                                            DataSourceID="EsDSAgenteMercado" ShowShadow="False" CssClass="dropDownListLongo"
                                                                                            TextField="Nome" ValueField="IdAgente" ValueType="System.String" Text='<%# Eval("Contratante") %>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>


                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Parâmetros para Fundo/Clube" Name="tab3">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Código ISIN:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textCodigoISIN" runat="server" ClientInstanceName="textCodigoISIN"
                                                                                            MaxLength="12" CssClass="textValor_5" Text='<%#Eval("CodigoIsin") %>' />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCodigoAnbid" runat="server" CssClass="labelNormal" Text="Cód. Anbima:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textCodigoAnbid" runat="server" CssClass="textButtonEdit" ClientInstanceName="inputCodigoAnbid"
                                                                                            Text='<%# Eval("CodigoAnbid") %>' MaxLength="6">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents ButtonClick="function(s, e) {delete popupBuscaFundo.importaFundo; popupBuscaFundo.Show();}" />
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labeCodigoCETIP" runat="server" CssClass="labelNormal" Text="Código CETIP:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textCodigoCetip" runat="server" ClientInstanceName="textCodigoCetip"
                                                                                            MaxLength="14" CssClass="textValor_5" Text='<%#Eval("CodigoCetip") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="LabelCodigoBloomberg" runat="server" CssClass="labelNormal" Text="Código Bloomberg:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textCodigoBloomberg" runat="server" ClientInstanceName="textCodigoBloomberg"
                                                                                            MaxLength="25" CssClass="textValor_5" Text='<%#Eval("CodigoBloomberg") %>' />
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label14" runat="server" CssClass="labelNormal" Text="Perfil de Risco:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropPerfilRisco" runat="server" ClientInstanceName="dropPerfilRisco"
                                                                                            DataSourceID="EsDSPerfilRisco" ShowShadow="False" CssClass="dropDownListLongo"
                                                                                            TextField="Perfil" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                                            ValueField="IdPerfilInvestidor" Text='<%# Eval("PerfilRisco") %>' ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelValorMinimoAplicacao" runat="server" CssClass="labelNormal" Text="Mínimo Aplic.:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textValorMinimoAplicacao" runat="server" CssClass="textValor_4"
                                                                                            ClientInstanceName="textValorMinimoAplicacao" MaxLength="12" NumberType="integer"
                                                                                            Text="<%#Bind('ValorMinimoAplicacao')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelValorMinimoResgate" runat="server" CssClass="labelNormal" Text="Mínimo Resg.:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textValorMinimoResgate" runat="server" CssClass="textValor_4"
                                                                                            ClientInstanceName="textValorMinimoResgate" MaxLength="12" NumberType="integer"
                                                                                            Text="<%#Bind('ValorMinimoResgate')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelValorMinimoInicial" runat="server" CssClass="labelNormal" Text="Mínimo Inicial:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textValorMinimoInicial" runat="server" CssClass="textValor_4"
                                                                                            ClientInstanceName="textValorMinimoInicial" MaxLength="12" NumberType="integer"
                                                                                            Text="<%#Bind('ValorMinimoInicial')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelValorMinimoSaldo" runat="server" CssClass="labelNormal" Text="Mínimo Saldo:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textValorMinimoSaldo" runat="server" CssClass="textValor_4"
                                                                                            ClientInstanceName="textValorMinimoSaldo" MaxLength="12" NumberType="integer"
                                                                                            Text="<%#Bind('ValorMinimoSaldo')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelValorMaximoAplicacao" runat="server" CssClass="labelNormal" Text="Máximo Aplic.:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textValorMaximoAplicacao" runat="server" CssClass="textValor_4"
                                                                                            ClientInstanceName="textValorMaximoAplicacao" MaxLength="12" NumberType="integer"
                                                                                            Text="<%#Bind('ValorMaximoAplicacao')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelHorarioFim" runat="server" CssClass="labelNormal" Text="Horário Aplic.:" /></td>
                                                                                    <td>
                                                                                        <table border="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <dxe:ASPxCheckBox ID="chkHorarioFim" runat="server" ClientInstanceName="chkHorarioFim">
                                                                                                        <ClientSideEvents CheckedChanged="function(s, e){
                                                                                            textHorarioFim.SetEnabled(s.GetChecked()); }" />
                                                                                                    </dxe:ASPxCheckBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <dxe:ASPxTimeEdit ID="textHorarioFim" runat="server" ClientInstanceName="textHorarioFim"
                                                                                                        Width="80" OnDataBinding="textHorarioFim_DataBinding" DateTime="1900-01-01" Value="<%#Bind('HorarioFim')%>" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelHorarioFimResgate" runat="server" CssClass="labelNormal" Text="Horário Resg.:" /></td>
                                                                                    <td>
                                                                                        <table border="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <dxe:ASPxCheckBox ID="chkHorarioFimResgate" runat="server" ClientInstanceName="chkHorarioFimResgate">
                                                                                                        <ClientSideEvents CheckedChanged="function(s, e){
                                                                                            textHorarioFimResgate.SetEnabled(s.GetChecked()); }" />
                                                                                                    </dxe:ASPxCheckBox>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <dxe:ASPxTimeEdit ID="textHorarioFimResgate" runat="server" ClientInstanceName="textHorarioFimResgate"
                                                                                                        Width="80" OnDataBinding="textHorarioFimResgate_DataBinding" DateTime="1900-01-01"
                                                                                                        Value="<%#Bind('HorarioFimResgate')%>" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelLiberaAplicacao" runat="server" CssClass="labelRequired" Text="Aplicação Liberada:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropLiberaAplicacao" runat="server" ClientInstanceName="dropLiberaAplicacao"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("LiberaAplicacao") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelLiberaResgate" runat="server" CssClass="labelRequired" Text="Resgate Liberado:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropLiberaResgate" runat="server" ClientInstanceName="dropLiberaResgate"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("LiberaResgate") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>


                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelPossuiResgateAutomatico" runat="server" CssClass="labelNormal" Text="Resg. Automático:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropPossuiResgateAutomatico" runat="server" ClientInstanceName="dropPossuiResgateAutomatico"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("PossuiResgateAutomatico") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelDiasCotizacaoAplicacao" runat="server" CssClass="labelRequired"
                                                                                            Text="Dias Conversão Aplic.:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textDiasCotizacaoAplicacao" runat="server" CssClass="textCurto"
                                                                                            ClientInstanceName="textDiasCotizacaoAplicacao" MaxLength="3" NumberType="integer"
                                                                                            Text="<%#Bind('DiasCotizacaoAplicacao')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelDiasCotizacaoResgate" runat="server" CssClass="labelRequired"
                                                                                            Text="Dias Conversão Resg.:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textDiasCotizacaoResgate" runat="server" CssClass="textCurto"
                                                                                            ClientInstanceName="textDiasCotizacaoResgate" MaxLength="3" NumberType="integer"
                                                                                            Text="<%#Bind('DiasCotizacaoResgate')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="label7" runat="server" CssClass="labelRequired" Text="Cont. Dias Resgate:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropContagemDiasCotizacaoResgate" runat="server" ClientInstanceName="dropContagemDiasCotizacaoResgate"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_5" Text='<%# Eval("ContagemDiasConversaoResgate") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Úteis" />
                                                                                                <dxe:ListEditItem Value="2" Text="Corridos" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelDiasLiquidacaoAplicacao" runat="server" CssClass="labelRequired"
                                                                                            Text="Dias Liquid. Aplic.:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textDiasLiquidacaoAplicacao" runat="server" CssClass="textCurto"
                                                                                            ClientInstanceName="textDiasLiquidacaoAplicacao" MaxLength="3" NumberType="integer"
                                                                                            Text="<%#Bind('DiasLiquidacaoAplicacao')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelDiasLiquidacaoResgate" runat="server" CssClass="labelRequired"
                                                                                            Text="Dias Liquid. Resg.:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textDiasLiquidacaoResgate" runat="server" CssClass="textCurto"
                                                                                            ClientInstanceName="textDiasLiquidacaoResgate" MaxLength="3" NumberType="integer"
                                                                                            Text="<%#Bind('DiasLiquidacaoResgate')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label71" runat="server" CssClass="labelRequired" Text="Prioridade Resg.:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropPrioridadeOperacao" runat="server" ClientInstanceName="dropContagemDiasCotizacaoResgate"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_2" Text='<%# Eval("PrioridadeOperacao") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Resg. Antes" />
                                                                                                <dxe:ListEditItem Value="2" Text="Resg. Depois" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelTributacaoAtualTitle" ClientInstanceName="labelTributacaoAtual1"
                                                                                            runat="server" Text="Tributação Atual:" Visible="false">
                                                                                        </dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxLabel ID="labelTributacaoAtual" ClientInstanceName="labelTributacaoAtual" Visible="false"
                                                                                            runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelDistribuicaoDividendo" runat="server" CssClass="labelRequired"
                                                                                            Text="Distribuição Dividendo:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropDistribuicaoDividendo" runat="server" ClientInstanceName="dropDistribuicaoDividendo"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("DistribuicaoDividendo") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Não Distribui" />
                                                                                                <dxe:ListEditItem Value="2" Text="Distribui Cotas" />
                                                                                                <dxe:ListEditItem Value="3" Text="Distribui Financeiro" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoFundo" runat="server" CssClass="labelRequired" Text="Tipo de Fundo:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoFundo" runat="server" ClientInstanceName="dropTipoFundo"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("TipoFundo") %>'
                                                                                            OnLoad="dropTipoFundo_OnLoad" ValueType="System.String">
                                                                                            <ClientSideEvents Init="function(s, e) 
                                                                                                {
                                                                                                    if(gridCadastro.IsNewRowEditing())
                                                                                                        dropTipoFundo.SetValue(1);
                                                                                                }" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>

                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelRendimento" runat="server" CssClass="labelNormal" Text="Inf. Rentabilidade:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropRendimento" runat="server" ClientInstanceName="dropRendimento"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("Rendimento") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Cota Patrimonial" />
                                                                                                <dxe:ListEditItem Value="2" Text="Cota Ex" />
                                                                                                <dxe:ListEditItem Value="3" Text="Rendimentos" />
                                                                                            </Items>
                                                                                            <ClientSideEvents Init="function(s, e) 
                                                                                                {
                                                                                                    if(gridCadastro.IsNewRowEditing())
                                                                                                        dropRendimento.SetValue(1);
                                                                                                }" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label8" runat="server" CssClass="labelRequired" Text="Estratégia:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropEstrategia" runat="server" ClientInstanceName="dropEstrategia"
                                                                                            DataSourceID="EsDSEstrategia" ShowShadow="False" DropDownStyle="DropDown" CssClass="dropDownListCurto"
                                                                                            TextField="Descricao" ValueField="IdEstrategia" Text='<%# Eval("IdEstrategia") %>'
                                                                                            ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoVisaoFundo" runat="server" CssClass="labelRequired" Text="Distribuição Fundos:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoVisaoFundo" runat="server" ClientInstanceName="dropTipoVisaoFundo"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("TipoVisaoFundo") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Não Trata" />
                                                                                                <dxe:ListEditItem Value="2" Text="Visão Distribuidor" />
                                                                                                <dxe:ListEditItem Value="3" Text="Visão Gestor" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="labelTipoFundoAtualTitulo" ClientInstanceName="labelTipoFundoAtualTitle"
                                                                                            runat="server" Text="Tipo de Fundo Atual:" Visible="false" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxLabel ID="labelTipoFundoAtual" ClientInstanceName="labelTipoFundoAtual" runat="server" Visible="false" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblCodigoCDA" runat="server" CssClass="labelNormal" Text="Código CDA:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textCodigoCDA" runat="server" CssClass="textValor_4" ClientInstanceName="textCodigoCDA"
                                                                                            MaxLength="16" NumberType="Integer" Text="<%#Bind('CodigoCDA')%> ">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label9" runat="server" CssClass="labelNormal" Text="Código STI:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxTextBox ID="textCodigoSTI" runat="server" CssClass="textValor_4" ClientInstanceName="textCodigoSTI"
                                                                                            Text="<%#Bind('CodigoSTI')%> ">
                                                                                        </dxe:ASPxTextBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label36" runat="server" CssClass="labelNormal" Text="Influência Gestor Local – CVM:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropInfluenciaGestorLocalCvm" runat="server" ClientInstanceName="dropInfluenciaGestorLocalCvm"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("InfluenciaGestorLocalCvm") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="lblTipoVisualizacaoResgCotista" runat="server" CssClass="labelRequired" Text="Tipo Visualização Resg.Cotistas:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoVisualizacaoResgCotista" runat="server" ClientInstanceName="dropTipoVisualizacaoResgCotista"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("TipoVisualizacaoResgCotista") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="C" Text="Consolidado" />
                                                                                                <dxe:ListEditItem Value="A" Text="Analítico" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label37" runat="server" CssClass="labelNormal" Text="Investimento Coletivo – CVM:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropInvestimentoColetivoCvm" runat="server" ClientInstanceName="dropInvestimentoColetivoCvm"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("InvestimentoColetivoCvm") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblComeCotasEntreRegatesConversao" runat="server" CssClass="labelNormal" Text="Come-Cotas entre Resgate e Conversão:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropComeCotasEntreRegatesConversao" runat="server" ClientInstanceName="dropComeCotasEntreRegatesConversao"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("ComeCotasEntreRegatesConversao") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>


                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Parâmetros de Tributação de Cotas" Name="tabTributacao">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoCusto" runat="server" CssClass="labelRequired" Text="Custo:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoCusto" runat="server" ClientInstanceName="dropTipoCusto"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("TipoCusto") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Aplicação" />
                                                                                                <dxe:ListEditItem Value="2" Text="Médio" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelPrazoMedio" runat="server" CssClass="labelNormal" Text="Prazo Médio:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropPrazoMedio" runat="server" ClientInstanceName="dropPrazoMedio"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("CalculaPrazoMedio") %>'
                                                                                            ValueType="System.String" OnLoad="dropDistribuicaoDividendo_Load">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="" Text="" />
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelCalculaIOF" runat="server" CssClass="labelRequired" Text="Calcula IOF:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCalculaIOF" runat="server" ClientInstanceName="dropCalculaIOF"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("CalculaIOF") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelTipoTributacao" runat="server" CssClass="labelRequired" Text="Tributação IR:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropTipoTributacao" runat="server" ClientInstanceName="dropTipoTributacao"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("TipoTributacao") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Curto Prazo" />
                                                                                                <dxe:ListEditItem Value="2" Text="Longo Prazo" />
                                                                                                <dxe:ListEditItem Value="3" Text="Ações" />
                                                                                                <dxe:ListEditItem Value="4" Text="CP (s/ ComeCotas)" />
                                                                                                <dxe:ListEditItem Value="5" Text="LP (s/ ComeCotas)" />
                                                                                                <dxe:ListEditItem Value="20" Text="Isento" />
                                                                                                <dxe:ListEditItem Value="21" Text="Alíquota Específica" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblContaPrzIOFVirtual" runat="server" CssClass="labelRequired" Text="Cont. Prazo IOF Virtual (Come-Cotas):"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropContaPrzIOFVirtual" runat="server" ClientInstanceName="dropContaPrzIOFVirtual"
                                                                                            ShowShadow="False" CssClass="dropDownListLongo" Text='<%# Eval("ContaPrzIOFVirtual") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="0" Text="Soma Dias de Conv.Resg" />
                                                                                                <dxe:ListEditItem Value="1" Text="Cont. até últ. dia corrido de Mai/Nov" />
                                                                                                <dxe:ListEditItem Value="2" Text="Cont. até últ. dia útil de Mai/Nov" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblRealizaCompensacao" runat="server" CssClass="labelRequired" Text="Compensa de Prejuízo:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropRealizaCompensacao" runat="server" ClientInstanceName="dropRealizaCompensacao"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("RealizaCompensacaoDePrejuizo") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label10" runat="server" CssClass="labelRequired" Text="Forma Compensação Prejuízo:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropCompensacaoPrejuizo" runat="server" ClientInstanceName="dropCompensacaoPrejuizo"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto" Text='<%# Eval("CompensacaoPrejuizo") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Padrão" />
                                                                                                <dxe:ListEditItem Value="2" Text="Direto" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblProjecaoComeCotas" runat="server" CssClass="labelRequired" Text="Projeção IR ComeCotas:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropProjecaoIRComeCotas" runat="server" ClientInstanceName="dropProjecaoIRComeCotas"
                                                                                            ShowShadow="False" CssClass="dropDownListLongo" Text='<%# Eval("ProjecaoIRComeCotas") %>'
                                                                                            ValueType="System.String">
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                                                    {
                                                                                                         lblDiasAposComeCotasIR.SetVisible(false);                                                                                                 
                                                                                                         textDiasAposComeCotasIR.SetVisible(false);
                                                                                                         if(s.GetSelectedIndex() == 3)
                                                                                                         {
                                                                                                            lblDiasAposComeCotasIR.SetVisible(true);
                                                                                                            textDiasAposComeCotasIR.SetVisible(true);
                                                                                                         }                                                                                                     
                                                                                                    }" />
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="4a Feira Seguinte" />
                                                                                                <dxe:ListEditItem Value="2" Text="Liquidação Resg." />
                                                                                                <dxe:ListEditItem Value="3" Text="Por Decêndio" />
                                                                                                <dxe:ListEditItem Value="4" Text="Alguns dias após o Resgate" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="lblDiasAposComeCotasIR" ClientInstanceName="lblDiasAposComeCotasIR"
                                                                                            runat="server" CssClass="labelRequired" Text="Dias Após ComeCotas (IR):" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textDiasAposComeCotasIR" runat="server" Width="150px" ClientInstanceName="textDiasAposComeCotasIR"
                                                                                            Text='<%#(Eval("DiasAposComeCotasIR") != null ? Eval("DiasAposComeCotasIR") : "0")%>'
                                                                                            NumberType="Integer">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelProjecaoIRResgate" runat="server" CssClass="labelRequired" Text="Projeção IR Resgate:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropProjecaoIRResgate" runat="server" ClientInstanceName="dropProjecaoIRResgate"
                                                                                            ShowShadow="False" CssClass="dropDownListLongo" Text='<%# Eval("ProjecaoIRResgate") %>'
                                                                                            ValueType="System.String">
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                                                {
                                                                                                     lblDiasAposResgateIR.SetVisible(false);                                                                                                 
                                                                                                     textDiasAposResgateIR.SetVisible(false);
                                                                                                     if(s.GetSelectedIndex() == 3)
                                                                                                     {
                                                                                                        lblDiasAposResgateIR.SetVisible(true);
                                                                                                        textDiasAposResgateIR.SetVisible(true);
                                                                                                     }                                                                                                     
                                                                                                }" />
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="4a Feira Seguinte" />
                                                                                                <dxe:ListEditItem Value="2" Text="Liquidação Resg." />
                                                                                                <dxe:ListEditItem Value="3" Text="Por Decêndio" />
                                                                                                <dxe:ListEditItem Value="4" Text="Alguns dias após o Resgate" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="lblDiasAposResgateIR" ClientInstanceName="lblDiasAposResgateIR" runat="server"
                                                                                            CssClass="labelRequired" Text="Dias Após Resgate (IR):" />
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxSpinEdit ID="textDiasAposResgateIR" runat="server" Width="150px" ClientInstanceName="textDiasAposResgateIR"
                                                                                            Text='<%#(Eval("DiasAposResgateIR") != null ? Eval("DiasAposResgateIR") : "0")%>'
                                                                                            NumberType="Integer">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>

                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="lblExportaGalgo" runat="server" CssClass="labelNormal" Text="Exporta Galgo:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropExportaGalgo" runat="server" ClientInstanceName="dropExportaGalgo"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" Text='<%# Eval("ExportaGalgo") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>

                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelProjecaoIOFResgate" runat="server" CssClass="labelRequired" Text="Projeção IOF Resgate:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropProjecaoIOFResgate" runat="server" ClientInstanceName="dropProjecaoIOFResgate"
                                                                                            ShowShadow="False" CssClass="dropDownListLongo" Text='<%# Eval("ProjecaoIOFResgate") %>'
                                                                                            ValueType="System.String">
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                                                {
                                                                                                     lblDiasAposResgateIOF.SetVisible(false);                                                                                                 
                                                                                                     textDiasAposResgateIOF.SetVisible(false);
                                                                                                     if(s.GetSelectedIndex() == 3)
                                                                                                     {
                                                                                                        lblDiasAposResgateIOF.SetVisible(true);
                                                                                                        textDiasAposResgateIOF.SetVisible(true);
                                                                                                     }                                                                                                     
                                                                                                }" />
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="4a Feira Seguinte" />
                                                                                                <dxe:ListEditItem Value="2" Text="Liquidação Resg." />
                                                                                                <dxe:ListEditItem Value="3" Text="Por Decêndio" />
                                                                                                <dxe:ListEditItem Value="4" Text="Alguns dias após o Resgate" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="lblDiasAposResgateIOF" ClientInstanceName="lblDiasAposResgateIOF" runat="server"
                                                                                            CssClass="labelRequired" Text="Dias Após Resgate (IOF):" />
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxSpinEdit ID="textDiasAposResgateIOF" runat="server" Width="150px" ClientInstanceName="textDiasAposResgateIOF"
                                                                                            Text='<%#(Eval("DiasAposResgateIOF") != null ? Eval("DiasAposResgateIOF") : "0")%>'
                                                                                            NumberType="Integer">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="lblContagemPrazoIOF" runat="server" CssClass="labelRequired" Text="Cont. Prazo IOF:"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropContagemPrazoIOF" runat="server" ClientInstanceName="dropContagemPrazoIOF"
                                                                                            ShowShadow="False" Text='<%# Eval("ContagemPrazoIOF") %>' CssClass="dropDownListCurto_2"
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Mellon" />
                                                                                                <dxe:ListEditItem Value="2" Text="Prazo Corrido" />
                                                                                                <dxe:ListEditItem Value="3" Text="Direta" />
                                                                                                <dxe:ListEditItem Value="4" Text="Personalizado" />
                                                                                                <dxe:ListEditItem Value="5" Text="Santander" />
                                                                                            </Items>
                                                                                            <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                                                    {
                                                                                                         lblDataFimContIOF.SetVisible(false);  
                                                                                                         lblDataInicioContIOF.SetVisible(false);  
                                                                                                         dropDataFimContIOF.SetVisible(false);                                                                                                 
                                                                                                         dropDataInicioContIOF.SetVisible(false);
                                                                                                         if(s.GetSelectedItem().value == 4)
                                                                                                         {
                                                                                                            lblDataFimContIOF.SetVisible(true);  
                                                                                                            lblDataInicioContIOF.SetVisible(true);  
                                                                                                            dropDataFimContIOF.SetVisible(true);                                                                                                 
                                                                                                            dropDataInicioContIOF.SetVisible(true);
                                                                                                         }                                                                                                     
                                                                                                    }" />
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="lblDataInicioContIOF" ClientInstanceName="lblDataInicioContIOF" runat="server" CssClass="labelRequired" Text="Data Início Contagem IOF:"></dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropDataInicioContIOF" runat="server" ClientInstanceName="dropDataInicioContIOF"
                                                                                            ShowShadow="False" Text='<%# Eval("DataInicioContIOF") %>' CssClass="dropDownListLongo"
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Dia da Solicit. Aplicação" />
                                                                                                <dxe:ListEditItem Value="2" Text="D+1 Solicit. Aplicação" />
                                                                                                <dxe:ListEditItem Value="3" Text="Data Conversão Aplicação" />
                                                                                                <dxe:ListEditItem Value="4" Text="Liq.Fin.Aplicação" />

                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="lblDataFimContIOF" ClientInstanceName="lblDataFimContIOF" runat="server" CssClass="labelRequired" Text="Data Fim Contagem IOF:"></dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropDataFimContIOF" runat="server" ClientInstanceName="dropDataFimContIOF"
                                                                                            ShowShadow="False" Text='<%# Eval("DataFimContIOF") %>' CssClass="dropDownListLongo"
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Dia da Solicit. Resgate" />
                                                                                                <dxe:ListEditItem Value="2" Text="D+1 Solicit. Resgate" />
                                                                                                <dxe:ListEditItem Value="3" Text="Data Conversão Resgate" />
                                                                                                <dxe:ListEditItem Value="4" Text="Liq.Fin.Resgate" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>

                                                                <%--<dxtc:TabPage Text="Complemento" Name="tab6" Visible="false">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label12" runat="server" CssClass="labelRequired" Text="Nome Emissão Série:" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <asp:TextBox ID="textNomeEmissaoSerie" runat="server" CssClass="textLongo"
                                                                                            Width="240" Text='<%# Eval("NomeEmissaoSerie") %>'>
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Carteira Fundo Pai:" />
                                                                                    </td>
                                                                                    <td colspan="4">
                                                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" style="float:left"
                                                                                            ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteiraFundoPai")%>' MaxLength="10"
                                                                                            NumberType="Integer">
                                                                                            <Buttons>
                                                                                                <dxe:EditButton>
                                                                                                </dxe:EditButton>
                                                                                            </Buttons>
                                                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                                                ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteira);}"
                                                                                                NumberChanged=" function(s,e) { OnCarteiraChange(s); }" />
                                                                                        </dxe:ASPxSpinEdit>

                                                                                        <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("NomeCarteiraFundoPai")%>' ></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label36" runat="server" CssClass="labelNormal" Text="Código Titulo Bovespa:" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxTextBox ID="textCodigoTituloBovespa" runat="server" CssClass="textLongo" MaxLength="20"
                                                                                            Text='<%#Eval("CodigoTituloBovespa")%>' />
                                                                                    </td>
                                                                                    

                                                                                </tr> 
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label37" runat="server" CssClass="labelRequired" Text="Multiplas Emissões?" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropMultiplasEmissoes" runat="server" ClientInstanceName="dropMultiplasEmissoes"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label38" runat="server" CssClass="labelRequired" Text="Multiplas Emissões?" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropMultiplasSeries" runat="server" ClientInstanceName="dropMultiplasSeries"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label39" runat="server" CssClass="labelRequired" Text="Data Base Início:" /></td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataBaseInicioEmissaoSerie" runat="server" ClientInstanceName="textDataBaseInicioEmissaoSerie"
                                                                                            Value='<%#Eval("DataBaseInicioEmissaoSerie")%>' />
                                                                                    </td>
                                                                                
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label40" runat="server" CssClass="labelRequired" Text="Emissao Encerrável?" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropEmissaoEncerravel" runat="server" ClientInstanceName="dropEmissaoEncerravel"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label41" runat="server" CssClass="labelNormal" Text="Data Fim:" /></td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="ASPxDateEdit2" runat="server" ClientInstanceName="textDataInicio"
                                                                                            Value='<%#Eval("DataFimEmissao")%>' />
                                                                                    </td>
                                                                                
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label42" runat="server" CssClass="labelRequired" Text="Classe da Cota:"></asp:Label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxComboBox ID="ASPxComboBox1" runat="server" ClientInstanceName="dropIndiceBenchmark"
                                                                                            DataSourceID="EsDSClasseCota" ShowShadow="False" CssClass="dropDownList" TextField="Descricao"
                                                                                            ValueField="IdClasseCota" Text='<%# Eval("IdClasseCota") %>' ValueType="System.String">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <dxe:ASPxLabel ID="ASPxLabel43" runat="server" CssClass="labelNormal" Text="Quantidade Cotas Aportadas:"></dxe:ASPxLabel>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textQuantidadeCotasAportadas" runat="server" ClientInstanceName="textQuantidadeCotasAportadas"
                                                                                            CssClass="textValor_5" Text='<%# Eval("QuantidadeCotasAportadas") %>' NumberType="Float" MaxLength="18"
                                                                                            DecimalPlaces="12">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label43" runat="server" CssClass="labelRequired" Text="Permite Rendimento?" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropPermiteRendimento" runat="server" ClientInstanceName="dropPermiteRendimento"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label44" runat="server" CssClass="labelRequired" Text="Permite Amortização?" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropPermiteAmortizacao" runat="server" ClientInstanceName="dropPermiteAmortizacao"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label46" runat="server" CssClass="labelNormal" Text="Periodicidade Cota" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropPeriodicidadeCota" runat="server" ClientInstanceName="dropPeriodicidadeCota"
                                                                                            ShowShadow="False" CssClass="dropDownList" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="" Text="Não definido" />
                                                                                                <dxe:ListEditItem Value="1" Text="Diário" />
                                                                                                <dxe:ListEditItem Value="2" Text="Semanal" />
                                                                                                <dxe:ListEditItem Value="3" Text="Mensal" />
                                                                                                <dxe:ListEditItem Value="4" Text="Bimestral" />
                                                                                                <dxe:ListEditItem Value="5" Text="Trimestral" />
                                                                                                <dxe:ListEditItem Value="6" Text="Quadrimestral" />
                                                                                                <dxe:ListEditItem Value="7" Text="Semestral" />
                                                                                                <dxe:ListEditItem Value="8" Text="Anual" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label47" runat="server" CssClass="labelNormal" Text="Periodicidade Rendimento" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropPeriodicidadeRendimento" runat="server" ClientInstanceName="dropPeriodicidadeRendimento"
                                                                                            ShowShadow="False" CssClass="dropDownList" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="" Text="Não definido" />
                                                                                                <dxe:ListEditItem Value="1" Text="Diário" />
                                                                                                <dxe:ListEditItem Value="2" Text="Semanal" />
                                                                                                <dxe:ListEditItem Value="3" Text="Mensal" />
                                                                                                <dxe:ListEditItem Value="4" Text="Bimestral" />
                                                                                                <dxe:ListEditItem Value="5" Text="Trimestral" />
                                                                                                <dxe:ListEditItem Value="6" Text="Quadrimestral" />
                                                                                                <dxe:ListEditItem Value="7" Text="Semestral" />
                                                                                                <dxe:ListEditItem Value="8" Text="Anual" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td> 
                                                                                    
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label48" runat="server" CssClass="labelRequired" Text="Início Periodicidade Rendimento:" /></td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textInicioPeriodicidadeRendimento" runat="server" ClientInstanceName="textInicioPeriodicidadeRendimento"
                                                                                            Value='<%#Eval("InicioPeriodicidadeRendimento")%>' />
                                                                                    </td>
                                                                                                                                                                       
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label49" runat="server" CssClass="labelNormal" Text="Periodicidade Amortização" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropPeriodicidadeAmortizacao" runat="server" ClientInstanceName="dropPeriodicidadeAmortizacao"
                                                                                            ShowShadow="False" CssClass="dropDownList" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="" Text="Não definido" />
                                                                                                <dxe:ListEditItem Value="1" Text="Diário" />
                                                                                                <dxe:ListEditItem Value="2" Text="Semanal" />
                                                                                                <dxe:ListEditItem Value="3" Text="Mensal" />
                                                                                                <dxe:ListEditItem Value="4" Text="Bimestral" />
                                                                                                <dxe:ListEditItem Value="5" Text="Trimestral" />
                                                                                                <dxe:ListEditItem Value="6" Text="Quadrimestral" />
                                                                                                <dxe:ListEditItem Value="7" Text="Semestral" />
                                                                                                <dxe:ListEditItem Value="8" Text="Anual" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td> 
                                                                                    
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label50" runat="server" CssClass="labelNormal" Text="Início Periodicidade Amortização:" /></td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textInicioPeriodicidadeAmortizacao" runat="server" ClientInstanceName="textInicioPeriodicidadeAmortizacao"
                                                                                            Value='<%#Eval("InicioPeriodicidadeAmortizacao")%>' />
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label51" runat="server" CssClass="labelRequired" Text="Retem IR Fonte Rendimento?" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropRetemIRFonteRendimento" runat="server" ClientInstanceName="dropRetemIRFonteRendimento"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="Label52" runat="server" CssClass="labelNormal" Text="Executa Resgate Fim Emissão?" />
                                                                                    </td>
                                                                                    
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropExecutaResgateFimEmissao" runat="server" ClientInstanceName="dropExecutaResgateFimEmissao"
                                                                                            ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                                <dxe:ListEditItem Value="N" Text="Não" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>  --%>

                                                                <dxtc:TabPage Text="Fundos Off Shore - Classe" Name="tab4">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelNomeClasse" runat="server" CssClass="labelRequired" Text="Nome:" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxTextBox ID="textNomeClasse" runat="server" ClientInstanceName="textNomeClasse"
                                                                                            Size="100" MaxLength="100" CssClass="textNormal" Text='<%#Eval("Nome1") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelDataInicioClasse" runat="server" CssClass="labelRequired" Text="Data Inicio:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textDataInicioClasse" runat="server" CssClass="textButtonEdit"
                                                                                            ClientInstanceName="textDataInicioClasse" Value='<%# Eval("DataInicio") %>'>
                                                                                        </dxe:ASPxDateEdit>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxCheckBox ID="chbSerieUnica" ClientInstanceName="chbSerieUnica" runat="server"
                                                                                            ValueChecked="true" ValueUnchecked="false" Text="Série Única" Checked='<%# Eval("SerieUnica") == null ? false : Eval("SerieUnica") == "S" ? true : false %>'>
                                                                                        </dxe:ASPxCheckBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label33" runat="server" CssClass="labelRequired" Text="Frequência Série:"> </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropFrequencia" runat="server" ClientInstanceName="dropFrequencia"
                                                                                            ShowShadow="False" CssClass="dropDownList" Text='<%# Eval("FrequenciaSerie") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Mensal" />
                                                                                                <dxe:ListEditItem Value="2" Text="Bimestral" />
                                                                                                <dxe:ListEditItem Value="3" Text="Trimestral" />
                                                                                                <dxe:ListEditItem Value="4" Text="Quadrimestral" />
                                                                                                <dxe:ListEditItem Value="6" Text="Semestral" />
                                                                                                <dxe:ListEditItem Value="12" Text="Anual" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label15" runat="server" CssClass="labelNormal" Text="Política Investimentos:" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <dxe:ASPxTextBox ID="textPoliticaInvestimentos" runat="server" ClientInstanceName="textPoliticaInvestimentos"
                                                                                            MaxLength="100" CssClass="textNormal" Text='<%#Eval("PoliticaInvestimentos") %>' />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label16" runat="server" CssClass="labelNormal" Text="Moeda:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropMoeda" DataSourceID="EsDSMoedas" CssClass="dropDownList"
                                                                                            runat="server" ShowShadow="False" TextField="Nome" ValueField="IdMoeda" ValueType="System.String"
                                                                                            ClientInstanceName="dropMoeda" Text='<%# Eval("Moeda") %>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label17" runat="server" CssClass="labelNormal" Text="Domicílio:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropDomicilio" DataSourceID="EsDSLocalNegociacao" CssClass="dropDownList"
                                                                                            runat="server" ShowShadow="False" TextField="Descricao" ValueField="IdLocalNegociacao"
                                                                                            ValueType="System.String" ClientInstanceName="dropDomicilio" Text='<%# Eval("Domicilio") %>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label18" runat="server" CssClass="labelNormal" Text="Taxa Adm:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textTaxaAdm" runat="server" ClientInstanceName="textTaxaAdm"
                                                                                            Text='<%# Eval("TaxaAdm")%>' NumberType="Float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label19" runat="server" CssClass="labelNormal" Text="Taxa Custódia:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textTaxaCustodia" runat="server" ClientInstanceName="textTaxaCustodia"
                                                                                            Text='<%# Eval("TaxaCustodia")%>' NumberType="Float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label20" runat="server" CssClass="labelNormal" Text="Taxa Performance:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textTaxaPerformance" runat="server" ClientInstanceName="textTaxaPerformance"
                                                                                            Text='<%# Eval("TaxaPerformance")%>' NumberType="Float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label21" runat="server" CssClass="labelNormal" Text="Indexador Perf.:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropIndexadorPerf" DataSourceID="EsDSIndice" CssClass="dropDownList"
                                                                                            runat="server" ShowShadow="False" TextField="Descricao" ValueField="IdIndice"
                                                                                            ValueType="System.String" ClientInstanceName="dropIndexadorPerf" Text='<%# Eval("IndexadorPerf") %>'>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label22" runat="server" CssClass="labelNormal" Text="Freq. Calc. Performance:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropFreqCalcPerformance" runat="server" ClientInstanceName="dropFreqCalcPerformance"
                                                                                            ShowShadow="False" CssClass="dropDownList" Text='<%# Eval("FreqCalcPerformance") %>'
                                                                                            ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Mensal" />
                                                                                                <dxe:ListEditItem Value="2" Text="Bimestral" />
                                                                                                <dxe:ListEditItem Value="3" Text="Trimestral" />
                                                                                                <dxe:ListEditItem Value="4" Text="Quadrimestral" />
                                                                                                <dxe:ListEditItem Value="6" Text="Semestral" />
                                                                                                <dxe:ListEditItem Value="12" Text="Anual" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label23" runat="server" CssClass="labelNormal" Text="Carência Resgate (Lock Up):" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textLockUp" runat="server" ClientInstanceName="textLockUp"
                                                                                            Text='<%# Eval("LockUp")%>' NumberType="Integer">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label24" runat="server" CssClass="labelNormal" Text="Hard/Soft:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropHardSoft" runat="server" ClientInstanceName="dropHardSoft"
                                                                                            ShowShadow="False" CssClass="dropDownList" Text='<%# Eval("HardSoft") %>' ValueType="System.String">
                                                                                            <Items>
                                                                                                <dxe:ListEditItem Value="1" Text="Hard" />
                                                                                                <dxe:ListEditItem Value="2" Text="Soft" />
                                                                                            </Items>
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label25" runat="server" CssClass="labelNormal" Text="Penalidade Resgate(%):" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textPenalidadeResgate" runat="server" ClientInstanceName="textPenalidadeResgate"
                                                                                            Text='<%# Eval("PenalidadeResgate")%>' NumberType="Float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label26" runat="server" CssClass="labelNormal" Text="Vl. Mínimo Aplicação:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textVlMinimoAplicacao" runat="server" ClientInstanceName="textVlMinimoAplicacao"
                                                                                            Text='<%# Eval("VlMinimoAplicacao")%>' NumberType="Float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label27" runat="server" CssClass="labelNormal" Text="Vl. Mínimo Resgate:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textVlMinimoResgate" runat="server" ClientInstanceName="textVlMinimoResgate"
                                                                                            Text='<%# Eval("VlMinimoResgate")%>' NumberType="Float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label28" runat="server" CssClass="labelNormal" Text="Vl. Mínimo Permanência:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textVlMinimoPermanencia" runat="server" ClientInstanceName="textVlMinimoPermanencia"
                                                                                            Text='<%# Eval("VlMinimoPermanencia")%>' NumberType="Float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label34" runat="server" CssClass="labelNormal" Text="Qtd Dias Conv. Aplicação (Notice):" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textQtdDiasConvAplicacao" runat="server" ClientInstanceName="textQtdDiasConvAplicacao"
                                                                                            Text='<%# Eval("QtdDiasConvAplicacao")%>' NumberType="Integer">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label35" runat="server" CssClass="labelNormal" Text="Qtd Dias Liq. Fin. Aplic.(Antes NAV):" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textQtdDiasLiqFinAplic" runat="server" ClientInstanceName="textQtdDiasLiqFinAplic"
                                                                                            Text='<%# Eval("QtdDiasLiqFinAplic")%>' NumberType="Integer">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label29" runat="server" CssClass="labelNormal" Text="Qtd Dias Conv. Resgate (Notice):" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textQtdDiasConvResgate" runat="server" ClientInstanceName="textQtdDiasConvResgate"
                                                                                            Text='<%# Eval("QtdDiasConvResgate")%>' NumberType="Integer">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label30" runat="server" CssClass="labelNormal" Text="Qtd Dias Liq. Fin.(após NAV):" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textQtdDiasLiqFin" runat="server" ClientInstanceName="textQtdDiasLiqFin"
                                                                                            Text='<%# Eval("QtdDiasLiqFin")%>' NumberType="Integer">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label31" runat="server" CssClass="labelNormal" Text="Hold Back(%):" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textHoldBack" runat="server" ClientInstanceName="textHoldBack"
                                                                                            Text='<%# Eval("HoldBack")%>' NumberType="Float" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label32" runat="server" CssClass="labelNormal" Text="Aniv. Hold Back(dd/MM):" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxDateEdit ID="textAnivHoldBack" runat="server" ClientInstanceName="textAnivHoldBack"
                                                                                            Value='<%# Eval("AnivHoldBack") %>' DisplayFormatString="dd/MM" EditFormatString="dd/MM">
                                                                                        </dxe:ASPxDateEdit>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <dxe:ASPxCheckBox ID="textSidePocket" ClientInstanceName="textSidePocket" runat="server"
                                                                                            ValueChecked="true" ValueUnchecked="false" Text="Side Pocket" Checked='<%# Eval("SidePocket") == null ? false : Eval("SidePocket") == "S" ? true : false %>'>
                                                                                        </dxe:ASPxCheckBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelPrazoValidadePrevia" runat="server" CssClass="labelNormal" Text="Prazo validade prévia (DC):"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textPrazoValidadePrevia" runat="server" CssClass="textCurto"
                                                                                            ClientInstanceName="textPrazoValidadePrevia" MaxLength="3" NumberType="integer"
                                                                                            Text="<%#Bind('PrazoValidadePrevia')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="labelIndexadorPrevia" runat="server" CssClass="labelNormal" Text="Indexador Prévia:" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxComboBox ID="dropIndexadorPrevia" runat="server" ClientInstanceName="dropIndexadorPrevia"
                                                                                            ShowShadow="False" CssClass="dropDownList" Text='<%# Eval("IdIndicePrevia") %>'
                                                                                            DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice">
                                                                                        </dxe:ASPxComboBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label_Longo">
                                                                                        <asp:Label ID="labelPrazoRepeticaoCotas" runat="server" CssClass="labelNormal" Text="Prazo repetição prévia (DC):"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <dxe:ASPxSpinEdit ID="textPrazoRepeticaoCotas" runat="server" CssClass="textCurto"
                                                                                            ClientInstanceName="textPrazoRepeticaoCotas" MaxLength="3" NumberType="integer"
                                                                                            Text="<%#Bind('PrazoRepeticaoCotas')%>">
                                                                                        </dxe:ASPxSpinEdit>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Fundos Off Shore - Séries" Name="tab5">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <div class="divDataGrid">
                                                                                            <dxwgv:ASPxGridView ID="gridSeries" ClientInstanceName="gridSeries" runat="server"
                                                                                                KeyFieldName="IdSeriesOffShore" DataSourceID="EsDSSeriesOffShore" OnRowUpdating="gridSeries_RowUpdating"
                                                                                                OnRowInserting="gridSeries_RowInserting" OnRowValidating="gridSeries_RowValidating"
                                                                                                AutoGenerateColumns="False" Width="100%" SettingsText-CommandCancel="Cancelar"
                                                                                                SettingsText-CommandUpdate="Atualizar" OnCustomUnboundColumnData="gridSeries_CustomUnboundColumnData">
                                                                                                <Columns>
                                                                                                    <dxwgv:GridViewCommandColumn Caption="Opções" Width="5%" VisibleIndex="0" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center" ShowEditButton="true" />
                                                                                                    <dxwgv:GridViewDataTextColumn FieldName="IdSeriesOffShore" Caption="ID" VisibleIndex="1"
                                                                                                        UnboundType="String" Width="5%" ReadOnly="true" HeaderStyle-HorizontalAlign="Center"
                                                                                                        CellStyle-HorizontalAlign="Center">
                                                                                                        <EditFormSettings Visible="False" />
                                                                                                    </dxwgv:GridViewDataTextColumn>
                                                                                                    <dxwgv:GridViewDataTextColumn FieldName="Mnemonico" Caption="Mnemônico" VisibleIndex="2"
                                                                                                        Width="40%" UnboundType="String" EditFormSettings-Caption="Mnemônico da Série:"
                                                                                                        EditCellStyle-HorizontalAlign="Left" EditFormCaptionStyle-HorizontalAlign="Right">
                                                                                                    </dxwgv:GridViewDataTextColumn>
                                                                                                    <dxwgv:GridViewDataTextColumn FieldName="Descricao" VisibleIndex="2" Width="45%"
                                                                                                        Visible="false" UnboundType="String" EditFormSettings-Caption="Descrição Série:"
                                                                                                        EditCellStyle-HorizontalAlign="Left" EditFormCaptionStyle-HorizontalAlign="Right">
                                                                                                        <EditFormSettings Visible="True" />
                                                                                                    </dxwgv:GridViewDataTextColumn>
                                                                                                    <dxwgv:GridViewDataTextColumn FieldName="VlCotaInicial" VisibleIndex="5" Visible="false"
                                                                                                        EditFormSettings-Caption="Vl. Cota Inicial:" EditFormCaptionStyle-HorizontalAlign="Right">
                                                                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" DisplayFormatInEditMode="true">
                                                                                                        </PropertiesTextEdit>
                                                                                                        <EditFormSettings Visible="True" />
                                                                                                    </dxwgv:GridViewDataTextColumn>
                                                                                                    <dxwgv:GridViewDataTextColumn FieldName="Isin" VisibleIndex="6" Visible="false" EditFormSettings-Caption="ISIN:"
                                                                                                        EditFormCaptionStyle-HorizontalAlign="Right">
                                                                                                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.000000000000;(#,##0.000000000000);0.000000000000}" DisplayFormatInEditMode="true"></PropertiesTextEdit>
                                                                                                        <EditFormSettings Visible="True" />
                                                                                                    </dxwgv:GridViewDataTextColumn>
                                                                                                    <dxwgv:GridViewDataTextColumn FieldName="DataRollUp" ReadOnly="true" VisibleIndex="7"
                                                                                                        Visible="false" EditFormSettings-Caption="Data Roll Up:" EditFormCaptionStyle-HorizontalAlign="Right">
                                                                                                        <EditFormSettings Visible="True" />
                                                                                                    </dxwgv:GridViewDataTextColumn>
                                                                                                    <dxwgv:GridViewDataCheckColumn FieldName="SerieInativa" Caption="Inativa" VisibleIndex="8"
                                                                                                        Width="5%" EditFormSettings-Caption="Inativa:" ReadOnly="true" EditFormCaptionStyle-HorizontalAlign="Right"
                                                                                                        UnboundType="Boolean" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                                                                        <DataItemTemplate>
                                                                                                            <dxe:ASPxCheckBox runat="server" Checked='<%# Eval("SerieInativa") == null ? false : (bool)Eval("SerieInativa") == true ? true : false %>' />
                                                                                                        </DataItemTemplate>
                                                                                                    </dxwgv:GridViewDataCheckColumn>
                                                                                                    <dxwgv:GridViewDataCheckColumn FieldName="Base" Caption="Série Base" VisibleIndex="9"
                                                                                                        Width="5%" EditFormSettings-Caption="Série Base:" ReadOnly="true" EditFormCaptionStyle-HorizontalAlign="Right"
                                                                                                        UnboundType="Boolean" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                                                                                        <DataItemTemplate>
                                                                                                            <dxe:ASPxCheckBox runat="server" Checked='<%# Eval("Base") == null ? false : (bool)Eval("Base") == true ? true : false %>'
                                                                                                                Enabled="false" />
                                                                                                        </DataItemTemplate>
                                                                                                    </dxwgv:GridViewDataCheckColumn>
                                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="PrazoRepeticaoCotas" VisibleIndex="10"
                                                                                                        UnboundType="String" PropertiesSpinEdit-NumberType="Integer" Visible="false"
                                                                                                        Caption="Prazo repetição cotas" Width="10%" EditFormSettings-Caption="Prazo repetição cotas:">
                                                                                                        <EditFormSettings Visible="true" />
                                                                                                        <EditFormCaptionStyle HorizontalAlign="Right" />
                                                                                                        <CellStyle Font-Bold="true" />
                                                                                                        <EditCellStyle Font-Bold="true" HorizontalAlign="Left" />
                                                                                                    </dxwgv:GridViewDataSpinEditColumn>
                                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="PrazoValidadePrevia" VisibleIndex="11"
                                                                                                        UnboundType="String" PropertiesSpinEdit-NumberType="Integer" Visible="false"
                                                                                                        Caption="Prazo validade prévia" Width="10%" EditFormSettings-Caption="Prazo validade prévia:">
                                                                                                        <EditFormSettings Visible="true" />
                                                                                                        <EditFormCaptionStyle HorizontalAlign="Right" />
                                                                                                        <CellStyle Font-Bold="true" />
                                                                                                        <EditCellStyle Font-Bold="true" HorizontalAlign="Left" />
                                                                                                    </dxwgv:GridViewDataSpinEditColumn>
                                                                                                    <dxwgv:GridViewDataComboBoxColumn FieldName="IdIndicePrevia" Caption="Indexador prévia"
                                                                                                        VisibleIndex="12" PropertiesComboBox-DataSourceID="EsDSIndice" EditFormSettings-Caption="Indexador prévia:"
                                                                                                        PropertiesComboBox-TextField="Descricao" Visible="false" PropertiesComboBox-ValueField="IdIndice">
                                                                                                        <EditFormSettings Visible="true" />
                                                                                                    </dxwgv:GridViewDataComboBoxColumn>
                                                                                                </Columns>
                                                                                                <SettingsCommandButton>
                                                                                                    <EditButton Text="Editar" />
                                                                                                </SettingsCommandButton>
                                                                                            </dxwgv:ASPxGridView>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                                <dxtc:TabPage Text="Complemento" Name="tab6">
                                                                    <ContentCollection>
                                                                        <dxw:ContentControl runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Nome Emissão Série:" />
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        <asp:TextBox ID="textNomeEmissaoSerie" runat="server" CssClass="textNome" Enabled="false"
                                                                                            Width="240" Text='<%# Eval("NomeEmissaoSerie") %>'>
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="td_Label">
                                                                                        <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Nome Emissão Série:" />
                                                                                    </td>


                                                                                </tr>

                                                                            </table>
                                                                        </dxw:ContentControl>
                                                                    </ContentCollection>
                                                                </dxtc:TabPage>
                                                            </TabPages>
                                                            <Paddings PaddingLeft="0px" />
                                                            <TabStyle HorizontalAlign="Center" />
                                                            <ContentStyle>
                                                                <Border BorderColor="#4986A2" />
                                                            </ContentStyle>
                                                        </dxtc:ASPxPageControl>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal11" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="850px" />
                                        <SettingsCustomizationWindow Enabled="True" />
                                        <SettingsText CustomizationWindowCaption="Lista de campos" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png" />
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Importando, aguarde..." ClientInstanceName="LoadingPanel" Modal="True" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanelLote" runat="server" Text="Processando, aguarde..." ClientInstanceName="LoadingPanelLote" Modal="True" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanelFundos" runat="server" Text="Importando, aguarde..." ClientInstanceName="LoadingPanelFundos" Modal="True" />

        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" />

        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
        <cc1:esDataSource ID="EsDSCategoriaFundo" runat="server" OnesSelect="EsDSCategoriaFundo_esSelect" />
        <cc1:esDataSource ID="EsDSSubCategoriaFundo" runat="server" OnesSelect="EsDSSubCategoriaFundo_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoEconomico" runat="server" OnesSelect="EsDSGrupoEconomico_esSelect" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSEstrategia" runat="server" OnesSelect="EsDSEstrategia_esSelect" />
        <cc1:esDataSource ID="EsDSSeriesOffShore" runat="server" OnesSelect="EsDSSeries_esSelect" />
        <cc1:esDataSource ID="EsDSMoedas" runat="server" OnesSelect="EsDSMoedas_esSelect" />
        <cc1:esDataSource ID="EsDSPerfilRisco" runat="server" OnesSelect="EsDSPerfilRisco_esSelect" />
        <cc1:esDataSource ID="EsDSSuitabilityHistorico" runat="server" OnesSelect="EsDSSuitabilityHistorico_esSelect" />
        <cc1:esDataSource ID="EsDSLocalNegociacao" runat="server" OnesSelect="EsDSLocalNegociacao_esSelect" />
        <cc1:esDataSource ID="EsDSClasseCota" runat="server" OnesSelect="EsDSClasseCota_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoPerfilMTM" runat="server" OnesSelect="EsDSGrupoPerfilMTM_esSelect" />
    </form>
</body>
</html>
