﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_GerOperacaoBMF, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
            
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }    
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }    
    function OnGetDataAtivoBMF(data) {
        btnEditAtivoBMF.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditAtivoBMF.GetValue());
        popupAtivoBMF.HideWindow();
        btnEditAtivoBMF.Focus();
    }    
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }                   
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                            
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                  
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];            
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            } 
            else
            {                                        
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                if (gridCadastro.cp_EditVisibleIndex == 'new')
                {
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente, textData);
                }
                else
                {
                    OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
                }
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            var resultSplit = e.result.split('|');
            e.result = resultSplit[0];
            
            if (e.result == '' && btnEditAtivoBMF.GetValue() != null)
            {
                popupMensagemAtivoBMF.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBMF.ShowAtElementByID(btnEditAtivoBMF.name);
                btnEditAtivoBMF.SetValue(''); 
                btnEditAtivoBMF.Focus();                 
            }
                                    
            var textTipoMercado = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textTipoMercado');
            var textPeso = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textPeso');
            
            if (textTipoMercado != null && textPeso != null)
            {
                if (resultSplit[1] != null)            
                {
                    textTipoMercado.value = resultSplit[1];
                    textPeso.value = resultSplit[2];                
                }
                else
                {
                    textTipoMercado.value = '';
                    textPeso.value = '';                
                }
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Operações de BMF (Gerencial)"></asp:Label>
    </div>
        
    <div id="mainContent">
                    
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td  colspan="2" width="450">
                                <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNormal" Text="Ativo/Série:"></asp:Label>
                            </td>
                                           
                            <td colspan="2">
                                <asp:TextBox ID="textCdAtivo" runat="server" CssClass="textCurto"></asp:TextBox>
                                <asp:TextBox ID="textSerie" runat="server" CssClass="textCurto"></asp:TextBox>
                            </td>    
                        </tr>
                        
                        <tr>
                            <td>                
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                            </td>
                                                
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />   
                            </td>  
                            
                            <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                            </td>                                                                            
                        </tr>
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
            
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal5" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="IdOperacao" DataSourceID="EsDSGerOperacaoBMF"                        
                        OnRowUpdating="gridCadastro_RowUpdating"
                        OnRowInserting="gridCadastro_RowInserting"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnPreRender="gridCadastro_PreRender"
                        OnCustomJSProperties="gridCadastro_CustomJSProperties"     
                        OnHtmlRowPrepared="gridCadastro_HtmlRowPrepared"   
                        OnBeforeGetCallbackResult="gridCadastro_PreRender"                     
                        >     
                    
                    <Columns>                    
                                             
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdCliente" Caption="Id" VisibleIndex="2" Width="10%" CellStyle-HorizontalAlign="left" />                        
                        <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="19%" />                                            
                        <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="4" Width="10%"/>
                        
                        <dxwgv:GridViewDataComboBoxColumn Caption="Trader/Estrat." FieldName="IdTrader" VisibleIndex="5" Width="20%">
                            <EditFormSettings Visible="False" />
                            <PropertiesComboBox DataSourceID="EsDSTrader" TextField="Nome" ValueField="IdTrader">
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="CdAtivoBMF" Caption="Código" VisibleIndex="6" Width="5%"/>                        
                        <dxwgv:GridViewDataColumn FieldName="Serie" Caption="Série" VisibleIndex="7" Width="5%"/>
                        
                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoOperacao" Caption="Tipo" VisibleIndex="8" Width="8%">
                        <PropertiesComboBox>
                        <Items>
                            <dxe:ListEditItem Value="C" Text="Compra" />
                            <dxe:ListEditItem Value="V" Text="Venda" />
                            <dxe:ListEditItem Value="CD" Text="Compra DT" />
                            <dxe:ListEditItem Value="VD" Text="Venda DT" />                            
                        </Items>
                        </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="9" Width="8%"                                 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}"></PropertiesTextEdit>        
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="PU" VisibleIndex="10" Width="10%"
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesTextEdit>                            
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdOperacao" Visible="false"/>                    
                        <dxwgv:GridViewDataColumn FieldName="Valor" Visible="false"/>
                    </Columns>
                    
                    <Templates>            
                    <EditForm>                          
                        <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                        
                        <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                        
                        <div class="editForm">
                            <table>
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                    </td>        
                                    
                                    <td>                                        
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" 
                                                    ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>'                                                    
                                                    MaxLength="10" NumberType="Integer">            
                                        <Buttons>                                           
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>       
                                        <ClientSideEvents                                                           
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} " 
                                                 ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                                />               
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                
                                    <td colspan="2">
                                        <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                                    </td>
                                </tr>
                            
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelTrader" runat="server" CssClass="labelRequired" Text="Trader/Estrat.:"> </asp:Label>
                                    </td>                
                                    <td colspan="3">
                                        <dxe:ASPxComboBox ID="dropTrader" runat="server" ClientInstanceName="dropTrader"
                                                            DataSourceID="EsDSTrader" IncrementalFilteringMode="Contains"  
                                                            ShowShadow="false" DropDownStyle="DropDown"
                                                            CssClass="dropDownList" TextField="Nome" ValueField="IdTrader"
                                                            Text='<%#Eval("IdTrader")%>'>
                                                <ClientSideEvents LostFocus="function(s, e) { 
                                                                            if(s.GetSelectedIndex() == -1)
                                                                                s.SetText(null); } "/>
                                        </dxe:ASPxComboBox>         
                                    </td>
                                </tr>  
                          
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelData" runat="server" CssClass="labelNormal" Text="Data:" />
                                    </td>                                     
                                    <td>
                                        <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" ClientEnabled="false" Value='<%#Eval("Data")%>' />
                                    </td> 
                                
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>                    
                                    </td>                    
                                    <td>
                                        <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" 
                                                            ShowShadow="false" DropDownStyle="DropDownList"
                                                            CssClass="dropDownListCurto" Text='<%#Eval("TipoOperacao")%>'>
                                        <Items>
                                        <dxe:ListEditItem Value="C" Text="Compra" />
                                        <dxe:ListEditItem Value="V" Text="Venda" />
                                        <dxe:ListEditItem Value="CD" Text="Compra DT" />
                                        <dxe:ListEditItem Value="VD" Text="Venda DT" />
                                        </Items>                                        
                                        </dxe:ASPxComboBox>                 
                                    </td>
                                </tr>   
                            
                                <tr>                                        
                                    <td class="td_Label">
                                        <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>                        
                                    </td>
                                    <td>
                                        <dxe:ASPxButtonEdit ID="btnEditAtivoBMF" runat="server" CssClass="textAtivoCurto" MaxLength="20" 
                                                            ClientInstanceName="btnEditAtivoBMF">            
                                        <Buttons>
                                            <dxe:EditButton></dxe:EditButton>                
                                        </Buttons>           
                                        <ClientSideEvents                              
                                             ButtonClick="function(s, e) {popupAtivoBMF.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {popupMensagemAtivoBMF.HideWindow();
                                                                        if (btnEditAtivoBMF.GetValue() != null)
                                                                        {
                                                                            ASPxCallback2.SendCallback(btnEditAtivoBMF.GetValue());
                                                                        }
                                                                        }"
                                        />                    
                                        </dxe:ASPxButtonEdit>
                                    </td>
                                    
                                    <td class="td_Label">
                                        <asp:Label ID="labelPeso" runat="server" CssClass="labelNormal" Text="Peso:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textPeso" runat="server" CssClass="textCurto" Enabled="false" />                                        
                                    </td>                                                                                       
                                </tr>
                                
                                <tr>                                
                                    <td class="td_Label">                                        
                                        <asp:Label ID="labelTipoMercado" runat="server" CssClass="labelNormal" Text="Mercado:"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="textTipoMercado" runat="server" CssClass="textNormal_5" Enabled="false" />                                        
                                    </td>                                                                                                                                       
                                </tr>                                
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Quantidade:"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor_5" ClientInstanceName="textQuantidade"
                                                                         MaxLength="12" NumberType="Integer" Text="<%#Bind('Quantidade')%>">            
                                            <ClientSideEvents    
                                             LostFocus="function(s, e) {var peso = document.getElementById(gridCadastro.cpTextPeso);
                                                                        pesoAux = peso.value.replace(',', '.')
                                                                        CalculoTriplo(textPU, textQuantidade, textValor, 1/parseFloat(pesoAux));
                                                                         }" />
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td  class="td_Label">
                                        <asp:Label ID="labelPU" runat="server" CssClass="labelRequired" Text="PU:"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <dxe:ASPxSpinEdit ID="textPU" runat="server" CssClass="textValor_5" ClientInstanceName="textPU"
                                                                         MaxLength="12" NumberType="Float" DecimalPlaces="8" Text="<%#Bind('PU')%>">            
                                        
                                            <ClientSideEvents    
                                             LostFocus="function(s, e) {var peso = document.getElementById(gridCadastro.cpTextPeso);
                                                                        pesoAux = peso.value.replace(',', '.')
                                                                        CalculoTriplo(textPU, textQuantidade, textValor, 1/parseFloat(pesoAux));
                                                                         }" />
                                        </dxe:ASPxSpinEdit>
                                    </td>     
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor:"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor"
                                                                         MaxLength="12" NumberType="Float" DecimalPlaces="2" Text="<%#Bind('Valor')%>">            
                                            <ClientSideEvents    
                                             LostFocus="function(s, e) {var peso = document.getElementById(gridCadastro.cpTextPeso);
                                                                        pesoAux = peso.value.replace(',', '.')
                                                                        CalculoTriplo(textPU, textQuantidade, textValor, 1/parseFloat(pesoAux));
                                                                         }" />
                                        </dxe:ASPxSpinEdit>
                                    </td>     
                                </tr>
                            </table>
                            
                            <div class="linhaH"></div>
                                        
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init" 
                                                   OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal7" runat="server" Text="OK+"/><div></div></asp:LinkButton>        

                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal11" runat="server" Text="OK"/><div></div></asp:LinkButton>        
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal12" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                            </div>        
                        </div>                        
                        </asp:Panel>
                    </EditForm>
                        
                    <StatusBar>
                        <div>
                            <div style="float:left">
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                            </div>                    
                        </div>
                    </StatusBar>
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="500px" />
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>
                    
                </dxwgv:ASPxGridView>            
            </div>        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSGerOperacaoBMF" runat="server" OnesSelect="EsDSGerOperacaoBMF_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSAtivoBMF" runat="server" OnesSelect="EsDSAtivoBMF_esSelect" />    
    <cc1:esDataSource ID="EsDSTrader" runat="server" OnesSelect="EsDSTrader_esSelect" />            
    
    </form>
</body>
</html>