﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Indice, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = false;
        var operacao = '';
        document.onkeydown = onDocumentKeyDown;

        function OnGetDataFeeder(data) {
            hiddenIdFeeder.SetValue(data);
            callBackPopupFeeder.SendCallback(data);
            popupFeeder.HideWindow();
            btnEditFeeder.Focus();
        }
    
       function OnGetDataCurva(data){
            hiddenIdCurva.SetValue(data);
            callBackPopupCurva.SendCallback(data);
            popupCurva.HideWindow();
            btnEditCurva.Focus();
        }
        
        function OnGetDataIndice(data) {
            hiddenIdIndice.SetValue(data);
            callBackPopupIndice.SendCallback(data);
            popupIndice.HideWindow();
            btnEditIndice.Focus();
        }          
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }   
            }
            operacao = '';
        }        
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callBackPopupCurva" runat="server" OnCallback="callBackPopupCurva_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCurva.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
    </dxcb:ASPxCallback>
    <dxcb:ASPxCallback ID="callBackPopupFeeder" runat="server" OnCallback="callBackPopupFeeder_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditFeeder.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupFeeder" ClientInstanceName="popupFeeder" runat="server"
        Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControFeeder" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridFeeder" runat="server" Width="100%" ClientInstanceName="gridFeeder"
                        AutoGenerateColumns="False" DataSourceID="EsDSFeeder" KeyFieldName="IdFeederPopup"
                        OnCustomDataCallback="gridFeeder_CustomDataCallback" OnCustomCallback="gridFeeder_CustomCallback"
                        OnHtmlRowCreated="gridFeeder_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdFeeder" Caption="Feeder" VisibleIndex="1"
                                Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="2"
                                Width="60%" />
                        </Columns>
                        <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridFeeder.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataFeeder);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Feeder." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridFeeder.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <dxcb:ASPxCallback ID="callBackPopupIndice" runat="server" OnCallback="callBackPopupIndice_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditIndice.SetValue(e.result); } " />
    </dxcb:ASPxCallback>
    <dxpc:ASPxPopupControl ID="popupIndice" ClientInstanceName="popupIndice" runat="server"
        Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControlIndice" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridIndice" runat="server" Width="100%" ClientInstanceName="gridIndice"
                        AutoGenerateColumns="False" DataSourceID="EsDSIndice" KeyFieldName="IdIndice"
                        OnCustomDataCallback="gridIndice_CustomDataCallback" OnCustomCallback="gridIndice_CustomCallback"
                        OnHtmlRowCreated="gridIndice_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdIndice" Caption="Índice" VisibleIndex="0"
                                Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                Width="60%" />
                        </Columns>
                        <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridIndice.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataIndice);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Índice." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridIndice.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <dxpc:ASPxPopupControl ID="popupCurva" ClientInstanceName="popupCurva" runat="server"
        Width="550px" HeaderText="" ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle"
        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentStyle VerticalAlign="Top">
        </ContentStyle>
        <ContentCollection>
            <dxpc:PopupControlContentControl ID="PopupControlContentControlCurva" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridCurva" runat="server" Width="100%" ClientInstanceName="gridCurva"
                        AutoGenerateColumns="False" DataSourceID="EsDSCurva" KeyFieldName="IdCurvaPopup"
                        OnCustomDataCallback="gridCurva_CustomDataCallback" OnCustomCallback="gridCurva_CustomCallback"
                        OnHtmlRowCreated="gridCurva_HtmlRowCreated">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn FieldName="IdCurvaRendaFixa" Caption="Curva " VisibleIndex="0"
                                Width="14%" />
                            <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1"
                                Width="60%" />
                        </Columns>
                        <Settings ShowFilterRow="true" ShowTitlePanel="True" />
                        <SettingsBehavior ColumnResizeMode="Disabled" />
                        <ClientSideEvents RowDblClick="function(s, e) {
                gridCurva.GetValuesOnCustomCallback(e.visibleIndex,OnGetDataCurva);}" Init="function(s, e) {e.cancel = true;}" />
                        <SettingsDetail ShowDetailButtons="False" />
                        <Styles AlternatingRow-Enabled="True">
                            <AlternatingRow Enabled="True">
                            </AlternatingRow>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                        </Styles>
                        <Images>
                            <PopupEditFormWindowClose Height="17px" Width="17px" />
                        </Images>
                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Tipo Curva." />
                    </dxwgv:ASPxGridView>
                </div>
            </dxpc:PopupControlContentControl>
        </ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCurva.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    <div class="divPanel">
        <table width="100%">
            <tr>
                <td>
                    <div id="container">
                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Índices"></asp:Label>
                        </div>
                        <div id="mainContent">
                            <div class="linkButton">
                                <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                    <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                    <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnPdf" OnClick="btnPDF_Click">
                                    <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnExcel" OnClick="btnExcel_Click">
                                    <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                    OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                    <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="divDataGrid">
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdIndice"
                                    DataSourceID="EsDSIndice" OnCustomCallback="gridCadastro_CustomCallback" OnRowInserting="gridCadastro_RowInserting"
                                    OnRowUpdating="gridCadastro_RowUpdating">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="5%" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataSpinEditColumn FieldName="IdIndice" VisibleIndex="1" Width="15%">
                                            <PropertiesSpinEdit SpinButtons-ShowIncrementButtons="false" DisplayFormatString="n0"
                                                MaxLength="5" MaxValue="32767">
                                            </PropertiesSpinEdit>
                                        </dxwgv:GridViewDataSpinEditColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="Descricao" Width="25%" VisibleIndex="2">
                                            <PropertiesTextEdit MaxLength="50">
                                            </PropertiesTextEdit>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoFeeder" Width="25%" VisibleIndex="3" UnboundType="String">
                                        </dxwgv:GridViewDataTextColumn>                                           
                                        <dxwgv:GridViewDataComboBoxColumn FieldName="Tipo" VisibleIndex="4" Width="10%">
                                            <PropertiesComboBox EncodeHtml="false">
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Percentual" />
                                                    <dxe:ListEditItem Value="2" Text="Decimal" />
                                                </Items>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoDivulgacao" VisibleIndex="5" Width="10%">
                                            <PropertiesComboBox EncodeHtml="false">
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Diário" />
                                                    <dxe:ListEditItem Value="2" Text="Mensal" />
                                                    <dxe:ListEditItem Value="3" Text="Mensal-1" />
                                                    <dxe:ListEditItem Value="4" Text="Mensal-2" />
                                                    <dxe:ListEditItem Value="5" Text="Mensal-3" />
                                                    <dxe:ListEditItem Value="6" Text="Diário-1" />
                                                    <dxe:ListEditItem Value="7" Text="Diário-2" />
                                                    <dxe:ListEditItem Value="8" Text="Diário-3" />
                                                </Items>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Índice Base" FieldName="IdIndiceBase"
                                            VisibleIndex="6" Width="15%" PropertiesComboBox-DropDownStyle="DropDownList">
                                            <PropertiesComboBox DataSourceID="EsDSIndice" TextField="Descricao" ValueField="IdIndice">
                                                <ValidationSettings RequiredField-ErrorText="">
                                                </ValidationSettings>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataSpinEditColumn FieldName="Percentual" Caption="% Índice" VisibleIndex="7"
                                            Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false"
                                                DecimalPlaces="2">
                                                <ValidationSettings RequiredField-ErrorText="">
                                                </ValidationSettings>
                                            </PropertiesSpinEdit>
                                        </dxwgv:GridViewDataSpinEditColumn>
                                        <dxwgv:GridViewDataSpinEditColumn FieldName="Taxa" VisibleIndex="8" Width="10%" HeaderStyle-HorizontalAlign="Right"
                                            CellStyle-HorizontalAlign="Right">
                                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false"
                                                DecimalPlaces="2">
                                                <ValidationSettings RequiredField-ErrorText="">
                                                </ValidationSettings>
                                            </PropertiesSpinEdit>
                                        </dxwgv:GridViewDataSpinEditColumn>    
                                        <dxwgv:GridViewDataTextColumn FieldName="CodigoBDS" Width="9%">
                                            <PropertiesTextEdit MaxLength="50">                                            
                                            <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                                            </PropertiesTextEdit>
                                        </dxwgv:GridViewDataTextColumn>   
                                        <dxwgv:GridViewDataTextColumn FieldName="DescricaoCurva" Width="25%" VisibleIndex="10" UnboundType="String">
                                        </dxwgv:GridViewDataTextColumn>                               
                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <div class="editForm">
                                                <dxe:ASPxTextBox ID="hiddenIdCurva" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdCurva"
                                                    Text='<%#Eval("IdCurva")%>' />
                                                <dxe:ASPxTextBox ID="hiddenIdFeeder" runat="server" CssClass="hiddenField" ClientInstanceName="hiddenIdFeeder"
                                                    Text='<%#Eval("IdFeeder")%>' />
                                                <table>                                                    
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelIdIndice" runat="server" AssociatedControlID="textIdIndice"
                                                                CssClass="labelRequired" Text="Id.Índice:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxTextBox ID="textIdIndice" ClientInstanceName="textIdIndice" runat="server" CssClass="textButtonEdit"
                                                                MaxLength="50" Text='<%#Eval("IdIndice")%>' OnLoad="textIdIndice_Load">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelDescricao" runat="server" AssociatedControlID="textDescricao"
                                                                CssClass="labelRequired" Text="Descrição:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxTextBox ID="textDescricao" ClientInstanceName="textDescricao" runat="server"
                                                                MaxLength="50" Text='<%#Eval("Descricao")%>' Width="100%">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr> 
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelFeeder" runat="server" CssClass="labelRequired" Text="Feeder:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButtonEdit ID="btnEditFeeder" runat="server" CssClass="textButtonEdit" EnableClientSideAPI="True"
                                                                Text='<%#(Eval("IdFeeder") != null ? "Feeder: " + Eval("IdFeeder") + "-" + Eval("DescricaoFeeder") : " ")%>'
                                                                ClientInstanceName="btnEditFeeder" ReadOnly="true" Width="100%">
                                                                <Buttons>
                                                                    <dxe:EditButton />
                                                                </Buttons>
                                                                <ClientSideEvents ButtonClick="function(s, e) {popupFeeder.ShowAtElementByID(s.name);}" />
                                                            </dxe:ASPxButtonEdit>
                                                        </td>                                                       
                                                    </tr>                                                                                                     
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="label1" runat="server" CssClass="labelRequired"
                                                                AssociatedControlID="dropTipo" Text="Tipo :" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropTipo" runat="server" CssClass="dropDownListLongo" Text='<%#Eval("Tipo")%>'>
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Percentual" />
                                                                <dxe:ListEditItem Value="2" Text="Decimal" />
                                                            </Items>                                                      
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>                                                    
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelTipoDivulgacao" runat="server" CssClass="labelRequired"
                                                                AssociatedControlID="dropTipoDivulgacao" Text="Tipo Divulgação:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropTipoDivulgacao" runat="server" CssClass="dropDownListLongo" Text='<%#Eval("TipoDivulgacao")%>'>
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Diário" />
                                                                <dxe:ListEditItem Value="2" Text="Mensal" />
                                                                <dxe:ListEditItem Value="3" Text="Mensal-1" />
                                                                <dxe:ListEditItem Value="4" Text="Mensal-2" />
                                                                <dxe:ListEditItem Value="5" Text="Mensal-3" />
                                                                <dxe:ListEditItem Value="6" Text="Diário-1" />
                                                                <dxe:ListEditItem Value="7" Text="Diário-2" />
                                                                <dxe:ListEditItem Value="8" Text="Diário-3" />
                                                            </Items>                                                      
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelCurva" runat="server" CssClass="labelNormal" Text="Curva:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButtonEdit ID="btnEditCurva" runat="server" CssClass="textButtonEdit"
                                                                Text='<%#(Eval("IdCurva") != null ? Eval("IdCurva") + "-" + Eval("DescricaoCurva") : " ")%>'
                                                                EnableClientSideAPI="True" ClientInstanceName="btnEditCurva" ReadOnly="true" Width="100%">
                                                                <Buttons>
                                                                    <dxe:EditButton />
                                                                </Buttons>
                                                                <ClientSideEvents ButtonClick="function(s, e) {popupCurva.ShowAtElementByID(s.name); gridCurva.PerformCallback('btnRefresh');}" />
                                                            </dxe:ASPxButtonEdit>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxButton ID="btnLimparCurva" runat="server" AutoPostBack="false" BackgroundImage-ImageUrl="~\imagens\delete.png"
                                                                BackgroundImage-HorizontalPosition="center" BackgroundImage-VerticalPosition="center"
                                                                BackgroundImage-Repeat="NoRepeat" ClientSideEvents-Click="function(s, e) { hiddenIdCurva.SetValue(null); btnEditCurva.SetValue(null); } ">
                                                            </dxe:ASPxButton>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelIndiceBase" runat="server" CssClass="labelNormal" AssociatedControlID="dropIndiceBase"
                                                                Text="Índice Base:">
                                                            </asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxComboBox ID="dropIndiceBase" runat="server" DataSourceID="EsDSIndice"
                                                                ValueField="IdIndice" TextField="Descricao" CssClass="dropDownListLongo" Text='<%#Eval("IdIndiceBase")%>'>
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelPercentual" runat="server" CssClass="labelNormal" Text="% Índice:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxSpinEdit ID="textPercentual" runat="server" ClientInstanceName="textPercentual" CssClass="textTaxa_5"
                                                                Text='<%# Eval("Percentual") %>' NumberType="Float" MaxLength="16" DecimalPlaces="8">
                                                            </dxe:ASPxSpinEdit>
                                                        </td>
                                                    </tr>                                                    
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Taxa:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxSpinEdit ID="textTaxa" runat="server" ClientInstanceName="textTaxa" CssClass="textTaxa_5"
                                                                Text='<%# Eval("Taxa") %>' NumberType="Float" MaxLength="16" DecimalPlaces="8">
                                                            </dxe:ASPxSpinEdit>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="labelCodigoBDS" runat="server" AssociatedControlID="textCodigoBDS"
                                                                CssClass="labelNormal" Text="Código BDS:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxSpinEdit ID="textCodigoBDS" ClientInstanceName="textCodigoBDS" runat="server" NumberType="Integer"
                                                                MaxLength="50" Text='<%#Eval("CodigoBDS")%>' Width="100%">
                                                            </dxe:ASPxSpinEdit>
                                                        </td>
                                                    </tr>  
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="label3" runat="server" AssociatedControlID="textCodigoBDS"
                                                                CssClass="labelNormal" Text="Tabela BDS:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxTextBox ID="textTabelaBDS" ClientInstanceName="textCodigoBDS" runat="server"
                                                                MaxLength="50" Text='<%#Eval("TabelaBDS")%>' Width="100%">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>    
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="label4" runat="server" AssociatedControlID="textIntervaloBDS"
                                                                CssClass="labelNormal" Text="Intervalo BDS:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxSpinEdit ID="textIntervaloBDS" ClientInstanceName="textIntervaloBDS" runat="server"
                                                                Text='<%#Eval("IntervaloBDS")%>' NumberType="Integer" MaxLength="1">
                                                            </dxe:ASPxSpinEdit>
                                                        </td>
                                                    </tr> 
                                                    <tr>
                                                        <td class="td_Label">
                                                            <asp:Label ID="label5" runat="server" AssociatedControlID="textAtributoBDS"
                                                                CssClass="labelNormal" Text="Atributo BDS:" />
                                                        </td>
                                                        <td>
                                                            <dxe:ASPxTextBox ID="textAtributoBDS" ClientInstanceName="textAtributoBDS" runat="server"
                                                                Text='<%#Eval("AtributoBDS")%>' MaxLength="20" Width="100%">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr> 
                                                </table>
                                                <div class="linhaH">
                                                </div>
                                                <div class="linkButton linkButtonNoBorder popupFooter">
                                                    <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                        OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal5" runat="server" Text="OK+" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                        CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                        <asp:Literal ID="Literal1" runat="server" Text="OK" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                        CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                        <asp:Literal ID="Literal2" runat="server" Text="Cancelar" />
                                                        <div>
                                                        </div>
                                                    </asp:LinkButton></div>
                                            </div>
                                        </EditForm>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="400px" />
                                    <SettingsCommandButton>
                                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                                        <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
    <cc1:esDataSource ID="EsDSIndice" runat="server" OnesSelect="EsDSIndice_esSelect" />
    <cc1:esDataSource ID="EsDSFeeder" runat="server" OnesSelect="EsDSFeeder_esSelect" />
    <cc1:esDataSource ID="EsDSCurva" runat="server" OnesSelect="EsDSCurva_esSelect" />
    </form>
</body>
</html>
