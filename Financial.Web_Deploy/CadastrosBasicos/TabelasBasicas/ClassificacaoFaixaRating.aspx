﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_ClassificacaoFaixaRating, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    popup=true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {            
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }  
            } 
            operacao = '';
        }" />
    </dxcb:ASPxCallback>
    <form id="form1" runat="server">
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Classificação de Faixa de Rating"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick="if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" OnCustomCallback="gridCadastro_CustomCallback"
                                    OnPreRender="gridCadastro_OnPreRender" OnRowInserting="gridCadastro_RowInserting"
                                    OnRowUpdating="gridCadastro_RowUpdating" OnCancelRowEditing="gridCadastro_CancelRowEditing"
                                    DataSourceID="EsDSClassificacaoFaixaRating" KeyFieldName="IdClassificacaoFaixaRating">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Perfil" FieldName="IdPerfilFaixaRating" CellStyle-HorizontalAlign="Left" Width="15%">
                                            <PropertiesComboBox DataSourceID="EsDSPerfilFaixaRating" TextField="DescricaoPerfil" ValueField="IdPerfilFaixaRating" ValueType="System.String" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Agencia" FieldName="IdAgenciaClassificadora" CellStyle-HorizontalAlign="Left" Width="15%">
                                            <PropertiesComboBox DataSourceID="EsDSAgenciaClassificadora" TextField="DescricaoAgencia" ValueField="IdAgenciaClassificadora" ValueType="System.String" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataDateColumn FieldName="DataVigencia" Caption="Data Vigência" CellStyle-HorizontalAlign="Left">
                                            <CellStyle HorizontalAlign="Left"></CellStyle>
                                        </dxwgv:GridViewDataDateColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Classificação" FieldName="Classificacao" CellStyle-HorizontalAlign="Left" Width="15%">
                                            <PropertiesComboBox></PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataColumn FieldName="SequenciaAgencia" Caption="Sequência Agência" CellStyle-HorizontalAlign="Left"/>
                                         <dxwgv:GridViewDataComboBoxColumn Caption="Código Nota" FieldName="IdPadraoNotas" CellStyle-HorizontalAlign="Left" Width="15%">
                                            <PropertiesComboBox DataSourceID="EsDSPadraoNota" TextField="CodigoNota" ValueField="IdPadraoNotas"/>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                            <asp:Panel ID="panelEdicao" runat="server">
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ID="labelIdPerfilFaixaRating" runat="server" ClientInstanceName="labelIdPerfilFaixaRating"
                                                                    CssClass="labelRequired" Text="Perfil:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropIdPerfilFaixaRating" runat="server" ClientInstanceName="dropIdPerfilFaixaRating"
                                                                    ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" DataSourceID="EsDSPerfilFaixaRating"
                                                                    TextField="DescricaoPerfil" ValueField="IdPerfilFaixaRating" Text='<%#Eval("IdPerfilFaixaRating")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataVigencia" runat="server" Text="Data Vigência:" CssClass="labelRequired" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataVigencia" runat="server" ClientInstanceName="textDataVigencia"
                                                                    Value='<%#Eval("DataVigencia")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ID="labelAgencia" runat="server" ClientInstanceName="labelAgencia"
                                                                    CssClass="labelRequired" Text="Agência:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropIdAgenciaClassificadora" runat="server" ClientInstanceName="dropIdAgenciaClassificadora"
                                                                    ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" DataSourceID="EsDSAgenciaClassificadora"
                                                                    TextField="DescricaoAgencia" ValueField="IdAgenciaClassificadora" Text='<%#Eval("IdAgenciaClassificadora")%>'>
                                                                    <ClientSideEvents SelectedIndexChanged="function(s,e) 
                                                                    {
                                                                         dropIdPadraoNotas.PerformCallback(s.GetValue());
                                                                    }" />
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelSequenciaAgencia" runat="server" Text="Seq. Agência:" CssClass="labelRequired"> </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textSequenciaAgencia" runat="server" ClientInstanceName="textSequenciaAgencia"
                                                                    Value='<%#Eval("SequenciaAgencia")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ID="labelIdPadraoNotas" runat="server" ClientInstanceName="labelIdPadraoNotas"
                                                                    CssClass="labelRequired" Text="Código Nota:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropIdPadraoNotas" runat="server" ClientInstanceName="dropIdPadraoNotas"
                                                                    OnCallback="dropIdPadraoNotas_Callback" ShowShadow="false" DropDownStyle="DropDown" OnDataBound="dropIdPadraoNotas_OnDataBound"
                                                                    CssClass="dropDownListLongo" 
                                                                    Text='<%#Eval("IdPadraoNotas")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ID="labelClassificacao" runat="server" ClientInstanceName="labelClassificacao"
                                                                    CssClass="labelRequired" Text="Classificação:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropClassificacao" runat="server" ClientInstanceName="dropClassificacao"
                                                                    ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" OnLoad="dropClassificacao_OnLoad"
                                                                    Text='<%#Eval("Classificacao")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr> 
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback();callbackAgenda.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                            CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback();return false; ">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                            CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </EditForm>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="40%" />
                                    <SettingsCommandButton>
                                        <ClearFilterButton>
                                            <Image Url="../../imagens/funnel--minus.png">
                                            </Image>
                                        </ClearFilterButton>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSClassificacaoFaixaRating" runat="server" OnesSelect="EsDSClassificacaoFaixaRating_esSelect" />
        <cc1:esDataSource ID="EsDSAgenciaClassificadora" runat="server" OnesSelect="EsDSAgenciaClassificadora_esSelect" />
        <cc1:esDataSource ID="EsDSPerfilFaixaRating" runat="server" OnesSelect="EsDSPerfilFaixaRating_esSelect" />
        <cc1:esDataSource ID="EsDSPadraoNota" runat="server" OnesSelect="EsDSPadraoNota_esSelect" />
    </form>
</body>
</html>
