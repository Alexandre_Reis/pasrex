﻿<%@ page language="C#" inherits="CadastrosBasicos_AgenteMercado, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web"
    TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    var operacao = '';
    document.onkeydown=onDocumentKeyDown;                         
    
    function OnGetDataPessoa(data) 
    {
        btnEditCodigoPessoa.SetValue(data);
        popupPessoa.HideWindow();
        callBackPessoa.SendCallback(btnEditCodigoPessoa.GetValue());
        btnEditCodigoPessoa.Focus();
    }
    
    function HabilitaCampo(enabled)
    {
        textCNPJ.SetEnabled(enabled);
        textEndereco.SetEnabled(enabled);
        textBairro.SetEnabled(enabled);
        textCidade.SetEnabled(enabled);
        textCEP.SetEnabled(enabled);
        textUF.SetEnabled(enabled);
        textDDD.SetEnabled(enabled);
        textTelefone.SetEnabled(enabled);
        textRamal.SetEnabled(enabled);
        textEmail.SetEnabled(enabled); 
		btnEditCodigoPessoa.SetEnabled(enabled);
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
                
        <dxcb:ASPxCallback ID="callBackPessoa" runat="server" OnCallback="pessoa_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {   
               var parametros = e.result.split('|');
               
               e.result = parametros[0];
               textNome.SetValue(parametros[1]);
               textApelidoAgente.SetValue(parametros[2]);
               textCNPJ.SetValue(parametros[3]);
               textEndereco.SetValue(parametros[4]);
               textBairro.SetValue(parametros[5]);
               textCidade.SetValue(parametros[6]);
               textCEP.SetValue(parametros[7]);
               textUF.SetValue(parametros[8]);
               textDDD.SetValue(parametros[9]);
               textTelefone.SetValue(parametros[10]);
               textRamal.SetValue(parametros[11]);
               textEmail.SetValue(parametros[12]);
               HabilitaCampo(false);

               var textApelido = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_tabCadastro_textApelido');                                                                                               
               OnCallBackCompleteCliente(s, e, popupMensagemPessoa, btnEditCodigoPessoa, textApelido);                                                     
            }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callBackCamposPessoa" runat="server" OnCallback="CamposPessoa_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {                   
               HabilitaCampo(e.result == 'S');
            }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Agentes de Mercado"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdAgente"
                                        DataSourceID="EsDSAgenteMercado" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdAgente" VisibleIndex="1" Visible="true" Width="5%" />
                                            <dxwgv:GridViewDataColumn FieldName="IdPessoa" VisibleIndex="1" Visible="true" Width="5%" UnboundType="String" />
                                            <dxwgv:GridViewDataColumn FieldName="Nome" VisibleIndex="2" Width="20%" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoBovespa" Caption="Cód.Bovespa" VisibleIndex="3"
                                                Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoBMF" Caption="Cód.BMF" VisibleIndex="4"
                                                Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="DigitoCodigoBovespa" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="DigitoCodigoBMF" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoBovespaCompleto" VisibleIndex="5" UnboundType="String"
                                                Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoBMFCompleto" VisibleIndex="6" UnboundType="String"
                                                Visible="false" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Corretora?" FieldName="FuncaoCorretora"
                                                VisibleIndex="7" Width="8%" ExportWidth="80">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Administrador?" FieldName="FuncaoAdministrador"
                                                VisibleIndex="8" Width="10%" ExportWidth="95">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Gestor?" FieldName="FuncaoGestor" VisibleIndex="9"
                                                Width="8%" ExportWidth="70">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Liquidante?" FieldName="FuncaoLiquidante"
                                                VisibleIndex="10" Width="8%" ExportWidth="80">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Custodiante?" FieldName="FuncaoCustodiante"
                                                VisibleIndex="11" Width="10%" ExportWidth="80">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Distribuidor?" FieldName="FuncaoDistribuidor"
                                                VisibleIndex="12" Width="10%" ExportWidth="80">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                             <dxwgv:GridViewDataComboBoxColumn Caption="Empresa Securitizada?" FieldName="EmpSecuritizada"
                                                VisibleIndex="13" Width="10%" ExportWidth="80">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="<div title='Sim'>Sim</div>" />
                                                        <dxe:ListEditItem Value="N" Text="<div title='Não'>Não</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>                                            
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoPessoa" Visible="false" UnboundType="String" />
                                            <dxwgv:GridViewDataColumn FieldName="CodigoAnbid" Visible="false" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxtc:ASPxPageControl ID="tabCadastro" runat="server" Height="100%" ActiveTabIndex="0"
                                                        TabSpacing="0px">
                                                        <TabPages>
                                                            <dxtc:TabPage Text="Informações Básicas">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">                                                                        
                                                                        <table>
                                                                            <tr>
                                                                                <td style="width: 50px;" class="td_Label">
                                                                                    <asp:Label ID="labelIdAgente" runat="server" CssClass="labelNormal" Text="Id.Agente:"> </asp:Label>
                                                                                </td>
                                                                                <td style="width: 303px;">
                                                                                    <dxe:ASPxTextBox ID="textIdAgente" ClientInstanceName="textIdAgente" runat="server" ClientEnabled="false" CssClass="textCurto" MaxLength="255"
                                                                                        Text='<%#Eval("IdAgente")%>' />
                                                                                </td>                                                                            
                                                                            </tr>                                                                        
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelPessoa" runat="server" CssClass="labelRequired" Text="Pessoa:" OnLoad="labelPessoa_Load"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoPessoa" runat="server" CssClass="textButtonEdit" ClientSideEvents-Init="function(s, e) { callBackCamposPessoa.SendCallback(); }"
                                                                                        ClientInstanceName="btnEditCodigoPessoa" Text='<%# Eval("IdPessoa") %>' OnLoad="btnEditCodigoPessoa_Load"
                                                                                        MaxLength="10" NumberType="Integer" Width="100%">
                                                                                        <Buttons>
                                                                                            <dxe:EditButton>
                                                                                            </dxe:EditButton>
                                                                                        </Buttons>
                                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textApelido').value = '';} "
                                                                                            ButtonClick="function(s, e) {popupPessoa.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemPessoa, callBackPessoa, btnEditCodigoPessoa);}" />
                                                                                    </dxe:ASPxSpinEdit>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <asp:TextBox ID="textApelido" runat="server" CssClass="textLongo" Enabled="False" Text='<%# Eval("ApelidoPessoa") %>'></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label5" runat="server" CssClass="labelRequired" Text="Nome:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <dxe:ASPxTextBox ID="textNome" ClientInstanceName="textNome" runat="server" CssClass="textNome"
                                                                                        MaxLength="100" Text='<%# Eval("Nome") %>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelApelido" runat="server" Text="Apelido:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <dxe:ASPxTextBox ID="textApelidoAgente" ClientInstanceName="textApelidoAgente" runat="server" CssClass="textNome"
                                                                                        MaxLength="100" Text='<%# Eval("Apelido") %>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCNPJ" runat="server" CssClass="labelNormal" Text="CNPJ:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                <dxe:ASPxTextBox ID="textCNPJ" ClientInstanceName="textCNPJ" runat="server" 
                                                                                        CssClass="textNormal" Text='<%#Eval("CNPJ")%>' MaxLength="18" OnLoad="textCNPJ_Load">
                                                                                        <ClientSideEvents KeyPress="function(s,e) 
                                                                                        {
                                                                                            var cnpj    = textCNPJ.GetText();
                                                                                                cnpj = cnpj.replace(/\D/g,'');                     
                                                                                                cnpj = cnpj.replace(/^(\d{2})(\d)/,'$1.$2')
                                                                                                cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cnpj = cnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
                                                                                                cnpj = cnpj.replace(/(\d{4})(\d)/,'$1-$2')                
                                                                                                textCNPJ.SetValue(cnpj);
	                                                                                    }" />
                                                                                        <ClientSideEvents Init="function(s,e) 
                                                                                        {
                                                                                            var cnpj = textCNPJ.GetText();
                                                                                                cnpj = cnpj.replace(/\D/g,'');                     
                                                                                                cnpj = cnpj.replace(/^(\d{2})(\d)/,'$1.$2')
                                                                                                cnpj = cnpj.replace(/^(\d{2})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cnpj = cnpj.replace(/\.(\d{3})(\d)/,'.$1/$2')             
                                                                                                cnpj = cnpj.replace(/(\d{4})(\d)/,'$1-$2')                
                                                                                                textCNPJ.SetValue(cnpj);
	                                                                                    }" />
                                                                                    </dxe:ASPxTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCodigoBovespa" runat="server" CssClass="labelNormal" Text="Cód.Bovespa:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textCodigoBovespa" runat="server" ClientInstanceName="textCodigoBovespa"
                                                                                        Text='<%#Eval("CodigoBovespaCompleto")%>' MaxLength="8" CssClass="textValor_5">
                                                                                    </dxe:ASPxTextBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCodigoBMF" runat="server" CssClass="labelNormal" Text="Cód.BMF:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textCodigoBMF" runat="server" ClientInstanceName="textCodigoBMF"
                                                                                        Text='<%#Eval("CodigoBMFCompleto")%>' MaxLength="8" CssClass="textValor_5">
                                                                                    </dxe:ASPxTextBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Cód.Cetip:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textCodigoCetip" runat="server" ClientInstanceName="textCodigoCetip"
                                                                                        Text='<%#Eval("CodigoCetip")%>' MaxLength="9" CssClass="textValor_5">
                                                                                    </dxe:ASPxTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelFuncaoCorretora" runat="server" CssClass="labelRequired" Text="Corretora? :"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropFuncaoCorretora" runat="server" ClientInstanceName="dropFuncaoCorretora"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("FuncaoCorretora")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label_Longo">
                                                                                    <asp:Label ID="labelFuncaoAdministrador" runat="server" CssClass="labelRequired"
                                                                                        Text="Administrador? :"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropFuncaoAdministrador" runat="server" ClientInstanceName="dropFuncaoAdministrador"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("FuncaoAdministrador")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelFuncaoGestor" runat="server" CssClass="labelRequired" Text="Gestor? :"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropFuncaoGestor" runat="server" ClientInstanceName="dropFuncaoGestor"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("FuncaoGestor")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>                                                                                
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelFuncaoLiquidante" runat="server" CssClass="labelRequired" Text="Liquidante? :"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropFuncaoLiquidante" runat="server" ClientInstanceName="dropFuncaoLiquidante"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("FuncaoLiquidante")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelFuncaoCustodiante" runat="server" CssClass="labelRequired" Text="Custodiante? :"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropFuncaoCustodiante" runat="server" ClientInstanceName="dropFuncaoCustodiante"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("FuncaoCustodiante")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelFuncaoDistribuidor" runat="server" CssClass="labelRequired" Text="Distribuidor? :"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropFuncaoDistribuidor" runat="server" ClientInstanceName="dropFuncaoDistribuidor"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("FuncaoDistribuidor")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCorretagemAdicional" runat="server" CssClass="labelNormal" AssociatedControlID="dropCorretagemAdicional"
                                                                                        Text="Corretagem Adic. :">
                                                                                    </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropCorretagemAdicional" runat="server" CssClass="dropDownListCurto"
                                                                                        Text='<%#Eval("CorretagemAdicional")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Por Mercado" />
                                                                                            <dxe:ListEditItem Value="3" Text="Rateia Ações" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                                  <td class="td_Label">
                                                                                    <asp:Label ID="labelEmpresaSecuritizada" runat="server" CssClass="labelRequired" Text="Emp. Securitizada:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxComboBox ID="dropEmpSecuritizada" runat="server" ClientInstanceName="dropEmpSecuritizada"
                                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_6"
                                                                                        Text='<%#Eval("EmpSecuritizada")%>'>
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                                        </Items>
                                                                                    </dxe:ASPxComboBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>                                                                            
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="lblCodigoAnbid" runat="server" CssClass="labelNormal" Text="Cód.Anbid:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textCodigoAnbid" runat="server" ClientInstanceName="textCodigoAnbid"
                                                                                        Text='<%#Eval("CodigoAnbid")%>' MaxLength="10" CssClass="textValor_5">
                                                                                    </dxe:ASPxTextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                            <dxtc:TabPage Text="Endereço/Outras">
                                                                <ContentCollection>
                                                                    <dxw:ContentControl runat="server">
                                                                        <table style="width: 820px;">
                                                                            <tr>
                                                                                <td style="width: 50px;" class="td_Label">
                                                                                    <asp:Label ID="labelEndereco" runat="server" CssClass="labelNormal" Text="Logradouro:"> </asp:Label>
                                                                                </td>
                                                                                <td style="width: 303px;">
                                                                                    <dxe:ASPxTextBox ID="textEndereco" ClientInstanceName="textEndereco" runat="server" CssClass="textLongo" MaxLength="255"
                                                                                        Text='<%#Eval("Endereco")%>' />
                                                                                </td>
                                                                                <td class="td_Label" style="width: 33px;">
                                                                                    <asp:Label ID="labelBairro" runat="server" CssClass="labelNormal" Text="Bairro:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="4">
                                                                                    <dxe:ASPxTextBox ID="textBairro" ClientInstanceName="textBairro" runat="server" CssClass="textNormal" MaxLength="100"
                                                                                        Text='<%#Eval("Bairro")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCidade" runat="server" CssClass="labelNormal" Text="Cidade:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textCidade" ClientInstanceName="textCidade" runat="server" CssClass="textLongo" MaxLength="100"
                                                                                        Text='<%#Eval("Cidade")%>' />
                                                                                </td>
                                                                                <td class="td_Label" style="width: 33px;">
                                                                                    <asp:Label ID="labelCEP" runat="server" CssClass="labelNormal" Text="CEP:"> </asp:Label>
                                                                                </td>
                                                                                <td style="width: 147px;">
                                                                                    <dxe:ASPxTextBox ID="textCEP" ClientInstanceName="textCEP" runat="server" CssClass="textNormal" MaxLength="10" Text='<%#Eval("CEP")%>' />
                                                                                </td>
                                                                                <td class="td_Label" style="width: 57px;">
                                                                                    <asp:Label ID="labelUF" runat="server" CssClass="labelNormal" Text="UF:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textUF" ClientInstanceName="textUF" runat="server" CssClass="textCurto" MaxLength="2" Text='<%#Eval("UF")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelDDD" runat="server" CssClass="labelNormal" Text="DDD:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textDDD" ClientInstanceName="textDDD" runat="server" CssClass="textCurto" MaxLength="2" Text='<%#Eval("Ddd")%>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelTelefone" runat="server" CssClass="labelNormal" Text="Telefone:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textTelefone" ClientInstanceName="textTelefone" runat="server" CssClass="textNormal" MaxLength="25"
                                                                                        Text='<%#Eval("Telefone")%>' />
                                                                                </td>
                                                                                <td class="td_Label" style="width: 17px;">
                                                                                    <asp:Label ID="labelRamal" runat="server" CssClass="labelNormal" Text="Ramal:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textRamal" ClientInstanceName="textRamal" runat="server" CssClass="textNormal" MaxLength="6" Text='<%#Eval("Ramal")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelFax" runat="server" CssClass="labelNormal" Text="Fax:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textFax" ClientInstanceName="textFax" runat="server" CssClass="textNormal" MaxLength="25" Text='<%#Eval("Fax")%>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelEmail" runat="server" CssClass="labelNormal" Text="Email:"> </asp:Label>
                                                                                </td>
                                                                                <td colspan="3">
                                                                                    <dxe:ASPxTextBox ID="textEmail" ClientInstanceName="textEmail" runat="server" CssClass="textNormal" MaxLength="100"
                                                                                        Text='<%#Eval("Email")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="WebSite:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textWebsite" runat="server" CssClass="textLongo" MaxLength="200"
                                                                                        Text='<%#Eval("WebSite")%>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Email Ouvidoria:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textEmailOuvidoria" runat="server" CssClass="textNormal" MaxLength="100"
                                                                                        Text='<%#Eval("EmailOuvidoria")%>' />
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Tel.Ouvidoria:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textTelOuvidoria" runat="server" CssClass="textNormal" MaxLength="25"
                                                                                        Text='<%#Eval("TelOuvidoria")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCPFResponsavel" runat="server" CssClass="labelNormal" Text="CPF Resp.:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <dxe:ASPxTextBox ID="textCPFResponsavel" ClientInstanceName="textCPFResponsavel" runat="server"
                                                                                        CssClass="textLongo" Text='<%#Eval("CPFResponsavel")%>' MaxLength="14">
                                                                                        <ClientSideEvents KeyPress="function(s,e) 
                                                                                        {
                                                                                            var cpf  = textCPFResponsavel.GetText();
                                                                                                cpf  = cpf.replace(/\D/g,'');                     
                                                                                                cpf  = cpf.replace(/^(\d{3})(\d)/,'$1.$2')
                                                                                                cpf  = cpf.replace(/^(\d{3})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpf  = cpf.replace(/(\d{3})(\d)/,'$1-$2')                
                                                                                                textCPFResponsavel.SetValue(cpf);
	                                                                                    }" />
                                                                                        <ClientSideEvents Init="function(s,e) 
                                                                                        {
                                                                                           var cpf  = textCPFResponsavel.GetText();
                                                                                                cpf  = cpf.replace(/\D/g,'');                     
                                                                                                cpf  = cpf.replace(/^(\d{3})(\d)/,'$1.$2')
                                                                                                cpf  = cpf.replace(/^(\d{3})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpf  = cpf.replace(/(\d{3})(\d)/,'$1-$2')                
                                                                                                textCPFResponsavel.SetValue(cpf);
	                                                                                    }" />
                                                                                    </dxe:ASPxTextBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelCPFResponsavel2" runat="server" CssClass="labelNormal" Text="CPF Resp.2:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                   <dxe:ASPxTextBox ID="textCPFResponsavel2" ClientInstanceName="textCPFResponsavel2" runat="server"
                                                                                        CssClass="textLongo" Text='<%#Eval("CPFResponsavel2")%>' MaxLength="14">
                                                                                        <ClientSideEvents KeyPress="function(s,e) 
                                                                                        {
                                                                                            var cpf  = textCPFResponsavel2.GetText();
                                                                                                cpf  = cpf.replace(/\D/g,'');                     
                                                                                                cpf  = cpf.replace(/^(\d{3})(\d)/,'$1.$2')
                                                                                                cpf  = cpf.replace(/^(\d{3})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpf  = cpf.replace(/(\d{3})(\d)/,'$1-$2')                
                                                                                                textCPFResponsavel2.SetValue(cpf);
	                                                                                    }" />
                                                                                        <ClientSideEvents Init="function(s,e) 
                                                                                        {
                                                                                           var cpf  = textCPFResponsavel2.GetText();
                                                                                                cpf  = cpf.replace(/\D/g,'');                     
                                                                                                cpf  = cpf.replace(/^(\d{3})(\d)/,'$1.$2')
                                                                                                cpf  = cpf.replace(/^(\d{3})\.(\d{3})(\d)/,'$1.$2.$3')   
                                                                                                cpf  = cpf.replace(/(\d{3})(\d)/,'$1-$2')                
                                                                                                textCPFResponsavel2.SetValue(cpf);
	                                                                                    }" />
                                                                                    </dxe:ASPxTextBox>
                                                                                </td>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="labelNomeResponsavel" runat="server" CssClass="labelNormal" Text="Nome Resp:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textNomeResponsavel" runat="server" CssClass="textNormal" MaxLength="60"
                                                                                        Text='<%#Eval("NomeResponsavel")%>' />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="td_Label">
                                                                                    <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Carta Patente:"> </asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="textCartaPatente" runat="server" CssClass="textNormal" MaxLength="20"
                                                                                        Text='<%#Eval("CartaPatente")%>' />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </dxw:ContentControl>
                                                                </ContentCollection>
                                                            </dxtc:TabPage>
                                                        </TabPages>
                                                    </dxtc:ASPxPageControl>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="620px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
        <cc1:esDataSource ID="EsDSPessoa" runat="server" OnesSelect="EsDSPessoa_esSelect" />
    </form>
</body>
</html>
