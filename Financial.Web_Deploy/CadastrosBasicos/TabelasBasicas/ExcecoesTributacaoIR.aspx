﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_ExcecoesTributacaoIR, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    try{
        document.onkeydown=onDocumentKeyDown;        
    }
    catch(ex)
    {
    }
    
    var operacao = '';
    
    function dropMercadoSelectIndexChanged(s,e)
    {    
        var trAtivoBolsa = document.getElementById('trAtivoBolsa');
        var trAtivoFundo = document.getElementById('trAtivoFundo');        
        var trAtivoCategoriaFundo = document.getElementById('trAtivoCategoriaFundo'); 
        var trAtivoRendaFixaTitulo = document.getElementById('trAtivoRendaFixaTitulo');     
        var trAtivoRendaFixaPapel = document.getElementById('trAtivoRendaFixaPapel');         
        
        trAtivoFundo.style.display = "none";
        trAtivoCategoriaFundo.style.display = "none";
        trAtivoBolsa.style.display = "none";
        trAtivoRendaFixaTitulo.style.display = "none";    
        trAtivoRendaFixaPapel.style.display = "none";    

        switch(s.GetValue()) {
            //Bolsa
            case "1":      
                trAtivoBolsa.style.display = "table-row";
                break;
            //RendaFixa
            case "3":                   
                trAtivoRendaFixaTitulo.style.display = "table-row";    
                trAtivoRendaFixaPapel.style.display = "table-row";                
                break;
            //Fundo
            case "5":                
                trAtivoFundo.style.display = "table-row";
                trAtivoCategoriaFundo.style.display = "table-row";
                break;            
            default:
                break;
        }
    }
    
    function OnGetDataCliente(data) {          
        if(!isNumber(data)){
            alert(data);
            return false;
        }
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }    
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }    
    
    function OnGetDataTituloRF(data) {        
        ASPxCallback1.SendCallback(data);
        popupTituloRF.HideWindow();
        btnEditTituloRF.Focus();
    }
    
    
    function OnGetDataAtivoBolsa(data) {
        btnEditAtivoBolsa.SetValue(data);        
        
        popupAtivoBolsa.HideWindow();
        btnEditAtivoBolsa.Focus();    
    }   
    
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        ASPxCallback3.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraFiltro.Focus();
    } 
    
    function OnGetDataCarteira(data) {       
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallbackCarteira.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />

        <dxcb:ASPxCallback ID="ASPxCallback3" runat="server" OnCallback="ASPxCallback3_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            if (gridCadastro.cp_EditVisibleIndex == -1) {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                
            }
            else {                                                                               
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="
            function(s, e) {
                var resultSplit = e.result.split('|');
                if (resultSplit[1] != null) btnEditTituloRF.SetValue(resultSplit[1]); 
                if (resultSplit[0] != null) hiddenIdTitulo.SetValue(resultSplit[0]); 
            } " />
        </dxcb:ASPxCallback>
        
        
        <dxcb:ASPxCallback ID="ASPxCallbackCarteira" runat="server" OnCallback="ASPxCallbackCarteira_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                var resultSplit = e.result.split('|');          
                e.result = resultSplit[1];
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira);}"
            />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Exceções Tributação IR"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false; ">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback(operacao);} return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnRowInserting="gridCadastro_RowInserting" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnCustomColumnDisplayText="gridCadastro_CustomColumnDisplayText" OnCancelRowEditing="gridCadastro_CancelRowEditing"
                                        OnLoad="gridCadastro_OnLoad" DataSourceID="EsDSExcecoesTributacaoIR" KeyFieldName="IdExcecoesTributacaoIR"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="Mercado" Caption="Mercado" VisibleIndex="1"
                                                Width="25%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="NomeAtivo" Caption="Ativo" VisibleIndex="3" UnboundType="String"
                                                Width="45%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoInvestidor" Caption="Tipo de Investidor"
                                                VisibleIndex="4" Width="15%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="IsencaoIR" Caption="Isenção IR" VisibleIndex="5"
                                                Width="10%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                            <dxwgv:GridViewDataColumn FieldName="AliquotaIR" Caption="Alíquota IR" VisibleIndex="6"
                                                Width="20%" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left"
                                                ExportWidth="100" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataInicio" Caption="Data Início" VisibleIndex="7"
                                                Width="10%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataFim" Caption="Data Final" VisibleIndex="8"
                                            Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="DescricaoCompleta" Visible="false" UnboundType="String"/>
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" Visible="false" UnboundType="String"/>

                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelMercado" runat="server" CssClass="labelRequired" Text="Mercado:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropMercado" ClientInstanceName="dropMercado" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("Mercado")%>' OnLoad="dropMercado_OnLoad">
                                                                        <ClientSideEvents SelectedIndexChanged="dropMercadoSelectIndexChanged" Init="dropMercadoSelectIndexChanged"/>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr id="trAtivoRendaFixaPapel" style="display: none">                                                                                                                                 
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoPapelRF" runat="server" CssClass="labelNormal" Text="Tipo/Classe Ativo:" />
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropPapelRF" ClientInstanceName="dropPapelRF" Width="380px"
                                                                        DataSourceID="EsDsPapelRF"  runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        TextField="DescricaoCompleta" ValueField="IdPapel" ValueType="System.String" Text='<%#Eval("TipoClasseAtivo")%>'>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr> 
                                                            <tr id="trAtivoRendaFixaTitulo" style="display: none">                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTitulo" runat="server" CssClass="labelNormal" Text="Título:" />
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxSpinEdit ID="hiddenIdTitulo" CssClass="hiddenField" ClientInstanceName="hiddenIdTitulo" runat="server" Text='<%#Eval("IdTituloRendaFixa")%>' ></dxe:ASPxSpinEdit>
                                                                    <dxe:ASPxButtonEdit ID="btnEditTituloRF" runat="server" CssClass="textButtonEdit" Text='<%#Eval("DescricaoCompleta")%>'
                                                                        ClientInstanceName="btnEditTituloRF" ReadOnly="true" Width="380px">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents ButtonClick="function(s, e) {popupTituloRF.ShowAtElementByID(s.name);  gridTituloRF.PerformCallback('btnRefresh');}"
                                                                         Init="function(s, e) { if(s.GetValue() == null) dropPapelRF.SetValue(null);  }"/>                                                                          
                                                                    </dxe:ASPxButtonEdit>
                                                                </td>                                                                
                                                            </tr>

                                                            <tr id="trAtivoBolsa" style="display: none">
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>
                                                                </td>
                                                                <td colspan="2">
                                                                    <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa" runat="server" Width="100%" CssClass="textAtivoCurto" Text='<%#Eval("CdAtivoBolsa")%>'
                                                                        MaxLength="25" ClientInstanceName="btnEditAtivoBolsa">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents ButtonClick="function(s, e) {popupAtivoBolsa.ShowAtElementByID(s.name);}"/>
                                                                    </dxe:ASPxButtonEdit>
                                                                </td>
                                                                
                                                            </tr>
                                                            
                                                            <tr id="trAtivoCategoriaFundo" style="display: none">       
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCategoriaFundo" runat="server" CssClass="labelRequired" Text="Tipo/Classe Ativo:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropCategoriaFundo" runat="server" ClientInstanceName="dropCategoriaFundo"
                                                                        DataSourceID="EsDSCategoriaFundo" ShowShadow="False" CssClass="dropDownListLongo"
                                                                        DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                        TextField="DescricaoCompleta" ValueField="IdCategoria" Text='<%# Eval("TipoClasseAtivo") %>'
                                                                        ValueType="System.String">
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr> 
                                                            <tr id="trAtivoFundo" style="display: none">
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" Text='<%#Eval("IdCarteira")%>'
                                                                        ClientInstanceName="btnEditCodigoCarteira" MaxLength="10" NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name); gridCarteira.PerformCallback('btnRefresh');}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallbackCarteira, btnEditCodigoCarteira);}" 
                                                                            Init="function(s, e) { if(s.GetValue() == null) dropCategoriaFundo.SetValue(null);  }"/>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" 
                                                                        Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoInvestidor" runat="server" CssClass="labelRequired" Text="Tipo de Investidor:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoInvestidor" ClientInstanceName="dropTipoInvestidor"
                                                                        runat="server" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                        OnLoad="dropTipoInvestidor_OnLoad" Text='<%#Eval("TipoInvestidor")%>'>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelIsencaoIR" runat="server" Text="Isenção IR:" CssClass="labelRequired" />
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropIsencaoIR" ClientInstanceName="dropIsencaoIR" runat="server"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                        OnLoad="dropIsencaoIR_OnLoad" Text='<%#Eval("IsencaoIR")%>'>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelAliquotaIR" runat="server" Text="Alíquota IR:" CssClass="labelnormal" />
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxSpinEdit ID="textAliquotaIR" runat="server" ClientInstanceName="textAliquotaIR" CssClass="textButtonEdit"
                                                                        Value='<%#Eval("AliquotaIR")%>'  />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Início:" /></td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" Value='<%#Eval("DataInicio")%>'/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Fim:" /></td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" Value='<%#Eval("DataFim")%>'/>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelRegimeEspecialTributacao" runat="server" CssClass="labelRequired" Text="Regime Especial de Tributação:" /></td>
                                                                <td colspan="3">
                                                                   <dxe:ASPxComboBox ID="dropRegimeEspecialTributacao" runat="server" ClientInstanceName="dropRegimeEspecialTributacao"
                                                                        ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"  Text='<%#Eval("RegimeEspecialTributacao")%>'
                                                                        CssClass="dropDownListCurto">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>                                                            
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar';callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal7" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsEditing PopupEditFormWidth="400px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSExcecoesTributacaoIR" runat="server" OnesSelect="EsDSExcecoesTributacaoIR_esSelect" />
        <cc1:esDataSource ID="EsDSAtivos" runat="server" OnesSelect="EsDSAtivos_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDsPapelRF" runat="server" OnesSelect="EsDsPapelRF_esSelect" />
        <cc1:esDataSource ID="EsDSTituloRF" runat="server" OnesSelect="EsDSTituloRF_esSelect" />
        <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />
        <cc1:esDataSource ID="EsDSCategoriaFundo" runat="server" OnesSelect="EsDSCategoriaFundo_esSelect" />
    </form>
</body>
</html>