﻿<%@ page language="C#" autoeventwireup="true" inherits="TabelasBasicas_AlteracaoTributacaoFieFundo, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
   
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraFiltro.Focus();
    }    
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoCotistaFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCotistaFiltro.GetValue());
        popupCotista.HideWindow();
        btnEditCodigoCotistaFiltro.Focus();
    }
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
     
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {
                alert(e.result);             
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else                
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }                
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
     
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
                if (gridCadastro.cp_EditVisibleIndex == -1)
                {                                  
                    e.result = resultSplit[0];
                    var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                    OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
                }
            }
            "/>
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            }
            else    
            {
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
            }
        }        
        " />
        </dxcb:ASPxCallback>
     
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Alteralção de Tabela de IR - Fundos(FIE-VGBL)"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <table border="0">
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelCarteiraFiltro" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoCarteiraFiltro" EnableClientSideAPI="true" CssFilePath="../../css/forms.css"
                                                            SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCarteiraFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteiraFiltro);}" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2" width="450">
                                                        <asp:TextBox ID="textNomeCarteiraFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoClienteFiltro" EnableClientSideAPI="true" CssFilePath="../../css/forms.css"
                                                            SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {popupMensagemCliente.HideWindow(); ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
                                                                    }" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="4" width="450">
                                                        <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelTabelaIr" runat="server" CssClass="labelNormal" Text="Tabela IR:"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropFiltroTabelaIr" runat="server" ClientInstanceName="dropFiltroTabelaIr"
                                                            ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                            CssClass="dropDownListCurto">
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Progressiva" />
                                                                <dxe:ListEditItem Value="2" Text="Regressiva" />
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <dxe:ASPxTextBox ID="hiddenIdTituloFiltro" runat="server" CssClass="hiddenField"
                                                            ClientInstanceName="hiddenIdTituloFiltro" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); 
                                                                                              popupFiltro_hiddenIdTituloFiltro_I.value='';
                                                                                              return false;">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal8" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="DataHistorico;IdOperacao"
                                        DataSourceID="EsDSPosicaoFundo" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True"/>

                                            <dxwgv:GridViewDataColumn FieldName="DataHistorico" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Operação" VisibleIndex="2" Width="10%" CellStyle-HorizontalAlign="left">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="3" Width="10%" CellStyle-HorizontalAlign="left">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoCliente" Caption="Cliente" Width="30%" VisibleIndex="4">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" Width="10%" CellStyle-HorizontalAlign="left" VisibleIndex="5">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoCarteira" Caption="Carteira" Width="30%" VisibleIndex="6">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="FieTabelaIr" Caption="Tabela IR" Width="10%" VisibleIndex="7">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Progressiva" />
                                                        <dxe:ListEditItem Value="2" Text="Regressiva" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdPosicao" Visible="false" VisibleIndex="2">
                                            </dxwgv:GridViewDataColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table>
                                                        
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataOperacao" runat="server" CssClass="labelRequired" Text="Operação:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataHistorico" runat="server" Enabled="false" ClientInstanceName="textDataHistorico" Value='<%#Eval("DataHistorico")%>'/>
                                                                </td>
                                                            </tr>
                                                        
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelOperacao" runat="server" CssClass="labelRequired" Text="Operação:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditOperacao" runat="server" CssClass="textButtonEdit" style="float:left"
                                                                        ClientInstanceName="btnEditOperacao" Text='<%#Eval("IdOperacao")%>' Enabled="false" NumberType="Integer"/>
                                                                    <dxe:ASPxSpinEdit ID="btnEditPosicao" runat="server" CssClass="textButtonEdit" style="float:left"
                                                                        ClientInstanceName="btnEditPosicao" Text='<%#Eval("IdPosicao")%>' Visible="false" NumberType="Integer"/>
                                                                </td>
                                                            </tr>
                                                        
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" ClientInstanceName="btnEditCodigoCliente" style="float:left"
                                                                        CssClass="textButtonEdit" Text='<%#Eval("IdCliente")%>' Enabled="false" NumberType="Integer"/>
                                                                    <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false" style="float:left; margin: 0px 2px;"
                                                                        Text='<%#Eval("ApelidoCliente")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" style="float:left"
                                                                        ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteira")%>' Enabled="false" NumberType="Integer"/>
                                                                    <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false" style="float:left; margin: 0px 2px;"
                                                                        Text='<%#Eval("ApelidoCarteira")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>

                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelFieTabelaIr" runat="server" CssClass="labelNormal" Text="Tabela IR:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropFieTabelaIr" runat="server" ClientInstanceName="dropFieTabelaIr"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                        Text='<%#Eval("FieTabelaIr")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Progressiva" />
                                                                            <dxe:ListEditItem Value="2" Text="Regressiva" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal11" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal12" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="600px" />
                                        <SettingsBehavior EnableCustomizationWindow ="true" />
                                        <SettingsText CustomizationWindowCaption="Lista de campos" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>

                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" LeftMargin="30" RightMargin="30"/>
        
        <cc1:esDataSource ID="EsDSPosicaoFundo" runat="server" OnesSelect="EsDSPosicaoFundo_esSelect" LowLevelBind="true"/>
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        													        
    </form>
</body>
</html>