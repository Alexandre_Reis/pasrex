﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_PermissaoMenu, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>


<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v15.2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
                   
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />
    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    
    <script language="javascript" type="text/javascript">
    function OnTreeClick(evt) {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if(isChkBoxClick) {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if(nxtSibling && nxtSibling.nodeType == 1) { //check if nxt sibling is not null & is an element node
                if(nxtSibling.tagName.toLowerCase() == "div") { //if node has children
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
   }
   
   function CheckUncheckChildren(childContainer, check) {
      var childChkBoxes = childContainer.getElementsByTagName("input");
      var childChkBoxCount = childChkBoxes.length;
      for(var i = 0; i<childChkBoxCount; i++) {
        childChkBoxes[i].checked = check;
      }
   }
   
   function CheckUncheckParents(srcChild, check) {
       var parentDiv = GetParentByTagName("div", srcChild);
       var parentNodeTable = parentDiv.previousSibling;
       
       if(parentNodeTable) {
            var checkUncheckSwitch;
            
            if(check) { //checkbox checked
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if(isAllSiblingsChecked) {
                    checkUncheckSwitch = true;
                }
                else {
                    return; //do not need to check parent if any(one or more) child not checked
                }
            }
            else { //checkbox unchecked
                checkUncheckSwitch = false;
            }
            
            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if(inpElemsInParentTable.length > 0) {
                var parentNodeChkBox = inpElemsInParentTable[0]; 
                parentNodeChkBox.checked = checkUncheckSwitch; 
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
   }
   
   function AreAllSiblingsChecked(chkBox) {
     var parentDiv = GetParentByTagName("div", chkBox);
     var childCount = parentDiv.childNodes.length;
     for(var i=0; i<childCount; i++) {
        if(parentDiv.childNodes[i].nodeType == 1) { //check if the child node is an element node
            if(parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
               var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
              //if any of sibling nodes are not checked, return false
              if(!prevChkBox.checked) { 
                return false;
              } 
            }
        }
     }
     return true;
   }
   
   //utility function to get the container of an element by tagname
   function GetParentByTagName(parentTagName, childElementObj) {
      var parent = childElementObj.parentNode;
      while(parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
         parent = parent.parentNode;
      }
    return parent;    
   }
</script>

</head>

<body>

    <dxcb:ASPxCallback ID="callbackExporta" runat="server" OnCallback="callbackExporta_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanelExport.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe =
                        document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                }else{
                    alert(e.result);
                }
            }
        }" />
    </dxcb:ASPxCallback>

    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallbackCarregar" runat="server" ClientInstanceName="ASPxCallbackCarregar">
        <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanelCarregar.Hide(); }" />
    </dxcb:ASPxCallback>
    
    <div class="divPanel divPanelNew">
        <div id="container_small">
            <div id="header">
                <asp:Label ID="lblHeader" runat="server" Text="Controle de Permissão por Funções"></asp:Label>
            </div>
            <div id="mainContent">
                <div class="reportFilter">
                    <div class="dataMessage"></div>
                    
                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" ShowHeader="False" BackColor="White" Border-BorderColor="#AECAF0">
                        <Border BorderColor="#AECAF0"></Border>
                        <PanelCollection>
                            <dxp:PanelContent runat="server">
                                                                
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                                <ContentTemplate>
                                    
                                    <div style="background-color:#F5F5F5;padding-bottom:10px;padding-top:10px">               
                                    <table border="0">
                                    <tr>
                                        <td class="td_Label">
                                            <asp:Label ID="labelGrupoUsuario" runat="server" CssClass="labelNormal" Text="Grupo :"></asp:Label>                    
                                        </td>                    
                                        <td>      
                                            <dxe:ASPxComboBox ID="dropGrupoUsuario" runat="server" DataSourceID="EsDSGrupoUsuario" 
                                                                ShowShadow="False" CssClass="dropDownListLongo"  OnSelectedIndexChanged="dropGrupoUsuario_SelectedIndexChanged"                                               
                                                                TextField="Descricao" ValueField="IdGrupo" AutoPostBack="true" ValueType="System.String">
                                                                                                                                                
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { 
                                                       ASPxCallbackCarregar.PerformCallback();
                                                       LoadingPanelCarregar.Show();
                                                    }" 
                                                />                                                                                                            
                                            </dxe:ASPxComboBox>
                                        </td>
                                        
                                        <td>                                                                                                                                                                           
                                            <div class="linkButton linkButtonPermissao linkButtonNoBorder">              

                                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" 
                                                    CssClass="btnOK" OnClick="btnOK_Click"
                                                    OnClientClick=" {
                                                            Callback.PerformCallback();
                                                            LoadingPanel.Show();
                                                        }"
                                                >
                                                    <asp:Literal ID="Literal2" runat="server" Text="Aplicar"/><div></div>
                                                </asp:LinkButton>
                                                                                                                                                                                                 
                                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" 
                                                    CssClass="btnCancel" OnClick="btnCancel_Click"
                                                    OnClientClick=" {
                                                            ASPxCallbackCarregar.PerformCallback();
                                                            LoadingPanelCarregar.Show();
                                                        }"
                                                >
                                                    <asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div>
                                                </asp:LinkButton>
                                                                                                                                                                                                                                                                                  
                                                <asp:LinkButton ID="btnColapsa" runat="server" Font-Overline="false"  CssClass="btnRun" OnClick="btnColapsaExpande_Click"><asp:Literal ID="Literal5" runat="server" Text="Colapsa/Expande"/><div></div></asp:LinkButton>
                                                                                             
                                                <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                                    OnClientClick="LoadingPanelExport.Show();
                                                                   callbackExporta.SendCallback();">
                                                    <asp:Literal ID="Literal3" runat="server" Text="Exportar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                                            </div>            
                                        </td>
                                    </tr>
                                    </table>
                                    </div>
                                    
                                    <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" ShowStartingNode="False" />
                                    
                                    <table border="0" id="PanelForLoading">
                                    <tr>
                                        <td colspan="2" style="padding-left:1%">                 
                                                                                        
                                            <div id="PanelForLoadingDiv" style="width:710px;height:350px; background-color:#FFFFFF; overflow:auto; text-align:left;">
                                                <asp:TreeView ID="treeView1" runat="server" DataSourceID="SiteMapDataSource1"   
                                                              ShowCheckBoxes="All" ForeColor="#020238" ShowLines="true"
                                                              OnTreeNodeDataBound="treeView1_TreeNodeDataBound">
                                                </asp:TreeView>                                    
                                            </div>
                                        </td>
                                     </tr>                                                                                                                                                               
                                    </table>

                                    <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel" Text="Aguarde..." />
                                    <dxlp:ASPxLoadingPanel ID="LoadingPanelCarregar" runat="server" ClientInstanceName="LoadingPanelCarregar" Text="Carregando..." />
                                    <dxlp:ASPxLoadingPanel ID="LoadingPanelExport" runat="server" Text="Exportando arquivo, aguarde..." ClientInstanceName="LoadingPanelExport" Modal="true" />                                    
                                                                                                                                                                    
                                </ContentTemplate>
                            </asp:UpdatePanel>
                                   
                            </dxp:PanelContent>
                        </PanelCollection>
                    </dxrp:ASPxRoundPanel>
                </div>                
            </div>
        </div>
    </div>
    
    <cc1:esDataSource ID="EsDSGrupoUsuario" runat="server" OnesSelect="EsDSGrupoUsuario_esSelect" />
    
    <dxwgv:ASPxGridView ID="gridExportacao1" runat="server" Visible="false" />
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridExportacao1" />
    
    
            
    </form>
</body>
</html>