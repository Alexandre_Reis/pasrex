<%@ WebHandler Language="C#" Class="TesteItau.TesteIntegraItauPortoMva" %>

using System;
using System.Web;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using Financial.Export;
using System.Net;
using Financial.InvestidorCotista;
using Financial.Interfaces.Export;


namespace TesteItau
{
    public class TesteIntegraItauPortoMva : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            Itau itau = new Itau();

            OrdemCotista ordem;

            string idOrdemParam = context.Request["idordem"];
            if (!string.IsNullOrEmpty(idOrdemParam))
            {
                ordem = new OrdemCotista();
                ordem.LoadByPrimaryKey(Convert.ToInt32(idOrdemParam));
            }
            else
            {

                OrdemCotistaCollection ordens = new OrdemCotistaCollection();
                ordens.LoadAll();
                ordem = ordens[0];
                ordem.Quantidade = 0;
                ordem.ValorBruto = 0;
                ordem.ValorLiquido = 0;
            }
            
            
            bool showPostData = context.Request["showpostdata"] == "1";
            bool showResponseData = context.Request["showresponsedata"] == "1";

            XmlDocument xmlDocument = itau.RetornaEop_Xml(ordem);

            string urlSiteItau = "https://www.itaucustodia.com.br/PassivoWebServices/xmlmva.jsp";
            
            string pattern = "(campo\\d+)\\s+"; //transformar id="campo 1     " em id="campo1"
            string postData = "strXML=<strXML>" + System.Text.RegularExpressions.Regex.Replace(xmlDocument.OuterXml, pattern, "$1") + "</strXML>";
            
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlSiteItau);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = postData.Length;

            string proxyInfo = System.Configuration.ConfigurationManager.AppSettings["WebServiceProxy"];

            if (!string.IsNullOrEmpty(proxyInfo))
            {
                //Utilizar padrao de proxy definido no Windows
                webRequest.Proxy = System.Net.WebProxy.GetDefaultProxy();
                webRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                if (proxyInfo != "default")
                {
                    //Um proxy especifico foi configurado
                    string[] proxyInfos = proxyInfo.Split(';');

                    //Setar url do proxy
                    webRequest.Proxy = new System.Net.WebProxy(proxyInfos[0]);

                    if (proxyInfos.Length > 1)
                    {
                        //Foram passadas tambem as credenciais de rede
                        webRequest.Proxy.Credentials = new System.Net.NetworkCredential(proxyInfos[1], proxyInfos[2], proxyInfos[3]);
                    }
                }
            }
            
            using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                requestWriter.Write(postData);
            }

            if (showPostData)
            {
                //context.Response.ContentType = "text/xml";
                context.Response.ContentType = "text/plain";
                context.Response.Write(postData.Replace("strXML=", ""));
                return;
            }

            //  This actually does the request and gets the response back
            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

            string responseData = string.Empty;

            using (StreamReader responseReader = new StreamReader(response.GetResponseStream()))
            {
                // dumps the HTML from the response into a string variable
                responseData = responseReader.ReadToEnd();
            }

            Financial.Interfaces.Export.Itau.ItauMsg retornoItau = null;
            XmlSerializer serializer = new XmlSerializer(typeof(Financial.Interfaces.Export.Itau.ItauMsg));

            using (StringReader reader = new StringReader(responseData))
            {
                retornoItau = (Financial.Interfaces.Export.Itau.ItauMsg)serializer.Deserialize(reader);
            }

            string msgRetorno = "";
            string codCotista = "";
            foreach (Financial.Interfaces.Export.Itau.Param param in retornoItau.parameter)
            {
                if (param.id.ToUpper() == "MSGRETORNO")
                {
                    msgRetorno = param.value;
                }
                if (param.id.ToUpper() == "CODCOTISTA")
                {
                    codCotista = param.value;
                }
            }

            if (showResponseData)
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write(responseData);
                return;
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("MsgRetorno: " + msgRetorno + " - CodCotista: " + codCotista);
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

  
}