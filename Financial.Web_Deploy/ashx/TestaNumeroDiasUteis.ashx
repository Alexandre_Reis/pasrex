<%@ WebHandler Language="C#" Class="TestaNumeroDiasUteis" %>

using System;
using System.Web;
using Financial.Util;
using Financial.Common.Enums;

public class TestaNumeroDiasUteis : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        int numeroDiasOld;
        int numeroDiasNovo;

        numeroDiasOld = Calendario.NumeroDiasOld(new DateTime(2014,9,1), new DateTime(2017,1,2), LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        numeroDiasNovo = Calendario.NumeroDias(new DateTime(2014, 9, 1), new DateTime(2017, 1, 2), LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

        throw new Exception(ParametrosConfiguracaoSistema.Outras.UsuarioIntegracao + "#" + ParametrosConfiguracaoSistema.Outras.SenhaIntegracao);
        
        throw new Exception("old: " + numeroDiasOld + " - new: " + numeroDiasNovo);
        
        int numeroDatas = 10;
        
        DateTime[] datasInicio = new DateTime[numeroDatas];
        DateTime[] datasFim = new DateTime[numeroDatas];

        DateTime dataInicioLoop = new DateTime(2009, 1, 1);
        DateTime dataFimLoop;
        Random random = new Random();
        int addDays = 0;
        for (int countData = 0; countData < numeroDatas; countData++)
        {
            addDays = random.Next(1, 10);
            dataInicioLoop = dataInicioLoop.AddDays(addDays);
            datasInicio[countData] = dataInicioLoop;
            addDays = random.Next(1, 200);
            dataFimLoop = dataInicioLoop.AddDays(addDays);
            datasFim[countData] = dataFimLoop;
            
        }
        
        /*datasInicio[0] = new DateTime(2013, 1, 1);
        datasInicio[1] = new DateTime(2013, 1, 2);
        datasInicio[2] = new DateTime(2013, 1, 3);
        datasInicio[3] = new DateTime(2013, 1, 4);
        datasInicio[4] = new DateTime(2013, 1, 5);
        datasInicio[5] = new DateTime(2013, 1, 6);
        datasInicio[6] = new DateTime(2013, 1, 7);
        datasInicio[7] = new DateTime(2013, 1, 8);
        datasInicio[8] = new DateTime(2013, 1, 9);
        datasInicio[9] = new DateTime(2013, 1, 10);

        datasInicio[10] = new DateTime(2013, 2, 5);
        datasInicio[11] = new DateTime(2013, 2, 9);
        datasInicio[12] = new DateTime(2013, 3, 7);
        datasInicio[13] = new DateTime(2013, 7, 4);
        datasInicio[14] = new DateTime(2013, 8, 5);
        datasInicio[15] = new DateTime(2013, 9, 6);
        datasInicio[16] = new DateTime(2013, 10, 7);
        datasInicio[17] = new DateTime(2013, 11, 8);
        datasInicio[18] = new DateTime(2013, 12, 9);
        datasInicio[19] = new DateTime(2013, 12, 10);

        datasInicio[20] = new DateTime(2013, 1, 1);
        datasInicio[21] = new DateTime(2013, 1, 1);
        datasInicio[22] = new DateTime(2013, 1, 2);
        datasInicio[23] = new DateTime(2013, 4, 21);
        datasInicio[24] = new DateTime(2013, 4, 20);
        datasInicio[25] = new DateTime(2013, 4, 22);
        
        
        datasFim[0] = new DateTime(2014, 1, 10);
        datasFim[1] = new DateTime(2014, 1, 9);
        datasFim[2] = new DateTime(2014, 1, 8);
        datasFim[3] = new DateTime(2014, 1, 7);
        datasFim[4] = new DateTime(2014, 1, 6);
        datasFim[5] = new DateTime(2014, 1, 5);
        datasFim[6] = new DateTime(2014, 1, 4);
        datasFim[7] = new DateTime(2014, 1, 3);
        datasFim[8] = new DateTime(2014, 1, 2);
        datasFim[9] = new DateTime(2014, 1, 1);

        datasFim[10] = new DateTime(2013, 2, 10);
        datasFim[11] = new DateTime(2013, 9, 7);
        datasFim[12] = new DateTime(2013, 3, 12);
        datasFim[13] = new DateTime(2013, 8, 7);
        datasFim[14] = new DateTime(2013, 10, 6);
        datasFim[15] = new DateTime(2013, 10, 5);
        datasFim[16] = new DateTime(2013, 11, 4);
        datasFim[17] = new DateTime(2013, 12, 3);
        datasFim[18] = new DateTime(2013, 12, 22);
        datasFim[19] = new DateTime(2014, 12, 21);

        datasFim[20] = new DateTime(2013, 5, 1);
        datasFim[21] = new DateTime(2013, 2, 1);
        datasFim[22] = new DateTime(2013, 5, 1);
        datasFim[23] = new DateTime(2013, 4, 28);
        datasFim[24] = new DateTime(2013, 4, 29);
        datasFim[25] = new DateTime(2013, 4, 27);*/
        
        for (int dataCount = 0; dataCount < datasInicio.Length; dataCount++)
        {
            DateTime dataInicio = datasInicio[dataCount];
            DateTime dataFim = datasFim[dataCount];

            numeroDiasOld = Calendario.NumeroDiasOld(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            numeroDiasNovo = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            
            if(numeroDiasNovo != numeroDiasOld){
                throw new Exception("Diferenša:" + dataInicio.Date + " - " + dataFim.Date);
            }

            numeroDiasOld = Calendario.NumeroDiasOld(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.NovaYorkBMF);
            numeroDiasNovo = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.NovaYorkBMF);

            if (numeroDiasNovo != numeroDiasOld)
            {
                throw new Exception("Diferenša:" + dataInicio.Date + " - " + dataFim.Date);
            }

            numeroDiasOld = Calendario.NumeroDiasOld(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Outros);
            numeroDiasNovo = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Outros);

            if (numeroDiasNovo != numeroDiasOld)
            {
                throw new Exception("Diferenša:" + dataInicio.Date + " - " + dataFim.Date);
            }

            numeroDiasOld = Calendario.NumeroDiasOld(dataInicio, dataFim, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            numeroDiasNovo = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            if (numeroDiasNovo != numeroDiasOld)
            {
                throw new Exception("Diferenša:" + dataInicio.Date + " - " + dataFim.Date);
            }
            
            numeroDiasOld = Calendario.NumeroDiasOld(dataInicio, dataFim, LocalFeriadoFixo.NovaYork, TipoFeriado.Brasil);
            numeroDiasNovo = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.NovaYork, TipoFeriado.Brasil);

            if (numeroDiasNovo != numeroDiasOld)
            {
                throw new Exception("Diferenša:" + dataInicio.Date + " - " + dataFim.Date);
            }

            numeroDiasOld = Calendario.NumeroDiasOld(dataInicio, dataFim, LocalFeriadoFixo.Bovespa, TipoFeriado.NovaYorkBMF);
            numeroDiasNovo = Calendario.NumeroDias(dataInicio, dataFim, LocalFeriadoFixo.Bovespa, TipoFeriado.NovaYorkBMF);
            
            if (numeroDiasNovo != numeroDiasOld)
            {
                throw new Exception("Diferenša:" + dataInicio.Date + " - " + dataFim.Date);
            }
        }
        
        context.Response.ContentType = "text/plain";
        context.Response.Write("Tudo igual");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}