<%@ WebHandler Language="C#" Class="PopulaPessoaCliente" %>

using System;
using System.Web;
using Financial.CRM;
using Financial.Investidor;

public class PopulaPessoaCliente : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        const int INIT_COUNT_PESSOA = 340780;
        const int PESSOAS_INSERIR = 5500;

        int countPessoa = INIT_COUNT_PESSOA;

        for (; countPessoa < (INIT_COUNT_PESSOA + PESSOAS_INSERIR); countPessoa++)
        {
            Pessoa pessoa = new Pessoa();
            pessoa.IdPessoa = countPessoa;
            pessoa.Nome = "Auto";
            pessoa.Apelido = "Auto";
            pessoa.Tipo = 1;
            pessoa.DataCadatro = DateTime.Now;
            pessoa.DataUltimaAlteracao = DateTime.Now;
            pessoa.Save();

            PessoaEndereco endereco = new PessoaEndereco();
            endereco.IdPessoa = countPessoa;
            endereco.RecebeCorrespondencia = "S";
            endereco.Endereco = "Auto";
            endereco.Numero = "1";
            endereco.Bairro = "Auto";
            endereco.Cidade = "Auto";
            endereco.Cep = "00000";
            endereco.Uf = "SP";
            endereco.Pais = "Brasil";
            endereco.Save();
            
            Cliente cliente = new Cliente();
            cliente.IdCliente = countPessoa;
            cliente.Nome = "Auto";
            cliente.Apelido = "Auto";
            cliente.IdTipo = 1;
            cliente.StatusAtivo = 1;
            cliente.Status = 2;
            cliente.DataDia = DateTime.Now;
            cliente.DataInicio = DateTime.Now;
            cliente.DataImplantacao = DateTime.Now;
            cliente.ApuraGanhoRV = "S";
            cliente.TipoControle = 5;
            cliente.IsentoIR = "N";
            cliente.IsentoIOF = "N";
            cliente.IdMoeda = 1;
            cliente.IdGrupoProcessamento = 1;
            cliente.IsProcessando = "N";
            cliente.StatusRealTime = 1;
            cliente.CalculaRealTime = "N";
            cliente.CalculaGerencial = "N";
            cliente.DescontaTributoPL = 2;
            cliente.GrossUP = 3;
            cliente.Save();
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}