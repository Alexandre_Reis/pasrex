﻿<%@ WebHandler Language="C#" Class="GetReport" %>

using System;
using System.Web;
using Financial.Relatorio;
using System.IO;

public class GetReport : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        
        DateTime dataInicio = ConvertStringToDateTime(context.Request.Params["dataInicio"]);
        DateTime dataFim = ConvertStringToDateTime(context.Request.Params["dataFim"]);
        //int? idCliente = 26458;

        string reportName = context.Request["report"];
        string cdAtivo = "";        
        
        Financial.Security.PermissaoCliente permissaoCliente = new Financial.Security.PermissaoCliente();
        int? idCliente = permissaoCliente.RetornaClienteAssociado(HttpContext.Current.User.Identity.Name);


        MemoryStream ms = new MemoryStream();
        if (reportName == "MapaResultado")
        {
            ReportMapaResultado report = new ReportMapaResultado(idCliente.Value, dataInicio, dataFim);
            report.ExportToPdf(ms);
        }
        else if (reportName == "MapaOperacaoBolsa")
        {
            string tipoMercado = "";
            ReportMapaOperacaoBolsa report = new ReportMapaOperacaoBolsa(dataInicio, dataFim, tipoMercado, idCliente, cdAtivo);
            report.ExportToPdf(ms);
        }
        else if (reportName == "MapaOperacaoBMF")
        {
            byte tipoMercado = (byte)Financial.Interfaces.Import.BMF.Enums.TipoMercadoBMF.Futuro;
            ReportMapaOperacaoBMF report = new ReportMapaOperacaoBMF(dataInicio, dataFim, tipoMercado, idCliente, cdAtivo);
            report.ExportToPdf(ms);
        }
        else if (reportName == "ExtratoContaCorrente")
        {
            ReportExtratoContaCorrente report = new ReportExtratoContaCorrente(idCliente.Value, dataInicio, dataFim, null, null);
            report.ExportToPdf(ms);
        }
        else if (reportName == "GanhoRendaVariavel")
        {
            ReportGanhoRendaVariavel report = new ReportGanhoRendaVariavel(idCliente.Value, dataInicio.Month, dataInicio.Year);
            report.ExportToPdf(ms);
        }
                
        ms.Seek(0, SeekOrigin.Begin);
        context.Response.ClearContent();
        context.Response.ClearHeaders();
        context.Response.Buffer = true;
        context.Response.Cache.SetCacheability(HttpCacheability.Private);
        context.Response.ContentType = "application/pdf";
        context.Response.AddHeader("Content-Length", ms.Length.ToString(System.Globalization.CultureInfo.CurrentCulture));
        context.Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}.pdf", reportName));

        context.Response.BinaryWrite(ms.ToArray());
        ms.Close();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
        context.Response.End();        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public DateTime ConvertStringToDateTime(string dataParam)
    {
        string[] dataParamSplit = dataParam.Split('-');
        return new DateTime(Convert.ToInt16(dataParamSplit[0]), Convert.ToInt16(dataParamSplit[1]), Convert.ToInt16(dataParamSplit[2]));
    }
    
}