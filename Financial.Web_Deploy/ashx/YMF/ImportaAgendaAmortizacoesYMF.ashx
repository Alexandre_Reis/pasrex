<%@ WebHandler Language="C#" Class="ImportaAgendaAmortizacoesYMF" %>


using System;
using System.Web;
using Financial.Fundo;
using Financial.Fundo.Enums;
using System.Data.SqlClient;
using Financial.Investidor;
using System.Collections.Generic;
using Financial.InvestidorCotista;
using System.Data;
using System.Text;


public class ImportaAgendaAmortizacoesYMF : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {

        SqlConnection conn = new Carteira().CreateSqlConnectionYMFCOT();

        //Carregar todos lancamentos de agenda de eventos presentes na YMF
        CarteiraCollection fundos = new CarteiraCollection();
        fundos.BuscaCarteirasFundosYMF(false);

        foreach (Carteira fundo in fundos)
        {

            AgendaFundoCollection agendaFundoCollection = new AgendaFundoCollection();
            agendaFundoCollection.Query.Where(agendaFundoCollection.Query.IdCarteira.Equal(fundo.IdCarteira));
            agendaFundoCollection.Load(agendaFundoCollection.Query);

            if (agendaFundoCollection.Count > 0)
            {
                continue;
            }

            int idCliente = fundo.IdCarteira.Value;
            //idCliente = 9091;
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            ClienteInterface clienteInterface = new ClienteInterface();
            clienteInterface.LoadByPrimaryKey(idCliente);
            string codFundo = clienteInterface.CodigoYMF;

            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataTable dataTable = new DataTable();
            dataTable.TableName = "COT_EVENTO";

            cmd.CommandText = String.Format(
                "select dt_evento, cot_evento.cd_fundo, cd_evento, vl_evento, " +
                "vl_cota_pre_evento, vl_cota_fechamento from {0} " +
                "left join cot_posicao_fundo on " +
                "cot_evento.cd_fundo = cot_posicao_fundo.cd_fundo " +
                "and cot_evento.dt_evento = cot_posicao_fundo.dt_posicao " +
                "where cot_evento.cd_fundo = '{1}'" +
                "order by dt_evento desc",
                dataTable.TableName, codFundo);

            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Connection = conn;

            adapter.SelectCommand = cmd;
            adapter.Fill(dataTable);

            if (dataTable.Rows.Count == 0)
            {
                continue;
            }
            
            foreach (DataRow dataRow in dataTable.Rows)
            {
                AgendaFundo agendaFundo = new AgendaFundo();
                agendaFundo.DataEvento = (DateTime)dataRow["DT_EVENTO"];
                agendaFundo.IdCarteira = fundo.IdCarteira;
                string cdEvento = (string)dataRow["CD_EVENTO"];
                     cdEvento = cdEvento.Trim().ToUpper();
                
                decimal cotaPre = Convert.ToDecimal(dataRow["VL_COTA_PRE_EVENTO"]);
                decimal cotaPos = Convert.ToDecimal(dataRow["VL_COTA_FECHAMENTO"]);

                decimal vlEvento = Convert.ToDecimal(dataRow["VL_EVENTO"]);
                if (cdEvento == "PRINC_JUR" || vlEvento > 0)
                {
                    decimal taxa = ((cotaPre - cotaPos) / cotaPre) * 100;
                    agendaFundo.Valor = 0;
                    agendaFundo.Taxa = taxa;

                    if (cdEvento == "PRINC_JUR")
                    {
                        agendaFundo.TipoEvento = (byte)TipoEventoFundo.AmortizacaoJuros;
                    }
                    else if (cdEvento == "PRINCIPAL")
                    {
                        agendaFundo.TipoEvento = (byte)TipoEventoFundo.Amortizacao;
                    }
                    else if (cdEvento == "JUROS")
                    {
                        agendaFundo.TipoEvento = (byte)TipoEventoFundo.Juros;
                    }
                    else
                    {
                        throw new Exception("Evento nao suportado: " + cdEvento + " fundo: " + fundo.IdCarteira);
                    }
                    agendaFundo.Save();
                }
            }
        }

        context.Response.ContentType = "text/plain";
        context.Response.Write("Agenda importada !");
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}