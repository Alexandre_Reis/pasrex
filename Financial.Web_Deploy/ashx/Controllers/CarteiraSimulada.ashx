<%@ WebHandler Language="C#" Class="CarteiraSimulada" %>

using System;
using System.Web;
using System.Collections.Generic;
using Financial.CRM;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Security;
using Financial.Security.Enums;
using Newtonsoft.Json;


public class CarteiraSimulada : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        CarteiraSimuladaViewModel carteiraSimuladaViewModel = new CarteiraSimuladaViewModel();

        string action = context.Request["action"];
        string pk = context.Request["pk"];

        JSONRetorno jsonRetorno = new JSONRetorno();
        if (action == "save")
        {
            try
            {
                carteiraSimuladaViewModel = this.Save(context, pk);
            }
            catch (Exception e)
            {
                jsonRetorno.erros.Add(e.Message);
            }
        }


        jsonRetorno.success = (jsonRetorno.erros.Count == 0);
        jsonRetorno.data = carteiraSimuladaViewModel;
        string json = JsonConvert.SerializeObject(jsonRetorno, Formatting.None);

        context.Response.ContentType = "application/json";
        context.Response.Write(json);
        
    }
 
    public CarteiraSimuladaViewModel Save(HttpContext context, string pk){
        CarteiraSimuladaViewModel carteiraSimuladaViewModel = new CarteiraSimuladaViewModel();
        carteiraSimuladaViewModel.IdCarteira = Convert.ToInt32(context.Request["IdCarteira"]);
        carteiraSimuladaViewModel.Nome = context.Request["Nome"];

        Pessoa pessoa = new Pessoa();
        pessoa.IdPessoa = carteiraSimuladaViewModel.IdCarteira.Value;
        pessoa.Nome = carteiraSimuladaViewModel.Nome;
        pessoa.Apelido = carteiraSimuladaViewModel.Nome;
        pessoa.Tipo = (byte)Financial.CRM.Enums.TipoPessoa.Fisica;
        pessoa.DataCadatro = System.DateTime.Now;
        pessoa.DataUltimaAlteracao = pessoa.DataCadatro;


        int idCarteira = carteiraSimuladaViewModel.IdCarteira.Value;
        
        //Encontrar template de carteira simulada
        Carteira templateCarteiraSimulada = new CarteiraCollection().BuscaCarteiraPorNome("%Template%", true);
        if(templateCarteiraSimulada == null){
            throw new Exception("Template de carteira n�o cadastrado");
        }
        int idCarteiraClonar = templateCarteiraSimulada.IdCarteira.Value;

        #region Salva o cliente de acordo com um Template
        Financial.Investidor.Cliente clienteReplicado = new Financial.Investidor.Cliente();
        Financial.Investidor.Cliente cliente = clienteReplicado.ReplicaCliente(idCarteira, idCarteiraClonar);
        // Troca o Nome e o Apelido
        cliente.Nome = carteiraSimuladaViewModel.Nome;
        cliente.Apelido = carteiraSimuladaViewModel.Nome;
        cliente.DataDia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        cliente.DataImplantacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        cliente.DataInicio = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        cliente.Status = (byte)StatusCliente.AbertoNaoCalculado;
        cliente.StatusRealTime = (byte)StatusRealTimeCliente.NaoExecutar;
        cliente.CalculaRealTime = "N";
        //
        cliente.StatusAtivo = (byte)StatusAtivoCliente.Ativo;
        cliente.TipoControle = (byte)TipoControleCliente.CarteiraSimulada;
        #endregion

        #region Salva a carteira de acordo com um Template
        Carteira carteiraReplicada = new Carteira();
        Carteira carteira = carteiraReplicada.ReplicaCarteira(idCarteira, idCarteiraClonar);
        // Troca o Nome e o Apelido
        carteira.Nome = carteiraSimuladaViewModel.Nome;
        carteira.Apelido = carteiraSimuladaViewModel.Nome;
        carteira.DataInicioCota = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        //
        carteira.StatusAtivo = (byte)Financial.Fundo.Enums.StatusAtivoCarteira.Ativo;
        //

        //Saves ao final
        try
        {
            pessoa.Save();
        }
        catch (Exception e)
        {
            throw new Exception("J� existe uma carteira cadastrada com o Id: " + pessoa.IdPessoa);
        }
        cliente.Save();        
        carteira.Save();
        #endregion

        
        //Permitir acesso aos usu�rios internos
        GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");

        usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
        usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

        UsuarioCollection usuarioCollection = new UsuarioCollection();
        usuarioCollection.Load(usuarioQuery);

        PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
        foreach (Usuario usuarioPermissao in usuarioCollection)
        {
            int idUsuario = usuarioPermissao.IdUsuario.Value;

            PermissaoCliente permissaoCliente = new PermissaoCliente();
            permissaoCliente.IdCliente = Convert.ToInt32(pessoa.IdPessoa.Value);
            permissaoCliente.IdUsuario = idUsuario;
            permissaoClienteCollection.AttachEntity(permissaoCliente);
        }
        permissaoClienteCollection.Save();

        return carteiraSimuladaViewModel;
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

    public class JSONRetorno
    {
        public bool success;
        public List<string> erros = new List<string>();
        public CarteiraSimuladaViewModel data;
    }
    
}


public class CarteiraSimuladaViewModel
{
    public int? IdCarteira;
    public string Nome;
    
    public CarteiraSimuladaViewModel()
    {

    }

    public CarteiraSimuladaViewModel(Carteira carteira)
    {
        this.IdCarteira = carteira.IdCarteira.Value;
    }
}