<%@ WebHandler Language="C#" Class="AjustaCustoCustodia" %>

using System;
using System.Web;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.Investidor;
using Financial.RendaFixa.Enums;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Web.Common;


public class AjustaCustoCustodia : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {

        const int ID_INICIAL_FUNDOS = 90000000;
        DateTime dataAnterior = new DateTime(2015, 6, 30);
        DateTime dataProc = new DateTime(2015, 7, 1);
        DateTime dataSeguinte = new DateTime(2015, 7, 2);
        DateTime dataPrimeiroDia = dataProc;

        

        //looping de datas
        do
        {
            PosicaoRendaFixaHistoricoCollection posClienteRFHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posClienteRFHistoricoCollection.Query.Select(posClienteRFHistoricoCollection.Query.IdCliente);
            posClienteRFHistoricoCollection.Query.GroupBy(posClienteRFHistoricoCollection.Query.IdCliente);
            posClienteRFHistoricoCollection.Query.Where(posClienteRFHistoricoCollection.Query.DataHistorico.Equal(dataProc),
                                                        posClienteRFHistoricoCollection.Query.IdCliente.LessThan(ID_INICIAL_FUNDOS));

            if (!posClienteRFHistoricoCollection.Query.Load())
            {
                return;
            }

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Calculando Taxa de cust�dia na data: " + dataProc.ToString(),
                                            HttpContext.Current.User.Identity.Name,
                                            "",
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion               

            foreach (PosicaoRendaFixaHistorico posCliRFHistorico in posClienteRFHistoricoCollection)
            {

                int idCliente = posCliRFHistorico.IdCliente.Value;

                TabelaCustosRendaFixaCollection tabelaCustosRendaFixaCollection = new TabelaCustosRendaFixaCollection();
                tabelaCustosRendaFixaCollection.Query.Where(tabelaCustosRendaFixaCollection.Query.DataReferencia.LessThanOrEqual(dataProc) &
                                                            tabelaCustosRendaFixaCollection.Query.TaxaAno.IsNotNull() &
                                                           (tabelaCustosRendaFixaCollection.Query.IdCliente.IsNull() | tabelaCustosRendaFixaCollection.Query.IdCliente.Equal(idCliente)));
                tabelaCustosRendaFixaCollection.Query.OrderBy(tabelaCustosRendaFixaCollection.Query.DataReferencia.Descending,
                    //Precisamos ordenar por especificidade de titulo e classe, para que primeiro sejam processados os mais especificos ateh os mais genericos
                        tabelaCustosRendaFixaCollection.Query.IdTitulo.Descending, tabelaCustosRendaFixaCollection.Query.Classe.Descending);
                tabelaCustosRendaFixaCollection.Query.Load();
                
                //Criar hashtable pra guardar posicoes que ja foram processadas. As posicoes que tiveram sido 
                //processadas por regras mais especificas nao podem ser processadas novamente
                Dictionary<int, int> posicoesProcessadas = new Dictionary<int, int>();

                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixa = new TituloRendaFixaQuery("T");
                PapelRendaFixaQuery papelRendaFixa = new PapelRendaFixaQuery("R");

                //utiliza PosicaoHistoricoRendaFixa                    
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdCliente,
                                            posicaoRendaFixaQuery.IdPosicao,
                                            posicaoRendaFixaQuery.ValorMercado,
                                            posicaoRendaFixaQuery.CustoCustodia,
                                            posicaoRendaFixaQuery.DataHistorico,
                                            posicaoRendaFixaQuery.IdAgente,
                                            posicaoRendaFixaQuery.IdTitulo,
                                            posicaoRendaFixaQuery.DataOperacao,
                                            papelRendaFixa.Classe.As("Classe"));
                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixa).On(posicaoRendaFixaQuery.IdTitulo == tituloRendaFixa.IdTitulo);
                posicaoRendaFixaQuery.InnerJoin(papelRendaFixa).On(papelRendaFixa.IdPapel == tituloRendaFixa.IdPapel);
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.DataHistorico.Equal(dataProc),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                            posicaoRendaFixaQuery.DataOperacao.LessThan(dataProc));

                posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

                foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
                {
                    decimal taxaAno = 0;
                    int idAgentePos = 0;
                    int idTituloPos = posicaoRendaFixa.IdTitulo.Value;
                    if( posicaoRendaFixa.IdAgente.HasValue)
                    {
                        idAgentePos = posicaoRendaFixa.IdAgente.Value;
                    }                   
                    
                    string classePos = posicaoRendaFixa.GetColumn("Classe").ToString();
                    
                    foreach (TabelaCustosRendaFixa tabelaCustosRendaFixa in tabelaCustosRendaFixaCollection)
                    {
                        int idAgenteTax = tabelaCustosRendaFixa.IdAgente.Value;
                        
                        int idTituloTax = 0;                        
                        if (tabelaCustosRendaFixa.IdTitulo.HasValue)
                        {
                            idTituloTax = tabelaCustosRendaFixa.IdTitulo.Value;
                        }
                        int idClienteTax = 0;
                        if (tabelaCustosRendaFixa.IdCliente.HasValue)
                        {
                            idClienteTax = tabelaCustosRendaFixa.IdCliente.Value; 
                        }
                        string classeTax = "";
                        if (tabelaCustosRendaFixa.Classe.HasValue)
                        {
                            classeTax = tabelaCustosRendaFixa.Classe.Value.ToString(); 
                        }

                        if (idClienteTax > 0 && idTituloTax == 0)
                        {
                            taxaAno = tabelaCustosRendaFixa.TaxaAno.Value;
                            break;
                        } 
                        
                        if( (idTituloPos == idTituloTax || classeTax == classePos) && 
                            ( idAgenteTax == idAgentePos ) && 
                            ( idCliente == idClienteTax || idClienteTax == 0))
                        {
                            taxaAno =  tabelaCustosRendaFixa.TaxaAno.Value;
                            break;
                        }
                    }
                    if (taxaAno == 0)
                    {
                        continue;
                    }
                    
                    if (!posicoesProcessadas.ContainsKey(posicaoRendaFixa.IdPosicao.Value))
                    {
                        decimal valorPosicao = posicaoRendaFixa.ValorMercado.HasValue ? posicaoRendaFixa.ValorMercado.Value : 0;

                        DateTime dataOperacao = posicaoRendaFixa.DataOperacao.Value;
                        int primeiroDiaOper = Calendario.NumeroDias(dataOperacao, dataProc);
                        int numeroDias = Calendario.NumeroDias(dataAnterior, dataProc);
                        decimal custoCustodiaDia = Utilitario.Truncate(valorPosicao * ((decimal)Math.Pow((double)(taxaAno / 100M + 1), (double)(numeroDias / 365M)) - 1), 2);

                        posicaoRendaFixa.CustoCustodia = posicaoRendaFixa.CustoCustodia.HasValue ? posicaoRendaFixa.CustoCustodia.Value : 0;

                        if (primeiroDiaOper ==  1 || dataPrimeiroDia == dataProc)
                        {
                            posicaoRendaFixa.CustoCustodia = custoCustodiaDia;
                        }
                        else
                        {
                            posicaoRendaFixa.CustoCustodia += custoCustodiaDia;
                        } 
                       
                        posicoesProcessadas.Add(posicaoRendaFixa.IdPosicao.Value, 1);

                        decimal custoCustodia = posicaoRendaFixa.CustoCustodia.Value;


                        #region atualiza posicao historica em d+1
                        PosicaoRendaFixaHistorico posHistoricaId = new PosicaoRendaFixaHistorico();
                        posHistoricaId.Query.Select(posHistoricaId.Query.CustoCustodia,
                                                   posHistoricaId.Query.IdPosicao,
                                                   posHistoricaId.Query.DataHistorico);
                        posHistoricaId.Query.Where(posHistoricaId.Query.IdCliente.Equal(idCliente),
                                                  posHistoricaId.Query.IdPosicao.Equal(posicaoRendaFixa.IdPosicao.Value),
                                                  posHistoricaId.Query.DataHistorico.Equal(dataSeguinte));

                        if (posHistoricaId.Query.Load())
                        {
                            posHistoricaId.CustoCustodia = custoCustodia;
                            posHistoricaId.Save();
                        }
                        #endregion                            

                        #region atualiza posicao de abertura em d+1
                        PosicaoRendaFixaAbertura posAberturaId = new PosicaoRendaFixaAbertura();
                        posAberturaId.Query.Select(posAberturaId.Query.CustoCustodia,
                                                   posAberturaId.Query.IdPosicao,
                                                   posAberturaId.Query.DataHistorico);
                        posAberturaId.Query.Where(posAberturaId.Query.IdCliente.Equal(idCliente),
                                                  posAberturaId.Query.IdPosicao.Equal(posicaoRendaFixa.IdPosicao.Value),
                                                  posAberturaId.Query.DataHistorico.Equal(dataSeguinte));
                        
                        if(posAberturaId.Query.Load()) 
                        {                                
                            posAberturaId.CustoCustodia = custoCustodia;
                            posAberturaId.Save();
                        }
                        #endregion

                    }
                }
                if (posicaoRendaFixaCollection != null)
                {
                    posicaoRendaFixaCollection.Save();
                }

            }
            // fim de uma data
            dataAnterior = dataProc;
            dataProc = Calendario.AdicionaDiaUtil(dataProc, 1);
            dataSeguinte = Calendario.AdicionaDiaUtil(dataProc, 1);

        } while (true);
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
