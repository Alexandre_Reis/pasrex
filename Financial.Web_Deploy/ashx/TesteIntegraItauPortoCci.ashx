<%@ WebHandler Language="C#" Class="TesteItau.TesteIntegraItauPortoCci" %>

using System;
using System.Web;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Net;
using Financial.Interfaces.Export;
using Financial.Investidor;

namespace TesteItau
{
    public class TesteIntegraItauPortoCci : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            Itau itau = new Itau();
            int idConta = Convert.ToInt32(context.Request["idconta"]);// 1000048214;
            int idPessoa = Convert.ToInt32(context.Request["idpessoa"]);// 1000048214;
            bool showPostData = context.Request["showpostdata"] == "1";
            bool showResponseData = context.Request["showresponsedata"] == "1";


            ContaCorrente contaCorrente = new ContaCorrente();

            contaCorrente.LoadByPrimaryKey(idConta);
            XmlDocument xmlDocument = itau.RetornaCci_XML(idPessoa, contaCorrente, "I");

            string urlSiteItau = "https://www.itaucustodia.com.br/PassivoWebServices/xmlcci.jsp";
            string postData = "strXML=" + xmlDocument.OuterXml.Replace("BUSSINESSID", "EBUSINESSID");
            
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(urlSiteItau);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = postData.Length;
            string proxyInfo = System.Configuration.ConfigurationManager.AppSettings["WebServiceProxy"];

            if (!string.IsNullOrEmpty(proxyInfo))
            {
                //Utilizar padrao de proxy definido no Windows
                webRequest.Proxy = System.Net.WebProxy.GetDefaultProxy();
                webRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                if (proxyInfo != "default")
                {
                    //Um proxy especifico foi configurado
                    string[] proxyInfos = proxyInfo.Split(';');

                    //Setar url do proxy
                    webRequest.Proxy = new System.Net.WebProxy(proxyInfos[0]);

                    if (proxyInfos.Length > 1)
                    {
                        //Foram passadas tambem as credenciais de rede
                        webRequest.Proxy.Credentials = new System.Net.NetworkCredential(proxyInfos[1], proxyInfos[2], proxyInfos[3]);
                    }
                }
            }
            
            using (StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                requestWriter.Write(postData);
            }

            if (showPostData)
            {
                //context.Response.ContentType = "text/xml";
                context.Response.ContentType = "text/plain";
                context.Response.Write(postData.Replace("strXML=", ""));
                return;
            }

            //  This actually does the request and gets the response back
            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

            string responseData = string.Empty;

            using (StreamReader responseReader = new StreamReader(response.GetResponseStream()))
            {
                // dumps the HTML from the response into a string variable
                responseData = responseReader.ReadToEnd();
            }

            ItauMsg retornoItau = null;
            XmlSerializer serializer = new XmlSerializer(typeof(ItauMsg));

            using (StringReader reader = new StringReader(responseData))
            {
                retornoItau = (ItauMsg)serializer.Deserialize(reader);
            }

            string msgRetorno = "";
            string codCotista = "";
            foreach (Param param in retornoItau.parameter)
            {
                if (param.id.ToUpper() == "MSGRETORNO")
                {
                    msgRetorno = param.value;
                }
                if (param.id.ToUpper() == "CODCOTISTA")
                {
                    codCotista = param.value;
                }
            }

            if (showResponseData)
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write(responseData);
                return;
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write("MsgRetorno: " + msgRetorno + " - CodCotista: " + codCotista);
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    [XmlRoot(ElementName = "itaumsg")]
    public class ItauMsg
    {
        public List<Param> parameter = new List<Param>();
        public ItauMsg()
        {

        }
    }

    public enum ParamTypes
    {
        number,
        text
    }

    [XmlType(TypeName = "param")]
    public class Param
    {
        [XmlAttribute]
        public string id;

        [XmlAttribute]
        public string value;
        public Param()
        {
        }
        public Param(string idParam, string valueParam, ParamTypes paramType, int paramLength)
        {
            this.id = idParam.PadRight(11, ' ');

            string formattedValue = "";
            if (valueParam.Length > paramLength)
            {
                //Nao eh necessario preencher com fillers, apenas capar no tamanho passado
                formattedValue = valueParam.Substring(0, paramLength);
            }
            else
            {
                //Precisamos preencher com 0s ou brancos
                if (paramType == ParamTypes.number)
                {
                    formattedValue = valueParam.PadLeft(paramLength, '0');
                }
                else if (paramType == ParamTypes.text)
                {
                    formattedValue = valueParam.PadRight(paramLength, ' ');
                }
                else
                {
                    throw new Exception("Tipo de par�metro n�o suportado");
                }
            }

            this.value = formattedValue;
        }
    }
}