<%@ WebHandler Language="C#" Class="CorrigeCEP" %>

using System;
using System.Web;
using Financial.CRM;

public class CorrigeCEP : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        return;
        PessoaEnderecoCollection pessoaEnderecoCollection = new PessoaEnderecoCollection();
        pessoaEnderecoCollection.LoadAll();

        bool houveAlteracao = false;
        foreach(Financial.CRM.PessoaEndereco pessoaEndereco in pessoaEnderecoCollection){
            string cep = pessoaEndereco.Cep;
            if (cep.Length == 7 && !cep.Contains("-"))
            {
                //"1234567" virar "01234-567"
                houveAlteracao = true;
                string novoCep = "0" + cep.Substring(0, 4) + "-" + cep.Substring(4, 3);
                pessoaEndereco.Cep = novoCep;
            }
            else if (cep.Length == 8 && !cep.Contains("-"))
            {
                //"12345678" virar "12345-678"
                houveAlteracao = true;
                string novoCep = cep.Substring(0, 5) + "-" + cep.Substring(5, 3);
                pessoaEndereco.Cep = novoCep;
            }
            
            
        }

        if (houveAlteracao)
        {
            pessoaEnderecoCollection.Save();
        }
        
        context.Response.ContentType = "text/plain";
        context.Response.Write("CEPs alterados com sucesso");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}