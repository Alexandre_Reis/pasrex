﻿<%@ page language="C#" autoeventwireup="true" inherits="Interfaces_RankingAnbima, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<title>Untitled Page</title>
<head runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript">    // Select All dos AspGridLookup
    

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxpc:ASPxPopupControl ID="popupCarteiras" AllowDragging="true" PopupElementID="popupCarteiras"
            EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Ranking Anbima - Controladoria de Ativo"
            runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <table>
                        <tr>
                            <td class="td_Label" style="width: 50px;">
                                <asp:Label ID="label4" runat="server" CssClass="labelRequired" Text="Data:" />
                            </td>
                            <td>
                                <dxe:ASPxDateEdit ID="dataRanking" runat="server" ClientInstanceName="dataRanking"
                                    EditFormat="Custom" EditFormatString="MM/yyyy" />
                            </td>
                        </tr>
                        <tr>
                            <td class="td_Label" style="width: 50px;">
                                <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Tipo Ranking:" />
                            </td>
                            <td>
                                <dxe:ASPxComboBox ID="tipoRanking" runat="server" CssClass="dropDownList" ClientInstanceName="tipoRanking">
                                    <Items>
                                        <dxe:ListEditItem Value="A" Text="Ativo" />
                                        <dxe:ListEditItem Value="P" Text="Passivo" />
                                    </Items>
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                    <div class="divDataGrid">
                        <dxwgv:ASPxGridView ID="gridConsulta" runat="server" ClientInstanceName="gridConsulta"
                            AutoGenerateColumns="False" DataSourceID="EsDSCarteira" KeyFieldName="IdCarteira"
                            OnHtmlRowCreated="gridConsulta_HtmlRowCreated" Width="850px">
                            <Columns>
                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                    <HeaderStyle HorizontalAlign="Center"/>
                                    <HeaderTemplate>
                                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" ClientSideEvents-ValueChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" OnInit="CheckBoxSelectAll"/>
                                    </HeaderTemplate>
                                </dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" ReadOnly="True" VisibleIndex="1"
                                    Width="7%" CellStyle-HorizontalAlign="left">
                                    <CellStyle HorizontalAlign="Left">
                                    </CellStyle>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="2" Width="25%" Settings-AutoFilterCondition="Contains">
                                    <Settings AutoFilterCondition="Contains"></Settings>
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <SettingsPager Mode="ShowAllRecords">
                            </SettingsPager>
                            <Settings ShowFilterRow="True" ShowStatusBar="Visible" VerticalScrollBarMode="Visible" />
                            <SettingsText EmptyDataRow="0 registros" />
                            <Images />
                            <Styles>
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                <AlternatingRow Enabled="True" />
                                <Cell Wrap="False" />
                            </Styles>
                            <SettingsCommandButton>
                                <ClearFilterButton Image-Url="../imagens/funnel--minus.png">
                                    <Image Url="../imagens/funnel--minus.png">
                                    </Image>
                                </ClearFilterButton>
                            </SettingsCommandButton>
                        </dxwgv:ASPxGridView>
                    </div>
                    <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                        <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                            CssClass="btnOK" OnClientClick="popupCarteiras.Hide(); CallbackPanel.PerformCallback('a'); return false;">
                            <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                            </div>
                        </asp:LinkButton>
                        <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                            CssClass="btnFilterCancel" OnClientClick="tipoRanking.SetValue(); dataRanking.SetDate(); cbAll.SetChecked(false); OnAllCheckedChanged(cbAll,null,gridConsulta); CallbackPanel.PerformCallback(); return false;">
                            <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                            </div>
                        </asp:LinkButton>
                    </div>
                    </br>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <div class="linkButton">
            <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                CssClass="btnFilter" OnClientClick="popupCarteiras.Show(); return false;">
                <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                </div>
            </asp:LinkButton>
        </div>
        <dxcp:ASPxCallbackPanel runat="server" ID="CallbackPanel" Height="250px" ClientInstanceName="CallbackPanel"
            OnCallback="callback_Panel">
            <PanelCollection>
                <dxp:PanelContent ID="panelPai" runat="server">
                    <dxp:PanelContent ID="geralPanel" runat="server" Visible="false">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="padding-top: 7px; vertical-align: top;">
                                    </td>
                                    <td>
                                        <div class="Spacer" style="width: 13px;">
                                        </div>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Instituição:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="instituicaoLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Código da instituição:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="instituicaoCodLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Responsável:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="responsavelLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Telefone para Contato:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="telefoneLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    E-mail:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="emailLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                    <dxp:PanelContent ID="ativoPanel" runat="server" Visible="false">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="padding-top: 7px; vertical-align: top;">
                                    </td>
                                    <td>
                                        <div class="Spacer" style="width: 13px;">
                                        </div>
                                    </td>
                                    <td>
                                        <table class="OptionsTable" style="border-collapse: separate">
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Total Geral de Ativos:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="totalGeralAtivosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td style="padding-top: 7px; vertical-align: top;">
                                    </td>
                                    <td>
                                        <div class="Spacer" style="width: 13px;">
                                        </div>
                                    </td>
                                    <td>
                                        <table class="OptionsTable" style="border-collapse: separate">
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Total de Ativos(Próprios):
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="totalAtivosPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Adm - Fundos de Investimentos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="admFundoInvestimentosPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Adm - Carteira Administrada:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="admCarteiraAdministradaPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Adm - Total:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="admTotalPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    EFPC - Fundos de Investimento:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="efpcFundosDeInvestimentoPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    EFPC - Carteira Administrada:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="efpcCarteiraAdministradaPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    EFPC - Total:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="efpcTotalPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Empresas - Fundos de Investimentos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="empFundosDeInvestimentosPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Empresas - Carteira Administrada:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="empCarteiraAdministradaPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Empresas - Total
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="empTotalPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Seguradoras - Fundos de Investimentos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="segFundosDeInvestimentosPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Seguradoras - Carteira Administrada:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="segCarteiraAdministradaPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Seguradoras - Total:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="segTotalPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    FIDC:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="fidcPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Clubes de Investimentos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="clubesDeInvestimentosPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Outros Investidores Institucionais:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="outrosInvestidoresInstitucionaisPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Número de Clientes:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="numeroClientesPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Quantidade de Veículos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="quantidadeVeiculosPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td style="padding-top: 7px; vertical-align: top;">
                                    </td>
                                    <td>
                                        <div class="Spacer" style="width: 13px;">
                                        </div>
                                    </td>
                                    <td>
                                        <table class="OptionsTable" style="border-collapse: separate">
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Total de Ativos(Outra Instituição):
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="totalAtivosOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Adm - Fundos de Investimentos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="admFundoInvestimentosOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Adm - Carteira Administrada:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="admCarteiraAdministradaOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Adm - Total:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="admTotalOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    EFPC - Fundos de Investimento:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="efpcFundosDeInvestimentoOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    EFPC - Carteira Administrada:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="efpcCarteiraAdministradaOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    EFPC - Total:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="efpcTotalOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Empresas - Fundos de Investimentos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="empFundosDeInvestimentosOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Empresas - Carteira Administrada:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="empCarteiraAdministradaOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Empresas - Total
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="empTotalOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Seguradoras - Fundos de Investimentos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="segFundosDeInvestimentosOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Seguradoras - Carteira Administrada:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="segCarteiraAdministradaOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Seguradoras - Total:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="segTotalOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    FIDC:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="fidcOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Clubes de Investimentos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="clubesDeInvestimentosOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Outros Investidores Institucionais:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="outrosInvestidoresInstitucionaisOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Número de Clientes:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="numeroClientesOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Quantidade de Veículos:
                                                </td>
                                                <td class="dxcp_tInfo">
                                                    <dxe:ASPxLabel ID="quantidadeVeiculosOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                    <dxp:PanelContent ID="passivoPanel" runat="server" Visible="false">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="padding-top: 7px; vertical-align: top;">
                                    </td>
                                    <td>
                                        <div class="Spacer" style="width: 13px;">
                                        </div>
                                    </td>
                                    <td>
                                        <table class="OptionsTable" style="border-collapse: separate">
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Total Geral de Veiculos:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="totalGeralVeiculosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Total Geral de Cotistas:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="totalGeralCotistasLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td style="padding-top: 7px; vertical-align: top;">
                                    </td>
                                    <td>
                                        <div class="Spacer" style="width: 13px;">
                                        </div>
                                    </td>
                                    <td>
                                        <table class="OptionsTable" style="border-collapse: separate">
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Total de Veiculos:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="totalVeiculosPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Fundos de Investimentos:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="fundosPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Clubes de Investimentos:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="clubesPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td class="dxcp_tCategory">
                                                    Numero de Cotistas:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="numeroCotistasPropriosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table>
                            <tbody>
                                <tr>
                                    <td style="padding-top: 7px; vertical-align: top;">
                                    </td>
                                    <td>
                                        <div class="Spacer" style="width: 13px;">
                                        </div>
                                    </td>
                                    <td>
                                        <table class="OptionsTable" style="border-collapse: separate">
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Total de Veiculos:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="totalVeiculosOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Fundos de Investimentos:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="fundosOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="dxcp_tCategory">
                                                    Clubes de Investimentos:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="clubesOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td class="dxcp_tCategory">
                                                    Numero de Cotistas:
                                                </td>
                                                <td class="dxcp_tInf o">
                                                    <dxe:ASPxLabel ID="numeroCotistasOutrosLabel" runat="server">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </dxp:PanelContent>
                </dxp:PanelContent>
            </PanelCollection>
        </dxcp:ASPxCallbackPanel>
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect"
            LowLevelBind="true" />
    </form>
</body>
</html>
