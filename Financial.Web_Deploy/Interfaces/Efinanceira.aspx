﻿<%@ page language="C#" autoeventwireup="true" inherits="CadastrosBasicos_Efinanceira, Financial.Web_Deploy" validaterequest="false" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <style>
    .statusArea{        
        width:500px;
        height:200px;
        border:1px solid #999999;
        font-family:Consolas,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New, monospace;
        }
    }
    </style>

    <script type="text/javascript">    
        
        function Download(idRecon){
        
            var newLocation = document.URL + '?Download=true&IdRecon=' + idRecon;
            debugger;
            var iframeId = 'iframe_download';
            var iframe = document.getElementById(iframeId);
            
            if (!iframe && document.createElement && (iframe = document.createElement('iframe'))) {
               
                iframe.name = iframe.id = iframeId;
                iframe.width = 0;
                iframe.height = 0;
                iframe.src = 'about:blank';
                document.body.appendChild(iframe);                                                                    
            }
            iframe.src = newLocation;
            
            
        }
        
        
        function gridFila_CustomButtonClick (idRecon){
            if(window.confirm("Tem certeza que deseja cancelar esta E-Financeira ?"))
                gridFila.PerformCallback(idRecon);
        }
        
        function Uploader_OnUploadComplete(args) {                
            if (args.errorText != '')
                alert(args.errorText);
            else
                gridFila.UpdateEdit();
                
            callbackAtualiza.SendCallback()
                
        }
        
        var interval;
        function SetaTimeoutAtualizacaoRecon(s,e) {
            if( typeof s != "undefined" )
            {   
                if(typeof s.GetChecked != "undefined")
                {
                    if(!s.GetChecked())
                    {
                        clearInterval(interval);                    
                        return;
                    }
                }
            }
            
            interval = setInterval(function(){callbackAtualiza.SendCallback()},timeoutAtualizacaoRecon.value);
        }
                
        
        
        window.onload= SetaTimeoutAtualizacaoRecon;
        
        
    </script>

    <title>Recon Anbima</title>
</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackAtualiza" runat="server" OnCallback="callbackAtualiza_Callback">
            <ClientSideEvents CallbackError="function(s,e){  
            e.handled = true;
        }" />
            <ClientSideEvents CallbackComplete="function(s,e){             
            
                gridFila.Refresh();
                gridCadastro.Refresh();
            
        }" />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackDownload" runat="server" OnCallback="callbackDownload_Callback">            
            <ClientSideEvents CallbackError="function(s,e){  
            e.handled = true;
        }" />
            <ClientSideEvents CallbackComplete="function(s,e){    
            debugger;
              if(e.result == 'PossuiArquivo')
              {
                Download(e.parameter);              
              }                       
        }" />
        </dxcb:ASPxCallback>
        <asp:HiddenField ID="timeoutAtualizacaoRecon" runat="server" />
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="mainContent">
                                <div>
                                    <div class="linkButton">
                                        <div style="width:33%;display:inline-block">
                                            <asp:LinkButton ID="btnAddRecon" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                                CssClass="btnEdit" OnClientClick="gridFila.AddNewRow(); return false;">
                                                <asp:Literal ID="Literal10" runat="server" Text="Add E-Financeira" />
                                                <div>
                                                </div>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnAtualiza" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                                CssClass="btnRefresh" OnClientClick="callbackAtualiza.SendCallback(); return false;">
                                                <asp:Literal ID="Literal1" runat="server" Text="Atualizar" />
                                                <div>
                                                </div>
                                            </asp:LinkButton>
                                            <dxe:ASPxCheckBox ID="teste" runat="server" Checked="true">
                                                <ClientSideEvents CheckedChanged="function(s, e) { 
                                                SetaTimeoutAtualizacaoRecon(s, e); 
                                            }" />
                                            </dxe:ASPxCheckBox>
                                            Atualização automatica
                                        </div>
                                        <div style="width:33%;display: inline-block;text-align: center;">Filas</div>
                                        <div style="width:33%;display: inline-block;"></div>
                                    </div>
                                    <dxwgv:ASPxGridView ID="gridFila" runat="server" KeyFieldName="IdRecon" AutoGenerateColumns="False"
                                        OnHtmlRowPrepared="gridFila_HtmlRowPrepared" 
                                        DataSourceID="EsDSFila" ClientSideEvents-CustomButtonClick="function(s,e){ gridFila_CustomButtonClick(s.keys[e.visibleIndex]) }"
                                        OnRowUpdating="gridFila_RowUpdating" OnCustomCallback="gridFila_CustomCallback"
                                        OnRowInserting="gridFila_RowInserting" ClientSideEvents-RowDblClick="function(s, e) {editFormOpen=true; gridFila.StartEditRow(e.visibleIndex);}">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ButtonType="Image" Width="5%">
                                                <CustomButtons>
                                                    <dxwgv:GridViewCommandColumnCustomButton ID="btnCancela" Image-Url="../imagens/delete.png"
                                                        Text="Cancelar">
                                                    </dxwgv:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdRecon" Caption="ID E-Financeira" Width="5%" VisibleIndex="1">
                                                <PropertiesTextEdit MaxLength="5" DisplayFormatString="#000000000">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DataRegistro" Width="9%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="60">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Usuario" Width="12%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="60">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DataInicioProcesso" Width="9%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="60">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>                                            
                                            <dxwgv:GridViewDataMemoColumn FieldName="Status" Width="35%" VisibleIndex="2">
                                                
                                            </dxwgv:GridViewDataMemoColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Observacao" Width="30%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="60">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="label1" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelIdRecon" runat="server" CssClass="labelRequired" Text="Id E-Financeira:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textIdRecon" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("IdRecon")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataRegistro" runat="server" CssClass="labelRequired" Text="Data Registro:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textDataRegistro" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("DataRegistro")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataInicioProcesso" runat="server" CssClass="labelRequired" Text="Data Inicio Processo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textDataInicioProcesso" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("DataInicioProcesso")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataFimProcesso" runat="server" CssClass="labelRequired" Text="Data Inicio Processo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textDataFimProcesso" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("DataFimProcesso")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelUsuario" runat="server" CssClass="labelRequired" Text="Usuario:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textUsuario" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("Usuario")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelObservacao" runat="server" CssClass="labelRequired" Text="Observacao:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" CssClass="textNome" 
                                                                        Enabled="false" Text='<%#Eval("Observacao")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelStatus" runat="server" CssClass="labelRequired" Text="Status:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textStatus" runat="server" TextMode="MultiLine" CssClass="textNome"
                                                                        Enabled="false" Text='<%#Eval("Status")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" OnClick="btnOK_Click"
                                                                OnLoad="btnOk_Load">
                                                                <asp:Literal ID="Literal13" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridFila.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal14" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                        </Templates>
                                    </dxwgv:ASPxGridView>
                                </div>
                                <br />
                                <div class="divDataGrid">
                                    <div class="linkButton">
                                        <div style="width: 50%; margin: 0 auto; text-align: center">
                                            Executadas</div>
                                    </div>
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdRecon" AutoGenerateColumns="False"  
                                        OnCustomButtonInitialize="gridCadastro_CustomButtonInitialize" DataSourceID="EsDSRecon" EnableCallBacks="true" 
                                        OnHtmlRowPrepared="gridCadastro_HtmlRowPrepared"
                                        ClientSideEvents-CustomButtonClick="function(s,e){ callbackDownload.PerformCallback(s.keys[e.visibleIndex])}">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="3%" ButtonType="Image">
                                                <CustomButtons>
                                                    <dxwgv:GridViewCommandColumnCustomButton  ID="downBtn" Image-Url="../imagens/document-xml.png"
                                                        Text="Download">
                                                    </dxwgv:GridViewCommandColumnCustomButton>
                                                    <dxwgv:GridViewCommandColumnCustomButton  ID="canceladoBtn" Image-Url="../imagens/exclamation-red-frame.png"
                                                        Text="Cancelado">
                                                    </dxwgv:GridViewCommandColumnCustomButton>
                                                </CustomButtons>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdRecon" Caption="ID E-Financeira" Width="5%" VisibleIndex="1">
                                                <PropertiesTextEdit MaxLength="5" DisplayFormatString="#000000000">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DataRegistro" Width="10%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="60">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Usuario" Width="12%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="60">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>                                                                                        
                                            <dxwgv:GridViewDataTextColumn FieldName="DataInicioProcesso" Width="10%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="60">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DataFimProcesso" Width="10%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="60">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Observacao" Width="30%" VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="60">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelIdRecon" runat="server" CssClass="labelRequired" Text="Id E-Financeira:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textIdRecon" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("IdRecon")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataRegistro" runat="server" CssClass="labelRequired" Text="Data Registro:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textDataRegistro" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("DataRegistro")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataInicioProcesso" runat="server" CssClass="labelRequired" Text="Data Inicio Processo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textDataInicioProcesso" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("DataInicioProcesso")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataFimProcesso" runat="server" CssClass="labelRequired" Text="Data Inicio Processo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textDataFimProcesso" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("DataFimProcesso")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelUsuario" runat="server" CssClass="labelRequired" Text="Usuario:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textUsuario" runat="server" CssClass="textNome" Enabled="false"
                                                                        Text='<%#Eval("Usuario")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelObservacao" runat="server" CssClass="labelRequired" Text="Observacao:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" CssClass="textNome"
                                                                        Enabled="false" Text='<%#Eval("Observacao")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelStatus" runat="server" CssClass="labelRequired" Text="Status:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textStatus" runat="server" TextMode="MultiLine" CssClass="statusArea"
                                                                        Enabled="false" Text='<%#Eval("Status")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                        </Templates>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <cc1:esDataSource ID="EsDSRecon" runat="server" OnesSelect="EsDSRecon_esSelect" LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSFila" runat="server" OnesSelect="EsDSFila_esSelect" LowLevelBind="true" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Exportando arquivo, aguarde..."
            ClientInstanceName="LoadingPanel" Modal="true" />
    </form>
</body>
</html>
