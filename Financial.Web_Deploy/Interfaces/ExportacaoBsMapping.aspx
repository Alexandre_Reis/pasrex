﻿<%@ page language="C#" autoeventwireup="true" inherits="Interfaces_ExportacaoBsMapping, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    
    <script type="text/javascript">
    function btnExportaClick(tipoData){           
        LoadingPanel.Show();
        callbackExporta.SendCallback(tipoData);
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackExporta" runat="server" OnCallback="callbackExporta_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                
                LoadingPanel.Hide();
                alert('Exportação concluida com sucesso');
                }" />  
            <ClientSideEvents CallbackError="function(s,e){LoadingPanel.Hide()}" />       
        </dxcb:ASPxCallback>
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="bs-mapping" />
                            </div>
                            <div id="mainContentSpace">
                                        <br />
                                        <br />
                                        <table border="0" align="center">
                                            <tr>
                                                <td>
                                                    <dxrp:ASPxRoundPanel ID="pnlPrincipal" runat="server" HeaderText=" " HeaderStyle-Font-Size="11px"
                                                        Width="459px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <br />
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelNormal" Text="Data Referência:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" />
                                                                        </td>
                                                                        <td>
                                                                            <div id="Div1" class="linkButton linkButtonNoBorder  linkButtonNoMargin">
                                                                                <asp:LinkButton ID="btnExportar" runat="server" Font-Overline="False" CssClass="btnRun" 
                                                                                    OnClientClick="btnExportaClick()">
                                                                                <asp:Literal ID="Literal2" runat="server" Text="Exportar" /><div>
                                                                                </div>
                                                                                </asp:LinkButton>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                        <HeaderStyle Font-Size="11px" />
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                        </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
         <div id="mainContent">
            
        <div class="linkButton" >               
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
    
        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" ClientInstanceName="gridCadastro" runat="server" EnableCallBacks="true"
            KeyFieldName="IdLog" DataSourceID="EsDSLoadLog"
            OnHtmlRowCreated="gridCadastro_HtmlRowCreated"  
            
            
            OnCellEditorInitialize="gridCadastro_CellEditorInitialize">        
                    
            <Columns>                           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" >
                
                   
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="IdLog" Caption="Id" VisibleIndex="1" Width="5%">                
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="IdExec" Width="5%" VisibleIndex="2">                
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="DbUserId" Width="8%" VisibleIndex="2">                
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="ProcedureName" Width="15%" VisibleIndex="2">                
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="DataInicioProcesso" Width="8%" VisibleIndex="2">                
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="DataFimProcesso" Width="8%" VisibleIndex="2">                
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Sucesso" Width="5%" VisibleIndex="2">                
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="DataRef" Width="10%" VisibleIndex="2">
                </dxwgv:GridViewDataTextColumn>
                
                
                <dxwgv:GridViewDataTextColumn FieldName="Message" Width="30%" VisibleIndex="2">                
                </dxwgv:GridViewDataTextColumn>
            </Columns>            
              
            </dxwgv:ASPxGridView>            
        </div>
          
    </div>
        <cc1:esDataSource ID="EsDSLoadLog" runat="server" OnesSelect="EsDSLoadLog_esSelect" LowLevelBind="true" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Exportando base, aguarde..." ClientInstanceName="LoadingPanel" Modal="true" />
        
    </form>
</body>
</html>
