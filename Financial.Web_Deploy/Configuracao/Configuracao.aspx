﻿<%@ page language="C#" autoeventwireup="true" inherits="Configuracao, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />
    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body>
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Configuração Geral" />
                            </div>
                            
                            <div id="mainContentSpace">
                                <br />
                                
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table border="0" align="center">
                                        <%--<table border="0" style="margin-left:15px">--%>
                                            <tr>
                                                <td valign="top">
                                                    <!-- Integrações -->
                                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" HeaderStyle-Font-Bold="true"
                                                        HeaderText="Integrações" Width="335px" HeaderStyle-Font-Size="11px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Bovespa:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropIntegracaoBolsa" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropIntegracaoBolsa" OnPreRender="dropIntegracaoBolsa_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="0" Text="Não Integra" />
                                                                                    <dxe:ListEditItem Value="1" Text="Sinacor" />
                                                                                    <dxe:ListEditItem Value="2" Text="Sinacor + BTC" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="BM&F(Fut/Opc):" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropIntegracaoBMF" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropIntegracaoBMF" OnPreRender="dropIntegracaoBMF_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="0" Text="Não Integra" />
                                                                                    <dxe:ListEditItem Value="1" Text="Sinacor" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label22" runat="server" CssClass="labelNormal" Text="Renda Fixa:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropIntegracaoRendaFixa" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropIntegracaoRendaFixa" OnPreRender="dropIntegracaoRendaFixa_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="0" Text="Não Integra" />
                                                                                    <dxe:ListEditItem Value="1" Text="Sinacor" />
                                                                                    <dxe:ListEditItem Value="2" Text="Virtual" />                                                                                    
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label16" runat="server" CssClass="labelNormal" Text="ContaCorrente:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropIntegracaoCC" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropIntegracaoCC" OnPreRender="dropIntegracaoCC_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="0" Text="Não Integra" />
                                                                                    <dxe:ListEditItem Value="1" Text="Sinacor" />
                                                                                    <dxe:ListEditItem Value="2" Text="Sinacor + IR DT" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="labelListaCodigos" runat="server" CssClass="labelNormal" Text="Códigos C/C:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textCodigosCC" runat="server" TextMode="MultiLine" Rows="5" CssClass="textNormal10"
                                                                                                OnPreRender="textCodigosCC_OnPrerender"/>                                                                            
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Schema Principal:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textSchemaSinacor" runat="server" TextMode="SingleLine" CssClass="textNormal10"
                                                                                                OnPreRender="textSchemaSinacor_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label34" runat="server" CssClass="labelNormal" Text="Schema BTC:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textSchemaBTC" runat="server" TextMode="SingleLine" CssClass="textNormal10"
                                                                                                OnPreRender="textSchemaBTC_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                     <tr>
                                                                        <td>
                                                                            <asp:Label ID="label38" runat="server" CssClass="labelNormal" Text="Usuário WS:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textUsuarioIntegracao" runat="server" TextMode="SingleLine" CssClass="textNormal10"
                                                                                                OnPreRender="textUsuarioIntegracao_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label39" runat="server" CssClass="labelNormal" Text="Senha WS:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textSenhaIntegracao" runat="server" TextMode="Password" CssClass="textNormal10"
                                                                                                OnPreRender="textSenhaIntegracao_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td></td><td></td>
                                                                    </tr>
                                                                    
                                                                    
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label42" runat="server" CssClass="labelNormal" Text="Nome Instituição:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textNomeInstituicao" runat="server"  CssClass="textNormal10"
                                                                                                OnPreRender="textNomeInstituicao_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label43" runat="server" CssClass="labelNormal" Text="Cód. Instituição:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textCodInstituicao" runat="server" CssClass="textNormal10"
                                                                                                OnPreRender="textCodInstituicao_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label44" runat="server" CssClass="labelNormal" Text="Responsável:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textResponsavelInstituicao" runat="server"  CssClass="textNormal10"
                                                                                                OnPreRender="textResponsavelInstituicao_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label45" runat="server" CssClass="labelNormal" Text="Telefone:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textTelefoneInstituicao" runat="server"  CssClass="textNormal10"
                                                                                                OnPreRender="textTelefoneInstituicao_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label46" runat="server" CssClass="labelNormal" Text="E-mail:"> </asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="textEmailInstituicao" runat="server"  CssClass="textNormal10"
                                                                                                OnPreRender="textEmailInstituicao_OnPrerender"/>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label47" runat="server" CssClass="labelNormal" Text="Galgo - PL / Cota:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropGalgoPlCota" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropGalgoPlCota" OnPreRender="dropGalgoPlCota_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="0" Text="Não Integra" />
                                                                                    <dxe:ListEditItem Value="1" Text="Integra - todas" />
                                                                                    <dxe:ListEditItem Value="2" Text="Integra – valida cotas repetidas" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                </table>                                                                
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                                <td width="20"></td>
                                                <td valign="top">
                                                    <!-- Bolsa -->
                                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" HeaderStyle-Font-Bold="true"
                                                        HeaderText="Parâmetros Bolsa" HeaderStyle-Font-Size="11px" Width="345px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <div style="height:8px"></div>
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">

                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label17" runat="server" CssClass="labelNormal" Text="Entrada Operações:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropEntradaOperacoes" runat="server" CssClass="dropDownListCurto_4"
                                                                                ClientInstanceName="dropEntradaOperacoes" OnPreRender="dropEntradaOperacoes_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Compras" />
                                                                                    <dxe:ListEditItem Value="2" Text="Vendas" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                                                                                        
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Tipo Visualização Bolsa:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropTipoVizualizacaoBolsa" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropTipoVizualizacaoBolsa" OnPreRender="dropTipoVizualizacaoBolsa_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Analitico" />
                                                                                    <dxe:ListEditItem Value="2" Text="Consolidado" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Prioridade Casamento Vencimento:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropBolsaPrioridadeCasamento" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropBolsaPrioridadeCasamento" OnPreRender="dropBolsaPrioridadeCasamento_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Primeiro Exercício" />
                                                                                    <dxe:ListEditItem Value="2" Text="Primeiro Termo" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="MTM Termo Vendido:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropBolsaMTMTermoVendido" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropBolsaMTMTermoVendido" OnPreRender="dropBolsaMTMTermoVendido_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Custódia BTC:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropCustodiaBTC" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropCustodiaBTC" OnPreRender="dropCustodiaBTC_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Não altera" />
                                                                                    <dxe:ListEditItem Value="2" Text="Altera sem IR" />
                                                                                    <dxe:ListEditItem Value="3" Text="Altera todos" />
                                                                                </Items>                
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label20" runat="server" CssClass="labelNormal" Text="Consolida Custo Médio:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropConsolidaCustoMedio" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropConsolidaCustoMedio" OnPreRender="dropConsolidaCustoMedio_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label33" runat="server" CssClass="labelNormal" Text="Emite Msg sem Cotação:" 
                                                                            tooltip="Checado emite mensagem para cotação inexistente ao processar um ativo de bolsa sem cotação" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkEmiteMsgSemCotacaoBolsa" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkEmiteMsgSemCotacaoBolsa" OnPreRender="chkEmiteMsgSemCotacaoBolsa_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label14" runat="server" CssClass="labelNormal" Text="Compensa IR Daytrade:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropBolsaCompensaIRDaytrade" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropBolsaCompensaIRDaytrade" OnPreRender="dropBolsaCompensaIRDaytrade_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label15" runat="server" CssClass="labelRequired" Text="Código Bovespa Default:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxSpinEdit ID="txtCodigoBovespa" runat="server" NumberType="Integer" ClientInstanceName="txtCodigoBovespa"
                                                                                MaxLength="6" OnPreRender="txtCodigoBovespa_OnPrerender" CssClass="textCurto"
                                                                                MinValue="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Renovação Automática de Termo:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropBolsaRenovacaoTermo" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropBolsaRenovacaoTermo" OnPreRender="dropBolsaRenovacaoTermo_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label23" runat="server" CssClass="labelNormal" Text="Aplica Isenção IR Ações:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropBolsaIsencaoIRAcoes" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropBolsaIsencaoIRAcoes" OnPreRender="dropBolsaIsencaoIRAcoes_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Tipo Visualização BMF:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropTipoVizualizacaoBMF" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropTipoVizualizacaoBMF" OnPreRender="dropTipoVizualizacaoBMF_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Analitico" />
                                                                                    <dxe:ListEditItem Value="2" Text="Consolidado" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Tipo Cálculo Taxas BMF:" /></td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropTipoCalculoTaxasBMF" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropTipoCalculoTaxasBMF" OnPreRender="dropTipoCalculoTaxasBMF_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Parâmetros" />
                                                                                    <dxe:ListEditItem Value="2" Text="Direto" />
                                                                                    <dxe:ListEditItem Value="3" Text="Não Calcula" />
                                                                                    <dxe:ListEditItem Value="4" Text="S/ Permanência" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>                                                    
                                                </td>
                                                <td width="20"></td>
                                                <td valign="top">                                                
                                                    <!-- Fundo -->
                                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel6" runat="server" HeaderStyle-Font-Bold="true"
                                                        HeaderText="Parâmetros Carteira - Gerais" HeaderStyle-Font-Size="11px" Width="400px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="labelDataBase" runat="server" CssClass="labelNormal" Text="Data Base:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="textDataBase" runat="server" ClientInstanceName="textDataBase" OnPreRender="textDataBase_OnPreRender"/>
                                                                        </td>
                                                                    </tr>
                                                                
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Tratamento para Cota Inexistente:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropCotaInexistente" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropCotaInexistente" OnPreRender="dropCotaInexistente_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Envia Exceção" />
                                                                                    <dxe:ListEditItem Value="2" Text="Busca Cota Superior" />
                                                                                    <dxe:ListEditItem Value="3" Text="Busca Cota Anterior" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label10" runat="server" CssClass="labelNormal" Text="Recalcula Cota Fechamento:" /></td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropRecalculaCotaFechamento" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropRecalculaCotaFechamento" OnPreRender="dropRecalculaCotaFechamento_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label19" runat="server" CssClass="labelNormal" Text="Retorno Benchmark:" /></td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropRetornoBenchmark" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropRetornoBenchmark" OnPreRender="dropRetornoBenchmark_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Deslocado" />
                                                                                    <dxe:ListEditItem Value="2" Text="Padrão" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label8" runat="server" CssClass="labelNormal" Text="Utiliza Cota Inicial Posição:" /></td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropUsaCotaInicialPosicaoIR" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropUsaCotaInicialPosicaoIR" OnPreRender="dropUsaCotaInicialPosicaoIR_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label18" runat="server" CssClass="labelNormal" Text="Retorno FDIC Ajustado:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropRetornoFDICAjustado" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropRetornoFDICAjustado" OnPreRender="dropRetornoFDICAjustado_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label21" runat="server" CssClass="labelNormal" Text="Distribuidor Padrão:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropAgenteDistribuidor" runat="server" ClientInstanceName="dropAgenteDistribuidor"
                                                                                                DataSourceID="EsDSAgenteMercadoDistribuidor" IncrementalFilteringMode="Contains"  
                                                                                                ShowShadow="false" DropDownStyle="DropDown"
                                                                                                CssClass="dropDownList" TextField="Nome" ValueField="IdAgente"
                                                                                                OnPreRender="dropAgenteDistribuidor_OnPreRender">                                                                              
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label24" runat="server" CssClass="labelNormal" Text="Custódia Renda Fixa:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropCustodiaRF" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropCustodiaRF" OnPreRender="dropCustodiaRF_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="1" Text="Não Controla" />
                                                                                    <dxe:ListEditItem Value="2" Text="Custodiante" />
                                                                                    <dxe:ListEditItem Value="3" Text="Clearing" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label32" runat="server" CssClass="labelNormal" Text="Alíquota Única Grossup:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropAliquotaUnicaGrossup" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropAliquotaUnicaGrossup" OnPreRender="dropAliquotaUnicaGrossup_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />                                                                                    
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label25" runat="server" CssClass="labelNormal" Text="Multi Conta:" 
                                                                                    tooltip="Checado mostra o combo de c/c na tela de liquidação" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkMultiConta" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkMultiConta" OnPreRender="chkMultiConta_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                                                                                                                                                                        
                                                                    </tr>
                                                                    
                                                                    <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label26" runat="server" CssClass="labelNormal" Text="Libera Data Lançamento Liquidação:" 
                                                                                tooltip="Checado Libera o campo de Data de Lançamento na tela de Liquidação" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkLiberaDtLancamento" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkLiberaDtLancamento" OnPreRender="chkLiberaDtLancamento_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>                                                                                                                                        

                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label27" runat="server" CssClass="labelNormal" Text="Agente Liquidação Exclusivo:" 
                                                                            tooltip="Checado mostra o combo de agente de liquidação na tela de Operação Bolsa/Ordem Bolsa e Ordem BMF" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkAgenteLiquidacao" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkAgenteLiquidacao" OnPreRender="chkAgenteLiquidacao_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label28" runat="server" CssClass="labelNormal" Text="Calcula Contábil:" 
                                                                                tooltip="Checado Mostra o Campo CalculaContabil no Cadastro de Cliente e os Campos IdEventoProvisao/IdEventoPagamento nos Cadastros de TabelaProvisão/TabelaTaxaAdministracao/TabelaTaxaPerformance" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkCalculaContabil" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkCalculaContabil" OnPreRender="chkCalculaContabil_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    
                                                                    <tr>                                                                                                                                           
                                                                        <td>
                                                                            <asp:Label ID="label29" runat="server" CssClass="labelNormal" Text="Permissão Interno Auto:" 
                                                                            tooltip="Checado dá permissão automática aos clientes e cotistas para todos os usuários que tenham TipoTrava = SemTrava" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkPermissaoAuto" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkPermissaoAuto" OnPreRender="chkPermissaoAuto_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label30" runat="server" CssClass="labelNormal" Text="Emite Msg sem Cotação:" 
                                                                            tooltip="Checado emite mensagem para cotação inexistente ao processar o MTM de títulos" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkEmiteMsgSemCotacao" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkEmiteMsgSemCotacao" OnPreRender="chkEmiteMsgSemCotacao_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                    <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label31" runat="server" CssClass="labelNormal" Text="Reseta data início em resgate total:" 
                                                                            tooltip="Checado reseta data de início da cota após resgates totais" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkResetDataInicio" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkResetDataInicio" OnPreRender="chkResetDataInicio_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                    <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label35" runat="server" CssClass="labelNormal" Text="Calcula Fatores Índices:" 
                                                                            tooltip="Calcula Fatores Índices" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkCalculaFatorIndice" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkCalculaFatorIndice" OnPreRender="chkCalculaFatorIndice_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                    <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label36" runat="server" CssClass="labelNormal" Text="Processa Indicador Carteira:" 
                                                                            tooltip="Processa Indicador Carteira" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkProcessaIndicadorcarteira" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkProcessaIndicadorcarteira" OnPreRender="chkProcessaIndicadorcarteira_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                    
                                                                    <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label37" runat="server" CssClass="labelNormal" Text="Suitability Renda Fixa:" 
                                                                            tooltip="Suitability Renda Fixa" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkSuitabilityRendaFixa" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkSuitabilityRendaFixa" OnPreRender="chkSuitabilityRendaFixa_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                    
                                                                    <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label40" runat="server" CssClass="labelNormal" Text="PFee Cota Fundos:" 
                                                                            tooltip="PFee Cotas Fundos" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropPfeeCotasFundos" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropPfeeCotasFundos" OnPreRender="dropPfeeCotasFundos_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                    
                                                                     <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="label41" runat="server" CssClass="labelNormal" Text="Descontar Rendimento Cupom:" 
                                                                            tooltip="Desconta Rendimento Cupom" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropDescontaRendimentoCupom" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropDescontaRendimentoCupom" OnPreRender="dropDescontaRendimentoCupom_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                   
                                                                    <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="labelRetroagirCarteirasDependentes" runat="server" CssClass="labelNormal" Text="Retroagir carteiras Dependentes:" 
                                                                            tooltip="Retroagir carteiras Dependentes" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkRetroagirCarteirasDependentes" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkRetroagirCarteirasDependentes" OnPreRender="chkRetroagirCarteirasDependentes_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                    
                                                                    <tr>                                                                        
                                                                        <td>
                                                                            <asp:Label ID="labelPermitirOperacoesRetroativas" runat="server" CssClass="labelNormal" Text="Permitir Operações Retroativas:" 
                                                                            tooltip="Permitir Operações Retroativas" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="chkPermitirOperacoesRetroativas" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="chkPermitirOperacoesRetroativas" OnPreRender="chkPermitirOperacoesRetroativas_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                                                                                                                                                                                                                               
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblTipoVisualizacaoResgCotista" runat="server" CssClass="labelNormal" Text="Tipo Visualização Resg.Cotista:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropTipoVisualizacaoResgCotista" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropTipoVisualizacaoResgCotista" OnPreRender="dropTipoVisualizacaoResgCotista_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="C" Text="Consolidado" />
                                                                                    <dxe:ListEditItem Value="A" Text="Analitico" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                        
                                                                    </tr>                                                                        
                                                                    
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel> 
                                                
                                            </td>
                                                <td width="20">
                                                </td>
                                                <td valign="top">
                                                    <dxrp:ASPxRoundPanel ID="roundPanelParametrosGerais" runat="server" HeaderStyle-Font-Bold="true"
                                                        HeaderText="Parâmetros Gerais" HeaderStyle-Font-Size="11px" Width="400px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblIdAutomaticoPessoa" runat="server" CssClass="labelNormal" Text="ID Automático no Cadastro de Pessoas:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropIdAutomaticoPessoa" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropIdAutomaticoPessoa" OnPreRender="dropIdAutomaticoPessoa_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblPermiteDuplDocumentoPessoa" runat="server" CssClass="labelNormal" Text="Permite Duplicidade de CPF/CNPJ no Cadastro de Pessoas:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropPermiteDuplDocumentoPessoa" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropPermiteDuplDocumentoPessoa" OnPreRender="dropPermiteDuplDocumentoPessoa_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblPropagaAlterNomeCliente" runat="server" CssClass="labelNormal" Text="Propaga Alteração de Nomes de Pessoas para Clientes:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropPropagaAlterNomeCliente" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropPropagaAlterNomeCliente" OnPreRender="dropPropagaAlterNomeCliente_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblEstendeConceitoPessoaAgente" runat="server" CssClass="labelNormal" Text="Estende o Conceito de Pessoa para Agentes de Mercado:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropEstendeConceitoPessoaAgente" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropEstendeConceitoPessoaAgente" OnPreRender="dropEstendeConceitoPessoaAgente_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblPropagaAlterNomeAgente" runat="server" CssClass="labelNormal" Text="Propaga Alteração de Nomes de Pessoas para Agentes de Mercado:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropPropagaAlterNomeAgente" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropPropagaAlterNomeAgente" OnPreRender="dropPropagaAlterNomeAgente_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblValidaQtdeResgateProcessamento" runat="server" CssClass="labelNormal" Text="Valida Qtde na data de Conversão (Processamento):" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropValidaQtdeResgateProcessamento" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropValidaQtdeResgateProcessamento" OnPreRender="dropValidaQtdeResgateProcessamento_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblAbortaProcRFPorErroCadastralTitulo" runat="server" CssClass="labelNormal" Text="Aborta processamento por erro cadastral no título de RF:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropAbortaProcRFPorErroCadastraTitulo" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropAbortaProcRFPorErroCadastralTitulo" OnPreRender="dropAbortaProcRFPorErroCadastralTitulo_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr> 
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblCalculaCofUsandoPLD0" runat="server" CssClass="labelNormal" Text="Calcula COF usando pl de D-0:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropCalculaCofUsandoPLD0" runat="server" CssClass="dropDownListCurto"
                                                                                ClientInstanceName="dropCalculaCofUsandoPLD0" OnPreRender="dropCalculaCofUsandoPLD0_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>                                                                                                                                          
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>                                                
                                            </tr>
                                            
                                            <tr/>
                                            <tr/>
                                            <tr/>
                                            <tr/>
                                        </table>
                                                                                                                                                                                                                   
                                        <div class="linkButton linkButtonTbar" style="padding-left:50pt" >
                                            <div class="linkButtonWrapper">
                                                    <asp:LinkButton ID="btnSalvar" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClick="btnSalvar_Click"><asp:Literal ID="Literal5" runat="server" Text="Salvar" /><div></div></asp:LinkButton>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        
        <cc1:esDataSource ID="EsDSAgenteMercadoDistribuidor" runat="server" OnesSelect="EsDSAgenteMercadoDistribuidor_esSelect" />
    </form>
</body>
</html>