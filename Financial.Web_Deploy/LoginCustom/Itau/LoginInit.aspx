﻿<%@ page language="C#" autoeventwireup="true" inherits="LoginInitItau, Financial.Web_Deploy" title="Login" meta:resourcekey="PageResource1" enableEventValidation="false" theme="DevEx" %>

<html>
<head id="Head1" runat="server">
    <link href="~/css/login.css?v2" rel="stylesheet" type="text/css" />
    <link href="~/cssCustom/login.css" rel="stylesheet" type="text/css" />
    <link href="~/css/keyboard.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />

    <script type="text/javascript" language="Javascript" src="../../js/ext-core.js"></script>

    <script type="text/javascript" language="Javascript" src="../../js/login/login.js"></script>

    <script type="text/javascript" language="Javascript" src="../../jsCustom/login/login.js"></script>

</head>
<body onresize="window.location.reload();">
    <form id="Form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />

        <script type="text/javascript" language="JavaScript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            function InitializeRequest(sender, args)  {
                $get('Login1').style.cursor = 'wait'; 

                // Get a reference to the element that raised the postback,
                //   and disables it.
                $get(args._postBackElement.id).disabled = true;
            }

            function EndRequest(sender, args) {
                $get('Login1').style.cursor = 'auto';

                // Get a reference to the element that raised the postback
                //   which is completing, and enable it.
                $get(sender._postBackSettings.sourceElement.id).disabled = false;
                Financial.Login.updateHTML();                       
            }
        </script>

        <asp:Login ID="Login1" runat="server" DisplayRememberMe="true" OnLoggedIn="Login1_LoggedIn">
            <LayoutTemplate>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="UserName" runat="server" CssClass="input_text input_text_username"></asp:TextBox>
                        <asp:TextBox ID="Password" runat="server" CssClass="input_text input_text_password keyboardInput"
                            TextMode="Password"></asp:TextBox>
                        <asp:CheckBox ID="RememberMe" runat="server" CssClass="remember_me" Text="Lembrar login" />
                        <asp:LinkButton ID="LoginButton" runat="server" Font-Underline="false" CommandName="Login"
                            CssClass="btn_login" ValidationGroup="Login1" Text="{text}" />
                        <a href="javascript:window.location='LembrarSenha.aspx' + window.location.search"
                            class="amnesia" title="Clique aqui para reaver sua senha" id="Login1_LembrarSenha">
                            {text}</a>
                        <div id="Login1_FailureText" class="failure-text">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </LayoutTemplate>
        </asp:Login>
        <div style="display: none">
            <asp:TextBox ID="AutoSubmit" runat="server"></asp:TextBox>
        </div>
    </form>

    <script type="text/javascript">
        if(Ext.fly('AutoSubmit').getValue() === '1'){
            document.body.style.display = 'none';
            __doPostBack('Login1$LoginButton','');
        }
    </script>

</body>
</html>
