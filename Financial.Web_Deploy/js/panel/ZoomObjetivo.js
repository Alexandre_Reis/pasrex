Ext.namespace('FDESK.panel.ZoomObjetivo');
FDESK.panel.ZoomObjetivo = function(config) {
	
	FDESK.panel.ZoomObjetivo.superclass.constructor.call(this, config);

};

Ext.extend(FDESK.panel.ZoomObjetivo, Ext.Panel, {
	cls : 'zoom-objetivo'
	
});
Ext.reg('zoomobjetivo', FDESK.panel.ZoomObjetivo);