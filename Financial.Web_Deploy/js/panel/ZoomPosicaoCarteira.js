Ext.namespace('FDESK.panel.ZoomPosicaoCarteira');
FDESK.panel.ZoomPosicaoCarteira = function(config) {
    
    var bodyWidth = Ext.getBody().getWidth();
    var singleColumnWidth = bodyWidth - 70;
    
    var panelCarteiraSintetica = {
		xtype : 'carteirasintetica',
		data : [],
		ref : 'tableCarteiraSintetica',
		collapsible : false,
		collapsed : false,
		height : 350,
		width: singleColumnWidth,
		style : 'margin: 30px 20px 30px 20px;',
		tools : [{
		        qtip : FDESK.AS.helpQtip,
		        id : 'help',
			    handler : function(){
			        FDESK.LE.openHelpWin({topic: 'module_2_1'});
			    }
		    }]
	};

    var panelRetornoAtivos = {
		xtype : 'retornoativos',
		data : [],
		ref : 'tableRetornoAtivos',
		collapsible : false,
		height : 350,
		width: singleColumnWidth,
		style : 'margin: 30px 20px 30px 20px;'
	};
	

    var toolsGrafico = [{
		        qtip : FDESK.AS.helpQtip,
		        id : 'help',
			    handler : function(){
			        FDESK.LE.openHelpWin({topic: 'module_2_2'});
			    }
		    }];
		    
	var graficoCarteira1 = {
		xtype : 'panelgrafico',
		ref : '../graficoCarteira1',
		showMenuChartTypes: true,
		columnWidth : .5,
		style : 'margin-right: 40px; margin-bottom: 30px;',
		title : '&nbsp',
		chartGroup : 'SingleSeries',
		tools : toolsGrafico
	};

	var graficoCarteira2 = {
		xtype : 'panelgrafico',
		ref : '../graficoCarteira2',
		showMenuChartTypes: true,
		columnWidth : .5,
		title : '&nbsp',
		chartGroup : 'SingleSeries',
		tools : toolsGrafico
	};

	var columns = {
		layout : 'column',
		xtype : 'panel',
		border : false,
	    width: singleColumnWidth,
		style : 'margin: 20px 20px 0 20px;',
		bodyStyle : 'background-color: transparent',
		defaults : {
			border : false,
			frame : true
		},
		items : [graficoCarteira1, graficoCarteira2]
	};

    if(FDESK.AS.user.isCarteiraSimulada){
	    this.items = [panelRetornoAtivos, columns];
	}else{
	    this.items = [panelCarteiraSintetica, panelRetornoAtivos, columns];
	}

	FDESK.panel.ZoomPosicaoCarteira.superclass.constructor.call(this, config);
	this.subscribeEvent('tabposicaocarteiratablesloaded', this.loadTables, this);
    

	this.subscribeEvent('zoomcardsresize', this.adjustHeight, this);
	this.subscribeEvent('ativocarteirasimuladaupdated', this.loadGraficos, this);
	this.subscribeEvent('dateperiodochanged', this.loadGraficos, this);
	this.subscribeEvent('environmentupdated', this.loadGraficos, this);
	this.subscribeEvent('explodefundoschanged', this.loadGraficos, this);

	this.graficoCarteira1.selectChart(0);
	this.graficoCarteira2.selectChart(1);
	
};

Ext.extend(FDESK.panel.ZoomPosicaoCarteira, Ext.Panel, {
			border : false,
			loadGraficos : function(params){
				this.graficoCarteira1.updateChart(null, {});
				this.graficoCarteira2.updateChart(null, {});
			},
			loadTables : function(data) {
			    if(data.carteiraSintetica && this.tableCarteiraSintetica){
				    this.tableCarteiraSintetica.loadData(data.carteiraSintetica);
				}
				
				if(data.retornoAtivos){
				    this.tableRetornoAtivos.loadData(data.retornoAtivos);
				}
			},
			adjustHeight : function() {
				var h = Math.round(((this.ownerCt.getWidth() - 90) / 2) / 1.25);
				this.graficoCarteira1.setHeight(h);
				this.graficoCarteira2.setHeight(h);
				
				var bodyWidth = Ext.getBody().getWidth();
                this.setWidth(bodyWidth);
			}
		});

Ext.reg('zoomposicaocarteira', FDESK.panel.ZoomPosicaoCarteira);