﻿// this javascript function gets the client resolution and compares it with server 
// side resolution setting and postback to synchronize the 2 resolutions. 
// this function is called through 'onload' property of 'body' tag 
function detectClientResolutionAndChange() {				
	//this variable will hold the current resolution of the client's desktop
	var clientRes="";
	//compare and save client's desktop resolution so it can be sent 
	//to server through post for changing page according to that resolution
	if ((screen.width == 1280) && (screen.height == 1024)) {
		document.getElementById("ClientResolution").setAttribute("value", "_1280x1024");
		clientRes="_1280x1024";		  
	}
	//compare and save client's desktop resolution so it can be sent 
	//to server through post for changing page according to that resolution
	if ((screen.width == 1024) && (screen.height == 768)){
	 	clientRes="_1024x768";
		document.getElementById("ClientResolution").setAttribute("value", "_1024x768");
	}
	//compare and save client's desktop resolution so it can be sent 
	//to server through post for changing page according to that resolution
	if ((screen.width == 800) && (screen.height == 600)) {
		clientRes="_800x600";
		document.getElementById("ClientResolution").setAttribute("value", "_800x600");
	}

	//Now that client resolution is captured and stored in clientRes variable 
	//Lets compare it with resolution setting page sent by server. If both are not same
	//post on server for changing according to the client's res. 
	//On initial page request, 1024*768 resoltuion setting page is sent, to client
	// Como o controle é usado na master Page ele é referenciado através de clt00$_[controle]	
	if (document.getElementById("ctl00$ServerResolution").getAttribute("value") != clientRes) {		
		//the html hidden input 'resolution' which i declared below, will tell server side code 
		//about the client's resolution			
		if (clientRes != "") {			
			//document.getElementById("aspnetForm").submit(); // nome do form gerado pelo visual studio
		}
	}
}