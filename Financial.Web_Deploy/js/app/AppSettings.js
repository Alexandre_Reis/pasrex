Ext.ns('FDESK.AppSettings');
FDESK.AppSettings = function() {

	var AppSettings = function() {

		this.tipoOperacao = {
			V : 'Venda',
			C : 'Compra',
			CD : 'Compra DT',
			VD : 'Venda DT',
			DE : 'Dep�sito',
			RE : 'Retirada'
		};

		this.tipoOperacaoRendaFixa = {
			'1' : 'Compra Final',
			'2' : 'Venda Final',
			'3' : 'Compra Revenda',
			'4' : 'Venda Recompra',
			'5' : 'Compra Casada',
			'6' : 'Venda Casada'
		};

		// Definicao das janelas
		/*
		 * Refresh de datas refresh todas tabelas da janela (acao)
		 * 
		 * Opcao = Acao
		 * 
		 * Renda Fixa basear em fundo com as infos: tabela1 : Qtde,
		 * PrecoUnitario , SaldoBruto, IR, IOF , SaldoLiquido (VloarCota virou
		 * PrecoUnitario) tabela2: Data, Tipo, PUOperacao, Qtde, Valor,
		 * Tributos, Valor Liquido
		 * 
		 * Aba saldos aplicacoes -> Movimentacoes
		 * 
		 * Fim renda fixa
		 * 
		 * Na carteira sintetica nao deixar expandir os tipos LiquidProjecao e
		 * SaldoCC
		 * 
		 * BMF = Acao
		 */

		Ext.apply(this, {
		    g_HideRelatorio : false,
			urlLogoff : "login/logoff.aspx",
			g_UseRemote : false,
			g_PathExt : 'js/ext',
			g_AppTitle : "FinancialDesk",
			g_AppId : "FDESK",
			defErrMsg : 'Ocorreu um erro durante esta opera��o. Por favor tente novamente.'
					+ '\n\nCaso o erro persista, entre em contato com a �rea de suporte.',

			offValue : '0',
			onValue : '1',
			helpQtip : 'Clique aqui para abrir a ajuda',
			tableOperacaoEmptyText : '<div class="table-empty-text">N�o existem opera��es cadastradas para o per�odo selecionado</div>'
		});

		this.addEvents({
					"environmentupdated" : true
				});

	};

	Ext.extend(AppSettings, Ext.util.Observable, {
		logoffText : 'Sair',

		init : function() {

		},

		loadEnvironment : function(cb, scope, params) {
			this.firstArg = cb;

            Ext.Ajax.timeout = 120000; //2 minutes

            var queryString = Ext.urlDecode(location.search.substring(1));
            var idCliente = queryString.idCliente;

			Ext.Ajax.request({
						scope : this,
						url : 'ashx/AppSettings.ashx',
						params : {idCliente: idCliente},
						failure : function(response) {
							Ext.trace(response);
							var obj = Ext.decode(response.responseText);
							this.errHandler(obj.erros);
							return false;
						},

						success : function(response) {
							var obj = Ext.decode(response.responseText);
							if (!obj.success) {
								if (obj.erros && obj.erros.length) {
									alert(obj.erros[0]);
									window.location = this.urlLogoff;
								}
								return false;
							}

							obj.attributes = {};
							
							this.user = {
							        idCliente : obj.idCliente,
									nomeCliente : obj.nomeCliente,
									dataCliente : Date.parseDate(obj.dataCliente, "Y-n-j"),
									travaCliente : obj.travaCliente,
									isCarteiraSimulada: obj.isCarteiraSimulada,
									acessaCarteiraSimulada: obj.acessaCarteiraSimulada
								};

							this.setDataReferencia(this.user.dataCliente, true);
							
							var elUser = Ext.get('x-nome-user');
							elUser.dom.innerHTML = this.user.nomeCliente;
							
							
							if (!this.loaded) {
							    this.paths = {};
							    
							    this.paths.base = this.g_WebDbName = (obj.applicationPath == '/' ? '' : obj.applicationPath);
							    this.paths.helpBase = obj.helpPath;
							    this.paths.crud = this.paths.base + '/ashx/crud.ashx';
							    
					    		Ext.apply(this.paths, {
						            getReport : this.paths.base
								        + '/ashx/GetReport.ashx'
					            });
						    
								
                                var divInfoUser = Ext.get('x-info-user');
                                divInfoUser.createChild({tag:'span', cls:'gbma'});
                                
							    elUser.on('click', function(){
							        FDESK.LE.showUserActions();
							    }, this);

								var elLogoff = Ext.get('x-logoff');
								elLogoff.on('click', function() {
											window.location = this.urlLogoff;
										}, this);

							    this.stores = {};
							    this.stores.indices = new Ext.data.JsonStore({
										    storeId : 'indices',
										    
										    fields : [{
													    name : 'IdIndice',
													    type : 'int'
												    }, 'Descricao'],
										    data : obj.indices || []
									    });
									    
                                this.stores.indices.sort('Descricao');

							    this.stores.tipoAtivo = new Ext.data.JsonStore({
										    storeId : 'tipoAtivo',
										    fields : [{
													    name : 'IdTipo',
													    type : 'int'
												    }, 'Tipo', 'Singular', 'Plural'],
										    data : obj.tipoAtivo
									    });
    							
    							this.tipoAtivoEnum = {};
						        for(var i=0; i< obj.tipoAtivo.length; i++){
						            var tipoAtivo = obj.tipoAtivo[i];
						            this.tipoAtivoEnum[tipoAtivo.Tipo] = tipoAtivo.IdTipo;
						        }
						        
							    this.stores.relatorioInfo = new Ext.data.JsonStore(
									    {
										    storeId : 'relatorioInfo',
										    fields : ['Url', 'Singular', {
													    name : 'Level',
													    type : 'int'
												    }, {
													    name : 'IsLeaf',
													    type : 'boolean'
												    }],
										    data : obj.relatorioInfo
									    });
    							

								this.loaded = true;
							};
							
							this.idIndiceDefault = obj.idIndiceDefault;
							this.setIdIndice(this.idIndiceDefault, true);
							this.observacaoExtrato = obj.observacaoExtrato;
							this.explodeFundos = false;

							var relayEventCfg = this.firstArg
									&& this.firstArg.action
									? this.firstArg
									: undefined;
							this.publishEvent('environmentupdated', relayEventCfg);

							if (cb && typeof cb == 'function') {
								cb.call(scope);
							};

						}
					});
		},

        setIdIndice : function(idIndice, supressEvent, updateWidget){
            this.idIndice = idIndice;
            
            if(updateWidget !== false){
                var widget = Ext.getCmp('combo-indice');
                if(widget){
                    widget.setValue(this.idIndice);
                }
            }

            if(supressEvent !== true){
                this.publishEvent('indicechanged', this.idIndice);
            }

        },
        
        setExplodeFundos : function(explodeFundos, supressEvent, updateWidget){
            this.explodeFundos = explodeFundos;
                        
            if(supressEvent !== true){
                this.publishEvent('explodefundoschanged', this.explodeFundos);
            }

        },

        setDataReferencia : function(dataReferencia, supressEvent, updateWidget){
            this.dataReferencia = dataReferencia;
            if(updateWidget !== false){
                var widget = Ext.getCmp('picker-data-referencia');
                if(widget){
                    widget.setMaxValue(this.dataReferencia);
                    widget.setValue(this.dataReferencia);
                }
            }

            if(supressEvent !== true){
                this.publishEvent('dateperiodochanged', this.dataReferencia);
            }

        },

		objsFromDomains : function(domains) {
			for (domain in domains) {
				if (domains[domain].length === undefined) {
					this[domain] = domains[domain];
				} else if (domains[domain].length) {
					this.objsFromDomain(domain, domains[domain]);
				}
			};
		},

		objsFromDomain : function(domain, els) {
			var arrName = domain + 'A';
			this[arrName] = els || [];

			var objName = domain;
			this[objName] = Solve.Util.Array.matrixToObj(this[arrName]);

			var storeName = domain + 'Store';
			if (this[storeName]) {
				Solve.Util.Store.rebuildStore(this[storeName], this[arrName]);
			} else {
				this[storeName] = Solve.Util.Store.makeStore(this[arrName],
						domain.toLowerCase() + 'store');
			}
		},

		loadStore : function(tipoReg, ashx, cb, scope, params) {
			Ext.Ajax.request({
						scope : this,
						url : this.g_WebDbName + '/ashx/' + ashx,
						failure : function() {
							this.errHandler();
							return false;
						},

						success : function(response) {
							if (!response.responseText) {
								alert('Nenhum registro encontrado');
								return;
							}
							var vals = Ext.decode(response.responseText);

							this.objsFromDomain(tipoReg, vals);

							if (cb && typeof cb == 'function') {
								cb.call(scope, params);
							};
						}
					});
		},

		errHandler : function(errorMessage) {
			if (errorMessage) {
				Ext.Msg.alert('Erro', errorMessage);
			} else {
				Ext.Msg.alert('Erro', this.defErrMsg);
			}
		}
	});

	return new AppSettings();
}();

FDESK.AS = FDESK.AppSettings;