Ext.namespace('FDESK.win.AtivoCarteiraSimulada');
FDESK.win.AtivoCarteiraSimulada = function(config) {
	Ext.apply(this, config);

    var tipoAtivoOptions = [];
    tipoAtivoOptions.push([FDESK.AS.tipoAtivoEnum.Acao, 'A��o']);
    //tipoAtivoOptions.push([FDESK.AS.tipoAtivoEnum.BMF, 'BMF']);
    tipoAtivoOptions.push([FDESK.AS.tipoAtivoEnum.CotaInvestimento, 'Fundo Investimento']);
    //tipoAtivoOptions.push([FDESK.AS.tipoAtivoEnum.RendaFixa, 'T�tulo Renda Fixa']);
    
    this.fTipoAtivo = new Ext.form.ComboBox({
				fieldLabel : 'TipoAtivo',
				hiddenName : 'TipoAtivo',
				store : tipoAtivoOptions,
				width : 200,
				mode : 'local',
				triggerAction : 'all',
				editable : false,
				allowBlank : false,
				listeners : {
					'select' : this.onTipoAtivoSelected,
					scope : this
				}
			});

    this.fCodigoAtivo = new Ext.form.TriggerField({
        fieldLabel: 'Ativo',
        editable: false,
        width : 200,
        allowBlank: false,
        id: 'CodigoAtivo'
    });
    
    this.fCodigoAtivo.onTriggerClick = this.onCodigoAtivoTriggerClick.createDelegate(this);
    
    this.subscribeEvent('ativoselected', function(
									idAtivo) {
								this.fCodigoAtivo.setValue(idAtivo);
							}, this);

    this.fValor = new Ext.form.NumberField({
        fieldLabel: 'Valor',
        id: 'Valor',
        decimalSeparator: ','
    });

    this.form = new Ext.form.FormPanel({
		id : 'form-ativo-carteira-simulada',
		autoHeight : true,
		params : {},
		labelWidth: 60,
		defaults : {
		    validationEvent: false,
		    validateOnBlur: false
		},
		//style : 'margin: 2px auto 0 auto;',
		bodyStyle : 'padding:25px 15px 15px 15px; background-color: #DFE8F6;',
		frame : false,
		items : [this.fTipoAtivo, this.fCodigoAtivo, this.fValor],
		border : false,
		bodyBorder: false,
		autoScroll: true,
		tipoRegistro : this.tipoRegistro,
		url : FDESK.AppSettings.g_WebDbName + '/ashx/controllers/ativocarteirasimulada.ashx'
	});
	
	FDESK.win.AtivoCarteiraSimulada.superclass.constructor.call(this, {
				width : 320,
				height : 226,
				autoScroll : true,
				resizable : false,
				modal : true,
				border : false,
        		bodyBorder: false,
				closeAction : 'hide',
				buttonAlign: 'left',
				buttons : [
				        {
							text : this.butDeleteText,
							cls : 'x-btn-plain',
							scope: this,
							handler : this.deleteForm
						}, '->', {
							text : this.butOkText,
							id : 'btn-ok-' + this.tipoRegistro.toLowerCase(),
							//iconCls : 'icon-disk',
							scope : this,
							handler: this.submitForm
						}, {
							text : this.butCancelText,
							cls : 'x-btn-plain',
							//iconCls : 'icon-minus-circle',

							handler : this.hide.createDelegate(this, [])
						}],

				items : this.form

			});

};

Ext.extend(FDESK.win.AtivoCarteiraSimulada, Ext.Window, {
			title : 'Ativo Carteira Simulada',
			tipoRegistro : 'CarteiraSimulada',
			butOkText : 'OK',
			butDeleteText : 'Apagar',
			butCancelText : 'Cancelar',
			
			onHide: function(){
			    popupCarteira.HideWindow();
			    popupAtivoBolsa.HideWindow();
			    popupAtivoBMF.HideWindow();
			    popupTituloRF.HideWindow();
			},

            onTipoAtivoSelected : function(combo, record, index) {
                this.fCodigoAtivo.setRawValue('');
			},

            onCodigoAtivoTriggerClick : function(){
                var tipoAtivo = this.fTipoAtivo.getValue();
                var fCotigoAtivoId = this.fCodigoAtivo.getEl().dom.id;
				if(tipoAtivo == FDESK.AS.tipoAtivoEnum.Acao){
                    popupAtivoBolsa.ShowAtElementByID(fCotigoAtivoId);
			    }else if(tipoAtivo == FDESK.AS.tipoAtivoEnum.BMF){
				    popupAtivoBMF.ShowAtElementByID(fCotigoAtivoId);
                }else if(tipoAtivo == FDESK.AS.tipoAtivoEnum.CotaInvestimento){
				    popupCarteira.ShowAtElementByID(fCotigoAtivoId);
                }else if(tipoAtivo == FDESK.AS.tipoAtivoEnum.RendaFixa){
				    popupTituloRF.ShowAtElementByID(fCotigoAtivoId);
                }
            },
            deleteForm: function(){
              this.form.params.action = 'delete';
                var bForm = this.form.getForm();
                bForm.submit({
							waitMsg : 'Apagando',
							scope : this,
							params : this.form.params,
							success : function(form, action) {
								var eventParams = {};

    							eventParams.action = this.form.params.action;

								this.publishEvent(this.tipoRegistro
												.toLowerCase()
												+ 'updated', eventParams);
								this.hide();
							},
							failure : function(form, action) {
							    if(action && action.result
										&& action.result.erros){
										    alert(action.result.erros);
										}
							}
						});  
            },
            
			updateForm : function(config, show) {
			    var basicForm = this.form.getForm();
			    basicForm.reset();
			    this.action = config.action;
    			if(!this.rendered){
    			    this.show();
    			}
                                
				//this.form.params.action = config.action;
				this.form.params.action = 'read';
				this.form.params.idCarteira = FDESK.AS.user.idCliente;
				this.form.params.pk = config.pkRegistro;
		
		        
		
				if(this.form.params.pk){
				    this.buttons[0].show();
		            this.fTipoAtivo.setDisabled(true);
		            this.fCodigoAtivo.setDisabled(true);
				    				
				    this.form.load({
							scope : this,
							method : 'GET',
							params : this.form.params,
							//url : FDESK.AppSettings.g_WebDbName + '/ashx/controllers/ativocarteirasimulada.ashx',
							failure : function(response) {
								var obj = Ext.decode(response.responseText);
								FDESK.AS.errHandler(obj && obj.erros);
								
								this.hide();
								this.fireEvent('formready');
							},
							success : function(response) {
								this.afterUpdateForm();
							}
						});
		        }else{
		            this.show();
		            this.fTipoAtivo.setDisabled(false);
		            this.fCodigoAtivo.setDisabled(false);
		            this.buttons[0].hide();
		        }
			},

			afterUpdateForm : function() {
				this.show();
				this.fireEvent('formready');
			},
				
			submitForm : function(params) {

				var bForm = this.form.getForm();
               
				var msgErro;
				if (!bForm.isValid()) {
					alert('Todos os campos possuem preenchimento obrigat�rio');
					return;
				}

                this.form.params.action = 'save';

				bForm.submit({
							waitMsg : 'Gravando',
							scope : this,
							params : this.form.params,
							success : function(form, action) {
								var eventParams = {};

    							eventParams.action = this.form.params.action;

								this.publishEvent(this.tipoRegistro
												.toLowerCase()
												+ 'updated', eventParams);
								this.hide();
							},
							failure : function(form, action) {
							    if(action && action.result
										&& action.result.erros){
										    alert(action.result.erros);
										}
							}
						});
			}
		});

