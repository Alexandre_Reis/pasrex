Ext.namespace('FDESK.win.RendaFixa');

FDESK.win.RendaFixa = function(config) {
	config = config || {};
	config.tableDetalheAtivoConfig = {
		xtype : 'basetable',
		tableCls : 'x-table-detalhe-ativo',
		ref : 'tableDetalheAtivo',
		width : '100%',
		readerConfig : {
			fields : [
			
			
			
			{
						name : 'DataOperacao',
						type : 'string',
						header : {
//							width : '20%',
							cls : 'centered',
							title : 'Data Opera��o'
						},
						cell : {
							cls : 'centered data'
						}
					}, {
						name : 'DataVencimento',
						type : 'string',
						header : {
	//						width : '20%',
							cls : 'centered',
							title : 'Data Vencimento'
						},
						cell : {
							cls : 'centered data'
						}
					}, {
						name : 'TaxaOperacao',
						type : 'float',
						header : {
//							width : '20%',
							cls : 'centered',
							title : 'Taxa Oper.'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					},{
						name : 'Quantidade',
						type : 'float',
						header : {
		//					width : '20%',
							cls : 'centered',
							title : 'Qtde.'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000/i")}'
						}
					},
					
					{
						name : 'PuOperacao',
						type : 'float',
						header : {
//							width : '20%',
							cls : 'centered',
							title : 'PU Opera��o'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					},
					
					{
						name : 'VlAplicAtualizado',
						type : 'float',
						header : {
//							width : '20%',
							cls : 'centered',
							title : 'Vl. Aplic. Atualizado'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}
					
					
					, {
						name : 'PuMercado',
						type : 'float',
						header : {
		//					width : '20%',
							cls : 'centered',
							title : 'PU Mercado'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					},  {
						name : 'SaldoBruto',
						type : 'float',
						header : {
	//						width : '20%',
							cls : 'centered',
							title : 'Saldo Bruto'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ValorIR',
						type : 'float',
						header : {
		//					width : '20%',
							cls : 'centered',
							title : 'IR'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'ValorIOF',
						type : 'float',
						header : {
		//					width : '20%',
							cls : 'centered',
							title : 'IOF'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'SaldoLiquido',
						type : 'float',
						header : {
			//				width : '20%',
							cls : 'centered',
							title : 'Saldo L�quido'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}]
		}
	};

	config.hideSeletor = true;

	FDESK.win.RendaFixa.superclass.constructor.call(this, config);

};

Ext.extend(FDESK.win.RendaFixa, FDESK.win.SeletorPeriodo, {
			title : 'Renda Fixa',
			width: '900',
			// tables : 'RendaFixa',
			fieldDescricao : 'DescricaoTitulo',
			posicaoSinteticaMethod : 'posicaoRendaFixaSintetica',
			operacaoAnaliticaMethod : 'operacaoRendaFixaAnalitica'
		});
