Ext.namespace('FDESK.win.TermoBolsa');

FDESK.win.TermoBolsa = function(config) {
	var tableDetalheAtivo = {
		xtype : 'basetable',
		tableCls : 'x-table-detalhe-ativo',
		ref : 'tableDetalheAtivo',
		width : '100%',
		readerConfig : {
			fields : [{
						name : 'DataOperacao',
						type : 'string',
						header : {
							width : '14%',
							cls : 'centered',
							title : 'Data Operacao'
						},
						cell : {
							cls : 'centered data'
						}
					},{
						name : 'DataVencimento',
						type : 'string',
						header : {
							width : '14%',
							cls : 'centered',
							title : 'Data Vencimento'
						},
						cell : {
							cls : 'centered data'
						}
					},{
						name : 'PuMercado',
						type : 'float',
						header : {
							width : '14%',
							cls : 'centered',
							title : 'PU Mercado'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,0000000/i")}'
						}
					}, {
						name : 'PuTermo',
						type : 'float',
						header : {
							width : '14%',
							cls : 'centered',
							title : 'PU Termo'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}, {
						name : 'Quantidade',
						type : 'float',
						header : {
							width : '14%',
							cls : 'centered',
							title : 'Quantidade'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,0000000/i")}'
						}
					}, {
						name : 'ValorMercado',
						type : 'float',
						header : {
							width : '14%',
							cls : 'centered',
							title : 'Valor Mercado'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,0000000/i")}'
						}
					}, {
						name : 'ValorTermo',
						type : 'float',
						header : {
							width : '16%',
							cls : 'centered',
							title : 'Valor Termo'
						},
						cell : {
							cls : 'centered',
							format : '{$n$:number("0.000,00/i")}'
						}
					}]
		}
	};

	config.items = [tableDetalheAtivo];

	FDESK.win.TermoBolsa.superclass.constructor.call(this, config);
};

Ext.extend(FDESK.win.TermoBolsa, FDESK.win.Ativo, {
			title : 'Termo Bolsa',

			updateForm : function(config, show) {
				this.params = {
					dateBegin : FDESK.AS.dataReferencia.format('Y-m-d'),
					dateEnd : FDESK.AS.dataReferencia.format('Y-m-d'),
					tables : this.tipoRegistro,
					date : FDESK.AS.dataReferencia.format('Y-m-d')
				};
				FDESK.win.TermoBolsa.superclass.updateForm.apply(this, [config,
								show, this.params]);
			},

			afterUpdateForm : function(obj) {
				var data = obj.posicaoTermoBolsaSintetica;
				if (data) {
					this.headerTitle.update({
								NomeAtivo : data.CdAtivoBolsa
							});

					this.tableDetalheAtivo.store
							.loadData([data]);
				}

				FDESK.win.TermoBolsa.superclass.afterUpdateForm.call(this);
			}
		});