Ext.namespace('FDESK', 'FDESK.LayoutEngine');

FDESK.LayoutEngine = function() {
	var LayoutEngine = function() {
	};

	Ext.extend(LayoutEngine, Ext.util.Observable, {
				windows : [],
				showUserActions : function(){
				    if(!this.menuUserActions)
				    {
				        var actionTrocaCarteira = new Ext.Action({
                            text: 'Visualizar outra carteira',
                            handler: function(){
                                popupCliente.ShowAtElementByID(this.id);
                            }
                        });

                        var actionCriarCarteiraSimulada = new Ext.Action({
                            text: 'Criar carteira simulada',
                            handler: function(){
                                FDESK.LE.openDocInWin({tipoRegistro: 'CarteiraSimulada', action: 'save'}, false);
                            }
                        });

				        var actionAlterarSenha = new Ext.Action({
                            text: 'Alterar informa��es do usu�rio',
                            handler: function(){
                                FDESK.LE.openDocInWin({tipoRegistro: 'Usuario'}, false);
                            }
                        });

                        var menuUserActionsItems = [];
                        menuUserActionsItems.push(actionTrocaCarteira);
                        if(FDESK.AS.user.acessaCarteiraSimulada){
                            menuUserActionsItems.push(actionCriarCarteiraSimulada);
                        }
                        menuUserActionsItems.push(actionAlterarSenha);

				        this.menuUserActions = new Ext.menu.Menu({
				            items: menuUserActionsItems
				        });
				        
				        this.menuUserActions.elAlignTo = Ext.get('x-info-user');
				    }
				    this.menuUserActions.show(this.menuUserActions.elAlignTo);
				},
				openHelpWin : function(config){
				    FDESK.LE.openDocInWin({tipoRegistro: 'Help', topic:config.topic}, false);
				},
				openDocInWin : function(formParams, maskBody) {
					if (maskBody) {
						Ext.getBody().mask('Carregando', 'x-mask-loading');
					}

					var tr = formParams.tipoRegistro;

					if (!this.windows['win' + tr]) {
						this.windows['win' + tr] = new FDESK.win[tr]({
									tipoRegistro : tr
								});
						if (maskBody) {
							this.windows['win' + tr].on('formready',
									function() {
										Ext.getBody().unmask();
									});
						}
					}

					this.windows['win' + tr].updateForm(formParams, true);
					return this['win' + tr];
				},

				loadTabTables : function(tableTabNames, params) {
			
					params = params || {};
					params.date = FDESK.AS.dataReferencia.format('Y-m-d');
					params.idIndice = FDESK.AS.idIndice;
                    params.explodeFundos = FDESK.AS.explodeFundos;

					// Se tableTabNames nao especificado, carrega todas as
					// tables dos tabs
					var tables = {};
					
					if(FDESK.AppSettings.g_ShowCarteiraOnline === true){
                        tables = {
						    carteiraOnline : ['CarteiraOnline']
					    };
					}else{
					    if(FDESK.AS.user.isCarteiraSimulada){
					        tables.indicadores = ['QuadroRetorno', 'Indicadores', 'RetornoPeriodo'];
					        tables.posicaoCarteira = ['RetornoAtivos'];
					    }else{
					        tables.indicadores = ['QuadroRetorno', 'Estatistica1', 'Estatistica2', 'Indicadores', 'RetornoPeriodo'];
					        tables.posicaoCarteira = ['CarteiraSintetica','RetornoAtivos'];
					    }
					    
					}

					var tablesToLoad = [], tabsToLoad = [];
					for (var tableTabName in tables) {
						if (!tableTabNames
								|| tableTabNames.indexOf(tableTabName) !== -1) {
							tablesToLoad = tablesToLoad
									.concat(tables[tableTabName]);
							tabsToLoad.push(tableTabName);
						}
					}


                    
					params.tables = tablesToLoad.join('|');
					params.idCliente = FDESK.AS.user.idCliente;

					Ext.getBody().mask('Carregando...',
							'ext-el-mask-msg x-mask-loading');
							
					Ext.Ajax.request({
								scope : this,
								method : 'GET',
								params : params,
								url : FDESK.AS.g_WebDbName + '/ashx/Table.ashx',
								failure : function(response) {
									var obj = Ext.decode(response.responseText);
									FDESK.AS.errHandler(obj && obj.erros);
								},
								success : function(response) {
									var data = Ext
											.decode(response.responseText);
									for (var i = 0; i < tabsToLoad.length; i++) {
										var eventName = String.format(
												'tab{0}tablesloaded',
												tabsToLoad[i].toLowerCase());
										this.publishEvent(eventName, data);
									}
									Ext.getBody().unmask();
								}
							});
				},

				init : function() {

					Ext.BLANK_IMAGE_URL = FDESK.AS.g_PathExt
							+ "/resources/images/default/s.gif";
					Ext.SSL_SECURE_URL = FDESK.AS.g_PathExt
							+ "/resources/images/default/s.gif";

					Ext.QuickTips.init();

					var headerPanelHeight = 65;

					var panelZoomCards = {
						xtype : 'zoomcards'
					};

					var centerPanel = {
						region : 'center',
						items : [panelZoomCards]
					};

					var headerPanel = {
						region : "north",
						ref : 'headerPanel',
						height : headerPanelHeight,
						header : false,
						bodyBorder : false,
						border : false,
						cls : 'header-panel',
						contentEl : 'x-taskbar',
						collapseMode : 'mini',
						collapsible : true,
						listeners : {
							collapse : function() {
								this.publishEvent('viewporttogglecollapse');
							},
							expand : function() {
								this.publishEvent('viewporttogglecollapse');
							},
							afterrender : function() {
								this.subscribeEvent(
										'collapsenorthpanelpressed',
										this.toggleCollapse, this);
							}
						}
					};

                    var southPanel;
                    if(FDESK.AS.observacaoExtrato && FDESK.AS.observacaoExtrato.length){
                        southPanel = {
                            region : "south",
                            collapseMode : 'mini',
						    collapsible : true,
						    ref : 'southPanel',
						    title: 'Disclaimer',
						    height: 100,
						    autoScroll: true,
						    //html: '<p style="margin: 5px 10px; font-family: Verdana; font-size: 10px; text-align: justify; color: #999;"><div><span style="font-family: Calibri; font-size: 8pt; ">&nbsp;</span></div><div><span style="font-family: Calibri; font-size: 8pt; "></span><p style="TEXT-ALIGN: justify; MARGIN: 0cm 0cm 10pt" class="MsoNormal"><span style="line-height: 115%; font-size: 7pt; font-family: "Times New Roman", serif; ">* A provis�o de Imposto de Renda de A��es,&nbsp;demonstrada no quadro posi��o de ativos, reflete o imposto a pagar por a��o, individualmente. Este c�lculo � baseado na al�quota de Imposto de Renda vigente e � calculada com base na diferen�a entre o pre�o da a��o no mercado na data de refer�ncia do relat�rio e o pre�o m�dio de compra desta a��o. Este valor de Imposto de Renda aqui demonstrado � um exerc�cio para aux�lio na an�lise de retorno l�quido da carteira e, em nenhum momento, reflete o valor a ser lan�ado como a pagar sobre opera��es envolvendo compra e venda de a��es. A Quadrante n�o se responsabiliza pelo eventual uso destes valores como base para preenchimento de guias de recolhimento de Imposto de Renda de A��es (DARF), pois este c�lculo individual n�o reflete o valor l�quido de compras e vendas de a��es do Cliente, assim como posi��es de a��es que n�o est�o demonstradas neste relat�rio gerencial. Para ativos de previd�ncia, a provis�o de IR � estimada.</span></p></div></p>',
						    html: '<p style="margin: 5px 10px; font-family: Verdana; font-size: 10px; text-align: justify; color: #999;">' + FDESK.AS.observacaoExtrato + '</p>',
						    listeners : {
							    collapse : function() {
								    this.publishEvent('viewporttogglecollapse');
							    },
							    expand : function() {
								    this.publishEvent('viewporttogglecollapse');
							    }
						    }
                        };
                    }

					this.viewport = new Ext.Viewport({
								style : 'background: transparent;',
								renderTo : Ext.getBody(),
								layout : 'border',
								items : [centerPanel, headerPanel, southPanel].compact()
							});

                    this.subscribeEvent('ativocarteirasimuladaupdated', function(
									dateParam) {
								this.loadTabTables(null);
							}, this);

					this.subscribeEvent('dateperiodochanged', function(
									dateParam) {
								this.loadTabTables(null);
							}, this);

                    this.subscribeEvent('explodefundoschanged', function(
									dateParam) {
								this.loadTabTables(null);
							}, this);

					this.subscribeEvent('indicechanged',
							function(idIndice) {
								this.loadTabTables(['indicadores']);
							}, this);
							
					this.subscribeEvent('environmentupdated',
							function() {
								this.loadTabTables(null);
							}, this);		
					
					//Precisamos chamar o loadtables pois o primeiro evento environmentupdated
				    //eh disparado antes da carga deste modulo
				    this.loadTabTables(null);
					

					setTimeout(function() {
								Ext.get('loading-outer').remove();
								Ext.get('loading-mask').remove();
							}, 250);
				}
			});
	return new LayoutEngine();
}();

FDESK.LE = FDESK.LayoutEngine;