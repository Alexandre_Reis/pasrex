﻿<%@ page language="C#" autoeventwireup="true" inherits="Consultas_EnquadraResultado, Financial.Web_Deploy" enableEventValidation="false" theme="DevEx" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>    
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <asp:TextBox ID="hiddenId" runat="server" CssClass="hiddenField" Text="" />
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
        
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Resultados de Enquadramento"></asp:Label>
    </div>
        
    <div id="mainContent">
                                  
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"
                            PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                            Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                            HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>        
                    
                        <tr>
                            <td>                
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                            </td>
                                                
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />   
                            </td>  
                            
                            <td class="labelCurto">
                                <asp:Label ID="labelDataFim" runat="server" CssClass="labelCurto" Text="Fim:"></asp:Label>
                            </td>
                                          
                            <td>
                                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                            </td>                                                                            
                        </tr>
                     </table>
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor=Black CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>                    
                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor=Black CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                                                                                                                
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>
            
            <dxpc:ASPxPopupControl ID="popupResultadoDetalhe" runat="server" HeaderText="" Width="700px" 
                        ContentStyle-VerticalAlign="Top" CloseAction="CloseButton"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="WindowCenter" AllowDragging="True"
                        Modal="true">
                <ContentStyle Paddings-PaddingLeft="3px" Paddings-PaddingRight="3px" Paddings-PaddingBottom="3px"></ContentStyle>        
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <div>
                    <dxwgv:ASPxGridView ID="gridResultadoDetalhe" runat="server" Width="100%"
                            ClientInstanceName="gridResultadoDetalhe"  AutoGenerateColumns="False" 
                            DataSourceID="EsDSEnquadraResultadoDetalhe" KeyFieldName="IdResultado"                            
                            OnCustomCallback="gridResultadoDetalhe_CustomCallback"
                            OnHtmlDataCellPrepared="gridResultadoDetalhe_HtmlDataCellPrepared">                    
                    <Columns>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Item" Caption="Detalhamento" VisibleIndex="2" Width="20%"/>
                        
                        <dxwgv:GridViewDataComboBoxColumn FieldName="Enquadrado" Caption="Status" VisibleIndex="4" Width="12%">
                            <PropertiesComboBox>
                            <Items>
                                <dxe:ListEditItem Value="S" Text="Enquadrado" />
                                <dxe:ListEditItem Value="N" Text="Desenquadrado" />                        
                            </Items>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Resultado" Caption="%Exposição" VisibleIndex="9" Width="8%" 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorBase" Caption="Vl Base" VisibleIndex="11" Width="13%" 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="ValorCriterio" Caption="Vl Exposição" VisibleIndex="13" Width="13%" 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdResultado" Visible="false"/>                        
                        <dxwgv:GridViewDataColumn FieldName="IdRegra" Visible="false"/>
                        
                    </Columns>
                    <Settings ShowFilterRow="True" ShowTitlePanel="True" VerticalScrollBarMode="Visible" VerticalScrollableHeight="250"/>
                    <SettingsPager PageSize="30"></SettingsPager>
                    <SettingsBehavior ColumnResizeMode="Disabled" />            
                    <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                        <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
                        <Cell Wrap="False"/>
                    </Styles>
                    <Images/>
                    
                    <SettingsText EmptyDataRow="0 Registros" Title="Resultados de Enquadramento (Detalhamento)" />
                    </dxwgv:ASPxGridView>        
                </div>      
                </dxpc:PopupControlContentControl>
                </ContentCollection>                        
                <ClientSideEvents PopUp="function(s, e) {gridResultadoDetalhe.PerformCallback(); }" />
                </dxpc:ASPxPopupControl>
    
            <div class="linkButton" >   
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal1" runat="server" Text="Filtrar"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                                KeyFieldName="IdResultado" DataSourceID="EsDSEnquadraResultado"
                                OnPreRender="gridCadastro_PreRender"
                                OnCustomCallback="gridCadastro_CustomCallback"
                                OnHtmlDataCellPrepared="gridCadastro_HtmlDataCellPrepared" 
                                OnBeforeGetCallbackResult="gridCadastro_PreRender">                    
            <Columns>       
                
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="0" Width="10%"/>    
                
                <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Carteira" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left"/>
                
                <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="2" Width="20%"/>
                
                <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição Regra" VisibleIndex="3" Width="15%"/>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="Enquadrado" Caption="Status" VisibleIndex="4" Width="8%">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="S" Text="Enquadrado" />
                        <dxe:ListEditItem Value="N" Text="Desenquadrado" />                        
                    </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Resultado" Caption="%Exposição" VisibleIndex="9" Width="8%" 
                        HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                        CellStyle-HorizontalAlign="Right">                    
                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="ValorBase" Caption="Vl Base" VisibleIndex="11" Width="13%" 
                        HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                        CellStyle-HorizontalAlign="Right">                    
                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="ValorCriterio" Caption="Vl Exposição" VisibleIndex="13" Width="13%" 
                        HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                        CellStyle-HorizontalAlign="Right">                    
                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>                            
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataColumn FieldName="IdResultado" Visible="false"/>                
                <dxwgv:GridViewDataColumn FieldName="IdRegra" Visible="false"/>                
            </Columns>
            
            <Templates>
                <StatusBar>
                    <div>
                        <div style="float:left">
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                        </div>                    
                    </div>
                </StatusBar>
            </Templates>
            
            <ClientSideEvents Init="function(s, e) {e.cancel = true;}"
                              RowDblClick="function(s, e) {
                                                document.getElementById('hiddenId').value = e.visibleIndex;
                                                popupResultadoDetalhe.ShowAtElementByID(); return false;}" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>

    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" />
        
    <cc1:esDataSource ID="EsDSEnquadraResultado" runat="server" OnesSelect="EsDSEnquadraResultado_esSelect" />
    <cc1:esDataSource ID="EsDSEnquadraResultadoDetalhe" runat="server" OnesSelect="EsDSEnquadraResultadoDetalhe_esSelect" />
        
    </form>
</body>
</html>