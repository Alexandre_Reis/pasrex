﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Investidor;
using Financial.Bolsa.Exceptions;
using Financial.Bolsa.Enums;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using Financial.Common;
using System.IO;
using Financial.Util;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Interfaces.Import.Bolsa;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Contabil;
using Financial.Contabil.Enums;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Tributo.Enums;
using System.Collections;
using Financial.InvestidorCotista;

namespace Financial.Bolsa {
    public partial class ProventoBolsaCliente : esProventoBolsaCliente {
        
        /// <summary>
        /// Carrega os proventos na ProventoBolsaCliente a partir do arquivo CDIX.
        /// Não carrega os proventos pagos no dia (deve ser usado o arquivo CMDF para isto).
        /// </summary>
        /// <param name="nomeArquivo"></param>
        /// <param name="data"></param>
        public void CarregaProventoCdix(CdixCollection cdixCollection, DateTime data)
        {
            #region GetIdAgente do Arquivo Cdix
            int codigoBovespaAgente = cdixCollection.CollectionCdix[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);            
            #endregion

            ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollection.DeletaProventoBolsa(data, (int)FonteProventoBolsa.ArquivoCDIX);

            Cdix cdix = new Cdix();
            // Pega os proventos do arquivo de Cdix
            
            DateTime? dataLancamento = null;
            for (int i = 0; i < cdixCollection.CollectionCdix.Count; i++)
            {
                cdix = cdixCollection.CollectionCdix[i];

                if (cdix.IsTipoRegistroHeader())
                {
                    dataLancamento = cdix.DataMovimento.Value;
                }

                if (cdix.IsTipoRegistroIdentificacaoDividendo())
                {
                    // Informações do Arquivo
                    DateTime dataEx = cdix.DataInicioProvento.Value;
                    byte tipoProvento = (byte)cdix.TipoProvento.Value;
                    string codigoIsin = cdix.CodigoIsin;
                    int codigoClienteBovespa = cdix.CodigoCliente.Value;
                    //       

                    CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                    bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
                    if (idClienteExiste)
                    {
                        int idCliente = codigoClienteAgente.IdCliente.Value;
                        int distribuicao = cdix.Distribuicao.Value;
                        decimal quantidade = cdix.QuantidadeBase.Value;
                        decimal percentual = cdix.PercentualIR.Value;

                        // Dado o codigo Isin pego o cdAtivoBolsa onde tipo mercado = VIS
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (ativoBolsa.BuscaCdAtivoBolsa(codigoIsin))
                        {//Se não tem codigo Isin cadastrado, não importa!
                            string cdAtivoBolsa = ativoBolsa.CdAtivoBolsa;
                            
                            //Só importa com datas de pagamento projetdas
                            if (cdix.DataPagamento > data)
                            {
                                #region Só importa se cliente existe e está ativo!
                                Cliente cliente = new Cliente();
                                List<esQueryItem> campos = new List<esQueryItem>();
                                campos.Add(cliente.Query.StatusAtivo);
                                campos.Add(cliente.Query.DataDia);
                                campos.Add(cliente.Query.Status);
                                campos.Add(cliente.Query.IdTipo);
                                bool existeCliente = cliente.LoadByPrimaryKey(idCliente);

                                if (existeCliente && cliente.IsAtivo) //Só importa para clientes ativos!
                                {
                                    int idTipoCliente = cliente.IdTipo.Value;
                                    byte status = cliente.Status.Value;

                                    if (idTipoCliente == (int)TipoClienteFixo.ClientePessoaFisica && ativoBolsa.TipoMercado == TipoMercadoBolsa.Imobiliario)
                                    {
                                        //Fundos imobiliários nao sao tributados para pessoa fisica
                                        percentual = 0;
                                    }

                                    #region Copia informações para ProventoBolsaCliente
                                    ProventoBolsaCliente proventoBolsaCliente = new ProventoBolsaCliente();
                                    //
                                    proventoBolsaCliente.DataLancamento = dataLancamento;
                                    proventoBolsaCliente.DataEx = dataEx;
                                    proventoBolsaCliente.DataPagamento = cdix.DataPagamento;
                                    proventoBolsaCliente.TipoProvento = tipoProvento;
                                    proventoBolsaCliente.CdAtivoBolsa = cdAtivoBolsa;
                                    proventoBolsaCliente.IdCliente = idCliente;
                                    proventoBolsaCliente.Distribuicao = distribuicao;
                                    proventoBolsaCliente.Quantidade = quantidade;
                                    proventoBolsaCliente.PercentualIR = percentual;
                                    proventoBolsaCliente.Valor = cdix.Valor.Value;
                                    proventoBolsaCliente.Fonte = (int)FonteProventoBolsa.ArquivoCDIX;
                                    proventoBolsaCliente.IdAgenteMercado = idAgente;
                                    //
                                    proventoBolsaCliente.Save();
                                    #endregion
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Carrega os proventos na ProventoBolsaCliente a partir de lançamentos feitos direto na ProventoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaProventosBolsa(int idCliente, DateTime data)
        {  
            ProventoBolsaClienteCollection proventoBolsaClienteCollectionDeletar = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollectionDeletar.Query.Where(proventoBolsaClienteCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                       proventoBolsaClienteCollectionDeletar.Query.DataEx.Equal(data),
                                                       proventoBolsaClienteCollectionDeletar.Query.Fonte.Equal(FonteProventoBolsa.Manual),
                                                       proventoBolsaClienteCollectionDeletar.Query.Valor.GreaterThan(0));
            if (proventoBolsaClienteCollectionDeletar.Query.Load())
            {
                proventoBolsaClienteCollectionDeletar.MarkAllAsDeleted();
                proventoBolsaClienteCollectionDeletar.Save();
            }

            ProventoBolsaCollection proventoBolsaCollection = new ProventoBolsaCollection();
            proventoBolsaCollection.Query.Where(proventoBolsaCollection.Query.DataEx.Equal(data));
            proventoBolsaCollection.Query.Load();

            ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();

            int idTipoCliente = 0;
            byte grossup = 0;
            byte distribuicaoDividendo = 0;
            if (proventoBolsaCollection.Count > 0)
            {
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCliente);
                idTipoCliente = cliente.IdTipo.Value;
                grossup = cliente.GrossUP.Value;

                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCliente);
                distribuicaoDividendo = carteira.DistribuicaoDividendo.GetValueOrDefault(0);
            }

            //Posições a serem consideradas devem ser as de fechamento do dia anterior à data ex (último dia "COM")
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            string isentoIR = "N";
            if (proventoBolsaCollection.Count > 0)
            {
                ClienteBolsa clienteBolsa = new ClienteBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(clienteBolsa.Query.IsentoIR);
                if (clienteBolsa.LoadByPrimaryKey(campos, idCliente))
                {
                    if (!String.IsNullOrEmpty(clienteBolsa.IsentoIR))
                    {
                        isentoIR = clienteBolsa.IsentoIR;
                    }
                }
            }

            foreach (ProventoBolsa proventoBolsa in proventoBolsaCollection)
            {
                string cdAtivoBolsa = proventoBolsa.CdAtivoBolsa;
                DateTime dataEx = proventoBolsa.DataEx.Value;
                DateTime dataPagamento = proventoBolsa.DataPagamento.Value;
                byte tipoProvento = proventoBolsa.TipoProvento.Value;
                decimal valor = proventoBolsa.Valor.Value;
                
                #region Gera o provento a partir de PosicaoBolsa, caso tenha o ativo
                decimal quantidade = 0;
                decimal valorProvento = 0;

                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.Quantidade.Sum(),
                                                             posicaoBolsaHistoricoCollection.Query.IdAgente);
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataAnterior),
                                                            posicaoBolsaHistoricoCollection.Query.Quantidade.GreaterThan(0));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.IdAgente);

                posicaoBolsaHistoricoCollection.Query.Load();

                foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                {
                    quantidade = posicaoBolsaHistorico.Quantidade.Value;

                    PosicaoTermoBolsa posicaoTermoBolsaVendido = new PosicaoTermoBolsa();
                    posicaoTermoBolsaVendido.Query.Select(posicaoTermoBolsaVendido.Query.Quantidade.Sum());
                    posicaoTermoBolsaVendido.Query.Where(posicaoTermoBolsaVendido.Query.CdAtivoBolsa.Like(cdAtivoBolsa + "%"),
                                                         posicaoTermoBolsaVendido.Query.IdCliente.Equal(idCliente),
                                                         posicaoTermoBolsaVendido.Query.Quantidade.LessThan(0));
                    posicaoTermoBolsaVendido.Query.Load();

                    decimal quantidadeTermo = posicaoTermoBolsaVendido.Quantidade.HasValue ? posicaoTermoBolsaVendido.Quantidade.Value : 0;

                    quantidade -= Math.Abs(quantidadeTermo);

                    valorProvento = Utilitario.Truncate(valor * quantidade, 2);
                    int idAgente = posicaoBolsaHistorico.IdAgente.Value;
            
                    if (quantidade != 0 && valorProvento != 0)
                    {
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(ativoBolsa.Query.Especificacao);
                        campos.Add(ativoBolsa.Query.TipoMercado);
                        ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                        decimal percentualIR = 0;
                        if (tipoProvento != (byte)TipoProventoBolsa.Amortizacao)
                        {
                            if ((idTipoCliente == (int)TipoClienteFixo.ClientePessoaFisica || idTipoCliente == (int)TipoClienteFixo.InvestidorEstrangeiro) &&
                                ativoBolsa.TipoMercado == TipoMercadoBolsa.Imobiliario)
                            {
                                if (grossup != (byte)GrossupCliente.NaoFaz && isentoIR == "N")
                                {
                                    if (idTipoCliente == (int)TipoClienteFixo.InvestidorEstrangeiro)
                                    {
                                        percentualIR = 25;
                                    }
                                    else
                                    {
                                        percentualIR = 15;
                                    }

                                    decimal valorIR = Utilitario.Truncate(valorProvento / (1M - percentualIR / 100M) * (percentualIR / 100M), 2);
                                    valorProvento = valorProvento + valorIR;
                                }
                                else
                                {
                                    percentualIR = 0;
                                }
                            }
                            else if ((idTipoCliente == (int)TipoClienteFixo.ClientePessoaFisica || idTipoCliente == (int)TipoClienteFixo.ClientePessoaJuridica) &&
                                tipoProvento == (byte)TipoProventoBolsa.JurosSobreCapital && isentoIR == "N")
                            {
                                percentualIR = 15;
                            }
                            else if (idTipoCliente == (int)TipoClienteFixo.InvestidorEstrangeiro && tipoProvento != (byte)TipoProventoBolsa.Dividendo && isentoIR == "N")
                            {
                                percentualIR = 25;
                            }
                            else if ((idTipoCliente == (int)TipoClienteFixo.ClientePessoaFisica || idTipoCliente == (int)TipoClienteFixo.ClientePessoaJuridica) &&
                                tipoProvento == (byte)TipoProventoBolsa.Rendimento && isentoIR == "N")
                            {
                                percentualIR = 20;
                            }

                        }

                        ProventoBolsaClienteCollection proventoBolsaClienteCollectionExiste = new ProventoBolsaClienteCollection();
                        proventoBolsaClienteCollectionExiste.Query.Select(proventoBolsaClienteCollectionExiste.Query.IdProvento);
                        proventoBolsaClienteCollectionExiste.Query.Where(proventoBolsaClienteCollectionExiste.Query.IdCliente.Equal(idCliente),
                                                                         proventoBolsaClienteCollectionExiste.Query.DataLancamento.Equal(data),
                                                                         proventoBolsaClienteCollectionExiste.Query.DataEx.Equal(dataEx),
                                                                         proventoBolsaClienteCollectionExiste.Query.DataPagamento.Equal(dataPagamento),
                                                                         proventoBolsaClienteCollectionExiste.Query.TipoProvento.Equal(tipoProvento),
                                                                         proventoBolsaClienteCollectionExiste.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                                         proventoBolsaClienteCollectionExiste.Query.Valor.Equal(valor),
                                                                         proventoBolsaClienteCollectionExiste.Query.Valor.GreaterThan(0),
                                                                         proventoBolsaClienteCollectionExiste.Query.Fonte.Equal(FonteProventoBolsa.Manual),
                                                                         proventoBolsaClienteCollectionExiste.Query.IdAgenteMercado.Equal(idAgente));
                        proventoBolsaClienteCollectionExiste.Query.Load();

                        if (proventoBolsaClienteCollectionExiste.Count == 0)
                        {
                            #region Copia informações para ProventoBolsaCliente
                            ProventoBolsaCliente proventoBolsaCliente = new ProventoBolsaCliente();
                            //
                            proventoBolsaCliente.IdCliente = idCliente;
                            proventoBolsaCliente.DataLancamento = data;
                            proventoBolsaCliente.DataEx = dataEx;
                            proventoBolsaCliente.DataPagamento = dataPagamento;
                            proventoBolsaCliente.TipoProvento = tipoProvento;
                            proventoBolsaCliente.CdAtivoBolsa = cdAtivoBolsa;
                            proventoBolsaCliente.Distribuicao = 0;
                            proventoBolsaCliente.Quantidade = quantidade;
                            proventoBolsaCliente.PercentualIR = percentualIR;
                            proventoBolsaCliente.Valor = valorProvento;
                            proventoBolsaCliente.Fonte = (int)FonteProventoBolsa.Manual;
                            proventoBolsaCliente.IdAgenteMercado = idAgente;
                            //
                            proventoBolsaCliente.Save();

                            if (tipoProvento == (byte)TipoProventoBolsa.Dividendo)
                            {
                                if (distribuicaoDividendo == (byte)DistribuicaoDividendosCarteira.DistribuiCotasCompetencia)
                                {
                                    //Calcula IR por cotista e grava valor líquido
                                    //Importante - Não podemos lançar o valor de percentualIR pois deve verificar se o cotista é isento ou não
                                    #region Calculo IR Repasse

                                    PosicaoCotistaAbertura posicaoCotistaAberturaTotal = new PosicaoCotistaAbertura();
                                    decimal totalCotas = posicaoCotistaAberturaTotal.RetornaTotalCotas(idCliente, data);

                                    //Seleciona Posicao de abertura
                                    PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
                                    posicaoCotistaAberturaCollection.Query.Select(posicaoCotistaAberturaCollection.Query.IdCotista,
                                                                                  posicaoCotistaAberturaCollection.Query.Quantidade.Sum());
                                    posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCliente),
                                                                                 posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(data),
                                                                                 posicaoCotistaAberturaCollection.Query.Quantidade.GreaterThan(0));
                                    posicaoCotistaAberturaCollection.Query.GroupBy(posicaoCotistaAberturaCollection.Query.IdCotista);
                                    posicaoCotistaAberturaCollection.Query.Load();

                                    decimal totalIR = 0;
                                    decimal valorResidual = valorProvento;
                                    int cont = 1;

                                    foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in posicaoCotistaAberturaCollection)
                                    {
                                        int idCotista = posicaoCotistaAbertura.IdCotista.Value;
                                        decimal fatorDistribuicao = posicaoCotistaAbertura.Quantidade.GetValueOrDefault(0) / totalCotas;

                                        decimal valorDistribuir = Utilitario.Truncate(valorProvento * fatorDistribuicao, 2);

                                        if (cont == posicaoCotistaAberturaCollection.Count)
                                        {
                                            valorDistribuir = valorResidual;
                                        }

                                        Cotista cotista = new Cotista();
                                        cotista.LoadByPrimaryKey(idCotista);
                                        decimal valorIr = 0;
                                        DateTime dataCorteRFB1585 = new DateTime(2015, 09, 02);
                                        if (cotista.IsentoIR.Equals("N") && DateTime.Compare(data, dataCorteRFB1585) >= 0)
                                        {
                                            valorIr = Utilitario.Truncate(valorDistribuir * 0.15M, 2);
                                            valorDistribuir -= valorIr;
                                            totalIR += valorIr;
                                        }

                                        valorResidual -= (valorDistribuir + valorIr);

                                        cont++;

                                    }
                                    #endregion


                                    //ProventoBolsaCliente proventoBolsaClienteDistribuicao = new ProventoBolsaCliente();
                                    ////
                                    //proventoBolsaClienteDistribuicao.IdCliente = idCliente;
                                    //proventoBolsaClienteDistribuicao.DataLancamento = data;
                                    //proventoBolsaClienteDistribuicao.DataEx = dataEx;
                                    //proventoBolsaClienteDistribuicao.DataPagamento = dataPagamento;
                                    //proventoBolsaClienteDistribuicao.TipoProvento = (byte)TipoProventoBolsa.DividendoDistribuicao;
                                    //proventoBolsaClienteDistribuicao.CdAtivoBolsa = cdAtivoBolsa;
                                    //proventoBolsaClienteDistribuicao.Distribuicao = 0;
                                    //proventoBolsaClienteDistribuicao.Quantidade = quantidade;
                                    //proventoBolsaClienteDistribuicao.PercentualIR = percentualIR;
                                    //proventoBolsaClienteDistribuicao.Valor = valorProvento - totalIR;
                                    //proventoBolsaClienteDistribuicao.Fonte = (int)FonteProventoBolsa.Manual;
                                    //proventoBolsaClienteDistribuicao.IdAgenteMercado = idAgente;
                                    ////
                                    //proventoBolsaClienteDistribuicao.Save();
                                }
                            }
                            #endregion
                        }
                    }
                }
                #endregion

                #region Gera o provento a partir de PosicaoTermoBolsa, caso tenha o ativo
                quantidade = 0;
                valorProvento = 0;                
                PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
                posicaoTermoBolsaHistorico.Query.Select(posicaoTermoBolsaHistorico.Query.Quantidade.Sum());
                posicaoTermoBolsaHistorico.Query.Where(posicaoTermoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                                  posicaoTermoBolsaHistorico.Query.CdAtivoBolsa.Substring(5).Equal(cdAtivoBolsa),
                                                  posicaoTermoBolsaHistorico.Query.DataHistorico.Equal(dataAnterior),
                                                  posicaoTermoBolsaHistorico.Query.Quantidade.GreaterThan(0));
                if (posicaoTermoBolsaHistorico.Query.Load())
                {
                    if (posicaoTermoBolsaHistorico.Quantidade.HasValue)
                    {
                        quantidade = posicaoTermoBolsaHistorico.Quantidade.Value;
                        valorProvento = Utilitario.Truncate(valor * quantidade, 2);
                    }
                }
                
                if (quantidade != 0 && valorProvento != 0)
                {
                    decimal percentualIR = 0;
                    if ((idTipoCliente == (int)TipoClienteFixo.ClientePessoaFisica || idTipoCliente == (int)TipoClienteFixo.ClientePessoaJuridica) &&
                        tipoProvento == (byte)TipoProventoBolsa.JurosSobreCapital)
                    {
                        percentualIR = 15;
                    }
                    else if ((idTipoCliente == (int)TipoClienteFixo.ClientePessoaFisica || idTipoCliente == (int)TipoClienteFixo.ClientePessoaJuridica) &&
                        tipoProvento == (byte)TipoProventoBolsa.Rendimento)
                    {
                        percentualIR = 20;
                    }

                    ProventoBolsaClienteCollection proventoBolsaClienteCollectionExiste = new ProventoBolsaClienteCollection();
                    proventoBolsaClienteCollectionExiste.Query.Select(proventoBolsaClienteCollectionExiste.Query.IdProvento);
                    proventoBolsaClienteCollectionExiste.Query.Where(proventoBolsaClienteCollectionExiste.Query.IdCliente.Equal(idCliente),
                                                                     proventoBolsaClienteCollectionExiste.Query.DataLancamento.Equal(data),
                                                                     proventoBolsaClienteCollectionExiste.Query.DataEx.Equal(dataEx),
                                                                     proventoBolsaClienteCollectionExiste.Query.DataPagamento.Equal(dataPagamento),
                                                                     proventoBolsaClienteCollectionExiste.Query.TipoProvento.Equal(tipoProvento),
                                                                     proventoBolsaClienteCollectionExiste.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                                     proventoBolsaClienteCollectionExiste.Query.Valor.GreaterThan(0),
                                                                     proventoBolsaClienteCollectionExiste.Query.Valor.Equal(valor),
                                                                     proventoBolsaClienteCollectionExiste.Query.Fonte.Equal(FonteProventoBolsa.Manual));
                    proventoBolsaClienteCollectionExiste.Query.Load();

                    if (proventoBolsaClienteCollectionExiste.Count == 0)
                    {
                        #region Copia informações para ProventoBolsaCliente
                        ProventoBolsaCliente proventoBolsaCliente = new ProventoBolsaCliente();
                        //
                        proventoBolsaCliente.IdCliente = idCliente;
                        proventoBolsaCliente.DataLancamento = data;
                        proventoBolsaCliente.DataEx = dataEx;
                        proventoBolsaCliente.DataPagamento = dataPagamento;
                        proventoBolsaCliente.TipoProvento = tipoProvento;
                        proventoBolsaCliente.CdAtivoBolsa = cdAtivoBolsa;
                        proventoBolsaCliente.Distribuicao = 0;
                        proventoBolsaCliente.Quantidade = quantidade;
                        proventoBolsaCliente.PercentualIR = percentualIR;
                        proventoBolsaCliente.Valor = valorProvento;
                        proventoBolsaCliente.Fonte = (int)FonteProventoBolsa.Manual;
                        //
                        proventoBolsaCliente.Save();

                        //if (tipoProvento == (byte)TipoProventoBolsa.Dividendo)
                        //{
                        //    if (distribuicaoDividendo == (byte)DistribuicaoDividendosCarteira.DistribuiFinanceiroCompetencia
                        //        || distribuicaoDividendo == (byte)DistribuicaoDividendosCarteira.DistribuiCotasCompetencia)
                        //    {
                        //        ProventoBolsaCliente proventoBolsaClienteDistribuicao = new ProventoBolsaCliente();

                        //        proventoBolsaClienteDistribuicao.IdCliente = idCliente;
                        //        proventoBolsaClienteDistribuicao.DataLancamento = data;
                        //        proventoBolsaClienteDistribuicao.DataEx = dataEx;
                        //        proventoBolsaClienteDistribuicao.DataPagamento = dataPagamento;
                        //        proventoBolsaClienteDistribuicao.TipoProvento = (byte)TipoProventoBolsa.DividendoDistribuicao;
                        //        proventoBolsaClienteDistribuicao.CdAtivoBolsa = cdAtivoBolsa;
                        //        proventoBolsaClienteDistribuicao.Distribuicao = 0;
                        //        proventoBolsaClienteDistribuicao.Quantidade = quantidade;
                        //        proventoBolsaClienteDistribuicao.PercentualIR = percentualIR;
                        //        proventoBolsaClienteDistribuicao.Valor = valorProvento;
                        //        proventoBolsaClienteDistribuicao.Fonte = (int)FonteProventoBolsa.Manual;
                        //        //
                        //        proventoBolsaClienteDistribuicao.Save();
                        //    }
                        //}
                        #endregion
                    }
                }
                #endregion
            }
        }

        /// <summary>
        /// Carrega os proventos na ProventoBolsaCliente a partir de lançamentos feitos direto na ProventoBolsa.
        /// No caso de BTC tomador, para gerar os estornos dos valores ao doador do BTC, não tem dependência de arquivo CDIX.
        /// No caso de BTC doador, checa para ver se houve importação de CDIX no dia, caso não tenha havido, faz o cálculo diretamente pela posição de bolsa de BTC doada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaProventosBolsaBTC(int idCliente, DateTime data)
        {
            DateTime dataAnteriorBolsa = Calendario.SubtraiDiaUtil(data, 2, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            //Lembrar que o Delete abaixo só é necessário para qtde < 0, já que qtdes > 0 já foram previamente deletadas na função de Proventos sobre ações
            ProventoBolsaClienteCollection proventoBolsaClienteCollectionDeletar = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollectionDeletar.Query.Where(proventoBolsaClienteCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                       proventoBolsaClienteCollectionDeletar.Query.DataLancamento.Equal(data),
                                                       proventoBolsaClienteCollectionDeletar.Query.Fonte.Equal(FonteProventoBolsa.Manual),
                                                       proventoBolsaClienteCollectionDeletar.Query.Valor.LessThan(0));
            if (proventoBolsaClienteCollectionDeletar.Query.Load())
            {
                proventoBolsaClienteCollectionDeletar.MarkAllAsDeleted();
                proventoBolsaClienteCollectionDeletar.Save();
            }            

            ProventoBolsaCollection proventoBolsaCollection = new ProventoBolsaCollection();
            proventoBolsaCollection.Query.Where(proventoBolsaCollection.Query.DataEx.GreaterThanOrEqual(dataAnteriorBolsa),
                                                proventoBolsaCollection.Query.DataEx.LessThanOrEqual(data));
            proventoBolsaCollection.Query.Load();

            ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
            
            foreach (ProventoBolsa proventoBolsa in proventoBolsaCollection)
            {
                string cdAtivoBolsa = proventoBolsa.CdAtivoBolsa;
                DateTime dataEx = proventoBolsa.DataEx.Value;
                DateTime dataPagamento = proventoBolsa.DataPagamento.Value;
                byte tipoProvento = proventoBolsa.TipoProvento.Value;
                decimal valor = proventoBolsa.Valor.Value;

                #region Gera o provento a partir de PosicaoEmprestimoBolsa, caso tenha o ativo
                decimal valorProvento = 0;

                PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection();
                posicaoEmprestimoBolsaAberturaCollection.BuscaPosicaoEmprestimoBolsaAberturaAtivo(idCliente, data , cdAtivoBolsa);

                bool temCDIX = false;
                bool alteraCustodia = false;
                byte distribuicaoDividendo = 0;
                if (posicaoEmprestimoBolsaAberturaCollection.Count > 0)
                {
                    temCDIX = this.TemImportacaoCDIX(idCliente, data);

                    Cliente cliente = new Cliente();
                    cliente.Query.Select(cliente.Query.ApuraGanhoRV);
                    cliente.Query.Where(cliente.Query.IdCliente.Equal(idCliente));
                    cliente.Query.Load();

                    alteraCustodia = (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraTodas ||
                                (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.NaoApura));

                    Carteira carteira = new Carteira();
                    carteira.LoadByPrimaryKey(idCliente);
                    distribuicaoDividendo = carteira.DistribuicaoDividendo.GetValueOrDefault(0);
                }

                for (int j = 0; j < posicaoEmprestimoBolsaAberturaCollection.Count; j++)
                {
                    PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsa = posicaoEmprestimoBolsaAberturaCollection[j];
                    #region Dados de PosicaoEmprestimoBolsa
                    decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                    DateTime dataRegistro = posicaoEmprestimoBolsa.DataRegistro.Value;
                    byte ponta = posicaoEmprestimoBolsa.PontaEmprestimo.Value;
                    int idAgente = posicaoEmprestimoBolsa.IdAgente.Value;
                    #endregion

                    //Este teste serve para poder tratar a questão do BTC sofrer efeitos de eventos com data-ex até d-2 data data de registro
                    if ((dataRegistro <= dataEx && dataEx == data) || (dataRegistro > dataEx && dataRegistro == data))
                    {
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(ativoBolsa.Query.Especificacao);
                        campos.Add(ativoBolsa.Query.TipoMercado);
                        ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);

                        if (ponta == (byte)PontaEmprestimoBolsa.Tomador || (!temCDIX && alteraCustodia))
                        {
                            valorProvento = Utilitario.Truncate(valor * quantidade, 2);

                            if (ponta == (byte)PontaEmprestimoBolsa.Tomador)
                            {                                
                                valorProvento = valorProvento * -1;
                            }
                        }

                        if (valorProvento != 0)
                        {
                            #region Copia informações para ProventoBolsaCliente
                            ProventoBolsaCliente proventoBolsaCliente = new ProventoBolsaCliente();
                            //
                            proventoBolsaCliente.IdCliente = idCliente;
                            proventoBolsaCliente.DataLancamento = data;
                            proventoBolsaCliente.DataEx = dataEx;
                            proventoBolsaCliente.DataPagamento = dataPagamento;
                            proventoBolsaCliente.TipoProvento = tipoProvento;
                            proventoBolsaCliente.CdAtivoBolsa = cdAtivoBolsa;
                            proventoBolsaCliente.Distribuicao = 0;
                            proventoBolsaCliente.Quantidade = quantidade;
                            proventoBolsaCliente.PercentualIR = 0;
                            proventoBolsaCliente.Valor = valorProvento;
                            proventoBolsaCliente.Fonte = (byte)FonteProventoBolsa.Manual;
                            proventoBolsaCliente.IdAgenteMercado = idAgente;
                            proventoBolsaCliente.TipoPosicaoBolsa = (short)TipoPosicaoBolsaFixo.Emprestimo;
                            //
                            proventoBolsaCliente.Save();

                            //if (tipoProvento == (byte)TipoProventoBolsa.Dividendo)
                            //{
                            //    if (distribuicaoDividendo == (byte)DistribuicaoDividendosCarteira.DistribuiFinanceiroCompetencia
                            //        || distribuicaoDividendo == (byte)DistribuicaoDividendosCarteira.DistribuiCotasCompetencia)
                            //    {
                            //        ProventoBolsaCliente proventoBolsaClienteDistribuicao = new ProventoBolsaCliente();

                            //        proventoBolsaClienteDistribuicao.IdCliente = idCliente;
                            //        proventoBolsaClienteDistribuicao.DataLancamento = data;
                            //        proventoBolsaClienteDistribuicao.DataEx = dataEx;
                            //        proventoBolsaClienteDistribuicao.DataPagamento = dataPagamento;
                            //        proventoBolsaClienteDistribuicao.TipoProvento = (byte)TipoProventoBolsa.DividendoDistribuicao;
                            //        proventoBolsaClienteDistribuicao.CdAtivoBolsa = cdAtivoBolsa;
                            //        proventoBolsaClienteDistribuicao.Distribuicao = 0;
                            //        proventoBolsaClienteDistribuicao.Quantidade = quantidade;
                            //        proventoBolsaClienteDistribuicao.PercentualIR = 0;
                            //        proventoBolsaClienteDistribuicao.Valor = valorProvento;
                            //        proventoBolsaClienteDistribuicao.Fonte = (int)FonteProventoBolsa.Manual;
                            //        proventoBolsaClienteDistribuicao.IdAgenteMercado = idAgente;
                            //        proventoBolsaClienteDistribuicao.TipoPosicaoBolsa = (short)TipoPosicaoBolsaFixo.Emprestimo;
                            //        //
                            //        proventoBolsaClienteDistribuicao.Save();
                            //    }
                            //}
                            #endregion
                        }
                    }
                }

                
                #endregion
            }
        }

        /// <summary>
	    /// 
	    /// </summary>
	    /// <param name="dataEx"></param>
	    /// <param name="tipoProvento"></param>
	    /// <param name="cdAtivoBolsa"></param>
	    /// <param name="idCliente"></param>
	    /// <param name="distribuicao"></param>
	    /// <param name="quantidade"></param>
	    /// <returns></returns>
        /// <exception cref="ArgumentException">Se tipoProvento for diferente dos tipos do enum TipoProvento</exception>
        public bool BuscaProventoBolsaCliente(DateTime dataEx, int tipoProvento,
            string cdAtivoBolsa, int idCliente, int distribuicao, decimal quantidade) {
            
            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("TipoProventoBolsa " + tipoProvento.ToString() + " incorreto. Os valores possiveis estão no enum TipoProventoBolsa");
            
            List<int> valoresPossiveis = TipoDoProvento.Values();
            if (!valoresPossiveis.Contains(tipoProvento)) {
                throw new ArgumentException(mensagem.ToString());
            }
            
            #endregion
 
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdProvento, this.Query.DataLancamento, this.Query.DataPagamento,
                         this.Query.PercentualIR)
                 .Where(this.Query.DataEx.Equal(dataEx),
                        this.Query.TipoProvento == tipoProvento,
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                        this.Query.IdCliente == idCliente,
                        this.Query.Distribuicao == distribuicao,
                        this.Query.Quantidade == quantidade);

            bool retorno = this.Query.Load();
            
            return retorno;        
        }                       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void LancaCCEventosDinheiro(int idCliente, DateTime data) 
        {
            #region Deleta as liquidações manuais oriundas de ProventoBolsa p/ não duplicar (a partir de D+2 já vem no CDIX)
            //Antes checa se está trabalhando com arquivo CDIX, senão nem precisa deletar!
            ProventoBolsaClienteCollection proventoBolsaClienteCollectionCDIX = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollectionCDIX.Query.Where(proventoBolsaClienteCollectionCDIX.Query.IdCliente.Equal(idCliente),
                                                       proventoBolsaClienteCollectionCDIX.Query.DataLancamento.Equal(data),
                                                       proventoBolsaClienteCollectionCDIX.Query.Fonte.Equal(FonteProventoBolsa.ArquivoCDIX));
            proventoBolsaClienteCollectionCDIX.Query.Load();
            //
            if (proventoBolsaClienteCollectionCDIX.Count > 0)
            {
                DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 2, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                //Notar que valores < 0 nao sao deletados, pois estes nao podem ser substituidos pelo CDIX, dado que sao relativos a BTC tomador
                //Excecao feita a IR sobre proventos...
                LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
                liquidacaoCollectionDeletar.Query.Select(liquidacaoCollectionDeletar.Query.IdLiquidacao);
                //
                List<byte> l = new List<byte>( new byte[] { (byte)OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes,
                                                            (byte)OrigemLancamentoLiquidacao.Bolsa.Dividendo,
                                                            (byte)OrigemLancamentoLiquidacao.Bolsa.DividendoDistribuicao,
                                                            (byte)OrigemLancamentoLiquidacao.Bolsa.JurosCapital,
                                                            (byte)OrigemLancamentoLiquidacao.Bolsa.Rendimento,
                                                            (byte)OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital,                                                            
                                                            (byte)OrigemLancamentoLiquidacao.Bolsa.OutrosProventos,
                                                            (byte)OrigemLancamentoLiquidacao.Bolsa.Amortizacao    
                                                          } );

                liquidacaoCollectionDeletar.Query.Where(
                                                 liquidacaoCollectionDeletar.Query.DataVencimento >= data,
                                                 liquidacaoCollectionDeletar.Query.DataLancamento <= dataAnterior,
                                                 liquidacaoCollectionDeletar.Query.IdCliente == idCliente,
                                                 liquidacaoCollectionDeletar.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Manual),
                                                 liquidacaoCollectionDeletar.Query.Valor >= 0, 
                                                 liquidacaoCollectionDeletar.Query.Origem.In(l));
                liquidacaoCollectionDeletar.Query.Load();
                liquidacaoCollectionDeletar.MarkAllAsDeleted();
                liquidacaoCollectionDeletar.Save();

                LiquidacaoCollection liquidacaoCollectionDeletar2 = new LiquidacaoCollection();
                liquidacaoCollectionDeletar2.Query.Select(liquidacaoCollectionDeletar2.Query.IdLiquidacao);
                //
                liquidacaoCollectionDeletar2.Query.Where(
                                                 liquidacaoCollectionDeletar2.Query.DataVencimento >= data,
                                                 liquidacaoCollectionDeletar2.Query.DataLancamento <= dataAnterior,
                                                 liquidacaoCollectionDeletar2.Query.IdCliente == idCliente,
                                                 liquidacaoCollectionDeletar2.Query.Fonte.Equal((byte)FonteLancamentoLiquidacao.Manual),
                                                 liquidacaoCollectionDeletar.Query.Origem.Equal((byte)OrigemLancamentoLiquidacao.Bolsa.IRProventos));
                liquidacaoCollectionDeletar2.Query.Load();
                liquidacaoCollectionDeletar2.MarkAllAsDeleted();
                liquidacaoCollectionDeletar2.Save();
            }
            #endregion
            
            //Deleta Liquidacao com Fonte=Interno, (Origem=Dividendo, JurosCapital, Rendimento, RestCapital ou Credito Acoes e afins) e Vcto >= data
            //ou Fonte=Manual, (Origem=Dividendo, JurosCapital, Rendimento, RestCapital ou Credito Acoes e afins) e Lacto = data
            LiquidacaoCollection liquidacaoCollectionDeletarLancamentoNaData = new LiquidacaoCollection();
            liquidacaoCollectionDeletarLancamentoNaData.DeletaLiquidacaoProventos(idCliente, data);
            
            //
            ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollection.BuscaProventoBolsaCliente(data, idCliente);

            Hashtable hsContaAtivo = new Hashtable();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            for (int i = 0; i < proventoBolsaClienteCollection.Count; i++) 
            {
                #region DadosProvento
                ProventoBolsaCliente proventoBolsaCliente = proventoBolsaClienteCollection[i];
                DateTime dataEx = proventoBolsaCliente.DataEx.Value;
                DateTime dataPagamento = proventoBolsaCliente.DataPagamento.Value;
                byte tipoProvento = proventoBolsaCliente.TipoProvento.Value;
                string cdAtivoBolsa = proventoBolsaCliente.CdAtivoBolsa;
                decimal quantidade = proventoBolsaCliente.Quantidade.Value;
                decimal percentualIR = proventoBolsaCliente.PercentualIR.Value;
                decimal valorIR = Utilitario.Truncate(proventoBolsaCliente.Valor.Value * percentualIR / 100, 2);
                decimal valor = proventoBolsaCliente.Valor.Value;
                byte fonteProventoBolsa = proventoBolsaCliente.Fonte.Value;
                int? idAgente = proventoBolsaCliente.IdAgenteMercado;
                int idMoedaAtivo = proventoBolsaCliente.UpToAtivoBolsaByCdAtivoBolsa.IdMoeda.Value;
                #endregion

                int idContaDefault = 0;
                #region Busca Conta Default
                if (hsContaAtivo.Contains(cdAtivoBolsa))
                {
                    idContaDefault = (int)hsContaAtivo[cdAtivoBolsa];
                }
                else
                {
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                    hsContaAtivo.Add(cdAtivoBolsa, idContaDefault);
                }
                #endregion

                string nomeAgente = "";
                if (idAgente.HasValue)
                {
                    AgenteMercado agenteMercado = new AgenteMercado();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(agenteMercado.Query.Nome);
                    agenteMercado.LoadByPrimaryKey(campos, idAgente.Value);
                    nomeAgente = agenteMercado.Nome;
                }

                if (cdAtivoBolsa.Length > 10) //Provento originado de Termo
                {
                    cdAtivoBolsa = cdAtivoBolsa.Substring(0, 5) + "T";
                }

                string descricao = string.Empty;
                int origem = (int) OrigemLancamentoLiquidacao.Bolsa.None;
                string quantidadeFormatada = quantidade.ToString("N0");
                if (proventoBolsaCliente.IsTipoProventoDividendo()) 
                {                    
                    if (proventoBolsaCliente.TipoPosicaoBolsa.GetValueOrDefault(0) == (int)TipoPosicaoBolsaFixo.Emprestimo)                    
                        descricao += "Posição de Empréstimo - ";

                    origem = (int)OrigemLancamentoLiquidacao.Bolsa.Dividendo;                    
                    descricao += "Dividendo s/ " + quantidadeFormatada + " " + cdAtivoBolsa;
                    
                    if(!Calendario.IsDiaUtil(dataPagamento, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
                    {
                        dataPagamento = Calendario.AdicionaDiaUtil(dataPagamento, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }
                }
                else if (proventoBolsaCliente.IsTipoProventoDividendoDistribuicao() )
                {
                    if (proventoBolsaCliente.TipoPosicaoBolsa.GetValueOrDefault(0) == (int)TipoPosicaoBolsaFixo.Emprestimo)
                        descricao += "Posição de Empréstimo - ";

                    descricao += "Repasse de dividendos em " + String.Format("{0:dd/MM/yyyy}", data);
                    origem = (int)OrigemLancamentoLiquidacao.Bolsa.DividendoDistribuicao;
                    valor *= (-1);

                    if (!Calendario.IsDiaUtil(dataPagamento, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
                    {
                        dataPagamento = Calendario.AdicionaDiaUtil(dataPagamento, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }
                }
                else if (proventoBolsaCliente.IsTipoProventoRestituicaoCapital())
                {
                    descricao = "Restituição Capital s/ " + quantidadeFormatada + " " + cdAtivoBolsa;
                    origem = (int)OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital;
                }
                else if (proventoBolsaCliente.IsTipoProventoJurosCapital())
                {
                    descricao = "Juros Capital s/ " + quantidadeFormatada + " " + cdAtivoBolsa;
                    origem = (int) OrigemLancamentoLiquidacao.Bolsa.JurosCapital;
                }
                else if (proventoBolsaCliente.IsTipoProventoJuros()) {
                    descricao = "Juros s/" + quantidadeFormatada + " " + cdAtivoBolsa;
                    origem = (int)OrigemLancamentoLiquidacao.Bolsa.OutrosProventos;
                }
                else if (proventoBolsaCliente.IsTipoProventoRendimento())
                {
                    descricao = "Rendimento s/ " + quantidadeFormatada + " " + cdAtivoBolsa;
                    origem = (int) OrigemLancamentoLiquidacao.Bolsa.Rendimento;
                }
                else if (proventoBolsaCliente.IsTipoProventoCreditoFracoesAcoes())
                {
                    descricao = "Crédito Fração Ações s/ " + quantidadeFormatada + " " + cdAtivoBolsa;
                    origem = (int)OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes;
                }
                else if (proventoBolsaCliente.IsTipoProventoBonificacaoDinheiro())
                {
                    descricao = "Bonificação em Dinheiro s/ " + quantidadeFormatada + " " + cdAtivoBolsa;
                    origem = (int)OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes;
                }
                else if (proventoBolsaCliente.IsTipoProventoAmortizacao())
                {
                    descricao = "Amortização s/ " + quantidadeFormatada + " " + cdAtivoBolsa;
                    origem = (int)OrigemLancamentoLiquidacao.Bolsa.Amortizacao ;
                }
                else 
                {
                    descricao = "Outros proventos s/ " + quantidadeFormatada + " " + cdAtivoBolsa;
                    origem = (int)OrigemLancamentoLiquidacao.Bolsa.OutrosProventos;
                }

                if (proventoBolsaCliente.Valor < 0)
                {
                    descricao = descricao + " a Pagar";
                }

                if (nomeAgente != "")
                {
                    descricao = descricao + " (" + nomeAgente + ")";
                }
                //

                int? idEvento = null;
                if (dataEx == data)
                {
                    if (proventoBolsaCliente.IsTipoProventoDividendo())
                    {
                        if (proventoBolsaCliente.Valor > 0)
                        {
                            idEvento = ContabRoteiro.RetornaIdEvento(ContabilOrigem.Bolsa.ProvisaoDividendosReceber);
                        }
                        else if (proventoBolsaCliente.Valor < 0)
                        {
                            idEvento = ContabRoteiro.RetornaIdEvento(ContabilOrigem.Bolsa.ProvisaoDividendosPagar);
                        }
                    }
                    else
                    {
                        if (proventoBolsaCliente.Valor > 0)
                        {
                            idEvento = ContabRoteiro.RetornaIdEvento(ContabilOrigem.Bolsa.ProvisaoJurosCapitalReceber);
                        }
                        else if (proventoBolsaCliente.Valor < 0)
                        {
                            idEvento = ContabRoteiro.RetornaIdEvento(ContabilOrigem.Bolsa.ProvisaoJurosCapitalPagar);
                        }
                    }
                }

                if (proventoBolsaCliente.Valor != 0)
                {
                    #region Insere liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataPagamento;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = origem;
                    liquidacao.IdEvento = idEvento;

                    if (proventoBolsaCliente.Fonte == (byte)FonteProventoBolsa.ArquivoCDIX)
                    {
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    }
                    else
                    {
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Manual;
                    }

                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                //
                if (valorIR != 0)
                {
                    descricao = "IR " + descricao;
                    #region Insere liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataPagamento;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valorIR * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Bolsa.IRProventos;

                    if (proventoBolsaCliente.Fonte == (byte)FonteProventoBolsa.ArquivoCDIX)
                    {
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    }
                    else
                    {
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Manual;
                    }

                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;
                    #endregion

                    liquidacaoCollection.AttachEntity(liquidacao);
                }                

            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Reduz o PU de custo em ativos FII para eventos do tipo Restituição de Capital no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaReducaoCustoFII(int idCliente, DateTime data)
        {
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            ProventoBolsaClienteQuery proventoBolsaClienteQuery = new ProventoBolsaClienteQuery("P");

            proventoBolsaClienteQuery.Select(proventoBolsaClienteQuery.CdAtivoBolsa,
                                             proventoBolsaClienteQuery.Valor,
                                             proventoBolsaClienteQuery.Quantidade);
            proventoBolsaClienteQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == proventoBolsaClienteQuery.CdAtivoBolsa);
            proventoBolsaClienteQuery.Where(proventoBolsaClienteQuery.IdCliente.Equal(idCliente),
                                            proventoBolsaClienteQuery.DataLancamento.Equal(data),
                                            proventoBolsaClienteQuery.TipoProvento.In(TipoProventoBolsa.RestituicaoCapital, TipoProventoBolsa.Amortizacao),
                                            ativoBolsaQuery.TipoMercado.Equal(TipoMercadoBolsa.Imobiliario));

            ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollection.Load(proventoBolsaClienteQuery);

            foreach (ProventoBolsaCliente proventoBolsaCliente in proventoBolsaClienteCollection)
            {
                string cdAtivoBolsa = proventoBolsaCliente.CdAtivoBolsa;
                decimal valor = proventoBolsaCliente.Valor.Value / proventoBolsaCliente.Quantidade.Value;

                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.IdPosicao,
                                                    posicaoBolsaCollection.Query.PUCusto,
                                                    posicaoBolsaCollection.Query.PUCustoLiquido);
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoBolsaCollection.Query.Load();

                foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                {
                    posicaoBolsa.PUCusto = posicaoBolsa.PUCusto.Value - valor;
                    posicaoBolsa.PUCustoLiquido = posicaoBolsa.PUCustoLiquido.Value - valor;
                }

                posicaoBolsaCollection.Save();
            }
        }

        /// <summary>
        /// Dsitribui dividendos estornando o valor provisionado de dividendos pelo clube/fundo.
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void DistribuiDividendos(int idCliente, DateTime data)
        {
            int diasCotizacao = 0;
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.DistribuicaoDividendo);
            campos.Add(carteira.Query.DiasCotizacaoAplicacao);
            carteira.LoadByPrimaryKey(campos, idCliente);
            
            diasCotizacao = carteira.DiasCotizacaoAplicacao.Value;
            byte distribuicaoDividendos = carteira.DistribuicaoDividendo.Value;

            if (distribuicaoDividendos != (byte)DistribuicaoDividendosCarteira.DistribuiFinanceiroCaixa &&
                distribuicaoDividendos != (byte)DistribuicaoDividendosCarteira.DistribuiFinanceiroCompetencia)
            {
                return;
            }

            PosicaoCotistaAbertura posicaoCotistaAberturaTotal = new PosicaoCotistaAbertura();
            decimal totalCotas = posicaoCotistaAberturaTotal.RetornaTotalCotas(idCliente, data);

            PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
            posicaoCotistaAberturaCollection.Query.Select(posicaoCotistaAberturaCollection.Query.IdCotista,
                                                          posicaoCotistaAberturaCollection.Query.Quantidade.Sum());
            posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCarteira.Equal(idCliente),
                                                         posicaoCotistaAberturaCollection.Query.DataHistorico.Equal(data),
                                                         posicaoCotistaAberturaCollection.Query.Quantidade.GreaterThan(0));
            posicaoCotistaAberturaCollection.Query.GroupBy(posicaoCotistaAberturaCollection.Query.IdCotista);
            posicaoCotistaAberturaCollection.Query.Load();

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.IdCliente.Equal(idCliente),
                                             liquidacaoCollection.Query.Origem.Equal((byte)OrigemLancamentoLiquidacao.Bolsa.Dividendo));
            if(distribuicaoDividendos == (byte)DistribuicaoDividendosCarteira.DistribuiFinanceiroCompetencia)
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.DataLancamento.Equal(data));
            else
                liquidacaoCollection.Query.Where(liquidacaoCollection.Query.DataVencimento.Equal(data));

            liquidacaoCollection.Query.Load();

            if (liquidacaoCollection.Count > 0)
            {
                #region delete IRProventos
                IRProventosCollection iRProventosCollection = new IRProventosCollection();
                iRProventosCollection.Query.Where(iRProventosCollection.Query.IdCarteira.Equal(idCliente));
                if(distribuicaoDividendos == (byte)DistribuicaoDividendosCarteira.DistribuiFinanceiroCompetencia)
                    iRProventosCollection.Query.Where(iRProventosCollection.Query.DataLancamento.Equal(data));
                else
                    iRProventosCollection.Query.Where(iRProventosCollection.Query.DataVencimento.Equal(data));

                iRProventosCollection.Query.Load();
                iRProventosCollection.MarkAllAsDeleted();
                iRProventosCollection.Save();
                #endregion
            }

            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                decimal totalIR = 0;
                decimal valorResidual = liquidacao.Valor.Value;
                int cont = 1;
                foreach (PosicaoCotistaAbertura posicaoCotistaAbertura in posicaoCotistaAberturaCollection)
                {
                    int idCotista = posicaoCotistaAbertura.IdCotista.Value;
                    decimal quantidade = posicaoCotistaAbertura.Quantidade.Value;
                    decimal fatorDistribuicao = quantidade / totalCotas;

                    decimal valorDistribuir = Utilitario.Truncate(liquidacao.Valor.Value * fatorDistribuicao, 2);

                    if (cont == posicaoCotistaAberturaCollection.Count)
                    {
                        valorDistribuir = valorResidual;
                    }

                    Cotista cotista = new Cotista();
                    cotista.LoadByPrimaryKey(idCotista);
                    decimal valorIr = 0;
                    DateTime dataCorteRFB1585 = new DateTime(2015, 09, 02);
                    if (cotista.IsentoIR.Equals("N") && DateTime.Compare(liquidacao.DataLancamento.Value, dataCorteRFB1585) >= 0)
                    {
                        valorIr = Utilitario.Truncate(valorDistribuir * 0.15M, 2);
                        valorDistribuir -= valorIr;
                        totalIR += valorIr;
                    }

                    valorResidual -= (valorDistribuir + valorIr);

                    #region insere Tabela para consulta da distribuicao
                    IRProventos irProventos = new IRProventos();
                    irProventos.IdCarteira = idCliente;
                    irProventos.IdCotista = idCotista;
                    irProventos.ValorDistribuido = valorDistribuir + valorIr;
                    irProventos.ValorIR = valorIr;
                    irProventos.ValorLiquido = valorDistribuir;
                    irProventos.Descricao = liquidacao.Descricao;
                    irProventos.DataLancamento = liquidacao.DataLancamento;
                    irProventos.DataVencimento = liquidacao.DataVencimento;
                    irProventos.IsentoIR = cotista.IsentoIR;
                    irProventos.Save();
                    #endregion

                    cont++;

                }

                Liquidacao liquidacaoNovo = new Liquidacao();
                liquidacaoNovo.IdCliente = idCliente;
                liquidacaoNovo.DataLancamento = data;
                liquidacaoNovo.DataVencimento = liquidacao.DataVencimento.Value;
                liquidacaoNovo.Descricao = "Distribuição (" + liquidacao.Descricao + ")";
                liquidacaoNovo.Valor = (liquidacao.Valor - totalIR) * -1;
                liquidacaoNovo.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                liquidacaoNovo.Origem = (int)OrigemLancamentoLiquidacao.Bolsa.Outros;
                liquidacaoNovo.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                liquidacaoNovo.IdConta = liquidacao.IdConta.Value;
                liquidacaoNovo.Save();

                //liquidacaoNovo = new Liquidacao();
                //liquidacaoNovo.IdCliente = idCliente;
                //liquidacaoNovo.DataLancamento = data;
                //liquidacaoNovo.DataVencimento = liquidacao.DataVencimento.Value;
                //liquidacaoNovo.Descricao = "Contrapartida - Distribuição (" + liquidacao.Descricao + ")";
                //liquidacaoNovo.Valor = (liquidacao.Valor - totalIR);
                //liquidacaoNovo.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                //liquidacaoNovo.Origem = (int)OrigemLancamentoLiquidacao.Bolsa.Outros;
                //liquidacaoNovo.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                //liquidacaoNovo.IdConta = liquidacao.IdConta.Value;
                //liquidacaoNovo.Save();

                #region RFB 1585
                if (totalIR > 0)
                {
                    //Lanca em CC um estorno do total de dividendos pagos na data
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                    //

                    string descricao = "IR - Distribuição de dividendos";

                    Liquidacao liquidacaoIR = new Liquidacao();
                    liquidacaoIR.IdCliente = idCliente;
                    liquidacaoIR.DataLancamento = data;
                    liquidacaoIR.DataVencimento = liquidacao.DataVencimento.Value; ;
                    liquidacaoIR.Descricao = descricao;
                    liquidacaoIR.Valor = totalIR * -1;
                    liquidacaoIR.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    liquidacaoIR.Origem = OrigemLancamentoLiquidacao.Bolsa.IRProventos;
                    liquidacaoIR.Fonte = (int)FonteLancamentoLiquidacao.Manual;
                    liquidacaoIR.IdConta = idContaDefault;
                    liquidacaoIR.Save();

                    //liquidacaoIR = new Liquidacao();
                    //liquidacaoIR.IdCliente = idCliente;
                    //liquidacaoIR.DataLancamento = data;
                    //liquidacaoIR.DataVencimento = liquidacao.DataVencimento.Value; ;
                    //liquidacaoIR.Descricao = "Contrapartida - " + descricao;
                    //liquidacaoIR.Valor = totalIR;
                    //liquidacaoIR.Situacao = (int)SituacaoLancamentoLiquidacao.Normal;
                    //liquidacaoIR.Origem = OrigemLancamentoLiquidacao.Bolsa.IRProventos;
                    //liquidacaoIR.Fonte = (int)FonteLancamentoLiquidacao.Interno;
                    //liquidacaoIR.IdConta = idContaDefault;
                    //liquidacaoIR.Save();


                }
                #endregion

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoProvento é Dividendo
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoProventoDividendo() 
        {
            return this.TipoProvento == TipoProventoBolsa.Dividendo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoProvento é Dividendo
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoProventoDividendoDistribuicao()
        {
            return this.TipoProvento == TipoProventoBolsa.DividendoDistribuicao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoProvento é RestCapital
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoProventoRestituicaoCapital()
        {
            return (this.TipoProvento == TipoProventoBolsa.RestituicaoCapital ||
                    this.TipoProvento == TipoProventoBolsa.RestituicaoCapitalComReducaoAcoes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoProvento é CreditoFracoesAcoes
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoProventoCreditoFracoesAcoes()
        {
            return this.TipoProvento == TipoProventoBolsa.CreditoFracoesAcoes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoProvento é Juros Sobre Capital
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoProventoJurosCapital() {
            return this.TipoProvento == TipoProventoBolsa.JurosSobreCapital;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoProvento é Juros
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoProventoJuros() {
            return this.TipoProvento == TipoProventoBolsa.Juros;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoProvento é Rendimento
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoProventoRendimento() {
            return (this.TipoProvento == TipoProventoBolsa.Rendimento || this.TipoProvento == TipoProventoBolsa.RendimentoLiquido);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoProvento é Boni em $
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoProventoBonificacaoDinheiro()
        {
            return (this.TipoProvento == TipoProventoBolsa.BonificacaoDinheiro);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoProvento é Amortização em $
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoProventoAmortizacao()
        {
            return (this.TipoProvento == TipoProventoBolsa.Amortizacao);
        }

        private bool TemImportacaoCDIX(int idCliente, DateTime data)
        {
            ProventoBolsaClienteCollection proventoBolsaClienteCollectionCDIX = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollectionCDIX.Query.Where(proventoBolsaClienteCollectionCDIX.Query.IdCliente.Equal(idCliente),
                                                           proventoBolsaClienteCollectionCDIX.Query.DataLancamento.Equal(data),
                                                           proventoBolsaClienteCollectionCDIX.Query.Fonte.Equal(FonteProventoBolsa.ArquivoCDIX));
            proventoBolsaClienteCollectionCDIX.Query.Load();

            if (proventoBolsaClienteCollectionCDIX.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }
}