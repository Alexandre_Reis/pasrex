﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using System.Reflection;

namespace Financial.Bolsa {
    public partial class PosicaoTermoBolsaCollection : esPosicaoTermoBolsaCollection {
        
        // Construtor
        // Cria uma nova PosicaoTermoBolsaCollection com os dados de PosicaoTermoBolsaHistoricoCollection
        // Todos do dados de PosicaoTermoBolsaHistoricoCollection são copiados com excessão da dataHistorico
        public PosicaoTermoBolsaCollection(PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection) {
            for (int i = 0; i < posicaoTermoBolsaHistoricoCollection.Count; i++) {
                //
                PosicaoTermoBolsa p = new PosicaoTermoBolsa();

                // Para cada Coluna de PosicaoTermoBolsaHistorico copia para PosicaoTermoBolsa
                foreach (esColumnMetadata colPosicaoHistorico in posicaoTermoBolsaHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != PosicaoTermoBolsaHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colPosicaoBolsa = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (posicaoTermoBolsaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
                            p.SetColumn(colPosicaoBolsa.Name, posicaoTermoBolsaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        //HACK: uso provisorio para Calculated Column
        protected void CreateExtendedProperties(esColumnMetadataCollection extendedProps) {
            ArrayList CalcList = GetCalculatedColumnList(typeof(PosicaoTermoBolsa));
            foreach (CalculatedColumn Item in CalcList) {
                if (null == extendedProps[Item.CalculatedColumnName]) {
                    esColumnMetadata col = new esColumnMetadata(Item.CalculatedColumnName, this.Meta.Columns.Count + 1, Item.DataType);
                    col.PropertyName = Item.CalculatedColumnName;
                    col.IsTransient = Item.IsTransient;
                    extendedProps.Add(col);
                }
            }
        }

        //HACK: uso provisorio para Calculated Column
        private static ArrayList GetCalculatedColumnList(Type destinationType) {
            //Get all the properties that are in the destination type.
            PropertyInfo[] destinationProperties = destinationType.GetProperties();
            ArrayList ReturnList = new ArrayList();
            CalculatedColumn Item;

            foreach (PropertyInfo property in destinationProperties) {
                //Get the CalculatedColumn attribute.
                object[] aliasAttributes = property.GetCustomAttributes(typeof(CalculatedColumn), true);

                //If found Add to the List
                if (aliasAttributes.Length > 0) {
                    bool IsTransient = (bool)((CalculatedColumn)aliasAttributes[0]).IsTransient;
                    Item = new CalculatedColumn(IsTransient);
                    Item.DataType = property.PropertyType;
                    Item.CalculatedColumnName = property.Name;
                    ReturnList.Add(Item);
                    //ReturnList.Add(property.Name);
                }
            }

            return ReturnList;
        }

        /// <summary>
        /// Carrega o objeto PosicaoTermoBolsaCollection com os campos IdPosicao, CdAtivoBolsa, 
        /// Quantidade, ValorMercado, PUMercado.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="cdAtivoBolsa"></param>
        public void BuscaPosicaoTermoBolsa(int idCliente) 
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();        
        }

        /// <summary>
        /// Carrega o objeto PosicaoTermoBolsaCollection com todos os campos de PosicaoTermoBolsa.
        /// Filtra Quantidade != 0.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="cdAtivoBolsa"></param>
        public void BuscaPosicaoTermoBolsaCompleta(int idCliente) 
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                             this.Query.Quantidade.NotEqual(0));
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoTermoBolsaCollection com todos os campos de PosicaoTermoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="cdAtivoBolsa"></param>
        public void BuscaPosicaoTermoBolsaCompleta(int idCliente, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.CdAtivoBolsa == cdAtivoBolsa);

            this.Query.Load();         
        }

        /// <summary>
        /// Deleta todas as posições do idCliente.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaPosicaoTermoBolsa(int idCliente) 
        {
            this.Query
                    .Select(this.Query.IdPosicao)
                    .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Método de inserção em PosicaoTermoBolsa a partir de PosicaoTermoBolsaHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InserePosicaoTermoBolsaHistorico(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            posicaoTermoBolsaHistoricoCollection.BuscaPosicaoTermoBolsaHistoricoCompleta(idCliente, dataHistorico);
            for (int i = 0; i < posicaoTermoBolsaHistoricoCollection.Count; i++) {
                PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = posicaoTermoBolsaHistoricoCollection[i];
                
                #region Copia de posicaoTermoBolsaHistorico para posicaoTermoBolsa
                esParameters esParams = new esParameters();
                esParams.Add("idPosicao", posicaoTermoBolsaHistorico.IdPosicao.Value);
                esParams.Add("IdAgente", posicaoTermoBolsaHistorico.IdAgente.Value);
                esParams.Add("IdCliente", posicaoTermoBolsaHistorico.IdCliente.Value);

                // Parametros que podem ser null
                if (!posicaoTermoBolsaHistorico.IdOperacao.HasValue)
                {
                    esParams.Add("IdOperacao", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdOperacao", posicaoTermoBolsaHistorico.IdOperacao);
                }

                if (!posicaoTermoBolsaHistorico.IdIndice.HasValue)
                {
                    esParams.Add("IdIndice", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdIndice", posicaoTermoBolsaHistorico.IdIndice);
                }
                //
                esParams.Add("CdAtivoBolsa", posicaoTermoBolsaHistorico.CdAtivoBolsa);
                esParams.Add("DataOperacao", posicaoTermoBolsaHistorico.DataOperacao.Value);
                esParams.Add("DataVencimento", posicaoTermoBolsaHistorico.DataVencimento.Value);
                esParams.Add("Quantidade", posicaoTermoBolsaHistorico.Quantidade.Value);
                esParams.Add("QuantidadeInicial", posicaoTermoBolsaHistorico.QuantidadeInicial.Value);
                esParams.Add("PrazoTotal", posicaoTermoBolsaHistorico.PrazoTotal.Value);
                esParams.Add("PrazoDecorrer", posicaoTermoBolsaHistorico.PrazoDecorrer.Value);
                esParams.Add("TaxaAno", posicaoTermoBolsaHistorico.TaxaAno.Value);
                esParams.Add("TaxaMTM", posicaoTermoBolsaHistorico.TaxaMTM.Value);
                esParams.Add("ValorTermo", posicaoTermoBolsaHistorico.ValorTermo.Value);
                esParams.Add("ValorTermoLiquido", posicaoTermoBolsaHistorico.ValorTermoLiquido.Value);
                esParams.Add("ValorMercado", posicaoTermoBolsaHistorico.ValorMercado.Value);
                esParams.Add("ValorCurva", posicaoTermoBolsaHistorico.ValorCurva.Value);
                esParams.Add("ValorCorrigido", posicaoTermoBolsaHistorico.ValorCorrigido.Value);
                esParams.Add("PUCustoLiquidoAcao", posicaoTermoBolsaHistorico.PUCustoLiquidoAcao.Value);
                esParams.Add("PUTermo", posicaoTermoBolsaHistorico.PUTermo.Value);
                esParams.Add("PUTermoLiquido", posicaoTermoBolsaHistorico.PUTermoLiquido.Value);
                esParams.Add("PUMercado", posicaoTermoBolsaHistorico.PUMercado.Value);
                esParams.Add("NumeroContrato", posicaoTermoBolsaHistorico.NumeroContrato);

                if (!posicaoTermoBolsaHistorico.IdTrader.HasValue)
                {
                    esParams.Add("IdTrader", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdTrader", posicaoTermoBolsaHistorico.IdTrader);
                }

                esParams.Add("RendimentoApropriado", posicaoTermoBolsaHistorico.RendimentoApropriado);
                esParams.Add("RendimentoApropriar", posicaoTermoBolsaHistorico.RendimentoApropriar);
                esParams.Add("RendimentoDia", posicaoTermoBolsaHistorico.RendimentoDia);
                esParams.Add("RendimentoTotal", posicaoTermoBolsaHistorico.RendimentoTotal);
                esParams.Add("TipoTermo", posicaoTermoBolsaHistorico.TipoTermo);

                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoTermoBolsa ON ");
                //
                sqlBuilder.Append("INSERT INTO PosicaoTermoBolsa (");
                sqlBuilder.Append(PosicaoTermoBolsaMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdIndice);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.DataOperacao);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.Quantidade);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.QuantidadeInicial);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PrazoTotal);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PrazoDecorrer);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.TaxaAno);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.TaxaMTM);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorTermo);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorTermoLiquido);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorMercado);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorCurva);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorCorrigido);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PUCustoLiquidoAcao);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PUTermo);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PUTermoLiquido);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PUMercado);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdTrader);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriado);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriar);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoDia);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoTotal);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.TipoTermo);

                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdIndice);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.DataOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.Quantidade);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.QuantidadeInicial);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PrazoTotal);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PrazoDecorrer);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.TaxaAno);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.TaxaMTM);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorTermo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorTermoLiquido);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorMercado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorCurva);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorCorrigido);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PUCustoLiquidoAcao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PUTermo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PUTermoLiquido);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PUMercado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdTrader);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriar);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoDia);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoTotal);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.TipoTermo);
                sqlBuilder.Append(")");

                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoTermoBolsa OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                #endregion
            }

            this.Save();
        }

        /// <summary>
        /// Método de inserção em PosicaoTermoBolsa a partir de PosicaoTermoBolsaAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InserePosicaoTermoBolsaAbertura(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            PosicaoTermoBolsaAberturaCollection posicaoTermoBolsaAberturaCollection = new PosicaoTermoBolsaAberturaCollection();
            posicaoTermoBolsaAberturaCollection.BuscaPosicaoTermoBolsaAberturaCompleta(idCliente, dataHistorico);
            for (int i = 0; i < posicaoTermoBolsaAberturaCollection.Count; i++)
            {
                PosicaoTermoBolsaAbertura posicaoTermoBolsaAbertura = posicaoTermoBolsaAberturaCollection[i];

                #region Copia de posicaoTermoBolsaAbertura para posicaoTermoBolsa
                esParameters esParams = new esParameters();
                esParams.Add("idPosicao", posicaoTermoBolsaAbertura.IdPosicao.Value);
                esParams.Add("IdAgente", posicaoTermoBolsaAbertura.IdAgente.Value);
                esParams.Add("IdCliente", posicaoTermoBolsaAbertura.IdCliente.Value);

                // Parametros que podem ser null
                if (!posicaoTermoBolsaAbertura.IdOperacao.HasValue)
                {
                    esParams.Add("IdOperacao", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdOperacao", posicaoTermoBolsaAbertura.IdOperacao);
                }

                if (!posicaoTermoBolsaAbertura.IdIndice.HasValue)
                {
                    esParams.Add("IdIndice", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdIndice", posicaoTermoBolsaAbertura.IdIndice);
                }
                //
                esParams.Add("CdAtivoBolsa", posicaoTermoBolsaAbertura.CdAtivoBolsa);
                esParams.Add("DataOperacao", posicaoTermoBolsaAbertura.DataOperacao.Value);
                esParams.Add("DataVencimento", posicaoTermoBolsaAbertura.DataVencimento.Value);
                esParams.Add("Quantidade", posicaoTermoBolsaAbertura.Quantidade.Value);
                esParams.Add("QuantidadeInicial", posicaoTermoBolsaAbertura.QuantidadeInicial.Value);
                esParams.Add("PrazoTotal", posicaoTermoBolsaAbertura.PrazoTotal.Value);
                esParams.Add("PrazoDecorrer", posicaoTermoBolsaAbertura.PrazoDecorrer.Value);
                esParams.Add("TaxaAno", posicaoTermoBolsaAbertura.TaxaAno.Value);
                esParams.Add("TaxaMTM", posicaoTermoBolsaAbertura.TaxaMTM.Value);
                esParams.Add("ValorTermo", posicaoTermoBolsaAbertura.ValorTermo.Value);
                esParams.Add("ValorTermoLiquido", posicaoTermoBolsaAbertura.ValorTermoLiquido.Value);
                esParams.Add("ValorMercado", posicaoTermoBolsaAbertura.ValorMercado.Value);
                esParams.Add("ValorCurva", posicaoTermoBolsaAbertura.ValorCurva.Value);
                esParams.Add("ValorCorrigido", posicaoTermoBolsaAbertura.ValorCorrigido.Value);
                esParams.Add("PUCustoLiquidoAcao", posicaoTermoBolsaAbertura.PUCustoLiquidoAcao.Value);
                esParams.Add("PUTermo", posicaoTermoBolsaAbertura.PUTermo.Value);
                esParams.Add("PUTermoLiquido", posicaoTermoBolsaAbertura.PUTermoLiquido.Value);
                esParams.Add("PUMercado", posicaoTermoBolsaAbertura.PUMercado.Value);
                esParams.Add("NumeroContrato", posicaoTermoBolsaAbertura.NumeroContrato);

                if (!posicaoTermoBolsaAbertura.IdTrader.HasValue)
                {
                    esParams.Add("IdTrader", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdTrader", posicaoTermoBolsaAbertura.IdTrader);
                }

                esParams.Add("RendimentoApropriado", posicaoTermoBolsaAbertura.RendimentoApropriado);
                esParams.Add("RendimentoApropriar", posicaoTermoBolsaAbertura.RendimentoApropriar);
                esParams.Add("RendimentoDia", posicaoTermoBolsaAbertura.RendimentoDia);
                esParams.Add("RendimentoTotal", posicaoTermoBolsaAbertura.RendimentoTotal);
                esParams.Add("TipoTermo", posicaoTermoBolsaAbertura.TipoTermo);

                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoTermoBolsa ON ");
                //
                sqlBuilder.Append("INSERT INTO PosicaoTermoBolsa (");
                sqlBuilder.Append(PosicaoTermoBolsaMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdIndice);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.DataOperacao);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.Quantidade);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.QuantidadeInicial);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PrazoTotal);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PrazoDecorrer);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.TaxaAno);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.TaxaMTM);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorTermo);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorTermoLiquido);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorMercado);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorCurva);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.ValorCorrigido);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PUCustoLiquidoAcao);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PUTermo);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PUTermoLiquido);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.PUMercado);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.IdTrader);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriado);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriar);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoDia);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoTotal);
                sqlBuilder.Append("," + PosicaoTermoBolsaMetadata.ColumnNames.TipoTermo);

                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdIndice);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.DataOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.Quantidade);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.QuantidadeInicial);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PrazoTotal);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PrazoDecorrer);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.TaxaAno);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.TaxaMTM);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorTermo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorTermoLiquido);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorMercado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorCurva);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.ValorCorrigido);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PUCustoLiquidoAcao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PUTermo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PUTermoLiquido);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.PUMercado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.IdTrader);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriar);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoDia);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.RendimentoTotal);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoTermoBolsaMetadata.ColumnNames.TipoTermo);
                sqlBuilder.Append(")");

                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoTermoBolsa OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                #endregion
            }

            this.Save();
        }

        /// <summary>
        /// Carrega o objeto PosicaoTermoBolsaCollection com os campos IdPosicao, IdOperacao, CdAtivoBolsa, 
        /// IdAgente, Quantidade, PUTermo, PUTermoLiquido, ValorTermo, NumeroContrato.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="dataVencimento"></param>
        public void BuscaPosicaoTermoBolsa(int idCliente, DateTime dataProximo, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.IdOperacao, this.Query.CdAtivoBolsa, this.Query.IdAgente,
                         this.Query.Quantidade, this.Query.PUTermo, this.Query.PUTermoLiquido,
                         this.Query.ValorTermo, this.Query.NumeroContrato)
                 .Where(this.Query.IdCliente == idCliente,                        
                        this.Query.DataVencimento.GreaterThanOrEqual(data),
                        this.Query.DataVencimento.LessThan(dataProximo));

            this.Query.Load();            
        }

        /// <summary>
        /// Carrega o objeto PosicaoTermoBolsaCollection com os campos CdAtivoBolsa, Quantidade.Sum, PUMercado.Avg, PUTermoLiquido.Avg, 
        /// ValorMercado.Sum.
        /// Group By CdAtivoBolsa
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoTermoBolsaAgrupado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa,
                         this.Query.Quantidade.Sum(),
                         this.Query.PUMercado.Avg(),
                         this.Query.PUTermoLiquido.Avg(),
                         this.Query.ValorMercado.Sum(),
                         this.Query.ValorTermoLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente)
                 .GroupBy(this.Query.CdAtivoBolsa);

            bool retorno = this.Query.Load();

            return retorno;
        }
    }
}
