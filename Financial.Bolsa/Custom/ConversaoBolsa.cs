﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Common.Enums;
using Financial.Interfaces.Import.Bolsa;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Investidor.Enums;

namespace Financial.Bolsa {
    public partial class ConversaoBolsa : esConversaoBolsa {

        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        #region Adiciona CodigoIsin
        protected override List<esPropertyDescriptor> GetLocalBindingProperties() {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
            props.Add(new esPropertyDescriptor(this, "CodigoIsin", typeof(string)));

            return props;
        }

        private string codigoIsin = "";
        public string CodigoIsin {
            get {                
                return this.codigoIsin;
            }
            set {
                this.codigoIsin = value;
            }
        }
        #endregion

        /// <summary>
        /// Distribui conversões lançadas na data para mercado a vista, em conversões de termo, opção.
        /// </summary>
        /// <param name="data"></param>
        public void DistribuiConversao(DateTime data, ConversaoBolsa conversaoBolsa) {
            AtivoBolsa ativoBolsaDestino = new AtivoBolsa();
            ativoBolsaDestino.LoadByPrimaryKey(conversaoBolsa.CdAtivoBolsaDestino);
            string especificacao = ativoBolsaDestino.Especificacao;
            int idEmissor = ativoBolsaDestino.IdEmissor.Value;
            string codigoIsin = ativoBolsaDestino.CodigoIsin;
            string tipoMercado = ativoBolsaDestino.TipoMercado;

            if (tipoMercado == TipoMercadoBolsa.MercadoVista) {
                #region Deleta todos os eventos de conversão de ativos que tenham o ativo principal da conversão como ativo objeto
                ConversaoBolsaQuery conversaoBolsaQuery = new ConversaoBolsaQuery("C");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
                conversaoBolsaQuery.Select(conversaoBolsaQuery.IdConversao);
                conversaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == conversaoBolsaQuery.CdAtivoBolsa);
                conversaoBolsaQuery.Where(ativoBolsaQuery.CdAtivoBolsaObjeto.Equal(conversaoBolsa.CdAtivoBolsa));
                conversaoBolsaQuery.Where(conversaoBolsaQuery.DataLancamento.Equal(conversaoBolsa.DataLancamento.Value));

                ConversaoBolsaCollection conversaoBolsaCollectionDeletar = new ConversaoBolsaCollection();
                conversaoBolsaCollectionDeletar.Load(conversaoBolsaQuery);
                conversaoBolsaCollectionDeletar.MarkAllAsDeleted();
                conversaoBolsaCollectionDeletar.Save();
                #endregion

                AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                if (ativoBolsaCollection.BuscaAtivoBolsa(conversaoBolsa.CdAtivoBolsa, data)) {
                    for (int j = 0; j < ativoBolsaCollection.Count; j++) {
                        string cdAtivoBolsa = ativoBolsaCollection[j].CdAtivoBolsa;

                        string cdAtivoBolsaDestino;
                        DateTime? davaVencimentoTermo = null;
                        if (ativoBolsaCollection[j].TipoMercado == TipoMercadoBolsa.Termo) {
                            cdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino + Utilitario.Right(cdAtivoBolsa, 9);

                            davaVencimentoTermo = AtivoBolsa.RetornaDataVencimentoTermo(cdAtivoBolsaDestino);
                        }
                        else {
                            cdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino.Substring(0, 4) + Utilitario.Right(cdAtivoBolsa, 3);
                        }

                        #region Verifica se cdAtivoBolsaDestino existe, e cadastra se nao existir
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsaDestino)) {
                            AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                            ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                            ativoBolsaNovo.CdAtivoBolsaObjeto = conversaoBolsa.CdAtivoBolsaDestino;
                            ativoBolsaNovo.CodigoIsin = codigoIsin;

                            if (ativoBolsaCollection[j].TipoMercado == TipoMercadoBolsa.Termo)
                            {
                                ativoBolsaNovo.DataVencimento = davaVencimentoTermo.Value;
                            }
                            else
                            {
                                ativoBolsaNovo.DataVencimento = ativoBolsaCollection[j].DataVencimento;
                            }

                            ativoBolsaNovo.Descricao = ativoBolsaDestino.Descricao;
                            ativoBolsaNovo.Especificacao = especificacao;
                            ativoBolsaNovo.IdEmissor = idEmissor;
                            ativoBolsaNovo.IdMoeda = 1;
                            ativoBolsaNovo.PUExercicio = ativoBolsaCollection[j].PUExercicio;
                            ativoBolsaNovo.TipoMercado = ativoBolsaCollection[j].TipoMercado;
                            ativoBolsaNovo.Save();

                            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                            if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(ativoBolsaDestino.CdAtivoBolsa, data)) {
                                FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                                fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                                fatorCotacaoBolsaNovo.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                fatorCotacaoBolsaNovo.Fator = fatorCotacaoBolsa.Fator.Value;
                                fatorCotacaoBolsaNovo.Save();
                            }
                        }
                        #endregion

                        #region Cria nova Conversao
                        ConversaoBolsa novaConversaoBolsa = new ConversaoBolsa();
                        novaConversaoBolsa.DataLancamento = conversaoBolsa.DataLancamento;
                        novaConversaoBolsa.DataEx = conversaoBolsa.DataEx;
                        novaConversaoBolsa.DataReferencia = conversaoBolsa.DataReferencia;
                        novaConversaoBolsa.FatorQuantidade = conversaoBolsa.FatorQuantidade;
                        novaConversaoBolsa.FatorPU = conversaoBolsa.FatorPU;
                        novaConversaoBolsa.Fonte = conversaoBolsa.Fonte;
                        novaConversaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        novaConversaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;

                        novaConversaoBolsa.InsereConversaoBolsa(novaConversaoBolsa);
                        #endregion
                    }
                }
            }

        }

        /// <summary>
        /// Método de inserção da Conversao.
        /// </summary>
        /// <param name="conversaoBolsa">Conversao com os Dados a Inserir</param>  
        /// <exception cref="ArgumentNullException">
        /// throws ArgumentNullException se campos Obrigatórios da inclusão estiverem nulos
        /// conversaoBolsa.DataLancamento ou
        /// conversaoBolsa.DataEx ou conversaoBolsa.DataReferencia        
        /// conversaoBolsa.FatorQuantidade ou conversaoBolsa.FatorPU
        /// conversaoBolsa.CdAtivoBolsa ou conversaoBolsa.CdAtivoBolsaDestino
        /// conversaoBolsa.Fonte         
        ///</exception>                   
        public void InsereConversaoBolsa(ConversaoBolsa conversaoBolsa) {
            #region ArgumentosNulos - throw ArgumentNullException
            if (!conversaoBolsa.DataLancamento.HasValue ||
               !conversaoBolsa.DataEx.HasValue ||
               !conversaoBolsa.DataReferencia.HasValue ||
               !conversaoBolsa.FatorQuantidade.HasValue ||
               !conversaoBolsa.FatorPU.HasValue ||
               String.IsNullOrEmpty(conversaoBolsa.CdAtivoBolsa) ||
               String.IsNullOrEmpty(conversaoBolsa.CdAtivoBolsaDestino) ||
               !conversaoBolsa.Fonte.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("conversaoBolsa.DataLancamento ou ")
                .Append("conversaoBolsa.DataEx ou ")
                .Append("conversaoBolsa.DataReferencia ou ")
                .Append("conversaoBolsa.FatorQuantidade ou ")
                .Append("conversaoBolsa.FatorPU ou ")
                .Append("conversaoBolsa.CdAtivoBolsa ou ")
                .Append("conversaoBolsa.CdAtivoBolsaDestino ou ")
                .Append("conversaoBolsa.Fonte ");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            ConversaoBolsa conversaoBolsaInserir = new ConversaoBolsa();
            //            
            // Campos Obrigatórios para a inserção
            conversaoBolsaInserir.DataLancamento = conversaoBolsa.DataLancamento;
            conversaoBolsaInserir.DataEx = conversaoBolsa.DataEx;
            conversaoBolsaInserir.DataReferencia = conversaoBolsa.DataReferencia;
            conversaoBolsaInserir.FatorQuantidade = conversaoBolsa.FatorQuantidade;
            conversaoBolsaInserir.FatorPU = conversaoBolsa.FatorPU;
            conversaoBolsaInserir.Fonte = conversaoBolsa.Fonte;
            conversaoBolsaInserir.CdAtivoBolsa = conversaoBolsa.CdAtivoBolsa;
            conversaoBolsaInserir.CdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino;
            //    
            conversaoBolsaInserir.Save();
        }

        /// <summary>
        /// Carrega ConversaoBolsa a partir da estrutura importada do arquivo PROD.
        /// </summary>
        /// <param name="prodCollection"></param>
        /// <param name="data"></param>
        /// <exception cref="AtivoNaoCadastradoException">Se cdAtivoBolsa não existir
        /// <exception cref="CodigoIsinNaoCadastradoException">Se CodigoIsin do Ativo não Existir
        public void CarregaProdConversao(ProdCollection prodCollection, DateTime data, bool geraException) 
        {
            // Pega os Proventos do Arquivo de Prod                          
            Prod prod = new Prod();
            ProdCollection prodCollectionSubList = new ProdCollection();

            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoConversao);

            //Deleta os eventos de conversão gerados a partir do arquivo PROD na data
            ConversaoBolsaCollection conversaoBolsaCollectionDeletar = new ConversaoBolsaCollection();
            conversaoBolsaCollectionDeletar.DeletaConversao(data, (int)FonteProventoBolsa.ArquivoPROD);

            ConversaoBolsaCollection conversaoBolsaCollection = new ConversaoBolsaCollection();
            //
            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++) 
            {
                prod = prodCollectionSubList.CollectionProd[i];

                #region Dados Prod
                DateTime dataEx = prod.DataEx.Value;
                DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                decimal fatorQuantidade = prod.ValorProvento.Value / 100;
                decimal fatorPu = 1 / fatorQuantidade;
                string codigoIsinDestino = prod.CodigoIsinDestino;
                #endregion

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (ativoBolsa.BuscaCdAtivoBolsa(codigoIsinDestino)) //Se não tem codigo Isin cadastrado, não importa!
                {
                    string cdAtivoBolsaDestino = ativoBolsa.CdAtivoBolsa;

                    AtivoBolsa ativoBolsaAux = new AtivoBolsa();
                    string cdAtivoBolsa = prod.CdAtivoBolsa;
                    // Se tamanho = 4, CodigoAtivo recebe o numero da especificacao
                    if (cdAtivoBolsa.Length == 4) {
                        // tres primeiras letras de especificacao ou duas primeiras letras se a ultima for em branco
                        string especificacao = prod.Especificacao;
                        if (especificacao.Length >= 3)
                            especificacao = especificacao.Substring(0, 3).Trim();
                        else {
                            especificacao = especificacao.Substring(0, 2).Trim();
                        }
                        string numeroEspecificacao = prod.ConverteEspecificacao(especificacao);
                        if (!numeroEspecificacao.Equals(EspecificacaoAtivo.EspecificacaoInvalida)) {
                            cdAtivoBolsa = cdAtivoBolsa + numeroEspecificacao;
                        }

                        // Depois de colocar a especificação verifica se existe o Ativo                    
                        ativoBolsaAux = new AtivoBolsa();
                        if (!ativoBolsaAux.LoadByPrimaryKey(cdAtivoBolsa)) {
                            // Se não existe o Ativo verifica se existe o ativo no SOMA
                            string cdAtivoSoma = cdAtivoBolsa + "B";
                            ativoBolsaAux = new AtivoBolsa();
                            if (!ativoBolsaAux.LoadByPrimaryKey(cdAtivoSoma)) 
                            {
                                AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                                ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                                ativoBolsaNovo.CodigoIsin = ""; //Não tem como buscar
                                ativoBolsaNovo.Descricao = cdAtivoBolsa;
                                ativoBolsaNovo.Especificacao = "*";
                                ativoBolsaNovo.IdEmissor = 1; //Assume qq emissor!
                                ativoBolsaNovo.IdMoeda = 1; //Assume qq moeda!
                                ativoBolsaNovo.PUExercicio = 0;
                                ativoBolsaNovo.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                ativoBolsaNovo.TipoPapel = (byte)TipoPapelAtivo.Normal;
                                ativoBolsaNovo.NumeroSerie = 0;
                                ativoBolsaNovo.Save();

                                FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                                fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                                fatorCotacaoBolsaNovo.DataReferencia = data;
                                fatorCotacaoBolsaNovo.Fator = 1M; //Assume 1!
                                fatorCotacaoBolsaNovo.Save();
                            }
                            else {
                                // Existe Ativo no Soma
                                cdAtivoBolsa = cdAtivoSoma;
                            }
                        }
                    }
                    else {
                        ativoBolsaAux = new AtivoBolsa();
                        if (!ativoBolsaAux.LoadByPrimaryKey(cdAtivoBolsa)) 
                        {
                            if (geraException)
                            {
                                throw new AtivoNaoCadastradoException("Ativo não cadastrado: " + cdAtivoBolsa);
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }

                    #region Cria Conversao
                    ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
                    conversaoBolsa.DataLancamento = data;
                    conversaoBolsa.DataEx = dataEx;
                    conversaoBolsa.DataReferencia = dataReferencia;
                    conversaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    conversaoBolsa.FatorQuantidade = fatorQuantidade;
                    conversaoBolsa.FatorPU = fatorPu;
                    conversaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;
                    conversaoBolsa.Fonte = (byte)FonteProventoBolsa.ArquivoPROD;
                    #endregion

                    conversaoBolsaCollection.AttachEntity(conversaoBolsa);
                }
                else 
                {
                    if (geraException)
                    {
                        throw new CodigoIsinNaoCadastradoException("Código Isin: " + codigoIsinDestino + " para o Ativo não cadastrado");
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            conversaoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega ConversaoBolsa a partir da estrutura importada do arquivo PROD.
        /// Não considera foreign Key Ativobolsa
        /// Importa mesmo se não existir AtivoBolsa
        /// </summary>
        /// <param name="prodCollection"></param>
        /// <param name="data"></param>
        /// <exception cref="Exception">Se houve Erro na Inserção</exception>
        public void CarregaProdConversaoSemFK(ProdCollection prodCollection, DateTime data) {
            // Pega os Proventos do Arquivo de Prod
            Prod prod = new Prod();
            ProdCollection prodCollectionSubList = new ProdCollection();

            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoConversao);

            //Deleta os eventos de conversão gerados a partir do arquivo PROD na data
            ConversaoBolsaCollection conversaoBolsaCollectionDeletar = new ConversaoBolsaCollection();
            conversaoBolsaCollectionDeletar.DeletaConversao(data, (int)FonteProventoBolsa.ArquivoPROD);

            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++) {
                prod = prodCollectionSubList.CollectionProd[i];

                #region Dados Prod
                DateTime dataEx = prod.DataEx.Value;
                DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                decimal fatorQuantidade = prod.ValorProvento.Value / 100;
                decimal fatorPu = 1 / fatorQuantidade;
                string codigoIsinDestino = prod.CodigoIsinDestino;
                string cdAtivoBolsa = prod.CdAtivoBolsa;
                #endregion

                #region Tratamento AtivoBolsa
                // Se tamanho = 4, CodigoAtivo recebe o numero da especificacao
                if (cdAtivoBolsa.Length == 4) {
                    #region Se tamanho do codigo ativo é 4

                    // tres primeiras letras de especificacao ou duas primeiras letras se a ultima for em branco
                    string especificacao = prod.Especificacao;
                    if (especificacao.Length >= 3)
                        especificacao = especificacao.Substring(0, 3).Trim();
                    else {
                        especificacao = especificacao.Substring(0, 2).Trim();
                    }
                    string numeroEspecificacao = prod.ConverteEspecificacao(especificacao);
                    if (!numeroEspecificacao.Equals(EspecificacaoAtivo.EspecificacaoInvalida)) {
                        cdAtivoBolsa = cdAtivoBolsa + numeroEspecificacao;
                    }
                    #endregion
                }
                #endregion

                string codigoIsin = null; // Inserido na Tabela Conversão se não existir AtivoBolsa com o codigoIsin
                string cdAtivoBolsaDestino = " ";

                #region Tratamento AtivoBolsa
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (ativoBolsa.BuscaCdAtivoBolsa(codigoIsinDestino)) { //Se tem codigo Isin cadastrado
                    cdAtivoBolsaDestino = ativoBolsa.CdAtivoBolsa;                                                            
                }
                else {
                    codigoIsin = prod.CodigoIsinDestino;
                } 
                #endregion

                #region Cria Conversao
                ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
                conversaoBolsa.DataLancamento = data;
                conversaoBolsa.DataEx = dataEx;
                conversaoBolsa.DataReferencia = dataReferencia;
                conversaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                conversaoBolsa.FatorQuantidade = fatorQuantidade;
                conversaoBolsa.FatorPU = fatorPu;
                conversaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;
                conversaoBolsa.Fonte = (byte)FonteProventoBolsa.ArquivoPROD;
                #endregion

                // Insere CotaçãoBolsa
                // Se cdAtivoBolsaDestino = " ", CodigoIsin estará preenchido
                try {
                    this.InsereConversaoComCodigoIsin(conversaoBolsa, codigoIsin);
                }
                catch (Exception e) {
                    throw new Exception("Erro Inserção: "+e.Message);
                }
            }
        }

        /// <summary>
        /// Inserção especial onde a tabela Conversao possui o campo extra CodigoIsin
        /// Usado para carregar o arquivo Prod sem dependência da Tabela AtivoBolsa
        /// </summary>
        private void InsereConversaoComCodigoIsin(ConversaoBolsa conversaoBolsa, string codigoIsin) {

            string[] campos = new string[] {
                ConversaoBolsaMetadata.ColumnNames.DataLancamento,
                ConversaoBolsaMetadata.ColumnNames.DataEx,
                ConversaoBolsaMetadata.ColumnNames.DataReferencia,
                ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsa,
                ConversaoBolsaMetadata.ColumnNames.FatorQuantidade,
                ConversaoBolsaMetadata.ColumnNames.FatorPU,
                ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino,
                ConversaoBolsaMetadata.ColumnNames.Fonte,
                "CodigoIsin"
            };

            string camposConcat = String.Join(", ", campos);
            string camposParams = String.Join(",@", campos);

            #region esParameters
            esParameters esParams = new esParameters();
            esParams.Add(campos[0], conversaoBolsa.DataLancamento.Value);
            esParams.Add(campos[1], conversaoBolsa.DataEx.Value);
            esParams.Add(campos[2], conversaoBolsa.DataReferencia.Value);
            esParams.Add(campos[3], conversaoBolsa.CdAtivoBolsa);
            esParams.Add(campos[4], conversaoBolsa.FatorQuantidade.Value);
            esParams.Add(campos[5], conversaoBolsa.FatorPU.Value);
            esParams.Add(campos[6], conversaoBolsa.CdAtivoBolsaDestino);
            esParams.Add(campos[7], conversaoBolsa.Fonte.Value);

            // Parametros que podem ser null
            if (String.IsNullOrEmpty(codigoIsin)) {
                esParams.Add("CodigoIsin", DBNull.Value);
            }
            else {
                esParams.Add("CodigoIsin", codigoIsin);
            }
            #endregion

            StringBuilder sqlBuilder = new StringBuilder();
            //
            sqlBuilder.Append("INSERT INTO ConversaoBolsa (");
            sqlBuilder.Append(camposConcat);
            sqlBuilder.Append(") VALUES ( ");
            sqlBuilder.Append("@" + camposParams);
            sqlBuilder.Append(")");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
        }

        /// <summary>
        /// Processa Atualizações de PosicaoBolsa a partir de Conversões Efetivadas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaConversao(int idCliente, DateTime data) 
        {
            ConversaoBolsaCollection conversaoBolsaCollection = new ConversaoBolsaCollection();
            conversaoBolsaCollection = conversaoBolsaCollection.BuscaConversaoBolsa(data, idCliente, true, false);

            #region Deleta operações com origem = ResultadoConversao na OperacaoBolsa
            List<int> origemOperacao = new List<int>();
            origemOperacao.Add(OrigemOperacaoBolsa.ResultadoConversao);

            OperacaoBolsaCollection operacaoBolsaCollectionDeletar = new OperacaoBolsaCollection();
            operacaoBolsaCollectionDeletar.DeletaOperacaoBolsa(idCliente, origemOperacao, data);
            #endregion

            // Guarda a Situação Anterior da Posicao ou Null Se não Existir Posicao
            PosicaoBolsaCollection situacaoProvisoriaPosicoes = null;

            #region Para cada Conversão
            for (int i = 0; i < conversaoBolsaCollection.Count; i++) {
                #region Dados de Conversão
                ConversaoBolsa conversaoBolsa = conversaoBolsaCollection[i];
                string cdAtivoBolsa = conversaoBolsa.CdAtivoBolsa;
                string cdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino;
                decimal fatorQuantidade = conversaoBolsa.FatorQuantidade.Value;
                decimal fatorPu = conversaoBolsa.FatorPU.Value;
                #endregion

                // Para Segundo Elemento em Diante
                if (i >= 1) {
                    #region Segundo Elemento em Diante
                    string cdAtivoConversaoAtual = conversaoBolsaCollection[i].CdAtivoBolsa;
                    string cdAtivoConversaoAnterior = conversaoBolsaCollection[i-1].CdAtivoBolsa;                    

                    // Identificado Conversão em Multiplos Passos
                    if (cdAtivoConversaoAtual == cdAtivoConversaoAnterior && situacaoProvisoriaPosicoes!= null) {
                        // Retorna a Posicao Provisoria Anterior para Afetar em cima dela
                        #region Conversao Multipla
                        PosicaoBolsaCollection posicaoBolsaCollection = (PosicaoBolsaCollection)Utilitario.Clone(situacaoProvisoriaPosicoes);
                        
                        #region Para cada Posicao
                        for (int j = 0; j < posicaoBolsaCollection.Count; j++) {
                            PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[j];

                            #region Dados de PosicaoBolsa
                            decimal quantidade = posicaoBolsa.Quantidade.Value;
                            decimal quantidadeBloqueada = posicaoBolsa.QuantidadeBloqueada.Value;
                            decimal puCusto = posicaoBolsa.PUCusto.Value;
                            decimal puCustoLiquido = posicaoBolsa.PUCustoLiquido.Value;
                            int idAgente = posicaoBolsa.IdAgente.Value;
                            #endregion

                            #region Converte quantidade e PU
                            decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                            decimal quantidadeBloqueadaConvertida = Utilitario.Truncate(quantidadeBloqueada * fatorQuantidade, 0);
                            decimal puCustoConvertido = puCusto * fatorPu;
                            decimal puCustoLiquidoConvertido = puCustoLiquido * fatorPu;
                            #endregion

                            decimal quantidadeFinal = quantidadeConvertida;
                            decimal quantidadeBloqueadaFinal = quantidadeBloqueadaConvertida;
                            decimal puCustoFinal = puCustoConvertido;
                            decimal puCustoLiquidoFinal = puCustoLiquidoConvertido;

                            decimal resultadoConversao = 0;

                            #region Trata caso específico para quando já tem o ativo destino na carteira
                            if (cdAtivoBolsa != cdAtivoBolsaDestino) //Meio esquisito este teste, mas só para garantir!
                            {
                                PosicaoBolsa posicaoBolsaExistente = new PosicaoBolsa();
                                if (posicaoBolsaExistente.BuscaPosicaoBolsa(idCliente, idAgente, cdAtivoBolsaDestino))
                                {
                                    decimal quantidadeExistente = posicaoBolsaExistente.Quantidade.Value;
                                    decimal quantidadeBloqueadaExistente = posicaoBolsaExistente.QuantidadeBloqueada.Value;
                                    decimal puCustoExistente = posicaoBolsaExistente.PUCusto.Value;
                                    decimal puCustoLiquidoExistente = posicaoBolsaExistente.PUCustoLiquido.Value;

                                    posicaoBolsaExistente.MarkAsDeleted();
                                    posicaoBolsaExistente.Save();

                                    FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                                    if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsaDestino, data))
                                    {
                                        if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                                        {
                                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                                            if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsaDestino))
                                            {
                                                throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsaDestino + " " + data.ToString("d"));
                                            }
                                        }
                                    }

                                    quantidadeFinal = quantidadeConvertida + quantidadeExistente;
                                    quantidadeBloqueadaFinal = quantidadeBloqueadaConvertida + quantidadeBloqueadaExistente;

                                    if ((quantidadeConvertida > 0 && quantidadeExistente < 0) ||
                                    (quantidadeConvertida < 0 && quantidadeExistente > 0))
                                    {
                                        if (Math.Abs(quantidadeConvertida) > Math.Abs(quantidadeExistente))
                                        {
                                            puCustoFinal = puCustoConvertido;
                                            puCustoLiquidoFinal = puCustoLiquidoConvertido;

                                            decimal quantidadeResultado = Math.Abs(quantidadeExistente);
                                            if (quantidadeConvertida > 0)
                                            {
                                                resultadoConversao = Math.Round(quantidadeResultado * (puCustoConvertido - puCustoExistente) / fator.Fator.Value, 2);
                                            }
                                            else
                                            {
                                                resultadoConversao = Math.Round(quantidadeResultado * (puCustoExistente - puCustoConvertido) / fator.Fator.Value, 2);
                                            }
                                        }
                                        else
                                        {
                                            puCustoFinal = puCustoExistente;
                                            puCustoLiquidoFinal = puCustoLiquidoExistente;

                                            decimal quantidadeResultado = Math.Abs(quantidadeConvertida);
                                            if (quantidadeConvertida > 0)
                                            {
                                                resultadoConversao = Math.Round(quantidadeResultado * (puCustoConvertido - puCustoExistente) / fator.Fator.Value, 2);
                                            }
                                            else
                                            {
                                                resultadoConversao = Math.Round(quantidadeResultado * (puCustoExistente - puCustoConvertido) / fator.Fator.Value, 2);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        decimal custoConvertido = quantidadeConvertida * puCustoConvertido / fator.Fator.Value;
                                        decimal custoLiquidoConvertido = quantidadeConvertida * puCustoLiquidoConvertido / fator.Fator.Value;

                                        decimal custoOriginal = quantidadeExistente * puCustoExistente / fator.Fator.Value;
                                        decimal custoLiquidoOriginal = quantidadeExistente * puCustoLiquidoExistente / fator.Fator.Value;

                                        puCustoFinal = ((custoOriginal + custoConvertido) / quantidadeFinal) / fator.Fator.Value;
                                        puCustoLiquidoFinal = ((custoLiquidoOriginal + custoLiquidoConvertido) / quantidadeFinal) / fator.Fator.Value;
                                    }
                                }

                                #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado calculado)
                                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                                operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Retirada;
                                operacaoBolsa.Data = data;
                                operacaoBolsa.Pu = 0;
                                operacaoBolsa.Valor = 0;
                                operacaoBolsa.ValorLiquido = 0;
                                operacaoBolsa.PULiquido = 0;
                                operacaoBolsa.Corretagem = 0;
                                operacaoBolsa.Emolumento = 0;
                                operacaoBolsa.LiquidacaoCBLC = 0;
                                operacaoBolsa.RegistroBolsa = 0;
                                operacaoBolsa.RegistroCBLC = 0;                                    //
                                operacaoBolsa.Quantidade = quantidade;
                                operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                                operacaoBolsa.IdAgenteCorretora = idAgente;
                                operacaoBolsa.IdAgenteLiquidacao = idAgente;
                                operacaoBolsa.Origem = OrigemOperacaoBolsa.ResultadoConversao;
                                operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                                operacaoBolsa.DataLiquidacao = data;
                                operacaoBolsa.PercentualDesconto = 0;
                                operacaoBolsa.Desconto = 0;
                                operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                                operacaoBolsa.IdCliente = idCliente;
                                operacaoBolsa.CalculaDespesas = "N";
                                operacaoBolsa.ResultadoRealizado = resultadoConversao;
                                operacaoBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                                operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                                operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                                operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                                operacaoBolsa.DataOperacao = data;

                                operacaoBolsa.Save();
                                #endregion

                               
                                #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado = zero)
                                operacaoBolsa = new OperacaoBolsa();
                                operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito;
                                operacaoBolsa.Data = data;
                                operacaoBolsa.Pu = 0;
                                operacaoBolsa.Valor = 0;
                                operacaoBolsa.ValorLiquido = 0;
                                operacaoBolsa.PULiquido = 0;
                                operacaoBolsa.Corretagem = 0;
                                operacaoBolsa.Emolumento = 0;
                                operacaoBolsa.LiquidacaoCBLC = 0;
                                operacaoBolsa.RegistroBolsa = 0;
                                operacaoBolsa.RegistroCBLC = 0;                                    //
                                operacaoBolsa.Quantidade = quantidadeConvertida;
                                operacaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                                operacaoBolsa.IdAgenteCorretora = idAgente;
                                operacaoBolsa.IdAgenteLiquidacao = idAgente;
                                operacaoBolsa.Origem = OrigemOperacaoBolsa.ResultadoConversao;
                                operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                                operacaoBolsa.DataLiquidacao = data;
                                operacaoBolsa.PercentualDesconto = 0;
                                operacaoBolsa.Desconto = 0;
                                operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                                operacaoBolsa.IdCliente = idCliente;
                                operacaoBolsa.CalculaDespesas = "N";
                                operacaoBolsa.ResultadoRealizado = 0;
                                operacaoBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                                operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                                operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                                operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                                operacaoBolsa.DataOperacao = data;

                                operacaoBolsa.Save();
                                #endregion
                            }
                            else
                            {
                                //Mesmo ativo. Faz deposito pelo fatorPU remanescente.
                                #region deposita fator residual
                                if (fatorPu > 0 && fatorPu < 1)
                                {
                                    decimal quantidadeConvertidaDep = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                                    decimal quantidadeBloqueadaConvertidaDep = Utilitario.Truncate(quantidadeBloqueada * fatorQuantidade, 0);
                                    decimal puCustoConvertidoDep = puCusto * fatorPu;
                                    decimal puCustoLiquidoConvertidoDep = puCustoLiquido * fatorPu;

                                    #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado calculado)
                                    OperacaoBolsa operacaoBolsaDep = new OperacaoBolsa();
                                    operacaoBolsaDep.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                    operacaoBolsaDep.TipoOperacao = TipoOperacaoBolsa.Deposito;
                                    operacaoBolsaDep.Data = data;
                                    operacaoBolsaDep.Pu = puCustoConvertidoDep;
                                    operacaoBolsaDep.Valor = 0;
                                    operacaoBolsaDep.ValorLiquido = 0;
                                    operacaoBolsaDep.PULiquido = puCustoLiquidoConvertidoDep;
                                    operacaoBolsaDep.Corretagem = 0;
                                    operacaoBolsaDep.Emolumento = 0;
                                    operacaoBolsaDep.LiquidacaoCBLC = 0;
                                    operacaoBolsaDep.RegistroBolsa = 0;
                                    operacaoBolsaDep.RegistroCBLC = 0;                                    //
                                    operacaoBolsaDep.Quantidade = quantidadeConvertidaDep;
                                    operacaoBolsaDep.CdAtivoBolsa = cdAtivoBolsa;
                                    operacaoBolsaDep.IdAgenteCorretora = idAgente;
                                    operacaoBolsaDep.IdAgenteLiquidacao = idAgente;
                                    operacaoBolsaDep.Origem = OrigemOperacaoBolsa.ResultadoConversao;
                                    operacaoBolsaDep.Fonte = (byte)FonteOperacaoBolsa.Interno;
                                    operacaoBolsaDep.DataLiquidacao = data;
                                    operacaoBolsaDep.PercentualDesconto = 0;
                                    operacaoBolsaDep.Desconto = 0;
                                    operacaoBolsaDep.IdLocal = LocalFeriadoFixo.Bovespa;
                                    operacaoBolsaDep.IdCliente = idCliente;
                                    operacaoBolsaDep.CalculaDespesas = "N";
                                    operacaoBolsaDep.ResultadoRealizado = resultadoConversao;
                                    operacaoBolsaDep.IdMoeda = (byte)ListaMoedaFixo.Real;

                                    operacaoBolsaDep.Save();
                                    #endregion
                                }
                                #endregion


                            }

                            #endregion

                            #region Atualiza em PosicaoBolsa
                            posicaoBolsa.MarkAllColumnsAsDirty(DataRowState.Added);
                            //
                            posicaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            posicaoBolsa.Quantidade = quantidadeFinal;
                            posicaoBolsa.QuantidadeBloqueada = quantidadeBloqueadaFinal;
                            posicaoBolsa.PUCusto = puCustoFinal;
                            posicaoBolsa.PUCustoLiquido = puCustoLiquidoFinal;
                            #endregion
                        }
                        #endregion

                        // Atualiza Posicao                        
                        if (posicaoBolsaCollection.Count >= 1) {
                            posicaoBolsaCollection.Save();
                        }
                        #endregion
                    }
                    else {
                        #region Conversao Normal
                        PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                        posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente == idCliente,
                                                           posicaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);

                        if (posicaoBolsaCollection.Query.Load()) {
                            situacaoProvisoriaPosicoes = (PosicaoBolsaCollection)Utilitario.Clone(posicaoBolsaCollection);
                        }
                        else {
                            situacaoProvisoriaPosicoes = null;
                        }

                        #region Para cada Posicao
                        for (int j = 0; j < posicaoBolsaCollection.Count; j++) {
                            PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[j];

                            #region Dados de PosicaoBolsa
                            decimal quantidade = posicaoBolsa.Quantidade.Value;
                            decimal quantidadeBloqueada = posicaoBolsa.QuantidadeBloqueada.Value;
                            decimal puCusto = posicaoBolsa.PUCusto.Value;
                            decimal puCustoLiquido = posicaoBolsa.PUCustoLiquido.Value;
                            int idAgente = posicaoBolsa.IdAgente.Value;
                            #endregion

                            #region Converte quantidade e PU
                            decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                            decimal quantidadeBloqueadaConvertida = Utilitario.Truncate(quantidadeBloqueada * fatorQuantidade, 0);
                            decimal puCustoConvertido = puCusto * fatorPu;
                            decimal puCustoLiquidoConvertido = puCustoLiquido * fatorPu;
                            #endregion

                            decimal quantidadeFinal = quantidadeConvertida;
                            decimal quantidadeBloqueadaFinal = quantidadeBloqueadaConvertida;
                            decimal puCustoFinal = puCustoConvertido;
                            decimal puCustoLiquidoFinal = puCustoLiquidoConvertido;

                            decimal resultadoConversao = 0;

                            #region Trata caso específico para quando já tem o ativo destino na carteira
                            if (cdAtivoBolsa != cdAtivoBolsaDestino) //Meio esquisito este teste, mas só para garantir!
                            {
                                PosicaoBolsa posicaoBolsaExistente = new PosicaoBolsa();
                                if (posicaoBolsaExistente.BuscaPosicaoBolsa(idCliente, idAgente, cdAtivoBolsaDestino)) 
                                {
                                    decimal quantidadeExistente = posicaoBolsaExistente.Quantidade.Value;
                                    decimal quantidadeBloqueadaExistente = posicaoBolsaExistente.QuantidadeBloqueada.Value;
                                    decimal puCustoExistente = posicaoBolsaExistente.PUCusto.Value;
                                    decimal puCustoLiquidoExistente = posicaoBolsaExistente.PUCustoLiquido.Value;

                                    posicaoBolsaExistente.MarkAsDeleted();
                                    posicaoBolsaExistente.Save();

                                    FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                                    if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsaDestino, data)) 
                                    {
                                        if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                                        {
                                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                                            if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsaDestino))
                                            {
                                                throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsaDestino + " " + data.ToString("d"));
                                            }
                                        }
                                    }

                                    quantidadeFinal = quantidadeConvertida + quantidadeExistente;
                                    quantidadeBloqueadaFinal = quantidadeBloqueadaConvertida + quantidadeBloqueadaExistente;
                                    
                                    if ((quantidadeConvertida > 0 && quantidadeExistente < 0) ||
                                    (quantidadeConvertida < 0 && quantidadeExistente > 0))
                                    {
                                        if (Math.Abs(quantidadeConvertida) > Math.Abs(quantidadeExistente))
                                        {
                                            puCustoFinal = puCustoConvertido;
                                            puCustoLiquidoFinal = puCustoLiquidoConvertido;

                                            decimal quantidadeResultado = Math.Abs(quantidadeExistente);
                                            if (quantidadeConvertida > 0)
                                            {
                                                resultadoConversao = Math.Round(quantidadeResultado * (puCustoConvertido - puCustoExistente) / fator.Fator.Value, 2);
                                            }
                                            else
                                            {
                                                resultadoConversao = Math.Round(quantidadeResultado * (puCustoExistente - puCustoConvertido) / fator.Fator.Value, 2);
                                            }
                                        }
                                        else
                                        {
                                            puCustoFinal = puCustoExistente;
                                            puCustoLiquidoFinal = puCustoLiquidoExistente;

                                            decimal quantidadeResultado = Math.Abs(quantidadeConvertida);
                                            if (quantidadeConvertida > 0)
                                            {
                                                resultadoConversao = Math.Round(quantidadeResultado * (puCustoConvertido - puCustoExistente) / fator.Fator.Value, 2);
                                            }
                                            else
                                            {
                                                resultadoConversao = Math.Round(quantidadeResultado * (puCustoExistente - puCustoConvertido) / fator.Fator.Value, 2);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        decimal custoConvertido = quantidadeConvertida * puCustoConvertido / fator.Fator.Value;
                                        decimal custoLiquidoConvertido = quantidadeConvertida * puCustoLiquidoConvertido / fator.Fator.Value;

                                        decimal custoOriginal = quantidadeExistente * puCustoExistente / fator.Fator.Value;
                                        decimal custoLiquidoOriginal = quantidadeExistente * puCustoLiquidoExistente / fator.Fator.Value;

                                        puCustoFinal = ((custoOriginal + custoConvertido) / quantidadeFinal) / fator.Fator.Value;
                                        puCustoLiquidoFinal = ((custoLiquidoOriginal + custoLiquidoConvertido) / quantidadeFinal) / fator.Fator.Value;
                                    }
                                }

                                #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado calculado)
                                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                                operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Retirada;
                                operacaoBolsa.Data = data;
                                operacaoBolsa.Pu = 0;
                                operacaoBolsa.Valor = 0;
                                operacaoBolsa.ValorLiquido = 0;
                                operacaoBolsa.PULiquido = 0;
                                operacaoBolsa.Corretagem = 0;
                                operacaoBolsa.Emolumento = 0;
                                operacaoBolsa.LiquidacaoCBLC = 0;
                                operacaoBolsa.RegistroBolsa = 0;
                                operacaoBolsa.RegistroCBLC = 0;                                    //
                                operacaoBolsa.Quantidade = quantidade;
                                operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                                operacaoBolsa.IdAgenteCorretora = idAgente;
                                operacaoBolsa.IdAgenteLiquidacao = idAgente;
                                operacaoBolsa.Origem = OrigemOperacaoBolsa.ResultadoConversao;
                                operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                                operacaoBolsa.DataLiquidacao = data;
                                operacaoBolsa.PercentualDesconto = 0;
                                operacaoBolsa.Desconto = 0;
                                operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                                operacaoBolsa.IdCliente = idCliente;
                                operacaoBolsa.CalculaDespesas = "N";
                                operacaoBolsa.ResultadoRealizado = 0;
                                operacaoBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                                operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                                operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                                operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                                operacaoBolsa.DataOperacao = data;

                                operacaoBolsa.Save();
                                #endregion

                                #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado = zero)
                                operacaoBolsa = new OperacaoBolsa();
                                operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                                operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito;
                                operacaoBolsa.Data = data;
                                operacaoBolsa.Pu = 0;
                                operacaoBolsa.Valor = 0;
                                operacaoBolsa.ValorLiquido = 0;
                                operacaoBolsa.PULiquido = 0;
                                operacaoBolsa.Corretagem = 0;
                                operacaoBolsa.Emolumento = 0;
                                operacaoBolsa.LiquidacaoCBLC = 0;
                                operacaoBolsa.RegistroBolsa = 0;
                                operacaoBolsa.RegistroCBLC = 0;                                    //
                                operacaoBolsa.Quantidade = quantidadeConvertida;
                                operacaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                                operacaoBolsa.IdAgenteCorretora = idAgente;
                                operacaoBolsa.IdAgenteLiquidacao = idAgente;
                                operacaoBolsa.Origem = OrigemOperacaoBolsa.ResultadoConversao;
                                operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                                operacaoBolsa.DataLiquidacao = data;
                                operacaoBolsa.PercentualDesconto = 0;
                                operacaoBolsa.Desconto = 0;
                                operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                                operacaoBolsa.IdCliente = idCliente;
                                operacaoBolsa.CalculaDespesas = "N";
                                operacaoBolsa.ResultadoRealizado = 0;
                                operacaoBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                                operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                                operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                                operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                                operacaoBolsa.DataOperacao = data;

                                operacaoBolsa.Save();
                                #endregion
                            }
                            #endregion

                            #region Atualiza em PosicaoBolsa
                            posicaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            posicaoBolsa.Quantidade = quantidadeFinal;
                            posicaoBolsa.QuantidadeBloqueada = quantidadeBloqueadaFinal;
                            posicaoBolsa.PUCusto = puCustoFinal;
                            posicaoBolsa.PUCustoLiquido = puCustoLiquidoFinal;
                            #endregion
                        }
                        #endregion

                        // Atualiza Posicao
                        if (posicaoBolsaCollection.Count >= 1) {
                            posicaoBolsaCollection.Save();
                        }
                        #endregion
                    }
                    #endregion
                }
                
                // Primeira vez
                if (i == 0) {
                    #region Entra na Primeira vez
                    PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                    posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente == idCliente,
                                                       posicaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);

                    if (posicaoBolsaCollection.Query.Load()) {
                        situacaoProvisoriaPosicoes = (PosicaoBolsaCollection)Utilitario.Clone(posicaoBolsaCollection);
                    }
                    else {
                        situacaoProvisoriaPosicoes = null;
                    }

                    #region Para cada Posicao
                    for (int j = 0; j < posicaoBolsaCollection.Count; j++) {
                        PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[j];

                        #region Dados de PosicaoBolsa
                        decimal quantidade = posicaoBolsa.Quantidade.Value;
                        decimal quantidadeBloqueada = posicaoBolsa.QuantidadeBloqueada.Value;
                        decimal puCusto = posicaoBolsa.PUCusto.Value;
                        decimal puCustoLiquido = posicaoBolsa.PUCustoLiquido.Value;
                        int idAgente = posicaoBolsa.IdAgente.Value;
                        #endregion

                        #region Converte quantidade e PU
                        decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                        decimal quantidadeBloqueadaConvertida = Utilitario.Truncate(quantidadeBloqueada * fatorQuantidade, 0);
                        decimal puCustoConvertido = puCusto * fatorPu;
                        decimal puCustoLiquidoConvertido = puCustoLiquido * fatorPu;
                        #endregion

                        decimal quantidadeFinal = quantidadeConvertida;
                        decimal quantidadeBloqueadaFinal = quantidadeBloqueadaConvertida;
                        decimal puCustoFinal = puCustoConvertido;
                        decimal puCustoLiquidoFinal = puCustoLiquidoConvertido;

                        #region Trata caso específico para quando já tem o ativo destino na carteira
                        if (cdAtivoBolsa != cdAtivoBolsaDestino) //Meio esquisito este teste, mas só para garantir!
                        {
                            decimal resultadoConversao = 0;

                            PosicaoBolsa posicaoBolsaExistente = new PosicaoBolsa();
                            if (posicaoBolsaExistente.BuscaPosicaoBolsa(idCliente, idAgente, cdAtivoBolsaDestino)) 
                            {
                                decimal quantidadeExistente = posicaoBolsaExistente.Quantidade.Value;
                                decimal quantidadeBloqueadaExistente = posicaoBolsaExistente.QuantidadeBloqueada.Value;
                                decimal puCustoExistente = posicaoBolsaExistente.PUCusto.Value;
                                decimal puCustoLiquidoExistente = posicaoBolsaExistente.PUCustoLiquido.Value;

                                posicaoBolsaExistente.MarkAsDeleted();
                                posicaoBolsaExistente.Save();

                                FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                                if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsaDestino, data)) 
                                {
                                    if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                                    {
                                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                                        if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsaDestino))
                                        {
                                            throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsaDestino + " " + data.ToString("d"));
                                        }
                                    }
                                }

                                quantidadeFinal = quantidadeConvertida + quantidadeExistente;
                                quantidadeBloqueadaFinal = quantidadeBloqueadaConvertida + quantidadeBloqueadaExistente;
                                
                                if ((quantidadeConvertida > 0 && quantidadeExistente < 0) ||
                                    (quantidadeConvertida < 0 && quantidadeExistente > 0))
                                {
                                    if (Math.Abs(quantidadeConvertida) > Math.Abs(quantidadeExistente))
                                    {
                                        puCustoFinal = puCustoConvertido;
                                        puCustoLiquidoFinal = puCustoLiquidoConvertido;

                                        decimal quantidadeResultado = Math.Abs(quantidadeExistente);
                                        if (quantidadeConvertida > 0)
                                        {                                            
                                            resultadoConversao = Math.Round(quantidadeResultado * (puCustoConvertido - puCustoExistente) / fator.Fator.Value, 2);
                                        }
                                        else
                                        {
                                            resultadoConversao = Math.Round(quantidadeResultado * (puCustoExistente - puCustoConvertido) / fator.Fator.Value, 2);
                                        }
                                    }
                                    else
                                    {
                                        puCustoFinal = puCustoExistente;
                                        puCustoLiquidoFinal = puCustoLiquidoExistente;

                                        decimal quantidadeResultado = Math.Abs(quantidadeConvertida);
                                        if (quantidadeConvertida > 0)
                                        {
                                            resultadoConversao = Math.Round(quantidadeResultado * (puCustoConvertido - puCustoExistente) / fator.Fator.Value, 2);
                                        }
                                        else
                                        {
                                            resultadoConversao = Math.Round(quantidadeResultado * (puCustoExistente - puCustoConvertido) / fator.Fator.Value, 2);
                                        }
                                    }
                                }
                                else
                                {
                                    decimal custoConvertido = quantidadeConvertida * puCustoConvertido / fator.Fator.Value;
                                    decimal custoLiquidoConvertido = quantidadeConvertida * puCustoLiquidoConvertido / fator.Fator.Value;

                                    decimal custoOriginal = quantidadeExistente * puCustoExistente / fator.Fator.Value;
                                    decimal custoLiquidoOriginal = quantidadeExistente * puCustoLiquidoExistente / fator.Fator.Value;

                                    puCustoFinal = ((custoOriginal + custoConvertido) / quantidadeFinal) / fator.Fator.Value;
                                    puCustoLiquidoFinal = ((custoLiquidoOriginal + custoLiquidoConvertido) / quantidadeFinal) / fator.Fator.Value;
                                }
                            }

                            #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado calculado)
                            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                            operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                            operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Retirada;
                            operacaoBolsa.Data = data;
                            operacaoBolsa.Pu = 0;
                            operacaoBolsa.Valor = 0;
                            operacaoBolsa.ValorLiquido = 0;
                            operacaoBolsa.PULiquido = 0;
                            operacaoBolsa.Corretagem = 0;
                            operacaoBolsa.Emolumento = 0;
                            operacaoBolsa.LiquidacaoCBLC = 0;
                            operacaoBolsa.RegistroBolsa = 0;
                            operacaoBolsa.RegistroCBLC = 0;                                    //
                            operacaoBolsa.Quantidade = quantidade;
                            operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                            operacaoBolsa.IdAgenteCorretora = idAgente;
                            operacaoBolsa.IdAgenteLiquidacao = idAgente;
                            operacaoBolsa.Origem = OrigemOperacaoBolsa.ResultadoConversao;
                            operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                            operacaoBolsa.DataLiquidacao = data;
                            operacaoBolsa.PercentualDesconto = 0;
                            operacaoBolsa.Desconto = 0;
                            operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                            operacaoBolsa.IdCliente = idCliente;
                            operacaoBolsa.CalculaDespesas = "N";
                            operacaoBolsa.ResultadoRealizado = resultadoConversao;
                            operacaoBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                            operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                            operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                            operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                            operacaoBolsa.DataOperacao = data;

                            operacaoBolsa.Save();
                            #endregion

                            #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado = zero)
                            operacaoBolsa = new OperacaoBolsa();
                            operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                            operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito;
                            operacaoBolsa.Data = data;
                            operacaoBolsa.Pu = 0;
                            operacaoBolsa.Valor = 0;
                            operacaoBolsa.ValorLiquido = 0;
                            operacaoBolsa.PULiquido = 0;
                            operacaoBolsa.Corretagem = 0;
                            operacaoBolsa.Emolumento = 0;
                            operacaoBolsa.LiquidacaoCBLC = 0;
                            operacaoBolsa.RegistroBolsa = 0;
                            operacaoBolsa.RegistroCBLC = 0;                                    //
                            operacaoBolsa.Quantidade = quantidadeConvertida;
                            operacaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            operacaoBolsa.IdAgenteCorretora = idAgente;
                            operacaoBolsa.IdAgenteLiquidacao = idAgente;
                            operacaoBolsa.Origem = OrigemOperacaoBolsa.ResultadoConversao;
                            operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                            operacaoBolsa.DataLiquidacao = data;
                            operacaoBolsa.PercentualDesconto = 0;
                            operacaoBolsa.Desconto = 0;
                            operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                            operacaoBolsa.IdCliente = idCliente;
                            operacaoBolsa.CalculaDespesas = "N";
                            operacaoBolsa.ResultadoRealizado = 0;
                            operacaoBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                            operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                            operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                            operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                            operacaoBolsa.DataOperacao = data;

                            operacaoBolsa.Save();
                            #endregion
                        }
                        #endregion

                        #region Atualiza em PosicaoBolsa
                        posicaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                        posicaoBolsa.Quantidade = quantidadeFinal;
                        posicaoBolsa.QuantidadeBloqueada = quantidadeBloqueadaFinal;
                        posicaoBolsa.PUCusto = puCustoFinal;
                        posicaoBolsa.PUCustoLiquido = puCustoLiquidoFinal;
                        #endregion
                    }
                    #endregion

                    // Atualiza Posicao
                    if (posicaoBolsaCollection.Count >= 1) {
                        posicaoBolsaCollection.Save();
                    }
                    #endregion
                }
            }
            #endregion
        }

        /// <summary>
        /// Processa Atualizações de PosicaoEmprestimoBolsa a Partir de Conversões Efetivadas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaConversaoEmprestimo(int idCliente, DateTime data)
        {
            ConversaoBolsaCollection conversaoBolsaCollection = new ConversaoBolsaCollection();
            conversaoBolsaCollection = conversaoBolsaCollection.BuscaConversaoBolsa(data, idCliente, false, false);

            // Guarda a Situação Anterior da Posicao ou Null Se não Existir Posicao
            PosicaoEmprestimoBolsaCollection situacaoProvisoriaPosicoes = null;

            #region Para Cada Conversão
            for (int i = 0; i < conversaoBolsaCollection.Count; i++) {
                #region Dados de Conversão
                ConversaoBolsa conversaoBolsa = conversaoBolsaCollection[i];
                string cdAtivoBolsa = conversaoBolsa.CdAtivoBolsa;
                string cdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino;
                decimal fatorQuantidade = conversaoBolsa.FatorQuantidade.Value;
                decimal fatorPu = conversaoBolsa.FatorPU.Value;
                #endregion

                // Para Segundo elemento em diante
                if (i >= 1) {
                    #region Segundo Elemento em Diante
                    string cdAtivoConversaoAtual = conversaoBolsaCollection[i].CdAtivoBolsa;
                    string cdAtivoConversaoAnterior = conversaoBolsaCollection[i - 1].CdAtivoBolsa;

                    // Identificado Conversão em Multiplos Passos
                    if (cdAtivoConversaoAtual == cdAtivoConversaoAnterior && situacaoProvisoriaPosicoes != null) {
                        // Retorna a Posicao Provisoria Anterior para Afetar em cima dela
                        #region Conversao Multipla
                        PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = (PosicaoEmprestimoBolsaCollection)Utilitario.Clone(situacaoProvisoriaPosicoes);

                        #region Para cada Posicao
                        for (int j = 0; j < posicaoEmprestimoBolsaCollection.Count; j++) {
                            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = (PosicaoEmprestimoBolsa)posicaoEmprestimoBolsaCollection[j];
                            #region Dados de PosicaoEmprestimoBolsa
                            decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                            decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;
                            #endregion

                            #region Converte quantidade e PU
                            decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                            decimal puConvertido = puLiquidoOriginal * fatorPu;
                            #endregion

                            #region Atualiza em posicaoEmprestimoBolsa
                            posicaoEmprestimoBolsa.MarkAllColumnsAsDirty(DataRowState.Added);
                            //
                            posicaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            posicaoEmprestimoBolsa.Quantidade = quantidadeConvertida;
                            posicaoEmprestimoBolsa.PULiquidoOriginal = puConvertido;
                            #endregion
                        }
                        #endregion

                        if (posicaoEmprestimoBolsaCollection.Count >= 1) {
                            posicaoEmprestimoBolsaCollection.Save();
                        }
                        #endregion
                    }
                    else {
                        #region Conversao Normal
                        PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                        posicaoEmprestimoBolsaCollection.BuscaPosicaoEmprestimoBolsaCompleta(idCliente, cdAtivoBolsa);

                        if (posicaoEmprestimoBolsaCollection.Query.Load()) {
                            situacaoProvisoriaPosicoes = (PosicaoEmprestimoBolsaCollection)Utilitario.Clone(posicaoEmprestimoBolsaCollection);
                        }
                        else {
                            situacaoProvisoriaPosicoes = null;
                        }

                        #region Para cada Posicao
                        for (int j = 0; j < posicaoEmprestimoBolsaCollection.Count; j++) {
                            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = (PosicaoEmprestimoBolsa)posicaoEmprestimoBolsaCollection[j];
                            #region Dados de PosicaoEmprestimoBolsa
                            decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                            decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;
                            #endregion

                            #region Converte quantidade e PU
                            decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                            decimal puConvertido = puLiquidoOriginal * fatorPu;
                            #endregion

                            #region Atualiza em posicaoEmprestimoBolsa
                            posicaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            posicaoEmprestimoBolsa.Quantidade = quantidadeConvertida;
                            posicaoEmprestimoBolsa.PULiquidoOriginal = puConvertido;
                            #endregion
                        }
                        #endregion

                        if (posicaoEmprestimoBolsaCollection.Count >= 1) {
                            posicaoEmprestimoBolsaCollection.Save();
                        }                      
                        #endregion
                    }
                    #endregion
                }

                // Primeira vez
                if (i == 0) {
                    #region Entra na Primeira vez
                    PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                    posicaoEmprestimoBolsaCollection.BuscaPosicaoEmprestimoBolsaCompleta(idCliente, cdAtivoBolsa);

                    if (posicaoEmprestimoBolsaCollection.Query.Load()) {
                        situacaoProvisoriaPosicoes = (PosicaoEmprestimoBolsaCollection)Utilitario.Clone(posicaoEmprestimoBolsaCollection);
                    }
                    else {
                        situacaoProvisoriaPosicoes = null;
                    }

                    #region Para cada Posicao
                    for (int j = 0; j < posicaoEmprestimoBolsaCollection.Count; j++) {
                        PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = (PosicaoEmprestimoBolsa)posicaoEmprestimoBolsaCollection[j];
                        #region Dados de PosicaoEmprestimoBolsa
                        decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                        decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;
                        #endregion

                        #region Converte quantidade e PU
                        decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                        decimal puConvertido = puLiquidoOriginal * fatorPu;
                        #endregion

                        #region Atualiza em posicaoEmprestimoBolsa
                        posicaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                        posicaoEmprestimoBolsa.Quantidade = quantidadeConvertida;
                        posicaoEmprestimoBolsa.PULiquidoOriginal = puConvertido;
                        #endregion
                    }
                    #endregion

                    if (posicaoEmprestimoBolsaCollection.Count >= 1) {
                        posicaoEmprestimoBolsaCollection.Save();
                    }
                    #endregion
                }
            }
            #endregion
        }

        /// <summary>
        /// Processa Atualizações de PosicaoTermoBolsa, a Partir de Conversões Efetivadas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaConversaoTermo(int idCliente, DateTime data) 
        {
            ConversaoBolsaCollection conversaoBolsaCollection = new ConversaoBolsaCollection();
            conversaoBolsaCollection = conversaoBolsaCollection.BuscaConversaoBolsa(data, idCliente, false, true);

            // Guarda a Situação Anterior da Posicao ou Null Se não Existir Posicao
            PosicaoTermoBolsaCollection situacaoProvisoriaPosicoes = null;

            #region Para Cada Conversão
            for (int i = 0; i < conversaoBolsaCollection.Count; i++) {
                #region Dados de Conversão
                ConversaoBolsa conversaoBolsa = conversaoBolsaCollection[i];
                string cdAtivoBolsa = conversaoBolsa.CdAtivoBolsa;
                string cdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino;
                decimal fatorQuantidade = conversaoBolsa.FatorQuantidade.Value;
                decimal fatorPu = conversaoBolsa.FatorPU.Value;
                #endregion

                // Para Segundo Elemento em diante
                if (i >= 1) {
                    #region Segundo Elemento em Diante
                    string cdAtivoConversaoAtual = conversaoBolsaCollection[i].CdAtivoBolsa;
                    string cdAtivoConversaoAnterior = conversaoBolsaCollection[i - 1].CdAtivoBolsa;

                    // Identificado Conversão em Multiplos Passos
                    if (cdAtivoConversaoAtual == cdAtivoConversaoAnterior && situacaoProvisoriaPosicoes != null) {
                        // Retorna a Posicao Provisoria Anterior para Afetar em cima dela
                        #region Conversao Multipla
                        PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = (PosicaoTermoBolsaCollection)Utilitario.Clone(situacaoProvisoriaPosicoes);

                        for (int j = 0; j < posicaoTermoBolsaCollection.Count; j++) {
                            PosicaoTermoBolsa posicaoTermoBolsa = (PosicaoTermoBolsa)posicaoTermoBolsaCollection[j];
                            
                            #region Dados de PosicaoTermoBolsa
                            decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                            decimal puCustoLiquidoAcao = posicaoTermoBolsa.PUCustoLiquidoAcao.Value;
                            decimal puTermo = posicaoTermoBolsa.PUTermo.Value;
                            decimal puTermoLiquido = posicaoTermoBolsa.PUTermoLiquido.Value;
                            #endregion

                            #region Converte quantidade e PUs
                            decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                            decimal puCustoLiquidoAcaoConvertido = puCustoLiquidoAcao * fatorPu;
                            decimal puTermoConvertido = puTermo * fatorPu;
                            decimal puTermoLiquidoConvertido = puTermoLiquido * fatorPu;
                            #endregion

                            #region Atualiza em PosicaoTermoBolsa
                            posicaoTermoBolsa.MarkAllColumnsAsDirty(DataRowState.Added);
                            //
                            posicaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            posicaoTermoBolsa.Quantidade = quantidadeConvertida;
                            posicaoTermoBolsa.PUCustoLiquidoAcao = puCustoLiquidoAcaoConvertido;
                            posicaoTermoBolsa.PUTermo = puTermoConvertido;
                            posicaoTermoBolsa.PUTermoLiquido = puTermoLiquidoConvertido;
                            #endregion
                        }
                        if (posicaoTermoBolsaCollection.Count >= 1) {
                            posicaoTermoBolsaCollection.Save();
                        }
                        #endregion
                    }
                    else {
                        #region Conversao Normal
                        PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                        posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsaCompleta(idCliente, cdAtivoBolsa);

                        if (posicaoTermoBolsaCollection.Query.Load()) {
                            situacaoProvisoriaPosicoes = (PosicaoTermoBolsaCollection)Utilitario.Clone(posicaoTermoBolsaCollection);
                        }
                        else {
                            situacaoProvisoriaPosicoes = null;
                        }

                        for (int j = 0; j < posicaoTermoBolsaCollection.Count; j++) {
                            PosicaoTermoBolsa posicaoTermoBolsa = (PosicaoTermoBolsa)posicaoTermoBolsaCollection[j];
                            #region Dados de PosicaoTermoBolsa
                            decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                            decimal puCustoLiquidoAcao = posicaoTermoBolsa.PUCustoLiquidoAcao.Value;
                            decimal puTermo = posicaoTermoBolsa.PUTermo.Value;
                            decimal puTermoLiquido = posicaoTermoBolsa.PUTermoLiquido.Value;
                            #endregion

                            #region Converte quantidade e PUs
                            decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                            decimal puCustoLiquidoAcaoConvertido = puCustoLiquidoAcao * fatorPu;
                            decimal puTermoConvertido = puTermo * fatorPu;
                            decimal puTermoLiquidoConvertido = puTermoLiquido * fatorPu;
                            #endregion

                            #region Atualiza em posicaoTermoBolsa
                            posicaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            posicaoTermoBolsa.Quantidade = quantidadeConvertida;
                            posicaoTermoBolsa.PUCustoLiquidoAcao = puCustoLiquidoAcaoConvertido;
                            posicaoTermoBolsa.PUTermo = puTermoConvertido;
                            posicaoTermoBolsa.PUTermoLiquido = puTermoLiquidoConvertido;
                            #endregion
                        }
                        if (posicaoTermoBolsaCollection.Count >= 1) {
                            posicaoTermoBolsaCollection.Save();
                        }                   
                        #endregion
                    }
                    #endregion
                }
                // Primeira vez
                if (i == 0) {
                    #region Entra na Primeira vez
                    PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                    posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsaCompleta(idCliente, cdAtivoBolsa);

                    if (posicaoTermoBolsaCollection.Query.Load()) {
                        situacaoProvisoriaPosicoes = (PosicaoTermoBolsaCollection)Utilitario.Clone(posicaoTermoBolsaCollection);
                    }
                    else {
                        situacaoProvisoriaPosicoes = null;
                    }
                    
                    for (int j = 0; j < posicaoTermoBolsaCollection.Count; j++) {
                        PosicaoTermoBolsa posicaoTermoBolsa = (PosicaoTermoBolsa)posicaoTermoBolsaCollection[j];
                        #region Dados de PosicaoTermoBolsa
                        decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                        decimal puCustoLiquidoAcao = posicaoTermoBolsa.PUCustoLiquidoAcao.Value;
                        decimal puTermo = posicaoTermoBolsa.PUTermo.Value;
                        decimal puTermoLiquido = posicaoTermoBolsa.PUTermoLiquido.Value;
                        #endregion

                        #region Converte quantidade e PUs
                        decimal quantidadeConvertida = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                        decimal puCustoLiquidoAcaoConvertido = puCustoLiquidoAcao * fatorPu;
                        decimal puTermoConvertido = puTermo * fatorPu;
                        decimal puTermoLiquidoConvertido = puTermoLiquido * fatorPu;
                        #endregion

                        #region Atualiza em posicaoTermoBolsa
                        posicaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                        posicaoTermoBolsa.Quantidade = quantidadeConvertida;
                        posicaoTermoBolsa.PUCustoLiquidoAcao = puCustoLiquidoAcaoConvertido;
                        posicaoTermoBolsa.PUTermo = puTermoConvertido;
                        posicaoTermoBolsa.PUTermoLiquido = puTermoLiquidoConvertido;
                        #endregion
                    }
                    if (posicaoTermoBolsaCollection.Count >= 1) {
                        posicaoTermoBolsaCollection.Save();
                    }
                    #endregion
                }
            }
            #endregion
        }
    }
}