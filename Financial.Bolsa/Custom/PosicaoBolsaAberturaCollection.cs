﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa
{
    public partial class PosicaoBolsaAberturaCollection : esPosicaoBolsaAberturaCollection
    {

        public PosicaoBolsaAberturaCollection(PosicaoBolsaCollection posicaoBolsaCollection, DateTime dataHistorico)
        {
            for (int i = 0; i < posicaoBolsaCollection.Count; i++)
            {
                //
                PosicaoBolsaAbertura p = new PosicaoBolsaAbertura();

                // Para cada Coluna de PosicaoBolsa copia para PosicaoBolsa
                foreach (esColumnMetadata colPosicao in posicaoBolsaCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas menos a Data 
                    esColumnMetadata colPosicaoBolsa = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                    if (posicaoBolsaCollection[i].GetColumn(colPosicao.Name) != null)
                    {
                        p.SetColumn(colPosicaoBolsa.Name, posicaoBolsaCollection[i].GetColumn(colPosicao.Name));
                    }
                    p.DataHistorico = dataHistorico;
                }
                this.AttachEntity(p);
            }
        }


        /// <summary>
        /// Deleta posições de bolsa históricas.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBolsaAberturaDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoBolsaAberturaCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();
        }
    }
}
