using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa {
    public partial class TransferenciaBolsaCollection : esTransferenciaBolsaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(TransferenciaBolsaCollection));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaTransferenciaBolsa(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTransferencia, this.Query.CdAtivoBolsa, this.Query.IdAgenteOrigem,
                        this.Query.IdAgenteDestino, this.Query.Quantidade, this.Query.Pu,
                        this.Query.IdCliente)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente);                 

            this.Query.Load();
        }                 
    }
}
