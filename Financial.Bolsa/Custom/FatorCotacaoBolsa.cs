﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa {
    public partial class FatorCotacaoBolsa : esFatorCotacaoBolsa 
    {        
        /// <summary>
        /// Carrega o objeto FatorCotacaoBolsa com os campos Fator, CdAtivoBolsa, DataReferencia.
        /// A data de referência é filtrada para todas as datas anteriores ou iguais a ela.
        /// </summary>
        /// <param name="cdAtivo"></param>
        /// <param name="dataReferencia"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaFatorCotacaoBolsa(string cdAtivoBolsa, DateTime dataReferencia) 
        {
            if (cdAtivoBolsa.Length >= 14) //Se for Termo, usa o fator cotação da ação correspondente
            {
                cdAtivoBolsa = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa);
            }

            FatorCotacaoBolsaCollection fatorCotacaoBolsaCollection = new FatorCotacaoBolsaCollection();

            // Busca todos os fatores menores que a data de referencia ordenado decrescente pela data
            fatorCotacaoBolsaCollection.Query
                 .Select(fatorCotacaoBolsaCollection.Query.Fator, 
                         fatorCotacaoBolsaCollection.Query.CdAtivoBolsa,
                         fatorCotacaoBolsaCollection.Query.DataReferencia)
                 .Where(fatorCotacaoBolsaCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia),
                        fatorCotacaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa)
                 .OrderBy(fatorCotacaoBolsaCollection.Query.DataReferencia.Descending);

            fatorCotacaoBolsaCollection.Query.Load();

            if (fatorCotacaoBolsaCollection.HasData)
            {
                // Copia informações de FatorCotacaoBolsaCollection para FatorCotacaoBolsa
                this.AddNew();
                this.Fator = ((FatorCotacaoBolsa)fatorCotacaoBolsaCollection[0]).Fator.Value;
                this.CdAtivoBolsa = ((FatorCotacaoBolsa)fatorCotacaoBolsaCollection[0]).CdAtivoBolsa;
                this.DataReferencia = ((FatorCotacaoBolsa)fatorCotacaoBolsaCollection[0]).DataReferencia;
            }
            else
            {
                // Retorna um fator default = 1
                this.AddNew();
                this.Fator = 1M;
                this.CdAtivoBolsa = cdAtivoBolsa;
                this.DataReferencia = new DateTime(2000, 1, 1);
            }
         
            return true;
        }

        /// <summary>
        /// Metodo de Insercao do Fato
        /// </summary>
        /// <param name="fatorCotacaoBolsa">FatorCotacao com os dados a inserir</param>
        /// throws ArgumentNullException se campos Obrigatórios da inclusão estiverem nulos
        /// fatorCotacaoBolsa.CdAtivoBolsa
        /// fatorCotacaoBolsa.DataReferencia
        /// fatorCotacaoBolsa.Fator
        public void InsereFatorCotacaoBolsa(FatorCotacaoBolsa fatorCotacaoBolsa) 
        {
            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(fatorCotacaoBolsa.CdAtivoBolsa) ||
               !fatorCotacaoBolsa.DataReferencia.HasValue ||
               !fatorCotacaoBolsa.Fator.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("fatorCotacaoBolsa.CdAtivoBolsa ou ")
                .Append("fatorCotacaoBolsa.DataReferencia ou ")
                .Append("fatorCotacaoBolsa.Fator ");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            FatorCotacaoBolsa fatorCotacaoBolsaInserir = new FatorCotacaoBolsa();
            fatorCotacaoBolsaInserir.AddNew();
            //            
            // Campos Obrigatórios para a inserção
            fatorCotacaoBolsaInserir.CdAtivoBolsa = fatorCotacaoBolsa.CdAtivoBolsa;
            fatorCotacaoBolsaInserir.DataReferencia = fatorCotacaoBolsa.DataReferencia;
            fatorCotacaoBolsaInserir.Fator = fatorCotacaoBolsa.Fator.Value;
            //         
            try {
                fatorCotacaoBolsaInserir.Save();
            }
            catch (Exception e) {
                System.Console.WriteLine(e.StackTrace);
                System.Console.WriteLine(e.Message);
                System.Console.WriteLine("FatorCotacaoBolsa cdativoBolsa: " + fatorCotacaoBolsa.CdAtivoBolsa);                
            }
        }

    }
}
