﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa.Enums;
using Financial.Util;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Interfaces.Import.Bolsa;
using Financial.Bolsa.Exceptions;
using Financial.Common.Enums;
using Financial.Interfaces.Sinacor;

namespace Financial.Bolsa {
    public partial class ProventoBolsa : esProventoBolsa
    {
        /// <summary>
        /// Carrega ProventoBolsa a partir da estrutura importada do arquivo PROD.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="nomeArquivo"></param>
        /// throws AtivoNaoCadastradoException se cdAtivoBolsa não existir
        public void CarregaProdProvento(ProdCollection prodCollection, DateTime data)
        {
            // Pega os Proventos do Arquivo de Prod                          
            Prod prod = new Prod();            
            ProdCollection prodCollectionSubList = new ProdCollection();

            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoEmDinheiro);

            //Deleta os proventos cadastrados na data, inclusive os manuais!
            ProventoBolsaCollection proventoBolsaCollectionDeletar = new ProventoBolsaCollection();
            proventoBolsaCollectionDeletar.Query.Where(proventoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            proventoBolsaCollectionDeletar.Query.Load();
            proventoBolsaCollectionDeletar.MarkAllAsDeleted();
            proventoBolsaCollectionDeletar.Save();
            //

            ProventoBolsaCollection proventoBolsaCollection = new ProventoBolsaCollection();
            //
            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++)
            {
                prod = prodCollectionSubList.CollectionProd[i];

                #region Dados Prod
                DateTime dataEx = prod.DataEx.Value;
                DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                DateTime dataPagamento = prod.DataPagamento.Value;
                decimal valor = prod.ValorProvento.Value;
                int tipoProvento = prod.TipoProvento.Value;
                #endregion

                string codigoISIN = prod.CodigoIsinBase;
                string tipoAtivoISIN = AtivoBolsa.RetornaTipoAtivoISIN(codigoISIN);

                if (tipoAtivoISIN == "CRI" || tipoAtivoISIN == "DBS") //Não carrega CRIs nem debentures
                {
                    continue;
                }

                string cdAtivoBolsa = prod.CdAtivoBolsa;
                #region Verifica se cdAtivoBolsa existe, e cadastra se nao existir
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                {
                    AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                    ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                    ativoBolsaNovo.CodigoIsin = ""; //Não tem como buscar
                    ativoBolsaNovo.Descricao = cdAtivoBolsa;
                    ativoBolsaNovo.Especificacao = "*";
                    ativoBolsaNovo.IdEmissor = 1; //Assume qq emissor!
                    ativoBolsaNovo.IdMoeda = (short)ListaMoedaFixo.Real;
                    ativoBolsaNovo.PUExercicio = 0;
                    ativoBolsaNovo.TipoMercado = TipoMercadoBolsa.MercadoVista;
                    ativoBolsaNovo.TipoPapel = (byte)TipoPapelAtivo.Normal;
                    ativoBolsaNovo.NumeroSerie = 0;                      
                    ativoBolsaNovo.Save();

                    FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                    fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                    fatorCotacaoBolsaNovo.DataReferencia = data;
                    fatorCotacaoBolsaNovo.Fator = 1; //Assume 1!
                    fatorCotacaoBolsaNovo.Save();
                }
                #endregion

                if (prod.SituacaoProvento.ToString() == "I")
                {
                    ProventoBolsaCollection proventoBolsaCollectionExiste = new ProventoBolsaCollection();
                    proventoBolsaCollectionExiste.Query.Where(proventoBolsaCollectionExiste.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                        proventoBolsaCollectionExiste.Query.DataEx.Equal(dataEx),
                                                        proventoBolsaCollectionExiste.Query.DataPagamento.Equal(dataPagamento),
                                                        proventoBolsaCollectionExiste.Query.TipoProvento.Equal(tipoProvento));
                    proventoBolsaCollectionExiste.Query.Load();

                    if (proventoBolsaCollectionExiste.Count == 0)
                    {
                        ProventoBolsa proventoBolsa = new ProventoBolsa();
                        proventoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        proventoBolsa.DataEx = dataEx;
                        proventoBolsa.DataLancamento = data;
                        proventoBolsa.DataPagamento = dataPagamento;
                        proventoBolsa.TipoProvento = (byte)tipoProvento;
                        proventoBolsa.Valor = valor;

                        proventoBolsaCollection.AttachEntity(proventoBolsa);
                    }
                }
                else if (prod.SituacaoProvento.ToString() == "A")
                {
                    ProventoBolsaCollection proventoBolsaCollectionAtualizar = new ProventoBolsaCollection();
                    proventoBolsaCollectionAtualizar.Query.Where(proventoBolsaCollectionAtualizar.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                        proventoBolsaCollectionAtualizar.Query.DataEx.Equal(dataEx),
                                                        proventoBolsaCollectionAtualizar.Query.TipoProvento.Equal(tipoProvento));
                    proventoBolsaCollectionAtualizar.Query.Load();

                    if (proventoBolsaCollectionAtualizar.Count > 0)
                    {
                        foreach (ProventoBolsa proventoBolsa in proventoBolsaCollectionAtualizar)
                        {
                            proventoBolsa.DataPagamento = dataPagamento;
                            proventoBolsa.Valor = valor;
                        }

                        proventoBolsaCollectionAtualizar.Save();
                    }
                }
            }

            proventoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega ProventoBolsa a partir da estrutura importada do arquivo PROD.
        /// Não considera foreign Key Ativobolsa
        /// Importa mesmo se não existir AtivoBolsa
        /// </summary>
        /// <param name="data"></param>
        /// <param name="nomeArquivo"></param>
        public void CarregaProdProventoSemFK(ProdCollection prodCollection, DateTime data) {
            // Pega os Proventos do Arquivo de Prod                          
            Prod prod = new Prod();
            ProdCollection prodCollectionSubList = new ProdCollection();

            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoEmDinheiro);

            //Deleta os proventos cadastrados na data, inclusive os manuais!
            ProventoBolsaCollection proventoBolsaCollectionDeletar = new ProventoBolsaCollection();
            proventoBolsaCollectionDeletar.Query.Where(proventoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            proventoBolsaCollectionDeletar.Query.Load();
            proventoBolsaCollectionDeletar.MarkAllAsDeleted();
            proventoBolsaCollectionDeletar.Save();
            //

            ProventoBolsaCollection proventoBolsaCollection = new ProventoBolsaCollection();
            //
            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++) {
                prod = prodCollectionSubList.CollectionProd[i];

                #region Dados Prod
                DateTime dataEx = prod.DataEx.Value;
                DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                DateTime dataPagamento = prod.DataPagamento.Value;
                decimal valor = prod.ValorProvento.Value;
                int tipoProvento = prod.TipoProvento.Value;
                #endregion

                string codigoISIN = prod.CodigoIsinBase;
                string tipoAtivoISIN = AtivoBolsa.RetornaTipoAtivoISIN(codigoISIN);

                if (tipoAtivoISIN == "CRI" || tipoAtivoISIN == "DBS") //Não carrega CRIs nem debentures
                {
                    continue;
                }

                string cdAtivoBolsa = prod.CdAtivoBolsa;

                if (prod.SituacaoProvento.ToString() == "I") {
                    ProventoBolsaCollection proventoBolsaCollectionExiste = new ProventoBolsaCollection();
                    proventoBolsaCollectionExiste.Query.Where(proventoBolsaCollectionExiste.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                        proventoBolsaCollectionExiste.Query.DataEx.Equal(dataEx),
                                                        proventoBolsaCollectionExiste.Query.DataPagamento.Equal(dataPagamento),
                                                        proventoBolsaCollectionExiste.Query.TipoProvento.Equal(tipoProvento));
                    proventoBolsaCollectionExiste.Query.Load();

                    if (proventoBolsaCollectionExiste.Count == 0) {
                        ProventoBolsa proventoBolsa = new ProventoBolsa();
                        proventoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        proventoBolsa.DataEx = dataEx;
                        proventoBolsa.DataLancamento = data;
                        proventoBolsa.DataPagamento = dataPagamento;
                        proventoBolsa.TipoProvento = (byte)tipoProvento;
                        proventoBolsa.Valor = valor;

                        proventoBolsaCollection.AttachEntity(proventoBolsa);
                    }
                }
                else if (prod.SituacaoProvento.ToString() == "A") {
                    ProventoBolsaCollection proventoBolsaCollectionAtualizar = new ProventoBolsaCollection();
                    proventoBolsaCollectionAtualizar.Query.Where(proventoBolsaCollectionAtualizar.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                        proventoBolsaCollectionAtualizar.Query.DataEx.Equal(dataEx),
                                                        proventoBolsaCollectionAtualizar.Query.TipoProvento.Equal(tipoProvento));
                    proventoBolsaCollectionAtualizar.Query.Load();

                    if (proventoBolsaCollectionAtualizar.Count > 0) {
                        foreach (ProventoBolsa proventoBolsa in proventoBolsaCollectionAtualizar) {
                            proventoBolsa.DataPagamento = dataPagamento;
                            proventoBolsa.Valor = valor;
                        }

                        proventoBolsaCollectionAtualizar.Save();
                    }
                }
            }

            proventoBolsaCollection.Save();
        }

        public void CarregaProventosSinacor(DateTime data)
        {
            this.CarregaProventosSinacor(data, true);
        }

        /// <summary>
        /// Carrega os proventos da base do Sinacor.
        /// </summary>
        /// <param name="data"></param>
        public void CarregaProventosSinacor(DateTime data, bool checaAtivoExistente)
        {
            VcfproventoCollection vcfproventoCollection = new VcfproventoCollection();
            vcfproventoCollection.BuscaProventosSinacor(data);

            #region Deleta lancamentos nas tabelas
            ProventoBolsaCollection proventoBolsaCollectionDeletar = new ProventoBolsaCollection();
            proventoBolsaCollectionDeletar.Query.Where(proventoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            proventoBolsaCollectionDeletar.Query.Load();
            proventoBolsaCollectionDeletar.MarkAllAsDeleted();
            proventoBolsaCollectionDeletar.Save();

            ConversaoBolsaCollection conversaoBolsaCollectionDeletar = new ConversaoBolsaCollection();
            conversaoBolsaCollectionDeletar.Query.Where(conversaoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            conversaoBolsaCollectionDeletar.Query.Load();
            conversaoBolsaCollectionDeletar.MarkAllAsDeleted();
            conversaoBolsaCollectionDeletar.Save();

            BonificacaoBolsaCollection bonificacaoBolsaCollectionDeletar = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollectionDeletar.Query.Where(bonificacaoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            bonificacaoBolsaCollectionDeletar.Query.Load();
            bonificacaoBolsaCollectionDeletar.MarkAllAsDeleted();
            bonificacaoBolsaCollectionDeletar.Save();

            GrupamentoBolsaCollection grupamentoBolsaCollectionDeletar = new GrupamentoBolsaCollection();
            grupamentoBolsaCollectionDeletar.Query.Where(grupamentoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            grupamentoBolsaCollectionDeletar.Query.Load();
            grupamentoBolsaCollectionDeletar.MarkAllAsDeleted();
            grupamentoBolsaCollectionDeletar.Save();

            SubscricaoBolsaCollection subscricaoBolsaCollectionDeletar = new SubscricaoBolsaCollection();
            subscricaoBolsaCollectionDeletar.Query.Where(subscricaoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            subscricaoBolsaCollectionDeletar.Query.Load();
            subscricaoBolsaCollectionDeletar.MarkAllAsDeleted();
            subscricaoBolsaCollectionDeletar.Save();
            #endregion

            foreach (Vcfprovento vcfprovento in vcfproventoCollection)
            {
                #region Tratamento dos dados de vcfprovento
                vcfprovento.CodNeg = vcfprovento.CodNeg.Trim();

                if (Utilitario.Right(vcfprovento.CodNeg, 1) == "E" || Utilitario.Right(vcfprovento.CodNeg, 1) == "F" ||
                    Utilitario.Right(vcfprovento.CodNeg, 1) == "L")
                {
                    int len_menos1 = vcfprovento.CodNeg.Length - 1;
                    vcfprovento.CodNeg = vcfprovento.CodNeg.Substring(0, len_menos1);
                }

                if (vcfprovento.CodIsin == null)
                {
                    vcfprovento.CodIsin = "";
                }
                else
                {
                    vcfprovento.CodIsin = vcfprovento.CodIsin.Trim();
                }

                if (vcfprovento.CodIsinDir == null)
                {
                    vcfprovento.CodIsinDir = "";
                }
                else
                {
                    vcfprovento.CodIsinDir = vcfprovento.CodIsinDir.Trim();
                }
                
                if (vcfprovento.CodIsinDest == null)
                {
                    vcfprovento.CodIsinDest = "";
                }
                else
                {
                    vcfprovento.CodIsinDest = vcfprovento.CodIsinDest.Trim();
                }
                #endregion

                if (checaAtivoExistente)
                {
                    #region Importa ativos do Sinacor caso não existem na base
                    if (vcfprovento.CodNeg.Trim() != "")
                    {
                        AtivoBolsa ativoBolsaExiste = new AtivoBolsa();
                        if (!ativoBolsaExiste.LoadByPrimaryKey(vcfprovento.CodNeg.Trim()))
                        {
                            AtivoBolsa ativoBolsaImportar = new AtivoBolsa();
                            ativoBolsaImportar.IntegraAtivoSinacor(vcfprovento.CodNeg.Trim(), vcfprovento.DataIniPgtoProv.Value);
                        }
                    }

                    if (vcfprovento.CodIsinDir.Trim() != "")
                    {
                        AtivoBolsa ativoBolsaExiste = new AtivoBolsa();
                        if (!ativoBolsaExiste.BuscaCdAtivoBolsa(vcfprovento.CodIsinDir.Trim()))
                        {
                            AtivoBolsa ativoBolsaImportar = new AtivoBolsa();
                            ativoBolsaImportar.IntegraAtivoSinacorPorIsin(vcfprovento.CodIsinDir.Trim(), vcfprovento.DataIniPgtoProv.Value);
                        }
                    }

                    if (vcfprovento.CodIsinDest.Trim() != "")
                    {
                        AtivoBolsa ativoBolsaExiste = new AtivoBolsa();
                        if (!ativoBolsaExiste.BuscaCdAtivoBolsa(vcfprovento.CodIsinDest.Trim()))
                        {
                            AtivoBolsa ativoBolsaImportar = new AtivoBolsa();
                            ativoBolsaImportar.IntegraAtivoSinacorPorIsin(vcfprovento.CodIsinDest.Trim(), vcfprovento.DataIniPgtoProv.Value);
                        }
                    }
                    #endregion
                }
                                
                if (vcfprovento.TipoProv == 10 || vcfprovento.TipoProv == 11 || vcfprovento.TipoProv == 12 || 
                    vcfprovento.TipoProv == 13 || vcfprovento.TipoProv == 14 || vcfprovento.TipoProv == 22 ||
                    vcfprovento.TipoProv == 98)
                {
                    #region Dividendo, Rest. Capital, Bonificação em $, Juros s/ Capital, Rendimento, Rest. Capital com redução de ações, Rendimento líquido
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    if (!checaAtivoExistente || ativoBolsa.LoadByPrimaryKey(vcfprovento.CodNeg))
                    {
                        ProventoBolsa proventoBolsa = new ProventoBolsa();
                        proventoBolsa.CdAtivoBolsa = vcfprovento.CodNeg;
                        proventoBolsa.DataEx = vcfprovento.DataIniPgtoProv.Value;
                        proventoBolsa.DataLancamento = vcfprovento.DataIniPgtoProv.Value;
                        proventoBolsa.DataPagamento = vcfprovento.DataPgtoDivi.Value;
                        proventoBolsa.TipoProvento = (byte)vcfprovento.TipoProv.Value;
                        proventoBolsa.Valor = vcfprovento.ValProv.Value;                        

                        try
                        {
                            proventoBolsa.Save();
                        }
                        catch (Exception)
                        {

                        }                                            
                    }
                    else
                    {
                    }
                    #endregion
                }
                else if (vcfprovento.TipoProv == 20)
                {
                    #region Bonificação
                    TbotitulosCollection tbotitulosCollection = new TbotitulosCollection();
                    tbotitulosCollection.BuscaAtivosPorIsin(vcfprovento.CodIsinDest, data);
                    if (tbotitulosCollection.Count > 0)
                    {
                        string cdAtivoBolsaDestino = tbotitulosCollection[0].CdCodneg;
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!checaAtivoExistente || ativoBolsa.LoadByPrimaryKey(vcfprovento.CodNeg))
                        {
                            AtivoBolsa ativoBolsaDestino = new AtivoBolsa();
                            if (!checaAtivoExistente || ativoBolsaDestino.LoadByPrimaryKey(cdAtivoBolsaDestino))
                            {
                                BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();
                                bonificacaoBolsa.CdAtivoBolsa = vcfprovento.CodNeg;
                                bonificacaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;
                                bonificacaoBolsa.DataEx = vcfprovento.DataIniPgtoProv.Value;
                                bonificacaoBolsa.DataLancamento = vcfprovento.DataIniPgtoProv.Value;
                                bonificacaoBolsa.DataReferencia = vcfprovento.DataIniPgtoProv.Value;
                                bonificacaoBolsa.Fonte = (byte)FonteProventoBolsa.Manual;
                                bonificacaoBolsa.Percentual = vcfprovento.ValProv.Value;
                                bonificacaoBolsa.PUBonificacao = 0; //No arquivo (que vem do Sinacor) não tem essa informação para Boni
                                bonificacaoBolsa.TipoFracao = (byte)IndicadorTratamentoFracaoEvento.Trunca;

                                try
                                {
                                    bonificacaoBolsa.Save();
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                        else
                        {
                        }
                    }
                    #endregion
                }
                else if (vcfprovento.TipoProv == 30 || vcfprovento.TipoProv == 40)
                {
                    #region Split/Inplit
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    if (!checaAtivoExistente || ativoBolsa.LoadByPrimaryKey(vcfprovento.CodNeg))
                    {
                        GrupamentoBolsa grupamentoBolsa = new GrupamentoBolsa();
                        grupamentoBolsa.CdAtivoBolsa = vcfprovento.CodNeg;
                        grupamentoBolsa.DataEx = vcfprovento.DataIniPgtoProv.Value;
                        grupamentoBolsa.DataLancamento = vcfprovento.DataIniPgtoProv.Value;
                        grupamentoBolsa.DataReferencia = vcfprovento.DataIniPgtoProv.Value;
                        grupamentoBolsa.Fonte = (byte)FonteProventoBolsa.Manual;

                        if (vcfprovento.TipoProv == 30)
                        {
                            grupamentoBolsa.TipoGrupamento = (byte)TipoGrupamentoBolsa.Desdobramento;
                            grupamentoBolsa.FatorQuantidade = vcfprovento.ValProv.Value / 100M + 1;
                        }
                        else
                        {
                            grupamentoBolsa.TipoGrupamento = (byte)TipoGrupamentoBolsa.Grupamento;
                            grupamentoBolsa.FatorQuantidade = 1 / vcfprovento.ValProv.Value;
                        }
                        grupamentoBolsa.FatorPU = Math.Round(1 / grupamentoBolsa.FatorQuantidade.Value, 15);

                        try
                        {
                            grupamentoBolsa.Save();
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                    }
                    #endregion
                }
                else if (vcfprovento.TipoProv == 60 || vcfprovento.TipoProv == 70 ||
                         vcfprovento.TipoProv == 80 || vcfprovento.TipoProv == 81 ||
                         vcfprovento.TipoProv == 90)
                {
                    #region Conversão
                    TbotitulosCollection tbotitulosCollection = new TbotitulosCollection();
                    tbotitulosCollection.BuscaAtivosPorIsin(vcfprovento.CodIsinDest, data);
                    if (tbotitulosCollection.Count > 0)
                    {
                        string cdAtivoBolsaDestino = tbotitulosCollection[0].CdCodneg;
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!checaAtivoExistente || ativoBolsa.LoadByPrimaryKey(vcfprovento.CodNeg))
                        {
                            AtivoBolsa ativoBolsaDestino = new AtivoBolsa();
                            if (!checaAtivoExistente || ativoBolsaDestino.LoadByPrimaryKey(cdAtivoBolsaDestino))
                            {
                                ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
                                conversaoBolsa.CdAtivoBolsa = vcfprovento.CodNeg;
                                conversaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;
                                conversaoBolsa.DataEx = vcfprovento.DataIniPgtoProv.Value;
                                conversaoBolsa.DataLancamento = vcfprovento.DataIniPgtoProv.Value;
                                conversaoBolsa.DataReferencia = vcfprovento.DataIniPgtoProv.Value;
                                conversaoBolsa.FatorQuantidade = vcfprovento.ValProv.Value / 100M;
                                conversaoBolsa.FatorPU = Math.Round(1 / conversaoBolsa.FatorQuantidade.Value, 15);
                                conversaoBolsa.Fonte = (byte)FonteProventoBolsa.Manual;

                                try
                                {
                                    conversaoBolsa.Save();
                                }
                                catch (Exception)
                                {

                                }
                            }
                            else
                            {
                            }
                        }
                        else
                        {
                        }
                    }
                    #endregion
                }
                else if (vcfprovento.TipoProv == 52)
                {
                    #region Subscrição
                    TbotitulosCollection tbotitulosCollection = new TbotitulosCollection();
                    tbotitulosCollection.BuscaAtivosPorIsin(vcfprovento.CodIsinOri, data);
                    if (tbotitulosCollection.Count > 0)
                    {
                        string cdAtivoBolsa = tbotitulosCollection[0].CdCodneg;

                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!checaAtivoExistente || ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                        {
                            string cdAtivoBolsaDireito = vcfprovento.CodNeg;

                            SubscricaoBolsa subscricaoBolsa = new SubscricaoBolsa();
                            subscricaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                            subscricaoBolsa.CdAtivoBolsaDireito = cdAtivoBolsaDireito;
                            subscricaoBolsa.DataEx = vcfprovento.DataIniPgtoProv.Value;
                            subscricaoBolsa.DataLancamento = vcfprovento.DataIniPgtoProv.Value;
                            subscricaoBolsa.DataReferencia = vcfprovento.DataIniPgtoProv.Value;
                            subscricaoBolsa.DataFimNegociacao = vcfprovento.DataLimSubs.Value;
                            subscricaoBolsa.Fator = vcfprovento.ValProv.Value;
                            subscricaoBolsa.Fonte = (byte)FonteProventoBolsa.Manual;
                            subscricaoBolsa.PrecoSubscricao = vcfprovento.PrecPapSubs.Value;

                            try
                            {
                                subscricaoBolsa.Save();
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    #endregion
                }            
            
            }
        }

        public void AtualizaFatorPU(DateTime? data)
        {
            GrupamentoBolsaQuery grupamentoBolsaQuery = new GrupamentoBolsaQuery("G");
            FatorCotacaoBolsaQuery fatorCotacaoBolsaQuery = new FatorCotacaoBolsaQuery("F");

            grupamentoBolsaQuery.Select(grupamentoBolsaQuery.CdAtivoBolsa,
                                        grupamentoBolsaQuery.IdGrupamento,
                                        grupamentoBolsaQuery.FatorPU,
                                        grupamentoBolsaQuery.DataEx);
            grupamentoBolsaQuery.InnerJoin(fatorCotacaoBolsaQuery).On(grupamentoBolsaQuery.CdAtivoBolsa == fatorCotacaoBolsaQuery.CdAtivoBolsa &&
                                                                      grupamentoBolsaQuery.DataEx == fatorCotacaoBolsaQuery.DataReferencia);

            if (data.HasValue)
            {
                grupamentoBolsaQuery.Where(grupamentoBolsaQuery.DataEx.Equal(data));
            }

            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            grupamentoBolsaCollection.Load(grupamentoBolsaQuery);

            foreach (GrupamentoBolsa grupamentoBolsa in grupamentoBolsaCollection)
            {
                string cdAtivoBolsa = grupamentoBolsa.CdAtivoBolsa;
                DateTime dataEx = grupamentoBolsa.DataEx.Value;
                DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataEx, 1);

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataAnterior))
                {
                    grupamentoBolsa.FatorPU = grupamentoBolsa.FatorPU.Value * fatorCotacaoBolsa.Fator.Value;
                }
            }

            grupamentoBolsaCollection.Save();
        }

        public void ImportaProventos(DateTime data)
        {
            #region Deleta lancamentos nas tabelas
            ProventoBolsaCollection proventoBolsaCollectionDeletar = new ProventoBolsaCollection();
            proventoBolsaCollectionDeletar.Query.Where(proventoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            proventoBolsaCollectionDeletar.Query.Load();
            proventoBolsaCollectionDeletar.MarkAllAsDeleted();
            proventoBolsaCollectionDeletar.Save();

            ConversaoBolsaCollection conversaoBolsaCollectionDeletar = new ConversaoBolsaCollection();
            conversaoBolsaCollectionDeletar.Query.Where(conversaoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            conversaoBolsaCollectionDeletar.Query.Load();
            conversaoBolsaCollectionDeletar.MarkAllAsDeleted();
            conversaoBolsaCollectionDeletar.Save();

            BonificacaoBolsaCollection bonificacaoBolsaCollectionDeletar = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollectionDeletar.Query.Where(bonificacaoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            bonificacaoBolsaCollectionDeletar.Query.Load();
            bonificacaoBolsaCollectionDeletar.MarkAllAsDeleted();
            bonificacaoBolsaCollectionDeletar.Save();

            GrupamentoBolsaCollection grupamentoBolsaCollectionDeletar = new GrupamentoBolsaCollection();
            grupamentoBolsaCollectionDeletar.Query.Where(grupamentoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            grupamentoBolsaCollectionDeletar.Query.Load();
            grupamentoBolsaCollectionDeletar.MarkAllAsDeleted();
            grupamentoBolsaCollectionDeletar.Save();

            SubscricaoBolsaCollection subscricaoBolsaCollectionDeletar = new SubscricaoBolsaCollection();
            subscricaoBolsaCollectionDeletar.Query.Where(subscricaoBolsaCollectionDeletar.Query.DataLancamento.Equal(data));
            subscricaoBolsaCollectionDeletar.Query.Load();
            subscricaoBolsaCollectionDeletar.MarkAllAsDeleted();
            subscricaoBolsaCollectionDeletar.Save();
            #endregion

            esConnectionElement connSQL = new esConnectionElement();
            connSQL.Name = "FinancialSource";
            connSQL.ProviderMetadataKey = "esDefault";
            connSQL.SqlAccessType = esSqlAccessType.DynamicSQL;
            connSQL.Provider = "EntitySpaces.SqlClientProvider";
            connSQL.ProviderClass = "DataProvider";
            connSQL.ConnectionString = "Data Source=108.60.206.26\\SQL2008;Initial Catalog=BOLSA;User ID=bolsa;Password=7xpt678_34;";
            esConfigSettings.ConnectionInfo.Connections.Add(connSQL);

            ProventoBolsaCollection proventoBolsaCollectionSource = new ProventoBolsaCollection();
            proventoBolsaCollectionSource.es.Connection.Name = "FinancialSource";
            proventoBolsaCollectionSource.Query.Where(proventoBolsaCollectionSource.Query.DataLancamento.Equal(data));
            proventoBolsaCollectionSource.Query.Load();

            foreach (ProventoBolsa proventoBolsa in proventoBolsaCollectionSource)
            {
                ProventoBolsa proventoBolsaNew = new ProventoBolsa();

                proventoBolsaNew.CdAtivoBolsa = proventoBolsa.CdAtivoBolsa;
                proventoBolsaNew.DataEx = proventoBolsa.DataEx;
                proventoBolsaNew.DataLancamento = proventoBolsa.DataLancamento;
                proventoBolsaNew.DataPagamento = proventoBolsa.DataPagamento;
                proventoBolsaNew.TipoProvento = proventoBolsa.TipoProvento;
                proventoBolsaNew.Valor = proventoBolsa.Valor;

                try
                {
                    proventoBolsaNew.Save();
                }
                catch (Exception e1)
                {
                }

            }

            BonificacaoBolsaCollection bonificacaoBolsaCollectionSource = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollectionSource.es.Connection.Name = "FinancialSource";
            bonificacaoBolsaCollectionSource.Query.Where(bonificacaoBolsaCollectionSource.Query.DataLancamento.Equal(data));
            bonificacaoBolsaCollectionSource.Query.Load();

            foreach (BonificacaoBolsa bonificacaoBolsa in bonificacaoBolsaCollectionSource)
            {
                BonificacaoBolsa bonificacaoBolsaNew = new BonificacaoBolsa();
                bonificacaoBolsaNew.CdAtivoBolsa = bonificacaoBolsa.CdAtivoBolsa;
                bonificacaoBolsaNew.CdAtivoBolsaDestino = bonificacaoBolsa.CdAtivoBolsaDestino;
                bonificacaoBolsaNew.DataEx = bonificacaoBolsa.DataEx;
                bonificacaoBolsaNew.DataLancamento = bonificacaoBolsa.DataLancamento;
                bonificacaoBolsaNew.DataReferencia = bonificacaoBolsa.DataReferencia;
                bonificacaoBolsaNew.Fonte = bonificacaoBolsa.Fonte;
                bonificacaoBolsaNew.Percentual = bonificacaoBolsa.Percentual;
                bonificacaoBolsaNew.PUBonificacao = bonificacaoBolsa.PUBonificacao;
                bonificacaoBolsaNew.TipoFracao = bonificacaoBolsa.TipoFracao;

                try
                {
                    bonificacaoBolsaNew.Save();
                }
                catch (Exception)
                {
                }
            }

            ConversaoBolsaCollection conversaoBolsaCollectionSource = new ConversaoBolsaCollection();
            conversaoBolsaCollectionSource.es.Connection.Name = "FinancialSource";
            conversaoBolsaCollectionSource.Query.Where(conversaoBolsaCollectionSource.Query.DataLancamento.Equal(data));
            conversaoBolsaCollectionSource.Query.Load();

            foreach (ConversaoBolsa conversaoBolsa in conversaoBolsaCollectionSource)
            {
                ConversaoBolsa conversaoBolsaNew = new ConversaoBolsa();
                conversaoBolsaNew.CdAtivoBolsa = conversaoBolsa.CdAtivoBolsa;
                conversaoBolsaNew.CdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino;
                conversaoBolsaNew.DataEx = conversaoBolsa.DataEx;
                conversaoBolsaNew.DataLancamento = conversaoBolsa.DataLancamento;
                conversaoBolsaNew.DataReferencia = conversaoBolsa.DataReferencia;
                conversaoBolsaNew.FatorPU = conversaoBolsa.FatorPU;
                conversaoBolsaNew.FatorQuantidade = conversaoBolsa.FatorQuantidade;
                conversaoBolsaNew.Fonte = conversaoBolsa.Fonte;

                try
                {
                    conversaoBolsaNew.Save();
                }
                catch (Exception)
                {
                }
            }

            GrupamentoBolsaCollection grupamentoBolsaCollectionSource = new GrupamentoBolsaCollection();
            grupamentoBolsaCollectionSource.es.Connection.Name = "FinancialSource";
            grupamentoBolsaCollectionSource.Query.Where(conversaoBolsaCollectionSource.Query.DataLancamento.Equal(data));
            grupamentoBolsaCollectionSource.Query.Load();

            foreach (GrupamentoBolsa grupamentoBolsa in grupamentoBolsaCollectionSource)
            {
                GrupamentoBolsa grupamentoBolsaNew = new GrupamentoBolsa();
                grupamentoBolsaNew.CdAtivoBolsa = grupamentoBolsa.CdAtivoBolsa;
                grupamentoBolsaNew.DataEx = grupamentoBolsa.DataEx;
                grupamentoBolsaNew.DataLancamento = grupamentoBolsa.DataLancamento;
                grupamentoBolsaNew.DataReferencia = grupamentoBolsa.DataReferencia;
                grupamentoBolsaNew.FatorPU = grupamentoBolsa.FatorPU;
                grupamentoBolsaNew.FatorQuantidade = grupamentoBolsa.FatorQuantidade;
                grupamentoBolsaNew.Fonte = grupamentoBolsa.Fonte;
                grupamentoBolsaNew.TipoGrupamento = grupamentoBolsa.TipoGrupamento;

                try
                {
                    grupamentoBolsaNew.Save();
                }
                catch (Exception)
                {
                }
            }

            SubscricaoBolsaCollection subscricaoBolsaCollectionSource = new SubscricaoBolsaCollection();
            subscricaoBolsaCollectionSource.es.Connection.Name = "FinancialSource";
            subscricaoBolsaCollectionSource.Query.Where(subscricaoBolsaCollectionSource.Query.DataLancamento.Equal(data));
            subscricaoBolsaCollectionSource.Query.Load();

            foreach (SubscricaoBolsa subscricaoBolsa in subscricaoBolsaCollectionSource)
            {
                SubscricaoBolsa subscricaoBolsaNew = new SubscricaoBolsa();
                subscricaoBolsaNew.CdAtivoBolsa = subscricaoBolsa.CdAtivoBolsa;
                subscricaoBolsaNew.CdAtivoBolsaDireito = subscricaoBolsa.CdAtivoBolsaDireito;
                subscricaoBolsaNew.DataEx = subscricaoBolsa.DataEx;
                subscricaoBolsaNew.DataFimNegociacao = subscricaoBolsa.DataFimNegociacao;
                subscricaoBolsaNew.DataLancamento = subscricaoBolsa.DataLancamento;
                subscricaoBolsaNew.DataReferencia = subscricaoBolsa.DataReferencia;
                subscricaoBolsaNew.Fator = subscricaoBolsa.Fator;
                subscricaoBolsaNew.Fonte = subscricaoBolsa.Fonte;
                subscricaoBolsaNew.PrecoSubscricao = subscricaoBolsa.PrecoSubscricao;

                try
                {
                    subscricaoBolsaNew.Save();
                }
                catch (Exception)
                {
                }

            }
        }
    }
}
