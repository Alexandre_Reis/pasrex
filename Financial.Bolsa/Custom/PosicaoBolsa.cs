﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Investidor;
using Financial.Bolsa.Enums;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Bolsa.Exceptions;
using System.IO;
using Financial.Util;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Interfaces.Import.Bolsa;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Financial.Investidor.Enums;
using Financial.Tributo.Enums;
using Financial.Interfaces.Sinacor;

namespace Financial.Bolsa
{
    public partial class PosicaoBolsa : esPosicaoBolsa
    {
        #region Adiciona NomeCliente/NomeAgenteMercado como uma propriedade virtual para bind em Design Time
        protected override List<esPropertyDescriptor> GetLocalBindingProperties() {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
            props.Add(new esPropertyDescriptor(this, "NomeCliente", typeof(string)));
            props.Add(new esPropertyDescriptor(this, "NomeAgenteMercado", typeof(string)));
            //
            return props;
        }
        
        private string nomeCliente = "";
        public string NomeCliente {
            get {
                //return this.UpToClienteByIdCliente.Nome;
                //return (string)this.GetColumn("NomeCliente");
                return this.nomeCliente;
            }
            set {
                //this.SetColumn("NomeCliente", value);
                this.nomeCliente = value;
            }
        }

        private string nomeAgenteMercado = "";
        public string NomeAgenteMercado {
            get {
                return this.nomeAgenteMercado;
            }
            set {
                this.nomeAgenteMercado = value;
            }
        }
        #endregion

        /// <summary>
        /// Retorna a quantidade em posição do ativo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadeAtivo(int idCliente, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna a quantidade em posição do ativo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadeAtivo(int idCliente, int idAgente, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                        this.Query.IdAgente.Equal(idAgente));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna a quantidade bloqueada em posição do ativo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaQuantidadeBloqueadaAtivo(int idCliente, int idAgente, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.QuantidadeBloqueada.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa.Like(cdAtivoBolsa + "%"),
                        this.Query.IdAgente.Equal(idAgente));

            this.Query.Load();

            return this.QuantidadeBloqueada.HasValue ? this.QuantidadeBloqueada.Value : 0;
        }

        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// Leva em conta posições doadas e tomadas de aluguel de ações - BTC.
        /// Leva em conta opções vendidas.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idEmissor"></param>
        /// <param name="idSetor"></param>
        /// <returns></returns>
        public decimal RetornaValorMercadoAcaoEnquadra(int idCliente, int? idEmissor, int? idSetor, int? tipoPapel, string tipoMercado, string bloqueado)
        {
            List<esQueryItem> campos = new List<esQueryItem>();

            //BTT = BTC Tomador
            //BTD = BTC Doador
            decimal totalValorMercado = 0;
            if (!tipoMercado.Contains("BT"))
            {
                #region Computa valor de mercado em PosicaoBolsa (levando em conta opções vendidas do mesmo ativo)
                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

                posicaoBolsaCollection.Query
                     .Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                             posicaoBolsaCollection.Query.ValorMercado,
                             posicaoBolsaCollection.Query.QuantidadeBloqueada,
                             posicaoBolsaCollection.Query.PUMercado,
                             posicaoBolsaCollection.Query.Quantidade,
                             posicaoBolsaCollection.Query.IdAgente)
                     .Where(posicaoBolsaCollection.Query.IdCliente == idCliente,
                            posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario),
                            posicaoBolsaCollection.Query.ValorMercado.GreaterThan(0));

                posicaoBolsaCollection.Query.Load();

                for (int i = 0; i < posicaoBolsaCollection.Count; i++)
                {
                    PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[i];
                    string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                    decimal valorMercado = posicaoBolsa.ValorMercado.Value;
                    decimal quantidade = posicaoBolsa.Quantidade.Value;
                    decimal puMercado = posicaoBolsa.PUMercado.Value;
                    int idAgente = posicaoBolsa.IdAgente.Value;
                    decimal fator = posicaoBolsa.Quantidade.Value * posicaoBolsa.PUMercado.Value / posicaoBolsa.ValorMercado.Value;
                    decimal valorMercadoBloqueado = posicaoBolsa.QuantidadeBloqueada.Value * posicaoBolsa.PUMercado.Value / fator;

                    PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
                    AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
                    posicaoBolsaQuery.Select(posicaoBolsaQuery.Quantidade.Sum());
                    posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
                    posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente),
                                            posicaoBolsaQuery.Quantidade.LessThan(0),
                                            posicaoBolsaQuery.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                                            posicaoBolsaQuery.IdAgente.Equal(idAgente),
                                            ativoBolsaQuery.CdAtivoBolsaObjeto.Equal(cdAtivoBolsa));
                    PosicaoBolsa posicaoBolsaOpcoes = new PosicaoBolsa();
                    posicaoBolsaOpcoes.Load(posicaoBolsaQuery);

                    decimal quantidadeOpcoesVendidas = Math.Abs(posicaoBolsaOpcoes.Quantidade.HasValue ? posicaoBolsaOpcoes.Quantidade.Value : 0);

                    bool passou = true;

                    if (idEmissor.HasValue)
                    {
                        #region Busca Emissor
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        campos.Add(ativoBolsa.Query.IdEmissor);
                        ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                        int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                        #endregion

                        if (idEmissor != idEmissorComparar)
                            passou = false;
                    }

                    if (idSetor.HasValue && passou)
                    {
                        #region Busca Setor
                        //Busca antes o IdEmissor
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        campos = new List<esQueryItem>();
                        campos.Add(ativoBolsa.Query.IdEmissor);
                        ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                        int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                        //
                        Emissor emissor = new Emissor();
                        campos = new List<esQueryItem>();
                        campos.Add(emissor.Query.IdSetor);
                        emissor.LoadByPrimaryKey(campos, idEmissorComparar);
                        int idSetorComparar = emissor.IdSetor.Value;
                        #endregion

                        if (idSetor != idSetorComparar)
                            passou = false;
                    }

                    if (tipoPapel.HasValue)
                    {
                        #region Busca Tipo Papel
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        campos = new List<esQueryItem>();
                        campos.Add(ativoBolsa.Query.TipoPapel);
                        ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                        int tipoPapelComparar = ativoBolsa.TipoPapel.Value;
                        #endregion

                        if (tipoPapel != tipoPapelComparar)
                            passou = false;
                    }

                    if (!String.IsNullOrEmpty(tipoMercado))
                    {
                        #region Busca Tipo Mercado
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        campos = new List<esQueryItem>();
                        campos.Add(ativoBolsa.Query.TipoMercado);
                        ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                        string tipoMercadoComparar = ativoBolsa.TipoMercado;
                        #endregion

                        if (tipoMercado != tipoMercadoComparar ||
                            (posicaoBolsa.Quantidade.Value < 0 && tipoMercado == "VPO") || //VPO = Vista > 0
                            (posicaoBolsa.Quantidade.Value > 0 && tipoMercado == "VNE")) //VPO = Vista < 0
                            passou = false;
                    }

                    if (!String.IsNullOrEmpty(bloqueado) && passou)
                    {
                        totalValorMercado += valorMercadoBloqueado;
                    }
                    else if (passou)
                    {
                        if (quantidadeOpcoesVendidas == 0)
                        {
                            totalValorMercado += valorMercado;
                        }
                        else
                        {
                            decimal quantidadeAcoes = quantidade - quantidadeOpcoesVendidas;
                            totalValorMercado += Utilitario.Truncate((quantidadeAcoes * puMercado) / fator, 2);
                        }
                    }

                }
                #endregion
            }

            Cliente cliente = new Cliente();
            campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.ApuraGanhoRV);
            cliente.LoadByPrimaryKey(campos, idCliente);
            DateTime dataDia = cliente.DataDia.Value;

            if (tipoMercado.Contains("BT") || 
                (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraTodas || 
                (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.NaoApura)))
            {
                decimal valorMercadoBTC_Doador = 0;
                if (!tipoMercado.Contains("BT") || tipoMercado == "BTD")
                {
                    #region Computa valor de mercado em PosicaoEmprestimoBolsa (Ponta Doador)
                    PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();

                    posicaoEmprestimoBolsaCollection.Query
                         .Select(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa, posicaoEmprestimoBolsaCollection.Query.ValorMercado)
                         .Where(posicaoEmprestimoBolsaCollection.Query.IdCliente == idCliente,
                                posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador,
                                posicaoEmprestimoBolsaCollection.Query.DataVencimento.GreaterThanOrEqual(dataDia),
                                posicaoEmprestimoBolsaCollection.Query.DataRegistro.LessThanOrEqual(dataDia));
                    posicaoEmprestimoBolsaCollection.Query.Load();

                    for (int i = 0; i < posicaoEmprestimoBolsaCollection.Count; i++)
                    {
                        PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection[i];
                        string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                        decimal valorMercado = posicaoEmprestimoBolsa.ValorMercado.Value;

                        bool passou = true;

                        if (idEmissor.HasValue)
                        {
                            #region Busca Emissor
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            campos = new List<esQueryItem>();
                            campos.Add(ativoBolsa.Query.IdEmissor);
                            ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                            int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                            #endregion

                            if (idEmissor != idEmissorComparar)
                                passou = false;
                        }

                        if (idSetor.HasValue)
                        {
                            #region Busca Setor
                            //Busca antes o IdEmissor
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            campos = new List<esQueryItem>();
                            campos.Add(ativoBolsa.Query.IdEmissor);
                            ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                            int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                            //
                            Emissor emissor = new Emissor();
                            campos = new List<esQueryItem>();
                            campos.Add(emissor.Query.IdSetor);
                            emissor.LoadByPrimaryKey(campos, idEmissorComparar);
                            int idSetorComparar = emissor.IdSetor.Value;
                            #endregion

                            if (idSetor != idSetorComparar)
                                passou = false;
                        }

                        if (tipoPapel.HasValue)
                        {
                            #region Busca Tipo Papel
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            campos = new List<esQueryItem>();
                            campos.Add(ativoBolsa.Query.TipoPapel);
                            ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                            int tipoPapelComparar = ativoBolsa.TipoPapel.Value;
                            #endregion

                            if (tipoPapel != tipoPapelComparar)
                                passou = false;
                        }

                        if (!String.IsNullOrEmpty(tipoMercado) && !tipoMercado.Contains("BT"))
                        {
                            #region Busca Tipo Mercado
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            campos = new List<esQueryItem>();
                            campos.Add(ativoBolsa.Query.TipoMercado);
                            ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                            string tipoMercadoComparar = ativoBolsa.TipoMercado;
                            #endregion

                            if (tipoMercado != tipoMercadoComparar)
                                passou = false;
                        }

                        if (passou)
                        {
                            valorMercadoBTC_Doador += valorMercado;
                        }

                    }
                    #endregion
                }

                if (tipoMercado != "BTD")
                {
                    totalValorMercado += valorMercadoBTC_Doador;
                }
                else
                {
                    totalValorMercado = valorMercadoBTC_Doador;
                }


                decimal valorMercadoBTC_Tomador = 0;
                if (!tipoMercado.Contains("BT") || tipoMercado == "BTT")
                {
                    #region Computa valor de mercado em PosicaoEmprestimoBolsa (Ponta Tomador)
                    PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaTomadorCollection = new PosicaoEmprestimoBolsaCollection();

                    posicaoEmprestimoBolsaTomadorCollection.Query
                         .Select(posicaoEmprestimoBolsaTomadorCollection.Query.CdAtivoBolsa,
                                 posicaoEmprestimoBolsaTomadorCollection.Query.ValorMercado)
                         .Where(posicaoEmprestimoBolsaTomadorCollection.Query.IdCliente == idCliente,
                                posicaoEmprestimoBolsaTomadorCollection.Query.PontaEmprestimo == (byte)PontaEmprestimoBolsa.Tomador,
                                posicaoEmprestimoBolsaTomadorCollection.Query.DataVencimento.GreaterThanOrEqual(dataDia),
                                posicaoEmprestimoBolsaTomadorCollection.Query.DataRegistro.LessThanOrEqual(dataDia));
                    posicaoEmprestimoBolsaTomadorCollection.Query.Load();

                    for (int i = 0; i < posicaoEmprestimoBolsaTomadorCollection.Count; i++)
                    {
                        PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaTomadorCollection[i];
                        string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                        decimal valorMercado = posicaoEmprestimoBolsa.ValorMercado.Value;

                        bool passou = true;

                        if (idEmissor.HasValue)
                        {
                            #region Busca Emissor
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            campos = new List<esQueryItem>();
                            campos.Add(ativoBolsa.Query.IdEmissor);
                            ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                            int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                            #endregion

                            if (idEmissor != idEmissorComparar)
                                passou = false;
                        }

                        if (idSetor.HasValue)
                        {
                            #region Busca Setor
                            //Busca antes o IdEmissor
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            campos = new List<esQueryItem>();
                            campos.Add(ativoBolsa.Query.IdEmissor);
                            ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                            int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                            //
                            Emissor emissor = new Emissor();
                            campos = new List<esQueryItem>();
                            campos.Add(emissor.Query.IdSetor);
                            emissor.LoadByPrimaryKey(campos, idEmissorComparar);
                            int idSetorComparar = emissor.IdSetor.Value;
                            #endregion

                            if (idSetor != idSetorComparar)
                                passou = false;
                        }

                        if (tipoPapel.HasValue)
                        {
                            #region Busca Tipo Papel
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            campos = new List<esQueryItem>();
                            campos.Add(ativoBolsa.Query.TipoPapel);
                            ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                            int tipoPapelComparar = ativoBolsa.TipoPapel.Value;
                            #endregion

                            if (tipoPapel != tipoPapelComparar)
                                passou = false;
                        }

                        if (!String.IsNullOrEmpty(tipoMercado) && !tipoMercado.Contains("BT"))
                        {
                            #region Busca Tipo Mercado
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            campos = new List<esQueryItem>();
                            campos.Add(ativoBolsa.Query.TipoMercado);
                            ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                            string tipoMercadoComparar = ativoBolsa.TipoMercado;
                            #endregion

                            if (tipoMercado != tipoMercadoComparar)
                                passou = false;
                        }

                        if (passou)
                        {
                            valorMercadoBTC_Tomador += valorMercado;
                        }

                    }
                    #endregion
                }

                if (tipoMercado != "BTT")
                {
                    totalValorMercado -= valorMercadoBTC_Tomador;
                }
                else
                {
                    totalValorMercado = valorMercadoBTC_Tomador;
                }
            }

            return totalValorMercado;
        }

        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorMercadoOpcaoEnquadra(int idCliente, string posicao)
        {
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            posicaoBolsaCollection.Query
                 .Select(posicaoBolsaCollection.Query.CdAtivoBolsa, posicaoBolsaCollection.Query.ValorMercado)
                 .Where(posicaoBolsaCollection.Query.IdCliente == idCliente,
                        posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));

            if (posicao == "C")
            {
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.Quantidade.GreaterThan(0));
            }
            else if (posicao == "V")
            {
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.Quantidade.LessThan(0));
            }

            posicaoBolsaCollection.Query.Load();

            decimal totalValorMercado = 0;

            for (int i = 0; i < posicaoBolsaCollection.Count; i++)
            {
                PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[i];
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                totalValorMercado += valorMercado;
            }

            return totalValorMercado;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado de todas as posições de opção de compra/venda com valor maior que 0</returns>
        public decimal RetornaValorMercadoOpcaoPosicaoComprada(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                        this.Query.Quantidade.GreaterThan(0));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado de todas as posições de opção de compra/venda com valor menor que 0</returns>
        public decimal RetornaValorMercadoOpcaoPosicaoVendida(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                        this.Query.Quantidade.LessThan(0));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.ValorMercado.Sum())
                 .Where(this.query.IdCliente == idCliente);

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");

            posicaoBolsaQuery.Select(posicaoBolsaQuery.IdPosicao);
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente),
                                    posicaoBolsaQuery.Quantidade.NotEqual(0),
                                    ativoBolsaQuery.IdMoeda.NotEqual(idMoedaCliente));

            PosicaoBolsaCollection posicaoBolsaCollectionExiste = new PosicaoBolsaCollection();
            posicaoBolsaCollectionExiste.Load(posicaoBolsaQuery);

            if (posicaoBolsaCollectionExiste.Count == 0)
            {
                return this.RetornaValorMercado(idCliente);
            }

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa, posicaoBolsaCollection.Query.ValorMercado.Sum());
            posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente));
            posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
            posicaoBolsaCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoBolsaCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal valor = posicaoBolsa.ValorMercado.Value;

                AtivoBolsa ativoBolsa = new AtivoBolsa();                
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.IdMoeda);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                int idMoedaAtivo = ativoBolsa.IdMoeda.Value;
                
                if (idMoedaCliente != idMoedaAtivo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsa com todos os campos de PosicaoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <param name="cdAtivoBolsa">Codigo do Ativo de Bolsa</param>
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoBolsa(int idCliente, int idAgente, String cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdAgente == idAgente,
                        this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa == cdAtivoBolsa);

            bool retorno = this.Query.Load();
            
            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsa com o campo PUCustoLiquido e Quantidade.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPuLiquido(int idCliente, int idAgente, String cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.PUCustoLiquido,
                         this.Query.Quantidade)
                 .Where(this.Query.IdCliente == idCliente,
                         this.Query.IdAgente == idAgente,
                         this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            bool retorno = this.Query.Load();
            
            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsa com todos os campos de PosicaoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaPosicaoBolsa(int idCliente, String cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            bool retorno = this.Query.Load();
         
            return retorno;
        }

        /// <summary>
        /// Atualiza a posicao do cliente, dada uma posicao, a nova quantidade e o preço.
        /// Call de entity.Save direto na função.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="pu">preço unitário da compra</param>
        /// <param name="puLiquido">puLiquido da compra</param>
        public void AtualizaPosicaoCompraComprado(int idPosicao, decimal quantidade, decimal pu, decimal puLiquido)
        {
            // TODO: tratar quando nao existir     
            try
            {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e)
            {
                throw e;
            }

            decimal totalFinanceiroAntes = this.Quantidade.Value * this.PUCusto.Value;
            decimal totalFinanceiroOperacao = quantidade * pu;
            decimal totalFinanceiroLiquidoAntes = this.Quantidade.Value * this.PUCustoLiquido.Value;
            decimal totalFinanceiroLiquidoOperacao = quantidade * puLiquido;

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            // novo PU - soma totais e divide pela nova quantidade
            this.PUCusto = (totalFinanceiroAntes + totalFinanceiroOperacao) / this.Quantidade;
            this.PUCustoLiquido = (totalFinanceiroLiquidoAntes + totalFinanceiroLiquidoOperacao) / this.Quantidade;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente, dada uma posicao, a nova quantidade e o preço.
        /// Call de entity.Save direto na função.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="puLiquido">puLiquido da compra</param>
        public void AtualizaPosicaoCompraVendido(int idPosicao, decimal quantidade, decimal pu, decimal puLiquido)
        {

            // TODO: tratar quando nao existir     
            try
            {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e)
            {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            this.PUCusto = pu;
            this.PUCustoLiquido = puLiquido;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a quantidade, somando a atual à quantidade passada.
        /// Call de entity.Save direto na função.
        /// </summary>
        /// <param name="idPosicao"></param>
        /// <param name="quantidade"></param>
        public void AtualizaQuantidade(int idPosicao, decimal quantidade)
        {
            // TODO: tratar quando nao existir     
            try
            {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e)
            {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade + quantidade;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Função básica de Inserção na PosicaoBolsa.
        /// Call de entity.Save direto na função.
        /// </summary>
        /// <param name="operacaoBolsa"></param>
        public void InserePosicaoBolsa(OperacaoBolsa operacaoBolsa)
        {
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();

            if (operacaoBolsa.TipoMercado == TipoMercadoBolsa.OpcaoCompra ||
                operacaoBolsa.TipoMercado == TipoMercadoBolsa.OpcaoVenda)
            {
                AtivoBolsa ativoBolsa = new AtivoBolsa();                
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataVencimento);
                campos.Add(ativoBolsa.Query.IdMoeda);
                if (ativoBolsa.LoadByPrimaryKey(campos, operacaoBolsa.CdAtivoBolsa))
                {
                    if (!ativoBolsa.DataVencimento.HasValue)
                    {
                        throw new Exception("Vencimento não caadastrado para a opção " + operacaoBolsa.CdAtivoBolsa);
                    }

                    DateTime dataVencimento = ativoBolsa.DataVencimento.Value;
                    posicaoBolsa.DataVencimento = dataVencimento;

                    if (operacaoBolsa.CdAtivoBolsa.Contains("IBOV"))
                    {
                        posicaoBolsa.DataVencimento = ativoBolsa.RetornaDataVencimentoOpcaoIBOV(operacaoBolsa.Data.Value, dataVencimento);
                    }
                    else if (ativoBolsa.IdMoeda == (int)ListaMoedaFixo.Real)
                    {
                        posicaoBolsa.DataVencimento = ativoBolsa.RetornaDataVencimentoOpcao(operacaoBolsa.Data.Value, dataVencimento);
                    }                   
                }
            }

            posicaoBolsa.IdCliente = operacaoBolsa.IdCliente;
            posicaoBolsa.IdAgente = operacaoBolsa.IdAgenteLiquidacao;
            posicaoBolsa.CdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
            posicaoBolsa.Quantidade = operacaoBolsa.Quantidade;
            posicaoBolsa.PUCustoLiquido = operacaoBolsa.PULiquido;
            posicaoBolsa.QuantidadeInicial = operacaoBolsa.Quantidade;
            posicaoBolsa.ValorCustoLiquido = operacaoBolsa.ValorLiquido;
            posicaoBolsa.PUCusto = operacaoBolsa.Pu;
            posicaoBolsa.TipoMercado = operacaoBolsa.TipoMercado;
            posicaoBolsa.Save();
        }

        /// <summary>
        /// Lança depositos/retiradas para zerar as posições vencidas do dia da PosicaoBolsa.
        /// Joga o resultado (pó) do vencimento nas boletas de Deposito/Retirada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void ProcessaVencimentoOpcao(int idCliente, DateTime data)
        {
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();

            // Busca as posicões de opcões que venceram para dar baixa.
            posicaoBolsaCollection.BuscaPosicaoBolsa(idCliente, data);

            #region Deleta operações com origem = VencimentoOpcao na OperacaoBolsa
            List<int> origemOperacao = new List<int>();
            origemOperacao.Add(OrigemOperacaoBolsa.VencimentoOpcao);

            OperacaoBolsaCollection operacaoBolsaCollectionDeletar = new OperacaoBolsaCollection();
            operacaoBolsaCollectionDeletar.DeletaOperacaoBolsa(idCliente, origemOperacao, data);            
            #endregion

            for (int i = 0; i < posicaoBolsaCollection.Count; i++)
            {
                PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[i];
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                int idAgente = posicaoBolsa.IdAgente.Value;
                decimal quantidadePosicao = posicaoBolsa.Quantidade.Value;
                decimal puMercado = posicaoBolsa.PUMercado.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                decimal quantidadeExercicio = 0;
                #region Verifica exercícios das posições (se tiver, precisa tratar p não gerar resultado de pó)
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                string cdAtivoBolsaObjeto = ativoBolsa.RetornaCdAtivoBolsaObjeto(cdAtivoBolsa);
                if (cdAtivoBolsaObjeto != "")
                {
                    operacaoBolsaCollection.BuscaOperacaoBolsaExercicio(idCliente, data, cdAtivoBolsa, 
                                                                        cdAtivoBolsaObjeto, idAgente);
                    for (int j = 0; j < operacaoBolsaCollection.Count; j++)
                    {
                        OperacaoBolsa operacaoBolsaAux = operacaoBolsaCollection[j];
                        decimal quantidadeOperacao = operacaoBolsaAux.Quantidade.Value;
                        quantidadeExercicio += quantidadeOperacao;
                    }
                }
                #endregion

                string tipoOperacao; 
                if (quantidadePosicao < 0)
                {
                    tipoOperacao = TipoOperacaoBolsa.Deposito;                    
                }
                else
                {
                    tipoOperacao = TipoOperacaoBolsa.Retirada;                    
                }
                decimal quantidadeVencida = Math.Abs(quantidadePosicao);

                if (quantidadeExercicio >= quantidadeVencida)
                {
                    quantidadeVencida = 0;
                }
                else
                {
                    quantidadeVencida -= quantidadeExercicio;
                }

                #region Busca fatorCotacaoBolsa
                FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                decimal fator = 1;
                if (fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                {
                    fator = fatorCotacao.Fator.Value;
                }
                else
                {
                    if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                    {
                        AtivoBolsa ativoBolsaIntegra = new AtivoBolsa();
                        if (!ativoBolsaIntegra.IntegraFatorCotacaoSinacor(cdAtivoBolsa))
                        {
                            throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsa + " " + data.ToString("d"));
                        }
                    }
                }
                #endregion

                if (quantidadeVencida != 0)
                {
                    decimal valorLiquido = Math.Round((quantidadeVencida * posicaoBolsa.PUCustoLiquido.Value) / fator, 2);
                    // Posições vendidas geram resultado positivo no vencimento
                    // Posições compradas geram resultado negativo no vencimento               

                    decimal resultadoRealizado = 0;
                    if (quantidadePosicao > 0)
                    {
                        resultadoRealizado = valorLiquido * (-1);
                    }
                    else
                    {
                        resultadoRealizado = valorLiquido;
                    }

                    #region cria Nova Operacao (não impactará posição, apenas para guardar os resultados do pó)
                    operacaoBolsa = new OperacaoBolsa();
                    operacaoBolsa.TipoMercado = posicaoBolsa.UpToAtivoBolsaByCdAtivoBolsa.TipoMercado;
                    operacaoBolsa.TipoOperacao = tipoOperacao;
                    operacaoBolsa.Data = data;
                    operacaoBolsa.Pu = posicaoBolsa.PUCusto;
                    operacaoBolsa.Quantidade = quantidadeVencida;
                    operacaoBolsa.CdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                    operacaoBolsa.IdAgenteCorretora = posicaoBolsa.IdAgente;
                    operacaoBolsa.IdAgenteLiquidacao = posicaoBolsa.IdAgente;
                    operacaoBolsa.PULiquido = posicaoBolsa.PUCustoLiquido;
                    operacaoBolsa.Valor = 0;
                    operacaoBolsa.DataLiquidacao = data;
                    operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.VencimentoOpcao;
                    operacaoBolsa.PercentualDesconto = 0;
                    operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                    operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                    operacaoBolsa.IdCliente = idCliente;
                    operacaoBolsa.ValorLiquido = 0;
                    operacaoBolsa.ResultadoRealizado = resultadoRealizado;
                    operacaoBolsa.CalculaDespesas = "N";
                    operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                    operacaoBolsa.DataOperacao = data;

                    operacaoBolsa.InsereOperacaoBolsa(operacaoBolsa);
                    #endregion
                }

                posicaoBolsa.Quantidade = 0;
            }

            posicaoBolsaCollection.Save();
        }

        /// <summary>
        ///  Atualiza ValorMercado, ValorCustoLiquido, ResultadoRealizar de uma posicao, dado um cliente 
        ///  e uma data de cotação.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        // TODO: logar
        public void AtualizaValores(int idCliente, DateTime data)
        {
            bool emiteMsgSemCotacaoBolsa = ParametrosConfiguracaoSistema.Bolsa.EmiteMsgSemCotacaoBolsa;

            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();            
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();

            #region Busca o tipo de cotação - média ou fechamento
            ClienteBolsa clienteBolsa = new ClienteBolsa();
            clienteBolsa.LoadByPrimaryKey(idCliente);

            if (!clienteBolsa.es.HasData)
            {
                throw new ClienteBolsaNaoCadastradoException("Cliente " + idCliente + " sem cadastro para Bolsa.");
            }
            int tipoCotacao = clienteBolsa.TipoCotacao.Value;
            #endregion

            // Arraylist com os Ids das posicões zeradas
            List<int> listaPosicaoZerada = new List<int>();
            posicaoBolsaCollection.BuscaPosicaoBolsa(idCliente);

            for (int i = 0; i < posicaoBolsaCollection.Count; i++)
            {
                posicaoBolsa = posicaoBolsaCollection[i];

                if (posicaoBolsa.Quantidade == 0)
                {
                    //Verifica se possui bloqueio
                    if (posicaoBolsa.QuantidadeBloqueada.Value == 0)
                    {
                        listaPosicaoZerada.Add(posicaoBolsa.IdPosicao.Value);
                        continue;
                    }
                }


                #region Busca cotação do dia
                CotacaoBolsa cotacao = new CotacaoBolsa();
                cotacao.BuscaCotacaoBolsa(posicaoBolsa.CdAtivoBolsa, data);

                decimal cotacaoDia;
                if (tipoCotacao == (int)TipoCotacaoBolsa.Medio)
                {
                    if (!cotacao.es.HasData || !cotacao.PUMedio.HasValue)
                    {
                        cotacao = new CotacaoBolsa();
                        cotacao.PUMedio = 0;
                    }
                    cotacaoDia = cotacao.PUMedio.Value;
                }
                else
                {
                    if (!cotacao.es.HasData || !cotacao.PUFechamento.HasValue)
                    {
                        cotacao = new CotacaoBolsa();
                        cotacao.PUFechamento = 0;
                    }
                    cotacaoDia = cotacao.PUFechamento.Value;
                }

                //Se a cotação vier zerada, busca a última disponível (para a situação de feriado na Bovespa)
                if (cotacaoDia == 0)
                {
                    if (emiteMsgSemCotacaoBolsa)
                    {
                        throw new Exception("Cotação não disponível ou zerada para o ativo " + posicaoBolsa.CdAtivoBolsa + " na data " + data.ToShortDateString() + ".");
                    }
                    else
                    {
                        CotacaoBolsa cotacaoAnterior = new CotacaoBolsa();
                        if (cotacaoAnterior.BuscaUltimaCotacaoBolsa(posicaoBolsa.CdAtivoBolsa, data))
                        {
                            if (tipoCotacao == (int)TipoCotacaoBolsa.Medio)
                            {
                                cotacaoDia = cotacaoAnterior.PUMedio.Value;
                            }
                            else if (cotacaoAnterior.PUFechamento.HasValue)
                            {
                                cotacaoDia = cotacaoAnterior.PUFechamento.Value;
                            }                                            
                        }
                    }
                }
                #endregion

                fator.BuscaFatorCotacaoBolsa(posicaoBolsa.CdAtivoBolsa, data);
                
                decimal valorMercado = Utilitario.Truncate((posicaoBolsa.Quantidade.Value * cotacaoDia) / (decimal)fator.Fator, 2);
                decimal valorCustoLiquido = Utilitario.Truncate((posicaoBolsa.Quantidade.Value * posicaoBolsa.PUCustoLiquido.Value) /
                                            (decimal)fator.Fator, 2);
                decimal resultadoRealizar = Utilitario.Truncate((posicaoBolsa.Quantidade.Value *
                                            (cotacaoDia - posicaoBolsa.PUCustoLiquido.Value)) / (decimal)fator.Fator, 2);

                posicaoBolsa.PUMercado = cotacaoDia;
                posicaoBolsa.ValorMercado = valorMercado;
                posicaoBolsa.ValorCustoLiquido = valorCustoLiquido;
                posicaoBolsa.ResultadoRealizar = resultadoRealizar;

            }

            // Update das posicões
            posicaoBolsaCollection.Save();

            // deleta as posicões zeradas
            this.ExcluiPosicaoBolsaZerada(listaPosicaoZerada);
        }

        /// <summary>
        /// Exclui da PosicaoBolsa todas as posições com quantidade = 0.
        /// </summary>
        /// <param name="listaIdPosicao">ids das posições a serem excluidas</param>
        private void ExcluiPosicaoBolsaZerada(List<int> listaIdPosicao)
        {
            // Deleta as posições zeradas sem carregá-las para a memoria                        
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            for (int i = 0; i < listaIdPosicao.Count; i++)
            {
                PosicaoBolsa posicaoBolsa = posicaoBolsaCollection.AddNew();
                posicaoBolsa.IdPosicao = listaIdPosicao[i];
                posicaoBolsa.AcceptChanges();
            }
            posicaoBolsaCollection.MarkAllAsDeleted();
            posicaoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsa com os campos IdPosicao, Quantidade, IdAgente, PUCusto, PUCustoLiquido.
        /// Ordenado por IdPosicao.Ascending.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns>Indica se retornou registro</returns>
        public bool BuscaPosicaoBolsaCliente(int idCliente, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.query.IdPosicao, this.query.Quantidade, this.query.IdAgente,
                         this.query.PUCusto, this.query.PUCustoLiquido)
                 .Where(this.query.IdCliente == idCliente,
                        this.query.CdAtivoBolsa == cdAtivoBolsa)
                 .OrderBy(this.query.IdPosicao.Ascending);

            bool retorno = this.Query.Load();
            
            return retorno;
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            PosicaoBolsaHistoricoCollection posicaoBolsaDeletarHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaDeletarHistoricoCollection.DeletaPosicaoBolsaHistoricoDataHistoricoMaiorIgual(idCliente, data);

            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoBolsaHistorico (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoBolsa WHERE IdCliente = " + idCliente;
            int count = 0;

            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            int columnCount = posicaoBolsaHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoBolsa in posicaoBolsaHistorico.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoBolsa.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoBolsa.Name == PosicaoBolsaHistoricoMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoBolsa.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());

        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCliente, DateTime data)
        {

            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoBolsaAbertura (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoBolsa WHERE IdCliente = " + idCliente;
            int count = 0;

            PosicaoBolsaAbertura posicaoBolsaAbertura = new PosicaoBolsaAbertura();
            int columnCount = posicaoBolsaAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoBolsa in posicaoBolsaAbertura.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoBolsa.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoBolsa.Name == PosicaoBolsaHistoricoMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoBolsa.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());

        }

        /// <summary>
        /// Atualiza a posicao do cliente, dada uma posicao, a nova quantidade e o preço.
        /// Call de entity.Save direto na função.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="pu">preço unitário da compra</param>
        /// <param name="puLiquido">puLiquido da compra</param>
        public void AtualizaPosicaoVendaVendido(int idPosicao, decimal quantidade, decimal pu, decimal puLiquido)
        {
            this.LoadByPrimaryKey(idPosicao);
            //
            decimal moduloQuantidadePosicao = Math.Abs(this.Quantidade.Value);
            decimal totalFinanceiroAntes = moduloQuantidadePosicao * this.PUCusto.Value;
            decimal totalFinanceiroOperacao = quantidade * pu;
            decimal totalFinanceiroLiquidoAntes = moduloQuantidadePosicao * this.PUCustoLiquido.Value;
            decimal totalFinanceiroLiquidoOperacao = quantidade * puLiquido;

            // nova Quantidade - Soma em módulo e multiplica por -1
            this.Quantidade = -1 * (moduloQuantidadePosicao + quantidade);
            // novo PU - soma totais e divide pela nova quantidade
            this.PUCusto = (totalFinanceiroAntes + totalFinanceiroOperacao) / Math.Abs(this.Quantidade.Value);
            this.PUCustoLiquido = (totalFinanceiroLiquidoAntes + totalFinanceiroLiquidoOperacao) / Math.Abs(this.Quantidade.Value);
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Atualiza a posicao do cliente, dada uma posicao, a nova quantidade e o preço.
        /// Call de entity.Save direto na função.
        /// </summary>
        /// <param name="idPosicao">Identificador da Posicao</param>
        /// <param name="quantidade">quantidade do ativo comprada</param>
        /// <param name="pu"></param>
        /// <param name="puLiquido">puLiquido da compra</param>
        public void AtualizaPosicaoVendaComprado(int idPosicao, decimal quantidade, decimal pu, decimal puLiquido)
        {
            // TODO: tratar quando nao existir     
            try
            {
                this.LoadByPrimaryKey(idPosicao);
            }
            catch (Exception e)
            {
                throw e;
            }

            // nova Quantidade
            this.Quantidade = this.Quantidade - quantidade;
            this.PUCusto = pu;
            this.PUCustoLiquido = puLiquido;
            // Salva a nova posicao
            this.Save();
        }

        /// <summary>
        /// Carrega na PosicaoBolsa a partir do arquivo CSGC da CBLC.
        /// </summary>
        /// <param name="nomeArquivo"></param>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// throws CotacaoNaoCadastradaException
        ///        FatorCotacaoNaoCadastradoException
        ///        ClienteNaoCadastradoException
        ///        CodigoClienteBovespaInvalido
        ///        AtivoNaoCadastradoException
        public void CarregaPosicaoCsgd(string nomeArquivo, int idCliente, DateTime data)
        {

            #region Exceção ClienteNaoCadastradoException
            Cliente cliente = new Cliente();
            if (!cliente.LoadByPrimaryKey(idCliente))
            {
                StringBuilder msg = new StringBuilder();
                msg.Append("Cliente não cadastrado: ")
                   .Append(idCliente);
                throw new ClienteNaoCadastradoException(msg.ToString());
            }
            #endregion

            #region GetIdAgente do Arquivo Csgd
            if (!File.Exists(nomeArquivo))
            {
                throw new ArquivoCsgdNaoEncontradoException("Arquivo Csgd: {0} não encontrado" + nomeArquivo);
            }

            StreamReader sr = new StreamReader(nomeArquivo);
            string linha = sr.ReadLine();
            int codigoBovespaAgente = Convert.ToInt32(linha.Substring(6, 4));
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            sr.Close();
            #endregion

            Csgd csgd = new Csgd();

            // Pega as posições do cliente do arquivo de Csgd                            
            CsgdCollection csgdCollection = csgd.ProcessaCsgd(nomeArquivo, data);
            //

            #region Exceção CodigoClienteBovespaInvalido
            CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
            if (!codigoClienteAgente.BuscaCodigoBovespaCliente(idAgente, idCliente))
            {
                StringBuilder msg = new StringBuilder();
                msg.Append("CodigoClienteBovespa na tabela CodigoClienteAgente não existe para o idCliente: ")
                   .Append(idCliente)
                   .Append("; IdAgente: ")
                   .Append(idAgente);
                throw new CodigoClienteBovespaInvalido(msg.ToString());
            }
            #endregion

            int codigoBovespaCliente = codigoClienteAgente.CodigoClienteBovespa.Value;

            CsgdCollection csgdCollectionAux = csgdCollection.FilterByIdCliente(codigoBovespaCliente);

            // Transação                       
            using (esTransactionScope scope = new esTransactionScope())
            {
                // Somente as posicões do idCliente
                for (int i = 0; i < csgdCollectionAux.CollectionCsgd.Count; i++)
                {
                    csgd = csgdCollectionAux.CollectionCsgd[i];
                    //                    
                    if (csgd.IsTipoRegistroIdentificacaoSaldo())
                    {
                        string cdAtivoBolsa = csgd.CdAtivoBolsa;
                        #region Excecao Ativo
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                        {
                            throw new AtivoNaoCadastradoException("Ativo não cadastrado: " + cdAtivoBolsa);
                        }
                        #endregion

                        // Pega a cotação
                        #region GetCotacao
                        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                        if (!cotacaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa))
                        {
                            StringBuilder msg = new StringBuilder();
                            msg.Append("Cotação não cadastrada para o Ativo: ")
                               .Append(cdAtivoBolsa)
                               .Append(" ")
                               .Append("na data: ")
                               .Append(data);
                            throw new CotacaoNaoCadastradaException(msg.ToString());
                        }
                        #endregion
                        // Pega o fatorCotacao
                        #region GetFatorCotacao
                        FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                        if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                        {
                            if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                            {
                                ativoBolsa = new AtivoBolsa();
                                if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsa))
                                {
                                    throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsa + " " + data.ToString("d"));
                                }
                            }
                        }

                        #endregion
                        //                
                        // Verifica se posicaoBolsa ja existe 
                        PosicaoBolsa posicaoBolsaAux = new PosicaoBolsa();
                        if (posicaoBolsaAux.BuscaPosicaoBolsa(idCliente, cdAtivoBolsa))
                        {
                            #region Update
                            posicaoBolsaAux.Quantidade += csgd.Quantidade.Value;
                            decimal valor = (posicaoBolsaAux.Quantidade.Value * cotacaoBolsa.PUMedio.Value) / fator.Fator.Value;
                            posicaoBolsaAux.ValorMercado = valor;
                            posicaoBolsaAux.ValorCustoLiquido = valor;
                            //
                            try
                            {
                                posicaoBolsaAux.Save();
                            }
                            catch (Exception e)
                            {
                                System.Console.WriteLine("Update PosicaoBolsa.idPosicao: " + posicaoBolsaAux.IdPosicao);
                                System.Console.WriteLine(e.Message + e.StackTrace);
                                System.Console.WriteLine(e.Message + e.StackTrace);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Insert
                            #region Copia para PosicaoBolsa
                            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                            posicaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                            //posicaoBolsa.DataVencimento = null;
                            //posicaoBolsa.ResultadoRealizar = 0;
                            posicaoBolsa.Quantidade = csgd.Quantidade.Value;
                            posicaoBolsa.QuantidadeInicial = csgd.Quantidade.Value;
                            posicaoBolsa.IdCliente = idCliente;
                            posicaoBolsa.CdAtivoBolsa = csgd.CdAtivoBolsa;
                            //
                            posicaoBolsa.IdAgente = idAgente;
                            //
                            posicaoBolsa.PUMercado = cotacaoBolsa.PUMedio;
                            posicaoBolsa.PUCusto = cotacaoBolsa.PUMedio;
                            posicaoBolsa.PUCustoLiquido = cotacaoBolsa.PUMedio;
                            //
                            decimal valor = (csgd.Quantidade.Value * cotacaoBolsa.PUMedio.Value) / fator.Fator.Value;
                            posicaoBolsa.ValorMercado = valor;
                            posicaoBolsa.ValorCustoLiquido = valor;
                            try
                            {
                                posicaoBolsa.Save();
                            }
                            catch (Exception e)
                            {
                                System.Console.WriteLine("Insert PosicaoBolsa.idPosicao: " + posicaoBolsa.IdPosicao);
                                System.Console.WriteLine(e.Message + e.StackTrace);
                                System.Console.WriteLine(e.Message + e.StackTrace);
                            }
                            #endregion
                            #endregion
                        }
                    }
                }
                //
                scope.Complete();

            }            
        }

        /// <summary>
        /// Carrega na PosicaoBolsa a partir do arquivo POSR da CBLC.
        /// </summary>
        /// <param name="nomeArquivo"></param>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// throws CotacaoNaoCadastradaException
        ///        ClienteNaoCadastradoException
        ///        AtivoNaoCadastradoException
        ///        CodigoClienteBovespaInvalido
        public void CarregaPosicaoPosr(string nomeArquivo, int idCliente, DateTime data)
        {

            #region Exceção ClienteNaoCadastradoException
            Cliente cliente = new Cliente();
            if (!cliente.LoadByPrimaryKey(idCliente))
            {
                StringBuilder msg = new StringBuilder();
                msg.Append("Cliente não cadastrado: ")
                   .Append(idCliente);
                throw new ClienteNaoCadastradoException(msg.ToString());
            }
            #endregion

            #region GetIdAgente do Arquivo Posr
            if (!File.Exists(nomeArquivo))
            {
                throw new ArquivoPosrNaoEncontradoException("Arquivo Posr: {0} não encontrado" + nomeArquivo);
            }

            StreamReader sr = new StreamReader(nomeArquivo);
            string linha = sr.ReadLine();
            int codigoBovespaAgente = Convert.ToInt32(linha.Substring(6, 4));
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            sr.Close();
            #endregion

            Posr posr = new Posr();

            // Para insercao posterior da collection
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            // Pega as posições do cliente do arquivo de Posr                            
            PosrCollection posrCollection = posr.ProcessaPosr(nomeArquivo, data);

            #region Exceção CodigoClienteBovespaInvalido
            CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
            if (!codigoClienteAgente.BuscaCodigoBovespaCliente(idAgente, idCliente))
            {
                StringBuilder msg = new StringBuilder();
                msg.Append("CodigoClienteBovespa na tabela CodigoClienteAgente não existe para o idCliente: ")
                   .Append(idCliente)
                   .Append("; IdAgente: ")
                   .Append(idAgente);
                throw new CodigoClienteBovespaInvalido(msg.ToString());
            }
            #endregion

            int codigoBovespaCliente = codigoClienteAgente.CodigoClienteBovespa.Value;

            // Filtro pelo IdCliente
            PosrCollection posrCollectionAux = posrCollection.FilterByIdCliente(codigoBovespaCliente);

            int? codigoAgente = null;
            for (int i = 0; i < posrCollectionAux.CollectionPosr.Count; i++)
            {
                posr = posrCollectionAux.CollectionPosr[i];
                //                    
                if (posr.IsTipoRegistroHeader())
                {
                    // Pega o IdAgente
                    codigoAgente = posr.CodigoAgente.Value;
                }
                else if (posr.IsTipoRegistroIdentificacaoPosicaoRegistrada())
                {
                    string cdAtivoBolsa = posr.CdAtivoBolsa;
                    #region Excecao Ativo
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    string tipoMercado;
                    if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                    {
                        throw new AtivoNaoCadastradoException("Ativo não cadastrado: " + cdAtivoBolsa);
                    }
                    else
                    {
                        tipoMercado = ativoBolsa.TipoMercado;
                    }
                    #endregion

                    // Pega a cotação                    
                    #region GetCotacao
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    if (!cotacaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa))
                    {
                        StringBuilder msg = new StringBuilder();
                        msg.Append("Cotação não cadastrada para o Ativo: ")
                           .Append(cdAtivoBolsa)
                           .Append(" ")
                           .Append("na data: ")
                           .Append(data);
                        throw new CotacaoNaoCadastradaException(msg.ToString());
                    }
                    #endregion

                    #region Copia para PosicaoBolsa
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    posicaoBolsa.TipoMercado = tipoMercado;
                    posicaoBolsa.DataVencimento = posr.DataVencimento.Value;
                    posicaoBolsa.ResultadoRealizar = 0;
                    posicaoBolsa.Quantidade = posr.Quantidade.Value;
                    posicaoBolsa.QuantidadeInicial = posr.Quantidade.Value;
                    posicaoBolsa.IdCliente = idCliente;
                    posicaoBolsa.CdAtivoBolsa = posr.CdAtivoBolsa;
                    //
                    posicaoBolsa.IdAgente = idAgente;
                    //
                    posicaoBolsa.PUMercado = cotacaoBolsa.PUMedio;
                    posicaoBolsa.PUCusto = cotacaoBolsa.PUMedio;
                    posicaoBolsa.PUCustoLiquido = cotacaoBolsa.PUMedio;
                    //
                    decimal valor = (posr.Quantidade.Value * cotacaoBolsa.PUMedio.Value) / posr.FatorCotacao.Value;
                    posicaoBolsa.ValorMercado = valor;
                    posicaoBolsa.ValorCustoLiquido = valor;
                    #endregion

                    posicaoBolsaCollection.AttachEntity(posicaoBolsa);
                }
            }

            #region Salva a collection de Posicao
            try
            {
                posicaoBolsaCollection.Save();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion
        }

        /// <summary>
        /// Carrega na PosicaoBolsa a partir do arquivo POSR da CBLC.
        /// </summary>
        /// <param name="posrCollection"></param>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// throws CotacaoNaoCadastradaException
        ///        ClienteNaoCadastradoException
        ///        AtivoNaoCadastradoException
        ///        CodigoClienteBovespaInvalido
        public void CarregaPosicaoPosr(PosrCollection posrCollection, DateTime data, int idCliente) 
        {

            #region Exceção ClienteNaoCadastradoException
            Cliente cliente = new Cliente();
            if (!cliente.LoadByPrimaryKey(idCliente)) {
                StringBuilder msg = new StringBuilder();
                msg.Append("Cliente não cadastrado: ")
                   .Append(idCliente);
                throw new ClienteNaoCadastradoException(msg.ToString());
            }
            #endregion

            #region GetIdAgente do Arquivo Posr
            int codigoBovespaAgente = posrCollection.CollectionPosr[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            Posr posr = new Posr();

            // Para insercao posterior da collection
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            // Pega as posições do cliente do arquivo de Posr                            

            #region Exceção CodigoClienteBovespaInvalido
            CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
            if (!codigoClienteAgente.BuscaCodigoBovespaCliente(idAgente, idCliente)) {
                StringBuilder msg = new StringBuilder();
                msg.Append("CodigoClienteBovespa na tabela CodigoClienteAgente não existe para o idCliente: ")
                   .Append(idCliente)
                   .Append("; IdAgente: ")
                   .Append(idAgente);
                throw new CodigoClienteBovespaInvalido(msg.ToString());
            }
            #endregion

            int codigoBovespaCliente = codigoClienteAgente.CodigoClienteBovespa.Value;

            // Filtro pelo IdCliente
            PosrCollection posrCollectionAux = posrCollection.FilterByIdCliente(codigoBovespaCliente);

            int? codigoAgente = null;
            for (int i = 0; i < posrCollectionAux.CollectionPosr.Count; i++) {
                posr = posrCollectionAux.CollectionPosr[i];
                //                    
                if (posr.IsTipoRegistroHeader()) {
                    // Pega o IdAgente
                    codigoAgente = posr.CodigoAgente.Value;
                }
                else if (posr.IsTipoRegistroIdentificacaoPosicaoRegistrada()) {
                    string cdAtivoBolsa = posr.CdAtivoBolsa;
                    #region Excecao Ativo
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    string tipoMercado;
                    if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa)) {
                        throw new AtivoNaoCadastradoException("Ativo não cadastrado: " + cdAtivoBolsa);
                    }
                    else {
                        tipoMercado = ativoBolsa.TipoMercado;
                    }
                    #endregion

                    // Pega a cotação                    
                    #region GetCotacao
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    if (!cotacaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa)) {
                        StringBuilder msg = new StringBuilder();
                        msg.Append("Cotação não cadastrada para o Ativo: ")
                           .Append(cdAtivoBolsa)
                           .Append(" ")
                           .Append("na data: ")
                           .Append(data);
                        throw new CotacaoNaoCadastradaException(msg.ToString());
                    }
                    #endregion

                    #region Copia para PosicaoBolsa
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    posicaoBolsa.TipoMercado = tipoMercado;
                    posicaoBolsa.DataVencimento = posr.DataVencimento.Value;
                    posicaoBolsa.ResultadoRealizar = 0;
                    posicaoBolsa.Quantidade = posr.Quantidade.Value;
                    posicaoBolsa.QuantidadeInicial = posr.Quantidade.Value;
                    posicaoBolsa.IdCliente = idCliente;
                    posicaoBolsa.CdAtivoBolsa = posr.CdAtivoBolsa;
                    //
                    posicaoBolsa.IdAgente = idAgente;
                    //
                    posicaoBolsa.PUMercado = cotacaoBolsa.PUMedio;
                    posicaoBolsa.PUCusto = cotacaoBolsa.PUMedio;
                    posicaoBolsa.PUCustoLiquido = cotacaoBolsa.PUMedio;
                    //
                    decimal valor = (posr.Quantidade.Value * cotacaoBolsa.PUMedio.Value) / posr.FatorCotacao.Value;
                    posicaoBolsa.ValorMercado = valor;
                    posicaoBolsa.ValorCustoLiquido = valor;
                    #endregion

                    posicaoBolsaCollection.AttachEntity(posicaoBolsa);
                }
            }

            #region Salva a collection de Posicao
            try {
                posicaoBolsaCollection.Save();
            }
            catch (System.Data.SqlClient.SqlException e) {
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion
        }

        /// <summary>
        /// Verifica se o cliente cobra ou não taxa de custodia
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaTaxaCustodia(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            // Somente executa se estiver no ultimo dia util do mes
            if (Calendario.IsUltimoDiaUtilMes(data))
            {
                TabelaTaxaCustodiaBolsaCollection tabelaTaxaCustodiaBolsaCollection = new TabelaTaxaCustodiaBolsaCollection();
                tabelaTaxaCustodiaBolsaCollection.BuscaTabelaTaxaCustodiaBolsa(idCliente, data);
                //

                // Nova Lista que terá os elementos corretos
                TabelaTaxaCustodiaBolsaCollection tabelaTaxaCustodiaBolsaCollectionAux = new TabelaTaxaCustodiaBolsaCollection();
                #region Exclui IdAgentes duplicados
                for (int i = 0; i < tabelaTaxaCustodiaBolsaCollection.Count; i++)
                {
                    TabelaTaxaCustodiaBolsa tabelaTaxaCustodiaBolsa = (TabelaTaxaCustodiaBolsa)tabelaTaxaCustodiaBolsaCollection[i];
                    int idAgente = tabelaTaxaCustodiaBolsa.IdAgente.Value;

                    // Se for o primeiro elemento insere
                    if (i == 0)
                    {
                        tabelaTaxaCustodiaBolsaCollectionAux.AttachEntity(tabelaTaxaCustodiaBolsa);
                    }
                    else
                    {
                        // Se idAgente != IdAgenteAnterior então insere o Objeto
                        int idAgenteAnterior = ((TabelaTaxaCustodiaBolsa)tabelaTaxaCustodiaBolsaCollection[i - 1]).IdAgente.Value;
                        if (idAgente != idAgenteAnterior)
                        {
                            tabelaTaxaCustodiaBolsaCollectionAux.AttachEntity(tabelaTaxaCustodiaBolsa);
                        }
                    }
                }
                #endregion

                // Collection com a data mais recente para cada idAgente
                #region Verifica se numeroPosicoesMes >= numeroDiasCustodia
                for (int i = 0; i < tabelaTaxaCustodiaBolsaCollectionAux.Count; i++)
                {
                    TabelaTaxaCustodiaBolsa tabelaTaxaCustodiaBolsa = (TabelaTaxaCustodiaBolsa)tabelaTaxaCustodiaBolsaCollectionAux[i];
                    int idAgente = tabelaTaxaCustodiaBolsa.IdAgente.Value;
                    int numeroDiasCustodia = tabelaTaxaCustodiaBolsa.NumeroDiasCustodia.Value;
                    decimal valorCustodia = tabelaTaxaCustodiaBolsa.Valor.Value;
                    //                                                                                                                                                                                                    
                    PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                    int numeroPosicoesMes = posicaoBolsaHistorico.BuscaNumeroPosicoesMes(idCliente, idAgente, data);
                    // Lancamento de uma Liquidacao
                    if (numeroPosicoesMes >= numeroDiasCustodia)
                    {
                        #region Nova Liquidacao
                        DateTime dataVencimento = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                        //                    
                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.DataVencimento = dataVencimento;
                        liquidacao.DataLancamento = data;
                        liquidacao.Descricao = "Taxa de Custódia CBLC";
                        liquidacao.Valor = valorCustodia;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Bolsa.TaxaCustodia;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdAgente = idAgente;
                        liquidacao.IdCliente = idCliente;
                        #endregion

                        liquidacaoCollection.AttachEntity(liquidacao);
                    }
                }
                #endregion

                // Insert de Liquidacao
                liquidacaoCollection.Save();
            }
        }

        /// <summary>
        /// Consolida o custo aberto por agente de custódia em um único custo médio por papel.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ConsolidaCustoMedio(int idCliente, DateTime data)
        {
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            PosicaoBolsaCollection posicaoBolsaCollectionCusto = new PosicaoBolsaCollection();
            posicaoBolsaCollection.BuscaPosicaoConsolidado(idCliente);

            for (int i = 0; i < posicaoBolsaCollection.Count; i++)
            {
                PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[i];

                #region Carrega as variáveis da collection
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                int cdAtivoBolsaCount = (int)posicaoBolsa.GetColumn("CdAtivoBolsaCount");
                decimal quantidade = (decimal)posicaoBolsa.GetColumn("QuantidadeSum");
                decimal custo = (decimal)posicaoBolsa.GetColumn("CustoSum");
                decimal custoLiquido = (decimal)posicaoBolsa.GetColumn("CustoLiquidoSum");
                #endregion

                //Atualiza apenas para quando o idCliente tiver mais de 1 posição do mesmo ativo em agentes diferentes
                if (cdAtivoBolsaCount > 1)
                {
                    //Os PUs são calculados de forma consolidada para o idCliente (independente de agente de custódia)
                    decimal puCustoMedio = custo / quantidade;
                    decimal puCustoLiquidoMedio = custoLiquido / quantidade;

                    #region Busca fatorCotacao para a data
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    if (!fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                    {
                        if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                        {
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsa))
                            {
                                throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsa + " " + data.ToString("d"));
                            }
                        }                        
                    }
                    decimal fatorCotacao = fatorCotacaoBolsa.Fator.Value;
                    #endregion

                    #region São atualizados os PUCusto, PUCustoLiquido e ValorCustoLiquido das posições abertas por agente
                    posicaoBolsaCollectionCusto = new PosicaoBolsaCollection();
                    posicaoBolsaCollectionCusto.BuscaPosicaoCusto(idCliente, cdAtivoBolsa);
                    for (int j = 0; j < posicaoBolsaCollectionCusto.Count; j++)
                    {
                        PosicaoBolsa posicaoBolsaCusto = posicaoBolsaCollectionCusto[j];
                        decimal quantidadePosicao = posicaoBolsaCusto.Quantidade.Value;
                        decimal valorCustoLiquidoMedio = Utilitario.Truncate(puCustoLiquidoMedio * quantidadePosicao / (decimal)fatorCotacao, 2);

                        posicaoBolsaCusto.PUCusto = puCustoMedio;
                        posicaoBolsaCusto.PUCustoLiquido = puCustoLiquidoMedio;
                        posicaoBolsaCusto.ValorCustoLiquido = valorCustoLiquidoMedio;
                    }
                    //Atualiza a posicaoBolsaCollectionCusto
                    posicaoBolsaCollectionCusto.Save();
                    #endregion
                }
            }
        }

        /// <summary>
        /// Soma do valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>Soma do valor de mercado de todas as posições de ações. 
        ///          Retorna 0 se não houver registros
        /// </returns>
        public decimal RetornaValorMercadoAcoes(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Soma do valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>Soma do valor de mercado de todas as posições de ações. 
        ///          Retorna 0 se não houver registros
        /// </returns>
        public decimal RetornaValorMercadoOpcoes(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Gera transferências automáticas a partir do custodiante indicado para ações e opções.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraTransferenciaAutomatica(int idCliente, DateTime data)
        {
            ClienteBolsa clienteBolsa = new ClienteBolsa();
            clienteBolsa.Query.Select(clienteBolsa.Query.IdCustodianteAcoes,
                                      clienteBolsa.Query.IdCustodianteOpcoes);
            clienteBolsa.Query.Where(clienteBolsa.Query.IdCliente.Equal(idCliente));
            clienteBolsa.Query.Load();

            if (clienteBolsa.IdCustodianteAcoes.HasValue || clienteBolsa.IdCustodianteOpcoes.HasValue)
            {
                TransferenciaBolsaCollection transferenciaBolsaCollectionDeletar = new TransferenciaBolsaCollection();
                transferenciaBolsaCollectionDeletar.Query.Select(transferenciaBolsaCollectionDeletar.Query.IdTransferencia);
                transferenciaBolsaCollectionDeletar.Query.Where(transferenciaBolsaCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                                transferenciaBolsaCollectionDeletar.Query.Data.Equal(data));
                transferenciaBolsaCollectionDeletar.Query.Load();
                transferenciaBolsaCollectionDeletar.MarkAllAsDeleted();
                transferenciaBolsaCollectionDeletar.Save();

                if (clienteBolsa.IdCustodianteAcoes.HasValue)
                {
                    int idCustodianteAcoes = clienteBolsa.IdCustodianteAcoes.Value;

                    PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                    posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                        posicaoBolsaCollection.Query.IdAgente,
                                                        posicaoBolsaCollection.Query.Quantidade,
                                                        posicaoBolsaCollection.Query.PUCustoLiquido);
                    posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                       posicaoBolsaCollection.Query.IdAgente.NotEqual(idCustodianteAcoes),
                                                       posicaoBolsaCollection.Query.IdCliente.Equal(idCliente));
                    posicaoBolsaCollection.Query.Load();

                    foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                    {
                        string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                        int idAgente = posicaoBolsa.IdAgente.Value;
                        decimal puCustoLiquido = posicaoBolsa.PUCustoLiquido.Value;
                        decimal quantidade = posicaoBolsa.Quantidade.Value;

                        TransferenciaBolsa transferenciaBolsa = new TransferenciaBolsa();
                        transferenciaBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        transferenciaBolsa.Data = data;
                        transferenciaBolsa.IdAgenteDestino = idCustodianteAcoes;
                        transferenciaBolsa.IdAgenteOrigem = idAgente;
                        transferenciaBolsa.IdCliente = idCliente;
                        transferenciaBolsa.Pu = puCustoLiquido;
                        transferenciaBolsa.Quantidade = quantidade;
                        transferenciaBolsa.Save();
                    }                    
                }

                if (clienteBolsa.IdCustodianteOpcoes.HasValue)
                {
                    int idCustodianteOpcoes = clienteBolsa.IdCustodianteOpcoes.Value;

                    PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                    posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                        posicaoBolsaCollection.Query.IdAgente,
                                                        posicaoBolsaCollection.Query.Quantidade,
                                                        posicaoBolsaCollection.Query.PUCustoLiquido);
                    posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra,
                                                                                                   TipoMercadoBolsa.OpcaoVenda),
                                                       posicaoBolsaCollection.Query.IdAgente.NotEqual(idCustodianteOpcoes),
                                                       posicaoBolsaCollection.Query.IdCliente.Equal(idCliente));
                    posicaoBolsaCollection.Query.Load();

                    foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                    {
                        string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                        int idAgente = posicaoBolsa.IdAgente.Value;
                        decimal puCustoLiquido = posicaoBolsa.PUCustoLiquido.Value;
                        decimal quantidade = posicaoBolsa.Quantidade.Value;

                        TransferenciaBolsa transferenciaBolsa = new TransferenciaBolsa();
                        transferenciaBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        transferenciaBolsa.Data = data;
                        transferenciaBolsa.IdAgenteDestino = idCustodianteOpcoes;
                        transferenciaBolsa.IdAgenteOrigem = idAgente;
                        transferenciaBolsa.IdCliente = idCliente;
                        transferenciaBolsa.Pu = puCustoLiquido;
                        transferenciaBolsa.Quantidade = quantidade;
                        transferenciaBolsa.Save();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="idTitulo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaPuAjustadoPosicao(int idCliente, string cdAtivoBolsa, DateTime data, DateTime dataHistorico, bool historico)
        {
            decimal pu = 0;

            if (historico)
            {
                PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
                pu = posicaoBolsaHistorico.RetornaPuPosicaoHistorico(idCliente, cdAtivoBolsa, data);

            }
            else
            {
                PosicaoBolsaCollection p = new PosicaoBolsaCollection();
                p.Query.Select(p.Query.PUMercado)
                       .Where(p.Query.IdCliente == idCliente &&
                              p.Query.CdAtivoBolsa == cdAtivoBolsa);

                if (p.Query.Load())
                {
                    if (p.Count > 0)
                    {
                        pu = p[0].PUMercado.Value;
                    }
                }
            }

            if (pu != 0)
            {
                ProventoBolsa proventoBolsa = new ProventoBolsa();
                proventoBolsa.Query.Select(proventoBolsa.Query.Valor.Sum())
                            .Where(proventoBolsa.Query.DataEx <= data &
                                   proventoBolsa.Query.DataEx > dataHistorico &
                                   proventoBolsa.Query.CdAtivoBolsa == cdAtivoBolsa);
                proventoBolsa.Query.Load();

                if (proventoBolsa.Valor.HasValue)
                {
                    pu += proventoBolsa.Valor.Value;
                }

                BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
                bonificacaoBolsaCollection.Query.Select(bonificacaoBolsaCollection.Query.Percentual)
                            .Where(bonificacaoBolsaCollection.Query.DataEx <= data &
                                   bonificacaoBolsaCollection.Query.DataEx > dataHistorico &
                                   bonificacaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);
                bonificacaoBolsaCollection.Query.Load();

                foreach (BonificacaoBolsa bonificacaoBolsa in bonificacaoBolsaCollection)
                {
                    pu = pu / (1M / (1M + (bonificacaoBolsa.Percentual.Value / 100M)));
                }

                GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
                grupamentoBolsaCollection.Query.Select(grupamentoBolsaCollection.Query.TipoGrupamento,
                                                       grupamentoBolsaCollection.Query.FatorPU)
                            .Where(grupamentoBolsaCollection.Query.DataEx <= data &
                                   grupamentoBolsaCollection.Query.DataEx > dataHistorico &
                                   grupamentoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);
                grupamentoBolsaCollection.Query.Load();

                foreach (GrupamentoBolsa grupamentoBolsa in grupamentoBolsaCollection)
                {
                    if (grupamentoBolsa.TipoGrupamento.Value == (byte)TipoGrupamentoBolsa.Grupamento)
                    {
                        pu = pu * grupamentoBolsa.FatorPU.Value;
                    }
                    else
                    {
                        pu = pu / grupamentoBolsa.FatorPU.Value;
                    }
                }
            }

            return pu;
        }

        /// <summary>
        /// Importa as posições a partir da VCFPOSICAO (Acoes, opcoes e termos).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ImportaPosicoesSinacor(int idCliente, DateTime data)
        {
            int codigoCliente = 0;
            int codigoCliente2 = 0;
            #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
            ClienteBolsa clienteBolsa = new ClienteBolsa();

            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!clienteBolsa.LoadByPrimaryKey(idCliente))
            {
                throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ Cliente ");
            }

            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor) && String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ IdCliente ");
            }

            //Se nenhum dos 2 códigos é número, lança exception
            if (!Utilitario.IsInteger(clienteBolsa.CodigoSinacor) && !Utilitario.IsInteger(clienteBolsa.CodigoSinacor2))
            {
                throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ o IdCliente ");
            }

            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = Convert.ToInt32(clienteBolsa.CodigoSinacor);
            }

            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = Convert.ToInt32(clienteBolsa.CodigoSinacor2);
            }

            //Se algum dos 2 códigos for nulo, repete o código de um para ao outro
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = codigoCliente2;
            }

            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = codigoCliente;
            }
            #endregion

            //Busca o IdAgenteMercado default
            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
            AgenteMercado agenteMercado = new AgenteMercado();
            int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

            #region Ações e Opções
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            VcfposicaoCollection vcfposicaoCollection = new VcfposicaoCollection();
            vcfposicaoCollection.es.Connection.Name = "Sinacor";
            vcfposicaoCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            vcfposicaoCollection.Query.Select(vcfposicaoCollection.Query.CodNeg,
                                              vcfposicaoCollection.Query.DataVenc,
                                              vcfposicaoCollection.Query.TipoMerc,
                                              vcfposicaoCollection.Query.QtdeTot.Sum());
            vcfposicaoCollection.Query.Where(
                                 (vcfposicaoCollection.Query.CodCli == codigoCliente | vcfposicaoCollection.Query.CodCli == codigoCliente2) &
                                 (vcfposicaoCollection.Query.TipoMerc.Like("VIS%") |
                                  vcfposicaoCollection.Query.TipoMerc.Like("OPC%") |
                                  vcfposicaoCollection.Query.TipoMerc.Like("OPV%"))
                                );
            vcfposicaoCollection.Query.Where(vcfposicaoCollection.Query.TipoGrup == "ACAO");
            vcfposicaoCollection.Query.GroupBy(vcfposicaoCollection.Query.CodNeg, vcfposicaoCollection.Query.DataVenc);
            vcfposicaoCollection.Query.Load();

            foreach (Vcfposicao vcfposicao in vcfposicaoCollection)
            {
                string cdAtivoBolsa = vcfposicao.CodNeg;
                decimal quantidade = vcfposicao.QtdeTot.Value;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.Query.Select(ativoBolsa.Query.TipoMercado,
                                        ativoBolsa.Query.DataVencimento);
                ativoBolsa.Query.Where(ativoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                ativoBolsa.Query.Load();

                decimal cotacaoFechamento = 0;
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                cotacaoBolsa.Query.Select(cotacaoBolsa.Query.PUFechamento);
                cotacaoBolsa.Query.Where(cotacaoBolsa.Query.Data.Equal(data),
                                         cotacaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                if (cotacaoBolsa.Query.Load())
                {
                    cotacaoFechamento = cotacaoBolsa.PUFechamento.Value;
                }

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                decimal valorMercado = Utilitario.Truncate(quantidade * cotacaoFechamento / fatorCotacaoBolsa.Fator.Value, 2);

                PosicaoBolsa posicaoBolsa = posicaoBolsaCollection.AddNew();
                posicaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                posicaoBolsa.DataVencimento = ativoBolsa.DataVencimento;
                posicaoBolsa.IdAgente = idAgenteMercado;
                posicaoBolsa.IdCliente = idCliente;
                posicaoBolsa.PUCusto = cotacaoFechamento;
                posicaoBolsa.PUCustoLiquido = cotacaoFechamento;
                posicaoBolsa.PUMercado = cotacaoFechamento;
                posicaoBolsa.Quantidade = quantidade;
                posicaoBolsa.QuantidadeBloqueada = 0;
                posicaoBolsa.QuantidadeInicial = quantidade;
                posicaoBolsa.ResultadoRealizar = 0;
                posicaoBolsa.TipoMercado = ativoBolsa.TipoMercado;
                posicaoBolsa.ValorCustoLiquido = valorMercado;
                posicaoBolsa.ValorMercado = valorMercado;
            }

            posicaoBolsaCollection.Save();
            #endregion

            #region Termos de Ações
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();

            vcfposicaoCollection = new VcfposicaoCollection();
            vcfposicaoCollection.es.Connection.Name = "Sinacor";
            vcfposicaoCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            vcfposicaoCollection.Query.Select(vcfposicaoCollection.Query.CodNeg,
                                              vcfposicaoCollection.Query.DataVenc,
                                              vcfposicaoCollection.Query.QtdeTot.Sum());
            vcfposicaoCollection.Query.Where((vcfposicaoCollection.Query.CodCli == codigoCliente | vcfposicaoCollection.Query.CodCli == codigoCliente2) &
                                             (vcfposicaoCollection.Query.TipoMerc.Like("TER%")));
            vcfposicaoCollection.Query.Where(vcfposicaoCollection.Query.TipoGrup == "ACAO");
            vcfposicaoCollection.Query.GroupBy(vcfposicaoCollection.Query.CodNeg, vcfposicaoCollection.Query.DataVenc);
            vcfposicaoCollection.Query.Load();

            foreach (Vcfposicao vcfposicao in vcfposicaoCollection)
            {
                string cdAtivoBolsa = vcfposicao.CodNeg;
                decimal quantidade = vcfposicao.QtdeTot.Value;
                DateTime dataVencimento = vcfposicao.DataVenc.Value;

                string cdAtivoBolsaTermo = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsa, dataVencimento);

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.Query.Select(ativoBolsa.Query.TipoMercado,
                                        ativoBolsa.Query.DataVencimento);
                ativoBolsa.Query.Where(ativoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsaTermo));
                ativoBolsa.Query.Load();

                decimal cotacaoFechamento = 0;
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                cotacaoBolsa.Query.Select(cotacaoBolsa.Query.PUFechamento);
                cotacaoBolsa.Query.Where(cotacaoBolsa.Query.Data.Equal(data),
                                         cotacaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                if (cotacaoBolsa.Query.Load())
                {
                    cotacaoFechamento = cotacaoBolsa.PUFechamento.Value;
                }

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                decimal valorMercado = Utilitario.Truncate(quantidade * cotacaoFechamento / fatorCotacaoBolsa.Fator.Value, 2);

                PosicaoTermoBolsa posicaoTermoBolsa = posicaoTermoBolsaCollection.AddNew();
                posicaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsaTermo;
                posicaoTermoBolsa.DataOperacao = data;
                posicaoTermoBolsa.DataVencimento = ativoBolsa.DataVencimento.Value;
                posicaoTermoBolsa.IdAgente = idAgenteMercado;
                posicaoTermoBolsa.IdCliente = idCliente;
                posicaoTermoBolsa.PrazoDecorrer = 0;
                posicaoTermoBolsa.PrazoTotal = 0;
                posicaoTermoBolsa.PUCustoLiquidoAcao = 0;
                posicaoTermoBolsa.PUMercado = cotacaoFechamento;
                posicaoTermoBolsa.PUTermo = cotacaoFechamento;
                posicaoTermoBolsa.PUTermoLiquido = cotacaoFechamento;
                posicaoTermoBolsa.Quantidade = quantidade;
                posicaoTermoBolsa.QuantidadeInicial = quantidade;
                posicaoTermoBolsa.RendimentoApropriado = 0;
                posicaoTermoBolsa.RendimentoApropriar = 0;
                posicaoTermoBolsa.RendimentoDia = 0;
                posicaoTermoBolsa.RendimentoTotal = 0;
                posicaoTermoBolsa.TaxaAno = 0;
                posicaoTermoBolsa.TaxaMTM = 0;
                posicaoTermoBolsa.TipoTermo = (byte)TipoTermoBolsa.Normal;
                posicaoTermoBolsa.ValorCorrigido = valorMercado;
                posicaoTermoBolsa.ValorCurva = valorMercado;
                posicaoTermoBolsa.ValorMercado = valorMercado;
                posicaoTermoBolsa.ValorTermo = valorMercado;
                posicaoTermoBolsa.ValorTermoLiquido = valorMercado;
            }

            posicaoTermoBolsaCollection.Save();
            #endregion

        }

        /// <summary>
        /// Reset de custo para contábil.
        /// </summary>
        public void ResetaCusto(int idCliente, DateTime data)
        {
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente));
            posicaoBolsaCollection.Query.Load();

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                posicaoBolsa.PUCustoLiquido = posicaoBolsa.PUMercado.Value;
            }

            posicaoBolsaCollection.Save();
        }
    }
}
