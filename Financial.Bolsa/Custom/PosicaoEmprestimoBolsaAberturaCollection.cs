﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa
{
	public partial class PosicaoEmprestimoBolsaAberturaCollection : esPosicaoEmprestimoBolsaAberturaCollection
	{
        // Construtor
        // Cria uma nova PosicaoEmprestimoBolsaAberturaCollection com os dados de PosicaoEmprestimoBolsaCollection
        // Todos os dados de PosicaoEmprestimoBolsaCollection são copiados, adicionando a dataHistorico
        public PosicaoEmprestimoBolsaAberturaCollection(PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection, DateTime dataHistorico)
        {
            for (int i = 0; i < posicaoEmprestimoBolsaCollection.Count; i++)
            {
                PosicaoEmprestimoBolsaAbertura p = new PosicaoEmprestimoBolsaAbertura();

                // Para cada Coluna de PosicaoEmprestimoBolsa copia para PosicaoEmprestimoBolsaAbertura
                foreach (esColumnMetadata colPosicao in posicaoEmprestimoBolsaCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas 
                    esColumnMetadata colPosicaoEmprestimoBolsaAbertura = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                    if (posicaoEmprestimoBolsaCollection[i].GetColumn(colPosicao.Name) != null)
                    {
                        p.SetColumn(colPosicaoEmprestimoBolsaAbertura.Name, posicaoEmprestimoBolsaCollection[i].GetColumn(colPosicao.Name));
                    }
                }

                p.DataHistorico = dataHistorico;

                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Deleta todas posições de emprestimo de bolsa.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoEmprestimoBolsaAberturaDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoEmprestimoBolsaAberturaCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();         
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoEmprestimoBolsaAberturaAtivo(int idCliente, DateTime dataHistorico, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico),
                       this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            this.Query.Load();
        }

        /// <summary>
        /// Retorna bool indicando se existe registro.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="dataHistorico"
        public bool ExistePosicaoEmprestimoBolsa(int idCliente, string cdAtivoBolsa, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Select(this.Query.IdPosicao)
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.CdAtivoBolsa == cdAtivoBolsa,
                       this.Query.DataHistorico.Equal(dataHistorico));

            bool existe = this.Query.Load();

            return existe;
        }
	}
}
