﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           18/10/2006 13:17:31
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;

namespace Financial.Bolsa {
    public partial class TabelaCustosBolsa : esTabelaCustosBolsa 
    {        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="tipoTaxaBolsa"></param>        
        /// <returns></returns>
        /// throws TabelaCustosBolsaNaoCadastradaException se não encontrar a taxa desejada
        /// throws ArgumentException se tipoTaxaBolsa não estiver dentro dos valores do enum TipoTaxaBolsa
        public void BuscaTabelaCustosBolsa(DateTime dataReferencia, int tipoTaxaBolsa) 
        {
            #region ArgumentoIncorreto - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("tipoTaxaBolsa incorreto. Os valores possiveis estão no enum TipoTaxaBolsa");

            List<int> valoresPossiveis = TipoTaxaBolsa.Values();
            if (!valoresPossiveis.Contains(tipoTaxaBolsa)) {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            TabelaCustosBolsaCollection tabelaCustosBolsaCollection = new TabelaCustosBolsaCollection();

            // Busca todas as taxas menores que a data de referencia ordenado decrescente pela data
            tabelaCustosBolsaCollection.Query
                 .Where(tabelaCustosBolsaCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia),
                        tabelaCustosBolsaCollection.Query.IdTabela == tipoTaxaBolsa)
                 .OrderBy(tabelaCustosBolsaCollection.Query.DataReferencia.Descending);

            tabelaCustosBolsaCollection.Query.Load();

            if (tabelaCustosBolsaCollection.HasData) {
                // Copia informações de tabelaCustosBovespaCollection para TabelaCustosBovespa                                
                this.AddNew();
                this.Descricao = ((TabelaCustosBolsa)tabelaCustosBolsaCollection[0]).Descricao;
                this.PercentualEmolumentoBolsa = ((TabelaCustosBolsa)tabelaCustosBolsaCollection[0]).PercentualEmolumentoBolsa;
                this.PercentualLiquidacaoCBLC = ((TabelaCustosBolsa)tabelaCustosBolsaCollection[0]).PercentualLiquidacaoCBLC;
                this.PercentualRegistroBolsa = ((TabelaCustosBolsa)tabelaCustosBolsaCollection[0]).PercentualRegistroBolsa;
                this.PercentualRegistroCBLC = ((TabelaCustosBolsa)tabelaCustosBolsaCollection[0]).PercentualRegistroCBLC;
                this.MinimoCBLC = ((TabelaCustosBolsa)tabelaCustosBolsaCollection[0]).MinimoCBLC;
            }
            else {
                StringBuilder mensagemAux = new StringBuilder();
                mensagemAux.Append("Taxa não cadastrada na tabela TabelaCustosBovespa ")
                           .Append("ou data de Referencia Inválida");
                throw new TabelaCustosBolsaNaoCadastradaException(mensagemAux.ToString());
            }
        }
    }
}
