﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa {
    public partial class PosicaoEmprestimoBolsaCollection : esPosicaoEmprestimoBolsaCollection 
    {        
        // Construtor
        // Cria uma nova PosicaoEmprestimoBolsaCollection com os dados de PosicaoEmprestimoBolsaHistoricoCollection
        // Todos do dados de PosicaoEmprestimoBolsaHistoricoCollection são copiados com excessão da dataHistorico
        public PosicaoEmprestimoBolsaCollection(PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection) {
            for (int i = 0; i < posicaoEmprestimoBolsaHistoricoCollection.Count; i++) {
                //
                PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

                // Para cada Coluna de PosicaoEmprestimoBolsaHistorico copia para PosicaoEmprestimoBolsa
                foreach (esColumnMetadata colPosicaoHistorico in posicaoEmprestimoBolsaHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colPosicaoEmprestimoBolsa = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (posicaoEmprestimoBolsaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
                            p.SetColumn(colPosicaoEmprestimoBolsa.Name, posicaoEmprestimoBolsaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Construtor
        /// Cria uma nova PosicaoEmprestimoBolsaCollection com os dados de PosicaoEmprestimoBolsaHistoricoCollection
        /// Todos do dados de PosicaoEmprestimoBolsaHistoricoCollection são copiados com excessão da dataHistorico
        /// Colunas Extras com valores provenientes de Join podem ser passadas
        /// </summary>
        /// <param name="posicaoEmprestimoBolsaHistoricoCollection"></param>
        /// <param name="colunasExtrasJoin">Colunas extras provenientes de um join</param>
        // Deprecated
        //public PosicaoEmprestimoBolsaCollection(PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection, List<string> colunasExtrasJoin) {
        //    for (int i = 0; i < posicaoEmprestimoBolsaHistoricoCollection.Count; i++) {
        //        //
        //        PosicaoEmprestimoBolsa p = new PosicaoEmprestimoBolsa();

        //        // Para cada Coluna de PosicaoEmprestimoBolsaHistorico copia para PosicaoEmprestimoBolsa
        //        foreach (esColumnMetadata colPosicaoHistorico in posicaoEmprestimoBolsaHistoricoCollection.es.Meta.Columns) {
        //            // Copia todas as colunas menos a Data Historico
        //            if (colPosicaoHistorico.PropertyName != PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataHistorico) {
        //                esColumnMetadata colPosicaoEmprestimoBolsa = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
        //                if (posicaoEmprestimoBolsaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
        //                    p.SetColumn(colPosicaoEmprestimoBolsa.Name, posicaoEmprestimoBolsaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
        //                }
        //            }
        //        }

        //        //for (int j = 0; j < colunasExtrasJoin.Count; j++) {
        //        //    this[i].SetColumn(colunasExtrasJoin[j], posicaoEmprestimoBolsaHistoricoCollection[i].GetColumn(colunasExtrasJoin[j]));    
        //        //}
                                
        //        //this[i].SetColumn("", posicaoEmprestimoBolsaHistoricoCollection[i].GetColumn(colunasExtrasJoin[j]));    

        //        this.AttachEntity(p);
        //    }
        //}

        /// <summary>
        /// Carrega o objeto PosicaoEmprestimoBolsaCollection com todos os campos de PosicaoEmprestimoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoEmprestimoBolsaCompleta(int idCliente) 
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente);
            this.Query.Load();        
        }

        /// <summary>
        /// Carrega o objeto BuscaPosicaoEmprestimoBolsa com os todos os de PosicaoEmprestimoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoEmprestimoBolsa(int idCliente) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente);
            this.Query.Load();            
        }

        /// <summary>
        /// Carrega o objeto BuscaPosicaoEmprestimoBolsa com os todos os de PosicaoEmprestimoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        public void BuscaPosicaoEmprestimoBolsa(int idCliente, DateTime dataVencimento) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataVencimento == dataVencimento);
            this.Query.Load();            
        }

        /// <summary>
        /// Deleta todas as posições em PosicaoBolsa do idCliente.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaPosicaoEmprestimoBolsa(int idCliente) 
        {
            this.Query
                    .Select(this.Query.IdPosicao)
                    .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Método de inserção em PosicaoEmprestimoBolsa a partir de PosicaoEmprestimoBolsaHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void InserePosicaoEmprestimoBolsaHistorico(int idCliente, DateTime data) 
        {
            this.QueryReset();
            PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
            posicaoEmprestimoBolsaHistoricoCollection.BuscaPosicaoEmprestimoBolsaHistoricoCompleta(idCliente, data);
            for (int i = 0; i < posicaoEmprestimoBolsaHistoricoCollection.Count; i++) {
                
                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = posicaoEmprestimoBolsaHistoricoCollection[i];

                #region Copia de posicaoEmprestimoBolsaHistorico para posicaoEmprestimoBolsa

                esParameters esParams = new esParameters();
                esParams.Add("idPosicao", posicaoEmprestimoBolsaHistorico.IdPosicao.Value);                
                esParams.Add("IdAgente", posicaoEmprestimoBolsaHistorico.IdAgente.Value);
                esParams.Add("IdCliente", posicaoEmprestimoBolsaHistorico.IdCliente.Value);

                // Parametros que podem ser null
                if (!posicaoEmprestimoBolsaHistorico.IdOperacao.HasValue) {
                    esParams.Add("IdOperacao", DBNull.Value);
                }
                else {
                    esParams.Add("IdOperacao", posicaoEmprestimoBolsaHistorico.IdOperacao);
                }
                               
                //
                esParams.Add("CdAtivoBolsa", posicaoEmprestimoBolsaHistorico.CdAtivoBolsa);
                esParams.Add("Quantidade", posicaoEmprestimoBolsaHistorico.Quantidade.Value);
                esParams.Add("PUMercado", posicaoEmprestimoBolsaHistorico.PUMercado.Value);
                esParams.Add("PULiquidoOriginal", posicaoEmprestimoBolsaHistorico.PULiquidoOriginal.Value);
                esParams.Add("ValorCorrigidoJuros", posicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros.Value);
                esParams.Add("ValorCorrigidoComissao", posicaoEmprestimoBolsaHistorico.ValorCorrigidoComissao.Value);
                esParams.Add("ValorCorrigidoCBLC", posicaoEmprestimoBolsaHistorico.ValorCorrigidoCBLC.Value);
                esParams.Add("ValorDiarioJuros", posicaoEmprestimoBolsaHistorico.ValorDiarioJuros.Value);
                esParams.Add("ValorDiarioComissao", posicaoEmprestimoBolsaHistorico.ValorDiarioComissao.Value);
                esParams.Add("ValorDiarioCBLC", posicaoEmprestimoBolsaHistorico.ValorDiarioCBLC.Value);
                esParams.Add("ValorBase", posicaoEmprestimoBolsaHistorico.ValorBase.Value);
                esParams.Add("PontaEmprestimo", posicaoEmprestimoBolsaHistorico.PontaEmprestimo.Value);
                esParams.Add("DataRegistro", posicaoEmprestimoBolsaHistorico.DataRegistro.Value);
                esParams.Add("DataVencimento", posicaoEmprestimoBolsaHistorico.DataVencimento.Value);
                esParams.Add("TaxaOperacao", posicaoEmprestimoBolsaHistorico.TaxaOperacao.Value);
                esParams.Add("TaxaComissao", posicaoEmprestimoBolsaHistorico.TaxaComissao.Value);
                esParams.Add("NumeroContrato", posicaoEmprestimoBolsaHistorico.NumeroContrato.Value);
                esParams.Add("TipoEmprestimo", posicaoEmprestimoBolsaHistorico.TipoEmprestimo.Value);
                esParams.Add("ValorMercado", posicaoEmprestimoBolsaHistorico.ValorMercado.Value);
                esParams.Add("PermiteDevolucaoAntecipada", posicaoEmprestimoBolsaHistorico.PermiteDevolucaoAntecipada);

                if (!posicaoEmprestimoBolsaHistorico.IdTrader.HasValue)
                {
                    esParams.Add("IdTrader", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdTrader", posicaoEmprestimoBolsaHistorico.IdTrader);
                }

                if (!posicaoEmprestimoBolsaHistorico.DataInicialDevolucaoAntecipada.HasValue)
                {
                    esParams.Add("DataInicialDevolucaoAntecipada", DBNull.Value);
                }
                else
                {
                    esParams.Add("DataInicialDevolucaoAntecipada", posicaoEmprestimoBolsaHistorico.DataInicialDevolucaoAntecipada.Value);
                }

                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoEmprestimoBolsa ON ");
                //
                sqlBuilder.Append("INSERT INTO PosicaoEmprestimoBolsa (");
                sqlBuilder.Append(PosicaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.Quantidade);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.PUMercado);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.PULiquidoOriginal);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoJuros);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoComissao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoCBLC);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioJuros);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioComissao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioCBLC);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorBase);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataRegistro);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.TaxaOperacao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.TaxaComissao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.TipoEmprestimo);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorMercado);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdTrader);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.PermiteDevolucaoAntecipada);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataInicialDevolucaoAntecipada);
                //              
                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.Quantidade);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.PUMercado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.PULiquidoOriginal);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoJuros);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoComissao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoCBLC);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioJuros);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioComissao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioCBLC);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorBase);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataRegistro);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.TaxaOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.TaxaComissao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.TipoEmprestimo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorMercado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdTrader);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.PermiteDevolucaoAntecipada);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataInicialDevolucaoAntecipada);
                sqlBuilder.Append(")");
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoEmprestimoBolsa OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                #endregion
                
            }         
        }

        /// <summary>
        /// Método de inserção em PosicaoEmprestimoBolsa a partir de PosicaoEmprestimoBolsaAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void InserePosicaoEmprestimoBolsaAbertura(int idCliente, DateTime data)
        {
            this.QueryReset();
            PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection();
            posicaoEmprestimoBolsaAberturaCollection.BuscaPosicaoEmprestimoBolsaAberturaCompleta(idCliente, data);
            for (int i = 0; i < posicaoEmprestimoBolsaAberturaCollection.Count; i++)
            {
                PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsaAbertura = posicaoEmprestimoBolsaAberturaCollection[i];

                #region Copia de posicaoEmprestimoBolsaAbertura para posicaoEmprestimoBolsa
                esParameters esParams = new esParameters();
                esParams.Add("idPosicao", posicaoEmprestimoBolsaAbertura.IdPosicao.Value);
                esParams.Add("IdAgente", posicaoEmprestimoBolsaAbertura.IdAgente.Value);
                esParams.Add("IdCliente", posicaoEmprestimoBolsaAbertura.IdCliente.Value);
                
                // Parametros que podem ser null
                if(!posicaoEmprestimoBolsaAbertura.IdOperacao.HasValue) {
                    esParams.Add("IdOperacao", DBNull.Value);
                }
                else {
                    esParams.Add("IdOperacao", posicaoEmprestimoBolsaAbertura.IdOperacao);
                }                                                                               
                //
                esParams.Add("CdAtivoBolsa", posicaoEmprestimoBolsaAbertura.CdAtivoBolsa);
                esParams.Add("Quantidade", posicaoEmprestimoBolsaAbertura.Quantidade.Value);
                esParams.Add("PUMercado", posicaoEmprestimoBolsaAbertura.PUMercado.Value);
                esParams.Add("PULiquidoOriginal", posicaoEmprestimoBolsaAbertura.PULiquidoOriginal.Value);
                esParams.Add("ValorCorrigidoJuros", posicaoEmprestimoBolsaAbertura.ValorCorrigidoJuros.Value);
                esParams.Add("ValorCorrigidoComissao", posicaoEmprestimoBolsaAbertura.ValorCorrigidoComissao.Value);
                esParams.Add("ValorCorrigidoCBLC", posicaoEmprestimoBolsaAbertura.ValorCorrigidoCBLC.Value);
                esParams.Add("ValorDiarioJuros", posicaoEmprestimoBolsaAbertura.ValorDiarioJuros.Value);
                esParams.Add("ValorDiarioComissao", posicaoEmprestimoBolsaAbertura.ValorDiarioComissao.Value);
                esParams.Add("ValorDiarioCBLC", posicaoEmprestimoBolsaAbertura.ValorDiarioCBLC.Value);
                esParams.Add("ValorBase", posicaoEmprestimoBolsaAbertura.ValorBase.Value);
                esParams.Add("PontaEmprestimo", posicaoEmprestimoBolsaAbertura.PontaEmprestimo.Value);
                esParams.Add("DataRegistro", posicaoEmprestimoBolsaAbertura.DataRegistro.Value);
                esParams.Add("DataVencimento", posicaoEmprestimoBolsaAbertura.DataVencimento.Value);
                esParams.Add("TaxaOperacao", posicaoEmprestimoBolsaAbertura.TaxaOperacao.Value);
                esParams.Add("TaxaComissao", posicaoEmprestimoBolsaAbertura.TaxaComissao.Value);
                esParams.Add("NumeroContrato", posicaoEmprestimoBolsaAbertura.NumeroContrato.Value);
                esParams.Add("TipoEmprestimo", posicaoEmprestimoBolsaAbertura.TipoEmprestimo.Value);
                esParams.Add("ValorMercado", posicaoEmprestimoBolsaAbertura.ValorMercado.Value);
                esParams.Add("PermiteDevolucaoAntecipada", posicaoEmprestimoBolsaAbertura.PermiteDevolucaoAntecipada);

                if (!posicaoEmprestimoBolsaAbertura.IdTrader.HasValue)
                {
                    esParams.Add("IdTrader", DBNull.Value);
                }
                else
                {
                    esParams.Add("IdTrader", posicaoEmprestimoBolsaAbertura.IdTrader);
                }

                if (!posicaoEmprestimoBolsaAbertura.DataInicialDevolucaoAntecipada.HasValue)
                {
                    esParams.Add("DataInicialDevolucaoAntecipada", DBNull.Value);
                }
                else
                {
                    esParams.Add("DataInicialDevolucaoAntecipada", posicaoEmprestimoBolsaAbertura.DataInicialDevolucaoAntecipada.Value);
                }
           
                StringBuilder sqlBuilder = new StringBuilder();
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoEmprestimoBolsa ON ");
                //
                sqlBuilder.Append("INSERT INTO PosicaoEmprestimoBolsa (");
                sqlBuilder.Append(PosicaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.Quantidade);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.PUMercado);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.PULiquidoOriginal);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoJuros);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoComissao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoCBLC);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioJuros);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioComissao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioCBLC);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorBase);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataRegistro);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.TaxaOperacao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.TaxaComissao);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.TipoEmprestimo);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorMercado);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdTrader);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.PermiteDevolucaoAntecipada);
                sqlBuilder.Append("," + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataInicialDevolucaoAntecipada);
                //              
                sqlBuilder.Append(") VALUES ( ");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdAgente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdCliente);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.Quantidade);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.PUMercado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.PULiquidoOriginal);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoJuros);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoComissao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorCorrigidoCBLC);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioJuros);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioComissao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorDiarioCBLC);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorBase);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataRegistro);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataVencimento);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.TaxaOperacao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.TaxaComissao);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.TipoEmprestimo);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.ValorMercado);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.IdTrader);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.PermiteDevolucaoAntecipada);
                sqlBuilder.Append(",");
                sqlBuilder.Append("@" + PosicaoEmprestimoBolsaMetadata.ColumnNames.DataInicialDevolucaoAntecipada);
                sqlBuilder.Append(")");
                sqlBuilder.Append("SET IDENTITY_INSERT PosicaoEmprestimoBolsa OFF ");
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);   
                #endregion
            }            
        }

        /// <summary>
        /// Carrega o objeto PosicaoEmprestimoBolsaCollection com todos os campos de PosicaoEmprestimoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="cdAtivoBolsa"></param>
        public void BuscaPosicaoEmprestimoBolsaCompleta(int idCliente, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.CdAtivoBolsa == cdAtivoBolsa);

            this.Query.Load();            
        }

        /// <summary>
        /// Retorna bool indicando se existe registro.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="cdAtivoBolsa"></param>
        public bool ExistePosicaoEmprestimoBolsa(int idCliente, string cdAtivoBolsa)
        {
            this.QueryReset();            
            this.Query
                .Select(this.Query.IdPosicao)
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.CdAtivoBolsa == cdAtivoBolsa);

            bool existe = this.Query.Load();

            return existe;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="idAgente"></param>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadeDisponivel(DateTime dataReferencia, string cdAtivoBolsa, int? idAgente, int idCliente)
        {
            decimal qtdePosicao = 0;            
            PosicaoEmprestimoBolsaAberturaCollection posicaoBolsaAberturaColl = new PosicaoEmprestimoBolsaAberturaCollection();
            posicaoBolsaAberturaColl.Query.Select(posicaoBolsaAberturaColl.Query.Quantidade.Sum());
            posicaoBolsaAberturaColl.Query.Where(posicaoBolsaAberturaColl.Query.DataHistorico.Equal(dataReferencia) &
                                                 posicaoBolsaAberturaColl.Query.CdAtivoBolsa.Equal(cdAtivoBolsa) &
                                                 posicaoBolsaAberturaColl.Query.IdCliente.Equal(idCliente));

            if (idAgente.HasValue)
                posicaoBolsaAberturaColl.Query.Where(posicaoBolsaAberturaColl.Query.IdAgente.Equal(idAgente));

            posicaoBolsaAberturaColl.Query.GroupBy(posicaoBolsaAberturaColl.Query.IdCliente, posicaoBolsaAberturaColl.Query.CdAtivoBolsa);

            if (posicaoBolsaAberturaColl.Query.Load())            
                qtdePosicao = posicaoBolsaAberturaColl[0].Quantidade.GetValueOrDefault(0);                            

            decimal qtdeOperacaoEntrada = 0;
            OperacaoEmprestimoBolsaCollection OperacaoBolsaEntradaColl = new OperacaoEmprestimoBolsaCollection();
            OperacaoBolsaEntradaColl.Query.Select(OperacaoBolsaEntradaColl.Query.Quantidade.Sum());
            OperacaoBolsaEntradaColl.Query.Where(OperacaoBolsaEntradaColl.Query.DataRegistro.Equal(dataReferencia) &
                                                 OperacaoBolsaEntradaColl.Query.CdAtivoBolsa.Equal(cdAtivoBolsa) &
                                                 OperacaoBolsaEntradaColl.Query.IdCliente.Equal(idCliente) &
                                                 OperacaoBolsaEntradaColl.Query.PontaEmprestimo.Equal((int)PontaEmprestimoBolsa.Tomador));

            if (idAgente.HasValue)
                OperacaoBolsaEntradaColl.Query.Where(OperacaoBolsaEntradaColl.Query.IdAgente.Equal(idAgente));

            OperacaoBolsaEntradaColl.Query.GroupBy(OperacaoBolsaEntradaColl.Query.IdCliente,
                                                   OperacaoBolsaEntradaColl.Query.CdAtivoBolsa);

            if (OperacaoBolsaEntradaColl.Query.Load())
                qtdeOperacaoEntrada = OperacaoBolsaEntradaColl[0].Quantidade.GetValueOrDefault(0);

            decimal qtdeOperacaoSaida = 0;
            OperacaoEmprestimoBolsaCollection OperacaoBolsaSaidaColl = new OperacaoEmprestimoBolsaCollection();
            OperacaoBolsaSaidaColl.Query.Select(OperacaoBolsaSaidaColl.Query.Quantidade.Sum());
            OperacaoBolsaSaidaColl.Query.Where(OperacaoBolsaSaidaColl.Query.DataRegistro.Equal(dataReferencia) &
                                               OperacaoBolsaSaidaColl.Query.CdAtivoBolsa.Equal(cdAtivoBolsa) &
                                               OperacaoBolsaSaidaColl.Query.IdCliente.Equal(idCliente) &
                                               OperacaoBolsaSaidaColl.Query.PontaEmprestimo.Equal((int)PontaEmprestimoBolsa.Doador));

            if (idAgente.HasValue)
                OperacaoBolsaSaidaColl.Query.Where(OperacaoBolsaSaidaColl.Query.IdAgente.Equal(idAgente));

            OperacaoBolsaSaidaColl.Query.GroupBy(OperacaoBolsaSaidaColl.Query.IdCliente,
                                                 OperacaoBolsaSaidaColl.Query.CdAtivoBolsa);

            if (OperacaoBolsaSaidaColl.Query.Load())
                qtdeOperacaoSaida = OperacaoBolsaSaidaColl[0].Quantidade.GetValueOrDefault(0);

            return qtdePosicao + qtdeOperacaoEntrada - qtdeOperacaoSaida;
        }
    }
}
