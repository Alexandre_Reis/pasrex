using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa
{
	public partial class FatorCotacaoBolsaCollection : esFatorCotacaoBolsaCollection
	{
        /// <summary>
        /// Carrega o objeto FatorCotacaoBolsaCollection com os campos DataReferencia, Fator.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        public void BuscaFatorCotacaoAtivo(string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.DataReferencia, this.Query.Fator)
                 .Where(this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            this.Query.Load();
        }

	}
}
