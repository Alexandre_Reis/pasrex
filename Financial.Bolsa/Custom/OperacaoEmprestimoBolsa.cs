﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Common.Enums;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using Financial.Bolsa.Enums;
using Financial.Util;
using Financial.Bolsa.Exceptions;
using System.IO;
using Financial.Common;
using Financial.Investidor;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Interfaces.Import.Bolsa;
using Financial.Investidor.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Tributo.Enums;

namespace Financial.Bolsa {
    public partial class OperacaoEmprestimoBolsa : esOperacaoEmprestimoBolsa {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public bool BuscaOperacaoEmprestimoBolsa(int idCliente, int numeroContrato) 
        {
            this.QueryReset();
            this.Query.Select(this.Query.IdOperacao, this.Query.CdAtivoBolsa)
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.NumeroContrato == numeroContrato);
                       
            bool retorno = this.Query.Load();
           
            return retorno;            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaOperacaoEmprestimo(int idCliente, DateTime data)
        {
            // Collections que serão usadas para salvar tudo de uma vez no final do loop
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            
            //
            OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
            operacaoEmprestimoBolsaCollection.BuscaOperacaoEmprestimoBolsaCompleta(idCliente, data);

            //Deleta antes todas as operações Bolsa do tipo deposito (origem = Emprestimo) na data
            List<int> origemLista = new List<int>();
            origemLista.Add(OrigemOperacaoBolsa.AberturaEmprestimo);
            OperacaoBolsaCollection operacaoBolsaCollectionDeletar = new OperacaoBolsaCollection();
            operacaoBolsaCollectionDeletar.DeletaOperacaoBolsa(idCliente, origemLista, data);
            
            for (int i = 0; i < operacaoEmprestimoBolsaCollection.Count; i++) {
                OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = operacaoEmprestimoBolsaCollection[i];
                
                #region Valores operacaoEmprestimoBolsa
                int idOperacao = operacaoEmprestimoBolsa.IdOperacao.Value;
                string cdAtivoBolsa = operacaoEmprestimoBolsa.CdAtivoBolsa;
                int idAgente = operacaoEmprestimoBolsa.IdAgente.Value;
                byte pontaEmprestimo = operacaoEmprestimoBolsa.PontaEmprestimo.Value;
                DateTime dataVencimento = operacaoEmprestimoBolsa.DataVencimento.Value;
                decimal taxaOperacao = operacaoEmprestimoBolsa.TaxaOperacao.Value;
                decimal taxaComissao = operacaoEmprestimoBolsa.TaxaComissao.Value;
                decimal quantidade = operacaoEmprestimoBolsa.Quantidade.Value;
                decimal pu = operacaoEmprestimoBolsa.Pu.Value;
                decimal valor = operacaoEmprestimoBolsa.Valor.Value;
                byte tipoEmprestimo = operacaoEmprestimoBolsa.TipoEmprestimo.Value;
                int numeroContrato = operacaoEmprestimoBolsa.NumeroContrato.Value;
                #endregion
                
                int idLocal = LocalFeriadoFixo.Bovespa;
                string tipoMercado = TipoMercadoBolsa.MercadoVista;
                int origem = OrigemOperacaoBolsa.AberturaEmprestimo;

                //Busca fatorCotacaoBolsa
                FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                decimal fator = 1;
                if (fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                {
                    fator = fatorCotacao.Fator.Value;
                }
                //

                decimal puLiquidoPosicao = 0;
                string tipoOperacaoBolsa = null;
                if (operacaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador()) {
                    tipoOperacaoBolsa = TipoOperacaoBolsa.Retirada;
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();

                    // Levo PuLiquido da posição original para a posicao do Emprestimo,
                    // de modo a saber o PU quando a posição voltar a carteira (considera PU de custo de posicao e PU Original de BTC liquidando na data)
                    decimal quantidadePosicao = 0;
                    decimal valorCustoPosicao = 0;
                    if (posicaoBolsa.BuscaPuLiquido(idCliente, idAgente, cdAtivoBolsa))
                    {
                        decimal puLiquido = posicaoBolsa.PUCustoLiquido.Value;
                        quantidadePosicao = posicaoBolsa.Quantidade.Value;
                        valorCustoPosicao = quantidadePosicao * puLiquido / fator;
                    }

                    LiquidacaoEmprestimoBolsaQuery liquidacaoEmprestimoBolsaQuery = new LiquidacaoEmprestimoBolsaQuery("L");
                    PosicaoEmprestimoBolsaAberturaQuery posicaoEmprestimoBolsaAberturaQuery = new PosicaoEmprestimoBolsaAberturaQuery("P");
                    liquidacaoEmprestimoBolsaQuery.Select(liquidacaoEmprestimoBolsaQuery.Quantidade,
                                                          posicaoEmprestimoBolsaAberturaQuery.PULiquidoOriginal);
                    liquidacaoEmprestimoBolsaQuery.InnerJoin(posicaoEmprestimoBolsaAberturaQuery).On(posicaoEmprestimoBolsaAberturaQuery.IdPosicao == liquidacaoEmprestimoBolsaQuery.IdPosicao);
                    liquidacaoEmprestimoBolsaQuery.Where(liquidacaoEmprestimoBolsaQuery.IdCliente.Equal(idCliente),
                                                         liquidacaoEmprestimoBolsaQuery.Data.Equal(data),
                                                         liquidacaoEmprestimoBolsaQuery.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                         posicaoEmprestimoBolsaAberturaQuery.DataHistorico.Equal(data),
                                                         posicaoEmprestimoBolsaAberturaQuery.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador));
                    LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
                    liquidacaoEmprestimoBolsaCollection.Load(liquidacaoEmprestimoBolsaQuery);

                    if (liquidacaoEmprestimoBolsaCollection.Count == 0)
                    {
                        liquidacaoEmprestimoBolsaQuery = new LiquidacaoEmprestimoBolsaQuery("L");
                        posicaoEmprestimoBolsaAberturaQuery = new PosicaoEmprestimoBolsaAberturaQuery("P");
                        liquidacaoEmprestimoBolsaQuery.Select(liquidacaoEmprestimoBolsaQuery.Quantidade,
                                                              posicaoEmprestimoBolsaAberturaQuery.PULiquidoOriginal);
                        liquidacaoEmprestimoBolsaQuery.InnerJoin(posicaoEmprestimoBolsaAberturaQuery).On(posicaoEmprestimoBolsaAberturaQuery.IdOperacao == liquidacaoEmprestimoBolsaQuery.IdOperacao);
                        liquidacaoEmprestimoBolsaQuery.Where(liquidacaoEmprestimoBolsaQuery.IdCliente.Equal(idCliente),
                                                             liquidacaoEmprestimoBolsaQuery.Data.Equal(data),
                                                             liquidacaoEmprestimoBolsaQuery.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                             posicaoEmprestimoBolsaAberturaQuery.DataHistorico.Equal(data),
                                                             posicaoEmprestimoBolsaAberturaQuery.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador));
                        liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
                        liquidacaoEmprestimoBolsaCollection.Load(liquidacaoEmprestimoBolsaQuery);
                    }

                    if (liquidacaoEmprestimoBolsaCollection.Count == 0)
                    {
                        liquidacaoEmprestimoBolsaQuery = new LiquidacaoEmprestimoBolsaQuery("L");
                        posicaoEmprestimoBolsaAberturaQuery = new PosicaoEmprestimoBolsaAberturaQuery("P");
                        liquidacaoEmprestimoBolsaQuery.Select(liquidacaoEmprestimoBolsaQuery.Quantidade,
                                                              posicaoEmprestimoBolsaAberturaQuery.PULiquidoOriginal);
                        liquidacaoEmprestimoBolsaQuery.InnerJoin(posicaoEmprestimoBolsaAberturaQuery).On(posicaoEmprestimoBolsaAberturaQuery.NumeroContrato == liquidacaoEmprestimoBolsaQuery.NumeroContrato);
                        liquidacaoEmprestimoBolsaQuery.Where(liquidacaoEmprestimoBolsaQuery.IdCliente.Equal(idCliente),
                                                             liquidacaoEmprestimoBolsaQuery.Data.Equal(data),
                                                             liquidacaoEmprestimoBolsaQuery.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                             posicaoEmprestimoBolsaAberturaQuery.DataHistorico.Equal(data),
                                                             posicaoEmprestimoBolsaAberturaQuery.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador));
                        liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
                        liquidacaoEmprestimoBolsaCollection.Load(liquidacaoEmprestimoBolsaQuery);
                    }

                    decimal quantidadeLiquidada = 0;
                    decimal valorCustoLiquidado = 0;
                    foreach (LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa in liquidacaoEmprestimoBolsaCollection)
                    {
                        decimal puOriginal = Convert.ToDecimal(liquidacaoEmprestimoBolsa.GetColumn(PosicaoEmprestimoBolsaAberturaMetadata.ColumnNames.PULiquidoOriginal));
                        decimal quantidadeLiquidacao = liquidacaoEmprestimoBolsa.Quantidade.Value;
                        quantidadeLiquidada += quantidadeLiquidacao;
                        valorCustoLiquidado += quantidadeLiquidacao * puOriginal / fator;
                    }

                    if ((quantidadePosicao + quantidadeLiquidada) == 0)
                    {
                        puLiquidoPosicao = 0;
                    }
                    else
                    {
                        puLiquidoPosicao = (valorCustoPosicao + valorCustoLiquidado) / (quantidadePosicao + quantidadeLiquidada) * fator;
                    }
                }
                else if (operacaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador()) {
                    puLiquidoPosicao = pu; // Aqui vai o PU do próprio empréstimo, para depois compor o resultado!
                    tipoOperacaoBolsa = TipoOperacaoBolsa.Deposito;
                }

                #region Calcula o valor de mercado do próprio dia
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                if (!cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsa, data))
                {
                    throw new CotacaoNaoCadastradaException("Cotação não cadastrada para o ativo " + cdAtivoBolsa + " em " + data.ToShortDateString());
                }

                decimal puFechamento = cotacaoBolsa.PUFechamento.Value;

                decimal valorMercado = Math.Round(quantidade * puFechamento / fator, 2);
                #endregion

                #region Insercao PosicaoEmprestimo
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection.AddNew();
                posicaoEmprestimoBolsa.IdOperacao = idOperacao;
                posicaoEmprestimoBolsa.IdAgente = idAgente;
                posicaoEmprestimoBolsa.IdCliente = idCliente;
                posicaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                posicaoEmprestimoBolsa.Quantidade = quantidade;                
                posicaoEmprestimoBolsa.PULiquidoOriginal = puLiquidoPosicao;
                posicaoEmprestimoBolsa.ValorBase = valor;
                posicaoEmprestimoBolsa.ValorMercado = valorMercado;
                posicaoEmprestimoBolsa.PontaEmprestimo = pontaEmprestimo;
                posicaoEmprestimoBolsa.DataRegistro = data;
                posicaoEmprestimoBolsa.DataVencimento = dataVencimento;
                posicaoEmprestimoBolsa.TaxaOperacao = taxaOperacao;
                posicaoEmprestimoBolsa.TaxaComissao = taxaComissao;
                posicaoEmprestimoBolsa.NumeroContrato = numeroContrato;
                posicaoEmprestimoBolsa.TipoEmprestimo = tipoEmprestimo;
                posicaoEmprestimoBolsa.IdTrader = operacaoEmprestimoBolsa.IdTrader;
                posicaoEmprestimoBolsa.DataInicialDevolucaoAntecipada = operacaoEmprestimoBolsa.DataInicialDevolucaoAntecipada;
                posicaoEmprestimoBolsa.PermiteDevolucaoAntecipada = operacaoEmprestimoBolsa.PermiteDevolucaoAntecipada;
                #endregion

                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.ApuraGanhoRV);
                cliente.LoadByPrimaryKey(campos, idCliente);
                if (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraTodas ||
                    (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.NaoApura))
                {
                    #region Insercao OperacaoBolsa
                    OperacaoBolsa operacaoBolsa = operacaoBolsaCollection.AddNew();
                    operacaoBolsa.TipoMercado = tipoMercado;
                    operacaoBolsa.TipoOperacao = tipoOperacaoBolsa;
                    operacaoBolsa.Data = data;
                    operacaoBolsa.Pu = pu;
                    operacaoBolsa.PULiquido = pu;
                    operacaoBolsa.Quantidade = quantidade;
                    operacaoBolsa.DataLiquidacao = data;
                    operacaoBolsa.Origem = (byte)origem;
                    operacaoBolsa.IdLocal = (Int16)idLocal;
                    operacaoBolsa.IdCliente = idCliente;
                    operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    operacaoBolsa.IdAgenteCorretora = idAgente;
                    operacaoBolsa.IdAgenteLiquidacao = idAgente;

                    if (pu > 0 && quantidade > 0)
                        operacaoBolsa.Valor = Math.Round(pu * quantidade, 2);
                    else
                        operacaoBolsa.Valor = 0;

                    operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                    operacaoBolsa.CalculaDespesas = "N";
                    operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                    operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                    operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                    operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                    operacaoBolsa.DataOperacao = data;
                    #endregion
                }
                
            }

            // Update das Collections
            #region Update das Collections
            posicaoEmprestimoBolsaCollection.Save();
            //
            operacaoBolsaCollection.Save();

            #endregion
        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo do Emprestimo da Posicao é Voluntario
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoEmprestimoVoluntario() {
            return this.TipoEmprestimo == (byte)TipoEmprestimoBolsa.Voluntario;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo do Emprestimo da Posicao é Compulsorio
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoEmprestimoCompulsorio() {
            return this.TipoEmprestimo == (byte)TipoEmprestimoBolsa.Compulsorio;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Ponta Emprestimo da posicao é Doador
        ///          False caso Contrario
        /// </returns>
        public bool IsPontaEmprestimoBolsaDoador() {
            return this.PontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Ponta Emprestimo da posicao é Tomador
        ///          False caso Contrario
        /// </returns>
        public bool IsPontaEmprestimoBolsaTomador() {
            return this.PontaEmprestimo == (byte)PontaEmprestimoBolsa.Tomador;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbtcCollection"></param>
        /// <param name="data">Nome do Arquivo Completo</param>
        /// throws AtivoNaoCadastradoException se codigo do Ativo não estiver cadastrado na Base
        public void CarregaOperacaoEmprestimoDbtc(DbtcCollection dbtcCollection, DateTime data) {
            #region GetIdAgente do Arquivo Dbtc

            int codigoBovespaAgente = dbtcCollection.CollectionDbtc[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            Dbtc dbtc = new Dbtc();
            OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
            OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollectionAux = new OperacaoEmprestimoBolsaCollection();

            // Deleta as operacões emprestimo do dia
            operacaoEmprestimoBolsaCollectionAux.DeletaOperacaoEmprestimoBolsa(data, FonteEmprestimoBolsa.ArquivoDBTC);

            // Pega as Operações Emprestimo do arquivo de Dbtc                            
            for (int i = 0; i < dbtcCollection.CollectionDbtc.Count; i++) {
                dbtc = dbtcCollection.CollectionDbtc[i];

                //Precisa tomar o cuidado abaixo, pq muitas vezes são trazidos registros de datas anteriores
                if (dbtc.IsTipoRegistroDoador() && DateTime.Compare(dbtc.DataRegistroDoador.Value, data) == 0 ||
                    dbtc.IsTipoRegistroTomador() && DateTime.Compare(dbtc.DataRegistroTomador.Value, data) == 0) {
                    #region Copia informações para OperacaoEmprestimoBolsa
                    OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = new OperacaoEmprestimoBolsa();

                    int? codigoClienteBovespa = null;
                    // OBS: Cuidado quando o tipoRegistro for header
                    if (dbtc.IsTipoRegistroDoador()) {
                        codigoClienteBovespa = dbtc.CodigoClienteDoador.Value;
                        // Setar operacaoEmprestimoBolsa.IdCliente depois
                        operacaoEmprestimoBolsa.CdAtivoBolsa = !String.IsNullOrEmpty(dbtc.CdAtivoBolsaDoador) ? dbtc.CdAtivoBolsaDoador : "";
                        operacaoEmprestimoBolsa.PontaEmprestimo = (byte)PontaEmprestimoBolsa.Doador;
                        operacaoEmprestimoBolsa.DataRegistro = dbtc.DataRegistroDoador.Value;
                        operacaoEmprestimoBolsa.DataVencimento = dbtc.DataVencimentoDoador.Value;
                        operacaoEmprestimoBolsa.TaxaOperacao = dbtc.TaxaOperacaoDoador.HasValue ? dbtc.TaxaOperacaoDoador.Value : 0;
                        operacaoEmprestimoBolsa.TaxaComissao = dbtc.TaxaComissaoDoador.HasValue ? dbtc.TaxaComissaoDoador.Value : 0;
                        operacaoEmprestimoBolsa.Quantidade = dbtc.QuantidadeDoador.HasValue ? dbtc.QuantidadeDoador.Value : 0;
                        operacaoEmprestimoBolsa.Pu = dbtc.PuDoador.HasValue ? dbtc.PuDoador.Value : 0;
                        operacaoEmprestimoBolsa.Valor = Utilitario.Truncate((dbtc.QuantidadeDoador.Value * dbtc.PuDoador.Value / dbtc.FatorCotacaoDoador.Value), 2);
                        operacaoEmprestimoBolsa.NumeroContrato = dbtc.NumeroContratoDoador.HasValue ? dbtc.NumeroContratoDoador.Value : 0;
                        byte tipoEmprestimo = dbtc.TipoOrigemDoador.Equals("AUTOMATICO") ? (byte)TipoEmprestimoBolsa.Compulsorio : (byte)TipoEmprestimoBolsa.Voluntario;
                        operacaoEmprestimoBolsa.TipoEmprestimo = tipoEmprestimo;
                    }
                    else if (dbtc.IsTipoRegistroTomador()) {
                        codigoClienteBovespa = dbtc.CodigoClienteTomador.Value;
                        // Setar operacaoEmprestimoBolsa.IdCliente depois
                        operacaoEmprestimoBolsa.CdAtivoBolsa = !String.IsNullOrEmpty(dbtc.CdAtivoBolsaTomador) ? dbtc.CdAtivoBolsaTomador : "";
                        operacaoEmprestimoBolsa.PontaEmprestimo = (byte)PontaEmprestimoBolsa.Tomador;
                        operacaoEmprestimoBolsa.DataRegistro = dbtc.DataRegistroTomador.Value;
                        operacaoEmprestimoBolsa.DataVencimento = dbtc.DataVencimentoTomador.Value;
                        operacaoEmprestimoBolsa.TaxaOperacao = dbtc.TaxaOperacaoTomador.HasValue ? dbtc.TaxaOperacaoTomador.Value : 0;
                        operacaoEmprestimoBolsa.TaxaComissao = dbtc.TaxaComissaoTomador.HasValue ? dbtc.TaxaComissaoTomador.Value : 0;
                        operacaoEmprestimoBolsa.Quantidade = dbtc.QuantidadeTomador.HasValue ? dbtc.QuantidadeTomador.Value : 0;
                        operacaoEmprestimoBolsa.Pu = dbtc.PuTomador.HasValue ? dbtc.PuTomador.Value : 0;
                        operacaoEmprestimoBolsa.Valor = Utilitario.Truncate((dbtc.QuantidadeTomador.Value * dbtc.PuTomador.Value / dbtc.FatorCotacaoTomador.Value), 2);
                        operacaoEmprestimoBolsa.NumeroContrato = dbtc.NumeroContratoTomador.HasValue ? dbtc.NumeroContratoTomador.Value : 0;
                        byte tipoEmprestimo = dbtc.TipoOrigemTomador.Equals("AUTOMATICO") ? (byte)TipoEmprestimoBolsa.Compulsorio : (byte)TipoEmprestimoBolsa.Voluntario;
                        operacaoEmprestimoBolsa.TipoEmprestimo = tipoEmprestimo;
                    }

                    // Só inclui os registros que forem doador ou tomador
                    if (dbtc.IsTipoRegistroTomador() || dbtc.IsTipoRegistroDoador()) {
                        // Só inclui os clientes que existirem na Base
                        int codigoClienteBovespaAux = codigoClienteBovespa.Value;
                        CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                        bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespaAux);

                        if (idClienteExiste) {
                            Cliente cliente = new Cliente();
                            cliente.LoadByPrimaryKey(codigoClienteAgente.IdCliente.Value);

                            //Só importa para clientes ativos
                            if (cliente.IsAtivo) {
                                //Só importa para clientes com dataDia anterior à data passada ou data igual e status != fechado
                                if ((DateTime.Compare(cliente.DataDia.Value, data) < 0) ||
                                   (DateTime.Compare(cliente.DataDia.Value, data) == 0 && cliente.Status.Value != (byte)StatusCliente.Divulgado)) {
                                    operacaoEmprestimoBolsa.IdCliente = codigoClienteAgente.IdCliente.Value;
                                    operacaoEmprestimoBolsa.IdAgente = idAgente;
                                    operacaoEmprestimoBolsa.Fonte = (byte)FonteEmprestimoBolsa.ArquivoDBTC;
                                    operacaoEmprestimoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;

                                    // lancamento de AtivoNaoCadastradoException                                       
                                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                                    if (!ativoBolsa.LoadByPrimaryKey(operacaoEmprestimoBolsa.CdAtivoBolsa)) {
                                        throw new AtivoNaoCadastradoException("Ativo não cadastrado: " + operacaoEmprestimoBolsa.CdAtivoBolsa);
                                    }

                                    //Inclui na collection
                                    operacaoEmprestimoBolsaCollection.AttachEntity(operacaoEmprestimoBolsa);
                                }
                            }
                        }
                    }
                    //
                    #endregion
                }
            }

            operacaoEmprestimoBolsaCollection.Save();
        }

        /// <summary>
        /// Integrã operações tomadas e doadas direto do Sinacor.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void IntegraOperacaoEmprestimoSinacor(int idCliente, DateTime data)
        {
            OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
            OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollectionAux = new OperacaoEmprestimoBolsaCollection();

            // Deleta as operacões emprestimo do dia
            operacaoEmprestimoBolsaCollectionAux.DeletaOperacaoEmprestimoBolsa(idCliente, data, FonteEmprestimoBolsa.Sinacor);

            ClienteBolsa clienteBolsa = new ClienteBolsa();

            #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!clienteBolsa.LoadByPrimaryKey(idCliente))
            {
                return;
            }

            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor) && String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                return;                
            }

            //Se nenhum dos 2 códigos é número, lança exception
            if (!Utilitario.IsInteger(clienteBolsa.CodigoSinacor) && !Utilitario.IsInteger(clienteBolsa.CodigoSinacor2))
            {
                return;                
            }

            int codigoCliente = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = Convert.ToInt32(clienteBolsa.CodigoSinacor);
            }
            int codigoCliente2 = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = Convert.ToInt32(clienteBolsa.CodigoSinacor2);
            }

            //Se algum dos 2 códigos for nulo, repete o código de um para ao outro
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = codigoCliente2;
            }
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = codigoCliente;
            }
            #endregion

            List<Int32> listaCodigos = new List<int>();
            listaCodigos.Add(codigoCliente);
            listaCodigos.Add(codigoCliente2);

            TbtcontratoCollection tbtcontratoCollection = new TbtcontratoCollection();
            tbtcontratoCollection.BuscaContratos(listaCodigos, data);

            if (ParametrosConfiguracaoSistema.Integracoes.SchemaBTC == "")
            {
                HbtcontratoCollection hbtcontratoCollection = new HbtcontratoCollection();
                hbtcontratoCollection.BuscaContratos(listaCodigos, data);

                TbtcontratoCollection tbtcontratoCollectionAux = new TbtcontratoCollection(hbtcontratoCollection);
                tbtcontratoCollection.Combine(tbtcontratoCollectionAux);
            }

            int idAgenteMercado = 0;
            if (tbtcontratoCollection.Count > 0)
            {
                //Busca o IdAgenteMercado default
                int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                AgenteMercado agenteMercado = new AgenteMercado();
                idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);
            }

            foreach (Tbtcontrato tbtcontrato in tbtcontratoCollection)
            {
                OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = operacaoEmprestimoBolsaCollection.AddNew();

                #region Carrega o ativo, ou integra do Sinacor preenchendo o objeto ativoBolsa
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (!ativoBolsa.LoadByPrimaryKey(tbtcontrato.CodNeg))
                {
                    //Integra ativo do Sinacor
                    if (!ativoBolsa.IntegraAtivoSinacor(tbtcontrato.CodNeg, tbtcontrato.DataAber.Value))
                    {
                        throw new AtivoNaoCadastradoException("Ativo não cadastrado no Sinacor - " + tbtcontrato.CodNeg);
                    }
                }
                #endregion          

                operacaoEmprestimoBolsa.CdAtivoBolsa = tbtcontrato.CodNeg;
                operacaoEmprestimoBolsa.DataRegistro = tbtcontrato.DataAber.Value;
                operacaoEmprestimoBolsa.DataVencimento = tbtcontrato.DataVenc.Value;
                operacaoEmprestimoBolsa.Fonte = (byte)FonteEmprestimoBolsa.Sinacor;
                operacaoEmprestimoBolsa.IdAgente = idAgenteMercado;
                operacaoEmprestimoBolsa.IdCliente = idCliente;
                operacaoEmprestimoBolsa.IdLocal = (short)LocalFeriadoFixo.Bovespa;
                operacaoEmprestimoBolsa.NumeroContrato = Convert.ToInt32(tbtcontrato.NumCotr.Value);
                operacaoEmprestimoBolsa.PontaEmprestimo = tbtcontrato.IndPosi == "D" ? (byte)PontaEmprestimoBolsa.Doador : (byte)PontaEmprestimoBolsa.Tomador;
                operacaoEmprestimoBolsa.Pu = tbtcontrato.PrecMed.HasValue ? tbtcontrato.PrecMed.Value : tbtcontrato.PrecDiaAnte.Value;
                operacaoEmprestimoBolsa.Quantidade = tbtcontrato.QtdeAcoe.Value;
                operacaoEmprestimoBolsa.TaxaComissao = tbtcontrato.TaxaComi.HasValue ? tbtcontrato.TaxaComi.Value : 0;
                operacaoEmprestimoBolsa.TaxaOperacao = tbtcontrato.TaxaRemu.HasValue ? tbtcontrato.TaxaRemu.Value : 0;
                operacaoEmprestimoBolsa.TipoEmprestimo = (byte)TipoEmprestimoBolsa.Voluntario; //HOJE NAO TEM ESSA INFORMAÇÃO NO SINACOR
                operacaoEmprestimoBolsa.Valor = Math.Round(operacaoEmprestimoBolsa.Pu.Value * operacaoEmprestimoBolsa.Quantidade.Value, 2);
            }

            operacaoEmprestimoBolsaCollection.Save();
        }
    }
}