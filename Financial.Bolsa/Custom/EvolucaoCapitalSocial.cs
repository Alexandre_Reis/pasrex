using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa
{
	public partial class EvolucaoCapitalSocial : esEvolucaoCapitalSocial
	{
        public decimal RetornaValorCapitalSocial(int idEmissor, DateTime data)
        {
            decimal valorFinal = 0;

            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa,
                                              ativoBolsaCollection.Query.Especificacao);
            ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.IdEmissor.Equal(idEmissor),
                                             ativoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
            ativoBolsaCollection.Query.Load();

            foreach (AtivoBolsa ativoBolsa in ativoBolsaCollection)
            {
                string cdAtivoBolsa = ativoBolsa.CdAtivoBolsa;
                
                EvolucaoCapitalSocialCollection evolucaoCapitalSocialCollection = new EvolucaoCapitalSocialCollection();
                evolucaoCapitalSocialCollection.Query.Select(evolucaoCapitalSocialCollection.Query.ValorFinal);
                evolucaoCapitalSocialCollection.Query.Where(evolucaoCapitalSocialCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                            evolucaoCapitalSocialCollection.Query.Data.LessThanOrEqual(data));
                evolucaoCapitalSocialCollection.Query.OrderBy(evolucaoCapitalSocialCollection.Query.Data.Descending);
                evolucaoCapitalSocialCollection.Query.Load();

                if (evolucaoCapitalSocialCollection.Count > 0)
                {
                    valorFinal += evolucaoCapitalSocialCollection[0].ValorFinal.Value;
                }                
            }

            return valorFinal;
            
        }

        public decimal RetornaQuantidade(int idEmissor, DateTime data, TipoEspecificacaoBolsa? tipo) 
        {
            decimal quantidadeFinal = 0;

            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa,
                                              ativoBolsaCollection.Query.Especificacao);
            ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.IdEmissor.Equal(idEmissor),
                                             ativoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
            ativoBolsaCollection.Query.Load();

            foreach (AtivoBolsa ativoBolsa in ativoBolsaCollection) 
            {
                string cdAtivoBolsa = ativoBolsa.CdAtivoBolsa;
                string especificacao = ativoBolsa.Especificacao.Trim();

                EspecificacaoBolsa especificacaoBolsa = new EspecificacaoBolsa();
                especificacaoBolsa.LoadByPrimaryKey(especificacao);

                if (!tipo.HasValue) 
                {
                    #region tipo = null
                    EvolucaoCapitalSocialCollection evolucaoCapitalSocialCollection = new EvolucaoCapitalSocialCollection();
                    evolucaoCapitalSocialCollection.Query.Select(evolucaoCapitalSocialCollection.Query.QuantidadeFinal);
                    evolucaoCapitalSocialCollection.Query.Where(evolucaoCapitalSocialCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                                evolucaoCapitalSocialCollection.Query.Data.LessThanOrEqual(data));
                    evolucaoCapitalSocialCollection.Query.OrderBy(evolucaoCapitalSocialCollection.Query.Data.Descending);
                    evolucaoCapitalSocialCollection.Query.Load();

                    if (evolucaoCapitalSocialCollection.Count > 0) {
                        quantidadeFinal += evolucaoCapitalSocialCollection[0].QuantidadeFinal.Value;
                    }
                    #endregion
                }
                else 
                { // tipo tem Valor
                    if (especificacaoBolsa.es.HasData) 
                    {
                        if (especificacaoBolsa.Tipo.Value == (byte)tipo) 
                        {
                            EvolucaoCapitalSocialCollection evolucaoCapitalSocialCollection = new EvolucaoCapitalSocialCollection();
                            evolucaoCapitalSocialCollection.Query.Select(evolucaoCapitalSocialCollection.Query.QuantidadeFinal);
                            evolucaoCapitalSocialCollection.Query.Where(evolucaoCapitalSocialCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                                        evolucaoCapitalSocialCollection.Query.Data.LessThanOrEqual(data));
                            evolucaoCapitalSocialCollection.Query.OrderBy(evolucaoCapitalSocialCollection.Query.Data.Descending);
                            evolucaoCapitalSocialCollection.Query.Load();

                            if (evolucaoCapitalSocialCollection.Count > 0) 
                            {
                                quantidadeFinal += evolucaoCapitalSocialCollection[0].QuantidadeFinal.Value;
                            }
                        }                    
                    }
                }
            }

            return quantidadeFinal;
        }
	}
}