﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;

namespace Financial.Bolsa {
    public partial class LiquidacaoEmprestimoBolsaCollection : esLiquidacaoEmprestimoBolsaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(LiquidacaoEmprestimoBolsaCollection));

        /// <summary>
        /// Carrega o objeto LiquidacaoEmprestimoBolsa com os campos IdLiquidacao, ValorLiquidacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public bool BuscaValorVencimento(int idOperacao, DateTime data) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdLiquidacao, this.Query.ValorLiquidacao)
                 .Where(this.Query.IdOperacao == idOperacao,
                        this.Query.Data == data);

            bool retorno = this.Query.Load();
            
            return retorno;             
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoEmprestimoBolsa com os campos IdLiquidacao, ValorLiquidacao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public bool BuscaValorVencimentoPorContrato(int numeroContrato, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdLiquidacao, this.Query.ValorLiquidacao)
                 .Where(this.Query.NumeroContrato == numeroContrato,
                        this.Query.Data == data);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de LiquidacaoEmprestimoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaLiquidacaoEmprestimoBolsa(DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.Data.Equal(data));

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoEmprestimoBolsa com os campos IdOperacao, IdAgente, CdAtivoBolsa,
        /// Quantidade, Valor, ValorLiquidacao, IdLiquidacao, IdPosicao, TipoLiquidacao, Fonte.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaLiquidacaoEmprestimoBolsa(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
                .Select(this.Query.IdOperacao, this.Query.IdAgente,
                        this.Query.CdAtivoBolsa, this.Query.Quantidade,
                        this.Query.Valor, this.Query.ValorLiquidacao, this.Query.IdLiquidacao,
                        this.Query.IdPosicao, this.Query.TipoLiquidacao, this.Query.Fonte, this.Query.NumeroContrato)
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.Data.Equal(data));
            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as liquidações de emprestimo pela data e tipo de fonte passados.
        /// Deleta apenas para clientes com dataDia menor que data ou (dataDia = data e Status != Fechado).
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaLiquidacaoEmprestimoBolsa(DateTime data, FonteLiquidacaoEmprestimoBolsa fonte) 
        {
            LiquidacaoEmprestimoBolsaQuery liquidacaoEmprestimoBolsaQuery = new LiquidacaoEmprestimoBolsaQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoEmprestimoBolsaQuery.Select(liquidacaoEmprestimoBolsaQuery.IdLiquidacao);
            liquidacaoEmprestimoBolsaQuery.InnerJoin(clienteQuery).On(liquidacaoEmprestimoBolsaQuery.IdCliente == clienteQuery.IdCliente);
            liquidacaoEmprestimoBolsaQuery.Where(liquidacaoEmprestimoBolsaQuery.Data.Equal(data) &
                                            liquidacaoEmprestimoBolsaQuery.Fonte.Equal(fonte) &
                                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &
                                            (
                                                clienteQuery.DataDia.LessThan(data) |
                                                (clienteQuery.DataDia.Equal(data) & clienteQuery.Status.NotEqual((byte)StatusCliente.Divulgado))
                                            )
                                            );
            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
            liquidacaoEmprestimoBolsaCollection.Load(liquidacaoEmprestimoBolsaQuery);

            liquidacaoEmprestimoBolsaCollection.MarkAllAsDeleted();
            liquidacaoEmprestimoBolsaCollection.Save();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idPosicao"></param>
        public void DeletaLiquidacaoEmprestimoBolsa(DateTime data, int idPosicao)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdLiquidacao)
              .Where(this.Query.Data.Equal(data),
                     this.Query.IdPosicao == idPosicao
              );

            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaLiquidacaoEmprestimoBolsa(int idCliente, DateTime data, FonteLiquidacaoEmprestimoBolsa fonte)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdLiquidacao)
              .Where(this.Query.Data.Equal(data),
                     this.Query.IdCliente.Equal(idCliente),
                     this.Query.Fonte.Equal(fonte));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas as liquidações de empréstimo.
        /// Filtra por:
        ///     TipoLiquidacao.Equal((byte)TipoLiquidacaoEmprestimoBolsa.Vencimento),
        ///     Fonte.NotEqual((byte)FonteLiquidacaoEmprestimoBolsa.ArquivoDBTL)        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaLiquidacaoEmprestimoBolsaVencimento(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdLiquidacao)
              .Where(this.Query.IdCliente.Equal(idCliente),
                     this.Query.Data.Equal(data),
                     this.Query.TipoLiquidacao.Equal((byte)TipoLiquidacaoEmprestimoBolsa.Vencimento),
                     this.Query.Fonte.NotIn((byte)FonteLiquidacaoEmprestimoBolsa.ArquivoDBTL,
                                            (byte)FonteLiquidacaoEmprestimoBolsa.Sinacor));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
    }
}
