﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Common;
using Financial.Util;
using Financial.ContaCorrente.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Financial.ContaCorrente;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Common.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Interfaces.Import.Bolsa;
using Financial.Interfaces.Import.Tema;
using Financial.Tributo;
using Financial.Tributo.Enums;
using Financial.Fundo;

namespace Financial.Bolsa
{
    public partial class OperacaoBolsa : esOperacaoBolsa
    {
        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn)
        {
            if (this.Table != null && !this.Table.Columns.Contains(columnName))
            {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// Leva em conta posições doadas e tomadas de aluguel de ações - BTC.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idEmissor"></param>
        /// <param name="idSetor"></param>
        /// <returns></returns>
        public decimal RetornaValorAcaoEnquadra(int idCliente, DateTime data, int? idEmissor, int? idSetor, int? tipoPapel, string tipoMercado)
        {
            decimal totalValor = 0;

            #region Computa valor em OrdemBolsa, filtrando pelos parametros
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();

            ordemBolsaCollection.Query
                 .Select(ordemBolsaCollection.Query.CdAtivoBolsa,
                         ordemBolsaCollection.Query.TipoOrdem,
                         ordemBolsaCollection.Query.Valor.Sum())
                 .Where(ordemBolsaCollection.Query.IdCliente.Equal(idCliente),
                        ordemBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                        ordemBolsaCollection.Query.Data.Equal(data))
                 .GroupBy(ordemBolsaCollection.Query.CdAtivoBolsa,
                         ordemBolsaCollection.Query.TipoOrdem);

            ordemBolsaCollection.Query.Load();

            List<esQueryItem> campos = new List<esQueryItem>();

            for (int i = 0; i < ordemBolsaCollection.Count; i++)
            {
                OrdemBolsa ordemBolsa = ordemBolsaCollection[i];
                string cdAtivoBolsa = ordemBolsa.CdAtivoBolsa;
                decimal valor = ordemBolsa.Valor.Value;

                string tipoOrdem = ordemBolsa.TipoOrdem;
                if (tipoOrdem == TipoOrdemBolsa.Venda)
                {
                    valor = valor * -1;
                }

                if (idEmissor.HasValue)
                {
                    #region Busca Emissor
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    campos.Add(ativoBolsa.Query.IdEmissor);
                    ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                    int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                    #endregion

                    if (idEmissor == idEmissorComparar)
                        totalValor += valor;
                }
                else if (idSetor.HasValue)
                {
                    #region Busca Setor
                    //Busca antes o IdEmissor
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    campos = new List<esQueryItem>();
                    campos.Add(ativoBolsa.Query.IdEmissor);
                    ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                    int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                    //
                    Emissor emissor = new Emissor();
                    campos = new List<esQueryItem>();
                    campos.Add(emissor.Query.IdSetor);
                    emissor.LoadByPrimaryKey(campos, idEmissorComparar);
                    int idSetorComparar = emissor.IdSetor.Value;
                    #endregion

                    if (idSetor == idSetorComparar)
                        totalValor += valor;
                }
                else if (tipoPapel.HasValue)
                {
                    #region Busca Tipo Papel
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    campos.Add(ativoBolsa.Query.TipoPapel);
                    ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                    int tipoPapelComparar = ativoBolsa.TipoPapel.Value;
                    #endregion

                    if (tipoPapel == tipoPapelComparar)
                        totalValor += valor;
                }
                else if (!String.IsNullOrEmpty(tipoMercado))
                {
                    #region Busca Tipo Mercado
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    campos.Add(ativoBolsa.Query.TipoMercado);
                    ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                    string tipoMercadoComparar = ativoBolsa.TipoMercado;
                    #endregion

                    if (tipoMercado == tipoMercadoComparar)
                        totalValor += valor;
                }
                else
                {
                    totalValor += valor;
                }

            }
            #endregion

            #region Computa valor em OperacaoBolsa, filtrando pelos parametros
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();

            operacaoBolsaCollection.Query
                 .Select(operacaoBolsaCollection.Query.CdAtivoBolsa,
                         operacaoBolsaCollection.Query.TipoOperacao,
                         operacaoBolsaCollection.Query.Valor.Sum())
                 .Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                        operacaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                        operacaoBolsaCollection.Query.Data.Equal(data),
                        operacaoBolsaCollection.Query.TipoOperacao.NotIn(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade),
                        operacaoBolsaCollection.Query.Fonte.NotEqual((byte)FonteOperacaoBolsa.OrdemBolsa))
                 .GroupBy(operacaoBolsaCollection.Query.CdAtivoBolsa,
                         operacaoBolsaCollection.Query.TipoOperacao);

            operacaoBolsaCollection.Query.Load();

            campos = new List<esQueryItem>();

            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                decimal valor = operacaoBolsa.Valor.Value;

                string tipoOperacao = operacaoBolsa.TipoOperacao;
                if (tipoOperacao == TipoOperacaoBolsa.Venda || tipoOperacao == TipoOperacaoBolsa.Retirada)
                {
                    valor = valor * -1;
                }

                if (idEmissor.HasValue)
                {
                    #region Busca Emissor
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    campos.Add(ativoBolsa.Query.IdEmissor);
                    ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                    int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                    #endregion

                    if (idEmissor == idEmissorComparar)
                        totalValor += valor;
                }
                else if (idSetor.HasValue)
                {
                    #region Busca Setor
                    //Busca antes o IdEmissor
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    campos = new List<esQueryItem>();
                    campos.Add(ativoBolsa.Query.IdEmissor);
                    ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                    int idEmissorComparar = ativoBolsa.IdEmissor.Value;
                    //
                    Emissor emissor = new Emissor();
                    campos = new List<esQueryItem>();
                    campos.Add(emissor.Query.IdSetor);
                    emissor.LoadByPrimaryKey(campos, idEmissorComparar);
                    int idSetorComparar = emissor.IdSetor.Value;
                    #endregion

                    if (idSetor == idSetorComparar)
                        totalValor += valor;
                }
                else if (tipoPapel.HasValue)
                {
                    #region Busca Tipo Papel
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    campos.Add(ativoBolsa.Query.TipoPapel);
                    ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                    int tipoPapelComparar = ativoBolsa.TipoPapel.Value;
                    #endregion

                    if (tipoPapel == tipoPapelComparar)
                        totalValor += valor;
                }
                else if (!String.IsNullOrEmpty(tipoMercado))
                {
                    #region Busca Tipo Mercado
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    campos.Add(ativoBolsa.Query.TipoMercado);
                    ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                    string tipoMercadoComparar = ativoBolsa.TipoMercado;
                    #endregion

                    if (tipoMercado == tipoMercadoComparar)
                        totalValor += valor;
                }
                else
                {
                    totalValor += valor;
                }

            }
            #endregion

            return totalValor;
        }

        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorOpcaoEnquadra(int idCliente, DateTime data)
        {
            decimal totalValor = 0;

            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();

            ordemBolsaCollection.Query
                 .Select(ordemBolsaCollection.Query.CdAtivoBolsa,
                         ordemBolsaCollection.Query.TipoOrdem,
                         ordemBolsaCollection.Query.Valor.Sum())
                 .Where(ordemBolsaCollection.Query.IdCliente.Equal(idCliente),
                        ordemBolsaCollection.Query.TipoMercado.NotEqual(TipoMercadoBolsa.MercadoVista),
                        ordemBolsaCollection.Query.Data.Equal(data))
                 .GroupBy(ordemBolsaCollection.Query.CdAtivoBolsa,
                          ordemBolsaCollection.Query.TipoOrdem);

            ordemBolsaCollection.Query.Load();

            for (int i = 0; i < ordemBolsaCollection.Count; i++)
            {
                OrdemBolsa ordemBolsa = ordemBolsaCollection[i];
                string cdAtivoBolsa = ordemBolsa.CdAtivoBolsa;
                decimal valor = ordemBolsa.Valor.Value;

                string tipoOrdem = ordemBolsa.TipoOrdem;
                if (tipoOrdem == TipoOrdemBolsa.Venda)
                {
                    valor = valor * -1;
                }

                totalValor += valor;
            }


            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();

            operacaoBolsaCollection.Query
                 .Select(operacaoBolsaCollection.Query.CdAtivoBolsa,
                         operacaoBolsaCollection.Query.TipoOperacao,
                         operacaoBolsaCollection.Query.Valor.Sum())
                 .Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                        operacaoBolsaCollection.Query.TipoMercado.NotEqual(TipoMercadoBolsa.MercadoVista),
                        operacaoBolsaCollection.Query.Data.Equal(data),
                        operacaoBolsaCollection.Query.TipoOperacao.NotIn(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade),
                        operacaoBolsaCollection.Query.Fonte.NotEqual((byte)FonteOperacaoBolsa.OrdemBolsa))
                 .GroupBy(operacaoBolsaCollection.Query.CdAtivoBolsa,
                          operacaoBolsaCollection.Query.TipoOperacao);

            operacaoBolsaCollection.Query.Load();

            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                decimal valor = operacaoBolsa.Valor.Value;

                string tipoOperacao = operacaoBolsa.TipoOperacao;
                if (tipoOperacao == TipoOperacaoBolsa.Venda || tipoOperacao == TipoOperacaoBolsa.Retirada)
                {
                    valor = valor * -1;
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoOperacao"></param>
        /// <param name="origemOperacao"></param>
        /// <param name="tipoMercado">TipoMercado aceita duas comparações: 
        ///                           Usar: TipoMercadoBolsa1','TipoMercadoBolsa2 </param>
        /// <param name="idAgente"></param>
        /// <returns> retorna 0 se não foi achado registro</returns>
        /// throws ArgumentException se 
        /// tipoOperacao for diferente dos tipos do enum TipoOperacaoBolsa
        /// origemOperacao for diferente dos tipos do enum OrigemOperacaoBolsa
        /// tipoMercado for diferente dos tipos do enum TipoMercadoBolsa
        public decimal BuscaValorSum(int idCliente, DateTime data, string tipoOperacao, int origemOperacao,
                                          string tipoMercado, int idAgente)
        {
            #region ArgumentosIncorretos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();

            List<string> valoresPossiveis = TipoOperacaoBolsa.Values();
            if (!valoresPossiveis.Contains(tipoOperacao))
            {
                mensagem.Append("tipoOperacao incorreto. Os valores possiveis estão no enum TipoOperacaoBolsa");
                throw new ArgumentException(mensagem.ToString());
            }

            List<int> valoresPossiveis1 = OrigemOperacaoBolsa.Values();
            if (!valoresPossiveis1.Contains(origemOperacao))
            {
                mensagem.Append("origemOperacao incorreto. Os valores possiveis estão no enum OrigemOperacaoBolsa");
                throw new ArgumentException(mensagem.ToString());
            }

            if (tipoMercado.Contains(","))
            {
                char[] caracter = new char[] { ',' };
                StringTokenizer token = new StringTokenizer(tipoMercado, caracter);
                foreach (string item in token)
                {
                    List<string> valoresPossiveis2 = TipoMercadoBolsa.Values();
                    // remove as aspas simples
                    string item1 = item.Replace("'", "");
                    if (!valoresPossiveis2.Contains(item1.Trim()))
                    {
                        mensagem.Append("tipoMercado incorreto. Os valores possiveis estão no enum TipoMercadoBolsa");
                        throw new ArgumentException(mensagem.ToString());
                    }
                }
            }
            else
            {
                List<string> valoresPossiveis3 = TipoMercadoBolsa.Values();
                if (!valoresPossiveis3.Contains(tipoMercado))
                {
                    mensagem.Append("tipoMercado incorreto. Os valores possiveis estão no enum TipoMercadoBolsa");
                    throw new ArgumentException(mensagem.ToString());
                }
            }

            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Equal(data),
                        this.Query.TipoOperacao.Equal(tipoOperacao),
                        this.Query.Origem == origemOperacao,
                        this.Query.TipoMercado.In(tipoMercado),
                        this.Query.IdAgenteCorretora == idAgente);

            this.Query.Load();

            decimal valor = (this.Valor.HasValue) ? this.Valor.Value : 0;

            return valor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaCorretagem(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaCorretorasOperadas(idCliente, data);
            //
            // Para cada Corretora Operada            
            string tipoMercado = "";
            StringBuilder tipoOperacao = new StringBuilder();
            tipoOperacao.Append(TipoOperacaoBolsa.Compra + "','")
                        .Append(TipoOperacaoBolsa.Venda + "','")
                        .Append(TipoOperacaoBolsa.CompraDaytrade + "','")
                        .Append(TipoOperacaoBolsa.VendaDaytrade);

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                int idAgente = operacaoBolsa.IdAgenteCorretora.Value;
                //
                AgenteMercado agenteMercado = new AgenteMercado();
                agenteMercado.LoadByPrimaryKey(idAgente);

                decimal volumeGeral = 0;

                #region Calcula Volume para MercadoVista
                OperacaoBolsaCollection operacaoBolsaCollectionMercadoVista = new OperacaoBolsaCollection();
                tipoMercado = TipoMercadoBolsa.MercadoVista;
                operacaoBolsaCollectionMercadoVista.BuscaOperacaoBolsa(idCliente, data, tipoMercado, tipoOperacao.ToString(), idAgente);

                OperacaoBolsaCollection operacaoBolsaCollectionMercadoFII = new OperacaoBolsaCollection();
                tipoMercado = TipoMercadoBolsa.Imobiliario;
                operacaoBolsaCollectionMercadoFII.BuscaOperacaoBolsa(idCliente, data, tipoMercado, tipoOperacao.ToString(), idAgente);
                //

                operacaoBolsaCollectionMercadoVista.Combine(operacaoBolsaCollectionMercadoFII);
                decimal volumeVista = 0;
                foreach (OperacaoBolsa operacaoBolsaMercadoVista in operacaoBolsaCollectionMercadoVista)
                {
                    decimal valor = operacaoBolsaMercadoVista.Valor.Value;
                    volumeVista += valor;
                    volumeGeral += valor;
                }
                #endregion

                #region Calcula Volume para OPC/OPV
                OperacaoBolsaCollection operacaoBolsaCollectionMercadoOpcao = new OperacaoBolsaCollection();
                tipoMercado = TipoMercadoBolsa.OpcaoCompra + "','" + TipoMercadoBolsa.OpcaoVenda;

                operacaoBolsaCollectionMercadoOpcao.BuscaOperacaoBolsa(idCliente, data, tipoMercado, tipoOperacao.ToString(), idAgente);
                //
                decimal volumeOpcao = 0;
                foreach (OperacaoBolsa operacaoBolsaOpcao in operacaoBolsaCollectionMercadoOpcao)
                {
                    decimal valor = operacaoBolsaOpcao.Valor.Value;
                    volumeOpcao += valor;
                    volumeGeral += valor;
                }
                #endregion

                #region Calcula Volume para Termo
                OperacaoBolsaCollection operacaoBolsaCollectionMercadoTermo = new OperacaoBolsaCollection();
                tipoMercado = TipoMercadoBolsa.Termo;
                operacaoBolsaCollectionMercadoTermo.BuscaOperacaoBolsa(idCliente, data, tipoMercado, tipoOperacao.ToString(), idAgente);
                //
                decimal volumeTermo = 0;
                foreach (OperacaoBolsa operacaoBolsaTermo in operacaoBolsaCollectionMercadoTermo)
                {
                    decimal valor = operacaoBolsaTermo.Valor.Value;
                    volumeTermo += valor;
                    volumeGeral += valor;
                }
                #endregion

                #region Calcula Volume para Futuro
                OperacaoBolsaCollection operacaoBolsaCollectionMercadoFuturo = new OperacaoBolsaCollection();
                tipoMercado = TipoMercadoBolsa.Futuro;
                operacaoBolsaCollectionMercadoFuturo.BuscaOperacaoBolsa(idCliente, data, tipoMercado, tipoOperacao.ToString(), idAgente);
                //
                decimal volumeFuturo = 0;
                foreach (OperacaoBolsa operacaoBolsaFuturo in operacaoBolsaCollectionMercadoTermo)
                {
                    decimal valor = operacaoBolsaFuturo.Valor.Value;
                    volumeFuturo += valor;
                    volumeGeral += valor;
                }
                #endregion

                decimal proporcionalValorAdicional;

                if (agenteMercado.IsCorretagemAdicionalPorMercado())
                {

                    #region CorretagemAdicionalPorMercado
                    // Valor Adicional é usado integralmente em todos os mercados
                    proporcionalValorAdicional = 1;

                    #region Corretagem MercadoVista
                    // Calcula Corretagem para MercadoVista
                    if (operacaoBolsaCollectionMercadoVista.HasData)
                    {
                        this.CalculaCorretagem(idCliente, data, idAgente, operacaoBolsaCollectionMercadoVista,
                                           volumeGeral, volumeVista, proporcionalValorAdicional);
                    }
                    #endregion

                    #region Corretagem MercadoOpcao
                    // Calcula Corretagem para MercadoOpcao
                    if (operacaoBolsaCollectionMercadoOpcao.HasData)
                    {
                        this.CalculaCorretagem(idCliente, data, idAgente, operacaoBolsaCollectionMercadoOpcao,
                                           volumeGeral, volumeOpcao, proporcionalValorAdicional);
                    }
                    #endregion

                    #region Corretagem MercadoTermo
                    // Calcula Corretagem para MercadoTermo
                    if (operacaoBolsaCollectionMercadoTermo.HasData)
                    {
                        this.CalculaCorretagem(idCliente, data, idAgente, operacaoBolsaCollectionMercadoTermo,
                                           volumeGeral, volumeTermo, proporcionalValorAdicional);
                    }
                    #endregion

                    #region Corretagem MercadoFuturo
                    // Calcula Corretagem para MercadoFuturo
                    if (operacaoBolsaCollectionMercadoFuturo.HasData)
                    {
                        this.CalculaCorretagem(idCliente, data, idAgente, operacaoBolsaCollectionMercadoFuturo,
                                           volumeGeral, volumeFuturo, proporcionalValorAdicional);
                    }
                    #endregion

                    #endregion

                }
                else if (agenteMercado.IsCorretagemAdicionalRateioAcoes())
                {

                    #region Rateio Acoes

                    #region Corretagem MercadoVista
                    // Calcula Corretagem para MercadoVista
                    proporcionalValorAdicional = 1;
                    //
                    if (operacaoBolsaCollectionMercadoVista.HasData)
                    {
                        this.CalculaCorretagem(idCliente, data, idAgente, operacaoBolsaCollectionMercadoVista,
                                           volumeGeral, volumeVista, proporcionalValorAdicional);
                    }

                    #endregion

                    #region Corretagem MercadoOpcao
                    // Calcula Corretagem para MercadoOpcao                                                  
                    // Não operou no mercado a vista e operou opções - rateio vai para opção
                    if (volumeVista == 0 && volumeOpcao != 0)
                    {
                        proporcionalValorAdicional = 1;
                    }
                    else
                    {
                        proporcionalValorAdicional = 0; //deve ser zerado pois opção não recebe nada do VlAdicional                    
                    }
                    //
                    if (operacaoBolsaCollectionMercadoOpcao.HasData)
                    {
                        this.CalculaCorretagem(idCliente, data, idAgente, operacaoBolsaCollectionMercadoOpcao,
                                           volumeGeral, volumeOpcao, proporcionalValorAdicional);
                    }
                    #endregion

                    #region Corretagem MercadoTermo
                    // Calcula Corretagem para MercadoTermo
                    // Não operou no mercado a vista e operou termo - rateio vai para Termo
                    if (volumeVista == 0 && volumeTermo != 0)
                    {
                        proporcionalValorAdicional = 1;
                    }
                    else
                    {
                        proporcionalValorAdicional = 0; //deve ser zerado pois Termo não recebe nada do VlAdicional                    
                    }
                    //
                    if (operacaoBolsaCollectionMercadoTermo.HasData)
                    {
                        this.CalculaCorretagem(idCliente, data, idAgente, operacaoBolsaCollectionMercadoTermo,
                                           volumeGeral, volumeTermo, proporcionalValorAdicional);
                    }
                    #endregion

                    #region Corretagem MercadoFuturo
                    // Calcula Corretagem para MercadoFuturo
                    // Não operou no mercado Vista e operou Futuro de ações - rateio vai para Futuro
                    if (volumeVista == 0 && volumeFuturo != 0)
                    {
                        proporcionalValorAdicional = 1;
                    }
                    else
                    {
                        proporcionalValorAdicional = 0; //deve ser zerado pois Futuro não recebe nada do VlAdicional
                    }
                    //
                    if (operacaoBolsaCollectionMercadoFuturo.HasData)
                    {
                        this.CalculaCorretagem(idCliente, data, idAgente, operacaoBolsaCollectionMercadoFuturo,
                                           volumeGeral, volumeFuturo, proporcionalValorAdicional);
                    }
                    #endregion

                    #endregion
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idAgente"></param>
        /// <param name="operacaoBolsaCollection"></param>
        /// <param name="totalValor"></param>
        /// <param name="proporcionalValorAdicional"></param>
        private void CalculaCorretagem(int idCliente, DateTime data, int idAgente,
                                        OperacaoBolsaCollection operacaoBolsaCollection,
                                        decimal volumeGeral, decimal volumeMercado,
                                        decimal proporcionalValorAdicional)
        {
            //Verifica o tipo de mercado da collection passada
            string tipoMercado = "";
            if (operacaoBolsaCollection.Count > 0)
            {
                tipoMercado = operacaoBolsaCollection[0].TipoMercado;
                if (tipoMercado == TipoMercadoBolsa.Imobiliario)
                {
                    tipoMercado = TipoMercadoBolsa.MercadoVista;
                }
            }

            PerfilCorretagemBolsa perfilCorretagemBolsa = new PerfilCorretagemBolsa();
            perfilCorretagemBolsa.BuscaPerfilCorretagemBolsa(idCliente, idAgente, data, tipoMercado);
            int idTemplate = perfilCorretagemBolsa.IdTemplate.Value;
            decimal descontoPerfil = perfilCorretagemBolsa.PercentualDesconto.Value / 100;
            decimal iss = perfilCorretagemBolsa.Iss.Value / 100;
            decimal corretagem = 0;

            TemplateCorretagemBolsa templateCorretagemBolsa = perfilCorretagemBolsa.UpToTemplateCorretagemBolsaByIdTemplate;
            decimal? valorFixoOperacao = templateCorretagemBolsa.ValorFixoOperacao;
            decimal? teto = templateCorretagemBolsa.Teto;
            decimal descontoTemplate = templateCorretagemBolsa.PercentualDesconto.Value / 100;

            // Corretagem por Valor Fixo (calcula pelo número de ordens do dia)
            if (templateCorretagemBolsa.IsTipoCorretagemValorFixo())
            {
                OrdemBolsa ordemBolsa = new OrdemBolsa();
                int numeroOrdensDia = ordemBolsa.BuscaNumeroOrdens(idCliente, data, idAgente);
                corretagem = Utilitario.Truncate((numeroOrdensDia * valorFixoOperacao.Value), 2);
            }
            // Corretagem por Quantidade de Ações (calcula pela quantidade de ações operadas)
            else if (templateCorretagemBolsa.IsTipoCorretagemQuantidadeAcoes())
            {
                OrdemBolsa ordemBolsa = new OrdemBolsa();
                decimal quantidadeAcoes = ordemBolsa.BuscaQuantidadeOperada(idCliente, data, idAgente);
                corretagem = Utilitario.Truncate((quantidadeAcoes * valorFixoOperacao.Value), 2);
            }
            // Corretagem por Faixa de Volume
            else if (templateCorretagemBolsa.IsTipoCorretagemPorVolume())
            {
                TabelaCorretagemBolsa tabelaCorretagemBolsa = new TabelaCorretagemBolsa();
                tabelaCorretagemBolsa.BuscaTabelaCorretagemBolsa(idTemplate, volumeGeral);
                decimal percentualTabela = tabelaCorretagemBolsa.Percentual.Value;
                decimal valorAdicional = tabelaCorretagemBolsa.ValorAdicional.Value;

                // Proporcionalização valor adicional pelo valor do mercado operado                                      
                valorAdicional = Math.Round(valorAdicional * (proporcionalValorAdicional), 2);
                //
                corretagem = Utilitario.Truncate((volumeMercado * percentualTabela / 100), 2) + valorAdicional;
            }

            if (descontoPerfil != 0)
            {
                decimal desconto = Utilitario.Truncate((corretagem * descontoPerfil), 2);
                corretagem = corretagem - desconto;
            }
            else if (descontoTemplate != 0)
            {
                decimal desconto = Utilitario.Truncate((corretagem * descontoTemplate), 2);
                corretagem = corretagem - desconto;
            }

            if (teto.HasValue)
            {
                if (corretagem > teto.Value)
                {
                    corretagem = teto.Value;
                }
            }

            decimal valorISS = 0;
            if (iss != 0) //Soma o ISS à corretagem
            {
                valorISS = Math.Round(iss * corretagem, 2);
                corretagem = corretagem + valorISS;
            }

            if (templateCorretagemBolsa.IsTipoCorretagemValorFixo())
            {
                foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
                {
                    operacaoBolsa.Corretagem = valorFixoOperacao.Value;
                }
                operacaoBolsaCollection.Save();
            }
            else
            {
                this.RateiaCorretagem(operacaoBolsaCollection, corretagem, valorISS, volumeMercado);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaTaxas(int idCliente, DateTime data)
        {
            // Pega o Tipo do Cliente
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            int tipoCliente = cliente.IdTipo.Value;
            //
            // Pega o idCorretora das operações feitas pelo cliente no dia
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaCorretorasOperadas(idCliente, data);
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                int idAgenteCorretora = operacaoBolsa.IdAgenteCorretora.Value;
                //                
                this.CalculaTaxasMercadoVistaDaytrade(idCliente, idAgenteCorretora, data);
                this.CalculaTaxasMercadoVistaFinal(tipoCliente, idCliente, idAgenteCorretora, data);
                this.CalculaTaxasExercicioOPC(idCliente, idAgenteCorretora, data);
                this.CalculaTaxasTermo(idCliente, idAgenteCorretora, data);
                this.CalculaTaxasFuturoFinal(idCliente, idAgenteCorretora, data);
                this.CalculaTaxasFuturoDaytrade(idCliente, idAgenteCorretora, data);
                this.CalculaTaxasOpcoesFinal(tipoCliente, idCliente, idAgenteCorretora, data);
                this.CalculaTaxasOpcoesDaytrade(idCliente, idAgenteCorretora, data);
                this.CalculaTaxasMercadoADR(idCliente, idAgenteCorretora, data);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        private void CalculaTaxasOpcoesDaytrade(int idCliente, int idAgenteCorretora, DateTime data)
        {
            // Opcoes Operacoes DayTrade
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            string tipoOperacao = TipoOperacaoBolsa.CompraDaytrade + "','" + TipoOperacaoBolsa.VendaDaytrade;
            string tipoMercado = TipoMercadoBolsa.OpcaoCompra + "','" + TipoMercadoBolsa.OpcaoVenda;
            string origem = Convert.ToString(OrigemOperacaoBolsa.Primaria);
            operacaoBolsaCollection.BuscaOperacaoBolsaCalculaTaxa(idCliente, data, tipoOperacao, tipoMercado, origem, idAgenteCorretora);
            //
            int idTabela = TipoTaxaBolsa.MercadoOpcao.DayTrade;
            //
            if (operacaoBolsaCollection.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollection, data, idTabela);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoCliente"></param>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        /// throws ArgumentException se tipoCliente for diferente dos tipos do enum TipoClienteFixo
        private void CalculaTaxasOpcoesFinal(int tipoCliente, int idCliente, int idAgenteCorretora, DateTime data)
        {

            #region ArgumentosIncorreto - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();

            List<int> valoresPossiveis = TipoClienteFixo.Values();
            if (!valoresPossiveis.Contains(tipoCliente))
            {
                mensagem.Append("tipoCliente incorreto. Os valores possiveis estão no enum TipoClienteFixo");
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            // Opcoes Operacoes Finais
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            string tipoOperacao = TipoOperacaoBolsa.Compra + "','" + TipoOperacaoBolsa.Venda;
            string tipoMercado = TipoMercadoBolsa.OpcaoCompra + "','" + TipoMercadoBolsa.OpcaoVenda;
            string origem = Convert.ToString(OrigemOperacaoBolsa.Primaria);
            operacaoBolsaCollection.BuscaOperacaoBolsaCalculaTaxa(idCliente, data, tipoOperacao, tipoMercado, origem, idAgenteCorretora);
            //
            // Busca a taxa de acordo com o tipo Cliente
            int idTabela;
            switch (tipoCliente)
            {
                case TipoClienteFixo.CarteiraPropria:
                    idTabela = TipoTaxaBolsa.MercadoOpcao.CarteiraPropria;
                    break;
                case TipoClienteFixo.Clube:
                    idTabela = TipoTaxaBolsa.MercadoOpcao.Clube;
                    break;
                case TipoClienteFixo.Fundo:
                    idTabela = TipoTaxaBolsa.MercadoOpcao.Fundo;
                    break;
                default:
                    idTabela = TipoTaxaBolsa.MercadoOpcao.Final;
                    break;
            }

            //
            if (operacaoBolsaCollection.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollection, data, idTabela);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        private void CalculaTaxasFuturoDaytrade(int idCliente, int idAgenteCorretora, DateTime data)
        {
            // Futuro daytrade
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            string tipoOperacao = TipoOperacaoBolsa.CompraDaytrade + "','" + TipoOperacaoBolsa.VendaDaytrade;
            string tipoMercado = TipoMercadoBolsa.Futuro;
            string origem = null;
            operacaoBolsaCollection.BuscaOperacaoBolsaCalculaTaxa(idCliente, data, tipoOperacao, tipoMercado, origem, idAgenteCorretora);
            //
            // verificar
            int idTabela = TipoTaxaBolsa.MercadoFuturo.DayTrade;
            //
            if (operacaoBolsaCollection.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollection, data, idTabela);
            }
        }

        /// <summary>
        /// 
        /// </summary>      
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        private void CalculaTaxasFuturoFinal(int idCliente, int idAgenteCorretora, DateTime data)
        {
            // Futuro final
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            string tipoOperacao = null;
            string tipoMercado = TipoMercadoBolsa.Futuro;
            string origem = Convert.ToString(OrigemOperacaoBolsa.Primaria);
            operacaoBolsaCollection.BuscaOperacaoBolsaCalculaTaxa(idCliente, data, tipoOperacao, tipoMercado, origem, idAgenteCorretora);
            //
            // verificar
            int idTabela = TipoTaxaBolsa.MercadoFuturo.Final;
            //
            if (operacaoBolsaCollection.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollection, data, idTabela);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        private void CalculaTaxasTermo(int idCliente, int idAgenteCorretora, DateTime data)
        {
            // Termo
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            string tipoOperacao = null;
            string tipoMercado = TipoMercadoBolsa.Termo;
            string origem = Convert.ToString(OrigemOperacaoBolsa.Primaria);
            operacaoBolsaCollection.BuscaOperacaoBolsaCalculaTaxa(idCliente, data, tipoOperacao, tipoMercado, origem, idAgenteCorretora);
            //            
            int idTabela = TipoTaxaBolsa.MercadoTermo;
            //
            if (operacaoBolsaCollection.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollection, data, idTabela);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        private void CalculaTaxasExercicioOPC(int idCliente, int idAgenteCorretora, DateTime data)
        {
            // Exercicio de posicoes lancadas OPC
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            string tipoOperacao = TipoOperacaoBolsa.Venda;
            string tipoMercado = TipoMercadoBolsa.MercadoVista;
            string origem = Convert.ToString(OrigemOperacaoBolsa.ExercicioOpcaoCompra);
            operacaoBolsaCollection.BuscaOperacaoBolsaCalculaTaxa(idCliente, data, tipoOperacao, tipoMercado, origem, idAgenteCorretora);
            //
            int idTabela = TipoTaxaBolsa.MercadoVista.ExercicioOpcaoLancadaOPC;
            //
            if (operacaoBolsaCollection.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollection, data, idTabela);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoCliente"></param>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        /// throws ArgumentException se tipoCliente for diferente dos tipos do enum TipoClienteFixo
        private void CalculaTaxasMercadoVistaFinal(int tipoCliente, int idCliente, int idAgenteCorretora, DateTime data)
        {
            #region ArgumentosIncorreto - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();

            List<int> valoresPossiveis = TipoClienteFixo.Values();
            if (!valoresPossiveis.Contains(tipoCliente))
            {
                mensagem.Append("tipoCliente incorreto. Os valores possiveis estão no enum TipoClienteFixo");
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            // MercadoVista Operacoes Finais (Inclui FII na consulta tb)
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaCalculaTaxa(idCliente, idAgenteCorretora, data);
            //

            OperacaoBolsaCollection operacaoBolsaCollectionBovespa = new OperacaoBolsaCollection(); //Operações cursadas no ambiente Bovespa
            OperacaoBolsaCollection operacaoBolsaCollectionBalcao = new OperacaoBolsaCollection(); //Operações cursadas no ambiente Balcão

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                if (Utilitario.Right(operacaoBolsa.CdAtivoBolsa, 1) == "B") //Indicativo de ativo Balcão
                {
                    operacaoBolsaCollectionBalcao.AttachEntity(operacaoBolsa);
                }
                else
                {
                    operacaoBolsaCollectionBovespa.AttachEntity(operacaoBolsa);
                }
            }

            // Busca a taxa de acordo com o tipo Cliente (para operações cursadas no ambiente Bovespa)
            int idTabela;
            switch (tipoCliente)
            {
                case TipoClienteFixo.CarteiraPropria:
                    idTabela = TipoTaxaBolsa.MercadoVista.CarteiraPropria;
                    break;
                case TipoClienteFixo.Clube:
                    idTabela = TipoTaxaBolsa.MercadoVista.Clube;
                    break;
                case TipoClienteFixo.Fundo:
                    idTabela = TipoTaxaBolsa.MercadoVista.Fundo;
                    break;
                default:
                    idTabela = TipoTaxaBolsa.MercadoVista.Final;
                    break;
            }
            //
            if (operacaoBolsaCollectionBovespa.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollectionBovespa, data, idTabela);
            }

            idTabela = TipoTaxaBolsa.Balcao;
            if (operacaoBolsaCollectionBalcao.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollectionBalcao, data, idTabela);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        /// throws ArgumentException se tipoCliente for diferente dos tipos do enum TipoClienteFixo
        private void CalculaTaxasMercadoVistaDaytrade(int idCliente, int idAgenteCorretora, DateTime data)
        {
            // MercadoVista Operacoes DayTrade
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            string tipoOperacao = TipoOperacaoBolsa.CompraDaytrade + "','" + TipoOperacaoBolsa.VendaDaytrade;
            string tipoMercado = TipoMercadoBolsa.MercadoVista + "','" + TipoMercadoBolsa.Imobiliario;
            string origem = null;
            operacaoBolsaCollection.BuscaOperacaoBolsaCalculaTaxa(idCliente, data, tipoOperacao, tipoMercado, origem, idAgenteCorretora);
            //

            OperacaoBolsaCollection operacaoBolsaCollectionBovespa = new OperacaoBolsaCollection(); //Operações cursadas no ambiente Bovespa
            OperacaoBolsaCollection operacaoBolsaCollectionBalcao = new OperacaoBolsaCollection(); //Operações cursadas no ambiente Balcão

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                if (Utilitario.Right(operacaoBolsa.CdAtivoBolsa, 1) == "B") //Indicativo de ativo Balcão
                {
                    operacaoBolsaCollectionBalcao.AttachEntity(operacaoBolsa);
                }
                else
                {
                    operacaoBolsaCollectionBovespa.AttachEntity(operacaoBolsa);
                }
            }

            if (operacaoBolsaCollectionBovespa.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollectionBovespa, data, TipoTaxaBolsa.MercadoVista.DayTrade);
            }

            if (operacaoBolsaCollectionBalcao.HasData)
            {
                this.CalculaTaxas(operacaoBolsaCollectionBalcao, data, TipoTaxaBolsa.Balcao);
            }
        }

        /// <summary>
        /// Calcula Sec Fee para vendas de ADRs ou ações Nyse/Nasdaq.
        /// </summary>
        /// <param name="tipoCliente"></param>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        /// throws ArgumentException se tipoCliente for diferente dos tipos do enum TipoClienteFixo
        private void CalculaTaxasMercadoADR(int idCliente, int idAgenteCorretora, DateTime data)
        {
            // MercadoVista Operacoes Finais
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaCalculaTaxaADR(idCliente, idAgenteCorretora, data);
            //
            // Busca a taxa de acordo com o tipo Cliente
            decimal SECFee = 0.00169M;
            //
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                operacaoBolsa.Emolumento = Math.Round(operacaoBolsa.Valor.Value * SECFee / 100M, 2);
            }
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operacaoBolsaCollection"></param>
        /// <param name="data"></param>
        /// <param name="tipoTaxa"></param>
        /// throw ArgumentException se tipoTaxa não estiver dentro dos valores do enum TipoTaxaBolsa   
        private void CalculaTaxas(OperacaoBolsaCollection operacaoBolsaCollection, DateTime data, int tipoTaxa)
        {
            #region ArgumentoIncorreto - throw ArgumentException
            List<int> valoresPossiveis = TipoTaxaBolsa.Values();
            if (!valoresPossiveis.Contains(tipoTaxa))
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("tipoTaxa incorreto. Os valores possiveis estão no enum TipoTaxaBolsa");
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            decimal totalValor = 0;
            foreach (OperacaoBolsa operacao in operacaoBolsaCollection)
            {
                totalValor += operacao.Valor.Value;
            }

            TabelaCustosBolsa tabelaCustosBolsa = new TabelaCustosBolsa();
            tabelaCustosBolsa.BuscaTabelaCustosBolsa(data, tipoTaxa);
            decimal percentualEmolumento = tabelaCustosBolsa.PercentualEmolumentoBolsa.Value;
            decimal percentualRegistroBolsa = tabelaCustosBolsa.PercentualRegistroBolsa.Value;
            decimal percentualTaxaLiquidacao = tabelaCustosBolsa.PercentualLiquidacaoCBLC.Value;
            decimal percentualRegistroCBLC = tabelaCustosBolsa.PercentualRegistroCBLC.Value;
            //           
            decimal emolumento = Utilitario.Truncate((percentualEmolumento / 100M * totalValor), 2);
            decimal registroBolsa = Utilitario.Truncate((percentualRegistroBolsa / 100M * totalValor), 2);
            decimal taxaLiquidacao = Utilitario.Truncate((percentualTaxaLiquidacao / 100M * totalValor), 2);
            decimal registroCBLC = Utilitario.Truncate((percentualRegistroCBLC / 100M * totalValor), 2);

            this.RateiaTaxas(operacaoBolsaCollection, emolumento, registroBolsa, taxaLiquidacao,
                                                        registroCBLC, totalValor);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operacaoBolsaCollection"></param>
        /// <param name="data"></param>
        /// <param name="taxa"></param>
        private void CalculaTaxas(OperacaoBolsaCollection operacaoBolsaCollection, DateTime data, decimal taxa)
        {
            decimal totalValor = 0;
            foreach (OperacaoBolsa operacao in operacaoBolsaCollection)
            {
                totalValor += operacao.Valor.Value;
            }

            decimal emolumento = Utilitario.Truncate((taxa / 100M * totalValor), 2);

            this.RateiaTaxas(operacaoBolsaCollection, emolumento, 0, 0, 0, totalValor);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operacaoBolsaCollection">Todas as operações de um cliente em uma corretora</param>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operacaoBolsaCollection">Todas as operações de um cliente em uma corretora</param>
        public void RateiaCorretagem(OperacaoBolsaCollection operacaoBolsaCollection)
        {
            decimal totalCorretagem = 0;
            decimal totalValor = 0;

            #region Calcula TotalCorretagem e TotalValor
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                totalCorretagem += operacaoBolsa.Corretagem.Value;
                totalValor += operacaoBolsa.Valor.Value;
            }
            #endregion

            #region Rateia Corretagem
            decimal corretagemResidual = totalCorretagem;
            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                decimal valor = operacaoBolsa.Valor.Value;
                decimal corretagem = (totalCorretagem * valor) / totalValor;
                decimal corretagemTruncada = Utilitario.Truncate(corretagem, 2);

                corretagemResidual -= corretagemTruncada;

                // Se é a ultima operacao da corretora, garante-se que 100% da corretagem 
                // foi distribuida, jogando todo o residual para a ultima operacao
                if (i == operacaoBolsaCollection.Count - 1)
                {
                    corretagemTruncada += corretagemResidual;
                }

                operacaoBolsa.Corretagem = corretagemTruncada;
            }
            #endregion

            // Salva a collection de operações
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Rateia a corretagem passada pelas operações na collection, ponderando pelo volume de cada operação.
        /// Realiza tb o cálculo do desconto individual da operação, se ela tiver um percentual específico!
        /// </summary>
        /// <param name="totalCorretagem">totalCorretagem</param>
        /// <param name="totalValor">totalValor</param>
        /// <param name="operacaoBolsaCollection">Todas as operações de um cliente em uma corretora</param>
        public void RateiaCorretagem(OperacaoBolsaCollection operacaoBolsaCollection, decimal totalCorretagem, decimal totalISS, decimal totalValor, bool calculaLiquido)
        {
            decimal corretagemResidual = totalCorretagem;
            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                decimal valor = operacaoBolsa.Valor.Value;
                decimal corretagem = 0;
                if (totalValor != 0)
                    corretagem = (totalCorretagem * valor) / totalValor;
                decimal corretagemTruncada = Utilitario.Truncate(corretagem, 2);

                corretagemResidual -= corretagemTruncada;

                // Se é a ultima operacao da corretora, garante-se que 100% da corretagem 
                // foi distribuida, jogando todo o residual para a ultima operacao
                if (i == operacaoBolsaCollection.Count - 1)
                {
                    corretagemTruncada += corretagemResidual;
                }

                //Aplica o percentual específico da operação!
                if (operacaoBolsa.PercentualDesconto.Value != 0)
                {
                    decimal desconto = Utilitario.Truncate(corretagemTruncada * operacaoBolsa.PercentualDesconto.Value / 100M, 2);
                    corretagemTruncada -= desconto;
                }

                operacaoBolsa.Corretagem = corretagemTruncada;

                if (i == operacaoBolsaCollection.Count - 1) //Joga todo o ISS na última operação
                {
                    operacaoBolsa.ValorISS = totalISS;
                }

                if (calculaLiquido)
                {
                    decimal fatorCotacao = (operacaoBolsa.Quantidade.Value * operacaoBolsa.Pu.Value) / operacaoBolsa.Valor.Value;

                    decimal valorLiquido = operacaoBolsa.Valor.Value - operacaoBolsa.Corretagem.Value;
                    decimal puLiquido = (valorLiquido * fatorCotacao) / operacaoBolsa.Quantidade.Value;

                    operacaoBolsa.ValorLiquido = valorLiquido;
                    operacaoBolsa.PULiquido = puLiquido;
                }
            }

            // Salva a collection de operações
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Rateia a corretagem passada pelas operações na collection, ponderando pelo volume de cada operação.
        /// Realiza tb o cálculo do desconto individual da operação, se ela tiver um percentual específico!
        /// </summary>
        /// <param name="totalCorretagem">totalCorretagem</param>
        /// <param name="totalValor">totalValor</param>
        /// <param name="operacaoBolsaCollection">Todas as operações de um cliente em uma corretora</param>
        public void RateiaCorretagem(OperacaoBolsaCollection operacaoBolsaCollection, decimal totalCorretagem, decimal totalISS, decimal totalValor)
        {
            this.RateiaCorretagem(operacaoBolsaCollection, totalCorretagem, totalISS, totalValor, false);
        }

        /// <summary>
        /// Recompõe os custos das opções exercidas, levando-se em conta os custos dos premios das posicoes das opcoes exercidas.
        /// Baixa tb as posicoes das opcoes que foram exercidas.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void TrataExercicioOpcao(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            //
            operacaoBolsaCollection.BuscaOperacaoBolsaCompoeCustoOpcao(idCliente, data);

            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                // Altera os Custos das opções.
                this.CompoeCustoOpcao(operacaoBolsa);
                //Baixa as opções exercidas
                this.BaixaOpcaoExercida(operacaoBolsa);
            }
            // Salva todas as Alterações no final
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Baixa as opcoes que foram exercidas.
        /// </summary>
        /// <param name="operacaoBolsa"></param>
        private void BaixaOpcaoExercida(OperacaoBolsa operacaoBolsa)
        {
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            posicaoBolsa.Query.Select(posicaoBolsa.Query.IdPosicao, posicaoBolsa.Query.Quantidade, posicaoBolsa.Query.TipoMercado);
            posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(operacaoBolsa.IdCliente.Value),
                                     posicaoBolsa.Query.CdAtivoBolsa.Equal(operacaoBolsa.CdAtivoBolsaOpcao),
                                     posicaoBolsa.Query.IdAgente.Equal(operacaoBolsa.IdAgenteLiquidacao.Value),
                                     posicaoBolsa.Query.Quantidade.NotEqual(0));

            if (posicaoBolsa.Query.Load())
            {
                string tipoMercado = posicaoBolsa.TipoMercado;

                if (operacaoBolsa.Quantidade.Value > Math.Abs(posicaoBolsa.Quantidade.Value))
                {
                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.CalculaGerencial);
                    cliente.LoadByPrimaryKey(campos, operacaoBolsa.IdCliente.Value);
                    if (cliente.CalculaGerencial == "N")
                    {
                        throw new QuantidadeCarteiraInsuficienteException("Quantidade exercida não corresponde à quantidade em posição da opção " + CdAtivoBolsaOpcao);
                    }
                }

                if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Compra || operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade)
                {
                    if (tipoMercado == TipoMercadoBolsa.OpcaoCompra && posicaoBolsa.Quantidade.Value < 0)
                    {
                        Cliente cliente = new Cliente();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(cliente.Query.CalculaGerencial);
                        cliente.LoadByPrimaryKey(campos, operacaoBolsa.IdCliente.Value);
                        if (cliente.CalculaGerencial == "N")
                        {
                            throw new QuantidadeCarteiraInsuficienteException("Quantidade exercida na Compra não corresponde à quantidade negativa da posição da opção " + CdAtivoBolsaOpcao);
                        }
                    }
                    else
                    {
                        posicaoBolsa.Quantidade -= operacaoBolsa.Quantidade.Value;
                    }
                }
                else if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Venda || operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.VendaDaytrade)
                {
                    if (tipoMercado == TipoMercadoBolsa.OpcaoVenda && posicaoBolsa.Quantidade.Value < 0)
                    {
                        Cliente cliente = new Cliente();
                        List<esQueryItem> campos = new List<esQueryItem>();
                        campos.Add(cliente.Query.CalculaGerencial);
                        cliente.LoadByPrimaryKey(campos, operacaoBolsa.IdCliente.Value);
                        if (cliente.CalculaGerencial == "N")
                        {
                            throw new QuantidadeCarteiraInsuficienteException("Quantidade exercida na Venda não corresponde à quantidade negativa da posição da opção " + CdAtivoBolsaOpcao);
                        }
                    }
                    else
                    {
                        posicaoBolsa.Quantidade += operacaoBolsa.Quantidade.Value;
                    }
                }

                posicaoBolsa.Save();
            }
            else
            {
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.CalculaGerencial);
                cliente.LoadByPrimaryKey(campos, operacaoBolsa.IdCliente.Value);
                if (cliente.CalculaGerencial == "N")
                {
                    throw new QuantidadeCarteiraInsuficienteException("Quantidade exercida não corresponde à quantidade em posição da opção " + CdAtivoBolsaOpcao);
                }
            }
        }

        /// <summary>
        /// OBS: Não Salva a Operação. Apenas calcula os novos valores de Custo da Opção.
        /// </summary>
        /// <param name="operacaoBolsa"></param>
        /// throws ArgumentNullException se 
        /// OperacaoBolsa.Data, OperacaoBolsa.IdAgenteCorretora,
        /// OperacaoBolsa.idCliente, OperacaoBolsa.puLiquido, operacaoBolsa.TipoOperacao
        /// OperacaoBolsa.idlocal
        /// estiver nulo
        private void CompoeCustoOpcao(OperacaoBolsa operacaoBolsa)
        {

            #region ArgumentosNulos - throw ArgumentNullException
            if (!operacaoBolsa.Data.HasValue ||
                !operacaoBolsa.IdAgenteCorretora.HasValue ||
                !operacaoBolsa.IdCliente.HasValue ||
                !operacaoBolsa.PULiquido.HasValue ||
                !operacaoBolsa.IdLocal.HasValue ||
                String.IsNullOrEmpty(operacaoBolsa.CdAtivoBolsaOpcao) ||
                String.IsNullOrEmpty(operacaoBolsa.TipoOperacao))
            {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("OperacaoBolsa.Data não pode ser null ")
                .Append("OperacaoBolsa.IdAgenteCorretora não pode ser null ")
                .Append("OperacaoBolsa.IdCliente não pode ser null ")
                .Append("OperacaoBolsa.PuLiquido não pode ser null ")
                .Append("OperacaoBolsa.idLocal não pode ser null ")
                .Append("OperacaoBolsa.CdAtivoBolsaOpcao não pode ser null ")
                .Append("OperacaoBolsa.TipoOperacao não pode ser null ");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(operacaoBolsa.Data.Value, 1, operacaoBolsa.IdLocal.Value,
                                                              TipoFeriado.Brasil);
            int idCliente = operacaoBolsa.IdCliente.Value;
            int IdAgenteCorretora = operacaoBolsa.IdAgenteCorretora.Value;
            decimal puLiquidoOperacao = operacaoBolsa.PULiquido.Value;
            string tipoOperacao = operacaoBolsa.TipoOperacao;
            string cdAtivoBolsaOpcao = operacaoBolsa.CdAtivoBolsaOpcao;
            //
            bool existe = posicaoBolsaHistorico.BuscaPosicaoBolsaHistorico(idCliente, dataAnterior, cdAtivoBolsaOpcao, IdAgenteCorretora);
            decimal puLiquidoOpcao = existe == true ? posicaoBolsaHistorico.PUCustoLiquido.Value : 0;

            puLiquidoOperacao += puLiquidoOpcao;

            operacaoBolsa.PULiquido = puLiquidoOperacao;
        }

        /// <summary>
        ///    Função para calcular o estoque das posições do cliente
        /// </summary>
        /// <param name="idCliente">Identificador do Cliente</param>
        /// <param name="dataCalculo">Data usada para processamento</param>        
        public void ProcessaCompra(int idCliente, DateTime dataCalculo, bool trataTransferencia)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();

            if (trataTransferencia)
            {
                operacaoBolsaCollection.BuscaOperacaoBolsaTransferenciaDeposito(idCliente, dataCalculo);
            }
            else
            {
                operacaoBolsaCollection.BuscaOperacaoBolsaCompra(idCliente, dataCalculo);
            }

            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                int idAgente = operacaoBolsa.IdAgenteLiquidacao.Value;
                string cdAtivo = operacaoBolsa.CdAtivoBolsa;
                decimal quantidade = operacaoBolsa.Quantidade.Value;
                decimal quantidadeCasadaExercicio = operacaoBolsa.QuantidadeCasadaExercicio.Value;
                decimal quantidadeCasadaTermo = operacaoBolsa.QuantidadeCasadaTermo.Value;
                decimal quantidadeOperacao = quantidade - quantidadeCasadaExercicio - quantidadeCasadaTermo;
                decimal puOperacao = operacaoBolsa.Pu.Value;
                decimal puLiquidoOperacao = operacaoBolsa.PULiquido.Value;

                operacaoBolsa.Quantidade = quantidadeOperacao;

                if (quantidadeOperacao != 0)
                {
                    // Tem Posição para o ativo - Atualiza a posição
                    if (posicaoBolsa.BuscaPosicaoBolsa(idCliente, idAgente, cdAtivo))
                    {
                        decimal quantidadePosicao = posicaoBolsa.Quantidade.Value;
                        // Comprado
                        if (quantidadePosicao >= 0)
                        {
                            // Ja possui posicao para o ativo - Atualizar quantidade e preco medio                  
                            posicaoBolsa.AtualizaPosicaoCompraComprado(posicaoBolsa.IdPosicao.Value, quantidadeOperacao,
                                                                       puOperacao, puLiquidoOperacao);
                        }
                        // Vendido
                        else
                        {
                            // Se quantidade comprada for suficiente para passar de uma posicao negativa 
                            // para positivo
                            if (quantidadeOperacao > Math.Abs(quantidadePosicao))
                            {
                                // Baixa posicao - Atualizar quantidade                                                        
                                if (operacaoBolsa.IsOrigemPrimaria() && !operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemTransferenciaCustodia() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemExercicioOpcao() && operacaoBolsa.IsTipoCompra() ||
                                    operacaoBolsa.IsOrigemLiquidacaoTermo() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemAberturaEmprestimo() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemLiquidacaoEmprestimo() && operacaoBolsa.IsTipoDeposito())
                                {
                                    operacaoBolsa.ProcessaResultadoNormal(operacaoBolsa, posicaoBolsa, quantidadeOperacao);
                                }
                                posicaoBolsa.AtualizaPosicaoCompraVendido(posicaoBolsa.IdPosicao.Value,
                                                                        quantidadeOperacao, puOperacao, puLiquidoOperacao);
                            }
                            else
                            {
                                // Baixa posicao - Atualizar apenas a quantidade
                                if (operacaoBolsa.IsOrigemPrimaria() && !operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemTransferenciaCustodia() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemExercicioOpcao() && operacaoBolsa.IsTipoCompra() ||
                                    operacaoBolsa.IsOrigemLiquidacaoTermo() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemAberturaEmprestimo() && operacaoBolsa.IsTipoDeposito() ||
                                    operacaoBolsa.IsOrigemLiquidacaoEmprestimo() && operacaoBolsa.IsTipoDeposito())
                                {
                                    operacaoBolsa.ProcessaResultadoNormal(operacaoBolsa, posicaoBolsa, quantidadeOperacao);
                                }
                                posicaoBolsa.AtualizaQuantidade(posicaoBolsa.IdPosicao.Value, quantidadeOperacao);
                            }
                        }
                    }
                    // Não tem posição para o Ativo - Inclusão da nova Posição
                    else
                    {
                        posicaoBolsa.InserePosicaoBolsa(operacaoBolsa);
                    }
                }
            }
        }

        /// <summary>
        ///  Atualiza resultado realizado da operacao
        /// </summary>
        /// <param name="operacaoBolsa">varios campos devem estar preenchidos</param>
        /// <param name="posicaoBolsa"></param>
        /// throws FatorCotacaoNaoCadastradoException quando o fator é invalido    
        /// throws ArgumentNullException quando os parametros de entrada necessarios estão nulos
        public void ProcessaResultadoNormal(OperacaoBolsa operacaoBolsa, PosicaoBolsa posicaoBolsa,
                                            decimal quantidadeOperacao)
        {

            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(operacaoBolsa.CdAtivoBolsa) ||
                 !operacaoBolsa.IdOperacao.HasValue ||
                 !operacaoBolsa.IdCliente.HasValue ||
                 !operacaoBolsa.Data.HasValue ||
                 !operacaoBolsa.Quantidade.HasValue ||
                 String.IsNullOrEmpty(operacaoBolsa.TipoOperacao) ||
                 !operacaoBolsa.PULiquido.HasValue ||
                 !operacaoBolsa.Pu.HasValue ||
                 !posicaoBolsa.Quantidade.HasValue ||
                 !posicaoBolsa.PUCustoLiquido.HasValue)
            {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBolsa.CdAtivoBolsa ou ")
                .Append("operacaoBolsa.IdOperacao ou ")
                .Append("operacaoBolsa.IdCliente ou ")
                .Append("operacaoBolsa.Data ou ")
                .Append("operacaoBolsa.Quantidade ou ")
                .Append("operacaoBolsa.PULiquido ou ")
                .Append("operacaoBolsa.PU ou ")
                .Append("posicaoBolsa.Quantidade ou ")
                .Append("posicaoBolsa.PUCustoLiquido");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
            if (!fator.BuscaFatorCotacaoBolsa(operacaoBolsa.CdAtivoBolsa, operacaoBolsa.Data.Value))
            {
                if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                {
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    if (!ativoBolsa.IntegraFatorCotacaoSinacor(operacaoBolsa.CdAtivoBolsa))
                    {
                        throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + operacaoBolsa.CdAtivoBolsa + " " + operacaoBolsa.Data.Value.ToString("d"));
                    }
                }
            }

            decimal quantidadeBase;
            decimal resultado = 0;

            if (Math.Abs(posicaoBolsa.Quantidade.Value) < quantidadeOperacao)
            {
                quantidadeBase = Math.Abs(posicaoBolsa.Quantidade.Value);
            }
            else
            {
                quantidadeBase = quantidadeOperacao;
            }

            // Verifica se está vendido
            switch (operacaoBolsa.TipoOperacao)
            {
                case TipoOperacaoBolsa.Compra:
                case TipoOperacaoBolsa.Deposito:
                    resultado = Math.Round(quantidadeBase *
                        (posicaoBolsa.PUCustoLiquido.Value - operacaoBolsa.PULiquido.Value) / fator.Fator.Value, 2);
                    break;
                case TipoOperacaoBolsa.Venda:
                case TipoOperacaoBolsa.Retirada:
                    resultado = Math.Round(quantidadeBase *
                            (operacaoBolsa.PULiquido.Value - posicaoBolsa.PUCustoLiquido.Value) / fator.Fator.Value, 2);
                    break;
            }

            OperacaoBolsa operacaoBolsaAux = new OperacaoBolsa();
            // Atualiza resultado realizado da Operação                                         
            operacaoBolsaAux.IdOperacao = operacaoBolsa.IdOperacao;
            operacaoBolsaAux.AcceptChanges();
            operacaoBolsaAux.ResultadoRealizado = resultado;
            operacaoBolsaAux.Save();

        }

        /// <summary>
        /// Zera todos os resultados (Exercicio, Termo, Normal) na data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ZeraResultados(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.IdOperacao,
                                                 operacaoBolsaCollection.Query.ResultadoExercicio,
                                                 operacaoBolsaCollection.Query.ResultadoRealizado,
                                                 operacaoBolsaCollection.Query.ResultadoTermo);
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data));
            operacaoBolsaCollection.Query.Load();
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                operacaoBolsa.ResultadoExercicio = 0;
                operacaoBolsa.ResultadoRealizado = 0;
                operacaoBolsa.ResultadoTermo = 0;
            }
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Zera todos os valores de taxas e corretagem para operacoes do tipo CalculaDespesas = N na data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ZeraValoresCustos(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.IdOperacao,
                                                 operacaoBolsaCollection.Query.Corretagem,
                                                 operacaoBolsaCollection.Query.Emolumento,
                                                 operacaoBolsaCollection.Query.RegistroBolsa,
                                                 operacaoBolsaCollection.Query.RegistroCBLC,
                                                 operacaoBolsaCollection.Query.LiquidacaoCBLC);
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.CalculaDespesas.Equal("S"),
                                                operacaoBolsaCollection.Query.Fonte.NotIn((byte)FonteOperacaoBolsa.Sinacor, (byte)FonteOperacaoBolsa.Gerencial));
            operacaoBolsaCollection.Query.Load();
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                operacaoBolsa.Corretagem = 0;
                operacaoBolsa.Emolumento = 0;
                operacaoBolsa.RegistroBolsa = 0;
                operacaoBolsa.RegistroCBLC = 0;
                operacaoBolsa.LiquidacaoCBLC = 0;
            }
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// metodo de Insercao na OperacaoBolsa
        /// </summary>
        /// <param name="operacaoBolsa">operacao com os dados a inserir</param>
        /// throws ArgumentNullException se campos Obrigatórios da inclusão estiverem nulos
        /// operacaoBolsa.TipoMercado ou operacaoBolsa.Data ou operacaoBolsa.Pu ou
        /// operacaoBolsa.PuLiquido ou operacaoBolsa.Valor ou operacaoBolsa.Quantidade ou 
        /// operacaoBolsa.DataLiquidacao ou operacaoBolsa.Origem ou 
        /// operacaoBolsa.PercentualDesconto ou operacaoBolsa.idLocal ou
        /// operacaoBolsa.idCliente ou operacaoBolsa.cdAtivoBolsa ou 
        /// operacaoBolsa.IdAgenteLiquidacao ou operacaoBolsa.IdAgenteCorretora 
        /// operacaoBolsa.Fonte forem nulos ou vazio
        /// 
        public void InsereOperacaoBolsa(OperacaoBolsa operacaoBolsa)
        {
            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(operacaoBolsa.TipoOperacao) ||
               String.IsNullOrEmpty(operacaoBolsa.TipoMercado) ||
               !operacaoBolsa.Data.HasValue ||
               !operacaoBolsa.Pu.HasValue ||
               !operacaoBolsa.PULiquido.HasValue ||
               !operacaoBolsa.Valor.HasValue ||
               !operacaoBolsa.Quantidade.HasValue ||
               !operacaoBolsa.DataLiquidacao.HasValue ||
               !operacaoBolsa.Origem.HasValue ||
               !operacaoBolsa.PercentualDesconto.HasValue ||
               !operacaoBolsa.IdLocal.HasValue ||
               !operacaoBolsa.IdCliente.HasValue ||
               String.IsNullOrEmpty(operacaoBolsa.CdAtivoBolsa) ||
               !operacaoBolsa.IdAgenteCorretora.HasValue ||
               !operacaoBolsa.IdAgenteLiquidacao.HasValue ||
               !operacaoBolsa.IdMoeda.HasValue ||
               !operacaoBolsa.Fonte.HasValue)
            {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBolsa.TipoOperacao ou ")
                .Append("operacaoBolsa.TipoMercado ou ")
                .Append("operacaoBolsa.Data ou ")
                .Append("operacaoBolsa.Pu ou ")
                .Append("operacaoBolsa.PULiquido ou ")
                .Append("operacaoBolsa.Valor ou ")
                .Append("operacaoBolsa.Quantidade ou ")
                .Append("operacaoBolsa.DataLiquidacao ou ")
                .Append("operacaoBolsa.Origem ou ")
                .Append("operacaoBolsa.PercentualDesconto ou ")
                .Append("operacaoBolsa.idLocal ou ")
                .Append("operacaoBolsa.idCliente ou ")
                .Append("operacaoBolsa.cdAtivoBolsa ou ")
                .Append("operacaoBolsa.IdAgenteCorretora ou ")
                .Append("operacaoBolsa.IdAgenteLiquidacao ou ")
                .Append("operacaoBolsa.IdMoeda ou ")
                .Append("operacaoBolsa.Fonte");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            OperacaoBolsa operacaoBolsaInserir = new OperacaoBolsa();
            //            
            // Campos Obrigatórios para a inserção
            operacaoBolsaInserir.TipoOperacao = operacaoBolsa.TipoOperacao;
            operacaoBolsaInserir.TipoMercado = operacaoBolsa.TipoMercado;
            operacaoBolsaInserir.Data = operacaoBolsa.Data;
            operacaoBolsaInserir.Pu = operacaoBolsa.Pu;
            operacaoBolsaInserir.PULiquido = operacaoBolsa.PULiquido;
            operacaoBolsaInserir.Valor = operacaoBolsa.Valor;
            operacaoBolsaInserir.Quantidade = operacaoBolsa.Quantidade;
            operacaoBolsaInserir.DataLiquidacao = operacaoBolsa.DataLiquidacao;
            operacaoBolsaInserir.Origem = operacaoBolsa.Origem;
            operacaoBolsaInserir.PercentualDesconto = operacaoBolsa.PercentualDesconto;
            operacaoBolsaInserir.IdLocal = operacaoBolsa.IdLocal;
            operacaoBolsaInserir.IdCliente = operacaoBolsa.IdCliente;
            operacaoBolsaInserir.CdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
            operacaoBolsaInserir.IdAgenteLiquidacao = operacaoBolsa.IdAgenteLiquidacao;
            operacaoBolsaInserir.IdAgenteCorretora = operacaoBolsa.IdAgenteCorretora;
            operacaoBolsaInserir.Fonte = operacaoBolsa.Fonte;
            operacaoBolsaInserir.CdAtivoBolsaOpcao = operacaoBolsa.CdAtivoBolsaOpcao;
            operacaoBolsaInserir.CalculaDespesas = operacaoBolsa.CalculaDespesas;
            operacaoBolsaInserir.IdMoeda = operacaoBolsa.IdMoeda;
            operacaoBolsaInserir.DataOperacao = operacaoBolsa.DataOperacao;
            operacaoBolsaInserir.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
            operacaoBolsaInserir.IdLocalNegociacao = LocalNegociacaoFixo.BOVESPA;
            operacaoBolsaInserir.IdClearing = (int)ClearingFixo.CBLC;

            // Campos não obrigatórios - se não for passado coloca valor default            
            operacaoBolsaInserir.ValorLiquido = operacaoBolsa.ValorLiquido.HasValue ? operacaoBolsa.ValorLiquido : 0;
            operacaoBolsaInserir.Corretagem = operacaoBolsa.Corretagem.HasValue ? operacaoBolsa.Corretagem : 0;
            operacaoBolsaInserir.Emolumento = operacaoBolsa.Emolumento.HasValue ? operacaoBolsa.Emolumento : 0;
            operacaoBolsaInserir.RegistroBolsa = operacaoBolsa.RegistroBolsa.HasValue ? operacaoBolsa.RegistroBolsa : 0;
            operacaoBolsaInserir.RegistroCBLC = operacaoBolsa.RegistroCBLC.HasValue ? operacaoBolsa.RegistroCBLC : 0;
            operacaoBolsaInserir.ResultadoRealizado = operacaoBolsa.ResultadoRealizado.HasValue ? operacaoBolsa.ResultadoRealizado : 0;
            operacaoBolsaInserir.Desconto = operacaoBolsa.Desconto.HasValue ? operacaoBolsa.Desconto : 0;
            operacaoBolsaInserir.QuantidadeCasadaExercicio = operacaoBolsa.QuantidadeCasadaExercicio.HasValue ? operacaoBolsa.QuantidadeCasadaExercicio : 0;
            operacaoBolsaInserir.QuantidadeCasadaTermo = operacaoBolsa.QuantidadeCasadaTermo.HasValue ? operacaoBolsa.QuantidadeCasadaTermo : 0;
            //                         
            operacaoBolsaInserir.Save();
        }

        /// <summary>
        ///    Função para calcular o estoque das posições do cliente
        /// </summary>
        /// <param name="idCliente">Identificador do Cliente</param>
        /// <param name="data">Data usada para processamento</param>        
        public void ProcessaVenda(int idCliente, DateTime data, bool trataTransferencia)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();

            if (trataTransferencia)
            {
                operacaoBolsaCollection.BuscaOperacaoBolsaTransferenciaRetirada(idCliente, data);
            }
            else
            {
                operacaoBolsaCollection.BuscaOperacaoBolsaVenda(idCliente, data);
            }

            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                decimal puOperacao = operacaoBolsa.Pu.Value;
                decimal puLiquidoOperacao = operacaoBolsa.PULiquido.Value;
                decimal quantidade = operacaoBolsa.Quantidade.Value;
                decimal quantidadeCasadaExercicio = operacaoBolsa.QuantidadeCasadaExercicio.Value;
                decimal quantidadeCasadaTermo = operacaoBolsa.QuantidadeCasadaTermo.Value;
                decimal quantidadeOperacao = quantidade - quantidadeCasadaExercicio - quantidadeCasadaTermo;
                string cdAtivo = operacaoBolsa.CdAtivoBolsa;
                int idAgente = operacaoBolsa.IdAgenteLiquidacao.Value;

                operacaoBolsa.Quantidade = quantidadeOperacao;

                if (quantidadeOperacao != 0)
                {
                    // Tem Posição para o ativo - Atualiza a posição
                    if (posicaoBolsa.BuscaPosicaoBolsa(idCliente, idAgente, cdAtivo))
                    {
                        decimal quantidadePosicao = posicaoBolsa.Quantidade.Value;
                        // Vendido
                        if (quantidadePosicao <= 0)
                        {
                            // Ja possui posicao para o ativo - Atualizar quantidade e preco medio                  
                            posicaoBolsa.AtualizaPosicaoVendaVendido(posicaoBolsa.IdPosicao.Value, quantidadeOperacao,
                                                                       puOperacao, puLiquidoOperacao);
                        }
                        // Comprado
                        else
                        {
                            // Se quantidade comprada for suficiente para passar de uma posicao positiva 
                            // para negativa
                            decimal quantidadeOperacaoNegativa = -1 * quantidadeOperacao;
                            if (quantidadeOperacao > quantidadePosicao)
                            {
                                if (operacaoBolsa.IsOrigemPrimaria() && !operacaoBolsa.IsTipoRetirada() ||
                                    operacaoBolsa.IsOrigemTransferenciaCustodia() && operacaoBolsa.IsTipoRetirada() ||
                                    operacaoBolsa.IsOrigemExercicioOpcao() && operacaoBolsa.IsTipoVenda() ||
                                    operacaoBolsa.IsOrigemLiquidacaoTermo() && operacaoBolsa.IsTipoRetirada())
                                {
                                    operacaoBolsa.ProcessaResultadoNormal(operacaoBolsa, posicaoBolsa, quantidadeOperacao);
                                }
                                // Baixa posicao - Atualizar quantidade                                                        
                                posicaoBolsa.AtualizaPosicaoVendaComprado(posicaoBolsa.IdPosicao.Value,
                                                                        quantidadeOperacao, puOperacao, puLiquidoOperacao);
                            }
                            else
                            {
                                if (operacaoBolsa.IsOrigemPrimaria() && !operacaoBolsa.IsTipoRetirada() ||
                                    operacaoBolsa.IsOrigemTransferenciaCustodia() && operacaoBolsa.IsTipoRetirada() ||
                                    operacaoBolsa.IsOrigemExercicioOpcao() && operacaoBolsa.IsTipoVenda() ||
                                    operacaoBolsa.IsOrigemLiquidacaoTermo() && operacaoBolsa.IsTipoRetirada())
                                {
                                    operacaoBolsa.ProcessaResultadoNormal(operacaoBolsa, posicaoBolsa, quantidadeOperacao);
                                }
                                // Baixa posicao - Atualizar apenas a quantidade                            
                                posicaoBolsa.AtualizaQuantidade(posicaoBolsa.IdPosicao.Value, quantidadeOperacaoNegativa);
                            }
                        }
                    }
                    // Não tem posição para o Ativo - Inclusão da nova Posição
                    else
                    {
                        // Quantidade da operação é positiva, mas quantidade da  posição fica negativa
                        operacaoBolsa.Quantidade = -1 * operacaoBolsa.Quantidade;
                        posicaoBolsa.InserePosicaoBolsa(operacaoBolsa);
                    }
                }
            }
        }

        /// <summary>
        /// Calcula o resultado quando uma das pontas casadas é exercicio
        /// </summary>
        private void ProcessaResultadoVencimentoExercicio(OperacaoBolsa operacaoBolsaCompra,
                                OperacaoBolsa operacaoBolsaVenda, decimal quantidadeCasadaExercicio)
        {

            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(operacaoBolsaCompra.CdAtivoBolsa) ||
                 !operacaoBolsaCompra.Data.HasValue ||
                 !operacaoBolsaCompra.Origem.HasValue ||
                 !operacaoBolsaVenda.Origem.HasValue ||
                 !operacaoBolsaCompra.QuantidadeCasadaExercicio.HasValue ||
                 !operacaoBolsaVenda.QuantidadeCasadaExercicio.HasValue ||
                 !operacaoBolsaCompra.PULiquido.HasValue ||
                 !operacaoBolsaVenda.PULiquido.HasValue ||
                 !operacaoBolsaCompra.QuantidadeCasadaTermo.HasValue ||
                 !operacaoBolsaVenda.QuantidadeCasadaTermo.HasValue)
            {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBolsaCompra.CdAtivoBolsa ou ")
                .Append("operacaoBolsaCompra.Data ou ")
                .Append("operacaoBolsaCompra.Origem ou ")
                .Append("operacaoBolsaVenda.Origem ou ")
                .Append("operacaoBolsaCompra.QuantidadeCasadaExercicio ou ")
                .Append("operacaoBolsaVenda.QuantidadeCasadaExercicio ou ")
                .Append("operacaoBolsaCompra.PULiquido ou ")
                .Append("operacaoBolsaVenda.PULiquido ou ")
                .Append("operacaoBolsaCompra.quantidadeCasadaTermo ou ")
                .Append("operacaoBolsaVenda.quantidadeCasadaTermo");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
            #region fatorInvalido - throw FatorCotacaoNaoCadastradoException
            if (!fator.BuscaFatorCotacaoBolsa(operacaoBolsaCompra.CdAtivoBolsa, operacaoBolsaCompra.Data.Value))
            {
                if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                {
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    if (!ativoBolsa.IntegraFatorCotacaoSinacor(operacaoBolsaCompra.CdAtivoBolsa))
                    {
                        throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + operacaoBolsaCompra.CdAtivoBolsa + " " + operacaoBolsaCompra.Data.Value.ToString("d"));
                    }
                }
            }
            #endregion

            if (operacaoBolsaCompra.Origem == OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                operacaoBolsaCompra.Origem == OrigemOperacaoBolsa.ExercicioOpcaoVenda)
            {
                operacaoBolsaCompra.ResultadoExercicio += Math.Round(quantidadeCasadaExercicio *
                    (operacaoBolsaVenda.PULiquido.Value - operacaoBolsaCompra.PULiquido.Value)
                    / fator.Fator.Value, 2);
            }
            else if (operacaoBolsaVenda.Origem == OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                     operacaoBolsaVenda.Origem == OrigemOperacaoBolsa.ExercicioOpcaoVenda)
            {
                operacaoBolsaVenda.ResultadoExercicio += Math.Round(quantidadeCasadaExercicio *
                    (operacaoBolsaVenda.PULiquido.Value - operacaoBolsaCompra.PULiquido.Value)
                    / fator.Fator.Value, 2);
            }
        }

        /// <summary>
        /// Calcula o resultado quando uma das pontas casadas é liquidacao de termo
        /// </summary>
        private void ProcessaResultadoVencimentoTermo(OperacaoBolsa operacaoBolsaCompra,
                                            OperacaoBolsa operacaoBolsaVenda, decimal quantidadeCasadaTermo)
        {
            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(operacaoBolsaCompra.CdAtivoBolsa) ||
                 !operacaoBolsaCompra.Data.HasValue ||
                 !operacaoBolsaCompra.Origem.HasValue ||
                 !operacaoBolsaVenda.Origem.HasValue ||
                 !operacaoBolsaCompra.QuantidadeCasadaExercicio.HasValue ||
                 !operacaoBolsaVenda.QuantidadeCasadaExercicio.HasValue ||
                 !operacaoBolsaCompra.PULiquido.HasValue ||
                 !operacaoBolsaVenda.PULiquido.HasValue ||
                 !operacaoBolsaCompra.QuantidadeCasadaTermo.HasValue ||
                 !operacaoBolsaVenda.QuantidadeCasadaTermo.HasValue)
            {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("operacaoBolsaCompra.CdAtivoBolsa ou ")
                .Append("operacaoBolsaCompra.Data ou ")
                .Append("operacaoBolsaCompra.Origem ou ")
                .Append("operacaoBolsaVenda.Origem ou ")
                .Append("operacaoBolsaCompra.QuantidadeCasadaExercicio ou ")
                .Append("operacaoBolsaVenda.QuantidadeCasadaExercicio ou ")
                .Append("operacaoBolsaCompra.PULiquido ou ")
                .Append("operacaoBolsaVenda.PULiquido ou ")
                .Append("operacaoBolsaCompra.quantidadeCasadaTermo ou ")
                .Append("operacaoBolsaVenda.quantidadeCasadaTermo");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
            #region fatorInvalido - throw FatorCotacaoNaoCadastradoException
            if (!fator.BuscaFatorCotacaoBolsa(operacaoBolsaCompra.CdAtivoBolsa, operacaoBolsaCompra.Data.Value))
            {
                if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                {
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    if (!ativoBolsa.IntegraFatorCotacaoSinacor(operacaoBolsaCompra.CdAtivoBolsa))
                    {
                        throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + operacaoBolsaCompra.CdAtivoBolsa + " " + operacaoBolsaCompra.Data.Value.ToString("d"));
                    }
                }
            }
            #endregion

            if (operacaoBolsaCompra.Origem == OrigemOperacaoBolsa.LiquidacaoTermo)
            {
                operacaoBolsaCompra.ResultadoTermo += Math.Round(quantidadeCasadaTermo *
                    (operacaoBolsaVenda.PULiquido.Value - operacaoBolsaCompra.PULiquido.Value)
                    / fator.Fator.Value, 2);
            }
            else if (operacaoBolsaVenda.Origem == OrigemOperacaoBolsa.LiquidacaoTermo)
            {
                operacaoBolsaVenda.ResultadoTermo += Math.Round(quantidadeCasadaTermo *
                    (operacaoBolsaVenda.PULiquido.Value - operacaoBolsaCompra.PULiquido.Value)
                    / fator.Fator.Value, 2);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="prioridadeCasamentoVencimento"></param>
        /// throw ArgumentException se tipoCasamentoVencimento não estiver dentro dos valores possiveis do enum TipoCasamentoVencimento
        public void CasaVencimento(int idCliente, DateTime data, PrioridadeCasamentoVencimento prioridadeCasamentoVencimento)
        {
            //Zera as quantidades casadas e resultados de termo e exercício
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.ZeraValoresCasamamentoTermoExecicio(idCliente, data);
            //

            if (prioridadeCasamentoVencimento == PrioridadeCasamentoVencimento.ExercicioPrimeiro)
            {
                this.CasaVencimentoExercicio(idCliente, data);
                this.CasaVencimentoTermo(idCliente, data);
            }
            if (prioridadeCasamentoVencimento == PrioridadeCasamentoVencimento.TermoPrimeiro)
            {
                this.CasaVencimentoTermo(idCliente, data);
                this.CasaVencimentoExercicio(idCliente, data);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void CasaVencimentoTermo(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollectionVenda = new OperacaoBolsaCollection();
            OperacaoBolsaCollection operacaoBolsaCollectionCompra = new OperacaoBolsaCollection();

            // Collection de Operacoes de Venda
            operacaoBolsaCollectionVenda.BuscaOperacaoBolsaVendaCasaVencimentoTermo(idCliente, data);
            for (int i = 0; i < operacaoBolsaCollectionVenda.Count; i++)
            {
                OperacaoBolsa operacaoBolsaVenda = operacaoBolsaCollectionVenda[i];
                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.Value - operacaoBolsaVenda.QuantidadeCasadaExercicio.Value;
                decimal quantidadeVendaCasada = operacaoBolsaVenda.QuantidadeCasadaTermo.Value;
                int idOperacaoVenda = operacaoBolsaVenda.IdOperacao.Value;
                int idAgenteCorretora = operacaoBolsaVenda.IdAgenteCorretora.Value;
                string cdAtivoBolsa = operacaoBolsaVenda.CdAtivoBolsa;
                decimal quantidadeDiferenca;

                // Collection de Operacoes de Compra
                if (!operacaoBolsaCollectionCompra.BuscaOperacaoBolsaCompraCasaVencimentoTermo(idCliente, data, idAgenteCorretora, cdAtivoBolsa))
                {
                    break;
                }

                for (int j = 0; j < operacaoBolsaCollectionCompra.Count; j++)
                {
                    OperacaoBolsa operacaoBolsaCompra = operacaoBolsaCollectionCompra[j];

                    // Uma das pontas (operacaoCompra ou operacaoVenda) deve ser LiquidacaoTermo para poder casar
                    if (!(operacaoBolsaCompra.Origem == OrigemOperacaoBolsa.LiquidacaoTermo ||
                           operacaoBolsaVenda.Origem == OrigemOperacaoBolsa.LiquidacaoTermo))
                    {
                        continue;
                    }

                    decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.Value - operacaoBolsaCompra.QuantidadeCasadaExercicio.Value;
                    decimal quantidadeCompraCasada = operacaoBolsaCompra.QuantidadeCasadaTermo.Value;
                    int idOperacaoCompra = operacaoBolsaCompra.IdOperacao.Value;

                    if (quantidadeCompra == quantidadeCompraCasada) continue;
                    if (quantidadeVenda == quantidadeVendaCasada) break;

                    decimal deltaQuantidadeVenda = quantidadeVenda - quantidadeVendaCasada;
                    decimal deltaQuantidadeCompra = quantidadeCompra - quantidadeCompraCasada;

                    if (deltaQuantidadeVenda < deltaQuantidadeCompra)
                    {
                        quantidadeDiferenca = deltaQuantidadeVenda;
                    }
                    else
                    {
                        quantidadeDiferenca = deltaQuantidadeCompra;
                    }

                    if (quantidadeDiferenca != 0)
                    {
                        operacaoBolsaCompra.QuantidadeCasadaTermo = quantidadeDiferenca + quantidadeCompraCasada;
                        operacaoBolsaVenda.QuantidadeCasadaTermo = quantidadeDiferenca + quantidadeVendaCasada;
                        quantidadeVendaCasada += quantidadeDiferenca;
                    }

                    this.ProcessaResultadoVencimentoTermo(operacaoBolsaCompra, operacaoBolsaVenda, quantidadeDiferenca);
                }

                operacaoBolsaCollectionCompra.Save();
            }

            operacaoBolsaCollectionVenda.Save();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void CasaVencimentoExercicio(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollectionVenda = new OperacaoBolsaCollection();
            OperacaoBolsaCollection operacaoBolsaCollectionCompra = new OperacaoBolsaCollection();

            // Collection de Operacoes de Venda
            operacaoBolsaCollectionVenda.BuscaOperacaoBolsaVendaCasaVencimentoExercicio(idCliente, data);
            for (int i = 0; i < operacaoBolsaCollectionVenda.Count; i++)
            {
                OperacaoBolsa operacaoBolsaVenda = operacaoBolsaCollectionVenda[i];
                decimal quantidadeVenda = operacaoBolsaVenda.Quantidade.Value - operacaoBolsaVenda.QuantidadeCasadaTermo.Value;
                decimal quantidadeVendaCasada = operacaoBolsaVenda.QuantidadeCasadaExercicio.Value;
                int idOperacaoVenda = operacaoBolsaVenda.IdOperacao.Value;
                int idAgenteCorretora = operacaoBolsaVenda.IdAgenteCorretora.Value;
                string cdAtivoBolsa = operacaoBolsaVenda.CdAtivoBolsa;
                decimal quantidadeDiferenca;

                // Collection de Operacoes de Compra
                if (!operacaoBolsaCollectionCompra.BuscaOperacaoBolsaCompraCasaVencimentoExercicio(idCliente, data, idAgenteCorretora, cdAtivoBolsa))
                {
                    break;
                }

                for (int j = 0; j < operacaoBolsaCollectionCompra.Count; j++)
                {
                    OperacaoBolsa operacaoBolsaCompra = operacaoBolsaCollectionCompra[j];
                    // Uma das pontas (operacaoCompra ou operacaoVenda) deve ser ExercicioOpcao para poder casar
                    if (!(operacaoBolsaCompra.IsOrigemExercicioOpcao() ||
                           operacaoBolsaVenda.IsOrigemExercicioOpcao()))
                    {
                        continue;
                    }

                    decimal quantidadeCompra = operacaoBolsaCompra.Quantidade.Value - operacaoBolsaCompra.QuantidadeCasadaTermo.Value;
                    decimal quantidadeCompraCasada = operacaoBolsaCompra.QuantidadeCasadaExercicio.Value;
                    int idOperacaoCompra = operacaoBolsaCompra.IdOperacao.Value;

                    if (quantidadeCompra == quantidadeCompraCasada) continue;
                    if (quantidadeVenda == quantidadeVendaCasada) break;

                    decimal deltaQuantidadeVenda = quantidadeVenda - quantidadeVendaCasada;
                    decimal deltaQuantidadeCompra = quantidadeCompra - quantidadeCompraCasada;

                    if (deltaQuantidadeVenda < deltaQuantidadeCompra)
                    {
                        quantidadeDiferenca = deltaQuantidadeVenda;
                    }
                    else
                    {
                        quantidadeDiferenca = deltaQuantidadeCompra;
                    }

                    if (quantidadeDiferenca != 0)
                    {
                        operacaoBolsaCompra.QuantidadeCasadaExercicio = quantidadeDiferenca + quantidadeCompraCasada;
                        operacaoBolsaVenda.QuantidadeCasadaExercicio = quantidadeDiferenca + quantidadeVendaCasada;
                        quantidadeVendaCasada += quantidadeDiferenca;
                    }

                    this.ProcessaResultadoVencimentoExercicio(operacaoBolsaCompra, operacaoBolsaVenda,
                                                               quantidadeDiferenca);
                }

                operacaoBolsaCollectionCompra.Save();
            }

            operacaoBolsaCollectionVenda.Save();
        }

        /// <summary>
        /// Rateia totais de emolumento, registro e taxa de liquidacao (no conjunto das operacoes passadas)
        /// proporcionalmente ao valor individual de cada operacao
        /// </summary>
        /// <param name="operacaoBolsaCollection"></param>
        /// <param name="totalEmolumento"></param>
        /// <param name="totalRegistroBolsa"></param>
        /// <param name="totalLiquidacaoCBLC"></param>
        /// <param name="totalRegistroCBLC"></param>
        /// <param name="totalValor"></param>
        public void RateiaTaxas(OperacaoBolsaCollection operacaoBolsaCollection, decimal totalEmolumento,
                                    decimal totalRegistroBolsa, decimal totalLiquidacaoCBLC,
                                    decimal totalRegistroCBLC, decimal totalValor)
        {
            this.RateiaTaxas(operacaoBolsaCollection, totalEmolumento, totalRegistroBolsa, totalLiquidacaoCBLC, totalRegistroCBLC, totalValor, false);
        }

        /// <summary>
        /// Rateia totais de emolumento, registro e taxa de liquidacao (no conjunto das operacoes passadas)
        /// proporcionalmente ao valor individual de cada operacao
        /// </summary>
        /// <param name="operacaoBolsaCollection"></param>
        /// <param name="totalEmolumento"></param>
        /// <param name="totalRegistroBolsa"></param>
        /// <param name="totalLiquidacaoCBLC"></param>
        /// <param name="totalRegistroCBLC"></param>
        /// <param name="totalValor"></param>
        public void RateiaTaxas(OperacaoBolsaCollection operacaoBolsaCollection, decimal totalEmolumento,
                                    decimal totalRegistroBolsa, decimal totalLiquidacaoCBLC,
                                    decimal totalRegistroCBLC, decimal totalValor, bool calculaLiquido)
        {
            decimal emolumentoResidual = totalEmolumento;
            decimal registroBolsaResidual = totalRegistroBolsa;
            decimal liquidacaoCBLCResidual = totalLiquidacaoCBLC;
            decimal registroCBLCResidual = totalRegistroCBLC;

            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                decimal valor = operacaoBolsa.Valor.Value;

                // calculo das proporções
                decimal emolumento = 0;
                decimal registroBolsa = 0;
                decimal liquidacaoCBLC = 0;
                decimal registroCBLC = 0;
                if (totalValor != 0)
                {
                    emolumento = Utilitario.Truncate(((totalEmolumento * valor) / totalValor), 2);
                    registroBolsa = Utilitario.Truncate(((totalRegistroBolsa * valor) / totalValor), 2);
                    liquidacaoCBLC = Utilitario.Truncate(((totalLiquidacaoCBLC * valor) / totalValor), 2);
                    registroCBLC = Utilitario.Truncate(((totalRegistroCBLC * valor) / totalValor), 2);
                }

                emolumentoResidual -= emolumento;
                registroBolsaResidual -= registroBolsa;
                liquidacaoCBLCResidual -= liquidacaoCBLC;
                registroCBLCResidual -= registroCBLC;

                // Se é a ultima operacao da corretora, garante-se que 100% da corretagem 
                // foi distribuida, jogando todo o residual para a ultima operacao
                if (i == operacaoBolsaCollection.Count - 1)
                {
                    emolumento += emolumentoResidual;
                    registroBolsa += registroBolsaResidual;
                    liquidacaoCBLC += liquidacaoCBLCResidual;
                    registroCBLC += registroCBLCResidual;
                }

                operacaoBolsa.Emolumento = emolumento;
                operacaoBolsa.RegistroBolsa = registroBolsa;
                operacaoBolsa.LiquidacaoCBLC = liquidacaoCBLC;
                operacaoBolsa.RegistroCBLC = registroCBLC;

                decimal taxas = emolumento + registroBolsa + liquidacaoCBLC + registroCBLC + operacaoBolsa.Corretagem.Value;

                if (calculaLiquido)
                {
                    decimal fatorCotacao = (operacaoBolsa.Quantidade.Value * operacaoBolsa.Pu.Value) / operacaoBolsa.Valor.Value;

                    decimal valorLiquido = 0;
                    if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Compra || operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade)
                    {
                        valorLiquido = operacaoBolsa.Valor.Value + taxas;
                    }
                    else
                    {
                        valorLiquido = operacaoBolsa.Valor.Value - taxas;
                    }

                    decimal puLiquido = (valorLiquido * fatorCotacao) / operacaoBolsa.Quantidade.Value;

                    operacaoBolsa.ValorLiquido = valorLiquido;
                    operacaoBolsa.PULiquido = puLiquido;
                }

            }

            // Salva a collection de operações
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega em OperacaoBolsa a partir de OrdemBolsa e, se for o caso, joga tb em OperacaoTermoBolsa.
        /// Método não transacionado!
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// throws FatorCotacaoNaoCadastradoException se fator não estiver cadastrado
        public void CarregaOrdemCasada(int idCliente, DateTime data, bool integraTudo)
        {
            OperacaoBolsaCollection operacaoBolsaCollectionDeletar = new OperacaoBolsaCollection();
            if (integraTudo)
            {
                operacaoBolsaCollectionDeletar.DeletaOperacaoOrdemCasada(idCliente, data);
            }
            else
            {
                operacaoBolsaCollectionDeletar.DeletaOperacaoOrdemCasadaNaoIntegrada(idCliente, data);
            }

            /* Para salvar a operacaoBolsa no final */
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            //
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();

            if (integraTudo)
            {
                ordemBolsaCollection.BuscaOrdemBolsaCompleta(idCliente, data);
            }
            else
            {
                ordemBolsaCollection.BuscaOrdemBolsaNaoIntegrada(idCliente, data);
            }

            for (int i = 0; i < ordemBolsaCollection.Count; i++)
            {
                OrdemBolsa ordemBolsa = ordemBolsaCollection[i];
                string cdAtivoBolsa = ordemBolsa.CdAtivoBolsa;

                decimal valor = ordemBolsa.Valor.Value;
                decimal quantidade = ordemBolsa.Quantidade.Value;
                decimal pu = ordemBolsa.Pu.Value;
                int? idLocalNegociacao = ordemBolsa.IdLocalNegociacao;
                int? idLocalCustodia = ordemBolsa.IdLocalCustodia;
                int? idClearing = ordemBolsa.IdClearing;

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                if (!fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                {
                    if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                    {
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsa))
                        {
                            throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsa + " " + data.ToString("d"));
                        }
                    }
                    else
                    {
                        throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsa + " " + data.ToString("d"));
                    }
                }
                decimal fator = fatorCotacaoBolsa.Fator.Value;

                #region OrdemBolsa
                int idOrdem = ordemBolsa.IdOrdem.Value;
                string tipoMercado = ordemBolsa.TipoMercado;
                string tipoOrdem = ordemBolsa.TipoOrdem;
                decimal quantidadeDaytrade = ordemBolsa.QuantidadeDayTrade.Value;
                decimal quantidadeNormal = quantidade - quantidadeDaytrade;
                decimal valorNormal = Utilitario.Truncate(((quantidadeNormal * pu) / fator), 2);
                decimal valorDaytrade = Utilitario.Truncate(((quantidadeDaytrade * pu) / fator), 2);
                decimal corretagem = ordemBolsa.Corretagem.Value;
                decimal emolumento = ordemBolsa.Emolumento.Value;
                decimal liquidacaoCBLC = ordemBolsa.LiquidacaoCBLC.Value;
                decimal registroBolsa = ordemBolsa.RegistroBolsa.Value;
                decimal registroCBLC = ordemBolsa.RegistroCBLC.Value;
                decimal percentualDesconto = ordemBolsa.PercentualDesconto.Value;
                decimal desconto = ordemBolsa.Desconto.Value;
                int idAgenteLiquidacao = ordemBolsa.IdAgenteLiquidacao.Value;
                int idAgenteCorretora = ordemBolsa.IdAgenteCorretora.Value;
                int fonteOrdem = ordemBolsa.Fonte.Value;
                DateTime? dataLiquidacao = ordemBolsa.DataLiquidacao;
                int idMoeda = ordemBolsa.IdMoeda.Value;
                decimal valorISS = ordemBolsa.ValorISS.Value;
                #endregion

                //Para ordens com fonte manual, identifica de forma especifica na OperacaoBolsa
                //Para os outros tipos de fonte, vale o mesmo tipo de fonte da OrdemBolsa
                int fonteOperacao = fonteOrdem;
                if (fonteOperacao == (int)FonteOrdemBolsa.Manual)
                {
                    fonteOperacao = (int)FonteOperacaoBolsa.OrdemBolsa;
                }

                if (tipoMercado == TipoMercadoBolsa.Termo)
                {
                    #region Busco os dados de OrdemTermoBolsa e dataVencimento do CdAtivoBolsa
                    OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();
                    if (!ordemTermoBolsa.LoadByPrimaryKey(idOrdem))
                    {
                        throw new OrdemTermoBolsaInexistente("OrdemTermoBolsa não associada a OrdemBolsa id: " + idOrdem);
                    }
                    decimal taxa = ordemTermoBolsa.Taxa.Value;
                    string numeroContrato = ordemTermoBolsa.NumeroContrato;

                    short? idIndice = null;
                    if (ordemTermoBolsa.IdIndice.HasValue)
                    {
                        idIndice = ordemTermoBolsa.IdIndice.Value;
                    }

                    AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(ativoBolsaTermo.Query.DataVencimento);
                    ativoBolsaTermo.LoadByPrimaryKey(campos, cdAtivoBolsa);

                    DateTime dataVencimento = ativoBolsaTermo.DataVencimento.Value;
                    #endregion

                    #region Insere OperacaoBolsa e InsereOperacaoTermoBolsa
                    string tipoOperacao = tipoOrdem;
                    decimal despesas = corretagem + emolumento + liquidacaoCBLC + registroBolsa + registroCBLC;

                    #region InsereOperacao
                    OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                    operacaoBolsa.TipoMercado = tipoMercado;
                    operacaoBolsa.TipoOperacao = tipoOperacao;
                    operacaoBolsa.Data = data;
                    operacaoBolsa.Pu = pu;
                    operacaoBolsa.Valor = valorNormal;
                    operacaoBolsa.ValorLiquido = ordemBolsa.IsTipoOrdemCompra()
                                                            ? operacaoBolsa.Valor + despesas
                                                            : operacaoBolsa.Valor - despesas;
                    operacaoBolsa.PULiquido = Utilitario.Truncate((operacaoBolsa.ValorLiquido.Value / quantidadeNormal) * fator, 10);

                    /* Taxas */
                    operacaoBolsa.Corretagem = corretagem;
                    operacaoBolsa.Emolumento = emolumento;
                    operacaoBolsa.LiquidacaoCBLC = liquidacaoCBLC;
                    operacaoBolsa.RegistroBolsa = registroBolsa;
                    operacaoBolsa.RegistroCBLC = registroCBLC;
                    //
                    operacaoBolsa.Quantidade = quantidade;
                    operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    operacaoBolsa.IdAgenteCorretora = idAgenteCorretora;
                    operacaoBolsa.IdAgenteLiquidacao = idAgenteLiquidacao;
                    operacaoBolsa.Origem = ordemBolsa.Origem;
                    operacaoBolsa.Fonte = (byte)fonteOperacao;

                    if (dataLiquidacao.HasValue)
                    {
                        operacaoBolsa.DataLiquidacao = dataLiquidacao.Value;
                    }
                    else
                    {
                        operacaoBolsa.DataLiquidacao = this.GetDataLiquidacao(data, tipoMercado, LocalFeriadoFixo.Bovespa);
                    }

                    operacaoBolsa.PercentualDesconto = percentualDesconto;
                    operacaoBolsa.Desconto = desconto;
                    operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                    operacaoBolsa.IdCliente = idCliente;
                    operacaoBolsa.CdAtivoBolsaOpcao = null;
                    operacaoBolsa.NumeroNegocio = ordemBolsa.NumeroNegocio;
                    operacaoBolsa.CalculaDespesas = ordemBolsa.CalculaDespesas;
                    operacaoBolsa.IdTrader = ordemBolsa.IdTrader;
                    operacaoBolsa.IdCategoriaMovimentacao = ordemBolsa.IdCategoriaMovimentacao;
                    operacaoBolsa.IdMoeda = idMoeda;
                    operacaoBolsa.ValorISS = valorISS;
                    operacaoBolsa.IdLocalNegociacao = idLocalNegociacao;
                    operacaoBolsa.IdLocalCustodia = idLocalCustodia;
                    operacaoBolsa.IdClearing = idClearing;
                    operacaoBolsa.DataOperacao = ordemBolsa.DataOperacao.GetValueOrDefault(data);
                    
                    operacaoBolsa.Save();
                    #endregion

                    #region InsereOperacaoTermoBolsa
                    //Pego o Id usado na OperacaoBolsa
                    int idOperacao = operacaoBolsa.IdOperacao.Value;

                    OperacaoTermoBolsa operacaoTermoBolsa = new OperacaoTermoBolsa();
                    operacaoTermoBolsa.IdOperacao = idOperacao;
                    operacaoTermoBolsa.Taxa = taxa;
                    operacaoTermoBolsa.NumeroContrato = numeroContrato;
                    operacaoTermoBolsa.IdIndice = idIndice;
                    operacaoTermoBolsa.DataVencimento = dataVencimento;
                    
                    operacaoTermoBolsa.Save();
                    #endregion
                    #endregion
                }
                else
                {
                    #region InsereOperacaoBolsa - Trata Não Termo (a vista, opcoes, futuro - normais e daytrade)
                    //
                    decimal corretagemDaytrade = 0;
                    decimal emolumentoDaytrade = 0;
                    decimal liquidacaoCBLCDaytrade = 0;
                    decimal registroBolsaDaytrade = 0;
                    decimal registroCBLCDaytrade = 0;
                    decimal valorISSDayTrade = 0;
                    //
                    if (quantidadeDaytrade != 0)
                    {
                        /* Proporcionalizo as taxas em relação ao todo da operação */
                        corretagemDaytrade = Utilitario.Truncate(corretagem * (quantidadeDaytrade / quantidade), 2);
                        emolumentoDaytrade = Utilitario.Truncate(emolumento * (quantidadeDaytrade / quantidade), 2);
                        liquidacaoCBLCDaytrade = Utilitario.Truncate(liquidacaoCBLC * (quantidadeDaytrade / quantidade), 2);
                        registroBolsaDaytrade = Utilitario.Truncate(registroBolsa * (quantidadeDaytrade / quantidade), 2);
                        registroCBLCDaytrade = Utilitario.Truncate(registroCBLC * (quantidadeDaytrade / quantidade), 2);
                        decimal despesas = corretagemDaytrade + emolumentoDaytrade + liquidacaoCBLCDaytrade +
                            registroBolsaDaytrade + registroCBLCDaytrade;

                        valorISSDayTrade = Utilitario.Truncate(valorISS * (quantidadeDaytrade / quantidade), 2);

                        /* Adiciono 'D' porque a operação é DT / C = CD, V = VD */
                        string tipoOperacao = tipoOrdem + "D";

                        #region InsereOperacao
                        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                        operacaoBolsa.TipoMercado = tipoMercado;
                        operacaoBolsa.TipoOperacao = tipoOperacao;
                        operacaoBolsa.Data = data;
                        operacaoBolsa.Pu = pu;
                        operacaoBolsa.Valor = valorDaytrade;
                        operacaoBolsa.ValorLiquido = ordemBolsa.IsTipoOrdemCompra()
                                                                ? operacaoBolsa.Valor + despesas
                                                                : operacaoBolsa.Valor - despesas;

                        operacaoBolsa.PULiquido = Utilitario.Truncate((operacaoBolsa.ValorLiquido.Value / quantidadeDaytrade) * fator, 10);
                        /* Taxas */
                        operacaoBolsa.Corretagem = corretagemDaytrade;
                        operacaoBolsa.Emolumento = emolumentoDaytrade;
                        operacaoBolsa.LiquidacaoCBLC = liquidacaoCBLCDaytrade;
                        operacaoBolsa.RegistroBolsa = registroBolsaDaytrade;
                        operacaoBolsa.RegistroCBLC = registroCBLCDaytrade;
                        //
                        operacaoBolsa.Quantidade = quantidadeDaytrade;
                        operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        operacaoBolsa.IdAgenteCorretora = idAgenteCorretora;
                        operacaoBolsa.IdAgenteLiquidacao = idAgenteLiquidacao;
                        operacaoBolsa.Origem = ordemBolsa.Origem;
                        operacaoBolsa.Fonte = (byte)fonteOperacao;                        

                        if (dataLiquidacao.HasValue)
                        {
                            operacaoBolsa.DataLiquidacao = dataLiquidacao.Value;
                        }
                        else
                        {
                            operacaoBolsa.DataLiquidacao = this.GetDataLiquidacao(data, tipoMercado, LocalFeriadoFixo.Bovespa);
                        }

                        operacaoBolsa.PercentualDesconto = percentualDesconto;
                        operacaoBolsa.Desconto = desconto;
                        operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                        operacaoBolsa.IdCliente = idCliente;
                        operacaoBolsa.CdAtivoBolsaOpcao = ordemBolsa.CdAtivoBolsaOpcao;
                        operacaoBolsa.NumeroNegocio = ordemBolsa.NumeroNegocio;
                        operacaoBolsa.CalculaDespesas = ordemBolsa.CalculaDespesas;
                        operacaoBolsa.IdTrader = ordemBolsa.IdTrader;
                        operacaoBolsa.IdMoeda = ordemBolsa.IdMoeda.Value;
                        operacaoBolsa.ValorISS = valorISSDayTrade;
                        operacaoBolsa.IdCategoriaMovimentacao = ordemBolsa.IdCategoriaMovimentacao;
                        operacaoBolsa.IdLocalNegociacao = idLocalNegociacao;
                        operacaoBolsa.IdLocalCustodia = idLocalCustodia;
                        operacaoBolsa.IdClearing = idClearing;
                        operacaoBolsa.DataOperacao = ordemBolsa.DataOperacao.GetValueOrDefault(data);

                        operacaoBolsa.Save();
                        #endregion
                    }

                    if (quantidadeNormal != 0)
                    {
                        decimal corretagemNormal = corretagem - corretagemDaytrade;
                        decimal emolumentoNormal = emolumento - emolumentoDaytrade;
                        decimal liquidacaoCBLCNormal = liquidacaoCBLC - liquidacaoCBLCDaytrade;
                        decimal registroBolsaNormal = registroBolsa - registroBolsaDaytrade;
                        decimal registroCBLCNormal = registroCBLC - registroCBLCDaytrade;
                        decimal despesas = corretagemNormal + emolumentoNormal + liquidacaoCBLCNormal +
                            registroBolsaNormal + registroCBLCNormal;

                        string tipoOperacao = tipoOrdem;

                        decimal valorISSNormal = valorISS - valorISSDayTrade;

                        #region InsereOperacao
                        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                        operacaoBolsa.TipoMercado = tipoMercado;
                        operacaoBolsa.TipoOperacao = tipoOperacao;
                        operacaoBolsa.Data = data;
                        operacaoBolsa.Pu = pu;
                        operacaoBolsa.Valor = valorNormal;
                        operacaoBolsa.ValorLiquido = ordemBolsa.IsTipoOrdemCompra()
                                                                ? operacaoBolsa.Valor + despesas
                                                                : operacaoBolsa.Valor - despesas;
                        operacaoBolsa.PULiquido = Utilitario.Truncate((operacaoBolsa.ValorLiquido.Value / quantidadeNormal) * fator, 10);

                        /* Taxas */
                        operacaoBolsa.Corretagem = corretagemNormal;
                        operacaoBolsa.Emolumento = emolumentoNormal;
                        operacaoBolsa.LiquidacaoCBLC = liquidacaoCBLCNormal;
                        operacaoBolsa.RegistroBolsa = registroBolsaNormal;
                        operacaoBolsa.RegistroCBLC = registroCBLCNormal;
                        //
                        operacaoBolsa.Quantidade = quantidadeNormal;
                        operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        operacaoBolsa.IdAgenteCorretora = idAgenteCorretora;
                        operacaoBolsa.IdAgenteLiquidacao = idAgenteLiquidacao;
                        operacaoBolsa.Origem = ordemBolsa.Origem;
                        operacaoBolsa.Fonte = (byte)fonteOperacao;

                        if (dataLiquidacao.HasValue)
                        {
                            operacaoBolsa.DataLiquidacao = dataLiquidacao.Value;
                        }
                        else
                        {
                            operacaoBolsa.DataLiquidacao = this.GetDataLiquidacao(data, tipoMercado, LocalFeriadoFixo.Bovespa);
                        }

                        operacaoBolsa.PercentualDesconto = percentualDesconto;
                        operacaoBolsa.Desconto = desconto;
                        operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                        operacaoBolsa.IdCliente = idCliente;
                        operacaoBolsa.CdAtivoBolsaOpcao = ordemBolsa.CdAtivoBolsaOpcao;
                        operacaoBolsa.NumeroNegocio = ordemBolsa.NumeroNegocio;
                        operacaoBolsa.CalculaDespesas = ordemBolsa.CalculaDespesas;
                        operacaoBolsa.IdTrader = ordemBolsa.IdTrader;
                        operacaoBolsa.IdMoeda = ordemBolsa.IdMoeda.Value;
                        operacaoBolsa.ValorISS = valorISSNormal;
                        operacaoBolsa.IdCategoriaMovimentacao = ordemBolsa.IdCategoriaMovimentacao;
                        operacaoBolsa.IdLocalNegociacao = idLocalNegociacao;
                        operacaoBolsa.IdLocalCustodia = idLocalCustodia;
                        operacaoBolsa.IdClearing = idClearing;
                        operacaoBolsa.DataOperacao = ordemBolsa.DataOperacao.GetValueOrDefault(data);

                        operacaoBolsa.Save();
                        #endregion
                    }
                    #endregion
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é Primaria
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemPrimaria()
        {
            return this.Origem == OrigemOperacaoBolsa.Primaria;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é ExercicioOpcaoCompra ou ExercicioOpcaoVenda
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemExercicioOpcao()
        {
            return (this.Origem == OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                    this.Origem == OrigemOperacaoBolsa.ExercicioOpcaoVenda);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é ExercicioOpcaoVenda
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemExercicioOpcaoVenda()
        {
            return this.Origem == OrigemOperacaoBolsa.ExercicioOpcaoVenda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é ExercicioOpcaoCompra
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemExercicioOpcaoCompra()
        {
            return this.Origem == OrigemOperacaoBolsa.ExercicioOpcaoCompra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é LiquidacaoTermo
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemLiquidacaoTermo()
        {
            return this.Origem == OrigemOperacaoBolsa.LiquidacaoTermo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é TransferenciaCustodia
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemTransferenciaCustodia()
        {
            return this.Origem == OrigemOperacaoBolsa.TransferenciaCorretora;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é AberturaEmprestimo
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemAberturaEmprestimo()
        {
            return this.Origem == OrigemOperacaoBolsa.AberturaEmprestimo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Origem da Operacao é LiquidacaoEmprestimo
        ///          False caso Contrario
        /// </returns>
        public bool IsOrigemLiquidacaoEmprestimo()
        {
            return this.Origem == OrigemOperacaoBolsa.LiquidacaoEmprestimo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Compra
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCompra()
        {
            return this.TipoOperacao == TipoOperacaoBolsa.Compra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Venda
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoVenda()
        {
            return this.TipoOperacao == TipoOperacaoBolsa.Venda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Retirada
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoRetirada()
        {
            return this.TipoOperacao == TipoOperacaoBolsa.Retirada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é CompraDaytrade
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCompraDaytrade()
        {
            return this.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é VendaDaytrade
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoVendaDaytrade()
        {
            return this.TipoOperacao == TipoOperacaoBolsa.VendaDaytrade;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Operacao é Deposito
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoDeposito()
        {
            return this.TipoOperacao == TipoOperacaoBolsa.Deposito;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoMercado da Operacao é OpçãoCompra
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMercadoOpcaoCompra()
        {
            return this.TipoMercado == TipoMercadoBolsa.OpcaoCompra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoMercado da Operacao é OpçãoVenda
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMercadoOpcaoVenda()
        {
            return this.TipoMercado == TipoMercadoBolsa.OpcaoVenda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoMercado da Operacao é A Vista
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMercadoVista()
        {
            return this.TipoMercado == TipoMercadoBolsa.MercadoVista;
        }

        /// <summary>
        ///     retorna os valores de todas as colunas da tabela OperacaoBolsa
        /// </summary>
        /// <returns></returns>
        /*private List<string> ColunasValues() {
            List<string> valoresPossiveis = new List<string>();
            
            OperacaoBolsaMetadata operacaoBolsaMetadata = OperacaoBolsaMetadata.Meta();
            foreach (esColumnMetadata esColumnMetadata in operacaoBolsaMetadata.Columns) {
                valoresPossiveis.Add(esColumnMetadata.Name);
            }                       
            return valoresPossiveis;
        }*/

        /// <summary>
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaValoresLiquidos(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaValores(idCliente, data);
            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                decimal valor = operacaoBolsa.Valor.Value;
                decimal quantidade = operacaoBolsa.Quantidade.Value;
                decimal pu = operacaoBolsa.Pu.Value;

                decimal fator = 0;
                if (valor != 0)
                {
                    fator = (quantidade * pu) / valor;
                }
                decimal puLiquido = operacaoBolsa.PULiquido.Value;
                // taxas
                decimal emolumento = operacaoBolsa.Emolumento.Value;
                decimal registroBolsa = operacaoBolsa.RegistroBolsa.Value;
                decimal liquidacaoCBLC = operacaoBolsa.LiquidacaoCBLC.Value;
                decimal registroCBLC = operacaoBolsa.RegistroCBLC.Value;
                decimal corretagem = operacaoBolsa.Corretagem.Value;
                decimal taxas = corretagem + emolumento + registroBolsa + liquidacaoCBLC + registroCBLC;
                decimal valorLiquido = 0;

                switch (operacaoBolsa.TipoOperacao)
                {
                    case TipoOperacaoBolsa.Compra:
                    case TipoOperacaoBolsa.CompraDaytrade:
                        if (fator != 0)
                        {
                            puLiquido = pu + (taxas * fator) / quantidade;
                        }
                        else
                        {
                            puLiquido = pu;
                        }

                        valorLiquido = valor + taxas;
                        break;
                    case TipoOperacaoBolsa.Venda:
                    case TipoOperacaoBolsa.VendaDaytrade:
                        if (fator != 0)
                        {
                            puLiquido = pu - (taxas * fator) / quantidade;
                        }
                        else
                        {
                            puLiquido = pu;
                        }

                        valorLiquido = valor - taxas;
                        break;
                }

                operacaoBolsa.ValorLiquido = valorLiquido;
                operacaoBolsa.PULiquido = puLiquido;
            }

            // Salva a collection de operações
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoVisualizacao"></param>
        /// enum ContaCorrente.VizualizacaoOperacaoBolsa
        public void LancaCCOperacao(int idCliente, DateTime data, VizualizacaoOperacaoBolsa tipoVisualizacao)
        {
            if (tipoVisualizacao == VizualizacaoOperacaoBolsa.Analitico)
            {
                this.LancaCCOperacaoAnalitico(idCliente, data);
                this.LancaCCOperacaoDespesas(idCliente, data);
            }
            else if (tipoVisualizacao == VizualizacaoOperacaoBolsa.Consolidado)
            {
                this.LancaCCOperacaoConsolidado(idCliente, data);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void LancaCCOperacaoAnalitico(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            //
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaLancaOperacaoAnalitico(idCliente, data);

            int idMoeda = 0;
            int idMoedaAnterior = 0;
            int idConta = 0;
            int idcontaAnterior = 0;

            if (operacaoBolsaCollection.Count > 0)
            {
                idMoeda = operacaoBolsaCollection[0].IdMoeda.Value;
            }

            int i = 0;
            while (i < operacaoBolsaCollection.Count)
            {
                idMoedaAnterior = idMoeda;

                int idContaDefault = 0;
                if (operacaoBolsaCollection.Count > 0 && i < operacaoBolsaCollection.Count)
                {
                    //Busca o idContaDefault do cliente
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);

                    idConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBolsaCollection[i].IdConta.HasValue && operacaoBolsaCollection[i].IdConta.Value > 0 ? operacaoBolsaCollection[i].IdConta.Value : idContaDefault;
                    idcontaAnterior = idConta;
                    //
                }

                while (idMoeda == idMoedaAnterior && i < operacaoBolsaCollection.Count && idConta == idcontaAnterior)
                {
                    OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                    decimal valorTotal = operacaoBolsa.Valor.Value;
                    DateTime dataLiquidacao = operacaoBolsa.DataLiquidacao.Value;
                    int idAgenteLiquidacao = operacaoBolsa.IdAgenteLiquidacao.Value;
                    string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                    // para calculo da origem Liquidacao                
                    string tipoMercado = operacaoBolsa.TipoMercado;
                    string tipoOperacao = operacaoBolsa.TipoOperacao;
                    int origemOperacao = operacaoBolsa.Origem.Value;
                    idMoeda = operacaoBolsa.IdMoeda.Value;

                    string descricao = "";
                    if (operacaoBolsa.IsTipoCompra() || operacaoBolsa.IsTipoCompraDaytrade())
                    {
                        descricao = "Compra";
                        valorTotal = valorTotal * -1;
                    }
                    else if (operacaoBolsa.IsTipoVenda() || operacaoBolsa.IsTipoVendaDaytrade())
                    {
                        descricao = "Venda";
                    }

                    if (operacaoBolsa.IsOrigemExercicioOpcao())
                    {
                        descricao = "Exercicio " + descricao;
                    }

                    descricao += " - " + cdAtivoBolsa;

                    AgenteMercado agenteMercado = new AgenteMercado();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(agenteMercado.Query.Nome);
                    agenteMercado.LoadByPrimaryKey(campos, idAgenteLiquidacao);

                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = descricao + "(" + agenteMercado.Nome + ")";
                    liquidacao.Valor = valorTotal;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = operacaoBolsa.GetOrigemLancamentoLiquidacao(tipoMercado, tipoOperacao, origemOperacao);
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteLiquidacao;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idConta;
                    #endregion

                    liquidacaoCollection.AttachEntity(liquidacao);

                    i += 1;

                    if (i < operacaoBolsaCollection.Count)
                        idConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBolsaCollection[i].IdConta.HasValue && operacaoBolsaCollection[i].IdConta.Value > 0 ? operacaoBolsaCollection[i].IdConta.Value : idContaDefault;

                }
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void LancaCCOperacaoConsolidado(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaLancaOperacaoConsolidado(idCliente, data);

            int idMoeda = 0;
            int idMoedaAnterior = 0;
            if (operacaoBolsaCollection.Count > 0)
            {
                idMoeda = operacaoBolsaCollection[0].IdMoeda.Value;
            }

            int i = 0;
            while (i < operacaoBolsaCollection.Count)
            {
                idMoedaAnterior = idMoeda;

                int idContaDefault = 0;
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                //

                while (idMoeda == idMoedaAnterior && i < operacaoBolsaCollection.Count)
                {
                    OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];
                    int idAgenteLiquidacao = operacaoBolsa.IdAgenteLiquidacao.Value;
                    DateTime dataLiquidacao = operacaoBolsa.DataLiquidacao.Value;
                    string tipoOperacao = operacaoBolsa.TipoOperacao;
                    idMoeda = operacaoBolsa.IdMoeda.Value;

                    int idConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBolsa.IdConta.HasValue && operacaoBolsa.IdConta.Value > 0 ? operacaoBolsa.IdConta.Value : idContaDefault;
                    int idContaAux = idConta;

                    int idAgenteLiquidacaoAux = idAgenteLiquidacao;
                    DateTime dataLiquidacaoAux = dataLiquidacao;
                    string tipoOperacaoAux = tipoOperacao;

                    //Faz o loop abaixo para somente pegar despesas de termo/futuro
                    decimal valorTotal = 0;
                    while (idAgenteLiquidacaoAux == idAgenteLiquidacao && dataLiquidacaoAux == dataLiquidacao && idConta == idContaAux)
                    {
                        string tipoMercado = operacaoBolsaCollection[i].TipoMercado;
                        decimal valorOperado = operacaoBolsaCollection[i].ValorLiquido.Value;
                        decimal despesas = operacaoBolsaCollection[i].Corretagem.Value + operacaoBolsaCollection[i].Emolumento.Value +
                                           operacaoBolsaCollection[i].LiquidacaoCBLC.Value + operacaoBolsaCollection[i].RegistroBolsa.Value +
                                           operacaoBolsaCollection[i].RegistroCBLC.Value;

                        if (tipoMercado == TipoMercadoBolsa.Futuro || tipoMercado == TipoMercadoBolsa.Termo)
                        {
                            valorTotal += despesas * -1;
                        }
                        else if (tipoOperacao == TipoOperacaoBolsa.Compra || tipoOperacao == TipoOperacaoBolsa.CompraDaytrade)
                        {
                            valorTotal += valorOperado * -1;
                        }
                        else
                        {
                            valorTotal += valorOperado;
                        }

                        if (i < operacaoBolsaCollection.Count - 1)
                        {
                            idAgenteLiquidacao = operacaoBolsaCollection[i + 1].IdAgenteLiquidacao.Value;
                            dataLiquidacao = operacaoBolsaCollection[i + 1].DataLiquidacao.Value;
                            tipoOperacao = operacaoBolsaCollection[i + 1].TipoOperacao;
                            idConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBolsaCollection[i + 1].IdConta.HasValue && operacaoBolsaCollection[i + 1].IdConta.Value > 0 ? operacaoBolsaCollection[i + 1].IdConta.Value : idContaDefault;

                            if (idAgenteLiquidacaoAux != idAgenteLiquidacao || dataLiquidacaoAux != dataLiquidacao || idContaAux != idConta)
                            {
                                break;
                            }
                            else
                            {
                                i += 1;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    //

                    AgenteMercado agenteMercado = new AgenteMercado();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(agenteMercado.Query.Nome);
                    agenteMercado.LoadByPrimaryKey(campos, idAgenteLiquidacaoAux);

                    #region Nova Liquidacao
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacaoAux;
                    liquidacao.Descricao = "Valor Liquido de Bolsa (" + agenteMercado.Nome + ")";
                    liquidacao.Valor = valorTotal;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Bolsa.Outros;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgenteLiquidacaoAux;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaAux;

                    #endregion

                    liquidacaoCollection.AttachEntity(liquidacao);

                    i += 1;
                }
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void LancaCCOperacaoDespesas(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.BuscaOperacaoBolsaLancaOperacaoDespesas(idCliente, data);

            int idMoeda = 0;
            int idMoedaAnterior = 0;
            int idConta = 0;
            int idContaAnterior = 0;
            if (operacaoBolsaCollection.Count > 0)
            {
                idMoeda = operacaoBolsaCollection[0].IdMoeda.Value;
            }

            int i = 0;
            while (i < operacaoBolsaCollection.Count)
            {
                idMoedaAnterior = idMoeda;

                int idContaDefault = 0;
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoeda);
                //

                idConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBolsaCollection[i].IdConta.HasValue && operacaoBolsaCollection[i].IdConta.Value > 0 ? operacaoBolsaCollection[i].IdConta.Value : idContaDefault;
                idContaAnterior = idConta;

                while (idMoeda == idMoedaAnterior && i < operacaoBolsaCollection.Count && idConta == idContaAnterior)
                {
                    OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];

                    // taxas
                    decimal corretagem = operacaoBolsa.Corretagem.Value;
                    decimal emolumento = operacaoBolsa.Emolumento.Value;
                    decimal liquidacaoCBLC = operacaoBolsa.LiquidacaoCBLC.Value;
                    decimal registroBolsa = operacaoBolsa.RegistroBolsa.Value;
                    decimal registroCBLC = operacaoBolsa.RegistroCBLC.Value;
                    //                             
                    DateTime dataLiquidacao = operacaoBolsa.DataLiquidacao.Value;
                    int idAgenteLiquidacao = operacaoBolsa.IdAgenteLiquidacao.Value;
                    idMoeda = operacaoBolsa.IdMoeda.Value;
                    //
                    decimal taxas = (emolumento + liquidacaoCBLC + registroBolsa + registroCBLC);

                    taxas = taxas * -1;
                    corretagem = corretagem * -1;

                    #region Liquidacao1
                    if (corretagem != 0)
                    {
                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataLiquidacao;
                        liquidacao.Descricao = "Corretagem s/ Operações em Bolsa";
                        liquidacao.Valor = corretagem;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = (int)OrigemLancamentoLiquidacao.Bolsa.Corretagem;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdAgente = idAgenteLiquidacao;
                        liquidacao.IdCliente = idCliente;
                        liquidacao.IdConta = idConta;

                        // Insere em Liquidacao 
                        liquidacaoCollection.AttachEntity(liquidacao);
                    }
                    #endregion

                    #region Liquidacao2
                    if (taxas != 0)
                    {
                        Liquidacao liquidacaoAux = new Liquidacao();
                        liquidacaoAux.DataLancamento = data;
                        liquidacaoAux.DataVencimento = dataLiquidacao;
                        liquidacaoAux.Descricao = "Despesas Taxas s/ Operações em Bolsa";
                        liquidacaoAux.Valor = taxas;
                        liquidacaoAux.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacaoAux.Origem = (int)OrigemLancamentoLiquidacao.Bolsa.DespesasTaxas;
                        liquidacaoAux.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacaoAux.IdAgente = idAgenteLiquidacao;
                        liquidacaoAux.IdCliente = idCliente;
                        liquidacaoAux.IdConta = idConta;

                        // Insere em Liquidacao 
                        liquidacaoCollection.AttachEntity(liquidacaoAux);
                    }
                    #endregion

                    i += 1;

                    if(i<operacaoBolsaCollection.Count)
                        idConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoBolsaCollection[i].IdConta.HasValue && operacaoBolsaCollection[i].IdConta.Value > 0 ? operacaoBolsaCollection[i].IdConta.Value : idContaDefault;
                }

            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoMercado"></param>
        /// <param name="tipoOperacao"></param>
        /// <param name="origemOperacao"></param>
        /// <returns>int representando a Origem do Lancamento da Liquidacao
        /// valores possiveis de retorno:
        /// OrigemLancamentoLiquidacao.CompraAcoes,
        /// OrigemLancamentoLiquidacao.VendaAcoes,
        /// OrigemLancamentoLiquidacao.CompraOpcoes,
        /// OrigemLancamentoLiquidacao.VendaOpcoes,
        /// OrigemLancamentoLiquidacao.ExercicioCompra,
        /// OrigemLancamentoLiquidacao.ExercicioVenda,
        /// </returns>
        /// throws ArgumentExcepition se 
        /// tipoMercado diferente dos valores do enum TipoMercadoBolsa
        /// tipoOperacao diferente dos valores do enum TipoOperacaoBolsa
        /// origemOperacao diferente dos valores do enum OrigemOperacaoBolsa
        public int GetOrigemLancamentoLiquidacao(string tipoMercado, string tipoOperacao, int origemOperacao)
        {

            #region ArgumentosNulos - throw ArgumentException
            List<string> valoresPossiveisTipoMercado = TipoMercadoBolsa.Values();
            if (!valoresPossiveisTipoMercado.Contains(tipoMercado))
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("tipoMercado incorreto. Os valores possiveis estão no enum TipoMercadoBolsa");
                throw new ArgumentException(mensagem.ToString());
            }
            List<string> valoresPossiveisTipoOperacao = TipoOperacaoBolsa.Values();
            if (!valoresPossiveisTipoOperacao.Contains(tipoOperacao))
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("tipoOperacao incorreto. Os valores possiveis estão no enum TipoOperacaoBolsa");
                throw new ArgumentException(mensagem.ToString());
            }
            List<int> valoresPossiveisOrigem = OrigemOperacaoBolsa.Values();
            if (!valoresPossiveisOrigem.Contains(origemOperacao))
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("origemOperacao incorreto. Os valores possiveis estão no enum OrigemOperacaoBolsa");
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            int origemLiquidacao = (int)OrigemLancamentoLiquidacao.Bolsa.None;

            #region Define Origem da Liquidacao
            if (this.IsTipoCompra() || this.IsTipoCompraDaytrade() &&
                this.IsOrigemPrimaria() &&
                this.IsTipoMercadoVista())
            {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.Bolsa.CompraAcoes;
            }
            else if (this.IsTipoVenda() || this.IsTipoVendaDaytrade() &&
                     this.IsOrigemPrimaria() &&
                     this.IsTipoMercadoVista())
            {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.Bolsa.VendaAcoes;
            }
            else if (this.IsTipoCompra() || this.IsTipoCompraDaytrade() &&
                     this.IsOrigemPrimaria() &&
                     this.IsTipoMercadoOpcaoCompra() || this.IsTipoMercadoOpcaoVenda())
            {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.Bolsa.CompraOpcoes;
            }
            else if (this.IsTipoVenda() || this.IsTipoVendaDaytrade() &&
                     this.IsOrigemPrimaria() &&
                     this.IsTipoMercadoOpcaoCompra() || this.IsTipoMercadoOpcaoVenda())
            {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.Bolsa.VendaOpcoes;
            }
            else if (this.IsTipoCompra() &&
                     this.IsOrigemExercicioOpcao() &&
                     this.IsTipoMercadoVista())
            {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.Bolsa.ExercicioCompra;
            }
            else if (this.IsTipoVenda() &&
                     this.IsOrigemExercicioOpcao() &&
                     this.IsTipoMercadoVista())
            {

                origemLiquidacao = (int)OrigemLancamentoLiquidacao.Bolsa.ExercicioVenda;
            }
            #endregion

            #region Confere se origem está dentro dos valores permitidos de retorno
            List<int> valoresPossiveisRetorno = new List<int>();
            valoresPossiveisRetorno.Add((int)OrigemLancamentoLiquidacao.Bolsa.CompraAcoes);
            valoresPossiveisRetorno.Add((int)OrigemLancamentoLiquidacao.Bolsa.VendaAcoes);
            valoresPossiveisRetorno.Add((int)OrigemLancamentoLiquidacao.Bolsa.CompraOpcoes);
            valoresPossiveisRetorno.Add((int)OrigemLancamentoLiquidacao.Bolsa.VendaOpcoes);
            valoresPossiveisRetorno.Add((int)OrigemLancamentoLiquidacao.Bolsa.ExercicioCompra);
            valoresPossiveisRetorno.Add((int)OrigemLancamentoLiquidacao.Bolsa.ExercicioVenda);

            if (!valoresPossiveisRetorno.Contains(origemLiquidacao))
            {
                Assert.Fail("Valor de retorno inconsistente");
            }
            #endregion

            return origemLiquidacao;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoMercado"></param>
        /// <param name="idLocal"></param>
        /// <param name="dataBase">dataBase para o calculo do dia da Liquidacao</param>
        /// <returns>Data da Liquidação de acordo com o Mercado
        /// </returns>
        /// throws ArgumentException se 
        /// tipoMercado diferente dos valores do enum TipoMercadoBolsa
        /// idLocal diferente dos valores do enum LocalFeriadoFixo
        public DateTime GetDataLiquidacao(DateTime dataBase, string tipoMercado, int idLocal)
        {

            #region ArgumentosNulos - throw ArgumentException
            List<string> valoresPossiveisTipoMercado = TipoMercadoBolsa.Values();
            if (!valoresPossiveisTipoMercado.Contains(tipoMercado))
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("tipoMercado incorreto. Os valores possiveis estão no enum TipoMercadoBolsa");
                throw new ArgumentException(mensagem.ToString());
            }
            List<int> valoresPossiveisLocal = LocalFeriadoFixo.Values();
            if (!valoresPossiveisLocal.Contains(idLocal))
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("idLocal incorreto. Os valores possiveis estão no enum LocalFeriadoFixo");
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            int dias = 0;
            if (tipoMercado == TipoMercadoBolsa.MercadoVista || tipoMercado == TipoMercadoBolsa.Termo || tipoMercado == TipoMercadoBolsa.Imobiliario)
            {
                dias = LiquidacaoMercado.Acoes;
            }
            else if (tipoMercado == TipoMercadoBolsa.OpcaoCompra ||
                    tipoMercado == TipoMercadoBolsa.OpcaoVenda)
            {
                dias = LiquidacaoMercado.OpcoesBolsa;
            }
            else if (tipoMercado == TipoMercadoBolsa.Futuro)
            {
                dias = LiquidacaoMercado.FuturosBolsa;
            }

            DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(dataBase, dias, idLocal, TipoFeriado.Brasil);

            return dataLiquidacao;
        }

        /// <summary>
        /// Busca totais de valor, quantidade e despesas dados os parametros passados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="idAgente"></param>
        /// <param name="tipoOperacao"></param>
        public void BuscaTotaisOperados(int idCliente, DateTime data, string cdAtivoBolsa, int idAgente, string tipoOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum(), this.Query.Quantidade.Sum(), this.Query.Corretagem.Sum(),
                         this.Query.RegistroBolsa.Sum(), this.Query.RegistroCBLC.Sum(),
                         this.Query.LiquidacaoCBLC.Sum(), this.Query.Emolumento.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Equal(data),
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                        this.Query.IdAgenteCorretora == idAgente,
                        this.Query.TipoOperacao.Equal(tipoOperacao)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                this.Valor = this.Valor.HasValue ? this.Valor.Value : 0;
                this.Quantidade = this.Quantidade.HasValue ? this.Quantidade.Value : 0;
                this.Corretagem = this.Corretagem.HasValue ? this.Corretagem.Value : 0;
                this.RegistroBolsa = this.RegistroBolsa.HasValue ? this.RegistroBolsa.Value : 0;
                this.RegistroCBLC = this.RegistroCBLC.HasValue ? this.RegistroCBLC.Value : 0;
                this.LiquidacaoCBLC = this.LiquidacaoCBLC.HasValue ? this.LiquidacaoCBLC.Value : 0;
                this.Emolumento = this.Emolumento.HasValue ? this.Emolumento.Value : 0;
            }
        }

        /// <summary>
        /// Carrega o objeto OperacaoBolsa com os campos ResultadoRealizadoSum(calculado)
        /// ,ResultadoExercicioSum(calculado), ResultadoTermoSum(calculado).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="tipoMercado">pode ser passado no formato IN(...)</param>
        public void BuscaTotalResultadoNormal(int idCliente, DateTime dataInicio, DateTime dataFim, string tipoMercado)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ResultadoRealizado.Sum(), this.Query.ResultadoExercicio.Sum(),
                         this.Query.ResultadoTermo.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(tipoMercado),
                        this.Query.TipoOperacao.NotIn(TipoOperacaoBolsa.CompraDaytrade,
                                                       TipoOperacaoBolsa.VendaDaytrade)
                        );
            this.Query.Load();

            // Se valores dos campos são nulos retorna zero
            if (this.es.HasData)
            {
                this.ResultadoRealizado = this.ResultadoRealizado.HasValue ? this.ResultadoRealizado.Value : 0;
                this.ResultadoExercicio = this.ResultadoExercicio.HasValue ? this.ResultadoExercicio.Value : 0;
                this.ResultadoTermo = this.ResultadoTermo.HasValue ? this.ResultadoTermo.Value : 0;
            }
        }

        public decimal RetornaTotalResultadoETF(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");

            operacaoBolsaQuery.Select(operacaoBolsaQuery.ResultadoRealizado.Sum());
            operacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == operacaoBolsaQuery.CdAtivoBolsa);
            operacaoBolsaQuery.Where(operacaoBolsaQuery.IdCliente == idCliente,
                                     operacaoBolsaQuery.Data.Between(dataInicio, dataFim),
                                     operacaoBolsaQuery.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.Compra),
                                     ativoBolsaQuery.TipoPapel.Equal((byte)TipoPapelAtivo.ETF));

            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Load(operacaoBolsaQuery);

            return operacaoBolsa.ResultadoRealizado.HasValue ? operacaoBolsa.ResultadoRealizado.Value : 0;
        }

        /// <summary>
        /// Retorna o total de resultado daytrade líquido de despesas.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <param name="tipoMercado">pode ser passado no formato IN(...)</param>
        public decimal RetornaTotalResultadoDayTrade(int idCliente, DateTime dataInicio, DateTime dataFim, string tipoMercado)
        {
            //Total de Venda DT (sem considerar exercícios)
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(tipoMercado),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.VendaDaytrade),
                        this.Query.Origem.NotIn((byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra, (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda)
                        );
            this.Query.Load();

            decimal valorVendaDT = 0;
            if (this.es.HasData)
            {
                valorVendaDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }


            //Total de Compra DT (sem considerar exercícios)
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(tipoMercado),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade),
                        this.Query.Origem.NotIn((byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra, (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda)
                        );
            this.Query.Load();

            decimal valorCompraDT = 0;
            if (this.es.HasData)
            {
                valorCompraDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            //Total de Venda e Compra DT (somente exercícios)
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query
                 .Select(operacaoBolsaCollection.Query.CdAtivoBolsa, this.Query.PULiquido, this.Query.Quantidade, this.Query.TipoOperacao)
                 .Where(operacaoBolsaCollection.Query.IdCliente == idCliente,
                        operacaoBolsaCollection.Query.Data.GreaterThanOrEqual(dataInicio),
                        operacaoBolsaCollection.Query.Data.LessThanOrEqual(dataFim),
                        operacaoBolsaCollection.Query.TipoMercado.In(tipoMercado),
                        operacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade),
                        operacaoBolsaCollection.Query.Origem.In((byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra, (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda)
                        );
            operacaoBolsaCollection.Query.Load();

            decimal valorLiquidoDTExercicio = 0;
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                string tipoOperacao = operacaoBolsa.TipoOperacao.Trim();
                decimal quantidade = operacaoBolsa.Quantidade.Value;
                string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                decimal puLiquido = operacaoBolsa.PULiquido.Value;
                decimal fator = 1M;
                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataFim))
                {
                    fator = fatorCotacaoBolsa.Fator.Value;
                }

                decimal valorLiquidoExercicio = Utilitario.Truncate(puLiquido * quantidade / fator, 2);

                if (tipoOperacao == TipoOperacaoBolsa.VendaDaytrade)
                {
                    valorLiquidoDTExercicio += valorLiquidoExercicio;
                }
                else
                {
                    valorLiquidoDTExercicio -= valorLiquidoExercicio;
                }
            }

            decimal valorLiquidoDT = valorVendaDT - valorCompraDT + valorLiquidoDTExercicio;

            return valorLiquidoDT;
        }

        /// <summary>
        /// Retorna o total de resultado daytrade líquido de despesas para ações.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>        
        public decimal RetornaTotalResultadoDayTradeAcoes(int idCliente, int idAgenteCorretora, DateTime dataInicio, DateTime dataFim)
        {
            //Total de Venda DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.VendaDaytrade)
                        );
            this.Query.Load();

            decimal valorVendaDT = 0;
            if (this.es.HasData)
            {
                valorVendaDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }


            //Total de Compra DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade)
                        );
            this.Query.Load();

            decimal valorCompraDT = 0;
            if (this.es.HasData)
            {
                valorCompraDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            decimal valorLiquidoDT = valorVendaDT - valorCompraDT;

            return valorLiquidoDT;
        }

        /// <summary>
        /// Retorna o total de resultado daytrade líquido de despesas para opções.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>        
        public decimal RetornaTotalResultadoDayTradeOpcoes(int idCliente, int idAgenteCorretora, DateTime dataInicio, DateTime dataFim)
        {
            //Total de Venda DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.VendaDaytrade)
                        );
            this.Query.Load();

            decimal valorVendaDT = 0;
            if (this.es.HasData)
            {
                valorVendaDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }


            //Total de Compra DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade)
                        );
            this.Query.Load();

            decimal valorCompraDT = 0;
            if (this.es.HasData)
            {
                valorCompraDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            decimal valorLiquidoDT = valorVendaDT - valorCompraDT;

            return valorLiquidoDT;
        }

        /// <summary>
        /// Retorna o total de resultado daytrade líquido de despesas para FII.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>        
        public decimal RetornaTotalResultadoDayTradeFII(int idCliente, int idAgenteCorretora, DateTime dataInicio, DateTime dataFim)
        {
            //Total de Venda DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.Equal(TipoMercadoBolsa.Imobiliario),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.VendaDaytrade)
                        );
            this.Query.Load();

            decimal valorVendaDT = 0;
            if (this.es.HasData)
            {
                valorVendaDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }


            //Total de Compra DT
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.GreaterThanOrEqual(dataInicio),
                        this.Query.Data.LessThanOrEqual(dataFim),
                        this.Query.TipoMercado.Equal(TipoMercadoBolsa.Imobiliario),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade)
                        );
            this.Query.Load();

            decimal valorCompraDT = 0;
            if (this.es.HasData)
            {
                valorCompraDT = this.ValorLiquido.HasValue ? this.ValorLiquido.Value : 0;
            }

            decimal valorLiquidoDT = valorVendaDT - valorCompraDT;

            return valorLiquidoDT;
        }

        /// <summary>
        /// Método central para deletar operações originadas de Liquidação e/ou Vencimento de Empréstimo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaOperacoesLiquidacaoEmprestimo(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.DeletaOperacaoLiquidacaoEmprestimo(idCliente, data);
        }

        /// <summary>
        /// Método central para deletar operações originadas de Liquidação e/ou Vencimento de Termo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaOperacoesLiquidacaoTermo(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.DeletaOperacaoLiquidacaoTermo(idCliente, data);
        }

        /// <summary>
        /// Retorna o valor total operado de vendas.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaTotalVendasAcoes(int idCliente, DateTime dataInicio, DateTime dataFim)
        {
            OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");

            operacaoBolsaQuery.Select(operacaoBolsaQuery.Valor.Sum());
            operacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == operacaoBolsaQuery.CdAtivoBolsa);
            operacaoBolsaQuery.Where(operacaoBolsaQuery.IdCliente == idCliente,
                                     operacaoBolsaQuery.Data.Between(dataInicio, dataFim),
                                     operacaoBolsaQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                     operacaoBolsaQuery.TipoOperacao.Equal(TipoOperacaoBolsa.Venda),
                                     ativoBolsaQuery.TipoPapel.NotEqual((byte)TipoPapelAtivo.ETF));

            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Load(operacaoBolsaQuery);

            return operacaoBolsa.Valor.HasValue ? operacaoBolsa.Valor.Value : 0;
        }

        /// <summary>
        /// Retorna o valor total operado de vendas.
        /// Filtrado por: TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaTotalVendas(int idCliente, string cdAtivoBolsa, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa == cdAtivoBolsa,
                        this.Query.Data.Between(dataInicio, dataFim),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));

            this.Query.Load();

            return this.Valor.HasValue ? this.Valor.Value : 0;
        }

        /// <summary>
        /// Retorna o valor total operado de compras.
        /// Filtrado por: TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaTotalCompras(int idCliente, string cdAtivoBolsa, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa == cdAtivoBolsa,
                        this.Query.Data.Between(dataInicio, dataFim),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));

            this.Query.Load();

            return this.Valor.HasValue ? this.Valor.Value : 0;
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N'.
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudo(int idCliente, DateTime data, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.Valor.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.CalculaDespesas.Equal("N"),
                                    operacaoBolsa.Query.Fonte.NotIn((byte)FonteOperacaoBolsa.Interno,
                                                                    (byte)FonteOperacaoBolsa.Sinacor));
            if (operacaoBolsa.Query.Load())
            {
                if (operacaoBolsa.Valor.HasValue)
                {
                    totalValor = operacaoBolsa.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.CalculaDespesas.Equal("N"),
                                                operacaoBolsa.Query.Fonte.NotIn((byte)FonteOperacaoBolsa.Interno,
                                                                    (byte)FonteOperacaoBolsa.Sinacor));
            operacaoBolsaCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa = operacaoBolsaCollection[i];
                decimal valor = operacaoBolsa.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima operacao da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima operacao
                if (i == operacaoBolsaCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }

                operacaoBolsa.Corretagem = corretagem;
                operacaoBolsa.Emolumento = taxas;
                operacaoBolsa.RegistroBolsa = 0;
                operacaoBolsa.LiquidacaoCBLC = 0;
                operacaoBolsa.RegistroCBLC = 0;
            }
            #endregion

            // Salva a collection de operações
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N'.
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tiposMercado"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudo(int idCliente, DateTime data, List<string> tiposMercado, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.Valor.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.CalculaDespesas.Equal("N"),
                                    operacaoBolsa.Query.Fonte.NotIn((byte)FonteOperacaoBolsa.Interno,
                                                                    (byte)FonteOperacaoBolsa.Sinacor),
                                    operacaoBolsa.Query.TipoMercado.In(tiposMercado));
            if (operacaoBolsa.Query.Load())
            {
                if (operacaoBolsa.Valor.HasValue)
                {
                    totalValor = operacaoBolsa.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.CalculaDespesas.Equal("N"),
                                                operacaoBolsa.Query.Fonte.NotIn((byte)FonteOperacaoBolsa.Interno,
                                                                                (byte)FonteOperacaoBolsa.Sinacor),
                                                operacaoBolsa.Query.TipoMercado.In(tiposMercado));
            operacaoBolsaCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa = operacaoBolsaCollection[i];
                decimal valor = operacaoBolsa.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima operacao da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima operacao
                if (i == operacaoBolsaCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }

                operacaoBolsa.Corretagem = corretagem;
                operacaoBolsa.Emolumento = taxas;
                operacaoBolsa.RegistroBolsa = 0;
                operacaoBolsa.LiquidacaoCBLC = 0;
                operacaoBolsa.RegistroCBLC = 0;
            }
            #endregion

            // Salva a collection de operações
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N'.
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudoPorIds(List<int> listaIds, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.Valor.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdOperacao.In(listaIds));
            if (operacaoBolsa.Query.Load())
            {
                if (operacaoBolsa.Valor.HasValue)
                {
                    totalValor = operacaoBolsa.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Where(operacaoBolsa.Query.IdOperacao.In(listaIds));
            operacaoBolsaCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa = operacaoBolsaCollection[i];
                decimal valor = operacaoBolsa.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima operacao da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima operacao
                if (i == operacaoBolsaCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }

                operacaoBolsa.Corretagem = corretagem;
                operacaoBolsa.Emolumento = taxas;
                operacaoBolsa.RegistroBolsa = 0;
                operacaoBolsa.LiquidacaoCBLC = 0;
                operacaoBolsa.RegistroCBLC = 0;
                operacaoBolsa.CalculaDespesas = "N";
            }
            #endregion

            // Salva a collection de operações
            operacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega operacoes de bolsa das carteiras/clientes filhas.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaOperacoesFilhas(int idCliente, DateTime data)
        {
            List<int> listaCliente = new List<int>();
            CarteiraMaeCollection carteiraMaeCollection = new CarteiraMaeCollection();
            carteiraMaeCollection.Query.Select(carteiraMaeCollection.Query.IdCarteiraFilha);
            carteiraMaeCollection.Query.Where(carteiraMaeCollection.Query.IdCarteiraMae.Equal(idCliente));
            carteiraMaeCollection.Query.Load();

            foreach (CarteiraMae carteiraMae in carteiraMaeCollection)
            {
                listaCliente.Add(carteiraMae.IdCarteiraFilha.Value);
            }

            if (listaCliente.Count == 0)
            {
                return;
            }

            OperacaoBolsaCollection operacaoBolsaCollectionDeletar = new OperacaoBolsaCollection();
            operacaoBolsaCollectionDeletar.Query.Where(operacaoBolsaCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                             operacaoBolsaCollectionDeletar.Query.Data.Equal(data),
                                             operacaoBolsaCollectionDeletar.Query.Fonte.NotEqual((byte)FonteOperacaoBolsa.Manual),
                                             operacaoBolsaCollectionDeletar.Query.Origem.In((byte)OrigemOperacaoBolsa.Primaria,
                                                                                            (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra,
                                                                                            (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda));
            operacaoBolsaCollectionDeletar.Query.Load();
            operacaoBolsaCollectionDeletar.MarkAllAsDeleted();
            operacaoBolsaCollectionDeletar.Save();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.In(listaCliente),
                                             operacaoBolsaCollection.Query.Data.Equal(data),
                                             operacaoBolsaCollection.Query.Fonte.NotEqual((byte)FonteOperacaoBolsa.Manual),
                                             operacaoBolsaCollection.Query.Origem.In((byte)OrigemOperacaoBolsa.Primaria,
                                                                                     (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra,
                                                                                     (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda));
            operacaoBolsaCollection.Query.Load();

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                OperacaoBolsa operacaoBolsaNova = new OperacaoBolsa();
                operacaoBolsaNova.LoadByPrimaryKey(operacaoBolsa.IdOperacao.Value);
                operacaoBolsaNova.MarkAllColumnsAsDirty(DataRowState.Added);
                operacaoBolsaNova.IdCliente = idCliente;
                operacaoBolsaNova.Save();
            }
        }

        /// <summary>
        /// Carrega em OperacaoBolsa a partir de arquivo lido da Tema.
        /// </summary>
        /// <param name="dicArquivos">
        ///  Dicionario com tres chaves que contem o conteudo dos arquivos Tema
        ///  ["Movimento"] = lista de Arquivos de Movimento
        ///  ["Mensal"] = lista de Arquivos de ApuracaoMensais
        ///  ["NotaCorretagem"] = lista de Arquivos de NotaCorretagem
        ///  ["HeaderNotaCorretagem"] = lista com os headers dos Arquivos de NotaCorretagem
        /// </param>        
        public void IntegraOperacaoTema(Dictionary<string, Object> dicArquivos)
        {
            //
            //List<Tema.Apuracao[]> apuracao = dicArquivos["Mensal"] as List<Tema.Apuracao[]>;
            //List<Tema.Movimento[]> movimentos = dicArquivos["Movimento"] as List<Tema.Movimento[]>;

            //for (int i = 0; i < apuracao.Count; i++) {
            //    Tema.Apuracao[] apur = apuracao[i];

            //    for (int j = 0; j < apur.Length; j++) {
            //        // apur[j].codAcao; 
            //    }
            //}

            //for (int i = 0; i < movimentos.Count; i++)
            //{
            //    Tema.Movimento[] mov = movimentos[i];

            //    for (int j = 0; j < mov.Length; j++)
            //    {
            //        //mov[j].codAcao;
            //    }
            //}


            List<List<Tema.DataHolderNotaCorretagem>> notas = dicArquivos["NotaCorretagem"] as List<List<Tema.DataHolderNotaCorretagem>>;  //cada List<Tema.DataHolderNotaCorretagem> é um arquivo com Detail e Header
            List<List<Tema.HeaderNotasCorretagem>> headersNotas = dicArquivos["HeaderNotaCorretagem"] as List<List<Tema.HeaderNotasCorretagem>>;  //cada List<Tema.HeaderNotasCorretagem> é um arquivo com os headers

            List<int> listaClientesInexistentes = new List<int>();
            for (int i = 0; i < headersNotas.Count; i++)
            {
                List<Tema.HeaderNotasCorretagem> headerNota = (List<Tema.HeaderNotasCorretagem>)headersNotas[i];

                foreach (Tema.HeaderNotasCorretagem header in headerNota)
                {
                    int idCliente = Convert.ToInt32(header.codCliente);

                    Cliente cliente = new Cliente();
                    cliente.Query.Select(cliente.Query.IdCliente);
                    cliente.Query.Where(cliente.Query.IdCliente.Equal(idCliente),
                        cliente.Query.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
                    cliente.Query.Load();

                    if (!cliente.IdCliente.HasValue)
                    {
                        if (!listaClientesInexistentes.Contains(idCliente))
                        {
                            listaClientesInexistentes.Add(idCliente);
                        }
                    }
                }
            }

            //OperacaoBolsaCollection operacaoBolsaCollectionDeletar = new OperacaoBolsaCollection();
            //operacaoBolsaCollectionDeletar.Query.Select(operacaoBolsaCollectionDeletar.Query.IdOperacao);
            //operacaoBolsaCollectionDeletar.Query.Where(operacaoBolsaCollectionDeletar.Query.Fonte.Equal((byte)FonteOperacaoBolsa.Tema));
            //operacaoBolsaCollectionDeletar.Query.Load();
            //operacaoBolsaCollectionDeletar.MarkAllAsDeleted();
            //operacaoBolsaCollectionDeletar.Save();

            //Busca o IdAgenteMercado default
            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
            AgenteMercado agenteMercado = new AgenteMercado();
            int idAgenteMercadoDefault = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

            bool integraAtivoNaoCadastrado = (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (int)IntegracaoBolsa.Sinacor ||
                                              ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (int)IntegracaoBolsa.Sinacor_BTC);

            OperacaoBolsaCollection listaOper = new OperacaoBolsaCollection();
            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();


            for (int i = 0; i < notas.Count; i++)
            {
                List<Tema.DataHolderNotaCorretagem> nota = (List<Tema.DataHolderNotaCorretagem>)notas[i];

                for (int j = 0; j < nota.Count; j++)
                {
                    //*** CAMPOS DO HEADER********************************************************************************
                    int idCliente = Convert.ToInt32(nota[j].header.codCliente);

                    if (listaClientesInexistentes.Contains(idCliente))
                    {
                        continue;
                    }

                    //
                    int numeroNota = 0;
                    try
                    {
                        string numOnly = string.Join(null, System.Text.RegularExpressions.Regex.Split(nota[j].header.numNota, "[^\\d]"));
                        numeroNota = Convert.ToInt32(numOnly);
                    }
                    catch (Exception)
                    {                        
                        
                    }
                    
                    //

                    DateTime dataOperacao = Convert.ToDateTime(nota[j].header.dataNota);
                    int idAgenteMercado = nota[j].header.idAgenteMercado;

                    if (idAgenteMercado == 0)
                    {
                        idAgenteMercado = idAgenteMercadoDefault;
                    }
                    //****************************************************************************************************

                    string tipoMercado = nota[j].detail.mercado;
                    string cdAtivoBolsa = nota[j].detail.codAcao;
                    string tipoOperacao = nota[j].detail.tipoOperacao;
                    decimal pu = nota[j].detail.preco;
                    decimal valor = nota[j].detail.valor;
                    decimal quantidade = nota[j].detail.quantidade;

                    int? numeroNegocio = null;
                    if (nota[j].detail.numeroNegocio != "")
                    {
                        string numOnly1 = string.Join(null, System.Text.RegularExpressions.Regex.Split(nota[j].detail.numeroNegocio, "[^\\d]"));
                        numeroNegocio = Convert.ToInt32(numOnly1);
                    }

                    if (tipoMercado != "TER")
                    {
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        ativoBolsa.Query.Select(ativoBolsa.Query.CdAtivoBolsa);
                        ativoBolsa.Query.Where(ativoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                        if (!ativoBolsa.Query.Load())
                        {
                            ativoBolsa = new AtivoBolsa();
                            if (integraAtivoNaoCadastrado)
                            {
                                ativoBolsa.IntegraAtivoSinacor(cdAtivoBolsa, dataOperacao);
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }

                    DateTime? dataVencimentoTermo = null;
                    byte origem = (byte)OrigemOrdemBolsa.Primaria;
                    string cdAtivoBolsaOpcao = null;
                    if (tipoMercado == "FRA" || tipoMercado == "LEI")
                    {
                        tipoMercado = TipoMercadoBolsa.MercadoVista;
                    }
                    else if (tipoMercado == "EOC" || tipoMercado == "EOV")
                    {
                        #region Trata exercicio de opção (EOC, EOV)
                        if (tipoMercado == "EOC")
                        {
                            origem = (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra;
                        }
                        else
                        {
                            origem = (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda;
                        }

                        //Precisa converter para A Vista, já que no arquivo vem como Opção
                        tipoMercado = TipoMercadoBolsa.MercadoVista;

                        //Busca o ativo objeto (ação) referente à opção operada                    
                        AtivoBolsa ativoBolsaOpcao = new AtivoBolsa();
                        string cdAtivoBolsaObjeto;

                        cdAtivoBolsaObjeto = ativoBolsaOpcao.RetornaCdAtivoBolsaObjeto(cdAtivoBolsa);
                        if (String.IsNullOrEmpty(cdAtivoBolsaObjeto))
                        {
                            throw new AtivoObjetoNaoCadastradoException("Ativo objeto não cadastrado para a opção " + cdAtivoBolsa);
                        }
                        else
                        {
                            //Guarda a opção em cdAtivoBolsaOpcao
                            cdAtivoBolsaOpcao = cdAtivoBolsa;
                            //O ativo bolsa passa a ser o ativo objeto da opção
                            cdAtivoBolsa = cdAtivoBolsaObjeto;
                        }
                        #endregion
                    }
                    else if (tipoMercado == "TER")
                    {
                        string cdAtivoBolsaObjeto = "";
                        try
                        {
                            dataVencimentoTermo = nota[j].detail.dtLiquidacaoTermo.Value;
                            if (!Calendario.IsDiaUtil(dataVencimentoTermo.Value, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
                            {
                                dataVencimentoTermo = Calendario.AdicionaDiaUtil(dataVencimentoTermo.Value, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                            }

                            cdAtivoBolsaObjeto = cdAtivoBolsa;

                            //Transforma o cdAtivoBolsa, incluindo a data do vencimento ao final                    
                            cdAtivoBolsa = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsa + "T", dataVencimentoTermo.Value);
                        }
                        catch (Exception)
                        {
                            throw new Exception("Nr de Nota " + nota[j].header.numNota + " com problemas no termo para o cliente " + idCliente.ToString());
                        }
                        

                        if (integraAtivoNaoCadastrado)
                        {
                            #region Se não existir o cdAtivoBolsa, cadastra em AtivoBolsa e em FatorCotacaoBolsa
                            AtivoBolsa ativoBolsaTermo = new AtivoBolsa();

                            if (!ativoBolsaTermo.LoadByPrimaryKey(cdAtivoBolsa))
                            {
                                //Busca dados do ativoObjeto em AtivoBolsa e em FatorCotacaoBolsa
                                AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                                if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsaObjeto))
                                {
                                    throw new AtivoNaoCadastradoException("Ativo " + cdAtivoBolsaObjeto + " não cadastrado");
                                }
                                string especificacao = ativoBolsaObjeto.Especificacao;
                                string descricao = ativoBolsaObjeto.Descricao;
                                string codigoIsin = ativoBolsaObjeto.CodigoIsin;

                                AtivoBolsa ativoBolsaTermoInsert = new AtivoBolsa();
                                ativoBolsaTermoInsert.CdAtivoBolsa = cdAtivoBolsa;
                                ativoBolsaTermoInsert.Especificacao = especificacao;
                                ativoBolsaTermoInsert.Descricao = descricao;
                                ativoBolsaTermoInsert.TipoMercado = TipoMercadoBolsa.Termo;
                                ativoBolsaTermoInsert.DataVencimento = dataVencimentoTermo;
                                ativoBolsaTermoInsert.CdAtivoBolsaObjeto = cdAtivoBolsaObjeto;
                                ativoBolsaTermoInsert.CodigoIsin = codigoIsin;
                                ativoBolsaTermoInsert.IdMoeda = (short)ListaMoedaFixo.Real;
                                ativoBolsaTermoInsert.IdEmissor = (int)ListaEmissorFixo.Padrao;
                                ativoBolsaTermoInsert.OpcaoIndice = 0; //default
                                ativoBolsaTermoInsert.NumeroSerie = 0;
                                ativoBolsaTermoInsert.TipoPapel = (byte)TipoPapelAtivo.Normal;

                                ativoBolsaTermoInsert.Save();

                                FatorCotacaoBolsaCollection fatorCotacaoCollection = new FatorCotacaoBolsaCollection();
                                fatorCotacaoCollection.BuscaFatorCotacaoAtivo(cdAtivoBolsaObjeto);

                                for (int x = 0; x < fatorCotacaoCollection.Count; x++)
                                {
                                    FatorCotacaoBolsa fatorCotacaoBolsa = fatorCotacaoCollection[x];

                                    FatorCotacaoBolsa fatorCotacaoBolsaInsert = new FatorCotacaoBolsa();
                                    fatorCotacaoBolsaInsert.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                    fatorCotacaoBolsaInsert.Fator = fatorCotacaoBolsa.Fator.Value;
                                    fatorCotacaoBolsaInsert.CdAtivoBolsa = cdAtivoBolsa;

                                    fatorCotacaoBolsaInsert.Save();
                                }

                            }
                            #endregion

                        }
                        else
                        {
                            continue;
                        }
                    }

                    byte fonte = (byte)FonteOperacaoBolsa.Tema;
                    Int16 idLocal = LocalFeriadoFixo.Bovespa;
                    int idMoeda = (int)ListaMoedaFixo.Real;

                    DateTime dataLiquidacao = this.GetDataLiquidacao(dataOperacao, tipoMercado, LocalFeriadoFixo.Bovespa);

                    OperacaoBolsa operacaoBolsa = new OperacaoBolsa();

                    #region InsereOperacao
                    operacaoBolsa.IdCliente = idCliente;
                    operacaoBolsa.IdAgenteCorretora = idAgenteMercado;
                    operacaoBolsa.IdAgenteLiquidacao = idAgenteMercado;

                    operacaoBolsa.TipoMercado = tipoMercado;
                    operacaoBolsa.TipoOperacao = tipoOperacao;
                    operacaoBolsa.Data = dataOperacao;
                    operacaoBolsa.Pu = pu;
                    operacaoBolsa.Valor = valor;
                    operacaoBolsa.ValorLiquido = 0;
                    operacaoBolsa.PULiquido = 0;

                    /* Taxas, a serem tratadas depois */
                    operacaoBolsa.Corretagem = 0;
                    operacaoBolsa.Emolumento = 0;
                    operacaoBolsa.LiquidacaoCBLC = 0;
                    operacaoBolsa.RegistroBolsa = 0;
                    operacaoBolsa.RegistroCBLC = 0;
                    operacaoBolsa.ValorISS = 0;
                    //
                    operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    operacaoBolsa.Quantidade = quantidade;
                    operacaoBolsa.Origem = origem;
                    operacaoBolsa.Fonte = fonte;
                    operacaoBolsa.DataLiquidacao = dataLiquidacao;
                    operacaoBolsa.IdLocal = idLocal;
                    operacaoBolsa.IdMoeda = idMoeda;
                    operacaoBolsa.CdAtivoBolsaOpcao = cdAtivoBolsaOpcao;

                    operacaoBolsa.PercentualDesconto = 0;
                    operacaoBolsa.Desconto = 0;
                    operacaoBolsa.NumeroNota = numeroNota;
                    operacaoBolsa.NumeroNegocio = numeroNegocio;
                    operacaoBolsa.CalculaDespesas = "N";

                    operacaoBolsa.Save();
                    #endregion

                    if (tipoMercado == "TER")
                    {
                        #region InsereOperacaoTermoBolsa
                        //Pego o Id usado na OperacaoBolsa
                        int idOperacao = operacaoBolsa.IdOperacao.Value;

                        OperacaoTermoBolsa operacaoTermoBolsa = new OperacaoTermoBolsa();
                        operacaoTermoBolsa.IdOperacao = idOperacao;
                        operacaoTermoBolsa.Taxa = 0;
                        operacaoTermoBolsa.DataVencimento = dataVencimentoTermo;

                        operacaoTermoBolsa.Save();
                        #endregion
                    }

                }
            }

            //listaOper.Save();

            #region Rateia despesas e taxas
            for (int i = 0; i < headersNotas.Count; i++)
            {
                List<Tema.HeaderNotasCorretagem> headerNota = (List<Tema.HeaderNotasCorretagem>)headersNotas[i];

                foreach (Tema.HeaderNotasCorretagem header in headerNota)
                {
                    int numeroNota = 0;
                    try
                    {
                        string numOnly = string.Join(null, System.Text.RegularExpressions.Regex.Split(header.numNota, "[^\\d]"));
                        numeroNota = Convert.ToInt32(numOnly);
                    }
                    catch (Exception)
                    {

                    }

                    DateTime data = Convert.ToDateTime(header.dataNota);
                    int idCliente = Convert.ToInt32(header.codCliente);

                    if (listaClientesInexistentes.Contains(idCliente))
                    {
                        continue;
                    }

                    int idAgenteMercado = header.idAgenteMercado == 0 ? idAgenteMercadoDefault : header.idAgenteMercado;

                    decimal corretagem = header.taxCorretagem;
                    decimal taxaCBLC = header.taxCBLCeCLC;
                    decimal taxaNegociacao = header.taxNegocicacao;
                    decimal taxaTermoOpcoes = header.taxTermoOpcao;

                    decimal valorISS = header.cobraISS == "S" ? header.valorISS : 0;

                    #region Rateia MercadoVista/Imobiliario
                    OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
                    operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.Fonte.Equal((byte)FonteOperacaoBolsa.Tema),
                                                        operacaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario),
                                                        operacaoBolsaCollection.Query.Data.Equal(data),
                                                        operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                        operacaoBolsaCollection.Query.IdAgenteCorretora.Equal(idAgenteMercado),
                                                        operacaoBolsaCollection.Query.NumeroNota.Equal(numeroNota));
                    operacaoBolsaCollection.Query.Load();

                    decimal totalValor = 0;
                    foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
                    {
                        totalValor += operacaoBolsa.Valor.Value;
                    }

                    this.RateiaCorretagem(operacaoBolsaCollection, corretagem, valorISS, totalValor, true);
                    this.RateiaTaxas(operacaoBolsaCollection, taxaNegociacao, 0, taxaCBLC, taxaTermoOpcoes, totalValor, true);
                    #endregion

                    #region Rateia OPC/OPV
                    operacaoBolsaCollection = new OperacaoBolsaCollection();
                    operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.Fonte.Equal((byte)FonteOperacaoBolsa.Tema),
                                                        operacaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                                                        operacaoBolsaCollection.Query.Data.Equal(data),
                                                        operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                        operacaoBolsaCollection.Query.IdAgenteCorretora.Equal(idAgenteMercado),
                                                        operacaoBolsaCollection.Query.NumeroNota.Equal(numeroNota));
                    operacaoBolsaCollection.Query.Load();

                    totalValor = 0;
                    foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
                    {
                        totalValor += operacaoBolsa.Valor.Value;
                    }

                    this.RateiaCorretagem(operacaoBolsaCollection, corretagem, valorISS, totalValor, true);
                    this.RateiaTaxas(operacaoBolsaCollection, taxaNegociacao, 0, taxaCBLC, taxaTermoOpcoes, totalValor, true);
                    #endregion

                    #region Rateia TER
                    operacaoBolsaCollection = new OperacaoBolsaCollection();
                    operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.Fonte.Equal((byte)FonteOperacaoBolsa.Tema),
                                                        operacaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.Termo),
                                                        operacaoBolsaCollection.Query.Data.Equal(data),
                                                        operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                        operacaoBolsaCollection.Query.IdAgenteCorretora.Equal(idAgenteMercado),
                                                        operacaoBolsaCollection.Query.NumeroNota.Equal(numeroNota));
                    operacaoBolsaCollection.Query.Load();

                    totalValor = 0;
                    foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
                    {
                        totalValor += operacaoBolsa.Valor.Value;
                    }

                    this.RateiaCorretagem(operacaoBolsaCollection, corretagem, valorISS, totalValor, true);
                    this.RateiaTaxas(operacaoBolsaCollection, taxaNegociacao, 0, taxaCBLC, taxaTermoOpcoes, totalValor, true);
                    #endregion
                }
            }
            #endregion

            //#region Lanca valores de IR Fonte Normal e Daytrade
            //for (int i = 0; i < headersNotas.Count; i++)
            //{
            //    List<Tema.HeaderNotasCorretagem> headerNota = (List<Tema.HeaderNotasCorretagem>)headersNotas[i];

            //    IRFonteCollection iRFonteCollection = new IRFonteCollection();
            //    foreach (Tema.HeaderNotasCorretagem header in headerNota)
            //    {
            //        DateTime data = Convert.ToDateTime(header.dataNota);
            //        int idCliente = Convert.ToInt32(header.codCliente);

            //        if (listaClientesInexistentes.Contains(idCliente))
            //        {
            //            continue;
            //        }

            //        int idAgenteMercado = header.idAgenteMercado == 0 ? idAgenteMercadoDefault : header.idAgenteMercado;

            //        decimal valorIRNormal = header.IRRFNormal;
            //        decimal valorIRDaytrade = header.IRRFDaytrade;

            //        if (valorIRNormal != 0)
            //        {
            //            IRFonte irFonte = iRFonteCollection.AddNew();
            //            irFonte.AddNew();
            //            irFonte.IdCliente = idCliente;
            //            irFonte.Data = data;
            //            irFonte.Identificador = (Int16)IdentificadorIR.IRFonteAcoes;
            //            irFonte.IdAgente = idAgenteMercado;
            //            irFonte.ValorBase = 0;
            //            irFonte.ValorIR = valorIRNormal;                        
            //        }

            //        if (valorIRDaytrade != 0)
            //        {
            //            IRFonte irFonte = iRFonteCollection.AddNew();
            //            irFonte.AddNew();
            //            irFonte.IdCliente = idCliente;
            //            irFonte.Data = data;
            //            irFonte.Identificador = (Int16)IdentificadorIR.IRFonteDaytrade;
            //            irFonte.IdAgente = idAgenteMercado;
            //            irFonte.ValorBase = 0;
            //            irFonte.ValorIR = valorIRDaytrade;                        
            //        }                    
            //    }
            //    iRFonteCollection.Save();

            //}
            //#endregion

        }
    }
}
