﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Util;

namespace Financial.Bolsa {
    public partial class BonificacaoBolsaCollection : esBonificacaoBolsaCollection 
    {        
        /// <summary>
        /// Carrega o objeto BonificacaoBolsaCollection com os campos IdBonificacao, CdAtivoBolsa, 
        /// CdAtivoBolsaDestino, DataReferencia, PUBonificacao, Percentual.
        /// </summary>        
        /// <param name="dataEx"></param>
        public void BuscaBonificacaoBolsa(DateTime dataEx) 
        {          
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdBonificacao, this.Query.CdAtivoBolsa, this.Query.CdAtivoBolsaDestino,
                         this.Query.DataReferencia, this.Query.PUBonificacao, this.Query.Percentual)
                 .Where(this.Query.DataEx.Equal(dataEx))
                 .OrderBy(this.Query.IdBonificacao.Ascending);

            this.Query.Load();         
        }

        /// <summary>
        /// Carrega o objeto ConversaoBolsaCollection com os campos IdConversao, CdAtivoBolsa, CdAtivoBolsaDestino,
        /// DataReferencia, FatorQuantidade, FatorPU.
        /// Para cada registro de ativo de ações, busca potenciais registros de opções ou termo, de acordo com os booleanos passados.
        /// </summary>        
        /// <param name="data"></param>
        public BonificacaoBolsaCollection BuscaBonificacaoBolsa(DateTime dataEx, int idCliente, bool buscaTermo)
        {
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            BonificacaoBolsaQuery bonificacaoBolsaQuery = new BonificacaoBolsaQuery("B");
            bonificacaoBolsaQuery.Select(bonificacaoBolsaQuery.IdBonificacao,
                                      bonificacaoBolsaQuery.CdAtivoBolsa,
                                      bonificacaoBolsaQuery.CdAtivoBolsaDestino,
                                      bonificacaoBolsaQuery.Percentual,
                                      bonificacaoBolsaQuery.PUBonificacao);
            bonificacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(bonificacaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);
            bonificacaoBolsaQuery.Where(bonificacaoBolsaQuery.DataEx.Equal(dataEx));
            bonificacaoBolsaQuery.Where(ativoBolsaQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
            bonificacaoBolsaQuery.OrderBy(bonificacaoBolsaQuery.IdBonificacao.Ascending);

            BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollection.Load(bonificacaoBolsaQuery);

            foreach (BonificacaoBolsa bonificacaoBolsa in bonificacaoBolsaCollection)
            {
                if (buscaTermo)
                {
                    AtivoBolsa ativoBolsaDestino = new AtivoBolsa();
                    ativoBolsaDestino.LoadByPrimaryKey(bonificacaoBolsa.CdAtivoBolsaDestino);

                    AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                    if (ativoBolsaCollection.BuscaAtivoBolsaTermo(bonificacaoBolsa.CdAtivoBolsa, dataEx))
                    {
                        for (int j = 0; j < ativoBolsaCollection.Count; j++)
                        {
                            string cdAtivoBolsa = ativoBolsaCollection[j].CdAtivoBolsa;
                            string cdAtivoBolsaDestino = bonificacaoBolsa.CdAtivoBolsaDestino + Utilitario.Right(cdAtivoBolsa, 9);

                            //Só adiciona se tiver realmente posição do termo na carteira
                            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                            posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.IdPosicao);
                            posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                     posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0),
                                                     posicaoTermoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                            posicaoTermoBolsaCollection.Query.Load();

                            if (posicaoTermoBolsaCollection.HasData)
                            {
                                #region Verifica se cdAtivoBolsaDestino existe, e cadastra se nao existir
                                AtivoBolsa ativoBolsa = new AtivoBolsa();
                                if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsaDestino))
                                {
                                    AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                                    ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                                    ativoBolsaNovo.CdAtivoBolsaObjeto = bonificacaoBolsa.CdAtivoBolsaDestino;
                                    ativoBolsaNovo.CodigoIsin = ativoBolsaDestino.CodigoIsin;
                                    ativoBolsaNovo.DataVencimento = ativoBolsaCollection[j].DataVencimento;
                                    ativoBolsaNovo.Descricao = ativoBolsaDestino.Descricao;
                                    ativoBolsaNovo.Especificacao = ativoBolsaDestino.Especificacao;
                                    ativoBolsaNovo.IdEmissor = ativoBolsaDestino.IdEmissor.Value;
                                    ativoBolsaNovo.IdMoeda = 1;
                                    ativoBolsaNovo.PUExercicio = ativoBolsaCollection[j].PUExercicio;
                                    ativoBolsaNovo.TipoMercado = ativoBolsaCollection[j].TipoMercado;
                                    ativoBolsaNovo.Save();

                                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                                    if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(ativoBolsaDestino.CdAtivoBolsa, dataEx))
                                    {
                                        FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                                        fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                                        fatorCotacaoBolsaNovo.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                        fatorCotacaoBolsaNovo.Fator = fatorCotacaoBolsa.Fator.Value;
                                        fatorCotacaoBolsaNovo.Save();
                                    }
                                }
                                #endregion

                                #region Adiciona a Conversao do ativo Termo achado
                                BonificacaoBolsa novaBonificacaoBolsa = new BonificacaoBolsa();
                                novaBonificacaoBolsa.DataLancamento = bonificacaoBolsa.DataLancamento;
                                novaBonificacaoBolsa.DataEx = bonificacaoBolsa.DataEx;
                                novaBonificacaoBolsa.DataReferencia = bonificacaoBolsa.DataReferencia;
                                novaBonificacaoBolsa.Percentual = bonificacaoBolsa.Percentual;
                                novaBonificacaoBolsa.PUBonificacao = bonificacaoBolsa.PUBonificacao;
                                novaBonificacaoBolsa.Fonte = bonificacaoBolsa.Fonte;
                                novaBonificacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                                novaBonificacaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;

                                bonificacaoBolsaCollection.AttachEntity(novaBonificacaoBolsa);
                                #endregion
                            }
                        }
                    }
                }
            }

            return bonificacaoBolsaCollection;

        }

        /// <summary>
        /// Carrega o objeto BonificacaoBolsaCollection com todos os campos de BonificacaoBolsa.
        /// </summary>        
        /// <param name="dataLancamento"></param>
        public void BuscaBonificacaoBolsaCompleta(DateTime dataLancamento) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.DataLancamento.Equal(dataLancamento));
            this.Query.Load();        
        }

        /// <summary>
        /// Carrega o objeto BonificacaoBolsaCollection com todos os campos de BonificacaoBolsa.
        /// </summary>        
        /// <param name="dataLancamento"></param>
        public void BuscaBonificacaoBolsaCompleta(DateTime dataLancamento, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.DataLancamento.Equal(dataLancamento),
                       this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as bonificações do dia, dada a fonte informada.
        /// </summary>
        /// <param name="dataLancamento"></param>        
        /// <param name="fonte"></param>        
        public void DeletaBonificacao(DateTime dataLancamento, int fonte)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdBonificacao)
                 .Where(this.Query.Fonte == fonte,
                        this.Query.DataLancamento.Equal(dataLancamento));
            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataEx"></param>        
        public bool BuscaBonificacaoTermoOpcao(DateTime dataEx)
        {
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            BonificacaoBolsaQuery bonificacaoBolsaQuery = new BonificacaoBolsaQuery("B");

            bonificacaoBolsaQuery.Select(bonificacaoBolsaQuery.IdBonificacao);
            bonificacaoBolsaQuery.Where(bonificacaoBolsaQuery.DataEx.Equal(dataEx));
            bonificacaoBolsaQuery.Where(ativoBolsaQuery.TipoMercado.NotEqual(TipoMercadoBolsa.MercadoVista));
            bonificacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(bonificacaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);

            BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollection.Load(bonificacaoBolsaQuery);

            return bonificacaoBolsaCollection.Count > 0;
        }
    }
}
