﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Collections;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Util;
using Financial.Common.Enums;

namespace Financial.Bolsa
{
	public partial class OperacaoBolsaCollection : esOperacaoBolsaCollection {

        /// <summary>
        /// Busca operações do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="idAgente"></param>
        /// <param name="origemOperacaoBolsa"></param>
        /// throws ArgumentException se Origem for diferente dos tipos do enum OrigemOperacaoBolsa        
        public void BuscaOperacaoBolsa(int idCliente, DateTime data, string cdAtivoBolsa, int idAgente, int origemOperacaoBolsa)
        {
            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("OrigemOperacaoBolsa incorreto. Os valores possiveis estão no enum OrigemOperacaoBolsa");

            List<int> valoresPossiveis = OrigemOperacaoBolsa.Values();
            if (!valoresPossiveis.Contains(origemOperacaoBolsa))
            {
                throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade, this.Query.TipoOperacao)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                        this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteLiquidacao == idAgente,
                        this.Query.Origem == origemOperacaoBolsa);
                  
            this.Query.Load();
        }

        /// <summary>
        /// Busca operações do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="idAgente"></param>
        /// <param name="origemOperacaoBolsa1"></param>
        /// <param name="origemOperacaoBolsa2"></param>        
        /// throws ArgumentException se Origem for diferente dos tipos do enum OrigemOperacaoBolsa        
        public void BuscaOperacaoBolsa(int idCliente, DateTime data, string cdAtivoBolsa, int idAgente,
                                       int origemOperacaoBolsa1, int origemOperacaoBolsa2) 
        {
            #region ArgumentosNulos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();
            mensagem.Append("OrigemOperacaoBolsa incorreto. Os valores possiveis estão no enum OrigemOperacaoBolsa");

            List<int> valoresPossiveis = OrigemOperacaoBolsa.Values();
            if (!valoresPossiveis.Contains(origemOperacaoBolsa1) ||
                !valoresPossiveis.Contains(origemOperacaoBolsa2)) {
                    throw new ArgumentException(mensagem.ToString());
            }
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade, this.Query.TipoOperacao)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                        this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteLiquidacao == idAgente,
                        this.Query.Or
                        ( 
                           this.Query.Origem == origemOperacaoBolsa1,
                           this.Query.Origem == origemOperacaoBolsa2
                        )
                  );
                        
            this.Query.Load();
        }

        /// <summary>
        /// Busca operações do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="cdAtivoBolsaObjeto"></param>
        /// <param name="idAgente"></param>
        public void BuscaOperacaoBolsaExercicio(int idCliente, DateTime data, string cdAtivoBolsa, 
                                                string cdAtivoBolsaObjeto, int idAgente)
        {           
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade, this.Query.TipoOperacao)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.CdAtivoBolsaOpcao.Equal(cdAtivoBolsa),
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsaObjeto),
                        this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteLiquidacao == idAgente,
                        this.Query.Origem.In((int)OrigemOperacaoBolsa.ExercicioOpcaoCompra, 
                                             (int)OrigemOperacaoBolsa.ExercicioOpcaoVenda));                        

            this.Query.Load();
        }

        /// <summary>
        /// Busca operações de exercicio do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBolsaExercicio(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsaOpcao, this.Query.IdAgenteLiquidacao, this.Query.TipoOperacao, this.Query.Quantidade.Sum())
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.Origem.In((int)OrigemOperacaoBolsa.ExercicioOpcaoCompra,
                                             (int)OrigemOperacaoBolsa.ExercicioOpcaoVenda))
                 .GroupBy(this.Query.CdAtivoBolsaOpcao, this.Query.IdAgenteLiquidacao, this.Query.TipoOperacao);

            this.Query.Load();
        }

        /// <summary>
        /// Busca operações do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsa(int idCliente, DateTime dataOperacao) 
        {        
            this.QueryReset();                 
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdAgenteCorretora, 
                        this.Query.Quantidade, this.Query.Data, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.TipoOperacao, 
                        this.Query.ResultadoRealizado, this.Query.TipoMercado, this.Query.Origem,
                        this.Query.QuantidadeCasadaExercicio, this.Query.QuantidadeCasadaTermo)
                 .Where(this.Query.Data.Equal(dataOperacao), 
                        this.Query.IdCliente == idCliente);

            this.Query.Load();
        }

        /// <summary>
        /// Busca operações do dia. Não traz operações de termo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaValores(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente,
                        this.Query.Quantidade, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.Corretagem,
                        this.Query.Emolumento, this.Query.RegistroBolsa, this.Query.RegistroCBLC, 
                        this.Query.LiquidacaoCBLC, this.Query.Valor, this.Query.TipoOperacao)
                 .Where(this.Query.Data.Equal(dataOperacao) &
                        this.Query.IdCliente == idCliente &
                        (   
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Compra |   
                            this.Query.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Venda |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.VendaDaytrade
                        )
                   );

            this.Query.Load();            
        }
         
        /// <summary>
        /// Busca operações de Compra/Deposito do dia. Não traz operações de termo, nem transferencia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaCompra(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdAgenteLiquidacao,
                        this.Query.Quantidade, this.Query.Data, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.TipoOperacao,
                        this.Query.ResultadoRealizado, this.Query.TipoMercado, this.Query.Origem,
                        this.Query.QuantidadeCasadaExercicio, this.Query.QuantidadeCasadaTermo)
                 .Where(
                        this.Query.Data.Equal(dataOperacao) &
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.VencimentoOpcao) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.TransferenciaCorretora) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.ResultadoConversao) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.ResultadoBonificacao) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.ResultadoGrupamento) &
                        this.Query.IdCliente == idCliente &
                        (
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Compra |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Deposito |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.IngressoAtivoImpactoCota |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.IngressoAtivoImpactoQtde

                        )
                 );
            this.Query.OrderBy(this.Query.IdOperacao.Ascending);     

            this.Query.Load();            
        }

        /// <summary>
        /// Busca operações de Venda/Retirada do dia. Não traz operações de termo, nem transferencia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaVenda(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdAgenteLiquidacao,
                        this.Query.Quantidade, this.Query.Data, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.TipoOperacao,
                        this.Query.ResultadoRealizado, this.Query.TipoMercado, this.Query.Origem,
                        this.Query.QuantidadeCasadaExercicio, this.Query.QuantidadeCasadaTermo)
                 .Where(
                        this.Query.Data.Equal(dataOperacao) &
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.VencimentoOpcao) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.TransferenciaCorretora) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.ResultadoConversao) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.ResultadoGrupamento) &
                        this.Query.Origem.NotEqual((int)OrigemOperacaoBolsa.ResultadoBonificacao) &
                        this.Query.IdCliente == idCliente &
                        (
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Venda |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.Retirada |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.RetiradaAtivoImpactoCota |
                            this.Query.TipoOperacao == TipoOperacaoBolsa.RetiradaAtivoImpactoQtde
                        )
                 );
            this.Query.OrderBy(this.Query.IdOperacao.Ascending);

            this.Query.Load();
        }

        /// <summary>
        /// Busca operações de Retirada por transferencia do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaTransferenciaRetirada(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdAgenteLiquidacao,
                        this.Query.Quantidade, this.Query.Data, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.TipoOperacao,
                        this.Query.ResultadoRealizado, this.Query.TipoMercado, this.Query.Origem,
                        this.Query.QuantidadeCasadaExercicio, this.Query.QuantidadeCasadaTermo)
                 .Where(
                        this.Query.Data.Equal(dataOperacao) &
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo) &
                        this.Query.Origem.Equal((int)OrigemOperacaoBolsa.TransferenciaCorretora) &
                        this.Query.IdCliente == idCliente &
                        this.Query.TipoOperacao == TipoOperacaoBolsa.Retirada                        
                 );

            this.Query.Load();
        }

        /// <summary>
        /// Busca operações de Deposito por transferencia do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaTransferenciaDeposito(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdAgenteLiquidacao,
                        this.Query.Quantidade, this.Query.Data, this.Query.PULiquido,
                        this.Query.ValorLiquido, this.Query.Pu, this.Query.TipoOperacao,
                        this.Query.ResultadoRealizado, this.Query.TipoMercado, this.Query.Origem,
                        this.Query.QuantidadeCasadaExercicio, this.Query.QuantidadeCasadaTermo)
                 .Where(
                        this.Query.Data.Equal(dataOperacao) &
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo) &
                        this.Query.Origem.Equal((int)OrigemOperacaoBolsa.TransferenciaCorretora) &
                        this.Query.IdCliente == idCliente &
                        this.Query.TipoOperacao == TipoOperacaoBolsa.Deposito
                 );

            this.Query.Load();
        }

        /// <summary>
        ///  Busca operações de Venda no Mercado a Vista do dia. Usada no casamento de vencimento Termo/Exercicio.   
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaVendaCasaVencimentoExercicio(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdAgenteCorretora,
                         this.Query.Quantidade, this.Query.QuantidadeCasadaExercicio,
                         this.Query.QuantidadeCasadaTermo, this.Query.Origem, this.Query.PULiquido,
                         this.Query.ResultadoExercicio, this.Query.ResultadoTermo)
                 .Where(
                         this.Query.Data.Equal(dataOperacao) &
                         this.Query.IdCliente == idCliente &

                         (
                             (this.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda) &
                              this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista))
                             |
                             (this.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Retirada) &
                              this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista) &
                              this.Query.Origem.Equal(OrigemOperacaoBolsa.LiquidacaoTermo))
                         )                         
                 )
                 .OrderBy(this.Query.IdAgenteCorretora.Ascending,
                          this.Query.CdAtivoBolsa.Ascending,
                          this.Query.Origem.Ascending,
                          this.Query.IdOperacao.Ascending);
           
            this.Query.Load();            
        }

        /// <summary>
        /// Busca operações de Compra no Mercado a Vista do dia. Usada no casamento de vencimento Termo/Exercicio.       
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="cdAtivo"></param>
        /// <returns>Indica se achou registro</returns>
        public bool BuscaOperacaoBolsaCompraCasaVencimentoExercicio(int idCliente, DateTime dataOperacao,
                                                                int idAgenteCorretora, string cdAtivo) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdAgenteCorretora,
                        this.Query.Quantidade, this.Query.QuantidadeCasadaExercicio,
                        this.Query.QuantidadeCasadaTermo, this.Query.Data, this.Query.Origem, 
                        this.Query.PULiquido, this.Query.ResultadoExercicio, this.Query.ResultadoTermo)
                 .Where(
                        this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao == TipoOperacaoBolsa.Compra,
                        this.Query.TipoMercado == TipoMercadoBolsa.MercadoVista,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.CdAtivoBolsa == cdAtivo
                 )
                 .OrderBy(this.Query.Origem.Ascending,
                          this.Query.IdOperacao.Ascending);


            bool retorno = this.Query.Load();            

            return retorno;            
        }

        /// <summary>
        /// Busca operações de Venda no Mercado a Vista do dia. Usada no casamento de vencimento Termo/Exercicio.            
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>        
        public void BuscaOperacaoBolsaVendaCasaVencimentoTermo(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdAgenteCorretora,
                         this.Query.Quantidade, this.Query.QuantidadeCasadaExercicio,
                         this.Query.QuantidadeCasadaTermo, this.Query.Origem, this.Query.PULiquido,
                         this.Query.ResultadoExercicio, this.Query.ResultadoTermo)
                 .Where(
                         this.Query.Data.Equal(dataOperacao) &
                         this.Query.IdCliente == idCliente &                         

                         (
                             (this.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda) &
                              this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista))
                             |
                             (this.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Retirada) &
                              this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista) &
                              this.Query.Origem.Equal(OrigemOperacaoBolsa.LiquidacaoTermo))
                         )                                  
                 )
                 .OrderBy(this.Query.IdAgenteCorretora.Ascending,
                          this.Query.CdAtivoBolsa.Ascending,
                          this.Query.Origem.Ascending,
                          this.Query.IdOperacao.Ascending);
            
            this.Query.Load();            
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <param name="idAgenteCorretora">identificador do agente corretora usada para Filtro</param>
        /// <param name="cdAtivo">codigo do Ativo usado para Filtro</param>
        /// <returns>True se foi achado algum registro
        ///          False caso contrario
        /// </returns>
        public bool BuscaOperacaoBolsaCompraCasaVencimentoTermo(int idCliente, DateTime dataOperacao,
                                                                int idAgenteCorretora, string cdAtivo) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, this.Query.IdAgenteCorretora,
                         this.Query.Quantidade, this.Query.QuantidadeCasadaExercicio,
                         this.Query.QuantidadeCasadaTermo, this.Query.Data, this.Query.Origem,
                         this.Query.PULiquido, this.Query.ResultadoExercicio, this.Query.ResultadoTermo)
                 .Where(
                         this.Query.Data.Equal(dataOperacao),
                         this.Query.IdCliente == idCliente,
                         this.Query.IdAgenteCorretora == idAgenteCorretora,
                         this.Query.CdAtivoBolsa == cdAtivo,
                         
                         this.Query.Or(
                                this.Query.And(
                                    this.Query.TipoOperacao == TipoOperacaoBolsa.Compra,
                                    this.Query.TipoMercado == TipoMercadoBolsa.MercadoVista
                                 ),
                                 this.Query.And(
                                    this.Query.TipoOperacao == TipoOperacaoBolsa.Deposito,
                                    this.Query.TipoMercado == TipoMercadoBolsa.MercadoVista,
                                    this.Query.Origem == OrigemOperacaoBolsa.LiquidacaoTermo
                             )
                         )
                 )
                 .OrderBy(this.Query.Origem.Ascending,
                          this.Query.IdOperacao.Ascending);
            
            bool retorno = this.Query.Load();
            
            return retorno;                               
        }

        /// <summary>
        ///    
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBolsaLancaOperacaoAnalitico(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdMoeda,
                        this.Query.IdAgenteLiquidacao, 
                        this.Query.TipoOperacao,
                        this.Query.DataLiquidacao, 
                        this.Query.TipoMercado,
                        this.Query.Origem, 
                        this.Query.CdAtivoBolsa,
                        this.Query.IdConta, 
                        this.Query.Valor.Sum()
                        )
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.NotIn(TipoMercadoBolsa.Termo, TipoMercadoBolsa.Futuro),
                        this.Query.TipoOperacao
                                   .In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade,                            
                                       TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade)
                   )
                   .GroupBy(this.Query.IdMoeda,
                            this.Query.IdAgenteLiquidacao,
                            this.Query.TipoOperacao,
                            this.Query.DataLiquidacao,
                            this.Query.TipoMercado,
                            this.Query.Origem,
                            this.Query.CdAtivoBolsa,
                            this.Query.IdConta
                   );

            this.Query.Load();            
        }

        /// <summary>
        /// Carrega o objeto OperacaoBolsaCollection com os campos IdAgenteLiquidacao, DataLiquidacao, TipoOperacao, IdMoeda
        /// TipoMercado, ValorLiquido.Sum(), Corretagem.Sum(), Emolumento.Sum(), RegistroBolsa.Sum(), RegistroCBLC.Sum(), LiquidacaoCBLC.Sum().
        /// Filtra por:
        ///         Data.Equal(dataOperacao),
        ///         IdCliente == idCliente,
        ///         TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade,                            
        ///                         TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade).
        ///         
        /// Group by: IdAgenteLiquidacao, DataLiquidacao, TipoOperacao, TipoMercado, IdMoeda.
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBolsaLancaOperacaoConsolidado(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdMoeda,
                        this.Query.IdAgenteLiquidacao,
                        this.Query.DataLiquidacao,
                        this.Query.TipoOperacao,
                        this.Query.TipoMercado,
                        this.Query.IdConta,
                        this.Query.ValorLiquido.Sum(),
                        this.Query.Corretagem.Sum(),
                        this.Query.Emolumento.Sum(),
                        this.Query.RegistroBolsa.Sum(),
                        this.Query.RegistroCBLC.Sum(),
                        this.Query.LiquidacaoCBLC.Sum()
                        )
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao
                                   .In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade,                            
                                       TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade)
                   )
                   .GroupBy(this.Query.IdMoeda, this.Query.IdAgenteLiquidacao, this.Query.DataLiquidacao,
                            this.Query.TipoOperacao, this.Query.TipoMercado, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        ///    
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns></returns>
        public void BuscaOperacaoBolsaLancaOperacaoDespesas(int idCliente, DateTime dataOperacao) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdMoeda,
                        this.Query.IdAgenteLiquidacao,
                        this.Query.DataLiquidacao,
                        this.Query.IdConta,
                        this.Query.Corretagem.Sum(),
                        this.Query.Emolumento.Sum(),
                        this.Query.LiquidacaoCBLC.Sum(),
                        this.Query.RegistroBolsa.Sum(),
                        this.Query.RegistroCBLC.Sum()                        
                 )
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao
                                   .In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade,
                                       TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade)
                   )
                   .GroupBy(this.Query.IdMoeda, this.Query.IdAgenteLiquidacao, this.Query.DataLiquidacao, this.Query.IdConta);

            this.Query.Load();
        }

        /// <summary>
        ///    
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data">Data da Operação usada para Filtro</param>
        /// <returns></returns>
        public void BuscaOperacaoBolsaIrFonteDaytrade(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor,
                        this.Query.TipoOperacao,
                        this.Query.Corretagem,
                        this.Query.Emolumento,
                        this.Query.LiquidacaoCBLC,
                        this.Query.RegistroBolsa,
                        this.Query.RegistroCBLC,
                        this.Query.IdAgenteCorretora
                 )
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade) 
                  )
                  .OrderBy(this.Query.IdAgenteCorretora.Ascending);

            this.Query.Load();            
        }
                      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBolsaIrFonte(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteCorretora)
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.Data.Equal(data) &
                            (
                                (
                                    this.Query.TipoOperacao == TipoOperacaoBolsa.Venda &
                                    this.Query.Origem == OrigemOperacaoBolsa.Primaria &
                                    this.Query.TipoMercado == TipoMercadoBolsa.MercadoVista
                                )
                                |
                                (
                                    this.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.Compra) &
                                    this.Query.Origem == OrigemOperacaoBolsa.Primaria &
                                    this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda)
                                )
                            )
                        ); 
                                                                                                
            this.Query.es.Distinct = true;
            this.Query.Load();
            
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBolsaIrFonte_FII(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteCorretora)
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.Data.Equal(data) &
                        this.Query.TipoOperacao == TipoOperacaoBolsa.Venda &
                        this.Query.Origem == OrigemOperacaoBolsa.Primaria &
                        this.Query.TipoMercado == TipoMercadoBolsa.Imobiliario
                        );

            this.Query.es.Distinct = true;
            this.Query.Load();

            this.Save();
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de OperacaoBolsaCollection.
        /// Filtra por: (TipoOperacao = "C" ou "V") + Origem = "Primaria" + TipoMercado = "Vista"
        ///                         ou
        ///             Origem = "ExercicioOpcaoVenda" + TipoMercado = "Vista"
        ///                         ou
        ///             TipoOperacao = "C" + Origem = "ExercicioOpcaoCompra" + TipoMercado = "Vista"
        /// Filtra por: CalculaDespesas.Equal("S") e TipoMercado.Equal(TipoMercadoBolsa.MercadoVista)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBolsaCalculaTaxa(int idCliente, int idAgenteCorretora, DateTime data) 
        {
            this.QueryReset();
            this.Query                 
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.Data.Equal(data) &
                        this.Query.CalculaDespesas.Equal("S") &
                        this.Query.IdAgenteCorretora == idAgenteCorretora &
                        this.Query.IdMoeda.NotEqual((int)ListaMoedaFixo.Dolar) &
                        (                       
                            (
                                this.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Venda) &
                                this.Query.Origem == OrigemOperacaoBolsa.Primaria &
                                this.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario)
                            ) 
                            
                            |
                                                    
                            ( this.Query.Origem == OrigemOperacaoBolsa.ExercicioOpcaoVenda & this.Query.TipoMercado == TipoMercadoBolsa.MercadoVista ) 
                            
                            |

                            (
                              this.Query.TipoOperacao == TipoOperacaoBolsa.Compra & this.Query.Origem == OrigemOperacaoBolsa.ExercicioOpcaoCompra &
                              this.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista)
                            )
                        )                        
                 );

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de OperacaoBolsaCollection.
        /// Filtra por: (TipoOperacao = "C" ou "V") + Origem = "Primaria" + TipoMercado = "Vista"
        ///                         ou
        ///             Origem = "ExercicioOpcaoVenda" + TipoMercado = "Vista"
        ///                         ou
        ///             TipoOperacao = "C" + Origem = "ExercicioOpcaoCompra" + TipoMercado = "Vista"
        /// Filtra por: CalculaDespesas.Equal("S") e TipoMercado.Equal(TipoMercadoBolsa.Imobiliario)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBolsaCalculaTaxaFII(int idCliente, int idAgenteCorretora, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.Data.Equal(data) &
                        this.Query.CalculaDespesas.Equal("S") &
                        this.Query.IdAgenteCorretora.Equal(idAgenteCorretora) &
                        this.Query.TipoMercado.Equal(TipoMercadoBolsa.Imobiliario)                        
                 );

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de OperacaoBolsaCollection.
        /// Filtra por: (TipoOperacao = "C" ou "V") + Origem = "Primaria" + TipoMercado = "Vista"
        ///                         ou
        ///             Origem = "ExercicioOpcaoVenda" + TipoMercado = "Vista"
        ///                         ou
        ///             TipoOperacao = "C" + Origem = "ExercicioOpcaoCompra" + TipoMercado = "Vista"
        /// Filtra por: CalculaDespesas.Equal("S")
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBolsaCalculaTaxaADR(int idCliente, int idAgenteCorretora, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Equal(data),
                        this.Query.CalculaDespesas.Equal("S"),
                        this.Query.IdAgenteCorretora.Equal(idAgenteCorretora),
                        this.Query.IdMoeda.Equal((int)ListaMoedaFixo.Dolar),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade)
                 );

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de OperacaoBolsaCollection.
        /// Filtra por: CalculaDespesas.Equal("S")
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoOperacao">
        /// Se for passado null desconsidera esse parametro
        /// tipoOperacao aceita duas ou mais comparações: 
        /// Usar: TipoOperacaoBolsa1','TipoOperacaoBolsa2
        /// </param>
        /// <param name="tipoMercado">
        /// Se for passado null desconsidera esse parametro
        /// TipoMercado aceita duas comparações: 
        /// Usar: TipoMercadoBolsa1','TipoMercadoBolsa2 </param>
        /// <param name="origemOperacao">
        /// Se for passado null desconsidera esse parametro
        /// origemOperacao aceita duas comparações:
        /// Usar: OrigemOperacaoBolsa1,OrigemOperacaoBolsa2
        /// OBS: Como OrigemOperacaoBolsa é do tipo inteiro as aspas simples não devem ser passadas
        /// </param>
        /// <param name="idAgenteCorretora"></param>
        /// 
        /// <returns></returns>
        /// throws ArgumentException se 
        /// tipoOperacao for diferente dos tipos do enum TipoOperacaoBolsa
        /// tipoMercado for diferente dos tipos do enum TipoMercadoBolsa
        /// origemOperacao for diferente dos tipos do enum OrigemOperacaoBolsa
        public void BuscaOperacaoBolsaCalculaTaxa(int idCliente, DateTime data, string tipoOperacao, string tipoMercado,
                                                  string origemOperacao, int idAgenteCorretora) 
        {        
            #region ArgumentosIncorretos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();

            #region Confere TipoOperacao
            if(tipoOperacao != null) {
                if (tipoOperacao.Contains(",")) {
                    char[] caracter = new char[] { ',' };
                    StringTokenizer token = new StringTokenizer(tipoOperacao, caracter);
                    foreach (string item in token) {
                        List<string> valoresPossiveis1 = TipoOperacaoBolsa.Values();
                        // remove as aspas simples
                        string item1 = item.Replace("'", "");
                        if (!valoresPossiveis1.Contains(item1.Trim())) {
                            mensagem.Append("tipoOperacao incorreto. Os valores possiveis estão no enum TipoOperacaoBolsa");
                            throw new ArgumentException(mensagem.ToString());
                        }
                    }
                }
                else {
                    List<string> valoresPossiveis1 = TipoOperacaoBolsa.Values();
                    if (!valoresPossiveis1.Contains(tipoOperacao)) {
                        mensagem.Append("tipoOperacao incorreto. Os valores possiveis estão no enum TipoOperacaoBolsa");
                        throw new ArgumentException(mensagem.ToString());
                    }
                }
            }
            #endregion

            #region Confere TipoMercado
            if (tipoMercado != null) {
                if (tipoMercado.Contains(",")) {
                    char[] caracter = new char[] { ',' };
                    StringTokenizer token = new StringTokenizer(tipoMercado, caracter);
                    foreach (string item in token) {
                        List<string> valoresPossiveis2 = TipoMercadoBolsa.Values();
                        // remove as aspas simples
                        string item2 = item.Replace("'", "");
                        if (!valoresPossiveis2.Contains(item2.Trim())) {
                            mensagem.Append("tipoMercado incorreto. Os valores possiveis estão no enum TipoMercadoBolsa");
                            throw new ArgumentException(mensagem.ToString());
                        }
                    }
                }
                else {
                    List<string> valoresPossiveis2 = TipoMercadoBolsa.Values();
                    if (!valoresPossiveis2.Contains(tipoMercado)) {
                        mensagem.Append("tipoMercado incorreto. Os valores possiveis estão no enum TipoMercadoBolsa");
                        throw new ArgumentException(mensagem.ToString());
                    }
                }
            }
            #endregion

            #region Confere OrigemOperacao
            if (origemOperacao != null) {
                if (origemOperacao.Contains(",")) {
                    char[] caracter = new char[] { ',' };
                    StringTokenizer token = new StringTokenizer(origemOperacao, caracter);
                    foreach (string item in token) {
                        List<int> valoresPossiveis3 = OrigemOperacaoBolsa.Values();
                        if (!valoresPossiveis3.Contains(Convert.ToInt32(item))) {
                            mensagem.Append("origemOperacao incorreto. Os valores possiveis estão no enum OrigemOperacaoBolsa");
                            throw new ArgumentException(mensagem.ToString());
                        }
                    }
                }
                else {
                    List<int> valoresPossiveis3 = OrigemOperacaoBolsa.Values();
                    if (!valoresPossiveis3.Contains(Convert.ToInt32(origemOperacao))) {
                        mensagem.Append("origemOperacao incorreto. Os valores possiveis estão no enum OrigemOperacaoBolsa");
                        throw new ArgumentException(mensagem.ToString());
                    }
                }
            }
            #endregion                                                                         
                     
            #endregion

            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.Data.Equal(data),
                        this.Query.IdMoeda.NotEqual((int)ListaMoedaFixo.Dolar),
                        this.Query.CalculaDespesas.Equal("S"));
            
            // Adicionando filtros
            if(tipoOperacao != null) {            
                this.Query.Where( this.Query.TipoOperacao.In(tipoOperacao) );
            }                                                
            if(tipoMercado != null) {
                this.Query.Where(this.Query.TipoMercado.In(tipoMercado));
            }                        
            if(origemOperacao != null) {
                this.Query.Where(this.Query.Origem.In(origemOperacao));
            }                        

            //
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBolsaCollection com o campo IdAgenteCorretora.     
        /// Filtra por: 
        ///    TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Venda, 
        ///                 TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade)
        ///    CalculaDespesas.Equal("S")
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data">Data da Operação usada para Filtro</param>
        /// <returns></returns>
        public void BuscaOperacaoBolsaCorretorasOperadas(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteCorretora)
                 .Where(
                        this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.Fonte.NotEqual((int)FonteOperacaoBolsa.Gerencial),
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Venda, 
                                            TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade),
                        this.Query.CalculaDespesas.Equal("S")
                 );                 
            this.Query.es.Distinct = true;

            this.Query.Load();
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data">Data da Operação usada para Filtro</param>
        /// <returns></returns>
        public void BuscaOperacaoBolsaOrdenadoCorretoraTipoMercado(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteCorretora, this.Query.TipoMercado, this.Query.Valor)
                 .Where(
                        this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente)
                .OrderBy(this.Query.IdAgenteCorretora.Ascending, this.Query.TipoMercado.Ascending);
                            
            this.Query.Load();            
        }

        /// <summary>
        /// Carrega o objeto OperacaoBolsaCollection com os campos IdAgenteCorretora, IdOperacao, Valor, Corretagem, PercentualDesconto, TipoMercado.
        /// Filtra por: this.Query.CalculaDespesas.Equal("S")
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoMercado">TipoMercado aceita duas comparações: 
        ///                           Usar: TipoMercadoBolsa1','TipoMercadoBolsa2 </param>
        /// <param name="tipoOperacao">TipoOperacao aceita duas comparações: 
        ///                           Usar: TipoOperacaoBolsa1','TipoOperacaoBolsa </param>        
        /// <param name="idAgente"></param>
        /// <returns> </returns>
        /// throws ArgumentException se 
        /// tipoMercado for diferente dos tipos do enum TipoMercadoBolsa
        /// tipoOperacao for diferente dos tipos do enum TipoOperacaoBolsa
        public void BuscaOperacaoBolsa(int idCliente, DateTime data, string tipoMercado, string tipoOperacao, int idAgente)
        {
            #region ArgumentosIncorretos - throw ArgumentException
            StringBuilder mensagem = new StringBuilder();

            if (tipoMercado.Contains(",")) {
                char[] caracter = new char[] { ',' };
                StringTokenizer token = new StringTokenizer(tipoMercado, caracter);
                foreach (string item in token) {
                    List<string> valoresPossiveis2 = TipoMercadoBolsa.Values();
                    // remove as aspas simples
                    string item1 = item.Replace("'", "");
                    if (!valoresPossiveis2.Contains(item1.Trim())) {
                        mensagem.Append("tipoMercado incorreto. Os valores possiveis estão no enum TipoMercadoBolsa");
                        throw new ArgumentException(mensagem.ToString());
                    }
                }
            }
            else {
                List<string> valoresPossiveis3 = TipoMercadoBolsa.Values();
                if (!valoresPossiveis3.Contains(tipoMercado)) {
                    mensagem.Append("tipoMercado incorreto. Os valores possiveis estão no enum TipoMercadoBolsa");
                    throw new ArgumentException(mensagem.ToString());
                }
            }

            mensagem = new StringBuilder();

            if (tipoOperacao.Contains(",")) {
                char[] caracter = new char[] { ',' };
                StringTokenizer token = new StringTokenizer(tipoOperacao, caracter);
                foreach (string item in token) {
                    List<string> valoresPossiveis = TipoOperacaoBolsa.Values();
                    // remove as aspas simples
                    string item1 = item.Replace("'", "");
                    if (!valoresPossiveis.Contains(item1.Trim())) {
                        mensagem.Append("tipoOperacao incorreto. Os valores possiveis estão no enum TipoOperacaoBolsa");
                        throw new ArgumentException(mensagem.ToString());
                    }
                }
            }
            else {
                List<string> valoresPossiveis2 = TipoOperacaoBolsa.Values();
                if (!valoresPossiveis2.Contains(tipoOperacao)) {
                    mensagem.Append("tipoOperacao incorreto. Os valores possiveis estão no enum TipoOperacaoBolsa");
                    throw new ArgumentException(mensagem.ToString());
                }
            }


            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteCorretora, this.Query.IdOperacao, this.Query.Valor,
                         this.Query.Corretagem, this.Query.PercentualDesconto, this.Query.TipoMercado, this.Query.ValorISS)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Data.Equal(data),
                        this.Query.TipoMercado.In(tipoMercado),
                        this.Query.TipoOperacao.In(tipoOperacao),
                        this.Query.IdAgenteCorretora == idAgente,
                        this.Query.CalculaDespesas.Equal("S"));

            this.Query.Load();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="origem"></param>
        /// throws ArgumentException se Origem for diferente dos tipos do enum OrigemOperacaoBolsa
        public void DeletaOperacaoBolsa(int idCliente, List<int> origem, DateTime data) 
        {
            #region ArgumentosNulos - throw ArgumentException
            /*StringBuilder mensagem = new StringBuilder();
            mensagem.Append("OrigemOperacaoBolsa incorreto. Os valores possiveis estão no enum OrigemOperacaoBolsa");

            List<int> valoresPossiveis = OrigemOperacaoBolsa.Values();
            if (!valoresPossiveis.Find(origem)) {
                throw new ArgumentException(mensagem.ToString());
            }
             */
            #endregion

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Origem.In(origem),
                        this.Query.Data.Equal(data));
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Busca operações para o caso de uso CompoeCustoOpcao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoBolsaCompoeCustoOpcao(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.Origem.In(OrigemOperacaoBolsa.ExercicioOpcaoCompra, OrigemOperacaoBolsa.ExercicioOpcaoVenda)
                  );

            this.Query.Load();
        }

        /// <summary>
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void BuscaOperacaoTermo(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao, this.Query.IdCliente, this.Query.CdAtivoBolsa, 
                            this.Query.IdAgenteLiquidacao, this.Query.IdTrader,
                            this.Query.PULiquido, this.Query.Valor, this.Query.ValorLiquido, 
                            this.Query.Pu, this.Query.TipoOperacao, this.Query.Quantidade)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado == TipoMercadoBolsa.Termo,
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Venda));

            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as operações em OperacaoBolsa (e operacaoTermoBolsa) com Fonte != (Interno, Manual).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void DeletaOperacaoOrdemCasada(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Fonte.NotIn((int)FonteOperacaoBolsa.Interno, (int)FonteOperacaoBolsa.Manual, (int)FonteOperacaoBolsa.Gerencial),
                        this.Query.Data.Equal(data));
            this.Query.Load();

            // Deleta as OperacoesTermoBolsa sem carregá-las para a memoria
            OperacaoTermoBolsaCollection operacaoTermoBolsaCollection = new OperacaoTermoBolsaCollection();

            for (int i = 0; i < this.Count; i++)
            {
                int idOperacao = this[i].IdOperacao.Value;

                //Antes de preparar para a deleção da operacaoTermoBolsa, checa se existe de fato na tabela
                OperacaoTermoBolsa operacaoTermoBolsaConfere = new OperacaoTermoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(operacaoTermoBolsaConfere.Query.IdOperacao);
                if (operacaoTermoBolsaConfere.LoadByPrimaryKey(campos, idOperacao))
                {
                    OperacaoTermoBolsa operacaoTermoBolsa = operacaoTermoBolsaCollection.AddNew();
                    operacaoTermoBolsa.IdOperacao = idOperacao;
                    operacaoTermoBolsa.AcceptChanges();
                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {
                if (operacaoTermoBolsaCollection.HasData)
                {
                    operacaoTermoBolsaCollection.MarkAllAsDeleted();
                    operacaoTermoBolsaCollection.Save(); // Apaga as OperacoesTermoBolsa                
                }

                if (this.HasData)
                {
                    this.MarkAllAsDeleted();
                    this.Save(); // Apaga OperacoesBolsa                
                }
                scope.Complete();
            }
        }

        /// <summary>
        /// Deleta todas as operações em OperacaoBolsa (e operacaoTermoBolsa) com Fonte != (Interno, Manual, Sinacor)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void DeletaOperacaoOrdemCasadaNaoIntegrada(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOperacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Fonte.NotIn((int)FonteOperacaoBolsa.Interno, (int)FonteOperacaoBolsa.Manual, (int)FonteOperacaoBolsa.Sinacor, (int)FonteOperacaoBolsa.Gerencial),
                        this.Query.Data.Equal(data));
            this.Query.Load();

            // Deleta as OperacoesTermoBolsa sem carregá-las para a memoria
            OperacaoTermoBolsaCollection operacaoTermoBolsaCollection = new OperacaoTermoBolsaCollection();

            for (int i = 0; i < this.Count; i++)
            {
                int idOperacao = this[i].IdOperacao.Value;

                //Antes de preparar para a deleção da operacaoTermoBolsa, checa se existe de fato na tabela
                OperacaoTermoBolsa operacaoTermoBolsaConfere = new OperacaoTermoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(operacaoTermoBolsaConfere.Query.IdOperacao);
                if (operacaoTermoBolsaConfere.LoadByPrimaryKey(campos, idOperacao))
                {
                    OperacaoTermoBolsa operacaoTermoBolsa = operacaoTermoBolsaCollection.AddNew();
                    operacaoTermoBolsa.IdOperacao = idOperacao;
                    operacaoTermoBolsa.AcceptChanges();
                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {
                if (operacaoTermoBolsaCollection.HasData)
                {
                    operacaoTermoBolsaCollection.MarkAllAsDeleted();
                    operacaoTermoBolsaCollection.Save(); // Apaga as OperacoesTermoBolsa                
                }

                if (this.HasData)
                {
                    this.MarkAllAsDeleted();
                    this.Save(); // Apaga OperacoesBolsa                
                }
                scope.Complete();
            }
        }

        /// <summary>
        /// Deleta o resultado da query:
        /// TipoOperacao.In(TipoOperacaoBolsa.Deposito, TipoOperacaoBolsa.Retirada)
        ///  & this.Query.Origem.Equal((int)OrigemOperacaoBolsa.LiquidacaoEmprestimo)
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="data"></param>        
        public void DeletaOperacaoLiquidacaoEmprestimo(int idCliente, DateTime data)
        {
            this.Query.Select(this.Query.IdOperacao);
            this.Query.Where(this.Query.IdCliente.Equal(idCliente),
                             this.query.Data.Equal(data),
                             this.Query.TipoOperacao.In(TipoOperacaoBolsa.Deposito, TipoOperacaoBolsa.Retirada),
                             this.Query.Origem.Equal((int)OrigemOperacaoBolsa.LiquidacaoEmprestimo));
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta o resultado da query:
        /// TipoOperacao.In(TipoOperacaoBolsa.Deposito, TipoOperacaoBolsa.Retirada)
        ///  & this.Query.Origem.Equal((int)OrigemOperacaoBolsa.LiquidacaoTermo)
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="data"></param>        
        public void DeletaOperacaoLiquidacaoTermo(int idCliente, DateTime data)
        {
            this.Query.Select(this.Query.IdOperacao);
            this.Query.Where(this.Query.IdCliente.Equal(idCliente),
                             this.query.Data.Equal(data),   
                             this.Query.TipoOperacao.In(TipoOperacaoBolsa.Deposito, TipoOperacaoBolsa.Retirada),
                             this.Query.Origem.Equal((int)OrigemOperacaoBolsa.LiquidacaoTermo));
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Zera as quantidades casadas e resultados de termo e exercicio.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="data"></param>        
        public void ZeraValoresCasamamentoTermoExecicio(int idCliente, DateTime data)
        {
            this.Query.Select(this.Query.IdOperacao, this.Query.QuantidadeCasadaExercicio, this.Query.QuantidadeCasadaTermo,
                              this.Query.ResultadoExercicio, this.Query.ResultadoTermo);
            this.Query.Where(this.Query.IdCliente.Equal(idCliente),
                             this.Query.Data.Equal(data));
            this.Query.Load();

            foreach (OperacaoBolsa operacaoBolsa in this)
            {
                operacaoBolsa.QuantidadeCasadaExercicio = 0;
                operacaoBolsa.QuantidadeCasadaTermo = 0;
                operacaoBolsa.ResultadoExercicio = 0;
                operacaoBolsa.ResultadoTermo = 0;
            }
            
            this.Save();
        }

        /// <summary>
        /// Busca operações de Compra/Deposito do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>                
        public void BuscaOperacaoBolsaCompraAgrupado(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa, this.Query.IdAgenteLiquidacao, this.Query.Quantidade.Sum())
                 .Where(
                        this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Deposito))
                 .GroupBy(this.Query.IdAgenteLiquidacao, this.Query.CdAtivoBolsa)
                 .OrderBy(this.Query.IdAgenteLiquidacao.Ascending, this.Query.CdAtivoBolsa.Ascending);

            this.Query.Load();
        }

        /// <summary>
        /// Busca operações de Venda/Retirada do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>                
        public void BuscaOperacaoBolsaVendaAgrupado(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa, this.Query.IdAgenteLiquidacao, this.Query.Quantidade.Sum())
                 .Where(
                        this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.Retirada))
                 .GroupBy(this.Query.IdAgenteLiquidacao, this.Query.CdAtivoBolsa)
                 .OrderBy(this.Query.IdAgenteLiquidacao.Ascending, this.Query.CdAtivoBolsa.Ascending);

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto OperacaoBolsaCollection com o campo IdAgenteCorretora.
        /// Filtra por TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade).
        /// Busca por Query.es.Distinct = true.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data">Data da Operação usada para Filtro</param>
        /// <returns></returns>
        public void BuscaCorretorasOperadasDayTrade(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdAgenteCorretora)
                 .Where(
                        this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade)                        
                 );
            this.Query.es.Distinct = true;

            this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaOperacaoBolsa(DateTime data, FonteOrdemBolsa fonte)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdOperacao)
              .Where(this.Query.Data.Equal(data),
                     this.Query.Fonte == fonte);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

	}
}
