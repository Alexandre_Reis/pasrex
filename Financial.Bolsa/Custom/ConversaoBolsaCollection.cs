﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Util;

namespace Financial.Bolsa {
    public partial class ConversaoBolsaCollection : esConversaoBolsaCollection 
    {        
        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Carrega o objeto ConversaoBolsaCollection com os campos IdConversao, CdAtivoBolsa, CdAtivoBolsaDestino,
        /// DataReferencia, FatorQuantidade, FatorPU.
        /// </summary>        
        /// <param name="data"></param>
        public void BuscaConversaoBolsa(DateTime dataEx) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdConversao, this.Query.CdAtivoBolsa, this.Query.CdAtivoBolsaDestino,
                         this.Query.DataReferencia, this.Query.FatorQuantidade, this.Query.FatorPU)
                 .Where(this.Query.DataEx.Equal(dataEx))
                 .OrderBy(this.Query.IdConversao.Ascending);
                 
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto ConversaoBolsaCollection com os campos IdConversao, CdAtivoBolsa, CdAtivoBolsaDestino,
        /// DataReferencia, FatorQuantidade, FatorPU.
        /// Para cada registro de ativo de ações, busca potenciais registros de opções ou termo, de acordo com os booleanos passados.
        /// </summary>        
        /// <param name="data"></param>
        public ConversaoBolsaCollection BuscaConversaoBolsa(DateTime dataEx, int idCliente, bool buscaOpcao, bool buscaTermo)
        {
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            ConversaoBolsaQuery conversaoBolsaQuery = new ConversaoBolsaQuery("C");
            conversaoBolsaQuery.Select(conversaoBolsaQuery.IdConversao,
                                      conversaoBolsaQuery.CdAtivoBolsa,
                                      conversaoBolsaQuery.CdAtivoBolsaDestino,
                                      conversaoBolsaQuery.FatorQuantidade,
                                      conversaoBolsaQuery.FatorPU);            
            conversaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(conversaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);
            conversaoBolsaQuery.Where(conversaoBolsaQuery.DataEx.Equal(dataEx));
            conversaoBolsaQuery.Where(ativoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.MercadoVista,
                                                                     TipoMercadoBolsa.Imobiliario));
            conversaoBolsaQuery.OrderBy(conversaoBolsaQuery.IdConversao.Descending);

            ConversaoBolsaCollection conversaoBolsaCollection = new ConversaoBolsaCollection();
            conversaoBolsaCollection.Load(conversaoBolsaQuery);

            foreach (ConversaoBolsa conversaoBolsa in conversaoBolsaCollection)
            {
                if (buscaOpcao)
                {
                    AtivoBolsa ativoBolsaDestino = new AtivoBolsa();
                    ativoBolsaDestino.LoadByPrimaryKey(conversaoBolsa.CdAtivoBolsaDestino);
                    
                    AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                    if (ativoBolsaCollection.BuscaAtivoBolsaOpcao(conversaoBolsa.CdAtivoBolsa, dataEx))
                    {
                        for (int j = 0; j < ativoBolsaCollection.Count; j++)
                        {
                            string cdAtivoBolsa = ativoBolsaCollection[j].CdAtivoBolsa;
                            string cdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino.Substring(0, 4) + Utilitario.Right(cdAtivoBolsa, 3);

                            //Só adiciona se tiver realmente posição da opção na carteira
                            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                            posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.IdPosicao);
                            posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                     posicaoBolsaCollection.Query.Quantidade.NotEqual(0),
                                                     posicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                            posicaoBolsaCollection.Query.Load();

                            if (posicaoBolsaCollection.HasData)
                            {
                                #region Verifica se cdAtivoBolsaDestino existe, e cadastra se nao existir
                                AtivoBolsa ativoBolsa = new AtivoBolsa();
                                if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsaDestino))
                                {
                                    AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                                    ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                                    ativoBolsaNovo.CdAtivoBolsaObjeto = conversaoBolsa.CdAtivoBolsaDestino;
                                    ativoBolsaNovo.CodigoIsin = ativoBolsaDestino.CodigoIsin;
                                    ativoBolsaNovo.DataVencimento = ativoBolsaCollection[j].DataVencimento;
                                    ativoBolsaNovo.Descricao = ativoBolsaDestino.Descricao;
                                    ativoBolsaNovo.Especificacao = ativoBolsaDestino.Especificacao;
                                    ativoBolsaNovo.IdEmissor = ativoBolsaDestino.IdEmissor.Value;
                                    ativoBolsaNovo.IdMoeda = 1;
                                    ativoBolsaNovo.PUExercicio = ativoBolsaCollection[j].PUExercicio;
                                    ativoBolsaNovo.TipoMercado = ativoBolsaCollection[j].TipoMercado;
                                    ativoBolsaNovo.TipoPapel = (byte)TipoPapelAtivo.Normal;
                                    ativoBolsaNovo.Save();

                                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                                    if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(ativoBolsaDestino.CdAtivoBolsa, dataEx))
                                    {
                                        FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                                        fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                                        fatorCotacaoBolsaNovo.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                        fatorCotacaoBolsaNovo.Fator = fatorCotacaoBolsa.Fator.Value;
                                        fatorCotacaoBolsaNovo.Save();
                                    }
                                }
                                #endregion

                                #region Adiciona a Conversao do ativo Opção achado
                                ConversaoBolsa novaConversaoBolsa = new ConversaoBolsa();
                                novaConversaoBolsa.DataLancamento = conversaoBolsa.DataLancamento;
                                novaConversaoBolsa.DataEx = conversaoBolsa.DataEx;
                                novaConversaoBolsa.DataReferencia = conversaoBolsa.DataReferencia;
                                novaConversaoBolsa.FatorQuantidade = conversaoBolsa.FatorQuantidade;
                                novaConversaoBolsa.FatorPU = conversaoBolsa.FatorPU;
                                novaConversaoBolsa.Fonte = conversaoBolsa.Fonte;
                                novaConversaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                                novaConversaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;

                                conversaoBolsaCollection.AttachEntity(novaConversaoBolsa);
                                #endregion
                            }
                        }
                    }
                }

                if (buscaTermo)
                {
                    AtivoBolsa ativoBolsaDestino = new AtivoBolsa();
                    ativoBolsaDestino.LoadByPrimaryKey(conversaoBolsa.CdAtivoBolsaDestino);

                    AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                    if (ativoBolsaCollection.BuscaAtivoBolsaTermo(conversaoBolsa.CdAtivoBolsa, dataEx))
                    {
                        for (int j = 0; j < ativoBolsaCollection.Count; j++)
                        {
                            string cdAtivoBolsa = ativoBolsaCollection[j].CdAtivoBolsa;
                            string cdAtivoBolsaDestino = conversaoBolsa.CdAtivoBolsaDestino + Utilitario.Right(cdAtivoBolsa, 9);

                            //Só adiciona se tiver realmente posição do termo na carteira
                            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                            posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.IdPosicao);
                            posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                     posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0),
                                                     posicaoTermoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                            posicaoTermoBolsaCollection.Query.Load();

                            if (posicaoTermoBolsaCollection.HasData)
                            {
                                #region Verifica se cdAtivoBolsaDestino existe, e cadastra se nao existir
                                AtivoBolsa ativoBolsa = new AtivoBolsa();
                                if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsaDestino))
                                {
                                    AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                                    ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                                    ativoBolsaNovo.CdAtivoBolsaObjeto = conversaoBolsa.CdAtivoBolsaDestino;
                                    ativoBolsaNovo.CodigoIsin = ativoBolsaDestino.CodigoIsin;
                                    ativoBolsaNovo.DataVencimento = ativoBolsaCollection[j].DataVencimento;
                                    ativoBolsaNovo.Descricao = ativoBolsaDestino.Descricao;
                                    ativoBolsaNovo.Especificacao = ativoBolsaDestino.Especificacao;
                                    ativoBolsaNovo.IdEmissor = ativoBolsaDestino.IdEmissor.Value;
                                    ativoBolsaNovo.IdMoeda = 1;
                                    ativoBolsaNovo.PUExercicio = ativoBolsaCollection[j].PUExercicio;
                                    ativoBolsaNovo.TipoMercado = ativoBolsaCollection[j].TipoMercado;
                                    ativoBolsaNovo.TipoPapel = (byte)TipoPapelAtivo.Normal;
                                    ativoBolsaNovo.Save();

                                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                                    if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(ativoBolsaDestino.CdAtivoBolsa, dataEx))
                                    {
                                        FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                                        fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                                        fatorCotacaoBolsaNovo.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                        fatorCotacaoBolsaNovo.Fator = fatorCotacaoBolsa.Fator.Value;
                                        fatorCotacaoBolsaNovo.Save();
                                    }
                                }
                                #endregion

                                #region Adiciona a Conversao do ativo Termo achado
                                ConversaoBolsa novaConversaoBolsa = new ConversaoBolsa();
                                novaConversaoBolsa.DataLancamento = conversaoBolsa.DataLancamento;
                                novaConversaoBolsa.DataEx = conversaoBolsa.DataEx;
                                novaConversaoBolsa.DataReferencia = conversaoBolsa.DataReferencia;
                                novaConversaoBolsa.FatorQuantidade = conversaoBolsa.FatorQuantidade;
                                novaConversaoBolsa.FatorPU = conversaoBolsa.FatorPU;
                                novaConversaoBolsa.Fonte = conversaoBolsa.Fonte;
                                novaConversaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                                novaConversaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;

                                conversaoBolsaCollection.AttachEntity(novaConversaoBolsa);
                                #endregion
                            }
                        }
                    }
                }
            }

            return conversaoBolsaCollection;
            
        }

        /// <summary>
        /// Carrega o objeto ConversaoBolsaCollection com todos os campos de ConversaoBolsa.
        /// </summary>        
        /// <param name="data"></param>
        public void BuscaConversaoBolsaCompleta(DateTime dataLancamento, string cdAtivoBolsa) {
            
            this.QueryReset();
            this.Query
                .Where(this.Query.DataLancamento.Equal(dataLancamento),
                       this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
            this.Query.Load();            
        }

        /// <summary>
        /// Deleta todos as conversões do dia, dada a fonte informada.
        /// </summary>
        /// <param name="data"></param>        
        /// <param name="fonte"></param>        
        public void DeletaConversao(DateTime dataLancamento, int fonte)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdConversao)
                 .Where(this.Query.Fonte == fonte,
                        this.Query.DataLancamento.Equal(dataLancamento));
            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataEx"></param>        
        public bool BuscaConversaoTermoOpcao(DateTime dataEx)
        {
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            ConversaoBolsaQuery conversaoBolsaQuery = new ConversaoBolsaQuery("C");

            conversaoBolsaQuery.Select(conversaoBolsaQuery.IdConversao);
            conversaoBolsaQuery.Where(conversaoBolsaQuery.DataEx.Equal(dataEx));
            conversaoBolsaQuery.Where(ativoBolsaQuery.TipoMercado.NotEqual(TipoMercadoBolsa.MercadoVista));
            conversaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(conversaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);

            ConversaoBolsaCollection conversaoBolsaCollection = new ConversaoBolsaCollection();
            conversaoBolsaCollection.Load(conversaoBolsaQuery);

            return conversaoBolsaCollection.Count > 0;
        }
    }
}
