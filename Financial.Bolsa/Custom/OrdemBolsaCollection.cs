﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;

namespace Financial.Bolsa {
    public partial class OrdemBolsaCollection : esOrdemBolsaCollection {
        
        /// <summary>
        ///    
        /// </summary>
        /// <param name="idCliente">Identificador do Cliente</param>
        /// <param name="dataOperacao">Data da Operação usada para Filtro</param>
        /// <returns></returns>
        public bool BuscaOrdemBolsa(int idCliente, DateTime dataOperacao) {
                        
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                        this.Query.IdAgenteCorretora, this.Query.CdAtivoBolsa)
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente);


            bool retorno = this.Query.Load();
            
            return retorno;
        }

        /// <summary>
        ///    
        /// </summary>
        /// <param name="idCliente">Identificador do Cliente</param>
        /// <param name="dataOperacao">Data da Operação usada para Filtro</param>
        /// <returns></returns>
        public bool BuscaOrdemBolsaCompleta(int idCliente, DateTime dataOperacao) {

            this.QueryReset();
            this.Query.Where(this.Query.Data.Equal(dataOperacao),
                             this.Query.IdCliente == idCliente);
            this.Query.OrderBy(this.Query.IdOrdem.Ascending);

            bool retorno = this.Query.Load();
          
            return retorno;            
        }

        /// <summary>
        /// Busca todas as ordens Bolsa que não tenham sido integradas (ex: Sinacor).
        /// </summary>
        /// <param name="idCliente">Identificador do Cliente</param>
        /// <param name="dataOperacao">Data da Operação usada para Filtro</param>
        /// <returns></returns>
        public bool BuscaOrdemBolsaNaoIntegrada(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query.Where(this.Query.Data.Equal(dataOperacao),
                             this.Query.IdCliente == idCliente,
                             this.Query.Fonte.NotEqual(FonteOrdemBolsa.Sinacor));
            this.Query.OrderBy(this.Query.IdOrdem.Ascending);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Atualiza a quantidade daytrade de todas as ordens filtradas por cliente e data 
        /// com a quantidade passada
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="quantidadeDaytrade"></param>
        public void AtualizaOrdemBolsa(int idCliente, DateTime data, decimal quantidadeDaytrade) 
        {
            OrdemBolsa ordemBolsa = new OrdemBolsa();
            this.BuscaOrdemBolsa(idCliente, data);

            for (int i = 0; i < this.Count; i++) {
                ordemBolsa = this[i];
                ordemBolsa.QuantidadeDayTrade = quantidadeDaytrade;
            }
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool BuscaOrdemBolsaVenda(int idCliente, DateTime data)
        {            
            this.QueryReset();
            
            this.Query
                 .Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                        this.Query.IdAgenteCorretora, this.Query.CdAtivoBolsa)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo),
                        this.Query.Origem.NotIn((byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra, (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda,
                                                (byte)OrigemOperacaoBolsa.Oferta),
                        this.Query.TipoOrdem == TipoOrdemBolsa.Venda)
                 .OrderBy(this.Query.IdAgenteCorretora.Ascending, 
                          this.Query.CdAtivoBolsa.Ascending, 
                          this.Query.IdOrdem.Ascending);

            bool retorno = this.Query.Load();
        
            return retorno;            

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool BuscaOrdemBolsaVendaGerencial(int idCliente, DateTime data) 
        {
            this.QueryReset();
            //
            this.Query
                 .Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                        this.Query.IdTrader, this.Query.CdAtivoBolsa)
                 .Where(this.Query.Data == data,
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo),
                        this.Query.TipoOrdem == TipoOrdemBolsa.Venda,
                        this.Query.IdTrader.IsNotNull())
                 .OrderBy(this.Query.IdTrader.Ascending,
                          this.Query.CdAtivoBolsa.Ascending,
                          this.Query.IdOrdem.Ascending);

            return this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="cdAtivoBolsa"></param>
        public bool BuscaOrdemBolsaCompra(int idCliente, DateTime data, int idAgenteCorretora, string cdAtivoBolsa) 
        {
            this.QueryReset();
            
            this.Query
                 .Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                        this.Query.IdAgenteCorretora, this.Query.CdAtivoBolsa)
                 .Where(this.Query.Data.Equal(data),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo),
                        this.Query.Origem.NotIn((byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra, (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda,
                                                (byte)OrigemOperacaoBolsa.Oferta),
                        this.Query.TipoOrdem == TipoOrdemBolsa.Compra,
                        this.Query.IdAgenteCorretora == idAgenteCorretora,
                        this.Query.CdAtivoBolsa == cdAtivoBolsa)
                 .OrderBy(this.Query.IdOrdem.Ascending);

            bool retorno = this.Query.Load();
            
            return retorno;                   
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idTrader"></param>
        /// <param name="cdAtivoBolsa"></param>
        public bool BuscaOrdemBolsaCompraGerencial(int idCliente, DateTime data, int idTrader, string cdAtivoBolsa) 
        {
            this.QueryReset();
            //
            this.Query
                 .Select(this.Query.IdOrdem, this.Query.Quantidade, this.Query.QuantidadeDayTrade,
                         this.Query.IdTrader, this.Query.CdAtivoBolsa)
                 .Where(this.Query.Data == data,
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.NotEqual(TipoMercadoBolsa.Termo),
                        this.Query.TipoOrdem == TipoOrdemBolsa.Compra,
                        this.Query.IdTrader == idTrader,
                        this.Query.CdAtivoBolsa == cdAtivoBolsa,
                        this.Query.IdTrader.IsNotNull())
                 .OrderBy(this.Query.IdOrdem.Ascending);

            return this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaOrdemBolsa(int idCliente, int idAgenteCorretora, DateTime data, FonteOrdemBolsa fonte)
        {                                   
            this.QueryReset();
            this.Query
              .Select(this.Query.IdOrdem)
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.IdAgenteCorretora == idAgenteCorretora,
                     this.Query.Data.Equal(data),
                     this.Query.Fonte == fonte);

            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();            
        }

        /// <summary>
        /// Deleta as ordens de OrdemBolsa e OrdemTermoBolsa, com Fonte = Sinacor.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="fonte"></param>        
        public void DeletaOrdemBolsaSinacor(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdOrdem)
              .Where(this.Query.IdCliente == idCliente,
                     this.Query.Data.Equal(data),
                     this.Query.Fonte == FonteOrdemBolsa.Sinacor);

            this.Query.Load();

            // Deleta as OrdensTermo sem carregá-las para a memoria
            OrdemTermoBolsaCollection ordemTermoBolsaColletion = new OrdemTermoBolsaCollection();

            for (int i = 0; i < this.Count; i++)
            {
                int idOrdem = this[i].IdOrdem.Value;

                //Antes de preparar para a deleção da ordemTermoBolsa, checa se existe de fato na tabela
                OrdemTermoBolsa ordemTermoBolsaConfere = new OrdemTermoBolsa();                
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ordemTermoBolsaConfere.Query.IdOrdem);
                if (ordemTermoBolsaConfere.LoadByPrimaryKey(campos, idOrdem))
                {
                    OrdemTermoBolsa ordemTermoBolsa = ordemTermoBolsaColletion.AddNew();
                    ordemTermoBolsa.IdOrdem = idOrdem;
                    ordemTermoBolsa.AcceptChanges();
                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {
                if (ordemTermoBolsaColletion.HasData)
                {
                    ordemTermoBolsaColletion.MarkAllAsDeleted();
                    ordemTermoBolsaColletion.Save(); // Apaga as OrdensTermoBolsa
                }

                if (this.HasData)
                {
                    this.MarkAllAsDeleted();
                    this.Save(); // Apaga OrdensBolsa
                }
                scope.Complete();
            }
        }

        /// <summary>
        /// Deleta as ordens de OrdemBolsa e OrdemTermoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="fonte"></param>        
        public void DeletaOrdemBolsa(int idCliente, DateTime data, byte fonte)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdOrdem)
              .Where(this.Query.IdCliente.Equal(idCliente),
                     this.Query.Data.Equal(data),
                     this.Query.Fonte.Equal(fonte));

            this.Query.Load();

            // Deleta as OrdensTermo sem carregá-las para a memoria
            OrdemTermoBolsaCollection ordemTermoBolsaColletion = new OrdemTermoBolsaCollection();

            for (int i = 0; i < this.Count; i++)
            {
                int idOrdem = this[i].IdOrdem.Value;

                //Antes de preparar para a deleção da ordemTermoBolsa, checa se existe de fato na tabela
                OrdemTermoBolsa ordemTermoBolsaConfere = new OrdemTermoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ordemTermoBolsaConfere.Query.IdOrdem);
                if (ordemTermoBolsaConfere.LoadByPrimaryKey(campos, idOrdem))
                {
                    OrdemTermoBolsa ordemTermoBolsa = ordemTermoBolsaColletion.AddNew();
                    ordemTermoBolsa.IdOrdem = idOrdem;
                    ordemTermoBolsa.AcceptChanges();
                }
            }

            using (esTransactionScope scope = new esTransactionScope())
            {
                if (ordemTermoBolsaColletion.HasData)
                {
                    ordemTermoBolsaColletion.MarkAllAsDeleted();
                    ordemTermoBolsaColletion.Save(); // Apaga as OrdensTermoBolsa
                }

                if (this.HasData)
                {
                    this.MarkAllAsDeleted();
                    this.Save(); // Apaga OrdensBolsa
                }
                scope.Complete();
            }
        }

        /// <summary>
        /// Deleta as ordens de OrdemBolsa e OrdemTermoBolsa.
        /// Deleta apenas para clientes com dataDia menor que data ou (dataDia = data e Status != Fechado).
        /// </summary>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>        
        public void DeletaOrdemBolsa(int idAgenteCorretora, DateTime data, FonteOrdemBolsa fonte)
        {
            OrdemBolsaQuery ordemBolsaQuery = new OrdemBolsaQuery("O");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            ordemBolsaQuery.Select(ordemBolsaQuery.IdOrdem);
            ordemBolsaQuery.InnerJoin(clienteQuery).On(ordemBolsaQuery.IdCliente == clienteQuery.IdCliente);
            ordemBolsaQuery.Where(ordemBolsaQuery.Data.Equal(data) &
                                            ordemBolsaQuery.Fonte.Equal(fonte) &
                                            ordemBolsaQuery.IdAgenteCorretora.Equal(idAgenteCorretora) &
                                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &
                                            (
                                                clienteQuery.DataDia.LessThan(data) |
                                                (clienteQuery.DataDia.Equal(data) & clienteQuery.Status.NotEqual((byte)StatusCliente.Divulgado))
                                            )
                                            );

            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            ordemBolsaCollection.Load(ordemBolsaQuery);
            
            // Deleta as OrdensTermo sem carregá-las para a memoria
            OrdemTermoBolsaCollection ordemTermoBolsaColletion = new OrdemTermoBolsaCollection();

            List<int> listaOrdens = new List<int>();
            for (int i = 0; i < ordemBolsaCollection.Count; i++)
            {
                int idOrdem = ordemBolsaCollection[i].IdOrdem.Value;

                listaOrdens.Add(idOrdem);
			}

            OrdemTermoBolsaCollection ordemTermoBolsaCollection = new OrdemTermoBolsaCollection();
            if (listaOrdens.Count > 0)
            {
                ordemTermoBolsaCollection.Query.Where(ordemTermoBolsaCollection.Query.IdOrdem.In(listaOrdens));
                ordemTermoBolsaCollection.Query.Load();
            }
            
            using (esTransactionScope scope = new esTransactionScope()) 
            {
                if (ordemTermoBolsaColletion.Count > 0)
                {
                    ordemTermoBolsaColletion.MarkAllAsDeleted();
                    ordemTermoBolsaColletion.Save(); // Apaga as OrdensTermoBolsa
                }

                if (ordemBolsaCollection.Count > 0)
                {
                    ordemBolsaCollection.MarkAllAsDeleted();
                    ordemBolsaCollection.Save(); // Apaga OrdensBolsa
                }
                scope.Complete();   
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgenteCorretora"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaOrdemBolsaSemTermo(int idAgenteCorretora, DateTime data, FonteOrdemBolsa fonte)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdOrdem)
              .Where(this.Query.IdAgenteCorretora == idAgenteCorretora,
                     this.Query.Data.Equal(data),
                     this.Query.Fonte == fonte);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Procura Elemento na Collection por idCliente, CdAtivoBolsa e TipoOperacao
        /// Operação feita na memoria 
        /// </summary>
        /// <param name="o">Objeto a comparar</param>
        /// <returns>Booleano indicando se existe Objeto na Collection
        /// Comparação é feita por idCliente, CdAtivoBolsa e TipoOperacao
        /// </returns>
        public bool Contains(OrdemBolsa o) {
            // Loop da Collection
            for (int i = 0; i < this.Count; i++) {
                OrdemBolsa ordemBolsa = this[i];
                if (ordemBolsa.IdCliente.Value == o.IdCliente.Value &&
                    ordemBolsa.CdAtivoBolsa.Trim() == o.CdAtivoBolsa.Trim() &&
                    ordemBolsa.TipoOrdem.Trim() == o.TipoOrdem.Trim()) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Procura Elemento na Collection por idCliente, cdAtivoBolsa e tipoOperacao
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="tipoOperacao"></param>
        /// <returns>Indice do Primeiro Objeto OrdemBolsa que Satisfaz os Critérios 
        ///          de Busca ou -1 se não Achar
        /// </returns>
        public int IndexOf(int idCliente, string cdAtivoBolsa, string tipoOperacao) {
            // Loop da Collection
            for (int i = 0; i < this.Count; i++) {
                OrdemBolsa ordemBolsa = this[i];
                if (ordemBolsa.IdCliente.Value == idCliente &&
                    ordemBolsa.CdAtivoBolsa.Trim() == cdAtivoBolsa.Trim() &&
                    ordemBolsa.TipoOrdem.Trim() == tipoOperacao.Trim()) {
                    
                    return i;
                }
            }

            return -1;
        }
    }
}