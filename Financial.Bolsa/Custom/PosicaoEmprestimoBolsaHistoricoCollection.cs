﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa {
    public partial class PosicaoEmprestimoBolsaHistoricoCollection : esPosicaoEmprestimoBolsaHistoricoCollection 
    {
        // Construtor
        // Cria uma nova PosicaoEmprestimoBolsaHistoricoCollection com os dados de PosicaoEmprestimoBolsaCollection
        // Todos os dados de PosicaoEmprestimoBolsaCollection são copiados, adicionando a dataHistorico
        public PosicaoEmprestimoBolsaHistoricoCollection(PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection, DateTime dataHistorico)
        {
            for (int i = 0; i < posicaoEmprestimoBolsaCollection.Count; i++)
            {
                PosicaoEmprestimoBolsaHistorico p = new PosicaoEmprestimoBolsaHistorico();

                // Para cada Coluna de PosicaoEmprestimoBolsa copia para PosicaoEmprestimoBolsaHistorico
                foreach (esColumnMetadata colPosicao in posicaoEmprestimoBolsaCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas 
                    esColumnMetadata colPosicaoEmprestimoBolsaHistorico = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                    if (posicaoEmprestimoBolsaCollection[i].GetColumn(colPosicao.Name) != null) 
                    {
                        p.SetColumn(colPosicaoEmprestimoBolsaHistorico.Name, posicaoEmprestimoBolsaCollection[i].GetColumn(colPosicao.Name));                        
                    }                    
                }

                p.DataHistorico = dataHistorico;

                this.AttachEntity(p);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoEmprestimoBolsaHistoricoCompleta(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();         
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoEmprestimoBolsaHistorico(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas posições de emprestimo de bolsa.
        /// Filtra por DataHistorico >= dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoEmprestimoBolsaHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas posições de emprestimo de bolsa.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoEmprestimoBolsaHistoricoDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoEmprestimoBolsaHistoricoCompletaDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();         
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivoBolsa"></param>
        public void BuscaPosicaoEmprestimoBolsaHistoricoCompleta(int idCliente, DateTime dataHistorico, 
                string cdAtivoBolsa) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.CdAtivoBolsa == cdAtivoBolsa,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();         
        }

        /// <summary>
        /// Retorna bool indicando se existe registro.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="dataHistorico"
        public bool ExistePosicaoEmprestimoBolsa(int idCliente, string cdAtivoBolsa, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Select(this.Query.IdPosicao)
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.CdAtivoBolsa == cdAtivoBolsa,
                       this.Query.DataHistorico.Equal(dataHistorico));

            bool existe = this.Query.Load();

            return existe;
        }
    }
}
