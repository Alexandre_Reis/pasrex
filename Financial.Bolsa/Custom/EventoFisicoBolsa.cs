﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Common.Enums;

namespace Financial.Bolsa {
    public partial class EventoFisicoBolsa : esEventoFisicoBolsa {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventoFisicoBolsa"></param>
        /// throws ArgumentNullException se campos Obrigatórios da inclusão estiverem nulos
        /// eventoFisicoBolsa.TipoEvento ou 
        /// eventoFisicoBolsa.TipoMovimento ou 
        /// eventoFisicoBolsa.TipoMercado ou
        /// eventoFisicoBolsa.Data ou
        /// eventoFisicoBolsa.Pu ou 
        /// eventoFisicoBolsa.PuLiquido ou 
        /// eventoFisicoBolsa.Quantidade ou 
        /// eventoFisicoBolsa.IdEventoVinculado ou
        /// eventoFisicoBolsa.IdCliente ou
        /// eventoFisicoBolsa.CdAtivoBolsa ou
        /// eventoFisicoBolsa.IdAgente
        public void InsereEventoFisicoBolsa(EventoFisicoBolsa eventoFisicoBolsa)  {
            
            #region ArgumentosNulos - throw ArgumentNullException
            if (!eventoFisicoBolsa.TipoEvento.HasValue ||
               String.IsNullOrEmpty(eventoFisicoBolsa.TipoMovimento) ||
               String.IsNullOrEmpty(eventoFisicoBolsa.TipoMercado) ||
               !eventoFisicoBolsa.Data.HasValue ||
               !eventoFisicoBolsa.Pu.HasValue ||
               !eventoFisicoBolsa.PULiquido.HasValue ||
               !eventoFisicoBolsa.Quantidade.HasValue ||
               !eventoFisicoBolsa.IdEventoVinculado.HasValue ||
               !eventoFisicoBolsa.IdCliente.HasValue ||
               String.IsNullOrEmpty( eventoFisicoBolsa.CdAtivoBolsa) ||
               !eventoFisicoBolsa.IdAgente.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("eventoFisicoBolsa.TipoEvento ou ")
                .Append("eventoFisicoBolsa.TipoMovimento ou ")
                .Append("eventoFisicoBolsa.TipoMercado ou ")
                .Append("eventoFisicoBolsa.Data ou ")
                .Append("eventoFisicoBolsa.Pu ou ")
                .Append("eventoFisicoBolsa.PuLiquido ou ")
                .Append("eventoFisicoBolsa.Quantidade ou ")
                .Append("eventoFisicoBolsa.IdEventoVinculado ou ")
                .Append("eventoFisicoBolsa.IdCliente ou ")
                .Append("eventoFisicoBolsa.CdAtivoBolsa ou ")
                .Append("eventoFisicoBolsa.IdAgente");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion
            
            eventoFisicoBolsa.Save();
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="idCliente"></param>
        ///// <param name="data"></param>
        ///// throws FatorCotacaoNaoCadastradoException quando o fator é invalido
        //public void ProcessaEventoFisico(int idCliente, DateTime data) 
        //{            
        //    EventoFisicoBolsaCollection eventoFisicoCollection = new EventoFisicoBolsaCollection();
        //    eventoFisicoCollection.BuscaEventoFisicoBolsa(idCliente, data);
        //    for (int i = 0; i < eventoFisicoCollection.Count; i++) {
        //        EventoFisicoBolsa eventoFisicoBolsa = eventoFisicoCollection[i];
                
        //        if ( eventoFisicoBolsa.IsTipoMovimentoRetirada() ){
        //            this.ProcessaEventoFisicoRetirada(idCliente, data, eventoFisicoBolsa);
        //        }
        //        else if ( eventoFisicoBolsa.IsTipoMovimentoDeposito() ) {
        //            this.ProcessaEventoFisicoDeposito(idCliente, data, eventoFisicoBolsa);
        //        }
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="idCliente"></param>
        ///// <param name="data"></param>
        ///// <param name="eventoFisico"></param>
        //private void ProcessaEventoFisicoRetirada(int idCliente, DateTime data, EventoFisicoBolsa eventoFisico) 
        //{   
        //    decimal pu = eventoFisico.Pu.Value;
        //    decimal puLiquido = eventoFisico.PULiquido.Value;
        //    decimal quantidadeEvento = eventoFisico.Quantidade.Value;
        //    string cdAtivo = eventoFisico.CdAtivoBolsa;
        //    int idAgente = eventoFisico.IdAgente.Value;
        //    string tipoMercado = eventoFisico.TipoMercado;

        //    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
        //    if (posicaoBolsa.BuscaPosicaoBolsa(idCliente, idAgente, cdAtivo)) {
        //        decimal quantidadePosicao = posicaoBolsa.Quantidade.Value;
        //        // Vendido
        //        if (quantidadePosicao <= 0) {
        //            posicaoBolsa.AtualizaPosicaoVendaVendido(posicaoBolsa.IdPosicao.Value,
        //                                        quantidadeEvento, pu, puLiquido);
        //        }
        //        else {
        //            // Se quantidade comprada for suficiente para passar de uma posicao negativa 
        //            // para positivo
        //            decimal quantidadeEventoNegativa = -1 * quantidadeEvento;                        
        //            if (quantidadeEvento > quantidadePosicao) {
        //                posicaoBolsa.AtualizaPosicaoVendaComprado(posicaoBolsa.IdPosicao.Value,
        //                                                        quantidadeEvento, pu, puLiquido);
        //            }
        //            else {
        //                // Baixa posicao - Atualizar apenas a quantidade
        //                posicaoBolsa.AtualizaQuantidade(posicaoBolsa.IdPosicao.Value, quantidadeEventoNegativa);
        //            }
        //        }
        //    }
        //    // Não tem posição - Inclusão da nova Posição
        //    else {
        //        FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
        //        if (!fator.BuscaFatorCotacaoBolsa(cdAtivo, data)) {
        //            throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivo + " " + data.ToString("d"));
        //        }
        //        #region cria nova Operacao
        //        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
        //        operacaoBolsa.AddNew();
        //        operacaoBolsa.IdCliente = idCliente;
        //        operacaoBolsa.IdAgenteCorretora = idAgente;
        //        operacaoBolsa.IdAgenteLiquidacao = idAgente;
        //        operacaoBolsa.CdAtivoBolsa = cdAtivo;
        //        operacaoBolsa.Quantidade = quantidadeEvento * -1;
        //        operacaoBolsa.Pu = pu;
        //        operacaoBolsa.PULiquido = puLiquido;
        //        operacaoBolsa.ValorLiquido = -1 * ((quantidadeEvento * puLiquido) / fator.Fator.Value);
        //        operacaoBolsa.TipoMercado = tipoMercado;
        //        operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
        //        #endregion
        //        //
        //        posicaoBolsa.InserePosicaoBolsa(operacaoBolsa);
        //    }
        //}
        
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="idCliente"></param>
        ///// <param name="data"></param>
        ///// <param name="eventoFisico"></param>
        //private void ProcessaEventoFisicoDeposito(int idCliente, DateTime data, EventoFisicoBolsa eventoFisico) 
        //{
        //    decimal pu = eventoFisico.Pu.Value;
        //    decimal puLiquido = eventoFisico.PULiquido.Value;
        //    decimal quantidadeEvento = eventoFisico.Quantidade.Value;
        //    string cdAtivo = eventoFisico.CdAtivoBolsa;
        //    int idAgente = eventoFisico.IdAgente.Value;
        //    string tipoMercado = eventoFisico.TipoMercado;

        //    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
        //    if (posicaoBolsa.BuscaPosicaoBolsa(idCliente, idAgente, cdAtivo)) {
        //        decimal quantidadePosicao = posicaoBolsa.Quantidade.Value;
        //        // Vendido
        //        if (quantidadePosicao >= 0) {
        //            posicaoBolsa.AtualizaPosicaoCompraComprado(posicaoBolsa.IdPosicao.Value,
        //                                        quantidadeEvento, pu, puLiquido);
        //        }
        //        else {
        //            // Se quantidade comprada for suficiente para passar de uma posicao negativa 
        //            // para positivo
        //            if (quantidadeEvento > Math.Abs(quantidadePosicao)) {
        //                posicaoBolsa.AtualizaPosicaoCompraVendido(posicaoBolsa.IdPosicao.Value,
        //                                                        quantidadeEvento, pu, puLiquido);
        //            }
        //            else {
        //                // Baixa posicao - Atualizar apenas a quantidade
        //                posicaoBolsa.AtualizaQuantidade(posicaoBolsa.IdPosicao.Value, quantidadeEvento);
        //            }
        //        }
        //    }
        //    // Não tem posição - Inclusão da nova Posição
        //    else {
        //        FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
        //        if (!fator.BuscaFatorCotacaoBolsa(cdAtivo, data)) {
        //            throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para o ativo e data : " + cdAtivo + " " + data.ToString("d"));
        //        }
        //        #region cria nova Operacao
        //        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
        //        operacaoBolsa.AddNew();
        //        operacaoBolsa.IdCliente = idCliente;
        //        operacaoBolsa.IdAgenteCorretora = idAgente;
        //        operacaoBolsa.IdAgenteLiquidacao = idAgente;
        //        operacaoBolsa.CdAtivoBolsa = cdAtivo;
        //        operacaoBolsa.Quantidade = quantidadeEvento;
        //        operacaoBolsa.Pu = pu;
        //        operacaoBolsa.PULiquido = puLiquido;
        //        operacaoBolsa.ValorLiquido = (quantidadeEvento * puLiquido) / fator.Fator.Value;
        //        operacaoBolsa.TipoMercado = tipoMercado;
        //        operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
        //        #endregion
        //        //
        //        posicaoBolsa.InserePosicaoBolsa(operacaoBolsa);
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo Movimento é Retirada
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMovimentoRetirada() {
            return this.TipoMovimento == TipoMovimentoEvento.Retirada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo Movimento é Deposito
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoMovimentoDeposito() {
            return this.TipoMovimento == TipoMovimentoEvento.Deposito;
        }
    }
}
