﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

using Financial.Investidor;
using Financial.Common;
using Financial.Interfaces.Import.Bolsa;
using Financial.Bolsa.Properties;
using Financial.Util;
using Financial.Interfaces.Sinacor;

using log4net;

using Financial.Bolsa.Enums;
using Financial.Common.Enums;

using Financial.Bolsa.Exceptions;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Common.Exceptions;
using Financial.Investidor.Enums;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Fundo;

namespace Financial.Bolsa {
    public partial class OrdemBolsa : esOrdemBolsa {
        // Log
        private static readonly ILog log = LogManager.GetLogger(typeof(OrdemBolsa));

        /// <summary>
        /// Classe Interna de OrdemBolsa para batimento do que é carregado em 
        /// OrdemBolsa vs Carga Externa (ex: NEGS).
        /// </summary>
        public class BatimentoOrdemBolsa
        {
            private int idCliente;
            private string apelido;
            private string cdAtivoBolsa;
            private string tipoOperacao;
            private decimal quantidadeFinancial;
            private decimal valorFinancial;
            private decimal quantidadeCarga;
            private decimal valorCarga;

            public int IdCliente
            {
                get { return idCliente; }
                set { idCliente = value; }
            }

            public string Apelido {
                get { return apelido; }
                set { apelido = value; }
            }

            public string CdAtivoBolsa
            {
                get { return cdAtivoBolsa; }
                set { cdAtivoBolsa = value; }
            }

            public string TipoOperacao {
                get { return tipoOperacao; }
                set { tipoOperacao = value; }
            }

            public decimal QuantidadeFinancial
            {
                get { return quantidadeFinancial; }
                set { quantidadeFinancial = value; }
            }

            public decimal ValorFinancial
            {
                get { return valorFinancial; }
                set { valorFinancial = value; }
            }

            public decimal QuantidadeCarga
            {
                get { return quantidadeCarga; }
                set { quantidadeCarga = value; }
            }

            public decimal ValorCarga
            {
                get { return valorCarga; }
                set { valorCarga = value; }
            }

            /// <summary>
            ///  Função Delegate que Indica se um BatimentoOrdemBolsa Existe
            /// </summary>
            /// <param name="d"></param>        
            /// <returns>  </returns>
            public bool Exists(BatimentoOrdemBolsa b) {
                return this.idCliente == b.idCliente &&
                       this.cdAtivoBolsa.Trim() == b.cdAtivoBolsa.Trim() &&
                       //this.tipoOperacao.Trim() == b.tipoOperacao.Trim() &&
                       this.quantidadeFinancial == b.quantidadeFinancial &&
                       this.valorFinancial == b.valorFinancial &&
                       this.quantidadeCarga == b.quantidadeCarga &&
                       this.valorCarga == b.valorCarga;
            }

            /// <summary>
            ///  Função Delegate que Serve para Ordenar a Collection de BatimentoOrdemBolsa pelo CdAtivoBolsa
            /// </summary>
            /// <param name="b1"></param>
            /// <param name="b2"></param>
            /// <returns> Se CdAtivo são Iguais Compara por TipoOperação            
            ///           1 se x maior y  
            ///          -1 se x menor y BatimentoOrdemBolsa
            /// </returns>
            public int OrderBatimentoOrdemBolsaByCdAtivoBolsa(BatimentoOrdemBolsa b1, BatimentoOrdemBolsa b2) {
                // Se Ativos são iguais ordena por TipoOperacao
                if (b1.cdAtivoBolsa.CompareTo(b2.cdAtivoBolsa) == 0) {
                    return b1.tipoOperacao.CompareTo(b2.tipoOperacao);
                }
                else {
                    return b1.cdAtivoBolsa.CompareTo(b2.cdAtivoBolsa);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CasaDayTrade(int idCliente, DateTime data)
        {            
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionCompra = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionVenda = new OrdemBolsaCollection();
            OrdemBolsa ordemBolsaCompra = new OrdemBolsa();
            OrdemBolsa ordemBolsaVenda = new OrdemBolsa();            
            //
            decimal quantidadeDayTrade = 0;
            // Atualiza quantidade daytrade = 0 de todas as ordens do cliente
            ordemBolsaCollection.AtualizaOrdemBolsa(idCliente, data, quantidadeDayTrade);
            //
            // Collection de Ordens de Venda
            ordemBolsaCollectionVenda.BuscaOrdemBolsaVenda(idCliente, data);

            for (int i = 0; i < ordemBolsaCollectionVenda.Count; i++) {
                ordemBolsaVenda = ordemBolsaCollectionVenda[i];
                decimal quantidadeVenda = ordemBolsaVenda.Quantidade.Value;
                decimal quantidadeVendaDT = ordemBolsaVenda.QuantidadeDayTrade.Value;
                int idOrdemVenda = ordemBolsaVenda.IdOrdem.Value;
                int idAgenteCorretora = ordemBolsaVenda.IdAgenteCorretora.Value;
                string cdAtivoBolsa = ordemBolsaVenda.CdAtivoBolsa;
                decimal quantidadeDiferenca;

                // Collection de ordens de compra
                if (!ordemBolsaCollectionCompra.BuscaOrdemBolsaCompra(idCliente, data, idAgenteCorretora, cdAtivoBolsa)) {
                    continue;
                }
               
                for (int j = 0; j < ordemBolsaCollectionCompra.Count; j++) {
                    ordemBolsaCompra = ordemBolsaCollectionCompra[j];
                    decimal quantidadeCompra = ordemBolsaCompra.Quantidade.Value;
                    decimal quantidadeCompraDT = ordemBolsaCompra.QuantidadeDayTrade.Value;
                    int idOrdemCompra = ordemBolsaCompra.IdOrdem.Value;
                    
                    if (quantidadeCompra == quantidadeCompraDT) continue;
                    if (quantidadeVenda == quantidadeVendaDT) break;

                    decimal deltaQuantidadeVenda = quantidadeVenda - quantidadeVendaDT;
                    decimal deltaQuantidadeCompra = quantidadeCompra - quantidadeCompraDT;
                                        
                    if (deltaQuantidadeVenda < deltaQuantidadeCompra) {
                        quantidadeDiferenca = deltaQuantidadeVenda;
                    }
                    else {
                        quantidadeDiferenca = deltaQuantidadeCompra;
                    }

                    if (quantidadeDiferenca != 0) {
                        this.AtualizaOrdemBolsa(idOrdemVenda, quantidadeDiferenca + quantidadeVendaDT);
                        this.AtualizaOrdemBolsa(idOrdemCompra, quantidadeDiferenca + quantidadeCompraDT);
                        quantidadeVendaDT += quantidadeDiferenca;
                    }
                }
            }
        }    
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idOrdem">PK da atualizacao</param>
        /// <param name="quantidadeDaytrade">quantidadeDaytrade</param>
        public void AtualizaOrdemBolsa(int idOrdem, decimal quantidadeDaytrade)
        {        
            OrdemBolsa ordemBolsaAux = new OrdemBolsa();
            ordemBolsaAux.AddNew();
            ordemBolsaAux.IdOrdem = idOrdem;
            ordemBolsaAux.AcceptChanges();                    
            ordemBolsaAux.QuantidadeDayTrade = quantidadeDaytrade;
            ordemBolsaAux.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Ordem é Venda
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoOrdemVenda() {
            return this.TipoOrdem == TipoOrdemBolsa.Venda;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo da Ordem é Compra
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoOrdemCompra() {
            return this.TipoOrdem == TipoOrdemBolsa.Compra;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente">idCliente não Transformado no idCliente Interno do Sistema</param>
        /// <param name="nomeArquivo">Nome do Arquivo Completo</param>
        /// <exception cref="AtivoNaoCadastradoException">Se Código do Ativo não estiver Cadastrado na Base</exception>
        /// <exception cref="ArquivoNegsNaoEncontradoException">Se Arquivo não Existe</exception>
        /// <exception cref="CodigoClienteBovespaInvalido">Se não existe idCliente no Sistema com o CodigoClienteBovespa informado no arquivo</exception>
        public void CarregaOrdemNegs(DateTime data, int idCliente, string nomeArquivo)
        {           
            #region GetIdAgente do Arquivo Negs
            if (!File.Exists(nomeArquivo)) {
                throw new ArquivoNegsNaoEncontradoException("\nArquivo Negs: "+ nomeArquivo+ " não encontrado.");
            }

            StreamReader sr = new StreamReader(nomeArquivo);
            string linha = sr.ReadLine();
            int codigoBovespaAgente = Convert.ToInt32(linha.Substring(6, 4));
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgenteCorretora = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            sr.Close();
            #endregion
            
            #region Confere o IdCliente Interno
            int codigoClienteBovespa = idCliente;
            CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
            bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
            if (!idClienteExiste) {             
                //throw new CodigoClienteBovespaInvalido("\nProcessando Arquivo Negs: Não Existe Cliente com o Código Cliente Bovespa Informado: " + codigoClienteBovespa);
                throw new CodigoClienteBovespaInvalido("\nProcessando Arquivo Negs: Não Existe Conta por Agente Cadastrada para o Cliente "+idCliente+" na Corretora "+codigoBovespaAgente);
            }
            #endregion    

            Negs negs = new Negs();
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionAux = new OrdemBolsaCollection();

            // Deleta as ordens de acordo com idCliente, idAgenteCorretora, data, e Fonte = ARQUIVONEGS
            ordemBolsaCollectionAux.DeletaOrdemBolsa(codigoClienteAgente.IdCliente.Value, idAgenteCorretora, data, FonteOrdemBolsa.ArquivoNEGS);
            // Pega as Ordens do arquivo de Negs 
            NegsCollection negsCollection = negs.ProcessaNegs(nomeArquivo, data);
            //
            // Ordena a Collection pelo campo NumeroNegocio
            negsCollection.CollectionNegs.Sort(negs.OrderNegsByNumeroNegocio);
                
            for (int i = 0; i < negsCollection.CollectionNegs.Count; i++) {
                negs = negsCollection.CollectionNegs[i];

                if (this.ProcessaNegs(negs)) {
                    #region Copia informações para OrdemBolsa
                    
                    if (negs.IsTipoRegistroDetalheNegocioRealizado()) 
                    {
                        string cdAtivoBolsa = negs.CdAtivoBolsa;

                        #region Trata Termo
                        if (negs.IsTipoMercadoTermo())
                        {   
                            DateTime dataVencimentoTermo = data.AddDays((double)negs.PrazoVencimento.Value);
                            string cdAtivoBolsaTermo = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsa, dataVencimentoTermo);

                            //Tira o 'T' da ultima posição
                            cdAtivoBolsa = cdAtivoBolsa.Remove(cdAtivoBolsa.Length - 1);
                            //

                            #region Se não existir o cdAtivoBolsa do Termo, cadastra em AtivoBolsa e em FatorCotacaoBolsa
                            AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                            if (!ativoBolsaTermo.LoadByPrimaryKey(cdAtivoBolsaTermo))
                            {
                                //Busca dados do ativoObjeto em AtivoBolsa e em FatorCotacaoBolsa
                                AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                                if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsa))
                                {
                                    throw new AtivoNaoCadastradoException("Ativo " + cdAtivoBolsa + " não cadastrado");
                                }
                                string especificacao = ativoBolsaObjeto.Especificacao;
                                string descricao = ativoBolsaObjeto.Descricao;
                                string codigoIsin = ativoBolsaObjeto.CodigoIsin;

                                AtivoBolsa ativoBolsaTermoInsert = new AtivoBolsa();
                                ativoBolsaTermoInsert.CdAtivoBolsa = cdAtivoBolsaTermo;
                                ativoBolsaTermoInsert.Especificacao = especificacao;
                                ativoBolsaTermoInsert.Descricao = descricao;
                                ativoBolsaTermoInsert.TipoMercado = TipoMercadoBolsa.Termo;
                                ativoBolsaTermoInsert.DataVencimento = dataVencimentoTermo;
                                ativoBolsaTermoInsert.CdAtivoBolsaObjeto = cdAtivoBolsa;
                                ativoBolsaTermoInsert.CodigoIsin = codigoIsin;
                                ativoBolsaTermoInsert.IdEmissor = 1; //default
                                ativoBolsaTermoInsert.IdMoeda = 1; //default
                                ativoBolsaTermoInsert.OpcaoIndice = 0; //default

                                ativoBolsaTermoInsert.Save();

                                FatorCotacaoBolsaCollection fatorCotacaoCollection = new FatorCotacaoBolsaCollection();
                                fatorCotacaoCollection.BuscaFatorCotacaoAtivo(cdAtivoBolsa);

                                for (int j = 0; j < fatorCotacaoCollection.Count; j++)
                                {
                                    FatorCotacaoBolsa fatorCotacaoBolsa = fatorCotacaoCollection[j];

                                    FatorCotacaoBolsa fatorCotacaoBolsaInsert = new FatorCotacaoBolsa();
                                    fatorCotacaoBolsaInsert.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                    fatorCotacaoBolsaInsert.Fator = fatorCotacaoBolsa.Fator.Value;
                                    fatorCotacaoBolsaInsert.CdAtivoBolsa = cdAtivoBolsaTermo;

                                    fatorCotacaoBolsaInsert.Save();
                                }

                            }
                            #endregion

                            //Volta ao cdAtivoBolsa o codigo total do Termo!!!
                            cdAtivoBolsa = cdAtivoBolsaTermo;
                            //
                        }                        
                        #endregion

                        string cdAtivoBolsaObjeto = negs.CdAtivoBolsaObjeto;

                        // retira a letra do Ativo                        
                        if (negs.IsTipoMercadoExercicioCompra() || negs.IsTipoMercadoExercicioVenda() ||
                            negs.IsTipoMercadoLeilaoTitulosNaoCotados() || negs.IsTipoMercadoLeilao() ||
                            negs.IsTipoMercadoFracionario()) {
                            int posicao = cdAtivoBolsa.Length - 1;
                            cdAtivoBolsa = cdAtivoBolsa.Remove(posicao);
                        }

                        // Lancamento de AtivoNaoCadastradoException                                       
                        #region Excecao de AtivoNaoCadastrado
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa)) {
                            throw new AtivoNaoCadastradoException("\nProcessando Arquivo Negs: Ativo não cadastrado: " + cdAtivoBolsa);
                        }

                        if (negs.IsTipoMercadoExercicioCompra() || negs.IsTipoMercadoExercicioVenda()) {
                            // Testa o cdAtivoObjeto existe
                            if (!String.IsNullOrEmpty(cdAtivoBolsaObjeto)) {
                                // lancamento de AtivoNaoCadastradoException                                       
                                ativoBolsa = new AtivoBolsa();
                                if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsaObjeto)) {
                                    throw new AtivoNaoCadastradoException("\nProcessando Arquivo Negs: Ativo não cadastrado: " + cdAtivoBolsaObjeto);
                                }
                            }
                        }
                        #endregion

                        string tipoMercado = String.Empty;
                        string cdAtivoBolsaOpcao = String.Empty;

                        byte origem = (byte)OrigemOrdemBolsa.Primaria;
                        if (negs.IsTipoMercadoExercicioCompra() || negs.IsTipoMercadoExercicioVenda()) { 
                            tipoMercado = TipoMercadoBolsa.MercadoVista;
                            cdAtivoBolsaOpcao = cdAtivoBolsa;
                            cdAtivoBolsa = cdAtivoBolsaObjeto;

                            origem = negs.IsTipoMercadoExercicioCompra()
                                     ? (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra
                                     : (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda;                                     
                        }                            
                        else if ( negs.IsTipoMercadoVista() || negs.IsTipoMercadoLeilaoTitulosNaoCotados() ||
                                  negs.IsTipoMercadoLeilao() ||negs.IsTipoMercadoFracionario() ) {
                            
                            tipoMercado = TipoMercadoBolsa.MercadoVista;                            
                        }
                        else if (negs.IsTipoMercadoOpcaoCompra()) {
                            tipoMercado = TipoMercadoBolsa.OpcaoCompra;                 
                        }
                        else if (negs.IsTipoMercadoOpcaoVenda()) {
                            tipoMercado = TipoMercadoBolsa.OpcaoVenda;
                        }
                        else if (negs.IsTipoMercadoTermo())
                        {
                            tipoMercado = TipoMercadoBolsa.Termo;
                        }

                        decimal valor = (negs.Quantidade.Value * negs.Pu.Value) / negs.FatorCotacao.Value;
                        // Preenche OrdemBolsa
                        #region Preenche OrdemBolsa
                        OrdemBolsa ordemBolsa = new OrdemBolsa();
                        ordemBolsa.TipoMercado = tipoMercado;
                        ordemBolsa.TipoOrdem = negs.TipoOperacao;
                        ordemBolsa.Data = data;
                        ordemBolsa.Pu = negs.Pu;
                        ordemBolsa.Valor = valor;
                        ordemBolsa.Quantidade = negs.Quantidade;
                        ordemBolsa.Origem = origem;
                        ordemBolsa.Fonte = (byte) FonteOrdemBolsa.ArquivoNEGS;
                        ordemBolsa.IdCliente = codigoClienteAgente.IdCliente.Value;                        
                        ordemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        if(!String.IsNullOrEmpty(cdAtivoBolsaOpcao)) {
                            ordemBolsa.CdAtivoBolsaOpcao = cdAtivoBolsaOpcao;
                        }                        
                        ordemBolsa.IdAgenteLiquidacao = idAgenteCorretora;
                        ordemBolsa.IdAgenteCorretora = idAgenteCorretora;
                        ordemBolsa.CalculaDespesas = "S";
                        ordemBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                        
                        #endregion
                        //
                        ordemBolsa.CheckValidData();
                        ordemBolsa.Save();
                        //

                        #region Verifica se o ativo é Termo, se for Inclusão em OrdemTermoBolsa
                        if (negs.IsTipoMercadoTermo())
                        {
                            int idOrdem = ordemBolsa.IdOrdem.Value;

                            OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();
                            ordemTermoBolsa.IdOrdem = idOrdem;
                            ordemTermoBolsa.IdIndice = null;
                            ordemTermoBolsa.NumeroContrato = "";
                            ordemTermoBolsa.Taxa = 0; //Por ora joga zero!

                            ordemTermoBolsa.Save();
                        }
                        #endregion
                    }
                    #endregion
                }
            }

            ordemBolsaCollection.Save();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="negsCollection"></param>
        /// <param name="data">idCliente não Transformado no idCliente Interno do Sistema</param>
        /// <param name="idCliente">idCliente não Transformado no idCliente Interno do Sistema</param>
        /// <exception cref="AtivoNaoCadastradoException">Se Código do Ativo não estiver Cadastrado na Base</exception>
        /// <exception cref="CodigoClienteBovespaInvalido">Se não existe idCliente no Sistema com o CodigoClienteBovespa informado no arquivo</exception>
        public void CarregaOrdemNegs(NegsCollection negsCollection, DateTime data, int idCliente) 
        {
            #region GetIdAgente do Arquivo Negs
            int codigoBovespaAgente = negsCollection.CollectionNegs[0].CodigoAgenteCorretora.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgenteCorretora = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            #region Confere o IdCliente Interno
            int codigoClienteBovespa = idCliente;
            CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
            bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
            if (!idClienteExiste) {
                //throw new CodigoClienteBovespaInvalido("\nProcessando Arquivo Negs: Não Existe Cliente com o Código Cliente Bovespa Informado: " + codigoClienteBovespa);
                throw new CodigoClienteBovespaInvalido("\nProcessando Arquivo Negs: Não Existe Conta por Agente Cadastrada para o Cliente " + idCliente + " na Corretora " + codigoBovespaAgente);
            }
            #endregion

            Negs negs = new Negs();
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionAux = new OrdemBolsaCollection();

            // Deleta as ordens de acordo com idCliente, idAgenteCorretora, data, e Fonte = ARQUIVONEGS
            ordemBolsaCollectionAux.DeletaOrdemBolsa(codigoClienteAgente.IdCliente.Value, idAgenteCorretora, data, FonteOrdemBolsa.ArquivoNEGS);
            
            // Pega as Ordens do arquivo de Negs 
            // Ordena a Collection pelo campo NumeroNegocio
            negsCollection.CollectionNegs.Sort(negs.OrderNegsByNumeroNegocio);

            for (int i = 0; i < negsCollection.CollectionNegs.Count; i++) {
                negs = negsCollection.CollectionNegs[i];

                if (this.ProcessaNegs(negs)) 
                {
                    #region Copia informações para OrdemBolsa

                    if (negs.IsTipoRegistroDetalheNegocioRealizado()) {
                        string cdAtivoBolsa = negs.CdAtivoBolsa;

                        #region Trata Termo
                        if (negs.IsTipoMercadoTermo()) {
                            DateTime dataVencimentoTermo = data.AddDays((double)negs.PrazoVencimento.Value);
                            string cdAtivoBolsaTermo = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsa, dataVencimentoTermo);

                            //Tira o 'T' da ultima posição
                            cdAtivoBolsa = cdAtivoBolsa.Remove(cdAtivoBolsa.Length - 1);
                            //

                            #region Se não existir o cdAtivoBolsa do Termo, cadastra em AtivoBolsa e em FatorCotacaoBolsa
                            AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                            if (!ativoBolsaTermo.LoadByPrimaryKey(cdAtivoBolsaTermo)) {
                                //Busca dados do ativoObjeto em AtivoBolsa e em FatorCotacaoBolsa
                                AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                                if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsa)) {
                                    throw new AtivoNaoCadastradoException("Ativo " + cdAtivoBolsa + " não cadastrado");
                                }
                                string especificacao = ativoBolsaObjeto.Especificacao;
                                string descricao = ativoBolsaObjeto.Descricao;
                                string codigoIsin = ativoBolsaObjeto.CodigoIsin;

                                AtivoBolsa ativoBolsaTermoInsert = new AtivoBolsa();
                                ativoBolsaTermoInsert.CdAtivoBolsa = cdAtivoBolsaTermo;
                                ativoBolsaTermoInsert.Especificacao = especificacao;
                                ativoBolsaTermoInsert.Descricao = descricao;
                                ativoBolsaTermoInsert.TipoMercado = TipoMercadoBolsa.Termo;
                                ativoBolsaTermoInsert.DataVencimento = dataVencimentoTermo;
                                ativoBolsaTermoInsert.CdAtivoBolsaObjeto = cdAtivoBolsa;
                                ativoBolsaTermoInsert.CodigoIsin = codigoIsin;
                                ativoBolsaTermoInsert.IdEmissor = 1; //default
                                ativoBolsaTermoInsert.IdMoeda = 1; //default
                                ativoBolsaTermoInsert.OpcaoIndice = 0; //default

                                ativoBolsaTermoInsert.Save();

                                FatorCotacaoBolsaCollection fatorCotacaoCollection = new FatorCotacaoBolsaCollection();
                                fatorCotacaoCollection.BuscaFatorCotacaoAtivo(cdAtivoBolsa);

                                for (int j = 0; j < fatorCotacaoCollection.Count; j++) {
                                    FatorCotacaoBolsa fatorCotacaoBolsa = fatorCotacaoCollection[j];

                                    FatorCotacaoBolsa fatorCotacaoBolsaInsert = new FatorCotacaoBolsa();
                                    fatorCotacaoBolsaInsert.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                    fatorCotacaoBolsaInsert.Fator = fatorCotacaoBolsa.Fator.Value;
                                    fatorCotacaoBolsaInsert.CdAtivoBolsa = cdAtivoBolsaTermo;

                                    fatorCotacaoBolsaInsert.Save();
                                }

                            }
                            #endregion

                            //Volta ao cdAtivoBolsa o codigo total do Termo!!!
                            cdAtivoBolsa = cdAtivoBolsaTermo;
                            //
                        }
                        #endregion


                        string cdAtivoBolsaObjeto = negs.CdAtivoBolsaObjeto;

                        // retira a letra do Ativo                        
                        if (negs.IsTipoMercadoExercicioCompra() || negs.IsTipoMercadoExercicioVenda() ||
                            negs.IsTipoMercadoLeilaoTitulosNaoCotados() || negs.IsTipoMercadoLeilao() ||
                            negs.IsTipoMercadoFracionario()) {
                            int posicao = cdAtivoBolsa.Length - 1;
                            cdAtivoBolsa = cdAtivoBolsa.Remove(posicao);
                        }

                        // Lancamento de AtivoNaoCadastradoException                                       
                        #region Excecao de AtivoNaoCadastrado
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa)) {
                            throw new AtivoNaoCadastradoException("\nProcessando Arquivo Negs: Ativo não cadastrado: " + cdAtivoBolsa);
                        }

                        if (negs.IsTipoMercadoExercicioCompra() || negs.IsTipoMercadoExercicioVenda()) {
                            // Testa o cdAtivoObjeto existe
                            if (!String.IsNullOrEmpty(cdAtivoBolsaObjeto)) {
                                // lancamento de AtivoNaoCadastradoException                                       
                                ativoBolsa = new AtivoBolsa();
                                if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsaObjeto)) {
                                    throw new AtivoNaoCadastradoException("\nProcessando Arquivo Negs: Ativo não cadastrado: " + cdAtivoBolsaObjeto);
                                }
                            }
                        }
                        #endregion

                        string tipoMercado = String.Empty;
                        string cdAtivoBolsaOpcao = String.Empty;

                        byte origem = (byte)OrigemOrdemBolsa.Primaria;
                        if (negs.IsTipoMercadoExercicioCompra() || negs.IsTipoMercadoExercicioVenda()) {
                            tipoMercado = TipoMercadoBolsa.MercadoVista;
                            cdAtivoBolsaOpcao = cdAtivoBolsa;
                            cdAtivoBolsa = cdAtivoBolsaObjeto;

                            origem = negs.IsTipoMercadoExercicioCompra()
                                     ? (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra
                                     : (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda;
                        }
                        else if (negs.IsTipoMercadoVista() || negs.IsTipoMercadoLeilaoTitulosNaoCotados() ||
                                  negs.IsTipoMercadoLeilao() || negs.IsTipoMercadoFracionario()) {

                            tipoMercado = TipoMercadoBolsa.MercadoVista;
                        }
                        else if (negs.IsTipoMercadoOpcaoCompra()) {
                            tipoMercado = TipoMercadoBolsa.OpcaoCompra;
                        }
                        else if (negs.IsTipoMercadoOpcaoVenda()) {
                            tipoMercado = TipoMercadoBolsa.OpcaoVenda;
                        }
                        else if (negs.IsTipoMercadoTermo()) {
                            tipoMercado = TipoMercadoBolsa.Termo;
                        }

                        decimal valor = (negs.Quantidade.Value * negs.Pu.Value) / negs.FatorCotacao.Value;
                        // Preenche OrdemBolsa
                        #region Preenche OrdemBolsa
                        OrdemBolsa ordemBolsa = new OrdemBolsa();
                        ordemBolsa.TipoMercado = tipoMercado;
                        ordemBolsa.TipoOrdem = negs.TipoOperacao;
                        ordemBolsa.Data = data;
                        ordemBolsa.Pu = negs.Pu;
                        ordemBolsa.Valor = valor;
                        ordemBolsa.Quantidade = negs.Quantidade;
                        ordemBolsa.Origem = origem;
                        ordemBolsa.Fonte = (byte)FonteOrdemBolsa.ArquivoNEGS;
                        ordemBolsa.IdCliente = codigoClienteAgente.IdCliente.Value;
                        ordemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        if (!String.IsNullOrEmpty(cdAtivoBolsaOpcao)) {
                            ordemBolsa.CdAtivoBolsaOpcao = cdAtivoBolsaOpcao;
                        }
                        ordemBolsa.IdAgenteLiquidacao = idAgenteCorretora;
                        ordemBolsa.IdAgenteCorretora = idAgenteCorretora;
                        ordemBolsa.CalculaDespesas = "S";
                        ordemBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                        #endregion
                        //
                        ordemBolsa.CheckValidData();
                        ordemBolsa.Save();
                        //

                        #region Verifica se o ativo é Termo, se for Inclusão em OrdemTermoBolsa
                        if (negs.IsTipoMercadoTermo()) {
                            int idOrdem = ordemBolsa.IdOrdem.Value;

                            OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();
                            ordemTermoBolsa.IdOrdem = idOrdem;
                            ordemTermoBolsa.IdIndice = null;
                            ordemTermoBolsa.NumeroContrato = "";
                            ordemTermoBolsa.Taxa = 0; //Por ora joga zero!

                            ordemTermoBolsa.Save();
                        }
                        #endregion
                    }
                    #endregion
                }
            }

            ordemBolsaCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pescCollection"></param>        
        /// <exception cref="AtivoNaoCadastradoException">Se Código do Ativo não estiver Cadastrado na Base</exception>
        /// <exception cref="CodigoClienteBovespaInvalido">Se não existe idCliente no Sistema com o CodigoClienteBovespa informado no arquivo</exception>
        public void CarregaOrdemPesc(PescCollection pescCollection, DateTime data) {
            #region GetIdAgente do Arquivo Pesc
            int codigoBovespaAgente = pescCollection.CollectionPesc[0].CodigoAgenteCorretora.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgenteCorretora = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            Pesc pesc = new Pesc();
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionAux = new OrdemBolsaCollection();

            #region Deleta as ordens de acordo com idAgenteCorretora, data, e Fonte = ARQUIVOPESC
            //ordemBolsaCollectionAux.DeletaOrdemBolsa(codigoClienteAgente.IdCliente.Value, idAgenteCorretora, data, FonteOrdemBolsa.ArquivoNEGS);

            bool delete = pescCollection.CollectionPesc.Count >= 2;
            if (delete) { // Se tem pelo menos dois elementos
                DateTime dataDelecao = pescCollection.CollectionPesc[1].DataPregao;

                ordemBolsaCollectionAux.Query
                        .Select(ordemBolsaCollectionAux.Query.IdOrdem)
                        .Where(ordemBolsaCollectionAux.Query.IdAgenteCorretora == idAgenteCorretora,
                               ordemBolsaCollectionAux.Query.Data == dataDelecao,
                               ordemBolsaCollectionAux.Query.Fonte == FonteOrdemBolsa.ArquivoPESC);

                ordemBolsaCollectionAux.Query.Load();
            }
            #endregion

            for (int i = 0; i < pescCollection.CollectionPesc.Count; i++) {
                pesc = pescCollection.CollectionPesc[i];

                // Não Armazena Mercado Termo
                if (pesc.IsTipoMercadoTermo()) {
                    continue;
                }

                #region Copia informações para OrdemBolsa

                    if (pesc.IsTipoRegistroEspecificacao()) {
                        string cdAtivoBolsa = pesc.CdAtivoBolsa;

                        int idCliente = pesc.CodigoCliente;

                        int codigoClienteBovespa = idCliente;
                        CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                        bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);

                        if (idClienteExiste)
                        {
                            string cdAtivoBolsaObjeto = pesc.CdAtivoBolsaObjeto;

                            // retira a letra do Ativo                        
                            if (pesc.IsTipoMercadoExercicioCompra() || pesc.IsTipoMercadoExercicioVenda() ||
                                pesc.IsTipoMercadoLeilaoTitulosNaoCotados() || pesc.IsTipoMercadoLeilao() ||
                                pesc.IsTipoMercadoFracionario())
                            {
                                int posicao = cdAtivoBolsa.Length - 1;
                                cdAtivoBolsa = cdAtivoBolsa.Remove(posicao);
                            }

                            // Lancamento de AtivoNaoCadastradoException                                       
                            #region Excecao de AtivoNaoCadastrado
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                            {
                                throw new AtivoNaoCadastradoException("\nProcessando Arquivo Pesc: Ativo não cadastrado: " + cdAtivoBolsa);
                            }

                            if (pesc.IsTipoMercadoExercicioCompra() || pesc.IsTipoMercadoExercicioVenda())
                            {
                                // Testa o cdAtivoObjeto existe
                                if (!String.IsNullOrEmpty(cdAtivoBolsaObjeto))
                                {
                                    // lancamento de AtivoNaoCadastradoException                                       
                                    ativoBolsa = new AtivoBolsa();
                                    if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsaObjeto))
                                    {
                                        throw new AtivoNaoCadastradoException("\nProcessando Arquivo Pesc: Ativo não cadastrado: " + cdAtivoBolsaObjeto);
                                    }
                                }
                            }
                            #endregion

                            string tipoMercado = String.Empty;
                            string cdAtivoBolsaOpcao = String.Empty;

                            byte origem = (byte)OrigemOrdemBolsa.Primaria;
                            #region TipoMercado e Origem
                            if (pesc.IsTipoMercadoExercicioCompra() || pesc.IsTipoMercadoExercicioVenda())
                            {
                                tipoMercado = TipoMercadoBolsa.MercadoVista;
                                cdAtivoBolsaOpcao = cdAtivoBolsa;
                                cdAtivoBolsa = cdAtivoBolsaObjeto;

                                origem = pesc.IsTipoMercadoExercicioCompra()
                                         ? (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra
                                         : (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda;
                            }
                            else if (pesc.IsTipoMercadoVista() || pesc.IsTipoMercadoLeilaoTitulosNaoCotados() ||
                                      pesc.IsTipoMercadoLeilao() || pesc.IsTipoMercadoFracionario())
                            {

                                tipoMercado = TipoMercadoBolsa.MercadoVista;
                            }
                            else if (pesc.IsTipoMercadoOpcaoCompra())
                            {
                                tipoMercado = TipoMercadoBolsa.OpcaoCompra;
                            }
                            else if (pesc.IsTipoMercadoOpcaoVenda())
                            {
                                tipoMercado = TipoMercadoBolsa.OpcaoVenda;
                            }
                            //else if (pesc.IsTipoMercadoTermo()) {
                            //    tipoMercado = TipoMercadoBolsa.Termo;
                            //}
                            #endregion

                            decimal valor = (pesc.Quantidade * pesc.Pu) / pesc.FatorCotacao;

                            // Preenche OrdemBolsa
                            #region Preenche OrdemBolsa
                            OrdemBolsa ordemBolsa = ordemBolsaCollection.AddNew();
                                                        
                            //
                            ordemBolsa.IdCliente = codigoClienteAgente.IdCliente.Value;
                            ordemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                            ordemBolsa.IdAgenteLiquidacao = idAgenteCorretora;
                            ordemBolsa.IdAgenteCorretora = idAgenteCorretora;
                            ordemBolsa.TipoMercado = tipoMercado;
                            ordemBolsa.TipoOrdem = pesc.Natureza;
                            ordemBolsa.Data = pesc.DataPregao;
                            ordemBolsa.Pu = pesc.Pu;
                            ordemBolsa.Origem = origem;
                            ordemBolsa.Fonte = (byte)FonteOrdemBolsa.ArquivoPESC;
                            ordemBolsa.DataOperacao = pesc.DataMovimento.HasValue ? pesc.DataMovimento : pesc.DataPregao;
                            ordemBolsa.IdLocalCustodia = ativoBolsa.IdLocalCustodia;
                            ordemBolsa.IdLocalNegociacao = ativoBolsa.IdLocalNegociacao;
                            ordemBolsa.IdClearing = ativoBolsa.IdClearing;

                            ordemBolsa.Valor = valor;
                            ordemBolsa.Quantidade = pesc.Quantidade;

                            if (!String.IsNullOrEmpty(cdAtivoBolsaOpcao))
                            {
                                ordemBolsa.CdAtivoBolsaOpcao = cdAtivoBolsaOpcao;
                            }

                            ordemBolsa.CalculaDespesas = "S";
                            ordemBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                            #endregion

                            ordemBolsa.CheckValidData();
                        }
                    }
                    #endregion                
            }

            using (esTransactionScope scope = new esTransactionScope()) {

                if (delete) {
                    ordemBolsaCollectionAux.MarkAllAsDeleted();
                    ordemBolsaCollectionAux.Save();
                }

                ordemBolsaCollection.Save();
                //
                scope.Complete();
            }           
        }
        
        private int MapTipoNotaCorretagemFonteOrdemBolsa(int tipoNotaCorretagem)
        {
            //Faz o mapeamento entro e ENUM TipoNotaCorretagem definido em Interfaces com a FonteOrdemBolsa
            int fonteOrdemBolsa;
            if(tipoNotaCorretagem == (int)Financial.Interfaces.Import.Bolsa.Enums.TipoNotaCorretagem.NotaHTML_ORRFAX02){
                fonteOrdemBolsa = (int)FonteOrdemBolsa.NotaHTML_ORRFAX02;
            }
            else if(tipoNotaCorretagem == (int)Financial.Interfaces.Import.Bolsa.Enums.TipoNotaCorretagem.NotaHTML_ORRLNOTA_FUT){
                fonteOrdemBolsa = (int)FonteOrdemBolsa.NotaHTML_ORRLNOTA_FUT;
            }
            else if(tipoNotaCorretagem == (int)Financial.Interfaces.Import.Bolsa.Enums.TipoNotaCorretagem.NotaHTML_ORRLNOTA_NOR){
                fonteOrdemBolsa = (int)FonteOrdemBolsa.NotaHTML_ORRLNOTA_NOR;
            }else{
                throw new FormatoNotaCorretagemNaoSuportadoException("Formato de Nota não suportado");
            }
            return fonteOrdemBolsa;
        }

        /// <summary>
        /// Carrega Notas de Corretagens
        /// </summary>
        /// <param name="resumoOperacoesCollection">Informações da Nota de Corretagem</param>
        /// <param name="data"></param>
        public void CarregaOrdemNotaCorretagem(List<NotaCorretagem.ResumoOperacoes> resumoOperacoesCollection, DateTime data) {
            // Variaveis para Delete NotaCorretagem
            List<int> idClienteDeletarOrdemBolsaNotaCorretagem = new List<int>();
            List<int> idAgenteDeletarOrdemBolsaNotaCorretagem = new List<int>();
            List<int> fonteOrdemBolsaDeletarOrdemBolsaNotaCorretagem = new List<int>();

            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionDeletar = new OrdemBolsaCollection();

            for (int i = 0; i < resumoOperacoesCollection.Count; i++) 
            {
                List<NotaCorretagem.Operacao> operacoes = resumoOperacoesCollection[i].Operacoes;

                #region Get IdAgente/CodigoBovespa
                AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();

                agenteMercadoCollection.Query.Select(agenteMercadoCollection.Query.IdAgente,
                                                     agenteMercadoCollection.Query.CodigoBovespa);
                //
                if (!String.IsNullOrEmpty(resumoOperacoesCollection[i].AgenteMercado))
                {
                    agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.Nome.ToLower() == resumoOperacoesCollection[i].AgenteMercado.ToLower());
                }
                else
                {
                    agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.CodigoBovespa == resumoOperacoesCollection[i].CodigoAgenteMercado);
                }

                agenteMercadoCollection.Query.Load();

                int? codigoBovespa = null;
                int? idAgente = null;
                if (agenteMercadoCollection.HasData) {
                    if (agenteMercadoCollection[0].IdAgente != null && agenteMercadoCollection[0].CodigoBovespa != null) {
                        codigoBovespa = agenteMercadoCollection[0].CodigoBovespa.Value;
                        idAgente = agenteMercadoCollection[0].IdAgente.Value;
                    }
                }
                if (codigoBovespa == null || idAgente == null) {
                    throw new IdAgenteNaoCadastradoException("Agente Mercado: " + resumoOperacoesCollection[i].AgenteMercado + " não Cadastrado");
                }
                #endregion

                #region Confere o IdCliente Interno
                CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                int? codigoClienteBovespa = null;
                if (resumoOperacoesCollection[i].Codigo.Contains("-")) {
                    int tam = resumoOperacoesCollection[i].Codigo.IndexOf("-");
                    string aux = resumoOperacoesCollection[i].Codigo.Substring(0, tam);
                    //                
                    codigoClienteBovespa = Convert.ToInt32(aux);
                }

                bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespa.Value, codigoClienteBovespa.Value);
                if (!idClienteExiste) {
                    throw new CodigoClienteBovespaInvalido("\nNota Corretagem não Processada. Não Existe Conta por Agente Cadastrada para o Cliente " + resumoOperacoesCollection[i].Codigo + " na Corretora " + codigoBovespa);
                }
                #endregion

                // Armazena o Cliente e a Corretora da Nota
                idClienteDeletarOrdemBolsaNotaCorretagem.Add(codigoClienteAgente.IdCliente.Value);
                idAgenteDeletarOrdemBolsaNotaCorretagem.Add(idAgente.Value);
                int fonteOrdemBolsa = this.MapTipoNotaCorretagemFonteOrdemBolsa(resumoOperacoesCollection[i].TipoNotaCorretagem);
                fonteOrdemBolsaDeletarOrdemBolsaNotaCorretagem.Add(fonteOrdemBolsa);

                #region Excecao Data Pregão Incorreta
                resumoOperacoesCollection[i].DataPregao = new DateTime(resumoOperacoesCollection[i].DataPregao.Year,
                                                                       resumoOperacoesCollection[i].DataPregao.Month,
                                                                       resumoOperacoesCollection[i].DataPregao.Day);
                if (resumoOperacoesCollection[i].DataPregao != data) {
                    throw new Exception("\nNota Corretagem não Processada. Data: " + resumoOperacoesCollection[i].DataPregao.ToShortDateString() + " Incorreta");
                }

                #endregion

                for (int j = 0; j < operacoes.Count; j++) {
                    #region Operações da Nota Corretagem relativas ao Cliente/Corretora

                    string cdAtivoBolsa = "";
                    string cdAtivoBolsaTermo = "";
                    DateTime dataVencimentoTermo = new DateTime();

                    if (operacoes[j].IsTipoMercadoExercicioCompra() || operacoes[j].IsTipoMercadoExercicioVenda())
                    {
                        string descricao = operacoes[j].AtivoDescricao.Trim().ToUpper();
                        string especificacao = operacoes[j].AtivoEspecificacao.Trim().ToUpper();
                        string tipoMercadoBolsa = operacoes[j].IsTipoMercadoExercicioCompra() ? TipoMercadoBolsa.OpcaoCompra : TipoMercadoBolsa.OpcaoVenda;
                        DateTime vencimento = resumoOperacoesCollection[i].DataPregao.Date;
                        decimal preco = operacoes[j].Preco;
                        
                        AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                        ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa);
                        ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.Descricao.Equal(descricao),
                                                         ativoBolsaCollection.Query.Especificacao.Equal(especificacao),
                                                         ativoBolsaCollection.Query.DataVencimento.Equal(vencimento),
                                                         ativoBolsaCollection.Query.PUExercicio.Equal(preco),
                                                         ativoBolsaCollection.Query.TipoMercado.Equal(tipoMercadoBolsa));
                        ativoBolsaCollection.Query.Load();

                        if (ativoBolsaCollection.Count == 0)
                        {
                            throw new AtivoNaoCadastradoException(String.Format("Ativo Bolsa:{0} Espec.:{1} Tipo Mercado:{2} Vencimento:{3} Preço:{4}",
                                descricao,
                                especificacao,
                                tipoMercadoBolsa,
                                vencimento,
                                preco));
                        }

                        cdAtivoBolsa = ativoBolsaCollection[0].CdAtivoBolsa.Trim().ToUpper();
                    }
                    else if (!String.IsNullOrEmpty(operacoes[j].Ativo))
                    {
                        cdAtivoBolsa = operacoes[j].Ativo.Trim().ToUpper();
                    }
                    else
                    {
                        string descricao = operacoes[j].AtivoDescricao.Trim().ToUpper();
                        string especificacao = operacoes[j].AtivoEspecificacao.Trim().ToUpper();

                        AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                        ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa);
                        ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.Descricao.Equal(descricao),
                                                         ativoBolsaCollection.Query.Especificacao.Equal(especificacao),
                                                         ativoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%9%"),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%10%"),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%11%"),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%12%"),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%13%"));
                        ativoBolsaCollection.Query.Load();

                        if (ativoBolsaCollection.Count == 0)
                        {
                            throw new AtivoNaoCadastradoException("Ativo Bolsa: " + descricao + " Espec. : " + especificacao + " não Cadastrado");
                        }
                                                
                        cdAtivoBolsa = ativoBolsaCollection[0].CdAtivoBolsa.Trim().ToUpper();
                        
                        if (operacoes[j].IsTipoMercadoTermo())
                        {
                            int prazo = operacoes[j].Prazo;
                            dataVencimentoTermo = resumoOperacoesCollection[i].DataPregao.AddDays((double)prazo);

                            cdAtivoBolsaTermo = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsa + "T", dataVencimentoTermo);
                        }
                    }
                    //                    

                    #region Excecao de AtivoNaoCadastrado
                    AtivoBolsa ativoBolsa = new AtivoBolsa();

                    if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa)) 
                    {
                        throw new AtivoNaoCadastradoException("\nNota Corretagem não Processada. Data: " + resumoOperacoesCollection[i].DataPregao.ToShortDateString() + "  - Ativo não cadastrado: " + cdAtivoBolsa);
                    }

                    if (operacoes[j].IsTipoMercadoTermo())
                    {
                        AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                        ativoBolsaTermo.TrataCadastroAtivoTermo(cdAtivoBolsa, cdAtivoBolsaTermo, dataVencimentoTermo);

                        cdAtivoBolsa = cdAtivoBolsaTermo;
                    }
                    #endregion

                    string cdAtivoBolsaOpcao = String.Empty;
                    string tipoMercado = String.Empty;
                    byte origem = (byte)OrigemOrdemBolsa.Primaria;

                    if (operacoes[j].IsTipoMercadoExercicioCompra() || operacoes[j].IsTipoMercadoExercicioVenda())
                    {
                        tipoMercado = TipoMercadoBolsa.MercadoVista;
                        cdAtivoBolsaOpcao = cdAtivoBolsa;
                        cdAtivoBolsa = ativoBolsa.CdAtivoBolsaObjeto;

                        origem = operacoes[j].IsTipoMercadoExercicioCompra()
                                 ? (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra
                                 : (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda;
                    }
                    else
                    {
                        tipoMercado = ativoBolsa.TipoMercado;
                    }

                    #region Copia informações para OrdemBolsa

                    // Preenche OrdemBolsa
                    #region Preenche OrdemBolsa
                    OrdemBolsa ordemBolsa = ordemBolsaCollection.AddNew();

                    ordemBolsa.IdCliente = codigoClienteAgente.IdCliente.Value;
                    ordemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    ordemBolsa.IdAgenteLiquidacao = idAgente;
                    ordemBolsa.IdAgenteCorretora = idAgente;
                    ordemBolsa.TipoOrdem = operacoes[j].Tipo.ToString();
                    ordemBolsa.Data = resumoOperacoesCollection[i].DataPregao;
                    ordemBolsa.Pu = operacoes[j].Preco;
                    ordemBolsa.Valor = operacoes[j].Valor;
                    ordemBolsa.Quantidade = operacoes[j].Quantidade;
                    ordemBolsa.Corretagem = 0;
                    ordemBolsa.Emolumento = 0;
                    ordemBolsa.LiquidacaoCBLC = 0;
                    ordemBolsa.RegistroBolsa = 0;
                    ordemBolsa.RegistroCBLC = 0;
                    ordemBolsa.PercentualDesconto = 0;
                    ordemBolsa.Desconto = 0;
                    ordemBolsa.QuantidadeDayTrade = 0;
                    ordemBolsa.Fonte = (byte)fonteOrdemBolsa;
                    if (!String.IsNullOrEmpty(cdAtivoBolsaOpcao))
                    {
                        ordemBolsa.CdAtivoBolsaOpcao = cdAtivoBolsaOpcao;
                    }
                    //
                    ordemBolsa.TipoMercado = tipoMercado;
                    
                    ordemBolsa.Origem = origem;

                    ordemBolsa.CalculaDespesas = "S";
                    ordemBolsa.IdMoeda = (int)ListaMoedaFixo.Real;

                    ordemBolsa.CheckValidData();

                    #endregion
                    //                
                    #endregion
                    #endregion
                }
            }

            using (esTransactionScope scope = new esTransactionScope()) {

                #region Deleta as ordens de acordo com idCliente, idAgenteCorretora, data, e Fonte = NOTA_HTML
                for (int i = 0; i < idClienteDeletarOrdemBolsaNotaCorretagem.Count; i++) {
                    ordemBolsaCollectionDeletar = new OrdemBolsaCollection();
                    ordemBolsaCollectionDeletar.Query.Where(ordemBolsaCollectionDeletar.Query.IdCliente == idClienteDeletarOrdemBolsaNotaCorretagem[i] &&
                                                            ordemBolsaCollectionDeletar.Query.IdAgenteCorretora == idAgenteDeletarOrdemBolsaNotaCorretagem[i] &&
                                                            ordemBolsaCollectionDeletar.Query.Data == data &&
                                                            ordemBolsaCollectionDeletar.Query.Fonte == (byte)fonteOrdemBolsaDeletarOrdemBolsaNotaCorretagem[i]);
                    ordemBolsaCollectionDeletar.Query.Load();
                    ordemBolsaCollectionDeletar.MarkAllAsDeleted();
                    ordemBolsaCollectionDeletar.Save();
                }
                #endregion

                //Salva Ordens
                ordemBolsaCollection.Save(); // Depois do Save identity estão prenchidos
                //

                #region Insere OrdemTermoBolsa
                ordemBolsaCollection.Filter = OrdemBolsaMetadata.ColumnNames.TipoMercado + " = '" + TipoMercadoBolsa.Termo + "' ";

                OrdemTermoBolsaCollection ordemTermoCollection = new OrdemTermoBolsaCollection();

                for (int i = 0; i < ordemBolsaCollection.Count; i++) {
                    OrdemTermoBolsa o = ordemTermoCollection.AddNew();
                    o.IdOrdem = ordemBolsaCollection[i].IdOrdem; // Seta o IdOrdem da OrdemBolsa Inserida
                    o.IdIndice = null;
                    o.NumeroContrato = "";
                    o.Taxa = 0;                    
                }
                ordemTermoCollection.Save();
                #endregion

                scope.Complete();
            }
        }

        /// <summary>
        /// Não processa o arquivo negs se:
        /// situacaoNegocio = "A"
        /// tipoMercado = "Ter"
        /// </summary>
        /// <returns>bool indicando se deve processar o arquivo negs</returns>
        private bool ProcessaNegs(Negs negs) {
            bool retorno = true;

            // Exclui pela SituacaoNegocio
            #region Exclui pela SituacaoNegocio
            if (negs.SituacaoNegocio.Equals('A')) {
                retorno = false;
            }
            #endregion

            return retorno;
        }

        /// <summary>
        /// Carrega em OrdemBolsa (ou OperacaoBolsa, caso na nota já traga o indicativo do DT) a partir de nota de corretagem PDF.
        /// </summary>
        /// <param name="streamArquivoPDF">Stream de Bytes do Arquivo PDF</param>
        /// <exception cref="AtivoNaoCadastradoException">Se Código do Ativo não estiver Cadastrado na Base</exception>
        /// <exception cref="CodigoClienteBovespaInvalido">Se não existe idCliente no Sistema com o CodigoClienteBovespa informado no arquivo</exception>
        public void CarregaNotaCorretagemPDF(Stream streamArquivoPDF) 
        {
            NotaCorretagemBolsaPDFCollection notaCorretagemBolsaPDFCollection = new NotaCorretagemBolsaPDFCollection();
            List<NotaCorretagemBolsaPDF> listaNotaCorretagem = notaCorretagemBolsaPDFCollection.RetornaOperacoes(streamArquivoPDF);

            if (listaNotaCorretagem.Count == 0) {
                return;
            }

            int codigoBovespaAgente = listaNotaCorretagem[0].codigoBovespa;

            #region GetIdAgente do Arquivo Negs
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgenteCorretora = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemTermoBolsaCollection ordemTermoBolsaCollection = new OrdemTermoBolsaCollection();

            List<int> listaIdCliente = new List<int>();
            List<DateTime> listaDatas = new List<DateTime>();
            List<decimal> listaCorretagem = new List<decimal>();
            List<decimal> listaTaxas = new List<decimal>();
            List<string> listaTipoMercado = new List<string>();
            foreach (NotaCorretagemBolsaPDF notaCorretagem in listaNotaCorretagem) 
            {
                #region Confere o IdCliente Interno
                int codigoClienteBovespa = notaCorretagem.codigoCliente;
                CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
                if (!idClienteExiste) {
                    throw new CodigoClienteBovespaInvalido("\nProcessando Arquivo Nota de Corretagem: Não Existe Conta por Agente Cadastrada para o Cliente " + codigoClienteBovespa + " na Corretora " + codigoBovespaAgente);
                }
                #endregion

                int idCliente = codigoClienteAgente.IdCliente.Value;

                PerfilCorretagemBolsa perfilCorretagemBolsa = new PerfilCorretagemBolsa();
                perfilCorretagemBolsa.BuscaPerfilCorretagemBolsa(idCliente, idAgenteCorretora, notaCorretagem.dataPregao, "");

                bool computaIss = perfilCorretagemBolsa.Iss.HasValue && perfilCorretagemBolsa.Iss.Value != 0;
                if (computaIss)
                {
                    notaCorretagem.corretagem += notaCorretagem.iss;
                }

                this.CheckValidData(idCliente, notaCorretagem.dataPregao); //Este check vale tanto para Insert em OrdemBolsa

                listaIdCliente.Add(idCliente);
                listaDatas.Add(notaCorretagem.dataPregao);

                listaCorretagem.Add(notaCorretagem.corretagem);
                listaTaxas.Add(notaCorretagem.taxaLiquidacao + notaCorretagem.registroCBLC + notaCorretagem.registroBolsa + notaCorretagem.emolumento);                

                if (notaCorretagem.listaDetalheNotaBolsaPDF.Count > 0)
                {
                    if (notaCorretagem.listaDetalheNotaBolsaPDF[0].tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoCompra ||
                        notaCorretagem.listaDetalheNotaBolsaPDF[0].tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoVenda)
                    {
                        listaTipoMercado.Add(TipoMercadoBolsa.OpcaoCompra); //Joga tudo de opções em OPC, pq as notas são segregadas em opções (OPC, OPV) e a vista (Ações, Frac e Termo)
                    }
                    else
                    {
                        listaTipoMercado.Add(TipoMercadoBolsa.MercadoVista);
                    }                    
                }

                foreach (DetalheNotaBolsaPDF detalheNotaBolsaPDF in notaCorretagem.listaDetalheNotaBolsaPDF) 
                {
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    string cdAtivoBolsa = "";

                    #region Trata AtivoBolsa, Monta cdAtivoBolsa
                    if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoCompra ||
                        detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoVenda ||
                        detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioCompra ||
                        detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioVenda) {
                        string[] cdAtivoSplit = detalheNotaBolsaPDF.ativo.Split(new char[] { ' ' });
                        string cdAtivo = cdAtivoSplit[0].Trim();

                        // retira a letra do Ativo
                        if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioCompra ||
                            detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioVenda) {
                            int posicao = cdAtivo.Length - 1;
                            cdAtivo = cdAtivo.Remove(posicao);
                        }

                        if (ativoBolsa.LoadByPrimaryKey(cdAtivo)) {
                            cdAtivoBolsa = ativoBolsa.CdAtivoBolsa;
                        }
                        else {
                            throw new AtivoNaoCadastradoException("Ativo não encontrado - " + cdAtivo);
                        }
                    }
                    else 
                    {
                        string descricao = detalheNotaBolsaPDF.ativo.Trim().Replace(" ", "");
                        string descricaoAux = descricao.Substring(0, 2); //Apenas para fazer o like no loop!
                        
                        AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                        ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa,
                                                          ativoBolsaCollection.Query.Descricao,
                                                          ativoBolsaCollection.Query.Especificacao);
                        ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.Descricao.Like(descricaoAux + "%"),
                                                         ativoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%9%"),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%10%"),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%11%"),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%12%"),
                                                         ativoBolsaCollection.Query.Especificacao.NotLike("%13%"));
                        ativoBolsaCollection.Query.Load();

                        bool achou = false;
                        foreach (AtivoBolsa ativoBolsaAux in ativoBolsaCollection)
                        {
                            string descricaoCompara = ativoBolsaAux.Descricao + ativoBolsaAux.Especificacao;
                            descricaoCompara = descricaoCompara.Replace(" ", "");

                            if (descricao.Contains(descricaoCompara))
                            {
                                achou = true;
                                cdAtivoBolsa = ativoBolsaAux.CdAtivoBolsa;
                            }

                            if (achou)
                                break;
                        }

                        if (!achou)
                        {
                            throw new AtivoNaoCadastradoException("Ativo não encontrado - " + descricao);
                        }
                    }

                    #region Trata Termo
                    if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.Termo) {
                        DateTime dataVencimentoTermo = notaCorretagem.dataPregao.AddDays(Convert.ToDouble(detalheNotaBolsaPDF.prazo));
                        string cdAtivoBolsaTermo = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsa + "T", dataVencimentoTermo);

                        #region Se não existir o cdAtivoBolsa do Termo, cadastra em AtivoBolsa e em FatorCotacaoBolsa
                        AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                        if (!ativoBolsaTermo.LoadByPrimaryKey(cdAtivoBolsaTermo)) {
                            //Busca dados do ativoObjeto em AtivoBolsa e em FatorCotacaoBolsa
                            AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                            if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsa)) {
                                throw new AtivoNaoCadastradoException("Ativo " + cdAtivoBolsa + " não cadastrado");
                            }
                            string especificacao = ativoBolsaObjeto.Especificacao;
                            string descricao = ativoBolsaObjeto.Descricao;
                            string codigoIsin = ativoBolsaObjeto.CodigoIsin;

                            AtivoBolsa ativoBolsaTermoInsert = new AtivoBolsa();
                            ativoBolsaTermoInsert.CdAtivoBolsa = cdAtivoBolsaTermo;
                            ativoBolsaTermoInsert.Especificacao = especificacao;
                            ativoBolsaTermoInsert.Descricao = descricao;
                            ativoBolsaTermoInsert.TipoMercado = TipoMercadoBolsa.Termo;
                            ativoBolsaTermoInsert.DataVencimento = dataVencimentoTermo;
                            ativoBolsaTermoInsert.CdAtivoBolsaObjeto = cdAtivoBolsa;
                            ativoBolsaTermoInsert.CodigoIsin = codigoIsin;
                            ativoBolsaTermoInsert.IdEmissor = (int)ListaEmissorFixo.Padrao;
                            ativoBolsaTermoInsert.IdMoeda = (short)ListaMoedaFixo.Real;
                            ativoBolsaTermoInsert.TipoPapel = (byte)TipoPapelAtivo.Normal;
                            ativoBolsaTermoInsert.OpcaoIndice = 0; //default
                            ativoBolsaTermoInsert.NumeroSerie = 0; //default

                            ativoBolsaTermoInsert.Save();

                            FatorCotacaoBolsaCollection fatorCotacaoCollection = new FatorCotacaoBolsaCollection();
                            fatorCotacaoCollection.BuscaFatorCotacaoAtivo(cdAtivoBolsa);

                            for (int j = 0; j < fatorCotacaoCollection.Count; j++) {
                                FatorCotacaoBolsa fatorCotacaoBolsa = fatorCotacaoCollection[j];

                                FatorCotacaoBolsa fatorCotacaoBolsaInsert = new FatorCotacaoBolsa();
                                fatorCotacaoBolsaInsert.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                fatorCotacaoBolsaInsert.Fator = fatorCotacaoBolsa.Fator.Value;
                                fatorCotacaoBolsaInsert.CdAtivoBolsa = cdAtivoBolsaTermo;

                                fatorCotacaoBolsaInsert.Save();
                            }

                        }
                        #endregion

                        //Volta ao cdAtivoBolsa o codigo total do Termo!!!
                        cdAtivoBolsa = cdAtivoBolsaTermo;
                        //
                    }
                    #endregion

                    #endregion

                    string cdAtivoOpcao = null;
                    string cdAtivoBolsaObjeto = "";
                    if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioCompra ||
                        detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioVenda) {
                        if (!String.IsNullOrEmpty(ativoBolsa.CdAtivoBolsaObjeto)) {
                            AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                            if (!ativoBolsaObjeto.LoadByPrimaryKey(ativoBolsa.CdAtivoBolsaObjeto)) {
                                throw new AtivoNaoCadastradoException("Ativo objeto não cadastrado - " + ativoBolsa.CdAtivoBolsaObjeto);
                            }
                        }
                        else {
                            throw new AtivoNaoCadastradoException("Ativo objeto não definido para a opção - " + cdAtivoBolsa);
                        }

                        cdAtivoBolsaObjeto = ativoBolsa.CdAtivoBolsaObjeto;
                        cdAtivoOpcao = cdAtivoBolsa;
                        cdAtivoBolsa = cdAtivoBolsaObjeto;
                    }

                    string tipoMercado = String.Empty;
                    if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioCompra ||
                        detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioVenda ||
                        detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.Vista ||
                        detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.Fracionario) 
                    {
                        tipoMercado = TipoMercadoBolsa.MercadoVista;
                    }
                    else if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoCompra) {
                        tipoMercado = TipoMercadoBolsa.OpcaoCompra;
                    }
                    else if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoVenda) {
                        tipoMercado = TipoMercadoBolsa.OpcaoVenda;
                    }
                    else if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.Termo) {
                        tipoMercado = TipoMercadoBolsa.Termo;
                    }

                    decimal valor = detalheNotaBolsaPDF.valor;

                    DateTime dataLiquidacao = new DateTime();
                    if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoCompra ||
                        detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.OpcaoVenda)
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(notaCorretagem.dataPregao, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }
                    else
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(notaCorretagem.dataPregao, 3, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }
                    
                    byte origem = (byte)OrigemOrdemBolsa.Primaria;
                    if (detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioCompra ||
                        detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioVenda) {
                        origem = detalheNotaBolsaPDF.tipoMercado == TipoDoMercadoNotaCorretagem.ExercicioCompra
                                 ? (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra
                                 : (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda;
                    }

                    #region Preenche OrdemBolsa
                    OrdemBolsa ordemBolsa = ordemBolsaCollection.AddNew();
                    ordemBolsa.CalculaDespesas = "N";
                    ordemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    ordemBolsa.CdAtivoBolsaOpcao = cdAtivoOpcao;
                    ordemBolsa.Data = notaCorretagem.dataPregao;
                    ordemBolsa.DataLiquidacao = dataLiquidacao;
                    ordemBolsa.Fonte = (byte)FonteOrdemBolsa.NotaPDFSinacor;
                    ordemBolsa.IdAgenteCorretora = idAgenteCorretora;
                    ordemBolsa.IdAgenteLiquidacao = idAgenteCorretora;
                    ordemBolsa.IdCliente = idCliente;
                    ordemBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                    ordemBolsa.Origem = origem;
                    ordemBolsa.Pu = detalheNotaBolsaPDF.pu;
                    ordemBolsa.Quantidade = detalheNotaBolsaPDF.quantidade;
                    ordemBolsa.TipoMercado = tipoMercado;
                    ordemBolsa.TipoOrdem = detalheNotaBolsaPDF.tipoOperacao;
                    ordemBolsa.Valor = detalheNotaBolsaPDF.valor;
                    #endregion                    

                }
            }

            using (esTransactionScope scope = new esTransactionScope()) 
            {
                #region Deleta antes de salvar as collections
                for (int i = 0; i < listaIdCliente.Count; i++)
                {
                    int idClienteLista = listaIdCliente[i];
                    DateTime dataLista = listaDatas[i];
                    
                    OrdemBolsaCollection ordemBolsaCollectionDeletar = new OrdemBolsaCollection();
                    ordemBolsaCollectionDeletar.Query.Select(ordemBolsaCollectionDeletar.Query.IdOrdem);
                    ordemBolsaCollectionDeletar.Query.Where(ordemBolsaCollectionDeletar.Query.IdCliente.Equal(idClienteLista),
                                                            ordemBolsaCollectionDeletar.Query.Data.Equal(dataLista),
                                                            ordemBolsaCollectionDeletar.Query.IdAgenteCorretora.Equal(idAgenteCorretora),
                                                            ordemBolsaCollectionDeletar.Query.Fonte.Equal((byte)FonteOrdemBolsa.NotaPDFSinacor));
                    ordemBolsaCollectionDeletar.Query.Load();
                    ordemBolsaCollectionDeletar.MarkAllAsDeleted();
                    ordemBolsaCollectionDeletar.Save();
                }

                ordemBolsaCollection.Save();
                #endregion

                #region Rateio de Corretagem e Taxas
                for (int i = 0; i < listaIdCliente.Count; i++)
                {
                    int idClienteLista = listaIdCliente[i];
                    DateTime dataLista = listaDatas[i];
                    
                    decimal corretagem = listaCorretagem[i];
                    decimal taxas = listaTaxas[i];
                    decimal totalDespesas = corretagem + taxas;

                    List<string> tiposMercado = new List<string>();
                    if (listaTipoMercado[i] == TipoMercadoBolsa.MercadoVista)
                    {
                        tiposMercado.Add("VIS");
                        tiposMercado.Add("TER");
                    }
                    else
                    {
                        tiposMercado.Add("OPC");
                        tiposMercado.Add("OPV");
                    }

                    if (totalDespesas != 0)
                    {
                        OrdemBolsa ordemBolsa = new OrdemBolsa();
                        ordemBolsa.RateiaTaxasTudo(idClienteLista, dataLista, idAgenteCorretora, tiposMercado, corretagem, taxas);
                    }
                }
                #endregion

                //Preenche (somente para operacoes do mercado = Termo)
                esEntityCollectionView<OrdemBolsa> vOrdemBolsa = new esEntityCollectionView<OrdemBolsa>(ordemBolsaCollection);
                vOrdemBolsa.Filter = "" + OrdemBolsaMetadata.ColumnNames.TipoMercado + " = 'TER'";
                foreach (OrdemBolsa ordemBolsa in vOrdemBolsa)
                {
                    string cdAtivoTermo = ordemBolsa.CdAtivoBolsa;
                    AtivoBolsa ativoTermo = new AtivoBolsa();
                    ativoTermo.LoadByPrimaryKey(cdAtivoTermo);

                    int idOrdem = ordemBolsa.IdOrdem.Value;

                    OrdemTermoBolsa ordemTermoBolsa = ordemTermoBolsaCollection.AddNew();
                    ordemTermoBolsa.IdOrdem = idOrdem;
                    ordemTermoBolsa.IdIndice = null;
                    ordemTermoBolsa.NumeroContrato = "";
                    ordemTermoBolsa.Taxa = 0; //Por ora joga zero!
                }
                ordemTermoBolsaCollection.Save();

                scope.Complete();
            }

        }

        /// <summary>
        ///    
        /// </summary>
        /// <param name="idCliente">Identificador do Cliente</param>
        /// <param name="dataOperacao">Data da Operação usada para Filtro</param>
        /// <param name="idAgente"></param>
        /// <returns></returns>
        public int BuscaNumeroOrdens(int idCliente, DateTime dataOperacao, int idAgente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdOrdem.Count())
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgente);

            this.Query.Load();

            return this.IdOrdem.Value;
        }

        /// <summary>
        ///    
        /// </summary>
        /// <param name="idCliente">Identificador do Cliente</param>
        /// <param name="dataOperacao">Data da Operação usada para Filtro</param>
        /// <param name="idAgente"></param>
        /// <returns></returns>
        public decimal BuscaQuantidadeOperada(int idCliente, DateTime dataOperacao, int idAgente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.Data.Equal(dataOperacao),
                        this.Query.IdCliente == idCliente,
                        this.Query.IdAgenteCorretora == idAgente);

            this.Query.Load();

            decimal quantidade = 0;
            if (this.Quantidade.HasValue)
            {
                quantidade = this.Quantidade.Value;
            }

            return quantidade;
        }

        /// <summary>
        /// Integra ordes vindas do Sinacor, e joga na tabela OrdemBolsa (e OrdemTermoBolsa, se forem ordens de Termo).
        /// Caso o ativo não exista, é cadastrado direto em AtivoBolsa e FatorCotacaoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        public void IntegraOrdensSinacor(int idCliente, DateTime dataOperacao, byte fonte, string tipoMercadoOperado)
        {
            ClienteBolsa clienteBolsa = new ClienteBolsa();

            #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!clienteBolsa.LoadByPrimaryKey(idCliente))
            {
                return;
            }

            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor) && String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2)) 
            {
                return;                
            }

            //Se nenhum dos 2 códigos é número, lança exception
            if (!Utilitario.IsInteger(clienteBolsa.CodigoSinacor) && !Utilitario.IsInteger(clienteBolsa.CodigoSinacor2))
            {
                return;                
            }            
                        
            int codigoCliente = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {                
                codigoCliente = Convert.ToInt32(clienteBolsa.CodigoSinacor);                
            }
            int codigoCliente2 = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = Convert.ToInt32(clienteBolsa.CodigoSinacor2);                
            }

            //Se algum dos 2 códigos for nulo, repete o código de um para ao outro
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = codigoCliente2;
            }
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = codigoCliente;
            }
            #endregion

            List<Int32> listaCodigos = new List<int>();
            listaCodigos.Add(codigoCliente);
            listaCodigos.Add(codigoCliente2);

            //Deleta ordens com Fonte = Sinacor (OrdemBolsa e OrdemTermoBolsa)
            OrdemBolsaCollection ordemBolsaCollectionDeletar = new OrdemBolsaCollection();
            ordemBolsaCollectionDeletar.DeletaOrdemBolsa(idCliente, dataOperacao, fonte);
            
            //Abre nova conexão para o Sinacor (ORACLE) e busca ordens do dia            
            VTbomovclCollection vtbomovclCollection = new VTbomovclCollection();
            vtbomovclCollection.BuscaOperacoesSINACOR(listaCodigos, dataOperacao);

            decimal percentualISS = 0;
            int idAgenteMercado = 0;
            if (vtbomovclCollection.Count > 0)
            {
                //Busca o IdAgenteMercado default
                int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                AgenteMercado agenteMercado = new AgenteMercado();
                idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

                PerfilCorretagemBolsaCollection perfilCorretagemBolsaCollection = new PerfilCorretagemBolsaCollection();
                perfilCorretagemBolsaCollection.Query.Where(perfilCorretagemBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                            perfilCorretagemBolsaCollection.Query.IdAgente.Equal(idAgenteMercado),                                                                
                                                            perfilCorretagemBolsaCollection.Query.DataReferencia.LessThanOrEqual(dataOperacao));
                perfilCorretagemBolsaCollection.Query.Load();

                if (perfilCorretagemBolsaCollection.Count != 0)
                {
                    percentualISS = perfilCorretagemBolsaCollection[0].Iss.Value;
                }
            }

            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();

            for (int i = 0; i < vtbomovclCollection.Count; i++)
            {
                VTbomovcl vtbomovcl = vtbomovclCollection[i];
                

                //Tratamento para CdAtivoBolsa
                string cdAtivoBolsa = vtbomovcl.CdNegocio.Trim();
                if (Utilitario.Right(cdAtivoBolsa, 1) == "E" || Utilitario.Right(cdAtivoBolsa, 1) == "F" ||
                    Utilitario.Right(cdAtivoBolsa, 1) == "L")
                {
                    int len_menos1 = cdAtivoBolsa.Length - 1;
                    cdAtivoBolsa = cdAtivoBolsa.Substring(0, len_menos1);
                }
                //

                Int32 numeroNegocio = Convert.ToInt32(vtbomovcl.NrNegocio.Value);
                string tipoOperacao = vtbomovcl.CdNatope;
                decimal quantidade = vtbomovcl.QtQtdesp.Value;
                decimal valor = vtbomovcl.VlTotneg.Value;
                decimal pu = vtbomovcl.VlNegocio.Value;
                DateTime? dataLiquidacao = vtbomovcl.PzProjCc;

                decimal corretagem = 0;
                if (vtbomovcl.VlCortot.HasValue)
                {
                    corretagem = vtbomovcl.VlCortot.Value;
                }

                decimal valorISS = vtbomovcl.VlIss.Value;
                if (percentualISS != 0 && vtbomovcl.VlIss.HasValue)
                {
                    corretagem = corretagem + valorISS;
                }

                decimal emolumento = 0;
                if (vtbomovcl.VlEmolumBv.HasValue)
                {
                    emolumento = vtbomovcl.VlEmolumBv.Value;
                }

                decimal liquidacaoCBLC = 0;
                if (vtbomovcl.VlEmolumCb.HasValue)
                {
                    liquidacaoCBLC = vtbomovcl.VlEmolumCb.Value;
                }

                decimal registroCBLC = 0;
                if (vtbomovcl.VlTaxreg.HasValue)
                {
                    registroCBLC = vtbomovcl.VlTaxreg.Value;
                }
                //

                #region Carrega o ativo, ou integra do Sinacor preenchendo o objeto ativoBolsa
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                //Apenas para ativos que NÃO sejam Termo
                if (Utilitario.Right(vtbomovcl.CdNegocio.Trim(), 1) != "T" && Utilitario.Right(vtbomovcl.CdNegocio.Trim(), 1) != "S")
                {
                    if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                    {
                        //Integra ativo do Sinacor
                        if (!ativoBolsa.IntegraAtivoSinacor(cdAtivoBolsa, dataOperacao))
                        {
                            throw new AtivoNaoCadastradoException("Ativo não cadastrado no Sinacor - " + cdAtivoBolsa);
                        }
                    }
                }

                string tipoMercado = "";
                if (Utilitario.Right(vtbomovcl.CdNegocio.Trim(), 1) == "T" || Utilitario.Right(vtbomovcl.CdNegocio.Trim(), 1) == "S")
                {
                    tipoMercado = "TER";
                }
                else
                {
                    tipoMercado = ativoBolsa.TipoMercado; //Já foi buscado na consulta logo acima
                }
                #endregion          

                #region Trata origem da ordem de bolsa, se for exercício, dá os devidos tratamentos
                byte origem = (byte)OrigemOrdemBolsa.Primaria;
                string cdAtivoBolsaOpcao = null;
                if (Utilitario.Right(vtbomovcl.CdNegocio.Trim(), 1) == "E")
                {
                    if (tipoMercado == TipoMercadoBolsa.OpcaoCompra)
                    {
                        origem = (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra;
                    }
                    else
                    {
                        origem = (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda;
                    }

                    //Precisa converter para A Vista, já que no Sinacor vem como Opção
                    tipoMercado = TipoMercadoBolsa.MercadoVista;
                    
                    //Busca o ativo objeto (ação) referente à opção operada                    
                    AtivoBolsa ativoBolsaOpcao = new AtivoBolsa();
                    string cdAtivoBolsaObjeto;                    
                    cdAtivoBolsaObjeto = ativoBolsaOpcao.RetornaCdAtivoBolsaObjeto(cdAtivoBolsa);
                    if (String.IsNullOrEmpty(cdAtivoBolsaObjeto))
                    {
                        throw new AtivoObjetoNaoCadastradoException("Ativo objeto não cadastrado para a opção " + cdAtivoBolsa);
                    }
                    else
                    {
                        //Guarda a opção em cdAtivoBolsaOpcao
                        cdAtivoBolsaOpcao = cdAtivoBolsa;
                        //O ativo bolsa passa a ser o ativo objeto da opção
                        cdAtivoBolsa = cdAtivoBolsaObjeto;
                    }
                }
                #endregion

                #region Verifica se o ativo é Termo, se for dá os devidos tratamentos
                DateTime dataVencimentoTermo;
                string numeroContrato = "";
                if (tipoMercado == TipoMercadoBolsa.Termo)
                {
                    Tornegd tornegd = new Tornegd();
                    int nrDiasVencimentoTermo = tornegd.RetornaDiasVencimentoTermoSINACOR(dataOperacao, numeroNegocio,
                                                                                    tipoOperacao, cdAtivoBolsa);
                    dataVencimentoTermo = dataOperacao.AddDays(nrDiasVencimentoTermo);

                    if (!Calendario.IsDiaUtil(dataVencimentoTermo, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
                    {
                        dataVencimentoTermo = Calendario.AdicionaDiaUtil(dataVencimentoTermo, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }
                    
                    int len_menos1 = cdAtivoBolsa.Length - 1;
                    string cdAtivoBolsaObjeto = cdAtivoBolsa.Substring(0, len_menos1);                    
                                        
                    //Transforma o cdAtivoBolsa, incluindo a data do vencimento ao final                    
                    cdAtivoBolsa = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsa, dataVencimentoTermo);

                    #region Se não existir o cdAtivoBolsa, cadastra em AtivoBolsa e em FatorCotacaoBolsa
                    AtivoBolsa ativoBolsaTermo = new AtivoBolsa();

                    if (!ativoBolsaTermo.LoadByPrimaryKey(cdAtivoBolsa))
                    {
                        //Busca dados do ativoObjeto em AtivoBolsa e em FatorCotacaoBolsa
                        AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                        if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsaObjeto))
                        {
                            throw new AtivoNaoCadastradoException("Ativo " + cdAtivoBolsaObjeto + " não cadastrado");
                        }
                        string especificacao = ativoBolsaObjeto.Especificacao;
                        string descricao = ativoBolsaObjeto.Descricao;
                        string codigoIsin = ativoBolsaObjeto.CodigoIsin;

                        AtivoBolsa ativoBolsaTermoInsert = new AtivoBolsa();
                        ativoBolsaTermoInsert.CdAtivoBolsa = cdAtivoBolsa;
                        ativoBolsaTermoInsert.Especificacao = especificacao;
                        ativoBolsaTermoInsert.Descricao = descricao;
                        ativoBolsaTermoInsert.TipoMercado = TipoMercadoBolsa.Termo;
                        ativoBolsaTermoInsert.DataVencimento = dataVencimentoTermo;
                        ativoBolsaTermoInsert.CdAtivoBolsaObjeto = cdAtivoBolsaObjeto;
                        ativoBolsaTermoInsert.CodigoIsin = codigoIsin;
                        ativoBolsaTermoInsert.IdMoeda = (short)ListaMoedaFixo.Real;
                        ativoBolsaTermoInsert.IdEmissor = (int)ListaEmissorFixo.Padrao;
                        ativoBolsaTermoInsert.OpcaoIndice = 0; //default
                        ativoBolsaTermoInsert.NumeroSerie = 0;
                        ativoBolsaTermoInsert.TipoPapel = (byte)TipoPapelAtivo.Normal;

                        EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
                        estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
                        estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Ações%"));
                        estrategiaCollection.Query.Load();

                        if (estrategiaCollection.Count > 0)
                        {
                            ativoBolsaTermoInsert.IdEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                        }

                        ativoBolsaTermoInsert.Save();

                        FatorCotacaoBolsaCollection fatorCotacaoCollection = new FatorCotacaoBolsaCollection();
                        fatorCotacaoCollection.BuscaFatorCotacaoAtivo(cdAtivoBolsaObjeto);

                        for (int j = 0; j < fatorCotacaoCollection.Count; j++)
                        {
                            FatorCotacaoBolsa fatorCotacaoBolsa = fatorCotacaoCollection[j];
                            
                            FatorCotacaoBolsa fatorCotacaoBolsaInsert = new FatorCotacaoBolsa();
                            fatorCotacaoBolsaInsert.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                            fatorCotacaoBolsaInsert.Fator = fatorCotacaoBolsa.Fator.Value;
                            fatorCotacaoBolsaInsert.CdAtivoBolsa = cdAtivoBolsa;

                            fatorCotacaoBolsaInsert.Save();
                        }
                        
                    }
                    #endregion

                    //Tenta buscar o nr do contrato da TCFPOSI_TERM
                    TcfposiTerm tcfposiTerm = new TcfposiTerm();
                    numeroContrato = tcfposiTerm.RetornaNumeroContrato(listaCodigos, vtbomovcl.CdNegocio.Trim(), dataVencimentoTermo, numeroNegocio).ToString();
                    //
                }                
                #endregion

                if (tipoMercadoOperado != "")
                {
                    if (tipoMercado != tipoMercadoOperado)
                        continue;
                }

                #region Inclusão em OrdemBolsa
                OrdemBolsa ordemBolsa = new OrdemBolsa();
                ordemBolsa.IdCliente = idCliente;
                ordemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                ordemBolsa.IdAgenteCorretora = idAgenteMercado;
                ordemBolsa.IdAgenteLiquidacao = idAgenteMercado;
                ordemBolsa.TipoMercado = tipoMercado;
                ordemBolsa.TipoOrdem = tipoOperacao;
                ordemBolsa.Data = dataOperacao;
                ordemBolsa.Pu = pu;
                ordemBolsa.Valor = valor;
                ordemBolsa.Quantidade = quantidade;
                ordemBolsa.Corretagem = corretagem;
                ordemBolsa.Emolumento = emolumento;
                ordemBolsa.RegistroCBLC = registroCBLC;
                ordemBolsa.LiquidacaoCBLC = liquidacaoCBLC;
                ordemBolsa.ValorISS = valorISS;
                ordemBolsa.Origem = origem;
                ordemBolsa.Fonte = fonte;
                ordemBolsa.PercentualDesconto = 0;
                ordemBolsa.Desconto = 0;
                ordemBolsa.QuantidadeDayTrade = 0;
                ordemBolsa.CdAtivoBolsaOpcao = cdAtivoBolsaOpcao;
                ordemBolsa.NumeroNegocio = numeroNegocio;
                ordemBolsa.CalculaDespesas = "N";
                ordemBolsa.DataLiquidacao = dataLiquidacao;
                ordemBolsa.IdMoeda = (int)ListaMoedaFixo.Real;

                ordemBolsa.Save();
                #endregion

                int idOrdem = ordemBolsa.IdOrdem.Value;

                #region Verifica se o ativo é Termo, se for Inclusão em OrdemTermoBolsa
                if (tipoMercado == TipoMercadoBolsa.Termo)
                {
                    OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();
                    ordemTermoBolsa.IdOrdem = idOrdem;
                    ordemTermoBolsa.IdIndice = null;
                    ordemTermoBolsa.NumeroContrato = numeroContrato;
                    ordemTermoBolsa.Taxa = 0; //Por ora joga zero!

                    ordemTermoBolsa.Save();
                }
                #endregion                                
            }

        }

        /// <summary>
        /// Carrega a partir da TabelaCRCA as ordens do cliente e data passados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaOrdemCRCA(int idCliente, DateTime data)
        {
            int codigoBovespaAgente = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);

            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);

            CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
            bool idClienteExiste = codigoClienteAgente.BuscaCodigoBovespaCliente(idAgente, idCliente);

            if (!idClienteExiste)
            {
                return;
            }

            TabelaCRCACollection tabelaCRCACollection = new TabelaCRCACollection();
            tabelaCRCACollection.Query.Where(tabelaCRCACollection.Query.CodigoCliente.Equal(codigoClienteAgente.CodigoClienteBovespa.Value),
                                             tabelaCRCACollection.Query.DataMovimento.Equal(data));
            tabelaCRCACollection.Query.Load();
                      
            
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionAux = new OrdemBolsaCollection();

            // Deleta as ordens de acordo com idCliente, idAgenteCorretora, data, e Fonte = ArquivoCRCA
            ordemBolsaCollectionAux.DeletaOrdemBolsa(idCliente, idAgente, data, FonteOrdemBolsa.ArquivoCRCA);
                        
            
            foreach (TabelaCRCA tabelaCRCA in tabelaCRCACollection)
            {
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (ativoBolsa.BuscaCdAtivoBolsa(tabelaCRCA.CodigoIsin))
                {
                    string cdAtivoBolsa = ativoBolsa.CdAtivoBolsa;

                    AtivoBolsa ativoBolsaMercado = new AtivoBolsa();
                    ativoBolsaMercado.LoadByPrimaryKey(cdAtivoBolsa);

                    #region Preenche OrdemBolsa
                    OrdemBolsa ordemBolsa = ordemBolsaCollection.AddNew();
                    ordemBolsa.TipoMercado = ativoBolsaMercado.TipoMercado;
                    ordemBolsa.TipoOrdem = TipoOrdemBolsa.Compra;
                    ordemBolsa.Data = data;
                    ordemBolsa.Pu = Math.Round((decimal)tabelaCRCA.ValorEfetivo / (decimal)tabelaCRCA.QuantidadeEfetiva, 2);
                    ordemBolsa.Valor = tabelaCRCA.ValorEfetivo;
                    ordemBolsa.Quantidade = Convert.ToDecimal(tabelaCRCA.QuantidadeEfetiva);
                    ordemBolsa.Origem = (byte)OrigemOrdemBolsa.Oferta;
                    ordemBolsa.Fonte = (byte)FonteOrdemBolsa.ArquivoCRCA;
                    ordemBolsa.IdCliente = idCliente;
                    ordemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    ordemBolsa.IdAgenteLiquidacao = idAgente;
                    ordemBolsa.IdAgenteCorretora = idAgente;
                    ordemBolsa.CalculaDespesas = "N";
                    ordemBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                    #endregion
                }
                
                             
            }

            ordemBolsaCollection.Save();
        }

        /// <summary>
        /// Recebe uma collection de Negs e uma collection de OrdemBolsa 
        /// (ambas consolidadas (agrupadas) e ordenadas por CdAtivoBolsa e TipoOperacao) 
        /// p/ comparar cliente a cliente, ativo a ativo, retornando uma lista de BatimentoBolsa 
        /// com as divergências encontradas de quantidade e/ou valor operados.
        /// </summary>
        /// <param name="ordemBolsaCollection"></param>
        /// <param name="negsCollection"></param>
        /// <returns></returns>
        /// <exception cref="FatorCotacaoNaoCadastradoException">Se Não existe fatorCotação para algum ativo da Collection de ordemBolsa</exception>
        /// <exception cref="ArgumentException">Se Não existe IdCliente em algum item da Collection de Negs</exception>
        public List<BatimentoOrdemBolsa> ProcessaBatimentoNegs(OrdemBolsaCollection ordemBolsaCollection, NegsCollection negsCollection) {
            for (int i = 0; i < negsCollection.CollectionNegs.Count; i++) {
                if (!negsCollection.CollectionNegs[i].ExtendAtributos.IdCliente.HasValue) {
                    throw new ArgumentException("IdCliente de Negs deve Conter Valor");
                }
            }
                        
            List<BatimentoOrdemBolsa> listaBatimentoBolsa = new List<BatimentoOrdemBolsa>();
            
            foreach (OrdemBolsa ordemBolsa in ordemBolsaCollection) {
                #region Negs Está na Collection de OrdemBolsa
                int idCliente = ordemBolsa.IdCliente.Value;
                string cdAtivoBolsa = ordemBolsa.CdAtivoBolsa.Trim();
                string tipoOperacao = ordemBolsa.TipoOrdem.Trim();
                decimal quantidade = ordemBolsa.Quantidade.Value;
                decimal valor = ordemBolsa.Valor.Value;

                Negs negs = negsCollection.CollectionNegs.Find(
                        delegate(Negs neg1) {
                            return neg1.ExtendAtributos.IdCliente.Value == idCliente &&
                                   neg1.CdAtivoBolsa == cdAtivoBolsa &&
                                   neg1.TipoOperacao.Trim() == tipoOperacao;
                        }
                    );

                // Negs não está em OrdemBolsa
                if (negs == null) {
                    #region Cria BatimentoOrdemBolsa
                    BatimentoOrdemBolsa batimentoOrdemBolsa = new BatimentoOrdemBolsa();
                    batimentoOrdemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    batimentoOrdemBolsa.IdCliente = idCliente;
                    //
                    batimentoOrdemBolsa.QuantidadeCarga = 0;
                    batimentoOrdemBolsa.ValorCarga = 0;
                    //
                    batimentoOrdemBolsa.QuantidadeFinancial = quantidade;
                    batimentoOrdemBolsa.ValorFinancial = valor;
                    //
                    batimentoOrdemBolsa.TipoOperacao = tipoOperacao;

                    // Adiciona na Lista de Inconsistencia
                    listaBatimentoBolsa.Add(batimentoOrdemBolsa);

                    #endregion                    
                }
                // Negs Está em Ordem Bolsa
                else {
                    // Se existe Diferença
                    if (negs.Quantidade.HasValue && negs.Quantidade != quantidade ||
                        negs.ExtendAtributos.Valor.HasValue && negs.ExtendAtributos.Valor != valor) {

                        #region Cria BatimentoBolsa
                        BatimentoOrdemBolsa batimentoOrdemBolsa = new BatimentoOrdemBolsa();
                        batimentoOrdemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        batimentoOrdemBolsa.IdCliente = idCliente;
                        batimentoOrdemBolsa.QuantidadeCarga = Convert.ToDecimal(negs.Quantidade);
                        batimentoOrdemBolsa.QuantidadeFinancial = quantidade;
                        batimentoOrdemBolsa.ValorCarga = negs.ExtendAtributos.Valor.Value;
                        batimentoOrdemBolsa.ValorFinancial = valor;
                        //
                        batimentoOrdemBolsa.TipoOperacao = tipoOperacao;

                        // Adiciona na Lista de Inconsistencia
                        listaBatimentoBolsa.Add(batimentoOrdemBolsa);
                        #endregion
                    }
                }
                #endregion
            }

            for(int i = 0; i < negsCollection.CollectionNegs.Count; i++) {
                #region OrdemBolsa Está na Collection de Negs

                int idCliente = negsCollection.CollectionNegs[i].ExtendAtributos.IdCliente.Value;
                string cdAtivoBolsa = negsCollection.CollectionNegs[i].CdAtivoBolsa.Trim();
                string tipoOperacao = negsCollection.CollectionNegs[i].TipoOperacao.Trim();
                decimal quantidade = negsCollection.CollectionNegs[i].Quantidade.Value;
                decimal valor = negsCollection.CollectionNegs[i].ExtendAtributos.Valor.Value;
                
                // Se Objeto comparado por IdCliente/CdAtivo/TipoOperacao está na Collection
                OrdemBolsa ordemBolsa = new OrdemBolsa();
                ordemBolsa.IdCliente = idCliente;
                ordemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                ordemBolsa.TipoOrdem = tipoOperacao;
                // Está na Collection
                if (ordemBolsaCollection.Contains(ordemBolsa)) {
                    int index = ordemBolsaCollection.IndexOf(idCliente, cdAtivoBolsa, tipoOperacao);
                    if (index != -1) {
                        // Se existe Diferença
                        if (ordemBolsaCollection[index].Quantidade.HasValue && ordemBolsaCollection[index].Quantidade != quantidade ||
                           ordemBolsaCollection[index].Valor.HasValue && ordemBolsaCollection[index].Valor != valor) {

                            #region Cria BatimentoBolsa
                            BatimentoOrdemBolsa batimentoOrdemBolsa = new BatimentoOrdemBolsa();
                            batimentoOrdemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                            batimentoOrdemBolsa.IdCliente = idCliente;
                            batimentoOrdemBolsa.QuantidadeCarga = quantidade;
                            batimentoOrdemBolsa.QuantidadeFinancial = Convert.ToDecimal(ordemBolsaCollection[index].Quantidade);
                            batimentoOrdemBolsa.ValorCarga = valor;
                            batimentoOrdemBolsa.ValorFinancial = ordemBolsaCollection[index].Valor.Value;
                            //
                            batimentoOrdemBolsa.TipoOperacao = tipoOperacao;
                            //
                            // Adiciona na Lista de Inconsistência somente se não existir Registro Igual
                            if (listaBatimentoBolsa.FindAll(batimentoOrdemBolsa.Exists).Count == 0) {
                                listaBatimentoBolsa.Add(batimentoOrdemBolsa);
                            }
                            #endregion
                        }
                    }
                }
                // Não Está na Collection
                else {
                    #region Cria BatimentoOrdemBolsa
                    // Adiciona Informações de Negs
                    BatimentoOrdemBolsa batimentoOrdemBolsa = new BatimentoOrdemBolsa();
                    batimentoOrdemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    batimentoOrdemBolsa.IdCliente = idCliente;
                    batimentoOrdemBolsa.QuantidadeCarga = quantidade;
                    batimentoOrdemBolsa.ValorCarga = valor;
                    //
                    batimentoOrdemBolsa.QuantidadeFinancial = 0;
                    batimentoOrdemBolsa.ValorFinancial = 0;
                    //
                    batimentoOrdemBolsa.TipoOperacao = tipoOperacao;
                    
                    // Adiciona na Lista de Inconsistencia somente se não exitir registro Igual                    
                    if (listaBatimentoBolsa.FindAll(batimentoOrdemBolsa.Exists).Count == 0) {
                        listaBatimentoBolsa.Add(batimentoOrdemBolsa);
                    }
                    #endregion                    
                }
                #endregion
            }

            return listaBatimentoBolsa;
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N'.
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudo(int idCliente, DateTime data, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OrdemBolsa ordemBolsa = new OrdemBolsa();
            ordemBolsa.Query.Select(ordemBolsa.Query.Valor.Sum());
            ordemBolsa.Query.Where(ordemBolsa.Query.IdCliente.Equal(idCliente),
                                    ordemBolsa.Query.Data.Equal(data),
                                    ordemBolsa.Query.CalculaDespesas.Equal("N"),
                                    ordemBolsa.Query.Fonte.NotIn((byte)FonteOrdemBolsa.Sinacor));
            if (ordemBolsa.Query.Load())
            {
                if (ordemBolsa.Valor.HasValue)
                {
                    totalValor = ordemBolsa.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            ordemBolsaCollection.Query.Where(ordemBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                ordemBolsaCollection.Query.Data.Equal(data),
                                                ordemBolsaCollection.Query.CalculaDespesas.Equal("N"),
                                                ordemBolsaCollection.Query.Fonte.NotIn((byte)FonteOrdemBolsa.Sinacor));
            ordemBolsaCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < ordemBolsaCollection.Count; i++)
            {
                ordemBolsa = new OrdemBolsa();
                ordemBolsa = ordemBolsaCollection[i];
                decimal valor = ordemBolsa.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima ordem da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima ordem
                if (i == ordemBolsaCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }

                ordemBolsa.Corretagem = corretagem;
                ordemBolsa.Emolumento = taxas;
                ordemBolsa.RegistroBolsa = 0;
                ordemBolsa.LiquidacaoCBLC = 0;
                ordemBolsa.RegistroCBLC = 0;
            }
            #endregion

            // Salva a collection de operações
            ordemBolsaCollection.Save();
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N'.
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tiposMercado"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudo(int idCliente, DateTime data, List<string> tiposMercado, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OrdemBolsa ordemBolsa = new OrdemBolsa();
            ordemBolsa.Query.Select(ordemBolsa.Query.Valor.Sum());
            ordemBolsa.Query.Where(ordemBolsa.Query.IdCliente.Equal(idCliente),
                                    ordemBolsa.Query.Data.Equal(data),
                                    ordemBolsa.Query.CalculaDespesas.Equal("N"),
                                    ordemBolsa.Query.Fonte.NotIn((byte)FonteOrdemBolsa.Sinacor),
                                    ordemBolsa.Query.TipoMercado.In(tiposMercado));
            if (ordemBolsa.Query.Load())
            {
                if (ordemBolsa.Valor.HasValue)
                {
                    totalValor = ordemBolsa.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            ordemBolsaCollection.Query.Where(ordemBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                ordemBolsaCollection.Query.Data.Equal(data),
                                                ordemBolsaCollection.Query.CalculaDespesas.Equal("N"),
                                                ordemBolsaCollection.Query.Fonte.NotIn((byte)FonteOrdemBolsa.Sinacor),
                                                ordemBolsaCollection.Query.TipoMercado.In(tiposMercado));
            ordemBolsaCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < ordemBolsaCollection.Count; i++)
            {
                ordemBolsa = new OrdemBolsa();
                ordemBolsa = ordemBolsaCollection[i];
                decimal valor = ordemBolsa.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima ordem da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima ordem
                if (i == ordemBolsaCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }

                ordemBolsa.Corretagem = corretagem;
                ordemBolsa.Emolumento = taxas;
                ordemBolsa.RegistroBolsa = 0;
                ordemBolsa.LiquidacaoCBLC = 0;
                ordemBolsa.RegistroCBLC = 0;
            }
            #endregion

            // Salva a collection de operações
            ordemBolsaCollection.Save();
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N'.
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idAgente"></param>
        /// <param name="tiposMercado"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudo(int idCliente, DateTime data, int idAgente, List<string> tiposMercado, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OrdemBolsa ordemBolsa = new OrdemBolsa();
            ordemBolsa.Query.Select(ordemBolsa.Query.Valor.Sum());
            ordemBolsa.Query.Where(ordemBolsa.Query.IdCliente.Equal(idCliente),
                                    ordemBolsa.Query.Data.Equal(data),
                                    ordemBolsa.query.IdAgenteCorretora.Equal(idAgente),
                                    ordemBolsa.Query.CalculaDespesas.Equal("N"),
                                    ordemBolsa.Query.Fonte.NotIn((byte)FonteOrdemBolsa.Sinacor),
                                    ordemBolsa.Query.TipoMercado.In(tiposMercado));
            if (ordemBolsa.Query.Load())
            {
                if (ordemBolsa.Valor.HasValue)
                {
                    totalValor = ordemBolsa.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            ordemBolsaCollection.Query.Where(ordemBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                ordemBolsaCollection.Query.Data.Equal(data),
                                                ordemBolsaCollection.Query.IdAgenteCorretora.Equal(idAgente),
                                                ordemBolsaCollection.Query.CalculaDespesas.Equal("N"),
                                                ordemBolsaCollection.Query.Fonte.NotIn((byte)FonteOrdemBolsa.Sinacor),
                                                ordemBolsaCollection.Query.TipoMercado.In(tiposMercado));
            ordemBolsaCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < ordemBolsaCollection.Count; i++)
            {
                ordemBolsa = new OrdemBolsa();
                ordemBolsa = ordemBolsaCollection[i];
                decimal valor = ordemBolsa.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima ordem da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima ordem
                if (i == ordemBolsaCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }

                ordemBolsa.Corretagem = corretagem;
                ordemBolsa.Emolumento = taxas;
                ordemBolsa.RegistroBolsa = 0;
                ordemBolsa.LiquidacaoCBLC = 0;
                ordemBolsa.RegistroCBLC = 0;
            }
            #endregion

            // Salva a collection de operações
            ordemBolsaCollection.Save();
        }

        private void CheckValidData()
        {
            this.CheckValidData(this.IdCliente.Value, this.Data.Value);
        }

        private void CheckValidData(int idCliente, DateTime data)
        {
            //Verifica se a Ordem pode ser salva

            //Criterio: DataOrdem nao pode ser anterior a data cliente ou entao é no mesmo dia e o cliente não está fechado

            Cliente clienteCheck = new Cliente();
            clienteCheck.LoadByPrimaryKey(idCliente);

            if ((data < clienteCheck.DataDia.Value) || (data == clienteCheck.DataDia.Value && clienteCheck.Status.Value == (byte)StatusCliente.Divulgado))
            {
                throw new Exception("\nOrdem não foi processada. Problema: Data informada anterior à data do cliente ou status do cliente fechado.");
            }
        }

        /// <summary>
        /// Rateia corretagem e taxas a partir dos valores informados do dia.
        /// Somente para operações com CalculaDespesas = 'N'.
        /// As taxas são jogadas todas em Emolumento, o resto fica zerado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="corretagem"></param>
        /// <param name="taxas"></param>
        public void RateiaTaxasTudoPorIds(List<int> listaIds, decimal totalCorretagem, decimal totalTaxas)
        {
            decimal corretagemResidual = totalCorretagem;
            decimal taxasResidual = totalTaxas;
            decimal totalValor = 0;

            OrdemBolsa ordemBolsa = new OrdemBolsa();
            ordemBolsa.Query.Select(ordemBolsa.Query.Valor.Sum());
            ordemBolsa.Query.Where(ordemBolsa.Query.IdOrdem.In(listaIds));
            if (ordemBolsa.Query.Load())
            {
                if (ordemBolsa.Valor.HasValue)
                {
                    totalValor = ordemBolsa.Valor.Value;
                }
            }
            else
            {
                return;
            }

            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            ordemBolsaCollection.Query.Where(ordemBolsaCollection.Query.IdOrdem.In(listaIds));
            ordemBolsaCollection.Query.Load();

            #region Rateia Taxas
            for (int i = 0; i < ordemBolsaCollection.Count; i++)
            {
                ordemBolsa = new OrdemBolsa();
                ordemBolsa = ordemBolsaCollection[i];
                decimal valor = ordemBolsa.Valor.Value;

                // calculo das proporções
                decimal corretagem = Utilitario.Truncate(((totalCorretagem * valor) / totalValor), 2);
                decimal taxas = Utilitario.Truncate(((totalTaxas * valor) / totalValor), 2);

                corretagemResidual -= corretagem;
                taxasResidual -= taxas;

                // Se é a ultima ordem da corretora, garante-se que 100% da corretagem e taxas
                // foram distribuidas, jogando todo o residual para a ultima ordem
                if (i == ordemBolsaCollection.Count - 1)
                {
                    corretagem += corretagemResidual;
                    taxas += taxasResidual;
                }

                ordemBolsa.Corretagem = corretagem;
                ordemBolsa.Emolumento = taxas;
                ordemBolsa.RegistroBolsa = 0;
                ordemBolsa.LiquidacaoCBLC = 0;
                ordemBolsa.RegistroCBLC = 0;
                ordemBolsa.CalculaDespesas = "N";
            }
            #endregion

            // Salva a collection de operações
            ordemBolsaCollection.Save();
        }
    }
}