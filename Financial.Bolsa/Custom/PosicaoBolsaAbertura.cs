﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common.Enums;
using Financial.Common;

namespace Financial.Bolsa
{
	public partial class PosicaoBolsaAbertura : esPosicaoBolsaAbertura
	{
        /// <summary>
        /// Retorna a quantidade em posição do ativo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadeAtivo(int idCliente, string cdAtivoBolsa, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                        this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();
                        
            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>        
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico) {
            this.QueryReset();
            this.Query
                 .Select(this.query.ValorMercado.Sum())
                 .Where(this.query.IdCliente == idCliente &&
                        this.query.DataHistorico == dataHistorico);

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime dataHistorico) {
            
            PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
            posicaoBolsaAberturaCollection.Query.Select(
                                            posicaoBolsaAberturaCollection.Query.CdAtivoBolsa, 
                                            posicaoBolsaAberturaCollection.Query.ValorMercado.Sum());

            posicaoBolsaAberturaCollection.Query.Where(posicaoBolsaAberturaCollection.Query.IdCliente == idCliente &&
                                                       posicaoBolsaAberturaCollection.Query.DataHistorico == dataHistorico);
            //
            posicaoBolsaAberturaCollection.Query.GroupBy(posicaoBolsaAberturaCollection.Query.CdAtivoBolsa);

            posicaoBolsaAberturaCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoBolsaAberturaCollection.Count > 0) {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, dataHistorico);
            }

            foreach (PosicaoBolsaAbertura posicaoBolsaAbertura in posicaoBolsaAberturaCollection) {
                string cdAtivoBolsa = posicaoBolsaAbertura.CdAtivoBolsa;
                decimal valor = posicaoBolsaAbertura.ValorMercado.Value;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.IdMoeda);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                int idMoedaAtivo = ativoBolsa.IdMoeda.Value;

                if (idMoedaCliente != idMoedaAtivo) {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real) {
                        fatorConversao = 1 / ptax;
                    }
                    else {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load()) {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, dataHistorico);
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }
	}
}
