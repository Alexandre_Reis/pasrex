﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Bolsa
{
	public partial class DistribuicaoBolsa : esDistribuicaoBolsa
	{
        /// <summary>
        /// Carrega o objeto DistribuicaoBolsa com os campos Distribuicao, CdAtivoBolsa, DataReferencia.
        /// A data de referência é filtrada para todas as datas anteriores ou iguais a ela.
        /// </summary>
        /// <param name="cdAtivo"></param>
        /// <param name="dataReferencia"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaDistribuicaoBolsa(string cdAtivoBolsa, DateTime dataReferencia)
        {
            if (cdAtivoBolsa.Length >= 14) //Se for Termo, usa o fator cotação da ação correspondente
            {
                cdAtivoBolsa = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa);
            }

            DistribuicaoBolsaCollection distribuicaoBolsaCollection = new DistribuicaoBolsaCollection();

            // Busca todos os menores que a data de referencia ordenado decrescente pela data
            distribuicaoBolsaCollection.Query
                 .Select(distribuicaoBolsaCollection.Query.Distribuicao,
                         distribuicaoBolsaCollection.Query.CdAtivoBolsa,
                         distribuicaoBolsaCollection.Query.DataReferencia)
                 .Where(distribuicaoBolsaCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia),
                        distribuicaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa)
                 .OrderBy(distribuicaoBolsaCollection.Query.DataReferencia.Descending);

            distribuicaoBolsaCollection.Query.Load();

            if (distribuicaoBolsaCollection.HasData)
            {
                // Copia informações de DistribuicaoBolsaCollection para DistribuicaoBolsa
                this.AddNew();
                this.Distribuicao = ((DistribuicaoBolsa)distribuicaoBolsaCollection[0]).Distribuicao;
                this.CdAtivoBolsa = ((DistribuicaoBolsa)distribuicaoBolsaCollection[0]).CdAtivoBolsa;
                this.DataReferencia = ((DistribuicaoBolsa)distribuicaoBolsaCollection[0]).DataReferencia;
            }
            else
            {
                // Retorna um fator default = 0
                this.AddNew();
                this.Distribuicao = 0;
                this.CdAtivoBolsa = cdAtivoBolsa;
                this.DataReferencia = new DateTime(2000, 1, 1);
            }

            return true;
        }
	}
}
