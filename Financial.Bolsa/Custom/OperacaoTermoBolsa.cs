﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Exceptions;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Util.Enums;
using Financial.Bolsa.Enums;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Investidor.Enums;
using Financial.Security;
using System.Collections;

namespace Financial.Bolsa
{
    public partial class OperacaoTermoBolsa : esOperacaoTermoBolsa
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// throws OperacaoTermoBolsaInexistente
        /// QuantidadeInsuficienteVendaTermo se quantidadePosicao + quantidadeOperacao < quantidadeTermo
        public void ProcessaOperacaoTermo(int idCliente, DateTime data)
        {
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();

            operacaoBolsaCollection.BuscaOperacaoTermo(idCliente, data);

            for (int i = 0; i < operacaoBolsaCollection.Count; i++)
            {
                OperacaoBolsa operacaoBolsa = operacaoBolsaCollection[i];

                #region Valores da Operacao
                int idOperacao = operacaoBolsa.IdOperacao.Value;
                string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                int idAgente = operacaoBolsa.IdAgenteLiquidacao.Value;
                string tipoOperacao = operacaoBolsa.TipoOperacao;
                decimal quantidade = operacaoBolsa.Quantidade.Value;
                decimal puOperacao = operacaoBolsa.Pu.Value;
                decimal puLiquido = operacaoBolsa.PULiquido.Value;
                decimal valorLiquido = operacaoBolsa.ValorLiquido.Value;
                decimal valorTermo = operacaoBolsa.Valor.Value;
                #endregion

                #region Exceção OperacaoTermoBolsaInexistente
                if (!operacaoBolsa.OperacaoTermoBolsa.es.HasData)
                {
                    throw new OperacaoTermoBolsaInexistente("OperacaoTermoBolsa não associada a OperacaoBolsa id: " + idOperacao);
                }
                #endregion

                #region Valores de OperacaoTermo
                // Hierarquico
                int idOperacaoTermo = operacaoBolsa.OperacaoTermoBolsa.IdOperacao.Value;
                decimal taxa = operacaoBolsa.OperacaoTermoBolsa.Taxa.Value;
                string numeroContrato = operacaoBolsa.OperacaoTermoBolsa.NumeroContrato;
                short? idIndice = null;
                if (operacaoBolsa.OperacaoTermoBolsa.IdIndice.HasValue)
                {
                    idIndice = operacaoBolsa.OperacaoTermoBolsa.IdIndice.Value;
                }
                DateTime dataVencimento = operacaoBolsa.OperacaoTermoBolsa.DataVencimento.Value;
                #endregion

                int prazoTermo = Calendario.NumeroDias(data, dataVencimento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                //
                //AtivoBolsa ativoBolsa = new AtivoBolsa();
                string cdAtivoBolsaTermo = cdAtivoBolsa;
                // Converte de Termo para Ação
                string cdAtivoBolsaAcao = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsaTermo);
                //                
                decimal puCustoLiquidoAcao = 0;
                decimal rendimentoTotal = 0;
                decimal valorMercado;
                decimal puMercado;
                decimal valorCurva;
                if (operacaoBolsa.IsTipoVenda())
                {
                    decimal quantidadePosicao = 0;
                    decimal quantidadeOperacao = 0;
                    decimal quantidadeTermo = quantidade;

                    #region Valores de Posicao
                    decimal valorCustoLiquido = 0;
                    decimal puCustoLiquidoPosicao = 0;
                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                    if (posicaoBolsa.BuscaPosicaoBolsa(idCliente, idAgente, cdAtivoBolsaAcao))
                    {
                        quantidadePosicao = posicaoBolsa.Quantidade.Value;
                        valorCustoLiquido = posicaoBolsa.ValorCustoLiquido.Value;
                        puCustoLiquidoPosicao = posicaoBolsa.PUCustoLiquido.Value;
                    }
                    #endregion

                    #region Valores de Operacao
                    decimal corretagem = 0;
                    decimal emolumento = 0;
                    decimal registroBolsa = 0;
                    decimal registroCBLC = 0;
                    decimal liquidacaoCBLC = 0;
                    decimal valor = 0;
                    OperacaoBolsa operacaoBolsaAux = new OperacaoBolsa();
                    operacaoBolsaAux.BuscaTotaisOperados(idCliente, data, cdAtivoBolsaAcao, idAgente, TipoOperacaoBolsa.Compra);
                    if (operacaoBolsaAux.es.HasData)
                    {
                        quantidadeOperacao = operacaoBolsaAux.Quantidade.Value;
                        corretagem = operacaoBolsaAux.Corretagem.Value;
                        emolumento = operacaoBolsaAux.Emolumento.Value;
                        registroBolsa = operacaoBolsaAux.RegistroBolsa.Value;
                        registroCBLC = operacaoBolsaAux.RegistroCBLC.Value;
                        liquidacaoCBLC = operacaoBolsaAux.LiquidacaoCBLC.Value;
                        valor = operacaoBolsaAux.Valor.Value;
                    }
                    #endregion

                    #region Processamento para Venda de Termo
                    // Se não tem a taxa informada na operação calcula-se a taxa
                    decimal totalDespesas = 0;
                    decimal totalQuantidadeOperado = 0;
                    decimal totalValorOperado = 0;

                    #region Cálculo da taxa implícita
                    if (taxa == 0)
                    {
                        totalDespesas = corretagem + emolumento + registroBolsa + registroCBLC + liquidacaoCBLC;
                        totalQuantidadeOperado = quantidadeOperacao;
                        totalValorOperado = valor;
                        //
                        FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                        if (!fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsaAcao, data))
                        {
                            if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                            {
                                AtivoBolsa ativoBolsa = new AtivoBolsa();
                                if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsaAcao))
                                {
                                    throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsaAcao + " " + data.ToString("d"));
                                }
                            }
                        }

                        if (totalQuantidadeOperado + quantidadePosicao == 0)
                        {
                            puCustoLiquidoAcao = 0;
                            taxa = 0;
                        }
                        else
                        {
                            if (valorCustoLiquido != 0 && totalValorOperado == 0) //Se não operou na data, pega direto o pu custo da posição
                            {
                                puCustoLiquidoAcao = puCustoLiquidoPosicao;
                            }
                            else
                            {
                                if (quantidade > totalQuantidadeOperado)
                                {
                                    puCustoLiquidoAcao = (totalValorOperado + valorCustoLiquido + totalDespesas) * fatorCotacao.Fator.Value /
                                                                  (totalQuantidadeOperado + quantidadePosicao);
                                }
                                else
                                {
                                    puCustoLiquidoAcao = (totalValorOperado + totalDespesas) * fatorCotacao.Fator.Value / totalQuantidadeOperado;
                                }
                            }

                            //Calcula taxa implícita da operação de termo.
                            //Notar que são descontados 3 dias, em funcao do periodo de liquidacao da compra da acao.
                            try
                            {
                                taxa = Util.CalculoFinanceiro.CalculaTaxaExponencial(prazoTermo - 3, BaseCalculo.Base252, puCustoLiquidoAcao, puLiquido);
                            }
                            catch (Exception)
                            {
                                taxa = 0;
                            }

                            rendimentoTotal = Math.Round((puLiquido - puCustoLiquidoAcao) * quantidade, 2);
                        }
                    }
                    #endregion

                    valorMercado = valorLiquido;
                    valorCurva = valorLiquido;
                    puMercado = puLiquido;
                    #endregion
                }
                else
                {
                    #region Processamento para Compra de Termo
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsaAcao, data);

                    if (cotacaoBolsa.es.HasData && cotacaoBolsa.PUMedio.HasValue)
                    {
                        puCustoLiquidoAcao = cotacaoBolsa.PUMedio.Value;
                    }

                    // Operacoes de Termo Compradas não apropriam juros
                    taxa = 0;
                    valorMercado = valorLiquido;
                    valorCurva = valorLiquido;
                    puMercado = puLiquido;
                    #endregion
                }

                #region Insercao PosicaoTermoBolsa
                if (operacaoBolsa.IsTipoVenda())
                {
                    quantidade = quantidade * -1;
                    valorTermo = valorTermo * -1;
                    valorLiquido = valorLiquido * -1;
                    valorMercado = valorMercado * -1;
                    valorCurva = valorCurva * -1;
                }
                PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                posicaoTermoBolsa.DataOperacao = data;
                posicaoTermoBolsa.DataVencimento = dataVencimento;
                posicaoTermoBolsa.Quantidade = quantidade;
                posicaoTermoBolsa.QuantidadeInicial = quantidade;
                posicaoTermoBolsa.PrazoTotal = (short)prazoTermo;
                posicaoTermoBolsa.PrazoDecorrer = (short)prazoTermo;
                posicaoTermoBolsa.TaxaAno = taxa;
                posicaoTermoBolsa.ValorTermo = valorTermo;
                posicaoTermoBolsa.ValorTermoLiquido = valorLiquido;
                posicaoTermoBolsa.ValorMercado = valorMercado;
                posicaoTermoBolsa.ValorCurva = valorCurva;
                posicaoTermoBolsa.ValorCorrigido = valorTermo;
                posicaoTermoBolsa.PUCustoLiquidoAcao = puCustoLiquidoAcao;
                posicaoTermoBolsa.PUTermo = puOperacao;
                posicaoTermoBolsa.PUTermoLiquido = puLiquido;
                posicaoTermoBolsa.PUMercado = puMercado;
                posicaoTermoBolsa.IdOperacao = idOperacao;
                posicaoTermoBolsa.IdAgente = idAgente;
                posicaoTermoBolsa.IdCliente = idCliente;
                posicaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                posicaoTermoBolsa.NumeroContrato = numeroContrato;
                posicaoTermoBolsa.IdIndice = idIndice;
                posicaoTermoBolsa.IdTrader = operacaoBolsa.IdTrader;
                posicaoTermoBolsa.RendimentoTotal = rendimentoTotal;
                posicaoTermoBolsa.RendimentoApropriado = 0;
                posicaoTermoBolsa.RendimentoApropriar = rendimentoTotal;
                posicaoTermoBolsa.RendimentoDia = 0;

                posicaoTermoBolsaCollection.AttachEntity(posicaoTermoBolsa);
                #endregion
            }

            // Save da Collection de posicaoTermoBolsa
            posicaoTermoBolsaCollection.Save();
            Hashtable hsContaAtivo = new Hashtable();
            foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
            {
                int idMoedaAtivo = posicaoTermoBolsa.UpToAtivoBolsaByCdAtivoBolsa.IdMoeda.Value;
                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;

                #region Insercao Liquidacao
                string descricao;
                byte origem;
                decimal valorTermo = posicaoTermoBolsa.ValorTermo.Value * -1;
                if (posicaoTermoBolsa.Quantidade > 0)
                {
                    descricao = "Compra Termo " + Utilitario.Truncate(posicaoTermoBolsa.Quantidade.Value, 2).ToString() +
                                " - " + posicaoTermoBolsa.CdAtivoBolsaAcao;
                    origem = (byte)OrigemLancamentoLiquidacao.Bolsa.CompraTermo;
                }
                else
                {
                    descricao = "Venda Termo " + Utilitario.Truncate(posicaoTermoBolsa.Quantidade.Value, 2).ToString() +
                                " - " + posicaoTermoBolsa.CdAtivoBolsaAcao;
                    origem = (byte)OrigemLancamentoLiquidacao.Bolsa.VendaTermo;
                }

                DateTime dataVencimento = posicaoTermoBolsa.DataVencimento.Value;
                if (!Calendario.IsDiaUtil(dataVencimento, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
                {
                    dataVencimento = Calendario.AdicionaDiaUtil(dataVencimento, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                }

                int idContaDefault = 0;
                #region Busca Conta Default
                if (hsContaAtivo.Contains(cdAtivoBolsa))
                {
                    idContaDefault = (int)hsContaAtivo[cdAtivoBolsa];
                }
                else
                {
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                    hsContaAtivo.Add(cdAtivoBolsa, idContaDefault);
                }
                #endregion

                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataVencimento;
                liquidacao.Descricao = descricao;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdAgente = posicaoTermoBolsa.IdAgente.Value;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaDefault;
                liquidacao.IdentificadorOrigem = posicaoTermoBolsa.IdPosicao.Value;
                liquidacao.Origem = origem;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Valor = valorTermo;
                liquidacaoCollection.AttachEntity(liquidacao);
                #endregion
            }

            liquidacaoCollection.Save();
        }
    }
}
