﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using System.IO;
using Financial.Common;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Common.Enums;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Interfaces.Import.Bolsa;
using Financial.Interfaces.Import.Bolsa.Enums;
using Financial.Investidor;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Investidor.Enums;

namespace Financial.Bolsa {
    public partial class OrdemTermoBolsa : esOrdemTermoBolsa {

        private static readonly ILog log = LogManager.GetLogger(typeof(OrdemTermoBolsa));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conrCollection"></param>
        /// <param name="data"></param>
        /// throws AtivoNaoCadastradoException se codigo do Ativo não estiver cadastrado na Base
        public void CarregaOrdemTermoConr(ConrCollection conrCollection, DateTime data) 
        {
            #region GetIdAgente do Arquivo Conr
            int codigoBovespaAgente = conrCollection.CollectionConr[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            Conr conr = new Conr();
            OrdemTermoBolsaCollection ordemTermoBolsaCollection = new OrdemTermoBolsaCollection();
            //
            OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
            OrdemBolsaCollection ordemBolsaCollectionAux = new OrdemBolsaCollection();

            // Deleta as ordens de acordo com idCliente, idAgente, data, e Fonte = ArquivoConr
            ordemBolsaCollectionAux.DeletaOrdemBolsa(idAgente, data, FonteOrdemBolsa.ArquivoCONR);
            
            using (esTransactionScope scope = new esTransactionScope()) {
                for (int i = 0; i < conrCollection.CollectionConr.Count; i++) {
                    conr = conrCollection.CollectionConr[i];

                    #region Copia informações para OrdemBolsa e OrdemTermoBolsa
                    if (conr.IsTipoRegistroIdentificacaoContratoRegistrado()) {

                        int codigoClienteBovespa = conr.CodigoCliente.Value;
                        CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                        bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
                        
                        if (idClienteExiste && conr.DataRegistro.Value.CompareTo(data) == 0) //Só pega para registros de termo na própria data!!!
                        {
                            int idCliente = codigoClienteAgente.IdCliente.Value;
                            string cdAtivoBolsa = conr.CdAtivoBolsa;
                            DateTime dataVencimento = conr.DataVencimento.Value;

                            Cliente cliente = new Cliente();
                            cliente.LoadByPrimaryKey(idCliente);

                            if (cliente.IsAtivo) //Só importa para clientes ativos!
                            {
                                //Só importa para clientes com dataDia anterior à data passada ou data igual e status != fechado
                                if ((DateTime.Compare(cliente.DataDia.Value, data) < 0) ||
                                            (DateTime.Compare(cliente.DataDia.Value, data) == 0 && cliente.Status.Value != (byte)StatusCliente.Divulgado)) {
                                    // Formato com T e data no formato 'ddmmaaaa'
                                    string cdAtivoBolsaObjeto = cdAtivoBolsa;
                                    StringBuilder dataAtivo = Utilitario.FormataData(dataVencimento, PatternData.formato3);
                                    cdAtivoBolsa = cdAtivoBolsa + "T" + dataAtivo.ToString();

                                    #region Verifica se cdAtivoBolsa existe, e cadastra se nao existir
                                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                                    if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa)) {
                                        AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                                        if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsaObjeto)) {
                                            throw new AtivoNaoCadastradoException("Ativo " + cdAtivoBolsaObjeto + " não cadastrado.");
                                        }

                                        AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                                        ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                                        ativoBolsaNovo.CdAtivoBolsaObjeto = cdAtivoBolsaObjeto;
                                        ativoBolsaNovo.CodigoIsin = ativoBolsaObjeto.CodigoIsin;
                                        ativoBolsaNovo.DataVencimento = dataVencimento;
                                        ativoBolsaNovo.Descricao = ativoBolsaObjeto.Descricao;
                                        ativoBolsaNovo.Especificacao = "*";
                                        ativoBolsaNovo.IdEmissor = 1;
                                        ativoBolsaNovo.IdMoeda = 1;
                                        ativoBolsaNovo.PUExercicio = 0;
                                        ativoBolsaNovo.TipoMercado = TipoMercadoBolsa.Termo;
                                        ativoBolsaNovo.TipoPapel = (byte)TipoPapelAtivo.Normal;
                                        ativoBolsaNovo.NumeroSerie = 0;
                                        ativoBolsaNovo.Save();

                                        FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                                        if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsaObjeto, data)) {
                                            FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                                            fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                                            fatorCotacaoBolsaNovo.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                            fatorCotacaoBolsaNovo.Fator = fatorCotacaoBolsa.Fator.Value;
                                            fatorCotacaoBolsaNovo.Save();
                                        }
                                    }
                                    #endregion

                                    // Preenche OrdemBolsa
                                    #region Preenche OrdemBolsa
                                    OrdemBolsa ordemBolsa = new OrdemBolsa();
                                    ordemBolsa.TipoMercado = TipoMercadoBolsa.Termo;
                                    ordemBolsa.TipoOrdem = Convert.ToString(conr.Tipo.Value);
                                    ordemBolsa.Data = data;
                                    ordemBolsa.Pu = conr.Pu;
                                    ordemBolsa.Valor = conr.Valor;
                                    ordemBolsa.Quantidade = Convert.ToDecimal(conr.Quantidade);
                                    ordemBolsa.Origem = (byte)OrigemOrdemBolsa.Primaria;
                                    ordemBolsa.Fonte = (byte)FonteOrdemBolsa.ArquivoCONR;
                                    ordemBolsa.IdCliente = idCliente;
                                    ordemBolsa.CdAtivoBolsa = cdAtivoBolsa;
                                    ordemBolsa.IdAgenteLiquidacao = idAgente;
                                    ordemBolsa.IdAgenteCorretora = idAgente;
                                    ordemBolsa.CalculaDespesas = "S";
                                    ordemBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                                    #endregion
                                    //
                                    // Preenche OrdemTermoBolsa
                                    #region Preenche OrdemTermoBolsa
                                    OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();
                                    ordemTermoBolsa.NumeroContrato = Convert.ToString(conr.NumeroContrato);
                                    //                    
                                    short? indice = null;
                                    #region Conversão de IdIndice
                                    switch (conr.IndicadorCorrecaoContrato) {
                                        case (int)IndiceCorrecaoContrato.Dolar:
                                            indice = ListaIndiceFixo.PTAX_800VENDA;
                                            break;
                                        case (int)IndiceCorrecaoContrato.TJLP:
                                            indice = ListaIndiceFixo.TJLP;
                                            break;
                                        case (int)IndiceCorrecaoContrato.MoedaCorrente:
                                            indice = null;
                                            break;
                                    }
                                    #endregion

                                    ordemTermoBolsa.IdIndice = indice;
                                    #endregion

                                    using (esTransactionScope scopeAux = new esTransactionScope()) {
                                        ordemBolsa.Save();
                                        // IdOrdem usado no insert
                                        int idOrdem = ordemBolsa.IdOrdem.Value;
                                        ordemTermoBolsa.IdOrdem = idOrdem;
                                        ordemTermoBolsa.Save();
                                        //
                                        scopeAux.Complete();
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
                scope.Complete();
            }
        }
    }
}
