﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa {
    public partial class EventoFisicoBolsaCollection : esEventoFisicoBolsaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(EventoFisicoBolsaCollection));

        /// <summary>
        /// Deleta eventos físicos do dia, dado o tipo evento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoEvento"></param>
        /// throws ArgumentException se Tipo Evento for Diferente dos valores de Enum TipoEvento
        public void DeletaEventoFisicoBolsa(int idCliente, DateTime data, int tipoEvento) {
       
            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            #region ArgumentosNulos - throw ArgumentException
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("TipoEvento incorreto. Os valores possiveis estão no enum TipoEventoBolsa");
                
                List<int> valoresPossiveis = TipoEventoBolsa.Values();
                if (!valoresPossiveis.Contains(tipoEvento)) {
                    throw new ArgumentException(mensagem.ToString());
                }
            #endregion

            this.QueryReset();
            this.Query
              .Select(this.Query.IdEvento)
              .Where(this.Query.Data.Equal(data),
                     this.Query.IdCliente == idCliente,
                     this.Query.TipoEvento == tipoEvento);

            this.Query.Load();
            #region Log Sql
            if (log.IsInfoEnabled) {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@Data1", "'" + data.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@IdCliente2", "'" + idCliente + "'");
                sql = sql.Replace("@TipoEvento3", "'" + tipoEvento + "'");
                log.Info(sql);
            }
            #endregion

            this.MarkAllAsDeleted();
            this.Save();
            
            #region logSaida
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }

        /// <summary>
        /// Carrega o objeto EventoFisicoBolsaCollection com os campos IdEvento, TipoMovimento, Pu, PULiquido,
        /// Quantidade, CdAtivoBolsa, IdAgente, TipoMercado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaEventoFisicoBolsa(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdEvento, this.Query.TipoMovimento, this.Query.Pu, this.Query.PULiquido,
                      this.Query.Quantidade, this.Query.CdAtivoBolsa, this.Query.IdAgente,
                      this.Query.TipoMercado)
              .Where(this.Query.Data.Equal(data),
                     this.Query.IdCliente == idCliente)
              .OrderBy(this.Query.IdEvento.Ascending);

            bool retorno = this.Query.Load();
            
            return retorno;
        }
    }
}
