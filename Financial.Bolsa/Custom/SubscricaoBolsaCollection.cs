﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Bolsa {
    public partial class SubscricaoBolsaCollection : esSubscricaoBolsaCollection 
    {
        /// <summary>
        /// Carrega o objeto SubscricaoBolsaCollection com todos os campos de SubscricaoBolsa.
        /// </summary>        
        /// <param name="data"></param>
        public void BuscaSubscricaoBolsa(DateTime dataEx)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.DataEx.Equal(dataEx))
                 .OrderBy(this.Query.IdSubscricao.Ascending);

            this.Query.Load();
        }

        /// <summary>
        /// Deleta todas as subscrições do dia, dada a fonte informada.
        /// </summary>
        /// <param name="data"></param>        
        /// <param name="fonte"></param>        
        public void DeletaSubscricao(DateTime dataLancamento, int fonte)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdSubscricao)
                 .Where(this.Query.Fonte == fonte,
                        this.Query.DataLancamento.Equal(dataLancamento));
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

    }
}
