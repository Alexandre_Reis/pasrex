﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           4/10/2006 09:34:01
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Exceptions;
using Financial.Common;

namespace Financial.Bolsa {
    public partial class PerfilCorretagemBolsa : esPerfilCorretagemBolsa {
        private static readonly ILog log = LogManager.GetLogger(typeof(PerfilCorretagemBolsa));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <returns></returns>
        /// throws PerfilCorretagemInvalidaException se não encontrar o perfil de corretagem desejado
        public void BuscaPerfilCorretagemBolsa(int idCliente, int idAgente, DateTime dataReferencia, string tipoMercado) 
        {
            PerfilCorretagemBolsaCollection perfilCorretagemBolsaCollection = new PerfilCorretagemBolsaCollection();

            // Busca todos percentuais menores que a data de referencia ordenado decrescente pela data (Prioridade p/ Mercado definido!)
            perfilCorretagemBolsaCollection.Query.Select(perfilCorretagemBolsaCollection.Query.IdTemplate,
                                                         perfilCorretagemBolsaCollection.Query.PercentualDesconto,
                                                         perfilCorretagemBolsaCollection.Query.Iss);
            perfilCorretagemBolsaCollection.Query.Where(perfilCorretagemBolsaCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia),
                                                        perfilCorretagemBolsaCollection.Query.IdCliente == idCliente,
                                                        perfilCorretagemBolsaCollection.Query.IdAgente == idAgente,
                                                        perfilCorretagemBolsaCollection.Query.TipoMercado.Equal(tipoMercado));
            perfilCorretagemBolsaCollection.Query.OrderBy(perfilCorretagemBolsaCollection.Query.DataReferencia.Descending);

            perfilCorretagemBolsaCollection.Query.Load();
            
            if (perfilCorretagemBolsaCollection.HasData) 
            {
                // Copia informações de perfilCorretagemBolsaCollection para PerfilCorretagemBolsa                                
                this.AddNew();
                this.IdTemplate = perfilCorretagemBolsaCollection[0].IdTemplate;
                this.PercentualDesconto = perfilCorretagemBolsaCollection[0].PercentualDesconto;
                this.Iss = perfilCorretagemBolsaCollection[0].Iss;
            }
            else //Procura sem tipo de mercado especifico (geral)
            {
                perfilCorretagemBolsaCollection = new PerfilCorretagemBolsaCollection();

                // Busca todos percentuais menores que a data de referencia ordenado decrescente pela data
                perfilCorretagemBolsaCollection.Query.Select(perfilCorretagemBolsaCollection.Query.IdTemplate,
                                                             perfilCorretagemBolsaCollection.Query.PercentualDesconto,
                                                             perfilCorretagemBolsaCollection.Query.Iss);
                perfilCorretagemBolsaCollection.Query.Where(perfilCorretagemBolsaCollection.Query.DataReferencia.LessThanOrEqual(dataReferencia),
                                                            perfilCorretagemBolsaCollection.Query.IdCliente == idCliente,
                                                            perfilCorretagemBolsaCollection.Query.IdAgente == idAgente,
                                                            perfilCorretagemBolsaCollection.Query.TipoMercado.Equal(""));
                perfilCorretagemBolsaCollection.Query.OrderBy(perfilCorretagemBolsaCollection.Query.DataReferencia.Descending);

                perfilCorretagemBolsaCollection.Query.Load();    

                if (perfilCorretagemBolsaCollection.HasData)
                {
                    // Copia informações de perfilCorretagemBolsaCollection para PerfilCorretagemBolsa                                
                    this.AddNew();
                    this.IdTemplate = perfilCorretagemBolsaCollection[0].IdTemplate;
                    this.PercentualDesconto = perfilCorretagemBolsaCollection[0].PercentualDesconto;
                    this.Iss = perfilCorretagemBolsaCollection[0].Iss;
                }
                else
                {
                    AgenteMercado agenteMercado = new AgenteMercado();
                    agenteMercado.LoadByPrimaryKey(idAgente);

                    StringBuilder mensagemAux = new StringBuilder();
                    mensagemAux.Append("Perfil de corretagem  não cadastrado na tabela PerfilCorretagemBolsa para o cliente " + idCliente + ", corretora " + agenteMercado.Nome)
                               .Append("ou data de Referencia Inválida");
                    throw new PerfilCorretagemNaoCadastradoException(mensagemAux.ToString());
                }
            }
        }
    }
}
