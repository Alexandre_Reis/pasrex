﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Common.Enums;
using Financial.Interfaces.Import.Bolsa;
using Financial.Investidor.Enums;

namespace Financial.Bolsa {
    public partial class BonificacaoBolsa : esBonificacaoBolsa {
        
        /// <summary>
        /// Distribui bonificações lançados na data para mercado a vista, em conversões de termo.
        /// Obs: Não deleta as bonificações já geradas por este método.
        /// </summary>
        /// <param name="data"></param>
        public void DistribuiBonificacao(DateTime data, BonificacaoBolsa bonificacaoBolsa) 
        {            
            AtivoBolsa ativoBolsaDestino = new AtivoBolsa();
            ativoBolsaDestino.LoadByPrimaryKey(bonificacaoBolsa.CdAtivoBolsaDestino);
            string especificacao = ativoBolsaDestino.Especificacao;
            int idEmissor = ativoBolsaDestino.IdEmissor.Value;
            string codigoIsin = ativoBolsaDestino.CodigoIsin;
            string tipoMercado = ativoBolsaDestino.TipoMercado;
            
            if (tipoMercado == TipoMercadoBolsa.MercadoVista)
            {
                #region Deleta todos os eventos de bonificação de ativos que tenham o ativo principal da bonificação como ativo objeto
                BonificacaoBolsaQuery bonificacaoBolsaQuery = new BonificacaoBolsaQuery("B");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
                bonificacaoBolsaQuery.Select(bonificacaoBolsaQuery.IdBonificacao);
                bonificacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == bonificacaoBolsaQuery.CdAtivoBolsa);
                bonificacaoBolsaQuery.Where(ativoBolsaQuery.CdAtivoBolsaObjeto.Equal(bonificacaoBolsa.CdAtivoBolsa));
                bonificacaoBolsaQuery.Where(bonificacaoBolsaQuery.DataLancamento.Equal(bonificacaoBolsa.DataLancamento.Value));

                BonificacaoBolsaCollection bonificacaoBolsaCollectionDeletar = new BonificacaoBolsaCollection();
                bonificacaoBolsaCollectionDeletar.Load(bonificacaoBolsaQuery);
                bonificacaoBolsaCollectionDeletar.MarkAllAsDeleted();
                bonificacaoBolsaCollectionDeletar.Save();
                #endregion

                AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                if (ativoBolsaCollection.BuscaAtivoBolsaTermo(bonificacaoBolsa.CdAtivoBolsa, data)) 
                {
                    for (int j = 0; j < ativoBolsaCollection.Count; j++) 
                    {
                        string cdAtivoBolsa = ativoBolsaCollection[j].CdAtivoBolsa;
                        string cdAtivoBolsaDestino = bonificacaoBolsa.CdAtivoBolsaDestino.Trim() + Utilitario.Right(cdAtivoBolsa.Trim(), 9);
                        
                        #region Verifica se cdAtivoBolsaDestino existe, e cadastra se nao existir
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsaDestino)) 
                        {
                            AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                            ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                            ativoBolsaNovo.CdAtivoBolsaObjeto = bonificacaoBolsa.CdAtivoBolsa;
                            ativoBolsaNovo.CodigoIsin = codigoIsin;
                            ativoBolsaNovo.DataVencimento = ativoBolsaCollection[j].DataVencimento;
                            ativoBolsaNovo.Descricao = ativoBolsaDestino.Descricao;
                            ativoBolsaNovo.Especificacao = especificacao;
                            ativoBolsaNovo.IdEmissor = idEmissor;
                            ativoBolsaNovo.IdMoeda = 1;
                            ativoBolsaNovo.PUExercicio = ativoBolsaCollection[j].PUExercicio;
                            ativoBolsaNovo.TipoMercado = ativoBolsaCollection[j].TipoMercado;                                
                            ativoBolsaNovo.Save();

                            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                            if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(ativoBolsaDestino.CdAtivoBolsa, data))
                            {
                                FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                                fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                                fatorCotacaoBolsaNovo.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                fatorCotacaoBolsaNovo.Fator = fatorCotacaoBolsa.Fator.Value;
                                fatorCotacaoBolsaNovo.Save();
                            }
                        }
                        #endregion

                        #region Cria nova Bonificacao do Termo
                        BonificacaoBolsa novaBonificacaoBolsa = new BonificacaoBolsa();
                        novaBonificacaoBolsa.AddNew();
                        novaBonificacaoBolsa.DataLancamento = bonificacaoBolsa.DataLancamento;
                        novaBonificacaoBolsa.DataEx = bonificacaoBolsa.DataEx;
                        novaBonificacaoBolsa.DataReferencia = bonificacaoBolsa.DataReferencia;
                        novaBonificacaoBolsa.TipoFracao = bonificacaoBolsa.TipoFracao;
                        novaBonificacaoBolsa.PUBonificacao = bonificacaoBolsa.PUBonificacao;                            
                        novaBonificacaoBolsa.Percentual = bonificacaoBolsa.Percentual;
                        novaBonificacaoBolsa.Fonte = bonificacaoBolsa.Fonte;                            
                        novaBonificacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        novaBonificacaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;
                        
                        novaBonificacaoBolsa.InsereBonificacaoBolsa(novaBonificacaoBolsa);
                        #endregion
                    }
                }
            }
                        
        }

        /// <summary>
        /// Método de inserção de Bonificacao
        /// </summary>
        /// <param name="bonificacaoBolsa">bonificacao com os dados a inserir</param>  
        /// throws ArgumentNullException se campos Obrigatórios da inclusão estiverem nulos
        /// bonificacaoBolsa.DataLancamento ou bonificacaoBolsa.DataAtualizacao ou
        /// bonificacaoBolsa.DataEx
        /// bonificacaoBolsa.TipoFracao ou bonificacaoBolsa.PUBonificacao
        /// bonificacaoBolsa.CdAtivoBolsa ou bonificacaoBolsa.Percentual
        /// bonificacaoBolsa.CdAtivoBolsaDestino ou bonificacaoBolsa.Fonte
        public void InsereBonificacaoBolsa(BonificacaoBolsa bonificacaoBolsa) 
        {
            #region ArgumentosNulos - throw ArgumentNullException
            if (!bonificacaoBolsa.DataLancamento.HasValue ||
               !bonificacaoBolsa.DataEx.HasValue ||
               !bonificacaoBolsa.DataReferencia.HasValue ||
               !bonificacaoBolsa.TipoFracao.HasValue ||
               !bonificacaoBolsa.PUBonificacao.HasValue ||
               String.IsNullOrEmpty(bonificacaoBolsa.CdAtivoBolsa) ||
               !bonificacaoBolsa.Percentual.HasValue ||
               String.IsNullOrEmpty(bonificacaoBolsa.CdAtivoBolsaDestino) ||
               !bonificacaoBolsa.Fonte.HasValue ) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("bonificacaoBolsa.DataLancamento ou ")
                .Append("bonificacaoBolsa.DataEx ou ")
                .Append("bonificacaoBolsa.DataReferencia ou ")
                .Append("bonificacaoBolsa.TipoFracao ou ")
                .Append("bonificacaoBolsa.PUBonificacao ou ")
                .Append("bonificacaoBolsa.CdAtivoBolsa ou ")
                .Append("bonificacaoBolsa.Percentual ou ")
                .Append("bonificacaoBolsa.CdAtivoBolsaDestino ou ")
                .Append("bonificacaoBolsa.Fonte ");
                
                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            BonificacaoBolsa bonificacaoBolsaInserir = new BonificacaoBolsa();
            bonificacaoBolsaInserir.AddNew();
            //            
            // Campos Obrigatórios para a inserção
            bonificacaoBolsaInserir.DataLancamento = bonificacaoBolsa.DataLancamento;
            bonificacaoBolsaInserir.DataEx = bonificacaoBolsa.DataEx;      
            bonificacaoBolsaInserir.DataReferencia = bonificacaoBolsa.DataReferencia;
            bonificacaoBolsaInserir.TipoFracao = bonificacaoBolsa.TipoFracao;
            bonificacaoBolsaInserir.PUBonificacao = bonificacaoBolsa.PUBonificacao;
            bonificacaoBolsaInserir.CdAtivoBolsa = bonificacaoBolsa.CdAtivoBolsa;
            bonificacaoBolsaInserir.Percentual = bonificacaoBolsa.Percentual;
            bonificacaoBolsaInserir.CdAtivoBolsaDestino = bonificacaoBolsa.CdAtivoBolsaDestino;      
            bonificacaoBolsaInserir.Fonte = bonificacaoBolsa.Fonte;      
            //    
            bonificacaoBolsaInserir.Save();
        }

        /// <summary>
        /// Carrega BonificacaoBolsa a partir da estrutura importada do arquivo PROD.
        /// </summary>
        /// <param name="prodCollection"></param>
        /// <param name="data"></param>
        /// throws AtivoNaoCadastradoException se cdAtivoBolsa não existir
        public void CarregaProdBonificacao(ProdCollection prodCollection, DateTime data) 
        {
            // Pega os Proventos do Arquivo de Prod                          
            Prod prod = new Prod();
            ProdCollection prodCollectionSubList = new ProdCollection();
            
            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoBonificacao);

            //Deleta os eventos de bonificação gerados a partir do arquivo PROD na data
            BonificacaoBolsaCollection bonificacaoBolsaCollectionDeletar = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollectionDeletar.DeletaBonificacao(data, (int)FonteProventoBolsa.ArquivoPROD);

            BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
            //
            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++) {
                prod = prodCollectionSubList.CollectionProd[i];

                string codigoIsinDestino = prod.CodigoIsinDestino;
                DateTime dataEx = prod.DataEx.Value;
                //
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (ativoBolsa.BuscaCdAtivoBolsa(codigoIsinDestino)) //Se não tem codigo Isin cadastrado, não importa!
                {
                    string cdAtivoBolsaDestino = ativoBolsa.CdAtivoBolsa;

                    DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    AtivoBolsa ativoBolsaAux = new AtivoBolsa();
                    string cdAtivoBolsa = prod.CdAtivoBolsa;
                    if (!ativoBolsaAux.LoadByPrimaryKey(cdAtivoBolsa)) 
                    {
                        AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                        ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                        ativoBolsaNovo.CodigoIsin = ""; //Não tem como buscar
                        ativoBolsaNovo.Descricao = cdAtivoBolsa;
                        ativoBolsaNovo.Especificacao = "*";
                        ativoBolsaNovo.IdEmissor = 1; //Assume qq emissor!
                        ativoBolsaNovo.IdMoeda = 1; //Assume qq moeda!
                        ativoBolsaNovo.PUExercicio = 0;
                        ativoBolsaNovo.TipoMercado = TipoMercadoBolsa.MercadoVista;
                        ativoBolsaNovo.TipoPapel = (byte)TipoPapelAtivo.Normal;
                        ativoBolsaNovo.NumeroSerie = 0;
                        ativoBolsaNovo.Save();

                        FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                        fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                        fatorCotacaoBolsaNovo.DataReferencia = data;
                        fatorCotacaoBolsaNovo.Fator = 1M; //Assume 1!
                        fatorCotacaoBolsaNovo.Save();
                    }

                    #region Cria Bonificacao
                    BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();
                    bonificacaoBolsa.AddNew();
                    bonificacaoBolsa.DataLancamento = data;
                    bonificacaoBolsa.DataEx = dataEx;
                    bonificacaoBolsa.DataReferencia = dataReferencia;
                    bonificacaoBolsa.TipoFracao = Convert.ToByte(prod.IndicadorTratamentoFracao);
                    bonificacaoBolsa.PUBonificacao = 0;
                    bonificacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    bonificacaoBolsa.Percentual = prod.ValorProvento;
                    bonificacaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;
                    bonificacaoBolsa.Fonte = (byte)FonteProventoBolsa.ArquivoPROD;
                    #endregion

                    bonificacaoBolsaCollection.AttachEntity(bonificacaoBolsa);
                }
            }

            bonificacaoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega BonificacaoBolsa a partir da estrutura importada do arquivo PROD.
        /// Não considera foreign Key Ativobolsa
        /// Importa mesmo se não existir AtivoBolsa
        /// </summary>
        /// <param name="prodCollection"></param>
        /// <param name="data"></param>
        public void CarregaProdBonificacaoSemFK(ProdCollection prodCollection, DateTime data) {
            // Pega os Proventos do Arquivo de Prod                          
            Prod prod = new Prod();
            ProdCollection prodCollectionSubList = new ProdCollection();

            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoBonificacao);

            //Deleta os eventos de bonificação gerados a partir do arquivo PROD na data
            BonificacaoBolsaCollection bonificacaoBolsaCollectionDeletar = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollectionDeletar.DeletaBonificacao(data, (int)FonteProventoBolsa.ArquivoPROD);

            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++) {
                prod = prodCollectionSubList.CollectionProd[i];

                #region Dados Prod
                string codigoIsinDestino = prod.CodigoIsinDestino;
                DateTime dataEx = prod.DataEx.Value;
                DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                string cdAtivoBolsa = prod.CdAtivoBolsa;
                #endregion

                string codigoIsin = null; // Inserido na Tabela Conversão se não existir AtivoBolsa com o codigoIsin
                string cdAtivoBolsaDestino = " ";

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (ativoBolsa.BuscaCdAtivoBolsa(codigoIsinDestino)) {
                    cdAtivoBolsaDestino = ativoBolsa.CdAtivoBolsa;
                }
                else {
                    codigoIsin = prod.CodigoIsinDestino;
                }

                #region Cria Bonificacao
                BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();
                bonificacaoBolsa.DataLancamento = data;
                bonificacaoBolsa.DataEx = dataEx;
                bonificacaoBolsa.DataReferencia = dataReferencia;
                bonificacaoBolsa.TipoFracao = Convert.ToByte(prod.IndicadorTratamentoFracao);
                bonificacaoBolsa.PUBonificacao = 0;
                bonificacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                bonificacaoBolsa.Percentual = prod.ValorProvento;
                bonificacaoBolsa.CdAtivoBolsaDestino = cdAtivoBolsaDestino;
                bonificacaoBolsa.Fonte = (byte)FonteProventoBolsa.ArquivoPROD;
                #endregion

                // Insere BonificacaoBolsa
                // Se cdAtivoBolsaDestino = " ", CodigoIsin estará preenchido
                try {
                    this.InsereBonificacaoComCodigoIsin(bonificacaoBolsa, codigoIsin);
                }
                catch (Exception e) {
                    throw new Exception("Erro Inserção: " + e.Message);
                }
            }
        }

        /// <summary>
        /// Inserção especial onde a tabela Bonificacao possui o campo extra CodigoIsin
        /// Usado para carregar o arquivo Prod sem dependência da Tabela AtivoBolsa
        /// </summary>
        private void InsereBonificacaoComCodigoIsin(BonificacaoBolsa bonificacaoBolsa, string codigoIsin) {

            string[] campos = new string[] {         
                BonificacaoBolsaMetadata.ColumnNames.DataLancamento,
                BonificacaoBolsaMetadata.ColumnNames.DataEx,
                BonificacaoBolsaMetadata.ColumnNames.DataReferencia,
                BonificacaoBolsaMetadata.ColumnNames.TipoFracao,
                BonificacaoBolsaMetadata.ColumnNames.PUBonificacao,
                BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsa,
                BonificacaoBolsaMetadata.ColumnNames.Percentual,
                BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino,
                BonificacaoBolsaMetadata.ColumnNames.Fonte,
                "CodigoIsin"
            };

            string camposConcat = String.Join(", ", campos);
            string camposParams = String.Join(",@", campos);

            esParameters esParams = new esParameters();
            esParams.Add(campos[0], bonificacaoBolsa.DataLancamento.Value);
            esParams.Add(campos[1], bonificacaoBolsa.DataEx.Value);
            esParams.Add(campos[2], bonificacaoBolsa.DataReferencia.Value);
            esParams.Add(campos[3], bonificacaoBolsa.TipoFracao.Value);
            esParams.Add(campos[4], bonificacaoBolsa.PUBonificacao);
            esParams.Add(campos[5], bonificacaoBolsa.CdAtivoBolsa);
            esParams.Add(campos[6], bonificacaoBolsa.Percentual.Value);
            esParams.Add(campos[7], bonificacaoBolsa.CdAtivoBolsaDestino);
            esParams.Add(campos[8], bonificacaoBolsa.Fonte.Value);

            // Parametros que podem ser null
            if (String.IsNullOrEmpty(codigoIsin)) {
                esParams.Add("CodigoIsin", DBNull.Value);
            }
            else {
                esParams.Add("CodigoIsin", codigoIsin);
            }

            //
            StringBuilder sqlBuilder = new StringBuilder();
            //
            sqlBuilder.Append("INSERT INTO BonificacaoBolsa (");
            sqlBuilder.Append(camposConcat);
            sqlBuilder.Append(") VALUES ( ");
            sqlBuilder.Append("@" + camposParams);
            sqlBuilder.Append(")");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
        }

        /// <summary>
        /// Processa atualizações de PosicaoBolsa, a partir de bonificações efetivadas no dia.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaBonificacao(int idCliente, DateTime data)
        {
            BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollection = bonificacaoBolsaCollection.BuscaBonificacaoBolsa(data, idCliente, false);

            #region Deleta operações com origem = ResultadoBonificacao na OperacaoBolsa
            List<int> origemOperacao = new List<int>();
            origemOperacao.Add(OrigemOperacaoBolsa.ResultadoBonificacao);

            OperacaoBolsaCollection operacaoBolsaCollectionDeletar = new OperacaoBolsaCollection();
            operacaoBolsaCollectionDeletar.DeletaOperacaoBolsa(idCliente, origemOperacao, data);
            #endregion

            for (int i = 0; i < bonificacaoBolsaCollection.Count; i++)
            {
                #region Dados de Bonificacao
                BonificacaoBolsa bonificacaoBolsa = bonificacaoBolsaCollection[i];
                string cdAtivoBolsaDestino = bonificacaoBolsa.CdAtivoBolsaDestino;
                string cdAtivoBolsa = bonificacaoBolsa.CdAtivoBolsa;
                decimal percentual = bonificacaoBolsa.Percentual.Value / 100;
                decimal puBonificacao = bonificacaoBolsa.PUBonificacao.Value;
                #endregion

                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.IdPosicao,
                                                    posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaCollection.Query.Quantidade,
                                                    posicaoBolsaCollection.Query.QuantidadeBloqueada,
                                                    posicaoBolsaCollection.Query.PUCusto,
                                                    posicaoBolsaCollection.Query.PUCustoLiquido,
                                                    posicaoBolsaCollection.Query.IdAgente,
                                                    posicaoBolsaCollection.Query.TipoMercado,
                                                    posicaoBolsaCollection.Query.DataVencimento);
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoBolsaCollection.Query.Load();

                for (int j = 0; j < posicaoBolsaCollection.Count; j++)
                {
                    #region Dados de PosicaoBolsa
                    PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[j];
                    decimal quantidade = posicaoBolsa.Quantidade.Value;
                    decimal quantidadeBloqueada = posicaoBolsa.QuantidadeBloqueada.Value;
                    decimal puCusto = posicaoBolsa.PUCusto.Value;
                    decimal puCustoLiquido = posicaoBolsa.PUCustoLiquido.Value;
                    int idAgente = posicaoBolsa.IdAgente.Value;
                    string tipoMercado = posicaoBolsa.TipoMercado;
                    DateTime? dataVencimento = posicaoBolsa.DataVencimento;
                    #endregion

                    decimal quantidadeBonificada = Utilitario.Truncate(quantidade * percentual, 0);
                    decimal quantidadeBloqueadaBonificada = Utilitario.Truncate(quantidadeBloqueada * percentual, 0);

                    if (quantidadeBonificada != 0)
                    {
                        FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                        if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                        {
                            if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                            {
                                AtivoBolsa ativoBolsa = new AtivoBolsa();
                                if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsa))
                                {
                                    throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsa + " " + data.ToString("d"));
                                }
                            }
                        }

                        decimal custoBonificacao = quantidadeBonificada * puBonificacao / fator.Fator.Value;

                        decimal quantidadeTotal = quantidadeBonificada + quantidade;
                        decimal quantidadeBloqueadaTotal = quantidadeBloqueadaBonificada + quantidadeBloqueada;

                        decimal custoOriginal = quantidade * puCusto / fator.Fator.Value;
                        decimal custoLiquidoOriginal = quantidade * puCustoLiquido / fator.Fator.Value;

                        decimal puCustoBonificado = ((custoOriginal + custoBonificacao) / quantidadeTotal) * fator.Fator.Value;
                        decimal puCustoLiquidoBonificado = ((custoLiquidoOriginal + custoBonificacao) / quantidadeTotal) * fator.Fator.Value;

                        decimal quantidadeFinal = quantidadeTotal;
                        decimal quantidadeBloqueadaFinal = quantidadeBloqueadaTotal;
                        decimal puCustoFinal = puCustoBonificado;
                        decimal puCustoLiquidoFinal = puCustoLiquidoBonificado;

                        if (cdAtivoBolsa != cdAtivoBolsaDestino)
                        {
                            #region Trata quando o ativo origem é diferente do ativo destino
                            PosicaoBolsa posicaoBolsaExistente = new PosicaoBolsa();
                            if (posicaoBolsaExistente.BuscaPosicaoBolsa(idCliente, idAgente, cdAtivoBolsaDestino))
                            {
                                #region Se o ativo já existe, faz custo médio com a quantidade bonificada
                                decimal quantidadeExistente = posicaoBolsaExistente.Quantidade.Value;
                                decimal quantidadeBloqueadaExistente = posicaoBolsaExistente.QuantidadeBloqueada.Value;
                                decimal puCustoExistente = posicaoBolsaExistente.PUCusto.Value;
                                decimal puCustoLiquidoExistente = posicaoBolsaExistente.PUCustoLiquido.Value;

                                fator = new FatorCotacaoBolsa();
                                if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsaDestino, data))
                                {
                                    if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                                    {
                                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                                        if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsaDestino))
                                        {
                                            throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsaDestino + " " + data.ToString("d"));
                                        }
                                    }
                                }

                                quantidadeFinal = quantidadeBonificada + quantidadeExistente;
                                quantidadeBloqueadaFinal = quantidadeBloqueadaBonificada + quantidadeBloqueadaExistente;

                                decimal custoExistente = quantidadeExistente * puCustoExistente / fator.Fator.Value;
                                decimal custoLiquidoExistente = quantidadeExistente * puCustoLiquidoExistente / fator.Fator.Value;

                                puCustoFinal = ((custoExistente + custoBonificacao) / quantidadeFinal) * fator.Fator.Value;
                                puCustoLiquidoFinal = ((custoLiquidoExistente + custoBonificacao) / quantidadeFinal) * fator.Fator.Value;

                                posicaoBolsaExistente.Quantidade = quantidadeFinal;
                                posicaoBolsaExistente.QuantidadeBloqueada = quantidadeBloqueadaFinal;
                                posicaoBolsaExistente.PUCusto = puCustoFinal;
                                posicaoBolsaExistente.PUCustoLiquido = puCustoLiquidoFinal;
                                posicaoBolsaExistente.Save();
                                #endregion
                            }
                            else
                            {
                                #region Cria posição nova com o ativo destino
                                PosicaoBolsa posicaoBolsaInsert = new PosicaoBolsa();
                                posicaoBolsaInsert.IdCliente = idCliente;
                                posicaoBolsaInsert.IdAgente = idAgente;
                                posicaoBolsaInsert.CdAtivoBolsa = cdAtivoBolsaDestino;
                                posicaoBolsaInsert.Quantidade = quantidadeBonificada;
                                posicaoBolsaInsert.QuantidadeBloqueada = quantidadeBloqueadaBonificada;
                                posicaoBolsaInsert.PUCusto = puBonificacao;
                                posicaoBolsaInsert.PUCustoLiquido = puBonificacao;
                                posicaoBolsaInsert.QuantidadeInicial = quantidadeBonificada;
                                posicaoBolsaInsert.TipoMercado = tipoMercado;
                                posicaoBolsaInsert.DataVencimento = dataVencimento;
                                posicaoBolsaInsert.ValorCustoLiquido = Utilitario.Truncate(quantidade * puBonificacao / fator.Fator.Value, 2);
                                posicaoBolsaInsert.Save();
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            #region Atualiza posicaoBolsa
                            posicaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            posicaoBolsa.Quantidade = quantidadeFinal;
                            posicaoBolsa.QuantidadeBloqueada = quantidadeBloqueadaFinal;
                            posicaoBolsa.PUCusto = puCustoBonificado;
                            posicaoBolsa.PUCustoLiquido = puCustoLiquidoBonificado;
                            #endregion
                        }

                        #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado = zero)
                        string tipoOperacao = "";
                        if (quantidadeBonificada > 0)
                        {
                            tipoOperacao = TipoOperacaoBolsa.Deposito;
                        }
                        else
                        {
                            tipoOperacao = TipoOperacaoBolsa.Retirada;
                        }

                        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                        operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                        operacaoBolsa.TipoOperacao = tipoOperacao;
                        operacaoBolsa.Data = data;
                        operacaoBolsa.Pu = Math.Abs(puBonificacao);
                        operacaoBolsa.Valor = Math.Abs(custoBonificacao);
                        operacaoBolsa.ValorLiquido = Math.Abs(custoBonificacao);
                        operacaoBolsa.PULiquido = puBonificacao;
                        operacaoBolsa.Corretagem = 0;
                        operacaoBolsa.Emolumento = 0;
                        operacaoBolsa.LiquidacaoCBLC = 0;
                        operacaoBolsa.RegistroBolsa = 0;
                        operacaoBolsa.RegistroCBLC = 0;                                    //
                        operacaoBolsa.Quantidade = Math.Abs(quantidadeBonificada);
                        operacaoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                        operacaoBolsa.IdAgenteCorretora = idAgente;
                        operacaoBolsa.IdAgenteLiquidacao = idAgente;
                        operacaoBolsa.Origem = OrigemOperacaoBolsa.ResultadoBonificacao;
                        operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                        operacaoBolsa.DataLiquidacao = data;
                        operacaoBolsa.PercentualDesconto = 0;
                        operacaoBolsa.Desconto = 0;
                        operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                        operacaoBolsa.IdCliente = idCliente;
                        operacaoBolsa.CalculaDespesas = "N";
                        operacaoBolsa.ResultadoRealizado = 0;
                        operacaoBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;                        
                        operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                        operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                        operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                        operacaoBolsa.DataOperacao = data;
                        operacaoBolsa.Save();
                        #endregion
                    }
                }
                posicaoBolsaCollection.Save();
            }
        }

        /// <summary>
        /// Processa atualizações de PosicaoEmprestimoBolsa, a partir de bonificações efetivadas no dia.
        /// Insere em EventoFisico, relativo às ações bonificadas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaBonificacaoEmprestimo(int idCliente, DateTime data) 
        {
            DateTime dataAnteriorBolsa = Calendario.SubtraiDiaUtil(data, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            BonificacaoBolsaQuery bonificacaoBolsaQuery = new BonificacaoBolsaQuery("B");
            bonificacaoBolsaQuery.Select(bonificacaoBolsaQuery.IdBonificacao,
                                      bonificacaoBolsaQuery.CdAtivoBolsa,
                                      bonificacaoBolsaQuery.CdAtivoBolsaDestino,
                                      bonificacaoBolsaQuery.Percentual,
                                      bonificacaoBolsaQuery.PUBonificacao,
                                      bonificacaoBolsaQuery.DataEx);
            bonificacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(bonificacaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);
            bonificacaoBolsaQuery.Where(bonificacaoBolsaQuery.DataEx.GreaterThanOrEqual(dataAnteriorBolsa),
                                        bonificacaoBolsaQuery.DataEx.LessThanOrEqual(data));
            bonificacaoBolsaQuery.Where(ativoBolsaQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
            bonificacaoBolsaQuery.OrderBy(bonificacaoBolsaQuery.IdBonificacao.Ascending);

            BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollection.Load(bonificacaoBolsaQuery);

            for (int i = 0; i < bonificacaoBolsaCollection.Count; i++) 
            {
                #region Dados de Bonificacao
                BonificacaoBolsa bonificacaoBolsa = bonificacaoBolsaCollection[i];
                string cdAtivoBolsaDestino = bonificacaoBolsa.CdAtivoBolsaDestino;                
                string cdAtivoBolsa = bonificacaoBolsa.CdAtivoBolsa;
                decimal percentual = bonificacaoBolsa.Percentual.Value / 100;
                decimal puBonificacao = bonificacaoBolsa.PUBonificacao.Value;
                DateTime dataEx = bonificacaoBolsa.DataEx.Value;
                #endregion          
      
                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoEmprestimoBolsaCollection.Query.Load();

                foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
                {
                    if (posicaoEmprestimoBolsa.DataVencimento.Value == data) //BTC só atualiza em D+2 da data ex, então no vencimento ainda não atualizou
                    {
                        continue;
                    }

                    #region Dados de PosicaoEmprestimoBolsa
                    int idAgente = posicaoEmprestimoBolsa.IdAgente.Value;
                    decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                    DateTime dataRegistro = posicaoEmprestimoBolsa.DataRegistro.Value;                    
                    #endregion

                    //Este teste serve para poder tratar a questão do BTC sofrer efeitos de eventos com data-ex até d-1 data data de registro
                    if ((dataRegistro <= dataEx && dataEx == data) || (dataRegistro > dataEx && dataRegistro == data))
                    {
                        FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                        fator.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);
                        
                        decimal quantidadeBonificada = Utilitario.Truncate(quantidade * percentual, 0);
                        decimal quantidadeTotal = quantidadeBonificada + quantidade;
                        decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;
                        

                        decimal puBonificado = 0;
                        if (cdAtivoBolsa == cdAtivoBolsaDestino)
                        {
                            decimal custoLiquidoOriginal = quantidade * puLiquidoOriginal / fator.Fator.Value;
                            decimal custoLiquidoBonificacao = quantidadeBonificada * puBonificacao / fator.Fator.Value;
                            puBonificado = ((custoLiquidoOriginal + custoLiquidoBonificacao) / quantidadeTotal) * fator.Fator.Value;
                        }
                        else
                        {
                            puBonificado = puBonificacao;
                        }

                        if (cdAtivoBolsa == cdAtivoBolsaDestino)
                        {
                            #region Atualiza posicaoEmprestimoBolsa
                            posicaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                            posicaoEmprestimoBolsa.Quantidade = quantidadeTotal;
                            posicaoEmprestimoBolsa.PULiquidoOriginal = puBonificado;
                            #endregion
                        }
                        else
                        {
                            PosicaoEmprestimoBolsa posicaoEmprestimoBolsaNovo = new PosicaoEmprestimoBolsa();
                            posicaoEmprestimoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDestino;
                            posicaoEmprestimoBolsaNovo.DataRegistro = posicaoEmprestimoBolsa.DataRegistro;
                            posicaoEmprestimoBolsaNovo.DataVencimento = posicaoEmprestimoBolsa.DataVencimento;
                            posicaoEmprestimoBolsaNovo.IdAgente = posicaoEmprestimoBolsa.IdAgente;
                            posicaoEmprestimoBolsaNovo.IdCliente = idCliente;
                            posicaoEmprestimoBolsaNovo.IdOperacao = posicaoEmprestimoBolsa.IdOperacao;
                            posicaoEmprestimoBolsaNovo.IdTrader = posicaoEmprestimoBolsa.IdTrader;
                            posicaoEmprestimoBolsaNovo.NumeroContrato = posicaoEmprestimoBolsa.NumeroContrato;
                            posicaoEmprestimoBolsaNovo.PontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo;
                            posicaoEmprestimoBolsaNovo.PULiquidoOriginal = puBonificado;
                            posicaoEmprestimoBolsaNovo.PUMercado = 0;
                            posicaoEmprestimoBolsaNovo.Quantidade = quantidadeBonificada;
                            posicaoEmprestimoBolsaNovo.TaxaComissao = 0;
                            posicaoEmprestimoBolsaNovo.TaxaOperacao = 0;
                            posicaoEmprestimoBolsaNovo.TipoEmprestimo = posicaoEmprestimoBolsa.TipoEmprestimo;
                            posicaoEmprestimoBolsaNovo.ValorBase = 0;
                            posicaoEmprestimoBolsaNovo.ValorCorrigidoCBLC = 0;
                            posicaoEmprestimoBolsaNovo.ValorCorrigidoComissao = 0;
                            posicaoEmprestimoBolsaNovo.ValorCorrigidoJuros = 0;
                            posicaoEmprestimoBolsaNovo.ValorDiarioCBLC = 0;
                            posicaoEmprestimoBolsaNovo.ValorDiarioComissao = 0;
                            posicaoEmprestimoBolsaNovo.ValorDiarioJuros = 0;
                            posicaoEmprestimoBolsaNovo.ValorMercado = 0;
                            posicaoEmprestimoBolsaNovo.Save();
                        }
                        
                    }
                }
                posicaoEmprestimoBolsaCollection.Save();
            }
        }

        /// <summary>
        /// Processa atualizações de PosicaoTermoBolsa, a partir de bonificações efetivadas no dia.        
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaBonificacaoTermo(int idCliente, DateTime data)
        {
            BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
            bonificacaoBolsaCollection = bonificacaoBolsaCollection.BuscaBonificacaoBolsa(data, idCliente, true);

            for (int i = 0; i < bonificacaoBolsaCollection.Count; i++)
            {
                #region Dados de Bonificacao
                BonificacaoBolsa bonificacaoBolsa = bonificacaoBolsaCollection[i];
                string cdAtivoBolsaDestino = bonificacaoBolsa.CdAtivoBolsaDestino;
                string cdAtivoBolsa = bonificacaoBolsa.CdAtivoBolsa;
                decimal percentual = bonificacaoBolsa.Percentual.Value / 100;
                decimal puBonificacao = bonificacaoBolsa.PUBonificacao.Value;
                #endregion

                PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsaCompleta(idCliente, cdAtivoBolsa);
                for (int j = 0; j < posicaoTermoBolsaCollection.Count; j++)
                {
                    PosicaoTermoBolsa posicaoTermoBolsa = posicaoTermoBolsaCollection[j];
                    #region Dados de PosicaoTermoBolsa
                    decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                    decimal puCustoLiquidoAcao = posicaoTermoBolsa.PUCustoLiquidoAcao.Value;
                    decimal puTermo = posicaoTermoBolsa.PUTermo.Value;
                    decimal puTermoLiquido = posicaoTermoBolsa.PUTermoLiquido.Value;
                    #endregion

                    FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                    if (!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                    {
                        if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                        {
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsa))
                            {
                                throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsa + " " + data.ToString("d"));
                            }
                        }
                    }

                    decimal quantidadeBonificada = Utilitario.Truncate(quantidade * percentual, 0);
                    decimal quantidadeTotal = quantidadeBonificada + quantidade;

                    #region Cálculo do novo puCustoLiquidoAcao
                    decimal custoLiquidoAcao = quantidade * puCustoLiquidoAcao / fator.Fator.Value;
                    decimal custoLiquidoAcaoBonificado = quantidadeBonificada * puBonificacao / fator.Fator.Value;
                    decimal puCustoLiquidoAcaoNovo = ((custoLiquidoAcao + custoLiquidoAcaoBonificado) / quantidadeTotal) * fator.Fator.Value;
                    #endregion

                    #region Cálculo do novo puTermo
                    decimal valorTermo = posicaoTermoBolsa.ValorTermo.Value;
                    decimal valorTermoBonificado = quantidadeBonificada * puBonificacao / fator.Fator.Value;
                    decimal puTermoNovo = ((valorTermo + valorTermoBonificado) / quantidadeTotal) * fator.Fator.Value;
                    #endregion

                    #region Cálculo do novo puTermoLiquido
                    decimal valorTermoLiquido = posicaoTermoBolsa.ValorTermoLiquido.Value;
                    decimal valorTermoLiquidoBonificado = quantidadeBonificada * puBonificacao / fator.Fator.Value;
                    decimal puTermoLiquidoNovo = ((valorTermoLiquido + valorTermoLiquidoBonificado) / quantidadeTotal) * fator.Fator.Value;
                    #endregion

                    #region Atualiza posicaoTermoBolsa
                    posicaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsaDestino;
                    posicaoTermoBolsa.Quantidade = quantidadeTotal;
                    posicaoTermoBolsa.PUCustoLiquidoAcao = puCustoLiquidoAcaoNovo;
                    posicaoTermoBolsa.PUTermo = puTermoNovo;
                    posicaoTermoBolsa.PUTermoLiquido = puTermoLiquidoNovo;
                    #endregion                    
                }
                posicaoTermoBolsaCollection.Save();
            }
        }
    }
}