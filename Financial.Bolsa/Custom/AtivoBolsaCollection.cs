using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Common.Enums;

namespace Financial.Bolsa {
    public partial class AtivoBolsaCollection : esAtivoBolsaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(AtivoBolsaCollection));

        /// <summary>
        /// Carrega o objeto AtivoBolsaCollection com os campos CdAtivoBolsa, TipoMercado.
        /// Filtra DataVencimento >= dataVencimento.
        /// Filtra TipoMercado IN (Termo, OpcaoCompra, OpcaoVenda).
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAtivoBolsa(string cdAtivoBolsa, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa, this.Query.TipoMercado)
                 .Where(this.Query.CdAtivoBolsaObjeto.Equal(cdAtivoBolsa),
                        this.Query.Or (
                            this.Query.TipoMercado.Equal(TipoMercadoBolsa.Termo),
                            this.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                            this.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda)
                        ),
                        this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento));

            bool retorno = this.Query.Load();
         
            return retorno;
        }

        /// <summary>
        /// Carrega o objeto AtivoBolsaCollection com os campos CdAtivoBolsa, TipoMercado.
        /// Filtra DataVencimento >= dataVencimento.
        /// Filtra TipoMercado In (OpcaoCompra, OpcaoVenda).
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAtivoBolsaOpcao(string cdAtivoBolsa, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.CdAtivoBolsaObjeto.Equal(cdAtivoBolsa),
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra,
                                                  TipoMercadoBolsa.OpcaoVenda),
                        this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto AtivoBolsaCollection com os campos CdAtivoBolsa, TipoMercado.
        /// Filtra DataVencimento >= dataVencimento.
        /// Filtra TipoMercado = Termo.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAtivoBolsaTermo(string cdAtivoBolsa, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa, this.Query.TipoMercado)
                 .Where(this.Query.CdAtivoBolsaObjeto.Equal(cdAtivoBolsa),
                        this.Query.TipoMercado.Equal(TipoMercadoBolsa.Termo),
                        this.Query.DataVencimento.GreaterThanOrEqual(dataVencimento));

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto AtivoBolsaCollection com todos os campos de AtivoBolsaCollection.
        /// Filtra IdMoeda = Dolar.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAtivoOffshore()
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdMoeda.NotEqual((short)ListaMoedaFixo.Real));

            bool retorno = this.Query.Load();

            return retorno;
        }
        
        /// <summary>
        /// Carrega o objeto AtivoBolsaCollection com todos os campos de AtivoBolsaCollection.
        /// Filtra IdMoeda = Real.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <returns>bool indicando se foi encontrado registros</returns>
        public bool BuscaAtivoOnshore()
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdMoeda.Equal((short)ListaMoedaFixo.Real));
                
            bool retorno = this.Query.Load();

            return retorno;
        }
    }
}

