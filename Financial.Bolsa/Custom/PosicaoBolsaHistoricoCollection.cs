﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa {
    public partial class PosicaoBolsaHistoricoCollection : esPosicaoBolsaHistoricoCollection 
    {
        // Construtor
        // Cria uma nova PosicaoBolsaHistoricoCollection com os dados de PosicaoBolsaCollection
        // Todos os dados de PosicaoBolsaCollection são copiados, adicionando a dataHistorico
        public PosicaoBolsaHistoricoCollection(PosicaoBolsaCollection posicaoBolsaCollection, DateTime dataHistorico)
        {
            using (esTransactionScope scope = new esTransactionScope())
            {
                int max = 0;
                PosicaoBolsaHistorico posicaoBolsaHistoricoMax = new PosicaoBolsaHistorico();
                posicaoBolsaHistoricoMax.Query.Select(posicaoBolsaHistoricoMax.Query.IdPosicao.Max());
                posicaoBolsaHistoricoMax.Query.Load();

                if (posicaoBolsaHistoricoMax.IdPosicao.HasValue)
                {
                    max = posicaoBolsaHistoricoMax.IdPosicao.Value + 1;
                }
                else
                {
                    max = 1;
                }

                for (int i = 0; i < posicaoBolsaCollection.Count; i++)
                {
                    PosicaoBolsaHistorico p = new PosicaoBolsaHistorico();

                    // Para cada Coluna de PosicaoBolsa copia para PosicaoBolsaHistorico
                    foreach (esColumnMetadata colPosicao in posicaoBolsaCollection.es.Meta.Columns)
                    {
                        // Copia todas as colunas 
                        esColumnMetadata colPosicaoBolsaHistorico = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                        if (posicaoBolsaCollection[i].GetColumn(colPosicao.Name) != null)
                        {
                            p.SetColumn(colPosicaoBolsaHistorico.Name, posicaoBolsaCollection[i].GetColumn(colPosicao.Name));
                        }
                    }
                    
                    p.DataHistorico = dataHistorico;
                    p.IdPosicao = max;

                    max += 1;

                    this.AttachEntity(p);
                }

                scope.Complete();
            }
        }
        
        /// <summary>
        /// Deleta posições de bolsa históricas.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBolsaHistorico(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta posições de bolsa históricas.
        /// Filtra por DataHistorico >= dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBolsaHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta posições de bolsa históricas.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoBolsaHistoricoDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoBolsaHistoricoCompleta(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();         
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoBolsaHistoricoCompletaDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();         
        }

        /// <summary>
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public bool BuscaPosicaoBolsaHistorico(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.DataHistorico, this.Query.Quantidade)
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCliente == idCliente);

            bool retorno = this.Query.Load();
         
            return retorno;        
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsaCollection com os campos CdAtivoBolsa, TipoMercado, Quantidade.Sum, PUMercado.Avg, PUCustoLiquido.Avg,
        /// ValorMercado.Sum, ValorCustoLiquido.Sum.
        /// Group By CdAtivoBolsa
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoBolsaAgrupado(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa,
                         this.Query.TipoMercado,
                         this.Query.Quantidade.Sum(),
                         this.Query.PUMercado.Avg(),
                         this.Query.PUCustoLiquido.Avg(),
                         this.Query.ValorMercado.Sum(),
                         this.Query.ValorCustoLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico))
                 .GroupBy(this.Query.CdAtivoBolsa, this.Query.TipoMercado);

            bool retorno = this.Query.Load();

            return retorno;
        }
    }
}
