﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Interfaces.Import.Bolsa;

namespace Financial.Bolsa {
    public partial class GrupamentoBolsa : esGrupamentoBolsa
    {        
        /// <summary>
        /// Distribui grupamentos lançados na data para mercado a vista, em conversões de termo, opção compra/venda.
        /// </summary>
        /// <param name="data"></param>
        public void DistribuiGrupamento(DateTime data, GrupamentoBolsa grupamentoBolsa) 
        {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(grupamentoBolsa.CdAtivoBolsa);
            string tipoMercado = ativoBolsa.TipoMercado;

            if (tipoMercado.Equals(TipoMercadoBolsa.MercadoVista))
            {
                #region Deleta todos os eventos de grupamento de ativos que tenham o ativo principal do grupamento como ativo objeto
                GrupamentoBolsaQuery grupamentoBolsaQuery = new GrupamentoBolsaQuery("G");
                AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
                grupamentoBolsaQuery.Select(grupamentoBolsaQuery.IdGrupamento);
                grupamentoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == grupamentoBolsaQuery.CdAtivoBolsa);
                grupamentoBolsaQuery.Where(ativoBolsaQuery.CdAtivoBolsaObjeto.Equal(grupamentoBolsa.CdAtivoBolsa));
                grupamentoBolsaQuery.Where(grupamentoBolsaQuery.DataLancamento.Equal(grupamentoBolsa.DataLancamento.Value));

                GrupamentoBolsaCollection grupamentoBolsaCollectionDeletar = new GrupamentoBolsaCollection();
                grupamentoBolsaCollectionDeletar.Load(grupamentoBolsaQuery);
                grupamentoBolsaCollectionDeletar.MarkAllAsDeleted();
                grupamentoBolsaCollectionDeletar.Save();
                #endregion

                AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                if (ativoBolsaCollection.BuscaAtivoBolsa(grupamentoBolsa.CdAtivoBolsa, data))
                {
                    for (int j = 0; j < ativoBolsaCollection.Count; j++) {
                        string cdAtivoBolsa = ativoBolsaCollection[j].CdAtivoBolsa;
                        
                        #region Cria novo grupamento do Termo ou da Opção
                        GrupamentoBolsa novoGrupamentoBolsa = new GrupamentoBolsa();
                        novoGrupamentoBolsa.AddNew();
                        novoGrupamentoBolsa.DataLancamento = grupamentoBolsa.DataLancamento;                            
                        novoGrupamentoBolsa.DataEx = grupamentoBolsa.DataEx;
                        novoGrupamentoBolsa.DataReferencia = grupamentoBolsa.DataReferencia;
                        novoGrupamentoBolsa.FatorQuantidade = grupamentoBolsa.FatorQuantidade;
                        novoGrupamentoBolsa.FatorPU = grupamentoBolsa.FatorPU;
                        novoGrupamentoBolsa.Fonte = grupamentoBolsa.Fonte;
                        novoGrupamentoBolsa.TipoGrupamento = grupamentoBolsa.TipoGrupamento;                                        
                        novoGrupamentoBolsa.CdAtivoBolsa = cdAtivoBolsa;                            
                        
                        novoGrupamentoBolsa.InsereGrupamentoBolsa(novoGrupamentoBolsa);
                        #endregion
                    }
                }
            }
            
        }

        /// <summary>
        /// Método de inserção de Grupamento
        /// </summary>
        /// <param name="grupamentoBolsa">grupamento com os dados a inserir</param>  
        /// throws ArgumentNullException se campos Obrigatórios da inclusão estiverem nulos
        /// grupamentoBolsa.DataLancamento
        /// grupamento.DataEx ou grupamento.DataReferencia ou 
        /// grupamento.CdAtivoBolsa ou grupamento.FatorQuantidade ou
        //  grupamento.FatorPU ou grupamento.Fonte   
        //  grupamento.TipoGrupamento
        public void InsereGrupamentoBolsa(GrupamentoBolsa grupamentoBolsa) 
        {
            #region ArgumentosNulos - throw ArgumentNullException
            if (!grupamentoBolsa.DataLancamento.HasValue ||
               !grupamentoBolsa.DataReferencia.HasValue ||
               String.IsNullOrEmpty(grupamentoBolsa.CdAtivoBolsa) ||
               !grupamentoBolsa.FatorQuantidade.HasValue ||
               !grupamentoBolsa.FatorPU.HasValue ||
               !grupamentoBolsa.Fonte.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("grupamentoBolsa.DataLancamento ou ")
                .Append("grupamentoBolsa.DataEx ou ")
                .Append("grupamentoBolsa.DataReferencia ou ")
                .Append("grupamentoBolsa.CdAtivoBolsa ou ")
                .Append("grupamentoBolsa.FatorQuantidade ou ")
                .Append("grupamentoBolsa.FatorPU ou ")
                .Append("grupamentoBolsa.Fonte ou ")
                .Append("grupamentoBolsa.TipoGrupamento ");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion
                        
            GrupamentoBolsa grupamentoBolsaInserir = new GrupamentoBolsa();
            grupamentoBolsaInserir.AddNew();
            //            
            // Campos Obrigatórios para a inserção
            grupamentoBolsaInserir.DataLancamento = grupamentoBolsa.DataLancamento;
            grupamentoBolsaInserir.DataEx = grupamentoBolsa.DataEx;
            grupamentoBolsaInserir.DataReferencia = grupamentoBolsa.DataReferencia;
            grupamentoBolsaInserir.CdAtivoBolsa = grupamentoBolsa.CdAtivoBolsa;
            grupamentoBolsaInserir.FatorQuantidade = grupamentoBolsa.FatorQuantidade;
            grupamentoBolsaInserir.FatorPU = grupamentoBolsa.FatorPU;
            grupamentoBolsaInserir.Fonte = grupamentoBolsa.Fonte;      
            grupamentoBolsaInserir.TipoGrupamento = grupamentoBolsa.TipoGrupamento;      
            //    
            grupamentoBolsaInserir.Save();
        }

        /// <summary>
        /// Carrega GrupamentoBolsa a partir da estrutura importada do arquivo PROD.
        /// </summary>
        /// <param name="prodCollection"></param>
        /// <param name="data"></param>
        /// throws AtivoNaoCadastradoException se cdAtivoBolsa não existir
        public void CarregaProdGrupamento(ProdCollection prodCollection, DateTime data) {
            // Pega os Proventos do Arquivo de Prod                          
            Prod prod = new Prod();
            
            ProdCollection prodCollectionSubList = new ProdCollection();

            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoDesdobramentoGrupamento);

            //Deleta os eventos de bonificação gerados a partir do arquivo PROD na data
            GrupamentoBolsaCollection grupamentoBolsaCollectionDeletar = new GrupamentoBolsaCollection();
            grupamentoBolsaCollectionDeletar.DeletaGrupamento(data, (int)FonteProventoBolsa.ArquivoPROD);

            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            //
            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++) {
                prod = prodCollectionSubList.CollectionProd[i];
                #region Dados Prod
                DateTime dataEx = prod.DataEx.Value;
                DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                #endregion

                AtivoBolsa ativoBolsaAux = new AtivoBolsa();
                string cdAtivoBolsa = prod.CdAtivoBolsa;
                if (!ativoBolsaAux.LoadByPrimaryKey(cdAtivoBolsa)) 
                {
                    AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                    ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                    ativoBolsaNovo.CodigoIsin = ""; //Não tem como buscar
                    ativoBolsaNovo.Descricao = cdAtivoBolsa;
                    ativoBolsaNovo.Especificacao = "*";
                    ativoBolsaNovo.IdEmissor = 1; //Assume qq emissor!
                    ativoBolsaNovo.IdMoeda = 1; //Assume qq moeda!
                    ativoBolsaNovo.PUExercicio = 0;
                    ativoBolsaNovo.TipoMercado = TipoMercadoBolsa.MercadoVista;
                    ativoBolsaNovo.TipoPapel = (byte)TipoPapelAtivo.Normal;
                    ativoBolsaNovo.NumeroSerie = 0;
                    ativoBolsaNovo.Save();

                    FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                    fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                    fatorCotacaoBolsaNovo.DataReferencia = data;
                    fatorCotacaoBolsaNovo.Fator = 1M; //Assume 1!
                    fatorCotacaoBolsaNovo.Save();
                }

                byte? tipoGrupamento = null;
                decimal fatorQuantidade = 0;
                decimal fatorPu = 1;

                if (prod.IsTipoProventoDesdobramento()) {
                    tipoGrupamento = (byte)TipoGrupamentoBolsa.Desdobramento;
                    fatorQuantidade = (prod.ValorProvento.Value / 100) + 1;
                    fatorPu = 1 / fatorQuantidade;
                }
                else if (prod.IsTipoProventoGrupamento()) {
                    tipoGrupamento = (byte)TipoGrupamentoBolsa.Grupamento;
                    fatorQuantidade = 1 / prod.ValorProvento.Value;
                    fatorPu = 1 / fatorQuantidade;
                }

                // Cria um Novo Grupamento
                #region Cria Grupamento
                GrupamentoBolsa grupamentoBolsa = new GrupamentoBolsa();
                grupamentoBolsa.AddNew();
                grupamentoBolsa.DataLancamento = data;
                grupamentoBolsa.DataEx = dataEx;
                grupamentoBolsa.DataReferencia = dataReferencia;
                grupamentoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                grupamentoBolsa.FatorQuantidade = fatorQuantidade;
                grupamentoBolsa.FatorPU = fatorPu;
                grupamentoBolsa.Fonte = (byte)FonteProventoBolsa.ArquivoPROD;
                grupamentoBolsa.TipoGrupamento = tipoGrupamento;
                #endregion

                grupamentoBolsaCollection.AttachEntity(grupamentoBolsa);
            }

            grupamentoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega GrupamentoBolsa a partir da estrutura importada do arquivo PROD.
        /// Não considera foreign Key Ativobolsa
        /// Importa mesmo se não existir AtivoBolsa
        /// </summary>
        /// <param name="prodCollection"></param>
        /// <param name="data"></param>
        public void CarregaProdGrupamentoSemFK(ProdCollection prodCollection, DateTime data) {
            // Pega os Proventos do Arquivo de Prod                          
            Prod prod = new Prod();

            ProdCollection prodCollectionSubList = new ProdCollection();

            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoDesdobramentoGrupamento);

            //Deleta os eventos de bonificação gerados a partir do arquivo PROD na data
            GrupamentoBolsaCollection grupamentoBolsaCollectionDeletar = new GrupamentoBolsaCollection();
            grupamentoBolsaCollectionDeletar.DeletaGrupamento(data, (int)FonteProventoBolsa.ArquivoPROD);

            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            //
            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++) {
                prod = prodCollectionSubList.CollectionProd[i];
                #region Dados Prod
                DateTime dataEx = prod.DataEx.Value;
                DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                #endregion

                string cdAtivoBolsa = prod.CdAtivoBolsa;

                byte? tipoGrupamento = null;
                decimal fatorQuantidade = 0;
                decimal fatorPu = 1;

                if (prod.IsTipoProventoDesdobramento()) {
                    tipoGrupamento = (byte)TipoGrupamentoBolsa.Desdobramento;
                    fatorQuantidade = (prod.ValorProvento.Value / 100) + 1;
                    fatorPu = 1 / fatorQuantidade;
                }
                else if (prod.IsTipoProventoGrupamento()) {
                    tipoGrupamento = (byte)TipoGrupamentoBolsa.Grupamento;
                    fatorQuantidade = 1 / prod.ValorProvento.Value;
                    fatorPu = 1 / fatorQuantidade;
                }

                // Cria um Novo Grupamento
                #region Cria Grupamento
                GrupamentoBolsa grupamentoBolsa = new GrupamentoBolsa();
                grupamentoBolsa.AddNew();
                grupamentoBolsa.DataLancamento = data;
                grupamentoBolsa.DataEx = dataEx;
                grupamentoBolsa.DataReferencia = dataReferencia;
                grupamentoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                grupamentoBolsa.FatorQuantidade = fatorQuantidade;
                grupamentoBolsa.FatorPU = fatorPu;
                grupamentoBolsa.Fonte = (byte)FonteProventoBolsa.ArquivoPROD;
                grupamentoBolsa.TipoGrupamento = tipoGrupamento;
                #endregion

                grupamentoBolsaCollection.AttachEntity(grupamentoBolsa);
            }

            grupamentoBolsaCollection.Save();
        }

        /// <summary>
        /// Processa atualizações de PosicaoBolsa, a partir de grupamentos efetivados no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaGrupamento(int idCliente, DateTime data)
        {
            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            grupamentoBolsaCollection = grupamentoBolsaCollection.BuscaGrupamentoBolsa(data, idCliente, true, false);

            #region Deleta operações com origem = ResultadoGrupamento na OperacaoBolsa
            List<int> origemOperacao = new List<int>();
            origemOperacao.Add(OrigemOperacaoBolsa.ResultadoGrupamento);

            OperacaoBolsaCollection operacaoBolsaCollectionDeletar = new OperacaoBolsaCollection();
            operacaoBolsaCollectionDeletar.DeletaOperacaoBolsa(idCliente, origemOperacao, data);
            #endregion

            for (int i = 0; i < grupamentoBolsaCollection.Count; i++)
            {
                #region Dados de Grupamento
                GrupamentoBolsa grupamentoBolsa = grupamentoBolsaCollection[i];
                string cdAtivoBolsa = grupamentoBolsa.CdAtivoBolsa;
                decimal fatorQuantidade = grupamentoBolsa.FatorQuantidade.Value;
                decimal fatorPu = grupamentoBolsa.FatorPU.Value;
                byte tipoGrupamento = grupamentoBolsa.TipoGrupamento.Value;
                #endregion

                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.IdPosicao,
                                                    posicaoBolsaCollection.Query.Quantidade,
                                                    posicaoBolsaCollection.Query.QuantidadeBloqueada,
                                                    posicaoBolsaCollection.Query.PUCusto,
                                                    posicaoBolsaCollection.Query.PUCustoLiquido,
                                                    posicaoBolsaCollection.Query.IdAgente,
                                                    posicaoBolsaCollection.Query.TipoMercado);
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                posicaoBolsaCollection.Query.Load();

                for (int j = 0; j < posicaoBolsaCollection.Count; j++)
                {
                    PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[j];

                    #region Dados de PosicaoBolsa
                    decimal quantidade = posicaoBolsa.Quantidade.Value;
                    decimal quantidadeBloqueada = posicaoBolsa.QuantidadeBloqueada.Value;
                    decimal puCusto = posicaoBolsa.PUCusto.Value;
                    decimal puCustoLiquido = posicaoBolsa.PUCustoLiquido.Value;
                    int idAgente = posicaoBolsa.IdAgente.Value;
                    string tipoMercado = posicaoBolsa.TipoMercado;
                    #endregion

                    #region Calcula nova quantidade e PU
                    decimal novaQuantidade = 0;
                    decimal novaQuantidadeBloqueada = 0;
                    decimal novoPUCusto = 0;
                    decimal novoPUCustoLiquido = 0;
                    if (tipoGrupamento == (int)TipoGrupamentoBolsa.Grupamento)
                    {
                        novaQuantidade = Utilitario.Truncate(quantidade / fatorQuantidade, 0);
                        novaQuantidadeBloqueada = Utilitario.Truncate(quantidadeBloqueada / fatorQuantidade, 0);
                        novoPUCusto = puCusto / fatorPu;
                        novoPUCustoLiquido = puCustoLiquido / fatorPu;
                    }
                    else
                    {
                        novaQuantidade = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                        novaQuantidadeBloqueada = Utilitario.Truncate(quantidadeBloqueada * fatorQuantidade, 0);
                        novoPUCusto = puCusto * fatorPu;
                        novoPUCustoLiquido = puCustoLiquido * fatorPu;
                    }

                    #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado = zero)
                    OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                    operacaoBolsa.TipoMercado = tipoMercado;
                    operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Retirada;
                    operacaoBolsa.Data = data;
                    operacaoBolsa.Pu = 0;
                    operacaoBolsa.Valor = 0;
                    operacaoBolsa.ValorLiquido = 0;
                    operacaoBolsa.PULiquido = 0;
                    operacaoBolsa.Corretagem = 0;
                    operacaoBolsa.Emolumento = 0;
                    operacaoBolsa.LiquidacaoCBLC = 0;
                    operacaoBolsa.RegistroBolsa = 0;
                    operacaoBolsa.RegistroCBLC = 0;                                    //
                    operacaoBolsa.Quantidade = quantidade;
                    operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    operacaoBolsa.IdAgenteCorretora = idAgente;
                    operacaoBolsa.IdAgenteLiquidacao = idAgente;
                    operacaoBolsa.Origem = OrigemOperacaoBolsa.ResultadoGrupamento;
                    operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                    operacaoBolsa.DataLiquidacao = data;
                    operacaoBolsa.PercentualDesconto = 0;
                    operacaoBolsa.Desconto = 0;
                    operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                    operacaoBolsa.IdCliente = idCliente;
                    operacaoBolsa.CalculaDespesas = "N";
                    operacaoBolsa.ResultadoRealizado = 0;
                    operacaoBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                    operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                    operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                    operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                    operacaoBolsa.DataOperacao = data;

                    operacaoBolsa.Save();
                    #endregion

                    #region InsereOperacao cdAtivoBolsa (com PU, valor = zero, resultado = zero)
                    operacaoBolsa = new OperacaoBolsa();
                    operacaoBolsa.TipoMercado = tipoMercado;
                    operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito;
                    operacaoBolsa.Data = data;
                    operacaoBolsa.Pu = 0;
                    operacaoBolsa.Valor = 0;
                    operacaoBolsa.ValorLiquido = 0;
                    operacaoBolsa.PULiquido = 0;
                    operacaoBolsa.Corretagem = 0;
                    operacaoBolsa.Emolumento = 0;
                    operacaoBolsa.LiquidacaoCBLC = 0;
                    operacaoBolsa.RegistroBolsa = 0;
                    operacaoBolsa.RegistroCBLC = 0;                                    //
                    operacaoBolsa.Quantidade = novaQuantidade;
                    operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    operacaoBolsa.IdAgenteCorretora = idAgente;
                    operacaoBolsa.IdAgenteLiquidacao = idAgente;
                    operacaoBolsa.Origem = OrigemOperacaoBolsa.ResultadoGrupamento;
                    operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                    operacaoBolsa.DataLiquidacao = data;
                    operacaoBolsa.PercentualDesconto = 0;
                    operacaoBolsa.Desconto = 0;
                    operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                    operacaoBolsa.IdCliente = idCliente;
                    operacaoBolsa.CalculaDespesas = "N";
                    operacaoBolsa.ResultadoRealizado = 0;
                    operacaoBolsa.IdMoeda = (byte)ListaMoedaFixo.Real;
                    operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                    operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                    operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                    operacaoBolsa.DataOperacao = data;

                    operacaoBolsa.Save();
                    #endregion
                    #endregion

                    #region Atualiza em PosicaoBolsa
                    posicaoBolsa.Quantidade = novaQuantidade;
                    posicaoBolsa.QuantidadeBloqueada = novaQuantidadeBloqueada;
                    posicaoBolsa.PUCusto = novoPUCusto;
                    posicaoBolsa.PUCustoLiquido = novoPUCustoLiquido;
                    #endregion
                }
                posicaoBolsaCollection.Save();
            }
        }

        /// <summary>
        /// Processa atualizações de PosicaoEmprestimoBolsa, a partir de grupamentos efetivados no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaGrupamentoEmprestimo(int idCliente, DateTime data) 
        {
            DateTime dataAnteriorBolsa = Calendario.SubtraiDiaUtil(data, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            GrupamentoBolsaQuery grupamentoBolsaQuery = new GrupamentoBolsaQuery("G");
            grupamentoBolsaQuery.Select(grupamentoBolsaQuery.IdGrupamento,
                                      grupamentoBolsaQuery.CdAtivoBolsa,
                                      grupamentoBolsaQuery.FatorQuantidade,
                                      grupamentoBolsaQuery.FatorPU,
                                      grupamentoBolsaQuery.TipoGrupamento,
                                      grupamentoBolsaQuery.DataEx);
            grupamentoBolsaQuery.InnerJoin(ativoBolsaQuery).On(grupamentoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);
            grupamentoBolsaQuery.Where(grupamentoBolsaQuery.DataEx.GreaterThanOrEqual(dataAnteriorBolsa),
                                       grupamentoBolsaQuery.DataEx.LessThanOrEqual(data));
            grupamentoBolsaQuery.Where(ativoBolsaQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
            grupamentoBolsaQuery.OrderBy(grupamentoBolsaQuery.IdGrupamento.Ascending);

            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            grupamentoBolsaCollection.Load(grupamentoBolsaQuery);

            for (int i = 0; i < grupamentoBolsaCollection.Count; i++)
            {
                #region Dados de Grupamento
                GrupamentoBolsa grupamentoBolsa = grupamentoBolsaCollection[i];
                string cdAtivoBolsa = grupamentoBolsa.CdAtivoBolsa;
                decimal fatorQuantidade = grupamentoBolsa.FatorQuantidade.Value;
                decimal fatorPu = grupamentoBolsa.FatorPU.Value;
                byte tipoGrupamento = grupamentoBolsa.TipoGrupamento.Value;
                DateTime dataEx = grupamentoBolsa.DataEx.Value;
                #endregion

                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                posicaoEmprestimoBolsaCollection.BuscaPosicaoEmprestimoBolsaCompleta(idCliente, cdAtivoBolsa);
                for (int j = 0; j < posicaoEmprestimoBolsaCollection.Count; j++)
                {
                    PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection[j];

                    if (posicaoEmprestimoBolsa.DataVencimento.Value == data) //BTC só atualiza em D+2 da data ex, então no vencimento ainda não atualizou
                    {
                        continue;
                    }

                    #region Dados de PosicaoEmprestimoBolsa
                    decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                    decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;
                    DateTime dataRegistro = posicaoEmprestimoBolsa.DataRegistro.Value;
                    #endregion
                                        
                    //Este teste serve para poder tratar a questão do BTC sofrer efeitos de eventos com data-ex até d-1 data data de registro
                    if ((dataRegistro <= dataEx && dataEx == data) || (dataRegistro > dataEx && dataRegistro == data))
                    {
                        #region Calcula nova quantidade e PU
                        decimal novaQuantidade = 0;
                        decimal novoPU = 0;
                        if (tipoGrupamento == (int)TipoGrupamentoBolsa.Grupamento)
                        {
                            novaQuantidade = Utilitario.Truncate(quantidade / fatorQuantidade, 0);
                            novoPU = puLiquidoOriginal / fatorPu;
                        }
                        else
                        {
                            novaQuantidade = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                            novoPU = puLiquidoOriginal * fatorPu;
                        }
                        #endregion

                        #region Atualiza em posicaoEmprestimoBolsa
                        posicaoEmprestimoBolsa.Quantidade = novaQuantidade;
                        posicaoEmprestimoBolsa.PULiquidoOriginal = novoPU;
                        #endregion
                    }
                }
                posicaoEmprestimoBolsaCollection.Save();
            }
        }

        /// <summary>
        /// Processa atualizações de PosicaoTermoBolsa, a partir de grupamentos efetivados no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaGrupamentoTermo(int idCliente, DateTime data)
        {
            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            grupamentoBolsaCollection = grupamentoBolsaCollection.BuscaGrupamentoBolsa(data, idCliente, false, true);

            for (int i = 0; i < grupamentoBolsaCollection.Count; i++)
            {
                #region Dados de Grupamento
                GrupamentoBolsa grupamentoBolsa = grupamentoBolsaCollection[i];
                string cdAtivoBolsa = grupamentoBolsa.CdAtivoBolsa;
                decimal fatorQuantidade = grupamentoBolsa.FatorQuantidade.Value;
                decimal fatorPu = grupamentoBolsa.FatorPU.Value;
                byte tipoGrupamento = grupamentoBolsa.TipoGrupamento.Value;
                #endregion

                PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsaCompleta(idCliente, cdAtivoBolsa);
                for (int j = 0; j < posicaoTermoBolsaCollection.Count; j++)
                {
                    PosicaoTermoBolsa posicaoTermoBolsa = (PosicaoTermoBolsa)posicaoTermoBolsaCollection[j];
                    #region Dados de PosicaoTermoBolsa
                    decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                    decimal puCustoLiquidoAcao = posicaoTermoBolsa.PUCustoLiquidoAcao.Value;
                    decimal puTermo = posicaoTermoBolsa.PUTermo.Value;
                    decimal puTermoLiquido = posicaoTermoBolsa.PUTermoLiquido.Value;
                    #endregion

                    #region Calcula nova quantidade e PUs
                    decimal novaQuantidade = 0;
                    decimal puCustoLiquidoAcaoNovo = 0;
                    decimal puTermoNovo = 0;
                    decimal puTermoLiquidoNovo = 0;
                    if (tipoGrupamento == (int)TipoGrupamentoBolsa.Grupamento)
                    {
                        novaQuantidade = Utilitario.Truncate(quantidade / fatorQuantidade, 0);
                        puCustoLiquidoAcaoNovo = puCustoLiquidoAcao / fatorPu;
                        puTermoNovo = puTermo / fatorPu;
                        puTermoLiquidoNovo = puTermoLiquido / fatorPu;                        
                    }
                    else
                    {
                        novaQuantidade = Utilitario.Truncate(quantidade * fatorQuantidade, 0);
                        puCustoLiquidoAcaoNovo = puCustoLiquidoAcao * fatorPu;
                        puTermoNovo = puTermo * fatorPu;
                        puTermoLiquidoNovo = puTermoLiquido * fatorPu;
                    }
                    #endregion

                    #region Atualiza em posicaoTermoBolsa
                    posicaoTermoBolsa.Quantidade = novaQuantidade;
                    posicaoTermoBolsa.PUCustoLiquidoAcao = puCustoLiquidoAcaoNovo;
                    posicaoTermoBolsa.PUTermoLiquido = puTermoLiquidoNovo;
                    posicaoTermoBolsa.PUTermo = puTermoNovo;
                    #endregion
                }
                posicaoTermoBolsaCollection.Save();
            }
        }

        /// <summary>
        /// Gera eventos físicos em EventoFisicoBolsa, a partir de eventos de grupamento de GrupamentoBolsa.
        /// Leva em conta posicoes de bolsa do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>        
        public void GeraGrupamento(int idCliente, DateTime data)
        {
            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            EventoFisicoBolsa eventoFisicoBolsa = new EventoFisicoBolsa();
            EventoFisicoBolsaCollection eventoFisicoBolsaCollection = new EventoFisicoBolsaCollection();

            // Deleta os Eventos do dia do tipo Grupamento
            eventoFisicoBolsaCollection.DeletaEventoFisicoBolsa(idCliente, data, TipoEventoBolsa.Grupamento);
            //
            grupamentoBolsaCollection.BuscaGrupamentoBolsa(data);

            decimal quantidadeConvertida = 0;
            decimal puCustoConvertido = 0;
            decimal puCustoLiquidoConvertido = 0;
            string cdAtivoBolsaAnterior = "";
            for (int i = 0; i < grupamentoBolsaCollection.Count; i++)
            {
                GrupamentoBolsa grupamentoBolsa = grupamentoBolsaCollection[i];
                string cdAtivoBolsa = grupamentoBolsa.CdAtivoBolsa;
                decimal fatorPu = grupamentoBolsa.FatorPU.Value;
                decimal fatorQuantidade = grupamentoBolsa.FatorQuantidade.Value;
                int tipoGrupamento = grupamentoBolsa.TipoGrupamento.Value;

                bool geraEvento = false;
                decimal quantidadeConverter = 0;
                decimal puCustoConverter = 0;
                decimal puCustoLiquidoConverter = 0;
                if (cdAtivoBolsa != cdAtivoBolsaAnterior)
                {
                    cdAtivoBolsaAnterior = cdAtivoBolsa;
                    if (posicaoBolsa.BuscaPosicaoBolsa(idCliente, cdAtivoBolsa))
                    {
                        geraEvento = true;

                        quantidadeConverter = posicaoBolsa.Quantidade.Value;
                        puCustoConverter = posicaoBolsa.PUCusto.Value;
                        puCustoLiquidoConverter = posicaoBolsa.PUCustoLiquido.Value;
                    }

                    quantidadeConvertida = 0;
                    puCustoConvertido = 0;
                    puCustoLiquidoConvertido = 0;
                }
                else if (quantidadeConvertida != 0) //Está buscando a qtde convertida do step anterior!
                {
                    geraEvento = true;

                    //Usa os valores que foram calculados para este ativo no step anterior
                    //Artifício usado para casos específicos de grupamento e desdobro no mesmo dia para o mesmo ativo
                    quantidadeConverter = quantidadeConvertida;
                    puCustoConverter = puCustoConvertido;
                    puCustoLiquidoConverter = puCustoLiquidoConvertido;
                }

                if (geraEvento)
                {
                    if (tipoGrupamento == (int)TipoGrupamentoBolsa.Grupamento)
                    {
                        quantidadeConvertida = Utilitario.Truncate(Math.Abs(quantidadeConverter) / fatorQuantidade, 0);
                        puCustoConvertido = puCustoConverter / fatorPu;
                        puCustoLiquidoConvertido = puCustoLiquidoConverter / fatorPu;
                    }
                    else
                    {
                        quantidadeConvertida = Utilitario.Truncate(Math.Abs(quantidadeConverter) * fatorQuantidade, 0);
                        puCustoConvertido = puCustoConverter * fatorPu;
                        puCustoLiquidoConvertido = puCustoLiquidoConverter * fatorPu;
                    }

                    #region Evento 1 para zerar a posição atual
                    EventoFisicoBolsa eventoFisicoBolsaInserir = new EventoFisicoBolsa();
                    eventoFisicoBolsaInserir.AddNew();
                    eventoFisicoBolsaInserir.TipoEvento = (byte)TipoEventoBolsa.Grupamento;
                    eventoFisicoBolsaInserir.TipoMercado = grupamentoBolsa.UpToAtivoBolsaByCdAtivoBolsa.TipoMercado; //HIERARQUICO
                    eventoFisicoBolsaInserir.Data = data;
                    eventoFisicoBolsaInserir.IdEventoVinculado = (byte)grupamentoBolsa.IdGrupamento;
                    eventoFisicoBolsaInserir.IdCliente = idCliente;
                    eventoFisicoBolsaInserir.IdAgente = posicaoBolsa.IdAgente.Value;
                    eventoFisicoBolsaInserir.CdAtivoBolsa = cdAtivoBolsa;

                    if (quantidadeConverter < 0)
                    {
                        eventoFisicoBolsaInserir.TipoMovimento = TipoOperacaoBolsa.Deposito;
                    }
                    else
                    {
                        eventoFisicoBolsaInserir.TipoMovimento = TipoOperacaoBolsa.Retirada;
                    }

                    eventoFisicoBolsaInserir.Pu = puCustoConverter;
                    eventoFisicoBolsaInserir.PULiquido = puCustoLiquidoConverter;
                    eventoFisicoBolsaInserir.Quantidade = quantidadeConverter;

                    eventoFisicoBolsa.InsereEventoFisicoBolsa(eventoFisicoBolsaInserir);
                    #endregion

                    #region Evento 2 para inserir a posição com o fator de agrupamento
                    eventoFisicoBolsaInserir = new EventoFisicoBolsa();
                    eventoFisicoBolsaInserir.AddNew();
                    eventoFisicoBolsaInserir.TipoEvento = (byte)TipoEventoBolsa.Grupamento;
                    eventoFisicoBolsaInserir.TipoMercado = grupamentoBolsa.UpToAtivoBolsaByCdAtivoBolsa.TipoMercado; //HIERARQUICO
                    eventoFisicoBolsaInserir.Data = data;
                    eventoFisicoBolsaInserir.IdEventoVinculado = (byte)grupamentoBolsa.IdGrupamento;
                    eventoFisicoBolsaInserir.IdCliente = idCliente;
                    eventoFisicoBolsaInserir.IdAgente = posicaoBolsa.IdAgente.Value;
                    eventoFisicoBolsaInserir.CdAtivoBolsa = cdAtivoBolsa;

                    if (quantidadeConverter < 0)
                    {
                        eventoFisicoBolsaInserir.TipoMovimento = TipoOperacaoBolsa.Retirada;
                    }
                    else
                    {
                        eventoFisicoBolsaInserir.TipoMovimento = TipoOperacaoBolsa.Deposito;
                    }
                    eventoFisicoBolsaInserir.Pu = puCustoConvertido;
                    eventoFisicoBolsaInserir.PULiquido = puCustoLiquidoConvertido;
                    eventoFisicoBolsaInserir.Quantidade = quantidadeConvertida;

                    eventoFisicoBolsa.InsereEventoFisicoBolsa(eventoFisicoBolsaInserir);
                    #endregion
                }
            }
        }

    }
}
