﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa {
    public partial class PosicaoEmprestimoBolsaHistorico : esPosicaoEmprestimoBolsaHistorico 
    {
        /// <summary>
        /// Soma do valor de mercado de todas as posições historicas dado um cliente e uma data.
        /// Posições doadas são somadas positivamente, tomadas negativamente.
        /// Retorna 0 se não existir registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public decimal RetornaValorMercadoNet(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCliente == idCliente,
                        this.Query.DataVencimento.GreaterThanOrEqual(dataHistorico),
                        this.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador));

            this.Query.Load();


            decimal net = 0;
            net = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;

            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCliente == idCliente,
                        this.Query.DataVencimento.GreaterThanOrEqual(dataHistorico),
                        this.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Tomador));

            this.Query.Load();

            if (this.ValorMercado.HasValue)
            {
                net -= this.ValorMercado.Value;
            }

            return net;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="pontaEmprestimoBolsa"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, PontaEmprestimoBolsa pontaEmprestimoBolsa, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.query.DataHistorico == dataHistorico,
                        this.Query.PontaEmprestimo.Equal(pontaEmprestimoBolsa));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        public decimal RetornaQuantidade(int idCliente, DateTime dataHistorico, string cdAtivoBolsa, PontaEmprestimoBolsa pontaEmprestimoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.PontaEmprestimo.Equal(pontaEmprestimoBolsa),
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico, string cdAtivoBolsa, PontaEmprestimoBolsa pontaEmprestimoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.PontaEmprestimo.Equal(pontaEmprestimoBolsa),
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico, PontaEmprestimoBolsa pontaEmprestimoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.PontaEmprestimo.Equal(pontaEmprestimoBolsa));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }
    }
}
