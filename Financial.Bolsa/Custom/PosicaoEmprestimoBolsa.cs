﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using Financial.Common.Enums;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Util.Enums;
using Financial.Investidor.Enums;
using Financial.Interfaces.Import.Bolsa;
using Financial.Investidor;
using Financial.Common;
using Financial.Interfaces.Sinacor;
using Financial.Fundo.Enums;

namespace Financial.Bolsa {
    public partial class PosicaoEmprestimoBolsa : esPosicaoEmprestimoBolsa {

        /// <summary>
        /// Soma do valor de mercado de todas as posições dado um cliente e uma data.
        /// Posições doadas são somadas positivamente, tomadas negativamente.
        /// Retorna 0 se não existir registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public decimal RetornaValorMercadoNet(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador));

            this.Query.Load();


            decimal net = 0;
            net = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;

            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Tomador));

            this.Query.Load();

            if (this.ValorMercado.HasValue)
            {
                net -= this.ValorMercado.Value;
            }

            return net;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="pontaEmprestimoBolsa"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, PontaEmprestimoBolsa pontaEmprestimoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.PontaEmprestimo.Equal(pontaEmprestimoBolsa));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        public decimal RetornaQuantidade(int idCliente, string cdAtivoBolsa, PontaEmprestimoBolsa pontaEmprestimoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.PontaEmprestimo.Equal(pontaEmprestimoBolsa),
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        public decimal RetornaValorMercado(int idCliente, string cdAtivoBolsa, PontaEmprestimoBolsa pontaEmprestimoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.PontaEmprestimo.Equal(pontaEmprestimoBolsa),
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data) 
        {
            try
            {
                PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaDeletarHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                posicaoEmprestimoBolsaDeletarHistoricoCollection.DeletaPosicaoEmprestimoBolsaHistoricoDataHistoricoMaiorIgual(idCliente, data);
            }
            catch { }

            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            posicaoEmprestimoBolsaCollection.BuscaPosicaoEmprestimoBolsaCompleta(idCliente);
            //
            PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
            //
            #region Copia de posicaoEmprestimoBolsa para posicaoEmprestimoBolsaHistorico
            for (int i = 0; i < posicaoEmprestimoBolsaCollection.Count; i++) {
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection[i];

                // Copia de posicaoEmprestimoBolsa para posicaoEmprestimoBolsaHistorico
                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = posicaoEmprestimoBolsaHistoricoCollection.AddNew();
                //
                posicaoEmprestimoBolsaHistorico.DataHistorico = data;
                posicaoEmprestimoBolsaHistorico.IdPosicao = posicaoEmprestimoBolsa.IdPosicao;
                posicaoEmprestimoBolsaHistorico.IdOperacao = posicaoEmprestimoBolsa.IdOperacao;
                posicaoEmprestimoBolsaHistorico.IdAgente = posicaoEmprestimoBolsa.IdAgente;
                posicaoEmprestimoBolsaHistorico.IdCliente = posicaoEmprestimoBolsa.IdCliente;
                posicaoEmprestimoBolsaHistorico.CdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                posicaoEmprestimoBolsaHistorico.Quantidade = posicaoEmprestimoBolsa.Quantidade;
                posicaoEmprestimoBolsaHistorico.PUMercado = posicaoEmprestimoBolsa.PUMercado;                
                posicaoEmprestimoBolsaHistorico.PULiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal;
                posicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros = posicaoEmprestimoBolsa.ValorCorrigidoJuros;
                posicaoEmprestimoBolsaHistorico.ValorCorrigidoComissao = posicaoEmprestimoBolsa.ValorCorrigidoComissao;
                posicaoEmprestimoBolsaHistorico.ValorCorrigidoCBLC = posicaoEmprestimoBolsa.ValorCorrigidoCBLC;
                posicaoEmprestimoBolsaHistorico.ValorBase = posicaoEmprestimoBolsa.ValorBase;
                posicaoEmprestimoBolsaHistorico.PontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo;
                posicaoEmprestimoBolsaHistorico.DataRegistro = posicaoEmprestimoBolsa.DataRegistro;
                posicaoEmprestimoBolsaHistorico.DataVencimento = posicaoEmprestimoBolsa.DataVencimento;
                posicaoEmprestimoBolsaHistorico.TaxaOperacao = posicaoEmprestimoBolsa.TaxaOperacao;
                posicaoEmprestimoBolsaHistorico.TaxaComissao = posicaoEmprestimoBolsa.TaxaComissao;
                posicaoEmprestimoBolsaHistorico.NumeroContrato = posicaoEmprestimoBolsa.NumeroContrato;
                posicaoEmprestimoBolsaHistorico.TipoEmprestimo = posicaoEmprestimoBolsa.TipoEmprestimo;
                posicaoEmprestimoBolsaHistorico.ValorMercado = posicaoEmprestimoBolsa.ValorMercado;
                posicaoEmprestimoBolsaHistorico.IdTrader = posicaoEmprestimoBolsa.IdTrader;
                posicaoEmprestimoBolsaHistorico.ValorDiarioJuros = posicaoEmprestimoBolsa.ValorDiarioJuros;
                posicaoEmprestimoBolsaHistorico.ValorDiarioComissao = posicaoEmprestimoBolsa.ValorDiarioComissao;
                posicaoEmprestimoBolsaHistorico.ValorDiarioCBLC = posicaoEmprestimoBolsa.ValorDiarioCBLC;
                posicaoEmprestimoBolsaHistorico.PermiteDevolucaoAntecipada = posicaoEmprestimoBolsa.PermiteDevolucaoAntecipada;
                posicaoEmprestimoBolsaHistorico.DataInicialDevolucaoAntecipada = posicaoEmprestimoBolsa.DataInicialDevolucaoAntecipada;
            }
            #endregion
            
            posicaoEmprestimoBolsaHistoricoCollection.Save();         
        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCliente, DateTime data)
        {
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            posicaoEmprestimoBolsaCollection.BuscaPosicaoEmprestimoBolsaCompleta(idCliente);
            //
            PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection();
            //
            #region Copia de posicaoEmprestimoBolsa para posicaoEmprestimoBolsaAbertura
            for (int i = 0; i < posicaoEmprestimoBolsaCollection.Count; i++)
            {
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = (PosicaoEmprestimoBolsa)posicaoEmprestimoBolsaCollection[i];

                // Copia de posicaoEmprestimoBolsa para posicaoEmprestimoBolsaAbertura
                PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsaAbertura = posicaoEmprestimoBolsaAberturaCollection.AddNew();
                //
                posicaoEmprestimoBolsaAbertura.DataHistorico = data;
                posicaoEmprestimoBolsaAbertura.IdPosicao = posicaoEmprestimoBolsa.IdPosicao;
                posicaoEmprestimoBolsaAbertura.IdOperacao = posicaoEmprestimoBolsa.IdOperacao;
                posicaoEmprestimoBolsaAbertura.IdAgente = posicaoEmprestimoBolsa.IdAgente;
                posicaoEmprestimoBolsaAbertura.IdCliente = posicaoEmprestimoBolsa.IdCliente;
                posicaoEmprestimoBolsaAbertura.CdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                posicaoEmprestimoBolsaAbertura.Quantidade = posicaoEmprestimoBolsa.Quantidade;
                posicaoEmprestimoBolsaAbertura.PUMercado = posicaoEmprestimoBolsa.PUMercado;
                posicaoEmprestimoBolsaAbertura.PULiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal;
                posicaoEmprestimoBolsaAbertura.ValorCorrigidoJuros = posicaoEmprestimoBolsa.ValorCorrigidoJuros;
                posicaoEmprestimoBolsaAbertura.ValorCorrigidoComissao = posicaoEmprestimoBolsa.ValorCorrigidoComissao;
                posicaoEmprestimoBolsaAbertura.ValorCorrigidoCBLC = posicaoEmprestimoBolsa.ValorCorrigidoCBLC;
                posicaoEmprestimoBolsaAbertura.ValorBase = posicaoEmprestimoBolsa.ValorBase;
                posicaoEmprestimoBolsaAbertura.PontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo;
                posicaoEmprestimoBolsaAbertura.DataRegistro = posicaoEmprestimoBolsa.DataRegistro;
                posicaoEmprestimoBolsaAbertura.DataVencimento = posicaoEmprestimoBolsa.DataVencimento;
                posicaoEmprestimoBolsaAbertura.TaxaOperacao = posicaoEmprestimoBolsa.TaxaOperacao;
                posicaoEmprestimoBolsaAbertura.TaxaComissao = posicaoEmprestimoBolsa.TaxaComissao;
                posicaoEmprestimoBolsaAbertura.NumeroContrato = posicaoEmprestimoBolsa.NumeroContrato;
                posicaoEmprestimoBolsaAbertura.TipoEmprestimo = posicaoEmprestimoBolsa.TipoEmprestimo;
                posicaoEmprestimoBolsaAbertura.ValorMercado = posicaoEmprestimoBolsa.ValorMercado;
                posicaoEmprestimoBolsaAbertura.IdTrader = posicaoEmprestimoBolsa.IdTrader;
                posicaoEmprestimoBolsaAbertura.ValorDiarioJuros = posicaoEmprestimoBolsa.ValorDiarioJuros;
                posicaoEmprestimoBolsaAbertura.ValorDiarioComissao = posicaoEmprestimoBolsa.ValorDiarioComissao;
                posicaoEmprestimoBolsaAbertura.ValorDiarioCBLC = posicaoEmprestimoBolsa.ValorDiarioCBLC;
                posicaoEmprestimoBolsaAbertura.PermiteDevolucaoAntecipada = posicaoEmprestimoBolsa.PermiteDevolucaoAntecipada;
                posicaoEmprestimoBolsaAbertura.DataInicialDevolucaoAntecipada = posicaoEmprestimoBolsa.DataInicialDevolucaoAntecipada;
            }
            #endregion

            posicaoEmprestimoBolsaAberturaCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// throws FatorCotacaoNaoCadastradoException
        public void AtualizaValorEmprestimo(int idCliente, DateTime data, bool atualizaValoresDiarios) 
        {
            //Limpa a Liquidacao de lctos de apropriação de juros e taxas de empréstimos
            List<int> origens = new List<int>();
            origens.Add((int)OrigemLancamentoLiquidacao.Bolsa.EmprestimoDoado);
            origens.Add((int)OrigemLancamentoLiquidacao.Bolsa.EmprestimoTomado);            
            List<int> fontes = new List<int>();
            fontes.Add((int)FonteLancamentoLiquidacao.Interno);

            LiquidacaoCollection liquidacaoCollectionDeletar = new LiquidacaoCollection();
            liquidacaoCollectionDeletar.DeletaLiquidacao(idCliente, origens, fontes);
            //

            TabelaCustosBolsa tabelaCustosBolsa = new TabelaCustosBolsa();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            //
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            posicaoEmprestimoBolsaCollection.BuscaPosicaoEmprestimoBolsa(idCliente);

            int idContaDefault = 0;
            if (posicaoEmprestimoBolsaCollection.Count > 0)
            {
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                idContaDefault = contaCorrente.RetornaContaDefault(idCliente);                
            }

            DateTime dataAnterior = new DateTime();
            if (atualizaValoresDiarios && posicaoEmprestimoBolsaCollection.Count > 0)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            }

            #region Busca o tipo de cotação - média ou fechamento
            ClienteBolsa clienteBolsa = new ClienteBolsa();
            clienteBolsa.Query.Select(clienteBolsa.Query.TipoCotacao);
            clienteBolsa.Query.Where(clienteBolsa.Query.IdCliente.Equal(idCliente));
            clienteBolsa.Query.Load();

            if (!clienteBolsa.es.HasData)
            {
                throw new ClienteBolsaNaoCadastradoException("Cliente " + idCliente + " sem cadastro para Bolsa.");
            }
            int tipoCotacao = clienteBolsa.TipoCotacao.Value;
            #endregion

            for (int i = 0; i < posicaoEmprestimoBolsaCollection.Count; i++) 
            {
                #region Valores PosicaoEmprestimoBolsa
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = (PosicaoEmprestimoBolsa)posicaoEmprestimoBolsaCollection[i];
                int idPosicao = posicaoEmprestimoBolsa.IdPosicao.Value;
                string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                decimal valorBase = posicaoEmprestimoBolsa.ValorBase.Value;                
                DateTime dataRegistro = posicaoEmprestimoBolsa.DataRegistro.Value;
                DateTime dataVencimento = posicaoEmprestimoBolsa.DataVencimento.Value;
                int idAgente = posicaoEmprestimoBolsa.IdAgente.Value;                
                decimal taxaComissao = posicaoEmprestimoBolsa.TaxaComissao.Value;
                #endregion

                decimal taxaOperacao = posicaoEmprestimoBolsa.TaxaOperacao.Value;

                FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);
                
                #region Custos
                decimal taxaLiquidacaoCBLC = 0;
                decimal valorMinimoCBLC = 0;
                if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador())
                {
                    if (posicaoEmprestimoBolsa.IsTipoEmprestimoVoluntario())
                    {
                        tabelaCustosBolsa.BuscaTabelaCustosBolsa(data, (Int16)TipoTaxaBolsa.EmprestimoBolsa.Voluntario);

                        if (tabelaCustosBolsa.MinimoCBLC.HasValue)
                        {
                            valorMinimoCBLC = tabelaCustosBolsa.MinimoCBLC.Value;
                        }
                    }
                    else
                    {
                        tabelaCustosBolsa.BuscaTabelaCustosBolsa(data, (Int16)TipoTaxaBolsa.EmprestimoBolsa.Compulsorio);
                    }

                    taxaLiquidacaoCBLC = tabelaCustosBolsa.PercentualLiquidacaoCBLC.Value;
                }                
                #endregion
                
                #region Fatores
                decimal fatorOperacao = CalculoFinanceiro.CalculaFatorPreExponencial(dataRegistro, data, taxaOperacao, BaseCalculo.Base252, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                decimal fatorComissao = 1;
                decimal fatorTaxaCBLC = 1;
               
                fatorComissao = CalculoFinanceiro.CalculaFatorPreExponencial(dataRegistro, data, taxaComissao, BaseCalculo.Base252,
                                                (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador())
                {
                    if (taxaLiquidacaoCBLC != 0)
                    {
                        fatorTaxaCBLC = CalculoFinanceiro.CalculaFatorPreExponencial(dataRegistro, data, taxaLiquidacaoCBLC, BaseCalculo.Base252,
                                                    (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }
                }

                #endregion
                               
                #region Novos Valores da posicaoEmprestimoBolsa
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                bool existeCotacao = cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsa, data);

                decimal puCotacao = 0;
                decimal valorMercado = 0;
                if (existeCotacao)
                {
                    if (tipoCotacao == (byte)TipoCotacaoBolsa.Fechamento)
                    {
                        puCotacao = cotacaoBolsa.PUFechamento.Value;                        
                    }
                    else
                    {
                        puCotacao = cotacaoBolsa.PUMedio.Value;                        
                    }

                    valorMercado = Utilitario.Truncate((puCotacao * quantidade) / fatorCotacao.Fator.Value, 2);
                }
                
                decimal valorCorrigidoJuros = Utilitario.Truncate(valorBase * fatorOperacao, 2);
                decimal valorCorrigidoComissao = Utilitario.Truncate(valorBase * fatorComissao, 2);
                decimal valorCorrigidoCBLC =  Utilitario.Truncate(valorBase * fatorTaxaCBLC, 2);                
                decimal valorJuros = valorCorrigidoJuros - valorBase;
                decimal valorComissao = valorCorrigidoComissao - valorBase;
                decimal valorCBLC = valorCorrigidoCBLC - valorBase;
                decimal valorCBLCProRata = Utilitario.Truncate(calculoCBLCProRata(dataRegistro, data, dataVencimento, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil, valorMinimoCBLC), 2);

                if (valorCBLC < valorCBLCProRata)
                {
                    valorCBLC = valorCBLCProRata;
                    valorCorrigidoCBLC = valorBase + valorCBLC;
                }
                
                // Faz o Update de PosicaoEmprestimoBolsa
                posicaoEmprestimoBolsa.ValorCorrigidoJuros = valorCorrigidoJuros;
                posicaoEmprestimoBolsa.ValorCorrigidoComissao = valorCorrigidoComissao;
                posicaoEmprestimoBolsa.ValorCorrigidoCBLC = valorCorrigidoCBLC;

                if (existeCotacao)
                {
                    posicaoEmprestimoBolsa.PUMercado = puCotacao;
                    posicaoEmprestimoBolsa.ValorMercado = valorMercado;
                }

                if (atualizaValoresDiarios)
                {
                    PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
                    posicaoEmprestimoBolsaHistorico.Query.Select(posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoCBLC,
                                                                posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoComissao,
                                                                posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoJuros);
                    posicaoEmprestimoBolsaHistorico.Query.Where(posicaoEmprestimoBolsaHistorico.Query.IdPosicao.Equal(idPosicao),
                                                               posicaoEmprestimoBolsaHistorico.Query.DataHistorico.Equal(dataAnterior));
                    if (posicaoEmprestimoBolsaHistorico.Query.Load())
                    {
                        posicaoEmprestimoBolsa.ValorDiarioCBLC = posicaoEmprestimoBolsa.ValorCorrigidoCBLC.Value - posicaoEmprestimoBolsaHistorico.ValorCorrigidoCBLC.Value;
                        posicaoEmprestimoBolsa.ValorDiarioComissao = posicaoEmprestimoBolsa.ValorCorrigidoComissao.Value - posicaoEmprestimoBolsaHistorico.ValorCorrigidoComissao.Value;
                        posicaoEmprestimoBolsa.ValorDiarioJuros = posicaoEmprestimoBolsa.ValorCorrigidoJuros.Value - posicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros.Value;
                    }
                }
                #endregion

                #region Novos Valores da liquidacao
                decimal valorLiquidacao = 0;
                string descricao = "";
                int origemLiquidacao = (int) OrigemLancamentoLiquidacao.Bolsa.None;
                if ( posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador() ) {
                    valorLiquidacao = valorJuros;
                    descricao = "Juros Empréstimo Doador - " + quantidade.ToString() + " " + cdAtivoBolsa;
                    origemLiquidacao = (int) OrigemLancamentoLiquidacao.Bolsa.EmprestimoDoado;
                }
                else if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador()) {
                    descricao = "Custos Empréstimo Tomador (Juros + Taxas) - " + quantidade.ToString() + " " + cdAtivoBolsa;
                    valorLiquidacao = (valorJuros + valorComissao + valorCBLC) * -1;
                    origemLiquidacao = (int) OrigemLancamentoLiquidacao.Bolsa.EmprestimoTomado;
                }

                #region Insercao Liquidacao
                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataVencimento;
                liquidacao.Descricao = descricao;
                liquidacao.Valor = valorLiquidacao;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = origemLiquidacao;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdAgente = idAgente;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdentificadorOrigem = idPosicao;
                liquidacao.IdConta = idContaDefault;
                liquidacaoCollection.AttachEntity(liquidacao);
                #endregion                               
                #endregion
            }
                            
            liquidacaoCollection.Save();         
            //
            posicaoEmprestimoBolsaCollection.Save();
        }

        public decimal calculoCBLCProRata(DateTime dataRegistro, DateTime data, DateTime dataVencimento, int idLocal, TipoFeriado tipoFeriado, decimal valorMinimoCBLC)
        {
            int numeroDias = Calendario.NumeroDias(dataRegistro, data, idLocal, tipoFeriado);
            int totalDias = Calendario.NumeroDias(dataRegistro, dataVencimento, idLocal, tipoFeriado);

            return valorMinimoCBLC / totalDias * numeroDias;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// throws FatorCotacaoNaoCadastradoException se fator não estiver cadastrado
        public void ProcessaVencimentoEmprestimo(int idCliente, DateTime data) 
        {
            //Deleta antes todas as liquidações (por vencimento) na data
            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollectionDeletar = new LiquidacaoEmprestimoBolsaCollection();
            liquidacaoEmprestimoBolsaCollectionDeletar.DeletaLiquidacaoEmprestimoBolsaVencimento(idCliente, data);
            //

            // Collections que serão usadas para salvar tudo de uma vez no final do loop            
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
            //
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            posicaoEmprestimoBolsaCollection.BuscaPosicaoEmprestimoBolsa(idCliente, data);
            for (int i = 0; i < posicaoEmprestimoBolsaCollection.Count; i++) 
            {
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection[i];

                #region Valores posicaoEmprestimoBolsa
                int? idOperacao = null;
                if (posicaoEmprestimoBolsa.IdOperacao.HasValue)
                {
                    idOperacao = posicaoEmprestimoBolsa.IdOperacao.Value;
                }
                int idPosicao = posicaoEmprestimoBolsa.IdPosicao.Value;
                int idAgente = posicaoEmprestimoBolsa.IdAgente.Value;
                string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;                
                decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;
                decimal valorCorrigidoJuros = posicaoEmprestimoBolsa.ValorCorrigidoJuros.Value;
                decimal valorCorrigidoComissao = posicaoEmprestimoBolsa.ValorCorrigidoComissao.Value;
                decimal valorCorrigidoCBLC = posicaoEmprestimoBolsa.ValorCorrigidoCBLC.Value;
                decimal valorBase = posicaoEmprestimoBolsa.ValorBase.Value;
                byte pontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo.Value;
                decimal puMercado = posicaoEmprestimoBolsa.PUMercado.Value;
                decimal valorMercado = posicaoEmprestimoBolsa.ValorMercado.Value;
                int numeroContrato = posicaoEmprestimoBolsa.NumeroContrato.Value;
                #endregion                
                
                decimal valorLiquidacao = 0;

                /* Se importou do DBTL o vencimento do emprestimo então nao lanca novamente a liquidação 
                 * e considera como valor do vencimento o que veio no arquivo */
                LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollectionAux = new LiquidacaoEmprestimoBolsaCollection();

                bool lancaLiquidacao = true;

                if (numeroContrato != 0 || idOperacao.HasValue)
                {
                    if (numeroContrato != 0 && liquidacaoEmprestimoBolsaCollectionAux.BuscaValorVencimentoPorContrato(numeroContrato, data))
                    {
                        lancaLiquidacao = false;
                    }
                    else if (idOperacao.HasValue)
                    {
                        if (liquidacaoEmprestimoBolsaCollectionAux.BuscaValorVencimento(idOperacao.Value, data))
                        {
                            lancaLiquidacao = false;
                        }
                    }
                }

                if (lancaLiquidacao)
                {
                    if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador())
                    {
                        valorLiquidacao = valorCorrigidoJuros - valorBase;
                    }
                    else if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador())
                    {
                        valorLiquidacao = (valorCorrigidoJuros + valorCorrigidoComissao + valorCorrigidoCBLC) - valorBase * 3M;
                    }

                    #region Insercao LiquidacaoEmprestimo
                    LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = liquidacaoEmprestimoBolsaCollection.AddNew();
                    liquidacaoEmprestimoBolsa.Data = data;
                    liquidacaoEmprestimoBolsa.TipoLiquidacao = (byte)TipoLiquidacaoEmprestimoBolsa.Vencimento;
                    liquidacaoEmprestimoBolsa.IdOperacao = idOperacao;
                    liquidacaoEmprestimoBolsa.IdPosicao = idPosicao;
                    liquidacaoEmprestimoBolsa.IdAgente = idAgente;
                    liquidacaoEmprestimoBolsa.IdCliente = idCliente;
                    liquidacaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    liquidacaoEmprestimoBolsa.Quantidade = quantidade;
                    liquidacaoEmprestimoBolsa.Valor = valorMercado;
                    liquidacaoEmprestimoBolsa.Fonte = (byte)FonteLiquidacaoEmprestimoBolsa.Manual;
                    liquidacaoEmprestimoBolsa.ValorLiquidacao = valorLiquidacao;
                    liquidacaoEmprestimoBolsa.NumeroContrato = numeroContrato;
                    #endregion                                
                }
            }

            // Update das Collections
            liquidacaoEmprestimoBolsaCollection.Save();
            //
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo do Emprestimo da Posicao é Voluntario
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoEmprestimoVoluntario() {
            return this.TipoEmprestimo == (byte) TipoEmprestimoBolsa.Voluntario;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Tipo do Emprestimo da Posicao é Compulsorio
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoEmprestimoCompulsorio() {
            return this.TipoEmprestimo == (byte) TipoEmprestimoBolsa.Compulsorio;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Ponta Emprestimo da posicao é Doador
        ///          False caso Contrario
        /// </returns>
        public bool IsPontaEmprestimoBolsaDoador() {
            return this.PontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se Ponta Emprestimo da posicao é Tomador
        ///          False caso Contrario
        /// </returns>
        public bool IsPontaEmprestimoBolsaTomador() {
            return this.PontaEmprestimo == (byte)PontaEmprestimoBolsa.Tomador;
        }

        /// <summary>
        /// Carrega o objeto PosicaoEmprestimoBolsaCollection com todos os campos de PosicaoEmprestimoBolsa.
        /// </summary>
        /// <param name="idOperacao"></param>
        public void BuscaPosicaoEmprestimoBolsa(int idOperacao)
        {
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdOperacao.Equal(idOperacao));
            posicaoEmprestimoBolsaCollection.Query.Load();

            this.CdAtivoBolsa = posicaoEmprestimoBolsaCollection[0].CdAtivoBolsa;
            this.DataRegistro = posicaoEmprestimoBolsaCollection[0].DataRegistro;
            this.DataVencimento = posicaoEmprestimoBolsaCollection[0].DataVencimento;
            this.IdAgente = posicaoEmprestimoBolsaCollection[0].IdAgente;
            this.IdCliente = posicaoEmprestimoBolsaCollection[0].IdCliente;
            this.IdOperacao = posicaoEmprestimoBolsaCollection[0].IdOperacao;
            this.IdPosicao = posicaoEmprestimoBolsaCollection[0].IdPosicao;
            this.PontaEmprestimo = posicaoEmprestimoBolsaCollection[0].PontaEmprestimo;
            this.PULiquidoOriginal = posicaoEmprestimoBolsaCollection[0].PULiquidoOriginal;
            this.PUMercado = posicaoEmprestimoBolsaCollection[0].PUMercado;
            this.Quantidade = posicaoEmprestimoBolsaCollection[0].Quantidade;
            this.TaxaComissao = posicaoEmprestimoBolsaCollection[0].TaxaComissao;
            this.TaxaOperacao = posicaoEmprestimoBolsaCollection[0].TaxaOperacao;
            this.ValorBase = posicaoEmprestimoBolsaCollection[0].ValorBase;
            this.ValorCorrigidoCBLC = posicaoEmprestimoBolsaCollection[0].ValorCorrigidoCBLC;
            this.ValorCorrigidoComissao = posicaoEmprestimoBolsaCollection[0].ValorCorrigidoComissao;
            this.ValorCorrigidoJuros = posicaoEmprestimoBolsaCollection[0].ValorCorrigidoJuros;
            this.ValorMercado = posicaoEmprestimoBolsaCollection[0].ValorMercado;
            this.ValorDiarioCBLC = posicaoEmprestimoBolsaCollection[0].ValorDiarioCBLC;
            this.ValorDiarioComissao = posicaoEmprestimoBolsaCollection[0].ValorDiarioComissao;
            this.ValorDiarioJuros = posicaoEmprestimoBolsaCollection[0].ValorDiarioJuros;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbtcCollection"></param>
        /// <param name="data">Nome do Arquivo Completo</param>
        /// throws AtivoNaoCadastradoException se codigo do Ativo não estiver cadastrado na Base
        public void CarregaPosicaoEmprestimoDbtc(DbtcCollection dbtcCollection, DateTime data)
        {
            #region GetIdAgente do Arquivo Dbtc

            int codigoBovespaAgente = dbtcCollection.CollectionDbtc[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            Dbtc dbtc = new Dbtc();
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();

            List<int> listaClientesDeletar = new List<int>();

            // Pega as Posições Emprestimo do arquivo de Dbtc                            
            for (int i = 0; i < dbtcCollection.CollectionDbtc.Count; i++)
            {
                dbtc = dbtcCollection.CollectionDbtc[i];

                #region Copia informações para PosicaoEmprestimoBolsa
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();

                int? codigoClienteBovespa = null;
                // OBS: Cuidado quando o tipoRegistro for header
                if (dbtc.IsTipoRegistroDoador())
                {   
                    codigoClienteBovespa = dbtc.CodigoClienteDoador.Value;
                    // Setar posicaoEmprestimoBolsa.IdCliente depois
                    posicaoEmprestimoBolsa.CdAtivoBolsa = !String.IsNullOrEmpty(dbtc.CdAtivoBolsaDoador) ? dbtc.CdAtivoBolsaDoador : "";
                    posicaoEmprestimoBolsa.PontaEmprestimo = (byte)PontaEmprestimoBolsa.Doador;
                    posicaoEmprestimoBolsa.DataRegistro = dbtc.DataRegistroDoador.Value;
                    posicaoEmprestimoBolsa.DataVencimento = dbtc.DataVencimentoDoador.Value;
                    posicaoEmprestimoBolsa.TaxaOperacao = dbtc.TaxaOperacaoDoador.HasValue ? dbtc.TaxaOperacaoDoador.Value : 0;
                    posicaoEmprestimoBolsa.TaxaComissao = dbtc.TaxaComissaoDoador.HasValue ? dbtc.TaxaComissaoDoador.Value : 0;
                    posicaoEmprestimoBolsa.Quantidade = dbtc.QuantidadeDoador.HasValue ? dbtc.QuantidadeDoador.Value : 0;
                    posicaoEmprestimoBolsa.PUMercado = 0;                    
                    posicaoEmprestimoBolsa.ValorBase = Utilitario.Truncate((dbtc.QuantidadeDoador.Value * dbtc.PuDoador.Value / dbtc.FatorCotacaoDoador.Value), 2);
                    posicaoEmprestimoBolsa.NumeroContrato = dbtc.NumeroContratoDoador.HasValue ? dbtc.NumeroContratoDoador.Value : 0;
                    byte tipoEmprestimo = dbtc.TipoOrigemDoador.Equals("AUTOMATICO") ? (byte)TipoEmprestimoBolsa.Compulsorio : (byte)TipoEmprestimoBolsa.Voluntario;
                    posicaoEmprestimoBolsa.TipoEmprestimo = tipoEmprestimo;
                }
                else if (dbtc.IsTipoRegistroTomador())
                {
                    codigoClienteBovespa = dbtc.CodigoClienteTomador.Value;
                    // Setar posicaoEmprestimoBolsa.IdCliente depois
                    posicaoEmprestimoBolsa.CdAtivoBolsa = !String.IsNullOrEmpty(dbtc.CdAtivoBolsaTomador) ? dbtc.CdAtivoBolsaTomador : "";
                    posicaoEmprestimoBolsa.PontaEmprestimo = (byte)PontaEmprestimoBolsa.Tomador;
                    posicaoEmprestimoBolsa.DataRegistro = dbtc.DataRegistroTomador.Value;
                    posicaoEmprestimoBolsa.DataVencimento = dbtc.DataVencimentoTomador.Value;
                    posicaoEmprestimoBolsa.TaxaOperacao = dbtc.TaxaOperacaoTomador.HasValue ? dbtc.TaxaOperacaoTomador.Value : 0;
                    posicaoEmprestimoBolsa.TaxaComissao = dbtc.TaxaComissaoTomador.HasValue ? dbtc.TaxaComissaoTomador.Value : 0;
                    posicaoEmprestimoBolsa.Quantidade = dbtc.QuantidadeTomador.HasValue ? dbtc.QuantidadeTomador.Value : 0;
                    posicaoEmprestimoBolsa.PUMercado = 0;
                    posicaoEmprestimoBolsa.PULiquidoOriginal = 0;
                    posicaoEmprestimoBolsa.ValorBase = Utilitario.Truncate((dbtc.QuantidadeTomador.Value * dbtc.PuTomador.Value / dbtc.FatorCotacaoTomador.Value), 2);
                    posicaoEmprestimoBolsa.NumeroContrato = dbtc.NumeroContratoTomador.HasValue ? dbtc.NumeroContratoTomador.Value : 0;
                    byte tipoEmprestimo = dbtc.TipoOrigemTomador.Equals("AUTOMATICO") ? (byte)TipoEmprestimoBolsa.Compulsorio : (byte)TipoEmprestimoBolsa.Voluntario;
                    posicaoEmprestimoBolsa.TipoEmprestimo = tipoEmprestimo;
                }

                // Só inclui os registros que forem doador ou tomador
                if (dbtc.IsTipoRegistroTomador() || dbtc.IsTipoRegistroDoador())
                {
                    // Só inclui os clientes que existirem na Base
                    int codigoClienteBovespaAux = codigoClienteBovespa.Value;
                    CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                    bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespaAux);

                    if (idClienteExiste)
                    {
                        Cliente cliente = new Cliente();
                        cliente.LoadByPrimaryKey(codigoClienteAgente.IdCliente.Value);

                        //Só importa para clientes ativos
                        if (cliente.IsAtivo)
                        {
                            //Só importa para clientes com dataDia anterior à data passada ou data igual e status != fechado
                            if (DateTime.Compare(cliente.DataDia.Value, data) == 0 && cliente.Status.Value != (byte)StatusCliente.Divulgado)
                            {
                                posicaoEmprestimoBolsa.IdCliente = codigoClienteAgente.IdCliente.Value;
                                posicaoEmprestimoBolsa.IdAgente = idAgente;
                                posicaoEmprestimoBolsa.ValorCorrigidoCBLC = 0;
                                posicaoEmprestimoBolsa.ValorCorrigidoComissao = 0;
                                posicaoEmprestimoBolsa.ValorCorrigidoJuros = 0;
                                posicaoEmprestimoBolsa.ValorDiarioCBLC = 0;
                                posicaoEmprestimoBolsa.ValorDiarioComissao = 0;
                                posicaoEmprestimoBolsa.ValorDiarioJuros = 0;

                                if (posicaoEmprestimoBolsa.PontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador)
                                {
                                    decimal puLiquidoPosicao = 0;
                                    PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                                    // Levo PuLiquido da posição original para a posicao do Emprestimo,
                                    // de modo a saber o PU quando a posição voltar a carteira
                                    if (posicaoBolsa.BuscaPuLiquido(posicaoEmprestimoBolsa.IdCliente.Value, idAgente, posicaoEmprestimoBolsa.CdAtivoBolsa))
                                    {
                                        puLiquidoPosicao = posicaoBolsa.PUCustoLiquido.Value;
                                    }

                                    posicaoEmprestimoBolsa.PULiquidoOriginal = puLiquidoPosicao;
                                }
                                
                                // lancamento de AtivoNaoCadastradoException                                       
                                AtivoBolsa ativoBolsa = new AtivoBolsa();
                                if (!ativoBolsa.LoadByPrimaryKey(posicaoEmprestimoBolsa.CdAtivoBolsa))
                                {
                                    throw new AtivoNaoCadastradoException("Ativo não cadastrado: " + posicaoEmprestimoBolsa.CdAtivoBolsa);
                                }

                                //Inclui na collection
                                posicaoEmprestimoBolsaCollection.AttachEntity(posicaoEmprestimoBolsa);

                                if (!listaClientesDeletar.Contains(cliente.IdCliente.Value))
                                {
                                    listaClientesDeletar.Add(cliente.IdCliente.Value);
                                }
                            }
                        }
                    }
                }
                //
                #endregion
                
            }

            if (listaClientesDeletar.Count > 0)
            {
                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollectionDeletar = new PosicaoEmprestimoBolsaCollection();
                posicaoEmprestimoBolsaCollectionDeletar.Query.Where(posicaoEmprestimoBolsaCollectionDeletar.Query.IdCliente.In(listaClientesDeletar));
                posicaoEmprestimoBolsaCollectionDeletar.Query.Load();
                posicaoEmprestimoBolsaCollectionDeletar.MarkAllAsDeleted();
                posicaoEmprestimoBolsaCollectionDeletar.Save();

                PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollectionDeletar = new PosicaoEmprestimoBolsaAberturaCollection();
                posicaoEmprestimoBolsaAberturaCollectionDeletar.Query.Where(posicaoEmprestimoBolsaAberturaCollectionDeletar.Query.IdCliente.In(listaClientesDeletar),
                                                                        posicaoEmprestimoBolsaAberturaCollectionDeletar.Query.DataHistorico.Equal(data));
                posicaoEmprestimoBolsaAberturaCollectionDeletar.Query.Load();
                posicaoEmprestimoBolsaAberturaCollectionDeletar.MarkAllAsDeleted();
                posicaoEmprestimoBolsaAberturaCollectionDeletar.Save();
            }

            posicaoEmprestimoBolsaCollection.Save();

            PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = 
                                new PosicaoEmprestimoBolsaAberturaCollection(posicaoEmprestimoBolsaCollection, data);
            posicaoEmprestimoBolsaAberturaCollection.Save();
        }

        /// <summary>
        /// Importa as posições a partir da VCFPOSIBTC (BTC).
        /// </summary>
        /// <param name="idCarteira"></param>
        /// <param name="data"></param>
        public void ImportaPosicoesSinacor(int idCliente, DateTime data)
        {
            int codigoCliente = 0;
            int codigoCliente2 = 0;
            #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
            ClienteBolsa clienteBolsa = new ClienteBolsa();

            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!clienteBolsa.LoadByPrimaryKey(idCliente))
            {
                throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ Cliente ");
            }

            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor) && String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ IdCliente ");
            }

            //Se nenhum dos 2 códigos é número, lança exception
            if (!Utilitario.IsInteger(clienteBolsa.CodigoSinacor) && !Utilitario.IsInteger(clienteBolsa.CodigoSinacor2))
            {
                throw new CodigoClienteBovespaInvalido("Cód. Bovespa Inválido/Inexistente p/ o IdCliente ");
            }

            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = Convert.ToInt32(clienteBolsa.CodigoSinacor);
            }

            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = Convert.ToInt32(clienteBolsa.CodigoSinacor2);
            }

            //Se algum dos 2 códigos for nulo, repete o código de um para ao outro
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = codigoCliente2;
            }

            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = codigoCliente;
            }
            #endregion

            //Busca o IdAgenteMercado default
            int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
            AgenteMercado agenteMercado = new AgenteMercado();
            int idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);

            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            #region BTC (Tomador)
            VcfposiBtcCollection vcfposiBtcCollection = new VcfposiBtcCollection();
            vcfposiBtcCollection.es.Connection.Name = "Sinacor";
            vcfposiBtcCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            vcfposiBtcCollection.Query.Where(
                                (vcfposiBtcCollection.Query.CodCli == codigoCliente | vcfposiBtcCollection.Query.CodCli == codigoCliente2));
            vcfposiBtcCollection.Query.Where(vcfposiBtcCollection.Query.TipoCotr == "T");
            vcfposiBtcCollection.Query.GroupBy(vcfposiBtcCollection.Query.CodNeg, vcfposiBtcCollection.Query.DataVenc);
            vcfposiBtcCollection.Query.Load();

            foreach (VcfposiBtc vcfposiBtc in vcfposiBtcCollection)
            {
                string cdAtivoBolsa = vcfposiBtc.CodNeg;
                decimal quantidade = Math.Abs(vcfposiBtc.QtdeAcoe.Value);
                DateTime dataVencimento = vcfposiBtc.DataVenc.Value;
                DateTime dataRegistro = vcfposiBtc.DataAber.Value;
                decimal taxa = vcfposiBtc.TaxaRemu.Value;
                int numeroContrato = (int)vcfposiBtc.NumCotr.Value;
                decimal puBase = vcfposiBtc.PrecMed.Value;

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                decimal valorBase = Utilitario.Truncate(quantidade * puBase / fatorCotacaoBolsa.Fator.Value, 2);

                decimal cotacaoFechamento = 0;
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                cotacaoBolsa.Query.Select(cotacaoBolsa.Query.PUFechamento);
                cotacaoBolsa.Query.Where(cotacaoBolsa.Query.Data.Equal(data),
                                         cotacaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                if (cotacaoBolsa.Query.Load())
                {
                    cotacaoFechamento = cotacaoBolsa.PUFechamento.Value;
                }

                decimal valorMercado = Utilitario.Truncate(quantidade * cotacaoFechamento / fatorCotacaoBolsa.Fator.Value, 2);

                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection.AddNew();
                posicaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                posicaoEmprestimoBolsa.DataRegistro = dataRegistro;
                posicaoEmprestimoBolsa.DataVencimento = dataVencimento;
                posicaoEmprestimoBolsa.IdAgente = idAgenteMercado;
                posicaoEmprestimoBolsa.IdCliente = idCliente;
                posicaoEmprestimoBolsa.NumeroContrato = numeroContrato;
                posicaoEmprestimoBolsa.PontaEmprestimo = (byte)PontaEmprestimoBolsa.Tomador;
                posicaoEmprestimoBolsa.PULiquidoOriginal = 0;
                posicaoEmprestimoBolsa.PUMercado = cotacaoFechamento;
                posicaoEmprestimoBolsa.Quantidade = quantidade;
                posicaoEmprestimoBolsa.TaxaComissao = 0;
                posicaoEmprestimoBolsa.TaxaOperacao = taxa;
                posicaoEmprestimoBolsa.TipoEmprestimo = (byte)TipoEmprestimoBolsa.Voluntario;
                posicaoEmprestimoBolsa.ValorBase = valorBase;
                posicaoEmprestimoBolsa.ValorCorrigidoCBLC = valorBase;
                posicaoEmprestimoBolsa.ValorCorrigidoComissao = valorBase;
                posicaoEmprestimoBolsa.ValorCorrigidoJuros = valorBase;
                posicaoEmprestimoBolsa.ValorDiarioCBLC = 0;
                posicaoEmprestimoBolsa.ValorDiarioComissao = 0;
                posicaoEmprestimoBolsa.ValorDiarioJuros = 0;
                posicaoEmprestimoBolsa.ValorMercado = valorMercado;
            }
            #endregion

            #region BTC (Tomador)
            vcfposiBtcCollection = new VcfposiBtcCollection();
            vcfposiBtcCollection.es.Connection.Name = "Sinacor";
            vcfposiBtcCollection.es.Connection.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

            vcfposiBtcCollection.Query.Where(
                                (vcfposiBtcCollection.Query.CodCli == codigoCliente | vcfposiBtcCollection.Query.CodCli == codigoCliente2));
            vcfposiBtcCollection.Query.Where(vcfposiBtcCollection.Query.TipoCotr == "D");
            vcfposiBtcCollection.Query.GroupBy(vcfposiBtcCollection.Query.CodNeg, vcfposiBtcCollection.Query.DataVenc);
            vcfposiBtcCollection.Query.Load();

            foreach (VcfposiBtc vcfposiBtc in vcfposiBtcCollection)
            {
                string cdAtivoBolsa = vcfposiBtc.CodNeg;
                decimal quantidade = Math.Abs(vcfposiBtc.QtdeAcoe.Value);
                DateTime dataVencimento = vcfposiBtc.DataVenc.Value;
                DateTime dataRegistro = vcfposiBtc.DataAber.Value;
                decimal taxa = vcfposiBtc.TaxaRemu.Value;
                int numeroContrato = (int)vcfposiBtc.NumCotr.Value;
                decimal puBase = vcfposiBtc.PrecMed.Value;

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                decimal valorBase = Utilitario.Truncate(quantidade * puBase / fatorCotacaoBolsa.Fator.Value, 2);

                decimal cotacaoFechamento = 0;
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                cotacaoBolsa.Query.Select(cotacaoBolsa.Query.PUFechamento);
                cotacaoBolsa.Query.Where(cotacaoBolsa.Query.Data.Equal(data),
                                         cotacaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                if (cotacaoBolsa.Query.Load())
                {
                    cotacaoFechamento = cotacaoBolsa.PUFechamento.Value;
                }

                decimal valorMercado = Utilitario.Truncate(quantidade * cotacaoFechamento / fatorCotacaoBolsa.Fator.Value, 2);

                PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = posicaoEmprestimoBolsaCollection.AddNew();
                posicaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                posicaoEmprestimoBolsa.DataRegistro = dataRegistro;
                posicaoEmprestimoBolsa.DataVencimento = dataVencimento;
                posicaoEmprestimoBolsa.IdAgente = idAgenteMercado;
                posicaoEmprestimoBolsa.IdCliente = idCliente;
                posicaoEmprestimoBolsa.NumeroContrato = numeroContrato;
                posicaoEmprestimoBolsa.PontaEmprestimo = (byte)PontaEmprestimoBolsa.Doador;
                posicaoEmprestimoBolsa.PULiquidoOriginal = 0;
                posicaoEmprestimoBolsa.PUMercado = cotacaoFechamento;
                posicaoEmprestimoBolsa.Quantidade = quantidade;
                posicaoEmprestimoBolsa.TaxaComissao = 0;
                posicaoEmprestimoBolsa.TaxaOperacao = taxa;
                posicaoEmprestimoBolsa.TipoEmprestimo = (byte)TipoEmprestimoBolsa.Voluntario;
                posicaoEmprestimoBolsa.ValorBase = valorBase;
                posicaoEmprestimoBolsa.ValorCorrigidoCBLC = valorBase;
                posicaoEmprestimoBolsa.ValorCorrigidoComissao = valorBase;
                posicaoEmprestimoBolsa.ValorCorrigidoJuros = valorBase;
                posicaoEmprestimoBolsa.ValorDiarioCBLC = 0;
                posicaoEmprestimoBolsa.ValorDiarioComissao = 0;
                posicaoEmprestimoBolsa.ValorDiarioJuros = 0;
                posicaoEmprestimoBolsa.ValorMercado = valorMercado;
            }
            #endregion
            posicaoEmprestimoBolsaCollection.Save();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoAberturaIndexada"></param>
        public void AberturaIndexada(Cliente cliente, DateTime data)
        {
            PosicaoEmprestimoBolsaCollection posicaoColl = new PosicaoEmprestimoBolsaCollection();
            PosicaoEmprestimoBolsaQuery posicaoQuery = new PosicaoEmprestimoBolsaQuery("Posicao");

            TipoAberturaIndexada tipoAberturaIndexada = (TipoAberturaIndexada)cliente.AberturaIndexada;
            short idIndexador = cliente.IdIndiceAbertura.Value;
            int idCliente = cliente.IdCliente.Value;
            int idLocalFeriado = cliente.IdLocal.Value;
            decimal cotacaoIndexador = 0;

            #region Carrega Posição

            if (tipoAberturaIndexada != TipoAberturaIndexada.Mandatorio)
                return;

            posicaoQuery.Select(posicaoQuery);
            posicaoQuery.Where(posicaoQuery.IdCliente.Equal(idCliente));

            if (!posicaoColl.Load(posicaoQuery))
                return;
            #endregion

            #region Busca Cotacao
            DateTime dataAnterior;
            if (idLocalFeriado != LocalFeriadoFixo.Brasil)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocalFeriado, TipoFeriado.Outros);
            }
            else
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocalFeriado, TipoFeriado.Brasil);
            }
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            cotacaoIndice.Query.Where(cotacaoIndice.Query.IdIndice.Equal(idIndexador)
                                      & cotacaoIndice.Query.Data.Equal(dataAnterior));

            if (cotacaoIndice.Query.Load())
            {
                cotacaoIndexador = Convert.ToDecimal(Math.Pow((double)(cotacaoIndice.Valor.Value / 100 + 1), (double)1 / 252));
            }
            else
            {
                Indice indice = new Indice();
                indice.LoadByPrimaryKey(idIndexador);
                throw new Exception("Cotação não cadastrada para o Indexador - " + indice.Descricao + " na data - " + dataAnterior);
            }
            #endregion

            foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoColl)
            {
                posicaoEmprestimoBolsa.PUMercado = posicaoEmprestimoBolsa.PUMercado.Value * cotacaoIndexador;
                posicaoEmprestimoBolsa.ValorMercado = posicaoEmprestimoBolsa.Quantidade.Value * posicaoEmprestimoBolsa.PUMercado.Value;                
            }

            posicaoColl.Save();
        }
    }
}
