﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Interfaces.Import.Bolsa;
using System.IO;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Common;
using Financial.Investidor;
using Financial.Util;
using Financial.Bolsa.Exceptions;
using Financial.Util.Enums;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa
{
	public partial class TabelaCONR : esTabelaCONR
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conrCollection"></param>
        /// <param name="data"></param>
        /// throws AtivoNaoCadastradoException se codigo do Ativo não estiver cadastrado na Base
        public void CarregaTabelaConr(ConrCollection conrCollection, DateTime data) {
            #region GetIdAgente do Arquivo Conr
            int codigoBovespaAgente = conrCollection.CollectionConr[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            Conr conr = new Conr();
            OrdemTermoBolsaCollection ordemTermoBolsaCollection = new OrdemTermoBolsaCollection();
            //

            TabelaCONRCollection tabelaCONRCollectionDeletar = new TabelaCONRCollection();
            tabelaCONRCollectionDeletar.Query.Where(tabelaCONRCollectionDeletar.Query.Data.Equal(data));
            tabelaCONRCollectionDeletar.Query.Load();
            tabelaCONRCollectionDeletar.MarkAllAsDeleted();
            tabelaCONRCollectionDeletar.Save();

            TabelaCONRCollection tabelaCONRCollection = new TabelaCONRCollection();

            // Pega as Ordens do arquivo de Conr 
            for (int i = 0; i < conrCollection.CollectionConr.Count; i++) {
                conr = conrCollection.CollectionConr[i];

                #region Copia informações para TabelaCONRCollection
                if (conr.IsTipoRegistroIdentificacaoContratoRegistrado()) {

                    int codigoClienteBovespa = conr.CodigoCliente.Value;
                    CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                    bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);
                    if (idClienteExiste && conr.DataRegistro.Value.CompareTo(data) == 0) //Só pega para registros de termo na própria data!!!
                    {
                        int idCliente = codigoClienteAgente.IdCliente.Value;

                        Cliente cliente = new Cliente();
                        cliente.LoadByPrimaryKey(idCliente);

                        if (cliente.IsAtivo) {
                            string cdAtivoBolsa = conr.CdAtivoBolsa.Substring(0, conr.CdAtivoBolsa.Length - 1);
                            DateTime dataVencimento = conr.DataVencimento.Value;

                            string cdAtivoBolsaObjeto = cdAtivoBolsa;

                            //Transforma o cdAtivoBolsa, incluindo a data do vencimento ao final                    
                            cdAtivoBolsa = cdAtivoBolsa + "T";
                            cdAtivoBolsa = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(cdAtivoBolsa, dataVencimento);

                            #region Verifica se cdAtivoBolsa existe, e cadastra se nao existir
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa)) {
                                AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
                                if (!ativoBolsaObjeto.LoadByPrimaryKey(cdAtivoBolsaObjeto)) {
                                    throw new AtivoNaoCadastradoException("Ativo " + cdAtivoBolsaObjeto + " não cadastrado.");
                                }

                                AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                                ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                                ativoBolsaNovo.CdAtivoBolsaObjeto = cdAtivoBolsaObjeto;
                                ativoBolsaNovo.CodigoIsin = ativoBolsaObjeto.CodigoIsin;
                                ativoBolsaNovo.DataVencimento = dataVencimento;
                                ativoBolsaNovo.Descricao = ativoBolsaObjeto.Descricao;
                                ativoBolsaNovo.Especificacao = "*";
                                ativoBolsaNovo.IdEmissor = 1;
                                ativoBolsaNovo.IdMoeda = 1;
                                ativoBolsaNovo.PUExercicio = 0;
                                ativoBolsaNovo.TipoMercado = TipoMercadoBolsa.Termo;
                                ativoBolsaNovo.NumeroSerie = 0;
                                ativoBolsaNovo.Save();

                                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                                if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsaObjeto, data)) {
                                    FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                                    fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                                    fatorCotacaoBolsaNovo.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                                    fatorCotacaoBolsaNovo.Fator = fatorCotacaoBolsa.Fator.Value;
                                    fatorCotacaoBolsaNovo.Save();
                                }
                            }
                            #endregion

                            TabelaCONR tabelaCONR = tabelaCONRCollection.AddNew();
                            tabelaCONR.CdAtivoBolsa = cdAtivoBolsa;
                            tabelaCONR.Data = data;
                            tabelaCONR.IdCliente = idCliente;
                            tabelaCONR.NumeroContrato = conr.NumeroContrato.Value;
                            tabelaCONR.NumeroNegocio = conr.NumeroNegocio.Value;
                            tabelaCONR.Preco = conr.Pu.Value;
                            tabelaCONR.Quantidade = Convert.ToDecimal(conr.Quantidade.Value);
                            tabelaCONR.TipoOperacao = Convert.ToString(conr.Tipo);

                            //Atualiza o contrato em OrdemBolsa
                            OrdemBolsa ordemBolsa = new OrdemBolsa();
                            ordemBolsa.Query.Select(ordemBolsa.Query.IdOrdem);
                            ordemBolsa.Query.Where(ordemBolsa.Query.IdCliente.Equal(idCliente),
                                                   ordemBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                   ordemBolsa.Query.Data.Equal(data),
                                                   ordemBolsa.Query.TipoOrdem.Equal(tabelaCONR.TipoOperacao),
                                                   ordemBolsa.Query.NumeroNegocio.Equal(tabelaCONR.NumeroNegocio.Value),
                                                   ordemBolsa.Query.Quantidade.Equal(tabelaCONR.Quantidade.Value),
                                                   ordemBolsa.Query.Pu.Equal(tabelaCONR.Preco.Value));
                            if (ordemBolsa.Query.Load()) {
                                int idOrdem = ordemBolsa.IdOrdem.Value;

                                OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();
                                if (ordemTermoBolsa.LoadByPrimaryKey(idOrdem)) {
                                    ordemTermoBolsa.NumeroContrato = Convert.ToString(tabelaCONR.NumeroContrato);
                                    ordemTermoBolsa.Save();
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            tabelaCONRCollection.Save();
        }
	}
}
