﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           26/12/2006 17:36:20
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa {
    public partial class TemplateCorretagemBolsa : esTemplateCorretagemBolsa {

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoCorretagem é Fixo
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCorretagemValorFixo() {
            return (this.TipoCorretagem == (byte) TipoCalculoCorretagem.ValorFixo);                                   
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoCorretagem é por Volume
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCorretagemPorVolume() {
            return (this.TipoCorretagem == (byte)TipoCalculoCorretagem.Tabela);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True se TipoCorretagem é por QuantidadeAcoes
        ///          False caso Contrario
        /// </returns>
        public bool IsTipoCorretagemQuantidadeAcoes()
        {
            return (this.TipoCorretagem == (byte)TipoCalculoCorretagem.QuantidadeAcoes);
        }

    }
}
