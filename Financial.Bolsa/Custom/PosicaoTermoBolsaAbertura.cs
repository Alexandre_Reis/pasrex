using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa
{
	public partial class PosicaoTermoBolsaAbertura : esPosicaoTermoBolsaAbertura
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(PosicaoTermoBolsaAbertura));

        /// <summary>
        /// Retorna a quantidade da posicao (IdPosicao) na data passada.
        /// </summary>
        /// <param name="IdPosicao"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadePosicao(int IdPosicao, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade)
                 .Where(this.Query.IdPosicao.Equal(IdPosicao),
                        this.Query.DataHistorico.Equal(data));

            this.Query.Load();

            decimal quantidade = 0;
            if (this.es.HasData && this.Quantidade.HasValue)
            {
                quantidade = this.Quantidade.Value;
            }

            return quantidade;
        }

        /// <summary>
        /// Carrega o objeto PosicaoTermoBolsaAbertura com os campos IdPosicao, CdAtivoBolsa, Quantidade.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="numeroContrato"></param>
        /// <param name="dataHistorico"></param>
        public bool BuscaPosicaoTermoBolsaAbertura(int idCliente, string numeroContrato, DateTime dataHistorico)
        {
            PosicaoTermoBolsaAberturaCollection posicaoTermoBolsaAberturaCollection = new PosicaoTermoBolsaAberturaCollection();
            posicaoTermoBolsaAberturaCollection.Query.Select(posicaoTermoBolsaAberturaCollection.Query.IdPosicao,
                                                             posicaoTermoBolsaAberturaCollection.Query.CdAtivoBolsa,
                                                             posicaoTermoBolsaAberturaCollection.Query.Quantidade);
            posicaoTermoBolsaAberturaCollection.Query.Where(posicaoTermoBolsaAberturaCollection.Query.IdCliente == idCliente,
                                                            posicaoTermoBolsaAberturaCollection.Query.NumeroContrato == numeroContrato,
                                                            posicaoTermoBolsaAberturaCollection.Query.DataHistorico == dataHistorico);
            posicaoTermoBolsaAberturaCollection.Query.Load();

            bool retorno = false;
            if (posicaoTermoBolsaAberturaCollection.Count > 0)
            {
                this.IdPosicao = posicaoTermoBolsaAberturaCollection[0].IdPosicao.Value;
                this.CdAtivoBolsa = posicaoTermoBolsaAberturaCollection[0].CdAtivoBolsa;
                this.Quantidade = posicaoTermoBolsaAberturaCollection[0].Quantidade.Value;
                retorno = true;
            }
            
            return retorno;
        }

        /// <summary>
        /// Retorna o valor de mercado para termos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico == dataHistorico,
                        this.Query.Quantidade != 0);

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna o rendimento a apropriar para termos. Somente para qtde menor que zero.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaRendimentoApropriar(int idCliente, DateTime dataHistorico) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.RendimentoApropriar.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                         this.Query.DataHistorico == dataHistorico,
                         this.Query.Quantidade < 0);

            this.Query.Load();

            return this.RendimentoApropriar.HasValue ? this.RendimentoApropriar.Value : 0;
        }
	}
}
