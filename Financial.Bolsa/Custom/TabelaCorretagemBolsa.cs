﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           26/12/2006 17:36:20
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa {
    public partial class TabelaCorretagemBolsa : esTabelaCorretagemBolsa {
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaCorretagemBolsa));

        /// <summary>
        /// Retorna um percentual de desconto de acordo com o perfil de Corretagem
        /// </summary>
        /// <param name="idTemplate"></param>
        /// <param name="totalValorOperado">total valor operado pelo cliente numa data, corretora e mercado</param>
        /// <returns>Se não foi encontrado faixa percentual = 0</returns>
        public void BuscaTabelaCorretagemBolsa(int idTemplate, decimal totalValorOperado) {

            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nome Metodo: ");
            }
            #endregion

            TabelaCorretagemBolsaCollection tabelaCorretagemBolsaCollection = new TabelaCorretagemBolsaCollection();

            // Busca todos percentuais menores que o total de Corretagem ordenado decrescente pela faixa
            tabelaCorretagemBolsaCollection.Query
                 .Select(tabelaCorretagemBolsaCollection.Query.Percentual, tabelaCorretagemBolsaCollection.Query.ValorAdicional)
                 .Where(tabelaCorretagemBolsaCollection.Query.Faixa.LessThanOrEqual(totalValorOperado),
                        tabelaCorretagemBolsaCollection.Query.IdTemplate == idTemplate)
                 .OrderBy(tabelaCorretagemBolsaCollection.Query.Faixa.Descending);

            tabelaCorretagemBolsaCollection.Query.Load();
            
            #region Log Sql
            if (log.IsInfoEnabled) {
                string sql = tabelaCorretagemBolsaCollection.Query.es.LastQuery;
                sql = sql.Replace("@Faixa1", "'" + totalValorOperado + "'");
                sql = sql.Replace("@IdTemplate2", "'" + idTemplate + "'");
                log.Info(sql);
            }
            #endregion

            if (tabelaCorretagemBolsaCollection.HasData) {
                // Copia informações de tabelaCorretagemBolsaCollection para TabelaCorretagemBolsa  
                this.AddNew();
                this.Percentual = ((TabelaCorretagemBolsa)tabelaCorretagemBolsaCollection[0]).Percentual;
                this.ValorAdicional = ((TabelaCorretagemBolsa)tabelaCorretagemBolsaCollection[0]).ValorAdicional;
            }
            else {
                this.AddNew();
                this.Percentual = 0;
                this.ValorAdicional = 0;
            }
        }
    }
}
