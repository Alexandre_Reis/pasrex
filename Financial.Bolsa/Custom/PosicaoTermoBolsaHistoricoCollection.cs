﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa {
    public partial class PosicaoTermoBolsaHistoricoCollection : esPosicaoTermoBolsaHistoricoCollection 
    {
        // Construtor
        // Cria uma nova PosicaoTermoBolsaHistoricoCollection com os dados de PosicaoTermoBolsaCollection
        // Todos os dados de PosicaoTermoBolsaCollection são copiados, adicionando a dataHistorico
        public PosicaoTermoBolsaHistoricoCollection(PosicaoTermoBolsaCollection posicaoTermoBolsaCollection, DateTime dataHistorico)
        {
            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++)
            {
                PosicaoTermoBolsaHistorico p = new PosicaoTermoBolsaHistorico();

                // Para cada Coluna de PosicaoTermoBolsa copia para PosicaoTermoBolsaHistorico
                foreach (esColumnMetadata colPosicao in posicaoTermoBolsaCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas 
                    esColumnMetadata colPosicaoTermoBolsaHistorico = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                    if (posicaoTermoBolsaCollection[i].GetColumn(colPosicao.Name) != null) 
                    {
                        p.SetColumn(colPosicaoTermoBolsaHistorico.Name, posicaoTermoBolsaCollection[i].GetColumn(colPosicao.Name));                        
                    }                    
                }

                p.DataHistorico = dataHistorico;

                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoTermoBolsaHistoricoCompleta(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();         
        }

        /// <summary>
        /// Deleta todas as posições de termo de bolsa.
        /// Filtra por DataHistorico >= dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoTermoBolsaHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.Query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas as posições de termo de bolsa.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoTermoBolsaHistoricoDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.Query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoTermoBolsaHistoricoCompletaDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();         
        }

        /// <summary>
        /// Carrega o objeto PosicaoTermoBolsaCollection com os campos CdAtivoBolsa, Quantidade.Sum, PUMercado.Avg, PUTermoLiquido.Avg, 
        /// ValorMercado.Sum.
        /// Group By CdAtivoBolsa
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoTermoBolsaAgrupado(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa,
                         this.Query.Quantidade.Sum(),
                         this.Query.PUMercado.Avg(),
                         this.Query.PUTermoLiquido.Avg(),
                         this.Query.ValorMercado.Sum(),
                         this.Query.ValorTermoLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico))
                 .GroupBy(this.Query.CdAtivoBolsa);

            bool retorno = this.Query.Load();

            return retorno;
        }
    }
}
