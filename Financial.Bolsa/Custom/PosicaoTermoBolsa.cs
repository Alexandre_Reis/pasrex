﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Exceptions;
using Financial.Bolsa.Enums;
using Financial.Investidor.Enums;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Util.Enums;
using Financial.Bolsa.Properties;
using Financial.ContaCorrente;
using Financial.Common;
using Financial.Interfaces.Sinacor;

namespace Financial.Bolsa
{
    public partial class PosicaoTermoBolsa : esPosicaoTermoBolsa
    {
        /// <summary>
        /// retorna o cdAtivoBolsaTermo convertido apra cdAtivoBolsa
        /// se cdAtivoBolsaTermo.lenght < 14 retorna o proprio cdAtivoBolsa
        /// </summary>
        [CalculatedColumn(true)]
        public string CdAtivoBolsaAcao {
            get {                
                if(this.CdAtivoBolsa.Length < 14) {
                    return this.str.CdAtivoBolsa;
                }
                else {
                    // Remove a data e o T
                    int posicao = this.CdAtivoBolsa.Length - 9;
                    string cdAtivoBolsaAcao = this.CdAtivoBolsa.Remove(posicao);
                    return cdAtivoBolsaAcao;
                }
            }      
        }

        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorMercadoEnquadra(int idCliente, string posicao)
        {
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();

            posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.CdAtivoBolsa, posicaoTermoBolsaCollection.Query.ValorMercado);
            posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente == idCliente,
                 	                                posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0));

            if (posicao == "C")
            {
                posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.Quantidade.GreaterThan(0));
            }
            else if (posicao == "V")
            {
                posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.Quantidade.LessThan(0));
            }

            posicaoTermoBolsaCollection.Query.Load();

            decimal totalValorMercado = 0;

            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++)
            {
                PosicaoTermoBolsa posicaoTermoBolsa = posicaoTermoBolsaCollection[i];
                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                decimal valorMercado = posicaoTermoBolsa.ValorMercado.Value;

                totalValorMercado += valorMercado;
            }

            return totalValorMercado;
        }

        /// <summary>
        /// Retorna o valor de mercado para termos.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorMercado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.NotEqual(0));

            this.Query.Load();

            decimal valorMercado = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;

            return valorMercado;
        }

        /// <summary>
        /// Retorna o rendimento a apropriar para termos. Somente para qtde menor que zero.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaRendimentoApropriar(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.RendimentoApropriar.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.LessThan(0));

            this.Query.Load();

            decimal rendimentoApropriar = this.RendimentoApropriar.HasValue ? this.RendimentoApropriar.Value : 0;

            return rendimentoApropriar;
        }

        /// <summary>
        /// Retorna o valor de mercado para termos com posição comprada.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorTermoComprado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.GreaterThan(0));

            this.Query.Load();

            decimal valorMercado = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;

            return valorMercado;
        }

        /// <summary>
        /// Retorna o valor de mercado para termos com posição vendida.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorTermoVendido(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.LessThan(0));

            this.Query.Load();

            decimal valorMercado = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;

            return valorMercado;
        }

        /// <summary>
        /// Gera backup na PosicaoTermoBolsaHistorico a partir das posições em PosicaoTermoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            try
            {
                PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaDeletarHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
                posicaoTermoBolsaDeletarHistoricoCollection.DeletaPosicaoTermoBolsaHistoricoDataHistoricoMaiorIgual(idCliente, data);
            }
            catch { }

            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsaCompleta(idCliente);
            //
            PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            //
            #region Copia de posicaoTermoBolsa para posicaoTermoBolsaHistorico
            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++)
            {
                PosicaoTermoBolsa posicaoTermoBolsa = posicaoTermoBolsaCollection[i];

                #region PosicaoTermoBolsaHistorico
                // Copia de posicaoTermoBolsa para posicaoTermoBolsaHistorico
                PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = posicaoTermoBolsaHistoricoCollection.AddNew();
                //
                posicaoTermoBolsaHistorico.IdPosicao = posicaoTermoBolsa.IdPosicao;
                posicaoTermoBolsaHistorico.DataHistorico = data;

                if (posicaoTermoBolsa.IdOperacao.HasValue)
                    posicaoTermoBolsaHistorico.IdOperacao = posicaoTermoBolsa.IdOperacao.Value;

                posicaoTermoBolsaHistorico.IdCliente = posicaoTermoBolsa.IdCliente;
                posicaoTermoBolsaHistorico.IdAgente = posicaoTermoBolsa.IdAgente;
                posicaoTermoBolsaHistorico.CdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                posicaoTermoBolsaHistorico.DataOperacao = posicaoTermoBolsa.DataOperacao;
                posicaoTermoBolsaHistorico.DataVencimento = posicaoTermoBolsa.DataVencimento;
                posicaoTermoBolsaHistorico.Quantidade = posicaoTermoBolsa.Quantidade;
                posicaoTermoBolsaHistorico.QuantidadeInicial = posicaoTermoBolsa.QuantidadeInicial;
                posicaoTermoBolsaHistorico.PrazoTotal = posicaoTermoBolsa.PrazoTotal;
                posicaoTermoBolsaHistorico.PrazoDecorrer = posicaoTermoBolsa.PrazoDecorrer;
                posicaoTermoBolsaHistorico.TaxaAno = posicaoTermoBolsa.TaxaAno;
                posicaoTermoBolsaHistorico.TaxaMTM = posicaoTermoBolsa.TaxaMTM;
                posicaoTermoBolsaHistorico.ValorTermo = posicaoTermoBolsa.ValorTermo;
                posicaoTermoBolsaHistorico.ValorTermoLiquido = posicaoTermoBolsa.ValorTermoLiquido;
                posicaoTermoBolsaHistorico.ValorMercado = posicaoTermoBolsa.ValorMercado;
                posicaoTermoBolsaHistorico.ValorCurva = posicaoTermoBolsa.ValorCurva;
                posicaoTermoBolsaHistorico.ValorCorrigido = posicaoTermoBolsa.ValorCorrigido;
                posicaoTermoBolsaHistorico.PUCustoLiquidoAcao = posicaoTermoBolsa.PUCustoLiquidoAcao;
                posicaoTermoBolsaHistorico.PUTermo = posicaoTermoBolsa.PUTermo;
                posicaoTermoBolsaHistorico.PUTermoLiquido = posicaoTermoBolsa.PUTermoLiquido;
                posicaoTermoBolsaHistorico.PUMercado = posicaoTermoBolsa.PUMercado;
                posicaoTermoBolsaHistorico.NumeroContrato = posicaoTermoBolsa.NumeroContrato;
                posicaoTermoBolsaHistorico.IdTrader = posicaoTermoBolsa.IdTrader;

                if (posicaoTermoBolsa.IdIndice.HasValue)
                    posicaoTermoBolsaHistorico.IdIndice = posicaoTermoBolsa.IdIndice.Value;

                posicaoTermoBolsaHistorico.RendimentoApropriado = posicaoTermoBolsa.RendimentoApropriado;
                posicaoTermoBolsaHistorico.RendimentoApropriar = posicaoTermoBolsa.RendimentoApropriar;
                posicaoTermoBolsaHistorico.RendimentoDia = posicaoTermoBolsa.RendimentoDia;
                posicaoTermoBolsaHistorico.RendimentoTotal = posicaoTermoBolsa.RendimentoTotal;
                posicaoTermoBolsaHistorico.TipoTermo = posicaoTermoBolsa.TipoTermo;
                #endregion
            }
            #endregion

            posicaoTermoBolsaHistoricoCollection.Save();
        }

        /// <summary>
        /// Gera backup na PosicaoTermoBolsaAbertura a partir das posições em PosicaoTermoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCliente, DateTime data)
        {
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsaCompleta(idCliente);
            //
            PosicaoTermoBolsaAberturaCollection posicaoTermoBolsaAberturaCollection = new PosicaoTermoBolsaAberturaCollection();
            //
            #region Copia de posicaoTermoBolsa para posicaoTermoBolsaAbertura
            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++)
            {
                PosicaoTermoBolsa posicaoTermoBolsa = (PosicaoTermoBolsa)posicaoTermoBolsaCollection[i];

                // Copia de posicaoTermoBolsa para posicaoTermoBolsaAbertura
                PosicaoTermoBolsaAbertura posicaoTermoBolsaAbertura = posicaoTermoBolsaAberturaCollection.AddNew();
                //
                posicaoTermoBolsaAbertura.IdPosicao = posicaoTermoBolsa.IdPosicao;
                posicaoTermoBolsaAbertura.DataHistorico = data;

                if (posicaoTermoBolsa.IdOperacao.HasValue)
                    posicaoTermoBolsaAbertura.IdOperacao = posicaoTermoBolsa.IdOperacao.Value;

                posicaoTermoBolsaAbertura.IdCliente = posicaoTermoBolsa.IdCliente;
                posicaoTermoBolsaAbertura.IdAgente = posicaoTermoBolsa.IdAgente;
                posicaoTermoBolsaAbertura.CdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                posicaoTermoBolsaAbertura.DataOperacao = posicaoTermoBolsa.DataOperacao;
                posicaoTermoBolsaAbertura.DataVencimento = posicaoTermoBolsa.DataVencimento;
                posicaoTermoBolsaAbertura.Quantidade = posicaoTermoBolsa.Quantidade;
                posicaoTermoBolsaAbertura.QuantidadeInicial = posicaoTermoBolsa.QuantidadeInicial;
                posicaoTermoBolsaAbertura.PrazoTotal = posicaoTermoBolsa.PrazoTotal;
                posicaoTermoBolsaAbertura.PrazoDecorrer = posicaoTermoBolsa.PrazoDecorrer;
                posicaoTermoBolsaAbertura.TaxaAno = posicaoTermoBolsa.TaxaAno;
                posicaoTermoBolsaAbertura.TaxaMTM = posicaoTermoBolsa.TaxaMTM;
                posicaoTermoBolsaAbertura.ValorTermo = posicaoTermoBolsa.ValorTermo;
                posicaoTermoBolsaAbertura.ValorTermoLiquido = posicaoTermoBolsa.ValorTermoLiquido;
                posicaoTermoBolsaAbertura.ValorMercado = posicaoTermoBolsa.ValorMercado;
                posicaoTermoBolsaAbertura.ValorCurva = posicaoTermoBolsa.ValorCurva;
                posicaoTermoBolsaAbertura.ValorCorrigido = posicaoTermoBolsa.ValorCorrigido;
                posicaoTermoBolsaAbertura.PUCustoLiquidoAcao = posicaoTermoBolsa.PUCustoLiquidoAcao;
                posicaoTermoBolsaAbertura.PUTermo = posicaoTermoBolsa.PUTermo;
                posicaoTermoBolsaAbertura.PUTermoLiquido = posicaoTermoBolsa.PUTermoLiquido;
                posicaoTermoBolsaAbertura.PUMercado = posicaoTermoBolsa.PUMercado;
                posicaoTermoBolsaAbertura.NumeroContrato = posicaoTermoBolsa.NumeroContrato;
                posicaoTermoBolsaAbertura.IdTrader = posicaoTermoBolsa.IdTrader;

                if (posicaoTermoBolsa.IdIndice.HasValue)
                    posicaoTermoBolsaAbertura.IdIndice = posicaoTermoBolsa.IdIndice.Value;

                posicaoTermoBolsaAbertura.RendimentoApropriado = posicaoTermoBolsa.RendimentoApropriado;
                posicaoTermoBolsaAbertura.RendimentoApropriar = posicaoTermoBolsa.RendimentoApropriar;
                posicaoTermoBolsaAbertura.RendimentoDia = posicaoTermoBolsa.RendimentoDia;
                posicaoTermoBolsaAbertura.RendimentoTotal = posicaoTermoBolsa.RendimentoTotal;
                posicaoTermoBolsaAbertura.TipoTermo = posicaoTermoBolsa.TipoTermo;
            }
            #endregion

            posicaoTermoBolsaAberturaCollection.Save();
        }

        /// <summary>
        /// Atualiza PuMercado, ValorMercado dado um cliente e uma data de cotação.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void AtualizaValores(int idCliente, DateTime data)
        {
            FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
            CotacaoBolsa cotacao = new CotacaoBolsa();
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();

            // Arraylist com os Ids das posicões zeradas
            List<int> listaPosicaoZerada = new List<int>();
            posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsa(idCliente);

            ClienteBolsa clienteBolsa = new ClienteBolsa();
            int tipoCotacao = 0;
            if (posicaoTermoBolsaCollection.Count > 0)
            {
                //Verifico se o cliente usa cotação média ou de fechamento                
                clienteBolsa.LoadByPrimaryKey(idCliente);

                if (!clienteBolsa.es.HasData)
                {
                    throw new ClienteBolsaNaoCadastradoException("Cliente " + idCliente + " sem cadastro para Bolsa.");
                }

                tipoCotacao = clienteBolsa.TipoCotacao.Value;
            }

            bool mtmTermoVendido = ParametrosConfiguracaoSistema.Bolsa.MTMTermoVendido;

            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++)
            {
                posicaoTermoBolsa = posicaoTermoBolsaCollection[i];

                //AtivoBolsa ativoBolsa = new AtivoBolsa();
                string cdAtivoBolsaTermo = posicaoTermoBolsa.CdAtivoBolsa;
                // Converte de Termo para Ação
                string cdAtivoBolsaAcao = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsaTermo);

                // Se posicaoTermoBolsa.Quantidade != 0 - update das posições
                if (posicaoTermoBolsa.Quantidade != 0)
                {
                    #region Busca cotação média ou fechamento, de acordo com o perfil do cliente Bolsa
                    cotacao.BuscaCotacaoBolsa(cdAtivoBolsaAcao, data);

                    decimal cotacaoDia;
                    if (clienteBolsa.TipoCotacao == (int)TipoCotacaoBolsa.Medio)
                    {
                        if (!cotacao.es.HasData || !cotacao.PUMedio.HasValue)
                        {
                            cotacao = new CotacaoBolsa();
                            cotacao.AddNew();
                            cotacao.PUMedio = 0;
                        }
                        cotacaoDia = cotacao.PUMedio.Value;
                    }
                    else
                    {
                        if (!cotacao.es.HasData || !cotacao.PUFechamento.HasValue)
                        {
                            cotacao = new CotacaoBolsa();
                            cotacao.AddNew();
                            cotacao.PUFechamento = 0;
                        }
                        cotacaoDia = cotacao.PUFechamento.Value;
                    }
                    //Se a cotação vier zerada, busca a última disponível (para a situação de feriado na Bovespa)
                    if (cotacaoDia == 0)
                    {
                        CotacaoBolsa cotacaoAnterior = new CotacaoBolsa();
                        if (cotacaoAnterior.BuscaUltimaCotacaoBolsa(cdAtivoBolsaAcao, data))
                        {
                            cotacaoDia = tipoCotacao == (int)TipoCotacaoBolsa.Medio
                                        ? cotacaoAnterior.PUMedio.Value
                                        : cotacaoAnterior.PUFechamento.Value;
                        }
                    }
                    #endregion
                    
                    if(!fator.BuscaFatorCotacaoBolsa(cdAtivoBolsaAcao, data)) 
                    {
                        if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                        {
                            AtivoBolsa ativoBolsa = new AtivoBolsa();
                            if (!ativoBolsa.IntegraFatorCotacaoSinacor(cdAtivoBolsaAcao))
                            {
                                throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + cdAtivoBolsaAcao + " " + data.ToString("d"));
                            }
                        }
                    }
                    //
                    decimal valorMercado = 0;
                    decimal puMercado = 0;
                    if (mtmTermoVendido || posicaoTermoBolsa.Quantidade.Value > 0)
                    {
                        valorMercado = (posicaoTermoBolsa.Quantidade.Value * cotacaoDia) / (decimal)fator.Fator;
                        puMercado = cotacaoDia;
                    }
                    else
                    {
                        valorMercado = posicaoTermoBolsa.ValorCurva.Value;
                        puMercado = valorMercado / posicaoTermoBolsa.Quantidade.Value * fator.Fator.Value;                     
                    }

                    posicaoTermoBolsa.PUMercado = puMercado;
                    posicaoTermoBolsa.ValorMercado = valorMercado;
                }
                // Se posicaoBolsa.Quantidade = 0 - delete das posições
                else
                {
                    listaPosicaoZerada.Add(posicaoTermoBolsa.IdPosicao.Value);
                }
            }

            // Update das posicões
            posicaoTermoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega o objeto PosicaoTermoBolsa com os campos IdPosicao, CdAtivoBolsa, Quantidade.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="numeroContrato"></param>
        public bool BuscaPosicaoTermoBolsa(int idCliente, string numeroContrato)
        {
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.IdPosicao,
                                                     posicaoTermoBolsaCollection.Query.CdAtivoBolsa,
                                                     posicaoTermoBolsaCollection.Query.Quantidade);
            posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente == idCliente,
                                                    posicaoTermoBolsaCollection.Query.NumeroContrato == numeroContrato);
            posicaoTermoBolsaCollection.Query.Load();

            bool retorno = false;
            if (posicaoTermoBolsaCollection.Count > 0)
            {
                this.IdPosicao = posicaoTermoBolsaCollection[0].IdPosicao.Value;
                this.CdAtivoBolsa = posicaoTermoBolsaCollection[0].CdAtivoBolsa;
                this.Quantidade = posicaoTermoBolsaCollection[0].Quantidade.Value;
                retorno = true;
            }

            return retorno;
        }

        /// <summary>
        /// Atualiza em PosicaoBolsa os seguintes:
        /// 
        /// [QuantidadeAbertura - Quantidade de D-1]
        /// [PrazoDecorrer - Diminui 1 Dia]
        /// [TaxaMTM - Pela curva interpolada da BMF]
        /// [ValorMercado - Comprado = Valor da ação / Vendido = MTM do ganho pré]
        /// [ValorCurva - Rendimento pré corrigido]
        /// [ValorCorrigido - Correção monetária pelo índice >> somente para Termo indexado]
        /// [PUMercado - PU da cotação da ação ou PU do MTM do ganho pré]
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void AtualizaPosicaoTermoAbertura(int idCliente, DateTime data)
        {
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsaCompleta(idCliente);

            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++)
            {
                PosicaoTermoBolsa posicaoTermoBolsa = posicaoTermoBolsaCollection[i];
                
                #region Valores de PosicaoTermoBolsa
                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                DateTime dataOperacao = posicaoTermoBolsa.DataOperacao.Value;
                DateTime dataVencimento = posicaoTermoBolsa.DataVencimento.Value;
                decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                int prazoTotal = posicaoTermoBolsa.PrazoTotal.Value;
                int prazoDecorrer = posicaoTermoBolsa.PrazoDecorrer.Value;
                decimal taxaTermo = posicaoTermoBolsa.TaxaAno.Value;
                decimal puCustoLiquidoAcao = posicaoTermoBolsa.PUCustoLiquidoAcao.Value;

                FatorCotacaoBolsa fator = new FatorCotacaoBolsa();
                if (!fator.BuscaFatorCotacaoBolsa(AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa), data))
                {
                    if (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor)
                    {
                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                        if (!ativoBolsa.IntegraFatorCotacaoSinacor(AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa)))
                        {
                            throw new FatorCotacaoNaoCadastradoException("FatorCotacao não cadastrado para a data  " + AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa) + " " + data.ToString("d"));
                        }
                    }
                }

                decimal puTermo = posicaoTermoBolsa.PUTermo.Value;
                decimal valorTermo = posicaoTermoBolsa.ValorTermo.Value;

                short? idIndice = null;
                if (posicaoTermoBolsa.IdIndice.HasValue)
                {
                    idIndice = posicaoTermoBolsa.IdIndice.Value;
                }
                decimal rendimentoTotal = posicaoTermoBolsa.RendimentoTotal.Value;
                decimal rendimentoApropriadoAtual = posicaoTermoBolsa.RendimentoApropriado.Value;
                #endregion

                #region Cálculo do PU e valor corrigidos (correção por índice)
                decimal valorCorrigido = valorTermo;
                decimal puCorrigido = puTermo;
                if (idIndice.HasValue)
                {
                    decimal fatorIndice = CalculoFinanceiro.CalculaFatorIndice(dataOperacao, data,
                                                                               idIndice.Value, 100, true);
                    puCorrigido = puTermo * fatorIndice;
                    valorCorrigido = Utilitario.Truncate((quantidade * puCorrigido) / fator.Fator.Value, 2);
                }
                #endregion
                
                decimal rendimentoApropriar = rendimentoTotal - rendimentoApropriadoAtual;
                decimal valorCustoLiquidoAcao = Utilitario.Truncate((quantidade * puCustoLiquidoAcao) / fator.Fator.Value, 2);
                decimal valorCurva = valorCustoLiquidoAcao;
                //

                DateTime dataBase = Calendario.AdicionaDiaUtil(dataOperacao, 3, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                
                decimal rendimentoDia = 0;
                decimal novoRendimentoApropriado = 0;
                decimal novoRendimentoApropriar = 0;
                if (taxaTermo != 0 && data.CompareTo(dataBase) >= 0)
                {
                    if (prazoDecorrer != 0)
                    {
                        rendimentoDia = Utilitario.Truncate(rendimentoApropriar / prazoDecorrer, 2);
                    }
                    valorCurva = valorCustoLiquidoAcao + rendimentoDia;
                    decimal puCurva = valorCurva / quantidade * fator.Fator.Value;
                    novoRendimentoApropriado = rendimentoApropriadoAtual + rendimentoDia;
                    
                    if (data.CompareTo(dataVencimento) == 0)
                    {
                        novoRendimentoApropriar = 0;
                        novoRendimentoApropriado = rendimentoTotal;
                    }
                    else
                    {
                        novoRendimentoApropriar = rendimentoTotal - novoRendimentoApropriado;
                    }
                }

                prazoDecorrer -= 1; //Diminui o prazo a decorrer
                
                #region Atualiza posicaoTermoBolsa
                posicaoTermoBolsa.PrazoDecorrer = (short)prazoDecorrer;
                posicaoTermoBolsa.ValorCurva = valorCurva;
                posicaoTermoBolsa.ValorCorrigido = valorCorrigido;

                if (taxaTermo != 0 && data.CompareTo(dataBase) >= 0)
                {
                    posicaoTermoBolsa.RendimentoApropriado = novoRendimentoApropriado;
                    posicaoTermoBolsa.RendimentoApropriar = novoRendimentoApropriar;
                    posicaoTermoBolsa.RendimentoDia = rendimentoDia;
                }
                #endregion
            }

            posicaoTermoBolsaCollection.Save();
        }

        /// <summary>
        /// Busca todas as posições de termo vencendo em D-3 do vcto e cria lançamentos em LiquidacaoTermoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaVencimentoTermo(int idCliente, DateTime data)
        {
            //Deleta antes todas as liquidações (por decurso) na data
            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollectionDeletar = new LiquidacaoTermoBolsaCollection();
            liquidacaoTermoBolsaCollectionDeletar.DeletaLiquidacaoTermoBolsaVencimento(idCliente, data);
            //
            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();

            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();

            bool integraSinacor = (ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor ||
                                   ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa == (byte)IntegracaoBolsa.Sinacor_BTC);

            bool renovacaoTermo = ParametrosConfiguracaoSistema.Bolsa.RenovacaoTermo;
            if (renovacaoTermo && !integraSinacor)
            {
                DateTime dataVencimentoD3 = Calendario.AdicionaDiaUtil(data, 3, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                DateTime dataVencimentoD3Anterior = Calendario.SubtraiDiaUtil(dataVencimentoD3, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                //dataVencimentoD3Anterior é necessário para poder tratar o vencimento de termos em dias não úteis!!!!
                posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsa(idCliente, dataVencimentoD3, dataVencimentoD3Anterior);
            }
            else
            {
                DateTime dataProximo = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                //dataProximo é necessário para poder tratar o vencimento de termos em dias não úteis!!!!
                posicaoTermoBolsaCollection.BuscaPosicaoTermoBolsa(idCliente, dataProximo, data);
            }            
            
            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++)
            {
                PosicaoTermoBolsa posicaoTermoBolsa = posicaoTermoBolsaCollection[i];
                int idPosicao = posicaoTermoBolsa.IdPosicao.Value;
                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                int idAgente = posicaoTermoBolsa.IdAgente.Value;
                decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                decimal puTermoLiquido = posicaoTermoBolsa.PUTermoLiquido.Value;
                decimal valor = posicaoTermoBolsa.ValorTermo.Value;
                string numeroContrato = posicaoTermoBolsa.NumeroContrato;

                //Checa se tem alguma liquidação por antecipação na data, para não duplicar
                LiquidacaoTermoBolsa liquidacaoTermoBolsaExistente = new LiquidacaoTermoBolsa();
                liquidacaoTermoBolsaExistente.Query.Select(liquidacaoTermoBolsaExistente.Query.Quantidade.Sum());
                liquidacaoTermoBolsaExistente.Query.Where(liquidacaoTermoBolsaExistente.Query.IdCliente.Equal(idCliente),
                                                          liquidacaoTermoBolsaExistente.Query.IdAgente.Equal(idAgente),
                                                          liquidacaoTermoBolsaExistente.Query.DataMovimento.Equal(data),
                                                          liquidacaoTermoBolsaExistente.Query.IdPosicao.Equal(idPosicao),
                                                          liquidacaoTermoBolsaExistente.Query.TipoLiquidacao.NotEqual((byte)TipoLiquidacaoTermoBolsa.Decurso));
                liquidacaoTermoBolsaExistente.Query.Load();

                decimal quantidadeLiquidadaExistente = 0;
                if (liquidacaoTermoBolsaExistente.es.HasData && liquidacaoTermoBolsaExistente.Quantidade.HasValue)
                {
                    quantidadeLiquidadaExistente = liquidacaoTermoBolsaExistente.Quantidade.Value;

                    quantidade -= quantidadeLiquidadaExistente;
                }
                //

                if (quantidade != 0)
                {
                    LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
                    liquidacaoTermoBolsa.AddNew();
                    liquidacaoTermoBolsa.IdPosicao = idPosicao;
                    liquidacaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    liquidacaoTermoBolsa.IdCliente = idCliente;
                    liquidacaoTermoBolsa.DataMovimento = data;
                    liquidacaoTermoBolsa.DataLiquidacao = data;
                    liquidacaoTermoBolsa.IdAgente = idAgente;
                    liquidacaoTermoBolsa.TipoLiquidacao = (byte)TipoLiquidacaoTermoBolsa.Decurso;
                    liquidacaoTermoBolsa.Quantidade = quantidade;
                    liquidacaoTermoBolsa.Pu = puTermoLiquido;
                    liquidacaoTermoBolsa.Valor = valor;
                    liquidacaoTermoBolsa.Fonte = (byte)FonteLiquidacaoTermoBolsa.Interno;
                    liquidacaoTermoBolsa.NumeroContrato = numeroContrato;

                    liquidacaoTermoBolsaCollection.AttachEntity(liquidacaoTermoBolsa);
                }
            }

            liquidacaoTermoBolsaCollection.Save();
        }

        /// <summary>
        /// Retorna a quantidade em posição do ativo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadeAtivo(int idCliente, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa.Like(cdAtivoBolsa + "%"));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// Atualiza o nr de contrato (a partir do Sinacor) dos termos que estiverem com nr de contrato vazio na PosicaoTermoBolsa.
        /// Amarra pelo idCliente, cdAtivoBolsa, qtde e dataOperacao.
        /// </summary>
        /// <param name="idCliente"></param>
        public void AtualizaContratoTermoSinacor(int idCliente)
        {
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente) &
                                                    (posicaoTermoBolsaCollection.Query.NumeroContrato.Equal("") |
                                                     posicaoTermoBolsaCollection.Query.NumeroContrato.Equal("0")) &
                                                    posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0));
            posicaoTermoBolsaCollection.Query.Load();

            List<int> listaCodigos = new List<int>();

            if (posicaoTermoBolsaCollection.Count > 0)
            {
                ClienteBolsa clienteBolsa = new ClienteBolsa();
                listaCodigos = clienteBolsa.RetornaCodigosBovespa(idCliente);
            }

            foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
            {
                string cdAtivo = posicaoTermoBolsa.CdAtivoBolsaAcao + "T";
                DateTime dataOperacao = posicaoTermoBolsa.DataOperacao.Value;
                DateTime dataVencimento = posicaoTermoBolsa.DataVencimento.Value;
                decimal quantidade = posicaoTermoBolsa.Quantidade.Value;

                TcfposiTerm tcfposiTerm = new TcfposiTerm();
                int numeroContrato = tcfposiTerm.RetornaNumeroContrato(listaCodigos, cdAtivo, dataOperacao, dataVencimento, quantidade);
                
                if (numeroContrato != 0)
                {
                    posicaoTermoBolsa.NumeroContrato = numeroContrato.ToString();
                }
            }

            posicaoTermoBolsaCollection.Save();
   
        }
    }
}
