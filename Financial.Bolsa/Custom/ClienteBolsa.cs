﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Exceptions;
using Financial.Util;

namespace Financial.Bolsa {
    public partial class ClienteBolsa : esClienteBolsa 
    {
        /// <summary>
        /// Retorna uma lista de códigos Bovespa dado um Id cliente passado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public List<int> RetornaCodigosBovespa(int idCliente)
        {
            List<int> lista = new List<int>();

            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!this.LoadByPrimaryKey(idCliente))
            {
                return lista;
            }

            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(this.CodigoSinacor) && String.IsNullOrEmpty(this.CodigoSinacor2))
            {
                return lista;
            }

            if (String.IsNullOrEmpty(this.CodigoSinacor))
            {
                this.CodigoSinacor = this.CodigoSinacor2;
            }

            if (String.IsNullOrEmpty(this.CodigoSinacor2))
            {
                this.CodigoSinacor2 = this.CodigoSinacor;
            }

            this.CodigoSinacor = this.CodigoSinacor.Replace(";", ",");
            this.CodigoSinacor2 = this.CodigoSinacor.Replace(";", ",");
            
            string[] codigoSplit1 = this.CodigoSinacor.Split(new char[] { ',' });
            string[] codigoSplit2 = this.CodigoSinacor2.Split(new char[] { ',' });

            foreach (string codigo in codigoSplit1)
            {
                //Se não é número, lança exception
                if (!Utilitario.IsInteger(codigo))
                {
                    throw new CodigoClienteBovespaInvalido("Código Bovespa inválido para o IdCliente " + idCliente);
                }
                
                if (!lista.Contains(Convert.ToInt32(codigo)))
                {
                    lista.Add(Convert.ToInt32(codigo));
                }
            }

            foreach (string codigo in codigoSplit2)
            {
                //Se não é número, lança exception
                if (!Utilitario.IsInteger(codigo))
                {
                    throw new CodigoClienteBovespaInvalido("Código Bovespa inválido para o IdCliente " + idCliente);
                }

                if (!lista.Contains(Convert.ToInt32(codigo)))
                {
                    lista.Add(Convert.ToInt32(codigo));
                }
            }

            return lista;
        }

    }
}
