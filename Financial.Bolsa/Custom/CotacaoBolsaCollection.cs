﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 1.5.0.0
                       MyGeneration Version # 1.1.5.1
                           4/10/2006 09:34:01
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Common.Enums;

namespace Financial.Bolsa {
    public partial class CotacaoBolsaCollection : esCotacaoBolsaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoBolsaCollection));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public void DeletaCotacaoBolsa(DateTime data) 
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.CdAtivoBolsa, this.Query.Data)
              .Where(this.Query.Data.Equal(data));

            this.Query.Load();
            
            this.MarkAllAsDeleted();            
            this.Save();
        }

        /// <summary>
        ///  Seleciona os ativos do dia anterior que não estão no dia atual
        /// </summary>
        /// <param name="data"></param>
        /// <returns>bool indicando se encontrou registros</returns>
        public bool BuscaCotacaoDiaAnterior(DateTime data) 
        {
            DateTime diaAnterior = Util.Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, 
                                                                  TipoFeriado.Brasil);

            CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();
            cotacaoBolsaCollection.Query
              .Select(cotacaoBolsaCollection.Query.CdAtivoBolsa)
              .Where(cotacaoBolsaCollection.Query.Data.Equal(data));

            cotacaoBolsaCollection.Query.Load();
                        
            string[] vetorStrings = new string[cotacaoBolsaCollection.Count];          
            for (int i = 0; i < cotacaoBolsaCollection.Count; i++) {
                string cdAtivo = cotacaoBolsaCollection[i].CdAtivoBolsa;
                vetorStrings[i] = cdAtivo;
            }
                                  
            this.QueryReset();
            this.Query
              .Select(this.Query.CdAtivoBolsa, this.Query.Data, this.Query.PUMedio,
                      this.Query.PUAbertura, this.Query.PUFechamento)
              .Where( this.Query.Data.Equal(diaAnterior),
                      this.Query.CdAtivoBolsa.NotIn(vetorStrings)
              );
           
            bool retorno = this.Query.Load();
            
            return retorno;
        }
    }
}
