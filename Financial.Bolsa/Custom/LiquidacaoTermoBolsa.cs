﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.IO;
using Financial.Common;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using Financial.Bolsa.Exceptions;
using log4net;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Interfaces.Import.Bolsa;
using Financial.ContaCorrente;
using Financial.Common.Enums;
using Financial.ContaCorrente.Enums;
using Financial.Investidor.Enums;
using Financial.Interfaces.Sinacor;
using System.Collections;

namespace Financial.Bolsa {
    public partial class LiquidacaoTermoBolsa : esLiquidacaoTermoBolsa
    {        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="conlCollection"></param>
        /// <param name="data"></param>
        public void CarregaLiquidacaoTermoBolsaConl(ConlCollection conlCollection, DateTime data) 
        {
            #region GetIdAgente do Arquivo Conl
            int codigoBovespaAgente = conlCollection.CollectionConl[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgenteCorretora = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();

            //Deleta LiquidaçãoTermo de acordo com data e Fonte
            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollectionAux = new LiquidacaoTermoBolsaCollection();
            liquidacaoTermoBolsaCollectionAux.DeletaLiquidacaoTermoBolsa(data, FonteLiquidacaoTermoBolsa.ArquivoCONL);

            // Carrega dados do Conl
            Conl conl = new Conl();

            #region Inserção em LiquidacaoTermoBolsa
            for (int i = 0; i < conlCollection.CollectionConl.Count; i++) {
                conl = conlCollection.CollectionConl[i];

                if (conl.IsTipoRegistroIdentificacaoContratoLiquidado()) {
                    #region Copia informações para LiquidacaoTermoBolsa
                    // Só inclui os clientes que existirem na Base
                    int codigoClienteBovespa = conl.CodigoCliente.Value;
                    CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                    bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);

                    if (idClienteExiste) {
                        int idCliente = codigoClienteAgente.IdCliente.Value;

                        Cliente cliente = new Cliente();
                        cliente.LoadByPrimaryKey(idCliente);

                        if (cliente.IsAtivo) //Só importa para clientes ativos!
                        {
                            //Só importa para clientes com dataDia anterior à data passada ou data igual e status != fechado
                            if ((DateTime.Compare(cliente.DataDia.Value, data) < 0) ||
                                    (DateTime.Compare(cliente.DataDia.Value, data) == 0 && cliente.Status.Value != (byte)StatusCliente.Divulgado)) {
                                int numeroContrato = conl.NumeroContrato.Value;
                                int tipoLiquidacao = conl.TipoLiquidacao.Value;
                                int quantidade = Math.Abs(conl.Quantidade.Value);
                                decimal pu = conl.Pu.Value;
                                decimal valor = Math.Abs(conl.Valor.Value);
                                string cdAtivoBolsa = conl.CdAtivoBolsa;
                                DateTime dataLiquidacao = conl.DataLiquidacao.Value;
                                DateTime dataVencimento = conl.DataVencimento.Value;

                                if (tipoLiquidacao != (int)TipoLiquidacaoTermoBolsa.Decurso) //Vcto o sistema já faz sozinho
                                {
                                    // Formato com T e data no formato 'ddmmaaaa'
                                    StringBuilder dataAtivo = Utilitario.FormataData(dataVencimento, PatternData.formato3);
                                    cdAtivoBolsa = cdAtivoBolsa + dataAtivo.ToString();

                                    int? idPosicao = null;
                                    if (DateTime.Compare(cliente.DataDia.Value, data) == 0) {
                                        PosicaoTermoBolsaAbertura posicaoTermoBolsaAbertura = new PosicaoTermoBolsaAbertura();
                                        if (posicaoTermoBolsaAbertura.BuscaPosicaoTermoBolsaAbertura(idCliente, Convert.ToString(numeroContrato), data)) 
                                        {
                                            idPosicao = posicaoTermoBolsaAbertura.IdPosicao.Value;
                                            
                                            if (posicaoTermoBolsaAbertura.Quantidade.Value < 0)
                                            {
                                                quantidade = quantidade * -1;
                                                valor = valor * -1;
                                            }
                                        }
                                    }
                                    else {
                                        PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                                        if (posicaoTermoBolsa.BuscaPosicaoTermoBolsa(idCliente, Convert.ToString(numeroContrato))) 
                                        {
                                            idPosicao = posicaoTermoBolsa.IdPosicao.Value;

                                            if (posicaoTermoBolsa.Quantidade.Value < 0)
                                            {
                                                quantidade = quantidade * -1;
                                                valor = valor * -1;
                                            }
                                        }
                                    }

                                    if (idPosicao.HasValue) 
                                    {
                                        // lancamento de AtivoNaoCadastradoException                                       
                                        AtivoBolsa ativoBolsa = new AtivoBolsa();
                                        if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa)) {
                                            throw new AtivoNaoCadastradoException("Ativo não cadastrado: " + cdAtivoBolsa);
                                        }

                                        #region Copia para LiquidacaoTermoBolsa
                                        LiquidacaoTermoBolsa liquidacaoTermoBolsa = liquidacaoTermoBolsaCollection.AddNew();
                                        liquidacaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                                        liquidacaoTermoBolsa.IdAgente = idAgenteCorretora;
                                        liquidacaoTermoBolsa.IdCliente = idCliente;
                                        liquidacaoTermoBolsa.IdPosicao = idPosicao;
                                        liquidacaoTermoBolsa.DataMovimento = data;
                                        liquidacaoTermoBolsa.TipoLiquidacao = (byte)tipoLiquidacao;
                                        liquidacaoTermoBolsa.Quantidade = quantidade;
                                        liquidacaoTermoBolsa.Pu = pu;
                                        liquidacaoTermoBolsa.Valor = valor;
                                        liquidacaoTermoBolsa.Fonte = (byte)FonteLiquidacaoTermoBolsa.ArquivoCONL;
                                        liquidacaoTermoBolsa.NumeroContrato = Convert.ToString(numeroContrato);
                                        liquidacaoTermoBolsa.DataLiquidacao = dataLiquidacao;
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }

            liquidacaoTermoBolsaCollection.Save();
            #endregion
        }

        /// <summary>
        /// Processa todas as liquidações (LPD, LPDE, LA, Decurso automático) do dia,
        /// jogando o valor projetado em Liquidacao e atualizando 
        /// a posição física do termo e da ação objeto (por depósito/retirada).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaLiquidacaoTermo(int idCliente, DateTime data)
        {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
                        
            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();
            liquidacaoTermoBolsaCollection.BuscaLiquidacaoTermoBolsa(idCliente, data);

            Hashtable hsContaAtivo = new Hashtable();
            for (int i = 0; i < liquidacaoTermoBolsaCollection.Count; i++)
            {
                LiquidacaoTermoBolsa liquidacaoTermoBolsa = liquidacaoTermoBolsaCollection[i];

                #region Carrega variáveis de LiquidacaoTermoBolsa
                int idAgente = liquidacaoTermoBolsa.IdAgente.Value;
                byte tipoLiquidacao = liquidacaoTermoBolsa.TipoLiquidacao.Value;
                string cdAtivoBolsa = liquidacaoTermoBolsa.CdAtivoBolsa;
                DateTime dataLiquidacao = liquidacaoTermoBolsa.DataLiquidacao.Value;
                decimal quantidadeLiquidada = liquidacaoTermoBolsa.Quantidade.Value;
                decimal pu = liquidacaoTermoBolsa.Pu.Value;
                decimal valor = liquidacaoTermoBolsa.Valor.Value;
                string numeroContrato = liquidacaoTermoBolsa.NumeroContrato;
                int idPosicao = liquidacaoTermoBolsa.IdPosicao.Value;
                int idMoedaAtivo = liquidacaoTermoBolsa.UpToAtivoBolsaByCdAtivoBolsa.IdMoeda.Value;
                #endregion

                #region Verifica os tipos de depósito/retirada a serem feitos para a ação e para o termo
                string tipoOperacaoAcao;
                if (quantidadeLiquidada > 0)
                {
                    tipoOperacaoAcao = TipoOperacaoBolsa.Deposito;
                }
                else {
                    tipoOperacaoAcao = TipoOperacaoBolsa.Retirada;                    
                }
                #endregion

                decimal quantidadeAbsoluta = Math.Abs(quantidadeLiquidada);

                #region Inserção na OperacaoBolsa para entrada/saída do a vista
                //Retorna a ação objeto do termo
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                string cdAtivoBolsaAcao = ativoBolsa.RetornaCdAtivoBolsaObjeto(cdAtivoBolsa);

                if (cdAtivoBolsaAcao == "")
                {
                    throw new Exception("O termo " + cdAtivoBolsa + " não tem cadastrado ativo objeto associado para liquidação.");
                }
                //
                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.AddNew();
                operacaoBolsa.IdCliente = idCliente;
                operacaoBolsa.CdAtivoBolsa = cdAtivoBolsaAcao;
                operacaoBolsa.IdAgenteLiquidacao = idAgente;
                operacaoBolsa.IdAgenteCorretora = idAgente;
                operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                operacaoBolsa.TipoOperacao = tipoOperacaoAcao;
                operacaoBolsa.Data = data;
                operacaoBolsa.Pu = pu;
                operacaoBolsa.PULiquido = pu;
                operacaoBolsa.Valor = valor;
                operacaoBolsa.ValorLiquido = valor;
                operacaoBolsa.Quantidade = quantidadeAbsoluta;
                operacaoBolsa.DataLiquidacao = dataLiquidacao;
                operacaoBolsa.Origem = OrigemOperacaoBolsa.LiquidacaoTermo;
                operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                operacaoBolsa.CalculaDespesas = "N";
                operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;

                operacaoBolsaCollection.AttachEntity(operacaoBolsa);
                #endregion

                int idContaDefault = 0;
                #region Busca Conta Default
                if (hsContaAtivo.Contains(cdAtivoBolsa))
                {
                    idContaDefault = (int)hsContaAtivo[cdAtivoBolsa];
                }
                else
                {
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                    hsContaAtivo.Add(cdAtivoBolsa, idContaDefault);
                }
                #endregion

                //Baixa da posição de termo
                PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
                if (!posicaoTermoBolsa.LoadByPrimaryKey(idPosicao))
                {
                    throw new Exception("A liquidação de " + cdAtivoBolsa + " - " + quantidadeLiquidada.ToString() + " perdeu o vínculo original com a posição. Favor relançar a liquidação.");
                }
                                
                //Ajusta rendimentos
                decimal quantidadePosicaoAbsoluta = Math.Abs(posicaoTermoBolsa.Quantidade.Value);

                if (quantidadePosicaoAbsoluta < quantidadeAbsoluta)
                {
                    throw new Exception("A liquidação de " + cdAtivoBolsa + " - " + quantidadeLiquidada.ToString() + " não possui quantidade suficiente.");
                }

                decimal fatorAjuste = (quantidadePosicaoAbsoluta - quantidadeAbsoluta) / quantidadePosicaoAbsoluta;
                posicaoTermoBolsa.RendimentoTotal = Math.Round(fatorAjuste * posicaoTermoBolsa.RendimentoTotal.Value, 2);
                posicaoTermoBolsa.RendimentoApropriado = Math.Round(fatorAjuste * posicaoTermoBolsa.RendimentoApropriado.Value, 2);
                posicaoTermoBolsa.RendimentoApropriar = Math.Round(fatorAjuste * posicaoTermoBolsa.RendimentoApropriar.Value, 2);
                posicaoTermoBolsa.ValorTermo = Math.Round(fatorAjuste * posicaoTermoBolsa.ValorTermo.Value, 2);
                posicaoTermoBolsa.ValorTermoLiquido = Math.Round(fatorAjuste * posicaoTermoBolsa.ValorTermoLiquido.Value, 2);
                posicaoTermoBolsa.ValorMercado = Math.Round(fatorAjuste * posicaoTermoBolsa.ValorMercado.Value, 2);
                posicaoTermoBolsa.ValorCorrigido = Math.Round(fatorAjuste * posicaoTermoBolsa.ValorCorrigido.Value, 2);
                posicaoTermoBolsa.ValorCurva = Math.Round(fatorAjuste * posicaoTermoBolsa.ValorCurva.Value, 2);                
                //
                posicaoTermoBolsa.Quantidade -= quantidadeLiquidada;
            
                posicaoTermoBolsa.Save();
                //

                if (tipoLiquidacao != (byte)TipoLiquidacaoTermoBolsa.Decurso)
                {
                    valor = valor * -1; //Termos comprados debitam o caixa, vendidos creditam o caixa!
                
                    #region Inserção em Liquidacao do valor projetado a liquidar do termo
                    string descricao = "Liquidação de Termo - " + Convert.ToString(quantidadeAbsoluta) + " " + cdAtivoBolsaAcao;
                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.AddNew();

                    liquidacao.IdCliente = idCliente;
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = descricao;
                    liquidacao.Valor = valor;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.Bolsa.AntecipacaoTermo;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdAgente = idAgente;
                    liquidacao.IdConta = idContaDefault;

                    liquidacaoCollection.AttachEntity(liquidacao);
                    #endregion

                    //Tenta baixar o valor a liquidar no vencimento (termos comprados e vendidos), pelo IdentificadorOrigem
                    Liquidacao liquidacaoAtualizar = new Liquidacao();
                    if (!liquidacaoAtualizar.AtualizaLiquidacaoPorIdentificadorOrigem(idPosicao, valor * -1))
                    {
                        //Se não conseguiu achar o lançamento em Liquidaco, ou IdentificadorOrigem = null
                        //lança uma contrapartida em Liquidacao do termo a vencer, diminuindo o valor a liquidar no vcto
                        #region Inserção em Liquidacao do valor projetado a liquidar do termo
                        descricao = "";
                        if (quantidadeLiquidada > 0)
                        {
                            descricao = "Compra Termo " + cdAtivoBolsaAcao + " (Baixa)";
                        }
                        else
                        {
                            descricao = "Venda Termo " + cdAtivoBolsaAcao + " (Baixa)";
                        }

                        liquidacao = new Liquidacao();
                        liquidacao.AddNew();

                        liquidacao.IdCliente = idCliente;
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = posicaoTermoBolsa.DataVencimento.Value;
                        liquidacao.Descricao = descricao;
                        liquidacao.Valor = valor * -1;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.Bolsa.AntecipacaoTermo;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdAgente = idAgente;
                        liquidacao.IdConta = idContaDefault;

                        liquidacaoCollection.AttachEntity(liquidacao);
                        #endregion
                    }
                    //
                }
            }

            //Save das collections
            operacaoBolsaCollection.Save();
            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Retorna o total (quantidade) liquidado da posicao (IdPosicao) na data passada.
        /// </summary>
        /// <param name="IdPosicao"></param>
        /// <param name="data"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public decimal RetornaTotalLiquidado(int IdPosicao, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdPosicao.Equal(IdPosicao),
                        this.Query.DataMovimento.Equal(data));

            this.Query.Load();

            decimal quantidade = 0;
            if (this.es.HasData && this.Quantidade.HasValue)
            {
                quantidade = this.Quantidade.Value;
            }

            return quantidade;
        }

        /// <summary>
        /// Integra as liquidações de Termos do dia do Sinacor.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void IntegraLiquidacaoTermoSinacor(int idCliente, DateTime data)
        {
            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();

            // Deleta as as Liquidações do dia
            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollectionDeletar = new LiquidacaoTermoBolsaCollection();
            liquidacaoTermoBolsaCollectionDeletar.DeletaLiquidacaoTermoBolsa(idCliente, data, FonteLiquidacaoTermoBolsa.Sinacor);

            ClienteBolsa clienteBolsa = new ClienteBolsa();

            #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!clienteBolsa.LoadByPrimaryKey(idCliente))
            {
                return;
            }

            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor) && String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                return;                
            }

            //Se nenhum dos 2 códigos é número, lança exception
            if (!Utilitario.IsInteger(clienteBolsa.CodigoSinacor) && !Utilitario.IsInteger(clienteBolsa.CodigoSinacor2))
            {
                return;                
            }

            int codigoCliente = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = Convert.ToInt32(clienteBolsa.CodigoSinacor);
            }
            int codigoCliente2 = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = Convert.ToInt32(clienteBolsa.CodigoSinacor2);
            }

            //Se algum dos 2 códigos for nulo, repete o código de um para ao outro
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = codigoCliente2;
            }
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = codigoCliente;
            }
            #endregion

            List<Int32> listaCodigos = new List<int>();
            listaCodigos.Add(codigoCliente);
            listaCodigos.Add(codigoCliente2);

            TcfmoviLiqdTermCollection tcfmoviLiqdTermCollection = new TcfmoviLiqdTermCollection();
            tcfmoviLiqdTermCollection.BuscaContratosLiquidados(listaCodigos, data);

            HcfmoviLiqdTermCollection hcfmoviLiqdTermCollection = new HcfmoviLiqdTermCollection();
            hcfmoviLiqdTermCollection.BuscaContratosLiquidados(listaCodigos, data);

            TcfmoviLiqdTermCollection tcfmoviLiqdTermCollectionAux = new TcfmoviLiqdTermCollection(hcfmoviLiqdTermCollection);
            tcfmoviLiqdTermCollection.Combine(tcfmoviLiqdTermCollectionAux);

            int idAgenteMercado = 0;
            if (tcfmoviLiqdTermCollection.Count > 0)
            {
                //Busca o IdAgenteMercado default
                int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                AgenteMercado agenteMercado = new AgenteMercado();
                idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);
            }

            foreach (TcfmoviLiqdTerm tcfmoviLiqdTerm in tcfmoviLiqdTermCollection)
            {
                DateTime dataVencimento = tcfmoviLiqdTerm.DataVenc.Value;
                string cdAtivoBolsa = AtivoBolsa.RetornaCdAtivoBolsaConcatenado(tcfmoviLiqdTerm.CodNeg, dataVencimento);
                DateTime dataLiquidacao = tcfmoviLiqdTerm.DataLiqd.Value;                
                string numeroContrato = tcfmoviLiqdTerm.NumCotr.Value.ToString();
                decimal quantidadeLiquidacao = tcfmoviLiqdTerm.QtdeLqdo.Value;
                byte tipoLiquidacao = Convert.ToByte(tcfmoviLiqdTerm.TipoLiqd.Value);

                AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                if (!ativoBolsaTermo.LoadByPrimaryKey(cdAtivoBolsa))
                {
                    throw new Exception("Existe uma liquidação de termo do ativo " + cdAtivoBolsa + ", sendo que este ativo não está cadastrado no sistema.");
                }
                
                PosicaoTermoBolsaAberturaCollection posicaoTermoBolsaAberturaCollection = new PosicaoTermoBolsaAberturaCollection();
                posicaoTermoBolsaAberturaCollection.Query.Select(posicaoTermoBolsaAberturaCollection.Query.IdPosicao,
                                                                posicaoTermoBolsaAberturaCollection.Query.DataVencimento,
                                                                posicaoTermoBolsaAberturaCollection.Query.Quantidade,
                                                                posicaoTermoBolsaAberturaCollection.Query.PUTermo);
                posicaoTermoBolsaAberturaCollection.Query.Where(posicaoTermoBolsaAberturaCollection.Query.NumeroContrato.Equal(numeroContrato),                                                                    
                                                                posicaoTermoBolsaAberturaCollection.Query.DataHistorico.Equal(data),
                                                                posicaoTermoBolsaAberturaCollection.Query.IdCliente.Equal(idCliente),
                                                                posicaoTermoBolsaAberturaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

                int? idPosicao = null;
                if (posicaoTermoBolsaAberturaCollection.Query.Load())
                {
                    idPosicao = posicaoTermoBolsaAberturaCollection[0].IdPosicao.Value;
                    decimal quantidadePosicao = posicaoTermoBolsaAberturaCollection[0].Quantidade.Value;
                    decimal puTermo = posicaoTermoBolsaAberturaCollection[0].PUTermo.Value;
                    string cdAtivoBolsaObjeto = AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa);

                    if (Math.Abs(quantidadeLiquidacao) > Math.Abs(quantidadePosicao))
                    {
                        quantidadeLiquidacao = quantidadePosicao;
                    }

                    if (quantidadePosicao < 0)
                    {
                        quantidadeLiquidacao = Math.Abs(quantidadeLiquidacao) * -1;
                    }

                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsaObjeto, data);

                    decimal valorLiquidacao = Math.Round(quantidadeLiquidacao * puTermo / fatorCotacaoBolsa.Fator.Value, 2);

                    LiquidacaoTermoBolsa liquidacaoTermoBolsa = liquidacaoTermoBolsaCollection.AddNew();
                    liquidacaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    liquidacaoTermoBolsa.DataLiquidacao = dataLiquidacao;
                    liquidacaoTermoBolsa.DataMovimento = data;
                    liquidacaoTermoBolsa.Fonte = (byte)FonteLiquidacaoTermoBolsa.Sinacor;
                    liquidacaoTermoBolsa.IdAgente = idAgenteMercado;
                    liquidacaoTermoBolsa.IdCliente = idCliente;
                    liquidacaoTermoBolsa.IdPosicao = idPosicao;
                    liquidacaoTermoBolsa.NumeroContrato = numeroContrato;
                    liquidacaoTermoBolsa.Pu = puTermo;
                    liquidacaoTermoBolsa.Quantidade = quantidadeLiquidacao;
                    liquidacaoTermoBolsa.TipoLiquidacao = tipoLiquidacao;
                    liquidacaoTermoBolsa.Valor = valorLiquidacao;
                }                
            }
            

            liquidacaoTermoBolsaCollection.Save();
        }
    }
}
