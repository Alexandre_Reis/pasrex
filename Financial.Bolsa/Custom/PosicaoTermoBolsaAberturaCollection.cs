﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa
{
	public partial class PosicaoTermoBolsaAberturaCollection : esPosicaoTermoBolsaAberturaCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(PosicaoTermoBolsaAberturaCollection));

        public PosicaoTermoBolsaAberturaCollection(PosicaoTermoBolsaCollection posicaoTermoBolsaCollection, DateTime dataHistorico)
        {
            for (int i = 0; i < posicaoTermoBolsaCollection.Count; i++)
            {
                //
                PosicaoTermoBolsaAbertura p = new PosicaoTermoBolsaAbertura();

                // Para cada Coluna de PosicaoTermoBolsa copia para PosicaoTermoBolsa
                foreach (esColumnMetadata colPosicao in posicaoTermoBolsaCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas menos a Data 
                    esColumnMetadata colPosicaoTermoBolsa = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                    if (posicaoTermoBolsaCollection[i].GetColumn(colPosicao.Name) != null)
                    {
                        p.SetColumn(colPosicaoTermoBolsa.Name, posicaoTermoBolsaCollection[i].GetColumn(colPosicao.Name));
                    }
                    p.DataHistorico = dataHistorico;
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Deleta todas as posições de termo de bolsa.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoTermoBolsaAberturaDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.query.IdPosicao, this.query.DataHistorico)
                    .Where(this.query.IdCliente == idCliente,
                           this.query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoTermoBolsaAberturaCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();         
        }
	}
}
