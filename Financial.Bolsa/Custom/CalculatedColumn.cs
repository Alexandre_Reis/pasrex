using System;
namespace Financial.Bolsa {
    //HACK: uso provisorio para Calculated Column
    public class CalculatedColumn : System.Attribute {
        
        private string _calculatedcolumnname = string.Empty;

        public CalculatedColumn(bool isTransient) {
            this.IsTransient = isTransient;
        }

        public string CalculatedColumnName {
            get { return _calculatedcolumnname; }
            set { _calculatedcolumnname = value; }
        }        
        
        private Type _datatype = typeof(string);
        
        public Type DataType {
            get { return _datatype; }
            set { _datatype = value; }
        }

        private bool _istransient;
        
        public bool IsTransient {
            get { return _istransient; }
            set { _istransient = value; }
        }
    }
}
