﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa
{
	public partial class BloqueioBolsa : esBloqueioBolsa
	{
        /// <summary>
        /// Processa todos os bloqueios/desbloqueios lançados na data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaBloqueioBolsa(int idCliente, DateTime data)
        {
            BloqueioBolsaCollection bloqueioBolsaCollection = new BloqueioBolsaCollection();
            bloqueioBolsaCollection.Query.Where(bloqueioBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                bloqueioBolsaCollection.Query.DataOperacao.Equal(data));
            bloqueioBolsaCollection.Query.Load();

            foreach (BloqueioBolsa bloqueioBolsa in bloqueioBolsaCollection)
            {
                string cdAtivoBolsa = bloqueioBolsa.CdAtivoBolsa;
                int idAgente = bloqueioBolsa.IdAgente.Value;
                decimal quantidade = bloqueioBolsa.Quantidade.Value;
                byte tipoOperacao = bloqueioBolsa.TipoOperacao.Value;
                int? tipoCarteiraBloqueada = bloqueioBolsa.TipoCarteiraBloqueada;

                PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                         posicaoBolsa.Query.IdAgente.Equal(idAgente));
                if (posicaoBolsa.Query.Load())
                {
                    if (tipoOperacao == (byte)TipoOperacaoBloqueio.Bloqueio)
                    {
                        bool bloqueioTotal = false;
                        if (posicaoBolsa.QuantidadeBloqueada.Value + quantidade > posicaoBolsa.Quantidade.Value)
                        {
                            posicaoBolsa.QuantidadeBloqueada = posicaoBolsa.Quantidade;
                            bloqueioTotal = true;
                        }
                        else
                        {
                            posicaoBolsa.QuantidadeBloqueada += quantidade;
                        }

                        if (tipoCarteiraBloqueada.HasValue)
                        {
                            #region Trata detalhamento da posição livre após o bloqueio
                            PosicaoBolsaDetalhe posicaoBolsaDetalheLivre = new PosicaoBolsaDetalhe();
                            if (posicaoBolsaDetalheLivre.LoadByPrimaryKey(data, idCliente, idAgente, cdAtivoBolsa, (int)TipoCarteiraBovespa.Livre))
                            {
                                if (bloqueioTotal)
                                {
                                    posicaoBolsaDetalheLivre.Quantidade = 0;
                                }
                                else
                                {
                                    posicaoBolsaDetalheLivre.Quantidade -= quantidade;
                                }
                            }
                            else
                            {
                                posicaoBolsaDetalheLivre = new PosicaoBolsaDetalhe();
                                posicaoBolsaDetalheLivre.CdAtivoBolsa = cdAtivoBolsa;
                                posicaoBolsaDetalheLivre.DataHistorico = data;
                                posicaoBolsaDetalheLivre.IdAgente = idAgente;
                                posicaoBolsaDetalheLivre.IdCliente = idCliente;

                                posicaoBolsaDetalheLivre.QuantidadeAbertura = 0;
                                posicaoBolsaDetalheLivre.Quantidade = posicaoBolsa.Quantidade.Value - posicaoBolsa.QuantidadeBloqueada.Value;

                                posicaoBolsaDetalheLivre.TipoCarteira = (int)TipoCarteiraBovespa.Livre;
                            }

                            posicaoBolsaDetalheLivre.Save();
                            #endregion

                            #region Trata detalhamento da posição bloqueada após o bloqueio
                            PosicaoBolsaDetalhe posicaoBolsaDetalheBloqueadaTotal = new PosicaoBolsaDetalhe();
                            posicaoBolsaDetalheBloqueadaTotal.Query.Select(posicaoBolsaDetalheBloqueadaTotal.Query.Quantidade.Sum());
                            posicaoBolsaDetalheBloqueadaTotal.Query.Where(posicaoBolsaDetalheBloqueadaTotal.Query.IdCliente.Equal(idCliente),
                                                                          posicaoBolsaDetalheBloqueadaTotal.Query.DataHistorico.Equal(data),
                                                                          posicaoBolsaDetalheBloqueadaTotal.Query.IdAgente.Equal(idAgente),
                                                                          posicaoBolsaDetalheBloqueadaTotal.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                                          posicaoBolsaDetalheBloqueadaTotal.Query.TipoCarteira.NotIn((int)TipoCarteiraBovespa.Livre,
                                                                                                                                      tipoCarteiraBloqueada.Value));
                            posicaoBolsaDetalheBloqueadaTotal.Query.Load();

                            decimal totalQuantidadeOutrasCarteiras = posicaoBolsaDetalheBloqueadaTotal.Quantidade.HasValue ? posicaoBolsaDetalheBloqueadaTotal.Quantidade.Value : 0;
  

                            PosicaoBolsaDetalhe posicaoBolsaDetalheBloqueada = new PosicaoBolsaDetalhe();
                            if (posicaoBolsaDetalheBloqueada.LoadByPrimaryKey(data, idCliente, idAgente, cdAtivoBolsa, tipoCarteiraBloqueada.Value))
                            {
                                if (bloqueioTotal)
                                {
                                    posicaoBolsaDetalheBloqueada.Quantidade = posicaoBolsa.Quantidade.Value - totalQuantidadeOutrasCarteiras;
                                }
                                else
                                {
                                    posicaoBolsaDetalheBloqueada.Quantidade += quantidade;

                                    if (posicaoBolsaDetalheBloqueada.Quantidade.Value + totalQuantidadeOutrasCarteiras > posicaoBolsa.Quantidade.Value)
                                    {
                                        posicaoBolsaDetalheBloqueada.Quantidade = posicaoBolsa.Quantidade.Value - totalQuantidadeOutrasCarteiras;
                                    }
                                }
                            }
                            else
                            {
                                posicaoBolsaDetalheBloqueada = new PosicaoBolsaDetalhe();
                                posicaoBolsaDetalheBloqueada.CdAtivoBolsa = cdAtivoBolsa;
                                posicaoBolsaDetalheBloqueada.DataHistorico = data;
                                posicaoBolsaDetalheBloqueada.IdAgente = idAgente;
                                posicaoBolsaDetalheBloqueada.IdCliente = idCliente;
                                posicaoBolsaDetalheBloqueada.QuantidadeAbertura = 0;

                                if (bloqueioTotal)
                                {
                                    posicaoBolsaDetalheBloqueada.Quantidade = posicaoBolsa.Quantidade.Value;
                                }
                                else
                                {
                                    posicaoBolsaDetalheBloqueada.Quantidade = quantidade;
                                }

                                posicaoBolsaDetalheBloqueada.TipoCarteira = tipoCarteiraBloqueada.Value;
                            }

                            posicaoBolsaDetalheBloqueada.Save();
                            #endregion

                        }
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoBloqueio.Desbloqueio)
                    {
                        bool desbloqueioTotal = false;
                        if (posicaoBolsa.QuantidadeBloqueada.Value - quantidade < 0)
                        {
                            posicaoBolsa.QuantidadeBloqueada = 0;
                        }
                        else
                        {
                            posicaoBolsa.QuantidadeBloqueada -= quantidade;
                        }

                        if (tipoCarteiraBloqueada.HasValue)
                        {
                            #region Trata detalhamento da posição livre após o desbloqueio
                            PosicaoBolsaDetalhe posicaoBolsaDetalheLivre = new PosicaoBolsaDetalhe();
                            if (posicaoBolsaDetalheLivre.LoadByPrimaryKey(data, idCliente, idAgente, cdAtivoBolsa, (int)TipoCarteiraBovespa.Livre))
                            {
                                if (desbloqueioTotal)
                                {
                                    posicaoBolsaDetalheLivre.Quantidade = posicaoBolsa.Quantidade.Value;
                                }
                                else
                                {
                                    posicaoBolsaDetalheLivre.Quantidade += quantidade;
                                }
                            }
                            else
                            {
                                posicaoBolsaDetalheLivre = new PosicaoBolsaDetalhe();
                                posicaoBolsaDetalheLivre.CdAtivoBolsa = cdAtivoBolsa;
                                posicaoBolsaDetalheLivre.DataHistorico = data;
                                posicaoBolsaDetalheLivre.IdAgente = idAgente;
                                posicaoBolsaDetalheLivre.IdCliente = idCliente;

                                posicaoBolsaDetalheLivre.QuantidadeAbertura = 0;

                                posicaoBolsaDetalheLivre.Quantidade = posicaoBolsa.Quantidade.Value - posicaoBolsa.QuantidadeBloqueada.Value;

                                posicaoBolsaDetalheLivre.TipoCarteira = (int)TipoCarteiraBovespa.Livre;
                            }

                            posicaoBolsaDetalheLivre.Save();
                            #endregion

                            #region Trata detalhamento da posição bloqueada após o desbloqueio
                            PosicaoBolsaDetalhe posicaoBolsaDetalheBloqueada = new PosicaoBolsaDetalhe();
                            if (posicaoBolsaDetalheBloqueada.LoadByPrimaryKey(data, idCliente, idAgente, cdAtivoBolsa, tipoCarteiraBloqueada.Value))
                            {
                                if (desbloqueioTotal)
                                {
                                    posicaoBolsaDetalheBloqueada.Quantidade = 0;
                                }
                                else
                                {
                                    posicaoBolsaDetalheBloqueada.Quantidade -= quantidade;

                                    if (posicaoBolsaDetalheBloqueada.Quantidade.Value < 0)
                                    {
                                        posicaoBolsaDetalheBloqueada.Quantidade = 0;
                                    }
                                }
                            }
                            else
                            {
                                posicaoBolsaDetalheBloqueada = new PosicaoBolsaDetalhe();
                                posicaoBolsaDetalheBloqueada.CdAtivoBolsa = cdAtivoBolsa;
                                posicaoBolsaDetalheBloqueada.DataHistorico = data;
                                posicaoBolsaDetalheBloqueada.IdAgente = idAgente;
                                posicaoBolsaDetalheBloqueada.IdCliente = idCliente;

                                posicaoBolsaDetalheBloqueada.QuantidadeAbertura = 0;
                                posicaoBolsaDetalheBloqueada.Quantidade = 0;                                

                                posicaoBolsaDetalheBloqueada.TipoCarteira = tipoCarteiraBloqueada.Value;
                            }

                            posicaoBolsaDetalheBloqueada.Save();
                            #endregion
                        }
                    }

                    posicaoBolsa.Save();
                }
            }
        }
	}
}
