﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Common;
using Financial.Interfaces.Import.Bolsa;
using Financial.Investidor;
using System.IO;
using Financial.Bolsa.Enums;
using Financial.Bolsa.Exceptions;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

namespace Financial.Bolsa
{
	public partial class TabelaCRCA : esTabelaCRCA
	{
        /// <summary>
        /// 
        /// </summary>
        /// <param name="crcaCollection"></param>
        /// <param name="data"></param>
        /// throws AtivoNaoCadastradoException se codigo do Ativo não estiver cadastrado na Base
        public void CarregaTabelaCrca(CrcaCollection crcaCollection, DateTime data) {
            
            #region GetIdAgente do Arquivo Crca
            int codigoBovespaAgente = crcaCollection.CollectionCrca[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            Crca crca = new Crca();

            TabelaCRCACollection tabelaCRCACollectionDeletar = new TabelaCRCACollection();
            tabelaCRCACollectionDeletar.Query.Where(tabelaCRCACollectionDeletar.Query.DataMovimento.Equal(data));
            tabelaCRCACollectionDeletar.Query.Load();
            tabelaCRCACollectionDeletar.MarkAllAsDeleted();
            tabelaCRCACollectionDeletar.Save();

            TabelaCRCACollection tabelaCRCACollection = new TabelaCRCACollection();

            // Pega as Ofertas do arquivo de Crca
            for (int i = 0; i < crcaCollection.CollectionCrca.Count; i++) {
                crca = crcaCollection.CollectionCrca[i];

                if ((int)crca.QuantidadeEfetiva != 0) 
                {                    
                    #region Copia informações para TabelaCRCACollection
                    if (crca.IsTipoRegistroMovimentoDia()) {
                        TabelaCRCA tabelaCRCA = tabelaCRCACollection.AddNew();
                        tabelaCRCA.CodigoCliente = (int)crca.CodigoCliente;
                        tabelaCRCA.CodigoCustodiante = (int)crca.CodigoCustodiante;
                        tabelaCRCA.CodigoIsin = crca.CodigoIsin;
                        tabelaCRCA.CpfCNPJ = 0; //Não será usado por ora
                        tabelaCRCA.DataMovimento = data;
                        tabelaCRCA.Modalidade = crca.ModalidadeOferta;
                        tabelaCRCA.QuantidadeEfetiva = (int)crca.QuantidadeEfetiva;
                        tabelaCRCA.ValorEfetivo = crca.ValorEfetivo;
                    }
                    #endregion
                }
            }

            tabelaCRCACollection.Save();            
        }
	}
}
