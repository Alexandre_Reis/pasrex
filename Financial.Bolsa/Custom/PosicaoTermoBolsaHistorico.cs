﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa {
    public partial class PosicaoTermoBolsaHistorico : esPosicaoTermoBolsaHistorico {
        private static readonly ILog log = LogManager.GetLogger(typeof(PosicaoTermoBolsaHistorico));

        /// <summary>
        /// Retorna a soma valor de mercado.
        /// Retorna 0 se não houver registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>        
        public decimal RetornaValorTermo(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();

            decimal valorMercado = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;

            return valorMercado;
        }

        /// <summary>
        /// Retorna a soma valor de mercado para termos com posição comprada.
        /// Retorna 0 se não houver registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>        
        public decimal RetornaValorTermoComprado(int idCliente, DateTime dataHistorico) 
        {        
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.Quantidade.GreaterThan(0));

            this.Query.Load();

            decimal valorMercado = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;

            return valorMercado;
        }

        /// <summary>
        /// Retorna a soma do valor de mercado para termos com posição vendida.
        /// Retorna 0 se não houver registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaValorTermoVendido(int idCliente, DateTime dataHistorico) 
        {        
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.Quantidade.LessThan(0));

            this.Query.Load();

            decimal valorMercado = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;

            return valorMercado;
        }

        /// <summary>
        /// Retorna a soma de termos com posição comprada.
        /// Retorna 0 se não houver registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>        
        public decimal RetornaValorTermoOriginalComprado(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorTermo.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.Quantidade.GreaterThan(0));

            this.Query.Load();
            
            decimal valorTermo = this.ValorTermo.HasValue ? this.ValorTermo.Value : 0;

            return valorTermo;
        }

        /// <summary>
        /// Retorna a soma de termos com posição vendida.
        /// Retorna 0 se não houver registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>        
        public decimal RetornaValorTermoOriginalVendido(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorTermo.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.Quantidade.LessThan(0));

            this.Query.Load();

            decimal valorTermo = this.ValorTermo.HasValue ? this.ValorTermo.Value : 0;

            return valorTermo;
        }

        /// <summary>
        /// Soma do valor de mercado de todas as posições historicas dado um cliente e uma data e um ativo.
        /// Retorna 0 se não existir registros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public decimal RetornaValorMercadoSum(int idCliente, string cdAtivoBolsa, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Soma do valor de mercado de todas as posições historicas dado um cliente e uma data e um ativo.
        /// Retorna 0 se não existir registros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public decimal RetornaValorMercadoSum(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.query.IdCliente == idCliente);

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna o rendimento a apropriar para termos. Somente para qtde menor que zero.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaRendimentoApropriar(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.RendimentoApropriar.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.query.DataHistorico == dataHistorico,
                        this.Query.Quantidade.LessThan(0));

            this.Query.Load();

            decimal rendimentoApropriar = this.RendimentoApropriar.HasValue ? this.RendimentoApropriar.Value : 0;

            return rendimentoApropriar;
        }
        
    }
}
