using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa
{
    public partial class PosicaoBolsaCollection : esPosicaoBolsaCollection
    {
        // Construtor
        // Cria uma nova PosicaoBolsaCollection com os dados de PosicaoBolsaHistoricoCollection
        // Todos do dados de PosicaoBolsaHistoricoCollection são copiados com excessão da dataHistorico
        public PosicaoBolsaCollection(PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection) {
            for (int i = 0; i < posicaoBolsaHistoricoCollection.Count; i++) {
                //
                PosicaoBolsa p = new PosicaoBolsa();

                // Para cada Coluna de PosicaoBolsaHistorico copia para PosicaoBolsa
                foreach (esColumnMetadata colPosicaoHistorico in posicaoBolsaHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != PosicaoBolsaHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colPosicaoBolsa = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (posicaoBolsaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
                            p.SetColumn(colPosicaoBolsa.Name, posicaoBolsaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsaCollection com os campos CdAtivoBolsa, ValorMercado.Sum.
        /// Group By CdAtivoBolsa
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoBolsaAgrupadoEnquadra(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa, this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente)
                 .GroupBy(this.Query.CdAtivoBolsa);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsaCollection com os campos CdAtivoBolsa.Substring(1, 4), Quantidade.Sum().
        /// Agrupado por CdAtivoBolsa.Substring(1, 4).
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoDescobertoEnquadra(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa.Substring(1, 4), this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.LessThan(0))
                 .GroupBy(this.Query.CdAtivoBolsa.Substring(1, 4));

            this.Query.Load();
        }

        /// <summary>
        /// Método de inserção em PosicaoBolsa, a partir de PosicaoBolsaHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InserePosicaoBolsaHistorico(int idCliente, DateTime dataHistorico)
        {
            #region Copia de posicaoBolsaHistorico para PosicaoBolsa
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoBolsa ON ");
            sqlBuilder.Append("INSERT INTO PosicaoBolsa (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoBolsaHistorico WHERE DataHistorico = " + "'" + dataHistorico.ToString("yyyyMMdd") + "' AND IdCliente = " + idCliente;
            int count = 0;

            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            int columnCount = posicaoBolsaHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoBolsa in posicaoBolsaHistorico.es.Meta.Columns)
            {
                count++;

                if (colPosicaoBolsa.Name == PosicaoBolsaHistoricoMetadata.ColumnNames.DataHistorico)
                    continue;

                //Insert
                sqlBuilder.Append(colPosicaoBolsa.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoBolsa.Name;

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoBolsa OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }


        /// <summary>
        /// Método de inserção em PosicaoBolsa, a partir de PosicaoBolsaAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataAbertura"></param>
        public void InserePosicaoBolsaAbertura(int idCliente, DateTime dataAbertura)
        {
            #region Copia de posicaoBolsaHistorico para PosicaoBolsa
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoBolsa ON ");
            sqlBuilder.Append("INSERT INTO PosicaoBolsa (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoBolsaAbertura WHERE DataHistorico = " + "'" + dataAbertura.ToString("yyyyMMdd") + "' AND IdCliente = " + idCliente;
            int count = 0;

            PosicaoBolsaAbertura posicaoBolsaAbertura = new PosicaoBolsaAbertura();
            int columnCount = posicaoBolsaAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoBolsa in posicaoBolsaAbertura.es.Meta.Columns)
            {
                count++;

                if (colPosicaoBolsa.Name == PosicaoBolsaAberturaMetadata.ColumnNames.DataHistorico)
                    continue;

                //Insert
                sqlBuilder.Append(colPosicaoBolsa.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoBolsa.Name;

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoBolsa OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsaCollection com os campos IdPosicao, CdAtivoBolsa, Quantidade,
        /// PUCustoLiquido, IdCliente, PUCusto, CdAtivoBolsa, IdAgente, PUMercado, ValorMercado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoBolsa(int idCliente, DateTime dataVencimento)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.CdAtivoBolsa, this.Query.Quantidade,
                         this.Query.PUCustoLiquido, this.Query.IdCliente, this.Query.PUCusto,
                         this.Query.CdAtivoBolsa, this.Query.IdAgente, this.Query.PUMercado,
                         this.Query.ValorMercado)
                 .Where(this.Query.DataVencimento.Equal(dataVencimento),
                        this.Query.IdCliente == idCliente,
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                        this.Query.Quantidade != 0);

            bool retorno = this.Query.Load();
            
            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsaCollection com os campos IdPosicao, CdAtivoBolsa, Quantidade,]
        /// PUCustoLiquido, ValorMercado, ValorCustoLiquido, ResultadoRealizar, QuantidadeAbertura,
        /// PUMercado.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoBolsa(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.CdAtivoBolsa, this.Query.Quantidade,
                         this.Query.PUCustoLiquido, this.Query.ValorMercado, this.Query.ValorCustoLiquido,
                         this.Query.ResultadoRealizar, this.Query.PUMercado, this.Query.QuantidadeBloqueada)
                 .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();            
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsaCollection com todos os campos de PosicaoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoBolsaCompleta(int idCliente)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente);
            this.Query.Load();         
        }

        /// <summary>
        /// Deleta todas as posições do idCliente.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaPosicaoBolsa(int idCliente)
        {
            this.Query
                    .Select(this.Query.IdPosicao)
                    .Where(this.Query.IdCliente == idCliente);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas as posições do idCliente de direito de subscrição com vencimento na data.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaDireitoSubscricaoVencido(int idCliente, DateTime dataVencimento)
        {
            this.Query
                    .Select(this.Query.IdPosicao)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataVencimento.Equal(dataVencimento),
                           this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
        
        /// <summary>
        /// Carrega o objeto PosicaoBolsaCollection com os campos CdAtivoBolsa, CdAtivoBolsa.Count(), Quantidade.Sum()
        /// ,custo.Sum(), custoLiquido.Sum().
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaPosicaoConsolidado(int idCliente)
        {
            string quantidade = PosicaoBolsaMetadata.ColumnNames.Quantidade;
            string puCusto = PosicaoBolsaMetadata.ColumnNames.PUCusto;
            string puCustoLiquido = PosicaoBolsaMetadata.ColumnNames.PUCustoLiquido;
            StringBuilder colunaQuantidade = new StringBuilder();
            colunaQuantidade = colunaQuantidade.Append("<SUM(ABS(")
                            .Append(quantidade)
                            .Append(")) AS QuantidadeSum>");
            StringBuilder colunaCusto = new StringBuilder();
            colunaCusto = colunaCusto.Append("<SUM(ABS(")
                            .Append(quantidade)
                            .Append("*")
                            .Append(puCusto)
                            .Append(")) AS CustoSum>");
            StringBuilder colunaCustoLiquido = new StringBuilder();
            colunaCustoLiquido = colunaCustoLiquido.Append("<SUM(ABS(")
                            .Append(quantidade)
                            .Append("*")
                            .Append(puCustoLiquido)
                            .Append(")) AS CustoLiquidoSum>");

            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa, this.Query.CdAtivoBolsa.Count().As("CdAtivoBolsaCount"),
                         colunaQuantidade.ToString(),
                         colunaCusto.ToString(), colunaCustoLiquido.ToString())
                 .Where(this.Query.IdCliente == idCliente)
                 .GroupBy(this.Query.CdAtivoBolsa);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsaCollection com os campos IdPosicao, Quantidade, PUCusto, 
        /// PUCustoLiquido, ValorCustoLiquido.
        /// Filtra apenas posição com quantidade positiva.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaPosicaoCusto(int idCliente, String cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.Quantidade, this.Query.PUCusto, this.Query.PUCustoLiquido,
                         this.Query.ValorCustoLiquido)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                        this.Query.Quantidade > 0);

            bool retorno = this.Query.Load();
            
            return retorno;
        }

        /// <summary>
        /// Carrega o objeto PosicaoBolsaCollection com os campos CdAtivoBolsa, TipoMercado, Quantidade.Sum, PUMercado.Avg, PUCustoLiquido.Avg, 
        /// ValorMercado.Sum, ValorCustoLiquido.Sum.
        /// Group By CdAtivoBolsa
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoBolsaAgrupado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsa, 
                         this.Query.TipoMercado,   
                         this.Query.Quantidade.Sum(),
                         this.Query.PUMercado.Avg(),
                         this.Query.PUCustoLiquido.Avg(),
                         this.Query.ValorMercado.Sum(),
                         this.Query.ValorCustoLiquido.Sum())
                 .Where(this.Query.IdCliente == idCliente)
                 .GroupBy(this.Query.CdAtivoBolsa, this.Query.TipoMercado)
                 .OrderBy(this.Query.TipoMercado.Descending, this.Query.CdAtivoBolsa.Ascending); ;

            bool retorno = this.Query.Load();
                        
            return retorno;
        }
		
		/// <summary>
        /// 
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="idAgente"></param>
        /// <param name="idCliente"></param>
        /// <param name="incluiBloqueio"></param>
        /// <param name="incluiEmprestimo"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadeDisponivel(DateTime dataReferencia, string cdAtivoBolsa, int? idAgente, int idCliente, bool incluiBloqueio, bool incluiEmprestimo)
        {
            List<string> lstTipoOperacaoEntrada = new List<string>();
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.Compra);
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.CompraDaytrade);
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.Deposito);
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.IngressoAtivoImpactoCota);
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.IngressoAtivoImpactoQtde);

            List<string> lstTipoOperacaoSaida = new List<string>();
            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.Venda);
            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.VendaDaytrade);
            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.Retirada);
            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.RetiradaAtivoImpactoCota);
            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.RetiradaAtivoImpactoQtde);

            decimal qtdePosicao = 0;
            decimal qtdeBloqueada = 0;
            PosicaoBolsaAberturaCollection posicaoBolsaAberturaColl = new PosicaoBolsaAberturaCollection();
            posicaoBolsaAberturaColl.Query.Select(posicaoBolsaAberturaColl.Query.Quantidade.Sum(), posicaoBolsaAberturaColl.Query.QuantidadeBloqueada.Sum());
            posicaoBolsaAberturaColl.Query.Where(posicaoBolsaAberturaColl.Query.DataHistorico.Equal(dataReferencia) &
                                                 posicaoBolsaAberturaColl.Query.CdAtivoBolsa.Equal(cdAtivoBolsa) &
                                                 posicaoBolsaAberturaColl.Query.IdCliente.Equal(idCliente));

            if (idAgente.HasValue)
                posicaoBolsaAberturaColl.Query.Where(posicaoBolsaAberturaColl.Query.IdAgente.Equal(idAgente));
                                                 
            posicaoBolsaAberturaColl.Query.GroupBy(posicaoBolsaAberturaColl.Query.IdCliente, posicaoBolsaAberturaColl.Query.CdAtivoBolsa);

            if (posicaoBolsaAberturaColl.Query.Load())
            {
                qtdePosicao = posicaoBolsaAberturaColl[0].Quantidade.GetValueOrDefault(0);
                qtdeBloqueada = posicaoBolsaAberturaColl[0].QuantidadeBloqueada.GetValueOrDefault(0);
            }

            decimal qtdeOperacaoEntrada = 0;
            OperacaoBolsaCollection OperacaoBolsaEntradaColl = new OperacaoBolsaCollection();
            OperacaoBolsaEntradaColl.Query.Select(OperacaoBolsaEntradaColl.Query.Quantidade.Sum());
            OperacaoBolsaEntradaColl.Query.Where(OperacaoBolsaEntradaColl.Query.DataOperacao.Equal(dataReferencia) &
                                                 OperacaoBolsaEntradaColl.Query.CdAtivoBolsa.Equal(cdAtivoBolsa) &
                                                 OperacaoBolsaEntradaColl.Query.IdCliente.Equal(idCliente) &
                                                 OperacaoBolsaEntradaColl.Query.TipoOperacao.In(lstTipoOperacaoEntrada.ToArray()));

            if (idAgente.HasValue)
                OperacaoBolsaEntradaColl.Query.Where(OperacaoBolsaEntradaColl.Query.IdAgenteCorretora.Equal(idAgente));

            OperacaoBolsaEntradaColl.Query.GroupBy(OperacaoBolsaEntradaColl.Query.IdCliente,
                                                   OperacaoBolsaEntradaColl.Query.CdAtivoBolsa);

            if (OperacaoBolsaEntradaColl.Query.Load())
                qtdeOperacaoEntrada = OperacaoBolsaEntradaColl[0].Quantidade.GetValueOrDefault(0);

            decimal qtdeOperacaoSaida = 0;
            OperacaoBolsaCollection OperacaoBolsaSaidaColl = new OperacaoBolsaCollection();
            OperacaoBolsaSaidaColl.Query.Select(OperacaoBolsaSaidaColl.Query.Quantidade.Sum());
            OperacaoBolsaSaidaColl.Query.Where(OperacaoBolsaSaidaColl.Query.DataOperacao.Equal(dataReferencia) &
                                               OperacaoBolsaSaidaColl.Query.CdAtivoBolsa.Equal(cdAtivoBolsa) &
                                               OperacaoBolsaSaidaColl.Query.IdCliente.Equal(idCliente) &
                                               OperacaoBolsaSaidaColl.Query.TipoOperacao.In(lstTipoOperacaoSaida.ToArray()));

            if (idAgente.HasValue)
                OperacaoBolsaSaidaColl.Query.Where(OperacaoBolsaSaidaColl.Query.IdAgenteCorretora.Equal(idAgente));

            OperacaoBolsaSaidaColl.Query.GroupBy(OperacaoBolsaSaidaColl.Query.IdCliente,
                                                 OperacaoBolsaSaidaColl.Query.CdAtivoBolsa);

            if (OperacaoBolsaSaidaColl.Query.Load())
                qtdeOperacaoSaida = OperacaoBolsaSaidaColl[0].Quantidade.GetValueOrDefault(0);

            #region Seleciona bloqueios
            decimal operacaoBloqueio = 0, operacaoDesbloqueio = 0;
            if (incluiBloqueio)
            {                
                BloqueioBolsaCollection bloqueioBolsaCollection = new BloqueioBolsaCollection();
                bloqueioBolsaCollection.Query.Select(bloqueioBolsaCollection.Query.Quantidade.Sum());
                bloqueioBolsaCollection.Query.Where(bloqueioBolsaCollection.Query.IdCliente.Equal(idCliente) &
                                                    bloqueioBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa) &
                                                    bloqueioBolsaCollection.Query.DataOperacao.Equal(dataReferencia) &
                                                    bloqueioBolsaCollection.Query.TipoOperacao.Equal(Financial.Bolsa.Enums.TipoOperacaoBloqueio.Bloqueio));

                if (idAgente.HasValue)
                    bloqueioBolsaCollection.Query.Where(bloqueioBolsaCollection.Query.IdAgente.Equal(idAgente));

                if (bloqueioBolsaCollection.Query.Load())
                    operacaoBloqueio = bloqueioBolsaCollection[0].Quantidade.GetValueOrDefault(0);

                bloqueioBolsaCollection.Query.Select(bloqueioBolsaCollection.Query.Quantidade.Sum());
                bloqueioBolsaCollection.Query.Where(bloqueioBolsaCollection.Query.IdCliente.Equal(idCliente) &
                                                    bloqueioBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa) &
                                                    bloqueioBolsaCollection.Query.DataOperacao.Equal(dataReferencia) &
                                                    bloqueioBolsaCollection.Query.TipoOperacao.Equal(Financial.Bolsa.Enums.TipoOperacaoBloqueio.Desbloqueio));

                if (idAgente.HasValue)
                    bloqueioBolsaCollection.Query.Where(bloqueioBolsaCollection.Query.IdAgente.Equal(idAgente));

                if (bloqueioBolsaCollection.Query.Load())
                    operacaoDesbloqueio = bloqueioBolsaCollection[0].Quantidade.GetValueOrDefault(0);
            }
            #endregion

            decimal qtdeEmprestimo = 0;
            if (incluiEmprestimo)
            {
                PosicaoEmprestimoBolsaCollection posicaoColl = new PosicaoEmprestimoBolsaCollection();
                qtdeEmprestimo = posicaoColl.RetornaQuantidadeDisponivel(dataReferencia, cdAtivoBolsa, idAgente, idCliente);
            }

            return qtdePosicao + qtdeOperacaoEntrada - qtdeOperacaoSaida - qtdeBloqueada - operacaoBloqueio + operacaoDesbloqueio + qtdeEmprestimo;
        }		
    }
}
