﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Exceptions;
using System.IO;
using Financial.Common;
using Financial.Investidor;
using Financial.Interfaces.Import.Bolsa.Exceptions;
using Financial.Interfaces.Import.Bolsa;
using Financial.Tributo.Custom;
using Financial.Investidor.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Tributo.Enums;

namespace Financial.Bolsa {
    public partial class LiquidacaoEmprestimoBolsa : esLiquidacaoEmprestimoBolsa 
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// throws FatorCotacaoNaoCadastradoException se fator não estiver cadastrado
        public void ProcessaLiquidacaoEmprestimo(int idCliente, DateTime data) 
        {
            // Collections que serão usadas para salvar tudo de uma vez no final do loop            
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollectionDeletar = new PosicaoEmprestimoBolsaCollection();
            //
            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
            liquidacaoEmprestimoBolsaCollection.BuscaLiquidacaoEmprestimoBolsa(idCliente, data);

            DateTime dataLiquidacao = new DateTime();
            int idContaDefault = 0;
            if (liquidacaoEmprestimoBolsaCollection.Count > 0)
            {
                dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                //
            }

            for (int i = 0; i < liquidacaoEmprestimoBolsaCollection.Count; i++) 
            {
                LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = liquidacaoEmprestimoBolsaCollection[i];

                #region Valores de LiquidacaoEmprestimoBolsa
                int? numeroContrato = liquidacaoEmprestimoBolsa.NumeroContrato;
                byte tipoLiquidacao = liquidacaoEmprestimoBolsa.TipoLiquidacao.Value;
                int idPosicao = liquidacaoEmprestimoBolsa.IdPosicao.Value;
                int idAgente = liquidacaoEmprestimoBolsa.IdAgente.Value;
                string cdAtivoBolsa = liquidacaoEmprestimoBolsa.CdAtivoBolsa;
                decimal quantidadeLiquidacao = liquidacaoEmprestimoBolsa.Quantidade.Value;
                decimal valorLiquidacao = liquidacaoEmprestimoBolsa.ValorLiquidacao.Value;
                decimal valorIR = 0;

                if (liquidacaoEmprestimoBolsa.ValorIR.HasValue)
                {
                    valorIR = Math.Abs(liquidacaoEmprestimoBolsa.ValorIR.Value);
                }

                int? idOperacao = liquidacaoEmprestimoBolsa.IdOperacao;
                byte fonte = liquidacaoEmprestimoBolsa.Fonte.Value;
                #endregion

                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                if (numeroContrato.HasValue && numeroContrato.Value != 0)
                {
                    //Tenta buscar pelo Nr Contrato                    
                    posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.NumeroContrato.Equal(numeroContrato.Value));
                    posicaoEmprestimoBolsaCollection.Query.Load();
                }
                if (posicaoEmprestimoBolsaCollection.Count == 0)
                {
                    posicaoEmprestimoBolsaCollection.QueryReset();
                    posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdPosicao.Equal(idPosicao));
                    posicaoEmprestimoBolsaCollection.Query.Load();

                    if (posicaoEmprestimoBolsaCollection.Count == 0)
                    {
                        if (idOperacao.HasValue)
                        {
                            posicaoEmprestimoBolsaCollection.QueryReset();
                            posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdOperacao.Equal(idOperacao.Value));
                            posicaoEmprestimoBolsaCollection.Query.Load();

                            if (posicaoEmprestimoBolsaCollection.Count == 0)
                            {
                                throw new Exception("A liquidação de " + cdAtivoBolsa + " - " + quantidadeLiquidacao.ToString() + " perdeu o vínculo original com a posição. Favor relançar a liquidação.");
                            }
                        }
                    }                    
                }

                decimal quantidadeResidual = quantidadeLiquidacao;
                decimal valorLiquidacaoCalculado = 0;
                foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
                {
                    byte pontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo.Value;
                    decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;
                    decimal puMercado = posicaoEmprestimoBolsa.PUMercado.Value;
                    decimal valorMercado = posicaoEmprestimoBolsa.ValorMercado.Value;
                    decimal valorCorrigidoJuros = posicaoEmprestimoBolsa.ValorCorrigidoJuros.Value;
                    decimal valorCorrigidoComissao = posicaoEmprestimoBolsa.ValorCorrigidoComissao.Value;
                    decimal valorCorrigidoCBLC = posicaoEmprestimoBolsa.ValorCorrigidoCBLC.Value;
                    decimal quantidadePosicao = posicaoEmprestimoBolsa.Quantidade.Value;
                    decimal valorBase = posicaoEmprestimoBolsa.ValorBase.Value;

                    decimal quantidade = 0;
                    if (quantidadeResidual > quantidadePosicao)
                    {
                        quantidade = quantidadePosicao;
                        quantidadeResidual -= quantidade;
                    }
                    else
                    {
                        quantidade = quantidadeResidual;
                        quantidadeResidual = 0;
                    }

                    #region Cálculo de juros, comissao, CBLC em caso de os valores não virem por arquivo
                    //Calculo os valores proporcionais da antecipação
                    decimal valorBaseAntecipado = Utilitario.Truncate(valorBase * quantidade / quantidadePosicao, 2);
                    decimal valorJurosLiquidacao = Utilitario.Truncate(valorCorrigidoJuros * quantidade / quantidadePosicao, 2);
                    decimal valorComissaoLiquidacao = Utilitario.Truncate(valorCorrigidoComissao * quantidade / quantidadePosicao, 2);
                    decimal valorCBLCLiquidacao = Utilitario.Truncate(valorCorrigidoCBLC * quantidade / quantidadePosicao, 2);

                    decimal valorMinimoCBLC = 0;
                    if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador())
                    {
                        if (posicaoEmprestimoBolsa.IsTipoEmprestimoVoluntario())
                        {
                            TabelaCustosBolsa tabelaCustosBolsa = new TabelaCustosBolsa();
                            tabelaCustosBolsa.BuscaTabelaCustosBolsa(data, (Int16)TipoTaxaBolsa.EmprestimoBolsa.Voluntario);

                            if (tabelaCustosBolsa.MinimoCBLC.HasValue)
                            {
                                valorMinimoCBLC = tabelaCustosBolsa.MinimoCBLC.Value;
                            }
                        }
                    }

                    if (valorCBLCLiquidacao < valorMinimoCBLC)
                    {
                        valorCBLCLiquidacao = valorMinimoCBLC;
                    }

                    // Se vier integrado pelo arquivo DBTL (ou se for vencimento), já existirão os valores da liquidação 
                    // (juros, comissao, CBCL). Caso contrário, o valorLiquidacao será calculado aqui abaixo:
                    if (valorLiquidacao == 0 && fonte == (byte)FonteLiquidacaoEmprestimoBolsa.Manual)
                    {
                        if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador())
                        {
                            valorLiquidacaoCalculado += valorJurosLiquidacao - valorBaseAntecipado;
                        }
                        else
                        {
                            valorLiquidacaoCalculado += (valorJurosLiquidacao + valorComissaoLiquidacao + valorCBLCLiquidacao) - valorBaseAntecipado * 3M;
                        }
                    }
                    #endregion

                    int origemOperacao = OrigemOperacaoBolsa.LiquidacaoEmprestimo;
                    int origemLiquidacao = OrigemLancamentoLiquidacao.Bolsa.AntecipacaoEmprestimo;
                    string tipoOperacaoBolsa = null;
                    if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador())
                    {
                        tipoOperacaoBolsa = TipoOperacaoBolsa.Deposito;
                    }
                    else if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador())
                    {
                        tipoOperacaoBolsa = TipoOperacaoBolsa.Retirada;
                    }

                    Cliente cliente = new Cliente();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(cliente.Query.ApuraGanhoRV);
                    cliente.LoadByPrimaryKey(campos, idCliente);
                    if (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraTodas ||
                        (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.NaoApura))
                    {
                        #region Insercao OperacaoBolsa
                        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                        operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                        operacaoBolsa.TipoOperacao = tipoOperacaoBolsa;
                        operacaoBolsa.Data = data;
                        operacaoBolsa.Pu = puLiquidoOriginal;
                        operacaoBolsa.PULiquido = puLiquidoOriginal;
                        operacaoBolsa.Quantidade = quantidade;
                        operacaoBolsa.DataLiquidacao = data;
                        operacaoBolsa.Valor = Math.Round(quantidade * puLiquidoOriginal, 2);
                        operacaoBolsa.Origem = (byte)origemOperacao;
                        operacaoBolsa.IdLocal = (Int16)LocalFeriadoFixo.Bovespa;
                        operacaoBolsa.IdCliente = idCliente;
                        operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        operacaoBolsa.IdAgenteCorretora = idAgente;
                        operacaoBolsa.IdAgenteLiquidacao = idAgente;
                        operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                        operacaoBolsa.CalculaDespesas = "N";
                        operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                        operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                        operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                        operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                        operacaoBolsa.DataOperacao = data;

                        operacaoBolsaCollection.AttachEntity(operacaoBolsa);
                        #endregion
                    }
                    
                    #region Update PosicaoEmprestimoBolsa (ou deleção na posicaoEmprestimoBolsaCollectionDeletar)
                    // Carrega a PosicaoEmprestmoBolsa                
                    PosicaoEmprestimoBolsa posicaoEmprestimoBolsaAux = new PosicaoEmprestimoBolsa();
                    posicaoEmprestimoBolsaAux.LoadByPrimaryKey(posicaoEmprestimoBolsa.IdPosicao.Value);
                    //
                    posicaoEmprestimoBolsaAux.Quantidade -= quantidade;
                    posicaoEmprestimoBolsaAux.ValorBase -= valorBaseAntecipado;
                    posicaoEmprestimoBolsaAux.ValorCorrigidoJuros -= valorJurosLiquidacao;
                    posicaoEmprestimoBolsaAux.ValorCorrigidoComissao -= valorComissaoLiquidacao;
                    posicaoEmprestimoBolsaAux.ValorCorrigidoCBLC -= valorCBLCLiquidacao;

                    // Adiciona na collection
                    if (posicaoEmprestimoBolsaAux.Quantidade == 0)
                    {
                        PosicaoEmprestimoBolsa posicaoChecar = posicaoEmprestimoBolsaCollectionDeletar.FindByPrimaryKey(posicaoEmprestimoBolsaAux.IdPosicao.Value);
                        if (posicaoChecar == null)
                        {
                            posicaoEmprestimoBolsaCollectionDeletar.AttachEntity(posicaoEmprestimoBolsaAux);
                        }
                    }
                    else
                    {
                        posicaoEmprestimoBolsaAux.Save();
                    }
                    #endregion

                    //Trata Inserção na Liquidacao, apenas no último step do loop
                    if (quantidadeResidual == 0)
                    {
                        if (valorLiquidacao == 0)
                        {
                            valorLiquidacao = valorLiquidacaoCalculado;
                        }

                        #region Insercao Liquidacao para valor da Liquidacao e IR
                        Liquidacao liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataLiquidacao;
                        StringBuilder descricao = new StringBuilder();

                        if (tipoLiquidacao == (byte)TipoLiquidacaoEmprestimoBolsa.Vencimento)
                        {
                            if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador())
                            {
                                descricao.Append("Liquidação (Vencimento) Juros Empréstimo - ").Append((int)quantidade).Append(" ").Append(cdAtivoBolsa);
                            }
                            else
                            {
                                descricao.Append("Liquidação (Vencimento) Custos Empréstimo (Juros + Taxas) - ").Append((int)quantidade).Append(" ").Append(cdAtivoBolsa);
                            }
                        }
                        else
                        {
                            if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador())
                            {
                                descricao.Append("Liquidação Juros Empréstimo - ").Append((int)quantidade).Append(" ").Append(cdAtivoBolsa);
                            }
                            else
                            {
                                descricao.Append("Liquidação Custos Empréstimo (Juros + Taxas) - ").Append((int)quantidade).Append(" ").Append(cdAtivoBolsa);
                            }
                        }

                        //Calculo do IR
                        ClienteBolsa clienteBolsa = new ClienteBolsa();
                        campos = new List<esQueryItem>();
                        campos.Add(clienteBolsa.Query.IsentoIR);
                        if (clienteBolsa.LoadByPrimaryKey(campos, idCliente))
                        {
                            if (clienteBolsa.IsentoIR == "N" && valorIR == 0)
                            {
                                if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaDoador())
                                {
                                    CalculoTributo calculoTributo = new CalculoTributo();
                                    decimal aliquotaIR = calculoTributo.RetornaAliquotaIRLongoPrazo(data, dataLiquidacao);
                                    valorIR = Utilitario.Truncate(valorLiquidacao * aliquotaIR / 100, 2);
                                }
                            }
                        }
                        //

                        if (posicaoEmprestimoBolsa.IsPontaEmprestimoBolsaTomador())
                        {
                            valorLiquidacao = valorLiquidacao * -1;
                        }

                        liquidacao.Descricao = descricao.ToString();
                        liquidacao.Valor = valorLiquidacao;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = origemLiquidacao;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdAgente = idAgente;
                        liquidacao.IdCliente = idCliente;
                        liquidacao.IdentificadorOrigem = idOperacao;
                        liquidacao.IdConta = idContaDefault;

                        liquidacaoCollection.AttachEntity(liquidacao);

                        if (valorIR != 0)
                        {
                            liquidacao = new Liquidacao();
                            liquidacao.DataLancamento = data;
                            liquidacao.DataVencimento = dataLiquidacao;
                            liquidacao.Descricao = "IR s/ " + descricao.ToString();
                            liquidacao.Valor = valorIR * -1;
                            liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                            liquidacao.Origem = origemLiquidacao;
                            liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                            liquidacao.IdAgente = idAgente;
                            liquidacao.IdCliente = idCliente;
                            liquidacao.IdentificadorOrigem = idOperacao;
                            liquidacao.IdConta = idContaDefault;

                            liquidacaoCollection.AttachEntity(liquidacao);
                        }
                        #endregion
                    }
                    //
                }

            }

            // Update das Collections
            #region Update das Collections
            // Insert
            operacaoBolsaCollection.Save();
            // Insert
            liquidacaoCollection.Save();
            // Delete
            posicaoEmprestimoBolsaCollectionDeletar.MarkAllAsDeleted();
            posicaoEmprestimoBolsaCollectionDeletar.Save();
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbtlCollection"></param>
        /// <param name="data"></param>
        /// throws AtivoNaoCadastradoException se cdAtivoBolsa não existir
        public void CarregaLiquidacaoEmprestimoDbtl(DbtlCollection dbtlCollection, DateTime data) 
        {
            #region GetIdAgente do Arquivo Dbtl
            int codigoBovespaAgente = dbtlCollection.CollectionDbtl[0].CodigoAgente.Value;
            AgenteMercado agenteMercado = new AgenteMercado();
            // Da exceção se Agente Mercado não existir
            int idAgente = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaAgente);
            #endregion

            Dbtl dbtl = new Dbtl();
            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollectionDelecao = new LiquidacaoEmprestimoBolsaCollection();

            // Deleta as as Liquidações do dia
            liquidacaoEmprestimoBolsaCollectionDelecao.DeletaLiquidacaoEmprestimoBolsa(data, FonteLiquidacaoEmprestimoBolsa.ArquivoDBTL);

            // Collection de Dbtl somente com tipoRegistro = 1
            DbtlCollection dbtlCollectionInsercao = new DbtlCollection();
            dbtlCollectionInsercao.CollectionDbtl = dbtlCollection.CollectionDbtl.FindAll(dbtl.FilterDbtlByTipoRegistroRelacaoLiquidacoesClienteContrato);
            //
            #region Inserção dos contratos liquidados no dia (antecipação e vencimento)
            for (int i = 0; i < dbtlCollectionInsercao.CollectionDbtl.Count; i++) {
                dbtl = dbtlCollectionInsercao.CollectionDbtl[i];

                #region Copia informações para LiquidacaoEmprestimoBolsa
                #region Dados do DBTL
                // Só inclui os clientes que existirem na Base
                int codigoClienteBovespa = dbtl.CodigoCliente.Value;
                CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);

                if (idClienteExiste) {
                    int idCliente = codigoClienteAgente.IdCliente.Value;

                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(idCliente);

                    if (cliente.IsAtivo) //Só importa para clientes ativos!
                    {
                        //Só importa para clientes com dataDia anterior à data passada ou data igual e status != fechado
                        if ((DateTime.Compare(cliente.DataDia.Value, data) < 0) ||
                                   (DateTime.Compare(cliente.DataDia.Value, data) == 0 && cliente.Status.Value != (byte)StatusCliente.Divulgado)) {
                            Int64 quantidade = dbtl.Quantidade.Value;
                            Int64 quantidadeInadimplente = dbtl.QuantidadeInadimplente.Value;
                            Int64 quantidadeLiquidada = quantidade - quantidadeInadimplente;
                            int numeroContrato = dbtl.NumeroContrato.Value;

                            //Pega IdPosicao da PosicaoEmprestimoBolsa referente ao nr do contrato
                            int? idPosicao = null;
                            DateTime? dataVencimento = null;
                            string cdAtivoBolsa = "";
                            if (DateTime.Compare(cliente.DataDia.Value, data) == 0) {
                                PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection();
                                posicaoEmprestimoBolsaAberturaCollection.Query.Select(posicaoEmprestimoBolsaAberturaCollection.Query.IdPosicao,
                                                                                posicaoEmprestimoBolsaAberturaCollection.Query.CdAtivoBolsa,
                                                                                posicaoEmprestimoBolsaAberturaCollection.Query.DataVencimento);
                                posicaoEmprestimoBolsaAberturaCollection.Query.Where(posicaoEmprestimoBolsaAberturaCollection.Query.NumeroContrato.Equal(numeroContrato),
                                                                                posicaoEmprestimoBolsaAberturaCollection.Query.DataHistorico.Equal(data),
                                                                                posicaoEmprestimoBolsaAberturaCollection.Query.IdCliente.Equal(idCliente));
                                if (posicaoEmprestimoBolsaAberturaCollection.Query.Load()) {
                                    idPosicao = posicaoEmprestimoBolsaAberturaCollection[0].IdPosicao.Value;
                                    cdAtivoBolsa = posicaoEmprestimoBolsaAberturaCollection[0].CdAtivoBolsa;
                                    dataVencimento = posicaoEmprestimoBolsaAberturaCollection[0].DataVencimento.Value;
                                }
                            }
                            else {
                                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                                posicaoEmprestimoBolsaCollection.Query.Select(posicaoEmprestimoBolsaCollection.Query.IdPosicao,
                                                                                posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa,
                                                                                posicaoEmprestimoBolsaCollection.Query.DataVencimento);
                                posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.NumeroContrato.Equal(numeroContrato),
                                                                             posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCliente));
                                if (posicaoEmprestimoBolsaCollection.Query.Load()) {
                                    idPosicao = posicaoEmprestimoBolsaCollection[0].IdPosicao.Value;
                                    cdAtivoBolsa = posicaoEmprestimoBolsaCollection[0].CdAtivoBolsa;
                                    dataVencimento = posicaoEmprestimoBolsaCollection[0].DataVencimento.Value;

                                }
                            }

                            if (idPosicao.HasValue) {
                                byte tipoLiquidacaoEmprestimoBolsa = 0;

                                if (DateTime.Compare(dataVencimento.Value, data) > 0) {
                                    tipoLiquidacaoEmprestimoBolsa = (byte)TipoLiquidacaoEmprestimoBolsa.Antecipacao;
                                }
                                else {
                                    tipoLiquidacaoEmprestimoBolsa = (byte)TipoLiquidacaoEmprestimoBolsa.Vencimento;
                                }

                                int? idOperacao = null;
                                #region Tenta buscar o IdOperacao vinculado em OperacaoEmprestimoBolsa, se não achar, idOperacao = null
                                OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
                                operacaoEmprestimoBolsaCollection.Query.Select(operacaoEmprestimoBolsaCollection.Query.IdOperacao);
                                operacaoEmprestimoBolsaCollection.Query.Where(operacaoEmprestimoBolsaCollection.Query.NumeroContrato.Equal(numeroContrato));
                                operacaoEmprestimoBolsaCollection.Query.Load();

                                if (operacaoEmprestimoBolsaCollection.Count > 0) {
                                    idOperacao = operacaoEmprestimoBolsaCollection[0].IdOperacao.Value;
                                }
                                #endregion

                                #region Copia para LiquidacaoEmprestimoBolsa
                                LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
                                liquidacaoEmprestimoBolsa.IdPosicao = idPosicao.Value;
                                liquidacaoEmprestimoBolsa.Data = data;
                                liquidacaoEmprestimoBolsa.TipoLiquidacao = tipoLiquidacaoEmprestimoBolsa;
                                liquidacaoEmprestimoBolsa.IdCliente = idCliente;
                                liquidacaoEmprestimoBolsa.IdAgente = idAgente;
                                liquidacaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                                liquidacaoEmprestimoBolsa.Quantidade = quantidadeLiquidada;
                                liquidacaoEmprestimoBolsa.Fonte = (byte)FonteLiquidacaoEmprestimoBolsa.ArquivoDBTL;
                                liquidacaoEmprestimoBolsa.IdOperacao = idOperacao;
                                liquidacaoEmprestimoBolsa.NumeroContrato = numeroContrato;
                                liquidacaoEmprestimoBolsa.Valor = 0;
                                liquidacaoEmprestimoBolsa.ValorLiquidacao = 0;
                                liquidacaoEmprestimoBolsa.ValorIR = 0;
                                #endregion

                                //Inclui na collection
                                liquidacaoEmprestimoBolsaCollection.AttachEntity(liquidacaoEmprestimoBolsa);
                            }
                        }
                    }
                }
                #endregion
                #endregion
            }

            liquidacaoEmprestimoBolsaCollection.Save();
            #endregion

            #region Atualização dos valores de liquidação e IR referentes aos contratos liquidados.
            //Carrega a collection com as liquidações (antecipações e vencimentos) da data.
            liquidacaoEmprestimoBolsaCollection.BuscaLiquidacaoEmprestimoBolsa(data);

            // Collection de Dbtl somente com tipoRegistro = 3
            DbtlCollection dbtlCollectionAtualizacao = new DbtlCollection();
            dbtlCollectionAtualizacao.CollectionDbtl = dbtlCollection.CollectionDbtl.FindAll(dbtl.FilterDbtlByTipoRegistroDetalhado);

            for (int i = 0; i < dbtlCollectionAtualizacao.CollectionDbtl.Count; i++) {
                dbtl = dbtlCollectionAtualizacao.CollectionDbtl[i];

                #region Copia informações para LiquidacaoEmprestimoBolsa
                // Só inclui os clientes que existirem na Base
                int codigoClienteBovespa = dbtl.CodigoCliente.Value;
                CodigoClienteAgente codigoClienteAgente = new CodigoClienteAgente();
                bool idClienteExiste = codigoClienteAgente.BuscaIdClienteInternoBovespa(codigoBovespaAgente, codigoClienteBovespa);

                if (idClienteExiste) {
                    int idCliente = codigoClienteAgente.IdCliente.Value;
                    int numeroContrato = dbtl.NumeroContrato.Value;
                    string descricaoLancamento = dbtl.Descricao;
                    decimal valorLancamento = dbtl.ValorLancamento.Value;
                    //           

                    #region Atualiza a Collection de LiquidacaoEmprestimoBolsa com os valores de Liquidação/IR
                    liquidacaoEmprestimoBolsaCollection.Filter = "";

                    StringBuilder filtro = new StringBuilder();
                    filtro.Append(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente).Append("= ").Append(idCliente);
                    filtro.Append(" AND ");
                    filtro.Append(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Data).Append("= '").Append(data).Append("'");
                    filtro.Append(" AND ");
                    filtro.Append(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato).Append("= ").Append(numeroContrato);
                    liquidacaoEmprestimoBolsaCollection.Filter = filtro.ToString();

                    if (liquidacaoEmprestimoBolsaCollection.Count > 0 && liquidacaoEmprestimoBolsaCollection[0].ValorLiquidacao.HasValue) {
                        if (descricaoLancamento == "BRUTO TOMADOR" || descricaoLancamento == "REMUNERACAO BRUTA DO DOADOR") {
                            liquidacaoEmprestimoBolsaCollection[0].ValorLiquidacao += valorLancamento;
                        }
                        else if (descricaoLancamento == "VALOR DO IR SOBRE REMUNERACAO DO DOADOR") {
                            liquidacaoEmprestimoBolsaCollection[0].ValorIR += valorLancamento;
                        }
                    }
                    #endregion

                }
                #endregion
            }

            // Limpa o Filtro
            liquidacaoEmprestimoBolsaCollection.Filter = "";
            // Update dos Valores de IR
            liquidacaoEmprestimoBolsaCollection.Save();
            #endregion
        }

        /// <summary>
        /// Integra as liquidações de BTC do dia do Sinacor.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void IntegraLiquidacaoEmprestimoSinacor(int idCliente, DateTime data)
        {            
            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();

            // Deleta as as Liquidações do dia
            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollectionDelecao = new LiquidacaoEmprestimoBolsaCollection();
            liquidacaoEmprestimoBolsaCollectionDelecao.DeletaLiquidacaoEmprestimoBolsa(idCliente, data, FonteLiquidacaoEmprestimoBolsa.Sinacor);

            ClienteBolsa clienteBolsa = new ClienteBolsa();

            #region Busca códigos do cliente (codigoCliente, codigoCliente2 na Bovespa)
            //Se não achar pelo menos o código do cliente associado, sai da função
            if (!clienteBolsa.LoadByPrimaryKey(idCliente))
            {
                return;
            }

            //Se não tiver código associado, lança exception
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor) && String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2)) 
            {
                return;                
            }

            //Se nenhum dos 2 códigos é número, lança exception
            if (!Utilitario.IsInteger(clienteBolsa.CodigoSinacor) && !Utilitario.IsInteger(clienteBolsa.CodigoSinacor2))
            {
                return;                
            }            
                        
            int codigoCliente = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {                
                codigoCliente = Convert.ToInt32(clienteBolsa.CodigoSinacor);                
            }
            int codigoCliente2 = 0;
            if (!String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = Convert.ToInt32(clienteBolsa.CodigoSinacor2);                
            }

            //Se algum dos 2 códigos for nulo, repete o código de um para ao outro
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor))
            {
                codigoCliente = codigoCliente2;
            }
            if (String.IsNullOrEmpty(clienteBolsa.CodigoSinacor2))
            {
                codigoCliente2 = codigoCliente;
            }
            #endregion

            List<Int32> listaCodigos = new List<int>();
            listaCodigos.Add(codigoCliente);
            listaCodigos.Add(codigoCliente2);
            
            TbtliquidacaoCollection tbtliquidacaoCollection = new TbtliquidacaoCollection();
            tbtliquidacaoCollection.BuscaContratosLiquidados(listaCodigos, data);

            if (ParametrosConfiguracaoSistema.Integracoes.SchemaBTC == "")
            {
                HbtliquidacaoCollection hbtliquidacaoCollection = new HbtliquidacaoCollection();
                hbtliquidacaoCollection.BuscaContratosLiquidados(listaCodigos, data);

                TbtliquidacaoCollection tbtliquidacaoCollectionAux = new TbtliquidacaoCollection(hbtliquidacaoCollection);
                tbtliquidacaoCollection.Combine(tbtliquidacaoCollectionAux);
            }

            int idAgenteMercado = 0;
            if (tbtliquidacaoCollection.Count > 0)
            {
                //Busca o IdAgenteMercado default
                int codigoBovespaDefault = Convert.ToInt32(ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault);
                AgenteMercado agenteMercado = new AgenteMercado();
                idAgenteMercado = agenteMercado.BuscaIdAgenteMercadoBovespa(codigoBovespaDefault);
            }

            foreach (Tbtliquidacao tbtliquidacao in tbtliquidacaoCollection)
            {
                string cdAtivoBolsa = tbtliquidacao.CodNeg;
                decimal preco = tbtliquidacao.PrecMed.Value;
                decimal quantidadeLiquidada = tbtliquidacao.QtdeAcoeLiqd.Value;
                int numeroContrato = Convert.ToInt32(tbtliquidacao.NumCotr.Value);
                decimal valorBaseLiquidado = Utilitario.Truncate(preco * quantidadeLiquidada, 2);
                decimal valor = tbtliquidacao.ValBrut.Value;

                decimal valorIR = tbtliquidacao.ValIr.Value;

                decimal valorLiquidacao = 0;
                if (ParametrosConfiguracaoSistema.Integracoes.SchemaBTC == "")
                {
                    decimal valorDoador = tbtliquidacao.ValBrutDoad.Value;
                    decimal valorComissao = tbtliquidacao.ValComi.Value;
                    decimal valorEmol = tbtliquidacao.ValEmolCblc.Value;                    

                    if (valorDoador != 0)
                    {
                        valorLiquidacao = Math.Abs(valorDoador);
                    }
                    else
                    {
                        valorLiquidacao = Math.Abs(valor) + Math.Abs(valorComissao) + Math.Abs(valorEmol);
                    }
                }
                else
                {
                    valorLiquidacao = Math.Abs(valor);
                }
                

                PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection();
                posicaoEmprestimoBolsaAberturaCollection.Query.Select(posicaoEmprestimoBolsaAberturaCollection.Query.IdPosicao,
                                                                posicaoEmprestimoBolsaAberturaCollection.Query.CdAtivoBolsa,
                                                                posicaoEmprestimoBolsaAberturaCollection.Query.DataVencimento);
                posicaoEmprestimoBolsaAberturaCollection.Query.Where(posicaoEmprestimoBolsaAberturaCollection.Query.NumeroContrato.Equal(numeroContrato),
                                                                posicaoEmprestimoBolsaAberturaCollection.Query.DataHistorico.Equal(data),
                                                                posicaoEmprestimoBolsaAberturaCollection.Query.IdCliente.Equal(idCliente));

                int? idPosicao = null;
                DateTime? dataVencimento = null;
                if (posicaoEmprestimoBolsaAberturaCollection.Query.Load())
                {
                    idPosicao = posicaoEmprestimoBolsaAberturaCollection[0].IdPosicao.Value;
                    dataVencimento = posicaoEmprestimoBolsaAberturaCollection[0].DataVencimento.Value;

                    if (idPosicao.HasValue)
                    {
                        byte tipoLiquidacaoEmprestimoBolsa = 0;
                        if (DateTime.Compare(dataVencimento.Value, data) > 0)
                        {
                            tipoLiquidacaoEmprestimoBolsa = (byte)TipoLiquidacaoEmprestimoBolsa.Antecipacao;
                        }
                        else
                        {
                            tipoLiquidacaoEmprestimoBolsa = (byte)TipoLiquidacaoEmprestimoBolsa.Vencimento;
                        }

                        int? idOperacao = null;
                        #region Tenta buscar o IdOperacao vinculado em OperacaoEmprestimoBolsa, se não achar, idOperacao = null
                        OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
                        operacaoEmprestimoBolsaCollection.Query.Select(operacaoEmprestimoBolsaCollection.Query.IdOperacao);
                        operacaoEmprestimoBolsaCollection.Query.Where(operacaoEmprestimoBolsaCollection.Query.NumeroContrato.Equal(numeroContrato));
                        operacaoEmprestimoBolsaCollection.Query.Load();

                        if (operacaoEmprestimoBolsaCollection.Count > 0)
                        {
                            idOperacao = operacaoEmprestimoBolsaCollection[0].IdOperacao.Value;
                        }
                        #endregion

                        #region Copia para LiquidacaoEmprestimoBolsa
                        LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
                        liquidacaoEmprestimoBolsa.IdPosicao = idPosicao.Value;
                        liquidacaoEmprestimoBolsa.Data = data;
                        liquidacaoEmprestimoBolsa.TipoLiquidacao = tipoLiquidacaoEmprestimoBolsa;
                        liquidacaoEmprestimoBolsa.IdCliente = idCliente;
                        liquidacaoEmprestimoBolsa.IdAgente = idAgenteMercado;
                        liquidacaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                        liquidacaoEmprestimoBolsa.Quantidade = quantidadeLiquidada;
                        liquidacaoEmprestimoBolsa.Fonte = (byte)FonteLiquidacaoEmprestimoBolsa.Sinacor;
                        liquidacaoEmprestimoBolsa.IdOperacao = idOperacao;
                        liquidacaoEmprestimoBolsa.NumeroContrato = numeroContrato;
                        liquidacaoEmprestimoBolsa.Valor = Math.Abs(valorBaseLiquidado);
                        liquidacaoEmprestimoBolsa.ValorLiquidacao = valorLiquidacao;
                        liquidacaoEmprestimoBolsa.ValorIR = Math.Abs(valorIR);
                        #endregion

                        //Inclui na collection
                        liquidacaoEmprestimoBolsaCollection.AttachEntity(liquidacaoEmprestimoBolsa);
                    }
                }
            }

            liquidacaoEmprestimoBolsaCollection.Save();                 
        }

        /// <summary>
        /// Retorna o total (quantidade) liquidado da posicao (IdPosicao) na data passada.
        /// </summary>
        /// <param name="IdPosicao"></param>
        /// <param name="data"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public decimal RetornaTotalLiquidado(int IdPosicao, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdPosicao.Equal(IdPosicao),
                        this.Query.Data.Equal(data));

            this.Query.Load();

            decimal quantidade = 0;
            if (this.es.HasData && this.Quantidade.HasValue)
            {
                quantidade = this.Quantidade.Value;
            }

            return quantidade;
        }
    }
}
