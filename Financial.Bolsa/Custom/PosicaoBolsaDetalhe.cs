using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using Financial.Bolsa.Exceptions;
using Financial.Common.Enums;

namespace Financial.Bolsa {
    public partial class PosicaoBolsaDetalhe : esPosicaoBolsaDetalhe 
    {
        public void TrataProcessamentoDiario(int idCliente, DateTime data)
        {
            PosicaoBolsaDetalheCollection posicaoBolsaDetalheCollectionDeletar = new PosicaoBolsaDetalheCollection();
            posicaoBolsaDetalheCollectionDeletar.Query.Where(posicaoBolsaDetalheCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                             posicaoBolsaDetalheCollectionDeletar.Query.DataHistorico.GreaterThanOrEqual(data));
            posicaoBolsaDetalheCollectionDeletar.Query.Load();
            posicaoBolsaDetalheCollectionDeletar.MarkAllAsDeleted();
            posicaoBolsaDetalheCollectionDeletar.Save();

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);


            PosicaoBolsaDetalheCollection posicaoBolsaDetalheCollection = new PosicaoBolsaDetalheCollection();
            posicaoBolsaDetalheCollection.Query.Where(posicaoBolsaDetalheCollection.Query.IdCliente.Equal(idCliente),
                                                      posicaoBolsaDetalheCollection.Query.DataHistorico.Equal(dataAnterior));
            posicaoBolsaDetalheCollection.Query.Load();

            PosicaoBolsaDetalheCollection posicaoBolsaDetalheCollectionInsert = new PosicaoBolsaDetalheCollection();

            foreach (PosicaoBolsaDetalhe posicaoBolsaDetalhe in posicaoBolsaDetalheCollection)
            {
                PosicaoBolsaDetalhe posicaoBolsaDetalheInsert = posicaoBolsaDetalheCollectionInsert.AddNew();
                posicaoBolsaDetalheInsert.CdAtivoBolsa = posicaoBolsaDetalhe.CdAtivoBolsa;
                posicaoBolsaDetalheInsert.DataHistorico = data;
                posicaoBolsaDetalheInsert.IdAgente = posicaoBolsaDetalhe.IdAgente.Value;
                posicaoBolsaDetalheInsert.IdCliente = idCliente;
                posicaoBolsaDetalheInsert.Quantidade = posicaoBolsaDetalhe.Quantidade.Value;
                posicaoBolsaDetalheInsert.QuantidadeAbertura = posicaoBolsaDetalhe.Quantidade.Value;
                posicaoBolsaDetalheInsert.TipoCarteira = posicaoBolsaDetalhe.TipoCarteira.Value;
            }

            posicaoBolsaDetalheCollectionInsert.Save();
        }
        

    }
}
