/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           12/3/2007 10:44:13
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa {
    public partial class TabelaTaxaCustodiaBolsaCollection : esTabelaTaxaCustodiaBolsaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(TabelaTaxaCustodiaBolsaCollection));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaTabelaTaxaCustodiaBolsa(int idCliente, DateTime data) {
            // TODO log da funcao

            this.QueryReset();
            this.Query
                 .Select(this.query.IdCliente, this.query.IdAgente, this.query.DataReferencia,
                         this.query.Valor, this.query.NumeroDiasCustodia)
                 .Where(this.query.DataReferencia.LessThanOrEqual(data),
                        this.query.IdCliente == idCliente)
                 .OrderBy(this.query.IdAgente.Ascending, this.query.DataReferencia.Descending);

            this.Query.Load();

            #region Log Sql
            if (log.IsInfoEnabled) {
                string sql = this.Query.es.LastQuery;
                sql = sql.Replace("@DataReferencia1", "'" + data.ToString("yyyy-MM-dd") + "'");
                sql = sql.Replace("@IdCliente2", "'" + idCliente + "'");
                log.Info(sql);
            }
            #endregion
        }
    }
}
