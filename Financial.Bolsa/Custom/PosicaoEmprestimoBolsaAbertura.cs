﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa
{
	public partial class PosicaoEmprestimoBolsaAbertura : esPosicaoEmprestimoBolsaAbertura
	{     
        /// <summary>
        /// Retorna a quantidade da posicao (IdPosicao) na data passada.
        /// </summary>
        /// <param name="IdPosicao"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadePosicao(int IdPosicao, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade)
                 .Where(this.Query.IdPosicao.Equal(IdPosicao),
                        this.Query.DataHistorico.Equal(data));

            this.Query.Load();

            decimal quantidade = 0;
            if (this.es.HasData && this.Quantidade.HasValue)
            {
                quantidade = this.Quantidade.Value;
            }

            return quantidade;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="pontaEmprestimoBolsa"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico, PontaEmprestimoBolsa pontaEmprestimoBolsa) 
        {            
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico == dataHistorico,
                        this.Query.PontaEmprestimo.Equal(pontaEmprestimoBolsa));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }
	}
}
