﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa {
    public partial class GrupamentoBolsaCollection : esGrupamentoBolsaCollection 
    {
        /// <summary>
        /// Carrega o objeto GrupamentoBolsaCollection com os campos IdGrupamento, CdAtivoBolsa, DataReferencia,
        /// FatorQuantidade, FatorPU, TipoGrupamento.
        /// OrderBy CdAtivoBolsa.
        /// </summary>        
        /// <param name="dataEx"></param>
        public void BuscaGrupamentoBolsa(DateTime dataEx) 
        {            
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdGrupamento, this.Query.CdAtivoBolsa, this.Query.DataReferencia,
                         this.Query.FatorQuantidade, this.Query.FatorPU, this.Query.TipoGrupamento)
                 .Where(this.Query.DataEx.Equal(dataEx))
                 .OrderBy(this.Query.CdAtivoBolsa.Ascending);     

            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto GrupamentoBolsaCollection com os campos IdGrupamento, CdAtivoBolsa, 
        /// DataReferencia, FatorQuantidade, FatorPU, TipoGrupamento.
        /// Para cada registro de ativo de ações, busca potenciais registros de opções ou termo, de acordo com os booleanos passados.
        /// </summary>        
        /// <param name="data"></param>
        public GrupamentoBolsaCollection BuscaGrupamentoBolsa(DateTime dataEx, int idCliente, bool buscaOpcao, bool buscaTermo)
        {
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            GrupamentoBolsaQuery grupamentoBolsaQuery = new GrupamentoBolsaQuery("G");
            grupamentoBolsaQuery.Select(grupamentoBolsaQuery.IdGrupamento,
                                      grupamentoBolsaQuery.CdAtivoBolsa,
                                      grupamentoBolsaQuery.FatorQuantidade,
                                      grupamentoBolsaQuery.FatorPU,
                                      grupamentoBolsaQuery.TipoGrupamento);
            grupamentoBolsaQuery.InnerJoin(ativoBolsaQuery).On(grupamentoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);
            grupamentoBolsaQuery.Where(grupamentoBolsaQuery.DataEx.Equal(dataEx));
            grupamentoBolsaQuery.Where(ativoBolsaQuery.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
            grupamentoBolsaQuery.OrderBy(grupamentoBolsaQuery.IdGrupamento.Ascending);

            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            grupamentoBolsaCollection.Load(grupamentoBolsaQuery);

            foreach (GrupamentoBolsa grupamentoBolsa in grupamentoBolsaCollection)
            {
                if (buscaOpcao)
                {
                    AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                    if (ativoBolsaCollection.BuscaAtivoBolsaOpcao(grupamentoBolsa.CdAtivoBolsa, dataEx))
                    {
                        for (int j = 0; j < ativoBolsaCollection.Count; j++)
                        {
                            string cdAtivoBolsa = ativoBolsaCollection[j].CdAtivoBolsa;
                            
                            //Só adiciona se tiver realmente posição da opção na carteira
                            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                            posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.IdPosicao);
                            posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                     posicaoBolsaCollection.Query.Quantidade.NotEqual(0),
                                                     posicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                            posicaoBolsaCollection.Query.Load();
                            if (posicaoBolsaCollection.HasData)
                            {
                                #region Adiciona o Grupamento do ativo Opção achado
                                GrupamentoBolsa novoGrupamentoBolsa = new GrupamentoBolsa();
                                novoGrupamentoBolsa.DataLancamento = grupamentoBolsa.DataLancamento;
                                novoGrupamentoBolsa.DataEx = grupamentoBolsa.DataEx;
                                novoGrupamentoBolsa.DataReferencia = grupamentoBolsa.DataReferencia;
                                novoGrupamentoBolsa.FatorQuantidade = grupamentoBolsa.FatorQuantidade;
                                novoGrupamentoBolsa.FatorPU = grupamentoBolsa.FatorPU;
                                novoGrupamentoBolsa.Fonte = grupamentoBolsa.Fonte;
                                novoGrupamentoBolsa.TipoGrupamento = grupamentoBolsa.TipoGrupamento;
                                novoGrupamentoBolsa.CdAtivoBolsa = cdAtivoBolsa;

                                grupamentoBolsaCollection.AttachEntity(novoGrupamentoBolsa);
                                #endregion
                            }
                        }
                    }
                }

                if (buscaTermo)
                {
                    AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
                    if (ativoBolsaCollection.BuscaAtivoBolsaTermo(grupamentoBolsa.CdAtivoBolsa, dataEx))
                    {
                        for (int j = 0; j < ativoBolsaCollection.Count; j++)
                        {
                            string cdAtivoBolsa = ativoBolsaCollection[j].CdAtivoBolsa;
                            
                            //Só adiciona se tiver realmente posição do termo na carteira
                            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                            posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.IdPosicao);
                            posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                     posicaoTermoBolsaCollection.Query.Quantidade.NotEqual(0),
                                                     posicaoTermoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                            posicaoTermoBolsaCollection.Query.Load();
                            if (posicaoTermoBolsaCollection.HasData)
                            {
                                #region Adiciona a Grupamento do ativo Termo achado
                                GrupamentoBolsa novoGrupamentoBolsa = new GrupamentoBolsa();
                                novoGrupamentoBolsa.DataLancamento = grupamentoBolsa.DataLancamento;
                                novoGrupamentoBolsa.DataEx = grupamentoBolsa.DataEx;
                                novoGrupamentoBolsa.DataReferencia = grupamentoBolsa.DataReferencia;
                                novoGrupamentoBolsa.FatorQuantidade = grupamentoBolsa.FatorQuantidade;
                                novoGrupamentoBolsa.FatorPU = grupamentoBolsa.FatorPU;
                                novoGrupamentoBolsa.Fonte = grupamentoBolsa.Fonte;
                                novoGrupamentoBolsa.TipoGrupamento = grupamentoBolsa.TipoGrupamento;
                                novoGrupamentoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                                
                                grupamentoBolsaCollection.AttachEntity(novoGrupamentoBolsa);
                                #endregion
                            }
                        }
                    }
                }
            }

            return grupamentoBolsaCollection;

        }

        /// <summary>
        /// Carrega o objeto GrupamentoBolsaCollection com todos os campos de GrupamentoBolsa.
        /// </summary>        
        /// <param name="dataLancamento"></param>
        public void BuscaGrupamentoBolsaCompleta(DateTime dataLancamento, string cdAtivoBolsa)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.DataLancamento.Equal(dataLancamento),
                       this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
            this.Query.Load();
        }

        /// <summary>
        /// Deleta todos os grupamentos, dado o tipo de fonte passado.
        /// </summary>
        /// <param name="dataLancamento"></param>        
        /// <param name="fonte"></param>        
        public void DeletaGrupamento(DateTime dataLancamento, int fonte)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdGrupamento)
                 .Where(this.Query.Fonte == fonte,
                        this.Query.DataLancamento.Equal(dataLancamento));
            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataEx"></param>        
        public bool BuscaGrupamentoTermoOpcao(DateTime dataEx)
        {
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            GrupamentoBolsaQuery grupamentoBolsaQuery = new GrupamentoBolsaQuery("G");

            grupamentoBolsaQuery.Select(grupamentoBolsaQuery.IdGrupamento);
            grupamentoBolsaQuery.Where(grupamentoBolsaQuery.DataEx.Equal(dataEx));
            grupamentoBolsaQuery.Where(ativoBolsaQuery.TipoMercado.NotIn(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
            grupamentoBolsaQuery.InnerJoin(ativoBolsaQuery).On(grupamentoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);

            GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
            grupamentoBolsaCollection.Load(grupamentoBolsaQuery);

            return grupamentoBolsaCollection.Count > 0;
        }

    }
}
