﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.Bolsa.Enums;
using Financial.Common.Enums;
using Financial.Interfaces.Import.Bolsa;
using Financial.Bolsa.Exceptions;

namespace Financial.Bolsa {
    public partial class SubscricaoBolsa : esSubscricaoBolsa 
    {
        /// <summary>
        /// Deleta em OperacaoBolsa depositos e retiradas com Origem = DireitoSubscricao.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void DeletaEventosDireitoSubscricao(int idCliente, DateTime data)
        {
            List<int> listaSubscricao = new List<int>();
            listaSubscricao.Add((int)OrigemOperacaoBolsa.DireitoSubscricao);

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.DeletaOperacaoBolsa(idCliente, listaSubscricao, data);            
        }

        /// <summary>
        /// Processa atualizações de PosicaoBolsa a partir de lançamentos de direito de subscrição efetivadas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void ProcessaNovoDireitoSubscricao(int idCliente, DateTime data)
        {
            SubscricaoBolsaCollection subscricaoBolsaCollection = new SubscricaoBolsaCollection();
            subscricaoBolsaCollection.BuscaSubscricaoBolsa(data);
            
            for (int i = 0; i < subscricaoBolsaCollection.Count; i++)
            {
                #region Dados de Subscrição
                SubscricaoBolsa subscricaoBolsa = subscricaoBolsaCollection[i];
                string cdAtivoBolsaDireito = subscricaoBolsa.CdAtivoBolsaDireito;
                string cdAtivoBolsa = subscricaoBolsa.CdAtivoBolsa;
                decimal fator = subscricaoBolsa.Fator.Value;                
                #endregion

                PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.IdPosicao,
                                                    posicaoBolsaCollection.Query.Quantidade,
                                                    posicaoBolsaCollection.Query.IdAgente);
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                   posicaoBolsaCollection.Query.Quantidade.GreaterThan(0));
                posicaoBolsaCollection.Query.Load();

                for (int j = 0; j < posicaoBolsaCollection.Count; j++)
                {
                    PosicaoBolsa posicaoBolsa = posicaoBolsaCollection[j];
                    
                    decimal quantidade = posicaoBolsa.Quantidade.Value;
                    int idAgente = posicaoBolsa.IdAgente.Value;

                    decimal quantidadeSubscrever = Utilitario.Truncate(quantidade * fator / 100, 0);

                    if (quantidadeSubscrever != 0)
                    {
                        //Cria um novo depósito para o direito de subscrição
                        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                        operacaoBolsa.IdCliente = idCliente;
                        operacaoBolsa.CdAtivoBolsa = cdAtivoBolsaDireito;
                        operacaoBolsa.IdAgenteCorretora = idAgente;
                        operacaoBolsa.IdAgenteLiquidacao = idAgente;
                        operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                        operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito;
                        operacaoBolsa.Pu = 0;
                        operacaoBolsa.PULiquido = 0;
                        operacaoBolsa.Valor = 0;
                        operacaoBolsa.Quantidade = quantidadeSubscrever;
                        operacaoBolsa.Data = data;
                        operacaoBolsa.DataLiquidacao = data;
                        operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.DireitoSubscricao;
                        operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                        operacaoBolsa.PercentualDesconto = 0;
                        operacaoBolsa.IdLocal = Convert.ToInt16(LocalFeriadoFixo.Bovespa);
                        operacaoBolsa.CalculaDespesas = "N";
                        operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                        operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                        operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                        operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                        operacaoBolsa.DataOperacao = data;

                        operacaoBolsa.Save();
                    }
                }                
            }
        }

        /// <summary>
        /// Processa atualizações de PosicaoEmprestimoBolsa a partir de lançamentos de direito de subscrição efetivadas no dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void ProcessaNovoDireitoSubscricaoEmprestimo(int idCliente, DateTime data)
        {
            DateTime dataAnteriorBolsa = Calendario.SubtraiDiaUtil(data, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");
            SubscricaoBolsaQuery subscricaoBolsaQuery = new SubscricaoBolsaQuery("B");
            subscricaoBolsaQuery.Select(subscricaoBolsaQuery);
            subscricaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(subscricaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);
            subscricaoBolsaQuery.Where(subscricaoBolsaQuery.DataEx.GreaterThanOrEqual(dataAnteriorBolsa),
                                        subscricaoBolsaQuery.DataEx.LessThanOrEqual(data));
            subscricaoBolsaQuery.Where(ativoBolsaQuery.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
            subscricaoBolsaQuery.OrderBy(subscricaoBolsaQuery.IdSubscricao.Ascending);

            SubscricaoBolsaCollection subscricaoBolsaCollection = new SubscricaoBolsaCollection();
            subscricaoBolsaCollection.Load(subscricaoBolsaQuery);

            for (int i = 0; i < subscricaoBolsaCollection.Count; i++)
            {
                #region Dados de Subscrição
                SubscricaoBolsa subscricaoBolsa = subscricaoBolsaCollection[i];
                string cdAtivoBolsaDireito = subscricaoBolsa.CdAtivoBolsaDireito;
                string cdAtivoBolsa = subscricaoBolsa.CdAtivoBolsa;
                decimal fator = subscricaoBolsa.Fator.Value;
                DateTime dataEx = subscricaoBolsa.DataEx.Value;
                #endregion

                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                           posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador));
                posicaoEmprestimoBolsaCollection.Query.Load();

                foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
                {
                    DateTime dataRegistro = posicaoEmprestimoBolsa.DataRegistro.Value;

                    if ((dataRegistro <= dataEx && dataEx == data) || (dataRegistro > dataEx && dataRegistro == data))
                    {
                        decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                        int idAgente = posicaoEmprestimoBolsa.IdAgente.Value;

                        decimal quantidadeSubscrever = Utilitario.Truncate(quantidade * fator / 100, 0);

                        if (quantidadeSubscrever != 0)
                        {
                            //Cria um novo depósito para o direito de subscrição
                            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                            operacaoBolsa.IdCliente = idCliente;
                            operacaoBolsa.CdAtivoBolsa = cdAtivoBolsaDireito;
                            operacaoBolsa.IdAgenteCorretora = idAgente;
                            operacaoBolsa.IdAgenteLiquidacao = idAgente;
                            operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                            operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito;
                            operacaoBolsa.Pu = 0;
                            operacaoBolsa.PULiquido = 0;
                            operacaoBolsa.Valor = 0;
                            operacaoBolsa.Quantidade = quantidadeSubscrever;
                            operacaoBolsa.Data = data;
                            operacaoBolsa.DataLiquidacao = data;
                            operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.DireitoSubscricao;
                            operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                            operacaoBolsa.PercentualDesconto = 0;
                            operacaoBolsa.IdLocal = Convert.ToInt16(LocalFeriadoFixo.Bovespa);
                            operacaoBolsa.CalculaDespesas = "N";
                            operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                            operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                            operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                            operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                            operacaoBolsa.DataOperacao = data;

                            operacaoBolsa.Save();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Verifica todos os direitos vencendo (data fim de negociacao) e baixa-os da PosicaoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        private void ProcessaBaixaDireitoSubscricao(int idCliente, DateTime data)
        {
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("P");
            SubscricaoBolsaQuery subscricaoBolsaQuery = new SubscricaoBolsaQuery("S");
            posicaoBolsaQuery.Select(posicaoBolsaQuery.CdAtivoBolsa,
                                     posicaoBolsaQuery.Quantidade,
                                     posicaoBolsaQuery.IdAgente);
            posicaoBolsaQuery.InnerJoin(subscricaoBolsaQuery).On(subscricaoBolsaQuery.CdAtivoBolsaDireito == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.Where(subscricaoBolsaQuery.DataFimNegociacao.Equal(data));
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente));
            posicaoBolsaQuery.GroupBy(posicaoBolsaQuery.CdAtivoBolsa, 
                                      posicaoBolsaQuery.Quantidade, 
                                      posicaoBolsaQuery.IdAgente);
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            posicaoBolsaCollection.Load(posicaoBolsaQuery);

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoBolsa.Quantidade.Value;
                int idAgente = posicaoBolsa.IdAgente.Value;

                string tipoOperacao = "";
                if (quantidade < 0)
                {
                    tipoOperacao = TipoOperacaoBolsa.Deposito;
                }
                else
                {
                    tipoOperacao = TipoOperacaoBolsa.Retirada;
                }

                //Cria uma nova retirada para o direito de subscrição
                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.IdCliente = idCliente;
                operacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                operacaoBolsa.IdAgenteCorretora = idAgente;
                operacaoBolsa.IdAgenteLiquidacao = idAgente;
                operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
                operacaoBolsa.TipoOperacao = tipoOperacao;
                operacaoBolsa.Pu = 0;
                operacaoBolsa.PULiquido = 0;
                operacaoBolsa.Valor = 0;
                operacaoBolsa.Quantidade = Math.Abs(quantidade);

                operacaoBolsa.Data = data;
                operacaoBolsa.DataLiquidacao = data;
                operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.DireitoSubscricao;
                operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Interno;
                operacaoBolsa.PercentualDesconto = 0;
                operacaoBolsa.IdLocal = Convert.ToInt16(LocalFeriadoFixo.Bovespa);
                operacaoBolsa.CalculaDespesas = "N";
                operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                operacaoBolsa.DataOperacao = data;

                operacaoBolsa.Save();
            }
        }
        
        /// <summary>
        /// Processa todos os eventos relativos a direito de subscrição.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaDireitoSubscricao(int idCliente, DateTime data)
        {
            this.DeletaEventosDireitoSubscricao(idCliente, data);
            this.ProcessaNovoDireitoSubscricao(idCliente, data);
            this.ProcessaNovoDireitoSubscricaoEmprestimo(idCliente, data);
            this.ProcessaBaixaDireitoSubscricao(idCliente, data);
        }

        /// <summary>
        /// Carrega SubscricaoBolsa a partir da estrutura importada do arquivo PROD.
        /// </summary>
        /// <param name="prodCollection"></param>
        /// <param name="data"></param>
        /// <exception cref="AtivoNaoCadastradoException">Se cdAtivoBolsa não existir
        /// <exception cref="CodigoIsinNaoCadastradoException">Se CodigoIsin do Ativo não Existir
        public void CarregaProdSubscricao(ProdCollection prodCollection, DateTime data, bool geraException)
        {
            // Pega os Proventos do Arquivo de Prod                          
            Prod prod = new Prod();

            ProdCollection prodCollectionSubList = new ProdCollection();

            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoSubscricao);

            //Deleta os eventos de subscrição (direitos) gerados a partir do arquivo PROD na data
            SubscricaoBolsaCollection subscricaoBolsaCollectionDeletar = new SubscricaoBolsaCollection();
            subscricaoBolsaCollectionDeletar.DeletaSubscricao(data, (int)FonteProventoBolsa.ArquivoPROD);

            SubscricaoBolsaCollection subscricaoBolsaCollection = new SubscricaoBolsaCollection();
            //
            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++)
            {
                prod = prodCollectionSubList.CollectionProd[i];

                #region Dados Prod
                string cdAtivoBolsaDireito = prod.CdAtivoBolsa;
                DateTime dataEx = prod.DataEx.Value;
                decimal fator = prod.ValorProvento.Value;
                DateTime dataFinalSubscricao = prod.DataFinalSubscricao.Value;
                decimal precoEmissaoSubscricao = prod.PrecoEmissaoSubscricao.Value;
                #endregion

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                string codigoIsinSubscricao = prod.CodigoIsinBaseSubscricao;
                if (!ativoBolsa.BuscaCdAtivoBolsa(codigoIsinSubscricao)) //Se não tem codigo Isin cadastrado, exception!
                {
                    if (geraException)
                    {
                        throw new CodigoIsinNaoCadastradoException("Código Isin: " + codigoIsinSubscricao + " para o Ativo não cadastrado");
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    #region Verifica se cdAtivoBolsaDireito existe, e cadastra se nao existir
                    AtivoBolsa ativoBolsaExiste = new AtivoBolsa();
                    if (!ativoBolsaExiste.LoadByPrimaryKey(cdAtivoBolsaDireito))
                    {
                        AtivoBolsa ativoBolsaNovo = new AtivoBolsa();
                        ativoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDireito;
                        ativoBolsaNovo.CodigoIsin = "";
                        ativoBolsaNovo.Descricao = cdAtivoBolsaDireito;
                        ativoBolsaNovo.Especificacao = "DIR";
                        ativoBolsaNovo.IdEmissor = 1;
                        ativoBolsaNovo.IdMoeda = 1;
                        ativoBolsaNovo.TipoMercado = TipoMercadoBolsa.MercadoVista;
                        ativoBolsaNovo.TipoPapel = (byte)TipoPapelAtivo.Normal;
                        ativoBolsaNovo.NumeroSerie = 0;
                        ativoBolsaNovo.Save();

                        FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                        if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(ativoBolsa.CdAtivoBolsa, data))
                        {
                            FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                            fatorCotacaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsaDireito;
                            fatorCotacaoBolsaNovo.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                            fatorCotacaoBolsaNovo.Fator = fatorCotacaoBolsa.Fator.Value;
                            fatorCotacaoBolsaNovo.Save();
                        }
                    }
                    #endregion

                    string cdAtivoBolsa = ativoBolsa.CdAtivoBolsa;

                    if (DateTime.Compare(dataEx, data) == 0) //Isso pq no prod a subscrição vem sucessivas vezes!
                    {
                        #region Cria Subscricao
                        SubscricaoBolsa subscricaoBolsaNovo = new SubscricaoBolsa();
                        subscricaoBolsaNovo.CdAtivoBolsaDireito = cdAtivoBolsaDireito;
                        subscricaoBolsaNovo.CdAtivoBolsa = cdAtivoBolsa;
                        subscricaoBolsaNovo.DataEx = dataEx;
                        subscricaoBolsaNovo.DataFimNegociacao = dataFinalSubscricao;
                        subscricaoBolsaNovo.DataLancamento = data;
                        subscricaoBolsaNovo.DataReferencia = dataReferencia;
                        subscricaoBolsaNovo.Fator = fator;
                        subscricaoBolsaNovo.PrecoSubscricao = precoEmissaoSubscricao;
                        subscricaoBolsaNovo.Fonte = (byte)FonteProventoBolsa.ArquivoPROD;

                        subscricaoBolsaCollection.AttachEntity(subscricaoBolsaNovo);
                        #endregion
                    }
                }
            }

            subscricaoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega SubscricaoBolsa a partir da estrutura importada do arquivo PROD.
        /// Não considera foreign Key Ativobolsa
        /// Importa mesmo se não existir AtivoBolsa
        /// </summary>
        /// <param name="prodCollection"></param>
        /// <param name="data"></param>
        public void CarregaProdSubscricaoSemFK(ProdCollection prodCollection, DateTime data) {
            // Pega os Proventos do Arquivo de Prod                          
            Prod prod = new Prod();

            ProdCollection prodCollectionSubList = new ProdCollection();

            // Filtra a Collection pelo TipoProvento
            prodCollectionSubList.CollectionProd = prodCollection.CollectionProd.FindAll(prod.FilterProdByTipoProventoSubscricao);

            //Deleta os eventos de subscrição (direitos) gerados a partir do arquivo PROD na data
            SubscricaoBolsaCollection subscricaoBolsaCollectionDeletar = new SubscricaoBolsaCollection();
            subscricaoBolsaCollectionDeletar.DeletaSubscricao(data, (int)FonteProventoBolsa.ArquivoPROD);

            for (int i = 0; i < prodCollectionSubList.CollectionProd.Count; i++) {
                prod = prodCollectionSubList.CollectionProd[i];

                #region Dados Prod
                string cdAtivoBolsaDireito = prod.CdAtivoBolsa;
                DateTime dataEx = prod.DataEx.Value;
                decimal fator = prod.ValorProvento.Value;
                DateTime dataFinalSubscricao = prod.DataFinalSubscricao.Value;
                decimal precoEmissaoSubscricao = prod.PrecoEmissaoSubscricao.Value;
                string codigoIsinSubscricao = prod.CodigoIsinBaseSubscricao;
                //
                DateTime dataReferencia = Calendario.SubtraiDiaUtil(dataEx, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                #endregion

                string codigoIsin = null; // Inserido na Tabela Subscricao se não existir AtivoBolsa com o codigoIsin
                string cdAtivoBolsa = " ";
                               
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (ativoBolsa.BuscaCdAtivoBolsa(codigoIsinSubscricao)) { //Se tem codigo Isin cadastrado
                    cdAtivoBolsa = ativoBolsa.CdAtivoBolsa;
                }
                else {
                    codigoIsin = prod.CodigoIsinBaseSubscricao;
                } 

                if (dataEx == data) { //Isso pq no prod a subscrição vem sucessivas vezes!

                    #region Cria Subscricao
                    SubscricaoBolsa subscricaoBolsa = new SubscricaoBolsa();
                    subscricaoBolsa.CdAtivoBolsaDireito = cdAtivoBolsaDireito;
                    subscricaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    subscricaoBolsa.DataEx = dataEx;
                    subscricaoBolsa.DataFimNegociacao = dataFinalSubscricao;
                    subscricaoBolsa.DataLancamento = data;
                    subscricaoBolsa.DataReferencia = dataReferencia;
                    subscricaoBolsa.Fator = fator;
                    subscricaoBolsa.PrecoSubscricao = precoEmissaoSubscricao;
                    subscricaoBolsa.Fonte = (byte)FonteProventoBolsa.ArquivoPROD;
                    #endregion

                    // Insere SubscricaoBolsa
                    // Se cdAtivoBolsa = " ", CodigoIsin estará preenchido
                    try {
                        this.InsereSubscricaoComCodigoIsin(subscricaoBolsa, codigoIsin);
                    }
                    catch (Exception e) {
                        throw new Exception("Erro Inserção: " + e.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Inserção especial onde a tabela Subscricao possui o campo extra CodigoIsin
        /// Usado para carregar o arquivo Prod sem dependência da Tabela AtivoBolsa
        /// </summary>
        private void InsereSubscricaoComCodigoIsin(SubscricaoBolsa subscricaoBolsa, string codigoIsin) {

            string[] campos = new string[] {         
                SubscricaoBolsaMetadata.ColumnNames.DataLancamento,
                SubscricaoBolsaMetadata.ColumnNames.DataEx,
                SubscricaoBolsaMetadata.ColumnNames.DataReferencia,
                SubscricaoBolsaMetadata.ColumnNames.PrecoSubscricao,
                SubscricaoBolsaMetadata.ColumnNames.Fator,
                SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsaDireito,
                SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsa,
                SubscricaoBolsaMetadata.ColumnNames.DataFimNegociacao,            
                SubscricaoBolsaMetadata.ColumnNames.Fonte,
                "CodigoIsin"
            };

            string camposConcat = String.Join(", ", campos);
            string camposParams = String.Join(",@", campos);

            esParameters esParams = new esParameters();
            esParams.Add(campos[0], subscricaoBolsa.DataLancamento.Value);
            esParams.Add(campos[1], subscricaoBolsa.DataEx.Value);
            esParams.Add(campos[2], subscricaoBolsa.DataReferencia.Value);
            esParams.Add(campos[3], subscricaoBolsa.PrecoSubscricao.Value);
            esParams.Add(campos[4], subscricaoBolsa.Fator.Value);
            esParams.Add(campos[5], subscricaoBolsa.CdAtivoBolsaDireito);
            esParams.Add(campos[6], subscricaoBolsa.CdAtivoBolsa);
            esParams.Add(campos[7], subscricaoBolsa.DataFimNegociacao.Value);
            esParams.Add(campos[8], subscricaoBolsa.Fonte.Value);

            // Parametros que podem ser null
            if (String.IsNullOrEmpty(codigoIsin)) {
                esParams.Add("CodigoIsin", DBNull.Value);
            }
            else {
                esParams.Add("CodigoIsin", codigoIsin);
            }

            //
            StringBuilder sqlBuilder = new StringBuilder();
            //
            sqlBuilder.Append("INSERT INTO SubscricaoBolsa (");
            sqlBuilder.Append(camposConcat);
            sqlBuilder.Append(") VALUES ( ");
            sqlBuilder.Append("@" + camposParams);
            sqlBuilder.Append(")");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
        }
    }
}
