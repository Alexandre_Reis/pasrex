using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.Bolsa {
    public partial class ProventoBolsaClienteCollection : esProventoBolsaClienteCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(ProventoBolsaClienteCollection));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaProventoBolsa(DateTime data, int fonte)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdProvento)
              .Where(this.Query.DataLancamento.Equal(data),
                     this.Query.Fonte == fonte);

            this.Query.Load();
         
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool BuscaProventoBolsaCliente(DateTime data, int idCliente) 
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.DataLancamento.Equal(data),
                        this.Query.DataPagamento.GreaterThanOrEqual(data),
                        this.Query.IdCliente == idCliente);
                
            bool retorno = this.Query.Load();
            
            return retorno;
        }                       


    }
}
