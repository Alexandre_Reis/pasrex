﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Interfaces.Import.Bolsa;
using Financial.Interfaces.Import.Offshore;
using Financial.Bolsa.Enums;

namespace Financial.Bolsa
{
    public partial class CotacaoBolsa : esCotacaoBolsa
    {
        /// <summary>
        /// Carrega o objeto CotacaoBolsa com os campos PUAbertura, PUFechamento, PUMedio, PUGerencial.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <returns>booleano indicando se achou registro</returns>
        public bool BuscaCotacaoBolsa(string cdAtivoBolsa, DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.PUAbertura, this.Query.PUFechamento, this.Query.PUMedio, this.Query.PUGerencial)
                 .Where(this.Query.CdAtivoBolsa == cdAtivoBolsa,
                        this.Query.Data == data);

            if (!this.Query.Load())
            {
                if (ParametrosConfiguracaoSistema.Bolsa.EmiteMsgSemCotacaoBolsa)                
                    throw new Exception("Não existe cotação cadastrada para o Ativo: " + cdAtivoBolsa + " na data: " + data.ToShortDateString());                
            }

            return this.es.HasData;
        }

        /// <summary>
        /// Carrega o objeto com os todos os campos de CotacaoBolsa.
        /// Busca pela data anterior mais próxima .
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>        
        /// <returns>booleando indicando se achou registro</returns>
        public bool BuscaUltimaCotacaoBolsa(string cdAtivoBolsa, DateTime data)
        {
            CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();

            cotacaoBolsaCollection.Query
                 .Where(cotacaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa &
                        cotacaoBolsaCollection.Query.Data.LessThanOrEqual(data) &
                        (cotacaoBolsaCollection.Query.PUFechamento.NotEqual(0) |
                          cotacaoBolsaCollection.Query.PUMedio.NotEqual(0))
                        )
                 .OrderBy(cotacaoBolsaCollection.Query.Data.Descending);

            cotacaoBolsaCollection.Query.Load();

            if (cotacaoBolsaCollection.HasData)
            {
                // Copia informações de historicoCotaCollection para historicoCota
                this.AddNew();
                this.PUAbertura = ((CotacaoBolsa)cotacaoBolsaCollection[0]).PUAbertura;
                this.PUFechamento = ((CotacaoBolsa)cotacaoBolsaCollection[0]).PUFechamento;
                this.PUMedio = ((CotacaoBolsa)cotacaoBolsaCollection[0]).PUMedio;
                this.PUGerencial = ((CotacaoBolsa)cotacaoBolsaCollection[0]).PUGerencial;
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Carrega CotacaoBolsa a partir da estrutura importada do arquivo BDIN.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        /// throws AtivoNaoCadastradoException se cdAtivoBolsa não existir
        public void CarregaCotacaoBdin(DateTime data, string pathArquivo)
        {
            Bdin bdin = new Bdin();
            CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();
            CotacaoBolsaCollection cotacaoBolsaCollectionAux = new CotacaoBolsaCollection();

            // Deleta as Cotações do dia
            cotacaoBolsaCollectionAux.DeletaCotacaoBolsa(data);
            // Pega as cotações do arquivo de Bdin                            
            BdinCollection bdinCollection = bdin.ProcessaBdin(data, pathArquivo);

            /* Guarda os ativos que estão com cotação do dia anterior */
            List<string> ativosCopiaDiaAnterior = new List<string>();
            
            for (int i = 0; i < bdinCollection.CollectionBdin.Count; i++)
            {
                bdin = bdinCollection.CollectionBdin[i];

                // TODO - Retirar esse codigo - Processamento de Termo/OPC COMPRA/Leilao/OPCOES COMPRA/OPCOES VENDA/Fracionario --
                if (bdin.TipoMercado != 30)
                {
                    // Para impedir gravação de lixo que tenha vindo do arquivo Bdin
                    if (!String.IsNullOrEmpty(bdin.CodigoNegociacao)) {
                                              
                        // Trata código de ativos repetidos que aparecem na sessão 2 e 9
                        // Se cotação já existe dentro da própria collection então considera o primeiro. 
                        // Se esse primeiro for zero considera o segundo
                        CotacaoBolsa c = cotacaoBolsaCollection.FindByPrimaryKey(data, bdin.CodigoNegociacao);
                        if (c == null) { // se não achou
                            CotacaoBolsa cotacaoBolsa = cotacaoBolsaCollection.AddNew(); // Coleção Final

                            #region Insere normal
                            cotacaoBolsa.Data = data;
                            cotacaoBolsa.CdAtivoBolsa = bdin.CodigoNegociacao;
                            cotacaoBolsa.PUAbertura = bdin.PuAbertura.HasValue ? bdin.PuAbertura : 0;
                            cotacaoBolsa.PUFechamento = bdin.PuFechamento.HasValue ? bdin.PuFechamento : 0;
                            cotacaoBolsa.PUMedio = bdin.PuMedio.HasValue ? bdin.PuMedio : 0;

                            // Se PUMedio ou PUFechamento = 0 então pega a cotação do dia anterior
                            if (!cotacaoBolsa.PUMedio.HasValue || !cotacaoBolsa.PUFechamento.HasValue ||
                                 cotacaoBolsa.PUMedio == 0 || cotacaoBolsa.PUFechamento == 0) {
                                DateTime diaAnterior = Util.Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                                CotacaoBolsa cotacaoDiaAnterior = new CotacaoBolsa();
                                cotacaoDiaAnterior.LoadByPrimaryKey(diaAnterior, cotacaoBolsa.CdAtivoBolsa);
                                // Se Foi carregado valor do dia anterior Então substitui valor dos PUs no dia atual
                                if (cotacaoDiaAnterior.es.HasData) {
                                    cotacaoBolsa.Data = data;
                                    cotacaoBolsa.PUAbertura = cotacaoDiaAnterior.PUAbertura;
                                    cotacaoBolsa.PUFechamento = cotacaoDiaAnterior.PUFechamento;
                                    cotacaoBolsa.PUMedio = cotacaoDiaAnterior.PUMedio;
                                    //
                                    //Informa que ativo está com cotação do dia anterior
                                    ativosCopiaDiaAnterior.Add(cotacaoBolsa.CdAtivoBolsa);
                                }
                            }
                            #endregion
                        }
                        else { // se achou

                            // Se ativo está com a cotação do dia anterior copia valor do segundo registro se ele for diferente de zero
                            if (ativosCopiaDiaAnterior.Contains(bdin.CodigoNegociacao)) {
                                if (bdin.PuAbertura.HasValue && bdin.PuAbertura != 0) {
                                    c.PUAbertura = bdin.PuAbertura;
                                }
                                if (bdin.PuMedio.HasValue && bdin.PuMedio != 0) {
                                    c.PUMedio = bdin.PuMedio;
                                }
                                if (bdin.PuFechamento.HasValue && bdin.PuFechamento != 0) {
                                    c.PUFechamento = bdin.PuFechamento;
                                }
                            }
                            else {
                                // se os valores do registro já existente forem zero substitue os valores pelos do segundo registro
                                if (c.PUAbertura.HasValue && c.PUAbertura == 0) {
                                    c.PUAbertura = bdin.PuAbertura.HasValue ? bdin.PuAbertura : 0;
                                }
                                if (c.PUMedio.HasValue && c.PUMedio == 0) {
                                    c.PUMedio = bdin.PuMedio.HasValue ? bdin.PuMedio : 0;
                                }
                                if (c.PUFechamento.HasValue && c.PUFechamento == 0) {
                                    c.PUFechamento = bdin.PuFechamento.HasValue ? bdin.PuFechamento : 0;                                
                                }
                            }                            
                        }
                    }
                }
            }


            #region String de Ativos
            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa);
            ativoBolsaCollection.Query.Load();
            Dictionary<string, string> cdAtivos = new Dictionary<string, string>();
            foreach (AtivoBolsa ativoBolsa in ativoBolsaCollection)
            {
                cdAtivos.Add(ativoBolsa.CdAtivoBolsa, ativoBolsa.CdAtivoBolsa);
            }
            #endregion

            // Filtra somente as cotações que possuem Ativo
            CotacaoBolsaCollection cotacaoBolsaCollectionFiltered = new CotacaoBolsaCollection();
            foreach (CotacaoBolsa cotacaoBolsa in cotacaoBolsaCollection)
            {
                if (cdAtivos.ContainsKey(cotacaoBolsa.CdAtivoBolsa))
                {
                    cotacaoBolsaCollectionFiltered.AttachEntity(cotacaoBolsa);
                }
            }

            // Salva todas as cotacoes da collection           
            try
            {
                cotacaoBolsaCollectionFiltered.Save();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }

            // Insere as cotações do dia anterior que não estão no dia de hoje
            cotacaoBolsaCollectionAux.QueryReset();
            if (cotacaoBolsaCollectionAux.BuscaCotacaoDiaAnterior(data))
            {
                // Altera a data Do dia anterior para a nova data
                for (int i = 0; i < cotacaoBolsaCollectionAux.Count; i++)
                {
                    CotacaoBolsa cotacaoBolsaAux = cotacaoBolsaCollectionAux[i];
                    cotacaoBolsaAux.Data = data;
                }
                // Salva os elementos da coleção               
                for (int i = 0; i < cotacaoBolsaCollectionAux.Count; i++)
                {
                    CotacaoBolsa cotacaoBolsa = cotacaoBolsaCollectionAux[i];
                    CotacaoBolsa cotacaoBolsaAux = new CotacaoBolsa();
                    cotacaoBolsaAux.AddNew();
                    cotacaoBolsaAux.Data = cotacaoBolsa.Data;
                    cotacaoBolsaAux.CdAtivoBolsa = cotacaoBolsa.CdAtivoBolsa;
                    cotacaoBolsaAux.PUMedio = cotacaoBolsa.PUMedio;
                    cotacaoBolsaAux.PUFechamento = cotacaoBolsa.PUFechamento;
                    cotacaoBolsaAux.PUAbertura = cotacaoBolsa.PUAbertura;
                    //           
                    cotacaoBolsaAux.Save();
                }
            }
        }

        /// <summary>
        /// Carrega CotacaoBolsa a partir da estrutura importada do arquivo BDIN.
        /// Desconsidera Foreign Key de AtivoBolsa
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaCotacaoBdinSemFK(DateTime data, string pathArquivo)
        {
            Bdin bdin = new Bdin();
            CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();
            CotacaoBolsaCollection cotacaoBolsaCollectionAux = new CotacaoBolsaCollection();

            // Deleta as Cotações do dia
            cotacaoBolsaCollectionAux.DeletaCotacaoBolsa(data);

            // Pega as cotações do arquivo de Bdin
            BdinCollection bdinCollection = bdin.ProcessaBdin(data, pathArquivo);

            for (int i = 0; i < bdinCollection.CollectionBdin.Count; i++)
            {
                bdin = bdinCollection.CollectionBdin[i];

                // TODO - Retirar esse codigo - Processamento de Termo/OPC COMPRA/Leilao/OPCOES COMPRA/OPCOES VENDA/Fracionario --
                if (bdin.TipoMercado != 30)
                {

                    // Para impedir gravação de lixo que tenha vindo do arquivo Bdin
                    if (!String.IsNullOrEmpty(bdin.CodigoNegociacao))
                    {
                        CotacaoBolsa cotacaoBolsa = cotacaoBolsaCollection.AddNew();
                        cotacaoBolsa.Data = data;
                        cotacaoBolsa.CdAtivoBolsa = bdin.CodigoNegociacao;
                        cotacaoBolsa.PUAbertura = bdin.PuAbertura.HasValue ? bdin.PuAbertura : 0;
                        cotacaoBolsa.PUFechamento = bdin.PuFechamento.HasValue ? bdin.PuFechamento : 0;
                        cotacaoBolsa.PUMedio = bdin.PuMedio.HasValue ? bdin.PuMedio : 0;

                        // Se PUMedio ou PUFechamento = 0 então pega a cotação do dia anterior
                        if (!cotacaoBolsa.PUMedio.HasValue || !cotacaoBolsa.PUFechamento.HasValue ||
                             cotacaoBolsa.PUMedio == 0 || cotacaoBolsa.PUFechamento == 0)
                        {

                            DateTime diaAnterior = Util.Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                            CotacaoBolsa cotacaoDiaAnterior = new CotacaoBolsa();
                            cotacaoDiaAnterior.LoadByPrimaryKey(diaAnterior, cotacaoBolsa.CdAtivoBolsa);
                            // Se Foi carregado valor do dia anterior Então substitui valor dos PUs no dia atual
                            if (cotacaoDiaAnterior.es.HasData)
                            {
                                cotacaoBolsa.Data = data;
                                cotacaoBolsa.PUAbertura = cotacaoDiaAnterior.PUAbertura;
                                cotacaoBolsa.PUFechamento = cotacaoDiaAnterior.PUFechamento;
                                cotacaoBolsa.PUMedio = cotacaoDiaAnterior.PUMedio;
                            }
                        }
                    }
                }
            }

            // Salva todas as cotacoes da collection           
            try
            {
                cotacaoBolsaCollection.Save();
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }

            // Insere as cotações do dia anterior que não estão no dia de hoje
            cotacaoBolsaCollectionAux.QueryReset();
            if (cotacaoBolsaCollectionAux.BuscaCotacaoDiaAnterior(data))
            {
                // Altera a data Do dia anterior para a nova data
                for (int i = 0; i < cotacaoBolsaCollectionAux.Count; i++)
                {
                    CotacaoBolsa cotacaoBolsaAux = cotacaoBolsaCollectionAux[i];
                    cotacaoBolsaAux.Data = data;
                }
                // Salva os elementos da coleção               
                for (int i = 0; i < cotacaoBolsaCollectionAux.Count; i++)
                {
                    CotacaoBolsa cotacaoBolsa = cotacaoBolsaCollectionAux[i];
                    CotacaoBolsa cotacaoBolsaAux = new CotacaoBolsa();
                    cotacaoBolsaAux.AddNew();
                    cotacaoBolsaAux.Data = cotacaoBolsa.Data;
                    cotacaoBolsaAux.CdAtivoBolsa = cotacaoBolsa.CdAtivoBolsa;
                    cotacaoBolsaAux.PUMedio = cotacaoBolsa.PUMedio;
                    cotacaoBolsaAux.PUFechamento = cotacaoBolsa.PUFechamento;
                    cotacaoBolsaAux.PUAbertura = cotacaoBolsa.PUAbertura;
                    //           
                    cotacaoBolsaAux.Save();
                }
            }
        }

        /// <summary>
        /// Carrega CotacaoBolsa a partir do site do yahoo
        /// Exemplo: http://finance.yahoo.com/d/quotes.csv?s=XOM+BBDb.TO+&f=sl1
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaCotacaoOffShore(DateTime data, string pathArquivo)
        {

            CotacaoOffShore cotacaoOffShore = new CotacaoOffShore();
            //
            CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();

            #region Monta Lista de String
            /* 1 elemento = até 100 elementos separados por + se existir
             * 2 elemento = mais 100 elementos se existir separados por +
             */
            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.BuscaAtivoOffshore();
            // 

            // Se não tem Ativos não faz nada
            if (ativoBolsaCollection.Count == 0)
            {
                return;
            }

            StringBuilder cdAtivos = new StringBuilder();

            const int QTD_ATIVOS = 100; // Quantidade de ativos por Grupo
            int tamanhoLista = (int)Math.Ceiling(ativoBolsaCollection.Count / 100M);

            List<string> ativos = new List<string>(tamanhoLista);

            for (int i = 0; i < ativoBolsaCollection.Count; i++)
            {
                //Faz o replace pois o Yahoo só entende sufixos com -
                string cdAtivo = ativoBolsaCollection[i].CdAtivoBolsa.Replace("/", "-").Replace(".", "-");

                if (!cdAtivos.ToString().Contains(cdAtivo))
                {
                    cdAtivos.Append(cdAtivo + "+");
                }

                if (i % QTD_ATIVOS == 0 && i != 0)
                { // Multiplos de QTD_ATIVOS
                    ativos.Add(cdAtivos.ToString());
                    cdAtivos = new StringBuilder(); // Zera a String     
                }
            }
            // Adiciona o ultimo grupo
            if (!String.IsNullOrEmpty(cdAtivos.ToString()))
            {
                ativos.Add(cdAtivos.ToString());
            }
            #endregion

            // Pega as Cotações do arquivo do .csv yahoo 
            List<CotacaoOffShore> cotacaoOffShoreCollection = cotacaoOffShore.ProcessaCotacaoOffShore(ativos, data, pathArquivo);

            for (int i = 0; i < cotacaoOffShoreCollection.Count; i++)
            {
                cotacaoOffShore = cotacaoOffShoreCollection[i];
                //
                bool achou = false;
                string cdAtivoBolsa = cotacaoOffShore.CdAtivo.Replace("\"", "");
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                {
                    achou = true;
                }
                else
                {
                    cdAtivoBolsa = cotacaoOffShore.CdAtivo.Replace("\"", "").Replace("-", "/");
                    ativoBolsa = new AtivoBolsa();
                    if (ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                    {
                        achou = true;
                    }
                    else
                    {
                        cdAtivoBolsa = cotacaoOffShore.CdAtivo.Replace("\"", "").Replace("-", ".");
                        ativoBolsa = new AtivoBolsa();
                        if (ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                        {
                            achou = true;
                        }
                    }
                }

                if (achou)
                {
                    CotacaoBolsa cotacaoBolsaDeletar = new CotacaoBolsa();
                    if (cotacaoBolsaDeletar.LoadByPrimaryKey(cotacaoOffShore.Data, cdAtivoBolsa))
                    {
                        cotacaoBolsaDeletar.MarkAsDeleted();
                        cotacaoBolsaDeletar.Save();
                    }

                    CotacaoBolsa cotacaoBolsa = cotacaoBolsaCollection.AddNew();
                    cotacaoBolsa.Data = cotacaoOffShore.Data;
                    cotacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    cotacaoBolsa.PUAbertura = cotacaoOffShore.Pu;
                    cotacaoBolsa.PUFechamento = cotacaoOffShore.Pu;
                    cotacaoBolsa.PUMedio = cotacaoOffShore.Pu;
                }
            }

            // Salva Todas as Cotações da Collection           
            try
            {
                cotacaoBolsaCollection.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            //Carrega as cotações do dia anterior que não tenham sido carregadas na data
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.NovaYork, TipoFeriado.Outros);
            foreach (AtivoBolsa ativoBolsaCotacao in ativoBolsaCollection)
            {
                string cdAtivoBolsaCotacao = ativoBolsaCotacao.CdAtivoBolsa;
                CotacaoBolsa cotacaoBolsaChecar = new CotacaoBolsa();
                if (!cotacaoBolsaChecar.LoadByPrimaryKey(data, cdAtivoBolsaCotacao))
                {
                    CotacaoBolsa cotacaoBolsaAnterior = new CotacaoBolsa();
                    if (cotacaoBolsaAnterior.LoadByPrimaryKey(dataAnterior, cdAtivoBolsaCotacao))
                    {
                        CotacaoBolsa cotacaoBolsaInserir = new CotacaoBolsa();
                        cotacaoBolsaInserir.Data = data;
                        cotacaoBolsaInserir.CdAtivoBolsa = cdAtivoBolsaCotacao;
                        cotacaoBolsaInserir.PUAbertura = cotacaoBolsaAnterior.PUAbertura;
                        cotacaoBolsaInserir.PUFechamento = cotacaoBolsaAnterior.PUFechamento;
                        cotacaoBolsaInserir.PUMedio = cotacaoBolsaAnterior.PUMedio;
                        cotacaoBolsaInserir.Save();
                    }
                }
            }
        }

        /// <summary>
        /// Carrega CotacaoBolsa a partir do site da Bloomberg.
        /// </summary>        
        public void CarregaCotacaoBloomberg(DateTime dataInicio, DateTime dataFim)
        {
            CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();

            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.BuscaAtivoOffshore();

            List<string> listaAtivos = new List<string>();
            List<string> listaEspec = new List<string>();
            foreach (AtivoBolsa ativoBolsa in ativoBolsaCollection)
            {
                listaAtivos.Add(ativoBolsa.CdAtivoBolsa);
                listaEspec.Add(ativoBolsa.Especificacao);
            }

            CotacaoBloomberg cotacaoBloombergExec = new CotacaoBloomberg();
            List<CotacaoBloomberg> listaCotacao = cotacaoBloombergExec.CarregaCotacaoBloomberg(listaAtivos, listaEspec, dataInicio, dataFim);

            foreach (CotacaoBloomberg cotacaoBloomberg in listaCotacao)
            {
                bool achou = false;
                string cdAtivoBolsa = cotacaoBloomberg.CdAtivo;
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                {
                    achou = true;
                }

                if (achou)
                {
                    CotacaoBolsa cotacaoBolsaDeletar = new CotacaoBolsa();
                    if (cotacaoBolsaDeletar.LoadByPrimaryKey(cotacaoBloomberg.Data, cdAtivoBolsa))
                    {
                        cotacaoBolsaDeletar.MarkAsDeleted();
                        cotacaoBolsaDeletar.Save();
                    }

                    CotacaoBolsa cotacaoBolsa = cotacaoBolsaCollection.AddNew();
                    cotacaoBolsa.Data = cotacaoBloomberg.Data;
                    cotacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    cotacaoBolsa.PUAbertura = cotacaoBloomberg.Pu;
                    cotacaoBolsa.PUFechamento = cotacaoBloomberg.Pu;
                    cotacaoBolsa.PUMedio = cotacaoBloomberg.Pu;
                }
            }

            // Salva Todas as Cotações da Collection           
            try
            {
                cotacaoBolsaCollection.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        /// <summary>
        /// Método de inserção de CotacaoBolsa
        /// </summary>
        /// <param name="cotacaoBolsa">cotacao com os dados a inserir</param>
        /// throws ArgumentNullException se campos Obrigatórios da inclusão estiverem nulos
        /// cotacaoBolsa.Data ou cotacaoBolsa.CdAtivoBolsa ou cotacaoBolsa.PUMedio ou
        /// cotacaoBolsa.PUFechamento ou cotacaoBolsa.PUAbertura
        public void InsereCotacaoBolsa(CotacaoBolsa cotacaoBolsa)
        {
            #region ArgumentosNulos - throw ArgumentNullException
            if (!cotacaoBolsa.Data.HasValue ||
               String.IsNullOrEmpty(cotacaoBolsa.CdAtivoBolsa) ||
               !cotacaoBolsa.PUMedio.HasValue ||
               !cotacaoBolsa.PUFechamento.HasValue ||
               !cotacaoBolsa.PUAbertura.HasValue)
            {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("cotacaoBolsa.Data ou ")
                .Append("cotacaoBolsa.CdAtivoBolsa ou ")
                .Append("cotacaoBolsa.PUMedio ou ")
                .Append("cotacaoBolsa.PUFechamento ou ")
                .Append("cotacaoBolsa.PUAbertura");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            CotacaoBolsa cotacaoBolsaInserir = new CotacaoBolsa();
            //            
            // Campos Obrigatórios para a inserção
            cotacaoBolsaInserir.Data = cotacaoBolsa.Data;
            cotacaoBolsaInserir.CdAtivoBolsa = cotacaoBolsa.CdAtivoBolsa;
            cotacaoBolsaInserir.PUMedio = cotacaoBolsa.PUMedio;
            cotacaoBolsaInserir.PUFechamento = cotacaoBolsa.PUFechamento;
            cotacaoBolsaInserir.PUAbertura = cotacaoBolsa.PUAbertura;
            //    
            cotacaoBolsaInserir.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="idTitulo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaCotacaoAjustada(string cdAtivoBolsa, DateTime data, DateTime dataHistorico)
        {
            return this.RetornaCotacaoAjustada(cdAtivoBolsa, data, data, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="idTitulo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaCotacaoAjustada(string cdAtivoBolsa, DateTime data, DateTime dataHistorico, bool apenasFisicos)
        {
            decimal pu = 0;

            CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
            cotacaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa);

            if (cotacaoBolsa.PUFechamento.HasValue && cotacaoBolsa.PUFechamento.Value != 0)
            {
                pu = cotacaoBolsa.PUFechamento.Value;

                if (!apenasFisicos)
                {
                    ProventoBolsa ProventoBolsa = new ProventoBolsa();
                    ProventoBolsa.Query.Select(ProventoBolsa.Query.Valor.Sum())
                                .Where(ProventoBolsa.Query.DataEx <= data &
                                       ProventoBolsa.Query.DataEx > dataHistorico &
                                       ProventoBolsa.Query.CdAtivoBolsa == cdAtivoBolsa);
                    ProventoBolsa.Query.Load();

                    if (ProventoBolsa.Valor.HasValue)
                    {
                        pu += ProventoBolsa.Valor.Value;
                    }
                }

                GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
                grupamentoBolsaCollection.Query.Select(grupamentoBolsaCollection.Query.FatorPU,
                                                       grupamentoBolsaCollection.Query.TipoGrupamento);
                grupamentoBolsaCollection.Query.Where(grupamentoBolsaCollection.Query.DataEx <= data &
                                                      grupamentoBolsaCollection.Query.DataEx > dataHistorico &
                                                      grupamentoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);
                grupamentoBolsaCollection.Query.Load();

                foreach (GrupamentoBolsa grupamentoBolsa in grupamentoBolsaCollection)
                {
                    byte tipoGrupamento = grupamentoBolsa.TipoGrupamento.Value;
                    decimal fatorPU = grupamentoBolsa.FatorPU.Value;

                    if (tipoGrupamento == (byte)TipoGrupamentoBolsa.Grupamento)
                    {
                        pu = pu / fatorPU;
                    }
                    else
                    {
                        pu = pu * fatorPU;
                    }
                }

            }

            return pu;
        }
    }
}
