using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;

namespace Financial.Bolsa {
	public partial class OperacaoEmprestimoBolsaCollection : esOperacaoEmprestimoBolsaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(OperacaoEmprestimoBolsaCollection));
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BuscaOperacaoEmprestimoBolsaCompleta(int idCliente, DateTime dataRegistro) 
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataRegistro.Equal(dataRegistro));
            this.Query.Load();            
        }
       
        /// <summary>
        /// Deleta as Operacoes Emprestimo por data e FonteEmprestimoBolsa = fonte.
        /// Deleta apenas para clientes com dataDia menor que dataRegistro ou (dataDia = dataRegistro e Status != Fechado).
        /// </summary>
        /// <param name="data"></param>
        public void DeletaOperacaoEmprestimoBolsa(DateTime dataRegistro, FonteEmprestimoBolsa fonte) 
        {
            OperacaoEmprestimoBolsaQuery operacaoEmprestimoBolsaQuery = new OperacaoEmprestimoBolsaQuery("O");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            operacaoEmprestimoBolsaQuery.Select(operacaoEmprestimoBolsaQuery.IdOperacao);
            operacaoEmprestimoBolsaQuery.InnerJoin(clienteQuery).On(operacaoEmprestimoBolsaQuery.IdCliente == clienteQuery.IdCliente);
            operacaoEmprestimoBolsaQuery.Where(operacaoEmprestimoBolsaQuery.DataRegistro.Equal(dataRegistro) &
                                            operacaoEmprestimoBolsaQuery.Fonte.Equal((byte)fonte) &
                                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &
                                            (
                                                clienteQuery.DataDia.LessThan(dataRegistro) |
                                                (clienteQuery.DataDia.Equal(dataRegistro) & clienteQuery.Status.NotEqual((byte)StatusCliente.Divulgado))
                                            )
                                            );

            OperacaoEmprestimoBolsaCollection operacaoEmprestimoBolsaCollection = new OperacaoEmprestimoBolsaCollection();
            operacaoEmprestimoBolsaCollection.Load(operacaoEmprestimoBolsaQuery);

            operacaoEmprestimoBolsaCollection.MarkAllAsDeleted();
            operacaoEmprestimoBolsaCollection.Save();
        }

        public void DeletaOperacaoEmprestimoBolsa(int idCliente, DateTime dataRegistro, FonteEmprestimoBolsa fonte)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdOperacao)
              .Where(this.Query.DataRegistro.Equal(dataRegistro),
                     this.Query.IdCliente.Equal(idCliente),
                     this.Query.Fonte.Equal(fonte));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
	}
}
