﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Investidor;
using Financial.Bolsa.Enums;
using Financial.Common.Enums;

namespace Financial.Bolsa {
    public partial class TransferenciaBolsa : esTransferenciaBolsa {
        private static readonly ILog log = LogManager.GetLogger(typeof(TransferenciaBolsa));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente">identitificador do cliente</param>
        /// <param name="data">Data da transferencia</param>
        public void ProcessaTransferenciaAgenteCustodia(int idCliente, DateTime data) 
        {                       
            TransferenciaBolsaCollection transferenciaBolsaCollection = new TransferenciaBolsaCollection();
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();

            // Apaga as operações com origem = transferencia de custodia
            List<int> origemOperacaoBolsa = new List<int>();
            origemOperacaoBolsa.Add(OrigemOperacaoBolsa.TransferenciaCorretora);
            operacaoBolsaCollection.DeletaOperacaoBolsa(idCliente, origemOperacaoBolsa, data); 
            transferenciaBolsaCollection.BuscaTransferenciaBolsa(idCliente, data);
            for (int i = 0; i < transferenciaBolsaCollection.Count; i++) 
            {                
                TransferenciaBolsa transferenciaBolsa = (TransferenciaBolsa)transferenciaBolsaCollection[i];

                decimal puCusto = 0;
                if (transferenciaBolsa.Pu.HasValue)
                {
                    puCusto = transferenciaBolsa.Pu.Value;
                }
                else
                {
                    PosicaoBolsaAbertura posicaoBolsaAbertura = new PosicaoBolsaAbertura();
                    posicaoBolsaAbertura.Query.Select(posicaoBolsaAbertura.Query.PUCustoLiquido);
                    posicaoBolsaAbertura.Query.Where(posicaoBolsaAbertura.Query.IdCliente.Equal(idCliente),
                                             posicaoBolsaAbertura.Query.DataHistorico.Equal(data),
                                             posicaoBolsaAbertura.Query.IdAgente.Equal(transferenciaBolsa.IdAgenteOrigem.Value),
                                             posicaoBolsaAbertura.Query.CdAtivoBolsa.Equal(transferenciaBolsa.CdAtivoBolsa));
                    if (posicaoBolsaAbertura.Query.Load())
                    {
                        puCusto = posicaoBolsaAbertura.PUCustoLiquido.Value;
                    }
                }
                
   
                operacaoBolsa.AddNew();
                operacaoBolsa.TipoMercado = transferenciaBolsa.UpToAtivoBolsaByCdAtivoBolsa.TipoMercado;

                if (transferenciaBolsa.Quantidade > 0)
                {
                    operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Retirada;
                }
                else
                {
                    operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito ;
                }
                
                operacaoBolsa.Data = data;
                operacaoBolsa.DataOperacao = data;//data operação = data registro(com orientação do braga)
                operacaoBolsa.Pu = puCusto;
                operacaoBolsa.Quantidade = Math.Abs(transferenciaBolsa.Quantidade.Value);
                operacaoBolsa.CdAtivoBolsa = transferenciaBolsa.CdAtivoBolsa;
                operacaoBolsa.IdAgenteCorretora = transferenciaBolsa.IdAgenteOrigem;
                operacaoBolsa.IdAgenteLiquidacao = transferenciaBolsa.IdAgenteOrigem;
                //
                operacaoBolsa.PULiquido = puCusto;
                operacaoBolsa.Valor = puCusto * Math.Abs(transferenciaBolsa.Quantidade.Value);                
                operacaoBolsa.DataLiquidacao = data;
                operacaoBolsa.Origem = (byte) OrigemOperacaoBolsa.TransferenciaCorretora;
                operacaoBolsa.PercentualDesconto = 0;
                operacaoBolsa.Fonte = (byte) FonteOperacaoBolsa.Interno;
                operacaoBolsa.IdLocal = LocalFeriadoFixo.Bovespa;
                operacaoBolsa.IdCliente = idCliente;
                operacaoBolsa.CalculaDespesas = "N";
                operacaoBolsa.IdMoeda = (int)ListaMoedaFixo.Real;
                operacaoBolsa.IdLocalCustodia = (int)LocalCustodiaFixo.CBLC;
                operacaoBolsa.IdClearing = (int)ClearingFixo.BOVESPA;
                operacaoBolsa.IdLocalNegociacao = (int)LocalNegociacaoFixo.BOVESPA;
                                                    
                // insere uma operação de ponta contrária
                operacaoBolsa.InsereOperacaoBolsa(operacaoBolsa);                
                if (transferenciaBolsa.Quantidade > 0)
                {
                    operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Deposito;
                }
                else
                {
                    operacaoBolsa.TipoOperacao = TipoOperacaoBolsa.Retirada;
                }

                operacaoBolsa.IdAgenteCorretora = transferenciaBolsa.IdAgenteDestino;
                operacaoBolsa.IdAgenteLiquidacao = transferenciaBolsa.IdAgenteDestino;
                operacaoBolsa.InsereOperacaoBolsa(operacaoBolsa);
            }            
        }
    }
}
