﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Bolsa.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Financial.Util;
using Financial.Bolsa.Exceptions;
using System.Reflection;
using System.Security;
using Financial.Interfaces.Import.Bolsa;
using Financial.Interfaces.Import.Bolsa.Enums;
using System.Web;
using Financial.Interfaces.Sinacor;
using Financial.Common.Enums;
using Financial.Common;
using Financial.Fundo;

namespace Financial.Bolsa {
    public partial class AtivoBolsa : esAtivoBolsa {
        
        /// <summary>
        /// Dada a data passada, retorna a data de vcto da opção (terceira 2a feira do mês), seja do próprio ano, seja do ano seguinte.
        /// </summary>
        /// <param name="dataOperacao"></param>
        /// <param name="dataVencimento"></param>
        /// <returns></returns>
        public DateTime RetornaDataVencimentoOpcao(DateTime dataOperacao, DateTime dataVencimento)
        {
            int ano = dataOperacao.Year;
            int mes = dataVencimento.Month;

            DateTime primeiroDiaMes = new DateTime(ano, mes, 01);
            DateTime dataVencimentoOpcao = Calendario.RetornaSegundaFeira(primeiroDiaMes, 3); //terceira 2a feira do mês

            if (dataOperacao > dataVencimentoOpcao) //Se operou e a data no mesmo ano já venceu, é pq a opção é do ano seguinte
            {
                DateTime primeiroDiaMesAnoSeguinte = new DateTime(ano + 1, mes, 01);
                dataVencimentoOpcao = Calendario.RetornaSegundaFeira(primeiroDiaMesAnoSeguinte, 3); //terceira 2a feira do mês
            }
            else if (dataOperacao.Year == DateTime.Now.Year) //Se estiver operando no ano corrente (Now) assume o vencimento do cadastro do ativo
            {
                dataVencimentoOpcao = dataVencimento;
            }

            if (!Calendario.IsDiaUtil(dataVencimentoOpcao, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
            {
                dataVencimentoOpcao = Calendario.AdicionaDiaUtil(dataVencimentoOpcao, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            }

            return dataVencimentoOpcao;
        }

        /// <summary>
        /// Dada a data passada, retorna a data de vcto da opção, somente IBOV, (4a feira mais próxima do dia 15), 
        /// seja do próprio ano, seja do ano seguinte.
        /// </summary>
        /// <param name="dataOperacao"></param>
        /// <param name="dataVencimento"></param>
        /// <returns></returns>
        public DateTime RetornaDataVencimentoOpcaoIBOV(DateTime dataOperacao, DateTime dataVencimento)
        {
            int ano = dataOperacao.Year;
            int mes = dataVencimento.Month;

            DateTime dataVencimentoOpcao = this.Retorna4aFeiraProximaDia15(ano, mes);

            if (DateTime.Compare(dataOperacao, dataVencimentoOpcao) > 0) //Se operou e a data no mesmo ano já venceu, é pq a opção é do ano seguinte
            {
                dataVencimentoOpcao = this.Retorna4aFeiraProximaDia15(ano + 1, mes);
            }

            if (!Calendario.IsDiaUtil(dataVencimentoOpcao, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
            {
                dataVencimentoOpcao = Calendario.AdicionaDiaUtil(dataVencimentoOpcao, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            }

            return dataVencimentoOpcao;
        }

        private DateTime Retorna4aFeiraProximaDia15(int ano, int mes)
        {
            DateTime dataRetorno = new DateTime();

            DateTime dia15 = new DateTime(ano, mes, 15);

            int contProx = 0;
            DateTime dataProx = dia15;
            while (dataProx.DayOfWeek != DayOfWeek.Wednesday)
            {
                dataProx = dataProx.AddDays(1);
                contProx += 1;
            }

            int contAnt = 0;
            DateTime dataAnt = dia15;
            while (dataAnt.DayOfWeek != DayOfWeek.Wednesday)
            {
                dataAnt = dataAnt.AddDays(-1);
                contAnt += 1;
            }

            if (contProx < contAnt)
            {
                dataRetorno = dataProx;
            }
            else
            {
                dataRetorno = dataAnt;
            }

            return dataRetorno;
        }

        /// <summary>
        /// Busca codigo do Ativo dado um codigo Isin onde tipoMercado = Vista.
        /// Se achar mais de 1 ativo com o mesmo código ISIN, pega o 1o da lista.
        /// </summary>
        /// <param name="codigoIsin">CodigoIsin</param>
        /// <returns>true se achou registro, false se não achou</returns>
        public bool BuscaCdAtivoBolsa(string codigoIsin) 
        {
            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa,
                                              ativoBolsaCollection.Query.TipoMercado);
            ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista,
                                                                                       TipoMercadoBolsa.Imobiliario), 
                                             ativoBolsaCollection.Query.CodigoIsin == codigoIsin);
            ativoBolsaCollection.Query.Load();


            bool retornou = ativoBolsaCollection.HasData;

            if (retornou)
            {
                this.CdAtivoBolsa = ativoBolsaCollection[0].CdAtivoBolsa;
                this.TipoMercado = ativoBolsaCollection[0].TipoMercado;
            }
           
            return retornou;
        }

        /// <summary>
        /// Busca codigo do Ativo pela descricao da empresa.
        /// </summary>
        /// <param name="codigoIsin">CodigoIsin</param>
        /// <returns>true se achou registro, false se não achou</returns>
        public bool BuscaCdAtivoBolsaPorDescricao(string descricao)
        {
            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa);
            ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.Descricao.Equal(descricao));
            ativoBolsaCollection.Query.Load();


            bool retornou = ativoBolsaCollection.HasData;

            if (retornou)
            {
                this.CdAtivoBolsa = ativoBolsaCollection[0].CdAtivoBolsa;
            }

            return retornou;
        }

        /// <summary>
        /// Retorna o código do ativo objeto. Em caso de estar NULL, retorna "".        
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns>Código do ativo objeto</returns>
        /// <exception cref="AtivoNaoCadastradoException">Se não for encontrado registro na consulta</exception>
        public string RetornaCdAtivoBolsaObjeto(string cdAtivoBolsa)
        {
            AtivoBolsaCollection ativo = new AtivoBolsaCollection();

            this.QueryReset();
            this.Query
                 .Select(this.Query.CdAtivoBolsaObjeto)
                 .Where(this.Query.CdAtivoBolsa == cdAtivoBolsa);

            if (!this.Query.Load())
            {
                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Ativo não cadastrado - " + cdAtivoBolsa);
                throw new AtivoNaoCadastradoException(mensagem.ToString());
            }
            else
            {
                return String.IsNullOrEmpty(this.CdAtivoBolsaObjeto) ? "" : this.CdAtivoBolsaObjeto;                               
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nomeArquivo">Nome do Arquivo Completo</param>
        /// <param name="data"></param>
        /// <exception cref="AtivoNaoCadastradoException">
        /// throws AtivoNaoCadastradoException se codigo do Ativo não estiver cadastrado na Base
        /// </exception>
        public void CarregaAtivoPapd(PapdCollection papdCollection)
        {
            Papd papd = new Papd();
            //  

            int? idEstrategia = null;
            EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
            estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
            estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Ações%"));
            estrategiaCollection.Query.Load();

            if (estrategiaCollection.Count > 0)
            {
                idEstrategia = estrategiaCollection[0].IdEstrategia.Value;
            }

            DateTime dataPregao = Calendario.SubtraiDiaUtil(papdCollection.CollectionPapd[0].DataPregao.Value, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            for (int i = 0; i < papdCollection.CollectionPapd.Count; i++)
            {
                papd = papdCollection.CollectionPapd[i];

                if (this.ProcessaPapd(papd))
                {
                    #region Copia informações para AtivoBolsa
                    AtivoBolsa ativoBolsa = new AtivoBolsa();
                    //
                    if (papd.isTipoRegistroDadosPapeis())
                    {
                        ativoBolsa.CdAtivoBolsa = !String.IsNullOrEmpty(papd.CdAtivoBolsa) ? papd.CdAtivoBolsa : "";
                        ativoBolsa.Descricao = !String.IsNullOrEmpty(papd.NomeEmpresa) ? papd.NomeEmpresa : "";
                        //
                        // Se tamanho = 4, CodigoAtivo recebe o numero da especificacao
                        if (ativoBolsa.CdAtivoBolsa.Length == 4)
                        {
                            // tres primeiras letras de especificacao ou duas primeiras letras se a ultima for em branco
                            string especificacao = papd.Especificacao;
                            if (especificacao.Length >= 3)
                                especificacao = especificacao.Substring(0, 3).Trim();
                            else
                            {
                                especificacao = especificacao.Substring(0, 2).Trim();
                            }
                            string numeroEspecificacao = papd.ConverteEspecificacao(especificacao);
                            if (!numeroEspecificacao.Equals(EspecificacaoAtivo.EspecificacaoInvalida))
                            {
                                ativoBolsa.CdAtivoBolsa = ativoBolsa.CdAtivoBolsa + numeroEspecificacao;
                            }
                        }

                        // Se Codigo do Ativo é leilao ou fracionario
                        ativoBolsa.CdAtivoBolsa = this.RetornaCdAtivoBolsaConvertido(ativoBolsa.CdAtivoBolsa);

                        // Se for mercadoVista, dataVencimento = null                        
                        DateTime? dataVencimento = papd.DataVencimento;
                        if (papd.ConverteTipoMercadoPapd(papd.TipoMercado.Value) == TipoMercadoBolsa.MercadoVista)
                        {
                            dataVencimento = null;
                        }

                        // Se for mercado Termo, codigoAtivo é concatenado com data de vencimento 
                        // se a data de vencimento for valida (diferente de 31/12/4000)
                        if (papd.ConverteTipoMercadoPapd(papd.TipoMercado.Value) == TipoMercadoBolsa.Termo)
                        {
                            continue;
                        }

                        /* Verifica se registro existe 
                         * Se registro existe = update
                         * Se registro não existe = insert
                         */

                        int idEmissor = 0;
                        string descricaoSetor = papd.DescricaoSetor;
                        string[] descricaoSplit = descricaoSetor.Split(new Char[] { '/' });
                        foreach (string item in descricaoSplit)
                        {
                            descricaoSetor = item;
                        }
                        #region Trata setor de atividade, vinculado a um emissor (ações busca pelo papt, p/ o resto pega o emissor do ativo objeto
                        if (papd.ConverteTipoMercadoPapd(papd.TipoMercado.Value) != TipoMercadoBolsa.MercadoVista)
                        {
                            if (!String.IsNullOrEmpty(papd.CdAtivoObjeto))
                            {
                                AtivoBolsa ativoObjeto = new AtivoBolsa();
                                if (ativoObjeto.LoadByPrimaryKey(papd.CdAtivoObjeto))
                                {
                                    idEmissor = ativoObjeto.IdEmissor.Value;
                                }
                                else
                                {
                                    EmissorCollection emissorCollection = new EmissorCollection();
                                    emissorCollection.Query.Select(emissorCollection.Query.IdEmissor);
                                    emissorCollection.Query.Where(emissorCollection.Query.Nome.In("OUTROS", "Outros"));
                                    emissorCollection.Query.Load();

                                    if (emissorCollection.Count != 0)
                                    {
                                        idEmissor = emissorCollection[0].IdEmissor.Value;
                                    }
                                    else
                                    {
                                        Setor setorNovo = new Setor();
                                        setorNovo.Nome = "OUTROS";
                                        setorNovo.Save();

                                        Emissor emissorNovo = new Emissor();
                                        emissorNovo.IdSetor = setorNovo.IdSetor.Value;
                                        emissorNovo.Nome = "OUTROS";
                                        emissorNovo.TipoEmissor = (byte)TipoEmissor.PessoaJuridica;
                                        emissorNovo.Save();

                                        idEmissor = emissorNovo.IdEmissor.Value;
                                    }
                                }
                            }
                            else
                            {
                                EmissorCollection emissorCollection = new EmissorCollection();
                                emissorCollection.Query.Select(emissorCollection.Query.IdEmissor);
                                emissorCollection.Query.Where(emissorCollection.Query.Nome.In("OUTROS", "Outros"));
                                emissorCollection.Query.Load();

                                if (emissorCollection.Count != 0)
                                {
                                    idEmissor = emissorCollection[0].IdEmissor.Value;
                                }
                                else
                                {
                                    Setor setorNovo = new Setor();
                                    setorNovo.Nome = "OUTROS";
                                    setorNovo.Save();

                                    Emissor emissorNovo = new Emissor();
                                    emissorNovo.IdSetor = setorNovo.IdSetor.Value;
                                    emissorNovo.Nome = "OUTROS";
                                    emissorNovo.TipoEmissor = (byte)TipoEmissor.PessoaJuridica;
                                    emissorNovo.Save();

                                    idEmissor = emissorNovo.IdEmissor.Value;
                                }
                            }
                        }
                        else
                        {
                            EmissorCollection emissorCollection = new EmissorCollection();
                            emissorCollection.Query.Select(emissorCollection.Query.IdEmissor);
                            emissorCollection.Query.Where(emissorCollection.Query.Nome.Like(papd.NomeEmpresa + "%"));
                            emissorCollection.Query.Load();

                            if (emissorCollection.Count != 0)
                            {
                                idEmissor = emissorCollection[0].IdEmissor.Value;
                            }
                            else
                            {
                                SetorCollection setorCollection = new SetorCollection();
                                setorCollection.Query.Select(setorCollection.Query.IdSetor);
                                setorCollection.Query.Where(setorCollection.Query.Nome.Like(descricaoSetor + "%"));
                                setorCollection.Query.Load();

                                short idSetor = 0;
                                if (setorCollection.Count != 0)
                                {
                                    idSetor = setorCollection[0].IdSetor.Value;
                                }
                                else
                                {
                                    Setor setorNovo = new Setor();
                                    setorNovo.Nome = descricaoSetor;
                                    setorNovo.Save();

                                    idSetor = setorNovo.IdSetor.Value;
                                }

                                Emissor emissorNovo = new Emissor();
                                emissorNovo.IdSetor = idSetor;
                                emissorNovo.Nome = papd.NomeEmpresa;

                                if (papd.DescricaoSetor.Contains("BANCO"))
                                {
                                    emissorNovo.TipoEmissor = (byte)TipoEmissor.InstituicaoFinanceira;
                                }
                                else
                                {
                                    emissorNovo.TipoEmissor = (byte)TipoEmissor.PessoaJuridica;
                                }
                                emissorNovo.Save();

                                idEmissor = emissorNovo.IdEmissor.Value;
                            }

                        }
                        #endregion


                        AtivoBolsa ativoBolsaAux = new AtivoBolsa();
                        // Update
                        if (ativoBolsaAux.LoadByPrimaryKey(ativoBolsa.CdAtivoBolsa))
                        {
                            #region Update
                            string especificacaoUpdate = papd.Especificacao;

                            if (especificacaoUpdate == "")
                            {
                                especificacaoUpdate = "*"; //Para não dar erro!
                            }
                            else
                            {
                                if (especificacaoUpdate.Length >= 3)
                                    especificacaoUpdate = especificacaoUpdate.Substring(0, 3).Trim();
                                else
                                {
                                    especificacaoUpdate = especificacaoUpdate.Substring(0, 2).Trim();
                                }
                            }

                            ativoBolsaAux.Especificacao = especificacaoUpdate;
                            ativoBolsaAux.Descricao = papd.NomeEmpresa;

                            if (papd.isIndicadorMercadoCertificadoInvestimentoFundoImobilario())
                            {
                                ativoBolsaAux.TipoMercado = TipoMercadoBolsa.Imobiliario;
                            }
                            else
                            {
                                ativoBolsaAux.TipoMercado = papd.ConverteTipoMercadoPapd(papd.TipoMercado.Value);
                            }
                            
                            ativoBolsaAux.PUExercicio = papd.PrecoExercicio != 0 ? papd.PrecoExercicio : null;
                            ativoBolsaAux.DataVencimento = dataVencimento;
                            ativoBolsaAux.CdAtivoBolsaObjeto = !String.IsNullOrEmpty(papd.CdAtivoObjeto) ? papd.CdAtivoObjeto : null;
                            ativoBolsaAux.CodigoIsin = !String.IsNullOrEmpty(papd.CodigoIsin) ? papd.CodigoIsin : "";
                            ativoBolsaAux.OpcaoIndice = (Int16)papd.IndicadorOpcao;
                            ativoBolsaAux.IdEmissor = idEmissor;
                            ativoBolsaAux.NumeroSerie = papd.NumeroSerie;
                            // Valores fixos
                            ativoBolsaAux.IdMoeda = (short)ListaMoedaFixo.Real;
                            //
                            // Update do Ativo                            
                            ativoBolsaAux.Save();
                            #endregion
                        }
                        // Insert
                        else
                        {
                            #region Insert
                            string especificacaoInsert = papd.Especificacao;

                            if (especificacaoInsert == "")
                            {
                                especificacaoInsert = "*"; //Para não dar erro!
                            }
                            else
                            {
                                if (especificacaoInsert.Length >= 3)
                                    especificacaoInsert = especificacaoInsert.Substring(0, 3).Trim();
                                else
                                {
                                    especificacaoInsert = especificacaoInsert.Substring(0, 2).Trim();
                                }
                            }
                            //
                            ativoBolsa.Especificacao = especificacaoInsert;
                            ativoBolsa.Descricao = papd.NomeEmpresa;

                            if (papd.isIndicadorMercadoCertificadoInvestimentoFundoImobilario())
                            {
                                ativoBolsa.TipoMercado = TipoMercadoBolsa.Imobiliario;
                            }
                            else
                            {
                                ativoBolsa.TipoMercado = papd.ConverteTipoMercadoPapd(papd.TipoMercado.Value);
                            }

                            ativoBolsa.PUExercicio = papd.PrecoExercicio != 0 ? papd.PrecoExercicio : null;
                            ativoBolsa.DataVencimento = dataVencimento;
                            ativoBolsa.CdAtivoBolsaObjeto = !String.IsNullOrEmpty(papd.CdAtivoObjeto) ? papd.CdAtivoObjeto : null;
                            ativoBolsa.CodigoIsin = !String.IsNullOrEmpty(papd.CodigoIsin) ? papd.CodigoIsin : "";
                            ativoBolsa.OpcaoIndice = (Int16)papd.IndicadorOpcao;
                            // Valores fixos
                            ativoBolsa.IdEmissor = idEmissor;
                            ativoBolsa.IdMoeda = (short)ListaMoedaFixo.Real;
                            ativoBolsa.NumeroSerie = papd.NumeroSerie;
                            ativoBolsa.TipoPapel = (byte)TipoPapelAtivo.Normal;
                            ativoBolsa.IdEstrategia = idEstrategia;                            
                            //

                            /* Inclui na collection somente se CodigoAtivo for maior que 4
                            e não inclui Termo com data Invalida */
                            if (ativoBolsa.CdAtivoBolsa.Length > 4)
                            {
                                // Insert do Ativo
                                ativoBolsa.Save();
                            }
                            #endregion
                        }
                    }
                    //
                    #endregion

                    #region Copia informações para FatorCotacao
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    //
                    if (papd.isTipoRegistroDadosPapeis())
                    {
                        string cdAtivo = ativoBolsa.CdAtivoBolsa;

                        // Somente inclui se codigo do Ativo for maior que 4 e termo não for invalido
                        //
                        if (cdAtivo.Length > 4 && this.IsTermoDataValida(papd))
                        {
                            DateTime dataFator = papd.DataInicioFatorCotacao.Value;

                            /* Verifica se registro existe 
                             * Se registro existe = update
                             * Se registro não existe = insert
                             */
                            FatorCotacaoBolsa fatorCotacaoBolsaAux = new FatorCotacaoBolsa();
                            // Update
                            if (fatorCotacaoBolsaAux.LoadByPrimaryKey(dataFator, cdAtivo))
                            {
                                #region Update
                                fatorCotacaoBolsaAux.Fator = (decimal)papd.FatorCotacao.Value;
                                //
                                //Update
                                fatorCotacaoBolsaAux.Save();
                                #endregion
                            }
                            // Insert
                            else
                            {
                                #region Insert
                                fatorCotacaoBolsa.DataReferencia = dataFator;
                                fatorCotacaoBolsa.CdAtivoBolsa = cdAtivo;
                                fatorCotacaoBolsa.Fator = (decimal)papd.FatorCotacao.Value;
                                //                                                        
                                fatorCotacaoBolsa.Save();
                                #endregion
                            }
                        }
                    }
                    //
                    #endregion

                    #region Copia informações para DistribuicaoBolsa
                    DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();
                    //
                    if (papd.isTipoRegistroDadosPapeis())
                    {
                        string cdAtivo = ativoBolsa.CdAtivoBolsa;

                        // Somente inclui se codigo do Ativo for maior que 4 e termo não for invalido
                        //
                        if (cdAtivo.Length > 4 && papd.DataInicioNegociacao.HasValue)
                        {
                            DateTime dataInicioNegociacao = papd.DataInicioNegociacao.Value;

                            /* Verifica se registro existe 
                             * Se registro existe = update
                             * Se registro não existe = insert
                             */
                            DistribuicaoBolsa distribuicaoBolsaAux = new DistribuicaoBolsa();
                            // Update
                            if (distribuicaoBolsaAux.LoadByPrimaryKey(dataInicioNegociacao, cdAtivo))
                            {
                                #region Update
                                distribuicaoBolsaAux.Distribuicao = papd.Distribuicao.Value;
                                //
                                //Update
                                distribuicaoBolsaAux.Save();
                                #endregion
                            }
                            // Insert
                            else
                            {
                                #region Insert
                                distribuicaoBolsa.DataReferencia = dataInicioNegociacao;
                                distribuicaoBolsa.CdAtivoBolsa = cdAtivo;
                                distribuicaoBolsa.Distribuicao = papd.Distribuicao.Value;
                                //                                                        
                                distribuicaoBolsa.Save();
                                #endregion
                            }
                        }
                    }
                    //
                    #endregion

                    #region Importa cotações de ativos de renda fixa (CRIs e debentures)
                    if (papd.isTipoRegistroDadosPapeis() && 
                            (papd.CodigoIsinTipoAtivo == "CRI" || 
                             papd.CodigoIsinTipoAtivo == "DBO" ||
                             papd.CodigoIsinTipoAtivo == "DBP" ||
                             papd.CodigoIsinTipoAtivo == "DBS" ||
                             papd.CodigoIsinTipoAtivo == "NPM"))
                    {
                        if (papd.PrecoFechamentoUltimo.Value != 0 || papd.PrecoMedioUltimo.Value != 0)
                        {
                            CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                            if (!cotacaoBolsa.LoadByPrimaryKey(dataPregao, this.RetornaCdAtivoBolsaConvertido(papd.CdAtivoBolsa)))
                            {
                                cotacaoBolsa = new CotacaoBolsa();
                                cotacaoBolsa.CdAtivoBolsa = this.RetornaCdAtivoBolsaConvertido(papd.CdAtivoBolsa);
                                cotacaoBolsa.Data = dataPregao;
                                cotacaoBolsa.PUAbertura = 0;
                            }                            
                            
                            cotacaoBolsa.PUFechamento = papd.PrecoFechamentoUltimo.Value;
                            cotacaoBolsa.PUMedio = papd.PrecoMedioUltimo.Value;
                            cotacaoBolsa.Save();
                        }
                    }
                    #endregion
                }
            }
        }
        
        /// <summary>
        /// Somente processa o arquivo papd se indicador Mercado for diferente de:
        /// FundoInvestimentoDireitoCreditorio = 21,
        /// FundoInvestimentoParticipacao = 22,
        /// Precatorio = 12,
        /// CertificadoInvestimentoFundoImobilario = 4,
        /// CertificadoAudioVisual = 6,
        /// ou TipoAtivo for diferente de:
        /// CRI, DBM, DBO, DBP, DBS
        /// </summary>
        /// <returns>bool indicando se deve processar o arquivo papd</returns>
        private bool ProcessaPapd(Papd papd) 
        {
            bool retorno = true;

            // Exclui pelo IndicadorMercado
            #region Exclui pelo IndicadorMercado
            if (papd.isIndicadorMercadoFundoInvestimentoDireitoCreditorio() ||
                papd.isIndicadorMercadoPrecatorio() ||
                papd.isIndicadorMercadoCertificadoAudioVisual()) {

                retorno = false;
            }
            #endregion

            // Exclui pelo TipoAtivo
            #region Exclui pelo TipoAtivo
            if (papd.CodigoIsinTipoAtivo == TipoAtivo.DBM) {

                retorno = false;
            }
            #endregion

            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns>bool indicando se codigo do ativo é fracionario</returns>
        private bool IsCdAtivoBolsaFracionario(string cdAtivoBolsa) 
        {
            return Utilitario.Right(cdAtivoBolsa, 1).Equals("F");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns>bool indicando se codigo do ativo é Leilao</returns>
        private bool IsCdAtivoBolsaLeilao(string cdAtivoBolsa) 
        {   
            return Utilitario.Right(cdAtivoBolsa, 1).Equals("L");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns>bool indicando se codigo do ativo é do Soma</returns>
        private bool IsCdAtivoBolsaSoma(string cdAtivoBolsa) 
        {
            return Utilitario.Right(cdAtivoBolsa, 1).Equals("B");
        }

        /// <summary>
        /// Remove o L ou o F se for Soma ou Leilao
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <returns></returns>
        public string RetornaCdAtivoBolsaConvertido(string cdAtivoBolsa) 
        {
            if (!String.IsNullOrEmpty(cdAtivoBolsa)) {
                if (this.IsCdAtivoBolsaFracionario(cdAtivoBolsa) ||
                    this.IsCdAtivoBolsaLeilao(cdAtivoBolsa)) {

                    int posicao = cdAtivoBolsa.Length - 1;
                    cdAtivoBolsa = cdAtivoBolsa.Remove(posicao);
                }
            }

            return cdAtivoBolsa;
        }

        /// <summary>
        /// Função usada em ativos com mercado = Termo.
        /// Retorna o cdAtivoBolsa convertido, incluindo a data de Vencimento ao final.
        /// Pressupõe que o cdAtivoBolsa é passado com T ao final.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string RetornaCdAtivoBolsaConcatenado(string cdAtivoBolsa, DateTime dataVencimento) 
        {
            StringBuilder cdAtivoBolsaConcatenado = new StringBuilder();
            string anoString = Convert.ToString(dataVencimento.Year);
            string mesString = Convert.ToString(dataVencimento.Month);
            string diaString = Convert.ToString(dataVencimento.Day);
            if (dataVencimento.Month < 10)
            {
                mesString = "0" + mesString;
            }
            if (dataVencimento.Day < 10)
            {
                diaString = "0" + diaString;
            }

            cdAtivoBolsaConcatenado.Append(cdAtivoBolsa)
                                    .Append(diaString)
                                    .Append(mesString)
                                    .Append(anoString);

            return cdAtivoBolsaConcatenado.ToString();                   
        }

        /// <summary>
        /// Convert cdAtivoTermo em cdAtivoBolsa
        /// </summary>
        /// <param name="cdAtivoBolsaTermo"></param>
        /// <returns></returns>
        /// throws ArgumentException se cdAtivoBolsaTermo.lenght < 14
        public static string RetornaCdAtivoBolsaAcao(string cdAtivoBolsaTermo) 
        {
            if (cdAtivoBolsaTermo.Length < 14) {                      
                throw new ArgumentException("cdAtivoBolsaTermo deve ter pelo menos 14 letras");
            }
            // Remove a data e o T
            int posicao = cdAtivoBolsaTermo.Length - 9;
            string cdAtivoBolsaAcao = cdAtivoBolsaTermo.Remove(posicao);

            return cdAtivoBolsaAcao;
        }

        /// <summary>
        /// Convert cdAtivoTermo em cdAtivoBolsa
        /// </summary>
        /// <param name="cdAtivoBolsaTermo"></param>
        /// <returns></returns>
        /// throws ArgumentException se cdAtivoBolsaTermo.lenght < 14
        public static DateTime RetornaDataVencimentoTermo(string cdAtivoBolsaTermo)
        {
            if (cdAtivoBolsaTermo.Length < 14)
            {
                throw new ArgumentException("cdAtivoBolsaTermo deve ter pelo menos 14 letras");
            }

            string data = Utilitario.Right(cdAtivoBolsaTermo, 8);

            DateTime dataVencimento = new DateTime(Convert.ToInt32(data.Substring(4, 4)), Convert.ToInt32(data.Substring(2, 2)), Convert.ToInt32(data.Substring(0, 2)));

            return dataVencimento;
        }

        /// <summary>
        /// Retorna o tipo de ativo (ACN, CTF, BDR etc) referente ao código ISIN passado.
        /// </summary>
        /// <param name="codigoISIN"></param>
        /// <returns></returns>
        public static string RetornaTipoAtivoISIN(string codigoISIN)
        {
            string tipo = "";

            if (codigoISIN.Length >= 10)
            {
                tipo = codigoISIN.Substring(6, 3);
            }

            return tipo;
        }

        /// <summary>
        /// Metodo de Insercao do Ativo
        /// </summary>
        /// <param name="ativoBolsa">Ativo com os dados a inserir</param>
        /// <exception cref="ArgumentNullException">
        /// throws ArgumentNullException se campos Obrigatórios da inclusão estiverem nulos
        /// ativoBolsa.CdAtivoBolsa ou ativoBolsa.Especificacao ou ativoBolsa.TipoMercado ou
        /// ativoBolsa.CodigoIsin ou ativoBolsa.IdEmissor ou 
        /// ativoBolsa.IdMoeda ou ativoBolsa.OpcaoIndice        
        /// </exception>
        public void InsereAtivoBolsa(AtivoBolsa ativoBolsa) 
        {
            #region ArgumentosNulos - throw ArgumentNullException
            if (String.IsNullOrEmpty(ativoBolsa.CdAtivoBolsa) ||
               String.IsNullOrEmpty(ativoBolsa.Especificacao) ||
               String.IsNullOrEmpty(ativoBolsa.TipoMercado) ||
               String.IsNullOrEmpty(ativoBolsa.CodigoIsin) ||
               !ativoBolsa.IdEmissor.HasValue ||
               !ativoBolsa.IdMoeda.HasValue ||
               !ativoBolsa.OpcaoIndice.HasValue) {

                StringBuilder mensagem = new StringBuilder();
                mensagem.Append("Falta Preencher ")
                .Append("ativoBolsa.CdAtivoBolsa ou ")
                .Append("ativoBolsa.Especificacao ou ")
                .Append("ativoBolsa.TipoMercado ou ")
                .Append("ativoBolsa.CodigoIsin ou ")
                .Append("ativoBolsa.IdEmissor ou ")
                .Append("ativoBolsa.IdMoeda ou ")
                .Append("ativoBolsa.OpcaoIndice ");

                throw new ArgumentNullException(mensagem.ToString());
            }
            #endregion

            AtivoBolsa ativoBolsaInserir = new AtivoBolsa();
            //            
            // Campos Obrigatórios para a inserção
            ativoBolsaInserir.CdAtivoBolsa = ativoBolsa.CdAtivoBolsa;
            ativoBolsaInserir.Especificacao = ativoBolsa.Especificacao;
            ativoBolsaInserir.TipoMercado = ativoBolsa.TipoMercado;
            ativoBolsaInserir.CodigoIsin = ativoBolsa.CodigoIsin;
            ativoBolsaInserir.IdEmissor = ativoBolsa.IdEmissor;
            ativoBolsaInserir.IdMoeda = ativoBolsa.IdMoeda;
            ativoBolsaInserir.OpcaoIndice = ativoBolsa.OpcaoIndice;
            ativoBolsaInserir.TipoPapel = ativoBolsa.TipoPapel;

            // Campos não obrigatórios - se não for passado coloca valor default            
            ativoBolsaInserir.DataVencimento = ativoBolsa.DataVencimento.HasValue ? ativoBolsa.DataVencimento : null;
            ativoBolsaInserir.PUExercicio = ativoBolsa.PUExercicio.HasValue ? ativoBolsa.PUExercicio : 0;
            ativoBolsaInserir.CdAtivoBolsaObjeto = !String.IsNullOrEmpty(ativoBolsa.CdAtivoBolsaObjeto) ? ativoBolsa.CdAtivoBolsaObjeto : null;            
            //        
            try {
                ativoBolsaInserir.Save();
            }
            catch (Exception e) {
                System.Console.WriteLine(e.StackTrace);
                System.Console.WriteLine(e.Message);
                System.Console.WriteLine("ativoBolsa cdativoBolsa: " + ativoBolsa.CdAtivoBolsa);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="papd"></param>
        /// <returns> bool indicando se a data de vencimento de um termo é Valida</returns>
        private bool IsTermoDataValida(Papd papd) 
        {   
            // Se for mercado Termo e a data de vencimento for invalida (31/12/4000)
            bool termoValido = true;
            if (papd.ConverteTipoMercadoPapd(papd.TipoMercado.Value) == TipoMercadoBolsa.Termo &&
                 !papd.IsValidoDataVencimento) {
                termoValido = false;
            }

            return termoValido;
        }

        /// <summary>
        /// Carrega o ativo a partir da tabela TBOTITULOS do Sinacor.
        /// Salva em this o novo ativo. Grava tb o fator de cotação do ativo.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        public bool IntegraAtivoSinacor(string cdAtivoBolsa, DateTime data)
        {
            bool achou = false;

            TbotitulosCollection tbotitulosCollection = new TbotitulosCollection();
            tbotitulosCollection.BuscaAtivoPorCodigo(cdAtivoBolsa);

            if (tbotitulosCollection.Count > 0)
            {
                achou = true;
                Tbotitulos tbotitulos = tbotitulosCollection[0];

                CarregaAtivo(tbotitulos);
            }

            return achou;
        }

        /// <summary>
        /// Carrega o ativo do Sinacor a partir do código Isin informado.
        /// Filtra por tipo de mercado = 'VIS'.
        /// </summary>
        /// <param name="codigoIsin"></param>
        /// <returns></returns>
        public bool IntegraAtivoSinacorPorIsin(string codigoIsin, DateTime data)
        {
            bool achou = false;

            TbotitulosCollection tbotitulosCollection = new TbotitulosCollection();
            tbotitulosCollection.BuscaAtivosPorIsin(codigoIsin, data);

            if (tbotitulosCollection.Count > 0)
            {
                achou = true;
                Tbotitulos tbotitulos = tbotitulosCollection[0];

                AtivoBolsa ativoBolsaExistente = new AtivoBolsa();
                if (ativoBolsaExistente.LoadByPrimaryKey(tbotitulos.CdCodneg.Trim()))
                {
                    ativoBolsaExistente.CodigoIsin = tbotitulos.CdCodisi;
                }
                else
                {
                    CarregaAtivo(tbotitulos);
                }
            }

            return achou;
        }

        /// <summary>
        /// Faz a carga efetiva em AtivoBolsa e FatorCotacaoBolsa.
        /// </summary>
        /// <param name="tbotitulos"></param>
        private void CarregaAtivo(Tbotitulos tbotitulos)
        {
            string tipoMercado = tbotitulos.CdTpmerc;
            if (tipoMercado != "VIS" && tipoMercado != "OPC" && tipoMercado != "OPV" &&
                tipoMercado != "SOM")
            {
                tipoMercado = "VIS";
            }
            string descricao = tbotitulos.NmNompre;

            string especificacao = "";
            if (tbotitulos.NmEspeci.Length > 3)
            {
                especificacao = tbotitulos.NmEspeci.Substring(0, 3);
            }
            else
            {
                especificacao = tbotitulos.NmEspeci;
            }

            decimal? puExercicio = tbotitulos.VlPreexe;
            DateTime? dataVencimento = tbotitulos.DtDatven;
            string cdAtivoBolsaObjeto = tbotitulos.CdTitobj;
            string codigoIsin = tbotitulos.CdCodisi;
            
            this.CdAtivoBolsa = tbotitulos.CdCodneg.Trim();
            this.Especificacao = especificacao;
            this.Descricao = descricao;
            this.TipoMercado = tipoMercado;
            this.PUExercicio = puExercicio;
            this.DataVencimento = dataVencimento;
            this.CdAtivoBolsaObjeto = cdAtivoBolsaObjeto;
            this.CodigoIsin = codigoIsin;
            this.IdMoeda = (short)ListaMoedaFixo.Real;
            this.IdEmissor = (int)ListaEmissorFixo.Padrao;
            this.NumeroSerie = 0;
            if (tbotitulos.NrNserie.HasValue)
            {
                this.NumeroSerie = (int)tbotitulos.NrNserie.Value;
            }
            this.TipoPapel = (byte)TipoPapelAtivo.Normal;

            EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
            estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
            estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Ações%"));
            estrategiaCollection.Query.Load();

            if (estrategiaCollection.Count > 0)
            {
                this.IdEstrategia = estrategiaCollection[0].IdEstrategia.Value;
            }

            this.Save();

            this.IntegraFatorCotacaoSinacor(tbotitulos.CdCodneg.Trim());
            this.IntegraDistribuicaoSinacor(tbotitulos.CdCodneg.Trim());
        }

        public bool IntegraFatorCotacaoSinacor(string cdAtivoBolsa)
        {
            TbotitfatCollection tbotitfatCollection = new TbotitfatCollection();
            tbotitfatCollection.BuscaFatorCotacao(cdAtivoBolsa);
            
            FatorCotacaoBolsaCollection fatorCotacaoBolsaCollection = new FatorCotacaoBolsaCollection();
            fatorCotacaoBolsaCollection.Query.Where(fatorCotacaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
            fatorCotacaoBolsaCollection.Query.Load();
            fatorCotacaoBolsaCollection.MarkAllAsDeleted();
            fatorCotacaoBolsaCollection.Save();

            int fator = 0;
            foreach (Tbotitfat tbotitfat in tbotitfatCollection)
            {
                if (fator == 0 || fator != (int)tbotitfat.NrFatcot.Value)
                {
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    fatorCotacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    fatorCotacaoBolsa.DataReferencia = tbotitfat.DtInifat.Value;
                    fatorCotacaoBolsa.Fator = tbotitfat.NrFatcot.Value;
                    fatorCotacaoBolsa.Save();

                    fator = (int)tbotitfat.NrFatcot.Value;
                }
            }

            return tbotitfatCollection.Count > 0;
        }

        public bool IntegraDistribuicaoSinacor(string cdAtivoBolsa)
        {
            VcfpapelCollection vcfpapelCollection = new VcfpapelCollection();
            vcfpapelCollection.BuscaPapel(cdAtivoBolsa);

            DistribuicaoBolsaCollection distribuicaoBolsaCollection = new DistribuicaoBolsaCollection();
            distribuicaoBolsaCollection.Query.Where(distribuicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
            distribuicaoBolsaCollection.Query.Load();
            distribuicaoBolsaCollection.MarkAllAsDeleted();
            distribuicaoBolsaCollection.Save();

            int distribuicao = 0;
            foreach (Vcfpapel vcfpapel in vcfpapelCollection)
            {
                if (distribuicao == 0 || distribuicao != (int)vcfpapel.NumDist.Value)
                {
                    DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();
                    distribuicaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
                    distribuicaoBolsa.DataReferencia = vcfpapel.DataIniNeg.Value;
                    distribuicaoBolsa.Distribuicao = (int)vcfpapel.NumDist.Value;
                    distribuicaoBolsa.Save();

                    distribuicao = (int)vcfpapel.NumDist.Value;
                }
            }

            return vcfpapelCollection.Count > 0;
        }

        /// <summary>
        /// Faz novo cadastro de ativo de termo, incluindo FatorCotacaoBolsa.
        /// </summary>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="cdAtivoBolsaTermo"></param>
        /// <param name="dataVencimento"></param>
        public void TrataCadastroAtivoTermo(string cdAtivoBolsa, string cdAtivoBolsaTermo, DateTime dataVencimento)
        {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            if (!ativoBolsa.LoadByPrimaryKey(cdAtivoBolsaTermo))
            {
                AtivoBolsa ativoBolsaAcao = new AtivoBolsa();
                if (ativoBolsaAcao.LoadByPrimaryKey(cdAtivoBolsa))
                {
                    AtivoBolsa ativoBolsaTermo = new AtivoBolsa();
                    ativoBolsaTermo.CdAtivoBolsa = cdAtivoBolsaTermo;
                    ativoBolsaTermo.DataVencimento = dataVencimento;
                    ativoBolsaTermo.Descricao = ativoBolsaAcao.Descricao;
                    ativoBolsaTermo.Especificacao = ativoBolsaAcao.Especificacao;
                    ativoBolsaTermo.IdEmissor = ativoBolsaAcao.IdEmissor.Value;
                    ativoBolsaTermo.IdEstrategia = ativoBolsaAcao.IdEstrategia;
                    ativoBolsaTermo.IdMoeda = ativoBolsaAcao.IdMoeda.Value;
                    ativoBolsaTermo.TipoMercado = TipoMercadoBolsa.Termo;
                    ativoBolsaTermo.CdAtivoBolsaObjeto = cdAtivoBolsa;
                    ativoBolsaTermo.CodigoIsin = "";

                    EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
                    estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
                    estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Ações%"));
                    estrategiaCollection.Query.Load();

                    if (estrategiaCollection.Count > 0)
                    {
                        ativoBolsaTermo.IdEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                    }

                    ativoBolsaTermo.Save();

                    FatorCotacaoBolsaCollection fatorCotacaoBolsaCollection = new FatorCotacaoBolsaCollection();
                    fatorCotacaoBolsaCollection.Query.Where(fatorCotacaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                    fatorCotacaoBolsaCollection.Query.Load();

                    foreach (FatorCotacaoBolsa fatorCotacaoBolsa in fatorCotacaoBolsaCollection)
                    {
                        FatorCotacaoBolsa fatorCotacaoBolsaTermo = new FatorCotacaoBolsa();
                        fatorCotacaoBolsaTermo.CdAtivoBolsa = cdAtivoBolsaTermo;
                        fatorCotacaoBolsaTermo.DataReferencia = fatorCotacaoBolsa.DataReferencia.Value;
                        fatorCotacaoBolsaTermo.Fator = fatorCotacaoBolsa.Fator.Value;
                        fatorCotacaoBolsaTermo.Save();
                    }
                }
            }
        }
        
    }
}
