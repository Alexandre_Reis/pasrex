using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Bolsa {
    public partial class ProventoBolsaCollection : esProventoBolsaCollection
    {
        /// <summary>
        /// Carrega o objeto ProventoBolsaCollection com todos os campos de ProventoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaProvento(DateTime dataEx)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.DataEx == dataEx);

            this.Query.Load();            
        }
    }
}
