﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Bolsa.Enums;
using log4net;
using Financial.Investidor;
using Financial.Investidor.Enums;

namespace Financial.Bolsa {
    public partial class LiquidacaoTermoBolsaCollection : esLiquidacaoTermoBolsaCollection {
        
        /// <summary>
        /// Deleta todas as liquidações de termo.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaLiquidacaoTermoBolsaVencimento(int idCliente, DateTime data)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdLiquidacao)
              .Where(this.Query.IdCliente.Equal(idCliente),
                     this.Query.DataMovimento.Equal(data),
                     this.Query.TipoLiquidacao.Equal((byte)TipoLiquidacaoTermoBolsa.Decurso));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta todas as liquidações de termo pela data de movimento e tipo de fonte passados.
        /// Deleta apenas para clientes com dataDia menor que dataMovimento ou (dataDia = dataMovimento e Status != Fechado).
        /// </summary>
        /// <param name="dataMovimento"></param>
        /// <param name="fonte"></param>
        public void DeletaLiquidacaoTermoBolsa(DateTime dataMovimento, FonteLiquidacaoTermoBolsa fonte) 
        {
            LiquidacaoTermoBolsaQuery liquidacaoTermoBolsaQuery = new LiquidacaoTermoBolsaQuery("L");
            ClienteQuery clienteQuery = new ClienteQuery("C");

            liquidacaoTermoBolsaQuery.Select(liquidacaoTermoBolsaQuery.IdLiquidacao);
            liquidacaoTermoBolsaQuery.InnerJoin(clienteQuery).On(liquidacaoTermoBolsaQuery.IdCliente == clienteQuery.IdCliente);
            liquidacaoTermoBolsaQuery.Where(liquidacaoTermoBolsaQuery.DataMovimento.Equal(dataMovimento) &
                                            liquidacaoTermoBolsaQuery.Fonte.Equal(fonte) &
                                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo) &
                                            (
                                                clienteQuery.DataDia.LessThan(dataMovimento) |
                                                (clienteQuery.DataDia.Equal(dataMovimento) & clienteQuery.Status.NotEqual((byte)StatusCliente.Divulgado))
                                            )
                                            );
            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();
            liquidacaoTermoBolsaCollection.Load(liquidacaoTermoBolsaQuery);

            liquidacaoTermoBolsaCollection.MarkAllAsDeleted();
            liquidacaoTermoBolsaCollection.Save();
        }

        /// <summary>
        /// Carrega o objeto LiquidacaoTermoBolsaCollection com todos os campos de LiquidacaoTermoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataMovimento"></param>        
        public void BuscaLiquidacaoTermoBolsa(int idCliente, DateTime dataMovimento)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.DataMovimento.Equal(dataMovimento),
                        this.Query.IdCliente == idCliente);

            this.Query.Load();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="fonte"></param>
        public void DeletaLiquidacaoTermoBolsa(int idCliente, DateTime data, FonteLiquidacaoTermoBolsa fonte)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.IdLiquidacao)
              .Where(this.Query.DataMovimento.Equal(data),
                     this.Query.IdCliente.Equal(idCliente),
                     this.Query.Fonte.Equal((byte)fonte));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
    }
}
