﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using System.IO;
using Financial.Util;
using Financial.Common.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Financial.Bolsa.Enums;
using Financial.Common;

namespace Financial.Bolsa
{
    public partial class PosicaoBolsaHistorico : esPosicaoBolsaHistorico
    {
        #region Adiciona NomeCliente/NomeAgenteMercado como uma propriedade virtual para bind em Design Time
        protected override List<esPropertyDescriptor> GetLocalBindingProperties() {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
            props.Add(new esPropertyDescriptor(this, "NomeCliente", typeof(string)));
            props.Add(new esPropertyDescriptor(this, "NomeAgenteMercado", typeof(string)));
            //
            return props;
        }

        private string nomeCliente = "";
        public string NomeCliente {
            get {
                return this.nomeCliente;
            }
            set {
                this.nomeCliente = value;
            }
        }

        private string nomeAgenteMercado = "";
        public string NomeAgenteMercado {
            get {
                return this.nomeAgenteMercado;
            }
            set {
                this.nomeAgenteMercado = value;
            }
        }
        #endregion

        /// <summary>
        /// Soma do valor de mercado de todas as posições historicas dado um cliente e uma data
        /// Retorna 0 se não existir registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public decimal RetornaValorMercadoSum(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.query.IdCliente == idCliente);

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }


        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoBolsaHistoricoQuery posicaoBolsaQuery = new PosicaoBolsaHistoricoQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");

            posicaoBolsaQuery.Select(posicaoBolsaQuery.IdPosicao);
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaQuery.CdAtivoBolsa);
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente),
                                    posicaoBolsaQuery.DataHistorico.Equal(data),
                                    posicaoBolsaQuery.Quantidade.NotEqual(0),
                                    ativoBolsaQuery.IdMoeda.NotEqual(idMoedaCliente));

            PosicaoBolsaHistoricoCollection posicaoBolsaCollectionExiste = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaCollectionExiste.Load(posicaoBolsaQuery);

            if (posicaoBolsaCollectionExiste.Count == 0)
            {
                return this.RetornaValorMercadoSum(idCliente,data);
            }

            PosicaoBolsaHistoricoCollection posicaoBolsaCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa, posicaoBolsaCollection.Query.ValorMercado.Sum());
            posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                               posicaoBolsaCollection.Query.DataHistorico.Equal(data));
            posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
            posicaoBolsaCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoBolsaCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoBolsaHistorico posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal valor = posicaoBolsa.ValorMercado.Value;

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.IdMoeda);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                int idMoedaAtivo = ativoBolsa.IdMoeda.Value;

                if (idMoedaCliente != idMoedaAtivo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Soma do valor de mercado de todas as posições historicas dado um cliente e uma data
        /// Retorna 0 se não existir registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.query.IdCliente == idCliente);

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Soma do valor de mercado de todas as posições historicas dado um cliente e uma data
        /// Retorna 0 se não existir registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public decimal RetornaValorMercadoBloqueado(int idCliente, DateTime dataHistorico)
        {
            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                        posicaoBolsaHistoricoCollection.Query.PUMercado.Avg(),
                                                        posicaoBolsaHistoricoCollection.Query.QuantidadeBloqueada.Sum());
            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                        posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                        posicaoBolsaHistoricoCollection.Query.QuantidadeBloqueada.NotEqual(0));
            posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
            posicaoBolsaHistoricoCollection.Query.Load();

            decimal valorPosicao = 0;
            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                decimal quantidade = posicaoBolsaHistorico.QuantidadeBloqueada.Value;
                decimal pu = posicaoBolsaHistorico.PUMercado.Value;

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataHistorico);

                decimal valor = Utilitario.Truncate(quantidade * pu / fatorCotacaoBolsa.Fator.Value, 2);

                valorPosicao += valor;
            }

            return valorPosicao;
        }

        /// <summary>
        /// Soma do valor de mercado de todas as posições historicas dado um cliente e uma data.
        /// Filtra tb pelo TipoPapel de AtivoBolsa.
        /// Retorna 0 se não existir registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico, byte tipoPapel)
        {
            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("P");
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("A");

            posicaoBolsaHistoricoQuery.Select(posicaoBolsaHistoricoQuery.ValorMercado.Sum());
            posicaoBolsaHistoricoQuery.InnerJoin(ativoBolsaQuery).On(ativoBolsaQuery.CdAtivoBolsa == posicaoBolsaHistoricoQuery.CdAtivoBolsa);
            posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente.Equal(idCliente),
                                             posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataHistorico),
                                             ativoBolsaQuery.TipoPapel.Equal(tipoPapel));
            posicaoBolsaHistorico.Load(posicaoBolsaHistoricoQuery);

            decimal valor = 0;
            if (posicaoBolsaHistorico.ValorMercado.HasValue)
            {
                valor = posicaoBolsaHistorico.ValorMercado.Value;
            }

            return valor;
        }

        /// <summary>
        /// Soma do valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>Soma do valor de mercado de todas as posições de ações. 
        ///          Retorna 0 se não houver registros
        /// </returns>
        public decimal RetornaValorMercadoAcoes(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Soma do valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>Soma do valor de mercado de todas as posições de ações. 
        ///          Retorna 0 se não houver registros
        /// </returns>
        public decimal RetornaValorMercadoOpcoes(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Soma do valor de custo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>Soma do valor de mercado de todas as posições de ações. 
        ///          Retorna 0 se não houver registros
        /// </returns>
        public decimal RetornaValorCustoOpcoes(int idCliente, DateTime dataHistorico)
        {
            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                         posicaoBolsaHistoricoCollection.Query.Quantidade.Sum(),
                                                         posicaoBolsaHistoricoCollection.Query.PUCusto.Avg());
            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                        posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(dataHistorico),
                                                        posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
            posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
            posicaoBolsaHistoricoCollection.Query.Load();

            decimal valorTotal = 0;
            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                string cdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
                decimal quantidade = posicaoBolsaHistorico.Quantidade.Value;
                decimal puCusto = posicaoBolsaHistorico.PUCusto.Value;

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataHistorico);

                decimal valor = Utilitario.Truncate(quantidade * puCusto / fatorCotacaoBolsa.Fator.Value, 2);
                valorTotal += valor;
            }

            return valorTotal;
        }

        /// <summary>
        /// Soma do valor de mercado do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>Soma do valor de mercado de todas as posições de opção de compra/venda com valor maior que 0. 
        ///          Retorna 0 se não houver registros
        /// </returns>
        public decimal RetornaValorMercadoOpcaoPosicaoComprada(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                        this.Query.Quantidade.GreaterThan(0));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Soma do valor de mercado do Cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>Soma do valor de mercado de todas as posições de opção de compra/venda com valor menor que 0. 
        ///          Retorna 0 se não houver registros
        /// </returns>        
        public decimal RetornaValorMercadoOpcaoPosicaoVendida(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda),
                        this.Query.Quantidade.LessThan(0));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Traz uma posicao historica dado um cliente, data do historico e ativo de bolsa    
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public bool BuscaPosicaoBolsaHistorico(int idCliente, DateTime dataHistorico, string cdAtivo)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.Quantidade, this.Query.IdAgente,
                         this.Query.PUCusto, this.Query.PUCustoLiquido)
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa == cdAtivo)
                 .OrderBy(this.Query.IdPosicao.Ascending);

            bool retorno = this.Query.Load();
            
            return retorno;
        }

        /// <summary>
        /// Traz uma posicao historica dado um cliente, data do historico e ativo de bolsa    
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="idAgente"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public bool BuscaPosicaoBolsaHistorico(int idCliente, DateTime dataHistorico, string cdAtivoBolsa,
                                                                                    int idAgente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.PUCustoLiquido)
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                        this.Query.IdCliente == idCliente,
                        this.Query.IdAgente == idAgente);

            bool retorno = this.Query.Load();
            
            return retorno;
        }

        /// <summary>
        /// Retorna quantas posições um cliente teve durante o mês
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAgente"></param>
        /// <param name="data">Será considerado o mes da data passada</param>
        /// <returns> número de dias que o cliente teve posição num determinado mês
        /// Resultado não pode ser maior que 31</returns>
        public int BuscaNumeroPosicoesMes(int idCliente, int idAgente, DateTime data)
        {
            DateTime diaInicio = new DateTime(data.Year, data.Month, 1);
            DateTime diaFim = Calendario.RetornaUltimoDiaUtilMes(data, 0, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();

            // Agupa por Data - posições de ativos diferentes na mesma data conta uma unica vez
            posicaoBolsaHistoricoCollection.QueryReset();
            posicaoBolsaHistoricoCollection.Query
                 .Select(this.Query.DataHistorico)
                 .Where(this.Query.DataHistorico.Between(diaInicio, diaFim),
                        this.Query.IdCliente == idCliente,
                        this.Query.IdAgente == idAgente,
                        this.Query.Quantidade.NotEqual(0))
                 .GroupBy(this.Query.DataHistorico);

            posicaoBolsaHistoricoCollection.Query.Load();
            
            if (posicaoBolsaHistoricoCollection.Count > 31)
            {
                Assert.Fail("Consulta errada. BuscaNumeroPosicoesMes não pode ser maior que 31: " + posicaoBolsaHistoricoCollection.Count);
            }
            return posicaoBolsaHistoricoCollection.Count;
        }

        /// <summary>
        /// Soma do valor de mercado de todas as posições historicas dado um cliente e uma data e um ativo.
        /// Retorna 0 se não existir registros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <param name="cdAtivo"></param>
        /// <returns> boolean indicando se houve registros</returns>
        public decimal RetornaValorMercadoSum(int idCliente, string cdAtivoBolsa, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.DataHistorico.Equal(dataHistorico),
                        this.query.IdCliente == idCliente,
                        this.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public decimal RetornaPuPosicaoHistorico(int idCliente, string cdAtivoBolsa, DateTime dataHistorico)
        {
            decimal pu = 0;

            PosicaoBolsaHistoricoCollection p = new PosicaoBolsaHistoricoCollection();
            p.Query.Select(p.Query.PUMercado)
                   .Where(p.Query.DataHistorico == dataHistorico &&
                          p.Query.IdCliente == idCliente &&
                          p.Query.CdAtivoBolsa == cdAtivoBolsa);

            if (p.Query.Load())
            {
                if (p.Count > 0)
                {
                    pu = p[0].PUMercado.Value;
                }
            }

            return pu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public decimal RetornaPuAjustadoPosicao(int idCliente, string cdAtivoBolsa, DateTime data, DateTime dataHistorico)
        {
            decimal pu = 0;

            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            pu = posicaoBolsaHistorico.RetornaPuPosicaoHistorico(idCliente, cdAtivoBolsa, data);

            if (pu != 0)
            {
                ProventoBolsa proventoBolsa = new ProventoBolsa();
                proventoBolsa.Query.Select(proventoBolsa.Query.Valor.Sum())
                            .Where(proventoBolsa.Query.DataEx <= data &
                                   proventoBolsa.Query.DataEx > dataHistorico &
                                   proventoBolsa.Query.CdAtivoBolsa == cdAtivoBolsa);
                proventoBolsa.Query.Load();

                if (proventoBolsa.Valor.HasValue)
                {
                    pu += proventoBolsa.Valor.Value;
                }

                BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
                bonificacaoBolsaCollection.Query.Select(bonificacaoBolsaCollection.Query.Percentual)
                            .Where(bonificacaoBolsaCollection.Query.DataEx <= data &
                                   bonificacaoBolsaCollection.Query.DataEx > dataHistorico &
                                   bonificacaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);
                bonificacaoBolsaCollection.Query.Load();

                foreach (BonificacaoBolsa bonificacaoBolsa in bonificacaoBolsaCollection)
                {
                    pu = pu / (1M / (1M + (bonificacaoBolsa.Percentual.Value / 100M)));
                }

                GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
                grupamentoBolsaCollection.Query.Select(grupamentoBolsaCollection.Query.TipoGrupamento,
                                                       grupamentoBolsaCollection.Query.FatorPU)
                            .Where(grupamentoBolsaCollection.Query.DataEx <= data &
                                   grupamentoBolsaCollection.Query.DataEx > dataHistorico &
                                   grupamentoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);
                grupamentoBolsaCollection.Query.Load();

                foreach (GrupamentoBolsa grupamentoBolsa in grupamentoBolsaCollection)
                {
                    if (grupamentoBolsa.TipoGrupamento.Value == (byte)TipoGrupamentoBolsa.Grupamento)
                    {
                        pu = pu * grupamentoBolsa.FatorPU.Value;
                    }
                    else
                    {
                        pu = pu / grupamentoBolsa.FatorPU.Value;
                    }
                }
            }

            return pu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="cdAtivoBolsa"></param>
        /// <param name="data"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public decimal RetornaPuAjustadoPosicao(int idCliente, string cdAtivoBolsa, DateTime data, DateTime dataHistorico, int idMoedaCliente, int idMoedaAtivo)
        {
            decimal pu = 0;

            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            pu = posicaoBolsaHistorico.RetornaPuPosicaoHistorico(idCliente, cdAtivoBolsa, data);

            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
            decimal fatorMoeda = conversaoMoeda.RetornaFatorConversao(idMoedaCliente, idMoedaAtivo, data);
            pu = pu * fatorMoeda;

            if (pu != 0)
            {
                ProventoBolsa proventoBolsa = new ProventoBolsa();
                proventoBolsa.Query.Select(proventoBolsa.Query.Valor.Sum())
                            .Where(proventoBolsa.Query.DataEx <= data &
                                   proventoBolsa.Query.DataEx > dataHistorico &
                                   proventoBolsa.Query.CdAtivoBolsa == cdAtivoBolsa);
                proventoBolsa.Query.Load();

                if (proventoBolsa.Valor.HasValue)
                {
                    pu += proventoBolsa.Valor.Value;
                }

                BonificacaoBolsaCollection bonificacaoBolsaCollection = new BonificacaoBolsaCollection();
                bonificacaoBolsaCollection.Query.Select(bonificacaoBolsaCollection.Query.Percentual)
                            .Where(bonificacaoBolsaCollection.Query.DataEx <= data &
                                   bonificacaoBolsaCollection.Query.DataEx > dataHistorico &
                                   bonificacaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);
                bonificacaoBolsaCollection.Query.Load();

                foreach (BonificacaoBolsa bonificacaoBolsa in bonificacaoBolsaCollection)
                {
                    pu = pu / (1M / (1M + (bonificacaoBolsa.Percentual.Value / 100M)));
                }

                GrupamentoBolsaCollection grupamentoBolsaCollection = new GrupamentoBolsaCollection();
                grupamentoBolsaCollection.Query.Select(grupamentoBolsaCollection.Query.TipoGrupamento,
                                                       grupamentoBolsaCollection.Query.FatorPU)
                            .Where(grupamentoBolsaCollection.Query.DataEx <= data &
                                   grupamentoBolsaCollection.Query.DataEx > dataHistorico &
                                   grupamentoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);
                grupamentoBolsaCollection.Query.Load();

                foreach (GrupamentoBolsa grupamentoBolsa in grupamentoBolsaCollection)
                {
                    if (grupamentoBolsa.TipoGrupamento.Value == (byte)TipoGrupamentoBolsa.Grupamento)
                    {
                        pu = pu * grupamentoBolsa.FatorPU.Value;
                    }
                    else
                    {
                        pu = pu / grupamentoBolsa.FatorPU.Value;
                    }
                }
            }

            return pu;
        }

        
    }
}
