﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.ContaCorrente.Enums;
using Financial.ContaCorrente;

namespace Financial.Bolsa.ThreadControl {
    
    /// <summary>
    ///  Classe que contem os metodos que serão executados pela Thread
    /// </summary>
    public class ThreadClass {
        /* Codigo do cliente*/
        //private int idCliente;
        
        /* Data que o cliente será processado */
        //private DateTime data;
        
        //Construtor
        /*
        public ThreadClass(int idCliente, DateTime data) {
            this.idCliente = idCliente;
            this.data = data;
        }*/


        /// <summary>
        /// 
        /// </summary>
        public void LimpaLiquidacao(int idCliente) {
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.DeletaLiquidacao(idCliente, OrigemLancamentoLiquidacao.Bolsa.Values());
        }

        /// <summary>
        /// 
        /// </summary>
        public void DistribuiBonificacao(DateTime data) {
            //BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();
            //bonificacaoBolsa.DistribuiBonificacao(data);
        }

        /// <summary>
        /// 
        /// </summary>
        public void DistribuiConversao(DateTime data) {
            //ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
            //conversaoBolsa.DistribuiConversao(data);
        }

        /// <summary>
        /// 
        /// </summary>
        public void DistribuiGrupamento(DateTime data) {
            //GrupamentoBolsa grupamentoBolsa = new GrupamentoBolsa();
            //grupamentoBolsa.DistribuiGrupamento(data);
        }
    }
}
