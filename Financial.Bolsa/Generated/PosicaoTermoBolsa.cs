/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;


































using Financial.Common;
using Financial.Investidor;




		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esPosicaoTermoBolsaCollection : esEntityCollection
	{
		public esPosicaoTermoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoTermoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoTermoBolsaQuery);
		}
		#endregion
		
		virtual public PosicaoTermoBolsa DetachEntity(PosicaoTermoBolsa entity)
		{
			return base.DetachEntity(entity) as PosicaoTermoBolsa;
		}
		
		virtual public PosicaoTermoBolsa AttachEntity(PosicaoTermoBolsa entity)
		{
			return base.AttachEntity(entity) as PosicaoTermoBolsa;
		}
		
		virtual public void Combine(PosicaoTermoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoTermoBolsa this[int index]
		{
			get
			{
				return base[index] as PosicaoTermoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoTermoBolsa);
		}
	}



	[Serializable]
	abstract public class esPosicaoTermoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoTermoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoTermoBolsa()
		{

		}

		public esPosicaoTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoTermoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao)
		{
			esPosicaoTermoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "PrazoTotal": this.str.PrazoTotal = (string)value; break;							
						case "PrazoDecorrer": this.str.PrazoDecorrer = (string)value; break;							
						case "TaxaAno": this.str.TaxaAno = (string)value; break;							
						case "TaxaMTM": this.str.TaxaMTM = (string)value; break;							
						case "ValorTermo": this.str.ValorTermo = (string)value; break;							
						case "ValorTermoLiquido": this.str.ValorTermoLiquido = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "ValorCurva": this.str.ValorCurva = (string)value; break;							
						case "ValorCorrigido": this.str.ValorCorrigido = (string)value; break;							
						case "PUCustoLiquidoAcao": this.str.PUCustoLiquidoAcao = (string)value; break;							
						case "PUTermo": this.str.PUTermo = (string)value; break;							
						case "PUTermoLiquido": this.str.PUTermoLiquido = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "RendimentoTotal": this.str.RendimentoTotal = (string)value; break;							
						case "RendimentoApropriar": this.str.RendimentoApropriar = (string)value; break;							
						case "RendimentoApropriado": this.str.RendimentoApropriado = (string)value; break;							
						case "RendimentoDia": this.str.RendimentoDia = (string)value; break;							
						case "TipoTermo": this.str.TipoTermo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "PrazoTotal":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.PrazoTotal = (System.Int16?)value;
							break;
						
						case "PrazoDecorrer":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.PrazoDecorrer = (System.Int16?)value;
							break;
						
						case "TaxaAno":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaAno = (System.Decimal?)value;
							break;
						
						case "TaxaMTM":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaMTM = (System.Decimal?)value;
							break;
						
						case "ValorTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTermo = (System.Decimal?)value;
							break;
						
						case "ValorTermoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTermoLiquido = (System.Decimal?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "ValorCurva":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCurva = (System.Decimal?)value;
							break;
						
						case "ValorCorrigido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCorrigido = (System.Decimal?)value;
							break;
						
						case "PUCustoLiquidoAcao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCustoLiquidoAcao = (System.Decimal?)value;
							break;
						
						case "PUTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUTermo = (System.Decimal?)value;
							break;
						
						case "PUTermoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUTermoLiquido = (System.Decimal?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "RendimentoTotal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoTotal = (System.Decimal?)value;
							break;
						
						case "RendimentoApropriar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoApropriar = (System.Decimal?)value;
							break;
						
						case "RendimentoApropriado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoApropriado = (System.Decimal?)value;
							break;
						
						case "RendimentoDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoDia = (System.Decimal?)value;
							break;
						
						case "TipoTermo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoTermo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(PosicaoTermoBolsaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(PosicaoTermoBolsaMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoTermoBolsaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoTermoBolsaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoTermoBolsaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoTermoBolsaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.PrazoTotal
		/// </summary>
		virtual public System.Int16? PrazoTotal
		{
			get
			{
				return base.GetSystemInt16(PosicaoTermoBolsaMetadata.ColumnNames.PrazoTotal);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoTermoBolsaMetadata.ColumnNames.PrazoTotal, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.PrazoDecorrer
		/// </summary>
		virtual public System.Int16? PrazoDecorrer
		{
			get
			{
				return base.GetSystemInt16(PosicaoTermoBolsaMetadata.ColumnNames.PrazoDecorrer);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoTermoBolsaMetadata.ColumnNames.PrazoDecorrer, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.TaxaAno
		/// </summary>
		virtual public System.Decimal? TaxaAno
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.TaxaAno);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.TaxaAno, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.TaxaMTM
		/// </summary>
		virtual public System.Decimal? TaxaMTM
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.TaxaMTM);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.TaxaMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.ValorTermo
		/// </summary>
		virtual public System.Decimal? ValorTermo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorTermo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.ValorTermoLiquido
		/// </summary>
		virtual public System.Decimal? ValorTermoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorTermoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorTermoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.ValorCurva
		/// </summary>
		virtual public System.Decimal? ValorCurva
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorCurva);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorCurva, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.ValorCorrigido
		/// </summary>
		virtual public System.Decimal? ValorCorrigido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorCorrigido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.ValorCorrigido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.PUCustoLiquidoAcao
		/// </summary>
		virtual public System.Decimal? PUCustoLiquidoAcao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.PUCustoLiquidoAcao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.PUCustoLiquidoAcao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.PUTermo
		/// </summary>
		virtual public System.Decimal? PUTermo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.PUTermo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.PUTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.PUTermoLiquido
		/// </summary>
		virtual public System.Decimal? PUTermoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.PUTermoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.PUTermoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.NumeroContrato
		/// </summary>
		virtual public System.String NumeroContrato
		{
			get
			{
				return base.GetSystemString(PosicaoTermoBolsaMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemString(PosicaoTermoBolsaMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoTermoBolsaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.RendimentoTotal
		/// </summary>
		virtual public System.Decimal? RendimentoTotal
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoTotal);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoTotal, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.RendimentoApropriar
		/// </summary>
		virtual public System.Decimal? RendimentoApropriar
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriar);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriar, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.RendimentoApropriado
		/// </summary>
		virtual public System.Decimal? RendimentoApropriado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.RendimentoDia
		/// </summary>
		virtual public System.Decimal? RendimentoDia
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoDia);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoDia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsa.TipoTermo
		/// </summary>
		virtual public System.Byte? TipoTermo
		{
			get
			{
				return base.GetSystemByte(PosicaoTermoBolsaMetadata.ColumnNames.TipoTermo);
			}
			
			set
			{
				base.SetSystemByte(PosicaoTermoBolsaMetadata.ColumnNames.TipoTermo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoTermoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrazoTotal
			{
				get
				{
					System.Int16? data = entity.PrazoTotal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoTotal = null;
					else entity.PrazoTotal = Convert.ToInt16(value);
				}
			}
				
			public System.String PrazoDecorrer
			{
				get
				{
					System.Int16? data = entity.PrazoDecorrer;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoDecorrer = null;
					else entity.PrazoDecorrer = Convert.ToInt16(value);
				}
			}
				
			public System.String TaxaAno
			{
				get
				{
					System.Decimal? data = entity.TaxaAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaAno = null;
					else entity.TaxaAno = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaMTM
			{
				get
				{
					System.Decimal? data = entity.TaxaMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaMTM = null;
					else entity.TaxaMTM = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTermo
			{
				get
				{
					System.Decimal? data = entity.ValorTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTermo = null;
					else entity.ValorTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTermoLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorTermoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTermoLiquido = null;
					else entity.ValorTermoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCurva
			{
				get
				{
					System.Decimal? data = entity.ValorCurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCurva = null;
					else entity.ValorCurva = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCorrigido
			{
				get
				{
					System.Decimal? data = entity.ValorCorrigido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCorrigido = null;
					else entity.ValorCorrigido = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCustoLiquidoAcao
			{
				get
				{
					System.Decimal? data = entity.PUCustoLiquidoAcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCustoLiquidoAcao = null;
					else entity.PUCustoLiquidoAcao = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUTermo
			{
				get
				{
					System.Decimal? data = entity.PUTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUTermo = null;
					else entity.PUTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUTermoLiquido
			{
				get
				{
					System.Decimal? data = entity.PUTermoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUTermoLiquido = null;
					else entity.PUTermoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.String data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToString(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String RendimentoTotal
			{
				get
				{
					System.Decimal? data = entity.RendimentoTotal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoTotal = null;
					else entity.RendimentoTotal = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoApropriar
			{
				get
				{
					System.Decimal? data = entity.RendimentoApropriar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoApropriar = null;
					else entity.RendimentoApropriar = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoApropriado
			{
				get
				{
					System.Decimal? data = entity.RendimentoApropriado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoApropriado = null;
					else entity.RendimentoApropriado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoDia
			{
				get
				{
					System.Decimal? data = entity.RendimentoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoDia = null;
					else entity.RendimentoDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoTermo
			{
				get
				{
					System.Byte? data = entity.TipoTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoTermo = null;
					else entity.TipoTermo = Convert.ToByte(value);
				}
			}
			

			private esPosicaoTermoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoTermoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoTermoBolsa : esPosicaoTermoBolsa
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoTermoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoTermoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrazoTotal
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.PrazoTotal, esSystemType.Int16);
			}
		} 
		
		public esQueryItem PrazoDecorrer
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.PrazoDecorrer, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TaxaAno
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.TaxaAno, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaMTM
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.TaxaMTM, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTermo
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.ValorTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTermoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.ValorTermoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCurva
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.ValorCurva, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCorrigido
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.ValorCorrigido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCustoLiquidoAcao
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.PUCustoLiquidoAcao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUTermo
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.PUTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUTermoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.PUTermoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.NumeroContrato, esSystemType.String);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RendimentoTotal
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.RendimentoTotal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoApropriar
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoApropriado
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoDia
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.RendimentoDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoTermo
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaMetadata.ColumnNames.TipoTermo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoTermoBolsaCollection")]
	public partial class PosicaoTermoBolsaCollection : esPosicaoTermoBolsaCollection, IEnumerable<PosicaoTermoBolsa>
	{
		public PosicaoTermoBolsaCollection()
		{

		}
		
		public static implicit operator List<PosicaoTermoBolsa>(PosicaoTermoBolsaCollection coll)
		{
			List<PosicaoTermoBolsa> list = new List<PosicaoTermoBolsa>();
			
			foreach (PosicaoTermoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoTermoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoTermoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoTermoBolsa AddNew()
		{
			PosicaoTermoBolsa entity = base.AddNewEntity() as PosicaoTermoBolsa;
			
			return entity;
		}

		public PosicaoTermoBolsa FindByPrimaryKey(System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(idPosicao) as PosicaoTermoBolsa;
		}


		#region IEnumerable<PosicaoTermoBolsa> Members

		IEnumerator<PosicaoTermoBolsa> IEnumerable<PosicaoTermoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoTermoBolsa;
			}
		}

		#endregion
		
		private PosicaoTermoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoTermoBolsa' table
	/// </summary>

	[Serializable]
	public partial class PosicaoTermoBolsa : esPosicaoTermoBolsa
	{
		public PosicaoTermoBolsa()
		{

		}
	
		public PosicaoTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoTermoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoTermoBolsaQuery query;
	}



	[Serializable]
	public partial class PosicaoTermoBolsaQuery : esPosicaoTermoBolsaQuery
	{
		public PosicaoTermoBolsaQuery()
		{

		}		
		
		public PosicaoTermoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoTermoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoTermoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.IdIndice, 2, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.IdAgente, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.DataOperacao, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.DataVencimento, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.Quantidade, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.QuantidadeInicial, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.PrazoTotal, 10, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.PrazoTotal;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.PrazoDecorrer, 11, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.PrazoDecorrer;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.TaxaAno, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.TaxaAno;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.TaxaMTM, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.TaxaMTM;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.ValorTermo, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.ValorTermo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.ValorTermoLiquido, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.ValorTermoLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.ValorMercado, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.ValorCurva, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.ValorCurva;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.ValorCorrigido, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.ValorCorrigido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.PUCustoLiquidoAcao, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.PUCustoLiquidoAcao;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.PUTermo, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.PUTermo;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.PUTermoLiquido, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.PUTermoLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.PUMercado, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.NumeroContrato, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.NumeroContrato;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.IdTrader, 24, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoTotal, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.RendimentoTotal;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriar, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.RendimentoApropriar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoApropriado, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.RendimentoApropriado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.RendimentoDia, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.RendimentoDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaMetadata.ColumnNames.TipoTermo, 29, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoTermoBolsaMetadata.PropertyNames.TipoTermo;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoTermoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdIndice = "IdIndice";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string PrazoTotal = "PrazoTotal";
			 public const string PrazoDecorrer = "PrazoDecorrer";
			 public const string TaxaAno = "TaxaAno";
			 public const string TaxaMTM = "TaxaMTM";
			 public const string ValorTermo = "ValorTermo";
			 public const string ValorTermoLiquido = "ValorTermoLiquido";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCurva = "ValorCurva";
			 public const string ValorCorrigido = "ValorCorrigido";
			 public const string PUCustoLiquidoAcao = "PUCustoLiquidoAcao";
			 public const string PUTermo = "PUTermo";
			 public const string PUTermoLiquido = "PUTermoLiquido";
			 public const string PUMercado = "PUMercado";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string IdTrader = "IdTrader";
			 public const string RendimentoTotal = "RendimentoTotal";
			 public const string RendimentoApropriar = "RendimentoApropriar";
			 public const string RendimentoApropriado = "RendimentoApropriado";
			 public const string RendimentoDia = "RendimentoDia";
			 public const string TipoTermo = "TipoTermo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdIndice = "IdIndice";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string PrazoTotal = "PrazoTotal";
			 public const string PrazoDecorrer = "PrazoDecorrer";
			 public const string TaxaAno = "TaxaAno";
			 public const string TaxaMTM = "TaxaMTM";
			 public const string ValorTermo = "ValorTermo";
			 public const string ValorTermoLiquido = "ValorTermoLiquido";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCurva = "ValorCurva";
			 public const string ValorCorrigido = "ValorCorrigido";
			 public const string PUCustoLiquidoAcao = "PUCustoLiquidoAcao";
			 public const string PUTermo = "PUTermo";
			 public const string PUTermoLiquido = "PUTermoLiquido";
			 public const string PUMercado = "PUMercado";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string IdTrader = "IdTrader";
			 public const string RendimentoTotal = "RendimentoTotal";
			 public const string RendimentoApropriar = "RendimentoApropriar";
			 public const string RendimentoApropriado = "RendimentoApropriado";
			 public const string RendimentoDia = "RendimentoDia";
			 public const string TipoTermo = "TipoTermo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoTermoBolsaMetadata))
			{
				if(PosicaoTermoBolsaMetadata.mapDelegates == null)
				{
					PosicaoTermoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoTermoBolsaMetadata.meta == null)
				{
					PosicaoTermoBolsaMetadata.meta = new PosicaoTermoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrazoTotal", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("PrazoDecorrer", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TaxaAno", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaMTM", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTermo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTermoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCurva", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCorrigido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCustoLiquidoAcao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUTermo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUTermoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RendimentoTotal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoApropriar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoApropriado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoTermo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "PosicaoTermoBolsa";
				meta.Destination = "PosicaoTermoBolsa";
				
				meta.spInsert = "proc_PosicaoTermoBolsaInsert";				
				meta.spUpdate = "proc_PosicaoTermoBolsaUpdate";		
				meta.spDelete = "proc_PosicaoTermoBolsaDelete";
				meta.spLoadAll = "proc_PosicaoTermoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoTermoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoTermoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
