/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		


























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esTabelaCustosBolsaCollection : esEntityCollection
	{
		public esTabelaCustosBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaCustosBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaCustosBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaCustosBolsaQuery);
		}
		#endregion
		
		virtual public TabelaCustosBolsa DetachEntity(TabelaCustosBolsa entity)
		{
			return base.DetachEntity(entity) as TabelaCustosBolsa;
		}
		
		virtual public TabelaCustosBolsa AttachEntity(TabelaCustosBolsa entity)
		{
			return base.AttachEntity(entity) as TabelaCustosBolsa;
		}
		
		virtual public void Combine(TabelaCustosBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaCustosBolsa this[int index]
		{
			get
			{
				return base[index] as TabelaCustosBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaCustosBolsa);
		}
	}



	[Serializable]
	abstract public class esTabelaCustosBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaCustosBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaCustosBolsa()
		{

		}

		public esTabelaCustosBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.Int16 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.Int16 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaCustosBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.Int16 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.Int16 idTabela)
		{
			esTabelaCustosBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.Int16 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "PercentualEmolumentoBolsa": this.str.PercentualEmolumentoBolsa = (string)value; break;							
						case "PercentualRegistroBolsa": this.str.PercentualRegistroBolsa = (string)value; break;							
						case "PercentualLiquidacaoCBLC": this.str.PercentualLiquidacaoCBLC = (string)value; break;							
						case "PercentualRegistroCBLC": this.str.PercentualRegistroCBLC = (string)value; break;							
						case "MinimoBovespa": this.str.MinimoBovespa = (string)value; break;							
						case "MinimoCBLC": this.str.MinimoCBLC = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdTabela = (System.Int16?)value;
							break;
						
						case "PercentualEmolumentoBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualEmolumentoBolsa = (System.Decimal?)value;
							break;
						
						case "PercentualRegistroBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualRegistroBolsa = (System.Decimal?)value;
							break;
						
						case "PercentualLiquidacaoCBLC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualLiquidacaoCBLC = (System.Decimal?)value;
							break;
						
						case "PercentualRegistroCBLC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualRegistroCBLC = (System.Decimal?)value;
							break;
						
						case "MinimoBovespa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.MinimoBovespa = (System.Decimal?)value;
							break;
						
						case "MinimoCBLC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.MinimoCBLC = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaCustosBolsa.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaCustosBolsaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaCustosBolsaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBolsa.IdTabela
		/// </summary>
		virtual public System.Int16? IdTabela
		{
			get
			{
				return base.GetSystemInt16(TabelaCustosBolsaMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt16(TabelaCustosBolsaMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBolsa.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TabelaCustosBolsaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TabelaCustosBolsaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBolsa.PercentualEmolumentoBolsa
		/// </summary>
		virtual public System.Decimal? PercentualEmolumentoBolsa
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.PercentualEmolumentoBolsa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.PercentualEmolumentoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBolsa.PercentualRegistroBolsa
		/// </summary>
		virtual public System.Decimal? PercentualRegistroBolsa
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.PercentualRegistroBolsa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.PercentualRegistroBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBolsa.PercentualLiquidacaoCBLC
		/// </summary>
		virtual public System.Decimal? PercentualLiquidacaoCBLC
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.PercentualLiquidacaoCBLC);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.PercentualLiquidacaoCBLC, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBolsa.PercentualRegistroCBLC
		/// </summary>
		virtual public System.Decimal? PercentualRegistroCBLC
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.PercentualRegistroCBLC);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.PercentualRegistroCBLC, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBolsa.MinimoBovespa
		/// </summary>
		virtual public System.Decimal? MinimoBovespa
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.MinimoBovespa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.MinimoBovespa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCustosBolsa.MinimoCBLC
		/// </summary>
		virtual public System.Decimal? MinimoCBLC
		{
			get
			{
				return base.GetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.MinimoCBLC);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCustosBolsaMetadata.ColumnNames.MinimoCBLC, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaCustosBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdTabela
			{
				get
				{
					System.Int16? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt16(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String PercentualEmolumentoBolsa
			{
				get
				{
					System.Decimal? data = entity.PercentualEmolumentoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualEmolumentoBolsa = null;
					else entity.PercentualEmolumentoBolsa = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualRegistroBolsa
			{
				get
				{
					System.Decimal? data = entity.PercentualRegistroBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualRegistroBolsa = null;
					else entity.PercentualRegistroBolsa = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualLiquidacaoCBLC
			{
				get
				{
					System.Decimal? data = entity.PercentualLiquidacaoCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualLiquidacaoCBLC = null;
					else entity.PercentualLiquidacaoCBLC = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualRegistroCBLC
			{
				get
				{
					System.Decimal? data = entity.PercentualRegistroCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualRegistroCBLC = null;
					else entity.PercentualRegistroCBLC = Convert.ToDecimal(value);
				}
			}
				
			public System.String MinimoBovespa
			{
				get
				{
					System.Decimal? data = entity.MinimoBovespa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MinimoBovespa = null;
					else entity.MinimoBovespa = Convert.ToDecimal(value);
				}
			}
				
			public System.String MinimoCBLC
			{
				get
				{
					System.Decimal? data = entity.MinimoCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MinimoCBLC = null;
					else entity.MinimoCBLC = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaCustosBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaCustosBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaCustosBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaCustosBolsa : esTabelaCustosBolsa
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaCustosBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCustosBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBolsaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBolsaMetadata.ColumnNames.IdTabela, esSystemType.Int16);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBolsaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem PercentualEmolumentoBolsa
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBolsaMetadata.ColumnNames.PercentualEmolumentoBolsa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualRegistroBolsa
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBolsaMetadata.ColumnNames.PercentualRegistroBolsa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualLiquidacaoCBLC
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBolsaMetadata.ColumnNames.PercentualLiquidacaoCBLC, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualRegistroCBLC
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBolsaMetadata.ColumnNames.PercentualRegistroCBLC, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem MinimoBovespa
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBolsaMetadata.ColumnNames.MinimoBovespa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem MinimoCBLC
		{
			get
			{
				return new esQueryItem(this, TabelaCustosBolsaMetadata.ColumnNames.MinimoCBLC, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaCustosBolsaCollection")]
	public partial class TabelaCustosBolsaCollection : esTabelaCustosBolsaCollection, IEnumerable<TabelaCustosBolsa>
	{
		public TabelaCustosBolsaCollection()
		{

		}
		
		public static implicit operator List<TabelaCustosBolsa>(TabelaCustosBolsaCollection coll)
		{
			List<TabelaCustosBolsa> list = new List<TabelaCustosBolsa>();
			
			foreach (TabelaCustosBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaCustosBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCustosBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaCustosBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaCustosBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaCustosBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCustosBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaCustosBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaCustosBolsa AddNew()
		{
			TabelaCustosBolsa entity = base.AddNewEntity() as TabelaCustosBolsa;
			
			return entity;
		}

		public TabelaCustosBolsa FindByPrimaryKey(System.DateTime dataReferencia, System.Int16 idTabela)
		{
			return base.FindByPrimaryKey(dataReferencia, idTabela) as TabelaCustosBolsa;
		}


		#region IEnumerable<TabelaCustosBolsa> Members

		IEnumerator<TabelaCustosBolsa> IEnumerable<TabelaCustosBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaCustosBolsa;
			}
		}

		#endregion
		
		private TabelaCustosBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaCustosBolsa' table
	/// </summary>

	[Serializable]
	public partial class TabelaCustosBolsa : esTabelaCustosBolsa
	{
		public TabelaCustosBolsa()
		{

		}
	
		public TabelaCustosBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCustosBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaCustosBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCustosBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaCustosBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCustosBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaCustosBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaCustosBolsaQuery query;
	}



	[Serializable]
	public partial class TabelaCustosBolsaQuery : esTabelaCustosBolsaQuery
	{
		public TabelaCustosBolsaQuery()
		{

		}		
		
		public TabelaCustosBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaCustosBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaCustosBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaCustosBolsaMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaCustosBolsaMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBolsaMetadata.ColumnNames.IdTabela, 1, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaCustosBolsaMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBolsaMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCustosBolsaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBolsaMetadata.ColumnNames.PercentualEmolumentoBolsa, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBolsaMetadata.PropertyNames.PercentualEmolumentoBolsa;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBolsaMetadata.ColumnNames.PercentualRegistroBolsa, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBolsaMetadata.PropertyNames.PercentualRegistroBolsa;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBolsaMetadata.ColumnNames.PercentualLiquidacaoCBLC, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBolsaMetadata.PropertyNames.PercentualLiquidacaoCBLC;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBolsaMetadata.ColumnNames.PercentualRegistroCBLC, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBolsaMetadata.PropertyNames.PercentualRegistroCBLC;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBolsaMetadata.ColumnNames.MinimoBovespa, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBolsaMetadata.PropertyNames.MinimoBovespa;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCustosBolsaMetadata.ColumnNames.MinimoCBLC, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCustosBolsaMetadata.PropertyNames.MinimoCBLC;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaCustosBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdTabela = "IdTabela";
			 public const string Descricao = "Descricao";
			 public const string PercentualEmolumentoBolsa = "PercentualEmolumentoBolsa";
			 public const string PercentualRegistroBolsa = "PercentualRegistroBolsa";
			 public const string PercentualLiquidacaoCBLC = "PercentualLiquidacaoCBLC";
			 public const string PercentualRegistroCBLC = "PercentualRegistroCBLC";
			 public const string MinimoBovespa = "MinimoBovespa";
			 public const string MinimoCBLC = "MinimoCBLC";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdTabela = "IdTabela";
			 public const string Descricao = "Descricao";
			 public const string PercentualEmolumentoBolsa = "PercentualEmolumentoBolsa";
			 public const string PercentualRegistroBolsa = "PercentualRegistroBolsa";
			 public const string PercentualLiquidacaoCBLC = "PercentualLiquidacaoCBLC";
			 public const string PercentualRegistroCBLC = "PercentualRegistroCBLC";
			 public const string MinimoBovespa = "MinimoBovespa";
			 public const string MinimoCBLC = "MinimoCBLC";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaCustosBolsaMetadata))
			{
				if(TabelaCustosBolsaMetadata.mapDelegates == null)
				{
					TabelaCustosBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaCustosBolsaMetadata.meta == null)
				{
					TabelaCustosBolsaMetadata.meta = new TabelaCustosBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdTabela", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PercentualEmolumentoBolsa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualRegistroBolsa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualLiquidacaoCBLC", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualRegistroCBLC", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("MinimoBovespa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("MinimoCBLC", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaCustosBolsa";
				meta.Destination = "TabelaCustosBolsa";
				
				meta.spInsert = "proc_TabelaCustosBolsaInsert";				
				meta.spUpdate = "proc_TabelaCustosBolsaUpdate";		
				meta.spDelete = "proc_TabelaCustosBolsaDelete";
				meta.spLoadAll = "proc_TabelaCustosBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaCustosBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaCustosBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
