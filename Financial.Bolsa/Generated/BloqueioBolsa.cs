/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			

using Financial.Investidor;										
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esBloqueioBolsaCollection : esEntityCollection
	{
		public esBloqueioBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "BloqueioBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esBloqueioBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esBloqueioBolsaQuery);
		}
		#endregion
		
		virtual public BloqueioBolsa DetachEntity(BloqueioBolsa entity)
		{
			return base.DetachEntity(entity) as BloqueioBolsa;
		}
		
		virtual public BloqueioBolsa AttachEntity(BloqueioBolsa entity)
		{
			return base.AttachEntity(entity) as BloqueioBolsa;
		}
		
		virtual public void Combine(BloqueioBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public BloqueioBolsa this[int index]
		{
			get
			{
				return base[index] as BloqueioBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(BloqueioBolsa);
		}
	}



	[Serializable]
	abstract public class esBloqueioBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esBloqueioBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esBloqueioBolsa()
		{

		}

		public esBloqueioBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idBloqueio)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBloqueio);
			else
				return LoadByPrimaryKeyStoredProcedure(idBloqueio);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idBloqueio)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esBloqueioBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdBloqueio == idBloqueio);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idBloqueio)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBloqueio);
			else
				return LoadByPrimaryKeyStoredProcedure(idBloqueio);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idBloqueio)
		{
			esBloqueioBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdBloqueio == idBloqueio);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idBloqueio)
		{
			esParameters parms = new esParameters();
			parms.Add("IdBloqueio",idBloqueio);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdBloqueio": this.str.IdBloqueio = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "TipoCarteiraBloqueada": this.str.TipoCarteiraBloqueada = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdBloqueio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBloqueio = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "TipoCarteiraBloqueada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCarteiraBloqueada = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to BloqueioBolsa.IdBloqueio
		/// </summary>
		virtual public System.Int32? IdBloqueio
		{
			get
			{
				return base.GetSystemInt32(BloqueioBolsaMetadata.ColumnNames.IdBloqueio);
			}
			
			set
			{
				base.SetSystemInt32(BloqueioBolsaMetadata.ColumnNames.IdBloqueio, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioBolsa.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(BloqueioBolsaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(BloqueioBolsaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioBolsa.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(BloqueioBolsaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(BloqueioBolsaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(BloqueioBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(BloqueioBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to BloqueioBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(BloqueioBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(BloqueioBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to BloqueioBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(BloqueioBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(BloqueioBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioBolsa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(BloqueioBolsaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				base.SetSystemInt32(BloqueioBolsaMetadata.ColumnNames.IdAgente, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioBolsa.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(BloqueioBolsaMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(BloqueioBolsaMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioBolsa.TipoCarteiraBloqueada
		/// </summary>
		virtual public System.Int32? TipoCarteiraBloqueada
		{
			get
			{
				return base.GetSystemInt32(BloqueioBolsaMetadata.ColumnNames.TipoCarteiraBloqueada);
			}
			
			set
			{
				base.SetSystemInt32(BloqueioBolsaMetadata.ColumnNames.TipoCarteiraBloqueada, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esBloqueioBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdBloqueio
			{
				get
				{
					System.Int32? data = entity.IdBloqueio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBloqueio = null;
					else entity.IdBloqueio = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String TipoCarteiraBloqueada
			{
				get
				{
					System.Int32? data = entity.TipoCarteiraBloqueada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCarteiraBloqueada = null;
					else entity.TipoCarteiraBloqueada = Convert.ToInt32(value);
				}
			}
			

			private esBloqueioBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esBloqueioBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esBloqueioBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class BloqueioBolsa : esBloqueioBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_BloqueioBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_BloqueioBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esBloqueioBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return BloqueioBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdBloqueio
		{
			get
			{
				return new esQueryItem(this, BloqueioBolsaMetadata.ColumnNames.IdBloqueio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, BloqueioBolsaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, BloqueioBolsaMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, BloqueioBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, BloqueioBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, BloqueioBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, BloqueioBolsaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, BloqueioBolsaMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoCarteiraBloqueada
		{
			get
			{
				return new esQueryItem(this, BloqueioBolsaMetadata.ColumnNames.TipoCarteiraBloqueada, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("BloqueioBolsaCollection")]
	public partial class BloqueioBolsaCollection : esBloqueioBolsaCollection, IEnumerable<BloqueioBolsa>
	{
		public BloqueioBolsaCollection()
		{

		}
		
		public static implicit operator List<BloqueioBolsa>(BloqueioBolsaCollection coll)
		{
			List<BloqueioBolsa> list = new List<BloqueioBolsa>();
			
			foreach (BloqueioBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  BloqueioBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BloqueioBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new BloqueioBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new BloqueioBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public BloqueioBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BloqueioBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(BloqueioBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public BloqueioBolsa AddNew()
		{
			BloqueioBolsa entity = base.AddNewEntity() as BloqueioBolsa;
			
			return entity;
		}

		public BloqueioBolsa FindByPrimaryKey(System.Int32 idBloqueio)
		{
			return base.FindByPrimaryKey(idBloqueio) as BloqueioBolsa;
		}


		#region IEnumerable<BloqueioBolsa> Members

		IEnumerator<BloqueioBolsa> IEnumerable<BloqueioBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as BloqueioBolsa;
			}
		}

		#endregion
		
		private BloqueioBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'BloqueioBolsa' table
	/// </summary>

	[Serializable]
	public partial class BloqueioBolsa : esBloqueioBolsa
	{
		public BloqueioBolsa()
		{

		}
	
		public BloqueioBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return BloqueioBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esBloqueioBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BloqueioBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public BloqueioBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BloqueioBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(BloqueioBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private BloqueioBolsaQuery query;
	}



	[Serializable]
	public partial class BloqueioBolsaQuery : esBloqueioBolsaQuery
	{
		public BloqueioBolsaQuery()
		{

		}		
		
		public BloqueioBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class BloqueioBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected BloqueioBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(BloqueioBolsaMetadata.ColumnNames.IdBloqueio, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioBolsaMetadata.PropertyNames.IdBloqueio;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioBolsaMetadata.ColumnNames.DataOperacao, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = BloqueioBolsaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioBolsaMetadata.ColumnNames.TipoOperacao, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = BloqueioBolsaMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioBolsaMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioBolsaMetadata.ColumnNames.CdAtivoBolsa, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = BloqueioBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioBolsaMetadata.ColumnNames.Quantidade, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = BloqueioBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioBolsaMetadata.ColumnNames.IdAgente, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioBolsaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioBolsaMetadata.ColumnNames.Observacao, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = BloqueioBolsaMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioBolsaMetadata.ColumnNames.TipoCarteiraBloqueada, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioBolsaMetadata.PropertyNames.TipoCarteiraBloqueada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public BloqueioBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdBloqueio = "IdBloqueio";
			 public const string DataOperacao = "DataOperacao";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Quantidade = "Quantidade";
			 public const string IdAgente = "IdAgente";
			 public const string Observacao = "Observacao";
			 public const string TipoCarteiraBloqueada = "TipoCarteiraBloqueada";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdBloqueio = "IdBloqueio";
			 public const string DataOperacao = "DataOperacao";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Quantidade = "Quantidade";
			 public const string IdAgente = "IdAgente";
			 public const string Observacao = "Observacao";
			 public const string TipoCarteiraBloqueada = "TipoCarteiraBloqueada";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(BloqueioBolsaMetadata))
			{
				if(BloqueioBolsaMetadata.mapDelegates == null)
				{
					BloqueioBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (BloqueioBolsaMetadata.meta == null)
				{
					BloqueioBolsaMetadata.meta = new BloqueioBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdBloqueio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoCarteiraBloqueada", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "BloqueioBolsa";
				meta.Destination = "BloqueioBolsa";
				
				meta.spInsert = "proc_BloqueioBolsaInsert";				
				meta.spUpdate = "proc_BloqueioBolsaUpdate";		
				meta.spDelete = "proc_BloqueioBolsaDelete";
				meta.spLoadAll = "proc_BloqueioBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_BloqueioBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private BloqueioBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
