/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:47
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Common;



namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esPosicaoEmprestimoBolsaHistoricoCollection : esEntityCollection
	{
		public esPosicaoEmprestimoBolsaHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoEmprestimoBolsaHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoEmprestimoBolsaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoEmprestimoBolsaHistoricoQuery);
		}
		#endregion
		
		virtual public PosicaoEmprestimoBolsaHistorico DetachEntity(PosicaoEmprestimoBolsaHistorico entity)
		{
			return base.DetachEntity(entity) as PosicaoEmprestimoBolsaHistorico;
		}
		
		virtual public PosicaoEmprestimoBolsaHistorico AttachEntity(PosicaoEmprestimoBolsaHistorico entity)
		{
			return base.AttachEntity(entity) as PosicaoEmprestimoBolsaHistorico;
		}
		
		virtual public void Combine(PosicaoEmprestimoBolsaHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoEmprestimoBolsaHistorico this[int index]
		{
			get
			{
				return base[index] as PosicaoEmprestimoBolsaHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoEmprestimoBolsaHistorico);
		}
	}



	[Serializable]
	abstract public class esPosicaoEmprestimoBolsaHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoEmprestimoBolsaHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoEmprestimoBolsaHistorico()
		{

		}

		public esPosicaoEmprestimoBolsaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoEmprestimoBolsaHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esPosicaoEmprestimoBolsaHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "PULiquidoOriginal": this.str.PULiquidoOriginal = (string)value; break;							
						case "ValorCorrigidoJuros": this.str.ValorCorrigidoJuros = (string)value; break;							
						case "ValorCorrigidoComissao": this.str.ValorCorrigidoComissao = (string)value; break;							
						case "ValorCorrigidoCBLC": this.str.ValorCorrigidoCBLC = (string)value; break;							
						case "ValorBase": this.str.ValorBase = (string)value; break;							
						case "PontaEmprestimo": this.str.PontaEmprestimo = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "TaxaOperacao": this.str.TaxaOperacao = (string)value; break;							
						case "TaxaComissao": this.str.TaxaComissao = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "TipoEmprestimo": this.str.TipoEmprestimo = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "ValorDiarioJuros": this.str.ValorDiarioJuros = (string)value; break;							
						case "ValorDiarioComissao": this.str.ValorDiarioComissao = (string)value; break;							
						case "ValorDiarioCBLC": this.str.ValorDiarioCBLC = (string)value; break;							
						case "PermiteDevolucaoAntecipada": this.str.PermiteDevolucaoAntecipada = (string)value; break;							
						case "DataInicialDevolucaoAntecipada": this.str.DataInicialDevolucaoAntecipada = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "PULiquidoOriginal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PULiquidoOriginal = (System.Decimal?)value;
							break;
						
						case "ValorCorrigidoJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCorrigidoJuros = (System.Decimal?)value;
							break;
						
						case "ValorCorrigidoComissao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCorrigidoComissao = (System.Decimal?)value;
							break;
						
						case "ValorCorrigidoCBLC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCorrigidoCBLC = (System.Decimal?)value;
							break;
						
						case "ValorBase":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBase = (System.Decimal?)value;
							break;
						
						case "PontaEmprestimo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.PontaEmprestimo = (System.Byte?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "TaxaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaOperacao = (System.Decimal?)value;
							break;
						
						case "TaxaComissao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaComissao = (System.Decimal?)value;
							break;
						
						case "NumeroContrato":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroContrato = (System.Int32?)value;
							break;
						
						case "TipoEmprestimo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoEmprestimo = (System.Byte?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "ValorDiarioJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDiarioJuros = (System.Decimal?)value;
							break;
						
						case "ValorDiarioComissao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDiarioComissao = (System.Decimal?)value;
							break;
						
						case "ValorDiarioCBLC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorDiarioCBLC = (System.Decimal?)value;
							break;
						
						case "DataInicialDevolucaoAntecipada":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicialDevolucaoAntecipada = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.PULiquidoOriginal
		/// </summary>
		virtual public System.Decimal? PULiquidoOriginal
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PULiquidoOriginal);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PULiquidoOriginal, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros
		/// </summary>
		virtual public System.Decimal? ValorCorrigidoJuros
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoJuros);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.ValorCorrigidoComissao
		/// </summary>
		virtual public System.Decimal? ValorCorrigidoComissao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoComissao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoComissao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.ValorCorrigidoCBLC
		/// </summary>
		virtual public System.Decimal? ValorCorrigidoCBLC
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoCBLC);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoCBLC, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.ValorBase
		/// </summary>
		virtual public System.Decimal? ValorBase
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorBase);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorBase, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.PontaEmprestimo
		/// </summary>
		virtual public System.Byte? PontaEmprestimo
		{
			get
			{
				return base.GetSystemByte(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PontaEmprestimo);
			}
			
			set
			{
				base.SetSystemByte(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PontaEmprestimo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.TaxaOperacao
		/// </summary>
		virtual public System.Decimal? TaxaOperacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TaxaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TaxaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.TaxaComissao
		/// </summary>
		virtual public System.Decimal? TaxaComissao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TaxaComissao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TaxaComissao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.NumeroContrato
		/// </summary>
		virtual public System.Int32? NumeroContrato
		{
			get
			{
				return base.GetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.TipoEmprestimo
		/// </summary>
		virtual public System.Byte? TipoEmprestimo
		{
			get
			{
				return base.GetSystemByte(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TipoEmprestimo);
			}
			
			set
			{
				base.SetSystemByte(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TipoEmprestimo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.ValorDiarioJuros
		/// </summary>
		virtual public System.Decimal? ValorDiarioJuros
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioJuros);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.ValorDiarioComissao
		/// </summary>
		virtual public System.Decimal? ValorDiarioComissao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioComissao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioComissao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.ValorDiarioCBLC
		/// </summary>
		virtual public System.Decimal? ValorDiarioCBLC
		{
			get
			{
				return base.GetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioCBLC);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioCBLC, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.PermiteDevolucaoAntecipada
		/// </summary>
		virtual public System.String PermiteDevolucaoAntecipada
		{
			get
			{
				return base.GetSystemString(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PermiteDevolucaoAntecipada);
			}
			
			set
			{
				base.SetSystemString(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PermiteDevolucaoAntecipada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoEmprestimoBolsaHistorico.DataInicialDevolucaoAntecipada
		/// </summary>
		virtual public System.DateTime? DataInicialDevolucaoAntecipada
		{
			get
			{
				return base.GetSystemDateTime(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataInicialDevolucaoAntecipada);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataInicialDevolucaoAntecipada, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoEmprestimoBolsaHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PULiquidoOriginal
			{
				get
				{
					System.Decimal? data = entity.PULiquidoOriginal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PULiquidoOriginal = null;
					else entity.PULiquidoOriginal = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCorrigidoJuros
			{
				get
				{
					System.Decimal? data = entity.ValorCorrigidoJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCorrigidoJuros = null;
					else entity.ValorCorrigidoJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCorrigidoComissao
			{
				get
				{
					System.Decimal? data = entity.ValorCorrigidoComissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCorrigidoComissao = null;
					else entity.ValorCorrigidoComissao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCorrigidoCBLC
			{
				get
				{
					System.Decimal? data = entity.ValorCorrigidoCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCorrigidoCBLC = null;
					else entity.ValorCorrigidoCBLC = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorBase
			{
				get
				{
					System.Decimal? data = entity.ValorBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBase = null;
					else entity.ValorBase = Convert.ToDecimal(value);
				}
			}
				
			public System.String PontaEmprestimo
			{
				get
				{
					System.Byte? data = entity.PontaEmprestimo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PontaEmprestimo = null;
					else entity.PontaEmprestimo = Convert.ToByte(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaOperacao
			{
				get
				{
					System.Decimal? data = entity.TaxaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaOperacao = null;
					else entity.TaxaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaComissao
			{
				get
				{
					System.Decimal? data = entity.TaxaComissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaComissao = null;
					else entity.TaxaComissao = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.Int32? data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoEmprestimo
			{
				get
				{
					System.Byte? data = entity.TipoEmprestimo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEmprestimo = null;
					else entity.TipoEmprestimo = Convert.ToByte(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorDiarioJuros
			{
				get
				{
					System.Decimal? data = entity.ValorDiarioJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDiarioJuros = null;
					else entity.ValorDiarioJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorDiarioComissao
			{
				get
				{
					System.Decimal? data = entity.ValorDiarioComissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDiarioComissao = null;
					else entity.ValorDiarioComissao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorDiarioCBLC
			{
				get
				{
					System.Decimal? data = entity.ValorDiarioCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorDiarioCBLC = null;
					else entity.ValorDiarioCBLC = Convert.ToDecimal(value);
				}
			}
				
			public System.String PermiteDevolucaoAntecipada
			{
				get
				{
					System.String data = entity.PermiteDevolucaoAntecipada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermiteDevolucaoAntecipada = null;
					else entity.PermiteDevolucaoAntecipada = Convert.ToString(value);
				}
			}
				
			public System.String DataInicialDevolucaoAntecipada
			{
				get
				{
					System.DateTime? data = entity.DataInicialDevolucaoAntecipada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicialDevolucaoAntecipada = null;
					else entity.DataInicialDevolucaoAntecipada = Convert.ToDateTime(value);
				}
			}
			

			private esPosicaoEmprestimoBolsaHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoEmprestimoBolsaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoEmprestimoBolsaHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoEmprestimoBolsaHistorico : esPosicaoEmprestimoBolsaHistorico
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PosicaoEmprestimoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoEmprestimoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_PosicaoEmprestimoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoEmprestimoBolsaHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoEmprestimoBolsaHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PULiquidoOriginal
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PULiquidoOriginal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCorrigidoJuros
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCorrigidoComissao
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoComissao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCorrigidoCBLC
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoCBLC, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorBase
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorBase, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PontaEmprestimo
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PontaEmprestimo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TaxaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaComissao
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TaxaComissao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.NumeroContrato, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoEmprestimo
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TipoEmprestimo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorDiarioJuros
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorDiarioComissao
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioComissao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorDiarioCBLC
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioCBLC, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PermiteDevolucaoAntecipada
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PermiteDevolucaoAntecipada, esSystemType.String);
			}
		} 
		
		public esQueryItem DataInicialDevolucaoAntecipada
		{
			get
			{
				return new esQueryItem(this, PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataInicialDevolucaoAntecipada, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoEmprestimoBolsaHistoricoCollection")]
	public partial class PosicaoEmprestimoBolsaHistoricoCollection : esPosicaoEmprestimoBolsaHistoricoCollection, IEnumerable<PosicaoEmprestimoBolsaHistorico>
	{
		public PosicaoEmprestimoBolsaHistoricoCollection()
		{

		}
		
		public static implicit operator List<PosicaoEmprestimoBolsaHistorico>(PosicaoEmprestimoBolsaHistoricoCollection coll)
		{
			List<PosicaoEmprestimoBolsaHistorico> list = new List<PosicaoEmprestimoBolsaHistorico>();
			
			foreach (PosicaoEmprestimoBolsaHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoEmprestimoBolsaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoEmprestimoBolsaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoEmprestimoBolsaHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoEmprestimoBolsaHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoEmprestimoBolsaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoEmprestimoBolsaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoEmprestimoBolsaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoEmprestimoBolsaHistorico AddNew()
		{
			PosicaoEmprestimoBolsaHistorico entity = base.AddNewEntity() as PosicaoEmprestimoBolsaHistorico;
			
			return entity;
		}

		public PosicaoEmprestimoBolsaHistorico FindByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idPosicao, dataHistorico) as PosicaoEmprestimoBolsaHistorico;
		}


		#region IEnumerable<PosicaoEmprestimoBolsaHistorico> Members

		IEnumerator<PosicaoEmprestimoBolsaHistorico> IEnumerable<PosicaoEmprestimoBolsaHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoEmprestimoBolsaHistorico;
			}
		}

		#endregion
		
		private PosicaoEmprestimoBolsaHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoEmprestimoBolsaHistorico' table
	/// </summary>

	[Serializable]
	public partial class PosicaoEmprestimoBolsaHistorico : esPosicaoEmprestimoBolsaHistorico
	{
		public PosicaoEmprestimoBolsaHistorico()
		{

		}
	
		public PosicaoEmprestimoBolsaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoEmprestimoBolsaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoEmprestimoBolsaHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoEmprestimoBolsaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoEmprestimoBolsaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoEmprestimoBolsaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoEmprestimoBolsaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoEmprestimoBolsaHistoricoQuery query;
	}



	[Serializable]
	public partial class PosicaoEmprestimoBolsaHistoricoQuery : esPosicaoEmprestimoBolsaHistoricoQuery
	{
		public PosicaoEmprestimoBolsaHistoricoQuery()
		{

		}		
		
		public PosicaoEmprestimoBolsaHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoEmprestimoBolsaHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoEmprestimoBolsaHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdOperacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdAgente, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PUMercado, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PULiquidoOriginal, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.PULiquidoOriginal;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoJuros, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.ValorCorrigidoJuros;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoComissao, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.ValorCorrigidoComissao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorCorrigidoCBLC, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.ValorCorrigidoCBLC;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorBase, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.ValorBase;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PontaEmprestimo, 13, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.PontaEmprestimo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataRegistro, 14, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataVencimento, 15, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TaxaOperacao, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.TaxaOperacao;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TaxaComissao, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.TaxaComissao;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.NumeroContrato, 18, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.NumeroContrato;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.TipoEmprestimo, 19, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.TipoEmprestimo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorMercado, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.IdTrader, 21, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioJuros, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.ValorDiarioJuros;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioComissao, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.ValorDiarioComissao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.ValorDiarioCBLC, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.ValorDiarioCBLC;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.PermiteDevolucaoAntecipada, 25, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.PermiteDevolucaoAntecipada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.DataInicialDevolucaoAntecipada, 26, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoEmprestimoBolsaHistoricoMetadata.PropertyNames.DataInicialDevolucaoAntecipada;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoEmprestimoBolsaHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Quantidade = "Quantidade";
			 public const string PUMercado = "PUMercado";
			 public const string PULiquidoOriginal = "PULiquidoOriginal";
			 public const string ValorCorrigidoJuros = "ValorCorrigidoJuros";
			 public const string ValorCorrigidoComissao = "ValorCorrigidoComissao";
			 public const string ValorCorrigidoCBLC = "ValorCorrigidoCBLC";
			 public const string ValorBase = "ValorBase";
			 public const string PontaEmprestimo = "PontaEmprestimo";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataVencimento = "DataVencimento";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string TaxaComissao = "TaxaComissao";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string TipoEmprestimo = "TipoEmprestimo";
			 public const string ValorMercado = "ValorMercado";
			 public const string IdTrader = "IdTrader";
			 public const string ValorDiarioJuros = "ValorDiarioJuros";
			 public const string ValorDiarioComissao = "ValorDiarioComissao";
			 public const string ValorDiarioCBLC = "ValorDiarioCBLC";
			 public const string PermiteDevolucaoAntecipada = "PermiteDevolucaoAntecipada";
			 public const string DataInicialDevolucaoAntecipada = "DataInicialDevolucaoAntecipada";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Quantidade = "Quantidade";
			 public const string PUMercado = "PUMercado";
			 public const string PULiquidoOriginal = "PULiquidoOriginal";
			 public const string ValorCorrigidoJuros = "ValorCorrigidoJuros";
			 public const string ValorCorrigidoComissao = "ValorCorrigidoComissao";
			 public const string ValorCorrigidoCBLC = "ValorCorrigidoCBLC";
			 public const string ValorBase = "ValorBase";
			 public const string PontaEmprestimo = "PontaEmprestimo";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataVencimento = "DataVencimento";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string TaxaComissao = "TaxaComissao";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string TipoEmprestimo = "TipoEmprestimo";
			 public const string ValorMercado = "ValorMercado";
			 public const string IdTrader = "IdTrader";
			 public const string ValorDiarioJuros = "ValorDiarioJuros";
			 public const string ValorDiarioComissao = "ValorDiarioComissao";
			 public const string ValorDiarioCBLC = "ValorDiarioCBLC";
			 public const string PermiteDevolucaoAntecipada = "PermiteDevolucaoAntecipada";
			 public const string DataInicialDevolucaoAntecipada = "DataInicialDevolucaoAntecipada";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoEmprestimoBolsaHistoricoMetadata))
			{
				if(PosicaoEmprestimoBolsaHistoricoMetadata.mapDelegates == null)
				{
					PosicaoEmprestimoBolsaHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoEmprestimoBolsaHistoricoMetadata.meta == null)
				{
					PosicaoEmprestimoBolsaHistoricoMetadata.meta = new PosicaoEmprestimoBolsaHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PULiquidoOriginal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCorrigidoJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCorrigidoComissao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCorrigidoCBLC", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorBase", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PontaEmprestimo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaComissao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoEmprestimo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorDiarioJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorDiarioComissao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorDiarioCBLC", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PermiteDevolucaoAntecipada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataInicialDevolucaoAntecipada", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "PosicaoEmprestimoBolsaHistorico";
				meta.Destination = "PosicaoEmprestimoBolsaHistorico";
				
				meta.spInsert = "proc_PosicaoEmprestimoBolsaHistoricoInsert";				
				meta.spUpdate = "proc_PosicaoEmprestimoBolsaHistoricoUpdate";		
				meta.spDelete = "proc_PosicaoEmprestimoBolsaHistoricoDelete";
				meta.spLoadAll = "proc_PosicaoEmprestimoBolsaHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoEmprestimoBolsaHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoEmprestimoBolsaHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
