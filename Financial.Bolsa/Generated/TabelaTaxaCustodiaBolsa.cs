/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		

using Financial.Common;
using Financial.Investidor;										



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esTabelaTaxaCustodiaBolsaCollection : esEntityCollection
	{
		public esTabelaTaxaCustodiaBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaTaxaCustodiaBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaTaxaCustodiaBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaTaxaCustodiaBolsaQuery);
		}
		#endregion
		
		virtual public TabelaTaxaCustodiaBolsa DetachEntity(TabelaTaxaCustodiaBolsa entity)
		{
			return base.DetachEntity(entity) as TabelaTaxaCustodiaBolsa;
		}
		
		virtual public TabelaTaxaCustodiaBolsa AttachEntity(TabelaTaxaCustodiaBolsa entity)
		{
			return base.AttachEntity(entity) as TabelaTaxaCustodiaBolsa;
		}
		
		virtual public void Combine(TabelaTaxaCustodiaBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaTaxaCustodiaBolsa this[int index]
		{
			get
			{
				return base[index] as TabelaTaxaCustodiaBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaTaxaCustodiaBolsa);
		}
	}



	[Serializable]
	abstract public class esTabelaTaxaCustodiaBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaTaxaCustodiaBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaTaxaCustodiaBolsa()
		{

		}

		public esTabelaTaxaCustodiaBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idCliente, idAgente);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idCliente, idAgente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaTaxaCustodiaBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.IdCliente == idCliente, query.IdAgente == idAgente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, idCliente, idAgente);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, idCliente, idAgente);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgente)
		{
			esTabelaTaxaCustodiaBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.IdCliente == idCliente, query.IdAgente == idAgente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgente)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("IdCliente",idCliente);			parms.Add("IdAgente",idAgente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "NumeroDiasCustodia": this.str.NumeroDiasCustodia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "NumeroDiasCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.NumeroDiasCustodia = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaTaxaCustodiaBolsa.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaCustodiaBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaCustodiaBolsa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaCustodiaBolsa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaTaxaCustodiaBolsa.NumeroDiasCustodia
		/// </summary>
		virtual public System.Int16? NumeroDiasCustodia
		{
			get
			{
				return base.GetSystemInt16(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.NumeroDiasCustodia);
			}
			
			set
			{
				base.SetSystemInt16(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.NumeroDiasCustodia, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaTaxaCustodiaBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroDiasCustodia
			{
				get
				{
					System.Int16? data = entity.NumeroDiasCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroDiasCustodia = null;
					else entity.NumeroDiasCustodia = Convert.ToInt16(value);
				}
			}
			

			private esTabelaTaxaCustodiaBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaTaxaCustodiaBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaTaxaCustodiaBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaTaxaCustodiaBolsa : esTabelaTaxaCustodiaBolsa
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_TabelaTaxaCustodiaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_TabelaTaxaCustodiaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaTaxaCustodiaBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaCustodiaBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaCustodiaBolsaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaCustodiaBolsaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroDiasCustodia
		{
			get
			{
				return new esQueryItem(this, TabelaTaxaCustodiaBolsaMetadata.ColumnNames.NumeroDiasCustodia, esSystemType.Int16);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaTaxaCustodiaBolsaCollection")]
	public partial class TabelaTaxaCustodiaBolsaCollection : esTabelaTaxaCustodiaBolsaCollection, IEnumerable<TabelaTaxaCustodiaBolsa>
	{
		public TabelaTaxaCustodiaBolsaCollection()
		{

		}
		
		public static implicit operator List<TabelaTaxaCustodiaBolsa>(TabelaTaxaCustodiaBolsaCollection coll)
		{
			List<TabelaTaxaCustodiaBolsa> list = new List<TabelaTaxaCustodiaBolsa>();
			
			foreach (TabelaTaxaCustodiaBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaTaxaCustodiaBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaCustodiaBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaTaxaCustodiaBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaTaxaCustodiaBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaTaxaCustodiaBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaCustodiaBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaTaxaCustodiaBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaTaxaCustodiaBolsa AddNew()
		{
			TabelaTaxaCustodiaBolsa entity = base.AddNewEntity() as TabelaTaxaCustodiaBolsa;
			
			return entity;
		}

		public TabelaTaxaCustodiaBolsa FindByPrimaryKey(System.DateTime dataReferencia, System.Int32 idCliente, System.Int32 idAgente)
		{
			return base.FindByPrimaryKey(dataReferencia, idCliente, idAgente) as TabelaTaxaCustodiaBolsa;
		}


		#region IEnumerable<TabelaTaxaCustodiaBolsa> Members

		IEnumerator<TabelaTaxaCustodiaBolsa> IEnumerable<TabelaTaxaCustodiaBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaTaxaCustodiaBolsa;
			}
		}

		#endregion
		
		private TabelaTaxaCustodiaBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaTaxaCustodiaBolsa' table
	/// </summary>

	[Serializable]
	public partial class TabelaTaxaCustodiaBolsa : esTabelaTaxaCustodiaBolsa
	{
		public TabelaTaxaCustodiaBolsa()
		{

		}
	
		public TabelaTaxaCustodiaBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaTaxaCustodiaBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaTaxaCustodiaBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaTaxaCustodiaBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaTaxaCustodiaBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaTaxaCustodiaBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaTaxaCustodiaBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaTaxaCustodiaBolsaQuery query;
	}



	[Serializable]
	public partial class TabelaTaxaCustodiaBolsaQuery : esTabelaTaxaCustodiaBolsaQuery
	{
		public TabelaTaxaCustodiaBolsaQuery()
		{

		}		
		
		public TabelaTaxaCustodiaBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaTaxaCustodiaBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaTaxaCustodiaBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaTaxaCustodiaBolsaMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaCustodiaBolsaMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.IdAgente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaTaxaCustodiaBolsaMetadata.PropertyNames.IdAgente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.Valor, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaTaxaCustodiaBolsaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaTaxaCustodiaBolsaMetadata.ColumnNames.NumeroDiasCustodia, 4, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TabelaTaxaCustodiaBolsaMetadata.PropertyNames.NumeroDiasCustodia;	
			c.NumericPrecision = 5;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaTaxaCustodiaBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string Valor = "Valor";
			 public const string NumeroDiasCustodia = "NumeroDiasCustodia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string Valor = "Valor";
			 public const string NumeroDiasCustodia = "NumeroDiasCustodia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaTaxaCustodiaBolsaMetadata))
			{
				if(TabelaTaxaCustodiaBolsaMetadata.mapDelegates == null)
				{
					TabelaTaxaCustodiaBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaTaxaCustodiaBolsaMetadata.meta == null)
				{
					TabelaTaxaCustodiaBolsaMetadata.meta = new TabelaTaxaCustodiaBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroDiasCustodia", new esTypeMap("smallint", "System.Int16"));			
				
				
				
				meta.Source = "TabelaTaxaCustodiaBolsa";
				meta.Destination = "TabelaTaxaCustodiaBolsa";
				
				meta.spInsert = "proc_TabelaTaxaCustodiaBolsaInsert";				
				meta.spUpdate = "proc_TabelaTaxaCustodiaBolsaUpdate";		
				meta.spDelete = "proc_TabelaTaxaCustodiaBolsaDelete";
				meta.spLoadAll = "proc_TabelaTaxaCustodiaBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaTaxaCustodiaBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaTaxaCustodiaBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
