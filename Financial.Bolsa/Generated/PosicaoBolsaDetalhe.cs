/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Investidor;





































		



			
					


		

		
		
		


		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esPosicaoBolsaDetalheCollection : esEntityCollection
	{
		public esPosicaoBolsaDetalheCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoBolsaDetalheCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoBolsaDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoBolsaDetalheQuery);
		}
		#endregion
		
		virtual public PosicaoBolsaDetalhe DetachEntity(PosicaoBolsaDetalhe entity)
		{
			return base.DetachEntity(entity) as PosicaoBolsaDetalhe;
		}
		
		virtual public PosicaoBolsaDetalhe AttachEntity(PosicaoBolsaDetalhe entity)
		{
			return base.AttachEntity(entity) as PosicaoBolsaDetalhe;
		}
		
		virtual public void Combine(PosicaoBolsaDetalheCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoBolsaDetalhe this[int index]
		{
			get
			{
				return base[index] as PosicaoBolsaDetalhe;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoBolsaDetalhe);
		}
	}



	[Serializable]
	abstract public class esPosicaoBolsaDetalhe : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoBolsaDetalheQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoBolsaDetalhe()
		{

		}

		public esPosicaoBolsaDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataHistorico, System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBolsa, System.Int32 tipoCarteira)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idCliente, idAgente, cdAtivoBolsa, tipoCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idCliente, idAgente, cdAtivoBolsa, tipoCarteira);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataHistorico, System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBolsa, System.Int32 tipoCarteira)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoBolsaDetalheQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataHistorico == dataHistorico, query.IdCliente == idCliente, query.IdAgente == idAgente, query.CdAtivoBolsa == cdAtivoBolsa, query.TipoCarteira == tipoCarteira);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataHistorico, System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBolsa, System.Int32 tipoCarteira)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataHistorico, idCliente, idAgente, cdAtivoBolsa, tipoCarteira);
			else
				return LoadByPrimaryKeyStoredProcedure(dataHistorico, idCliente, idAgente, cdAtivoBolsa, tipoCarteira);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataHistorico, System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBolsa, System.Int32 tipoCarteira)
		{
			esPosicaoBolsaDetalheQuery query = this.GetDynamicQuery();
			query.Where(query.DataHistorico == dataHistorico, query.IdCliente == idCliente, query.IdAgente == idAgente, query.CdAtivoBolsa == cdAtivoBolsa, query.TipoCarteira == tipoCarteira);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataHistorico, System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBolsa, System.Int32 tipoCarteira)
		{
			esParameters parms = new esParameters();
			parms.Add("DataHistorico",dataHistorico);			parms.Add("IdCliente",idCliente);			parms.Add("IdAgente",idAgente);			parms.Add("CdAtivoBolsa",cdAtivoBolsa);			parms.Add("TipoCarteira",tipoCarteira);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "TipoCarteira": this.str.TipoCarteira = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeAbertura": this.str.QuantidadeAbertura = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "TipoCarteira":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCarteira = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeAbertura":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeAbertura = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoBolsaDetalhe.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoBolsaDetalheMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoBolsaDetalheMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaDetalhe.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoBolsaDetalheMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoBolsaDetalheMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaDetalhe.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoBolsaDetalheMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoBolsaDetalheMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaDetalhe.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(PosicaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(PosicaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaDetalhe.TipoCarteira
		/// </summary>
		virtual public System.Int32? TipoCarteira
		{
			get
			{
				return base.GetSystemInt32(PosicaoBolsaDetalheMetadata.ColumnNames.TipoCarteira);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoBolsaDetalheMetadata.ColumnNames.TipoCarteira, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaDetalhe.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaDetalheMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaDetalheMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaDetalhe.QuantidadeAbertura
		/// </summary>
		virtual public System.Decimal? QuantidadeAbertura
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaDetalheMetadata.ColumnNames.QuantidadeAbertura);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaDetalheMetadata.ColumnNames.QuantidadeAbertura, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoBolsaDetalhe entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String TipoCarteira
			{
				get
				{
					System.Int32? data = entity.TipoCarteira;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCarteira = null;
					else entity.TipoCarteira = Convert.ToInt32(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeAbertura
			{
				get
				{
					System.Decimal? data = entity.QuantidadeAbertura;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeAbertura = null;
					else entity.QuantidadeAbertura = Convert.ToDecimal(value);
				}
			}
			

			private esPosicaoBolsaDetalhe entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoBolsaDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoBolsaDetalhe can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoBolsaDetalhe : esPosicaoBolsaDetalhe
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PosicaoBolsaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_PosicaoBolsaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoBolsaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoBolsaDetalheQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoBolsaDetalheMetadata.Meta();
			}
		}	
		

		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaDetalheMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaDetalheMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaDetalheMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoCarteira
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaDetalheMetadata.ColumnNames.TipoCarteira, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaDetalheMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeAbertura
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaDetalheMetadata.ColumnNames.QuantidadeAbertura, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoBolsaDetalheCollection")]
	public partial class PosicaoBolsaDetalheCollection : esPosicaoBolsaDetalheCollection, IEnumerable<PosicaoBolsaDetalhe>
	{
		public PosicaoBolsaDetalheCollection()
		{

		}
		
		public static implicit operator List<PosicaoBolsaDetalhe>(PosicaoBolsaDetalheCollection coll)
		{
			List<PosicaoBolsaDetalhe> list = new List<PosicaoBolsaDetalhe>();
			
			foreach (PosicaoBolsaDetalhe emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoBolsaDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoBolsaDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoBolsaDetalhe(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoBolsaDetalhe();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoBolsaDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoBolsaDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoBolsaDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoBolsaDetalhe AddNew()
		{
			PosicaoBolsaDetalhe entity = base.AddNewEntity() as PosicaoBolsaDetalhe;
			
			return entity;
		}

		public PosicaoBolsaDetalhe FindByPrimaryKey(System.DateTime dataHistorico, System.Int32 idCliente, System.Int32 idAgente, System.String cdAtivoBolsa, System.Int32 tipoCarteira)
		{
			return base.FindByPrimaryKey(dataHistorico, idCliente, idAgente, cdAtivoBolsa, tipoCarteira) as PosicaoBolsaDetalhe;
		}


		#region IEnumerable<PosicaoBolsaDetalhe> Members

		IEnumerator<PosicaoBolsaDetalhe> IEnumerable<PosicaoBolsaDetalhe>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoBolsaDetalhe;
			}
		}

		#endregion
		
		private PosicaoBolsaDetalheQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoBolsaDetalhe' table
	/// </summary>

	[Serializable]
	public partial class PosicaoBolsaDetalhe : esPosicaoBolsaDetalhe
	{
		public PosicaoBolsaDetalhe()
		{

		}
	
		public PosicaoBolsaDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoBolsaDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoBolsaDetalheQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoBolsaDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoBolsaDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoBolsaDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoBolsaDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoBolsaDetalheQuery query;
	}



	[Serializable]
	public partial class PosicaoBolsaDetalheQuery : esPosicaoBolsaDetalheQuery
	{
		public PosicaoBolsaDetalheQuery()
		{

		}		
		
		public PosicaoBolsaDetalheQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoBolsaDetalheMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoBolsaDetalheMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoBolsaDetalheMetadata.ColumnNames.DataHistorico, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoBolsaDetalheMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaDetalheMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBolsaDetalheMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaDetalheMetadata.ColumnNames.IdAgente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBolsaDetalheMetadata.PropertyNames.IdAgente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoBolsaDetalheMetadata.PropertyNames.CdAtivoBolsa;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaDetalheMetadata.ColumnNames.TipoCarteira, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBolsaDetalheMetadata.PropertyNames.TipoCarteira;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaDetalheMetadata.ColumnNames.Quantidade, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaDetalheMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaDetalheMetadata.ColumnNames.QuantidadeAbertura, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaDetalheMetadata.PropertyNames.QuantidadeAbertura;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoBolsaDetalheMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string TipoCarteira = "TipoCarteira";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeAbertura = "QuantidadeAbertura";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string TipoCarteira = "TipoCarteira";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeAbertura = "QuantidadeAbertura";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoBolsaDetalheMetadata))
			{
				if(PosicaoBolsaDetalheMetadata.mapDelegates == null)
				{
					PosicaoBolsaDetalheMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoBolsaDetalheMetadata.meta == null)
				{
					PosicaoBolsaDetalheMetadata.meta = new PosicaoBolsaDetalheMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoCarteira", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeAbertura", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PosicaoBolsaDetalhe";
				meta.Destination = "PosicaoBolsaDetalhe";
				
				meta.spInsert = "proc_PosicaoBolsaDetalheInsert";				
				meta.spUpdate = "proc_PosicaoBolsaDetalheUpdate";		
				meta.spDelete = "proc_PosicaoBolsaDetalheDelete";
				meta.spLoadAll = "proc_PosicaoBolsaDetalheLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoBolsaDetalheLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoBolsaDetalheMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
