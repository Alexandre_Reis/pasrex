/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
























using Financial.Common;
using Financial.Investidor;		














		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esEventoFisicoBolsaCollection : esEntityCollection
	{
		public esEventoFisicoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EventoFisicoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esEventoFisicoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEventoFisicoBolsaQuery);
		}
		#endregion
		
		virtual public EventoFisicoBolsa DetachEntity(EventoFisicoBolsa entity)
		{
			return base.DetachEntity(entity) as EventoFisicoBolsa;
		}
		
		virtual public EventoFisicoBolsa AttachEntity(EventoFisicoBolsa entity)
		{
			return base.AttachEntity(entity) as EventoFisicoBolsa;
		}
		
		virtual public void Combine(EventoFisicoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EventoFisicoBolsa this[int index]
		{
			get
			{
				return base[index] as EventoFisicoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EventoFisicoBolsa);
		}
	}



	[Serializable]
	abstract public class esEventoFisicoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEventoFisicoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esEventoFisicoBolsa()
		{

		}

		public esEventoFisicoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idEvento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEvento);
			else
				return LoadByPrimaryKeyStoredProcedure(idEvento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idEvento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEventoFisicoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdEvento == idEvento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idEvento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idEvento);
			else
				return LoadByPrimaryKeyStoredProcedure(idEvento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idEvento)
		{
			esEventoFisicoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdEvento == idEvento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idEvento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdEvento",idEvento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdEvento": this.str.IdEvento = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "TipoEvento": this.str.TipoEvento = (string)value; break;							
						case "TipoMovimento": this.str.TipoMovimento = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "PULiquido": this.str.PULiquido = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "IdEventoVinculado": this.str.IdEventoVinculado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdEvento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEvento = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "TipoEvento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoEvento = (System.Byte?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "PULiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PULiquido = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "IdEventoVinculado":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdEventoVinculado = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.IdEvento
		/// </summary>
		virtual public System.Int32? IdEvento
		{
			get
			{
				return base.GetSystemInt32(EventoFisicoBolsaMetadata.ColumnNames.IdEvento);
			}
			
			set
			{
				base.SetSystemInt32(EventoFisicoBolsaMetadata.ColumnNames.IdEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(EventoFisicoBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFisicoBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(EventoFisicoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(EventoFisicoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(EventoFisicoBolsaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(EventoFisicoBolsaMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.TipoMercado
		/// </summary>
		virtual public System.String TipoMercado
		{
			get
			{
				return base.GetSystemString(EventoFisicoBolsaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemString(EventoFisicoBolsaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.TipoEvento
		/// </summary>
		virtual public System.Byte? TipoEvento
		{
			get
			{
				return base.GetSystemByte(EventoFisicoBolsaMetadata.ColumnNames.TipoEvento);
			}
			
			set
			{
				base.SetSystemByte(EventoFisicoBolsaMetadata.ColumnNames.TipoEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.TipoMovimento
		/// </summary>
		virtual public System.String TipoMovimento
		{
			get
			{
				return base.GetSystemString(EventoFisicoBolsaMetadata.ColumnNames.TipoMovimento);
			}
			
			set
			{
				base.SetSystemString(EventoFisicoBolsaMetadata.ColumnNames.TipoMovimento, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(EventoFisicoBolsaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(EventoFisicoBolsaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(EventoFisicoBolsaMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFisicoBolsaMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.PULiquido
		/// </summary>
		virtual public System.Decimal? PULiquido
		{
			get
			{
				return base.GetSystemDecimal(EventoFisicoBolsaMetadata.ColumnNames.PULiquido);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFisicoBolsaMetadata.ColumnNames.PULiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(EventoFisicoBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(EventoFisicoBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to EventoFisicoBolsa.IdEventoVinculado
		/// </summary>
		virtual public System.Byte? IdEventoVinculado
		{
			get
			{
				return base.GetSystemByte(EventoFisicoBolsaMetadata.ColumnNames.IdEventoVinculado);
			}
			
			set
			{
				base.SetSystemByte(EventoFisicoBolsaMetadata.ColumnNames.IdEventoVinculado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEventoFisicoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdEvento
			{
				get
				{
					System.Int32? data = entity.IdEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEvento = null;
					else entity.IdEvento = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.String data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToString(value);
				}
			}
				
			public System.String TipoEvento
			{
				get
				{
					System.Byte? data = entity.TipoEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEvento = null;
					else entity.TipoEvento = Convert.ToByte(value);
				}
			}
				
			public System.String TipoMovimento
			{
				get
				{
					System.String data = entity.TipoMovimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMovimento = null;
					else entity.TipoMovimento = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String PULiquido
			{
				get
				{
					System.Decimal? data = entity.PULiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PULiquido = null;
					else entity.PULiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdEventoVinculado
			{
				get
				{
					System.Byte? data = entity.IdEventoVinculado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEventoVinculado = null;
					else entity.IdEventoVinculado = Convert.ToByte(value);
				}
			}
			

			private esEventoFisicoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEventoFisicoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEventoFisicoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EventoFisicoBolsa : esEventoFisicoBolsa
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_EventoFisicoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_EventoFisicoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_EventoFisicoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEventoFisicoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EventoFisicoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdEvento
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.IdEvento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.TipoMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoEvento
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.TipoEvento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoMovimento
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.TipoMovimento, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PULiquido
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.PULiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdEventoVinculado
		{
			get
			{
				return new esQueryItem(this, EventoFisicoBolsaMetadata.ColumnNames.IdEventoVinculado, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EventoFisicoBolsaCollection")]
	public partial class EventoFisicoBolsaCollection : esEventoFisicoBolsaCollection, IEnumerable<EventoFisicoBolsa>
	{
		public EventoFisicoBolsaCollection()
		{

		}
		
		public static implicit operator List<EventoFisicoBolsa>(EventoFisicoBolsaCollection coll)
		{
			List<EventoFisicoBolsa> list = new List<EventoFisicoBolsa>();
			
			foreach (EventoFisicoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EventoFisicoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFisicoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EventoFisicoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EventoFisicoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EventoFisicoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFisicoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EventoFisicoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EventoFisicoBolsa AddNew()
		{
			EventoFisicoBolsa entity = base.AddNewEntity() as EventoFisicoBolsa;
			
			return entity;
		}

		public EventoFisicoBolsa FindByPrimaryKey(System.Int32 idEvento)
		{
			return base.FindByPrimaryKey(idEvento) as EventoFisicoBolsa;
		}


		#region IEnumerable<EventoFisicoBolsa> Members

		IEnumerator<EventoFisicoBolsa> IEnumerable<EventoFisicoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EventoFisicoBolsa;
			}
		}

		#endregion
		
		private EventoFisicoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EventoFisicoBolsa' table
	/// </summary>

	[Serializable]
	public partial class EventoFisicoBolsa : esEventoFisicoBolsa
	{
		public EventoFisicoBolsa()
		{

		}
	
		public EventoFisicoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EventoFisicoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esEventoFisicoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EventoFisicoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EventoFisicoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EventoFisicoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EventoFisicoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EventoFisicoBolsaQuery query;
	}



	[Serializable]
	public partial class EventoFisicoBolsaQuery : esEventoFisicoBolsaQuery
	{
		public EventoFisicoBolsaQuery()
		{

		}		
		
		public EventoFisicoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EventoFisicoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EventoFisicoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.IdEvento, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.IdEvento;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.CdAtivoBolsa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.IdAgente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.TipoMercado, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.TipoMercado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.TipoEvento, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.TipoEvento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.TipoMovimento, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.TipoMovimento;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.Data, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.Pu, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.PULiquido, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.PULiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.Quantidade, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EventoFisicoBolsaMetadata.ColumnNames.IdEventoVinculado, 11, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EventoFisicoBolsaMetadata.PropertyNames.IdEventoVinculado;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EventoFisicoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdEvento = "IdEvento";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgente = "IdAgente";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoEvento = "TipoEvento";
			 public const string TipoMovimento = "TipoMovimento";
			 public const string Data = "Data";
			 public const string Pu = "PU";
			 public const string PULiquido = "PULiquido";
			 public const string Quantidade = "Quantidade";
			 public const string IdEventoVinculado = "IdEventoVinculado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdEvento = "IdEvento";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgente = "IdAgente";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoEvento = "TipoEvento";
			 public const string TipoMovimento = "TipoMovimento";
			 public const string Data = "Data";
			 public const string Pu = "Pu";
			 public const string PULiquido = "PULiquido";
			 public const string Quantidade = "Quantidade";
			 public const string IdEventoVinculado = "IdEventoVinculado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EventoFisicoBolsaMetadata))
			{
				if(EventoFisicoBolsaMetadata.mapDelegates == null)
				{
					EventoFisicoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EventoFisicoBolsaMetadata.meta == null)
				{
					EventoFisicoBolsaMetadata.meta = new EventoFisicoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdEvento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoEvento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoMovimento", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PULiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdEventoVinculado", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EventoFisicoBolsa";
				meta.Destination = "EventoFisicoBolsa";
				
				meta.spInsert = "proc_EventoFisicoBolsaInsert";				
				meta.spUpdate = "proc_EventoFisicoBolsaUpdate";		
				meta.spDelete = "proc_EventoFisicoBolsaDelete";
				meta.spLoadAll = "proc_EventoFisicoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_EventoFisicoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EventoFisicoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
