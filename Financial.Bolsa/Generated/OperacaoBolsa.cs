/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/03/2016 18:55:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Investidor;



namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esOperacaoBolsaCollection : esEntityCollection
	{
		public esOperacaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoBolsaQuery);
		}
		#endregion
		
		virtual public OperacaoBolsa DetachEntity(OperacaoBolsa entity)
		{
			return base.DetachEntity(entity) as OperacaoBolsa;
		}
		
		virtual public OperacaoBolsa AttachEntity(OperacaoBolsa entity)
		{
			return base.AttachEntity(entity) as OperacaoBolsa;
		}
		
		virtual public void Combine(OperacaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoBolsa this[int index]
		{
			get
			{
				return base[index] as OperacaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoBolsa);
		}
	}



	[Serializable]
	abstract public class esOperacaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoBolsa()
		{

		}

		public esOperacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOperacaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "IdAgenteLiquidacao": this.str.IdAgenteLiquidacao = (string)value; break;							
						case "IdAgenteCorretora": this.str.IdAgenteCorretora = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "PULiquido": this.str.PULiquido = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Corretagem": this.str.Corretagem = (string)value; break;							
						case "Emolumento": this.str.Emolumento = (string)value; break;							
						case "LiquidacaoCBLC": this.str.LiquidacaoCBLC = (string)value; break;							
						case "RegistroBolsa": this.str.RegistroBolsa = (string)value; break;							
						case "RegistroCBLC": this.str.RegistroCBLC = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "ResultadoRealizado": this.str.ResultadoRealizado = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "PercentualDesconto": this.str.PercentualDesconto = (string)value; break;							
						case "Desconto": this.str.Desconto = (string)value; break;							
						case "IdLocal": this.str.IdLocal = (string)value; break;							
						case "QuantidadeCasadaTermo": this.str.QuantidadeCasadaTermo = (string)value; break;							
						case "QuantidadeCasadaExercicio": this.str.QuantidadeCasadaExercicio = (string)value; break;							
						case "ResultadoExercicio": this.str.ResultadoExercicio = (string)value; break;							
						case "ResultadoTermo": this.str.ResultadoTermo = (string)value; break;							
						case "CdAtivoBolsaOpcao": this.str.CdAtivoBolsaOpcao = (string)value; break;							
						case "NumeroNegocio": this.str.NumeroNegocio = (string)value; break;							
						case "CalculaDespesas": this.str.CalculaDespesas = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "NumeroNota": this.str.NumeroNota = (string)value; break;							
						case "ValorISS": this.str.ValorISS = (string)value; break;							
						case "IdLocalCustodia": this.str.IdLocalCustodia = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "IdClearing": this.str.IdClearing = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "IdBoletaExterna": this.str.IdBoletaExterna = (string)value; break;							
						case "UserTimeStamp": this.str.UserTimeStamp = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteLiquidacao = (System.Int32?)value;
							break;
						
						case "IdAgenteCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteCorretora = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "PULiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PULiquido = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Corretagem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Corretagem = (System.Decimal?)value;
							break;
						
						case "Emolumento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Emolumento = (System.Decimal?)value;
							break;
						
						case "LiquidacaoCBLC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.LiquidacaoCBLC = (System.Decimal?)value;
							break;
						
						case "RegistroBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RegistroBolsa = (System.Decimal?)value;
							break;
						
						case "RegistroCBLC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RegistroCBLC = (System.Decimal?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "ResultadoRealizado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizado = (System.Decimal?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Origem = (System.Byte?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "PercentualDesconto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualDesconto = (System.Decimal?)value;
							break;
						
						case "Desconto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Desconto = (System.Decimal?)value;
							break;
						
						case "IdLocal":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdLocal = (System.Int16?)value;
							break;
						
						case "QuantidadeCasadaTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeCasadaTermo = (System.Decimal?)value;
							break;
						
						case "QuantidadeCasadaExercicio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeCasadaExercicio = (System.Decimal?)value;
							break;
						
						case "ResultadoExercicio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoExercicio = (System.Decimal?)value;
							break;
						
						case "ResultadoTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoTermo = (System.Decimal?)value;
							break;
						
						case "NumeroNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroNegocio = (System.Int32?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMoeda = (System.Int32?)value;
							break;
						
						case "NumeroNota":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroNota = (System.Int32?)value;
							break;
						
						case "ValorISS":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorISS = (System.Decimal?)value;
							break;
						
						case "IdLocalCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalCustodia = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IdClearing":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClearing = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "IdBoletaExterna":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBoletaExterna = (System.Int32?)value;
							break;
						
						case "UserTimeStamp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UserTimeStamp = (System.DateTime?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdAgenteLiquidacao
		/// </summary>
		virtual public System.Int32? IdAgenteLiquidacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdAgenteLiquidacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdAgenteLiquidacao, value))
				{
					this._UpToAgenteMercadoByIdAgenteLiquidacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdAgenteCorretora
		/// </summary>
		virtual public System.Int32? IdAgenteCorretora
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdAgenteCorretora);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdAgenteCorretora, value))
				{
					this._UpToAgenteMercadoByIdAgenteCorretora = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.TipoMercado
		/// </summary>
		virtual public System.String TipoMercado
		{
			get
			{
				return base.GetSystemString(OperacaoBolsaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemString(OperacaoBolsaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.TipoOperacao
		/// </summary>
		virtual public System.String TipoOperacao
		{
			get
			{
				return base.GetSystemString(OperacaoBolsaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemString(OperacaoBolsaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(OperacaoBolsaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoBolsaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.PULiquido
		/// </summary>
		virtual public System.Decimal? PULiquido
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.PULiquido);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.PULiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.Corretagem
		/// </summary>
		virtual public System.Decimal? Corretagem
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Corretagem);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Corretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.Emolumento
		/// </summary>
		virtual public System.Decimal? Emolumento
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Emolumento);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Emolumento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.LiquidacaoCBLC
		/// </summary>
		virtual public System.Decimal? LiquidacaoCBLC
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.LiquidacaoCBLC);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.LiquidacaoCBLC, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.RegistroBolsa
		/// </summary>
		virtual public System.Decimal? RegistroBolsa
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.RegistroBolsa);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.RegistroBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.RegistroCBLC
		/// </summary>
		virtual public System.Decimal? RegistroCBLC
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.RegistroCBLC);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.RegistroCBLC, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoBolsaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoBolsaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.ResultadoRealizado
		/// </summary>
		virtual public System.Decimal? ResultadoRealizado
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ResultadoRealizado);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ResultadoRealizado, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.Origem
		/// </summary>
		virtual public System.Byte? Origem
		{
			get
			{
				return base.GetSystemByte(OperacaoBolsaMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemByte(OperacaoBolsaMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OperacaoBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.PercentualDesconto
		/// </summary>
		virtual public System.Decimal? PercentualDesconto
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.PercentualDesconto);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.PercentualDesconto, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.Desconto
		/// </summary>
		virtual public System.Decimal? Desconto
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Desconto);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.Desconto, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdLocal
		/// </summary>
		virtual public System.Int16? IdLocal
		{
			get
			{
				return base.GetSystemInt16(OperacaoBolsaMetadata.ColumnNames.IdLocal);
			}
			
			set
			{
				if(base.SetSystemInt16(OperacaoBolsaMetadata.ColumnNames.IdLocal, value))
				{
					this._UpToLocalFeriadoByIdLocal = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.QuantidadeCasadaTermo
		/// </summary>
		virtual public System.Decimal? QuantidadeCasadaTermo
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaTermo);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.QuantidadeCasadaExercicio
		/// </summary>
		virtual public System.Decimal? QuantidadeCasadaExercicio
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaExercicio);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaExercicio, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.ResultadoExercicio
		/// </summary>
		virtual public System.Decimal? ResultadoExercicio
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ResultadoExercicio);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ResultadoExercicio, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.ResultadoTermo
		/// </summary>
		virtual public System.Decimal? ResultadoTermo
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ResultadoTermo);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ResultadoTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.CdAtivoBolsaOpcao
		/// </summary>
		virtual public System.String CdAtivoBolsaOpcao
		{
			get
			{
				return base.GetSystemString(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao);
			}
			
			set
			{
				if(base.SetSystemString(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.NumeroNegocio
		/// </summary>
		virtual public System.Int32? NumeroNegocio
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.NumeroNegocio);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.NumeroNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.CalculaDespesas
		/// </summary>
		virtual public System.String CalculaDespesas
		{
			get
			{
				return base.GetSystemString(OperacaoBolsaMetadata.ColumnNames.CalculaDespesas);
			}
			
			set
			{
				base.SetSystemString(OperacaoBolsaMetadata.ColumnNames.CalculaDespesas, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdTrader, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdMoeda
		/// </summary>
		virtual public System.Int32? IdMoeda
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(OperacaoBolsaMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(OperacaoBolsaMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.NumeroNota
		/// </summary>
		virtual public System.Int32? NumeroNota
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.NumeroNota);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.NumeroNota, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.ValorISS
		/// </summary>
		virtual public System.Decimal? ValorISS
		{
			get
			{
				return base.GetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ValorISS);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoBolsaMetadata.ColumnNames.ValorISS, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdLocalCustodia
		/// </summary>
		virtual public System.Int32? IdLocalCustodia
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdLocalCustodia);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdLocalCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdClearing
		/// </summary>
		virtual public System.Int32? IdClearing
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdClearing);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdClearing, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoBolsaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoBolsaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdBoletaExterna
		/// </summary>
		virtual public System.Int32? IdBoletaExterna
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdBoletaExterna);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdBoletaExterna, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.UserTimeStamp
		/// </summary>
		virtual public System.DateTime? UserTimeStamp
		{
			get
			{
				return base.GetSystemDateTime(OperacaoBolsaMetadata.ColumnNames.UserTimeStamp);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoBolsaMetadata.ColumnNames.UserTimeStamp, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdConta, value))
				{
					this._UpToContaCorrenteByIdConta = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoBolsa.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoBolsaMetadata.ColumnNames.IdCategoriaMovimentacao, value))
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteLiquidacao;
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteCorretora;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsaOpcao;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected CategoriaMovimentacao _UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
        internal protected Financial.Investidor.ContaCorrente _UpToContaCorrenteByIdConta;
		[CLSCompliant(false)]
		internal protected LocalFeriado _UpToLocalFeriadoByIdLocal;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String IdAgenteLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdAgenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteLiquidacao = null;
					else entity.IdAgenteLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteCorretora
			{
				get
				{
					System.Int32? data = entity.IdAgenteCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteCorretora = null;
					else entity.IdAgenteCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.String data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToString(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.String data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String PULiquido
			{
				get
				{
					System.Decimal? data = entity.PULiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PULiquido = null;
					else entity.PULiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Corretagem
			{
				get
				{
					System.Decimal? data = entity.Corretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Corretagem = null;
					else entity.Corretagem = Convert.ToDecimal(value);
				}
			}
				
			public System.String Emolumento
			{
				get
				{
					System.Decimal? data = entity.Emolumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Emolumento = null;
					else entity.Emolumento = Convert.ToDecimal(value);
				}
			}
				
			public System.String LiquidacaoCBLC
			{
				get
				{
					System.Decimal? data = entity.LiquidacaoCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LiquidacaoCBLC = null;
					else entity.LiquidacaoCBLC = Convert.ToDecimal(value);
				}
			}
				
			public System.String RegistroBolsa
			{
				get
				{
					System.Decimal? data = entity.RegistroBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroBolsa = null;
					else entity.RegistroBolsa = Convert.ToDecimal(value);
				}
			}
				
			public System.String RegistroCBLC
			{
				get
				{
					System.Decimal? data = entity.RegistroCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroCBLC = null;
					else entity.RegistroCBLC = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String ResultadoRealizado
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizado = null;
					else entity.ResultadoRealizado = Convert.ToDecimal(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Byte? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToByte(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String PercentualDesconto
			{
				get
				{
					System.Decimal? data = entity.PercentualDesconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualDesconto = null;
					else entity.PercentualDesconto = Convert.ToDecimal(value);
				}
			}
				
			public System.String Desconto
			{
				get
				{
					System.Decimal? data = entity.Desconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Desconto = null;
					else entity.Desconto = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdLocal
			{
				get
				{
					System.Int16? data = entity.IdLocal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocal = null;
					else entity.IdLocal = Convert.ToInt16(value);
				}
			}
				
			public System.String QuantidadeCasadaTermo
			{
				get
				{
					System.Decimal? data = entity.QuantidadeCasadaTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeCasadaTermo = null;
					else entity.QuantidadeCasadaTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeCasadaExercicio
			{
				get
				{
					System.Decimal? data = entity.QuantidadeCasadaExercicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeCasadaExercicio = null;
					else entity.QuantidadeCasadaExercicio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoExercicio
			{
				get
				{
					System.Decimal? data = entity.ResultadoExercicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoExercicio = null;
					else entity.ResultadoExercicio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoTermo
			{
				get
				{
					System.Decimal? data = entity.ResultadoTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoTermo = null;
					else entity.ResultadoTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAtivoBolsaOpcao
			{
				get
				{
					System.String data = entity.CdAtivoBolsaOpcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaOpcao = null;
					else entity.CdAtivoBolsaOpcao = Convert.ToString(value);
				}
			}
				
			public System.String NumeroNegocio
			{
				get
				{
					System.Int32? data = entity.NumeroNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroNegocio = null;
					else entity.NumeroNegocio = Convert.ToInt32(value);
				}
			}
				
			public System.String CalculaDespesas
			{
				get
				{
					System.String data = entity.CalculaDespesas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaDespesas = null;
					else entity.CalculaDespesas = Convert.ToString(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int32? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt32(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String NumeroNota
			{
				get
				{
					System.Int32? data = entity.NumeroNota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroNota = null;
					else entity.NumeroNota = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorISS
			{
				get
				{
					System.Decimal? data = entity.ValorISS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorISS = null;
					else entity.ValorISS = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdLocalCustodia
			{
				get
				{
					System.Int32? data = entity.IdLocalCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalCustodia = null;
					else entity.IdLocalCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdClearing
			{
				get
				{
					System.Int32? data = entity.IdClearing;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClearing = null;
					else entity.IdClearing = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdBoletaExterna
			{
				get
				{
					System.Int32? data = entity.IdBoletaExterna;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBoletaExterna = null;
					else entity.IdBoletaExterna = Convert.ToInt32(value);
				}
			}
				
			public System.String UserTimeStamp
			{
				get
				{
					System.DateTime? data = entity.UserTimeStamp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UserTimeStamp = null;
					else entity.UserTimeStamp = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
			

			private esOperacaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoBolsa : esOperacaoBolsa
	{

				
		#region OperacaoTermoBolsa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - OperacaoBolsa_OperacaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoTermoBolsa OperacaoTermoBolsa
		{
			get
			{
				if(this._OperacaoTermoBolsa == null)
				{
					this._OperacaoTermoBolsa = new OperacaoTermoBolsa();
					this._OperacaoTermoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("OperacaoTermoBolsa", this._OperacaoTermoBolsa);
				
					if(this.IdOperacao != null)
					{
						this._OperacaoTermoBolsa.Query.Where(this._OperacaoTermoBolsa.Query.IdOperacao == this.IdOperacao);
						this._OperacaoTermoBolsa.Query.Load();
					}
				}

				return this._OperacaoTermoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoTermoBolsa != null) 
				{ 
					this.RemovePostOneSave("OperacaoTermoBolsa"); 
					this._OperacaoTermoBolsa = null;
					
				} 
			}          			
		}

		private OperacaoTermoBolsa _OperacaoTermoBolsa;
		#endregion

				
		#region UpToAgenteMercadoByIdAgenteLiquidacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_OperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteLiquidacao
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteLiquidacao == null
					&& IdAgenteLiquidacao != null					)
				{
					this._UpToAgenteMercadoByIdAgenteLiquidacao = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteLiquidacao", this._UpToAgenteMercadoByIdAgenteLiquidacao);
					this._UpToAgenteMercadoByIdAgenteLiquidacao.Query.Where(this._UpToAgenteMercadoByIdAgenteLiquidacao.Query.IdAgente == this.IdAgenteLiquidacao);
					this._UpToAgenteMercadoByIdAgenteLiquidacao.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteLiquidacao;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteLiquidacao");
				

				if(value == null)
				{
					this.IdAgenteLiquidacao = null;
					this._UpToAgenteMercadoByIdAgenteLiquidacao = null;
				}
				else
				{
					this.IdAgenteLiquidacao = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteLiquidacao = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteLiquidacao", this._UpToAgenteMercadoByIdAgenteLiquidacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAgenteMercadoByIdAgenteCorretora - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_OperacaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteCorretora
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteCorretora == null
					&& IdAgenteCorretora != null					)
				{
					this._UpToAgenteMercadoByIdAgenteCorretora = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteCorretora.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteCorretora", this._UpToAgenteMercadoByIdAgenteCorretora);
					this._UpToAgenteMercadoByIdAgenteCorretora.Query.Where(this._UpToAgenteMercadoByIdAgenteCorretora.Query.IdAgente == this.IdAgenteCorretora);
					this._UpToAgenteMercadoByIdAgenteCorretora.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteCorretora;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteCorretora");
				

				if(value == null)
				{
					this.IdAgenteCorretora = null;
					this._UpToAgenteMercadoByIdAgenteCorretora = null;
				}
				else
				{
					this.IdAgenteCorretora = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteCorretora = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteCorretora", this._UpToAgenteMercadoByIdAgenteCorretora);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsaOpcao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_OperacaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsaOpcao
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsaOpcao == null
					&& CdAtivoBolsaOpcao != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaOpcao", this._UpToAtivoBolsaByCdAtivoBolsaOpcao);
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsaOpcao.Query.CdAtivoBolsa == this.CdAtivoBolsaOpcao);
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsaOpcao;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsaOpcao");
				

				if(value == null)
				{
					this.CdAtivoBolsaOpcao = null;
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao = null;
				}
				else
				{
					this.CdAtivoBolsaOpcao = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaOpcao", this._UpToAtivoBolsaByCdAtivoBolsaOpcao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_OperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCategoriaMovimentacaoByIdCategoriaMovimentacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoBolsa_CatMovimentacao_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaMovimentacao UpToCategoriaMovimentacaoByIdCategoriaMovimentacao
		{
			get
			{
				if(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao == null
					&& IdCategoriaMovimentacao != null					)
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = new CategoriaMovimentacao();
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Where(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.IdCategoriaMovimentacao == this.IdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Load();
				}

				return this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao");
				

				if(value == null)
				{
					this.IdCategoriaMovimentacao = null;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
				else
				{
					this.IdCategoriaMovimentacao = value.IdCategoriaMovimentacao;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = value;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_OperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContaCorrenteByIdConta - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_OperacaoBolsa_ContaCorrente
		/// </summary>

		[XmlIgnore]
        public Financial.Investidor.ContaCorrente UpToContaCorrenteByIdConta
		{
			get
			{
				if(this._UpToContaCorrenteByIdConta == null
					&& IdConta != null					)
				{
                    this._UpToContaCorrenteByIdConta = new Financial.Investidor.ContaCorrente();
					this._UpToContaCorrenteByIdConta.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
					this._UpToContaCorrenteByIdConta.Query.Where(this._UpToContaCorrenteByIdConta.Query.IdConta == this.IdConta);
					this._UpToContaCorrenteByIdConta.Query.Load();
				}

				return this._UpToContaCorrenteByIdConta;
			}
			
			set
			{
				this.RemovePreSave("UpToContaCorrenteByIdConta");
				

				if(value == null)
				{
					this.IdConta = null;
					this._UpToContaCorrenteByIdConta = null;
				}
				else
				{
					this.IdConta = value.IdConta;
					this._UpToContaCorrenteByIdConta = value;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
				}
				
			}
		}
		#endregion
		

				
		#region UpToLocalFeriadoByIdLocal - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - LocalFeriado_OperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public LocalFeriado UpToLocalFeriadoByIdLocal
		{
			get
			{
				if(this._UpToLocalFeriadoByIdLocal == null
					&& IdLocal != null					)
				{
					this._UpToLocalFeriadoByIdLocal = new LocalFeriado();
					this._UpToLocalFeriadoByIdLocal.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToLocalFeriadoByIdLocal", this._UpToLocalFeriadoByIdLocal);
					this._UpToLocalFeriadoByIdLocal.Query.Where(this._UpToLocalFeriadoByIdLocal.Query.IdLocal == this.IdLocal);
					this._UpToLocalFeriadoByIdLocal.Query.Load();
				}

				return this._UpToLocalFeriadoByIdLocal;
			}
			
			set
			{
				this.RemovePreSave("UpToLocalFeriadoByIdLocal");
				

				if(value == null)
				{
					this.IdLocal = null;
					this._UpToLocalFeriadoByIdLocal = null;
				}
				else
				{
					this.IdLocal = value.IdLocal;
					this._UpToLocalFeriadoByIdLocal = value;
					this.SetPreSave("UpToLocalFeriadoByIdLocal", this._UpToLocalFeriadoByIdLocal);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteLiquidacao != null)
			{
				this.IdAgenteLiquidacao = this._UpToAgenteMercadoByIdAgenteLiquidacao.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteCorretora != null)
			{
				this.IdAgenteCorretora = this._UpToAgenteMercadoByIdAgenteCorretora.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao != null)
			{
				this.IdCategoriaMovimentacao = this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.IdCategoriaMovimentacao;
			}
			if(!this.es.IsDeleted && this._UpToContaCorrenteByIdConta != null)
			{
				this.IdConta = this._UpToContaCorrenteByIdConta.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToLocalFeriadoByIdLocal != null)
			{
				this.IdLocal = this._UpToLocalFeriadoByIdLocal.IdLocal;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
			if(this._OperacaoTermoBolsa != null)
			{
				if(this._OperacaoTermoBolsa.es.IsAdded)
				{
					this._OperacaoTermoBolsa.IdOperacao = this.IdOperacao;
				}
			}
		}
		
	}



	[Serializable]
	abstract public class esOperacaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdAgenteLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteCorretora
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdAgenteCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.TipoMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.TipoOperacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PULiquido
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.PULiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Corretagem
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Corretagem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Emolumento
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Emolumento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem LiquidacaoCBLC
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.LiquidacaoCBLC, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RegistroBolsa
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.RegistroBolsa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RegistroCBLC
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.RegistroCBLC, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ResultadoRealizado
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.ResultadoRealizado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Origem, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PercentualDesconto
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.PercentualDesconto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Desconto
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Desconto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdLocal
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdLocal, esSystemType.Int16);
			}
		} 
		
		public esQueryItem QuantidadeCasadaTermo
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeCasadaExercicio
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaExercicio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoExercicio
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.ResultadoExercicio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoTermo
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.ResultadoTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAtivoBolsaOpcao
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, esSystemType.String);
			}
		} 
		
		public esQueryItem NumeroNegocio
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.NumeroNegocio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CalculaDespesas
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.CalculaDespesas, esSystemType.String);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdMoeda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem NumeroNota
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.NumeroNota, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorISS
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.ValorISS, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdLocalCustodia
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdLocalCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdClearing
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdClearing, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdBoletaExterna
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdBoletaExterna, esSystemType.Int32);
			}
		} 
		
		public esQueryItem UserTimeStamp
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.UserTimeStamp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, OperacaoBolsaMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoBolsaCollection")]
	public partial class OperacaoBolsaCollection : esOperacaoBolsaCollection, IEnumerable<OperacaoBolsa>
	{
		public OperacaoBolsaCollection()
		{

		}
		
		public static implicit operator List<OperacaoBolsa>(OperacaoBolsaCollection coll)
		{
			List<OperacaoBolsa> list = new List<OperacaoBolsa>();
			
			foreach (OperacaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoBolsa AddNew()
		{
			OperacaoBolsa entity = base.AddNewEntity() as OperacaoBolsa;
			
			return entity;
		}

		public OperacaoBolsa FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OperacaoBolsa;
		}


		#region IEnumerable<OperacaoBolsa> Members

		IEnumerator<OperacaoBolsa> IEnumerable<OperacaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoBolsa;
			}
		}

		#endregion
		
		private OperacaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class OperacaoBolsa : esOperacaoBolsa
	{
		public OperacaoBolsa()
		{

		}
	
		public OperacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoBolsaQuery query;
	}



	[Serializable]
	public partial class OperacaoBolsaQuery : esOperacaoBolsaQuery
	{
		public OperacaoBolsaQuery()
		{

		}		
		
		public OperacaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdAgenteLiquidacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdAgenteLiquidacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdAgenteCorretora, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdAgenteCorretora;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.TipoMercado, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.TipoMercado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.TipoOperacao, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.TipoOperacao;
			c.CharacterMaxLength = 2;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Data, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Pu, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.PULiquido, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.PULiquido;	
			c.NumericPrecision = 18;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Valor, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.ValorLiquido, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Quantidade, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Corretagem, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Corretagem;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Emolumento, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Emolumento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.LiquidacaoCBLC, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.LiquidacaoCBLC;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.RegistroBolsa, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.RegistroBolsa;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.RegistroCBLC, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.RegistroCBLC;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.DataLiquidacao, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.ResultadoRealizado, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.ResultadoRealizado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Origem, 20, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Fonte, 21, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.PercentualDesconto, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.PercentualDesconto;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Desconto, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Desconto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdLocal, 24, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdLocal;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaTermo, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.QuantidadeCasadaTermo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.QuantidadeCasadaExercicio, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.QuantidadeCasadaExercicio;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.ResultadoExercicio, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.ResultadoExercicio;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.ResultadoTermo, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.ResultadoTermo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.CdAtivoBolsaOpcao;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.NumeroNegocio, 30, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.NumeroNegocio;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.CalculaDespesas, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.CalculaDespesas;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdTrader, 32, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdMoeda, 33, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.Observacao, 34, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.NumeroNota, 35, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.NumeroNota;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.ValorISS, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.ValorISS;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdLocalCustodia, 37, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdLocalCustodia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdLocalNegociacao, 38, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdClearing, 39, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdClearing;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.DataOperacao, 40, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdBoletaExterna, 41, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdBoletaExterna;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.UserTimeStamp, 42, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.UserTimeStamp;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdConta, 43, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoBolsaMetadata.ColumnNames.IdCategoriaMovimentacao, 44, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoBolsaMetadata.PropertyNames.IdCategoriaMovimentacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgenteLiquidacao = "IdAgenteLiquidacao";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Data = "Data";
			 public const string Pu = "PU";
			 public const string PULiquido = "PULiquido";
			 public const string Valor = "Valor";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string LiquidacaoCBLC = "LiquidacaoCBLC";
			 public const string RegistroBolsa = "RegistroBolsa";
			 public const string RegistroCBLC = "RegistroCBLC";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string ResultadoRealizado = "ResultadoRealizado";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string Desconto = "Desconto";
			 public const string IdLocal = "IdLocal";
			 public const string QuantidadeCasadaTermo = "QuantidadeCasadaTermo";
			 public const string QuantidadeCasadaExercicio = "QuantidadeCasadaExercicio";
			 public const string ResultadoExercicio = "ResultadoExercicio";
			 public const string ResultadoTermo = "ResultadoTermo";
			 public const string CdAtivoBolsaOpcao = "CdAtivoBolsaOpcao";
			 public const string NumeroNegocio = "NumeroNegocio";
			 public const string CalculaDespesas = "CalculaDespesas";
			 public const string IdTrader = "IdTrader";
			 public const string IdMoeda = "IdMoeda";
			 public const string Observacao = "Observacao";
			 public const string NumeroNota = "NumeroNota";
			 public const string ValorISS = "ValorISS";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdClearing = "IdClearing";
			 public const string DataOperacao = "DataOperacao";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdConta = "IdConta";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgenteLiquidacao = "IdAgenteLiquidacao";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Data = "Data";
			 public const string Pu = "Pu";
			 public const string PULiquido = "PULiquido";
			 public const string Valor = "Valor";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string LiquidacaoCBLC = "LiquidacaoCBLC";
			 public const string RegistroBolsa = "RegistroBolsa";
			 public const string RegistroCBLC = "RegistroCBLC";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string ResultadoRealizado = "ResultadoRealizado";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string Desconto = "Desconto";
			 public const string IdLocal = "IdLocal";
			 public const string QuantidadeCasadaTermo = "QuantidadeCasadaTermo";
			 public const string QuantidadeCasadaExercicio = "QuantidadeCasadaExercicio";
			 public const string ResultadoExercicio = "ResultadoExercicio";
			 public const string ResultadoTermo = "ResultadoTermo";
			 public const string CdAtivoBolsaOpcao = "CdAtivoBolsaOpcao";
			 public const string NumeroNegocio = "NumeroNegocio";
			 public const string CalculaDespesas = "CalculaDespesas";
			 public const string IdTrader = "IdTrader";
			 public const string IdMoeda = "IdMoeda";
			 public const string Observacao = "Observacao";
			 public const string NumeroNota = "NumeroNota";
			 public const string ValorISS = "ValorISS";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdClearing = "IdClearing";
			 public const string DataOperacao = "DataOperacao";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdConta = "IdConta";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoBolsaMetadata))
			{
				if(OperacaoBolsaMetadata.mapDelegates == null)
				{
					OperacaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoBolsaMetadata.meta == null)
				{
					OperacaoBolsaMetadata.meta = new OperacaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgenteLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PULiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Corretagem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Emolumento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("LiquidacaoCBLC", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RegistroBolsa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RegistroCBLC", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ResultadoRealizado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Origem", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PercentualDesconto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Desconto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdLocal", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("QuantidadeCasadaTermo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeCasadaExercicio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoExercicio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoTermo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CdAtivoBolsaOpcao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("NumeroNegocio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CalculaDespesas", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("NumeroNota", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorISS", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdLocalCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdClearing", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdBoletaExterna", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("UserTimeStamp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "OperacaoBolsa";
				meta.Destination = "OperacaoBolsa";
				
				meta.spInsert = "proc_OperacaoBolsaInsert";				
				meta.spUpdate = "proc_OperacaoBolsaUpdate";		
				meta.spDelete = "proc_OperacaoBolsaDelete";
				meta.spLoadAll = "proc_OperacaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
