/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;























using Financial.Common;
using Financial.Investidor;















		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esClienteBolsaCollection : esEntityCollection
	{
		public esClienteBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClienteBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esClienteBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClienteBolsaQuery);
		}
		#endregion
		
		virtual public ClienteBolsa DetachEntity(ClienteBolsa entity)
		{
			return base.DetachEntity(entity) as ClienteBolsa;
		}
		
		virtual public ClienteBolsa AttachEntity(ClienteBolsa entity)
		{
			return base.AttachEntity(entity) as ClienteBolsa;
		}
		
		virtual public void Combine(ClienteBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClienteBolsa this[int index]
		{
			get
			{
				return base[index] as ClienteBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClienteBolsa);
		}
	}



	[Serializable]
	abstract public class esClienteBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClienteBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esClienteBolsa()
		{

		}

		public esClienteBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esClienteBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente)
		{
			esClienteBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "TipoCotacao": this.str.TipoCotacao = (string)value; break;							
						case "CodigoSinacor": this.str.CodigoSinacor = (string)value; break;							
						case "CodigoSinacor2": this.str.CodigoSinacor2 = (string)value; break;							
						case "IdAssessor": this.str.IdAssessor = (string)value; break;							
						case "TipoCusto": this.str.TipoCusto = (string)value; break;							
						case "IsentoIR": this.str.IsentoIR = (string)value; break;							
						case "IdCustodianteAcoes": this.str.IdCustodianteAcoes = (string)value; break;							
						case "IdCustodianteOpcoes": this.str.IdCustodianteOpcoes = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "TipoCotacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCotacao = (System.Byte?)value;
							break;
						
						case "IdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAssessor = (System.Int32?)value;
							break;
						
						case "TipoCusto":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCusto = (System.Byte?)value;
							break;
						
						case "IdCustodianteAcoes":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCustodianteAcoes = (System.Int32?)value;
							break;
						
						case "IdCustodianteOpcoes":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCustodianteOpcoes = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClienteBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ClienteBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ClienteBolsaMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBolsa.TipoCotacao
		/// </summary>
		virtual public System.Byte? TipoCotacao
		{
			get
			{
				return base.GetSystemByte(ClienteBolsaMetadata.ColumnNames.TipoCotacao);
			}
			
			set
			{
				base.SetSystemByte(ClienteBolsaMetadata.ColumnNames.TipoCotacao, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBolsa.CodigoSinacor
		/// </summary>
		virtual public System.String CodigoSinacor
		{
			get
			{
				return base.GetSystemString(ClienteBolsaMetadata.ColumnNames.CodigoSinacor);
			}
			
			set
			{
				base.SetSystemString(ClienteBolsaMetadata.ColumnNames.CodigoSinacor, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBolsa.CodigoSinacor2
		/// </summary>
		virtual public System.String CodigoSinacor2
		{
			get
			{
				return base.GetSystemString(ClienteBolsaMetadata.ColumnNames.CodigoSinacor2);
			}
			
			set
			{
				base.SetSystemString(ClienteBolsaMetadata.ColumnNames.CodigoSinacor2, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBolsa.IdAssessor
		/// </summary>
		virtual public System.Int32? IdAssessor
		{
			get
			{
				return base.GetSystemInt32(ClienteBolsaMetadata.ColumnNames.IdAssessor);
			}
			
			set
			{
				if(base.SetSystemInt32(ClienteBolsaMetadata.ColumnNames.IdAssessor, value))
				{
					this._UpToAssessorByIdAssessor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClienteBolsa.TipoCusto
		/// </summary>
		virtual public System.Byte? TipoCusto
		{
			get
			{
				return base.GetSystemByte(ClienteBolsaMetadata.ColumnNames.TipoCusto);
			}
			
			set
			{
				base.SetSystemByte(ClienteBolsaMetadata.ColumnNames.TipoCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBolsa.IsentoIR
		/// </summary>
		virtual public System.String IsentoIR
		{
			get
			{
				return base.GetSystemString(ClienteBolsaMetadata.ColumnNames.IsentoIR);
			}
			
			set
			{
				base.SetSystemString(ClienteBolsaMetadata.ColumnNames.IsentoIR, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBolsa.IdCustodianteAcoes
		/// </summary>
		virtual public System.Int32? IdCustodianteAcoes
		{
			get
			{
				return base.GetSystemInt32(ClienteBolsaMetadata.ColumnNames.IdCustodianteAcoes);
			}
			
			set
			{
				base.SetSystemInt32(ClienteBolsaMetadata.ColumnNames.IdCustodianteAcoes, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteBolsa.IdCustodianteOpcoes
		/// </summary>
		virtual public System.Int32? IdCustodianteOpcoes
		{
			get
			{
				return base.GetSystemInt32(ClienteBolsaMetadata.ColumnNames.IdCustodianteOpcoes);
			}
			
			set
			{
				base.SetSystemInt32(ClienteBolsaMetadata.ColumnNames.IdCustodianteOpcoes, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Assessor _UpToAssessorByIdAssessor;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClienteBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCotacao
			{
				get
				{
					System.Byte? data = entity.TipoCotacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCotacao = null;
					else entity.TipoCotacao = Convert.ToByte(value);
				}
			}
				
			public System.String CodigoSinacor
			{
				get
				{
					System.String data = entity.CodigoSinacor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoSinacor = null;
					else entity.CodigoSinacor = Convert.ToString(value);
				}
			}
				
			public System.String CodigoSinacor2
			{
				get
				{
					System.String data = entity.CodigoSinacor2;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoSinacor2 = null;
					else entity.CodigoSinacor2 = Convert.ToString(value);
				}
			}
				
			public System.String IdAssessor
			{
				get
				{
					System.Int32? data = entity.IdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAssessor = null;
					else entity.IdAssessor = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCusto
			{
				get
				{
					System.Byte? data = entity.TipoCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCusto = null;
					else entity.TipoCusto = Convert.ToByte(value);
				}
			}
				
			public System.String IsentoIR
			{
				get
				{
					System.String data = entity.IsentoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIR = null;
					else entity.IsentoIR = Convert.ToString(value);
				}
			}
				
			public System.String IdCustodianteAcoes
			{
				get
				{
					System.Int32? data = entity.IdCustodianteAcoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCustodianteAcoes = null;
					else entity.IdCustodianteAcoes = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCustodianteOpcoes
			{
				get
				{
					System.Int32? data = entity.IdCustodianteOpcoes;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCustodianteOpcoes = null;
					else entity.IdCustodianteOpcoes = Convert.ToInt32(value);
				}
			}
			

			private esClienteBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClienteBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClienteBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClienteBolsa : esClienteBolsa
	{

				
		#region UpToAssessorByIdAssessor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Assessor_ClienteBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Assessor UpToAssessorByIdAssessor
		{
			get
			{
				if(this._UpToAssessorByIdAssessor == null
					&& IdAssessor != null					)
				{
					this._UpToAssessorByIdAssessor = new Assessor();
					this._UpToAssessorByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
					this._UpToAssessorByIdAssessor.Query.Where(this._UpToAssessorByIdAssessor.Query.IdAssessor == this.IdAssessor);
					this._UpToAssessorByIdAssessor.Query.Load();
				}

				return this._UpToAssessorByIdAssessor;
			}
			
			set
			{
				this.RemovePreSave("UpToAssessorByIdAssessor");
				

				if(value == null)
				{
					this.IdAssessor = null;
					this._UpToAssessorByIdAssessor = null;
				}
				else
				{
					this.IdAssessor = value.IdAssessor;
					this._UpToAssessorByIdAssessor = value;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
				}
				
			}
		}
		#endregion
		

		#region UpToCliente - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_ClienteBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToCliente
		{
			get
			{
				if(this._UpToCliente == null
					&& IdCliente != null					)
				{
					this._UpToCliente = new Cliente();
					this._UpToCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCliente", this._UpToCliente);
					this._UpToCliente.Query.Where(this._UpToCliente.Query.IdCliente == this.IdCliente);
					this._UpToCliente.Query.Load();
				}

				return this._UpToCliente;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToCliente");

				if(value == null)
				{
					this._UpToCliente = null;
				}
				else
				{
					this._UpToCliente = value;
					this.SetPreSave("UpToCliente", this._UpToCliente);
				}
				
				
			} 
		}

		private Cliente _UpToCliente;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAssessorByIdAssessor != null)
			{
				this.IdAssessor = this._UpToAssessorByIdAssessor.IdAssessor;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClienteBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClienteBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ClienteBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCotacao
		{
			get
			{
				return new esQueryItem(this, ClienteBolsaMetadata.ColumnNames.TipoCotacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CodigoSinacor
		{
			get
			{
				return new esQueryItem(this, ClienteBolsaMetadata.ColumnNames.CodigoSinacor, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoSinacor2
		{
			get
			{
				return new esQueryItem(this, ClienteBolsaMetadata.ColumnNames.CodigoSinacor2, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAssessor
		{
			get
			{
				return new esQueryItem(this, ClienteBolsaMetadata.ColumnNames.IdAssessor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCusto
		{
			get
			{
				return new esQueryItem(this, ClienteBolsaMetadata.ColumnNames.TipoCusto, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IsentoIR
		{
			get
			{
				return new esQueryItem(this, ClienteBolsaMetadata.ColumnNames.IsentoIR, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCustodianteAcoes
		{
			get
			{
				return new esQueryItem(this, ClienteBolsaMetadata.ColumnNames.IdCustodianteAcoes, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCustodianteOpcoes
		{
			get
			{
				return new esQueryItem(this, ClienteBolsaMetadata.ColumnNames.IdCustodianteOpcoes, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClienteBolsaCollection")]
	public partial class ClienteBolsaCollection : esClienteBolsaCollection, IEnumerable<ClienteBolsa>
	{
		public ClienteBolsaCollection()
		{

		}
		
		public static implicit operator List<ClienteBolsa>(ClienteBolsaCollection coll)
		{
			List<ClienteBolsa> list = new List<ClienteBolsa>();
			
			foreach (ClienteBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClienteBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClienteBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClienteBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClienteBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClienteBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClienteBolsa AddNew()
		{
			ClienteBolsa entity = base.AddNewEntity() as ClienteBolsa;
			
			return entity;
		}

		public ClienteBolsa FindByPrimaryKey(System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(idCliente) as ClienteBolsa;
		}


		#region IEnumerable<ClienteBolsa> Members

		IEnumerator<ClienteBolsa> IEnumerable<ClienteBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClienteBolsa;
			}
		}

		#endregion
		
		private ClienteBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClienteBolsa' table
	/// </summary>

	[Serializable]
	public partial class ClienteBolsa : esClienteBolsa
	{
		public ClienteBolsa()
		{

		}
	
		public ClienteBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClienteBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esClienteBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClienteBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClienteBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClienteBolsaQuery query;
	}



	[Serializable]
	public partial class ClienteBolsaQuery : esClienteBolsaQuery
	{
		public ClienteBolsaQuery()
		{

		}		
		
		public ClienteBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClienteBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClienteBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClienteBolsaMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteBolsaMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBolsaMetadata.ColumnNames.TipoCotacao, 1, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteBolsaMetadata.PropertyNames.TipoCotacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBolsaMetadata.ColumnNames.CodigoSinacor, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteBolsaMetadata.PropertyNames.CodigoSinacor;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBolsaMetadata.ColumnNames.CodigoSinacor2, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteBolsaMetadata.PropertyNames.CodigoSinacor2;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBolsaMetadata.ColumnNames.IdAssessor, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteBolsaMetadata.PropertyNames.IdAssessor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBolsaMetadata.ColumnNames.TipoCusto, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ClienteBolsaMetadata.PropertyNames.TipoCusto;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBolsaMetadata.ColumnNames.IsentoIR, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteBolsaMetadata.PropertyNames.IsentoIR;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBolsaMetadata.ColumnNames.IdCustodianteAcoes, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteBolsaMetadata.PropertyNames.IdCustodianteAcoes;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteBolsaMetadata.ColumnNames.IdCustodianteOpcoes, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteBolsaMetadata.PropertyNames.IdCustodianteOpcoes;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClienteBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string TipoCotacao = "TipoCotacao";
			 public const string CodigoSinacor = "CodigoSinacor";
			 public const string CodigoSinacor2 = "CodigoSinacor2";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoCusto = "TipoCusto";
			 public const string IsentoIR = "IsentoIR";
			 public const string IdCustodianteAcoes = "IdCustodianteAcoes";
			 public const string IdCustodianteOpcoes = "IdCustodianteOpcoes";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string TipoCotacao = "TipoCotacao";
			 public const string CodigoSinacor = "CodigoSinacor";
			 public const string CodigoSinacor2 = "CodigoSinacor2";
			 public const string IdAssessor = "IdAssessor";
			 public const string TipoCusto = "TipoCusto";
			 public const string IsentoIR = "IsentoIR";
			 public const string IdCustodianteAcoes = "IdCustodianteAcoes";
			 public const string IdCustodianteOpcoes = "IdCustodianteOpcoes";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClienteBolsaMetadata))
			{
				if(ClienteBolsaMetadata.mapDelegates == null)
				{
					ClienteBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClienteBolsaMetadata.meta == null)
				{
					ClienteBolsaMetadata.meta = new ClienteBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCotacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CodigoSinacor", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoSinacor2", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAssessor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCusto", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IsentoIR", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdCustodianteAcoes", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCustodianteOpcoes", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "ClienteBolsa";
				meta.Destination = "ClienteBolsa";
				
				meta.spInsert = "proc_ClienteBolsaInsert";				
				meta.spUpdate = "proc_ClienteBolsaUpdate";		
				meta.spDelete = "proc_ClienteBolsaDelete";
				meta.spLoadAll = "proc_ClienteBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClienteBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClienteBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
