/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/11/2014 18:05:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esIncorporacaoCisaoFusaoBolsaCollection : esEntityCollection
	{
		public esIncorporacaoCisaoFusaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "IncorporacaoCisaoFusaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esIncorporacaoCisaoFusaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esIncorporacaoCisaoFusaoBolsaQuery);
		}
		#endregion
		
		virtual public IncorporacaoCisaoFusaoBolsa DetachEntity(IncorporacaoCisaoFusaoBolsa entity)
		{
			return base.DetachEntity(entity) as IncorporacaoCisaoFusaoBolsa;
		}
		
		virtual public IncorporacaoCisaoFusaoBolsa AttachEntity(IncorporacaoCisaoFusaoBolsa entity)
		{
			return base.AttachEntity(entity) as IncorporacaoCisaoFusaoBolsa;
		}
		
		virtual public void Combine(IncorporacaoCisaoFusaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public IncorporacaoCisaoFusaoBolsa this[int index]
		{
			get
			{
				return base[index] as IncorporacaoCisaoFusaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(IncorporacaoCisaoFusaoBolsa);
		}
	}



	[Serializable]
	abstract public class esIncorporacaoCisaoFusaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esIncorporacaoCisaoFusaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esIncorporacaoCisaoFusaoBolsa()
		{

		}

		public esIncorporacaoCisaoFusaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idIncorporacaoCisaoFusaoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(idIncorporacaoCisaoFusaoBolsa);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idIncorporacaoCisaoFusaoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(idIncorporacaoCisaoFusaoBolsa);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			esIncorporacaoCisaoFusaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdIncorporacaoCisaoFusaoBolsa == idIncorporacaoCisaoFusaoBolsa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			esParameters parms = new esParameters();
			parms.Add("IdIncorporacaoCisaoFusaoBolsa",idIncorporacaoCisaoFusaoBolsa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdIncorporacaoCisaoFusaoBolsa": this.str.IdIncorporacaoCisaoFusaoBolsa = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "DataPosicao": this.str.DataPosicao = (string)value; break;							
						case "DataEx": this.str.DataEx = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdIncorporacaoCisaoFusaoBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdIncorporacaoCisaoFusaoBolsa = (System.Int32?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Tipo = (System.Int32?)value;
							break;
						
						case "DataPosicao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPosicao = (System.DateTime?)value;
							break;
						
						case "DataEx":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEx = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsa.IdIncorporacaoCisaoFusaoBolsa
		/// </summary>
		virtual public System.Int32? IdIncorporacaoCisaoFusaoBolsa
		{
			get
			{
				return base.GetSystemInt32(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.IdIncorporacaoCisaoFusaoBolsa);
			}
			
			set
			{
				base.SetSystemInt32(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.IdIncorporacaoCisaoFusaoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsa.Tipo
		/// </summary>
		virtual public System.Int32? Tipo
		{
			get
			{
				return base.GetSystemInt32(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemInt32(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.Tipo, value);
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsa.DataPosicao
		/// </summary>
		virtual public System.DateTime? DataPosicao
		{
			get
			{
				return base.GetSystemDateTime(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.DataPosicao);
			}
			
			set
			{
				base.SetSystemDateTime(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.DataPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsa.DataEx
		/// </summary>
		virtual public System.DateTime? DataEx
		{
			get
			{
				return base.GetSystemDateTime(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.DataEx);
			}
			
			set
			{
				base.SetSystemDateTime(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.DataEx, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esIncorporacaoCisaoFusaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdIncorporacaoCisaoFusaoBolsa
			{
				get
				{
					System.Int32? data = entity.IdIncorporacaoCisaoFusaoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIncorporacaoCisaoFusaoBolsa = null;
					else entity.IdIncorporacaoCisaoFusaoBolsa = Convert.ToInt32(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Int32? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String DataPosicao
			{
				get
				{
					System.DateTime? data = entity.DataPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPosicao = null;
					else entity.DataPosicao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEx
			{
				get
				{
					System.DateTime? data = entity.DataEx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEx = null;
					else entity.DataEx = Convert.ToDateTime(value);
				}
			}
			

			private esIncorporacaoCisaoFusaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esIncorporacaoCisaoFusaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esIncorporacaoCisaoFusaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class IncorporacaoCisaoFusaoBolsa : esIncorporacaoCisaoFusaoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esIncorporacaoCisaoFusaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return IncorporacaoCisaoFusaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdIncorporacaoCisaoFusaoBolsa
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.IdIncorporacaoCisaoFusaoBolsa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.Tipo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem DataPosicao
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.DataPosicao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataEx
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.DataEx, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("IncorporacaoCisaoFusaoBolsaCollection")]
	public partial class IncorporacaoCisaoFusaoBolsaCollection : esIncorporacaoCisaoFusaoBolsaCollection, IEnumerable<IncorporacaoCisaoFusaoBolsa>
	{
		public IncorporacaoCisaoFusaoBolsaCollection()
		{

		}
		
		public static implicit operator List<IncorporacaoCisaoFusaoBolsa>(IncorporacaoCisaoFusaoBolsaCollection coll)
		{
			List<IncorporacaoCisaoFusaoBolsa> list = new List<IncorporacaoCisaoFusaoBolsa>();
			
			foreach (IncorporacaoCisaoFusaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  IncorporacaoCisaoFusaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IncorporacaoCisaoFusaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new IncorporacaoCisaoFusaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new IncorporacaoCisaoFusaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public IncorporacaoCisaoFusaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncorporacaoCisaoFusaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(IncorporacaoCisaoFusaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public IncorporacaoCisaoFusaoBolsa AddNew()
		{
			IncorporacaoCisaoFusaoBolsa entity = base.AddNewEntity() as IncorporacaoCisaoFusaoBolsa;
			
			return entity;
		}

		public IncorporacaoCisaoFusaoBolsa FindByPrimaryKey(System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			return base.FindByPrimaryKey(idIncorporacaoCisaoFusaoBolsa) as IncorporacaoCisaoFusaoBolsa;
		}


		#region IEnumerable<IncorporacaoCisaoFusaoBolsa> Members

		IEnumerator<IncorporacaoCisaoFusaoBolsa> IEnumerable<IncorporacaoCisaoFusaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as IncorporacaoCisaoFusaoBolsa;
			}
		}

		#endregion
		
		private IncorporacaoCisaoFusaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'IncorporacaoCisaoFusaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class IncorporacaoCisaoFusaoBolsa : esIncorporacaoCisaoFusaoBolsa
	{
		public IncorporacaoCisaoFusaoBolsa()
		{

		}
	
		public IncorporacaoCisaoFusaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IncorporacaoCisaoFusaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esIncorporacaoCisaoFusaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IncorporacaoCisaoFusaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public IncorporacaoCisaoFusaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncorporacaoCisaoFusaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(IncorporacaoCisaoFusaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private IncorporacaoCisaoFusaoBolsaQuery query;
	}



	[Serializable]
	public partial class IncorporacaoCisaoFusaoBolsaQuery : esIncorporacaoCisaoFusaoBolsaQuery
	{
		public IncorporacaoCisaoFusaoBolsaQuery()
		{

		}		
		
		public IncorporacaoCisaoFusaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class IncorporacaoCisaoFusaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IncorporacaoCisaoFusaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.IdIncorporacaoCisaoFusaoBolsa, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaMetadata.PropertyNames.IdIncorporacaoCisaoFusaoBolsa;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.Tipo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.CdAtivoBolsa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.DataPosicao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaMetadata.PropertyNames.DataPosicao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.DataEx, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaMetadata.PropertyNames.DataEx;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public IncorporacaoCisaoFusaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdIncorporacaoCisaoFusaoBolsa = "IdIncorporacaoCisaoFusaoBolsa";
			 public const string Tipo = "Tipo";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataPosicao = "DataPosicao";
			 public const string DataEx = "DataEx";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdIncorporacaoCisaoFusaoBolsa = "IdIncorporacaoCisaoFusaoBolsa";
			 public const string Tipo = "Tipo";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataPosicao = "DataPosicao";
			 public const string DataEx = "DataEx";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IncorporacaoCisaoFusaoBolsaMetadata))
			{
				if(IncorporacaoCisaoFusaoBolsaMetadata.mapDelegates == null)
				{
					IncorporacaoCisaoFusaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IncorporacaoCisaoFusaoBolsaMetadata.meta == null)
				{
					IncorporacaoCisaoFusaoBolsaMetadata.meta = new IncorporacaoCisaoFusaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdIncorporacaoCisaoFusaoBolsa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Tipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataPosicao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEx", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "IncorporacaoCisaoFusaoBolsa";
				meta.Destination = "IncorporacaoCisaoFusaoBolsa";
				
				meta.spInsert = "proc_IncorporacaoCisaoFusaoBolsaInsert";				
				meta.spUpdate = "proc_IncorporacaoCisaoFusaoBolsaUpdate";		
				meta.spDelete = "proc_IncorporacaoCisaoFusaoBolsaDelete";
				meta.spLoadAll = "proc_IncorporacaoCisaoFusaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_IncorporacaoCisaoFusaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IncorporacaoCisaoFusaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
