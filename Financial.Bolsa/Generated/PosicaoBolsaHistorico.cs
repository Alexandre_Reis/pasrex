/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;































using Financial.Common;
using Financial.Investidor;		







		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esPosicaoBolsaHistoricoCollection : esEntityCollection
	{
		public esPosicaoBolsaHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoBolsaHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoBolsaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoBolsaHistoricoQuery);
		}
		#endregion
		
		virtual public PosicaoBolsaHistorico DetachEntity(PosicaoBolsaHistorico entity)
		{
			return base.DetachEntity(entity) as PosicaoBolsaHistorico;
		}
		
		virtual public PosicaoBolsaHistorico AttachEntity(PosicaoBolsaHistorico entity)
		{
			return base.AttachEntity(entity) as PosicaoBolsaHistorico;
		}
		
		virtual public void Combine(PosicaoBolsaHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoBolsaHistorico this[int index]
		{
			get
			{
				return base[index] as PosicaoBolsaHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoBolsaHistorico);
		}
	}



	[Serializable]
	abstract public class esPosicaoBolsaHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoBolsaHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoBolsaHistorico()
		{

		}

		public esPosicaoBolsaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoBolsaHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esPosicaoBolsaHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "PUCusto": this.str.PUCusto = (string)value; break;							
						case "PUCustoLiquido": this.str.PUCustoLiquido = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "ValorCustoLiquido": this.str.ValorCustoLiquido = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "QuantidadeBloqueada": this.str.QuantidadeBloqueada = (string)value; break;							
						case "ResultadoRealizar": this.str.ResultadoRealizar = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "PUCusto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCusto = (System.Decimal?)value;
							break;
						
						case "PUCustoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCustoLiquido = (System.Decimal?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "ValorCustoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCustoLiquido = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "QuantidadeBloqueada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeBloqueada = (System.Decimal?)value;
							break;
						
						case "ResultadoRealizar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ResultadoRealizar = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoBolsaHistoricoMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoBolsaHistoricoMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoBolsaHistoricoMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoBolsaHistoricoMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoBolsaHistoricoMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoBolsaHistoricoMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoBolsaHistoricoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoBolsaHistoricoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(PosicaoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(PosicaoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.TipoMercado
		/// </summary>
		virtual public System.String TipoMercado
		{
			get
			{
				return base.GetSystemString(PosicaoBolsaHistoricoMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemString(PosicaoBolsaHistoricoMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoBolsaHistoricoMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoBolsaHistoricoMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.PUCusto
		/// </summary>
		virtual public System.Decimal? PUCusto
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.PUCusto);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.PUCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.PUCustoLiquido
		/// </summary>
		virtual public System.Decimal? PUCustoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.PUCustoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.PUCustoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.ValorCustoLiquido
		/// </summary>
		virtual public System.Decimal? ValorCustoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorCustoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorCustoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.QuantidadeBloqueada
		/// </summary>
		virtual public System.Decimal? QuantidadeBloqueada
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.QuantidadeBloqueada);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.QuantidadeBloqueada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoBolsaHistorico.ResultadoRealizar
		/// </summary>
		virtual public System.Decimal? ResultadoRealizar
		{
			get
			{
				return base.GetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoBolsaHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.String data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToString(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCusto
			{
				get
				{
					System.Decimal? data = entity.PUCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCusto = null;
					else entity.PUCusto = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCustoLiquido
			{
				get
				{
					System.Decimal? data = entity.PUCustoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCustoLiquido = null;
					else entity.PUCustoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCustoLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorCustoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCustoLiquido = null;
					else entity.ValorCustoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeBloqueada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeBloqueada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeBloqueada = null;
					else entity.QuantidadeBloqueada = Convert.ToDecimal(value);
				}
			}
				
			public System.String ResultadoRealizar
			{
				get
				{
					System.Decimal? data = entity.ResultadoRealizar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ResultadoRealizar = null;
					else entity.ResultadoRealizar = Convert.ToDecimal(value);
				}
			}
			

			private esPosicaoBolsaHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoBolsaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoBolsaHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoBolsaHistorico : esPosicaoBolsaHistorico
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PosicaoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_PosicaoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoBolsaHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoBolsaHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.TipoMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCusto
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.PUCusto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCustoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.PUCustoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCustoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.ValorCustoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeBloqueada
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.QuantidadeBloqueada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ResultadoRealizar
		{
			get
			{
				return new esQueryItem(this, PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoBolsaHistoricoCollection")]
	public partial class PosicaoBolsaHistoricoCollection : esPosicaoBolsaHistoricoCollection, IEnumerable<PosicaoBolsaHistorico>
	{
		public PosicaoBolsaHistoricoCollection()
		{

		}
		
		public static implicit operator List<PosicaoBolsaHistorico>(PosicaoBolsaHistoricoCollection coll)
		{
			List<PosicaoBolsaHistorico> list = new List<PosicaoBolsaHistorico>();
			
			foreach (PosicaoBolsaHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoBolsaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoBolsaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoBolsaHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoBolsaHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoBolsaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoBolsaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoBolsaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoBolsaHistorico AddNew()
		{
			PosicaoBolsaHistorico entity = base.AddNewEntity() as PosicaoBolsaHistorico;
			
			return entity;
		}

		public PosicaoBolsaHistorico FindByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idPosicao, dataHistorico) as PosicaoBolsaHistorico;
		}


		#region IEnumerable<PosicaoBolsaHistorico> Members

		IEnumerator<PosicaoBolsaHistorico> IEnumerable<PosicaoBolsaHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoBolsaHistorico;
			}
		}

		#endregion
		
		private PosicaoBolsaHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoBolsaHistorico' table
	/// </summary>

	[Serializable]
	public partial class PosicaoBolsaHistorico : esPosicaoBolsaHistorico
	{
		public PosicaoBolsaHistorico()
		{

		}
	
		public PosicaoBolsaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoBolsaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoBolsaHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoBolsaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoBolsaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoBolsaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoBolsaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoBolsaHistoricoQuery query;
	}



	[Serializable]
	public partial class PosicaoBolsaHistoricoQuery : esPosicaoBolsaHistoricoQuery
	{
		public PosicaoBolsaHistoricoQuery()
		{

		}		
		
		public PosicaoBolsaHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoBolsaHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoBolsaHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.IdAgente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.TipoMercado, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.TipoMercado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.DataVencimento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.PUMercado, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.PUCusto, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.PUCusto;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.PUCustoLiquido, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.PUCustoLiquido;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorMercado, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.ValorCustoLiquido, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.ValorCustoLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.Quantidade, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.QuantidadeInicial, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.QuantidadeBloqueada, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.QuantidadeBloqueada;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoBolsaHistoricoMetadata.ColumnNames.ResultadoRealizar, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoBolsaHistoricoMetadata.PropertyNames.ResultadoRealizar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoBolsaHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdAgente = "IdAgente";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string PUCustoLiquido = "PUCustoLiquido";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCustoLiquido = "ValorCustoLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string ResultadoRealizar = "ResultadoRealizar";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdAgente = "IdAgente";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string TipoMercado = "TipoMercado";
			 public const string DataVencimento = "DataVencimento";
			 public const string PUMercado = "PUMercado";
			 public const string PUCusto = "PUCusto";
			 public const string PUCustoLiquido = "PUCustoLiquido";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCustoLiquido = "ValorCustoLiquido";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string ResultadoRealizar = "ResultadoRealizar";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoBolsaHistoricoMetadata))
			{
				if(PosicaoBolsaHistoricoMetadata.mapDelegates == null)
				{
					PosicaoBolsaHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoBolsaHistoricoMetadata.meta == null)
				{
					PosicaoBolsaHistoricoMetadata.meta = new PosicaoBolsaHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCusto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCustoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCustoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeBloqueada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ResultadoRealizar", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PosicaoBolsaHistorico";
				meta.Destination = "PosicaoBolsaHistorico";
				
				meta.spInsert = "proc_PosicaoBolsaHistoricoInsert";				
				meta.spUpdate = "proc_PosicaoBolsaHistoricoUpdate";		
				meta.spDelete = "proc_PosicaoBolsaHistoricoDelete";
				meta.spLoadAll = "proc_PosicaoBolsaHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoBolsaHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoBolsaHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
