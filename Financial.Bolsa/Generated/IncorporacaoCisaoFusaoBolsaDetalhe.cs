/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/11/2014 18:05:48
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esIncorporacaoCisaoFusaoBolsaDetalheCollection : esEntityCollection
	{
		public esIncorporacaoCisaoFusaoBolsaDetalheCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "IncorporacaoCisaoFusaoBolsaDetalheCollection";
		}

		#region Query Logic
		protected void InitQuery(esIncorporacaoCisaoFusaoBolsaDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esIncorporacaoCisaoFusaoBolsaDetalheQuery);
		}
		#endregion
		
		virtual public IncorporacaoCisaoFusaoBolsaDetalhe DetachEntity(IncorporacaoCisaoFusaoBolsaDetalhe entity)
		{
			return base.DetachEntity(entity) as IncorporacaoCisaoFusaoBolsaDetalhe;
		}
		
		virtual public IncorporacaoCisaoFusaoBolsaDetalhe AttachEntity(IncorporacaoCisaoFusaoBolsaDetalhe entity)
		{
			return base.AttachEntity(entity) as IncorporacaoCisaoFusaoBolsaDetalhe;
		}
		
		virtual public void Combine(IncorporacaoCisaoFusaoBolsaDetalheCollection collection)
		{
			base.Combine(collection);
		}
		
		new public IncorporacaoCisaoFusaoBolsaDetalhe this[int index]
		{
			get
			{
				return base[index] as IncorporacaoCisaoFusaoBolsaDetalhe;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(IncorporacaoCisaoFusaoBolsaDetalhe);
		}
	}



	[Serializable]
	abstract public class esIncorporacaoCisaoFusaoBolsaDetalhe : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esIncorporacaoCisaoFusaoBolsaDetalheQuery GetDynamicQuery()
		{
			return null;
		}

		public esIncorporacaoCisaoFusaoBolsaDetalhe()
		{

		}

		public esIncorporacaoCisaoFusaoBolsaDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String cdAtivoBolsa, System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBolsa, idIncorporacaoCisaoFusaoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBolsa, idIncorporacaoCisaoFusaoBolsa);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String cdAtivoBolsa, System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBolsa, idIncorporacaoCisaoFusaoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBolsa, idIncorporacaoCisaoFusaoBolsa);
		}

		private bool LoadByPrimaryKeyDynamic(System.String cdAtivoBolsa, System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			esIncorporacaoCisaoFusaoBolsaDetalheQuery query = this.GetDynamicQuery();
			query.Where(query.CdAtivoBolsa == cdAtivoBolsa, query.IdIncorporacaoCisaoFusaoBolsa == idIncorporacaoCisaoFusaoBolsa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String cdAtivoBolsa, System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			esParameters parms = new esParameters();
			parms.Add("CdAtivoBolsa",cdAtivoBolsa);			parms.Add("IdIncorporacaoCisaoFusaoBolsa",idIncorporacaoCisaoFusaoBolsa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdIncorporacaoCisaoFusaoBolsa": this.str.IdIncorporacaoCisaoFusaoBolsa = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "Fator": this.str.Fator = (string)value; break;							
						case "Custo": this.str.Custo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdIncorporacaoCisaoFusaoBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdIncorporacaoCisaoFusaoBolsa = (System.Int32?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "Fator":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Fator = (System.Decimal?)value;
							break;
						
						case "Custo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Custo = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsaDetalhe.IdIncorporacaoCisaoFusaoBolsa
		/// </summary>
		virtual public System.Int32? IdIncorporacaoCisaoFusaoBolsa
		{
			get
			{
				return base.GetSystemInt32(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.IdIncorporacaoCisaoFusaoBolsa);
			}
			
			set
			{
				base.SetSystemInt32(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.IdIncorporacaoCisaoFusaoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsaDetalhe.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsaDetalhe.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsaDetalhe.Fator
		/// </summary>
		virtual public System.Decimal? Fator
		{
			get
			{
				return base.GetSystemDecimal(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Fator);
			}
			
			set
			{
				base.SetSystemDecimal(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Fator, value);
			}
		}
		
		/// <summary>
		/// Maps to IncorporacaoCisaoFusaoBolsaDetalhe.Custo
		/// </summary>
		virtual public System.Decimal? Custo
		{
			get
			{
				return base.GetSystemDecimal(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Custo);
			}
			
			set
			{
				base.SetSystemDecimal(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Custo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esIncorporacaoCisaoFusaoBolsaDetalhe entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdIncorporacaoCisaoFusaoBolsa
			{
				get
				{
					System.Int32? data = entity.IdIncorporacaoCisaoFusaoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIncorporacaoCisaoFusaoBolsa = null;
					else entity.IdIncorporacaoCisaoFusaoBolsa = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String Fator
			{
				get
				{
					System.Decimal? data = entity.Fator;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fator = null;
					else entity.Fator = Convert.ToDecimal(value);
				}
			}
				
			public System.String Custo
			{
				get
				{
					System.Decimal? data = entity.Custo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Custo = null;
					else entity.Custo = Convert.ToDecimal(value);
				}
			}
			

			private esIncorporacaoCisaoFusaoBolsaDetalhe entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esIncorporacaoCisaoFusaoBolsaDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esIncorporacaoCisaoFusaoBolsaDetalhe can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class IncorporacaoCisaoFusaoBolsaDetalhe : esIncorporacaoCisaoFusaoBolsaDetalhe
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esIncorporacaoCisaoFusaoBolsaDetalheQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return IncorporacaoCisaoFusaoBolsaDetalheMetadata.Meta();
			}
		}	
		

		public esQueryItem IdIncorporacaoCisaoFusaoBolsa
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.IdIncorporacaoCisaoFusaoBolsa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Fator
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Fator, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Custo
		{
			get
			{
				return new esQueryItem(this, IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Custo, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("IncorporacaoCisaoFusaoBolsaDetalheCollection")]
	public partial class IncorporacaoCisaoFusaoBolsaDetalheCollection : esIncorporacaoCisaoFusaoBolsaDetalheCollection, IEnumerable<IncorporacaoCisaoFusaoBolsaDetalhe>
	{
		public IncorporacaoCisaoFusaoBolsaDetalheCollection()
		{

		}
		
		public static implicit operator List<IncorporacaoCisaoFusaoBolsaDetalhe>(IncorporacaoCisaoFusaoBolsaDetalheCollection coll)
		{
			List<IncorporacaoCisaoFusaoBolsaDetalhe> list = new List<IncorporacaoCisaoFusaoBolsaDetalhe>();
			
			foreach (IncorporacaoCisaoFusaoBolsaDetalhe emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  IncorporacaoCisaoFusaoBolsaDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IncorporacaoCisaoFusaoBolsaDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new IncorporacaoCisaoFusaoBolsaDetalhe(row);
		}

		override protected esEntity CreateEntity()
		{
			return new IncorporacaoCisaoFusaoBolsaDetalhe();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public IncorporacaoCisaoFusaoBolsaDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncorporacaoCisaoFusaoBolsaDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(IncorporacaoCisaoFusaoBolsaDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public IncorporacaoCisaoFusaoBolsaDetalhe AddNew()
		{
			IncorporacaoCisaoFusaoBolsaDetalhe entity = base.AddNewEntity() as IncorporacaoCisaoFusaoBolsaDetalhe;
			
			return entity;
		}

		public IncorporacaoCisaoFusaoBolsaDetalhe FindByPrimaryKey(System.String cdAtivoBolsa, System.Int32 idIncorporacaoCisaoFusaoBolsa)
		{
			return base.FindByPrimaryKey(cdAtivoBolsa, idIncorporacaoCisaoFusaoBolsa) as IncorporacaoCisaoFusaoBolsaDetalhe;
		}


		#region IEnumerable<IncorporacaoCisaoFusaoBolsaDetalhe> Members

		IEnumerator<IncorporacaoCisaoFusaoBolsaDetalhe> IEnumerable<IncorporacaoCisaoFusaoBolsaDetalhe>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as IncorporacaoCisaoFusaoBolsaDetalhe;
			}
		}

		#endregion
		
		private IncorporacaoCisaoFusaoBolsaDetalheQuery query;
	}


	/// <summary>
	/// Encapsulates the 'IncorporacaoCisaoFusaoBolsaDetalhe' table
	/// </summary>

	[Serializable]
	public partial class IncorporacaoCisaoFusaoBolsaDetalhe : esIncorporacaoCisaoFusaoBolsaDetalhe
	{
		public IncorporacaoCisaoFusaoBolsaDetalhe()
		{

		}
	
		public IncorporacaoCisaoFusaoBolsaDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return IncorporacaoCisaoFusaoBolsaDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esIncorporacaoCisaoFusaoBolsaDetalheQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new IncorporacaoCisaoFusaoBolsaDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public IncorporacaoCisaoFusaoBolsaDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new IncorporacaoCisaoFusaoBolsaDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(IncorporacaoCisaoFusaoBolsaDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private IncorporacaoCisaoFusaoBolsaDetalheQuery query;
	}



	[Serializable]
	public partial class IncorporacaoCisaoFusaoBolsaDetalheQuery : esIncorporacaoCisaoFusaoBolsaDetalheQuery
	{
		public IncorporacaoCisaoFusaoBolsaDetalheQuery()
		{

		}		
		
		public IncorporacaoCisaoFusaoBolsaDetalheQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class IncorporacaoCisaoFusaoBolsaDetalheMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected IncorporacaoCisaoFusaoBolsaDetalheMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.IdIncorporacaoCisaoFusaoBolsa, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaDetalheMetadata.PropertyNames.IdIncorporacaoCisaoFusaoBolsa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaDetalheMetadata.PropertyNames.CdAtivoBolsa;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Percentual, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaDetalheMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Fator, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaDetalheMetadata.PropertyNames.Fator;	
			c.NumericPrecision = 5;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.Custo, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = IncorporacaoCisaoFusaoBolsaDetalheMetadata.PropertyNames.Custo;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public IncorporacaoCisaoFusaoBolsaDetalheMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdIncorporacaoCisaoFusaoBolsa = "IdIncorporacaoCisaoFusaoBolsa";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Percentual = "Percentual";
			 public const string Fator = "Fator";
			 public const string Custo = "Custo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdIncorporacaoCisaoFusaoBolsa = "IdIncorporacaoCisaoFusaoBolsa";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Percentual = "Percentual";
			 public const string Fator = "Fator";
			 public const string Custo = "Custo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(IncorporacaoCisaoFusaoBolsaDetalheMetadata))
			{
				if(IncorporacaoCisaoFusaoBolsaDetalheMetadata.mapDelegates == null)
				{
					IncorporacaoCisaoFusaoBolsaDetalheMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (IncorporacaoCisaoFusaoBolsaDetalheMetadata.meta == null)
				{
					IncorporacaoCisaoFusaoBolsaDetalheMetadata.meta = new IncorporacaoCisaoFusaoBolsaDetalheMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdIncorporacaoCisaoFusaoBolsa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Fator", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Custo", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "IncorporacaoCisaoFusaoBolsaDetalhe";
				meta.Destination = "IncorporacaoCisaoFusaoBolsaDetalhe";
				
				meta.spInsert = "proc_IncorporacaoCisaoFusaoBolsaDetalheInsert";				
				meta.spUpdate = "proc_IncorporacaoCisaoFusaoBolsaDetalheUpdate";		
				meta.spDelete = "proc_IncorporacaoCisaoFusaoBolsaDetalheDelete";
				meta.spLoadAll = "proc_IncorporacaoCisaoFusaoBolsaDetalheLoadAll";
				meta.spLoadByPrimaryKey = "proc_IncorporacaoCisaoFusaoBolsaDetalheLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private IncorporacaoCisaoFusaoBolsaDetalheMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
