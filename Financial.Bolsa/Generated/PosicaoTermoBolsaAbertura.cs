/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



using Financial.Common;
using Financial.Investidor;										

			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esPosicaoTermoBolsaAberturaCollection : esEntityCollection
	{
		public esPosicaoTermoBolsaAberturaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoTermoBolsaAberturaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoTermoBolsaAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoTermoBolsaAberturaQuery);
		}
		#endregion
		
		virtual public PosicaoTermoBolsaAbertura DetachEntity(PosicaoTermoBolsaAbertura entity)
		{
			return base.DetachEntity(entity) as PosicaoTermoBolsaAbertura;
		}
		
		virtual public PosicaoTermoBolsaAbertura AttachEntity(PosicaoTermoBolsaAbertura entity)
		{
			return base.AttachEntity(entity) as PosicaoTermoBolsaAbertura;
		}
		
		virtual public void Combine(PosicaoTermoBolsaAberturaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoTermoBolsaAbertura this[int index]
		{
			get
			{
				return base[index] as PosicaoTermoBolsaAbertura;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoTermoBolsaAbertura);
		}
	}



	[Serializable]
	abstract public class esPosicaoTermoBolsaAbertura : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoTermoBolsaAberturaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoTermoBolsaAbertura()
		{

		}

		public esPosicaoTermoBolsaAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoTermoBolsaAberturaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esPosicaoTermoBolsaAberturaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "PrazoTotal": this.str.PrazoTotal = (string)value; break;							
						case "PrazoDecorrer": this.str.PrazoDecorrer = (string)value; break;							
						case "TaxaAno": this.str.TaxaAno = (string)value; break;							
						case "TaxaMTM": this.str.TaxaMTM = (string)value; break;							
						case "ValorTermo": this.str.ValorTermo = (string)value; break;							
						case "ValorTermoLiquido": this.str.ValorTermoLiquido = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "ValorCurva": this.str.ValorCurva = (string)value; break;							
						case "ValorCorrigido": this.str.ValorCorrigido = (string)value; break;							
						case "PUCustoLiquidoAcao": this.str.PUCustoLiquidoAcao = (string)value; break;							
						case "PUTermo": this.str.PUTermo = (string)value; break;							
						case "PUTermoLiquido": this.str.PUTermoLiquido = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "RendimentoTotal": this.str.RendimentoTotal = (string)value; break;							
						case "RendimentoApropriar": this.str.RendimentoApropriar = (string)value; break;							
						case "RendimentoApropriado": this.str.RendimentoApropriado = (string)value; break;							
						case "RendimentoDia": this.str.RendimentoDia = (string)value; break;							
						case "TipoTermo": this.str.TipoTermo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "PrazoTotal":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.PrazoTotal = (System.Int16?)value;
							break;
						
						case "PrazoDecorrer":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.PrazoDecorrer = (System.Int16?)value;
							break;
						
						case "TaxaAno":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaAno = (System.Decimal?)value;
							break;
						
						case "TaxaMTM":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaMTM = (System.Decimal?)value;
							break;
						
						case "ValorTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTermo = (System.Decimal?)value;
							break;
						
						case "ValorTermoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorTermoLiquido = (System.Decimal?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "ValorCurva":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCurva = (System.Decimal?)value;
							break;
						
						case "ValorCorrigido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCorrigido = (System.Decimal?)value;
							break;
						
						case "PUCustoLiquidoAcao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCustoLiquidoAcao = (System.Decimal?)value;
							break;
						
						case "PUTermo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUTermo = (System.Decimal?)value;
							break;
						
						case "PUTermoLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUTermoLiquido = (System.Decimal?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "RendimentoTotal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoTotal = (System.Decimal?)value;
							break;
						
						case "RendimentoApropriar":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoApropriar = (System.Decimal?)value;
							break;
						
						case "RendimentoApropriado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoApropriado = (System.Decimal?)value;
							break;
						
						case "RendimentoDia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoDia = (System.Decimal?)value;
							break;
						
						case "TipoTermo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoTermo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(PosicaoTermoBolsaAberturaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(PosicaoTermoBolsaAberturaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.PrazoTotal
		/// </summary>
		virtual public System.Int16? PrazoTotal
		{
			get
			{
				return base.GetSystemInt16(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PrazoTotal);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PrazoTotal, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.PrazoDecorrer
		/// </summary>
		virtual public System.Int16? PrazoDecorrer
		{
			get
			{
				return base.GetSystemInt16(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PrazoDecorrer);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PrazoDecorrer, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.TaxaAno
		/// </summary>
		virtual public System.Decimal? TaxaAno
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.TaxaAno);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.TaxaAno, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.TaxaMTM
		/// </summary>
		virtual public System.Decimal? TaxaMTM
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.TaxaMTM);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.TaxaMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.ValorTermo
		/// </summary>
		virtual public System.Decimal? ValorTermo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorTermo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.ValorTermoLiquido
		/// </summary>
		virtual public System.Decimal? ValorTermoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorTermoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorTermoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.ValorCurva
		/// </summary>
		virtual public System.Decimal? ValorCurva
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorCurva);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorCurva, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.ValorCorrigido
		/// </summary>
		virtual public System.Decimal? ValorCorrigido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorCorrigido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorCorrigido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.PUCustoLiquidoAcao
		/// </summary>
		virtual public System.Decimal? PUCustoLiquidoAcao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUCustoLiquidoAcao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUCustoLiquidoAcao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.PUTermo
		/// </summary>
		virtual public System.Decimal? PUTermo
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUTermo);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.PUTermoLiquido
		/// </summary>
		virtual public System.Decimal? PUTermoLiquido
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUTermoLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUTermoLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.NumeroContrato
		/// </summary>
		virtual public System.String NumeroContrato
		{
			get
			{
				return base.GetSystemString(PosicaoTermoBolsaAberturaMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemString(PosicaoTermoBolsaAberturaMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.RendimentoTotal
		/// </summary>
		virtual public System.Decimal? RendimentoTotal
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoTotal);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoTotal, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.RendimentoApropriar
		/// </summary>
		virtual public System.Decimal? RendimentoApropriar
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoApropriar);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoApropriar, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.RendimentoApropriado
		/// </summary>
		virtual public System.Decimal? RendimentoApropriado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoApropriado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoApropriado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.RendimentoDia
		/// </summary>
		virtual public System.Decimal? RendimentoDia
		{
			get
			{
				return base.GetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoDia);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoDia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoTermoBolsaAbertura.TipoTermo
		/// </summary>
		virtual public System.Byte? TipoTermo
		{
			get
			{
				return base.GetSystemByte(PosicaoTermoBolsaAberturaMetadata.ColumnNames.TipoTermo);
			}
			
			set
			{
				base.SetSystemByte(PosicaoTermoBolsaAberturaMetadata.ColumnNames.TipoTermo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoTermoBolsaAbertura entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrazoTotal
			{
				get
				{
					System.Int16? data = entity.PrazoTotal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoTotal = null;
					else entity.PrazoTotal = Convert.ToInt16(value);
				}
			}
				
			public System.String PrazoDecorrer
			{
				get
				{
					System.Int16? data = entity.PrazoDecorrer;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoDecorrer = null;
					else entity.PrazoDecorrer = Convert.ToInt16(value);
				}
			}
				
			public System.String TaxaAno
			{
				get
				{
					System.Decimal? data = entity.TaxaAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaAno = null;
					else entity.TaxaAno = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaMTM
			{
				get
				{
					System.Decimal? data = entity.TaxaMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaMTM = null;
					else entity.TaxaMTM = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTermo
			{
				get
				{
					System.Decimal? data = entity.ValorTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTermo = null;
					else entity.ValorTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorTermoLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorTermoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorTermoLiquido = null;
					else entity.ValorTermoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCurva
			{
				get
				{
					System.Decimal? data = entity.ValorCurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCurva = null;
					else entity.ValorCurva = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCorrigido
			{
				get
				{
					System.Decimal? data = entity.ValorCorrigido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCorrigido = null;
					else entity.ValorCorrigido = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCustoLiquidoAcao
			{
				get
				{
					System.Decimal? data = entity.PUCustoLiquidoAcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCustoLiquidoAcao = null;
					else entity.PUCustoLiquidoAcao = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUTermo
			{
				get
				{
					System.Decimal? data = entity.PUTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUTermo = null;
					else entity.PUTermo = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUTermoLiquido
			{
				get
				{
					System.Decimal? data = entity.PUTermoLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUTermoLiquido = null;
					else entity.PUTermoLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.String data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToString(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String RendimentoTotal
			{
				get
				{
					System.Decimal? data = entity.RendimentoTotal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoTotal = null;
					else entity.RendimentoTotal = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoApropriar
			{
				get
				{
					System.Decimal? data = entity.RendimentoApropriar;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoApropriar = null;
					else entity.RendimentoApropriar = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoApropriado
			{
				get
				{
					System.Decimal? data = entity.RendimentoApropriado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoApropriado = null;
					else entity.RendimentoApropriado = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoDia
			{
				get
				{
					System.Decimal? data = entity.RendimentoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoDia = null;
					else entity.RendimentoDia = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoTermo
			{
				get
				{
					System.Byte? data = entity.TipoTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoTermo = null;
					else entity.TipoTermo = Convert.ToByte(value);
				}
			}
			

			private esPosicaoTermoBolsaAbertura entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoTermoBolsaAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoTermoBolsaAbertura can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoTermoBolsaAbertura : esPosicaoTermoBolsaAbertura
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoTermoBolsaAberturaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoTermoBolsaAberturaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrazoTotal
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.PrazoTotal, esSystemType.Int16);
			}
		} 
		
		public esQueryItem PrazoDecorrer
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.PrazoDecorrer, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TaxaAno
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.TaxaAno, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaMTM
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.TaxaMTM, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTermo
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorTermoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorTermoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCurva
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorCurva, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCorrigido
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorCorrigido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCustoLiquidoAcao
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUCustoLiquidoAcao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUTermo
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUTermo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUTermoLiquido
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUTermoLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.NumeroContrato, esSystemType.String);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem RendimentoTotal
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoTotal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoApropriar
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoApropriar, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoApropriado
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoApropriado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoDia
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoDia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoTermo
		{
			get
			{
				return new esQueryItem(this, PosicaoTermoBolsaAberturaMetadata.ColumnNames.TipoTermo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoTermoBolsaAberturaCollection")]
	public partial class PosicaoTermoBolsaAberturaCollection : esPosicaoTermoBolsaAberturaCollection, IEnumerable<PosicaoTermoBolsaAbertura>
	{
		public PosicaoTermoBolsaAberturaCollection()
		{

		}
		
		public static implicit operator List<PosicaoTermoBolsaAbertura>(PosicaoTermoBolsaAberturaCollection coll)
		{
			List<PosicaoTermoBolsaAbertura> list = new List<PosicaoTermoBolsaAbertura>();
			
			foreach (PosicaoTermoBolsaAbertura emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoTermoBolsaAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoTermoBolsaAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoTermoBolsaAbertura(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoTermoBolsaAbertura();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoTermoBolsaAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoTermoBolsaAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoTermoBolsaAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoTermoBolsaAbertura AddNew()
		{
			PosicaoTermoBolsaAbertura entity = base.AddNewEntity() as PosicaoTermoBolsaAbertura;
			
			return entity;
		}

		public PosicaoTermoBolsaAbertura FindByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idPosicao, dataHistorico) as PosicaoTermoBolsaAbertura;
		}


		#region IEnumerable<PosicaoTermoBolsaAbertura> Members

		IEnumerator<PosicaoTermoBolsaAbertura> IEnumerable<PosicaoTermoBolsaAbertura>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoTermoBolsaAbertura;
			}
		}

		#endregion
		
		private PosicaoTermoBolsaAberturaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoTermoBolsaAbertura' table
	/// </summary>

	[Serializable]
	public partial class PosicaoTermoBolsaAbertura : esPosicaoTermoBolsaAbertura
	{
		public PosicaoTermoBolsaAbertura()
		{

		}
	
		public PosicaoTermoBolsaAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoTermoBolsaAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoTermoBolsaAberturaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoTermoBolsaAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoTermoBolsaAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoTermoBolsaAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoTermoBolsaAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoTermoBolsaAberturaQuery query;
	}



	[Serializable]
	public partial class PosicaoTermoBolsaAberturaQuery : esPosicaoTermoBolsaAberturaQuery
	{
		public PosicaoTermoBolsaAberturaQuery()
		{

		}		
		
		public PosicaoTermoBolsaAberturaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoTermoBolsaAberturaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoTermoBolsaAberturaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdOperacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdIndice, 3, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdCliente, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdAgente, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.CdAtivoBolsa, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataOperacao, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.DataVencimento, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.Quantidade, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.QuantidadeInicial, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PrazoTotal, 11, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.PrazoTotal;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PrazoDecorrer, 12, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.PrazoDecorrer;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.TaxaAno, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.TaxaAno;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.TaxaMTM, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.TaxaMTM;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorTermo, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.ValorTermo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorTermoLiquido, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.ValorTermoLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorMercado, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorCurva, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.ValorCurva;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.ValorCorrigido, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.ValorCorrigido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUCustoLiquidoAcao, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.PUCustoLiquidoAcao;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUTermo, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.PUTermo;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUTermoLiquido, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.PUTermoLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.PUMercado, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.NumeroContrato, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.NumeroContrato;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.IdTrader, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoTotal, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.RendimentoTotal;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoApropriar, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.RendimentoApropriar;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoApropriado, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.RendimentoApropriado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.RendimentoDia, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.RendimentoDia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoTermoBolsaAberturaMetadata.ColumnNames.TipoTermo, 30, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoTermoBolsaAberturaMetadata.PropertyNames.TipoTermo;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoTermoBolsaAberturaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdIndice = "IdIndice";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string PrazoTotal = "PrazoTotal";
			 public const string PrazoDecorrer = "PrazoDecorrer";
			 public const string TaxaAno = "TaxaAno";
			 public const string TaxaMTM = "TaxaMTM";
			 public const string ValorTermo = "ValorTermo";
			 public const string ValorTermoLiquido = "ValorTermoLiquido";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCurva = "ValorCurva";
			 public const string ValorCorrigido = "ValorCorrigido";
			 public const string PUCustoLiquidoAcao = "PUCustoLiquidoAcao";
			 public const string PUTermo = "PUTermo";
			 public const string PUTermoLiquido = "PUTermoLiquido";
			 public const string PUMercado = "PUMercado";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string IdTrader = "IdTrader";
			 public const string RendimentoTotal = "RendimentoTotal";
			 public const string RendimentoApropriar = "RendimentoApropriar";
			 public const string RendimentoApropriado = "RendimentoApropriado";
			 public const string RendimentoDia = "RendimentoDia";
			 public const string TipoTermo = "TipoTermo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdIndice = "IdIndice";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string PrazoTotal = "PrazoTotal";
			 public const string PrazoDecorrer = "PrazoDecorrer";
			 public const string TaxaAno = "TaxaAno";
			 public const string TaxaMTM = "TaxaMTM";
			 public const string ValorTermo = "ValorTermo";
			 public const string ValorTermoLiquido = "ValorTermoLiquido";
			 public const string ValorMercado = "ValorMercado";
			 public const string ValorCurva = "ValorCurva";
			 public const string ValorCorrigido = "ValorCorrigido";
			 public const string PUCustoLiquidoAcao = "PUCustoLiquidoAcao";
			 public const string PUTermo = "PUTermo";
			 public const string PUTermoLiquido = "PUTermoLiquido";
			 public const string PUMercado = "PUMercado";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string IdTrader = "IdTrader";
			 public const string RendimentoTotal = "RendimentoTotal";
			 public const string RendimentoApropriar = "RendimentoApropriar";
			 public const string RendimentoApropriado = "RendimentoApropriado";
			 public const string RendimentoDia = "RendimentoDia";
			 public const string TipoTermo = "TipoTermo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoTermoBolsaAberturaMetadata))
			{
				if(PosicaoTermoBolsaAberturaMetadata.mapDelegates == null)
				{
					PosicaoTermoBolsaAberturaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoTermoBolsaAberturaMetadata.meta == null)
				{
					PosicaoTermoBolsaAberturaMetadata.meta = new PosicaoTermoBolsaAberturaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrazoTotal", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("PrazoDecorrer", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TaxaAno", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaMTM", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTermo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorTermoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCurva", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCorrigido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCustoLiquidoAcao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUTermo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUTermoLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("RendimentoTotal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoApropriar", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoApropriado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoDia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoTermo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "PosicaoTermoBolsaAbertura";
				meta.Destination = "PosicaoTermoBolsaAbertura";
				
				meta.spInsert = "proc_PosicaoTermoBolsaAberturaInsert";				
				meta.spUpdate = "proc_PosicaoTermoBolsaAberturaUpdate";		
				meta.spDelete = "proc_PosicaoTermoBolsaAberturaDelete";
				meta.spLoadAll = "proc_PosicaoTermoBolsaAberturaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoTermoBolsaAberturaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoTermoBolsaAberturaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
