/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					



		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esCotacaoBolsaCollection : esEntityCollection
	{
		public esCotacaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoBolsaQuery);
		}
		#endregion
		
		virtual public CotacaoBolsa DetachEntity(CotacaoBolsa entity)
		{
			return base.DetachEntity(entity) as CotacaoBolsa;
		}
		
		virtual public CotacaoBolsa AttachEntity(CotacaoBolsa entity)
		{
			return base.AttachEntity(entity) as CotacaoBolsa;
		}
		
		virtual public void Combine(CotacaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoBolsa this[int index]
		{
			get
			{
				return base[index] as CotacaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoBolsa);
		}
	}



	[Serializable]
	abstract public class esCotacaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoBolsa()
		{

		}

		public esCotacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.String cdAtivoBolsa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, cdAtivoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(data, cdAtivoBolsa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.String cdAtivoBolsa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data, query.CdAtivoBolsa == cdAtivoBolsa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.String cdAtivoBolsa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, cdAtivoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(data, cdAtivoBolsa);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.String cdAtivoBolsa)
		{
			esCotacaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.CdAtivoBolsa == cdAtivoBolsa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.String cdAtivoBolsa)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("CdAtivoBolsa",cdAtivoBolsa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "PUMedio": this.str.PUMedio = (string)value; break;							
						case "PUFechamento": this.str.PUFechamento = (string)value; break;							
						case "PUAbertura": this.str.PUAbertura = (string)value; break;							
						case "PUGerencial": this.str.PUGerencial = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "PUMedio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMedio = (System.Decimal?)value;
							break;
						
						case "PUFechamento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUFechamento = (System.Decimal?)value;
							break;
						
						case "PUAbertura":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUAbertura = (System.Decimal?)value;
							break;
						
						case "PUGerencial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUGerencial = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoBolsa.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(CotacaoBolsaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoBolsaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(CotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(CotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBolsa.PUMedio
		/// </summary>
		virtual public System.Decimal? PUMedio
		{
			get
			{
				return base.GetSystemDecimal(CotacaoBolsaMetadata.ColumnNames.PUMedio);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoBolsaMetadata.ColumnNames.PUMedio, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBolsa.PUFechamento
		/// </summary>
		virtual public System.Decimal? PUFechamento
		{
			get
			{
				return base.GetSystemDecimal(CotacaoBolsaMetadata.ColumnNames.PUFechamento);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoBolsaMetadata.ColumnNames.PUFechamento, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBolsa.PUAbertura
		/// </summary>
		virtual public System.Decimal? PUAbertura
		{
			get
			{
				return base.GetSystemDecimal(CotacaoBolsaMetadata.ColumnNames.PUAbertura);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoBolsaMetadata.ColumnNames.PUAbertura, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoBolsa.PUGerencial
		/// </summary>
		virtual public System.Decimal? PUGerencial
		{
			get
			{
				return base.GetSystemDecimal(CotacaoBolsaMetadata.ColumnNames.PUGerencial);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoBolsaMetadata.ColumnNames.PUGerencial, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String PUMedio
			{
				get
				{
					System.Decimal? data = entity.PUMedio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMedio = null;
					else entity.PUMedio = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUFechamento
			{
				get
				{
					System.Decimal? data = entity.PUFechamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUFechamento = null;
					else entity.PUFechamento = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUAbertura
			{
				get
				{
					System.Decimal? data = entity.PUAbertura;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUAbertura = null;
					else entity.PUAbertura = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUGerencial
			{
				get
				{
					System.Decimal? data = entity.PUGerencial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUGerencial = null;
					else entity.PUGerencial = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoBolsa : esCotacaoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_CotacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, CotacaoBolsaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, CotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem PUMedio
		{
			get
			{
				return new esQueryItem(this, CotacaoBolsaMetadata.ColumnNames.PUMedio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUFechamento
		{
			get
			{
				return new esQueryItem(this, CotacaoBolsaMetadata.ColumnNames.PUFechamento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUAbertura
		{
			get
			{
				return new esQueryItem(this, CotacaoBolsaMetadata.ColumnNames.PUAbertura, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUGerencial
		{
			get
			{
				return new esQueryItem(this, CotacaoBolsaMetadata.ColumnNames.PUGerencial, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoBolsaCollection")]
	public partial class CotacaoBolsaCollection : esCotacaoBolsaCollection, IEnumerable<CotacaoBolsa>
	{
		public CotacaoBolsaCollection()
		{

		}
		
		public static implicit operator List<CotacaoBolsa>(CotacaoBolsaCollection coll)
		{
			List<CotacaoBolsa> list = new List<CotacaoBolsa>();
			
			foreach (CotacaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoBolsa AddNew()
		{
			CotacaoBolsa entity = base.AddNewEntity() as CotacaoBolsa;
			
			return entity;
		}

		public CotacaoBolsa FindByPrimaryKey(System.DateTime data, System.String cdAtivoBolsa)
		{
			return base.FindByPrimaryKey(data, cdAtivoBolsa) as CotacaoBolsa;
		}


		#region IEnumerable<CotacaoBolsa> Members

		IEnumerator<CotacaoBolsa> IEnumerable<CotacaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoBolsa;
			}
		}

		#endregion
		
		private CotacaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class CotacaoBolsa : esCotacaoBolsa
	{
		public CotacaoBolsa()
		{

		}
	
		public CotacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoBolsaQuery query;
	}



	[Serializable]
	public partial class CotacaoBolsaQuery : esCotacaoBolsaQuery
	{
		public CotacaoBolsaQuery()
		{

		}		
		
		public CotacaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoBolsaMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoBolsaMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBolsaMetadata.ColumnNames.PUMedio, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoBolsaMetadata.PropertyNames.PUMedio;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBolsaMetadata.ColumnNames.PUFechamento, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoBolsaMetadata.PropertyNames.PUFechamento;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBolsaMetadata.ColumnNames.PUAbertura, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoBolsaMetadata.PropertyNames.PUAbertura;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoBolsaMetadata.ColumnNames.PUGerencial, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoBolsaMetadata.PropertyNames.PUGerencial;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string PUMedio = "PUMedio";
			 public const string PUFechamento = "PUFechamento";
			 public const string PUAbertura = "PUAbertura";
			 public const string PUGerencial = "PUGerencial";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string PUMedio = "PUMedio";
			 public const string PUFechamento = "PUFechamento";
			 public const string PUAbertura = "PUAbertura";
			 public const string PUGerencial = "PUGerencial";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoBolsaMetadata))
			{
				if(CotacaoBolsaMetadata.mapDelegates == null)
				{
					CotacaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoBolsaMetadata.meta == null)
				{
					CotacaoBolsaMetadata.meta = new CotacaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PUMedio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUFechamento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUAbertura", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUGerencial", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoBolsa";
				meta.Destination = "CotacaoBolsa";
				
				meta.spInsert = "proc_CotacaoBolsaInsert";				
				meta.spUpdate = "proc_CotacaoBolsaUpdate";		
				meta.spDelete = "proc_CotacaoBolsaDelete";
				meta.spLoadAll = "proc_CotacaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
