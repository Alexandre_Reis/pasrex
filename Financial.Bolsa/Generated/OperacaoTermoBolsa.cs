/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		

		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esOperacaoTermoBolsaCollection : esEntityCollection
	{
		public esOperacaoTermoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoTermoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoTermoBolsaQuery);
		}
		#endregion
		
		virtual public OperacaoTermoBolsa DetachEntity(OperacaoTermoBolsa entity)
		{
			return base.DetachEntity(entity) as OperacaoTermoBolsa;
		}
		
		virtual public OperacaoTermoBolsa AttachEntity(OperacaoTermoBolsa entity)
		{
			return base.AttachEntity(entity) as OperacaoTermoBolsa;
		}
		
		virtual public void Combine(OperacaoTermoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoTermoBolsa this[int index]
		{
			get
			{
				return base[index] as OperacaoTermoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoTermoBolsa);
		}
	}



	[Serializable]
	abstract public class esOperacaoTermoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoTermoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoTermoBolsa()
		{

		}

		public esOperacaoTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOperacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOperacaoTermoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdOperacao == idOperacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOperacaoTermoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoTermoBolsa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoTermoBolsaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoTermoBolsaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoTermoBolsa.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(OperacaoTermoBolsaMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoTermoBolsaMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoTermoBolsa.NumeroContrato
		/// </summary>
		virtual public System.String NumeroContrato
		{
			get
			{
				return base.GetSystemString(OperacaoTermoBolsaMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemString(OperacaoTermoBolsaMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoTermoBolsa.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(OperacaoTermoBolsaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				base.SetSystemInt16(OperacaoTermoBolsaMetadata.ColumnNames.IdIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoTermoBolsa.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(OperacaoTermoBolsaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoTermoBolsaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoTermoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.String data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToString(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
			

			private esOperacaoTermoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoTermoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoTermoBolsa : esOperacaoTermoBolsa
	{

		#region UpToOperacaoBolsa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - OperacaoBolsa_OperacaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoBolsa UpToOperacaoBolsa
		{
			get
			{
				if(this._UpToOperacaoBolsa == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoBolsa = new OperacaoBolsa();
					this._UpToOperacaoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoBolsa", this._UpToOperacaoBolsa);
					this._UpToOperacaoBolsa.Query.Where(this._UpToOperacaoBolsa.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoBolsa.Query.Load();
				}

				return this._UpToOperacaoBolsa;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToOperacaoBolsa");

				if(value == null)
				{
					this._UpToOperacaoBolsa = null;
				}
				else
				{
					this._UpToOperacaoBolsa = value;
					this.SetPreSave("UpToOperacaoBolsa", this._UpToOperacaoBolsa);
				}
				
				
			} 
		}

		private OperacaoBolsa _UpToOperacaoBolsa;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoBolsa != null)
			{
				this.IdOperacao = this._UpToOperacaoBolsa.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOperacaoTermoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoTermoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoTermoBolsaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, OperacaoTermoBolsaMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, OperacaoTermoBolsaMetadata.ColumnNames.NumeroContrato, esSystemType.String);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, OperacaoTermoBolsaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, OperacaoTermoBolsaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoTermoBolsaCollection")]
	public partial class OperacaoTermoBolsaCollection : esOperacaoTermoBolsaCollection, IEnumerable<OperacaoTermoBolsa>
	{
		public OperacaoTermoBolsaCollection()
		{

		}
		
		public static implicit operator List<OperacaoTermoBolsa>(OperacaoTermoBolsaCollection coll)
		{
			List<OperacaoTermoBolsa> list = new List<OperacaoTermoBolsa>();
			
			foreach (OperacaoTermoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoTermoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoTermoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoTermoBolsa AddNew()
		{
			OperacaoTermoBolsa entity = base.AddNewEntity() as OperacaoTermoBolsa;
			
			return entity;
		}

		public OperacaoTermoBolsa FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OperacaoTermoBolsa;
		}


		#region IEnumerable<OperacaoTermoBolsa> Members

		IEnumerator<OperacaoTermoBolsa> IEnumerable<OperacaoTermoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoTermoBolsa;
			}
		}

		#endregion
		
		private OperacaoTermoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoTermoBolsa' table
	/// </summary>

	[Serializable]
	public partial class OperacaoTermoBolsa : esOperacaoTermoBolsa
	{
		public OperacaoTermoBolsa()
		{

		}
	
		public OperacaoTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoTermoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoTermoBolsaQuery query;
	}



	[Serializable]
	public partial class OperacaoTermoBolsaQuery : esOperacaoTermoBolsaQuery
	{
		public OperacaoTermoBolsaQuery()
		{

		}		
		
		public OperacaoTermoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoTermoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoTermoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoTermoBolsaMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoTermoBolsaMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoTermoBolsaMetadata.ColumnNames.Taxa, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoTermoBolsaMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoTermoBolsaMetadata.ColumnNames.NumeroContrato, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoTermoBolsaMetadata.PropertyNames.NumeroContrato;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoTermoBolsaMetadata.ColumnNames.IdIndice, 3, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoTermoBolsaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoTermoBolsaMetadata.ColumnNames.DataVencimento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoTermoBolsaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoTermoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string Taxa = "Taxa";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string IdIndice = "IdIndice";
			 public const string DataVencimento = "DataVencimento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string Taxa = "Taxa";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string IdIndice = "IdIndice";
			 public const string DataVencimento = "DataVencimento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoTermoBolsaMetadata))
			{
				if(OperacaoTermoBolsaMetadata.mapDelegates == null)
				{
					OperacaoTermoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoTermoBolsaMetadata.meta == null)
				{
					OperacaoTermoBolsaMetadata.meta = new OperacaoTermoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "OperacaoTermoBolsa";
				meta.Destination = "OperacaoTermoBolsa";
				
				meta.spInsert = "proc_OperacaoTermoBolsaInsert";				
				meta.spUpdate = "proc_OperacaoTermoBolsaUpdate";		
				meta.spDelete = "proc_OperacaoTermoBolsaDelete";
				meta.spLoadAll = "proc_OperacaoTermoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoTermoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoTermoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
