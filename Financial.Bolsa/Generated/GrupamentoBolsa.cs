/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		


		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esGrupamentoBolsaCollection : esEntityCollection
	{
		public esGrupamentoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GrupamentoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esGrupamentoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGrupamentoBolsaQuery);
		}
		#endregion
		
		virtual public GrupamentoBolsa DetachEntity(GrupamentoBolsa entity)
		{
			return base.DetachEntity(entity) as GrupamentoBolsa;
		}
		
		virtual public GrupamentoBolsa AttachEntity(GrupamentoBolsa entity)
		{
			return base.AttachEntity(entity) as GrupamentoBolsa;
		}
		
		virtual public void Combine(GrupamentoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GrupamentoBolsa this[int index]
		{
			get
			{
				return base[index] as GrupamentoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GrupamentoBolsa);
		}
	}



	[Serializable]
	abstract public class esGrupamentoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGrupamentoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esGrupamentoBolsa()
		{

		}

		public esGrupamentoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupamento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupamento);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupamento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idGrupamento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esGrupamentoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdGrupamento == idGrupamento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupamento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupamento);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupamento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupamento)
		{
			esGrupamentoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupamento == idGrupamento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupamento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupamento",idGrupamento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupamento": this.str.IdGrupamento = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataEx": this.str.DataEx = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "FatorQuantidade": this.str.FatorQuantidade = (string)value; break;							
						case "FatorPU": this.str.FatorPU = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "TipoGrupamento": this.str.TipoGrupamento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupamento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupamento = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataEx":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEx = (System.DateTime?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "FatorQuantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FatorQuantidade = (System.Decimal?)value;
							break;
						
						case "FatorPU":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FatorPU = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "TipoGrupamento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoGrupamento = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GrupamentoBolsa.IdGrupamento
		/// </summary>
		virtual public System.Int32? IdGrupamento
		{
			get
			{
				return base.GetSystemInt32(GrupamentoBolsaMetadata.ColumnNames.IdGrupamento);
			}
			
			set
			{
				base.SetSystemInt32(GrupamentoBolsaMetadata.ColumnNames.IdGrupamento, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupamentoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(GrupamentoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(GrupamentoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to GrupamentoBolsa.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(GrupamentoBolsaMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(GrupamentoBolsaMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupamentoBolsa.DataEx
		/// </summary>
		virtual public System.DateTime? DataEx
		{
			get
			{
				return base.GetSystemDateTime(GrupamentoBolsaMetadata.ColumnNames.DataEx);
			}
			
			set
			{
				base.SetSystemDateTime(GrupamentoBolsaMetadata.ColumnNames.DataEx, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupamentoBolsa.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(GrupamentoBolsaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(GrupamentoBolsaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupamentoBolsa.FatorQuantidade
		/// </summary>
		virtual public System.Decimal? FatorQuantidade
		{
			get
			{
				return base.GetSystemDecimal(GrupamentoBolsaMetadata.ColumnNames.FatorQuantidade);
			}
			
			set
			{
				base.SetSystemDecimal(GrupamentoBolsaMetadata.ColumnNames.FatorQuantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupamentoBolsa.FatorPU
		/// </summary>
		virtual public System.Decimal? FatorPU
		{
			get
			{
				return base.GetSystemDecimal(GrupamentoBolsaMetadata.ColumnNames.FatorPU);
			}
			
			set
			{
				base.SetSystemDecimal(GrupamentoBolsaMetadata.ColumnNames.FatorPU, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupamentoBolsa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(GrupamentoBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(GrupamentoBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupamentoBolsa.TipoGrupamento
		/// </summary>
		virtual public System.Byte? TipoGrupamento
		{
			get
			{
				return base.GetSystemByte(GrupamentoBolsaMetadata.ColumnNames.TipoGrupamento);
			}
			
			set
			{
				base.SetSystemByte(GrupamentoBolsaMetadata.ColumnNames.TipoGrupamento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGrupamentoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupamento
			{
				get
				{
					System.Int32? data = entity.IdGrupamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupamento = null;
					else entity.IdGrupamento = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEx
			{
				get
				{
					System.DateTime? data = entity.DataEx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEx = null;
					else entity.DataEx = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String FatorQuantidade
			{
				get
				{
					System.Decimal? data = entity.FatorQuantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatorQuantidade = null;
					else entity.FatorQuantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String FatorPU
			{
				get
				{
					System.Decimal? data = entity.FatorPU;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatorPU = null;
					else entity.FatorPU = Convert.ToDecimal(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String TipoGrupamento
			{
				get
				{
					System.Byte? data = entity.TipoGrupamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoGrupamento = null;
					else entity.TipoGrupamento = Convert.ToByte(value);
				}
			}
			

			private esGrupamentoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGrupamentoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGrupamentoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GrupamentoBolsa : esGrupamentoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_Grupamento_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGrupamentoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GrupamentoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupamento
		{
			get
			{
				return new esQueryItem(this, GrupamentoBolsaMetadata.ColumnNames.IdGrupamento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, GrupamentoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, GrupamentoBolsaMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataEx
		{
			get
			{
				return new esQueryItem(this, GrupamentoBolsaMetadata.ColumnNames.DataEx, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, GrupamentoBolsaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem FatorQuantidade
		{
			get
			{
				return new esQueryItem(this, GrupamentoBolsaMetadata.ColumnNames.FatorQuantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem FatorPU
		{
			get
			{
				return new esQueryItem(this, GrupamentoBolsaMetadata.ColumnNames.FatorPU, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, GrupamentoBolsaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoGrupamento
		{
			get
			{
				return new esQueryItem(this, GrupamentoBolsaMetadata.ColumnNames.TipoGrupamento, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GrupamentoBolsaCollection")]
	public partial class GrupamentoBolsaCollection : esGrupamentoBolsaCollection, IEnumerable<GrupamentoBolsa>
	{
		public GrupamentoBolsaCollection()
		{

		}
		
		public static implicit operator List<GrupamentoBolsa>(GrupamentoBolsaCollection coll)
		{
			List<GrupamentoBolsa> list = new List<GrupamentoBolsa>();
			
			foreach (GrupamentoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GrupamentoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupamentoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GrupamentoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GrupamentoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GrupamentoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupamentoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GrupamentoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GrupamentoBolsa AddNew()
		{
			GrupamentoBolsa entity = base.AddNewEntity() as GrupamentoBolsa;
			
			return entity;
		}

		public GrupamentoBolsa FindByPrimaryKey(System.Int32 idGrupamento)
		{
			return base.FindByPrimaryKey(idGrupamento) as GrupamentoBolsa;
		}


		#region IEnumerable<GrupamentoBolsa> Members

		IEnumerator<GrupamentoBolsa> IEnumerable<GrupamentoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GrupamentoBolsa;
			}
		}

		#endregion
		
		private GrupamentoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GrupamentoBolsa' table
	/// </summary>

	[Serializable]
	public partial class GrupamentoBolsa : esGrupamentoBolsa
	{
		public GrupamentoBolsa()
		{

		}
	
		public GrupamentoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GrupamentoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esGrupamentoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupamentoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GrupamentoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupamentoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GrupamentoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GrupamentoBolsaQuery query;
	}



	[Serializable]
	public partial class GrupamentoBolsaQuery : esGrupamentoBolsaQuery
	{
		public GrupamentoBolsaQuery()
		{

		}		
		
		public GrupamentoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GrupamentoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GrupamentoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GrupamentoBolsaMetadata.ColumnNames.IdGrupamento, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupamentoBolsaMetadata.PropertyNames.IdGrupamento;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupamentoBolsaMetadata.ColumnNames.CdAtivoBolsa, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = GrupamentoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupamentoBolsaMetadata.ColumnNames.DataLancamento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GrupamentoBolsaMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupamentoBolsaMetadata.ColumnNames.DataEx, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GrupamentoBolsaMetadata.PropertyNames.DataEx;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupamentoBolsaMetadata.ColumnNames.DataReferencia, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = GrupamentoBolsaMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupamentoBolsaMetadata.ColumnNames.FatorQuantidade, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GrupamentoBolsaMetadata.PropertyNames.FatorQuantidade;	
			c.NumericPrecision = 20;
			c.NumericScale = 11;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupamentoBolsaMetadata.ColumnNames.FatorPU, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = GrupamentoBolsaMetadata.PropertyNames.FatorPU;	
			c.NumericPrecision = 20;
			c.NumericScale = 15;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupamentoBolsaMetadata.ColumnNames.Fonte, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GrupamentoBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupamentoBolsaMetadata.ColumnNames.TipoGrupamento, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = GrupamentoBolsaMetadata.PropertyNames.TipoGrupamento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GrupamentoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupamento = "IdGrupamento";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataReferencia = "DataReferencia";
			 public const string FatorQuantidade = "FatorQuantidade";
			 public const string FatorPU = "FatorPU";
			 public const string Fonte = "Fonte";
			 public const string TipoGrupamento = "TipoGrupamento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupamento = "IdGrupamento";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataReferencia = "DataReferencia";
			 public const string FatorQuantidade = "FatorQuantidade";
			 public const string FatorPU = "FatorPU";
			 public const string Fonte = "Fonte";
			 public const string TipoGrupamento = "TipoGrupamento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GrupamentoBolsaMetadata))
			{
				if(GrupamentoBolsaMetadata.mapDelegates == null)
				{
					GrupamentoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GrupamentoBolsaMetadata.meta == null)
				{
					GrupamentoBolsaMetadata.meta = new GrupamentoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupamento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEx", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("FatorQuantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("FatorPU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoGrupamento", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "GrupamentoBolsa";
				meta.Destination = "GrupamentoBolsa";
				
				meta.spInsert = "proc_GrupamentoBolsaInsert";				
				meta.spUpdate = "proc_GrupamentoBolsaUpdate";		
				meta.spDelete = "proc_GrupamentoBolsaDelete";
				meta.spLoadAll = "proc_GrupamentoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_GrupamentoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GrupamentoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
