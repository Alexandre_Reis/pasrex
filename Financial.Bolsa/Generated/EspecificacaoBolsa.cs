/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esEspecificacaoBolsaCollection : esEntityCollection
	{
		public esEspecificacaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EspecificacaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esEspecificacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEspecificacaoBolsaQuery);
		}
		#endregion
		
		virtual public EspecificacaoBolsa DetachEntity(EspecificacaoBolsa entity)
		{
			return base.DetachEntity(entity) as EspecificacaoBolsa;
		}
		
		virtual public EspecificacaoBolsa AttachEntity(EspecificacaoBolsa entity)
		{
			return base.AttachEntity(entity) as EspecificacaoBolsa;
		}
		
		virtual public void Combine(EspecificacaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EspecificacaoBolsa this[int index]
		{
			get
			{
				return base[index] as EspecificacaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EspecificacaoBolsa);
		}
	}



	[Serializable]
	abstract public class esEspecificacaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEspecificacaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esEspecificacaoBolsa()
		{

		}

		public esEspecificacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String especificacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(especificacao);
			else
				return LoadByPrimaryKeyStoredProcedure(especificacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String especificacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEspecificacaoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Especificacao == especificacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String especificacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(especificacao);
			else
				return LoadByPrimaryKeyStoredProcedure(especificacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.String especificacao)
		{
			esEspecificacaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.Especificacao == especificacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String especificacao)
		{
			esParameters parms = new esParameters();
			parms.Add("Especificacao",especificacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Especificacao": this.str.Especificacao = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EspecificacaoBolsa.Especificacao
		/// </summary>
		virtual public System.String Especificacao
		{
			get
			{
				return base.GetSystemString(EspecificacaoBolsaMetadata.ColumnNames.Especificacao);
			}
			
			set
			{
				base.SetSystemString(EspecificacaoBolsaMetadata.ColumnNames.Especificacao, value);
			}
		}
		
		/// <summary>
		/// Maps to EspecificacaoBolsa.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(EspecificacaoBolsaMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(EspecificacaoBolsaMetadata.ColumnNames.Tipo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEspecificacaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Especificacao
			{
				get
				{
					System.String data = entity.Especificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Especificacao = null;
					else entity.Especificacao = Convert.ToString(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
			

			private esEspecificacaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEspecificacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEspecificacaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EspecificacaoBolsa : esEspecificacaoBolsa
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEspecificacaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EspecificacaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem Especificacao
		{
			get
			{
				return new esQueryItem(this, EspecificacaoBolsaMetadata.ColumnNames.Especificacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, EspecificacaoBolsaMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EspecificacaoBolsaCollection")]
	public partial class EspecificacaoBolsaCollection : esEspecificacaoBolsaCollection, IEnumerable<EspecificacaoBolsa>
	{
		public EspecificacaoBolsaCollection()
		{

		}
		
		public static implicit operator List<EspecificacaoBolsa>(EspecificacaoBolsaCollection coll)
		{
			List<EspecificacaoBolsa> list = new List<EspecificacaoBolsa>();
			
			foreach (EspecificacaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EspecificacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EspecificacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EspecificacaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EspecificacaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EspecificacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EspecificacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EspecificacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EspecificacaoBolsa AddNew()
		{
			EspecificacaoBolsa entity = base.AddNewEntity() as EspecificacaoBolsa;
			
			return entity;
		}

		public EspecificacaoBolsa FindByPrimaryKey(System.String especificacao)
		{
			return base.FindByPrimaryKey(especificacao) as EspecificacaoBolsa;
		}


		#region IEnumerable<EspecificacaoBolsa> Members

		IEnumerator<EspecificacaoBolsa> IEnumerable<EspecificacaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EspecificacaoBolsa;
			}
		}

		#endregion
		
		private EspecificacaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EspecificacaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class EspecificacaoBolsa : esEspecificacaoBolsa
	{
		public EspecificacaoBolsa()
		{

		}
	
		public EspecificacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EspecificacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esEspecificacaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EspecificacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EspecificacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EspecificacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EspecificacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EspecificacaoBolsaQuery query;
	}



	[Serializable]
	public partial class EspecificacaoBolsaQuery : esEspecificacaoBolsaQuery
	{
		public EspecificacaoBolsaQuery()
		{

		}		
		
		public EspecificacaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EspecificacaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EspecificacaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EspecificacaoBolsaMetadata.ColumnNames.Especificacao, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = EspecificacaoBolsaMetadata.PropertyNames.Especificacao;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EspecificacaoBolsaMetadata.ColumnNames.Tipo, 1, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EspecificacaoBolsaMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EspecificacaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Especificacao = "Especificacao";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Especificacao = "Especificacao";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EspecificacaoBolsaMetadata))
			{
				if(EspecificacaoBolsaMetadata.mapDelegates == null)
				{
					EspecificacaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EspecificacaoBolsaMetadata.meta == null)
				{
					EspecificacaoBolsaMetadata.meta = new EspecificacaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Especificacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EspecificacaoBolsa";
				meta.Destination = "EspecificacaoBolsa";
				
				meta.spInsert = "proc_EspecificacaoBolsaInsert";				
				meta.spUpdate = "proc_EspecificacaoBolsaUpdate";		
				meta.spDelete = "proc_EspecificacaoBolsaDelete";
				meta.spLoadAll = "proc_EspecificacaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_EspecificacaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EspecificacaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
