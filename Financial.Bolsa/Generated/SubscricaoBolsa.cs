/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		


























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esSubscricaoBolsaCollection : esEntityCollection
	{
		public esSubscricaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SubscricaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esSubscricaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSubscricaoBolsaQuery);
		}
		#endregion
		
		virtual public SubscricaoBolsa DetachEntity(SubscricaoBolsa entity)
		{
			return base.DetachEntity(entity) as SubscricaoBolsa;
		}
		
		virtual public SubscricaoBolsa AttachEntity(SubscricaoBolsa entity)
		{
			return base.AttachEntity(entity) as SubscricaoBolsa;
		}
		
		virtual public void Combine(SubscricaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SubscricaoBolsa this[int index]
		{
			get
			{
				return base[index] as SubscricaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SubscricaoBolsa);
		}
	}



	[Serializable]
	abstract public class esSubscricaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSubscricaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esSubscricaoBolsa()
		{

		}

		public esSubscricaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idSubscricao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSubscricao);
			else
				return LoadByPrimaryKeyStoredProcedure(idSubscricao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idSubscricao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esSubscricaoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdSubscricao == idSubscricao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idSubscricao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSubscricao);
			else
				return LoadByPrimaryKeyStoredProcedure(idSubscricao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idSubscricao)
		{
			esSubscricaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdSubscricao == idSubscricao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idSubscricao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdSubscricao",idSubscricao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdSubscricao": this.str.IdSubscricao = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataEx": this.str.DataEx = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "PrecoSubscricao": this.str.PrecoSubscricao = (string)value; break;							
						case "Fator": this.str.Fator = (string)value; break;							
						case "CdAtivoBolsaDireito": this.str.CdAtivoBolsaDireito = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "DataFimNegociacao": this.str.DataFimNegociacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdSubscricao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSubscricao = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataEx":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEx = (System.DateTime?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "PrecoSubscricao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PrecoSubscricao = (System.Decimal?)value;
							break;
						
						case "Fator":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Fator = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Fonte = (System.Int32?)value;
							break;
						
						case "DataFimNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimNegociacao = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SubscricaoBolsa.IdSubscricao
		/// </summary>
		virtual public System.Int32? IdSubscricao
		{
			get
			{
				return base.GetSystemInt32(SubscricaoBolsaMetadata.ColumnNames.IdSubscricao);
			}
			
			set
			{
				base.SetSystemInt32(SubscricaoBolsaMetadata.ColumnNames.IdSubscricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SubscricaoBolsa.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(SubscricaoBolsaMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(SubscricaoBolsaMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to SubscricaoBolsa.DataEx
		/// </summary>
		virtual public System.DateTime? DataEx
		{
			get
			{
				return base.GetSystemDateTime(SubscricaoBolsaMetadata.ColumnNames.DataEx);
			}
			
			set
			{
				base.SetSystemDateTime(SubscricaoBolsaMetadata.ColumnNames.DataEx, value);
			}
		}
		
		/// <summary>
		/// Maps to SubscricaoBolsa.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(SubscricaoBolsaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(SubscricaoBolsaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to SubscricaoBolsa.PrecoSubscricao
		/// </summary>
		virtual public System.Decimal? PrecoSubscricao
		{
			get
			{
				return base.GetSystemDecimal(SubscricaoBolsaMetadata.ColumnNames.PrecoSubscricao);
			}
			
			set
			{
				base.SetSystemDecimal(SubscricaoBolsaMetadata.ColumnNames.PrecoSubscricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SubscricaoBolsa.Fator
		/// </summary>
		virtual public System.Decimal? Fator
		{
			get
			{
				return base.GetSystemDecimal(SubscricaoBolsaMetadata.ColumnNames.Fator);
			}
			
			set
			{
				base.SetSystemDecimal(SubscricaoBolsaMetadata.ColumnNames.Fator, value);
			}
		}
		
		/// <summary>
		/// Maps to SubscricaoBolsa.CdAtivoBolsaDireito
		/// </summary>
		virtual public System.String CdAtivoBolsaDireito
		{
			get
			{
				return base.GetSystemString(SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsaDireito);
			}
			
			set
			{
				if(base.SetSystemString(SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsaDireito, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsaDireito = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SubscricaoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SubscricaoBolsa.Fonte
		/// </summary>
		virtual public System.Int32? Fonte
		{
			get
			{
				return base.GetSystemInt32(SubscricaoBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemInt32(SubscricaoBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to SubscricaoBolsa.DataFimNegociacao
		/// </summary>
		virtual public System.DateTime? DataFimNegociacao
		{
			get
			{
				return base.GetSystemDateTime(SubscricaoBolsaMetadata.ColumnNames.DataFimNegociacao);
			}
			
			set
			{
				base.SetSystemDateTime(SubscricaoBolsaMetadata.ColumnNames.DataFimNegociacao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsaDireito;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSubscricaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdSubscricao
			{
				get
				{
					System.Int32? data = entity.IdSubscricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSubscricao = null;
					else entity.IdSubscricao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEx
			{
				get
				{
					System.DateTime? data = entity.DataEx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEx = null;
					else entity.DataEx = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String PrecoSubscricao
			{
				get
				{
					System.Decimal? data = entity.PrecoSubscricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrecoSubscricao = null;
					else entity.PrecoSubscricao = Convert.ToDecimal(value);
				}
			}
				
			public System.String Fator
			{
				get
				{
					System.Decimal? data = entity.Fator;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fator = null;
					else entity.Fator = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAtivoBolsaDireito
			{
				get
				{
					System.String data = entity.CdAtivoBolsaDireito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaDireito = null;
					else entity.CdAtivoBolsaDireito = Convert.ToString(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Int32? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToInt32(value);
				}
			}
				
			public System.String DataFimNegociacao
			{
				get
				{
					System.DateTime? data = entity.DataFimNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimNegociacao = null;
					else entity.DataFimNegociacao = Convert.ToDateTime(value);
				}
			}
			

			private esSubscricaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSubscricaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSubscricaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SubscricaoBolsa : esSubscricaoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_SubscricaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsaDireito - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_SubscricaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsaDireito
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsaDireito == null
					&& CdAtivoBolsaDireito != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsaDireito = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsaDireito.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaDireito", this._UpToAtivoBolsaByCdAtivoBolsaDireito);
					this._UpToAtivoBolsaByCdAtivoBolsaDireito.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsaDireito.Query.CdAtivoBolsa == this.CdAtivoBolsaDireito);
					this._UpToAtivoBolsaByCdAtivoBolsaDireito.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsaDireito;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsaDireito");
				

				if(value == null)
				{
					this.CdAtivoBolsaDireito = null;
					this._UpToAtivoBolsaByCdAtivoBolsaDireito = null;
				}
				else
				{
					this.CdAtivoBolsaDireito = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsaDireito = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaDireito", this._UpToAtivoBolsaByCdAtivoBolsaDireito);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSubscricaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SubscricaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdSubscricao
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.IdSubscricao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataEx
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.DataEx, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PrecoSubscricao
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.PrecoSubscricao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Fator
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.Fator, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAtivoBolsaDireito
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsaDireito, esSystemType.String);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.Fonte, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataFimNegociacao
		{
			get
			{
				return new esQueryItem(this, SubscricaoBolsaMetadata.ColumnNames.DataFimNegociacao, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SubscricaoBolsaCollection")]
	public partial class SubscricaoBolsaCollection : esSubscricaoBolsaCollection, IEnumerable<SubscricaoBolsa>
	{
		public SubscricaoBolsaCollection()
		{

		}
		
		public static implicit operator List<SubscricaoBolsa>(SubscricaoBolsaCollection coll)
		{
			List<SubscricaoBolsa> list = new List<SubscricaoBolsa>();
			
			foreach (SubscricaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SubscricaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SubscricaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SubscricaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SubscricaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SubscricaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SubscricaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SubscricaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SubscricaoBolsa AddNew()
		{
			SubscricaoBolsa entity = base.AddNewEntity() as SubscricaoBolsa;
			
			return entity;
		}

		public SubscricaoBolsa FindByPrimaryKey(System.Int32 idSubscricao)
		{
			return base.FindByPrimaryKey(idSubscricao) as SubscricaoBolsa;
		}


		#region IEnumerable<SubscricaoBolsa> Members

		IEnumerator<SubscricaoBolsa> IEnumerable<SubscricaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SubscricaoBolsa;
			}
		}

		#endregion
		
		private SubscricaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SubscricaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class SubscricaoBolsa : esSubscricaoBolsa
	{
		public SubscricaoBolsa()
		{

		}
	
		public SubscricaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SubscricaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esSubscricaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SubscricaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SubscricaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SubscricaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SubscricaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SubscricaoBolsaQuery query;
	}



	[Serializable]
	public partial class SubscricaoBolsaQuery : esSubscricaoBolsaQuery
	{
		public SubscricaoBolsaQuery()
		{

		}		
		
		public SubscricaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SubscricaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SubscricaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.IdSubscricao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.IdSubscricao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.DataLancamento, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.DataEx, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.DataEx;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.DataReferencia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.PrecoSubscricao, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.PrecoSubscricao;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.Fator, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.Fator;	
			c.NumericPrecision = 20;
			c.NumericScale = 11;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsaDireito, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.CdAtivoBolsaDireito;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsa, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.Fonte, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SubscricaoBolsaMetadata.ColumnNames.DataFimNegociacao, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = SubscricaoBolsaMetadata.PropertyNames.DataFimNegociacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SubscricaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdSubscricao = "IdSubscricao";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataReferencia = "DataReferencia";
			 public const string PrecoSubscricao = "PrecoSubscricao";
			 public const string Fator = "Fator";
			 public const string CdAtivoBolsaDireito = "CdAtivoBolsaDireito";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Fonte = "Fonte";
			 public const string DataFimNegociacao = "DataFimNegociacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdSubscricao = "IdSubscricao";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataReferencia = "DataReferencia";
			 public const string PrecoSubscricao = "PrecoSubscricao";
			 public const string Fator = "Fator";
			 public const string CdAtivoBolsaDireito = "CdAtivoBolsaDireito";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Fonte = "Fonte";
			 public const string DataFimNegociacao = "DataFimNegociacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SubscricaoBolsaMetadata))
			{
				if(SubscricaoBolsaMetadata.mapDelegates == null)
				{
					SubscricaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SubscricaoBolsaMetadata.meta == null)
				{
					SubscricaoBolsaMetadata.meta = new SubscricaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdSubscricao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEx", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PrecoSubscricao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Fator", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CdAtivoBolsaDireito", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fonte", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataFimNegociacao", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "SubscricaoBolsa";
				meta.Destination = "SubscricaoBolsa";
				
				meta.spInsert = "proc_SubscricaoBolsaInsert";				
				meta.spUpdate = "proc_SubscricaoBolsaUpdate";		
				meta.spDelete = "proc_SubscricaoBolsaDelete";
				meta.spLoadAll = "proc_SubscricaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_SubscricaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SubscricaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
