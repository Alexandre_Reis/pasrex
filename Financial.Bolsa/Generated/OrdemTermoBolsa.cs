/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		

using Financial.Common;
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esOrdemTermoBolsaCollection : esEntityCollection
	{
		public esOrdemTermoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OrdemTermoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esOrdemTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOrdemTermoBolsaQuery);
		}
		#endregion
		
		virtual public OrdemTermoBolsa DetachEntity(OrdemTermoBolsa entity)
		{
			return base.DetachEntity(entity) as OrdemTermoBolsa;
		}
		
		virtual public OrdemTermoBolsa AttachEntity(OrdemTermoBolsa entity)
		{
			return base.AttachEntity(entity) as OrdemTermoBolsa;
		}
		
		virtual public void Combine(OrdemTermoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OrdemTermoBolsa this[int index]
		{
			get
			{
				return base[index] as OrdemTermoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OrdemTermoBolsa);
		}
	}



	[Serializable]
	abstract public class esOrdemTermoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOrdemTermoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esOrdemTermoBolsa()
		{

		}

		public esOrdemTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOrdem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOrdem);
			else
				return LoadByPrimaryKeyStoredProcedure(idOrdem);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOrdem)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOrdemTermoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdOrdem == idOrdem);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOrdem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOrdem);
			else
				return LoadByPrimaryKeyStoredProcedure(idOrdem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOrdem)
		{
			esOrdemTermoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOrdem == idOrdem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOrdem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOrdem",idOrdem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOrdem": this.str.IdOrdem = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOrdem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOrdem = (System.Int32?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OrdemTermoBolsa.IdOrdem
		/// </summary>
		virtual public System.Int32? IdOrdem
		{
			get
			{
				return base.GetSystemInt32(OrdemTermoBolsaMetadata.ColumnNames.IdOrdem);
			}
			
			set
			{
				base.SetSystemInt32(OrdemTermoBolsaMetadata.ColumnNames.IdOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemTermoBolsa.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(OrdemTermoBolsaMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemTermoBolsaMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemTermoBolsa.NumeroContrato
		/// </summary>
		virtual public System.String NumeroContrato
		{
			get
			{
				return base.GetSystemString(OrdemTermoBolsaMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemString(OrdemTermoBolsaMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemTermoBolsa.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(OrdemTermoBolsaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(OrdemTermoBolsaMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOrdemTermoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOrdem
			{
				get
				{
					System.Int32? data = entity.IdOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOrdem = null;
					else entity.IdOrdem = Convert.ToInt32(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.String data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToString(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
			

			private esOrdemTermoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOrdemTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOrdemTermoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OrdemTermoBolsa : esOrdemTermoBolsa
	{

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_OrdemTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

		#region UpToOrdemBolsa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - OrdemBolsa_OrdemTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemBolsa UpToOrdemBolsa
		{
			get
			{
				if(this._UpToOrdemBolsa == null
					&& IdOrdem != null					)
				{
					this._UpToOrdemBolsa = new OrdemBolsa();
					this._UpToOrdemBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOrdemBolsa", this._UpToOrdemBolsa);
					this._UpToOrdemBolsa.Query.Where(this._UpToOrdemBolsa.Query.IdOrdem == this.IdOrdem);
					this._UpToOrdemBolsa.Query.Load();
				}

				return this._UpToOrdemBolsa;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToOrdemBolsa");

				if(value == null)
				{
					this._UpToOrdemBolsa = null;
				}
				else
				{
					this._UpToOrdemBolsa = value;
					this.SetPreSave("UpToOrdemBolsa", this._UpToOrdemBolsa);
				}
				
				
			} 
		}

		private OrdemBolsa _UpToOrdemBolsa;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOrdemBolsa != null)
			{
				this.IdOrdem = this._UpToOrdemBolsa.IdOrdem;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOrdemTermoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OrdemTermoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOrdem
		{
			get
			{
				return new esQueryItem(this, OrdemTermoBolsaMetadata.ColumnNames.IdOrdem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, OrdemTermoBolsaMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, OrdemTermoBolsaMetadata.ColumnNames.NumeroContrato, esSystemType.String);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, OrdemTermoBolsaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OrdemTermoBolsaCollection")]
	public partial class OrdemTermoBolsaCollection : esOrdemTermoBolsaCollection, IEnumerable<OrdemTermoBolsa>
	{
		public OrdemTermoBolsaCollection()
		{

		}
		
		public static implicit operator List<OrdemTermoBolsa>(OrdemTermoBolsaCollection coll)
		{
			List<OrdemTermoBolsa> list = new List<OrdemTermoBolsa>();
			
			foreach (OrdemTermoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OrdemTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OrdemTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OrdemTermoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OrdemTermoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OrdemTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrdemTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OrdemTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OrdemTermoBolsa AddNew()
		{
			OrdemTermoBolsa entity = base.AddNewEntity() as OrdemTermoBolsa;
			
			return entity;
		}

		public OrdemTermoBolsa FindByPrimaryKey(System.Int32 idOrdem)
		{
			return base.FindByPrimaryKey(idOrdem) as OrdemTermoBolsa;
		}


		#region IEnumerable<OrdemTermoBolsa> Members

		IEnumerator<OrdemTermoBolsa> IEnumerable<OrdemTermoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OrdemTermoBolsa;
			}
		}

		#endregion
		
		private OrdemTermoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OrdemTermoBolsa' table
	/// </summary>

	[Serializable]
	public partial class OrdemTermoBolsa : esOrdemTermoBolsa
	{
		public OrdemTermoBolsa()
		{

		}
	
		public OrdemTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OrdemTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esOrdemTermoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OrdemTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OrdemTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrdemTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OrdemTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OrdemTermoBolsaQuery query;
	}



	[Serializable]
	public partial class OrdemTermoBolsaQuery : esOrdemTermoBolsaQuery
	{
		public OrdemTermoBolsaQuery()
		{

		}		
		
		public OrdemTermoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OrdemTermoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OrdemTermoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OrdemTermoBolsaMetadata.ColumnNames.IdOrdem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemTermoBolsaMetadata.PropertyNames.IdOrdem;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemTermoBolsaMetadata.ColumnNames.Taxa, 1, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemTermoBolsaMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemTermoBolsaMetadata.ColumnNames.NumeroContrato, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemTermoBolsaMetadata.PropertyNames.NumeroContrato;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemTermoBolsaMetadata.ColumnNames.IdIndice, 3, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OrdemTermoBolsaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OrdemTermoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOrdem = "IdOrdem";
			 public const string Taxa = "Taxa";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string IdIndice = "IdIndice";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOrdem = "IdOrdem";
			 public const string Taxa = "Taxa";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string IdIndice = "IdIndice";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OrdemTermoBolsaMetadata))
			{
				if(OrdemTermoBolsaMetadata.mapDelegates == null)
				{
					OrdemTermoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OrdemTermoBolsaMetadata.meta == null)
				{
					OrdemTermoBolsaMetadata.meta = new OrdemTermoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOrdem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));			
				
				
				
				meta.Source = "OrdemTermoBolsa";
				meta.Destination = "OrdemTermoBolsa";
				
				meta.spInsert = "proc_OrdemTermoBolsaInsert";				
				meta.spUpdate = "proc_OrdemTermoBolsaUpdate";		
				meta.spDelete = "proc_OrdemTermoBolsaDelete";
				meta.spLoadAll = "proc_OrdemTermoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_OrdemTermoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OrdemTermoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
