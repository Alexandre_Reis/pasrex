/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		


























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esTabelaCorretagemBolsaCollection : esEntityCollection
	{
		public esTabelaCorretagemBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaCorretagemBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaCorretagemBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaCorretagemBolsaQuery);
		}
		#endregion
		
		virtual public TabelaCorretagemBolsa DetachEntity(TabelaCorretagemBolsa entity)
		{
			return base.DetachEntity(entity) as TabelaCorretagemBolsa;
		}
		
		virtual public TabelaCorretagemBolsa AttachEntity(TabelaCorretagemBolsa entity)
		{
			return base.AttachEntity(entity) as TabelaCorretagemBolsa;
		}
		
		virtual public void Combine(TabelaCorretagemBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaCorretagemBolsa this[int index]
		{
			get
			{
				return base[index] as TabelaCorretagemBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaCorretagemBolsa);
		}
	}



	[Serializable]
	abstract public class esTabelaCorretagemBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaCorretagemBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaCorretagemBolsa()
		{

		}

		public esTabelaCorretagemBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaCorretagemBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esTabelaCorretagemBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "IdTemplate": this.str.IdTemplate = (string)value; break;							
						case "Faixa": this.str.Faixa = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "ValorAdicional": this.str.ValorAdicional = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "IdTemplate":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTemplate = (System.Int32?)value;
							break;
						
						case "Faixa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Faixa = (System.Decimal?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "ValorAdicional":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorAdicional = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaCorretagemBolsa.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaCorretagemBolsaMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCorretagemBolsaMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBolsa.IdTemplate
		/// </summary>
		virtual public System.Int32? IdTemplate
		{
			get
			{
				return base.GetSystemInt32(TabelaCorretagemBolsaMetadata.ColumnNames.IdTemplate);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaCorretagemBolsaMetadata.ColumnNames.IdTemplate, value))
				{
					this._UpToTemplateCorretagemBolsaByIdTemplate = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBolsa.Faixa
		/// </summary>
		virtual public System.Decimal? Faixa
		{
			get
			{
				return base.GetSystemDecimal(TabelaCorretagemBolsaMetadata.ColumnNames.Faixa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCorretagemBolsaMetadata.ColumnNames.Faixa, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBolsa.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(TabelaCorretagemBolsaMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCorretagemBolsaMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCorretagemBolsa.ValorAdicional
		/// </summary>
		virtual public System.Decimal? ValorAdicional
		{
			get
			{
				return base.GetSystemDecimal(TabelaCorretagemBolsaMetadata.ColumnNames.ValorAdicional);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCorretagemBolsaMetadata.ColumnNames.ValorAdicional, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TemplateCorretagemBolsa _UpToTemplateCorretagemBolsaByIdTemplate;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaCorretagemBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTemplate
			{
				get
				{
					System.Int32? data = entity.IdTemplate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTemplate = null;
					else entity.IdTemplate = Convert.ToInt32(value);
				}
			}
				
			public System.String Faixa
			{
				get
				{
					System.Decimal? data = entity.Faixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Faixa = null;
					else entity.Faixa = Convert.ToDecimal(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorAdicional
			{
				get
				{
					System.Decimal? data = entity.ValorAdicional;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorAdicional = null;
					else entity.ValorAdicional = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaCorretagemBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaCorretagemBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaCorretagemBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaCorretagemBolsa : esTabelaCorretagemBolsa
	{

				
		#region UpToTemplateCorretagemBolsaByIdTemplate - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TemplateCorretagemBolsa_TabelaCorretagemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public TemplateCorretagemBolsa UpToTemplateCorretagemBolsaByIdTemplate
		{
			get
			{
				if(this._UpToTemplateCorretagemBolsaByIdTemplate == null
					&& IdTemplate != null					)
				{
					this._UpToTemplateCorretagemBolsaByIdTemplate = new TemplateCorretagemBolsa();
					this._UpToTemplateCorretagemBolsaByIdTemplate.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTemplateCorretagemBolsaByIdTemplate", this._UpToTemplateCorretagemBolsaByIdTemplate);
					this._UpToTemplateCorretagemBolsaByIdTemplate.Query.Where(this._UpToTemplateCorretagemBolsaByIdTemplate.Query.IdTemplate == this.IdTemplate);
					this._UpToTemplateCorretagemBolsaByIdTemplate.Query.Load();
				}

				return this._UpToTemplateCorretagemBolsaByIdTemplate;
			}
			
			set
			{
				this.RemovePreSave("UpToTemplateCorretagemBolsaByIdTemplate");
				

				if(value == null)
				{
					this.IdTemplate = null;
					this._UpToTemplateCorretagemBolsaByIdTemplate = null;
				}
				else
				{
					this.IdTemplate = value.IdTemplate;
					this._UpToTemplateCorretagemBolsaByIdTemplate = value;
					this.SetPreSave("UpToTemplateCorretagemBolsaByIdTemplate", this._UpToTemplateCorretagemBolsaByIdTemplate);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTemplateCorretagemBolsaByIdTemplate != null)
			{
				this.IdTemplate = this._UpToTemplateCorretagemBolsaByIdTemplate.IdTemplate;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaCorretagemBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCorretagemBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBolsaMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTemplate
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBolsaMetadata.ColumnNames.IdTemplate, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Faixa
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBolsaMetadata.ColumnNames.Faixa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBolsaMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorAdicional
		{
			get
			{
				return new esQueryItem(this, TabelaCorretagemBolsaMetadata.ColumnNames.ValorAdicional, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaCorretagemBolsaCollection")]
	public partial class TabelaCorretagemBolsaCollection : esTabelaCorretagemBolsaCollection, IEnumerable<TabelaCorretagemBolsa>
	{
		public TabelaCorretagemBolsaCollection()
		{

		}
		
		public static implicit operator List<TabelaCorretagemBolsa>(TabelaCorretagemBolsaCollection coll)
		{
			List<TabelaCorretagemBolsa> list = new List<TabelaCorretagemBolsa>();
			
			foreach (TabelaCorretagemBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaCorretagemBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCorretagemBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaCorretagemBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaCorretagemBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaCorretagemBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCorretagemBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaCorretagemBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaCorretagemBolsa AddNew()
		{
			TabelaCorretagemBolsa entity = base.AddNewEntity() as TabelaCorretagemBolsa;
			
			return entity;
		}

		public TabelaCorretagemBolsa FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as TabelaCorretagemBolsa;
		}


		#region IEnumerable<TabelaCorretagemBolsa> Members

		IEnumerator<TabelaCorretagemBolsa> IEnumerable<TabelaCorretagemBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaCorretagemBolsa;
			}
		}

		#endregion
		
		private TabelaCorretagemBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaCorretagemBolsa' table
	/// </summary>

	[Serializable]
	public partial class TabelaCorretagemBolsa : esTabelaCorretagemBolsa
	{
		public TabelaCorretagemBolsa()
		{

		}
	
		public TabelaCorretagemBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCorretagemBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaCorretagemBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCorretagemBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaCorretagemBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCorretagemBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaCorretagemBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaCorretagemBolsaQuery query;
	}



	[Serializable]
	public partial class TabelaCorretagemBolsaQuery : esTabelaCorretagemBolsaQuery
	{
		public TabelaCorretagemBolsaQuery()
		{

		}		
		
		public TabelaCorretagemBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaCorretagemBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaCorretagemBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaCorretagemBolsaMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCorretagemBolsaMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBolsaMetadata.ColumnNames.IdTemplate, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCorretagemBolsaMetadata.PropertyNames.IdTemplate;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBolsaMetadata.ColumnNames.Faixa, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCorretagemBolsaMetadata.PropertyNames.Faixa;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBolsaMetadata.ColumnNames.Percentual, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCorretagemBolsaMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCorretagemBolsaMetadata.ColumnNames.ValorAdicional, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCorretagemBolsaMetadata.PropertyNames.ValorAdicional;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaCorretagemBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdTemplate = "IdTemplate";
			 public const string Faixa = "Faixa";
			 public const string Percentual = "Percentual";
			 public const string ValorAdicional = "ValorAdicional";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string IdTemplate = "IdTemplate";
			 public const string Faixa = "Faixa";
			 public const string Percentual = "Percentual";
			 public const string ValorAdicional = "ValorAdicional";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaCorretagemBolsaMetadata))
			{
				if(TabelaCorretagemBolsaMetadata.mapDelegates == null)
				{
					TabelaCorretagemBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaCorretagemBolsaMetadata.meta == null)
				{
					TabelaCorretagemBolsaMetadata.meta = new TabelaCorretagemBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTemplate", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Faixa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorAdicional", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaCorretagemBolsa";
				meta.Destination = "TabelaCorretagemBolsa";
				
				meta.spInsert = "proc_TabelaCorretagemBolsaInsert";				
				meta.spUpdate = "proc_TabelaCorretagemBolsaUpdate";		
				meta.spDelete = "proc_TabelaCorretagemBolsaDelete";
				meta.spLoadAll = "proc_TabelaCorretagemBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaCorretagemBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaCorretagemBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
