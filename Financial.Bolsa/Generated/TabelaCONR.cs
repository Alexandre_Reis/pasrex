/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		






using Financial.Investidor;




















		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esTabelaCONRCollection : esEntityCollection
	{
		public esTabelaCONRCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaCONRCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaCONRQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaCONRQuery);
		}
		#endregion
		
		virtual public TabelaCONR DetachEntity(TabelaCONR entity)
		{
			return base.DetachEntity(entity) as TabelaCONR;
		}
		
		virtual public TabelaCONR AttachEntity(TabelaCONR entity)
		{
			return base.AttachEntity(entity) as TabelaCONR;
		}
		
		virtual public void Combine(TabelaCONRCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaCONR this[int index]
		{
			get
			{
				return base[index] as TabelaCONR;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaCONR);
		}
	}



	[Serializable]
	abstract public class esTabelaCONR : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaCONRQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaCONR()
		{

		}

		public esTabelaCONR(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idConr)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idConr);
			else
				return LoadByPrimaryKeyStoredProcedure(idConr);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idConr)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaCONRQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdConr == idConr);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idConr)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idConr);
			else
				return LoadByPrimaryKeyStoredProcedure(idConr);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idConr)
		{
			esTabelaCONRQuery query = this.GetDynamicQuery();
			query.Where(query.IdConr == idConr);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idConr)
		{
			esParameters parms = new esParameters();
			parms.Add("IdConr",idConr);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdConr": this.str.IdConr = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "NumeroNegocio": this.str.NumeroNegocio = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Preco": this.str.Preco = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdConr":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConr = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "NumeroNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroNegocio = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Preco":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Preco = (System.Decimal?)value;
							break;
						
						case "NumeroContrato":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroContrato = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaCONR.IdConr
		/// </summary>
		virtual public System.Int32? IdConr
		{
			get
			{
				return base.GetSystemInt32(TabelaCONRMetadata.ColumnNames.IdConr);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCONRMetadata.ColumnNames.IdConr, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCONR.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(TabelaCONRMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaCONRMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaCONR.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TabelaCONRMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaCONRMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCONR.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(TabelaCONRMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(TabelaCONRMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaCONR.NumeroNegocio
		/// </summary>
		virtual public System.Int32? NumeroNegocio
		{
			get
			{
				return base.GetSystemInt32(TabelaCONRMetadata.ColumnNames.NumeroNegocio);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCONRMetadata.ColumnNames.NumeroNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCONR.TipoOperacao
		/// </summary>
		virtual public System.String TipoOperacao
		{
			get
			{
				return base.GetSystemString(TabelaCONRMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemString(TabelaCONRMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCONR.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(TabelaCONRMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCONRMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCONR.Preco
		/// </summary>
		virtual public System.Decimal? Preco
		{
			get
			{
				return base.GetSystemDecimal(TabelaCONRMetadata.ColumnNames.Preco);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCONRMetadata.ColumnNames.Preco, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCONR.NumeroContrato
		/// </summary>
		virtual public System.Int32? NumeroContrato
		{
			get
			{
				return base.GetSystemInt32(TabelaCONRMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCONRMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaCONR entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdConr
			{
				get
				{
					System.Int32? data = entity.IdConr;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConr = null;
					else entity.IdConr = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String NumeroNegocio
			{
				get
				{
					System.Int32? data = entity.NumeroNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroNegocio = null;
					else entity.NumeroNegocio = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.String data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToString(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Preco
			{
				get
				{
					System.Decimal? data = entity.Preco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Preco = null;
					else entity.Preco = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.Int32? data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToInt32(value);
				}
			}
			

			private esTabelaCONR entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaCONRQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaCONR can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaCONR : esTabelaCONR
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_TabelaCONR_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_TabelaCONR_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaCONRQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCONRMetadata.Meta();
			}
		}	
		

		public esQueryItem IdConr
		{
			get
			{
				return new esQueryItem(this, TabelaCONRMetadata.ColumnNames.IdConr, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, TabelaCONRMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TabelaCONRMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, TabelaCONRMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem NumeroNegocio
		{
			get
			{
				return new esQueryItem(this, TabelaCONRMetadata.ColumnNames.NumeroNegocio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, TabelaCONRMetadata.ColumnNames.TipoOperacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, TabelaCONRMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Preco
		{
			get
			{
				return new esQueryItem(this, TabelaCONRMetadata.ColumnNames.Preco, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, TabelaCONRMetadata.ColumnNames.NumeroContrato, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaCONRCollection")]
	public partial class TabelaCONRCollection : esTabelaCONRCollection, IEnumerable<TabelaCONR>
	{
		public TabelaCONRCollection()
		{

		}
		
		public static implicit operator List<TabelaCONR>(TabelaCONRCollection coll)
		{
			List<TabelaCONR> list = new List<TabelaCONR>();
			
			foreach (TabelaCONR emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaCONRMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCONRQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaCONR(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaCONR();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaCONRQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCONRQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaCONRQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaCONR AddNew()
		{
			TabelaCONR entity = base.AddNewEntity() as TabelaCONR;
			
			return entity;
		}

		public TabelaCONR FindByPrimaryKey(System.Int32 idConr)
		{
			return base.FindByPrimaryKey(idConr) as TabelaCONR;
		}


		#region IEnumerable<TabelaCONR> Members

		IEnumerator<TabelaCONR> IEnumerable<TabelaCONR>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaCONR;
			}
		}

		#endregion
		
		private TabelaCONRQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaCONR' table
	/// </summary>

	[Serializable]
	public partial class TabelaCONR : esTabelaCONR
	{
		public TabelaCONR()
		{

		}
	
		public TabelaCONR(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCONRMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaCONRQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCONRQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaCONRQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCONRQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaCONRQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaCONRQuery query;
	}



	[Serializable]
	public partial class TabelaCONRQuery : esTabelaCONRQuery
	{
		public TabelaCONRQuery()
		{

		}		
		
		public TabelaCONRQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaCONRMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaCONRMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaCONRMetadata.ColumnNames.IdConr, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCONRMetadata.PropertyNames.IdConr;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCONRMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCONRMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCONRMetadata.ColumnNames.Data, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaCONRMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCONRMetadata.ColumnNames.CdAtivoBolsa, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCONRMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCONRMetadata.ColumnNames.NumeroNegocio, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCONRMetadata.PropertyNames.NumeroNegocio;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCONRMetadata.ColumnNames.TipoOperacao, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCONRMetadata.PropertyNames.TipoOperacao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCONRMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCONRMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCONRMetadata.ColumnNames.Preco, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCONRMetadata.PropertyNames.Preco;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCONRMetadata.ColumnNames.NumeroContrato, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCONRMetadata.PropertyNames.NumeroContrato;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaCONRMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdConr = "IdConr";
			 public const string IdCliente = "IdCliente";
			 public const string Data = "Data";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string NumeroNegocio = "NumeroNegocio";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Quantidade = "Quantidade";
			 public const string Preco = "Preco";
			 public const string NumeroContrato = "NumeroContrato";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdConr = "IdConr";
			 public const string IdCliente = "IdCliente";
			 public const string Data = "Data";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string NumeroNegocio = "NumeroNegocio";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Quantidade = "Quantidade";
			 public const string Preco = "Preco";
			 public const string NumeroContrato = "NumeroContrato";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaCONRMetadata))
			{
				if(TabelaCONRMetadata.mapDelegates == null)
				{
					TabelaCONRMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaCONRMetadata.meta == null)
				{
					TabelaCONRMetadata.meta = new TabelaCONRMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdConr", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("NumeroNegocio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Preco", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TabelaCONR";
				meta.Destination = "TabelaCONR";
				
				meta.spInsert = "proc_TabelaCONRInsert";				
				meta.spUpdate = "proc_TabelaCONRUpdate";		
				meta.spDelete = "proc_TabelaCONRDelete";
				meta.spLoadAll = "proc_TabelaCONRLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaCONRLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaCONRMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
