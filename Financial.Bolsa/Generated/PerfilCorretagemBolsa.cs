/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





























using Financial.Common;
using Financial.Investidor;









		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esPerfilCorretagemBolsaCollection : esEntityCollection
	{
		public esPerfilCorretagemBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilCorretagemBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilCorretagemBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilCorretagemBolsaQuery);
		}
		#endregion
		
		virtual public PerfilCorretagemBolsa DetachEntity(PerfilCorretagemBolsa entity)
		{
			return base.DetachEntity(entity) as PerfilCorretagemBolsa;
		}
		
		virtual public PerfilCorretagemBolsa AttachEntity(PerfilCorretagemBolsa entity)
		{
			return base.AttachEntity(entity) as PerfilCorretagemBolsa;
		}
		
		virtual public void Combine(PerfilCorretagemBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilCorretagemBolsa this[int index]
		{
			get
			{
				return base[index] as PerfilCorretagemBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilCorretagemBolsa);
		}
	}



	[Serializable]
	abstract public class esPerfilCorretagemBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilCorretagemBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilCorretagemBolsa()
		{

		}

		public esPerfilCorretagemBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 idAgente, System.DateTime dataReferencia, System.String tipoMercado)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idAgente, dataReferencia, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idAgente, dataReferencia, tipoMercado);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int32 idAgente, System.DateTime dataReferencia, System.String tipoMercado)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPerfilCorretagemBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.IdAgente == idAgente, query.DataReferencia == dataReferencia, query.TipoMercado == tipoMercado);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 idAgente, System.DateTime dataReferencia, System.String tipoMercado)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idAgente, dataReferencia, tipoMercado);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idAgente, dataReferencia, tipoMercado);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 idAgente, System.DateTime dataReferencia, System.String tipoMercado)
		{
			esPerfilCorretagemBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.IdAgente == idAgente, query.DataReferencia == dataReferencia, query.TipoMercado == tipoMercado);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 idAgente, System.DateTime dataReferencia, System.String tipoMercado)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("IdAgente",idAgente);			parms.Add("DataReferencia",dataReferencia);			parms.Add("TipoMercado",tipoMercado);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "IdTemplate": this.str.IdTemplate = (string)value; break;							
						case "PercentualDesconto": this.str.PercentualDesconto = (string)value; break;							
						case "Iss": this.str.Iss = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "IdTemplate":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTemplate = (System.Int32?)value;
							break;
						
						case "PercentualDesconto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualDesconto = (System.Decimal?)value;
							break;
						
						case "Iss":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Iss = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilCorretagemBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PerfilCorretagemBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilCorretagemBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBolsa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PerfilCorretagemBolsaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilCorretagemBolsaMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBolsa.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(PerfilCorretagemBolsaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(PerfilCorretagemBolsaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBolsa.TipoMercado
		/// </summary>
		virtual public System.String TipoMercado
		{
			get
			{
				return base.GetSystemString(PerfilCorretagemBolsaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemString(PerfilCorretagemBolsaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBolsa.IdTemplate
		/// </summary>
		virtual public System.Int32? IdTemplate
		{
			get
			{
				return base.GetSystemInt32(PerfilCorretagemBolsaMetadata.ColumnNames.IdTemplate);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilCorretagemBolsaMetadata.ColumnNames.IdTemplate, value))
				{
					this._UpToTemplateCorretagemBolsaByIdTemplate = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBolsa.PercentualDesconto
		/// </summary>
		virtual public System.Decimal? PercentualDesconto
		{
			get
			{
				return base.GetSystemDecimal(PerfilCorretagemBolsaMetadata.ColumnNames.PercentualDesconto);
			}
			
			set
			{
				base.SetSystemDecimal(PerfilCorretagemBolsaMetadata.ColumnNames.PercentualDesconto, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilCorretagemBolsa.ISS
		/// </summary>
		virtual public System.Decimal? Iss
		{
			get
			{
				return base.GetSystemDecimal(PerfilCorretagemBolsaMetadata.ColumnNames.Iss);
			}
			
			set
			{
				base.SetSystemDecimal(PerfilCorretagemBolsaMetadata.ColumnNames.Iss, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected TemplateCorretagemBolsa _UpToTemplateCorretagemBolsaByIdTemplate;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilCorretagemBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.String data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToString(value);
				}
			}
				
			public System.String IdTemplate
			{
				get
				{
					System.Int32? data = entity.IdTemplate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTemplate = null;
					else entity.IdTemplate = Convert.ToInt32(value);
				}
			}
				
			public System.String PercentualDesconto
			{
				get
				{
					System.Decimal? data = entity.PercentualDesconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualDesconto = null;
					else entity.PercentualDesconto = Convert.ToDecimal(value);
				}
			}
				
			public System.String Iss
			{
				get
				{
					System.Decimal? data = entity.Iss;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Iss = null;
					else entity.Iss = Convert.ToDecimal(value);
				}
			}
			

			private esPerfilCorretagemBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilCorretagemBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilCorretagemBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilCorretagemBolsa : esPerfilCorretagemBolsa
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_PerfilCorretagemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PerfilCorretagemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTemplateCorretagemBolsaByIdTemplate - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TemplateCorretagemBolsa_PerfilCorretagemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public TemplateCorretagemBolsa UpToTemplateCorretagemBolsaByIdTemplate
		{
			get
			{
				if(this._UpToTemplateCorretagemBolsaByIdTemplate == null
					&& IdTemplate != null					)
				{
					this._UpToTemplateCorretagemBolsaByIdTemplate = new TemplateCorretagemBolsa();
					this._UpToTemplateCorretagemBolsaByIdTemplate.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTemplateCorretagemBolsaByIdTemplate", this._UpToTemplateCorretagemBolsaByIdTemplate);
					this._UpToTemplateCorretagemBolsaByIdTemplate.Query.Where(this._UpToTemplateCorretagemBolsaByIdTemplate.Query.IdTemplate == this.IdTemplate);
					this._UpToTemplateCorretagemBolsaByIdTemplate.Query.Load();
				}

				return this._UpToTemplateCorretagemBolsaByIdTemplate;
			}
			
			set
			{
				this.RemovePreSave("UpToTemplateCorretagemBolsaByIdTemplate");
				

				if(value == null)
				{
					this.IdTemplate = null;
					this._UpToTemplateCorretagemBolsaByIdTemplate = null;
				}
				else
				{
					this.IdTemplate = value.IdTemplate;
					this._UpToTemplateCorretagemBolsaByIdTemplate = value;
					this.SetPreSave("UpToTemplateCorretagemBolsaByIdTemplate", this._UpToTemplateCorretagemBolsaByIdTemplate);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToTemplateCorretagemBolsaByIdTemplate != null)
			{
				this.IdTemplate = this._UpToTemplateCorretagemBolsaByIdTemplate.IdTemplate;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilCorretagemBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilCorretagemBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBolsaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBolsaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBolsaMetadata.ColumnNames.TipoMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem IdTemplate
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBolsaMetadata.ColumnNames.IdTemplate, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PercentualDesconto
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBolsaMetadata.ColumnNames.PercentualDesconto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Iss
		{
			get
			{
				return new esQueryItem(this, PerfilCorretagemBolsaMetadata.ColumnNames.Iss, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilCorretagemBolsaCollection")]
	public partial class PerfilCorretagemBolsaCollection : esPerfilCorretagemBolsaCollection, IEnumerable<PerfilCorretagemBolsa>
	{
		public PerfilCorretagemBolsaCollection()
		{

		}
		
		public static implicit operator List<PerfilCorretagemBolsa>(PerfilCorretagemBolsaCollection coll)
		{
			List<PerfilCorretagemBolsa> list = new List<PerfilCorretagemBolsa>();
			
			foreach (PerfilCorretagemBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilCorretagemBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilCorretagemBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilCorretagemBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilCorretagemBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilCorretagemBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilCorretagemBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilCorretagemBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilCorretagemBolsa AddNew()
		{
			PerfilCorretagemBolsa entity = base.AddNewEntity() as PerfilCorretagemBolsa;
			
			return entity;
		}

		public PerfilCorretagemBolsa FindByPrimaryKey(System.Int32 idCliente, System.Int32 idAgente, System.DateTime dataReferencia, System.String tipoMercado)
		{
			return base.FindByPrimaryKey(idCliente, idAgente, dataReferencia, tipoMercado) as PerfilCorretagemBolsa;
		}


		#region IEnumerable<PerfilCorretagemBolsa> Members

		IEnumerator<PerfilCorretagemBolsa> IEnumerable<PerfilCorretagemBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilCorretagemBolsa;
			}
		}

		#endregion
		
		private PerfilCorretagemBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilCorretagemBolsa' table
	/// </summary>

	[Serializable]
	public partial class PerfilCorretagemBolsa : esPerfilCorretagemBolsa
	{
		public PerfilCorretagemBolsa()
		{

		}
	
		public PerfilCorretagemBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilCorretagemBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilCorretagemBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilCorretagemBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilCorretagemBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilCorretagemBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilCorretagemBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilCorretagemBolsaQuery query;
	}



	[Serializable]
	public partial class PerfilCorretagemBolsaQuery : esPerfilCorretagemBolsaQuery
	{
		public PerfilCorretagemBolsaQuery()
		{

		}		
		
		public PerfilCorretagemBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilCorretagemBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilCorretagemBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilCorretagemBolsaMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilCorretagemBolsaMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBolsaMetadata.ColumnNames.IdAgente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilCorretagemBolsaMetadata.PropertyNames.IdAgente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBolsaMetadata.ColumnNames.DataReferencia, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PerfilCorretagemBolsaMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBolsaMetadata.ColumnNames.TipoMercado, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = PerfilCorretagemBolsaMetadata.PropertyNames.TipoMercado;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBolsaMetadata.ColumnNames.IdTemplate, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilCorretagemBolsaMetadata.PropertyNames.IdTemplate;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBolsaMetadata.ColumnNames.PercentualDesconto, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PerfilCorretagemBolsaMetadata.PropertyNames.PercentualDesconto;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilCorretagemBolsaMetadata.ColumnNames.Iss, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PerfilCorretagemBolsaMetadata.PropertyNames.Iss;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilCorretagemBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string DataReferencia = "DataReferencia";
			 public const string TipoMercado = "TipoMercado";
			 public const string IdTemplate = "IdTemplate";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string Iss = "ISS";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string DataReferencia = "DataReferencia";
			 public const string TipoMercado = "TipoMercado";
			 public const string IdTemplate = "IdTemplate";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string Iss = "Iss";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilCorretagemBolsaMetadata))
			{
				if(PerfilCorretagemBolsaMetadata.mapDelegates == null)
				{
					PerfilCorretagemBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilCorretagemBolsaMetadata.meta == null)
				{
					PerfilCorretagemBolsaMetadata.meta = new PerfilCorretagemBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdTemplate", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PercentualDesconto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ISS", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "PerfilCorretagemBolsa";
				meta.Destination = "PerfilCorretagemBolsa";
				
				meta.spInsert = "proc_PerfilCorretagemBolsaInsert";				
				meta.spUpdate = "proc_PerfilCorretagemBolsaUpdate";		
				meta.spDelete = "proc_PerfilCorretagemBolsaDelete";
				meta.spLoadAll = "proc_PerfilCorretagemBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilCorretagemBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilCorretagemBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
