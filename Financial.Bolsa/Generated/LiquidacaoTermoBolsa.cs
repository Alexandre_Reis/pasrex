/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;






































using Financial.Common;
using Financial.Investidor;		
		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esLiquidacaoTermoBolsaCollection : esEntityCollection
	{
		public esLiquidacaoTermoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LiquidacaoTermoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esLiquidacaoTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLiquidacaoTermoBolsaQuery);
		}
		#endregion
		
		virtual public LiquidacaoTermoBolsa DetachEntity(LiquidacaoTermoBolsa entity)
		{
			return base.DetachEntity(entity) as LiquidacaoTermoBolsa;
		}
		
		virtual public LiquidacaoTermoBolsa AttachEntity(LiquidacaoTermoBolsa entity)
		{
			return base.AttachEntity(entity) as LiquidacaoTermoBolsa;
		}
		
		virtual public void Combine(LiquidacaoTermoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LiquidacaoTermoBolsa this[int index]
		{
			get
			{
				return base[index] as LiquidacaoTermoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LiquidacaoTermoBolsa);
		}
	}



	[Serializable]
	abstract public class esLiquidacaoTermoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLiquidacaoTermoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esLiquidacaoTermoBolsa()
		{

		}

		public esLiquidacaoTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLiquidacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idLiquidacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLiquidacaoTermoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLiquidacao == idLiquidacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLiquidacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLiquidacao)
		{
			esLiquidacaoTermoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdLiquidacao == idLiquidacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLiquidacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLiquidacao",idLiquidacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLiquidacao": this.str.IdLiquidacao = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataMovimento": this.str.DataMovimento = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "TipoLiquidacao": this.str.TipoLiquidacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLiquidacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataMovimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataMovimento = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "TipoLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoLiquidacao = (System.Byte?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.IdLiquidacao
		/// </summary>
		virtual public System.Int32? IdLiquidacao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoTermoBolsaMetadata.ColumnNames.IdLiquidacao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoTermoBolsaMetadata.ColumnNames.IdLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(LiquidacaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(LiquidacaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoTermoBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoTermoBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.DataMovimento
		/// </summary>
		virtual public System.DateTime? DataMovimento
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoTermoBolsaMetadata.ColumnNames.DataMovimento);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoTermoBolsaMetadata.ColumnNames.DataMovimento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoTermoBolsaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoTermoBolsaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoTermoBolsaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoTermoBolsaMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoTermoBolsaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoTermoBolsaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.TipoLiquidacao
		/// </summary>
		virtual public System.Byte? TipoLiquidacao
		{
			get
			{
				return base.GetSystemByte(LiquidacaoTermoBolsaMetadata.ColumnNames.TipoLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoTermoBolsaMetadata.ColumnNames.TipoLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoTermoBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoTermoBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoTermoBolsaMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoTermoBolsaMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoTermoBolsaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoTermoBolsaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(LiquidacaoTermoBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoTermoBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTermoBolsa.NumeroContrato
		/// </summary>
		virtual public System.String NumeroContrato
		{
			get
			{
				return base.GetSystemString(LiquidacaoTermoBolsaMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemString(LiquidacaoTermoBolsaMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLiquidacaoTermoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacao = null;
					else entity.IdLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataMovimento
			{
				get
				{
					System.DateTime? data = entity.DataMovimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataMovimento = null;
					else entity.DataMovimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoLiquidacao
			{
				get
				{
					System.Byte? data = entity.TipoLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLiquidacao = null;
					else entity.TipoLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.String data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToString(value);
				}
			}
			

			private esLiquidacaoTermoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLiquidacaoTermoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLiquidacaoTermoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LiquidacaoTermoBolsa : esLiquidacaoTermoBolsa
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_LiquidacaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_LiquidacaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_LiquidacaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLiquidacaoTermoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoTermoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.IdLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataMovimento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.DataMovimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.TipoLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTermoBolsaMetadata.ColumnNames.NumeroContrato, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LiquidacaoTermoBolsaCollection")]
	public partial class LiquidacaoTermoBolsaCollection : esLiquidacaoTermoBolsaCollection, IEnumerable<LiquidacaoTermoBolsa>
	{
		public LiquidacaoTermoBolsaCollection()
		{

		}
		
		public static implicit operator List<LiquidacaoTermoBolsa>(LiquidacaoTermoBolsaCollection coll)
		{
			List<LiquidacaoTermoBolsa> list = new List<LiquidacaoTermoBolsa>();
			
			foreach (LiquidacaoTermoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LiquidacaoTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LiquidacaoTermoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LiquidacaoTermoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LiquidacaoTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LiquidacaoTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LiquidacaoTermoBolsa AddNew()
		{
			LiquidacaoTermoBolsa entity = base.AddNewEntity() as LiquidacaoTermoBolsa;
			
			return entity;
		}

		public LiquidacaoTermoBolsa FindByPrimaryKey(System.Int32 idLiquidacao)
		{
			return base.FindByPrimaryKey(idLiquidacao) as LiquidacaoTermoBolsa;
		}


		#region IEnumerable<LiquidacaoTermoBolsa> Members

		IEnumerator<LiquidacaoTermoBolsa> IEnumerable<LiquidacaoTermoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LiquidacaoTermoBolsa;
			}
		}

		#endregion
		
		private LiquidacaoTermoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LiquidacaoTermoBolsa' table
	/// </summary>

	[Serializable]
	public partial class LiquidacaoTermoBolsa : esLiquidacaoTermoBolsa
	{
		public LiquidacaoTermoBolsa()
		{

		}
	
		public LiquidacaoTermoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoTermoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esLiquidacaoTermoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoTermoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LiquidacaoTermoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoTermoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LiquidacaoTermoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LiquidacaoTermoBolsaQuery query;
	}



	[Serializable]
	public partial class LiquidacaoTermoBolsaQuery : esLiquidacaoTermoBolsaQuery
	{
		public LiquidacaoTermoBolsaQuery()
		{

		}		
		
		public LiquidacaoTermoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LiquidacaoTermoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LiquidacaoTermoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.IdLiquidacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.IdLiquidacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.DataMovimento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.DataMovimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.DataLiquidacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.IdAgente, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.IdPosicao, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.TipoLiquidacao, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.TipoLiquidacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.Quantidade, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.Pu, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.Valor, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.Fonte, 11, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTermoBolsaMetadata.ColumnNames.NumeroContrato, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = LiquidacaoTermoBolsaMetadata.PropertyNames.NumeroContrato;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LiquidacaoTermoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdCliente = "IdCliente";
			 public const string DataMovimento = "DataMovimento";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string IdAgente = "IdAgente";
			 public const string IdPosicao = "IdPosicao";
			 public const string TipoLiquidacao = "TipoLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "PU";
			 public const string Valor = "Valor";
			 public const string Fonte = "Fonte";
			 public const string NumeroContrato = "NumeroContrato";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdCliente = "IdCliente";
			 public const string DataMovimento = "DataMovimento";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string IdAgente = "IdAgente";
			 public const string IdPosicao = "IdPosicao";
			 public const string TipoLiquidacao = "TipoLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "Pu";
			 public const string Valor = "Valor";
			 public const string Fonte = "Fonte";
			 public const string NumeroContrato = "NumeroContrato";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LiquidacaoTermoBolsaMetadata))
			{
				if(LiquidacaoTermoBolsaMetadata.mapDelegates == null)
				{
					LiquidacaoTermoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LiquidacaoTermoBolsaMetadata.meta == null)
				{
					LiquidacaoTermoBolsaMetadata.meta = new LiquidacaoTermoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataMovimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "LiquidacaoTermoBolsa";
				meta.Destination = "LiquidacaoTermoBolsa";
				
				meta.spInsert = "proc_LiquidacaoTermoBolsaInsert";				
				meta.spUpdate = "proc_LiquidacaoTermoBolsaUpdate";		
				meta.spDelete = "proc_LiquidacaoTermoBolsaDelete";
				meta.spLoadAll = "proc_LiquidacaoTermoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_LiquidacaoTermoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LiquidacaoTermoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
