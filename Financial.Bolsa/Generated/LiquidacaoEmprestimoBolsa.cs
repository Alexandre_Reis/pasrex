/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;

























using Financial.Common;
using Financial.Investidor;		













		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esLiquidacaoEmprestimoBolsaCollection : esEntityCollection
	{
		public esLiquidacaoEmprestimoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LiquidacaoEmprestimoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esLiquidacaoEmprestimoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLiquidacaoEmprestimoBolsaQuery);
		}
		#endregion
		
		virtual public LiquidacaoEmprestimoBolsa DetachEntity(LiquidacaoEmprestimoBolsa entity)
		{
			return base.DetachEntity(entity) as LiquidacaoEmprestimoBolsa;
		}
		
		virtual public LiquidacaoEmprestimoBolsa AttachEntity(LiquidacaoEmprestimoBolsa entity)
		{
			return base.AttachEntity(entity) as LiquidacaoEmprestimoBolsa;
		}
		
		virtual public void Combine(LiquidacaoEmprestimoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LiquidacaoEmprestimoBolsa this[int index]
		{
			get
			{
				return base[index] as LiquidacaoEmprestimoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LiquidacaoEmprestimoBolsa);
		}
	}



	[Serializable]
	abstract public class esLiquidacaoEmprestimoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLiquidacaoEmprestimoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esLiquidacaoEmprestimoBolsa()
		{

		}

		public esLiquidacaoEmprestimoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLiquidacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idLiquidacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLiquidacaoEmprestimoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLiquidacao == idLiquidacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLiquidacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLiquidacao)
		{
			esLiquidacaoEmprestimoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdLiquidacao == idLiquidacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLiquidacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLiquidacao",idLiquidacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLiquidacao": this.str.IdLiquidacao = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "TipoLiquidacao": this.str.TipoLiquidacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "ValorLiquidacao": this.str.ValorLiquidacao = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLiquidacao = (System.Int32?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "TipoLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoLiquidacao = (System.Byte?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "ValorLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquidacao = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "NumeroContrato":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroContrato = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.IdLiquidacao
		/// </summary>
		virtual public System.Int32? IdLiquidacao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdLiquidacao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.TipoLiquidacao
		/// </summary>
		virtual public System.Byte? TipoLiquidacao
		{
			get
			{
				return base.GetSystemByte(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.TipoLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.TipoLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdAgente, value))
				{
					this._UpToAgenteMercadoByIdAgente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoEmprestimoBolsaByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.ValorLiquidacao
		/// </summary>
		virtual public System.Decimal? ValorLiquidacao
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.ValorLiquidacao);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.ValorLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoEmprestimoBolsa.NumeroContrato
		/// </summary>
		virtual public System.Int32? NumeroContrato
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgente;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected OperacaoEmprestimoBolsa _UpToOperacaoEmprestimoBolsaByIdOperacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLiquidacaoEmprestimoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacao = null;
					else entity.IdLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoLiquidacao
			{
				get
				{
					System.Byte? data = entity.TipoLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLiquidacao = null;
					else entity.TipoLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorLiquidacao
			{
				get
				{
					System.Decimal? data = entity.ValorLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquidacao = null;
					else entity.ValorLiquidacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.Int32? data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToInt32(value);
				}
			}
			

			private esLiquidacaoEmprestimoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLiquidacaoEmprestimoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLiquidacaoEmprestimoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LiquidacaoEmprestimoBolsa : esLiquidacaoEmprestimoBolsa
	{

				
		#region UpToAgenteMercadoByIdAgente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_AntecipacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgente
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgente == null
					&& IdAgente != null					)
				{
					this._UpToAgenteMercadoByIdAgente = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Where(this._UpToAgenteMercadoByIdAgente.Query.IdAgente == this.IdAgente);
					this._UpToAgenteMercadoByIdAgente.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgente;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgente");
				

				if(value == null)
				{
					this.IdAgente = null;
					this._UpToAgenteMercadoByIdAgente = null;
				}
				else
				{
					this.IdAgente = value.IdAgente;
					this._UpToAgenteMercadoByIdAgente = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgente", this._UpToAgenteMercadoByIdAgente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_AntecipacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToOperacaoEmprestimoBolsaByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoEmprestimoBolsa_LiquidacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoEmprestimoBolsa UpToOperacaoEmprestimoBolsaByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoEmprestimoBolsaByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoEmprestimoBolsaByIdOperacao = new OperacaoEmprestimoBolsa();
					this._UpToOperacaoEmprestimoBolsaByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoEmprestimoBolsaByIdOperacao", this._UpToOperacaoEmprestimoBolsaByIdOperacao);
					this._UpToOperacaoEmprestimoBolsaByIdOperacao.Query.Where(this._UpToOperacaoEmprestimoBolsaByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoEmprestimoBolsaByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoEmprestimoBolsaByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoEmprestimoBolsaByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoEmprestimoBolsaByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoEmprestimoBolsaByIdOperacao = value;
					this.SetPreSave("UpToOperacaoEmprestimoBolsaByIdOperacao", this._UpToOperacaoEmprestimoBolsaByIdOperacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgente != null)
			{
				this.IdAgente = this._UpToAgenteMercadoByIdAgente.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToOperacaoEmprestimoBolsaByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoEmprestimoBolsaByIdOperacao.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLiquidacaoEmprestimoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoEmprestimoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.TipoLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.ValorLiquidacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LiquidacaoEmprestimoBolsaCollection")]
	public partial class LiquidacaoEmprestimoBolsaCollection : esLiquidacaoEmprestimoBolsaCollection, IEnumerable<LiquidacaoEmprestimoBolsa>
	{
		public LiquidacaoEmprestimoBolsaCollection()
		{

		}
		
		public static implicit operator List<LiquidacaoEmprestimoBolsa>(LiquidacaoEmprestimoBolsaCollection coll)
		{
			List<LiquidacaoEmprestimoBolsa> list = new List<LiquidacaoEmprestimoBolsa>();
			
			foreach (LiquidacaoEmprestimoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LiquidacaoEmprestimoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoEmprestimoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LiquidacaoEmprestimoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LiquidacaoEmprestimoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LiquidacaoEmprestimoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoEmprestimoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LiquidacaoEmprestimoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LiquidacaoEmprestimoBolsa AddNew()
		{
			LiquidacaoEmprestimoBolsa entity = base.AddNewEntity() as LiquidacaoEmprestimoBolsa;
			
			return entity;
		}

		public LiquidacaoEmprestimoBolsa FindByPrimaryKey(System.Int32 idLiquidacao)
		{
			return base.FindByPrimaryKey(idLiquidacao) as LiquidacaoEmprestimoBolsa;
		}


		#region IEnumerable<LiquidacaoEmprestimoBolsa> Members

		IEnumerator<LiquidacaoEmprestimoBolsa> IEnumerable<LiquidacaoEmprestimoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LiquidacaoEmprestimoBolsa;
			}
		}

		#endregion
		
		private LiquidacaoEmprestimoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LiquidacaoEmprestimoBolsa' table
	/// </summary>

	[Serializable]
	public partial class LiquidacaoEmprestimoBolsa : esLiquidacaoEmprestimoBolsa
	{
		public LiquidacaoEmprestimoBolsa()
		{

		}
	
		public LiquidacaoEmprestimoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoEmprestimoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esLiquidacaoEmprestimoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoEmprestimoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LiquidacaoEmprestimoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoEmprestimoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LiquidacaoEmprestimoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LiquidacaoEmprestimoBolsaQuery query;
	}



	[Serializable]
	public partial class LiquidacaoEmprestimoBolsaQuery : esLiquidacaoEmprestimoBolsaQuery
	{
		public LiquidacaoEmprestimoBolsaQuery()
		{

		}		
		
		public LiquidacaoEmprestimoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LiquidacaoEmprestimoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LiquidacaoEmprestimoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdLiquidacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.IdLiquidacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Data, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.TipoLiquidacao, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.TipoLiquidacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdAgente, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Quantidade, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Valor, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Fonte, 9, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.ValorLiquidacao, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.ValorLiquidacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.ValorIR, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoEmprestimoBolsaMetadata.PropertyNames.NumeroContrato;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LiquidacaoEmprestimoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string IdPosicao = "IdPosicao";
			 public const string Data = "Data";
			 public const string TipoLiquidacao = "TipoLiquidacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Quantidade = "Quantidade";
			 public const string Valor = "Valor";
			 public const string Fonte = "Fonte";
			 public const string IdOperacao = "IdOperacao";
			 public const string ValorLiquidacao = "ValorLiquidacao";
			 public const string ValorIR = "ValorIR";
			 public const string NumeroContrato = "NumeroContrato";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string IdPosicao = "IdPosicao";
			 public const string Data = "Data";
			 public const string TipoLiquidacao = "TipoLiquidacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdAgente = "IdAgente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Quantidade = "Quantidade";
			 public const string Valor = "Valor";
			 public const string Fonte = "Fonte";
			 public const string IdOperacao = "IdOperacao";
			 public const string ValorLiquidacao = "ValorLiquidacao";
			 public const string ValorIR = "ValorIR";
			 public const string NumeroContrato = "NumeroContrato";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LiquidacaoEmprestimoBolsaMetadata))
			{
				if(LiquidacaoEmprestimoBolsaMetadata.mapDelegates == null)
				{
					LiquidacaoEmprestimoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LiquidacaoEmprestimoBolsaMetadata.meta == null)
				{
					LiquidacaoEmprestimoBolsaMetadata.meta = new LiquidacaoEmprestimoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorLiquidacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "LiquidacaoEmprestimoBolsa";
				meta.Destination = "LiquidacaoEmprestimoBolsa";
				
				meta.spInsert = "proc_LiquidacaoEmprestimoBolsaInsert";				
				meta.spUpdate = "proc_LiquidacaoEmprestimoBolsaUpdate";		
				meta.spDelete = "proc_LiquidacaoEmprestimoBolsaDelete";
				meta.spLoadAll = "proc_LiquidacaoEmprestimoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_LiquidacaoEmprestimoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LiquidacaoEmprestimoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
