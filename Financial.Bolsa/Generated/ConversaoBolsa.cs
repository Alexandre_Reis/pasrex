/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					



		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esConversaoBolsaCollection : esEntityCollection
	{
		public esConversaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ConversaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esConversaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esConversaoBolsaQuery);
		}
		#endregion
		
		virtual public ConversaoBolsa DetachEntity(ConversaoBolsa entity)
		{
			return base.DetachEntity(entity) as ConversaoBolsa;
		}
		
		virtual public ConversaoBolsa AttachEntity(ConversaoBolsa entity)
		{
			return base.AttachEntity(entity) as ConversaoBolsa;
		}
		
		virtual public void Combine(ConversaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ConversaoBolsa this[int index]
		{
			get
			{
				return base[index] as ConversaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ConversaoBolsa);
		}
	}



	[Serializable]
	abstract public class esConversaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esConversaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esConversaoBolsa()
		{

		}

		public esConversaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idConversao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idConversao);
			else
				return LoadByPrimaryKeyStoredProcedure(idConversao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idConversao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esConversaoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdConversao == idConversao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idConversao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idConversao);
			else
				return LoadByPrimaryKeyStoredProcedure(idConversao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idConversao)
		{
			esConversaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdConversao == idConversao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idConversao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdConversao",idConversao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdConversao": this.str.IdConversao = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataEx": this.str.DataEx = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "FatorQuantidade": this.str.FatorQuantidade = (string)value; break;							
						case "FatorPU": this.str.FatorPU = (string)value; break;							
						case "CdAtivoBolsaDestino": this.str.CdAtivoBolsaDestino = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdConversao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConversao = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataEx":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEx = (System.DateTime?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "FatorQuantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FatorQuantidade = (System.Decimal?)value;
							break;
						
						case "FatorPU":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.FatorPU = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ConversaoBolsa.IdConversao
		/// </summary>
		virtual public System.Int32? IdConversao
		{
			get
			{
				return base.GetSystemInt32(ConversaoBolsaMetadata.ColumnNames.IdConversao);
			}
			
			set
			{
				base.SetSystemInt32(ConversaoBolsaMetadata.ColumnNames.IdConversao, value);
			}
		}
		
		/// <summary>
		/// Maps to ConversaoBolsa.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(ConversaoBolsaMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(ConversaoBolsaMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to ConversaoBolsa.DataEx
		/// </summary>
		virtual public System.DateTime? DataEx
		{
			get
			{
				return base.GetSystemDateTime(ConversaoBolsaMetadata.ColumnNames.DataEx);
			}
			
			set
			{
				base.SetSystemDateTime(ConversaoBolsaMetadata.ColumnNames.DataEx, value);
			}
		}
		
		/// <summary>
		/// Maps to ConversaoBolsa.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(ConversaoBolsaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(ConversaoBolsaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to ConversaoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ConversaoBolsa.FatorQuantidade
		/// </summary>
		virtual public System.Decimal? FatorQuantidade
		{
			get
			{
				return base.GetSystemDecimal(ConversaoBolsaMetadata.ColumnNames.FatorQuantidade);
			}
			
			set
			{
				base.SetSystemDecimal(ConversaoBolsaMetadata.ColumnNames.FatorQuantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to ConversaoBolsa.FatorPU
		/// </summary>
		virtual public System.Decimal? FatorPU
		{
			get
			{
				return base.GetSystemDecimal(ConversaoBolsaMetadata.ColumnNames.FatorPU);
			}
			
			set
			{
				base.SetSystemDecimal(ConversaoBolsaMetadata.ColumnNames.FatorPU, value);
			}
		}
		
		/// <summary>
		/// Maps to ConversaoBolsa.CdAtivoBolsaDestino
		/// </summary>
		virtual public System.String CdAtivoBolsaDestino
		{
			get
			{
				return base.GetSystemString(ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino);
			}
			
			set
			{
				if(base.SetSystemString(ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsaDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ConversaoBolsa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(ConversaoBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(ConversaoBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsaDestino;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esConversaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdConversao
			{
				get
				{
					System.Int32? data = entity.IdConversao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConversao = null;
					else entity.IdConversao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEx
			{
				get
				{
					System.DateTime? data = entity.DataEx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEx = null;
					else entity.DataEx = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String FatorQuantidade
			{
				get
				{
					System.Decimal? data = entity.FatorQuantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatorQuantidade = null;
					else entity.FatorQuantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String FatorPU
			{
				get
				{
					System.Decimal? data = entity.FatorPU;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.FatorPU = null;
					else entity.FatorPU = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAtivoBolsaDestino
			{
				get
				{
					System.String data = entity.CdAtivoBolsaDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaDestino = null;
					else entity.CdAtivoBolsaDestino = Convert.ToString(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
			

			private esConversaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esConversaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esConversaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ConversaoBolsa : esConversaoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_ConversaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsaDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_ConversaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsaDestino
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsaDestino == null
					&& CdAtivoBolsaDestino != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsaDestino = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaDestino", this._UpToAtivoBolsaByCdAtivoBolsaDestino);
					this._UpToAtivoBolsaByCdAtivoBolsaDestino.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsaDestino.Query.CdAtivoBolsa == this.CdAtivoBolsaDestino);
					this._UpToAtivoBolsaByCdAtivoBolsaDestino.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsaDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsaDestino");
				

				if(value == null)
				{
					this.CdAtivoBolsaDestino = null;
					this._UpToAtivoBolsaByCdAtivoBolsaDestino = null;
				}
				else
				{
					this.CdAtivoBolsaDestino = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsaDestino = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaDestino", this._UpToAtivoBolsaByCdAtivoBolsaDestino);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esConversaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ConversaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdConversao
		{
			get
			{
				return new esQueryItem(this, ConversaoBolsaMetadata.ColumnNames.IdConversao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, ConversaoBolsaMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataEx
		{
			get
			{
				return new esQueryItem(this, ConversaoBolsaMetadata.ColumnNames.DataEx, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, ConversaoBolsaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem FatorQuantidade
		{
			get
			{
				return new esQueryItem(this, ConversaoBolsaMetadata.ColumnNames.FatorQuantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem FatorPU
		{
			get
			{
				return new esQueryItem(this, ConversaoBolsaMetadata.ColumnNames.FatorPU, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAtivoBolsaDestino
		{
			get
			{
				return new esQueryItem(this, ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino, esSystemType.String);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, ConversaoBolsaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ConversaoBolsaCollection")]
	public partial class ConversaoBolsaCollection : esConversaoBolsaCollection, IEnumerable<ConversaoBolsa>
	{
		public ConversaoBolsaCollection()
		{

		}
		
		public static implicit operator List<ConversaoBolsa>(ConversaoBolsaCollection coll)
		{
			List<ConversaoBolsa> list = new List<ConversaoBolsa>();
			
			foreach (ConversaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ConversaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ConversaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ConversaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ConversaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ConversaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ConversaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ConversaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ConversaoBolsa AddNew()
		{
			ConversaoBolsa entity = base.AddNewEntity() as ConversaoBolsa;
			
			return entity;
		}

		public ConversaoBolsa FindByPrimaryKey(System.Int32 idConversao)
		{
			return base.FindByPrimaryKey(idConversao) as ConversaoBolsa;
		}


		#region IEnumerable<ConversaoBolsa> Members

		IEnumerator<ConversaoBolsa> IEnumerable<ConversaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ConversaoBolsa;
			}
		}

		#endregion
		
		private ConversaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ConversaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class ConversaoBolsa : esConversaoBolsa
	{
		public ConversaoBolsa()
		{

		}
	
		public ConversaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ConversaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esConversaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ConversaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ConversaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ConversaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ConversaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ConversaoBolsaQuery query;
	}



	[Serializable]
	public partial class ConversaoBolsaQuery : esConversaoBolsaQuery
	{
		public ConversaoBolsaQuery()
		{

		}		
		
		public ConversaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ConversaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ConversaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ConversaoBolsaMetadata.ColumnNames.IdConversao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ConversaoBolsaMetadata.PropertyNames.IdConversao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoBolsaMetadata.ColumnNames.DataLancamento, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ConversaoBolsaMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoBolsaMetadata.ColumnNames.DataEx, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ConversaoBolsaMetadata.PropertyNames.DataEx;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoBolsaMetadata.ColumnNames.DataReferencia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ConversaoBolsaMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsa, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ConversaoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoBolsaMetadata.ColumnNames.FatorQuantidade, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ConversaoBolsaMetadata.PropertyNames.FatorQuantidade;	
			c.NumericPrecision = 20;
			c.NumericScale = 11;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoBolsaMetadata.ColumnNames.FatorPU, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ConversaoBolsaMetadata.PropertyNames.FatorPU;	
			c.NumericPrecision = 20;
			c.NumericScale = 15;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = ConversaoBolsaMetadata.PropertyNames.CdAtivoBolsaDestino;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ConversaoBolsaMetadata.ColumnNames.Fonte, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ConversaoBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ConversaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdConversao = "IdConversao";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string FatorQuantidade = "FatorQuantidade";
			 public const string FatorPU = "FatorPU";
			 public const string CdAtivoBolsaDestino = "CdAtivoBolsaDestino";
			 public const string Fonte = "Fonte";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdConversao = "IdConversao";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string FatorQuantidade = "FatorQuantidade";
			 public const string FatorPU = "FatorPU";
			 public const string CdAtivoBolsaDestino = "CdAtivoBolsaDestino";
			 public const string Fonte = "Fonte";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ConversaoBolsaMetadata))
			{
				if(ConversaoBolsaMetadata.mapDelegates == null)
				{
					ConversaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ConversaoBolsaMetadata.meta == null)
				{
					ConversaoBolsaMetadata.meta = new ConversaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdConversao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEx", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("FatorQuantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("FatorPU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CdAtivoBolsaDestino", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "ConversaoBolsa";
				meta.Destination = "ConversaoBolsa";
				
				meta.spInsert = "proc_ConversaoBolsaInsert";				
				meta.spUpdate = "proc_ConversaoBolsaUpdate";		
				meta.spDelete = "proc_ConversaoBolsaDelete";
				meta.spLoadAll = "proc_ConversaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ConversaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ConversaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
