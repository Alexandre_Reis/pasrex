/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/07/2015 18:18:44
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Gerencial;
using Financial.Fundo;



namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esAtivoBolsaCollection : esEntityCollection
	{
		public esAtivoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AtivoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esAtivoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAtivoBolsaQuery);
		}
		#endregion
		
		virtual public AtivoBolsa DetachEntity(AtivoBolsa entity)
		{
			return base.DetachEntity(entity) as AtivoBolsa;
		}
		
		virtual public AtivoBolsa AttachEntity(AtivoBolsa entity)
		{
			return base.AttachEntity(entity) as AtivoBolsa;
		}
		
		virtual public void Combine(AtivoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AtivoBolsa this[int index]
		{
			get
			{
				return base[index] as AtivoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AtivoBolsa);
		}
	}



	[Serializable]
	abstract public class esAtivoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAtivoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esAtivoBolsa()
		{

		}

		public esAtivoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String cdAtivoBolsa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBolsa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String cdAtivoBolsa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esAtivoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.CdAtivoBolsa == cdAtivoBolsa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String cdAtivoBolsa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBolsa);
		}

		private bool LoadByPrimaryKeyDynamic(System.String cdAtivoBolsa)
		{
			esAtivoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.CdAtivoBolsa == cdAtivoBolsa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String cdAtivoBolsa)
		{
			esParameters parms = new esParameters();
			parms.Add("CdAtivoBolsa",cdAtivoBolsa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Especificacao": this.str.Especificacao = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "PUExercicio": this.str.PUExercicio = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "CdAtivoBolsaObjeto": this.str.CdAtivoBolsaObjeto = (string)value; break;							
						case "CodigoIsin": this.str.CodigoIsin = (string)value; break;							
						case "IdEmissor": this.str.IdEmissor = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "OpcaoIndice": this.str.OpcaoIndice = (string)value; break;							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "CodigoCDA": this.str.CodigoCDA = (string)value; break;							
						case "NumeroSerie": this.str.NumeroSerie = (string)value; break;							
						case "TipoPapel": this.str.TipoPapel = (string)value; break;							
						case "DataInicioVigencia": this.str.DataInicioVigencia = (string)value; break;							
						case "DataFimVigencia": this.str.DataFimVigencia = (string)value; break;							
						case "IdGrupoEconomico": this.str.IdGrupoEconomico = (string)value; break;							
						case "IdLocalCustodia": this.str.IdLocalCustodia = (string)value; break;							
						case "IdClearing": this.str.IdClearing = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "CodigoCusip": this.str.CodigoCusip = (string)value; break;							
						case "CodigoBDS": this.str.CodigoBDS = (string)value; break;
                        case "InvestimentoColetivoCvm": this.str.InvestimentoColetivoCvm = (string)value; break;
                        case "Participacao": this.str.Participacao = (string)value; break;
                    }
				}
				else
				{
					switch (name)
					{	
						case "PUExercicio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUExercicio = (System.Decimal?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "IdEmissor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEmissor = (System.Int32?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdMoeda = (System.Int16?)value;
							break;
						
						case "OpcaoIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.OpcaoIndice = (System.Int16?)value;
							break;
						
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "CodigoCDA":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoCDA = (System.Int32?)value;
							break;
						
						case "NumeroSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroSerie = (System.Int32?)value;
							break;
						
						case "TipoPapel":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPapel = (System.Byte?)value;
							break;
						
						case "DataInicioVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioVigencia = (System.DateTime?)value;
							break;
						
						case "DataFimVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimVigencia = (System.DateTime?)value;
							break;
						
						case "IdGrupoEconomico":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupoEconomico = (System.Int32?)value;
							break;
						
						case "IdLocalCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalCustodia = (System.Int32?)value;
							break;
						
						case "IdClearing":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClearing = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "CodigoBDS":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoBDS = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AtivoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				base.SetSystemString(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.Especificacao
		/// </summary>
		virtual public System.String Especificacao
		{
			get
			{
				return base.GetSystemString(AtivoBolsaMetadata.ColumnNames.Especificacao);
			}
			
			set
			{
				base.SetSystemString(AtivoBolsaMetadata.ColumnNames.Especificacao, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(AtivoBolsaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(AtivoBolsaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.TipoMercado
		/// </summary>
		virtual public System.String TipoMercado
		{
			get
			{
				return base.GetSystemString(AtivoBolsaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemString(AtivoBolsaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.PUExercicio
		/// </summary>
		virtual public System.Decimal? PUExercicio
		{
			get
			{
				return base.GetSystemDecimal(AtivoBolsaMetadata.ColumnNames.PUExercicio);
			}
			
			set
			{
				base.SetSystemDecimal(AtivoBolsaMetadata.ColumnNames.PUExercicio, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(AtivoBolsaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(AtivoBolsaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.CdAtivoBolsaObjeto
		/// </summary>
		virtual public System.String CdAtivoBolsaObjeto
		{
			get
			{
				return base.GetSystemString(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto);
			}
			
			set
			{
				base.SetSystemString(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.CodigoIsin
		/// </summary>
		virtual public System.String CodigoIsin
		{
			get
			{
				return base.GetSystemString(AtivoBolsaMetadata.ColumnNames.CodigoIsin);
			}
			
			set
			{
				base.SetSystemString(AtivoBolsaMetadata.ColumnNames.CodigoIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.IdEmissor
		/// </summary>
		virtual public System.Int32? IdEmissor
		{
			get
			{
				return base.GetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdEmissor);
			}
			
			set
			{
				if(base.SetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdEmissor, value))
				{
					this._UpToEmissorByIdEmissor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.IdMoeda
		/// </summary>
		virtual public System.Int16? IdMoeda
		{
			get
			{
				return base.GetSystemInt16(AtivoBolsaMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				if(base.SetSystemInt16(AtivoBolsaMetadata.ColumnNames.IdMoeda, value))
				{
					this._UpToMoedaByIdMoeda = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.OpcaoIndice
		/// </summary>
		virtual public System.Int16? OpcaoIndice
		{
			get
			{
				return base.GetSystemInt16(AtivoBolsaMetadata.ColumnNames.OpcaoIndice);
			}
			
			set
			{
				base.SetSystemInt16(AtivoBolsaMetadata.ColumnNames.OpcaoIndice, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				if(base.SetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdEstrategia, value))
				{
					this._UpToEstrategiaByIdEstrategia = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.CodigoCDA
		/// </summary>
		virtual public System.Int32? CodigoCDA
		{
			get
			{
				return base.GetSystemInt32(AtivoBolsaMetadata.ColumnNames.CodigoCDA);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBolsaMetadata.ColumnNames.CodigoCDA, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.NumeroSerie
		/// </summary>
		virtual public System.Int32? NumeroSerie
		{
			get
			{
				return base.GetSystemInt32(AtivoBolsaMetadata.ColumnNames.NumeroSerie);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBolsaMetadata.ColumnNames.NumeroSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.TipoPapel
		/// </summary>
		virtual public System.Byte? TipoPapel
		{
			get
			{
				return base.GetSystemByte(AtivoBolsaMetadata.ColumnNames.TipoPapel);
			}
			
			set
			{
				base.SetSystemByte(AtivoBolsaMetadata.ColumnNames.TipoPapel, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.DataInicioVigencia
		/// </summary>
		virtual public System.DateTime? DataInicioVigencia
		{
			get
			{
				return base.GetSystemDateTime(AtivoBolsaMetadata.ColumnNames.DataInicioVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(AtivoBolsaMetadata.ColumnNames.DataInicioVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.DataFimVigencia
		/// </summary>
		virtual public System.DateTime? DataFimVigencia
		{
			get
			{
				return base.GetSystemDateTime(AtivoBolsaMetadata.ColumnNames.DataFimVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(AtivoBolsaMetadata.ColumnNames.DataFimVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.IdGrupoEconomico
		/// </summary>
		virtual public System.Int32? IdGrupoEconomico
		{
			get
			{
				return base.GetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdGrupoEconomico);
			}
			
			set
			{
				if(base.SetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdGrupoEconomico, value))
				{
					this._UpToGrupoEconomicoByIdGrupoEconomico = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.IdLocalCustodia
		/// </summary>
		virtual public System.Int32? IdLocalCustodia
		{
			get
			{
				return base.GetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdLocalCustodia);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdLocalCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.IdClearing
		/// </summary>
		virtual public System.Int32? IdClearing
		{
			get
			{
				return base.GetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdClearing);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdClearing, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBolsaMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.CodigoCusip
		/// </summary>
		virtual public System.String CodigoCusip
		{
			get
			{
				return base.GetSystemString(AtivoBolsaMetadata.ColumnNames.CodigoCusip);
			}
			
			set
			{
				base.SetSystemString(AtivoBolsaMetadata.ColumnNames.CodigoCusip, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsa.CodigoBDS
		/// </summary>
		virtual public System.Int32? CodigoBDS
		{
			get
			{
				return base.GetSystemInt32(AtivoBolsaMetadata.ColumnNames.CodigoBDS);
			}
			
			set
			{
				base.SetSystemInt32(AtivoBolsaMetadata.ColumnNames.CodigoBDS, value);
			}
		}

        /// <summary>
        /// Maps to AtivoBolsa.InvestimentoColetivoCvm
        /// </summary>
        virtual public System.String InvestimentoColetivoCvm
        {
            get
            {
                return base.GetSystemString(AtivoBolsaMetadata.ColumnNames.InvestimentoColetivoCvm);
            }

            set
            {
                base.SetSystemString(AtivoBolsaMetadata.ColumnNames.InvestimentoColetivoCvm, value);
            }
        }

        [CLSCompliant(false)]
		
		/// <summary>
		/// Maps to AtivoBolsa.Participacao
		/// </summary>
		virtual public System.String Participacao
		{
			get
			{
				return base.GetSystemString(AtivoBolsaMetadata.ColumnNames.Participacao);
			}
			
			set
			{
				base.SetSystemString(AtivoBolsaMetadata.ColumnNames.Participacao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Emissor _UpToEmissorByIdEmissor;
		[CLSCompliant(false)]
		internal protected Estrategia _UpToEstrategiaByIdEstrategia;
		[CLSCompliant(false)]
		internal protected GrupoEconomico _UpToGrupoEconomicoByIdGrupoEconomico;
		[CLSCompliant(false)]
		internal protected Moeda _UpToMoedaByIdMoeda;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAtivoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Especificacao
			{
				get
				{
					System.String data = entity.Especificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Especificacao = null;
					else entity.Especificacao = Convert.ToString(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.String data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToString(value);
				}
			}
				
			public System.String PUExercicio
			{
				get
				{
					System.Decimal? data = entity.PUExercicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUExercicio = null;
					else entity.PUExercicio = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBolsaObjeto
			{
				get
				{
					System.String data = entity.CdAtivoBolsaObjeto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaObjeto = null;
					else entity.CdAtivoBolsaObjeto = Convert.ToString(value);
				}
			}
				
			public System.String CodigoIsin
			{
				get
				{
					System.String data = entity.CodigoIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoIsin = null;
					else entity.CodigoIsin = Convert.ToString(value);
				}
			}
				
			public System.String IdEmissor
			{
				get
				{
					System.Int32? data = entity.IdEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEmissor = null;
					else entity.IdEmissor = Convert.ToInt32(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int16? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt16(value);
				}
			}
				
			public System.String OpcaoIndice
			{
				get
				{
					System.Int16? data = entity.OpcaoIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OpcaoIndice = null;
					else entity.OpcaoIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCDA
			{
				get
				{
					System.Int32? data = entity.CodigoCDA;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCDA = null;
					else entity.CodigoCDA = Convert.ToInt32(value);
				}
			}
				
			public System.String NumeroSerie
			{
				get
				{
					System.Int32? data = entity.NumeroSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroSerie = null;
					else entity.NumeroSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoPapel
			{
				get
				{
					System.Byte? data = entity.TipoPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPapel = null;
					else entity.TipoPapel = Convert.ToByte(value);
				}
			}
				
			public System.String DataInicioVigencia
			{
				get
				{
					System.DateTime? data = entity.DataInicioVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioVigencia = null;
					else entity.DataInicioVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFimVigencia
			{
				get
				{
					System.DateTime? data = entity.DataFimVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimVigencia = null;
					else entity.DataFimVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdGrupoEconomico
			{
				get
				{
					System.Int32? data = entity.IdGrupoEconomico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoEconomico = null;
					else entity.IdGrupoEconomico = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalCustodia
			{
				get
				{
					System.Int32? data = entity.IdLocalCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalCustodia = null;
					else entity.IdLocalCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdClearing
			{
				get
				{
					System.Int32? data = entity.IdClearing;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClearing = null;
					else entity.IdClearing = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCusip
			{
				get
				{
					System.String data = entity.CodigoCusip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCusip = null;
					else entity.CodigoCusip = Convert.ToString(value);
				}
			}
				
			public System.String CodigoBDS
			{
				get
				{
					System.Int32? data = entity.CodigoBDS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoBDS = null;
					else entity.CodigoBDS = Convert.ToInt32(value);
				}
			}
				
			public System.String Participacao
			{
				get
				{
					System.String data = entity.Participacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Participacao = null;
					else entity.Participacao = Convert.ToString(value);
				}
			}
			
            public System.String InvestimentoColetivoCvm
            {
                get
                {
                    System.String data = entity.InvestimentoColetivoCvm;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.InvestimentoColetivoCvm = null;
                    else entity.InvestimentoColetivoCvm = Convert.ToString(value);
                }
            }

            private esAtivoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAtivoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAtivoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AtivoBolsa : esAtivoBolsa
	{

				
		#region AtivoBolsaHistoricoCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_AtivoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsaHistoricoCollection AtivoBolsaHistoricoCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa == null)
				{
					this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa = new AtivoBolsaHistoricoCollection();
					this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AtivoBolsaHistoricoCollectionByCdAtivoBolsa", this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Where(this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa.fks.Add(AtivoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("AtivoBolsaHistoricoCollectionByCdAtivoBolsa"); 
					this._AtivoBolsaHistoricoCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private AtivoBolsaHistoricoCollection _AtivoBolsaHistoricoCollectionByCdAtivoBolsa;
		#endregion

				
		#region BloqueioBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_BloqueioBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public BloqueioBolsaCollection BloqueioBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._BloqueioBolsaCollectionByCdAtivoBolsa == null)
				{
					this._BloqueioBolsaCollectionByCdAtivoBolsa = new BloqueioBolsaCollection();
					this._BloqueioBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("BloqueioBolsaCollectionByCdAtivoBolsa", this._BloqueioBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._BloqueioBolsaCollectionByCdAtivoBolsa.Query.Where(this._BloqueioBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._BloqueioBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._BloqueioBolsaCollectionByCdAtivoBolsa.fks.Add(BloqueioBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._BloqueioBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._BloqueioBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("BloqueioBolsaCollectionByCdAtivoBolsa"); 
					this._BloqueioBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private BloqueioBolsaCollection _BloqueioBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region BonificacaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_BonificacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public BonificacaoBolsaCollection BonificacaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._BonificacaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._BonificacaoBolsaCollectionByCdAtivoBolsa = new BonificacaoBolsaCollection();
					this._BonificacaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("BonificacaoBolsaCollectionByCdAtivoBolsa", this._BonificacaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._BonificacaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._BonificacaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._BonificacaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._BonificacaoBolsaCollectionByCdAtivoBolsa.fks.Add(BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._BonificacaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._BonificacaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("BonificacaoBolsaCollectionByCdAtivoBolsa"); 
					this._BonificacaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private BonificacaoBolsaCollection _BonificacaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region BonificacaoBolsaCollectionByCdAtivoBolsaDestino - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_BonificacaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public BonificacaoBolsaCollection BonificacaoBolsaCollectionByCdAtivoBolsaDestino
		{
			get
			{
				if(this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino == null)
				{
					this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino = new BonificacaoBolsaCollection();
					this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("BonificacaoBolsaCollectionByCdAtivoBolsaDestino", this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino);
				
					if(this.CdAtivoBolsa != null)
					{
						this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino.Query.Where(this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino.Query.CdAtivoBolsaDestino == this.CdAtivoBolsa);
						this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino.Query.Load();

						// Auto-hookup Foreign Keys
						this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino.fks.Add(BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino, this.CdAtivoBolsa);
					}
				}

				return this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino != null) 
				{ 
					this.RemovePostSave("BonificacaoBolsaCollectionByCdAtivoBolsaDestino"); 
					this._BonificacaoBolsaCollectionByCdAtivoBolsaDestino = null;
					
				} 
			} 			
		}

		private BonificacaoBolsaCollection _BonificacaoBolsaCollectionByCdAtivoBolsaDestino;
		#endregion

				
		#region ConversaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_ConversaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public ConversaoBolsaCollection ConversaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._ConversaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._ConversaoBolsaCollectionByCdAtivoBolsa = new ConversaoBolsaCollection();
					this._ConversaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ConversaoBolsaCollectionByCdAtivoBolsa", this._ConversaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._ConversaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._ConversaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._ConversaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._ConversaoBolsaCollectionByCdAtivoBolsa.fks.Add(ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._ConversaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ConversaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("ConversaoBolsaCollectionByCdAtivoBolsa"); 
					this._ConversaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private ConversaoBolsaCollection _ConversaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region ConversaoBolsaCollectionByCdAtivoBolsaDestino - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_ConversaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public ConversaoBolsaCollection ConversaoBolsaCollectionByCdAtivoBolsaDestino
		{
			get
			{
				if(this._ConversaoBolsaCollectionByCdAtivoBolsaDestino == null)
				{
					this._ConversaoBolsaCollectionByCdAtivoBolsaDestino = new ConversaoBolsaCollection();
					this._ConversaoBolsaCollectionByCdAtivoBolsaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ConversaoBolsaCollectionByCdAtivoBolsaDestino", this._ConversaoBolsaCollectionByCdAtivoBolsaDestino);
				
					if(this.CdAtivoBolsa != null)
					{
						this._ConversaoBolsaCollectionByCdAtivoBolsaDestino.Query.Where(this._ConversaoBolsaCollectionByCdAtivoBolsaDestino.Query.CdAtivoBolsaDestino == this.CdAtivoBolsa);
						this._ConversaoBolsaCollectionByCdAtivoBolsaDestino.Query.Load();

						// Auto-hookup Foreign Keys
						this._ConversaoBolsaCollectionByCdAtivoBolsaDestino.fks.Add(ConversaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino, this.CdAtivoBolsa);
					}
				}

				return this._ConversaoBolsaCollectionByCdAtivoBolsaDestino;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ConversaoBolsaCollectionByCdAtivoBolsaDestino != null) 
				{ 
					this.RemovePostSave("ConversaoBolsaCollectionByCdAtivoBolsaDestino"); 
					this._ConversaoBolsaCollectionByCdAtivoBolsaDestino = null;
					
				} 
			} 			
		}

		private ConversaoBolsaCollection _ConversaoBolsaCollectionByCdAtivoBolsaDestino;
		#endregion

				
		#region CotacaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_CotacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public CotacaoBolsaCollection CotacaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._CotacaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._CotacaoBolsaCollectionByCdAtivoBolsa = new CotacaoBolsaCollection();
					this._CotacaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CotacaoBolsaCollectionByCdAtivoBolsa", this._CotacaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._CotacaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._CotacaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._CotacaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._CotacaoBolsaCollectionByCdAtivoBolsa.fks.Add(CotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._CotacaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CotacaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("CotacaoBolsaCollectionByCdAtivoBolsa"); 
					this._CotacaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private CotacaoBolsaCollection _CotacaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region DistribuicaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_DistribuicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public DistribuicaoBolsaCollection DistribuicaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._DistribuicaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._DistribuicaoBolsaCollectionByCdAtivoBolsa = new DistribuicaoBolsaCollection();
					this._DistribuicaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("DistribuicaoBolsaCollectionByCdAtivoBolsa", this._DistribuicaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._DistribuicaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._DistribuicaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._DistribuicaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._DistribuicaoBolsaCollectionByCdAtivoBolsa.fks.Add(DistribuicaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._DistribuicaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._DistribuicaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("DistribuicaoBolsaCollectionByCdAtivoBolsa"); 
					this._DistribuicaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private DistribuicaoBolsaCollection _DistribuicaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region EventoFisicoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_EventoFisicoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public EventoFisicoBolsaCollection EventoFisicoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._EventoFisicoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._EventoFisicoBolsaCollectionByCdAtivoBolsa = new EventoFisicoBolsaCollection();
					this._EventoFisicoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoFisicoBolsaCollectionByCdAtivoBolsa", this._EventoFisicoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._EventoFisicoBolsaCollectionByCdAtivoBolsa.Query.Where(this._EventoFisicoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._EventoFisicoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoFisicoBolsaCollectionByCdAtivoBolsa.fks.Add(EventoFisicoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._EventoFisicoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoFisicoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("EventoFisicoBolsaCollectionByCdAtivoBolsa"); 
					this._EventoFisicoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private EventoFisicoBolsaCollection _EventoFisicoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region EvolucaoCapitalSocialCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_EvolucaoCapitalSocial_AtivoBolsa
		/// </summary>

		[XmlIgnore]
		public EvolucaoCapitalSocialCollection EvolucaoCapitalSocialCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa == null)
				{
					this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa = new EvolucaoCapitalSocialCollection();
					this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EvolucaoCapitalSocialCollectionByCdAtivoBolsa", this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa.Query.Where(this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa.fks.Add(EvolucaoCapitalSocialMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("EvolucaoCapitalSocialCollectionByCdAtivoBolsa"); 
					this._EvolucaoCapitalSocialCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private EvolucaoCapitalSocialCollection _EvolucaoCapitalSocialCollectionByCdAtivoBolsa;
		#endregion

				
		#region ExcecoesTributacaoIRCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__ExcecoesT__CdAti__1F6E958F
		/// </summary>

		[XmlIgnore]
		public ExcecoesTributacaoIRCollection ExcecoesTributacaoIRCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa == null)
				{
					this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa = new ExcecoesTributacaoIRCollection();
					this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ExcecoesTributacaoIRCollectionByCdAtivoBolsa", this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa.Query.Where(this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa.fks.Add(ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("ExcecoesTributacaoIRCollectionByCdAtivoBolsa"); 
					this._ExcecoesTributacaoIRCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private ExcecoesTributacaoIRCollection _ExcecoesTributacaoIRCollectionByCdAtivoBolsa;
		#endregion

				
		#region FatorCotacaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_FatorCotacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public FatorCotacaoBolsaCollection FatorCotacaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._FatorCotacaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._FatorCotacaoBolsaCollectionByCdAtivoBolsa = new FatorCotacaoBolsaCollection();
					this._FatorCotacaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("FatorCotacaoBolsaCollectionByCdAtivoBolsa", this._FatorCotacaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._FatorCotacaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._FatorCotacaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._FatorCotacaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._FatorCotacaoBolsaCollectionByCdAtivoBolsa.fks.Add(FatorCotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._FatorCotacaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._FatorCotacaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("FatorCotacaoBolsaCollectionByCdAtivoBolsa"); 
					this._FatorCotacaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private FatorCotacaoBolsaCollection _FatorCotacaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region GerOperacaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_GerOperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public GerOperacaoBolsaCollection GerOperacaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._GerOperacaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._GerOperacaoBolsaCollectionByCdAtivoBolsa = new GerOperacaoBolsaCollection();
					this._GerOperacaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerOperacaoBolsaCollectionByCdAtivoBolsa", this._GerOperacaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._GerOperacaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._GerOperacaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._GerOperacaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerOperacaoBolsaCollectionByCdAtivoBolsa.fks.Add(GerOperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._GerOperacaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerOperacaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("GerOperacaoBolsaCollectionByCdAtivoBolsa"); 
					this._GerOperacaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private GerOperacaoBolsaCollection _GerOperacaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region GerPosicaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_GerPosicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBolsaCollection GerPosicaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._GerPosicaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._GerPosicaoBolsaCollectionByCdAtivoBolsa = new GerPosicaoBolsaCollection();
					this._GerPosicaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBolsaCollectionByCdAtivoBolsa", this._GerPosicaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._GerPosicaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._GerPosicaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._GerPosicaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBolsaCollectionByCdAtivoBolsa.fks.Add(GerPosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._GerPosicaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("GerPosicaoBolsaCollectionByCdAtivoBolsa"); 
					this._GerPosicaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private GerPosicaoBolsaCollection _GerPosicaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_GerPosicaoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBolsaAberturaCollection GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa == null)
				{
					this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa = new GerPosicaoBolsaAberturaCollection();
					this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa", this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa.Query.Where(this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa.fks.Add(GerPosicaoBolsaAberturaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa"); 
					this._GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private GerPosicaoBolsaAberturaCollection _GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa;
		#endregion

				
		#region GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_GerPosicaoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public GerPosicaoBolsaHistoricoCollection GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa == null)
				{
					this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa = new GerPosicaoBolsaHistoricoCollection();
					this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa", this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Where(this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa.fks.Add(GerPosicaoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa"); 
					this._GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private GerPosicaoBolsaHistoricoCollection _GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa;
		#endregion

				
		#region GrupamentoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_Grupamento_FK1
		/// </summary>

		[XmlIgnore]
		public GrupamentoBolsaCollection GrupamentoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._GrupamentoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._GrupamentoBolsaCollectionByCdAtivoBolsa = new GrupamentoBolsaCollection();
					this._GrupamentoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("GrupamentoBolsaCollectionByCdAtivoBolsa", this._GrupamentoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._GrupamentoBolsaCollectionByCdAtivoBolsa.Query.Where(this._GrupamentoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._GrupamentoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._GrupamentoBolsaCollectionByCdAtivoBolsa.fks.Add(GrupamentoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._GrupamentoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._GrupamentoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("GrupamentoBolsaCollectionByCdAtivoBolsa"); 
					this._GrupamentoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private GrupamentoBolsaCollection _GrupamentoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - IncorporacaoCisaoFusaoBolsa_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public IncorporacaoCisaoFusaoBolsaCollection IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa = new IncorporacaoCisaoFusaoBolsaCollection();
					this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa", this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa.fks.Add(IncorporacaoCisaoFusaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa"); 
					this._IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private IncorporacaoCisaoFusaoBolsaCollection _IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - IncorporacaoCisaoFusaoBolsaDetalhe_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public IncorporacaoCisaoFusaoBolsaDetalheCollection IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa == null)
				{
					this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa = new IncorporacaoCisaoFusaoBolsaDetalheCollection();
					this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa", this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa.Query.Where(this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa.fks.Add(IncorporacaoCisaoFusaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa"); 
					this._IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private IncorporacaoCisaoFusaoBolsaDetalheCollection _IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa;
		#endregion

				
		#region LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_AntecipacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoEmprestimoBolsaCollection LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa = new LiquidacaoEmprestimoBolsaCollection();
					this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa", this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa.Query.Where(this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa.fks.Add(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa"); 
					this._LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private LiquidacaoEmprestimoBolsaCollection _LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region LiquidacaoTermoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_LiquidacaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoTermoBolsaCollection LiquidacaoTermoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa = new LiquidacaoTermoBolsaCollection();
					this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoTermoBolsaCollectionByCdAtivoBolsa", this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa.Query.Where(this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa.fks.Add(LiquidacaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("LiquidacaoTermoBolsaCollectionByCdAtivoBolsa"); 
					this._LiquidacaoTermoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private LiquidacaoTermoBolsaCollection _LiquidacaoTermoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region OperacaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_OperacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoBolsaCollection OperacaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._OperacaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._OperacaoBolsaCollectionByCdAtivoBolsa = new OperacaoBolsaCollection();
					this._OperacaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoBolsaCollectionByCdAtivoBolsa", this._OperacaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._OperacaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._OperacaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._OperacaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoBolsaCollectionByCdAtivoBolsa.fks.Add(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._OperacaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("OperacaoBolsaCollectionByCdAtivoBolsa"); 
					this._OperacaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private OperacaoBolsaCollection _OperacaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region OperacaoBolsaCollectionByCdAtivoBolsaOpcao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_OperacaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public OperacaoBolsaCollection OperacaoBolsaCollectionByCdAtivoBolsaOpcao
		{
			get
			{
				if(this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao == null)
				{
					this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao = new OperacaoBolsaCollection();
					this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoBolsaCollectionByCdAtivoBolsaOpcao", this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao);
				
					if(this.CdAtivoBolsa != null)
					{
						this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao.Query.Where(this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao.Query.CdAtivoBolsaOpcao == this.CdAtivoBolsa);
						this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao.fks.Add(OperacaoBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, this.CdAtivoBolsa);
					}
				}

				return this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao != null) 
				{ 
					this.RemovePostSave("OperacaoBolsaCollectionByCdAtivoBolsaOpcao"); 
					this._OperacaoBolsaCollectionByCdAtivoBolsaOpcao = null;
					
				} 
			} 			
		}

		private OperacaoBolsaCollection _OperacaoBolsaCollectionByCdAtivoBolsaOpcao;
		#endregion

				
		#region OrdemBolsaCollectionByCdAtivoBolsaOpcao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_OrdemBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public OrdemBolsaCollection OrdemBolsaCollectionByCdAtivoBolsaOpcao
		{
			get
			{
				if(this._OrdemBolsaCollectionByCdAtivoBolsaOpcao == null)
				{
					this._OrdemBolsaCollectionByCdAtivoBolsaOpcao = new OrdemBolsaCollection();
					this._OrdemBolsaCollectionByCdAtivoBolsaOpcao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBolsaCollectionByCdAtivoBolsaOpcao", this._OrdemBolsaCollectionByCdAtivoBolsaOpcao);
				
					if(this.CdAtivoBolsa != null)
					{
						this._OrdemBolsaCollectionByCdAtivoBolsaOpcao.Query.Where(this._OrdemBolsaCollectionByCdAtivoBolsaOpcao.Query.CdAtivoBolsaOpcao == this.CdAtivoBolsa);
						this._OrdemBolsaCollectionByCdAtivoBolsaOpcao.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBolsaCollectionByCdAtivoBolsaOpcao.fks.Add(OrdemBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, this.CdAtivoBolsa);
					}
				}

				return this._OrdemBolsaCollectionByCdAtivoBolsaOpcao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBolsaCollectionByCdAtivoBolsaOpcao != null) 
				{ 
					this.RemovePostSave("OrdemBolsaCollectionByCdAtivoBolsaOpcao"); 
					this._OrdemBolsaCollectionByCdAtivoBolsaOpcao = null;
					
				} 
			} 			
		}

		private OrdemBolsaCollection _OrdemBolsaCollectionByCdAtivoBolsaOpcao;
		#endregion

				
		#region OrdemBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_OrdemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemBolsaCollection OrdemBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._OrdemBolsaCollectionByCdAtivoBolsa == null)
				{
					this._OrdemBolsaCollectionByCdAtivoBolsa = new OrdemBolsaCollection();
					this._OrdemBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemBolsaCollectionByCdAtivoBolsa", this._OrdemBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._OrdemBolsaCollectionByCdAtivoBolsa.Query.Where(this._OrdemBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._OrdemBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemBolsaCollectionByCdAtivoBolsa.fks.Add(OrdemBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._OrdemBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("OrdemBolsaCollectionByCdAtivoBolsa"); 
					this._OrdemBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private OrdemBolsaCollection _OrdemBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaCollection PosicaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoBolsaCollectionByCdAtivoBolsa = new PosicaoBolsaCollection();
					this._PosicaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaCollectionByCdAtivoBolsa", this._PosicaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._PosicaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaCollectionByCdAtivoBolsa.fks.Add(PosicaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaCollectionByCdAtivoBolsa"); 
					this._PosicaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoBolsaCollection _PosicaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoBolsaAberturaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaAberturaCollection PosicaoBolsaAberturaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa = new PosicaoBolsaAberturaCollection();
					this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaAberturaCollectionByCdAtivoBolsa", this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa.Query.Where(this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa.fks.Add(PosicaoBolsaAberturaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaAberturaCollectionByCdAtivoBolsa"); 
					this._PosicaoBolsaAberturaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoBolsaAberturaCollection _PosicaoBolsaAberturaCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoBolsaDetalheCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoBolsaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaDetalheCollection PosicaoBolsaDetalheCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa = new PosicaoBolsaDetalheCollection();
					this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaDetalheCollectionByCdAtivoBolsa", this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa.Query.Where(this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa.fks.Add(PosicaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaDetalheCollectionByCdAtivoBolsa"); 
					this._PosicaoBolsaDetalheCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoBolsaDetalheCollection _PosicaoBolsaDetalheCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoBolsaHistoricoCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoBolsaHistoricoCollection PosicaoBolsaHistoricoCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa = new PosicaoBolsaHistoricoCollection();
					this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoBolsaHistoricoCollectionByCdAtivoBolsa", this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Where(this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa.fks.Add(PosicaoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoBolsaHistoricoCollectionByCdAtivoBolsa"); 
					this._PosicaoBolsaHistoricoCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoBolsaHistoricoCollection _PosicaoBolsaHistoricoCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaCollection PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa = new PosicaoEmprestimoBolsaCollection();
					this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa", this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa.Query.Where(this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa.fks.Add(PosicaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa"); 
					this._PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaCollection _PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoEmprestimoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaAberturaCollection PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa = new PosicaoEmprestimoBolsaAberturaCollection();
					this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa", this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa.Query.Where(this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa.fks.Add(PosicaoEmprestimoBolsaAberturaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa"); 
					this._PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaAberturaCollection _PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoEmprestimoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoEmprestimoBolsaHistoricoCollection PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa = new PosicaoEmprestimoBolsaHistoricoCollection();
					this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa", this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Where(this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa.fks.Add(PosicaoEmprestimoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa"); 
					this._PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoEmprestimoBolsaHistoricoCollection _PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoTermoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaCollection PosicaoTermoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoTermoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoTermoBolsaCollectionByCdAtivoBolsa = new PosicaoTermoBolsaCollection();
					this._PosicaoTermoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaCollectionByCdAtivoBolsa", this._PosicaoTermoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoTermoBolsaCollectionByCdAtivoBolsa.Query.Where(this._PosicaoTermoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoTermoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaCollectionByCdAtivoBolsa.fks.Add(PosicaoTermoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoTermoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaCollectionByCdAtivoBolsa"); 
					this._PosicaoTermoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaCollection _PosicaoTermoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoTermoBolsaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaAberturaCollection PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa = new PosicaoTermoBolsaAberturaCollection();
					this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa", this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa.Query.Where(this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa.fks.Add(PosicaoTermoBolsaAberturaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa"); 
					this._PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaAberturaCollection _PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa;
		#endregion

				
		#region PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_PosicaoTermoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoTermoBolsaHistoricoCollection PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa == null)
				{
					this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa = new PosicaoTermoBolsaHistoricoCollection();
					this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa", this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Where(this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa.fks.Add(PosicaoTermoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa"); 
					this._PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private PosicaoTermoBolsaHistoricoCollection _PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa;
		#endregion

				
		#region ProventoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_ProventoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public ProventoBolsaCollection ProventoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._ProventoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._ProventoBolsaCollectionByCdAtivoBolsa = new ProventoBolsaCollection();
					this._ProventoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ProventoBolsaCollectionByCdAtivoBolsa", this._ProventoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._ProventoBolsaCollectionByCdAtivoBolsa.Query.Where(this._ProventoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._ProventoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._ProventoBolsaCollectionByCdAtivoBolsa.fks.Add(ProventoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._ProventoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ProventoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("ProventoBolsaCollectionByCdAtivoBolsa"); 
					this._ProventoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private ProventoBolsaCollection _ProventoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region ProventoBolsaClienteCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_ProventoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public ProventoBolsaClienteCollection ProventoBolsaClienteCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._ProventoBolsaClienteCollectionByCdAtivoBolsa == null)
				{
					this._ProventoBolsaClienteCollectionByCdAtivoBolsa = new ProventoBolsaClienteCollection();
					this._ProventoBolsaClienteCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ProventoBolsaClienteCollectionByCdAtivoBolsa", this._ProventoBolsaClienteCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._ProventoBolsaClienteCollectionByCdAtivoBolsa.Query.Where(this._ProventoBolsaClienteCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._ProventoBolsaClienteCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._ProventoBolsaClienteCollectionByCdAtivoBolsa.fks.Add(ProventoBolsaClienteMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._ProventoBolsaClienteCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ProventoBolsaClienteCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("ProventoBolsaClienteCollectionByCdAtivoBolsa"); 
					this._ProventoBolsaClienteCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private ProventoBolsaClienteCollection _ProventoBolsaClienteCollectionByCdAtivoBolsa;
		#endregion

				
		#region SubscricaoBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_SubscricaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public SubscricaoBolsaCollection SubscricaoBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._SubscricaoBolsaCollectionByCdAtivoBolsa == null)
				{
					this._SubscricaoBolsaCollectionByCdAtivoBolsa = new SubscricaoBolsaCollection();
					this._SubscricaoBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SubscricaoBolsaCollectionByCdAtivoBolsa", this._SubscricaoBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._SubscricaoBolsaCollectionByCdAtivoBolsa.Query.Where(this._SubscricaoBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._SubscricaoBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._SubscricaoBolsaCollectionByCdAtivoBolsa.fks.Add(SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._SubscricaoBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SubscricaoBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("SubscricaoBolsaCollectionByCdAtivoBolsa"); 
					this._SubscricaoBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private SubscricaoBolsaCollection _SubscricaoBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region SubscricaoBolsaCollectionByCdAtivoBolsaDireito - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_SubscricaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public SubscricaoBolsaCollection SubscricaoBolsaCollectionByCdAtivoBolsaDireito
		{
			get
			{
				if(this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito == null)
				{
					this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito = new SubscricaoBolsaCollection();
					this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("SubscricaoBolsaCollectionByCdAtivoBolsaDireito", this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito);
				
					if(this.CdAtivoBolsa != null)
					{
						this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito.Query.Where(this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito.Query.CdAtivoBolsaDireito == this.CdAtivoBolsa);
						this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito.Query.Load();

						// Auto-hookup Foreign Keys
						this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito.fks.Add(SubscricaoBolsaMetadata.ColumnNames.CdAtivoBolsaDireito, this.CdAtivoBolsa);
					}
				}

				return this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito != null) 
				{ 
					this.RemovePostSave("SubscricaoBolsaCollectionByCdAtivoBolsaDireito"); 
					this._SubscricaoBolsaCollectionByCdAtivoBolsaDireito = null;
					
				} 
			} 			
		}

		private SubscricaoBolsaCollection _SubscricaoBolsaCollectionByCdAtivoBolsaDireito;
		#endregion

				
		#region TabelaCONRCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_TabelaCONR_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaCONRCollection TabelaCONRCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._TabelaCONRCollectionByCdAtivoBolsa == null)
				{
					this._TabelaCONRCollectionByCdAtivoBolsa = new TabelaCONRCollection();
					this._TabelaCONRCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaCONRCollectionByCdAtivoBolsa", this._TabelaCONRCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._TabelaCONRCollectionByCdAtivoBolsa.Query.Where(this._TabelaCONRCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._TabelaCONRCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaCONRCollectionByCdAtivoBolsa.fks.Add(TabelaCONRMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._TabelaCONRCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaCONRCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("TabelaCONRCollectionByCdAtivoBolsa"); 
					this._TabelaCONRCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private TabelaCONRCollection _TabelaCONRCollectionByCdAtivoBolsa;
		#endregion

				
		#region TransferenciaBolsaCollectionByCdAtivoBolsa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - AtivoBolsa_TransferenciaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public TransferenciaBolsaCollection TransferenciaBolsaCollectionByCdAtivoBolsa
		{
			get
			{
				if(this._TransferenciaBolsaCollectionByCdAtivoBolsa == null)
				{
					this._TransferenciaBolsaCollectionByCdAtivoBolsa = new TransferenciaBolsaCollection();
					this._TransferenciaBolsaCollectionByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TransferenciaBolsaCollectionByCdAtivoBolsa", this._TransferenciaBolsaCollectionByCdAtivoBolsa);
				
					if(this.CdAtivoBolsa != null)
					{
						this._TransferenciaBolsaCollectionByCdAtivoBolsa.Query.Where(this._TransferenciaBolsaCollectionByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
						this._TransferenciaBolsaCollectionByCdAtivoBolsa.Query.Load();

						// Auto-hookup Foreign Keys
						this._TransferenciaBolsaCollectionByCdAtivoBolsa.fks.Add(TransferenciaBolsaMetadata.ColumnNames.CdAtivoBolsa, this.CdAtivoBolsa);
					}
				}

				return this._TransferenciaBolsaCollectionByCdAtivoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TransferenciaBolsaCollectionByCdAtivoBolsa != null) 
				{ 
					this.RemovePostSave("TransferenciaBolsaCollectionByCdAtivoBolsa"); 
					this._TransferenciaBolsaCollectionByCdAtivoBolsa = null;
					
				} 
			} 			
		}

		private TransferenciaBolsaCollection _TransferenciaBolsaCollectionByCdAtivoBolsa;
		#endregion

				
		#region UpToEmissorByIdEmissor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Emissor_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Emissor UpToEmissorByIdEmissor
		{
			get
			{
				if(this._UpToEmissorByIdEmissor == null
					&& IdEmissor != null					)
				{
					this._UpToEmissorByIdEmissor = new Emissor();
					this._UpToEmissorByIdEmissor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEmissorByIdEmissor", this._UpToEmissorByIdEmissor);
					this._UpToEmissorByIdEmissor.Query.Where(this._UpToEmissorByIdEmissor.Query.IdEmissor == this.IdEmissor);
					this._UpToEmissorByIdEmissor.Query.Load();
				}

				return this._UpToEmissorByIdEmissor;
			}
			
			set
			{
				this.RemovePreSave("UpToEmissorByIdEmissor");
				

				if(value == null)
				{
					this.IdEmissor = null;
					this._UpToEmissorByIdEmissor = null;
				}
				else
				{
					this.IdEmissor = value.IdEmissor;
					this._UpToEmissorByIdEmissor = value;
					this.SetPreSave("UpToEmissorByIdEmissor", this._UpToEmissorByIdEmissor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEstrategiaByIdEstrategia - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Estrategia_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Estrategia UpToEstrategiaByIdEstrategia
		{
			get
			{
				if(this._UpToEstrategiaByIdEstrategia == null
					&& IdEstrategia != null					)
				{
					this._UpToEstrategiaByIdEstrategia = new Estrategia();
					this._UpToEstrategiaByIdEstrategia.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Where(this._UpToEstrategiaByIdEstrategia.Query.IdEstrategia == this.IdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Load();
				}

				return this._UpToEstrategiaByIdEstrategia;
			}
			
			set
			{
				this.RemovePreSave("UpToEstrategiaByIdEstrategia");
				

				if(value == null)
				{
					this.IdEstrategia = null;
					this._UpToEstrategiaByIdEstrategia = null;
				}
				else
				{
					this.IdEstrategia = value.IdEstrategia;
					this._UpToEstrategiaByIdEstrategia = value;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
				}
				
			}
		}
		#endregion
		

				
		#region UpToGrupoEconomicoByIdGrupoEconomico - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_GrupoEconomico_FK1
		/// </summary>

		[XmlIgnore]
		public GrupoEconomico UpToGrupoEconomicoByIdGrupoEconomico
		{
			get
			{
				if(this._UpToGrupoEconomicoByIdGrupoEconomico == null
					&& IdGrupoEconomico != null					)
				{
					this._UpToGrupoEconomicoByIdGrupoEconomico = new GrupoEconomico();
					this._UpToGrupoEconomicoByIdGrupoEconomico.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrupoEconomicoByIdGrupoEconomico", this._UpToGrupoEconomicoByIdGrupoEconomico);
					this._UpToGrupoEconomicoByIdGrupoEconomico.Query.Where(this._UpToGrupoEconomicoByIdGrupoEconomico.Query.IdGrupo == this.IdGrupoEconomico);
					this._UpToGrupoEconomicoByIdGrupoEconomico.Query.Load();
				}

				return this._UpToGrupoEconomicoByIdGrupoEconomico;
			}
			
			set
			{
				this.RemovePreSave("UpToGrupoEconomicoByIdGrupoEconomico");
				

				if(value == null)
				{
					this.IdGrupoEconomico = null;
					this._UpToGrupoEconomicoByIdGrupoEconomico = null;
				}
				else
				{
					this.IdGrupoEconomico = value.IdGrupo;
					this._UpToGrupoEconomicoByIdGrupoEconomico = value;
					this.SetPreSave("UpToGrupoEconomicoByIdGrupoEconomico", this._UpToGrupoEconomicoByIdGrupoEconomico);
				}
				
			}
		}
		#endregion
		

				
		#region UpToMoedaByIdMoeda - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Moeda_AtivoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Moeda UpToMoedaByIdMoeda
		{
			get
			{
				if(this._UpToMoedaByIdMoeda == null
					&& IdMoeda != null					)
				{
					this._UpToMoedaByIdMoeda = new Moeda();
					this._UpToMoedaByIdMoeda.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToMoedaByIdMoeda", this._UpToMoedaByIdMoeda);
					this._UpToMoedaByIdMoeda.Query.Where(this._UpToMoedaByIdMoeda.Query.IdMoeda == this.IdMoeda);
					this._UpToMoedaByIdMoeda.Query.Load();
				}

				return this._UpToMoedaByIdMoeda;
			}
			
			set
			{
				this.RemovePreSave("UpToMoedaByIdMoeda");
				

				if(value == null)
				{
					this.IdMoeda = null;
					this._UpToMoedaByIdMoeda = null;
				}
				else
				{
					this.IdMoeda = value.IdMoeda;
					this._UpToMoedaByIdMoeda = value;
					this.SetPreSave("UpToMoedaByIdMoeda", this._UpToMoedaByIdMoeda);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AtivoBolsaHistoricoCollectionByCdAtivoBolsa", typeof(AtivoBolsaHistoricoCollection), new AtivoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "BloqueioBolsaCollectionByCdAtivoBolsa", typeof(BloqueioBolsaCollection), new BloqueioBolsa()));
			props.Add(new esPropertyDescriptor(this, "BonificacaoBolsaCollectionByCdAtivoBolsa", typeof(BonificacaoBolsaCollection), new BonificacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "BonificacaoBolsaCollectionByCdAtivoBolsaDestino", typeof(BonificacaoBolsaCollection), new BonificacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "ConversaoBolsaCollectionByCdAtivoBolsa", typeof(ConversaoBolsaCollection), new ConversaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "ConversaoBolsaCollectionByCdAtivoBolsaDestino", typeof(ConversaoBolsaCollection), new ConversaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "CotacaoBolsaCollectionByCdAtivoBolsa", typeof(CotacaoBolsaCollection), new CotacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "DistribuicaoBolsaCollectionByCdAtivoBolsa", typeof(DistribuicaoBolsaCollection), new DistribuicaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "EventoFisicoBolsaCollectionByCdAtivoBolsa", typeof(EventoFisicoBolsaCollection), new EventoFisicoBolsa()));
			props.Add(new esPropertyDescriptor(this, "EvolucaoCapitalSocialCollectionByCdAtivoBolsa", typeof(EvolucaoCapitalSocialCollection), new EvolucaoCapitalSocial()));
			props.Add(new esPropertyDescriptor(this, "ExcecoesTributacaoIRCollectionByCdAtivoBolsa", typeof(ExcecoesTributacaoIRCollection), new ExcecoesTributacaoIR()));
			props.Add(new esPropertyDescriptor(this, "FatorCotacaoBolsaCollectionByCdAtivoBolsa", typeof(FatorCotacaoBolsaCollection), new FatorCotacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "GerOperacaoBolsaCollectionByCdAtivoBolsa", typeof(GerOperacaoBolsaCollection), new GerOperacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBolsaCollectionByCdAtivoBolsa", typeof(GerPosicaoBolsaCollection), new GerPosicaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBolsaAberturaCollectionByCdAtivoBolsa", typeof(GerPosicaoBolsaAberturaCollection), new GerPosicaoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "GerPosicaoBolsaHistoricoCollectionByCdAtivoBolsa", typeof(GerPosicaoBolsaHistoricoCollection), new GerPosicaoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "GrupamentoBolsaCollectionByCdAtivoBolsa", typeof(GrupamentoBolsaCollection), new GrupamentoBolsa()));
			props.Add(new esPropertyDescriptor(this, "IncorporacaoCisaoFusaoBolsaCollectionByCdAtivoBolsa", typeof(IncorporacaoCisaoFusaoBolsaCollection), new IncorporacaoCisaoFusaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "IncorporacaoCisaoFusaoBolsaDetalheCollectionByCdAtivoBolsa", typeof(IncorporacaoCisaoFusaoBolsaDetalheCollection), new IncorporacaoCisaoFusaoBolsaDetalhe()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoEmprestimoBolsaCollectionByCdAtivoBolsa", typeof(LiquidacaoEmprestimoBolsaCollection), new LiquidacaoEmprestimoBolsa()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoTermoBolsaCollectionByCdAtivoBolsa", typeof(LiquidacaoTermoBolsaCollection), new LiquidacaoTermoBolsa()));
			props.Add(new esPropertyDescriptor(this, "OperacaoBolsaCollectionByCdAtivoBolsa", typeof(OperacaoBolsaCollection), new OperacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "OperacaoBolsaCollectionByCdAtivoBolsaOpcao", typeof(OperacaoBolsaCollection), new OperacaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "OrdemBolsaCollectionByCdAtivoBolsaOpcao", typeof(OrdemBolsaCollection), new OrdemBolsa()));
			props.Add(new esPropertyDescriptor(this, "OrdemBolsaCollectionByCdAtivoBolsa", typeof(OrdemBolsaCollection), new OrdemBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaCollectionByCdAtivoBolsa", typeof(PosicaoBolsaCollection), new PosicaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaAberturaCollectionByCdAtivoBolsa", typeof(PosicaoBolsaAberturaCollection), new PosicaoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaDetalheCollectionByCdAtivoBolsa", typeof(PosicaoBolsaDetalheCollection), new PosicaoBolsaDetalhe()));
			props.Add(new esPropertyDescriptor(this, "PosicaoBolsaHistoricoCollectionByCdAtivoBolsa", typeof(PosicaoBolsaHistoricoCollection), new PosicaoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaCollectionByCdAtivoBolsa", typeof(PosicaoEmprestimoBolsaCollection), new PosicaoEmprestimoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaAberturaCollectionByCdAtivoBolsa", typeof(PosicaoEmprestimoBolsaAberturaCollection), new PosicaoEmprestimoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoEmprestimoBolsaHistoricoCollectionByCdAtivoBolsa", typeof(PosicaoEmprestimoBolsaHistoricoCollection), new PosicaoEmprestimoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaCollectionByCdAtivoBolsa", typeof(PosicaoTermoBolsaCollection), new PosicaoTermoBolsa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaAberturaCollectionByCdAtivoBolsa", typeof(PosicaoTermoBolsaAberturaCollection), new PosicaoTermoBolsaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoTermoBolsaHistoricoCollectionByCdAtivoBolsa", typeof(PosicaoTermoBolsaHistoricoCollection), new PosicaoTermoBolsaHistorico()));
			props.Add(new esPropertyDescriptor(this, "ProventoBolsaCollectionByCdAtivoBolsa", typeof(ProventoBolsaCollection), new ProventoBolsa()));
			props.Add(new esPropertyDescriptor(this, "ProventoBolsaClienteCollectionByCdAtivoBolsa", typeof(ProventoBolsaClienteCollection), new ProventoBolsaCliente()));
			props.Add(new esPropertyDescriptor(this, "SubscricaoBolsaCollectionByCdAtivoBolsa", typeof(SubscricaoBolsaCollection), new SubscricaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "SubscricaoBolsaCollectionByCdAtivoBolsaDireito", typeof(SubscricaoBolsaCollection), new SubscricaoBolsa()));
			props.Add(new esPropertyDescriptor(this, "TabelaCONRCollectionByCdAtivoBolsa", typeof(TabelaCONRCollection), new TabelaCONR()));
			props.Add(new esPropertyDescriptor(this, "TransferenciaBolsaCollectionByCdAtivoBolsa", typeof(TransferenciaBolsaCollection), new TransferenciaBolsa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEmissorByIdEmissor != null)
			{
				this.IdEmissor = this._UpToEmissorByIdEmissor.IdEmissor;
			}
			if(!this.es.IsDeleted && this._UpToEstrategiaByIdEstrategia != null)
			{
				this.IdEstrategia = this._UpToEstrategiaByIdEstrategia.IdEstrategia;
			}
			if(!this.es.IsDeleted && this._UpToGrupoEconomicoByIdGrupoEconomico != null)
			{
				this.IdGrupoEconomico = this._UpToGrupoEconomicoByIdGrupoEconomico.IdGrupo;
			}
			if(!this.es.IsDeleted && this._UpToMoedaByIdMoeda != null)
			{
				this.IdMoeda = this._UpToMoedaByIdMoeda.IdMoeda;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAtivoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AtivoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Especificacao
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.Especificacao, esSystemType.String);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.TipoMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem PUExercicio
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.PUExercicio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBolsaObjeto
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoIsin
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.CodigoIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem IdEmissor
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.IdEmissor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.IdMoeda, esSystemType.Int16);
			}
		} 
		
		public esQueryItem OpcaoIndice
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.OpcaoIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCDA
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.CodigoCDA, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NumeroSerie
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.NumeroSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoPapel
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.TipoPapel, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataInicioVigencia
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.DataInicioVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFimVigencia
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.DataFimVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdGrupoEconomico
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.IdGrupoEconomico, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalCustodia
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.IdLocalCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdClearing
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.IdClearing, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCusip
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.CodigoCusip, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoBDS
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.CodigoBDS, esSystemType.Int32);
			}
		}

        public esQueryItem InvestimentoColetivoCvm
        {
            get
            {
                return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.InvestimentoColetivoCvm, esSystemType.String);
            }
		} 
		
		public esQueryItem Participacao
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaMetadata.ColumnNames.Participacao, esSystemType.String);
			}
		} 
		
	}

	[Serializable]
	[XmlType("AtivoBolsaCollection")]
	public partial class AtivoBolsaCollection : esAtivoBolsaCollection, IEnumerable<AtivoBolsa>
	{
		public AtivoBolsaCollection()
		{

		}
		
		public static implicit operator List<AtivoBolsa>(AtivoBolsaCollection coll)
		{
			List<AtivoBolsa> list = new List<AtivoBolsa>();
			
			foreach (AtivoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AtivoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AtivoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AtivoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AtivoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AtivoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AtivoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AtivoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AtivoBolsa AddNew()
		{
			AtivoBolsa entity = base.AddNewEntity() as AtivoBolsa;
			
			return entity;
		}

		public AtivoBolsa FindByPrimaryKey(System.String cdAtivoBolsa)
		{
			return base.FindByPrimaryKey(cdAtivoBolsa) as AtivoBolsa;
		}


		#region IEnumerable<AtivoBolsa> Members

		IEnumerator<AtivoBolsa> IEnumerable<AtivoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AtivoBolsa;
			}
		}

		#endregion
		
		private AtivoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AtivoBolsa' table
	/// </summary>

	[Serializable]
	public partial class AtivoBolsa : esAtivoBolsa
	{
		public AtivoBolsa()
		{

		}
	
		public AtivoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AtivoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esAtivoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AtivoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AtivoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AtivoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AtivoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AtivoBolsaQuery query;
	}



	[Serializable]
	public partial class AtivoBolsaQuery : esAtivoBolsaQuery
	{
		public AtivoBolsaQuery()
		{

		}		
		
		public AtivoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AtivoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AtivoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsa, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.Especificacao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.Especificacao;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.TipoMercado, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.TipoMercado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.PUExercicio, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.PUExercicio;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.DataVencimento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsaObjeto, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.CdAtivoBolsaObjeto;
			c.CharacterMaxLength = 14;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.CodigoIsin, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.CodigoIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.IdEmissor, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.IdEmissor;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.IdMoeda, 9, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.OpcaoIndice, 10, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.OpcaoIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.IdEstrategia, 11, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.IdEstrategia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.CodigoCDA, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.CodigoCDA;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.NumeroSerie, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.NumeroSerie;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.TipoPapel, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.TipoPapel;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.DataInicioVigencia, 15, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.DataInicioVigencia;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.DataFimVigencia, 16, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.DataFimVigencia;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.IdGrupoEconomico, 17, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.IdGrupoEconomico;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.IdLocalCustodia, 18, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.IdLocalCustodia;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('3')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.IdClearing, 19, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.IdClearing;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('4')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.IdLocalNegociacao, 20, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('5')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.CodigoCusip, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.CodigoCusip;
			c.CharacterMaxLength = 9;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.CodigoBDS, 22, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.CodigoBDS;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c);

            c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.InvestimentoColetivoCvm, 23, typeof(System.String), esSystemType.String);
            c.PropertyName = AtivoBolsaMetadata.PropertyNames.InvestimentoColetivoCvm;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.HasDefault = true;
            c.Default = @"('N')";
            _columns.Add(c);

			c = new esColumnMetadata(AtivoBolsaMetadata.ColumnNames.Participacao, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBolsaMetadata.PropertyNames.Participacao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			c.IsNullable = true;
			_columns.Add(c); 


        }
		#endregion
	
		static public AtivoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Especificacao = "Especificacao";
			 public const string Descricao = "Descricao";
			 public const string TipoMercado = "TipoMercado";
			 public const string PUExercicio = "PUExercicio";
			 public const string DataVencimento = "DataVencimento";
			 public const string CdAtivoBolsaObjeto = "CdAtivoBolsaObjeto";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string IdEmissor = "IdEmissor";
			 public const string IdMoeda = "IdMoeda";
			 public const string OpcaoIndice = "OpcaoIndice";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string CodigoCDA = "CodigoCDA";
			 public const string NumeroSerie = "NumeroSerie";
			 public const string TipoPapel = "TipoPapel";
			 public const string DataInicioVigencia = "DataInicioVigencia";
			 public const string DataFimVigencia = "DataFimVigencia";
			 public const string IdGrupoEconomico = "IdGrupoEconomico";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdClearing = "IdClearing";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string CodigoCusip = "CodigoCusip";
			 public const string CodigoBDS = "CodigoBDS";
             public const string InvestimentoColetivoCvm = "InvestimentoColetivoCvm";
			 public const string Participacao = "Participacao";
        }
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Especificacao = "Especificacao";
			 public const string Descricao = "Descricao";
			 public const string TipoMercado = "TipoMercado";
			 public const string PUExercicio = "PUExercicio";
			 public const string DataVencimento = "DataVencimento";
			 public const string CdAtivoBolsaObjeto = "CdAtivoBolsaObjeto";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string IdEmissor = "IdEmissor";
			 public const string IdMoeda = "IdMoeda";
			 public const string OpcaoIndice = "OpcaoIndice";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string CodigoCDA = "CodigoCDA";
			 public const string NumeroSerie = "NumeroSerie";
			 public const string TipoPapel = "TipoPapel";
			 public const string DataInicioVigencia = "DataInicioVigencia";
			 public const string DataFimVigencia = "DataFimVigencia";
			 public const string IdGrupoEconomico = "IdGrupoEconomico";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdClearing = "IdClearing";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string CodigoCusip = "CodigoCusip";
			 public const string CodigoBDS = "CodigoBDS";
             public const string InvestimentoColetivoCvm = "InvestimentoColetivoCvm";
			 public const string Participacao = "Participacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AtivoBolsaMetadata))
			{
				if(AtivoBolsaMetadata.mapDelegates == null)
				{
					AtivoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AtivoBolsaMetadata.meta == null)
				{
					AtivoBolsaMetadata.meta = new AtivoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Especificacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PUExercicio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBolsaObjeto", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoIsin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdEmissor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("OpcaoIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCDA", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NumeroSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoPapel", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataInicioVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFimVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdGrupoEconomico", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdClearing", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCusip", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoBDS", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("InvestimentoColetivoCvm", new esTypeMap("char", "System.String"));
                meta.AddTypeMap("Participacao", new esTypeMap("char", "System.String"));

				meta.Destination = "AtivoBolsa";
				
				meta.spInsert = "proc_AtivoBolsaInsert";				
				meta.spUpdate = "proc_AtivoBolsaUpdate";		
				meta.spDelete = "proc_AtivoBolsaDelete";
				meta.spLoadAll = "proc_AtivoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_AtivoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AtivoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
