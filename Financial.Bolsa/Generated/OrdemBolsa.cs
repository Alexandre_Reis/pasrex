/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/03/2016 18:55:47
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Investidor;



namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esOrdemBolsaCollection : esEntityCollection
	{
		public esOrdemBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OrdemBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esOrdemBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOrdemBolsaQuery);
		}
		#endregion
		
		virtual public OrdemBolsa DetachEntity(OrdemBolsa entity)
		{
			return base.DetachEntity(entity) as OrdemBolsa;
		}
		
		virtual public OrdemBolsa AttachEntity(OrdemBolsa entity)
		{
			return base.AttachEntity(entity) as OrdemBolsa;
		}
		
		virtual public void Combine(OrdemBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OrdemBolsa this[int index]
		{
			get
			{
				return base[index] as OrdemBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OrdemBolsa);
		}
	}



	[Serializable]
	abstract public class esOrdemBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOrdemBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esOrdemBolsa()
		{

		}

		public esOrdemBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOrdem)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOrdem);
			else
				return LoadByPrimaryKeyStoredProcedure(idOrdem);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOrdem)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOrdem);
			else
				return LoadByPrimaryKeyStoredProcedure(idOrdem);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOrdem)
		{
			esOrdemBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOrdem == idOrdem);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOrdem)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOrdem",idOrdem);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOrdem": this.str.IdOrdem = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "IdAgenteLiquidacao": this.str.IdAgenteLiquidacao = (string)value; break;							
						case "IdAgenteCorretora": this.str.IdAgenteCorretora = (string)value; break;							
						case "TipoMercado": this.str.TipoMercado = (string)value; break;							
						case "TipoOrdem": this.str.TipoOrdem = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Corretagem": this.str.Corretagem = (string)value; break;							
						case "Emolumento": this.str.Emolumento = (string)value; break;							
						case "LiquidacaoCBLC": this.str.LiquidacaoCBLC = (string)value; break;							
						case "RegistroBolsa": this.str.RegistroBolsa = (string)value; break;							
						case "RegistroCBLC": this.str.RegistroCBLC = (string)value; break;							
						case "Origem": this.str.Origem = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "PercentualDesconto": this.str.PercentualDesconto = (string)value; break;							
						case "Desconto": this.str.Desconto = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "QuantidadeDayTrade": this.str.QuantidadeDayTrade = (string)value; break;							
						case "CdAtivoBolsaOpcao": this.str.CdAtivoBolsaOpcao = (string)value; break;							
						case "NumeroNegocio": this.str.NumeroNegocio = (string)value; break;							
						case "CalculaDespesas": this.str.CalculaDespesas = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "NumeroNota": this.str.NumeroNota = (string)value; break;							
						case "ValorISS": this.str.ValorISS = (string)value; break;							
						case "IdLocalCustodia": this.str.IdLocalCustodia = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "IdClearing": this.str.IdClearing = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "UserTimeStamp": this.str.UserTimeStamp = (string)value; break;							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOrdem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOrdem = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgenteLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteLiquidacao = (System.Int32?)value;
							break;
						
						case "IdAgenteCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteCorretora = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Corretagem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Corretagem = (System.Decimal?)value;
							break;
						
						case "Emolumento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Emolumento = (System.Decimal?)value;
							break;
						
						case "LiquidacaoCBLC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.LiquidacaoCBLC = (System.Decimal?)value;
							break;
						
						case "RegistroBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RegistroBolsa = (System.Decimal?)value;
							break;
						
						case "RegistroCBLC":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RegistroCBLC = (System.Decimal?)value;
							break;
						
						case "Origem":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Origem = (System.Byte?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "PercentualDesconto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualDesconto = (System.Decimal?)value;
							break;
						
						case "Desconto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Desconto = (System.Decimal?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "QuantidadeDayTrade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeDayTrade = (System.Decimal?)value;
							break;
						
						case "NumeroNegocio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroNegocio = (System.Int32?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMoeda = (System.Int32?)value;
							break;
						
						case "NumeroNota":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroNota = (System.Int32?)value;
							break;
						
						case "ValorISS":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorISS = (System.Decimal?)value;
							break;
						
						case "IdLocalCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalCustodia = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IdClearing":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClearing = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "UserTimeStamp":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.UserTimeStamp = (System.DateTime?)value;
							break;
						
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OrdemBolsa.IdOrdem
		/// </summary>
		virtual public System.Int32? IdOrdem
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdOrdem);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(OrdemBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(OrdemBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.IdAgenteLiquidacao
		/// </summary>
		virtual public System.Int32? IdAgenteLiquidacao
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdAgenteLiquidacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdAgenteLiquidacao, value))
				{
					this._UpToAgenteMercadoByIdAgenteLiquidacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.IdAgenteCorretora
		/// </summary>
		virtual public System.Int32? IdAgenteCorretora
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdAgenteCorretora);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdAgenteCorretora, value))
				{
					this._UpToAgenteMercadoByIdAgenteCorretora = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.TipoMercado
		/// </summary>
		virtual public System.String TipoMercado
		{
			get
			{
				return base.GetSystemString(OrdemBolsaMetadata.ColumnNames.TipoMercado);
			}
			
			set
			{
				base.SetSystemString(OrdemBolsaMetadata.ColumnNames.TipoMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.TipoOrdem
		/// </summary>
		virtual public System.String TipoOrdem
		{
			get
			{
				return base.GetSystemString(OrdemBolsaMetadata.ColumnNames.TipoOrdem);
			}
			
			set
			{
				base.SetSystemString(OrdemBolsaMetadata.ColumnNames.TipoOrdem, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(OrdemBolsaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemBolsaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.Corretagem
		/// </summary>
		virtual public System.Decimal? Corretagem
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Corretagem);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Corretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.Emolumento
		/// </summary>
		virtual public System.Decimal? Emolumento
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Emolumento);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Emolumento, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.LiquidacaoCBLC
		/// </summary>
		virtual public System.Decimal? LiquidacaoCBLC
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.LiquidacaoCBLC);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.LiquidacaoCBLC, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.RegistroBolsa
		/// </summary>
		virtual public System.Decimal? RegistroBolsa
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.RegistroBolsa);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.RegistroBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.RegistroCBLC
		/// </summary>
		virtual public System.Decimal? RegistroCBLC
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.RegistroCBLC);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.RegistroCBLC, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.Origem
		/// </summary>
		virtual public System.Byte? Origem
		{
			get
			{
				return base.GetSystemByte(OrdemBolsaMetadata.ColumnNames.Origem);
			}
			
			set
			{
				base.SetSystemByte(OrdemBolsaMetadata.ColumnNames.Origem, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OrdemBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OrdemBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.PercentualDesconto
		/// </summary>
		virtual public System.Decimal? PercentualDesconto
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.PercentualDesconto);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.PercentualDesconto, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.Desconto
		/// </summary>
		virtual public System.Decimal? Desconto
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Desconto);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.Desconto, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.QuantidadeDayTrade
		/// </summary>
		virtual public System.Decimal? QuantidadeDayTrade
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.QuantidadeDayTrade);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.QuantidadeDayTrade, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.CdAtivoBolsaOpcao
		/// </summary>
		virtual public System.String CdAtivoBolsaOpcao
		{
			get
			{
				return base.GetSystemString(OrdemBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao);
			}
			
			set
			{
				if(base.SetSystemString(OrdemBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.NumeroNegocio
		/// </summary>
		virtual public System.Int32? NumeroNegocio
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.NumeroNegocio);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.NumeroNegocio, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.CalculaDespesas
		/// </summary>
		virtual public System.String CalculaDespesas
		{
			get
			{
				return base.GetSystemString(OrdemBolsaMetadata.ColumnNames.CalculaDespesas);
			}
			
			set
			{
				base.SetSystemString(OrdemBolsaMetadata.ColumnNames.CalculaDespesas, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(OrdemBolsaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemBolsaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.IdMoeda
		/// </summary>
		virtual public System.Int32? IdMoeda
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.NumeroNota
		/// </summary>
		virtual public System.Int32? NumeroNota
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.NumeroNota);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.NumeroNota, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.ValorISS
		/// </summary>
		virtual public System.Decimal? ValorISS
		{
			get
			{
				return base.GetSystemDecimal(OrdemBolsaMetadata.ColumnNames.ValorISS);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemBolsaMetadata.ColumnNames.ValorISS, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.IdLocalCustodia
		/// </summary>
		virtual public System.Int32? IdLocalCustodia
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdLocalCustodia);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdLocalCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.IdClearing
		/// </summary>
		virtual public System.Int32? IdClearing
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdClearing);
			}
			
			set
			{
				base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdClearing, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OrdemBolsaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemBolsaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.UserTimeStamp
		/// </summary>
		virtual public System.DateTime? UserTimeStamp
		{
			get
			{
				return base.GetSystemDateTime(OrdemBolsaMetadata.ColumnNames.UserTimeStamp);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemBolsaMetadata.ColumnNames.UserTimeStamp, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemBolsa.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemBolsaMetadata.ColumnNames.IdCategoriaMovimentacao, value))
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteLiquidacao;
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteCorretora;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsaOpcao;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected CategoriaMovimentacao _UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOrdemBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOrdem
			{
				get
				{
					System.Int32? data = entity.IdOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOrdem = null;
					else entity.IdOrdem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String IdAgenteLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdAgenteLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteLiquidacao = null;
					else entity.IdAgenteLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteCorretora
			{
				get
				{
					System.Int32? data = entity.IdAgenteCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteCorretora = null;
					else entity.IdAgenteCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoMercado
			{
				get
				{
					System.String data = entity.TipoMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoMercado = null;
					else entity.TipoMercado = Convert.ToString(value);
				}
			}
				
			public System.String TipoOrdem
			{
				get
				{
					System.String data = entity.TipoOrdem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOrdem = null;
					else entity.TipoOrdem = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Corretagem
			{
				get
				{
					System.Decimal? data = entity.Corretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Corretagem = null;
					else entity.Corretagem = Convert.ToDecimal(value);
				}
			}
				
			public System.String Emolumento
			{
				get
				{
					System.Decimal? data = entity.Emolumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Emolumento = null;
					else entity.Emolumento = Convert.ToDecimal(value);
				}
			}
				
			public System.String LiquidacaoCBLC
			{
				get
				{
					System.Decimal? data = entity.LiquidacaoCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.LiquidacaoCBLC = null;
					else entity.LiquidacaoCBLC = Convert.ToDecimal(value);
				}
			}
				
			public System.String RegistroBolsa
			{
				get
				{
					System.Decimal? data = entity.RegistroBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroBolsa = null;
					else entity.RegistroBolsa = Convert.ToDecimal(value);
				}
			}
				
			public System.String RegistroCBLC
			{
				get
				{
					System.Decimal? data = entity.RegistroCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RegistroCBLC = null;
					else entity.RegistroCBLC = Convert.ToDecimal(value);
				}
			}
				
			public System.String Origem
			{
				get
				{
					System.Byte? data = entity.Origem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Origem = null;
					else entity.Origem = Convert.ToByte(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String PercentualDesconto
			{
				get
				{
					System.Decimal? data = entity.PercentualDesconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualDesconto = null;
					else entity.PercentualDesconto = Convert.ToDecimal(value);
				}
			}
				
			public System.String Desconto
			{
				get
				{
					System.Decimal? data = entity.Desconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Desconto = null;
					else entity.Desconto = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeDayTrade
			{
				get
				{
					System.Decimal? data = entity.QuantidadeDayTrade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeDayTrade = null;
					else entity.QuantidadeDayTrade = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAtivoBolsaOpcao
			{
				get
				{
					System.String data = entity.CdAtivoBolsaOpcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaOpcao = null;
					else entity.CdAtivoBolsaOpcao = Convert.ToString(value);
				}
			}
				
			public System.String NumeroNegocio
			{
				get
				{
					System.Int32? data = entity.NumeroNegocio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroNegocio = null;
					else entity.NumeroNegocio = Convert.ToInt32(value);
				}
			}
				
			public System.String CalculaDespesas
			{
				get
				{
					System.String data = entity.CalculaDespesas;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CalculaDespesas = null;
					else entity.CalculaDespesas = Convert.ToString(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int32? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt32(value);
				}
			}
				
			public System.String NumeroNota
			{
				get
				{
					System.Int32? data = entity.NumeroNota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroNota = null;
					else entity.NumeroNota = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorISS
			{
				get
				{
					System.Decimal? data = entity.ValorISS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorISS = null;
					else entity.ValorISS = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdLocalCustodia
			{
				get
				{
					System.Int32? data = entity.IdLocalCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalCustodia = null;
					else entity.IdLocalCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdClearing
			{
				get
				{
					System.Int32? data = entity.IdClearing;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClearing = null;
					else entity.IdClearing = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String UserTimeStamp
			{
				get
				{
					System.DateTime? data = entity.UserTimeStamp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UserTimeStamp = null;
					else entity.UserTimeStamp = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
			

			private esOrdemBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOrdemBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOrdemBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OrdemBolsa : esOrdemBolsa
	{

				
		#region OrdemTermoBolsa - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - OrdemBolsa_OrdemTermoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemTermoBolsa OrdemTermoBolsa
		{
			get
			{
				if(this._OrdemTermoBolsa == null)
				{
					this._OrdemTermoBolsa = new OrdemTermoBolsa();
					this._OrdemTermoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostOneSave("OrdemTermoBolsa", this._OrdemTermoBolsa);
				
					if(this.IdOrdem != null)
					{
						this._OrdemTermoBolsa.Query.Where(this._OrdemTermoBolsa.Query.IdOrdem == this.IdOrdem);
						this._OrdemTermoBolsa.Query.Load();
					}
				}

				return this._OrdemTermoBolsa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemTermoBolsa != null) 
				{ 
					this.RemovePostOneSave("OrdemTermoBolsa"); 
					this._OrdemTermoBolsa = null;
					
				} 
			}          			
		}

		private OrdemTermoBolsa _OrdemTermoBolsa;
		#endregion

				
		#region UpToAgenteMercadoByIdAgenteLiquidacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_OrdemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteLiquidacao
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteLiquidacao == null
					&& IdAgenteLiquidacao != null					)
				{
					this._UpToAgenteMercadoByIdAgenteLiquidacao = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteLiquidacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteLiquidacao", this._UpToAgenteMercadoByIdAgenteLiquidacao);
					this._UpToAgenteMercadoByIdAgenteLiquidacao.Query.Where(this._UpToAgenteMercadoByIdAgenteLiquidacao.Query.IdAgente == this.IdAgenteLiquidacao);
					this._UpToAgenteMercadoByIdAgenteLiquidacao.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteLiquidacao;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteLiquidacao");
				

				if(value == null)
				{
					this.IdAgenteLiquidacao = null;
					this._UpToAgenteMercadoByIdAgenteLiquidacao = null;
				}
				else
				{
					this.IdAgenteLiquidacao = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteLiquidacao = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteLiquidacao", this._UpToAgenteMercadoByIdAgenteLiquidacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAgenteMercadoByIdAgenteCorretora - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_OrdemBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteCorretora
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteCorretora == null
					&& IdAgenteCorretora != null					)
				{
					this._UpToAgenteMercadoByIdAgenteCorretora = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteCorretora.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteCorretora", this._UpToAgenteMercadoByIdAgenteCorretora);
					this._UpToAgenteMercadoByIdAgenteCorretora.Query.Where(this._UpToAgenteMercadoByIdAgenteCorretora.Query.IdAgente == this.IdAgenteCorretora);
					this._UpToAgenteMercadoByIdAgenteCorretora.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteCorretora;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteCorretora");
				

				if(value == null)
				{
					this.IdAgenteCorretora = null;
					this._UpToAgenteMercadoByIdAgenteCorretora = null;
				}
				else
				{
					this.IdAgenteCorretora = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteCorretora = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteCorretora", this._UpToAgenteMercadoByIdAgenteCorretora);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsaOpcao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_OrdemBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsaOpcao
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsaOpcao == null
					&& CdAtivoBolsaOpcao != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaOpcao", this._UpToAtivoBolsaByCdAtivoBolsaOpcao);
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsaOpcao.Query.CdAtivoBolsa == this.CdAtivoBolsaOpcao);
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsaOpcao;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsaOpcao");
				

				if(value == null)
				{
					this.CdAtivoBolsaOpcao = null;
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao = null;
				}
				else
				{
					this.CdAtivoBolsaOpcao = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsaOpcao = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaOpcao", this._UpToAtivoBolsaByCdAtivoBolsaOpcao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_OrdemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCategoriaMovimentacaoByIdCategoriaMovimentacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OrdemBolsa_CatMovimentacao_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaMovimentacao UpToCategoriaMovimentacaoByIdCategoriaMovimentacao
		{
			get
			{
				if(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao == null
					&& IdCategoriaMovimentacao != null					)
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = new CategoriaMovimentacao();
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Where(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.IdCategoriaMovimentacao == this.IdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Load();
				}

				return this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao");
				

				if(value == null)
				{
					this.IdCategoriaMovimentacao = null;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
				else
				{
					this.IdCategoriaMovimentacao = value.IdCategoriaMovimentacao;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = value;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_OrdemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_OrdemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteLiquidacao != null)
			{
				this.IdAgenteLiquidacao = this._UpToAgenteMercadoByIdAgenteLiquidacao.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteCorretora != null)
			{
				this.IdAgenteCorretora = this._UpToAgenteMercadoByIdAgenteCorretora.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao != null)
			{
				this.IdCategoriaMovimentacao = this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.IdCategoriaMovimentacao;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
			if(this._OrdemTermoBolsa != null)
			{
				if(this._OrdemTermoBolsa.es.IsAdded)
				{
					this._OrdemTermoBolsa.IdOrdem = this.IdOrdem;
				}
			}
		}
		
	}



	[Serializable]
	abstract public class esOrdemBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OrdemBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOrdem
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdOrdem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgenteLiquidacao
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdAgenteLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteCorretora
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdAgenteCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoMercado
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.TipoMercado, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoOrdem
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.TipoOrdem, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Corretagem
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.Corretagem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Emolumento
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.Emolumento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem LiquidacaoCBLC
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.LiquidacaoCBLC, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RegistroBolsa
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.RegistroBolsa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RegistroCBLC
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.RegistroCBLC, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Origem
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.Origem, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PercentualDesconto
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.PercentualDesconto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Desconto
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.Desconto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeDayTrade
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.QuantidadeDayTrade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAtivoBolsaOpcao
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, esSystemType.String);
			}
		} 
		
		public esQueryItem NumeroNegocio
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.NumeroNegocio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CalculaDespesas
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.CalculaDespesas, esSystemType.String);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdMoeda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem NumeroNota
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.NumeroNota, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorISS
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.ValorISS, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdLocalCustodia
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdLocalCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdClearing
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdClearing, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem UserTimeStamp
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.UserTimeStamp, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, OrdemBolsaMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OrdemBolsaCollection")]
	public partial class OrdemBolsaCollection : esOrdemBolsaCollection, IEnumerable<OrdemBolsa>
	{
		public OrdemBolsaCollection()
		{

		}
		
		public static implicit operator List<OrdemBolsa>(OrdemBolsaCollection coll)
		{
			List<OrdemBolsa> list = new List<OrdemBolsa>();
			
			foreach (OrdemBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OrdemBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OrdemBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OrdemBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OrdemBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OrdemBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrdemBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OrdemBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OrdemBolsa AddNew()
		{
			OrdemBolsa entity = base.AddNewEntity() as OrdemBolsa;
			
			return entity;
		}

		public OrdemBolsa FindByPrimaryKey(System.Int32 idOrdem)
		{
			return base.FindByPrimaryKey(idOrdem) as OrdemBolsa;
		}


		#region IEnumerable<OrdemBolsa> Members

		IEnumerator<OrdemBolsa> IEnumerable<OrdemBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OrdemBolsa;
			}
		}

		#endregion
		
		private OrdemBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OrdemBolsa' table
	/// </summary>

	[Serializable]
	public partial class OrdemBolsa : esOrdemBolsa
	{
		public OrdemBolsa()
		{

		}
	
		public OrdemBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OrdemBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esOrdemBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OrdemBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OrdemBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrdemBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OrdemBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OrdemBolsaQuery query;
	}



	[Serializable]
	public partial class OrdemBolsaQuery : esOrdemBolsaQuery
	{
		public OrdemBolsaQuery()
		{

		}		
		
		public OrdemBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OrdemBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OrdemBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdOrdem, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdOrdem;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.CdAtivoBolsa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdAgenteLiquidacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdAgenteLiquidacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdAgenteCorretora, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdAgenteCorretora;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.TipoMercado, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.TipoMercado;
			c.CharacterMaxLength = 3;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.TipoOrdem, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.TipoOrdem;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.Data, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.Pu, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.Valor, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.Quantidade, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.Corretagem, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.Corretagem;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.Emolumento, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.Emolumento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.LiquidacaoCBLC, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.LiquidacaoCBLC;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.RegistroBolsa, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.RegistroBolsa;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.RegistroCBLC, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.RegistroCBLC;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.Origem, 16, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.Origem;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.Fonte, 17, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.PercentualDesconto, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.PercentualDesconto;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.Desconto, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.Desconto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdTrader, 20, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.QuantidadeDayTrade, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.QuantidadeDayTrade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.CdAtivoBolsaOpcao, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.CdAtivoBolsaOpcao;
			c.CharacterMaxLength = 25;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.NumeroNegocio, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.NumeroNegocio;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.CalculaDespesas, 24, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.CalculaDespesas;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.DataLiquidacao, 25, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdMoeda, 26, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.NumeroNota, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.NumeroNota;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.ValorISS, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.ValorISS;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdLocalCustodia, 29, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdLocalCustodia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdLocalNegociacao, 30, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdClearing, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdClearing;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.DataOperacao, 32, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.UserTimeStamp, 33, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.UserTimeStamp;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemBolsaMetadata.ColumnNames.IdCategoriaMovimentacao, 34, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemBolsaMetadata.PropertyNames.IdCategoriaMovimentacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OrdemBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOrdem = "IdOrdem";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgenteLiquidacao = "IdAgenteLiquidacao";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOrdem = "TipoOrdem";
			 public const string Data = "Data";
			 public const string Pu = "PU";
			 public const string Valor = "Valor";
			 public const string Quantidade = "Quantidade";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string LiquidacaoCBLC = "LiquidacaoCBLC";
			 public const string RegistroBolsa = "RegistroBolsa";
			 public const string RegistroCBLC = "RegistroCBLC";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string Desconto = "Desconto";
			 public const string IdTrader = "IdTrader";
			 public const string QuantidadeDayTrade = "QuantidadeDayTrade";
			 public const string CdAtivoBolsaOpcao = "CdAtivoBolsaOpcao";
			 public const string NumeroNegocio = "NumeroNegocio";
			 public const string CalculaDespesas = "CalculaDespesas";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string IdMoeda = "IdMoeda";
			 public const string NumeroNota = "NumeroNota";
			 public const string ValorISS = "ValorISS";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdClearing = "IdClearing";
			 public const string DataOperacao = "DataOperacao";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOrdem = "IdOrdem";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgenteLiquidacao = "IdAgenteLiquidacao";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string TipoMercado = "TipoMercado";
			 public const string TipoOrdem = "TipoOrdem";
			 public const string Data = "Data";
			 public const string Pu = "Pu";
			 public const string Valor = "Valor";
			 public const string Quantidade = "Quantidade";
			 public const string Corretagem = "Corretagem";
			 public const string Emolumento = "Emolumento";
			 public const string LiquidacaoCBLC = "LiquidacaoCBLC";
			 public const string RegistroBolsa = "RegistroBolsa";
			 public const string RegistroCBLC = "RegistroCBLC";
			 public const string Origem = "Origem";
			 public const string Fonte = "Fonte";
			 public const string PercentualDesconto = "PercentualDesconto";
			 public const string Desconto = "Desconto";
			 public const string IdTrader = "IdTrader";
			 public const string QuantidadeDayTrade = "QuantidadeDayTrade";
			 public const string CdAtivoBolsaOpcao = "CdAtivoBolsaOpcao";
			 public const string NumeroNegocio = "NumeroNegocio";
			 public const string CalculaDespesas = "CalculaDespesas";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string IdMoeda = "IdMoeda";
			 public const string NumeroNota = "NumeroNota";
			 public const string ValorISS = "ValorISS";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdClearing = "IdClearing";
			 public const string DataOperacao = "DataOperacao";
			 public const string UserTimeStamp = "UserTimeStamp";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OrdemBolsaMetadata))
			{
				if(OrdemBolsaMetadata.mapDelegates == null)
				{
					OrdemBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OrdemBolsaMetadata.meta == null)
				{
					OrdemBolsaMetadata.meta = new OrdemBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOrdem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgenteLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoMercado", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoOrdem", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Corretagem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Emolumento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("LiquidacaoCBLC", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RegistroBolsa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RegistroCBLC", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Origem", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PercentualDesconto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Desconto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeDayTrade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CdAtivoBolsaOpcao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("NumeroNegocio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CalculaDespesas", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("NumeroNota", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorISS", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdLocalCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdClearing", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("UserTimeStamp", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "OrdemBolsa";
				meta.Destination = "OrdemBolsa";
				
				meta.spInsert = "proc_OrdemBolsaInsert";				
				meta.spUpdate = "proc_OrdemBolsaUpdate";		
				meta.spDelete = "proc_OrdemBolsaDelete";
				meta.spLoadAll = "proc_OrdemBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_OrdemBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OrdemBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
