/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;



namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esOperacaoEmprestimoBolsaCollection : esEntityCollection
	{
		public esOperacaoEmprestimoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoEmprestimoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoEmprestimoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoEmprestimoBolsaQuery);
		}
		#endregion
		
		virtual public OperacaoEmprestimoBolsa DetachEntity(OperacaoEmprestimoBolsa entity)
		{
			return base.DetachEntity(entity) as OperacaoEmprestimoBolsa;
		}
		
		virtual public OperacaoEmprestimoBolsa AttachEntity(OperacaoEmprestimoBolsa entity)
		{
			return base.AttachEntity(entity) as OperacaoEmprestimoBolsa;
		}
		
		virtual public void Combine(OperacaoEmprestimoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoEmprestimoBolsa this[int index]
		{
			get
			{
				return base[index] as OperacaoEmprestimoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoEmprestimoBolsa);
		}
	}



	[Serializable]
	abstract public class esOperacaoEmprestimoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoEmprestimoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoEmprestimoBolsa()
		{

		}

		public esOperacaoEmprestimoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOperacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOperacaoEmprestimoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdOperacao == idOperacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOperacaoEmprestimoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "PontaEmprestimo": this.str.PontaEmprestimo = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "TaxaOperacao": this.str.TaxaOperacao = (string)value; break;							
						case "TaxaComissao": this.str.TaxaComissao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "NumeroContrato": this.str.NumeroContrato = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "TipoEmprestimo": this.str.TipoEmprestimo = (string)value; break;							
						case "IdLocal": this.str.IdLocal = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "PermiteDevolucaoAntecipada": this.str.PermiteDevolucaoAntecipada = (string)value; break;							
						case "DataInicialDevolucaoAntecipada": this.str.DataInicialDevolucaoAntecipada = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "PontaEmprestimo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.PontaEmprestimo = (System.Byte?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "TaxaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaOperacao = (System.Decimal?)value;
							break;
						
						case "TaxaComissao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaComissao = (System.Decimal?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "NumeroContrato":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroContrato = (System.Int32?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "TipoEmprestimo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoEmprestimo = (System.Byte?)value;
							break;
						
						case "IdLocal":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdLocal = (System.Int16?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "DataInicialDevolucaoAntecipada":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicialDevolucaoAntecipada = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(OperacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				base.SetSystemString(OperacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdAgente, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.PontaEmprestimo
		/// </summary>
		virtual public System.Byte? PontaEmprestimo
		{
			get
			{
				return base.GetSystemByte(OperacaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo);
			}
			
			set
			{
				base.SetSystemByte(OperacaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(OperacaoEmprestimoBolsaMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoEmprestimoBolsaMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(OperacaoEmprestimoBolsaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoEmprestimoBolsaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.TaxaOperacao
		/// </summary>
		virtual public System.Decimal? TaxaOperacao
		{
			get
			{
				return base.GetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.TaxaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.TaxaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.TaxaComissao
		/// </summary>
		virtual public System.Decimal? TaxaComissao
		{
			get
			{
				return base.GetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.TaxaComissao);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.TaxaComissao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.Pu, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoEmprestimoBolsaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.NumeroContrato
		/// </summary>
		virtual public System.Int32? NumeroContrato
		{
			get
			{
				return base.GetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OperacaoEmprestimoBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoEmprestimoBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.TipoEmprestimo
		/// </summary>
		virtual public System.Byte? TipoEmprestimo
		{
			get
			{
				return base.GetSystemByte(OperacaoEmprestimoBolsaMetadata.ColumnNames.TipoEmprestimo);
			}
			
			set
			{
				base.SetSystemByte(OperacaoEmprestimoBolsaMetadata.ColumnNames.TipoEmprestimo, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.IdLocal
		/// </summary>
		virtual public System.Int16? IdLocal
		{
			get
			{
				return base.GetSystemInt16(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdLocal);
			}
			
			set
			{
				base.SetSystemInt16(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdLocal, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.PermiteDevolucaoAntecipada
		/// </summary>
		virtual public System.String PermiteDevolucaoAntecipada
		{
			get
			{
				return base.GetSystemString(OperacaoEmprestimoBolsaMetadata.ColumnNames.PermiteDevolucaoAntecipada);
			}
			
			set
			{
				base.SetSystemString(OperacaoEmprestimoBolsaMetadata.ColumnNames.PermiteDevolucaoAntecipada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoEmprestimoBolsa.DataInicialDevolucaoAntecipada
		/// </summary>
		virtual public System.DateTime? DataInicialDevolucaoAntecipada
		{
			get
			{
				return base.GetSystemDateTime(OperacaoEmprestimoBolsaMetadata.ColumnNames.DataInicialDevolucaoAntecipada);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoEmprestimoBolsaMetadata.ColumnNames.DataInicialDevolucaoAntecipada, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoEmprestimoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String PontaEmprestimo
			{
				get
				{
					System.Byte? data = entity.PontaEmprestimo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PontaEmprestimo = null;
					else entity.PontaEmprestimo = Convert.ToByte(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaOperacao
			{
				get
				{
					System.Decimal? data = entity.TaxaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaOperacao = null;
					else entity.TaxaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaComissao
			{
				get
				{
					System.Decimal? data = entity.TaxaComissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaComissao = null;
					else entity.TaxaComissao = Convert.ToDecimal(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroContrato
			{
				get
				{
					System.Int32? data = entity.NumeroContrato;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroContrato = null;
					else entity.NumeroContrato = Convert.ToInt32(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String TipoEmprestimo
			{
				get
				{
					System.Byte? data = entity.TipoEmprestimo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEmprestimo = null;
					else entity.TipoEmprestimo = Convert.ToByte(value);
				}
			}
				
			public System.String IdLocal
			{
				get
				{
					System.Int16? data = entity.IdLocal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocal = null;
					else entity.IdLocal = Convert.ToInt16(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String PermiteDevolucaoAntecipada
			{
				get
				{
					System.String data = entity.PermiteDevolucaoAntecipada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermiteDevolucaoAntecipada = null;
					else entity.PermiteDevolucaoAntecipada = Convert.ToString(value);
				}
			}
				
			public System.String DataInicialDevolucaoAntecipada
			{
				get
				{
					System.DateTime? data = entity.DataInicialDevolucaoAntecipada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicialDevolucaoAntecipada = null;
					else entity.DataInicialDevolucaoAntecipada = Convert.ToDateTime(value);
				}
			}
			

			private esOperacaoEmprestimoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoEmprestimoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoEmprestimoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoEmprestimoBolsa : esOperacaoEmprestimoBolsa
	{

				
		#region LiquidacaoEmprestimoBolsaCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - OperacaoEmprestimoBolsa_LiquidacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoEmprestimoBolsaCollection LiquidacaoEmprestimoBolsaCollectionByIdOperacao
		{
			get
			{
				if(this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao == null)
				{
					this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao = new LiquidacaoEmprestimoBolsaCollection();
					this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoEmprestimoBolsaCollectionByIdOperacao", this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao.Query.Where(this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao.fks.Add(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("LiquidacaoEmprestimoBolsaCollectionByIdOperacao"); 
					this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private LiquidacaoEmprestimoBolsaCollection _LiquidacaoEmprestimoBolsaCollectionByIdOperacao;
		#endregion

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Trader_OperacaoEmprestimoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "LiquidacaoEmprestimoBolsaCollectionByIdOperacao", typeof(LiquidacaoEmprestimoBolsaCollection), new LiquidacaoEmprestimoBolsa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao != null)
			{
				foreach(LiquidacaoEmprestimoBolsa obj in this._LiquidacaoEmprestimoBolsaCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOperacaoEmprestimoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoEmprestimoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PontaEmprestimo
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.TaxaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaComissao
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.TaxaComissao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroContrato
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoEmprestimo
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.TipoEmprestimo, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdLocal
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.IdLocal, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PermiteDevolucaoAntecipada
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.PermiteDevolucaoAntecipada, esSystemType.String);
			}
		} 
		
		public esQueryItem DataInicialDevolucaoAntecipada
		{
			get
			{
				return new esQueryItem(this, OperacaoEmprestimoBolsaMetadata.ColumnNames.DataInicialDevolucaoAntecipada, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoEmprestimoBolsaCollection")]
	public partial class OperacaoEmprestimoBolsaCollection : esOperacaoEmprestimoBolsaCollection, IEnumerable<OperacaoEmprestimoBolsa>
	{
		public OperacaoEmprestimoBolsaCollection()
		{

		}
		
		public static implicit operator List<OperacaoEmprestimoBolsa>(OperacaoEmprestimoBolsaCollection coll)
		{
			List<OperacaoEmprestimoBolsa> list = new List<OperacaoEmprestimoBolsa>();
			
			foreach (OperacaoEmprestimoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoEmprestimoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoEmprestimoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoEmprestimoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoEmprestimoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoEmprestimoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoEmprestimoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoEmprestimoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoEmprestimoBolsa AddNew()
		{
			OperacaoEmprestimoBolsa entity = base.AddNewEntity() as OperacaoEmprestimoBolsa;
			
			return entity;
		}

		public OperacaoEmprestimoBolsa FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OperacaoEmprestimoBolsa;
		}


		#region IEnumerable<OperacaoEmprestimoBolsa> Members

		IEnumerator<OperacaoEmprestimoBolsa> IEnumerable<OperacaoEmprestimoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoEmprestimoBolsa;
			}
		}

		#endregion
		
		private OperacaoEmprestimoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoEmprestimoBolsa' table
	/// </summary>

	[Serializable]
	public partial class OperacaoEmprestimoBolsa : esOperacaoEmprestimoBolsa
	{
		public OperacaoEmprestimoBolsa()
		{

		}
	
		public OperacaoEmprestimoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoEmprestimoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoEmprestimoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoEmprestimoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoEmprestimoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoEmprestimoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoEmprestimoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoEmprestimoBolsaQuery query;
	}



	[Serializable]
	public partial class OperacaoEmprestimoBolsaQuery : esOperacaoEmprestimoBolsaQuery
	{
		public OperacaoEmprestimoBolsaQuery()
		{

		}		
		
		public OperacaoEmprestimoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoEmprestimoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoEmprestimoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdAgente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.PontaEmprestimo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.DataRegistro, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.DataVencimento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.TaxaOperacao, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.TaxaOperacao;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.TaxaComissao, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.TaxaComissao;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.Quantidade, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.Pu, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.Valor, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.NumeroContrato;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.Fonte, 13, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.TipoEmprestimo, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.TipoEmprestimo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdLocal, 15, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.IdLocal;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.IdTrader, 16, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.PermiteDevolucaoAntecipada, 17, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.PermiteDevolucaoAntecipada;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoEmprestimoBolsaMetadata.ColumnNames.DataInicialDevolucaoAntecipada, 18, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoEmprestimoBolsaMetadata.PropertyNames.DataInicialDevolucaoAntecipada;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoEmprestimoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgente = "IdAgente";
			 public const string PontaEmprestimo = "PontaEmprestimo";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataVencimento = "DataVencimento";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string TaxaComissao = "TaxaComissao";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "PU";
			 public const string Valor = "Valor";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string Fonte = "Fonte";
			 public const string TipoEmprestimo = "TipoEmprestimo";
			 public const string IdLocal = "IdLocal";
			 public const string IdTrader = "IdTrader";
			 public const string PermiteDevolucaoAntecipada = "PermiteDevolucaoAntecipada";
			 public const string DataInicialDevolucaoAntecipada = "DataInicialDevolucaoAntecipada";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgente = "IdAgente";
			 public const string PontaEmprestimo = "PontaEmprestimo";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataVencimento = "DataVencimento";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string TaxaComissao = "TaxaComissao";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "Pu";
			 public const string Valor = "Valor";
			 public const string NumeroContrato = "NumeroContrato";
			 public const string Fonte = "Fonte";
			 public const string TipoEmprestimo = "TipoEmprestimo";
			 public const string IdLocal = "IdLocal";
			 public const string IdTrader = "IdTrader";
			 public const string PermiteDevolucaoAntecipada = "PermiteDevolucaoAntecipada";
			 public const string DataInicialDevolucaoAntecipada = "DataInicialDevolucaoAntecipada";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoEmprestimoBolsaMetadata))
			{
				if(OperacaoEmprestimoBolsaMetadata.mapDelegates == null)
				{
					OperacaoEmprestimoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoEmprestimoBolsaMetadata.meta == null)
				{
					OperacaoEmprestimoBolsaMetadata.meta = new OperacaoEmprestimoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PontaEmprestimo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaComissao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroContrato", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoEmprestimo", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdLocal", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PermiteDevolucaoAntecipada", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("DataInicialDevolucaoAntecipada", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "OperacaoEmprestimoBolsa";
				meta.Destination = "OperacaoEmprestimoBolsa";
				
				meta.spInsert = "proc_OperacaoEmprestimoBolsaInsert";				
				meta.spUpdate = "proc_OperacaoEmprestimoBolsaUpdate";		
				meta.spDelete = "proc_OperacaoEmprestimoBolsaDelete";
				meta.spLoadAll = "proc_OperacaoEmprestimoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoEmprestimoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoEmprestimoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
