/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					



		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esBonificacaoBolsaCollection : esEntityCollection
	{
		public esBonificacaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "BonificacaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esBonificacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esBonificacaoBolsaQuery);
		}
		#endregion
		
		virtual public BonificacaoBolsa DetachEntity(BonificacaoBolsa entity)
		{
			return base.DetachEntity(entity) as BonificacaoBolsa;
		}
		
		virtual public BonificacaoBolsa AttachEntity(BonificacaoBolsa entity)
		{
			return base.AttachEntity(entity) as BonificacaoBolsa;
		}
		
		virtual public void Combine(BonificacaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public BonificacaoBolsa this[int index]
		{
			get
			{
				return base[index] as BonificacaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(BonificacaoBolsa);
		}
	}



	[Serializable]
	abstract public class esBonificacaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esBonificacaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esBonificacaoBolsa()
		{

		}

		public esBonificacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idBonificacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBonificacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idBonificacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idBonificacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esBonificacaoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdBonificacao == idBonificacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idBonificacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBonificacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idBonificacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idBonificacao)
		{
			esBonificacaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdBonificacao == idBonificacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idBonificacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdBonificacao",idBonificacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdBonificacao": this.str.IdBonificacao = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataEx": this.str.DataEx = (string)value; break;							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "TipoFracao": this.str.TipoFracao = (string)value; break;							
						case "PUBonificacao": this.str.PUBonificacao = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "CdAtivoBolsaDestino": this.str.CdAtivoBolsaDestino = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdBonificacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBonificacao = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataEx":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEx = (System.DateTime?)value;
							break;
						
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "TipoFracao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoFracao = (System.Byte?)value;
							break;
						
						case "PUBonificacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUBonificacao = (System.Decimal?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to BonificacaoBolsa.IdBonificacao
		/// </summary>
		virtual public System.Int32? IdBonificacao
		{
			get
			{
				return base.GetSystemInt32(BonificacaoBolsaMetadata.ColumnNames.IdBonificacao);
			}
			
			set
			{
				base.SetSystemInt32(BonificacaoBolsaMetadata.ColumnNames.IdBonificacao, value);
			}
		}
		
		/// <summary>
		/// Maps to BonificacaoBolsa.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(BonificacaoBolsaMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(BonificacaoBolsaMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to BonificacaoBolsa.DataEx
		/// </summary>
		virtual public System.DateTime? DataEx
		{
			get
			{
				return base.GetSystemDateTime(BonificacaoBolsaMetadata.ColumnNames.DataEx);
			}
			
			set
			{
				base.SetSystemDateTime(BonificacaoBolsaMetadata.ColumnNames.DataEx, value);
			}
		}
		
		/// <summary>
		/// Maps to BonificacaoBolsa.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(BonificacaoBolsaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(BonificacaoBolsaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to BonificacaoBolsa.TipoFracao
		/// </summary>
		virtual public System.Byte? TipoFracao
		{
			get
			{
				return base.GetSystemByte(BonificacaoBolsaMetadata.ColumnNames.TipoFracao);
			}
			
			set
			{
				base.SetSystemByte(BonificacaoBolsaMetadata.ColumnNames.TipoFracao, value);
			}
		}
		
		/// <summary>
		/// Maps to BonificacaoBolsa.PUBonificacao
		/// </summary>
		virtual public System.Decimal? PUBonificacao
		{
			get
			{
				return base.GetSystemDecimal(BonificacaoBolsaMetadata.ColumnNames.PUBonificacao);
			}
			
			set
			{
				base.SetSystemDecimal(BonificacaoBolsaMetadata.ColumnNames.PUBonificacao, value);
			}
		}
		
		/// <summary>
		/// Maps to BonificacaoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to BonificacaoBolsa.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(BonificacaoBolsaMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(BonificacaoBolsaMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to BonificacaoBolsa.CdAtivoBolsaDestino
		/// </summary>
		virtual public System.String CdAtivoBolsaDestino
		{
			get
			{
				return base.GetSystemString(BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino);
			}
			
			set
			{
				if(base.SetSystemString(BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsaDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to BonificacaoBolsa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(BonificacaoBolsaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(BonificacaoBolsaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsaDestino;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esBonificacaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdBonificacao
			{
				get
				{
					System.Int32? data = entity.IdBonificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBonificacao = null;
					else entity.IdBonificacao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEx
			{
				get
				{
					System.DateTime? data = entity.DataEx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEx = null;
					else entity.DataEx = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoFracao
			{
				get
				{
					System.Byte? data = entity.TipoFracao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoFracao = null;
					else entity.TipoFracao = Convert.ToByte(value);
				}
			}
				
			public System.String PUBonificacao
			{
				get
				{
					System.Decimal? data = entity.PUBonificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUBonificacao = null;
					else entity.PUBonificacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String CdAtivoBolsaDestino
			{
				get
				{
					System.String data = entity.CdAtivoBolsaDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsaDestino = null;
					else entity.CdAtivoBolsaDestino = Convert.ToString(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
			

			private esBonificacaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esBonificacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esBonificacaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class BonificacaoBolsa : esBonificacaoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_BonificacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsaDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_BonificacaoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsaDestino
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsaDestino == null
					&& CdAtivoBolsaDestino != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsaDestino = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsaDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaDestino", this._UpToAtivoBolsaByCdAtivoBolsaDestino);
					this._UpToAtivoBolsaByCdAtivoBolsaDestino.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsaDestino.Query.CdAtivoBolsa == this.CdAtivoBolsaDestino);
					this._UpToAtivoBolsaByCdAtivoBolsaDestino.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsaDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsaDestino");
				

				if(value == null)
				{
					this.CdAtivoBolsaDestino = null;
					this._UpToAtivoBolsaByCdAtivoBolsaDestino = null;
				}
				else
				{
					this.CdAtivoBolsaDestino = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsaDestino = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsaDestino", this._UpToAtivoBolsaByCdAtivoBolsaDestino);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esBonificacaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return BonificacaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdBonificacao
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.IdBonificacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataEx
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.DataEx, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoFracao
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.TipoFracao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PUBonificacao
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.PUBonificacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CdAtivoBolsaDestino
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino, esSystemType.String);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, BonificacaoBolsaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("BonificacaoBolsaCollection")]
	public partial class BonificacaoBolsaCollection : esBonificacaoBolsaCollection, IEnumerable<BonificacaoBolsa>
	{
		public BonificacaoBolsaCollection()
		{

		}
		
		public static implicit operator List<BonificacaoBolsa>(BonificacaoBolsaCollection coll)
		{
			List<BonificacaoBolsa> list = new List<BonificacaoBolsa>();
			
			foreach (BonificacaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  BonificacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BonificacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new BonificacaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new BonificacaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public BonificacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BonificacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(BonificacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public BonificacaoBolsa AddNew()
		{
			BonificacaoBolsa entity = base.AddNewEntity() as BonificacaoBolsa;
			
			return entity;
		}

		public BonificacaoBolsa FindByPrimaryKey(System.Int32 idBonificacao)
		{
			return base.FindByPrimaryKey(idBonificacao) as BonificacaoBolsa;
		}


		#region IEnumerable<BonificacaoBolsa> Members

		IEnumerator<BonificacaoBolsa> IEnumerable<BonificacaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as BonificacaoBolsa;
			}
		}

		#endregion
		
		private BonificacaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'BonificacaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class BonificacaoBolsa : esBonificacaoBolsa
	{
		public BonificacaoBolsa()
		{

		}
	
		public BonificacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return BonificacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esBonificacaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BonificacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public BonificacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BonificacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(BonificacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private BonificacaoBolsaQuery query;
	}



	[Serializable]
	public partial class BonificacaoBolsaQuery : esBonificacaoBolsaQuery
	{
		public BonificacaoBolsaQuery()
		{

		}		
		
		public BonificacaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class BonificacaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected BonificacaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.IdBonificacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.IdBonificacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.DataLancamento, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.DataEx, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.DataEx;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.DataReferencia, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.DataReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.TipoFracao, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.TipoFracao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.PUBonificacao, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.PUBonificacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.Percentual, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 20;
			c.NumericScale = 11;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.CdAtivoBolsaDestino, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.CdAtivoBolsaDestino;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BonificacaoBolsaMetadata.ColumnNames.Fonte, 9, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = BonificacaoBolsaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public BonificacaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdBonificacao = "IdBonificacao";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataReferencia = "DataReferencia";
			 public const string TipoFracao = "TipoFracao";
			 public const string PUBonificacao = "PUBonificacao";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Percentual = "Percentual";
			 public const string CdAtivoBolsaDestino = "CdAtivoBolsaDestino";
			 public const string Fonte = "Fonte";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdBonificacao = "IdBonificacao";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataReferencia = "DataReferencia";
			 public const string TipoFracao = "TipoFracao";
			 public const string PUBonificacao = "PUBonificacao";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Percentual = "Percentual";
			 public const string CdAtivoBolsaDestino = "CdAtivoBolsaDestino";
			 public const string Fonte = "Fonte";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(BonificacaoBolsaMetadata))
			{
				if(BonificacaoBolsaMetadata.mapDelegates == null)
				{
					BonificacaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (BonificacaoBolsaMetadata.meta == null)
				{
					BonificacaoBolsaMetadata.meta = new BonificacaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdBonificacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEx", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoFracao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PUBonificacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CdAtivoBolsaDestino", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "BonificacaoBolsa";
				meta.Destination = "BonificacaoBolsa";
				
				meta.spInsert = "proc_BonificacaoBolsaInsert";				
				meta.spUpdate = "proc_BonificacaoBolsaUpdate";		
				meta.spDelete = "proc_BonificacaoBolsaDelete";
				meta.spLoadAll = "proc_BonificacaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_BonificacaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private BonificacaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
