/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:15
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		


		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esFatorCotacaoBolsaCollection : esEntityCollection
	{
		public esFatorCotacaoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "FatorCotacaoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esFatorCotacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esFatorCotacaoBolsaQuery);
		}
		#endregion
		
		virtual public FatorCotacaoBolsa DetachEntity(FatorCotacaoBolsa entity)
		{
			return base.DetachEntity(entity) as FatorCotacaoBolsa;
		}
		
		virtual public FatorCotacaoBolsa AttachEntity(FatorCotacaoBolsa entity)
		{
			return base.AttachEntity(entity) as FatorCotacaoBolsa;
		}
		
		virtual public void Combine(FatorCotacaoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public FatorCotacaoBolsa this[int index]
		{
			get
			{
				return base[index] as FatorCotacaoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(FatorCotacaoBolsa);
		}
	}



	[Serializable]
	abstract public class esFatorCotacaoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esFatorCotacaoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esFatorCotacaoBolsa()
		{

		}

		public esFatorCotacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBolsa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esFatorCotacaoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CdAtivoBolsa == cdAtivoBolsa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBolsa);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			esFatorCotacaoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CdAtivoBolsa == cdAtivoBolsa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CdAtivoBolsa",cdAtivoBolsa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Fator": this.str.Fator = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "Fator":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Fator = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to FatorCotacaoBolsa.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(FatorCotacaoBolsaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(FatorCotacaoBolsaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to FatorCotacaoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(FatorCotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(FatorCotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to FatorCotacaoBolsa.Fator
		/// </summary>
		virtual public System.Decimal? Fator
		{
			get
			{
				return base.GetSystemDecimal(FatorCotacaoBolsaMetadata.ColumnNames.Fator);
			}
			
			set
			{
				base.SetSystemDecimal(FatorCotacaoBolsaMetadata.ColumnNames.Fator, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esFatorCotacaoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Fator
			{
				get
				{
					System.Decimal? data = entity.Fator;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fator = null;
					else entity.Fator = Convert.ToDecimal(value);
				}
			}
			

			private esFatorCotacaoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esFatorCotacaoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esFatorCotacaoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class FatorCotacaoBolsa : esFatorCotacaoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_FatorCotacaoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esFatorCotacaoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return FatorCotacaoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, FatorCotacaoBolsaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, FatorCotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Fator
		{
			get
			{
				return new esQueryItem(this, FatorCotacaoBolsaMetadata.ColumnNames.Fator, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("FatorCotacaoBolsaCollection")]
	public partial class FatorCotacaoBolsaCollection : esFatorCotacaoBolsaCollection, IEnumerable<FatorCotacaoBolsa>
	{
		public FatorCotacaoBolsaCollection()
		{

		}
		
		public static implicit operator List<FatorCotacaoBolsa>(FatorCotacaoBolsaCollection coll)
		{
			List<FatorCotacaoBolsa> list = new List<FatorCotacaoBolsa>();
			
			foreach (FatorCotacaoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  FatorCotacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FatorCotacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new FatorCotacaoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new FatorCotacaoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public FatorCotacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FatorCotacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(FatorCotacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public FatorCotacaoBolsa AddNew()
		{
			FatorCotacaoBolsa entity = base.AddNewEntity() as FatorCotacaoBolsa;
			
			return entity;
		}

		public FatorCotacaoBolsa FindByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			return base.FindByPrimaryKey(dataReferencia, cdAtivoBolsa) as FatorCotacaoBolsa;
		}


		#region IEnumerable<FatorCotacaoBolsa> Members

		IEnumerator<FatorCotacaoBolsa> IEnumerable<FatorCotacaoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as FatorCotacaoBolsa;
			}
		}

		#endregion
		
		private FatorCotacaoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'FatorCotacaoBolsa' table
	/// </summary>

	[Serializable]
	public partial class FatorCotacaoBolsa : esFatorCotacaoBolsa
	{
		public FatorCotacaoBolsa()
		{

		}
	
		public FatorCotacaoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return FatorCotacaoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esFatorCotacaoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new FatorCotacaoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public FatorCotacaoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new FatorCotacaoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(FatorCotacaoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private FatorCotacaoBolsaQuery query;
	}



	[Serializable]
	public partial class FatorCotacaoBolsaQuery : esFatorCotacaoBolsaQuery
	{
		public FatorCotacaoBolsaQuery()
		{

		}		
		
		public FatorCotacaoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class FatorCotacaoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected FatorCotacaoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(FatorCotacaoBolsaMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = FatorCotacaoBolsaMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FatorCotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = FatorCotacaoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(FatorCotacaoBolsaMetadata.ColumnNames.Fator, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = FatorCotacaoBolsaMetadata.PropertyNames.Fator;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public FatorCotacaoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Fator = "Fator";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Fator = "Fator";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(FatorCotacaoBolsaMetadata))
			{
				if(FatorCotacaoBolsaMetadata.mapDelegates == null)
				{
					FatorCotacaoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (FatorCotacaoBolsaMetadata.meta == null)
				{
					FatorCotacaoBolsaMetadata.meta = new FatorCotacaoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Fator", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "FatorCotacaoBolsa";
				meta.Destination = "FatorCotacaoBolsa";
				
				meta.spInsert = "proc_FatorCotacaoBolsaInsert";				
				meta.spUpdate = "proc_FatorCotacaoBolsaUpdate";		
				meta.spDelete = "proc_FatorCotacaoBolsaDelete";
				meta.spLoadAll = "proc_FatorCotacaoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_FatorCotacaoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private FatorCotacaoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
