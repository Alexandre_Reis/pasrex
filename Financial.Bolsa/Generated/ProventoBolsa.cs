/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:16
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esProventoBolsaCollection : esEntityCollection
	{
		public esProventoBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ProventoBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esProventoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esProventoBolsaQuery);
		}
		#endregion
		
		virtual public ProventoBolsa DetachEntity(ProventoBolsa entity)
		{
			return base.DetachEntity(entity) as ProventoBolsa;
		}
		
		virtual public ProventoBolsa AttachEntity(ProventoBolsa entity)
		{
			return base.AttachEntity(entity) as ProventoBolsa;
		}
		
		virtual public void Combine(ProventoBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ProventoBolsa this[int index]
		{
			get
			{
				return base[index] as ProventoBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ProventoBolsa);
		}
	}



	[Serializable]
	abstract public class esProventoBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esProventoBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esProventoBolsa()
		{

		}

		public esProventoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idProvento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idProvento);
			else
				return LoadByPrimaryKeyStoredProcedure(idProvento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idProvento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esProventoBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdProvento == idProvento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idProvento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idProvento);
			else
				return LoadByPrimaryKeyStoredProcedure(idProvento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idProvento)
		{
			esProventoBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdProvento == idProvento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idProvento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdProvento",idProvento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdProvento": this.str.IdProvento = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataEx": this.str.DataEx = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "TipoProvento": this.str.TipoProvento = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdProvento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdProvento = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataEx":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEx = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "TipoProvento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoProvento = (System.Byte?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ProventoBolsa.IdProvento
		/// </summary>
		virtual public System.Int32? IdProvento
		{
			get
			{
				return base.GetSystemInt32(ProventoBolsaMetadata.ColumnNames.IdProvento);
			}
			
			set
			{
				base.SetSystemInt32(ProventoBolsaMetadata.ColumnNames.IdProvento, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(ProventoBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(ProventoBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsa.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(ProventoBolsaMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(ProventoBolsaMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsa.DataEx
		/// </summary>
		virtual public System.DateTime? DataEx
		{
			get
			{
				return base.GetSystemDateTime(ProventoBolsaMetadata.ColumnNames.DataEx);
			}
			
			set
			{
				base.SetSystemDateTime(ProventoBolsaMetadata.ColumnNames.DataEx, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsa.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(ProventoBolsaMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(ProventoBolsaMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsa.TipoProvento
		/// </summary>
		virtual public System.Byte? TipoProvento
		{
			get
			{
				return base.GetSystemByte(ProventoBolsaMetadata.ColumnNames.TipoProvento);
			}
			
			set
			{
				base.SetSystemByte(ProventoBolsaMetadata.ColumnNames.TipoProvento, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(ProventoBolsaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(ProventoBolsaMetadata.ColumnNames.Valor, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esProventoBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdProvento
			{
				get
				{
					System.Int32? data = entity.IdProvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdProvento = null;
					else entity.IdProvento = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEx
			{
				get
				{
					System.DateTime? data = entity.DataEx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEx = null;
					else entity.DataEx = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoProvento
			{
				get
				{
					System.Byte? data = entity.TipoProvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoProvento = null;
					else entity.TipoProvento = Convert.ToByte(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
			

			private esProventoBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esProventoBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esProventoBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ProventoBolsa : esProventoBolsa
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_ProventoBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esProventoBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ProventoBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdProvento
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaMetadata.ColumnNames.IdProvento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataEx
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaMetadata.ColumnNames.DataEx, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoProvento
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaMetadata.ColumnNames.TipoProvento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ProventoBolsaCollection")]
	public partial class ProventoBolsaCollection : esProventoBolsaCollection, IEnumerable<ProventoBolsa>
	{
		public ProventoBolsaCollection()
		{

		}
		
		public static implicit operator List<ProventoBolsa>(ProventoBolsaCollection coll)
		{
			List<ProventoBolsa> list = new List<ProventoBolsa>();
			
			foreach (ProventoBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ProventoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ProventoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ProventoBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ProventoBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ProventoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ProventoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ProventoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ProventoBolsa AddNew()
		{
			ProventoBolsa entity = base.AddNewEntity() as ProventoBolsa;
			
			return entity;
		}

		public ProventoBolsa FindByPrimaryKey(System.Int32 idProvento)
		{
			return base.FindByPrimaryKey(idProvento) as ProventoBolsa;
		}


		#region IEnumerable<ProventoBolsa> Members

		IEnumerator<ProventoBolsa> IEnumerable<ProventoBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ProventoBolsa;
			}
		}

		#endregion
		
		private ProventoBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ProventoBolsa' table
	/// </summary>

	[Serializable]
	public partial class ProventoBolsa : esProventoBolsa
	{
		public ProventoBolsa()
		{

		}
	
		public ProventoBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ProventoBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esProventoBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ProventoBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ProventoBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ProventoBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ProventoBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ProventoBolsaQuery query;
	}



	[Serializable]
	public partial class ProventoBolsaQuery : esProventoBolsaQuery
	{
		public ProventoBolsaQuery()
		{

		}		
		
		public ProventoBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ProventoBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ProventoBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ProventoBolsaMetadata.ColumnNames.IdProvento, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ProventoBolsaMetadata.PropertyNames.IdProvento;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaMetadata.ColumnNames.CdAtivoBolsa, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ProventoBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaMetadata.ColumnNames.DataLancamento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ProventoBolsaMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaMetadata.ColumnNames.DataEx, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ProventoBolsaMetadata.PropertyNames.DataEx;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaMetadata.ColumnNames.DataPagamento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ProventoBolsaMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaMetadata.ColumnNames.TipoProvento, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ProventoBolsaMetadata.PropertyNames.TipoProvento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaMetadata.ColumnNames.Valor, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ProventoBolsaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 20;
			c.NumericScale = 11;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ProventoBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdProvento = "IdProvento";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataPagamento = "DataPagamento";
			 public const string TipoProvento = "TipoProvento";
			 public const string Valor = "Valor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdProvento = "IdProvento";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataPagamento = "DataPagamento";
			 public const string TipoProvento = "TipoProvento";
			 public const string Valor = "Valor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ProventoBolsaMetadata))
			{
				if(ProventoBolsaMetadata.mapDelegates == null)
				{
					ProventoBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ProventoBolsaMetadata.meta == null)
				{
					ProventoBolsaMetadata.meta = new ProventoBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdProvento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEx", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoProvento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ProventoBolsa";
				meta.Destination = "ProventoBolsa";
				
				meta.spInsert = "proc_ProventoBolsaInsert";				
				meta.spUpdate = "proc_ProventoBolsaUpdate";		
				meta.spDelete = "proc_ProventoBolsaDelete";
				meta.spLoadAll = "proc_ProventoBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ProventoBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ProventoBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
