/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		


























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esTemplateCorretagemBolsaCollection : esEntityCollection
	{
		public esTemplateCorretagemBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TemplateCorretagemBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTemplateCorretagemBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTemplateCorretagemBolsaQuery);
		}
		#endregion
		
		virtual public TemplateCorretagemBolsa DetachEntity(TemplateCorretagemBolsa entity)
		{
			return base.DetachEntity(entity) as TemplateCorretagemBolsa;
		}
		
		virtual public TemplateCorretagemBolsa AttachEntity(TemplateCorretagemBolsa entity)
		{
			return base.AttachEntity(entity) as TemplateCorretagemBolsa;
		}
		
		virtual public void Combine(TemplateCorretagemBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TemplateCorretagemBolsa this[int index]
		{
			get
			{
				return base[index] as TemplateCorretagemBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TemplateCorretagemBolsa);
		}
	}



	[Serializable]
	abstract public class esTemplateCorretagemBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTemplateCorretagemBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTemplateCorretagemBolsa()
		{

		}

		public esTemplateCorretagemBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTemplate)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTemplate);
			else
				return LoadByPrimaryKeyStoredProcedure(idTemplate);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTemplate)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTemplateCorretagemBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTemplate == idTemplate);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTemplate)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTemplate);
			else
				return LoadByPrimaryKeyStoredProcedure(idTemplate);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTemplate)
		{
			esTemplateCorretagemBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTemplate == idTemplate);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTemplate)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTemplate",idTemplate);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTemplate": this.str.IdTemplate = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "TipoCorretagem": this.str.TipoCorretagem = (string)value; break;							
						case "ValorFixoOperacao": this.str.ValorFixoOperacao = (string)value; break;							
						case "Teto": this.str.Teto = (string)value; break;							
						case "PercentualDesconto": this.str.PercentualDesconto = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTemplate":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTemplate = (System.Int32?)value;
							break;
						
						case "TipoCorretagem":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCorretagem = (System.Byte?)value;
							break;
						
						case "ValorFixoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFixoOperacao = (System.Decimal?)value;
							break;
						
						case "Teto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Teto = (System.Decimal?)value;
							break;
						
						case "PercentualDesconto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualDesconto = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TemplateCorretagemBolsa.IdTemplate
		/// </summary>
		virtual public System.Int32? IdTemplate
		{
			get
			{
				return base.GetSystemInt32(TemplateCorretagemBolsaMetadata.ColumnNames.IdTemplate);
			}
			
			set
			{
				base.SetSystemInt32(TemplateCorretagemBolsaMetadata.ColumnNames.IdTemplate, value);
			}
		}
		
		/// <summary>
		/// Maps to TemplateCorretagemBolsa.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TemplateCorretagemBolsaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TemplateCorretagemBolsaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to TemplateCorretagemBolsa.TipoCorretagem
		/// </summary>
		virtual public System.Byte? TipoCorretagem
		{
			get
			{
				return base.GetSystemByte(TemplateCorretagemBolsaMetadata.ColumnNames.TipoCorretagem);
			}
			
			set
			{
				base.SetSystemByte(TemplateCorretagemBolsaMetadata.ColumnNames.TipoCorretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to TemplateCorretagemBolsa.ValorFixoOperacao
		/// </summary>
		virtual public System.Decimal? ValorFixoOperacao
		{
			get
			{
				return base.GetSystemDecimal(TemplateCorretagemBolsaMetadata.ColumnNames.ValorFixoOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(TemplateCorretagemBolsaMetadata.ColumnNames.ValorFixoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TemplateCorretagemBolsa.Teto
		/// </summary>
		virtual public System.Decimal? Teto
		{
			get
			{
				return base.GetSystemDecimal(TemplateCorretagemBolsaMetadata.ColumnNames.Teto);
			}
			
			set
			{
				base.SetSystemDecimal(TemplateCorretagemBolsaMetadata.ColumnNames.Teto, value);
			}
		}
		
		/// <summary>
		/// Maps to TemplateCorretagemBolsa.PercentualDesconto
		/// </summary>
		virtual public System.Decimal? PercentualDesconto
		{
			get
			{
				return base.GetSystemDecimal(TemplateCorretagemBolsaMetadata.ColumnNames.PercentualDesconto);
			}
			
			set
			{
				base.SetSystemDecimal(TemplateCorretagemBolsaMetadata.ColumnNames.PercentualDesconto, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTemplateCorretagemBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTemplate
			{
				get
				{
					System.Int32? data = entity.IdTemplate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTemplate = null;
					else entity.IdTemplate = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String TipoCorretagem
			{
				get
				{
					System.Byte? data = entity.TipoCorretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCorretagem = null;
					else entity.TipoCorretagem = Convert.ToByte(value);
				}
			}
				
			public System.String ValorFixoOperacao
			{
				get
				{
					System.Decimal? data = entity.ValorFixoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFixoOperacao = null;
					else entity.ValorFixoOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String Teto
			{
				get
				{
					System.Decimal? data = entity.Teto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Teto = null;
					else entity.Teto = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualDesconto
			{
				get
				{
					System.Decimal? data = entity.PercentualDesconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualDesconto = null;
					else entity.PercentualDesconto = Convert.ToDecimal(value);
				}
			}
			

			private esTemplateCorretagemBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTemplateCorretagemBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTemplateCorretagemBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TemplateCorretagemBolsa : esTemplateCorretagemBolsa
	{

				
		#region PerfilCorretagemBolsaCollectionByIdTemplate - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TemplateCorretagemBolsa_PerfilCorretagemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public PerfilCorretagemBolsaCollection PerfilCorretagemBolsaCollectionByIdTemplate
		{
			get
			{
				if(this._PerfilCorretagemBolsaCollectionByIdTemplate == null)
				{
					this._PerfilCorretagemBolsaCollectionByIdTemplate = new PerfilCorretagemBolsaCollection();
					this._PerfilCorretagemBolsaCollectionByIdTemplate.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilCorretagemBolsaCollectionByIdTemplate", this._PerfilCorretagemBolsaCollectionByIdTemplate);
				
					if(this.IdTemplate != null)
					{
						this._PerfilCorretagemBolsaCollectionByIdTemplate.Query.Where(this._PerfilCorretagemBolsaCollectionByIdTemplate.Query.IdTemplate == this.IdTemplate);
						this._PerfilCorretagemBolsaCollectionByIdTemplate.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilCorretagemBolsaCollectionByIdTemplate.fks.Add(PerfilCorretagemBolsaMetadata.ColumnNames.IdTemplate, this.IdTemplate);
					}
				}

				return this._PerfilCorretagemBolsaCollectionByIdTemplate;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilCorretagemBolsaCollectionByIdTemplate != null) 
				{ 
					this.RemovePostSave("PerfilCorretagemBolsaCollectionByIdTemplate"); 
					this._PerfilCorretagemBolsaCollectionByIdTemplate = null;
					
				} 
			} 			
		}

		private PerfilCorretagemBolsaCollection _PerfilCorretagemBolsaCollectionByIdTemplate;
		#endregion

				
		#region TabelaCorretagemBolsaCollectionByIdTemplate - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TemplateCorretagemBolsa_TabelaCorretagemBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public TabelaCorretagemBolsaCollection TabelaCorretagemBolsaCollectionByIdTemplate
		{
			get
			{
				if(this._TabelaCorretagemBolsaCollectionByIdTemplate == null)
				{
					this._TabelaCorretagemBolsaCollectionByIdTemplate = new TabelaCorretagemBolsaCollection();
					this._TabelaCorretagemBolsaCollectionByIdTemplate.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaCorretagemBolsaCollectionByIdTemplate", this._TabelaCorretagemBolsaCollectionByIdTemplate);
				
					if(this.IdTemplate != null)
					{
						this._TabelaCorretagemBolsaCollectionByIdTemplate.Query.Where(this._TabelaCorretagemBolsaCollectionByIdTemplate.Query.IdTemplate == this.IdTemplate);
						this._TabelaCorretagemBolsaCollectionByIdTemplate.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaCorretagemBolsaCollectionByIdTemplate.fks.Add(TabelaCorretagemBolsaMetadata.ColumnNames.IdTemplate, this.IdTemplate);
					}
				}

				return this._TabelaCorretagemBolsaCollectionByIdTemplate;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaCorretagemBolsaCollectionByIdTemplate != null) 
				{ 
					this.RemovePostSave("TabelaCorretagemBolsaCollectionByIdTemplate"); 
					this._TabelaCorretagemBolsaCollectionByIdTemplate = null;
					
				} 
			} 			
		}

		private TabelaCorretagemBolsaCollection _TabelaCorretagemBolsaCollectionByIdTemplate;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "PerfilCorretagemBolsaCollectionByIdTemplate", typeof(PerfilCorretagemBolsaCollection), new PerfilCorretagemBolsa()));
			props.Add(new esPropertyDescriptor(this, "TabelaCorretagemBolsaCollectionByIdTemplate", typeof(TabelaCorretagemBolsaCollection), new TabelaCorretagemBolsa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._PerfilCorretagemBolsaCollectionByIdTemplate != null)
			{
				foreach(PerfilCorretagemBolsa obj in this._PerfilCorretagemBolsaCollectionByIdTemplate)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTemplate = this.IdTemplate;
					}
				}
			}
			if(this._TabelaCorretagemBolsaCollectionByIdTemplate != null)
			{
				foreach(TabelaCorretagemBolsa obj in this._TabelaCorretagemBolsaCollectionByIdTemplate)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTemplate = this.IdTemplate;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTemplateCorretagemBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TemplateCorretagemBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTemplate
		{
			get
			{
				return new esQueryItem(this, TemplateCorretagemBolsaMetadata.ColumnNames.IdTemplate, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TemplateCorretagemBolsaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoCorretagem
		{
			get
			{
				return new esQueryItem(this, TemplateCorretagemBolsaMetadata.ColumnNames.TipoCorretagem, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ValorFixoOperacao
		{
			get
			{
				return new esQueryItem(this, TemplateCorretagemBolsaMetadata.ColumnNames.ValorFixoOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Teto
		{
			get
			{
				return new esQueryItem(this, TemplateCorretagemBolsaMetadata.ColumnNames.Teto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualDesconto
		{
			get
			{
				return new esQueryItem(this, TemplateCorretagemBolsaMetadata.ColumnNames.PercentualDesconto, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TemplateCorretagemBolsaCollection")]
	public partial class TemplateCorretagemBolsaCollection : esTemplateCorretagemBolsaCollection, IEnumerable<TemplateCorretagemBolsa>
	{
		public TemplateCorretagemBolsaCollection()
		{

		}
		
		public static implicit operator List<TemplateCorretagemBolsa>(TemplateCorretagemBolsaCollection coll)
		{
			List<TemplateCorretagemBolsa> list = new List<TemplateCorretagemBolsa>();
			
			foreach (TemplateCorretagemBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TemplateCorretagemBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TemplateCorretagemBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TemplateCorretagemBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TemplateCorretagemBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TemplateCorretagemBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TemplateCorretagemBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TemplateCorretagemBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TemplateCorretagemBolsa AddNew()
		{
			TemplateCorretagemBolsa entity = base.AddNewEntity() as TemplateCorretagemBolsa;
			
			return entity;
		}

		public TemplateCorretagemBolsa FindByPrimaryKey(System.Int32 idTemplate)
		{
			return base.FindByPrimaryKey(idTemplate) as TemplateCorretagemBolsa;
		}


		#region IEnumerable<TemplateCorretagemBolsa> Members

		IEnumerator<TemplateCorretagemBolsa> IEnumerable<TemplateCorretagemBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TemplateCorretagemBolsa;
			}
		}

		#endregion
		
		private TemplateCorretagemBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TemplateCorretagemBolsa' table
	/// </summary>

	[Serializable]
	public partial class TemplateCorretagemBolsa : esTemplateCorretagemBolsa
	{
		public TemplateCorretagemBolsa()
		{

		}
	
		public TemplateCorretagemBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TemplateCorretagemBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esTemplateCorretagemBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TemplateCorretagemBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TemplateCorretagemBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TemplateCorretagemBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TemplateCorretagemBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TemplateCorretagemBolsaQuery query;
	}



	[Serializable]
	public partial class TemplateCorretagemBolsaQuery : esTemplateCorretagemBolsaQuery
	{
		public TemplateCorretagemBolsaQuery()
		{

		}		
		
		public TemplateCorretagemBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TemplateCorretagemBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TemplateCorretagemBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TemplateCorretagemBolsaMetadata.ColumnNames.IdTemplate, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TemplateCorretagemBolsaMetadata.PropertyNames.IdTemplate;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TemplateCorretagemBolsaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TemplateCorretagemBolsaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TemplateCorretagemBolsaMetadata.ColumnNames.TipoCorretagem, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TemplateCorretagemBolsaMetadata.PropertyNames.TipoCorretagem;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TemplateCorretagemBolsaMetadata.ColumnNames.ValorFixoOperacao, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TemplateCorretagemBolsaMetadata.PropertyNames.ValorFixoOperacao;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TemplateCorretagemBolsaMetadata.ColumnNames.Teto, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TemplateCorretagemBolsaMetadata.PropertyNames.Teto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TemplateCorretagemBolsaMetadata.ColumnNames.PercentualDesconto, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TemplateCorretagemBolsaMetadata.PropertyNames.PercentualDesconto;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TemplateCorretagemBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTemplate = "IdTemplate";
			 public const string Descricao = "Descricao";
			 public const string TipoCorretagem = "TipoCorretagem";
			 public const string ValorFixoOperacao = "ValorFixoOperacao";
			 public const string Teto = "Teto";
			 public const string PercentualDesconto = "PercentualDesconto";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTemplate = "IdTemplate";
			 public const string Descricao = "Descricao";
			 public const string TipoCorretagem = "TipoCorretagem";
			 public const string ValorFixoOperacao = "ValorFixoOperacao";
			 public const string Teto = "Teto";
			 public const string PercentualDesconto = "PercentualDesconto";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TemplateCorretagemBolsaMetadata))
			{
				if(TemplateCorretagemBolsaMetadata.mapDelegates == null)
				{
					TemplateCorretagemBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TemplateCorretagemBolsaMetadata.meta == null)
				{
					TemplateCorretagemBolsaMetadata.meta = new TemplateCorretagemBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTemplate", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoCorretagem", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ValorFixoOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Teto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualDesconto", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TemplateCorretagemBolsa";
				meta.Destination = "TemplateCorretagemBolsa";
				
				meta.spInsert = "proc_TemplateCorretagemBolsaInsert";				
				meta.spUpdate = "proc_TemplateCorretagemBolsaUpdate";		
				meta.spDelete = "proc_TemplateCorretagemBolsaDelete";
				meta.spLoadAll = "proc_TemplateCorretagemBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TemplateCorretagemBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TemplateCorretagemBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
