/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esTabelaCRCACollection : esEntityCollection
	{
		public esTabelaCRCACollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaCRCACollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaCRCAQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaCRCAQuery);
		}
		#endregion
		
		virtual public TabelaCRCA DetachEntity(TabelaCRCA entity)
		{
			return base.DetachEntity(entity) as TabelaCRCA;
		}
		
		virtual public TabelaCRCA AttachEntity(TabelaCRCA entity)
		{
			return base.AttachEntity(entity) as TabelaCRCA;
		}
		
		virtual public void Combine(TabelaCRCACollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaCRCA this[int index]
		{
			get
			{
				return base[index] as TabelaCRCA;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaCRCA);
		}
	}



	[Serializable]
	abstract public class esTabelaCRCA : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaCRCAQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaCRCA()
		{

		}

		public esTabelaCRCA(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaCRCAQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTabela == idTabela);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTabela);
			else
				return LoadByPrimaryKeyStoredProcedure(idTabela);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
		{
			esTabelaCRCAQuery query = this.GetDynamicQuery();
			query.Where(query.IdTabela == idTabela);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTabela",idTabela);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTabela": this.str.IdTabela = (string)value; break;							
						case "DataMovimento": this.str.DataMovimento = (string)value; break;							
						case "Modalidade": this.str.Modalidade = (string)value; break;							
						case "CpfCNPJ": this.str.CpfCNPJ = (string)value; break;							
						case "CodigoCustodiante": this.str.CodigoCustodiante = (string)value; break;							
						case "CodigoCliente": this.str.CodigoCliente = (string)value; break;							
						case "QuantidadeEfetiva": this.str.QuantidadeEfetiva = (string)value; break;							
						case "ValorEfetivo": this.str.ValorEfetivo = (string)value; break;							
						case "CodigoIsin": this.str.CodigoIsin = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTabela":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTabela = (System.Int32?)value;
							break;
						
						case "DataMovimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataMovimento = (System.DateTime?)value;
							break;
						
						case "CpfCNPJ":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CpfCNPJ = (System.Int32?)value;
							break;
						
						case "CodigoCustodiante":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoCustodiante = (System.Int32?)value;
							break;
						
						case "CodigoCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoCliente = (System.Int32?)value;
							break;
						
						case "QuantidadeEfetiva":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.QuantidadeEfetiva = (System.Int32?)value;
							break;
						
						case "ValorEfetivo":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorEfetivo = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaCRCA.IdTabela
		/// </summary>
		virtual public System.Int32? IdTabela
		{
			get
			{
				return base.GetSystemInt32(TabelaCRCAMetadata.ColumnNames.IdTabela);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCRCAMetadata.ColumnNames.IdTabela, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCRCA.DataMovimento
		/// </summary>
		virtual public System.DateTime? DataMovimento
		{
			get
			{
				return base.GetSystemDateTime(TabelaCRCAMetadata.ColumnNames.DataMovimento);
			}
			
			set
			{
				base.SetSystemDateTime(TabelaCRCAMetadata.ColumnNames.DataMovimento, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCRCA.Modalidade
		/// </summary>
		virtual public System.String Modalidade
		{
			get
			{
				return base.GetSystemString(TabelaCRCAMetadata.ColumnNames.Modalidade);
			}
			
			set
			{
				base.SetSystemString(TabelaCRCAMetadata.ColumnNames.Modalidade, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCRCA.CpfCNPJ
		/// </summary>
		virtual public System.Int32? CpfCNPJ
		{
			get
			{
				return base.GetSystemInt32(TabelaCRCAMetadata.ColumnNames.CpfCNPJ);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCRCAMetadata.ColumnNames.CpfCNPJ, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCRCA.CodigoCustodiante
		/// </summary>
		virtual public System.Int32? CodigoCustodiante
		{
			get
			{
				return base.GetSystemInt32(TabelaCRCAMetadata.ColumnNames.CodigoCustodiante);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCRCAMetadata.ColumnNames.CodigoCustodiante, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCRCA.CodigoCliente
		/// </summary>
		virtual public System.Int32? CodigoCliente
		{
			get
			{
				return base.GetSystemInt32(TabelaCRCAMetadata.ColumnNames.CodigoCliente);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCRCAMetadata.ColumnNames.CodigoCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCRCA.QuantidadeEfetiva
		/// </summary>
		virtual public System.Int32? QuantidadeEfetiva
		{
			get
			{
				return base.GetSystemInt32(TabelaCRCAMetadata.ColumnNames.QuantidadeEfetiva);
			}
			
			set
			{
				base.SetSystemInt32(TabelaCRCAMetadata.ColumnNames.QuantidadeEfetiva, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCRCA.ValorEfetivo
		/// </summary>
		virtual public System.Decimal? ValorEfetivo
		{
			get
			{
				return base.GetSystemDecimal(TabelaCRCAMetadata.ColumnNames.ValorEfetivo);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaCRCAMetadata.ColumnNames.ValorEfetivo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaCRCA.CodigoIsin
		/// </summary>
		virtual public System.String CodigoIsin
		{
			get
			{
				return base.GetSystemString(TabelaCRCAMetadata.ColumnNames.CodigoIsin);
			}
			
			set
			{
				base.SetSystemString(TabelaCRCAMetadata.ColumnNames.CodigoIsin, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaCRCA entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTabela
			{
				get
				{
					System.Int32? data = entity.IdTabela;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTabela = null;
					else entity.IdTabela = Convert.ToInt32(value);
				}
			}
				
			public System.String DataMovimento
			{
				get
				{
					System.DateTime? data = entity.DataMovimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataMovimento = null;
					else entity.DataMovimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Modalidade
			{
				get
				{
					System.String data = entity.Modalidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Modalidade = null;
					else entity.Modalidade = Convert.ToString(value);
				}
			}
				
			public System.String CpfCNPJ
			{
				get
				{
					System.Int32? data = entity.CpfCNPJ;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CpfCNPJ = null;
					else entity.CpfCNPJ = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCustodiante
			{
				get
				{
					System.Int32? data = entity.CodigoCustodiante;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCustodiante = null;
					else entity.CodigoCustodiante = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCliente
			{
				get
				{
					System.Int32? data = entity.CodigoCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCliente = null;
					else entity.CodigoCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeEfetiva
			{
				get
				{
					System.Int32? data = entity.QuantidadeEfetiva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeEfetiva = null;
					else entity.QuantidadeEfetiva = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorEfetivo
			{
				get
				{
					System.Decimal? data = entity.ValorEfetivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorEfetivo = null;
					else entity.ValorEfetivo = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodigoIsin
			{
				get
				{
					System.String data = entity.CodigoIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoIsin = null;
					else entity.CodigoIsin = Convert.ToString(value);
				}
			}
			

			private esTabelaCRCA entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaCRCAQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaCRCA can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaCRCA : esTabelaCRCA
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaCRCAQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCRCAMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTabela
		{
			get
			{
				return new esQueryItem(this, TabelaCRCAMetadata.ColumnNames.IdTabela, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataMovimento
		{
			get
			{
				return new esQueryItem(this, TabelaCRCAMetadata.ColumnNames.DataMovimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Modalidade
		{
			get
			{
				return new esQueryItem(this, TabelaCRCAMetadata.ColumnNames.Modalidade, esSystemType.String);
			}
		} 
		
		public esQueryItem CpfCNPJ
		{
			get
			{
				return new esQueryItem(this, TabelaCRCAMetadata.ColumnNames.CpfCNPJ, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCustodiante
		{
			get
			{
				return new esQueryItem(this, TabelaCRCAMetadata.ColumnNames.CodigoCustodiante, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCliente
		{
			get
			{
				return new esQueryItem(this, TabelaCRCAMetadata.ColumnNames.CodigoCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeEfetiva
		{
			get
			{
				return new esQueryItem(this, TabelaCRCAMetadata.ColumnNames.QuantidadeEfetiva, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorEfetivo
		{
			get
			{
				return new esQueryItem(this, TabelaCRCAMetadata.ColumnNames.ValorEfetivo, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodigoIsin
		{
			get
			{
				return new esQueryItem(this, TabelaCRCAMetadata.ColumnNames.CodigoIsin, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaCRCACollection")]
	public partial class TabelaCRCACollection : esTabelaCRCACollection, IEnumerable<TabelaCRCA>
	{
		public TabelaCRCACollection()
		{

		}
		
		public static implicit operator List<TabelaCRCA>(TabelaCRCACollection coll)
		{
			List<TabelaCRCA> list = new List<TabelaCRCA>();
			
			foreach (TabelaCRCA emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaCRCAMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCRCAQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaCRCA(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaCRCA();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaCRCAQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCRCAQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaCRCAQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaCRCA AddNew()
		{
			TabelaCRCA entity = base.AddNewEntity() as TabelaCRCA;
			
			return entity;
		}

		public TabelaCRCA FindByPrimaryKey(System.Int32 idTabela)
		{
			return base.FindByPrimaryKey(idTabela) as TabelaCRCA;
		}


		#region IEnumerable<TabelaCRCA> Members

		IEnumerator<TabelaCRCA> IEnumerable<TabelaCRCA>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaCRCA;
			}
		}

		#endregion
		
		private TabelaCRCAQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaCRCA' table
	/// </summary>

	[Serializable]
	public partial class TabelaCRCA : esTabelaCRCA
	{
		public TabelaCRCA()
		{

		}
	
		public TabelaCRCA(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaCRCAMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaCRCAQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaCRCAQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaCRCAQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaCRCAQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaCRCAQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaCRCAQuery query;
	}



	[Serializable]
	public partial class TabelaCRCAQuery : esTabelaCRCAQuery
	{
		public TabelaCRCAQuery()
		{

		}		
		
		public TabelaCRCAQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaCRCAMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaCRCAMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaCRCAMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCRCAMetadata.PropertyNames.IdTabela;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCRCAMetadata.ColumnNames.DataMovimento, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TabelaCRCAMetadata.PropertyNames.DataMovimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCRCAMetadata.ColumnNames.Modalidade, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCRCAMetadata.PropertyNames.Modalidade;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCRCAMetadata.ColumnNames.CpfCNPJ, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCRCAMetadata.PropertyNames.CpfCNPJ;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCRCAMetadata.ColumnNames.CodigoCustodiante, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCRCAMetadata.PropertyNames.CodigoCustodiante;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCRCAMetadata.ColumnNames.CodigoCliente, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCRCAMetadata.PropertyNames.CodigoCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCRCAMetadata.ColumnNames.QuantidadeEfetiva, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaCRCAMetadata.PropertyNames.QuantidadeEfetiva;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCRCAMetadata.ColumnNames.ValorEfetivo, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaCRCAMetadata.PropertyNames.ValorEfetivo;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaCRCAMetadata.ColumnNames.CodigoIsin, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = TabelaCRCAMetadata.PropertyNames.CodigoIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaCRCAMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string DataMovimento = "DataMovimento";
			 public const string Modalidade = "Modalidade";
			 public const string CpfCNPJ = "CpfCNPJ";
			 public const string CodigoCustodiante = "CodigoCustodiante";
			 public const string CodigoCliente = "CodigoCliente";
			 public const string QuantidadeEfetiva = "QuantidadeEfetiva";
			 public const string ValorEfetivo = "ValorEfetivo";
			 public const string CodigoIsin = "CodigoIsin";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTabela = "IdTabela";
			 public const string DataMovimento = "DataMovimento";
			 public const string Modalidade = "Modalidade";
			 public const string CpfCNPJ = "CpfCNPJ";
			 public const string CodigoCustodiante = "CodigoCustodiante";
			 public const string CodigoCliente = "CodigoCliente";
			 public const string QuantidadeEfetiva = "QuantidadeEfetiva";
			 public const string ValorEfetivo = "ValorEfetivo";
			 public const string CodigoIsin = "CodigoIsin";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaCRCAMetadata))
			{
				if(TabelaCRCAMetadata.mapDelegates == null)
				{
					TabelaCRCAMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaCRCAMetadata.meta == null)
				{
					TabelaCRCAMetadata.meta = new TabelaCRCAMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataMovimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Modalidade", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CpfCNPJ", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCustodiante", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeEfetiva", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorEfetivo", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CodigoIsin", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TabelaCRCA";
				meta.Destination = "TabelaCRCA";
				
				meta.spInsert = "proc_TabelaCRCAInsert";				
				meta.spUpdate = "proc_TabelaCRCAUpdate";		
				meta.spDelete = "proc_TabelaCRCADelete";
				meta.spLoadAll = "proc_TabelaCRCALoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaCRCALoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaCRCAMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
