/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































using Financial.Common;
using Financial.Investidor;

		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esTransferenciaBolsaCollection : esEntityCollection
	{
		public esTransferenciaBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TransferenciaBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTransferenciaBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTransferenciaBolsaQuery);
		}
		#endregion
		
		virtual public TransferenciaBolsa DetachEntity(TransferenciaBolsa entity)
		{
			return base.DetachEntity(entity) as TransferenciaBolsa;
		}
		
		virtual public TransferenciaBolsa AttachEntity(TransferenciaBolsa entity)
		{
			return base.AttachEntity(entity) as TransferenciaBolsa;
		}
		
		virtual public void Combine(TransferenciaBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TransferenciaBolsa this[int index]
		{
			get
			{
				return base[index] as TransferenciaBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TransferenciaBolsa);
		}
	}



	[Serializable]
	abstract public class esTransferenciaBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTransferenciaBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTransferenciaBolsa()
		{

		}

		public esTransferenciaBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTransferencia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferencia);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTransferencia)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTransferenciaBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTransferencia == idTransferencia);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTransferencia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTransferencia);
			else
				return LoadByPrimaryKeyStoredProcedure(idTransferencia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTransferencia)
		{
			esTransferenciaBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTransferencia == idTransferencia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTransferencia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTransferencia",idTransferencia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTransferencia": this.str.IdTransferencia = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "IdAgenteOrigem": this.str.IdAgenteOrigem = (string)value; break;							
						case "IdAgenteDestino": this.str.IdAgenteDestino = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTransferencia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTransferencia = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAgenteOrigem":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteOrigem = (System.Int32?)value;
							break;
						
						case "IdAgenteDestino":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteDestino = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TransferenciaBolsa.IdTransferencia
		/// </summary>
		virtual public System.Int32? IdTransferencia
		{
			get
			{
				return base.GetSystemInt32(TransferenciaBolsaMetadata.ColumnNames.IdTransferencia);
			}
			
			set
			{
				base.SetSystemInt32(TransferenciaBolsaMetadata.ColumnNames.IdTransferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBolsa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(TransferenciaBolsaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaBolsaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBolsa.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(TransferenciaBolsaMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(TransferenciaBolsaMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBolsa.IdAgenteOrigem
		/// </summary>
		virtual public System.Int32? IdAgenteOrigem
		{
			get
			{
				return base.GetSystemInt32(TransferenciaBolsaMetadata.ColumnNames.IdAgenteOrigem);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaBolsaMetadata.ColumnNames.IdAgenteOrigem, value))
				{
					this._UpToAgenteMercadoByIdAgenteOrigem = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBolsa.IdAgenteDestino
		/// </summary>
		virtual public System.Int32? IdAgenteDestino
		{
			get
			{
				return base.GetSystemInt32(TransferenciaBolsaMetadata.ColumnNames.IdAgenteDestino);
			}
			
			set
			{
				if(base.SetSystemInt32(TransferenciaBolsaMetadata.ColumnNames.IdAgenteDestino, value))
				{
					this._UpToAgenteMercadoByIdAgenteDestino = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBolsa.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TransferenciaBolsaMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TransferenciaBolsaMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBolsa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(TransferenciaBolsaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(TransferenciaBolsaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to TransferenciaBolsa.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(TransferenciaBolsaMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(TransferenciaBolsaMetadata.ColumnNames.Pu, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteOrigem;
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteDestino;
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTransferenciaBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTransferencia
			{
				get
				{
					System.Int32? data = entity.IdTransferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTransferencia = null;
					else entity.IdTransferencia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String IdAgenteOrigem
			{
				get
				{
					System.Int32? data = entity.IdAgenteOrigem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteOrigem = null;
					else entity.IdAgenteOrigem = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteDestino
			{
				get
				{
					System.Int32? data = entity.IdAgenteDestino;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteDestino = null;
					else entity.IdAgenteDestino = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
			

			private esTransferenciaBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTransferenciaBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTransferenciaBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TransferenciaBolsa : esTransferenciaBolsa
	{

				
		#region UpToAgenteMercadoByIdAgenteOrigem - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_TransferenciaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteOrigem
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteOrigem == null
					&& IdAgenteOrigem != null					)
				{
					this._UpToAgenteMercadoByIdAgenteOrigem = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteOrigem.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteOrigem", this._UpToAgenteMercadoByIdAgenteOrigem);
					this._UpToAgenteMercadoByIdAgenteOrigem.Query.Where(this._UpToAgenteMercadoByIdAgenteOrigem.Query.IdAgente == this.IdAgenteOrigem);
					this._UpToAgenteMercadoByIdAgenteOrigem.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteOrigem;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteOrigem");
				

				if(value == null)
				{
					this.IdAgenteOrigem = null;
					this._UpToAgenteMercadoByIdAgenteOrigem = null;
				}
				else
				{
					this.IdAgenteOrigem = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteOrigem = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteOrigem", this._UpToAgenteMercadoByIdAgenteOrigem);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAgenteMercadoByIdAgenteDestino - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AgenteMercado_TransferenciaBolsa_FK2
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteDestino
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteDestino == null
					&& IdAgenteDestino != null					)
				{
					this._UpToAgenteMercadoByIdAgenteDestino = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteDestino.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteDestino", this._UpToAgenteMercadoByIdAgenteDestino);
					this._UpToAgenteMercadoByIdAgenteDestino.Query.Where(this._UpToAgenteMercadoByIdAgenteDestino.Query.IdAgente == this.IdAgenteDestino);
					this._UpToAgenteMercadoByIdAgenteDestino.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteDestino;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteDestino");
				

				if(value == null)
				{
					this.IdAgenteDestino = null;
					this._UpToAgenteMercadoByIdAgenteDestino = null;
				}
				else
				{
					this.IdAgenteDestino = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteDestino = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteDestino", this._UpToAgenteMercadoByIdAgenteDestino);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_TransferenciaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_TransferenciaBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteOrigem != null)
			{
				this.IdAgenteOrigem = this._UpToAgenteMercadoByIdAgenteOrigem.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteDestino != null)
			{
				this.IdAgenteDestino = this._UpToAgenteMercadoByIdAgenteDestino.IdAgente;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTransferenciaBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTransferencia
		{
			get
			{
				return new esQueryItem(this, TransferenciaBolsaMetadata.ColumnNames.IdTransferencia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, TransferenciaBolsaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, TransferenciaBolsaMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgenteOrigem
		{
			get
			{
				return new esQueryItem(this, TransferenciaBolsaMetadata.ColumnNames.IdAgenteOrigem, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteDestino
		{
			get
			{
				return new esQueryItem(this, TransferenciaBolsaMetadata.ColumnNames.IdAgenteDestino, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TransferenciaBolsaMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, TransferenciaBolsaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, TransferenciaBolsaMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TransferenciaBolsaCollection")]
	public partial class TransferenciaBolsaCollection : esTransferenciaBolsaCollection, IEnumerable<TransferenciaBolsa>
	{
		public TransferenciaBolsaCollection()
		{

		}
		
		public static implicit operator List<TransferenciaBolsa>(TransferenciaBolsaCollection coll)
		{
			List<TransferenciaBolsa> list = new List<TransferenciaBolsa>();
			
			foreach (TransferenciaBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TransferenciaBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TransferenciaBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TransferenciaBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TransferenciaBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TransferenciaBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TransferenciaBolsa AddNew()
		{
			TransferenciaBolsa entity = base.AddNewEntity() as TransferenciaBolsa;
			
			return entity;
		}

		public TransferenciaBolsa FindByPrimaryKey(System.Int32 idTransferencia)
		{
			return base.FindByPrimaryKey(idTransferencia) as TransferenciaBolsa;
		}


		#region IEnumerable<TransferenciaBolsa> Members

		IEnumerator<TransferenciaBolsa> IEnumerable<TransferenciaBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TransferenciaBolsa;
			}
		}

		#endregion
		
		private TransferenciaBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TransferenciaBolsa' table
	/// </summary>

	[Serializable]
	public partial class TransferenciaBolsa : esTransferenciaBolsa
	{
		public TransferenciaBolsa()
		{

		}
	
		public TransferenciaBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TransferenciaBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esTransferenciaBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TransferenciaBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TransferenciaBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TransferenciaBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TransferenciaBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TransferenciaBolsaQuery query;
	}



	[Serializable]
	public partial class TransferenciaBolsaQuery : esTransferenciaBolsaQuery
	{
		public TransferenciaBolsaQuery()
		{

		}		
		
		public TransferenciaBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TransferenciaBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TransferenciaBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TransferenciaBolsaMetadata.ColumnNames.IdTransferencia, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaBolsaMetadata.PropertyNames.IdTransferencia;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBolsaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaBolsaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBolsaMetadata.ColumnNames.CdAtivoBolsa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = TransferenciaBolsaMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBolsaMetadata.ColumnNames.IdAgenteOrigem, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaBolsaMetadata.PropertyNames.IdAgenteOrigem;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBolsaMetadata.ColumnNames.IdAgenteDestino, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TransferenciaBolsaMetadata.PropertyNames.IdAgenteDestino;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBolsaMetadata.ColumnNames.Data, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TransferenciaBolsaMetadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBolsaMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TransferenciaBolsaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 20;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TransferenciaBolsaMetadata.ColumnNames.Pu, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TransferenciaBolsaMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 20;
			c.NumericScale = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TransferenciaBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTransferencia = "IdTransferencia";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgenteOrigem = "IdAgenteOrigem";
			 public const string IdAgenteDestino = "IdAgenteDestino";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "PU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTransferencia = "IdTransferencia";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string IdAgenteOrigem = "IdAgenteOrigem";
			 public const string IdAgenteDestino = "IdAgenteDestino";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Pu = "Pu";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TransferenciaBolsaMetadata))
			{
				if(TransferenciaBolsaMetadata.mapDelegates == null)
				{
					TransferenciaBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TransferenciaBolsaMetadata.meta == null)
				{
					TransferenciaBolsaMetadata.meta = new TransferenciaBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTransferencia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgenteOrigem", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteDestino", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TransferenciaBolsa";
				meta.Destination = "TransferenciaBolsa";
				
				meta.spInsert = "proc_TransferenciaBolsaInsert";				
				meta.spUpdate = "proc_TransferenciaBolsaUpdate";		
				meta.spDelete = "proc_TransferenciaBolsaDelete";
				meta.spLoadAll = "proc_TransferenciaBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TransferenciaBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TransferenciaBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
