/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 21/05/2015 16:50:22
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;




































using Financial.Investidor;


		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esProventoBolsaClienteCollection : esEntityCollection
	{
		public esProventoBolsaClienteCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ProventoBolsaClienteCollection";
		}

		#region Query Logic
		protected void InitQuery(esProventoBolsaClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esProventoBolsaClienteQuery);
		}
		#endregion
		
		virtual public ProventoBolsaCliente DetachEntity(ProventoBolsaCliente entity)
		{
			return base.DetachEntity(entity) as ProventoBolsaCliente;
		}
		
		virtual public ProventoBolsaCliente AttachEntity(ProventoBolsaCliente entity)
		{
			return base.AttachEntity(entity) as ProventoBolsaCliente;
		}
		
		virtual public void Combine(ProventoBolsaClienteCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ProventoBolsaCliente this[int index]
		{
			get
			{
				return base[index] as ProventoBolsaCliente;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ProventoBolsaCliente);
		}
	}



	[Serializable]
	abstract public class esProventoBolsaCliente : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esProventoBolsaClienteQuery GetDynamicQuery()
		{
			return null;
		}

		public esProventoBolsaCliente()
		{

		}

		public esProventoBolsaCliente(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idProvento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idProvento);
			else
				return LoadByPrimaryKeyStoredProcedure(idProvento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idProvento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esProventoBolsaClienteQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdProvento == idProvento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idProvento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idProvento);
			else
				return LoadByPrimaryKeyStoredProcedure(idProvento);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idProvento)
		{
			esProventoBolsaClienteQuery query = this.GetDynamicQuery();
			query.Where(query.IdProvento == idProvento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idProvento)
		{
			esParameters parms = new esParameters();
			parms.Add("IdProvento",idProvento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdProvento": this.str.IdProvento = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "DataLancamento": this.str.DataLancamento = (string)value; break;							
						case "DataEx": this.str.DataEx = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "TipoProvento": this.str.TipoProvento = (string)value; break;							
						case "Distribuicao": this.str.Distribuicao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "PercentualIR": this.str.PercentualIR = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "IdAgenteMercado": this.str.IdAgenteMercado = (string)value; break;							
						case "TipoPosicaoBolsa": this.str.TipoPosicaoBolsa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdProvento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdProvento = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataLancamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLancamento = (System.DateTime?)value;
							break;
						
						case "DataEx":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEx = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "TipoProvento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoProvento = (System.Byte?)value;
							break;
						
						case "Distribuicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Distribuicao = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "PercentualIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualIR = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "IdAgenteMercado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteMercado = (System.Int32?)value;
							break;
						
						case "TipoPosicaoBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.TipoPosicaoBolsa = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.IdProvento
		/// </summary>
		virtual public System.Int32? IdProvento
		{
			get
			{
				return base.GetSystemInt32(ProventoBolsaClienteMetadata.ColumnNames.IdProvento);
			}
			
			set
			{
				base.SetSystemInt32(ProventoBolsaClienteMetadata.ColumnNames.IdProvento, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ProventoBolsaClienteMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(ProventoBolsaClienteMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(ProventoBolsaClienteMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(ProventoBolsaClienteMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.DataLancamento
		/// </summary>
		virtual public System.DateTime? DataLancamento
		{
			get
			{
				return base.GetSystemDateTime(ProventoBolsaClienteMetadata.ColumnNames.DataLancamento);
			}
			
			set
			{
				base.SetSystemDateTime(ProventoBolsaClienteMetadata.ColumnNames.DataLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.DataEx
		/// </summary>
		virtual public System.DateTime? DataEx
		{
			get
			{
				return base.GetSystemDateTime(ProventoBolsaClienteMetadata.ColumnNames.DataEx);
			}
			
			set
			{
				base.SetSystemDateTime(ProventoBolsaClienteMetadata.ColumnNames.DataEx, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(ProventoBolsaClienteMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(ProventoBolsaClienteMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.TipoProvento
		/// </summary>
		virtual public System.Byte? TipoProvento
		{
			get
			{
				return base.GetSystemByte(ProventoBolsaClienteMetadata.ColumnNames.TipoProvento);
			}
			
			set
			{
				base.SetSystemByte(ProventoBolsaClienteMetadata.ColumnNames.TipoProvento, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.Distribuicao
		/// </summary>
		virtual public System.Int32? Distribuicao
		{
			get
			{
				return base.GetSystemInt32(ProventoBolsaClienteMetadata.ColumnNames.Distribuicao);
			}
			
			set
			{
				base.SetSystemInt32(ProventoBolsaClienteMetadata.ColumnNames.Distribuicao, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(ProventoBolsaClienteMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(ProventoBolsaClienteMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(ProventoBolsaClienteMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(ProventoBolsaClienteMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.PercentualIR
		/// </summary>
		virtual public System.Decimal? PercentualIR
		{
			get
			{
				return base.GetSystemDecimal(ProventoBolsaClienteMetadata.ColumnNames.PercentualIR);
			}
			
			set
			{
				base.SetSystemDecimal(ProventoBolsaClienteMetadata.ColumnNames.PercentualIR, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(ProventoBolsaClienteMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(ProventoBolsaClienteMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.IdAgenteMercado
		/// </summary>
		virtual public System.Int32? IdAgenteMercado
		{
			get
			{
				return base.GetSystemInt32(ProventoBolsaClienteMetadata.ColumnNames.IdAgenteMercado);
			}
			
			set
			{
				base.SetSystemInt32(ProventoBolsaClienteMetadata.ColumnNames.IdAgenteMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to ProventoBolsaCliente.TipoPosicaoBolsa
		/// </summary>
		virtual public System.Int16? TipoPosicaoBolsa
		{
			get
			{
				return base.GetSystemInt16(ProventoBolsaClienteMetadata.ColumnNames.TipoPosicaoBolsa);
			}
			
			set
			{
				base.SetSystemInt16(ProventoBolsaClienteMetadata.ColumnNames.TipoPosicaoBolsa, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esProventoBolsaCliente entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdProvento
			{
				get
				{
					System.Int32? data = entity.IdProvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdProvento = null;
					else entity.IdProvento = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String DataLancamento
			{
				get
				{
					System.DateTime? data = entity.DataLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLancamento = null;
					else entity.DataLancamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEx
			{
				get
				{
					System.DateTime? data = entity.DataEx;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEx = null;
					else entity.DataEx = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoProvento
			{
				get
				{
					System.Byte? data = entity.TipoProvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoProvento = null;
					else entity.TipoProvento = Convert.ToByte(value);
				}
			}
				
			public System.String Distribuicao
			{
				get
				{
					System.Int32? data = entity.Distribuicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Distribuicao = null;
					else entity.Distribuicao = Convert.ToInt32(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String PercentualIR
			{
				get
				{
					System.Decimal? data = entity.PercentualIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualIR = null;
					else entity.PercentualIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String IdAgenteMercado
			{
				get
				{
					System.Int32? data = entity.IdAgenteMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteMercado = null;
					else entity.IdAgenteMercado = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoPosicaoBolsa
			{
				get
				{
					System.Int16? data = entity.TipoPosicaoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPosicaoBolsa = null;
					else entity.TipoPosicaoBolsa = Convert.ToInt16(value);
				}
			}
			

			private esProventoBolsaCliente entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esProventoBolsaClienteQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esProventoBolsaCliente can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ProventoBolsaCliente : esProventoBolsaCliente
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_ProventoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_ProventoBolsa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esProventoBolsaClienteQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ProventoBolsaClienteMetadata.Meta();
			}
		}	
		

		public esQueryItem IdProvento
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.IdProvento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem DataLancamento
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.DataLancamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataEx
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.DataEx, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoProvento
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.TipoProvento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Distribuicao
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.Distribuicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PercentualIR
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.PercentualIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdAgenteMercado
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.IdAgenteMercado, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoPosicaoBolsa
		{
			get
			{
				return new esQueryItem(this, ProventoBolsaClienteMetadata.ColumnNames.TipoPosicaoBolsa, esSystemType.Int16);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ProventoBolsaClienteCollection")]
	public partial class ProventoBolsaClienteCollection : esProventoBolsaClienteCollection, IEnumerable<ProventoBolsaCliente>
	{
		public ProventoBolsaClienteCollection()
		{

		}
		
		public static implicit operator List<ProventoBolsaCliente>(ProventoBolsaClienteCollection coll)
		{
			List<ProventoBolsaCliente> list = new List<ProventoBolsaCliente>();
			
			foreach (ProventoBolsaCliente emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ProventoBolsaClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ProventoBolsaClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ProventoBolsaCliente(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ProventoBolsaCliente();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ProventoBolsaClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ProventoBolsaClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ProventoBolsaClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ProventoBolsaCliente AddNew()
		{
			ProventoBolsaCliente entity = base.AddNewEntity() as ProventoBolsaCliente;
			
			return entity;
		}

		public ProventoBolsaCliente FindByPrimaryKey(System.Int32 idProvento)
		{
			return base.FindByPrimaryKey(idProvento) as ProventoBolsaCliente;
		}


		#region IEnumerable<ProventoBolsaCliente> Members

		IEnumerator<ProventoBolsaCliente> IEnumerable<ProventoBolsaCliente>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ProventoBolsaCliente;
			}
		}

		#endregion
		
		private ProventoBolsaClienteQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ProventoBolsaCliente' table
	/// </summary>

	[Serializable]
	public partial class ProventoBolsaCliente : esProventoBolsaCliente
	{
		public ProventoBolsaCliente()
		{

		}
	
		public ProventoBolsaCliente(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ProventoBolsaClienteMetadata.Meta();
			}
		}
		
		
		
		override protected esProventoBolsaClienteQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ProventoBolsaClienteQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ProventoBolsaClienteQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ProventoBolsaClienteQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ProventoBolsaClienteQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ProventoBolsaClienteQuery query;
	}



	[Serializable]
	public partial class ProventoBolsaClienteQuery : esProventoBolsaClienteQuery
	{
		public ProventoBolsaClienteQuery()
		{

		}		
		
		public ProventoBolsaClienteQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ProventoBolsaClienteMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ProventoBolsaClienteMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.IdProvento, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.IdProvento;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.CdAtivoBolsa, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.CdAtivoBolsa;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.DataLancamento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.DataLancamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.DataEx, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.DataEx;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.DataPagamento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.TipoProvento, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.TipoProvento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.Distribuicao, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.Distribuicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.Quantidade, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.Valor, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.PercentualIR, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.PercentualIR;	
			c.NumericPrecision = 8;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.Fonte, 11, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.IdAgenteMercado, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.IdAgenteMercado;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ProventoBolsaClienteMetadata.ColumnNames.TipoPosicaoBolsa, 13, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = ProventoBolsaClienteMetadata.PropertyNames.TipoPosicaoBolsa;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ProventoBolsaClienteMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdProvento = "IdProvento";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataPagamento = "DataPagamento";
			 public const string TipoProvento = "TipoProvento";
			 public const string Distribuicao = "Distribuicao";
			 public const string Quantidade = "Quantidade";
			 public const string Valor = "Valor";
			 public const string PercentualIR = "PercentualIR";
			 public const string Fonte = "Fonte";
			 public const string IdAgenteMercado = "IdAgenteMercado";
			 public const string TipoPosicaoBolsa = "TipoPosicaoBolsa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdProvento = "IdProvento";
			 public const string IdCliente = "IdCliente";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataLancamento = "DataLancamento";
			 public const string DataEx = "DataEx";
			 public const string DataPagamento = "DataPagamento";
			 public const string TipoProvento = "TipoProvento";
			 public const string Distribuicao = "Distribuicao";
			 public const string Quantidade = "Quantidade";
			 public const string Valor = "Valor";
			 public const string PercentualIR = "PercentualIR";
			 public const string Fonte = "Fonte";
			 public const string IdAgenteMercado = "IdAgenteMercado";
			 public const string TipoPosicaoBolsa = "TipoPosicaoBolsa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ProventoBolsaClienteMetadata))
			{
				if(ProventoBolsaClienteMetadata.mapDelegates == null)
				{
					ProventoBolsaClienteMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ProventoBolsaClienteMetadata.meta == null)
				{
					ProventoBolsaClienteMetadata.meta = new ProventoBolsaClienteMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdProvento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataLancamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEx", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoProvento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Distribuicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PercentualIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdAgenteMercado", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoPosicaoBolsa", new esTypeMap("smallint", "System.Int16"));			
				
				
				
				meta.Source = "ProventoBolsaCliente";
				meta.Destination = "ProventoBolsaCliente";
				
				meta.spInsert = "proc_ProventoBolsaClienteInsert";				
				meta.spUpdate = "proc_ProventoBolsaClienteUpdate";		
				meta.spDelete = "proc_ProventoBolsaClienteDelete";
				meta.spLoadAll = "proc_ProventoBolsaClienteLoadAll";
				meta.spLoadByPrimaryKey = "proc_ProventoBolsaClienteLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ProventoBolsaClienteMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
