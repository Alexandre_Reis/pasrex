/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:14
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esAtivoBolsaHistoricoCollection : esEntityCollection
	{
		public esAtivoBolsaHistoricoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AtivoBolsaHistoricoCollection";
		}

		#region Query Logic
		protected void InitQuery(esAtivoBolsaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAtivoBolsaHistoricoQuery);
		}
		#endregion
		
		virtual public AtivoBolsaHistorico DetachEntity(AtivoBolsaHistorico entity)
		{
			return base.DetachEntity(entity) as AtivoBolsaHistorico;
		}
		
		virtual public AtivoBolsaHistorico AttachEntity(AtivoBolsaHistorico entity)
		{
			return base.AttachEntity(entity) as AtivoBolsaHistorico;
		}
		
		virtual public void Combine(AtivoBolsaHistoricoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AtivoBolsaHistorico this[int index]
		{
			get
			{
				return base[index] as AtivoBolsaHistorico;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AtivoBolsaHistorico);
		}
	}



	[Serializable]
	abstract public class esAtivoBolsaHistorico : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAtivoBolsaHistoricoQuery GetDynamicQuery()
		{
			return null;
		}

		public esAtivoBolsaHistorico()
		{

		}

		public esAtivoBolsaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBolsa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esAtivoBolsaHistoricoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CdAtivoBolsa == cdAtivoBolsa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, cdAtivoBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, cdAtivoBolsa);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			esAtivoBolsaHistoricoQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CdAtivoBolsa == cdAtivoBolsa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CdAtivoBolsa",cdAtivoBolsa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AtivoBolsaHistorico.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(AtivoBolsaHistoricoMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(AtivoBolsaHistoricoMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsaHistorico.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(AtivoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(AtivoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AtivoBolsaHistorico.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(AtivoBolsaHistoricoMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(AtivoBolsaHistoricoMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAtivoBolsaHistorico entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
			

			private esAtivoBolsaHistorico entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAtivoBolsaHistoricoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAtivoBolsaHistorico can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AtivoBolsaHistorico : esAtivoBolsaHistorico
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - AtivoBolsa_AtivoBolsaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAtivoBolsaHistoricoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AtivoBolsaHistoricoMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaHistoricoMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, AtivoBolsaHistoricoMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AtivoBolsaHistoricoCollection")]
	public partial class AtivoBolsaHistoricoCollection : esAtivoBolsaHistoricoCollection, IEnumerable<AtivoBolsaHistorico>
	{
		public AtivoBolsaHistoricoCollection()
		{

		}
		
		public static implicit operator List<AtivoBolsaHistorico>(AtivoBolsaHistoricoCollection coll)
		{
			List<AtivoBolsaHistorico> list = new List<AtivoBolsaHistorico>();
			
			foreach (AtivoBolsaHistorico emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AtivoBolsaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AtivoBolsaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AtivoBolsaHistorico(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AtivoBolsaHistorico();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AtivoBolsaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AtivoBolsaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AtivoBolsaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AtivoBolsaHistorico AddNew()
		{
			AtivoBolsaHistorico entity = base.AddNewEntity() as AtivoBolsaHistorico;
			
			return entity;
		}

		public AtivoBolsaHistorico FindByPrimaryKey(System.DateTime dataReferencia, System.String cdAtivoBolsa)
		{
			return base.FindByPrimaryKey(dataReferencia, cdAtivoBolsa) as AtivoBolsaHistorico;
		}


		#region IEnumerable<AtivoBolsaHistorico> Members

		IEnumerator<AtivoBolsaHistorico> IEnumerable<AtivoBolsaHistorico>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AtivoBolsaHistorico;
			}
		}

		#endregion
		
		private AtivoBolsaHistoricoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AtivoBolsaHistorico' table
	/// </summary>

	[Serializable]
	public partial class AtivoBolsaHistorico : esAtivoBolsaHistorico
	{
		public AtivoBolsaHistorico()
		{

		}
	
		public AtivoBolsaHistorico(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AtivoBolsaHistoricoMetadata.Meta();
			}
		}
		
		
		
		override protected esAtivoBolsaHistoricoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AtivoBolsaHistoricoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AtivoBolsaHistoricoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AtivoBolsaHistoricoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AtivoBolsaHistoricoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AtivoBolsaHistoricoQuery query;
	}



	[Serializable]
	public partial class AtivoBolsaHistoricoQuery : esAtivoBolsaHistoricoQuery
	{
		public AtivoBolsaHistoricoQuery()
		{

		}		
		
		public AtivoBolsaHistoricoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AtivoBolsaHistoricoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AtivoBolsaHistoricoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AtivoBolsaHistoricoMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AtivoBolsaHistoricoMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaHistoricoMetadata.ColumnNames.CdAtivoBolsa, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = AtivoBolsaHistoricoMetadata.PropertyNames.CdAtivoBolsa;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AtivoBolsaHistoricoMetadata.ColumnNames.DataVencimento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AtivoBolsaHistoricoMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AtivoBolsaHistoricoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataVencimento = "DataVencimento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string DataVencimento = "DataVencimento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AtivoBolsaHistoricoMetadata))
			{
				if(AtivoBolsaHistoricoMetadata.mapDelegates == null)
				{
					AtivoBolsaHistoricoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AtivoBolsaHistoricoMetadata.meta == null)
				{
					AtivoBolsaHistoricoMetadata.meta = new AtivoBolsaHistoricoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "AtivoBolsaHistorico";
				meta.Destination = "AtivoBolsaHistorico";
				
				meta.spInsert = "proc_AtivoBolsaHistoricoInsert";				
				meta.spUpdate = "proc_AtivoBolsaHistoricoUpdate";		
				meta.spDelete = "proc_AtivoBolsaHistoricoDelete";
				meta.spLoadAll = "proc_AtivoBolsaHistoricoLoadAll";
				meta.spLoadByPrimaryKey = "proc_AtivoBolsaHistoricoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AtivoBolsaHistoricoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
