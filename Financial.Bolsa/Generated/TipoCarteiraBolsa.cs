/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/04/2014 11:38:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		


























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esTipoCarteiraBolsaCollection : esEntityCollection
	{
		public esTipoCarteiraBolsaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TipoCarteiraBolsaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTipoCarteiraBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTipoCarteiraBolsaQuery);
		}
		#endregion
		
		virtual public TipoCarteiraBolsa DetachEntity(TipoCarteiraBolsa entity)
		{
			return base.DetachEntity(entity) as TipoCarteiraBolsa;
		}
		
		virtual public TipoCarteiraBolsa AttachEntity(TipoCarteiraBolsa entity)
		{
			return base.AttachEntity(entity) as TipoCarteiraBolsa;
		}
		
		virtual public void Combine(TipoCarteiraBolsaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TipoCarteiraBolsa this[int index]
		{
			get
			{
				return base[index] as TipoCarteiraBolsa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TipoCarteiraBolsa);
		}
	}



	[Serializable]
	abstract public class esTipoCarteiraBolsa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTipoCarteiraBolsaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTipoCarteiraBolsa()
		{

		}

		public esTipoCarteiraBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCarteiraBolsa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteiraBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteiraBolsa);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCarteiraBolsa)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTipoCarteiraBolsaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCarteiraBolsa == idCarteiraBolsa);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCarteiraBolsa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCarteiraBolsa);
			else
				return LoadByPrimaryKeyStoredProcedure(idCarteiraBolsa);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCarteiraBolsa)
		{
			esTipoCarteiraBolsaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCarteiraBolsa == idCarteiraBolsa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCarteiraBolsa)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCarteiraBolsa",idCarteiraBolsa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCarteiraBolsa": this.str.IdCarteiraBolsa = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCarteiraBolsa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraBolsa = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TipoCarteiraBolsa.IdCarteiraBolsa
		/// </summary>
		virtual public System.Int32? IdCarteiraBolsa
		{
			get
			{
				return base.GetSystemInt32(TipoCarteiraBolsaMetadata.ColumnNames.IdCarteiraBolsa);
			}
			
			set
			{
				base.SetSystemInt32(TipoCarteiraBolsaMetadata.ColumnNames.IdCarteiraBolsa, value);
			}
		}
		
		/// <summary>
		/// Maps to TipoCarteiraBolsa.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TipoCarteiraBolsaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TipoCarteiraBolsaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTipoCarteiraBolsa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCarteiraBolsa
			{
				get
				{
					System.Int32? data = entity.IdCarteiraBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraBolsa = null;
					else entity.IdCarteiraBolsa = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esTipoCarteiraBolsa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTipoCarteiraBolsaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTipoCarteiraBolsa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TipoCarteiraBolsa : esTipoCarteiraBolsa
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTipoCarteiraBolsaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TipoCarteiraBolsaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCarteiraBolsa
		{
			get
			{
				return new esQueryItem(this, TipoCarteiraBolsaMetadata.ColumnNames.IdCarteiraBolsa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TipoCarteiraBolsaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TipoCarteiraBolsaCollection")]
	public partial class TipoCarteiraBolsaCollection : esTipoCarteiraBolsaCollection, IEnumerable<TipoCarteiraBolsa>
	{
		public TipoCarteiraBolsaCollection()
		{

		}
		
		public static implicit operator List<TipoCarteiraBolsa>(TipoCarteiraBolsaCollection coll)
		{
			List<TipoCarteiraBolsa> list = new List<TipoCarteiraBolsa>();
			
			foreach (TipoCarteiraBolsa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TipoCarteiraBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoCarteiraBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TipoCarteiraBolsa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TipoCarteiraBolsa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TipoCarteiraBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoCarteiraBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TipoCarteiraBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TipoCarteiraBolsa AddNew()
		{
			TipoCarteiraBolsa entity = base.AddNewEntity() as TipoCarteiraBolsa;
			
			return entity;
		}

		public TipoCarteiraBolsa FindByPrimaryKey(System.Int32 idCarteiraBolsa)
		{
			return base.FindByPrimaryKey(idCarteiraBolsa) as TipoCarteiraBolsa;
		}


		#region IEnumerable<TipoCarteiraBolsa> Members

		IEnumerator<TipoCarteiraBolsa> IEnumerable<TipoCarteiraBolsa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TipoCarteiraBolsa;
			}
		}

		#endregion
		
		private TipoCarteiraBolsaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TipoCarteiraBolsa' table
	/// </summary>

	[Serializable]
	public partial class TipoCarteiraBolsa : esTipoCarteiraBolsa
	{
		public TipoCarteiraBolsa()
		{

		}
	
		public TipoCarteiraBolsa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TipoCarteiraBolsaMetadata.Meta();
			}
		}
		
		
		
		override protected esTipoCarteiraBolsaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TipoCarteiraBolsaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TipoCarteiraBolsaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TipoCarteiraBolsaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TipoCarteiraBolsaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TipoCarteiraBolsaQuery query;
	}



	[Serializable]
	public partial class TipoCarteiraBolsaQuery : esTipoCarteiraBolsaQuery
	{
		public TipoCarteiraBolsaQuery()
		{

		}		
		
		public TipoCarteiraBolsaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TipoCarteiraBolsaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TipoCarteiraBolsaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TipoCarteiraBolsaMetadata.ColumnNames.IdCarteiraBolsa, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TipoCarteiraBolsaMetadata.PropertyNames.IdCarteiraBolsa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TipoCarteiraBolsaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = TipoCarteiraBolsaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TipoCarteiraBolsaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCarteiraBolsa = "IdCarteiraBolsa";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCarteiraBolsa = "IdCarteiraBolsa";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TipoCarteiraBolsaMetadata))
			{
				if(TipoCarteiraBolsaMetadata.mapDelegates == null)
				{
					TipoCarteiraBolsaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TipoCarteiraBolsaMetadata.meta == null)
				{
					TipoCarteiraBolsaMetadata.meta = new TipoCarteiraBolsaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCarteiraBolsa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "TipoCarteiraBolsa";
				meta.Destination = "TipoCarteiraBolsa";
				
				meta.spInsert = "proc_TipoCarteiraBolsaInsert";				
				meta.spUpdate = "proc_TipoCarteiraBolsaUpdate";		
				meta.spDelete = "proc_TipoCarteiraBolsaDelete";
				meta.spLoadAll = "proc_TipoCarteiraBolsaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TipoCarteiraBolsaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TipoCarteiraBolsaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
