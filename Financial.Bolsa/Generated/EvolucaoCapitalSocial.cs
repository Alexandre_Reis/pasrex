/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 03/06/2014 17:59:09
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Bolsa
{

	[Serializable]
	abstract public class esEvolucaoCapitalSocialCollection : esEntityCollection
	{
		public esEvolucaoCapitalSocialCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "EvolucaoCapitalSocialCollection";
		}

		#region Query Logic
		protected void InitQuery(esEvolucaoCapitalSocialQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esEvolucaoCapitalSocialQuery);
		}
		#endregion
		
		virtual public EvolucaoCapitalSocial DetachEntity(EvolucaoCapitalSocial entity)
		{
			return base.DetachEntity(entity) as EvolucaoCapitalSocial;
		}
		
		virtual public EvolucaoCapitalSocial AttachEntity(EvolucaoCapitalSocial entity)
		{
			return base.AttachEntity(entity) as EvolucaoCapitalSocial;
		}
		
		virtual public void Combine(EvolucaoCapitalSocialCollection collection)
		{
			base.Combine(collection);
		}
		
		new public EvolucaoCapitalSocial this[int index]
		{
			get
			{
				return base[index] as EvolucaoCapitalSocial;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(EvolucaoCapitalSocial);
		}
	}



	[Serializable]
	abstract public class esEvolucaoCapitalSocial : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esEvolucaoCapitalSocialQuery GetDynamicQuery()
		{
			return null;
		}

		public esEvolucaoCapitalSocial()
		{

		}

		public esEvolucaoCapitalSocial(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.String cdAtivoBolsa, System.DateTime data)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBolsa, data);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBolsa, data);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.String cdAtivoBolsa, System.DateTime data)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esEvolucaoCapitalSocialQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.CdAtivoBolsa == cdAtivoBolsa, query.Data == data);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.String cdAtivoBolsa, System.DateTime data)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(cdAtivoBolsa, data);
			else
				return LoadByPrimaryKeyStoredProcedure(cdAtivoBolsa, data);
		}

		private bool LoadByPrimaryKeyDynamic(System.String cdAtivoBolsa, System.DateTime data)
		{
			esEvolucaoCapitalSocialQuery query = this.GetDynamicQuery();
			query.Where(query.CdAtivoBolsa == cdAtivoBolsa, query.Data == data);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.String cdAtivoBolsa, System.DateTime data)
		{
			esParameters parms = new esParameters();
			parms.Add("CdAtivoBolsa",cdAtivoBolsa);			parms.Add("Data",data);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "CdAtivoBolsa": this.str.CdAtivoBolsa = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "QuantidadeFinal": this.str.QuantidadeFinal = (string)value; break;							
						case "ValorFinal": this.str.ValorFinal = (string)value; break;							
						case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "QuantidadeFinal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeFinal = (System.Decimal?)value;
							break;
						
						case "ValorFinal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorFinal = (System.Decimal?)value;
							break;
						
						case "Tipo":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Tipo = (System.Byte?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to EvolucaoCapitalSocial.CdAtivoBolsa
		/// </summary>
		virtual public System.String CdAtivoBolsa
		{
			get
			{
				return base.GetSystemString(EvolucaoCapitalSocialMetadata.ColumnNames.CdAtivoBolsa);
			}
			
			set
			{
				if(base.SetSystemString(EvolucaoCapitalSocialMetadata.ColumnNames.CdAtivoBolsa, value))
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to EvolucaoCapitalSocial.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(EvolucaoCapitalSocialMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(EvolucaoCapitalSocialMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to EvolucaoCapitalSocial.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(EvolucaoCapitalSocialMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(EvolucaoCapitalSocialMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to EvolucaoCapitalSocial.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(EvolucaoCapitalSocialMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(EvolucaoCapitalSocialMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to EvolucaoCapitalSocial.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(EvolucaoCapitalSocialMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(EvolucaoCapitalSocialMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to EvolucaoCapitalSocial.QuantidadeFinal
		/// </summary>
		virtual public System.Decimal? QuantidadeFinal
		{
			get
			{
				return base.GetSystemDecimal(EvolucaoCapitalSocialMetadata.ColumnNames.QuantidadeFinal);
			}
			
			set
			{
				base.SetSystemDecimal(EvolucaoCapitalSocialMetadata.ColumnNames.QuantidadeFinal, value);
			}
		}
		
		/// <summary>
		/// Maps to EvolucaoCapitalSocial.ValorFinal
		/// </summary>
		virtual public System.Decimal? ValorFinal
		{
			get
			{
				return base.GetSystemDecimal(EvolucaoCapitalSocialMetadata.ColumnNames.ValorFinal);
			}
			
			set
			{
				base.SetSystemDecimal(EvolucaoCapitalSocialMetadata.ColumnNames.ValorFinal, value);
			}
		}
		
		/// <summary>
		/// Maps to EvolucaoCapitalSocial.Tipo
		/// </summary>
		virtual public System.Byte? Tipo
		{
			get
			{
				return base.GetSystemByte(EvolucaoCapitalSocialMetadata.ColumnNames.Tipo);
			}
			
			set
			{
				base.SetSystemByte(EvolucaoCapitalSocialMetadata.ColumnNames.Tipo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected AtivoBolsa _UpToAtivoBolsaByCdAtivoBolsa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esEvolucaoCapitalSocial entity)
			{
				this.entity = entity;
			}
			
	
			public System.String CdAtivoBolsa
			{
				get
				{
					System.String data = entity.CdAtivoBolsa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CdAtivoBolsa = null;
					else entity.CdAtivoBolsa = Convert.ToString(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String QuantidadeFinal
			{
				get
				{
					System.Decimal? data = entity.QuantidadeFinal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeFinal = null;
					else entity.QuantidadeFinal = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorFinal
			{
				get
				{
					System.Decimal? data = entity.ValorFinal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorFinal = null;
					else entity.ValorFinal = Convert.ToDecimal(value);
				}
			}
				
			public System.String Tipo
			{
				get
				{
					System.Byte? data = entity.Tipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Tipo = null;
					else entity.Tipo = Convert.ToByte(value);
				}
			}
			

			private esEvolucaoCapitalSocial entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esEvolucaoCapitalSocialQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esEvolucaoCapitalSocial can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class EvolucaoCapitalSocial : esEvolucaoCapitalSocial
	{

				
		#region UpToAtivoBolsaByCdAtivoBolsa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_EvolucaoCapitalSocial_AtivoBolsa
		/// </summary>

		[XmlIgnore]
		public AtivoBolsa UpToAtivoBolsaByCdAtivoBolsa
		{
			get
			{
				if(this._UpToAtivoBolsaByCdAtivoBolsa == null
					&& CdAtivoBolsa != null					)
				{
					this._UpToAtivoBolsaByCdAtivoBolsa = new AtivoBolsa();
					this._UpToAtivoBolsaByCdAtivoBolsa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Where(this._UpToAtivoBolsaByCdAtivoBolsa.Query.CdAtivoBolsa == this.CdAtivoBolsa);
					this._UpToAtivoBolsaByCdAtivoBolsa.Query.Load();
				}

				return this._UpToAtivoBolsaByCdAtivoBolsa;
			}
			
			set
			{
				this.RemovePreSave("UpToAtivoBolsaByCdAtivoBolsa");
				

				if(value == null)
				{
					this.CdAtivoBolsa = null;
					this._UpToAtivoBolsaByCdAtivoBolsa = null;
				}
				else
				{
					this.CdAtivoBolsa = value.CdAtivoBolsa;
					this._UpToAtivoBolsaByCdAtivoBolsa = value;
					this.SetPreSave("UpToAtivoBolsaByCdAtivoBolsa", this._UpToAtivoBolsaByCdAtivoBolsa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esEvolucaoCapitalSocialQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return EvolucaoCapitalSocialMetadata.Meta();
			}
		}	
		

		public esQueryItem CdAtivoBolsa
		{
			get
			{
				return new esQueryItem(this, EvolucaoCapitalSocialMetadata.ColumnNames.CdAtivoBolsa, esSystemType.String);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, EvolucaoCapitalSocialMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, EvolucaoCapitalSocialMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, EvolucaoCapitalSocialMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, EvolucaoCapitalSocialMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem QuantidadeFinal
		{
			get
			{
				return new esQueryItem(this, EvolucaoCapitalSocialMetadata.ColumnNames.QuantidadeFinal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorFinal
		{
			get
			{
				return new esQueryItem(this, EvolucaoCapitalSocialMetadata.ColumnNames.ValorFinal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Tipo
		{
			get
			{
				return new esQueryItem(this, EvolucaoCapitalSocialMetadata.ColumnNames.Tipo, esSystemType.Byte);
			}
		} 
		
	}



	[Serializable]
	[XmlType("EvolucaoCapitalSocialCollection")]
	public partial class EvolucaoCapitalSocialCollection : esEvolucaoCapitalSocialCollection, IEnumerable<EvolucaoCapitalSocial>
	{
		public EvolucaoCapitalSocialCollection()
		{

		}
		
		public static implicit operator List<EvolucaoCapitalSocial>(EvolucaoCapitalSocialCollection coll)
		{
			List<EvolucaoCapitalSocial> list = new List<EvolucaoCapitalSocial>();
			
			foreach (EvolucaoCapitalSocial emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  EvolucaoCapitalSocialMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EvolucaoCapitalSocialQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new EvolucaoCapitalSocial(row);
		}

		override protected esEntity CreateEntity()
		{
			return new EvolucaoCapitalSocial();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public EvolucaoCapitalSocialQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EvolucaoCapitalSocialQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(EvolucaoCapitalSocialQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public EvolucaoCapitalSocial AddNew()
		{
			EvolucaoCapitalSocial entity = base.AddNewEntity() as EvolucaoCapitalSocial;
			
			return entity;
		}

		public EvolucaoCapitalSocial FindByPrimaryKey(System.String cdAtivoBolsa, System.DateTime data)
		{
			return base.FindByPrimaryKey(cdAtivoBolsa, data) as EvolucaoCapitalSocial;
		}


		#region IEnumerable<EvolucaoCapitalSocial> Members

		IEnumerator<EvolucaoCapitalSocial> IEnumerable<EvolucaoCapitalSocial>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as EvolucaoCapitalSocial;
			}
		}

		#endregion
		
		private EvolucaoCapitalSocialQuery query;
	}


	/// <summary>
	/// Encapsulates the 'EvolucaoCapitalSocial' table
	/// </summary>

	[Serializable]
	public partial class EvolucaoCapitalSocial : esEvolucaoCapitalSocial
	{
		public EvolucaoCapitalSocial()
		{

		}
	
		public EvolucaoCapitalSocial(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return EvolucaoCapitalSocialMetadata.Meta();
			}
		}
		
		
		
		override protected esEvolucaoCapitalSocialQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new EvolucaoCapitalSocialQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public EvolucaoCapitalSocialQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new EvolucaoCapitalSocialQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(EvolucaoCapitalSocialQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private EvolucaoCapitalSocialQuery query;
	}



	[Serializable]
	public partial class EvolucaoCapitalSocialQuery : esEvolucaoCapitalSocialQuery
	{
		public EvolucaoCapitalSocialQuery()
		{

		}		
		
		public EvolucaoCapitalSocialQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class EvolucaoCapitalSocialMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected EvolucaoCapitalSocialMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(EvolucaoCapitalSocialMetadata.ColumnNames.CdAtivoBolsa, 0, typeof(System.String), esSystemType.String);
			c.PropertyName = EvolucaoCapitalSocialMetadata.PropertyNames.CdAtivoBolsa;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EvolucaoCapitalSocialMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = EvolucaoCapitalSocialMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EvolucaoCapitalSocialMetadata.ColumnNames.Quantidade, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EvolucaoCapitalSocialMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EvolucaoCapitalSocialMetadata.ColumnNames.Valor, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EvolucaoCapitalSocialMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EvolucaoCapitalSocialMetadata.ColumnNames.Descricao, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = EvolucaoCapitalSocialMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EvolucaoCapitalSocialMetadata.ColumnNames.QuantidadeFinal, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EvolucaoCapitalSocialMetadata.PropertyNames.QuantidadeFinal;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EvolucaoCapitalSocialMetadata.ColumnNames.ValorFinal, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = EvolucaoCapitalSocialMetadata.PropertyNames.ValorFinal;	
			c.NumericPrecision = 18;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(EvolucaoCapitalSocialMetadata.ColumnNames.Tipo, 7, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = EvolucaoCapitalSocialMetadata.PropertyNames.Tipo;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public EvolucaoCapitalSocialMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Valor = "Valor";
			 public const string Descricao = "Descricao";
			 public const string QuantidadeFinal = "QuantidadeFinal";
			 public const string ValorFinal = "ValorFinal";
			 public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string CdAtivoBolsa = "CdAtivoBolsa";
			 public const string Data = "Data";
			 public const string Quantidade = "Quantidade";
			 public const string Valor = "Valor";
			 public const string Descricao = "Descricao";
			 public const string QuantidadeFinal = "QuantidadeFinal";
			 public const string ValorFinal = "ValorFinal";
			 public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(EvolucaoCapitalSocialMetadata))
			{
				if(EvolucaoCapitalSocialMetadata.mapDelegates == null)
				{
					EvolucaoCapitalSocialMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (EvolucaoCapitalSocialMetadata.meta == null)
				{
					EvolucaoCapitalSocialMetadata.meta = new EvolucaoCapitalSocialMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("CdAtivoBolsa", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("QuantidadeFinal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorFinal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Tipo", new esTypeMap("tinyint", "System.Byte"));			
				
				
				
				meta.Source = "EvolucaoCapitalSocial";
				meta.Destination = "EvolucaoCapitalSocial";
				
				meta.spInsert = "proc_EvolucaoCapitalSocialInsert";				
				meta.spUpdate = "proc_EvolucaoCapitalSocialUpdate";		
				meta.spDelete = "proc_EvolucaoCapitalSocialDelete";
				meta.spLoadAll = "proc_EvolucaoCapitalSocialLoadAll";
				meta.spLoadByPrimaryKey = "proc_EvolucaoCapitalSocialLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private EvolucaoCapitalSocialMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
