﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using Financial.Bolsa.Enums;
using Financial.Util;
using Financial.Common.Enums;
using System.Data.SqlClient;
using Financial.ContaCorrente.Enums;
using Financial.Bolsa.Properties;
using Financial.ContaCorrente;
using System.Threading;
using Financial.Bolsa.ThreadControl;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor.Enums;
using Financial.Investidor;
using Financial.Investidor.Controller;
using Financial.Fundo.Enums;
using Financial.Fundo;
using Financial.Common;

namespace Financial.Bolsa.Controller {

    public class ControllerBolsa {
        

        /// <summary>
        /// Reprocessa as posições, por tipo = abertura ou tipo = fechamento.
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoBolsa
        public void ReprocessaPosicao(int idCliente, DateTime data, TipoReprocessamento tipoReprocessamento)
        {
            //TODO FAZER TODO O TRATAMENTO PARA POSICAOBOLSADETALHE

            #region Deletes Posicao
            #region Delete PosicaoBolsa
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            try
            {
                posicaoBolsaCollection.DeletaPosicaoBolsa(idCliente);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoBolsa");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Delete PosicaoEmprestimoBolsa
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            try
            {
                posicaoEmprestimoBolsaCollection.DeletaPosicaoEmprestimoBolsa(idCliente);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoEmprestimoBolsa");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion
            
            #region Delete PosicaoTermoBolsa
            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            try
            {
                posicaoTermoBolsaCollection.DeletaPosicaoTermoBolsa(idCliente);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoTermoBolsa");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion            
            #endregion

            #region Inserts das Posições baseado nas posições de fechamento, ou de abertura
            //Para SqlServer precisa dar um tratamento especial para a PK Identity
            // Nome da Configuração Default SQL
            string sql = "";
            esUtility u = new esUtility();
            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    u = new esUtility();
                    sql = " set identity_insert PosicaoBolsa on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert PosicaoBolsa a partir de PosicaoBolsaHistorico
                        posicaoBolsaCollection = new PosicaoBolsaCollection();
                        try
                        {
                            posicaoBolsaCollection.InserePosicaoBolsaHistorico(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoBolsa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert PosicaoBolsa a partir de PosicaoBolsaAbertura
                        posicaoBolsaCollection = new PosicaoBolsaCollection();
                        try
                        {
                            posicaoBolsaCollection.InserePosicaoBolsaAbertura(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoBolsa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    scope.Complete();
                }

                u = new esUtility();
                sql = " set identity_insert PosicaoBolsa off ";
                u.ExecuteNonQuery(esQueryType.Text, sql, "");

                using (esTransactionScope scope = new esTransactionScope())
                {
                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert PosicaoEmprestimoBolsa a partir de PosicaoEmprestimoBolsaHistorico
                        posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                        try
                        {
                            posicaoEmprestimoBolsaCollection.InserePosicaoEmprestimoBolsaHistorico(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoEmprestimoBolsa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert PosicaoEmprestimoBolsa a partir de PosicaoEmprestimoBolsaAbertura
                        posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                        try
                        {
                            posicaoEmprestimoBolsaCollection.InserePosicaoEmprestimoBolsaAbertura(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoEmprestimoBolsa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    scope.Complete();
                }

                using (esTransactionScope scope = new esTransactionScope())
                {
                    u = new esUtility();
                    sql = " set identity_insert PosicaoTermoBolsa on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert PosicaoTermoBolsa a partir de PosicaoTermoBolsaHistorico
                        posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                        try
                        {
                            posicaoTermoBolsaCollection.InserePosicaoTermoBolsaHistorico(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoTermoBolsa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert PosicaoTermoBolsa a partir de PosicaoTermoBolsaAbertura
                        posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                        try
                        {
                            posicaoTermoBolsaCollection.InserePosicaoTermoBolsaAbertura(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoTermoBolsa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    scope.Complete();
                }

                u = new esUtility();
                sql = " set identity_insert PosicaoTermoBolsa off ";
                u.ExecuteNonQuery(esQueryType.Text, sql, "");
            }
            else
            {
                //TODO ESCREVER NA UNHA O SQL, POIS O ORACLE NÃO PERMITE DESLIGAR A SEQUENCE
                if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                {
                    #region Insert PosicaoBolsa a partir de PosicaoBolsaHistorico
                    posicaoBolsaCollection = new PosicaoBolsaCollection();
                    try
                    {
                        posicaoBolsaCollection.InserePosicaoBolsaHistorico(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoBolsa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion

                    #region Insert PosicaoEmprestimoBolsa a partir de PosicaoEmprestimoBolsaHistorico
                    posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                    try
                    {
                        posicaoEmprestimoBolsaCollection.InserePosicaoEmprestimoBolsaHistorico(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoEmprestimoBolsa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion

                    #region Insert PosicaoTermoBolsa a partir de PosicaoTermoBolsaHistorico
                    posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                    try
                    {
                        posicaoTermoBolsaCollection.InserePosicaoTermoBolsaHistorico(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoTermoBolsa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
                else
                {
                    #region Insert PosicaoBolsa a partir de PosicaoBolsaAbertura
                    posicaoBolsaCollection = new PosicaoBolsaCollection();
                    try
                    {
                        posicaoBolsaCollection.InserePosicaoBolsaAbertura(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoBolsa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion

                    #region Insert PosicaoEmprestimoBolsa a partir de PosicaoEmprestimoBolsaAbertura
                    posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
                    try
                    {
                        posicaoEmprestimoBolsaCollection.InserePosicaoEmprestimoBolsaAbertura(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoEmprestimoBolsa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion

                    #region Insert PosicaoTermoBolsa a partir de PosicaoTermoBolsaAbertura
                    posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
                    try
                    {
                        posicaoTermoBolsaCollection.InserePosicaoTermoBolsaAbertura(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoTermoBolsa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
            }
            #endregion            
            
            this.DeletaPosicoesAbertura(idCliente, data);

            this.DeletaPosicoesHistorico(idCliente, data);
            

            Cliente c = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>(
                                    new esQueryItem[] { c.Query.DataDia, c.Query.DataImplantacao }
                              );
            c.LoadByPrimaryKey(campos, idCliente);

            if (c.DataDia.Value == c.DataImplantacao.Value)
            {
                PosicaoEmprestimoBolsa posicaoEmprestimoBolsaAux = new PosicaoEmprestimoBolsa();

                //Cálculo dos juros e custos do BTC
                posicaoEmprestimoBolsaAux.AtualizaValorEmprestimo(idCliente, data, false);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se TipoReprocessamento é Fechamento</returns>
        /*
        private bool IsTipoReprocessamentoFechamento() {
            return this.tipoReprocessamento == (int) TipoReprocessamentoBolsa.Fechamento;
        } */

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se TipoReprocessamento é Abertura</returns>
        /*
        private bool IsTipoReprocessamentoAbertura() {
            return this.tipoReprocessamento == (int) TipoReprocessamentoBolsa.Abertura;
        } */

        /// <summary>
        /// Atualiza as posições de abertura de termo e empréstimo.
        /// Joga de Posicao para PosicaoAbertura (PosicaoBolsa, PosicaoEmprestimoBolsa, PosicaoTermoBolsa).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaAbertura(int idCliente, DateTime data)
        {
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
            PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
            ProventoBolsaCliente proventoBolsaCliente = new ProventoBolsaCliente();

            //Atualiza as posições (remuneração dia)
            posicaoEmprestimoBolsa.AtualizaValorEmprestimo(idCliente, data, true);
            posicaoTermoBolsa.AtualizaPosicaoTermoAbertura(idCliente, data);
            //

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            posicaoBolsa.ResetaCusto(idCliente, data);

            #region TrataProcessamentoDiario
            (new PosicaoBolsaDetalhe()).TrataProcessamentoDiario(idCliente, data);
            #endregion

            #region CarregaOperacoesFilhas
            (new OperacaoBolsa()).CarregaOperacoesFilhas(idCliente, data);
            #endregion

            #region Tratamento de PosicaoBolsa (eventos de bolsa - boni, conversao, grupamento, subscrição)
            SubscricaoBolsa subscricaoBolsa = new SubscricaoBolsa();
            subscricaoBolsa.ProcessaDireitoSubscricao(idCliente, data);

            BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();
            bonificacaoBolsa.ProcessaBonificacao(idCliente, data);

            ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
            conversaoBolsa.ProcessaConversao(idCliente, data);

            GrupamentoBolsa grupamentoBolsa = new GrupamentoBolsa();
            grupamentoBolsa.ProcessaGrupamento(idCliente, data);
            #endregion

            #region Tratamento de Termo (eventos de bolsa - boni, conversao, grupamento)
            bonificacaoBolsa = new BonificacaoBolsa();
            bonificacaoBolsa.ProcessaBonificacaoTermo(idCliente, data);

            conversaoBolsa = new ConversaoBolsa();
            conversaoBolsa.ProcessaConversaoTermo(idCliente, data);

            grupamentoBolsa = new GrupamentoBolsa();
            grupamentoBolsa.ProcessaGrupamentoTermo(idCliente, data);
            #endregion

            if (cliente.TipoControle.Value != (byte)TipoControleCliente.IRRendaVariavel)
            {
                #region CarregaProventosBolsa
                proventoBolsaCliente.CarregaProventosBolsa(idCliente, data);
                #endregion
            }

            #region ProcessaReducaoCustoFII
            proventoBolsaCliente.ProcessaReducaoCustoFII(idCliente, data);
            #endregion

            #region ZeraValoresCustos
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.ZeraValoresCustos(idCliente, data);
            #endregion

            #region ProcessaVencimentoEmprestimo
            posicaoEmprestimoBolsa.ProcessaVencimentoEmprestimo(idCliente, data);
            #endregion

            #region ProcessaAntecipacaoEmprestimo
            (new LiquidacaoEmprestimoBolsa()).ProcessaLiquidacaoEmprestimo(idCliente, data);
            #endregion

            #region AtualizaValorEmprestimo
            posicaoEmprestimoBolsa.AtualizaValorEmprestimo(idCliente, data, false);
            #endregion
            //

            if (cliente.TipoControle.Value != (byte)TipoControleCliente.IRRendaVariavel) //Tem que vir após o CarregaProventosBolsa e CarregaProventosBolsaBTC
            {
                #region LancaCCProvento
                proventoBolsaCliente.LancaCCEventosDinheiro(idCliente, data);
                #endregion
            }


            #region CalculaValoresLiquidos
            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.CalculaValoresLiquidos(idCliente, data);
            #endregion



            #region ProcessaVencimentoTermo
            posicaoTermoBolsa.ProcessaVencimentoTermo(idCliente, data);
            #endregion

            #region ProcessaLiquidacaoTermo
            (new LiquidacaoTermoBolsa()).ProcessaLiquidacaoTermo(idCliente, data);
            #endregion
            //

            #region ZeraResultados
            operacaoBolsa.ZeraResultados(idCliente, data);
            #endregion

            #region TrataExercicioOpcao
            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.TrataExercicioOpcao(idCliente, data);
            #endregion

            #region CasaVencimento
            operacaoBolsa = new OperacaoBolsa();

            if (ParametrosConfiguracaoSistema.Bolsa.PrioridadeCasamentoVencimento == (int)PrioridadeCasamentoVencimento.ExercicioPrimeiro)
            {
                operacaoBolsa.CasaVencimento(idCliente, data, PrioridadeCasamentoVencimento.ExercicioPrimeiro);
            }
            else if (ParametrosConfiguracaoSistema.Bolsa.PrioridadeCasamentoVencimento == (int)PrioridadeCasamentoVencimento.TermoPrimeiro)
            {
                operacaoBolsa.CasaVencimento(idCliente, data, PrioridadeCasamentoVencimento.TermoPrimeiro);
            }
            #endregion


            #region ProcessaVencimentoOpcao
            posicaoBolsa = new PosicaoBolsa();
            posicaoBolsa.ProcessaVencimentoOpcao(idCliente, data);
            #endregion


            #region AtualizaValores (PosicaoBolsa e PosicaoTermoBolsa)
            posicaoBolsa.AtualizaValores(idCliente, data);

            posicaoTermoBolsa.AtualizaValores(idCliente, data);
            #endregion

            #region Calcula Prazo Médio
            this.CalculaPrazoMedioBolsa(data, idCliente);
            #endregion

            #region Calcula Rentabildade
            this.CalculaRentabilidadeBolsa(data, idCliente);
            #endregion

            //Carrega as posições atuais para Posicao de Abertura (Bolsa, Emprestimo, Termo)
            posicaoBolsa.GeraPosicaoAbertura(idCliente, data);
            posicaoEmprestimoBolsa.GeraPosicaoAbertura(idCliente, data);
            posicaoTermoBolsa.GeraPosicaoAbertura(idCliente, data);
            //
        }

        /// <summary>
        /// Backup de PosicaoAtual para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacao(int idCliente, DateTime data) 
        {
            this.DeletaPosicoesHistorico(idCliente, data);

            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
            PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();

            posicaoBolsa.GeraBackup(idCliente, data);
            posicaoEmprestimoBolsa.GeraBackup(idCliente, data);
            posicaoTermoBolsa.GeraBackup(idCliente, data);            
        }

        /// <summary>
        /// Realiza todos os cálculos financeiros e despesas, atualiza a custódia, gera fluxos de pagamento futuros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="integraBolsa">Indica se integra ordens do Sinacor e carrega para operações</param>
        public void ExecutaFechamento(int idCliente, DateTime data, ParametrosProcessamento parametrosProcessamento) 
        {
            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.TipoControle);
            campos.Add(cliente.Query.IdMoeda);
            campos.Add(cliente.Query.DataImplantacao);
            cliente.LoadByPrimaryKey(campos, idCliente);
            
            this.ReprocessaPosicao(idCliente, data, TipoReprocessamento.CalculoDiario);
                        
            // Inicialização objetos
            BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();
            ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
            GrupamentoBolsa grupamentoBolsa = new GrupamentoBolsa();
            SubscricaoBolsa subscricaoBolsa = new SubscricaoBolsa();
            OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = new OperacaoEmprestimoBolsa();
            LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
            OrdemBolsa ordemBolsa = new OrdemBolsa();
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            EventoFisicoBolsa eventoFisicoBolsa = new EventoFisicoBolsa();
            TransferenciaBolsa transferenciaBolsa = new TransferenciaBolsa();
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            OperacaoTermoBolsa operacaoTermoBolsa = new OperacaoTermoBolsa();            
            PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
            LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
            ProventoBolsaCliente proventoBolsaCliente = new ProventoBolsaCliente();
            ProventoBolsa proventoBolsa = new ProventoBolsa();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            BloqueioBolsa bloqueioBolsa = new BloqueioBolsa();
            PosicaoBolsaDetalhe posicaoBolsaDetalhe = new PosicaoBolsaDetalhe();

            if ((parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.Bolsa.SinacorPosicaoInicialBolsa) ||
                parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.Bolsa.SinacorPosicaoInicialBTC))
                && data == cliente.DataImplantacao.Value)
            {
                if (parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.Bolsa.SinacorPosicaoInicialBolsa))
                {
                    posicaoBolsa.ImportaPosicoesSinacor(idCliente, data);                 
                }

                if (parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.Bolsa.SinacorPosicaoInicialBTC))
                {                    
                    posicaoEmprestimoBolsa.ImportaPosicoesSinacor(idCliente, data);
                }

                return;
            }

            if (parametrosProcessamento.ResetCusto)
            {
                posicaoBolsa.ResetaCusto(idCliente, data);
            }

            if (!parametrosProcessamento.IgnoraOperacoes)
            {
                #region TrataProcessamentoDiario
                posicaoBolsaDetalhe.TrataProcessamentoDiario(idCliente, data);
                #endregion

                #region CarregaOperacoesFilhas
                operacaoBolsa.CarregaOperacoesFilhas(idCliente, data);
                #endregion

                #region Tratamento de PosicaoBolsa (eventos de bolsa - boni, conversao, grupamento, subscrição)
                subscricaoBolsa = new SubscricaoBolsa();
                subscricaoBolsa.ProcessaDireitoSubscricao(idCliente, data);

                bonificacaoBolsa = new BonificacaoBolsa();
                bonificacaoBolsa.ProcessaBonificacao(idCliente, data);

                conversaoBolsa = new ConversaoBolsa();
                conversaoBolsa.ProcessaConversao(idCliente, data);

                grupamentoBolsa = new GrupamentoBolsa();
                grupamentoBolsa.ProcessaGrupamento(idCliente, data);
                #endregion

                #region Tratamento de Termo (eventos de bolsa - boni, conversao, grupamento)
                bonificacaoBolsa = new BonificacaoBolsa();
                bonificacaoBolsa.ProcessaBonificacaoTermo(idCliente, data);

                conversaoBolsa = new ConversaoBolsa();
                conversaoBolsa.ProcessaConversaoTermo(idCliente, data);

                grupamentoBolsa = new GrupamentoBolsa();
                grupamentoBolsa.ProcessaGrupamentoTermo(idCliente, data);
                #endregion

                if (cliente.TipoControle.Value != (byte)TipoControleCliente.IRRendaVariavel)
                {
                    #region CarregaProventosBolsa
                    proventoBolsaCliente.CarregaProventosBolsa(idCliente, data);
                    #endregion
                }

                #region ProcessaReducaoCustoFII
                proventoBolsaCliente.ProcessaReducaoCustoFII(idCliente, data);
                #endregion

                #region ZeraValoresCustos
                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.ZeraValoresCustos(idCliente, data);
                #endregion

                #region Integração Sinacor
                if (parametrosProcessamento.IntegraBolsa)
                {
                    string tipoMercado = "";
                    if (parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.Bolsa.SinacorOperacaoAcoes))
                    {
                        tipoMercado = TipoMercadoBolsa.MercadoVista;
                    }
                    else if (parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.Bolsa.SinacorOperacaoFII))
                    {
                        tipoMercado = TipoMercadoBolsa.Imobiliario;
                    }

                    ordemBolsa.IntegraOrdensSinacor(idCliente, data, (byte)FonteOrdemBolsa.Sinacor, tipoMercado);
                    liquidacaoTermoBolsa.IntegraLiquidacaoTermoSinacor(idCliente, data);
                    posicaoTermoBolsa.AtualizaContratoTermoSinacor(idCliente);
                }

                if (parametrosProcessamento.IntegraBTC)
                {
                    operacaoEmprestimoBolsa.IntegraOperacaoEmprestimoSinacor(idCliente, data);
                    liquidacaoEmprestimoBolsa.IntegraLiquidacaoEmprestimoSinacor(idCliente, data);
                }
                #endregion

                #region CasaDayTrade
                ordemBolsa.CasaDayTrade(idCliente, data);
                #endregion

                // Tratamento de Emprestimos
                #region Emprestimos
                #region ProcessaOperacaoEmprestimo
                operacaoEmprestimoBolsa.ProcessaOperacaoEmprestimo(idCliente, data);
                #endregion

                if (cliente.TipoControle.Value != (byte)TipoControleCliente.IRRendaVariavel)
                {
                    #region Tratamento de Empréstimo (eventos de bolsa - boni, conversao, grupamento)
                    bonificacaoBolsa = new BonificacaoBolsa();
                    bonificacaoBolsa.ProcessaBonificacaoEmprestimo(idCliente, data);

                    conversaoBolsa = new ConversaoBolsa();
                    conversaoBolsa.ProcessaConversaoEmprestimo(idCliente, data);

                    grupamentoBolsa = new GrupamentoBolsa();
                    grupamentoBolsa.ProcessaGrupamentoEmprestimo(idCliente, data);
                    #endregion

                    #region CarregaProventosBolsaBTC
                    proventoBolsaCliente.CarregaProventosBolsaBTC(idCliente, data);
                    #endregion
                }

                #region Método central para deleção de Operações de bolsa originadas de Vcto e/ou Liquidação de Empréstimo
                operacaoBolsa.DeletaOperacoesLiquidacaoEmprestimo(idCliente, data);
                #endregion

                #region ProcessaVencimentoEmprestimo
                posicaoEmprestimoBolsa.ProcessaVencimentoEmprestimo(idCliente, data);
                #endregion

                #region ProcessaAntecipacaoEmprestimo
                liquidacaoEmprestimoBolsa.ProcessaLiquidacaoEmprestimo(idCliente, data);
                #endregion

                #region AtualizaValorEmprestimo
                posicaoEmprestimoBolsa.AtualizaValorEmprestimo(idCliente, data, false);
                #endregion
                #endregion
                //

                if (cliente.TipoControle.Value != (byte)TipoControleCliente.IRRendaVariavel) //Tem que vir após o CarregaProventosBolsa e CarregaProventosBolsaBTC
                {
                    #region LancaCCProvento
                    proventoBolsaCliente.LancaCCEventosDinheiro(idCliente, data);
                    #endregion
                }

                //Atentar para o lugar do método (após o casamento DT), pq IPOs e ofertas secundárias não casam daytrade
                #region CarregaOrdemCRCA
                ordemBolsa.CarregaOrdemCRCA(idCliente, data);
                #endregion

                #region CarregaOrdemCasada
                if (parametrosProcessamento.IntegraBolsa)
                {
                    operacaoBolsa.CarregaOrdemCasada(idCliente, data, true);
                }
                else
                {
                    operacaoBolsa.CarregaOrdemCasada(idCliente, data, false);
                }
                #endregion

                #region CalculaTaxas
                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.CalculaTaxas(idCliente, data);
                #endregion

                #region CalculaCorretagem
                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.CalculaCorretagem(idCliente, data);
                #endregion

                #region CalculaValoresLiquidos
                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.CalculaValoresLiquidos(idCliente, data);
                #endregion

                //Tratamento de Termo (operação, liquidação e vencimento)
                #region Termos (Liquidação)
                #region ProcessaOperacaoTermo
                operacaoTermoBolsa.ProcessaOperacaoTermo(idCliente, data);
                #endregion

                #region Método central para deleção de Operações de bolsa originadas de Vcto e/ou Liquidação de Termo
                operacaoBolsa.DeletaOperacoesLiquidacaoTermo(idCliente, data);
                #endregion

                #region ProcessaVencimentoTermo
                posicaoTermoBolsa.ProcessaVencimentoTermo(idCliente, data);
                #endregion

                #region ProcessaLiquidacaoTermo
                liquidacaoTermoBolsa.ProcessaLiquidacaoTermo(idCliente, data);
                #endregion
                #endregion
                //

                #region ZeraResultados
                operacaoBolsa.ZeraResultados(idCliente, data);
                #endregion

                #region TrataExercicioOpcao
                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.TrataExercicioOpcao(idCliente, data);
                #endregion

                #region CasaVencimento
                operacaoBolsa = new OperacaoBolsa();

                if (ParametrosConfiguracaoSistema.Bolsa.PrioridadeCasamentoVencimento == (int)PrioridadeCasamentoVencimento.ExercicioPrimeiro)
                {
                    operacaoBolsa.CasaVencimento(idCliente, data, PrioridadeCasamentoVencimento.ExercicioPrimeiro);
                }
                else if (ParametrosConfiguracaoSistema.Bolsa.PrioridadeCasamentoVencimento == (int)PrioridadeCasamentoVencimento.TermoPrimeiro)
                {
                    operacaoBolsa.CasaVencimento(idCliente, data, PrioridadeCasamentoVencimento.TermoPrimeiro);
                }
                #endregion

                #region Processa Compra/Venda
                int entradaOperacao = ParametrosConfiguracaoSistema.Bolsa.EntradaOperacoes;
                switch (entradaOperacao)
                {
                    // Processa Primeiro as Operações de Compras
                    case (int)EntradaOperacao.Compras:

                        #region ProcessaCompra
                        operacaoBolsa = new OperacaoBolsa();
                        operacaoBolsa.ProcessaCompra(idCliente, data, false);
                        #endregion

                        #region ProcessaVenda
                        operacaoBolsa = new OperacaoBolsa();
                        operacaoBolsa.ProcessaVenda(idCliente, data, false);
                        #endregion

                        break;

                    // Processa Primeiro as Operações de Vendas
                    case (int)EntradaOperacao.Vendas:

                        #region ProcessaVenda
                        operacaoBolsa = new OperacaoBolsa();
                        operacaoBolsa.ProcessaVenda(idCliente, data, false);
                        #endregion

                        #region ProcessaCompra
                        operacaoBolsa = new OperacaoBolsa();
                        operacaoBolsa.ProcessaCompra(idCliente, data, false);
                        #endregion

                        break;
                }
                #endregion

                #region GeraTransferenciaAutomatica
                posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.GeraTransferenciaAutomatica(idCliente, data);
                #endregion

                #region ProcessaTransferenciaAgenteCustodia
                transferenciaBolsa.ProcessaTransferenciaAgenteCustodia(idCliente, data);

                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.ProcessaCompra(idCliente, data, true);

                operacaoBolsa = new OperacaoBolsa();
                operacaoBolsa.ProcessaVenda(idCliente, data, true);
                #endregion

                #region ProcessaVencimentoOpcao
                posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.ProcessaVencimentoOpcao(idCliente, data);
                #endregion
            }

            #region AtualizaValores (PosicaoBolsa e PosicaoTermoBolsa)
            posicaoBolsa.AtualizaValores(idCliente, data);

            posicaoTermoBolsa.AtualizaValores(idCliente, data);
            #endregion
            
            if (!parametrosProcessamento.IgnoraOperacoes)
            {
                #region LancaCCOperacao
                operacaoBolsa = new OperacaoBolsa();

                if (ParametrosConfiguracaoSistema.Bolsa.TipoVisualizacao == (int)VizualizacaoOperacaoBolsa.Analitico)
                {
                    operacaoBolsa.LancaCCOperacao(idCliente, data, VizualizacaoOperacaoBolsa.Analitico);
                }
                else if (ParametrosConfiguracaoSistema.Bolsa.TipoVisualizacao == (int)VizualizacaoOperacaoBolsa.Consolidado)
                {
                    operacaoBolsa.LancaCCOperacao(idCliente, data, VizualizacaoOperacaoBolsa.Consolidado);
                }
                #endregion
            }
            
            #region ConsolidaCustoMedio
            if (ParametrosConfiguracaoSistema.Bolsa.ConsolidacaoCustoMedio)
            {
                posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.ConsolidaCustoMedio(idCliente, data);
            }
            #endregion
        
            if (cliente.TipoControle.Value != (byte)TipoControleCliente.IRRendaVariavel)
            {
               #region ProcessaBloqueioBolsa
                bloqueioBolsa.ProcessaBloqueioBolsa(idCliente, data);
                #endregion
            }

            #region Calcula Prazo Médio
            this.CalculaPrazoMedioBolsa(data, idCliente);
            #endregion

            #region Calcula Rentabildade
            this.CalculaRentabilidadeBolsa(data, idCliente);
            #endregion

        }

        /// <summary>
        /// Deleta as posicoes de abertura em PosicaoBolsa, PosicaoEmprestimoBolsa e PosicaoTermoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaPosicoesAbertura(int idCliente, DateTime data)
        {
            PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
            posicaoBolsaAberturaCollection.DeletaPosicaoBolsaAberturaDataHistoricoMaior(idCliente, data);
            
            PosicaoEmprestimoBolsaAberturaCollection posicaoEmprestimoBolsaAberturaCollection = new PosicaoEmprestimoBolsaAberturaCollection();
            posicaoEmprestimoBolsaAberturaCollection.DeletaPosicaoEmprestimoBolsaAberturaDataHistoricoMaior(idCliente, data);

            PosicaoTermoBolsaAberturaCollection posicaoTermoBolsaAberturaCollection = new PosicaoTermoBolsaAberturaCollection();
            posicaoTermoBolsaAberturaCollection.DeletaPosicaoTermoBolsaAberturaDataHistoricoMaior(idCliente, data);            
        }

        /// <summary>
        /// Deleta as posicoes de historico em PosicaoBolsa, PosicaoEmprestimoBolsa e PosicaoTermoBolsa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaPosicoesHistorico(int idCliente, DateTime data)
        {
            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoCollection.DeletaPosicaoBolsaHistoricoDataHistoricoMaiorIgual(idCliente, data);
            
            PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
            posicaoEmprestimoBolsaHistoricoCollection.DeletaPosicaoEmprestimoBolsaHistoricoDataHistoricoMaiorIgual(idCliente, data);
            
            PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
            posicaoTermoBolsaHistoricoCollection.DeletaPosicaoTermoBolsaHistoricoDataHistoricoMaiorIgual(idCliente, data);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        public void CalculaPrazoMedioBolsa(DateTime data, int idCliente)
        {
            PrazoMedioCollection prazoMedioColl = new PrazoMedioCollection();
            prazoMedioColl.DeletaPrazoMedioMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoBolsa);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);
            string calculaPrazoMedio = string.IsNullOrEmpty(carteira.CalculaPrazoMedio) ? string.Empty : carteira.CalculaPrazoMedio;

            if (!calculaPrazoMedio.Equals("S"))
                return;

            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery("posicaoBolsa");
            PosicaoBolsaCollection posicaoBolsaColl = new PosicaoBolsaCollection();
            posicaoBolsaQuery.Select(posicaoBolsaQuery.ValorMercado.Sum(),
                                   posicaoBolsaQuery.IdCliente);
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente));
            posicaoBolsaQuery.GroupBy(posicaoBolsaQuery.IdCliente);

            if (posicaoBolsaColl.Load(posicaoBolsaQuery))
            {
                PrazoMedio prazoMedio = new PrazoMedio();
                prazoMedio.InserePrazoMedio(data, idCliente, "Posição Bolsa", (int)TipoAtivoAuxiliar.OperacaoBolsa, posicaoBolsaColl[0].ValorMercado.Value, 1, null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        public void CalculaRentabilidadeBolsa(DateTime data, int idCliente)
        {
            #region Se carteira mãe não calcula rentabilidade ( precisa desenvolver )
            CarteiraMae carteiraMae = new CarteiraMae();
            if (carteiraMae.IsCarteiraMae(idCliente))
                return;
            #endregion

            #region EntitySpaces
            string tableAtivoBolsa = "AtivoBolsa";
            string tablePosicaoFechamento = "PosicaoFechamento";
            string tablePosicaoAbertura = "PosicaoAbertura";
            string tablePosicaoHistorico = "PosicaoHistorico";
            string tableMoedaAtivo = "MoedaAtivo";
            string fieldQtdeAbertura = "QtdeAbertura";
            string fieldQtdeFechamento = "QtdeFechamento";
            string fieldValorFechamentoLiquido = "ValorFechamentoLiquido";
            string fieldValorAberturaLiquido = "ValorAberturaLiquido";
            string fieldValorFechamentoBruto = "ValorFechamentoBruto";
            string fieldValorAberturaBruto = "ValorAberturaBruto";
            string fieldDescricaoMoedaAtivo = "DescricaoMoedaAtivo";
            string fieldIdMoedaAtivo = "IdMoedaAtivo";
            #endregion

            #region Objetos
            Dictionary<int, string> dicMoedasAtivo = new Dictionary<int, string>();
            Dictionary<string, decimal> dicCotacao = new Dictionary<string, decimal>();
            Dictionary<string, Rentabilidade.RentabilidadeConversao> dicRentConversao = new Dictionary<string, Rentabilidade.RentabilidadeConversao>();
            List<OperacaoBolsa> lstOperacoesEntrada = new List<OperacaoBolsa>();
            List<OperacaoBolsa> lstOperacoesSaida = new List<OperacaoBolsa>();
            List<ProventoBolsa> lstProventoBolsa = new List<ProventoBolsa>();
            List<OperacaoBolsa> lstBonificacao = new List<OperacaoBolsa>();
            List<ProventoBolsa> lstProventoBolsaAtivo = new List<ProventoBolsa>();
            List<SubscricaoBolsa> lstSubscricaoBolsa = new List<SubscricaoBolsa>();
            List<Rentabilidade> lstRentabilidadeAnterior = new List<Rentabilidade>();
            List<string> lstTipoOperacaoEntrada = new List<string>();
            List<string> lstTipoOperacaoSaida = new List<string>();


            //Querys
            PosicaoBolsaQuery posicaoBolsaQuery = new PosicaoBolsaQuery(tablePosicaoFechamento);
            AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery(tableAtivoBolsa);
            PosicaoBolsaAberturaQuery posicaoBolsaAberturaQuery = new PosicaoBolsaAberturaQuery(tablePosicaoAbertura);
            PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery(tablePosicaoHistorico);
            OperacaoBolsaQuery operacaoBolsaSaidaQuery = new OperacaoBolsaQuery();
            OperacaoBolsaQuery operacaoBolsaEntradaQuery = new OperacaoBolsaQuery();
            OperacaoBolsaQuery operacaoBolsaBonificacaoQuery = new OperacaoBolsaQuery();
            SubscricaoBolsaQuery subscricaoBolsaQuery = new SubscricaoBolsaQuery();
            MoedaQuery moedaQuery = new MoedaQuery(tableMoedaAtivo);

            //Collection
            PosicaoBolsaCollection posicaoBolsaColl = new PosicaoBolsaCollection();
            RentabilidadeCollection rentabilidadeColl = new RentabilidadeCollection();
            OperacaoBolsaCollection operacaoBolsaSaidaColl = new OperacaoBolsaCollection();
            OperacaoBolsaCollection operacaoBolsaEntradaColl = new OperacaoBolsaCollection();
            ProventoBolsaCollection proventoBolsaColl = new ProventoBolsaCollection();
            SubscricaoBolsaCollection subscricaoBolsaColl = new SubscricaoBolsaCollection();
            OperacaoBolsaCollection operacaoBolsaBonificacaoColl = new OperacaoBolsaCollection();
            PosicaoBolsaAberturaCollection posicaoBolsaAberturaColl = new PosicaoBolsaAberturaCollection();

            //Comuns        
            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            Cliente cliente = new Cliente();
            Moeda moeda = new Moeda();
            OperacaoBolsa operacaoAux;
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            #endregion

            #region Variaveis
            decimal valorFechamentoLiquido = 0;
            decimal valorAberturaLiquido = 0;
            decimal valorEntrada = 0;
            decimal valorSaidaLiquido = 0;
            decimal valorRendaLiquido = 0;
            decimal valorRendaBruto = 0;
            decimal valorRentabilidadeLiquido = 0;
            decimal valorRentabilidadeBrutaAcumulada = 0;
            decimal valorRentabilidadeBrutaDiaria = 0;
            decimal valorFechamentoBruto = 0;
            decimal valorAberturaBruto = 0;
            decimal valorSaidaBruto = 0;
            decimal qtdeEntrada = 0;
            decimal qtdeSaida = 0;
            decimal qtdeAbertura = 0;
            decimal qtdeFechamento = 0;
            decimal cambio = 0;
            string descricaoCompleta = string.Empty;
            string descricaoMoedaCarteira = string.Empty;
            string descricaoMoedaAtivo = string.Empty;
            string keyConversao = string.Empty;
            short idMoedaAtivo = 0;
            short idMoedaCarteira = 0;
            byte tipoConversao;
            string cdAtivoBolsa;
            #endregion

            #region Popula Listas
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.Compra);
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.CompraDaytrade);
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.Deposito);
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.IngressoAtivoImpactoCota);
            lstTipoOperacaoEntrada.Add(TipoOperacaoBolsa.IngressoAtivoImpactoQtde);


            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.Venda);
            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.VendaDaytrade);
            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.Retirada);
            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.RetiradaAtivoImpactoCota);
            lstTipoOperacaoSaida.Add(TipoOperacaoBolsa.RetiradaAtivoImpactoQtde);
            #endregion

            #region Deleta Rentabilidade
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoBolsa);
            #endregion
            
            
            #region Carrega as Posições



            posicaoBolsaAberturaQuery.Select(posicaoBolsaAberturaQuery.CdAtivoBolsa,
                                    posicaoBolsaAberturaQuery.DataVencimento,
                                    posicaoBolsaAberturaQuery.ValorMercado.Sum().As(fieldValorFechamentoBruto),
                                    posicaoBolsaAberturaQuery.ValorMercado.Sum().As(fieldValorFechamentoLiquido),
                                    posicaoBolsaAberturaQuery.Quantidade.Sum().As(fieldQtdeFechamento),
                                    posicaoBolsaAberturaQuery.ValorMercado.Sum().Coalesce("0").As(fieldValorAberturaBruto),
                                    posicaoBolsaAberturaQuery.ValorMercado.Sum().Coalesce("0").As(fieldValorAberturaLiquido),
                                    posicaoBolsaAberturaQuery.Quantidade.Sum().Coalesce("0").As(fieldQtdeAbertura),
                                    moedaQuery.Nome.As(fieldDescricaoMoedaAtivo),
                                    ativoBolsaQuery.IdMoeda.Coalesce("0").As(fieldIdMoedaAtivo));
            posicaoBolsaAberturaQuery.InnerJoin(ativoBolsaQuery).On(posicaoBolsaAberturaQuery.CdAtivoBolsa.Equal(ativoBolsaQuery.CdAtivoBolsa));
            posicaoBolsaAberturaQuery.LeftJoin(moedaQuery).On(moedaQuery.IdMoeda.Equal(ativoBolsaQuery.IdMoeda));
            posicaoBolsaAberturaQuery.LeftJoin(posicaoBolsaQuery).On(posicaoBolsaAberturaQuery.CdAtivoBolsa.Equal(posicaoBolsaQuery.CdAtivoBolsa)
                                                                  & posicaoBolsaAberturaQuery.IdCliente.Equal(posicaoBolsaQuery.IdCliente));
            posicaoBolsaAberturaQuery.Where(posicaoBolsaAberturaQuery.IdCliente.Equal(idCliente),
                                   posicaoBolsaAberturaQuery.DataHistorico.Equal(data),
                                   posicaoBolsaQuery.IdPosicao.IsNull());
            posicaoBolsaAberturaQuery.GroupBy(posicaoBolsaAberturaQuery.CdAtivoBolsa,
                                    posicaoBolsaAberturaQuery.DataVencimento,
                                    posicaoBolsaAberturaQuery.IdCliente,
                                    moedaQuery.Nome,
                                    ativoBolsaQuery.IdMoeda.Coalesce("0"));       
     
            posicaoBolsaAberturaColl.Load(posicaoBolsaAberturaQuery);
                

            posicaoBolsaQuery.Select(posicaoBolsaQuery.CdAtivoBolsa,
                                    posicaoBolsaQuery.DataVencimento,
                                    posicaoBolsaQuery.ValorMercado.Sum().As(fieldValorFechamentoBruto),
                                    posicaoBolsaQuery.ValorMercado.Sum().As(fieldValorFechamentoLiquido),
                                    posicaoBolsaQuery.Quantidade.Sum().As(fieldQtdeFechamento),
                                    posicaoBolsaAberturaQuery.ValorMercado.Sum().Coalesce("0").As(fieldValorAberturaBruto),
                                    posicaoBolsaAberturaQuery.ValorMercado.Sum().Coalesce("0").As(fieldValorAberturaLiquido),
                                    posicaoBolsaAberturaQuery.Quantidade.Sum().Coalesce("0").As(fieldQtdeAbertura),
                                    moedaQuery.Nome.As(fieldDescricaoMoedaAtivo),
                                    ativoBolsaQuery.IdMoeda.Coalesce("0").As(fieldIdMoedaAtivo));
            posicaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(posicaoBolsaQuery.CdAtivoBolsa.Equal(ativoBolsaQuery.CdAtivoBolsa));
            posicaoBolsaQuery.LeftJoin(moedaQuery).On(moedaQuery.IdMoeda.Equal(ativoBolsaQuery.IdMoeda));
            posicaoBolsaQuery.LeftJoin(posicaoBolsaAberturaQuery).On(posicaoBolsaAberturaQuery.CdAtivoBolsa.Equal(posicaoBolsaQuery.CdAtivoBolsa)                                                                   
                                                                   & posicaoBolsaAberturaQuery.IdCliente.Equal(posicaoBolsaQuery.IdCliente)
                                                                   & posicaoBolsaAberturaQuery.DataHistorico.Equal("'" + data.ToString("yyyyMMdd") + "'"));
            posicaoBolsaQuery.Where(posicaoBolsaQuery.IdCliente.Equal(idCliente));
            posicaoBolsaQuery.GroupBy(posicaoBolsaQuery.CdAtivoBolsa,
                                    posicaoBolsaQuery.DataVencimento,
                                    posicaoBolsaQuery.IdCliente,
                                    moedaQuery.Nome,
                                    ativoBolsaQuery.IdMoeda.Coalesce("0"));

            posicaoBolsaColl.Load(posicaoBolsaQuery);                                   

            #endregion

            #region Carrega as Operações que movimentaram a Operação na data de processamento (Crédito)
            operacaoBolsaEntradaQuery.Select(operacaoBolsaEntradaQuery.CdAtivoBolsa,
                                           operacaoBolsaEntradaQuery.Valor.Sum(),
                                           operacaoBolsaEntradaQuery.Quantidade.Sum());
            operacaoBolsaEntradaQuery.Where(operacaoBolsaEntradaQuery.IdCliente.Equal(idCliente)
                                           & operacaoBolsaEntradaQuery.Data.Equal(data)
                                           & operacaoBolsaEntradaQuery.TipoOperacao.In(lstTipoOperacaoEntrada.ToArray()));
            operacaoBolsaEntradaQuery.GroupBy(operacaoBolsaEntradaQuery.CdAtivoBolsa);
            operacaoBolsaEntradaQuery.OrderBy(operacaoBolsaEntradaQuery.CdAtivoBolsa.Ascending);

            if (operacaoBolsaEntradaColl.Load(operacaoBolsaEntradaQuery))
                lstOperacoesEntrada = (List<OperacaoBolsa>)operacaoBolsaEntradaColl;
            #endregion

            #region Carrega as Operações que movimentaram a Operação na data de processamento (Débito)
            operacaoBolsaSaidaQuery.Select(operacaoBolsaSaidaQuery.CdAtivoBolsa,
                                           operacaoBolsaSaidaQuery.Valor.Sum(),
                                           operacaoBolsaSaidaQuery.Quantidade.Sum());
            operacaoBolsaSaidaQuery.Where(operacaoBolsaSaidaQuery.IdCliente.Equal(idCliente)
                                           & operacaoBolsaSaidaQuery.Data.Equal(data)
                                           & operacaoBolsaSaidaQuery.TipoOperacao.In(lstTipoOperacaoSaida.ToArray()));
            operacaoBolsaSaidaQuery.GroupBy(operacaoBolsaSaidaQuery.CdAtivoBolsa);
            operacaoBolsaSaidaQuery.OrderBy(operacaoBolsaSaidaQuery.CdAtivoBolsa.Ascending);

            if (operacaoBolsaSaidaColl.Load(operacaoBolsaSaidaQuery))
                lstOperacoesSaida = (List<OperacaoBolsa>)operacaoBolsaSaidaColl;
            #endregion

            #region Carrega Moeda e Carteira
            cliente.LoadByPrimaryKey(idCliente);

            if (moeda.LoadByPrimaryKey(cliente.IdMoeda.Value))
            {
                descricaoMoedaCarteira = moeda.Nome;
                idMoedaCarteira = moeda.IdMoeda.Value;
            }
            #endregion

            #region Carrega Rendas dos Ativos
            //Proventos
            proventoBolsaColl.Query.Select(proventoBolsaColl.Query.TipoProvento,
                                           proventoBolsaColl.Query.CdAtivoBolsa,
                                           proventoBolsaColl.Query.Valor.Sum());
            proventoBolsaColl.Query.Where(proventoBolsaColl.Query.DataEx.GreaterThanOrEqual(data) & proventoBolsaColl.Query.DataLancamento.LessThanOrEqual(data));
            proventoBolsaColl.Query.GroupBy(proventoBolsaColl.Query.TipoProvento,
                                            proventoBolsaColl.Query.CdAtivoBolsa);
            proventoBolsaColl.Query.OrderBy(proventoBolsaColl.Query.CdAtivoBolsa.Ascending);

            if (proventoBolsaColl.Query.Load())
                lstProventoBolsa = (List<ProventoBolsa>)proventoBolsaColl;

            //Bonificação
            operacaoBolsaBonificacaoQuery.Select(operacaoBolsaBonificacaoQuery.CdAtivoBolsa,
                                           operacaoBolsaBonificacaoQuery.Valor.Sum(),
                                           operacaoBolsaBonificacaoQuery.Quantidade.Sum(),
                                           operacaoBolsaBonificacaoQuery.ValorLiquido.Sum());
            operacaoBolsaBonificacaoQuery.Where(operacaoBolsaBonificacaoQuery.IdCliente.Equal(idCliente)
                                           & operacaoBolsaBonificacaoQuery.Data.Equal(data)
                                           & operacaoBolsaBonificacaoQuery.Origem.In((byte)OrigemOperacaoBolsa.ResultadoBonificacao)
                                           & operacaoBolsaBonificacaoQuery.TipoOperacao.In(TipoOperacaoBolsa.Deposito, TipoOperacaoBolsa.Retirada));
            operacaoBolsaBonificacaoQuery.GroupBy(operacaoBolsaBonificacaoQuery.CdAtivoBolsa);
            operacaoBolsaBonificacaoQuery.OrderBy(operacaoBolsaBonificacaoQuery.CdAtivoBolsa.Ascending);

            if (operacaoBolsaBonificacaoColl.Load(operacaoBolsaBonificacaoQuery))
                lstBonificacao = (List<OperacaoBolsa>)operacaoBolsaBonificacaoColl;

            //Subscrição
            subscricaoBolsaQuery.Where(subscricaoBolsaQuery.DataEx.LessThanOrEqual(data) & subscricaoBolsaQuery.DataEx.GreaterThanOrEqual(data));
            if (subscricaoBolsaColl.Load(subscricaoBolsaQuery))
                lstSubscricaoBolsa = (List<SubscricaoBolsa>)subscricaoBolsaColl;
            #endregion

            #region Carrega Rentabilidade do dia anterior
            RentabilidadeCollection rentabilidadeAntCollection = new RentabilidadeCollection();
            rentabilidadeAntCollection.Query.Where(rentabilidadeAntCollection.Query.IdCliente.Equal(idCliente) &
                                                   rentabilidadeAntCollection.Query.TipoAtivo.In((int)TipoAtivoAuxiliar.OperacaoBolsa) &
                                                   rentabilidadeAntCollection.Query.Data.Equal(dataAnterior));

            if (rentabilidadeAntCollection.Query.Load())
                lstRentabilidadeAnterior = (List<Rentabilidade>)rentabilidadeAntCollection;
            #endregion

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaColl)
            {
                cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa.Trim();

                SubscricaoBolsa subscricaoBolsa = new SubscricaoBolsa();
                subscricaoBolsa = lstSubscricaoBolsa.Find(delegate(SubscricaoBolsa x) { return x.CdAtivoBolsaDireito.Trim().Equals(cdAtivoBolsa); });
                if (subscricaoBolsa != null && !String.IsNullOrEmpty(subscricaoBolsa.CdAtivoBolsaDireito))
                {
                    continue;
                }

                idMoedaAtivo = Convert.ToInt16(posicaoBolsa.GetColumn(fieldIdMoedaAtivo));
                descricaoMoedaAtivo = posicaoBolsa.GetColumn(fieldDescricaoMoedaAtivo).ToString();
                valorFechamentoLiquido = 0;
                valorAberturaLiquido = 0;
                valorEntrada = 0;
                valorSaidaLiquido = 0;
                valorRendaLiquido = 0;
                valorRendaBruto = 0;
                valorRentabilidadeLiquido = 0;
                valorRentabilidadeBrutaAcumulada = 0;
                valorRentabilidadeBrutaDiaria = 0;
                valorFechamentoBruto = 0;
                valorAberturaBruto = 0;
                valorSaidaBruto = 0;
                qtdeEntrada = 0;
                qtdeSaida = 0;
                qtdeAbertura = 0;
                qtdeFechamento = 0;

                #region Operações Entrada
                operacaoAux = new OperacaoBolsa();
                operacaoAux = lstOperacoesEntrada.Find(delegate(OperacaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); });
                if (operacaoAux != null && !String.IsNullOrEmpty(operacaoAux.CdAtivoBolsa))
                {
                    valorEntrada = Convert.ToDecimal(operacaoAux.Valor.Value);
                    qtdeEntrada = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                }
                else
                {
                    valorEntrada = 0;
                    qtdeEntrada = 0;
                }
                #endregion

                #region Operações Saida
                operacaoAux = new OperacaoBolsa();
                operacaoAux = lstOperacoesSaida.Find(delegate(OperacaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); });
                if (operacaoAux != null && !String.IsNullOrEmpty(operacaoAux.CdAtivoBolsa))
                {
                    valorSaidaLiquido = Convert.ToDecimal(operacaoAux.Valor.Value);
                    valorSaidaBruto = Convert.ToDecimal(operacaoAux.Valor.Value);
                    qtdeSaida = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                }
                else
                {
                    valorSaidaLiquido = 0;
                    valorSaidaBruto = 0;
                    qtdeSaida = 0;
                }
                #endregion

                if (!Decimal.TryParse(posicaoBolsa.GetColumn(fieldValorAberturaBruto).ToString(), out valorAberturaBruto))
                    valorAberturaBruto = 0;

                if (!Decimal.TryParse(posicaoBolsa.GetColumn(fieldValorFechamentoBruto).ToString(), out valorFechamentoBruto))
                    valorFechamentoBruto = 0;

                if (!Decimal.TryParse(posicaoBolsa.GetColumn(fieldValorFechamentoLiquido).ToString(), out valorFechamentoLiquido))
                    valorFechamentoLiquido = 0;

                if (!Decimal.TryParse(posicaoBolsa.GetColumn(fieldValorAberturaLiquido).ToString(), out valorAberturaLiquido))
                    valorAberturaLiquido = 0;

                if (!Decimal.TryParse(posicaoBolsa.GetColumn(fieldQtdeAbertura).ToString(), out qtdeAbertura))
                    qtdeAbertura = 0;

                if (!Decimal.TryParse(posicaoBolsa.GetColumn(fieldQtdeFechamento).ToString(), out qtdeFechamento))
                    qtdeFechamento = 0;

                #region Rendas
                //Proventos
                lstProventoBolsaAtivo.AddRange(lstProventoBolsa.FindAll(delegate(ProventoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); }));
                if (lstProventoBolsaAtivo.Count > 0)
                {
                    foreach (ProventoBolsa provento in lstProventoBolsaAtivo)
                    {
                        valorRendaLiquido += provento.Valor.Value * (qtdeAbertura == 0 ? qtdeFechamento : qtdeAbertura);
                        valorRendaBruto += provento.Valor.Value * (qtdeAbertura == 0 ? qtdeFechamento : qtdeAbertura);
                    }
                }

                //Bonificação
                operacaoAux = new OperacaoBolsa();
                operacaoAux = lstBonificacao.Find(delegate(OperacaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); });
                if (operacaoAux != null && !String.IsNullOrEmpty(operacaoAux.CdAtivoBolsa))
                {
                    valorRendaBruto += operacaoAux.Valor.GetValueOrDefault(0);
                    valorRendaLiquido += (operacaoAux.ValorLiquido.GetValueOrDefault(0) == 0 ? operacaoAux.ValorLiquido.Value : operacaoAux.Valor.Value);
                }

                //Subscrição
                subscricaoBolsa = new SubscricaoBolsa();
                subscricaoBolsa = lstSubscricaoBolsa.Find(delegate(SubscricaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); });
                if (subscricaoBolsa != null && !String.IsNullOrEmpty(subscricaoBolsa.CdAtivoBolsa))
                {
                    List<PosicaoBolsa> lstPosicaoBolsa = (List<PosicaoBolsa>)posicaoBolsaColl;
                    PosicaoBolsa posicaoBolsaAux = lstPosicaoBolsa.Find(delegate(PosicaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(subscricaoBolsa.CdAtivoBolsaDireito.Trim()); });

                    if (!string.IsNullOrEmpty(posicaoBolsaAux.CdAtivoBolsa))
                    {
                        decimal valorSubscricao = 0;
                        if (Decimal.TryParse(posicaoBolsaAux.GetColumn(fieldValorFechamentoLiquido).ToString(), out valorSubscricao))
                        {
                            valorRendaLiquido += valorSubscricao;
                            valorRendaBruto += valorSubscricao;
                        }
                    }
                }
                #endregion

                #region Rentabilidade Bruta
                try
                {
                    valorRentabilidadeBrutaDiaria = ((valorFechamentoBruto + valorSaidaBruto) / (valorAberturaBruto + valorEntrada - valorRendaBruto)) - 1;
                }
                catch (Exception ex)
                {
                    valorRentabilidadeBrutaDiaria = 0;
                }
                #endregion

                #region Rentabilidade Liquida
                try
                {
                    valorRentabilidadeLiquido = ((valorFechamentoLiquido + valorSaidaLiquido) / (valorAberturaLiquido + valorEntrada - valorRendaLiquido)) - 1;
                }
                catch (Exception ex)
                {
                    valorRentabilidadeLiquido = 0;
                }
                #endregion

                #region Rentabilidade Acumulada
                valorRentabilidadeBrutaAcumulada = valorRentabilidadeBrutaDiaria;
                Rentabilidade rentabilidadeAux = new Rentabilidade();
                rentabilidadeAux = lstRentabilidadeAnterior.Find(delegate(Rentabilidade x) { return x.CdAtivoBolsa.Equals(posicaoBolsa.CdAtivoBolsa); });
                if (rentabilidadeAux != null && rentabilidadeAux.IdRentabilidade.Value > 0)
                {
                    if (rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.GetValueOrDefault(0) > 0)
                        valorRentabilidadeBrutaAcumulada = ((1 + rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.Value) / 100) * ((1 + valorRentabilidadeBrutaDiaria) / 100);
                }
                #endregion

                if (posicaoBolsa.DataVencimento.HasValue)
                    descricaoCompleta = posicaoBolsa.CdAtivoBolsa + " - Vencto: " + String.Format("{0:dd/MM/yyyy}", posicaoBolsa.DataVencimento.Value);
                else
                    descricaoCompleta = posicaoBolsa.CdAtivoBolsa;

                #region Popula objeto
                Rentabilidade rentabilidade = rentabilidadeColl.AddNew();
                rentabilidade.CodigoAtivo = descricaoCompleta;
                rentabilidade.TipoAtivo = (int)TipoAtivoAuxiliar.OperacaoBolsa;
                rentabilidade.Data = data;
                rentabilidade.IdCliente = idCliente;
                rentabilidade.CdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                rentabilidade.QuantidadeFinalAtivo = qtdeFechamento;
                rentabilidade.QuantidadeInicialAtivo = qtdeAbertura;
                rentabilidade.QuantidadeTotalEntradaAtivo = qtdeEntrada;
                rentabilidade.QuantidadeTotalSaidaAtivo = qtdeSaida;

                //Ativo
                rentabilidade.CodigoMoedaAtivo = descricaoMoedaAtivo;
                rentabilidade.PatrimonioFinalMoedaPortfolio = rentabilidade.PatrimonioFinalMoedaAtivo = valorFechamentoLiquido;
                rentabilidade.PatrimonioInicialMoedaPortfolio = rentabilidade.PatrimonioInicialMoedaAtivo = valorAberturaLiquido;
                rentabilidade.RentabilidadeMoedaPortfolio = rentabilidade.RentabilidadeMoedaAtivo = valorRentabilidadeLiquido;
                rentabilidade.ValorFinanceiroEntradaMoedaPortfolio = rentabilidade.ValorFinanceiroEntradaMoedaAtivo = valorEntrada;
                rentabilidade.ValorFinanceiroSaidaMoedaPortfolio = rentabilidade.ValorFinanceiroSaidaMoedaAtivo = valorSaidaLiquido;
                rentabilidade.ValorFinanceiroRendasMoedaPortfolio = rentabilidade.ValorFinanceiroRendasMoedaAtivo = valorRendaLiquido;
                rentabilidade.RentabilidadeBrutaDiariaMoedaAtivo = rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio = valorRentabilidadeBrutaDiaria;
                rentabilidade.RentabilidadeBrutaAcumMoedaAtivo = rentabilidade.RentabilidadeBrutaAcumMoedaPortfolio = valorRentabilidadeBrutaAcumulada;
                rentabilidade.RentabilidadeGrossUpDiariaMoedaAtivo = rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio = 0;
                rentabilidade.RentabilidadeGrossUpAcumMoedaAtivo = rentabilidade.RentabilidadeGrossUpAcumMoedaPortfolio = 0;
                rentabilidade.PatrimonioFinalBrutoMoedaAtivo = rentabilidade.PatrimonioFinalBrutoMoedaPortfolio = valorFechamentoBruto;
                rentabilidade.PatrimonioInicialBrutoMoedaPortfolio = rentabilidade.PatrimonioInicialBrutoMoedaAtivo = valorAberturaBruto;
                rentabilidade.PatrimonioFinalGrossUpMoedaAtivo = rentabilidade.PatrimonioFinalGrossUpMoedaPortfolio = valorFechamentoBruto;

                //Portfolio
                rentabilidade.CodigoMoedaPortfolio = descricaoMoedaCarteira;

                //Converte os valores para a moeda da carteira
                rentabilidade.ConverteValores(idMoedaAtivo, idMoedaCarteira, ref dicRentConversao);
                #endregion

            }

            //rentabilidadeColl.Save();

            //faz para as posições zeradas 
            foreach (PosicaoBolsaAbertura posicaoBolsa in posicaoBolsaAberturaColl)
            {
                cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa.Trim();

                SubscricaoBolsa subscricaoBolsa = new SubscricaoBolsa();
                subscricaoBolsa = lstSubscricaoBolsa.Find(delegate(SubscricaoBolsa x) { return x.CdAtivoBolsaDireito.Trim().Equals(cdAtivoBolsa); });
                if (subscricaoBolsa != null && !String.IsNullOrEmpty(subscricaoBolsa.CdAtivoBolsaDireito))
                {
                    continue;
                }

                idMoedaAtivo = Convert.ToInt16(posicaoBolsa.GetColumn(fieldIdMoedaAtivo));
                descricaoMoedaAtivo = posicaoBolsa.GetColumn(fieldDescricaoMoedaAtivo).ToString();
                valorFechamentoLiquido = 0;
                valorAberturaLiquido = 0;
                valorEntrada = 0;
                valorSaidaLiquido = 0;
                valorRendaLiquido = 0;
                valorRendaBruto = 0;
                valorRentabilidadeLiquido = 0;
                valorRentabilidadeBrutaAcumulada = 0;
                valorRentabilidadeBrutaDiaria = 0;
                valorFechamentoBruto = 0;
                valorAberturaBruto = 0;
                valorSaidaBruto = 0;
                qtdeEntrada = 0;
                qtdeSaida = 0;
                qtdeAbertura = 0;
                qtdeFechamento = 0;

                #region Operações Entrada
                operacaoAux = new OperacaoBolsa();
                operacaoAux = lstOperacoesEntrada.Find(delegate(OperacaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); });
                if (operacaoAux != null && !String.IsNullOrEmpty(operacaoAux.CdAtivoBolsa))
                {
                    valorEntrada = Convert.ToDecimal(operacaoAux.Valor.Value);
                    qtdeEntrada = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                }
                else
                {
                    valorEntrada = 0;
                    qtdeEntrada = 0;
                }
                #endregion

                #region Operações Saida
                operacaoAux = new OperacaoBolsa();
                operacaoAux = lstOperacoesSaida.Find(delegate(OperacaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); });
                if (operacaoAux != null && !String.IsNullOrEmpty(operacaoAux.CdAtivoBolsa))
                {
                    valorSaidaLiquido = Convert.ToDecimal(operacaoAux.Valor.Value);
                    valorSaidaBruto = Convert.ToDecimal(operacaoAux.Valor.Value);
                    qtdeSaida = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                }
                else
                {
                    valorSaidaLiquido = 0;
                    valorSaidaBruto = 0;
                    qtdeSaida = 0;
                }
                #endregion

                if (!Decimal.TryParse(posicaoBolsa.GetColumn(fieldValorAberturaBruto).ToString(), out valorAberturaBruto))
                    valorAberturaBruto = 0;

                valorFechamentoBruto = 0;
                valorFechamentoLiquido = 0;

                if (!Decimal.TryParse(posicaoBolsa.GetColumn(fieldValorAberturaLiquido).ToString(), out valorAberturaLiquido))
                    valorAberturaLiquido = 0;

                if (!Decimal.TryParse(posicaoBolsa.GetColumn(fieldQtdeAbertura).ToString(), out qtdeAbertura))
                    qtdeAbertura = 0;

                qtdeFechamento = 0;

                #region Rendas
                //Proventos
                lstProventoBolsaAtivo.AddRange(lstProventoBolsa.FindAll(delegate(ProventoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); }));
                if (lstProventoBolsaAtivo.Count > 0)
                {
                    foreach (ProventoBolsa provento in lstProventoBolsaAtivo)
                    {
                        valorRendaLiquido += provento.Valor.Value * (qtdeAbertura == 0 ? qtdeFechamento : qtdeAbertura);
                        valorRendaBruto += provento.Valor.Value * (qtdeAbertura == 0 ? qtdeFechamento : qtdeAbertura);
                    }
                }

                //Bonificação
                operacaoAux = new OperacaoBolsa();
                operacaoAux = lstBonificacao.Find(delegate(OperacaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); });
                if (operacaoAux != null && !String.IsNullOrEmpty(operacaoAux.CdAtivoBolsa))
                {
                    valorRendaBruto += operacaoAux.Valor.GetValueOrDefault(0);
                    valorRendaLiquido += (operacaoAux.ValorLiquido.GetValueOrDefault(0) == 0 ? operacaoAux.ValorLiquido.Value : operacaoAux.Valor.Value);
                }

                //Subscrição
                subscricaoBolsa = new SubscricaoBolsa();
                subscricaoBolsa = lstSubscricaoBolsa.Find(delegate(SubscricaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(cdAtivoBolsa); });
                if (subscricaoBolsa != null && !String.IsNullOrEmpty(subscricaoBolsa.CdAtivoBolsa))
                {
                    List<PosicaoBolsa> lstPosicaoBolsa = (List<PosicaoBolsa>)posicaoBolsaColl;
                    PosicaoBolsa posicaoBolsaAux = lstPosicaoBolsa.Find(delegate(PosicaoBolsa x) { return x.CdAtivoBolsa.Trim().Equals(subscricaoBolsa.CdAtivoBolsaDireito.Trim()); });

                    if (!string.IsNullOrEmpty(posicaoBolsaAux.CdAtivoBolsa))
                    {
                        decimal valorSubscricao = 0;
                        if (Decimal.TryParse(posicaoBolsaAux.GetColumn(fieldValorFechamentoLiquido).ToString(), out valorSubscricao))
                        {
                            valorRendaLiquido += valorSubscricao;
                            valorRendaBruto += valorSubscricao;
                        }
                    }
                }
                #endregion

                #region Rentabilidade Bruta
                try
                {
                    valorRentabilidadeBrutaDiaria = ((valorFechamentoBruto + valorSaidaBruto) / (valorAberturaBruto + valorEntrada - valorRendaBruto)) - 1;
                }
                catch (Exception ex)
                {
                    valorRentabilidadeBrutaDiaria = 0;
                }
                #endregion

                #region Rentabilidade Liquida
                try
                {
                    valorRentabilidadeLiquido = ((valorFechamentoLiquido + valorSaidaLiquido) / (valorAberturaLiquido + valorEntrada - valorRendaLiquido)) - 1;
                }
                catch (Exception ex)
                {
                    valorRentabilidadeLiquido = 0;
                }
                #endregion

                #region Rentabilidade Acumulada
                valorRentabilidadeBrutaAcumulada = valorRentabilidadeBrutaDiaria;
                Rentabilidade rentabilidadeAux = new Rentabilidade();
                rentabilidadeAux = lstRentabilidadeAnterior.Find(delegate(Rentabilidade x) { return x.CdAtivoBolsa.Equals(posicaoBolsa.CdAtivoBolsa); });
                if (rentabilidadeAux != null && rentabilidadeAux.IdRentabilidade.Value > 0)
                {
                    if (rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.GetValueOrDefault(0) > 0)
                        valorRentabilidadeBrutaAcumulada = ((1 + rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.Value) / 100) * ((1 + valorRentabilidadeBrutaDiaria) / 100);
                }
                #endregion

                if (posicaoBolsa.DataVencimento.HasValue)
                    descricaoCompleta = posicaoBolsa.CdAtivoBolsa + " - Vencto: " + String.Format("{0:dd/MM/yyyy}", posicaoBolsa.DataVencimento.Value);
                else
                    descricaoCompleta = posicaoBolsa.CdAtivoBolsa;

                #region Popula objeto
                Rentabilidade rentabilidade = rentabilidadeColl.AddNew();
                rentabilidade.CodigoAtivo = descricaoCompleta;
                rentabilidade.TipoAtivo = (int)TipoAtivoAuxiliar.OperacaoBolsa;
                rentabilidade.Data = data;
                rentabilidade.IdCliente = idCliente;
                rentabilidade.CdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                rentabilidade.QuantidadeFinalAtivo = qtdeFechamento;
                rentabilidade.QuantidadeInicialAtivo = qtdeAbertura;
                rentabilidade.QuantidadeTotalEntradaAtivo = qtdeEntrada;
                rentabilidade.QuantidadeTotalSaidaAtivo = qtdeSaida;

                //Ativo
                rentabilidade.CodigoMoedaAtivo = descricaoMoedaAtivo;
                rentabilidade.PatrimonioFinalMoedaPortfolio = rentabilidade.PatrimonioFinalMoedaAtivo = valorFechamentoLiquido;
                rentabilidade.PatrimonioInicialMoedaPortfolio = rentabilidade.PatrimonioInicialMoedaAtivo = valorAberturaLiquido;
                rentabilidade.RentabilidadeMoedaPortfolio = rentabilidade.RentabilidadeMoedaAtivo = valorRentabilidadeLiquido;
                rentabilidade.ValorFinanceiroEntradaMoedaPortfolio = rentabilidade.ValorFinanceiroEntradaMoedaAtivo = valorEntrada;
                rentabilidade.ValorFinanceiroSaidaMoedaPortfolio = rentabilidade.ValorFinanceiroSaidaMoedaAtivo = valorSaidaLiquido;
                rentabilidade.ValorFinanceiroRendasMoedaPortfolio = rentabilidade.ValorFinanceiroRendasMoedaAtivo = valorRendaLiquido;
                rentabilidade.RentabilidadeBrutaDiariaMoedaAtivo = rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio = valorRentabilidadeBrutaDiaria;
                rentabilidade.RentabilidadeBrutaAcumMoedaAtivo = rentabilidade.RentabilidadeBrutaAcumMoedaPortfolio = valorRentabilidadeBrutaAcumulada;
                rentabilidade.RentabilidadeGrossUpDiariaMoedaAtivo = rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio = 0;
                rentabilidade.RentabilidadeGrossUpAcumMoedaAtivo = rentabilidade.RentabilidadeGrossUpAcumMoedaPortfolio = 0;
                rentabilidade.PatrimonioFinalBrutoMoedaAtivo = rentabilidade.PatrimonioFinalBrutoMoedaPortfolio = valorFechamentoBruto;
                rentabilidade.PatrimonioInicialBrutoMoedaPortfolio = rentabilidade.PatrimonioInicialBrutoMoedaAtivo = valorAberturaBruto;
                rentabilidade.PatrimonioFinalGrossUpMoedaAtivo = rentabilidade.PatrimonioFinalGrossUpMoedaPortfolio = valorFechamentoBruto;

                //Portfolio
                rentabilidade.CodigoMoedaPortfolio = descricaoMoedaCarteira;

                //Converte os valores para a moeda da carteira
                rentabilidade.ConverteValores(idMoedaAtivo, idMoedaCarteira, ref dicRentConversao);
                #endregion

            }

            rentabilidadeColl.Save();
        }

    }    
}
