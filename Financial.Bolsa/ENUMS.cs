﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Financial.Bolsa.Properties;
using System.Globalization;
using System.Threading;
using System.Resources;
using System.Reflection;

namespace Financial.Bolsa.Enums {

    public enum TipoCotacaoBolsa
    {
        Medio = 1,
        Fechamento = 2
    }

    public static class TipoOperacaoBolsa {
        public const string Compra = "C";
        public const string Venda = "V";
        public const string CompraDaytrade = "CD";
        public const string VendaDaytrade = "VD";
        public const string Deposito = "DE";
        public const string Retirada = "RE";
        public const string IngressoAtivoImpactoQtde = "IQ";
        public const string IngressoAtivoImpactoCota = "IC";
        public const string RetiradaAtivoImpactoQtde = "RQ";
        public const string RetiradaAtivoImpactoCota = "RC";

        /// <summary>
        ///     Valores possiveis para o Enum
        /// </summary>
        /// <returns></returns>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(Compra);
            valoresPossiveis.Add(Venda);
            valoresPossiveis.Add(CompraDaytrade);
            valoresPossiveis.Add(VendaDaytrade);
            valoresPossiveis.Add(Deposito);
            valoresPossiveis.Add(Retirada);
            valoresPossiveis.Add(IngressoAtivoImpactoQtde);
            valoresPossiveis.Add(IngressoAtivoImpactoCota);
            valoresPossiveis.Add(RetiradaAtivoImpactoQtde);
            valoresPossiveis.Add(RetiradaAtivoImpactoCota);
            return valoresPossiveis;
        }
        
        /// <summary>
        ///    Classe Usada para Retornar os textos das Constantes
        /// </summary>
        public static class str {
            /// <summary>
            /// Retorna os tipoOperacões como string
            /// </summary>
            /// <exception>throws ArgumentException se não existir um enum com o valor fornecido</exception> 
            /// <returns></returns>            
            //HACK: Verificar possibilidade de construir através de refletion sem usar switch case                       
            public static string RetornaTexto(string tipoOperacaoBolsa) {
                string tipoOperacao = "";
                switch (tipoOperacaoBolsa) {
                    case TipoOperacaoBolsa.Compra:
                        tipoOperacao = Resources._TipoOperacaoBolsa_Compra;
                        break;
                    case TipoOperacaoBolsa.Venda:
                        tipoOperacao = Resources._TipoOperacaoBolsa_Venda;
                        break;
                    case TipoOperacaoBolsa.CompraDaytrade:
                        tipoOperacao = Resources._TipoOperacaoBolsa_CompraDaytrade;
                        break;
                    case TipoOperacaoBolsa.VendaDaytrade:
                        tipoOperacao = Resources._TipoOperacaoBolsa_VendaDaytrade;
                        break;
                    case TipoOperacaoBolsa.Deposito:
                        tipoOperacao = Resources._TipoOperacaoBolsa_Deposito;
                        break;
                    case TipoOperacaoBolsa.Retirada:
                        tipoOperacao = Resources._TipoOperacaoBolsa_Retirada;
                        break;
                    default:
                        throw new ArgumentException("TipoOperacaoBolsa não possui Constante com valor " + tipoOperacaoBolsa);
                }
                return tipoOperacao;
            }
        }
    }

    public static class TipoMercadoBolsa {
        // Todos os valores devem estar cadastrados no Resource do Componente
        public const string MercadoVista = "VIS";
        public const string OpcaoCompra = "OPC";
        public const string OpcaoVenda = "OPV";
        public const string Termo = "TER";
        public const string Futuro = "FUT";
        public const string Imobiliario = "IMO";

        /// <summary>
        ///     Valores possiveis para o Enum
        /// </summary>
        /// <returns></returns>
        public static List<string> Values() {
            List<string> valoresPossiveis = new List<string>();
            valoresPossiveis.Add(MercadoVista);
            valoresPossiveis.Add(OpcaoCompra);
            valoresPossiveis.Add(OpcaoVenda);
            valoresPossiveis.Add(Termo);
            valoresPossiveis.Add(Futuro);
            valoresPossiveis.Add(Imobiliario);
            return valoresPossiveis;
        }

        /// <summary>
        ///    Classe Usada para Retornar os textos das Constantes
        /// </summary>
        public static class str {
            /// <summary>
            /// Mapeamento do valor do enum para o texto
            /// </summary>
            /// <param name="tipoMercadoBolsa"></param>
            /// <exception>throws ArgumentException se não existir um enum com o valor fornecido</exception> 
            /// <returns></returns>            
            //HACK: Verificar possibilidade de construir através de refletion sem usar switch case
            public static string RetornaTexto(string tipoMercadoBolsa) {
                string tipoMercado = "";
                switch (tipoMercadoBolsa) {
                    case TipoMercadoBolsa.MercadoVista:
                        tipoMercado = Resources._TipoMercado_MercadoVista;
                        break;
                    case TipoMercadoBolsa.OpcaoCompra:
                        tipoMercado = Resources._TipoMercado_OpcaoCompra;
                        break;
                    case TipoMercadoBolsa.OpcaoVenda:
                        tipoMercado = Resources._TipoMercado_OpcaoVenda;
                        break;
                    case TipoMercadoBolsa.Termo:
                        tipoMercado = Resources._TipoMercado_Termo;
                        break;
                    case TipoMercadoBolsa.Futuro:
                        tipoMercado = Resources._TipoMercado_Futuro;
                        break;
                    case TipoMercadoBolsa.Imobiliario:
                        tipoMercado = Resources._TipoMercado_Imobiliario;
                        break;
                    default:
                        throw new ArgumentException("TipoMercadoBolsa não possue Constante com valor " + tipoMercadoBolsa);
                }
                return tipoMercado;
            }
        }
    }

    /// <summary>
    /// Enum dos tipos de origem, conforme abaixo:
    /// 
    /// Primaria - Operacao Manual ou integrada
    /// ExercicioOpcao - DE/RE proveniente de exercicio de opcao
    /// LiquidacaoTermo - DE/RE proveniente de liquidacao (vencimento ou antecipacao) de termo
    /// TransferenciaCorretora - DE/RE proveniente de transferencia de corretora
    /// VencimentoOpcao - DE/RE proveniente de vencimento (pó) de opcao
    /// LiquidacaoEmprestimo - DE/RE proveniente de liquidacao (vencimento ou antecipacao) de aluguel
    /// LiquidacaoFuturo - DE/RE proveniente de liquidacao (vencimento ou antecipacao) de futuro de acoes
    /// </summary>
    public static class OrigemOperacaoBolsa {
        public const int Primaria = 1;
        public const int ExercicioOpcaoCompra = 2;
        public const int ExercicioOpcaoVenda = 3;
        public const int LiquidacaoTermo = 4;
        public const int TransferenciaCorretora = 5;
        public const int VencimentoOpcao = 6;
        public const int AberturaEmprestimo = 7;
        public const int LiquidacaoEmprestimo = 8;
        public const int LiquidacaoFuturo = 9;
        public const int OperacaoPOP = 10;
        public const int DireitoSubscricao = 20;
        public const int Oferta = 30;
        public const int ResultadoConversao = 80;
        public const int ResultadoGrupamento = 81;
        public const int ResultadoBonificacao = 82;
        public const int NaoEspecificado = 100;

        /// <summary>
        ///     Valores possiveis para o Enum
        /// </summary>
        /// <returns></returns>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Primaria);
            valoresPossiveis.Add(ExercicioOpcaoCompra);
            valoresPossiveis.Add(ExercicioOpcaoVenda);
            valoresPossiveis.Add(LiquidacaoTermo);
            valoresPossiveis.Add(TransferenciaCorretora);
            valoresPossiveis.Add(VencimentoOpcao);
            valoresPossiveis.Add(AberturaEmprestimo);
            valoresPossiveis.Add(LiquidacaoEmprestimo);
            valoresPossiveis.Add(LiquidacaoFuturo);
            valoresPossiveis.Add(OperacaoPOP);
            valoresPossiveis.Add(DireitoSubscricao);
            valoresPossiveis.Add(Oferta);
            return valoresPossiveis;
        }

        /// <summary>
        ///    Classe Usada para Retornar os textos das Constantes
        /// </summary>
        public static class str {
            /// <summary>
            /// Retorna  como string
            /// </summary>
            /// <exception>throws ArgumentException se não existir um enum com o valor fornecido</exception> 
            /// <returns></returns>            
            //HACK: Verificar possibilidade de construir através de refletion sem usar switch case
            public static string RetornaTexto(int origemOperacaoBolsa) {
                string origemOperacao = "";
                switch (origemOperacaoBolsa) {
                    case OrigemOperacaoBolsa.Primaria:
                        origemOperacao = Resources._OrigemOperacaoBolsa_Primaria;
                        break;
                    case OrigemOperacaoBolsa.ExercicioOpcaoCompra:
                        origemOperacao = Resources._OrigemOperacaoBolsa_ExercicioOpcaoCompra;
                        break;
                    case OrigemOperacaoBolsa.ExercicioOpcaoVenda:
                        origemOperacao = Resources._OrigemOperacaoBolsa_ExercicioOpcaoVenda;
                        break;
                    case OrigemOperacaoBolsa.LiquidacaoTermo:
                        origemOperacao = Resources._OrigemOperacaoBolsa_LiquidacaoTermo;
                        break;
                    case OrigemOperacaoBolsa.TransferenciaCorretora:
                        origemOperacao = Resources._OrigemOperacaoBolsa_TransferenciaCorretora;
                        break;
                    case OrigemOperacaoBolsa.VencimentoOpcao:
                        origemOperacao = Resources._OrigemOperacaoBolsa_VencimentoOpcao;
                        break;
                    case OrigemOperacaoBolsa.AberturaEmprestimo:
                        origemOperacao = Resources._OrigemOperacaoBolsa_AberturaEmprestimo;
                        break;
                    case OrigemOperacaoBolsa.LiquidacaoEmprestimo:
                        origemOperacao = Resources._OrigemOperacaoBolsa_LiquidacaoEmprestimo;
                        break;
                    case OrigemOperacaoBolsa.LiquidacaoFuturo:
                        origemOperacao = Resources._OrigemOperacaoBolsa_LiquidacaoFuturo;
                        break;
                    case OrigemOperacaoBolsa.OperacaoPOP:
                        origemOperacao = Resources._OrigemOperacaoBolsa_OperacaoPOP;
                        break;
                    case OrigemOperacaoBolsa.DireitoSubscricao:
                        origemOperacao = Resources._OrigemOperacaoBolsa_DireitoSubscricao;
                        break;
                    case OrigemOperacaoBolsa.Oferta:
                        origemOperacao = Resources._OrigemOperacaoBolsa_Oferta;
                        break;
                    default:
                        throw new ArgumentException("Origem Operacao não possue Constante com valor " + origemOperacaoBolsa);
                }
                return origemOperacao;
            }
        }
    }

    /// <summary>
    /// Enum dos tipos de fonte de dados para OperacaoBolsa
    /// </summary>
    public enum FonteOperacaoBolsa {
        Interno = 1,
        Manual = 2,        
        Sinacor = 3,
        ArquivoNEGS = 4,
        ArquivoCONR = 5,
        ArquivoCRCA = 6,
        ArquivoPESC = 7,
        ArquivoGABS = 8,
        NotaPDFSinacor = 20,
        Tema = 50,
        OrdemBolsa = 100,
        Gerencial = 105
    }
        
    public static class TipoEventoBolsa {
        public const int Bonificacao = 20;        
        public const int Grupamento = 40;
        public const int Subscricao = 50;
        public const int Conversao = 60;        

        /// <summary>
        /// Valores possiveis para o enum
        /// </summary>
        /// <returns></returns>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            valoresPossiveis.Add(Bonificacao);
            valoresPossiveis.Add(Grupamento);
            valoresPossiveis.Add(Conversao);            
            valoresPossiveis.Add(Subscricao);
            return valoresPossiveis;
        }
    }

    public static class TipoMovimentoEvento {
        public const string Deposito = "DE";
        public const string Retirada = "RE";
    }
        
    public static class TipoOrdemBolsa {
        public const string Compra = "C";
        public const string Venda = "V";
        public const string Ingresso = "I";
        public const string Retirada = "R";
    }

    public enum OrigemOrdemBolsa
    {
        Primaria = 1,
        ExercicioOpcaoCompra = 2,
        ExercicioOpcaoVenda = 3,
        OperacaoPOP = 10,
        Oferta = 30
    }

    /// <summary>
    /// Enum dos tipos de fonte de dados para OrdemBolsa
    /// </summary>
    public enum FonteOrdemBolsa {
        Manual = 2,
        Sinacor = 3,
        ArquivoNEGS = 4,
        ArquivoCONR = 5,
        ArquivoCRCA = 6,
        ArquivoPESC = 7,
        NotaHTML_ORRFAX02 = 8,
        NotaHTML_ORRLNOTA_FUT = 9,
        NotaHTML_ORRLNOTA_NOR = 10,
        NotaPDFSinacor = 20,
        SinacorExterno = 30
    }
        
    public enum TipoEmprestimoBolsa {
        Voluntario = 1,
        Compulsorio = 2
    }
        
    public enum PontaEmprestimoBolsa {
        Doador = 1,
        Tomador = 2
    }
        
    public enum FonteEmprestimoBolsa {
        Manual = 1,
        ArquivoDBTC = 2,
        PlanilhaMellon = 3,
        Sinacor = 4
    }
        
    public enum TipoLiquidacaoEmprestimoBolsa {
        Antecipacao = 1,
        Vencimento = 2
    }

    public enum FonteLiquidacaoEmprestimoBolsa
    {
        Manual = 1,
        ArquivoDBTL = 2,
        PlanilhaMellon = 3,
        Sinacor = 4
    }

    public enum CustodiaBTC
    {
        NaoAltera = 1,
        AlteraClientesSemIR = 2,
        AlteraTodas = 3
    }
        
    /// <summary>
    /// Taxas com validade a partir de 01/12/2006
    /// </summary>
    public static class TipoTaxaBolsa 
    {
        public static class MercadoVista
        {
            #region Constantes
            public const int Final = 1;
            public const int DayTrade = 2;
            public const int CarteiraPropria = 3;
            public const int ExercicioOpcaoLancadaOPC = 4;
            public const int Fundo = 5;
            public const int Clube = 6;
            public const int ExercicioOpcaoIndice = 7;
            #endregion

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(Final);
                valoresPossiveis.Add(DayTrade);
                valoresPossiveis.Add(CarteiraPropria);
                valoresPossiveis.Add(ExercicioOpcaoLancadaOPC);
                valoresPossiveis.Add(Fundo);
                valoresPossiveis.Add(Clube);
                valoresPossiveis.Add(ExercicioOpcaoIndice);
                return valoresPossiveis;
            }
        }

        public static class MercadoFuturo {
            #region Constantes
            public const int Final = 30;
            public const int DayTrade = 31;
            #endregion

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(Final);
                valoresPossiveis.Add(DayTrade);
                return valoresPossiveis;
            }
        }

        public static class MercadoOpcao {
            #region Constantes
            public const int Final = 40;
            public const int DayTrade = 41;
            public const int CarteiraPropria = 42;
            public const int Fundo = 43;
            public const int Clube = 44;
            #endregion

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(Final);
                valoresPossiveis.Add(DayTrade);
                valoresPossiveis.Add(CarteiraPropria);
                valoresPossiveis.Add(Fundo);
                valoresPossiveis.Add(Clube);
                return valoresPossiveis;
            }
        }

        public static class MercadoOpcaoIndice {
            #region Constantes
            public const int Final = 50;
            public const int DayTrade = 51;
            public const int CarteiraPropria = 52;
            public const int Fundo = 53;
            public const int Clube = 54;
            #endregion

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(Final);
                valoresPossiveis.Add(DayTrade);
                valoresPossiveis.Add(CarteiraPropria);
                valoresPossiveis.Add(Fundo);
                valoresPossiveis.Add(Clube);
                return valoresPossiveis;
            }
        }

        public static class Box {
            #region Constantes
            public const int Final = 60;
            public const int CarteiraPropria = 61;
            public const int Fundo = 62;
            public const int Clube = 63;
            #endregion

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(Final);
                valoresPossiveis.Add(CarteiraPropria);
                valoresPossiveis.Add(Fundo);
                valoresPossiveis.Add(Clube);
                return valoresPossiveis;
            }
        }
        
        public static class EmprestimoBolsa {
            #region Constantes
            public const int Voluntario = 100;
            public const int Compulsorio = 101;
            #endregion

            /// <summary>
            /// Valores possiveis para o enum
            /// </summary>
            /// <returns></returns>
            public static List<int> Values() {
                List<int> valoresPossiveis = new List<int>();
                valoresPossiveis.Add(Voluntario);
                valoresPossiveis.Add(Compulsorio);
                return valoresPossiveis;
            }
        }

        public const int Leilao = 15;

        public const int MercadoTermo = 20;

        public const int Balcao = 70;        
        
        /// <summary>
        /// Valores possiveis para o enum
        /// Concatenação de todas as outras listas
        /// </summary>
        /// <returns></returns>
        public static List<int> Values() {
            List<int> valoresPossiveis = new List<int>();
            // 
            valoresPossiveis.Add(Leilao);
            valoresPossiveis.Add(MercadoTermo);
            valoresPossiveis.Add(Balcao);            

            /* MercadoVista */
            #region MercadoVista
            List<int> valores = MercadoVista.Values();
            foreach (int valor in valores) {
                valoresPossiveis.Add(valor);
            }
            #endregion

            /* MercadoFuturo */
            #region MercadoFuturo
            List<int> valores1 = MercadoFuturo.Values();
            foreach (int valor in valores1) {
                valoresPossiveis.Add(valor);
            }
            #endregion

            /* MercadoOpcao */
            #region MercadoOpcao
            List<int> valores2 = MercadoOpcao.Values();
            foreach (int valor in valores2) {
                valoresPossiveis.Add(valor);
            }
            #endregion

            /* MercadoOpcaoIndice */
            #region MercadoOpcaoIndice
            List<int> valores3 = MercadoOpcaoIndice.Values();
            foreach (int valor in valores3) {
                valoresPossiveis.Add(valor);
            }
            #endregion

            /* Box */
            #region Box
            List<int> valores4 = Box.Values();
            foreach (int valor in valores4) {
                valoresPossiveis.Add(valor);
            }
            #endregion

            /* EmprestimoBolsa */
            #region EmprestimoBolsa
            List<int> valores5 = EmprestimoBolsa.Values();
            foreach (int valor in valores5) {
                valoresPossiveis.Add(valor);
            }
            #endregion

            // Confere numero de elementos da lista
            int quantidade = valores.Count + valores1.Count + valores2.Count + valores3.Count +
                             valores4.Count + valores5.Count + 3;
            Assert.AreEqual(quantidade, valoresPossiveis.Count);
            return valoresPossiveis;
        }
    }

    /// <summary>
    /// Indica o que deve vir primeiro no metodo de casamento das operações
    /// oriundas de Exercicio de Opções e Liquidação de Termos
    /// </summary>
    public enum PrioridadeCasamentoVencimento {
        ExercicioPrimeiro = 1,
        TermoPrimeiro = 2    
    }

    /// <summary>
    /// Indica se a corretagem é calculada por faixas de volume (tabela) ou por $ fixo por operação
    /// </summary>
    public enum TipoCalculoCorretagem {
        ValorFixo = 1,
        Tabela = 2,
        QuantidadeAcoes = 3
    }

    /// <summary>
    /// Indica se o valor adicional da faixa de volume da tabela de corretagem deve 
    /// ser adicionado por Mercado Operado ou deve ser Rateado por cada Mercado
    /// </summary>
    public enum TipoCorretagemAdicional {
        PorMercado = 1,
        RateiaAcoesTermoFuturo = 2,
        RateiaAcoes = 3
    }

    public enum FonteProventoBolsa
    {
        Manual = 1,
        ArquivoPROD = 2,
        ArquivoCDIX = 3
    }

    public static class TipoProventoBolsa {
        public const int Dividendo = 10;
        public const int DividendoDistribuicao = 25;
        public const int RestituicaoCapital = 11;
        public const int BonificacaoDinheiro = 12;
        public const int JurosSobreCapital = 13;
        public const int Rendimento = 14;
        public const int Juros = 16;
        public const int Amortizacao = 17;
        public const int RestituicaoCapitalComReducaoAcoes = 22;
        public const int CreditoFracoesAcoes = 72;
        public const int RendimentoLiquido = 98;

        /// <summary>
        ///    Classe Usada para Retornar os textos das Constantes
        /// </summary>
        public static class str {

            /// <summary>
            /// Retorna os tipoProventoBolsa como string - Só traduz para português. Não foi feito suporte MultiLingua.
            /// </summary>
            /// <exception>throws ArgumentException se não existir um enum com o valor fornecido</exception> 
            /// <returns></returns>            
            public static string RetornaTexto(int tipoProventoBolsa) {
                string tipoProvento = "";

                switch (tipoProventoBolsa) {
                    case TipoProventoBolsa.Dividendo:
                        tipoProvento = "Dividendo";
                        break;
                    case TipoProventoBolsa.DividendoDistribuicao:
                        tipoProvento = "Dividendo Distribuição";
                        break;
                    case TipoProventoBolsa.RestituicaoCapital:
                        tipoProvento = "Restituição Capital";
                        break;
                    case TipoProventoBolsa.BonificacaoDinheiro:
                        tipoProvento = "Bonificação Dinheiro";
                        break;
                    case TipoProventoBolsa.JurosSobreCapital:
                        tipoProvento = "Juros Sobre Capital";
                        break;
                    case TipoProventoBolsa.Rendimento:
                        tipoProvento = "Rendimento";
                        break;
                    case TipoProventoBolsa.RestituicaoCapitalComReducaoAcoes:
                        tipoProvento = "Restituição Capital com Redução Ações";
                        break;
                    case TipoProventoBolsa.CreditoFracoesAcoes:
                        tipoProvento = "Crédito Frações Ações";
                        break;
                    case TipoProventoBolsa.RendimentoLiquido:
                        tipoProvento = "Rendimento Líquido";
                        break;
                    default:
                        throw new ArgumentException("TipoProventoBolsa não possue Constante com valor " + tipoProventoBolsa);
                }
                return tipoProvento;
            }
        }
    }

    public enum TipoGrupamentoBolsa
    {
        Grupamento = 1,
        Desdobramento = 2
    }

    /// <summary>
    /// LA - Liquidacao Antecipada
    /// LPD - Liquidacao Por Diferenca
    /// Decurso - Vencimento
    /// LPDE - Liquidacao Por Diferenca Especial
    /// </summary>
    public enum TipoLiquidacaoTermoBolsa
    {
        Antecipada = 1, //LA - Liquidacao Antecipada
        Diferenca = 2, //LPD - Liquidacao Por Diferenca
        Decurso = 3, //Decurso - Vencimento
        DiferencaEspecial = 4 //LPDE - Liquidacao Por Diferenca Especial
    }

    /// <summary>
    /// DiaAnterior - Cotação de D-1 na abertura do termo, Cotação de D-1 em relação à data atual.
    /// DiaAnterior_D0 - Cotação de D-1 na abertura do termo, Cotação de D0 na data atual.
    /// D0_DiaAnterior - Cotação de D0 na abertura do termo, Cotação de D-1 em relação à data atual.
    /// D0 - Cotação de D0 na abertura do termo, Cotação de D0 na data atual.
    /// </summary>
    public enum CotacaoCorrecaoTermo
    {
        DiaAnterior = 1,
        DiaAnterior_D0 = 2,        
        D0_DiaAnterior = 3,
        D0 = 4,
    }

    public enum FonteLiquidacaoTermoBolsa
    {
        Manual = 1,
        Interno = 2,
        ArquivoCONL = 3,
        Sinacor
    }

    public enum IndicadorTratamentoFracaoEvento
    {
        Trunca = 1,
        Arredonda = 2
    }

    public enum TipoOperacaoBloqueio
    {
        Bloqueio = 1,
        Desbloqueio = 2
    }

    /// <summary>
    /// Ordem de Entrada das Operações de acordo com a tabela Configuração
    /// </summary>
    public enum EntradaOperacao {
        Compras = 1,
        Vendas = 2
    }

    public enum TipoPapelAtivo
    {
        Normal = 1,
        BonusSubscricao = 2,
        ReciboSubscricao = 3,
        ETF = 4,
        CertificadoDepositoAcoes = 5,
        ReciboDepositoAcoes = 6,
        BDRNivelI = 7,
        BDRNivelII = 8,
        BDRNivelIII = 9,

    }

    public enum StatusOrdemBolsa
    {
        Digitado = 1,
        Aprovado = 2,
        Processado = 3,
        Cancelado = 4
    }

    public enum TipoEspecificacaoBolsa
    {
        Ordinaria = 1,
        Preferencial = 2,
        Unit = 3
    }

    public enum TipoTermoBolsa
    {
        Normal = 1,
        Dolar = 2,
        Pontos = 3,
        Flexivel = 4
    }

    public enum TipoCarteiraBovespa
    {
        Livre = 21016,
        GarantiasGerais = 23019,
        GarantiaBMF = 23906,
        TermosFlexiveis = 25018,
        CoberturaOpcoes = 27014,
        BTC_Tomado = 22012,
        BTC_Doado = 28010,
        ContaMargem = 21059
    }

    public enum TipoPosicaoBolsaFixo
    {
        Bolsa = 1,
        Emprestimo = 2,
        Termo = 3
    }
    
}
