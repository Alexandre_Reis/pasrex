﻿using System;

namespace Financial.Bolsa.Exceptions {            
    /// <summary>
    /// Classe base de Exceção do componente de Bolsa
    /// </summary>
    public class BolsaException: Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public BolsaException() {  }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public BolsaException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public BolsaException(string mensagem, Exception inner) : base(mensagem, inner)  {}
    }
    
    /// <summary>
    /// Exceção de CodigoClienteBovespaInvalido
    /// </summary>
    public class CodigoClienteBovespaInvalido: BolsaException {
         /// <summary>
        ///  Construtor
        /// </summary>
        public CodigoClienteBovespaInvalido() {  }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CodigoClienteBovespaInvalido(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de fatorInvalido
    /// </summary>
    public class FatorCotacaoNaoCadastradoException: BolsaException {
         /// <summary>
        ///  Construtor
        /// </summary>
        public FatorCotacaoNaoCadastradoException() {  }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public FatorCotacaoNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de AgenteMercadoCadastradoException
    /// </summary>
    public class AgenteMercadoNaoCadastradoException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public AgenteMercadoNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public AgenteMercadoNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de AtivoNaoCadastradoException
    /// </summary>
    public class AtivoNaoCadastradoException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public AtivoNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public AtivoNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ClienteNaoCadastradoException
    /// </summary>
    public class ClienteNaoCadastradoException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ClienteNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ClienteNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de CotacaoNaoCadastradaException
    /// </summary>
    public class CotacaoNaoCadastradaException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CotacaoNaoCadastradaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CotacaoNaoCadastradaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de TabelaCustosBolsa
    /// </summary>
    public class TabelaCustosBolsaNaoCadastradaException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public TabelaCustosBolsaNaoCadastradaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public TabelaCustosBolsaNaoCadastradaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de PerfilCorretagemInvalidaException
    /// </summary>
    public class PerfilCorretagemNaoCadastradoException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public PerfilCorretagemNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public PerfilCorretagemNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de CarteiraInvalida
    /// </summary>
    public class CarteiraCustodiaInvalidaException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CarteiraCustodiaInvalidaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CarteiraCustodiaInvalidaException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de QuantidadeCarteiraInsuficiente
    /// </summary>
    public class QuantidadeCarteiraInsuficienteException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public QuantidadeCarteiraInsuficienteException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public QuantidadeCarteiraInsuficienteException(string mensagem) : base(mensagem) { }
    }
        
    /// <summary>
    /// Exceção VencimentoOpcaoException
    /// </summary>
    public class VencimentoOpcaoException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public VencimentoOpcaoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public VencimentoOpcaoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção ContratoInexistenteException
    /// </summary>
    public class ContratoInexistenteException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ContratoInexistenteException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ContratoInexistenteException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de ClienteBolsa
    /// </summary>
    public class ClienteBolsaNaoCadastradoException : BolsaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public ClienteBolsaNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public ClienteBolsaNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de OrdemTermoBolsaInexistente
    /// </summary>
    public class OrdemTermoBolsaInexistente : BolsaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public OrdemTermoBolsaInexistente() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public OrdemTermoBolsaInexistente(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de OperacaoTermoBolsaInexistente
    /// </summary>
    public class OperacaoTermoBolsaInexistente : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public OperacaoTermoBolsaInexistente() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public OperacaoTermoBolsaInexistente(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de QuantidadeInsuficienteVendaTermo
    /// </summary>
    public class QuantidadeInsuficienteVendaTermo : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public QuantidadeInsuficienteVendaTermo() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public QuantidadeInsuficienteVendaTermo(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de AtivoObjetoNaoCadastradoException
    /// </summary>
    public class AtivoObjetoNaoCadastradoException : BolsaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public AtivoObjetoNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public AtivoObjetoNaoCadastradoException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de FormatoNotaCorretagemNaoSuportadoException
    /// </summary>
    public class FormatoNotaCorretagemNaoSuportadoException : BolsaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public FormatoNotaCorretagemNaoSuportadoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public FormatoNotaCorretagemNaoSuportadoException(string mensagem) : base(mensagem) { }
    }


    /// <summary>
    /// Exceção de CodigoIsinNaoCadastradoException
    /// </summary>
    public class CodigoIsinNaoCadastradoException : BolsaException {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CodigoIsinNaoCadastradoException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CodigoIsinNaoCadastradoException(string mensagem) : base(mensagem) { }
    }


    /// <summary>
    /// Exceção de DataReferenciaInvalida
    /// </summary>
    public class DataReferenciaInvalidaException : BolsaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public DataReferenciaInvalidaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public DataReferenciaInvalidaException(string mensagem) : base(mensagem) { }
    }
}


