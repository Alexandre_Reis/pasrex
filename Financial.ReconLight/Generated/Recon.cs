/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/03/2015 15:03:33
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.ReconLight
{

	[Serializable]
	abstract public class esReconCollection : esEntityCollection
	{
		public esReconCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ReconCollection";
		}

		#region Query Logic
		protected void InitQuery(esReconQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esReconQuery);
		}
		#endregion
		
		virtual public Recon DetachEntity(Recon entity)
		{
			return base.DetachEntity(entity) as Recon;
		}
		
		virtual public Recon AttachEntity(Recon entity)
		{
			return base.AttachEntity(entity) as Recon;
		}
		
		virtual public void Combine(ReconCollection collection)
		{
			base.Combine(collection);
		}
		
		new public Recon this[int index]
		{
			get
			{
				return base[index] as Recon;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(Recon);
		}
	}



	[Serializable]
	abstract public class esRecon : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esReconQuery GetDynamicQuery()
		{
			return null;
		}

		public esRecon()
		{

		}

		public esRecon(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idRecon)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRecon);
			else
				return LoadByPrimaryKeyStoredProcedure(idRecon);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idRecon)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRecon);
			else
				return LoadByPrimaryKeyStoredProcedure(idRecon);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idRecon)
		{
			esReconQuery query = this.GetDynamicQuery();
			query.Where(query.IdRecon == idRecon);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idRecon)
		{
			esParameters parms = new esParameters();
			parms.Add("IdRecon",idRecon);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdRecon": this.str.IdRecon = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "DataInicioProcesso": this.str.DataInicioProcesso = (string)value; break;							
						case "DataFimProcesso": this.str.DataFimProcesso = (string)value; break;							
						case "IdUsuario": this.str.IdUsuario = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "DataCancelamento": this.str.DataCancelamento = (string)value; break;							
						case "Status": this.str.Status = (string)value; break;							
						case "MessageId": this.str.MessageId = (string)value; break;
                        case "Tipo": this.str.Tipo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdRecon":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdRecon = (System.Int32?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "DataInicioProcesso":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioProcesso = (System.DateTime?)value;
							break;
						
						case "DataFimProcesso":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFimProcesso = (System.DateTime?)value;
							break;
						
						case "IdUsuario":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdUsuario = (System.Int32?)value;
							break;
						
						case "DataCancelamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataCancelamento = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to Recon.IdRecon
		/// </summary>
		virtual public System.Int32? IdRecon
		{
			get
			{
				return base.GetSystemInt32(ReconMetadata.ColumnNames.IdRecon);
			}
			
			set
			{
				base.SetSystemInt32(ReconMetadata.ColumnNames.IdRecon, value);
			}
		}
		
		/// <summary>
		/// Maps to Recon.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(ReconMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(ReconMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to Recon.DataInicioProcesso
		/// </summary>
		virtual public System.DateTime? DataInicioProcesso
		{
			get
			{
				return base.GetSystemDateTime(ReconMetadata.ColumnNames.DataInicioProcesso);
			}
			
			set
			{
				base.SetSystemDateTime(ReconMetadata.ColumnNames.DataInicioProcesso, value);
			}
		}
		
		/// <summary>
		/// Maps to Recon.DataFimProcesso
		/// </summary>
		virtual public System.DateTime? DataFimProcesso
		{
			get
			{
				return base.GetSystemDateTime(ReconMetadata.ColumnNames.DataFimProcesso);
			}
			
			set
			{
				base.SetSystemDateTime(ReconMetadata.ColumnNames.DataFimProcesso, value);
			}
		}
		
		/// <summary>
		/// Maps to Recon.IdUsuario
		/// </summary>
		virtual public System.Int32? IdUsuario
		{
			get
			{
				return base.GetSystemInt32(ReconMetadata.ColumnNames.IdUsuario);
			}
			
			set
			{
				base.SetSystemInt32(ReconMetadata.ColumnNames.IdUsuario, value);
			}
		}
		
		/// <summary>
		/// Maps to Recon.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(ReconMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(ReconMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to Recon.DataCancelamento
		/// </summary>
		virtual public System.DateTime? DataCancelamento
		{
			get
			{
				return base.GetSystemDateTime(ReconMetadata.ColumnNames.DataCancelamento);
			}
			
			set
			{
				base.SetSystemDateTime(ReconMetadata.ColumnNames.DataCancelamento, value);
			}
		}
		
		/// <summary>
		/// Maps to Recon.Status
		/// </summary>
		virtual public System.String Status
		{
			get
			{
				return base.GetSystemString(ReconMetadata.ColumnNames.Status);
			}
			
			set
			{
				base.SetSystemString(ReconMetadata.ColumnNames.Status, value);
			}
		}
		
		/// <summary>
		/// Maps to Recon.MessageId
		/// </summary>
		virtual public System.String MessageId
		{
			get
			{
				return base.GetSystemString(ReconMetadata.ColumnNames.MessageId);
			}
			
			set
			{
				base.SetSystemString(ReconMetadata.ColumnNames.MessageId, value);
			}
		}

        /// <summary>
        /// Maps to Recon.Tipo
        /// </summary>
        virtual public System.String Tipo
        {
            get
            {
                return base.GetSystemString(ReconMetadata.ColumnNames.Tipo);
            }

            set
            {
                base.SetSystemString(ReconMetadata.ColumnNames.Tipo, value);
            }
        }
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esRecon entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdRecon
			{
				get
				{
					System.Int32? data = entity.IdRecon;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRecon = null;
					else entity.IdRecon = Convert.ToInt32(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataInicioProcesso
			{
				get
				{
					System.DateTime? data = entity.DataInicioProcesso;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioProcesso = null;
					else entity.DataInicioProcesso = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataFimProcesso
			{
				get
				{
					System.DateTime? data = entity.DataFimProcesso;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFimProcesso = null;
					else entity.DataFimProcesso = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdUsuario
			{
				get
				{
					System.Int32? data = entity.IdUsuario;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdUsuario = null;
					else entity.IdUsuario = Convert.ToInt32(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String DataCancelamento
			{
				get
				{
					System.DateTime? data = entity.DataCancelamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataCancelamento = null;
					else entity.DataCancelamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Status
			{
				get
				{
					System.String data = entity.Status;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Status = null;
					else entity.Status = Convert.ToString(value);
				}
			}
				
			public System.String MessageId
			{
				get
				{
					System.String data = entity.MessageId;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.MessageId = null;
					else entity.MessageId = Convert.ToString(value);
				}
			}

            public System.String Tipo
            {
                get
                {
                    System.String data = entity.Tipo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Tipo = null;
                    else entity.Tipo = Convert.ToString(value);
                }
            }

			private esRecon entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esReconQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esRecon can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class Recon : esRecon
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esReconQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ReconMetadata.Meta();
			}
		}	
		

		public esQueryItem IdRecon
		{
			get
			{
				return new esQueryItem(this, ReconMetadata.ColumnNames.IdRecon, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, ReconMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataInicioProcesso
		{
			get
			{
				return new esQueryItem(this, ReconMetadata.ColumnNames.DataInicioProcesso, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataFimProcesso
		{
			get
			{
				return new esQueryItem(this, ReconMetadata.ColumnNames.DataFimProcesso, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdUsuario
		{
			get
			{
				return new esQueryItem(this, ReconMetadata.ColumnNames.IdUsuario, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, ReconMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem DataCancelamento
		{
			get
			{
				return new esQueryItem(this, ReconMetadata.ColumnNames.DataCancelamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Status
		{
			get
			{
				return new esQueryItem(this, ReconMetadata.ColumnNames.Status, esSystemType.String);
			}
		} 
		
		public esQueryItem MessageId
		{
			get
			{
				return new esQueryItem(this, ReconMetadata.ColumnNames.MessageId, esSystemType.String);
			}
		}

        public esQueryItem Tipo
        {
            get
            {
                return new esQueryItem(this, ReconMetadata.ColumnNames.Tipo, esSystemType.String);
            }
        } 
		
	}



	[Serializable]
	[XmlType("ReconCollection")]
	public partial class ReconCollection : esReconCollection, IEnumerable<Recon>
	{
		public ReconCollection()
		{

		}
		
		public static implicit operator List<Recon>(ReconCollection coll)
		{
			List<Recon> list = new List<Recon>();
			
			foreach (Recon emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ReconMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ReconQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new Recon(row);
		}

		override protected esEntity CreateEntity()
		{
			return new Recon();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ReconQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ReconQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ReconQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public Recon AddNew()
		{
			Recon entity = base.AddNewEntity() as Recon;
			
			return entity;
		}

		public Recon FindByPrimaryKey(System.Int32 idRecon)
		{
			return base.FindByPrimaryKey(idRecon) as Recon;
		}


		#region IEnumerable<Recon> Members

		IEnumerator<Recon> IEnumerable<Recon>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as Recon;
			}
		}

		#endregion
		
		private ReconQuery query;
	}


	/// <summary>
	/// Encapsulates the 'Recon' table
	/// </summary>

	[Serializable]
	public partial class Recon : esRecon
	{
		public Recon()
		{

		}
	
		public Recon(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ReconMetadata.Meta();
			}
		}
		
		
		
		override protected esReconQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ReconQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ReconQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ReconQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ReconQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ReconQuery query;
	}



	[Serializable]
	public partial class ReconQuery : esReconQuery
	{
		public ReconQuery()
		{

		}		
		
		public ReconQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ReconMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ReconMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ReconMetadata.ColumnNames.IdRecon, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ReconMetadata.PropertyNames.IdRecon;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ReconMetadata.ColumnNames.DataRegistro, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ReconMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ReconMetadata.ColumnNames.DataInicioProcesso, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ReconMetadata.PropertyNames.DataInicioProcesso;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ReconMetadata.ColumnNames.DataFimProcesso, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ReconMetadata.PropertyNames.DataFimProcesso;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ReconMetadata.ColumnNames.IdUsuario, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ReconMetadata.PropertyNames.IdUsuario;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ReconMetadata.ColumnNames.Observacao, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = ReconMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ReconMetadata.ColumnNames.DataCancelamento, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ReconMetadata.PropertyNames.DataCancelamento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ReconMetadata.ColumnNames.Status, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = ReconMetadata.PropertyNames.Status;
			c.CharacterMaxLength = 8000;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ReconMetadata.ColumnNames.MessageId, 8, typeof(System.String), esSystemType.String);
			c.PropertyName = ReconMetadata.PropertyNames.MessageId;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c);

            c = new esColumnMetadata(ReconMetadata.ColumnNames.Tipo, 9, typeof(System.String), esSystemType.String);
            c.PropertyName = ReconMetadata.PropertyNames.Tipo;
            c.CharacterMaxLength = 50;
            c.NumericPrecision = 0;
            c.HasDefault = true;
            c.Default = @"('RECON')";
            _columns.Add(c); 
				
		}
		#endregion
	
		static public ReconMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdRecon = "IdRecon";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataInicioProcesso = "DataInicioProcesso";
			 public const string DataFimProcesso = "DataFimProcesso";
			 public const string IdUsuario = "IdUsuario";
			 public const string Observacao = "Observacao";
			 public const string DataCancelamento = "DataCancelamento";
			 public const string Status = "Status";
			 public const string MessageId = "MessageId";
             public const string Tipo = "Tipo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdRecon = "IdRecon";
			 public const string DataRegistro = "DataRegistro";
			 public const string DataInicioProcesso = "DataInicioProcesso";
			 public const string DataFimProcesso = "DataFimProcesso";
			 public const string IdUsuario = "IdUsuario";
			 public const string Observacao = "Observacao";
			 public const string DataCancelamento = "DataCancelamento";
			 public const string Status = "Status";
			 public const string MessageId = "MessageId";
             public const string Tipo = "Tipo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ReconMetadata))
			{
				if(ReconMetadata.mapDelegates == null)
				{
					ReconMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ReconMetadata.meta == null)
				{
					ReconMetadata.meta = new ReconMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdRecon", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataInicioProcesso", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataFimProcesso", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdUsuario", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataCancelamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Status", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("MessageId", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("Tipo", new esTypeMap("varchar", "System.String"));		

				
				
				meta.Source = "Recon";
				meta.Destination = "Recon";
				
				meta.spInsert = "proc_ReconInsert";				
				meta.spUpdate = "proc_ReconUpdate";		
				meta.spDelete = "proc_ReconDelete";
				meta.spLoadAll = "proc_ReconLoadAll";
				meta.spLoadByPrimaryKey = "proc_ReconLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ReconMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
