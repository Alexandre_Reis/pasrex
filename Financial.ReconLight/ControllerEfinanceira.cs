﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atlas.Link;
using Atlas.Link.Client;
using Atlas.Link.Model;
using System.IO;
using System.Net;
using Atlas.Link.Settings;
using Link.ViewModel;
using DevExpress.Spreadsheet;

namespace Financial.ReconLight
{
    /// <summary>
    /// ControllerEfinanceira
    /// </summary>
    public class ControllerEfinanceira
    {
        #region Fields
        private AtlasLinkSection _config;
        protected static Common.Logging.ILog Logger;
        #endregion

        #region Constructor
        public ControllerEfinanceira()
        {
            _config = (AtlasLinkSection)System.Configuration.ConfigurationManager.GetSection("atlasLinkGroup/atlasLink");

            ServiceLocator.Register(typeof(Common.Logging.ILoggerFactoryAdapter), typeof(Atlas.Link.Logging.AtlasNLogFactory));
            ServiceLocator.Register(typeof(ISettingsStorageProvider), typeof(Atlas.Link.Settings.FileSettingsProvider));
            ServiceLocator.Register(typeof(Atlas.Link.Net.IConnectionProvider), typeof(Atlas.Link.Net.ConfigConnectionProvider));
            ServiceLocator.Register(typeof(Atlas.Link.Cache.IDataCache), typeof(Atlas.Link.Cache.RestServerCache));
            ServiceLocator.Register(typeof(IApplicationSettings), typeof(Atlas.Presentation.Settings.ApplicationSettingsProvider));

            Logger = ServiceLocator.Get<Common.Logging.ILoggerFactoryAdapter>().GetLogger(typeof(ControllerEfinanceira));

            Logger.Debug("Nova Instancia ControllerEfinanceira criada...");

        }
        #endregion

        #region Methods
        /// <summary>
        /// XMLs the cliente.
        /// </summary>
        /// <param name="idRecon">The identifier recon.</param>
        /// <returns></returns>
        public String XmlCliente(Int32 idRecon)
        {
            Logger.Debug("executando XmlCliente...");

            if (string.IsNullOrWhiteSpace(_config.Efinanceira.LocalFolder))
            {
                throw new Exception("Parametro diretorio local xml nao encontrado");
            }
            if (string.IsNullOrWhiteSpace(_config.Efinanceira.SufixoCliente))
            {
                throw new Exception("Parametro sufixo do cliente nao encontrado");
            }
            var path = _config.Efinanceira.LocalFolder;
            var fileName = idRecon.ToString("#000000000") + _config.Efinanceira.SufixoCliente;

            Logger.Debug("XmlCliente executada!");

            return Path.Combine(path, fileName);

        }

        /// <summary>
        /// XMLs the pas.
        /// </summary>
        /// <param name="idRecon">The identifier recon.</param>
        /// <returns></returns>
        public String XmlPas(Int32 idRecon)
        {
            Logger.Debug("executando XmlPas...");

            if (string.IsNullOrWhiteSpace(_config.Efinanceira.LocalFolder))
            {
                throw new Exception("Parametro diretorio local xml nao encontrado");
            }
            if (string.IsNullOrWhiteSpace(_config.Efinanceira.SufixoPas))
            {
                throw new Exception("Parametro sufixo do PAS nao encontrado");
            }

            var path = _config.Efinanceira.LocalFolder;
            var fileName = idRecon.ToString("#000000000") + _config.Efinanceira.SufixoPas;

            Logger.Debug("XmlPas executada!");

            return Path.Combine(path, fileName);
        }

        /// <summary>
        /// XLSs the resultado.
        /// </summary>
        /// <param name="idRecon">The identifier recon.</param>
        /// <returns></returns>
        public String XlsResultado(Int32 idRecon)
        {
            Logger.Debug("executando XlsResultado...");

            if (string.IsNullOrWhiteSpace(_config.Efinanceira.LocalFolder))
            {
                throw new Exception("Parametro diretorio local xml nao encontrado");
            }
            if (string.IsNullOrWhiteSpace(_config.Efinanceira.SufixoResultado))
            {
                throw new Exception("Parametro sufixo do resultado nao encontrado");
            }

            var path = _config.Efinanceira.LocalFolder;

            var fileName = idRecon.ToString("#000000000") + _config.Efinanceira.SufixoResultado;

            Logger.Debug("XmlPas executada!");

            return Path.Combine(path, fileName);
        }

        public bool Atualiza()
        {
            Logger.Debug("executando Atualiza...");

            //verifica se não existe nenhum recon sendo executado no momento
            var reconQuery = new ReconQuery();
            reconQuery.Where(reconQuery.DataFimProcesso.IsNull(), reconQuery.DataInicioProcesso.IsNotNull(), reconQuery.DataCancelamento.IsNull());
            reconQuery.Where(reconQuery.Tipo == Atlas.Link.Model.Enums.Tipo.EFINANCEIRA);
            var coll = new ReconCollection();
            coll.Load(reconQuery);

            //caso não exista nenhum processo sendo executado
            if (coll.Count == 0)
                return Processa();

            Verifica();

            Logger.Debug("Atualiza executada!");

            return false;
        }

        /// <summary>
        /// Processas this instance.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Nao foi possivel a definicao do nome da rota de reconciliacao.\nSolicite ao administrador para verificar a chave NomeRotaReconciliacao no web.config
        /// or
        /// </exception>
        public bool Processa()
        {
            Logger.Debug("executando Processa...");

            //indica se o processo foi iniciado com sucesso
            var retorno = false;

            var reconQuery = new ReconQuery();
            reconQuery.Where(reconQuery.DataInicioProcesso.IsNull(), reconQuery.DataFimProcesso.IsNull(), reconQuery.DataCancelamento.IsNull());
            reconQuery.Where(reconQuery.Tipo == Atlas.Link.Model.Enums.Tipo.EFINANCEIRA);

            //reconQuery.OrderBy(reconQuery.IdRecon);
            var coll = new ReconCollection();
            coll.Load(reconQuery);

            try
            {
                //caso existam processos na fila executar o primeiro ou seja o que possui o menor id
                if (coll.Count > 0)
                {
                    var rota = GetTransformationRoute();

                    // inicia processo de reconciliacao no link server
                    var response = new Atlas.Link.Client.RouteClient().Trigger(rota.ID);

                    if (response.ResponseStatus == ResponseType.OK)
                    {
                        coll[0].DataInicioProcesso = DateTime.Now;
                        retorno = true;
                    }
                    else
                    {
                        var msg = "Erro ao executar trigger de eFinanceira(ResponseStatus Error): " + response.Text;
                        Logger.Error(msg);
                        coll[0].AtualizaStatus(msg, DateTime.Now);
                    }

                    coll.Save();
                }
            }
            catch (Exception ex)
            {
                coll[0].DataInicioProcesso = null;
                var msg = string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n", "Erro ao iniciar processamento: ", ex.Message, ex.StackTrace, ex.InnerException == null ? null : ex.InnerException, ex.InnerException == null ? null : ex.InnerException.StackTrace);
                coll[0].AtualizaStatus(msg, DateTime.Now);
                Logger.Error(msg);
                coll.Save();
                return false;
            }

            Verifica();

            Logger.Debug("Processa executada!");

            return retorno;
        }


        /// <summary>
        /// Verificas this instance.
        /// </summary>
        public void Verifica()
        {
            Logger.Debug("executando Verifica...");

            try
            {
                var rota = GetTransformationRoute();

                var dt = DateTime.Today;

                var messageClient = new Atlas.Link.Client.MessageClient();
                var messages = messageClient.GetMessagesFrom(dt);

                var messageList = messages.Where(x => x.Route.ID == rota.ID).Select(message => new ReconciliationMessage
                {
                    CreatedAt = message.CreatedAt,
                    UpdatedAt = message.UpdatedAt,
                    RouteId = message.Route.ID,
                    MessageStatus = message.MessageStatus,
                    LastDuration = 0,
                    MessageId = message.ID
                }).ToList();

                var reconQuery = new ReconQuery();
                reconQuery.Where(reconQuery.DataInicioProcesso.IsNotNull(), reconQuery.DataFimProcesso.IsNull(), reconQuery.DataCancelamento.IsNull());
                reconQuery.Where(reconQuery.Tipo == Atlas.Link.Model.Enums.Tipo.EFINANCEIRA);
                reconQuery.OrderBy(reconQuery.IdRecon.Ascending);
                var coll = new ReconCollection();
                coll.Load(reconQuery);

                if (coll.Count == 1)
                {
                    var queryMessageId = new ReconQuery();

                    reconQuery.Where(queryMessageId.MessageId.In(messageList.ConvertAll(delegate(ReconciliationMessage msg) { return msg.MessageId; })));
                    var reconsFiltradas = new ReconCollection();
                    reconsFiltradas.Load(queryMessageId);
                    var recons = new List<Recon>(reconsFiltradas);

                    //filtra apenas as mensagens que vierem depois do inicio do processo e que não estejam registradas a nenhum processo anterior

                    var msgs = messageList.FindAll(delegate(ReconciliationMessage msg) { return (recons.Find(delegate(Recon r) { return (r.MessageId == msg.MessageId) || r.DataInicioProcesso > msg.CreatedAt; }) == null); });

                    //order by desc
                    msgs.Sort(delegate(ReconciliationMessage msg1, ReconciliationMessage msg2)
                    {
                        if (msg1.CreatedAt == msg2.CreatedAt) return 0;
                        if (msg1.CreatedAt > msg2.CreatedAt) return -1;
                        return 1;
                    });

                    var timeoutSemMensagens = _config.Efinanceira.TimeoutSemMensagens;

                    if (msgs.Count == 0)
                    {
                        if (DateTime.Now - coll[0].DataInicioProcesso > TimeSpan.FromMinutes(timeoutSemMensagens))
                        {
                            coll[0].Cancela("Serviço de efinanceira não responde. Tempo de resposta(" + timeoutSemMensagens + " minutos) excedido.");
                            coll.Save();
                            return;
                        }
                        coll[0].AtualizaStatus("Aguardando processamento.", DateTime.Now);
                        coll.Save();
                        return;
                    }

                    if (msgs.Count > 0)
                    {
                        //caso a rotina tenha sido executada com sucesso salva o xls e encerra a rotina
                        if (msgs[0].MessageStatus == Enums.MessageStatus.SUCCESS || msgs[0].MessageStatus == Enums.MessageStatus.WARNING)
                        {
                            if (msgs[0].MessageStatus == Enums.MessageStatus.WARNING)
                                coll[0].AtualizaStatus("Warning Status.", DateTime.Now);
                            SalvaResultadoRecon(coll[0], msgs[0].MessageId);
                            coll.Save();
                            return;
                        }

                        //caso esteja pendente atualiza o status 
                        if (msgs[0].MessageStatus == Enums.MessageStatus.PENDING)
                        {
                            coll[0].AtualizaStatus("Pendente no servidor", DateTime.Now);
                            coll.Save();
                            return;
                        }

                        var timeoutPendingRecon = _config.Efinanceira.TimeoutPending;

                        //timeout caso demore muito tempo em pending antes de terminar a rotina cancela
                        if (msgs[0].MessageStatus == Enums.MessageStatus.PENDING && msgs[0].CreatedAt - coll[0].DataInicioProcesso.Value > TimeSpan.FromMinutes(timeoutPendingRecon))
                        {
                            coll[0].Cancela("Tempo limite(" + timeoutPendingRecon + " minutos) excedido.");
                            coll.Save();
                            return;
                        }

                        //caso de erro atualiza o status e cancela a recon
                        if (msgs[0].MessageStatus == Enums.MessageStatus.ERROR)
                        {
                            coll[0].Cancela("Erro interno do serviço de Recon.");
                            coll.Save();
                        }
                    }

                }
                else if (coll.Count > 1)
                {
                    var s = new StringBuilder();
                    foreach (var r in coll)
                    {
                        s.Append(r.IdRecon + ",");
                    }

                    s.Remove(s.Length - 1, s.Length);

                    CancelaTodos("As recons: " + s + " estavam rodando simultaneamente.");
                }
            }
            catch (Exception ex)
            {
                CancelaTodos(ex.Message);
            }

            Logger.Debug("Verifica executada!");

        }

        /// <summary>
        /// Copia os arquivos para o PAS
        /// </summary>
        /// <param name="r">The r.</param>
        /// <param name="messageId">The message identifier.</param>
        /// <exception cref="System.Exception"></exception>
        public void SalvaResultadoRecon(Recon r, String messageId)
        {
            Logger.Debug("executando SalvaResultadoRecon...");

            try
            {
                var rota = GetTransformationRoute();

                var sourceFileAname = GetEndpointAttribute(rota.TransformerBusinessRules[0].OutComponents[0], "fileName");
                var sourceFileApath = GetEndpointAttribute(rota.TransformerBusinessRules[0].OutComponents[0], "fileLocation");

                var sourceFile = Path.Combine(sourceFileApath, sourceFileAname);

                var fileInfo = new FileInfo(sourceFile);

                if (!fileInfo.Exists)
                {
                    throw new Exception("Nao foi possivel encontrar o arquivo de resultado " + sourceFile);
                }

                var targetFile = XlsResultado(r.IdRecon.Value);

                File.Copy(sourceFile, targetFile);

                r.MessageId = messageId;

                r.FinalizaProcesso();
            }
            catch (Exception ex)
            {
                r.AtualizaStatus("Falha ao salvar resultado da eFinanceira: " + ex.Message, DateTime.Now);
            }

            Logger.Debug("SalvaResultadoRecon executada!");

        }

        //cancela qualquer atividade de recon que estiver em processamento
        /// <summary>
        /// Cancelas the todos.
        /// </summary>
        /// <param name="motivo">The motivo.</param>
        public void CancelaTodos(string motivo)
        {
            Logger.Debug("executando CancelaTodos...");

            var reconQuery = new ReconQuery();
            reconQuery.Where(reconQuery.DataFimProcesso.IsNull(), reconQuery.DataInicioProcesso.IsNotNull());
            reconQuery.Where(reconQuery.Tipo == Atlas.Link.Model.Enums.Tipo.EFINANCEIRA);
            var coll = new ReconCollection();
            coll.Load(reconQuery);

            foreach (var r in coll)
            {
                r.Cancela(motivo);
            }

            coll.Save();

            Logger.Debug("CancelaTodos executada!");

        }
        //     <add key="ReconSufixoResultado" value="-Resultado.xls"/>
        //<add key="ReconSufixoCliente" value="-Cliente.xml"/>
        //<add key="ReconSufixoPas" value="-Pas.xml"/>
        //<add key="LocalRecon" value="C:/temp/"/>
        /// <summary>
        /// Limpars the recons.
        /// </summary>
        public void LimparRecons()
        {
            Logger.Debug("executando LimparRecons...");

            var coll = new ReconCollection();
            var reconQuery = new ReconQuery();
            reconQuery.Where(reconQuery.Tipo == Atlas.Link.Model.Enums.Tipo.EFINANCEIRA);
            coll.Load(reconQuery);


            foreach (var r in coll)
            {
                var fileInfo = new FileInfo(XlsResultado(r.IdRecon.Value));
                if (!fileInfo.Exists)
                    r.Cancela("Não foi encontrado o arquivo resultado.");

                fileInfo = new FileInfo(XmlCliente(r.IdRecon.Value));
                if (!fileInfo.Exists)
                    r.Cancela("Não foi encontrado o arquivo cliente.");

                fileInfo = new FileInfo(XmlPas(r.IdRecon.Value));
                if (!fileInfo.Exists)
                    r.Cancela("Não foi encontrado o arquivo Pas.");
            }

            coll.Save();

            Logger.Debug("LimparRecons executada!");

        }

        public static void CopyStream(Stream input, Stream output)
        {
            var buffer = new byte[16 * 1024];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }
        #endregion

        #region Utility Methods
        /// <summary>
        /// Gets the endpoint attribute.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="attribute">The attribute.</param>
        /// <returns></returns>
        private string GetEndpointAttribute(Endpoint endpoint, string attribute)
        {
            var attributeList = EndpointAttributeList.Deserialize(endpoint.Attributes);

            if (attributeList != null)
            {
                var firstOrDefault = attributeList.FirstOrDefault(x => x.Name == attribute);
                if (firstOrDefault != null)
                    return firstOrDefault.Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the reconciliation route.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Nao foi possivel a definicao do nome da rota de reconciliacao.\nSolicite ao administrador para verificar a chave NomeRotaReconciliacao no web.config
        /// or
        /// </exception>
        private Atlas.Link.Model.Route GetTransformationRoute()
        {
            Logger.Debug("executando GetTransformationRoute...");

            if (string.IsNullOrWhiteSpace(_config.Connection.HostName))
            {
                throw new Exception("HostName do atlas link server nao encontrado. Solicite ao administrador verificar no web.config <atlasLinkGroup> <atlasLink>, tag <connection>");
            }

            Logger.Debug(string.Format("HostName {0}", _config.Connection.HostName));

            if (_config.Connection.HostPort == 0)
            {
                throw new Exception("HostPort do atlas link server nao encontrado. Solicite ao administrador verificar no web.config <atlasLinkGroup> <atlasLink>, tag <connection>");
            }
            Logger.Debug(string.Format("HostPort {0}", _config.Connection.HostPort));

            var password = Base64Decode(_config.Credentials.Password);

            var request = new LogonRequest { UserName = _config.Credentials.UserName, Password = password };
            var xxx = new LogonClient();
            var result = xxx.LogonUser(request);

            var client = new Atlas.Link.Client.RouteClient();

            var routeName = _config.Efinanceira.NomeRota;
            if (string.IsNullOrWhiteSpace(routeName))
            {
                throw new Exception("Nao foi possivel obter a definicao do nome da rota de eFinanceira.\nSolicite ao administrador para verificar a chave NomeRota no web.config");
            }

            // busca a rota de reconciliacao

            var rota = client.GetAllRoutesForType(Enums.RouteType.ATLAS_LINK).FirstOrDefault(x => x.Category == routeName);

            if (rota == null)
            {
                throw new Exception(string.Format("Nao foi possivel encontrar rota de reconciliacao {0}", routeName));
            }

            Logger.Debug("GetTransformationRoute executada!");

            return rota;
        }

        /// <summary>
        /// Generates the work sheet.
        /// </summary>
        /// <param name="workbook">The workbook.</param>
        /// <param name="description">The description.</param>
        /// <param name="rows">The rows.</param>
        private void GenerateWorkSheet(IWorkbook workbook, string description, object rows)
        {
            Logger.Debug("executando GenerateWorkSheet...");

            var worksheet = workbook.Worksheets.Add();
            worksheet.Name = description;

            List<DynamicFieldDescriptor> fieldDescriptors;
            BaseDynamicViewModel reconRows;

            if (rows is ReconResultDiffDataViewModel)
            {
                fieldDescriptors = (rows as ReconResultDiffDataViewModel).MetaData.FieldDescriptors;
                reconRows = (rows as ReconResultDiffDataViewModel);
            }
            else
            {
                fieldDescriptors = (rows as ReconResultErrorDataViewModel).MetaData.FieldDescriptors;
                reconRows = (rows as ReconResultErrorDataViewModel);
            }


            // header
            int i = 0;
            foreach (var dynamicFieldDescriptor in fieldDescriptors)
            {
                worksheet[0, i].Value = dynamicFieldDescriptor.DisplayName;
                worksheet[0, i].Font.FontStyle = SpreadsheetFontStyle.Bold;
                worksheet[0, i].FillColor = System.Drawing.Color.Khaki;
                worksheet.Columns[i].Width = 400;
                i++;
            }

            // data
            i = 1;
            foreach (var matchedRow in reconRows)
            {
                var j = 0;
                foreach (var dynamicFieldDescriptor in fieldDescriptors)
                {
                    worksheet[i, j].Value = matchedRow.GetFieldValue(dynamicFieldDescriptor.Name).ToString();
                    j++;
                }
                i++;
            }

            Logger.Debug("GenerateWorkSheet executada!");

        }
        #endregion

        #region classes
        /// <summary>
        /// 
        /// </summary>
        public class ReconciliationMessage
        {
            /// <summary>
            /// Gets or sets the message status.
            /// </summary>
            /// <value>
            /// The message status.
            /// </value>
            public Enums.MessageStatus MessageStatus { get; set; }

            /// <summary>
            /// Gets or sets the route identifier.
            /// </summary>
            /// <value>
            /// The route identifier.
            /// </value>
            public string RouteId { get; set; }

            /// <summary>
            /// Gets or sets the created at.
            /// </summary>
            /// <value>
            /// The created at.
            /// </value>
            public DateTime CreatedAt { get; set; }

            /// <summary>
            /// Gets or sets the updated at.
            /// </summary>
            /// <value>
            /// The updated at.
            /// </value>
            public DateTime UpdatedAt { get; set; }

            /// <summary>
            /// Gets or sets the last duration.
            /// </summary>
            /// <value>
            /// The last duration.
            /// </value>
            public int LastDuration { get; set; }

            /// <summary>
            /// Gets or sets the message identifier.
            /// </summary>
            /// <value>
            /// The message identifier.
            /// </value>
            public string MessageId { get; set; }
        }

        #endregion

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
