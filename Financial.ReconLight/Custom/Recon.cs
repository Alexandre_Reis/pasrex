/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 17/03/2015 17:30:50
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.ReconLight
{
	public partial class Recon : esRecon
	{

        public void Cancela(String mensagem)
        {
            this.DataCancelamento = DateTime.Now;
            this.AtualizaStatus("Recon cancelado: " + mensagem, this.DataCancelamento.Value);
        }

        public void AtualizaStatus(String mensagem, DateTime dataOcorrencia)
        {
            if (Status != null && Status.Length > 7500)
            {
                this.Status += "[" + dataOcorrencia + "] - Recon cancelado: numero maximo de tentativas excedido" ;
                this.DataCancelamento = DateTime.Now;
                return;
            }
            if (!string.IsNullOrEmpty(this.Status))
                this.Status += Environment.NewLine;

            this.Status += "[" + dataOcorrencia + "] - " + mensagem;
        }

        public void FinalizaProcesso()
        {   
            this.DataFimProcesso = DateTime.Now;
            AtualizaStatus("Processo finalizado.", this.DataFimProcesso.Value);
        }

        
    }
}
