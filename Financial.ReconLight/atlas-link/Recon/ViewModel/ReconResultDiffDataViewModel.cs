﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atlas.Link;
using Atlas.Link.Model;
using Atlas.Link.Model.Recon;
using Atlas.Recon.Model;

namespace Link.ViewModel
{
    public class ReconResultDiffDataViewModel : BaseDynamicViewModel
    {
        #region Constructors

        public ReconResultDiffDataViewModel(Reconciliation reconRequest, ReconResultFileViewModel leftFile, ReconResultFileViewModel rightFile, IEnumerable<ReconciliationRow> reconRows, Enums.ReconciliationCategory category)
        {
            
            if (leftFile.FileIndex.FieldNames.Count == 0)
                throw new Exception("Left File does not contain field headers");
            if (rightFile.FileIndex.FieldNames.Count == 0)
                throw new Exception("Right File does not contain field headers");

            foreach (var keyField in reconRequest.KeyFields)
            {
                var leftName = keyField.LeftField.Name;
                var leftColNbr = leftFile.FileIndex.FieldNames.IndexWhere(x => x.Value.Equals(leftName, StringComparison.InvariantCultureIgnoreCase));

                var rightName = keyField.RightField.Name;
                var rightColNbr = rightFile.FileIndex.FieldNames.IndexWhere(x => x.Value.Equals(rightName, StringComparison.InvariantCultureIgnoreCase));

                if (leftColNbr != rightColNbr)
                    throw new Exception("Key Field index mismatch for field: " + leftName);

                var colName = String.Format("key.{0}", leftName);
                MetaData.FieldDescriptors.Add(new ReconResultEntityCol(colName, leftName, leftColNbr, ColumnType.Key));
            }

            foreach (var cmpField in reconRequest.CompFields)
            {
                var leftName = cmpField.LeftField.Name;
                var leftColNbr = leftFile.FileIndex.FieldNames.IndexWhere(x => x.Value.Equals(leftName, StringComparison.InvariantCultureIgnoreCase));

                var rightName = cmpField.RightField.Name;
                var rightColNbr = rightFile.FileIndex.FieldNames.IndexWhere(x => x.Value.Equals(rightName, StringComparison.InvariantCultureIgnoreCase));

                if (leftColNbr != rightColNbr)
                    throw new Exception("Comp Field index mismatch for field: " + leftName);

                var colName = String.Format("cmp.{0}", leftName);
                var colDesc = leftName;
                MetaData.FieldDescriptors.Add(new ReconResultEntityCol(colName, colDesc, leftColNbr, ColumnType.Comp));
            }

            foreach (var row in reconRows.Where(x => x.Category == category))
            {
                var unmatchedColumns = String.IsNullOrWhiteSpace(row.UnmatchedColumns) ? null : row.UnmatchedColumns.Split(',').Select(x => Convert.ToInt32(x) - 1).ToArray();
                var matchedByVarColumns = String.IsNullOrWhiteSpace(row.MatchedByVarianceColumns) ? null : row.MatchedByVarianceColumns.Split(',').Select(x => Convert.ToInt32(x) - 1).ToArray();

                var colData = new Dictionary<String, Object>();
                var leftRowNbr = (row.SourceARowIndex - 1);
                var rightRowNbr = (row.SourceBRowIndex - 1);

                foreach (var entityCol in MetaData.FieldDescriptors.Cast<ReconResultEntityCol>())
                {
                    var leftCell = leftFile.FileIndex.Rows[leftRowNbr][entityCol.ColNbr];
                    var rightCell = rightFile.FileIndex.Rows[rightRowNbr][entityCol.ColNbr];
                    var dataCell = new ReconResultEntityDualValue
                    {
                        LeftValue = leftCell.Value,
                        RightValue = rightCell.Value,
                        MatchState = ReconDataMatchState.None,
                        LeftOffsetStart = leftCell.Offset,
                        LeftOffsetEnd = leftCell.Offset + leftCell.Length,
                        RightOffsetStart = rightCell.Offset,
                        RightOffsetEnd = rightCell.Offset + rightCell.Length,
                    };

                    if (entityCol.ColType == ColumnType.Comp)
                    {
                        if ((unmatchedColumns != null) && (unmatchedColumns.Contains(entityCol.ColNbr)))
                            dataCell.MatchState = ReconDataMatchState.Unmatched;

                        else if ((matchedByVarColumns != null) && (matchedByVarColumns.Contains(entityCol.ColNbr)))
                            dataCell.MatchState = ReconDataMatchState.MatchVar;

                        else
                            dataCell.MatchState = ReconDataMatchState.Matched;
                    }

                    colData.Add(entityCol.Name, dataCell);
                }

                DynamicData.Add(new DynamicEntityRow(colData));
            }

        }

        #endregion
    }
}
