﻿using System;
using System.Collections.Generic;
using Atlas.Link;
using Atlas.Link.Model;
using Atlas.Link.Model.Recon;

namespace Link.ViewModel
{
    public class ReconResultUnmatchedCompsViewModel : BaseDynamicViewModel, IDataFocusReceiver
    {
        #region Constructors

        public ReconResultUnmatchedCompsViewModel(LinkReconciliationResult reconResult)
            : base()
        {
            Reconciliation recon = reconResult.Message.Route.TransformerComponent.Reconciliation;

            for (int i = 0; i < recon.KeyFields.Count; i++)
            {
                ReconciliationKeyField fld = recon.KeyFields[i];
                String fieldName = String.Format("key.{0}", i);
                String fieldDesc = (fld.LeftField.Name == fld.RightField.Name) ? fld.RightField.Name : String.Format("{0}/{1}", fld.LeftField.Name, fld.RightField.Name);

                base.MetaData.FieldDescriptors.Add(new DynamicFieldDescriptor(fieldName, fieldDesc));
            }

            foreach (var fld in recon.CompFields)
            {
                base.MetaData.FieldDescriptors.Add(new DynamicFieldDescriptor(String.Format("lft.{0}", fld.LeftField.Name), String.Format("{0} ({1})", fld.LeftField.Name, "Left")));
                base.MetaData.FieldDescriptors.Add(new DynamicFieldDescriptor(String.Format("rht.{0}", fld.RightField.Name), String.Format("{0} ({1})", fld.RightField.Name, "Right")));
            }

            foreach (ReconRowDifference row in reconResult.DifferentRows)
            {
                Dictionary<String, Object> data = new Dictionary<String, Object>();
                String[] keyValues = row.Key.Split(';');

                for (int i = 0; i < recon.KeyFields.Count; i++)
                {
                    var fldDesc = base.MetaData.FieldDescriptors[i];
                    data.Add(fldDesc.Name, keyValues[i]);
                }

                data.Add(String.Format("lft.{0}", row.LeftField.Name), row.LeftValue);
                data.Add(String.Format("rht.{0}", row.RightField.Name), row.RightValue);

                ReconResultDiffEntity entity = new ReconResultDiffEntity(data);
                entity.LeftRowNbr = row.LeftSourceRowIndex;
                entity.RightRowNbr = row.RightSourceRowIndex;
                base.DynamicData.Add(entity);
            }
        }

        #endregion

        #region Binding Properties

        public Int32 FocusedRowNbr
        {
            get { return _focusedRowNbr; }
            set
            {
                if (value != _focusedRowNbr)
                {
                    _focusedRowNbr = value;
                    RaisePropertyChanged(() => this.FocusedRowNbr);
                }
            }
        }
        private Int32 _focusedRowNbr = 0;

        public Int32 FocusedColNbr
        {
            get { return _focusedColNbr; }
            set
            {
                if (value != _focusedColNbr)
                {
                    _focusedColNbr = value;
                    RaisePropertyChanged(() => this.FocusedColNbr);
                }
            }
        }
        private Int32 _focusedColNbr = 0;

        #endregion

        #region IDataFocusReceiver

        public void PositionChanged(int sourceRowNbr, string sourceFieldName)
        {
            this.FocusedColNbr = base.MetaData.FieldDescriptors.IndexWhere(x => x.Name.Equals(sourceFieldName, StringComparison.CurrentCultureIgnoreCase));
            this.FocusedRowNbr = sourceRowNbr;
        }

        #endregion
    }
}