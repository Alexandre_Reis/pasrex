﻿using System;
using Atlas.Link.IO;

namespace Link.ViewModel
{
    public class ReconResultFileViewModel : ITextBoxSelectionController
    {
        #region Construction

        public ReconResultFileViewModel(String text)
        {
            _fileText = text;
            _hasHeader = true;
            _fileIndex = new TextFileIndexer(ref text, ";", "\r\n", _hasHeader, TextValueHandler);
        }

        #endregion

        #region Fields

        private String _fileText;
        private Boolean _hasHeader;
        private TextFileIndexer _fileIndex;

        #endregion

        #region Properties

        public String Text
        {
            get { return _fileText; }
        }
        public TextFileIndexer FileIndex
        {
            get { return _fileIndex; }
        }
        public Boolean HasHeader
        {
            get { return _hasHeader; }
        }
        public Int32 RowCount
        {
            get { return _fileIndex.Rows.Count; }
        }

        #endregion

        #region Methods

        private String TextValueHandler(Int32 offset, Int32 length)
        {
            return _fileText.Substring(offset, length);
        }

        public void DoClearSelection()
        {
            SelectTextEventHandler handler = SelectText;
            if (handler != null)
            {
                handler(this, new SelectionEventArgs(0, 0));
            }
        }
        public void DoSelectAll()
        {
            SelecAllEventHandler handler = SelectAll;
            if (handler != null)
                handler(this);
        }
        public void DoSelectText(Int32 rowNbr, Int32 colNbr)
        {
            SelectTextEventHandler handler = SelectText;
            if (handler != null)
            {
                if (_hasHeader)
                    rowNbr = rowNbr + 1;

                if (rowNbr < _fileIndex.Rows.Count)
                {
                    var row = _fileIndex.Rows[rowNbr];

                    if (colNbr < row.Count)
                    {
                        TextFileField cell = row[colNbr];
                        handler(this, new SelectionEventArgs(cell.Offset, cell.Length));
                    }
                }
            }
        }

        #endregion

        #region ITextBoxSelectionController

        public event SelecAllEventHandler SelectAll;
        public event SelectTextEventHandler SelectText;

        #endregion
    }
}