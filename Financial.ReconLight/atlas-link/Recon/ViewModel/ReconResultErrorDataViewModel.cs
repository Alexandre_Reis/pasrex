﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Link;
using Atlas.Link.Model;
using Atlas.Link.Model.Recon;
using Atlas.Recon.Model;

namespace Link.ViewModel
{
    /// <summary>
    /// The ViewModel for Recon Errors. These are single source errors like dups.
    /// </summary>
    public class ReconResultErrorDataViewModel : BaseDynamicViewModel
    {
        #region Constructors

        public ReconResultErrorDataViewModel(IEnumerable<TransformationField> keyFlds, IEnumerable<TransformationField> compFlds, ReconResultFileViewModel file, IEnumerable<ReconciliationRow> reconRows, Enums.ReconciliationCategory category)
        {
            ReconDataSource source;

            switch (category)
            {
                case Enums.ReconciliationCategory.MISSING_IN_A:
                    source = ReconDataSource.Left;
                    break;
                case Enums.ReconciliationCategory.MISSING_IN_B:
                    source = ReconDataSource.Right;
                    break;
                case Enums.ReconciliationCategory.DUPLICATE_IN_A:
                    source = ReconDataSource.Left;
                    break;
                case Enums.ReconciliationCategory.DUPLICATE_IN_B:
                    source = ReconDataSource.Right;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("category");
            }

            if (file.FileIndex.FieldNames.Count == 0)
                throw new Exception("Source file does not contain field names");

            foreach (var fld in keyFlds)
            {
                var keyField = fld;
                var colName = keyField.Name;
                var colDesc = keyField.Name;
                var colNbr = file.FileIndex.FieldNames.IndexWhere(x => x.Value.Equals(keyField.Name, StringComparison.InvariantCultureIgnoreCase));

                if (colNbr < 0)
                    throw new Exception("Key Field not found in source file: " + keyField.Name);

                MetaData.FieldDescriptors.Add(new ReconResultEntityCol(colName, colDesc, colNbr, ColumnType.Key));
            }

            foreach (var fld in compFlds)
            {
                var cmpField = fld;
                var colName = cmpField.Name;
                var colDesc = cmpField.Name;
                var colNbr = file.FileIndex.FieldNames.IndexWhere(x => x.Value.Equals(cmpField.Name, StringComparison.InvariantCultureIgnoreCase));

                if (colNbr < 0)
                    throw new Exception("Comp Field not found in source file: " + cmpField.Name);

                MetaData.FieldDescriptors.Add(new ReconResultEntityCol(colName, colDesc, colNbr, ColumnType.Comp));
            }

            foreach (var row in reconRows.Where(x => x.Category == category))
            {
                var colData = new Dictionary<String, Object>();
                var rowNbr = (source == ReconDataSource.Left) ? (row.SourceARowIndex - 1) : (row.SourceBRowIndex - 1);

                //todo: Assert

                foreach (var entityCol in MetaData.FieldDescriptors.Cast<ReconResultEntityCol>())
                {
                    var fileCell = file.FileIndex.Rows[rowNbr][entityCol.ColNbr];
                    var dataCell = new ReconResultEntityValue
                    {
                        Value = fileCell.Value,
                        Source = source,
                        OffsetStart = fileCell.Offset,
                        OffsetEnd = fileCell.Offset + fileCell.Length,
                    };

                    colData.Add(entityCol.Name, dataCell);
                }

                DynamicData.Add(new DynamicEntityRow(colData));
            }
        }

        #endregion
    }
}
