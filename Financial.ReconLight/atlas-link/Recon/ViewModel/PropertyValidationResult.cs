﻿using System;
using System.Collections.Generic;

namespace Link.ViewModel
{
    public class PropertyValidationResult
    {
        #region Construction

        public PropertyValidationResult(String propertyName)
        {
            this.PropertyName = propertyName;
            this.Errors = new List<String>();
        }

        #endregion

        #region Properties

        public String PropertyName { get; private set; }

        internal List<String> Errors { get; private set; }

        #endregion

        #region Methods

        public void Add(String errorString)
        {
            this.Errors.Add(errorString);
        }
        public void Clear()
        {
            this.Errors.Clear();
        }

        #endregion
    }
}
