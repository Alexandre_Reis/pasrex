﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Common.Logging;

namespace Link.ViewModel
{
    public delegate void RequestViewCloseEvent(Object sender, EventArgs e);

    public enum ViewModelState
    {
        Initialized,
        CloseCancel,
        CloseSave,
        Unknown
    }

    /// <summary>
    /// The base class for MVVM View Models.
    /// </summary>
    public abstract class BaseViewModel : INotifyPropertyChanged, IDisposable // DependencyObject
    {
        #region Factory Members

        public static TViewModel CreateViewModel<TViewModel, TEntity>(TEntity modelEntity)
        //where TEntity : BaseAuditableEntity
        //where TViewModel : AuditableEntityViewModel<TEntity>
        {
            Type viewmodelType = typeof(TViewModel);
            Type entityType = typeof(TEntity);

            ConstructorInfo ctor = viewmodelType.GetConstructor(new Type[] { entityType });

            if (ctor == null)
                throw new Exception("Unable to find suitable constructor for type: " + viewmodelType.FullName);

            Object viewmodel = ctor.Invoke(new Object[] { modelEntity });

            return (TViewModel)viewmodel;
        }

        #endregion

        #region Construction

        private static Dictionary<RuntimeTypeHandle, ViewModelDef> _viewmodelTypeCache = new Dictionary<RuntimeTypeHandle, ViewModelDef>();

        private static ViewModelDef InitializeType(Type viewmodelType)
        {
            RuntimeTypeHandle viewmodelHandle = viewmodelType.TypeHandle;
            ViewModelDef viewmodelDef;

            //TODO: benchmark this lock
            lock (_viewmodelTypeCache)
            {
                if (_viewmodelTypeCache.TryGetValue(viewmodelHandle, out viewmodelDef) == false)
                {
                    viewmodelDef = new ViewModelDef(viewmodelType);
                    _viewmodelTypeCache.Add(viewmodelHandle, viewmodelDef);
                }
            }

            return viewmodelDef;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseViewModel"/> class.
        /// </summary>
        public BaseViewModel()
        {
            _viewmodelDef = InitializeType(this.GetType()); // parse and cache the meta-data for this ViewModel type

            this.State = ViewModelState.Initialized;
            this.EntityIsModified = false;

            
        }

        #endregion

        #region Fields

        private ViewModelDef _viewmodelDef;

        #endregion

        #region View Model state change hooks

        /// <summary>
        /// Views the model initialize.
        /// </summary>
        public virtual void ViewModelInitialize()
        {
            // nada
        }
        public virtual void ViewModelActivate()
        {
            // nada
        }
        public virtual void ViewModelDeactivate()
        {
            // nada
        }
        public virtual void ViewModelFinalize()
        {
            // nada
        }

        #endregion

        #region Utility Properties

        /// <summary>
        /// Returns a flag indicating whether the Entity Model state has been modified.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [entity is modified]; otherwise, <c>false</c>.
        /// </value>
        protected Boolean EntityIsModified { get; private set; }

        /// <summary>
        /// Returns a flag indicating whether the Viewmodel state has been modified.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is modified]; otherwise, <c>false</c>.
        /// </value>
        public virtual Boolean IsModified
        {
            get { return this.EntityIsModified; }
        }

        /// <summary>
        /// Gets the name of the entity which the ViewModel represents.
        /// </summary>
        /// <value>
        /// The name of the entity.
        /// </value>
        public virtual string EntityName
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the state of the ViewModel.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        public ViewModelState State { get; private set; }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        /// <value>
        /// The logger.
        /// </value>
        protected ILog Logger
        {
            get { return _viewmodelDef.Logger; }
        }
        /// <summary>
        /// Gets a value indicating whether new entity validation is delayed.
        /// </summary>
        /// <value>
        /// <c>true</c> if [delayed new validation]; otherwise, <c>false</c>.
        /// </value>
        protected Boolean DelayedNewValidation
        {
            get { return _viewmodelDef.DelayedNewValidation; }
        }

        /// <summary>
        /// Gets the timestamp when the ViewModel was last modified.
        /// </summary>
        /// <value>
        /// The last modified time.
        /// </value>
        public DateTime LastModifiedTime
        {
            get { return _lastModifiedTime; }
            internal set { _lastModifiedTime = value; }
        }
        private DateTime _lastModifiedTime = DateTime.Now;

        #endregion

        #region Dispatcher Support Members

        protected void ExecuteOnUIThread(Action action)
        {
            
        }
        protected void ExecuteOnUIThread<T>(Action<T> action, T data)
        {
            
        }

        #endregion

        #region Localization Support Members

        private void LanguageChangedHandler(object sender, EventArgs e)
        {
            DoUpdateOnLanguageChange();
        }
        /// <summary>
        /// Allows descendants to react to changes in language.
        /// </summary>
        protected virtual void DoUpdateOnLanguageChange()
        {
            //
        }

        #endregion

        #region View Close Members

        public event RequestViewCloseEvent RequestViewClose;

        protected void DoRequestViewClose(Boolean success)
        {
            RequestViewCloseEvent handler = RequestViewClose;

            if (handler != null)
            {
                this.State = (success) ? ViewModelState.CloseSave : ViewModelState.CloseCancel;
                handler(this, new EventArgs());
            }
        }

        #endregion

        #region Command Support Members

        protected virtual void DoSetCommandState()
        {
            //
        }
        protected void HandleCommandError(String methodName, Exception exception)
        {
            
        }

        #endregion

        #region Validation Support Members

        [Obsolete("Implement a Validate[PropertyName]() method instead.", false)]
        protected virtual void DoValidation(string propertyName)
        {
            //
        }
        [Obsolete("New records are validated automatically.", true)]
        public virtual void DoValidationToNewRecords()
        {
            //
        }

        #endregion

        #region INotifyPropertyChanged

        private Int32 _propertyChangeCount = 0;
        private List<String> _propertyChangeItems = new List<String>();

        /// <summary>
        /// Occurs when a property changes
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void DoPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;

            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Raises the property changed event.
        /// This implementation uses a Linq expression to resolve the property name.
        /// This is the preferred implementation because it verifies whether the
        /// value has actually been modified before executing validation and 
        /// calling event handler.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpresssion">The property expresssion.</param>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">propertyExpresssion</exception>
        protected Boolean RaisePropertyChanged<T>(System.Linq.Expressions.Expression<Func<T>> propertyExpresssion, ref T field, T value)
        {
            if (propertyExpresssion == null)
                throw new ArgumentNullException("propertyExpresssion");

            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;

            field = value;

            

            return true;
        }
        /// <summary>
        /// Raises the property changed event.
        /// This implementation uses a Linq expression to resolve the property name.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyExpresssion">The property expresssion.</param>
        /// <exception cref="System.ArgumentNullException">propertyExpresssion</exception>
        protected void RaisePropertyChanged<T>(System.Linq.Expressions.Expression<Func<T>> propertyExpresssion, Boolean bypassValidation = false)
        {
            if (propertyExpresssion == null)
                throw new ArgumentNullException("propertyExpresssion");

            
        }
        /// <summary>
        /// Raises the property changed event.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="bypassValidation">if set to <c>true</c> [bypass validation].</param>
        protected void RaisePropertyChanged(string propertyName, Boolean bypassValidation = false)
        {
            ViewModelPropertyDef propDef;

            _propertyChangeCount++;
            try
            {
                

                if (_viewmodelDef.Properties.TryGetValue(propertyName, out propDef))
                {
                    // only set the modified flag if the property allows it
                    if (propDef.BypassModifiedFlag == false)
                    {
                        this.EntityIsModified = true;
                        
                    }

                    // invoke validation check for the first INPC call on properties that allow it
                    if (bypassValidation == false)
                        DoPropertyValidation(propertyName, propDef);
                }
            }
            finally
            {
                _propertyChangeCount--;
            }

            if (_propertyChangeCount == 0)
            {
                // validate whole entity
                if (bypassValidation == false)
                    DoEntityValidation();

                // finally call event handlers for cached property changes
                _propertyChangeItems.ForEach(x => DoPropertyChanged(x));
                _propertyChangeItems.Clear();

                // invoke command state check once for the first INPC call on properties that allow it
                if (bypassValidation == false)
                    DoSetCommandState();
            }
        }

        #endregion

        #region IlocalizedDataErrorInfo

        
        private void DoPropertyValidation(String propertyName, ViewModelPropertyDef propDef)
        {
            
        }
        private void DoEntityValidation()
        {
            
        }

        /// <summary>
        /// Returns a flag indicating whether errors exist.
        /// </summary>
        /// <value>
        /// The errors value.
        /// </value>
        public Boolean HasErrors
        {
            get { return false; }
        }

        /// <summary>
        /// Validates the whole entity.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// This validation allows initial validation to be invoked by
        /// descendant or owner types, only once.
        /// The main use case will be for new entity validation to be called
        /// by the owner in specific cases where validation needs to be invoked
        /// immediately, or during a save command before the HasErrors is checked.
        /// After the initial call, further validation will be invoked by property
        /// setter calls.
        /// </remarks>
        public Boolean PerformValidation()
        {
            // validate all properties
            foreach (var kvp in _viewmodelDef.Properties)
            {
                // invoke validation: check for the first INPC call on properties that allow it
                DoPropertyValidation(kvp.Key, kvp.Value);
            }

            // validate whole entity
            DoEntityValidation();

            //foreach (var kvp in _viewmodelDef.Properties)
            //{
            //    // update the UI
            //    DoPropertyChanged(kvp.Key);
            //}

            return this.HasErrors;
        }

        [Obsolete("Use the new validation mechanism", false)]
        public void ClearPropertyErrors(String propertyName)
        {
            
        }

        [Obsolete("Use the new validation mechanism", false)]
        public void SetPropertyError(String propertyName, params string[] errorStrings)
        {
            
        }


        #endregion

        #region IDataErrorInfo

        

        

        #endregion

        #region IDisposable

        protected virtual void DoDispose()
        {
            // nada
        }

        public void Dispose()
        {
            DoDispose();
        }

        #endregion

        #region Runtime Exception Handling

        public static Boolean IgnoreException(Exception ex)
        {
            return ((ex is Spring.Rest.Client.RestClientException) && (ex.Message.Contains("Subject does not have permission")));
        }

        #endregion
    }
}
