﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlas.Link.Model;
using Atlas.Recon.Model;

namespace Link.ViewModel
{
    public class ReconResultViewModel : BaseViewModel
    {
        #region Constructors

        public ReconResultViewModel(ReconciliationResult reconResult, ReconciliationRoute reconRoute)
        {
            
            var leftKeys = reconRoute.Reconciliation.KeyFields.ConvertAll(x => x.LeftField).ToList();
            var rightKeys = reconRoute.Reconciliation.KeyFields.ConvertAll(x => x.RightField).ToList();
            var leftComps = reconRoute.Reconciliation.CompFields.ConvertAll(x => x.LeftField).ToList();
            var rightComps = reconRoute.Reconciliation.CompFields.ConvertAll(x => x.RightField).ToList();

            // parse text files
            LeftFile = new ReconResultFileViewModel(reconResult.LeftFileData);
            RightFile = new ReconResultFileViewModel(reconResult.RightFileData);

            // create detail data
            MatchedRows = new ReconResultDiffDataViewModel(reconRoute.Reconciliation, LeftFile, RightFile, reconResult.ReconciliationRows, Enums.ReconciliationCategory.MATCHED);
            MatchedVarRows = new ReconResultDiffDataViewModel(reconRoute.Reconciliation, LeftFile, RightFile, reconResult.ReconciliationRows, Enums.ReconciliationCategory.MATCHED_BY_VARIANCE);
            UnmatchedRows = new ReconResultDiffDataViewModel(reconRoute.Reconciliation, LeftFile, RightFile, reconResult.ReconciliationRows, Enums.ReconciliationCategory.UNMATCHED);
            LeftNotInRightRows = new ReconResultErrorDataViewModel(leftKeys, leftComps, LeftFile, reconResult.ReconciliationRows, Enums.ReconciliationCategory.MISSING_IN_A);
            RightNotInLeftRows = new ReconResultErrorDataViewModel(rightKeys, rightComps, RightFile, reconResult.ReconciliationRows, Enums.ReconciliationCategory.MISSING_IN_B);
            LeftDuplicateRows = new ReconResultErrorDataViewModel(leftKeys, leftComps, LeftFile, reconResult.ReconciliationRows, Enums.ReconciliationCategory.DUPLICATE_IN_A);
            RightDuplicateRows = new ReconResultErrorDataViewModel(rightKeys, rightComps, RightFile, reconResult.ReconciliationRows, Enums.ReconciliationCategory.DUPLICATE_IN_B);

            // create summary data 
            LeftFileSummary = GetSummary(LeftFile.RowCount, MatchedVarRows.Count, UnmatchedRows.Count, LeftNotInRightRows.Count, LeftDuplicateRows.Count);
            RightFileSummary = GetSummary(RightFile.RowCount, MatchedVarRows.Count, UnmatchedRows.Count, RightNotInLeftRows.Count, RightDuplicateRows.Count);
        }

        #endregion

        #region Utility Members

        private List<ReconResultSummaryViewModel> GetSummary(Int32 totalCnt, Int32 withinvarCnt, Int32 unmatchedCnt, Int32 missingCnt, Int32 duplicateCnt)
        {
            var result = new List<ReconResultSummaryViewModel>();

            Int32 matchedCnt = (totalCnt - (withinvarCnt + unmatchedCnt + missingCnt + duplicateCnt));

            // populate summary stats
            //result.Add(new ReconResultSummaryViewModel { SummaryType = StringConst.Data.ReconMatched.Text, SummaryValue = matchedCnt });
            //result.Add(new ReconResultSummaryViewModel { SummaryType = StringConst.Data.ReconWithinVar.Text, SummaryValue = withinvarCnt });
            //result.Add(new ReconResultSummaryViewModel { SummaryType = StringConst.Data.ReconUnmatched.Text, SummaryValue = unmatchedCnt });
            //result.Add(new ReconResultSummaryViewModel { SummaryType = StringConst.Data.ReconMissing.Text, SummaryValue = missingCnt });
            //result.Add(new ReconResultSummaryViewModel { SummaryType = StringConst.Data.ReconDuplicates.Text, SummaryValue = duplicateCnt });

            return result;
        }

        #endregion

        #region Binding Properties

        /// <summary>
        /// Gets or sets the selected view index.
        /// </summary>
        /// <value>
        /// The selected view index.
        /// </value>
        public Int32 SelectedView
        {
            get { return _selectedView; }
            set
            {
                if (value != _selectedView)
                {
                    _selectedView = value;
                    //SelectedViewType view = (SelectedViewType)_selectedView;
                    //SetTextSelection(view);
                    RaisePropertyChanged(() => IsFirstViewSelected);
                }
            }
        }
        private Int32 _selectedView;

        /// <summary>
        /// Gets a value indicating whether the first (summary) view is selected.
        /// </summary>
        /// <value>
        /// <c>true</c> if [the summary view is selected]; otherwise, <c>false</c>.
        /// </value>
        public Boolean IsFirstViewSelected
        {
            get { return (_selectedView == 0); }
        }

        public ReconResultFileViewModel LeftFile { get; private set; }
        public ReconResultFileViewModel RightFile { get; private set; }

        public ReconResultDiffDataViewModel MatchedVarRows { get; private set; }
        public ReconResultDiffDataViewModel MatchedRows { get; private set; }
        public ReconResultDiffDataViewModel UnmatchedRows { get; private set; }
        public ReconResultErrorDataViewModel LeftNotInRightRows { get; private set; }
        public ReconResultErrorDataViewModel RightNotInLeftRows { get; private set; }
        public ReconResultErrorDataViewModel LeftDuplicateRows { get; private set; }
        public ReconResultErrorDataViewModel RightDuplicateRows { get; private set; }

        public List<ReconResultSummaryViewModel> LeftFileSummary { get; private set; }
        public List<ReconResultSummaryViewModel> RightFileSummary { get; private set; }

        #endregion

        #region Binding Command

        private void DoCloseCommand()
        {
            DoRequestViewClose(true);
        }

        #endregion
    }
}
