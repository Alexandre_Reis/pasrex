﻿using System;
using System.Collections.Generic;
using Atlas.Link;
using Atlas.Link.Model;
using Atlas.Link.Model.Recon;

namespace Link.ViewModel
{
    public class ReconResultMissingKeysViewModel : BaseDynamicViewModel, IDataFocusReceiver
    {
        #region Constructors

        public ReconResultMissingKeysViewModel(IList<TransformationField> keyFlds, IList<TransformationField> compFlds, List<ReconDataRow> dupRows)
            : base()
        {
            foreach (var fld in keyFlds)
            {
                DynamicFieldDescriptor fldDesc = new DynamicFieldDescriptor(fld.Name);
                base.MetaData.FieldDescriptors.Add(fldDesc);
            }

            foreach (var row in dupRows)
            {
                Dictionary<String, Object> data = new Dictionary<String, Object>();

                String[] keyValues = row.Key.Split(';');

                for (int i = 0; i < base.MetaData.FieldDescriptors.Count; i++)
                {
                    var fldDesc = base.MetaData.FieldDescriptors[i];
                    data.Add(fldDesc.Name, keyValues[i]);
                }

                DynamicEntityRow entity = new DynamicEntityRow(data);
                base.DynamicData.Add(entity);
            }
        }

        #endregion

        #region Binding Properties

        public Int32 FocusedRowNbr
        {
            get { return _focusedRowNbr; }
            set
            {
                if (value != _focusedRowNbr)
                {
                    _focusedRowNbr = value;
                    RaisePropertyChanged(() => this.FocusedRowNbr);
                }
            }
        }
        private Int32 _focusedRowNbr = 0;

        public Int32 FocusedColNbr
        {
            get { return _focusedColNbr; }
            set
            {
                if (value != _focusedColNbr)
                {
                    _focusedColNbr = value;
                    RaisePropertyChanged(() => this.FocusedColNbr);
                }
            }
        }
        private Int32 _focusedColNbr = 0;

        #endregion

        #region IDataFocusReceiver

        public void PositionChanged(int sourceRowNbr, string sourceFieldName)
        {
            this.FocusedColNbr = base.MetaData.FieldDescriptors.IndexWhere(x => x.Name.Equals(sourceFieldName, StringComparison.CurrentCultureIgnoreCase));
            this.FocusedRowNbr = sourceRowNbr;
        }

        #endregion
    }
}