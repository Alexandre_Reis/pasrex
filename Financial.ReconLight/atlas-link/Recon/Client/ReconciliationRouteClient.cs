﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Recon.Model;
using Atlas.Link.Net;

namespace Atlas.Recon.Client
{
    [ServiceContract]
    public interface IReconciliationRouteService
    {
        #region REST Methods

        [OperationContract]
        ReconciliationRouteList GetAllRoutes();

        [OperationContract]
        ReconciliationRouteList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        ReconciliationRoute Save(ReconciliationRoute route);

        [OperationContract]
        Atlas.Link.Model.StandardResponse Delete(String routeId);

        [OperationContract]
        Atlas.Link.Model.StandardResponse Trigger(String routeId);

        [OperationContract]
        Atlas.Link.Model.StandardResponse Enable(String routeId);

        [OperationContract]
        Atlas.Link.Model.StandardResponse Disable(String routeId);

        #endregion
    }

    [ServiceHost("", BaseURL = "/reconroute")]
    [XmlSerializerFormat]
    public class ReconciliationRouteClient : SpringRestClient<IReconciliationRouteService>, IReconciliationRouteService, Atlas.Link.Client.IPollClient<ReconciliationRoute>, Atlas.Link.Client.IUpdateClient<ReconciliationRoute>
    {
        #region IRouteService

        [WebInvoke(Method = "GET", UriTemplate = "/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationRouteList GetAllRoutes()
        {
            return Execute<ReconciliationRouteList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "GET", UriTemplate = "/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationRouteList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<ReconciliationRouteList>(System.Reflection.MethodInfo.GetCurrentMethod(), Atlas.Link.Client.Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "POST", UriTemplate = "/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationRoute Save(ReconciliationRoute route)
        {
            return ExecuteWithBody<ReconciliationRoute>(System.Reflection.MethodInfo.GetCurrentMethod(), route);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/{routeId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Atlas.Link.Model.StandardResponse Delete(String routeId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
            return null; //TODO...
        }

        [WebInvoke(Method = "POST", UriTemplate = "/{routeId}/trigger")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Atlas.Link.Model.StandardResponse Trigger(String routeId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
            return new StandardResponse {ResponseStatus = ResponseType.OK, Text = "ok"};
        }

        [WebInvoke(Method = "POST", UriTemplate = "/{routeId}/enabled")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Atlas.Link.Model.StandardResponse Enable(String routeId)
        {
            return Execute<Atlas.Link.Model.StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
        }

        [WebInvoke(Method = "POST", UriTemplate = "/{routeId}/disabled")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Atlas.Link.Model.StandardResponse Disable(String routeId)
        {
            return Execute<Atlas.Link.Model.StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
        }

        #endregion

        #region IPollClient

        System.Collections.Generic.IList<ReconciliationRoute> Atlas.Link.Client.IPollClient<ReconciliationRoute>.FetchAll()
        {
            return GetAllRoutes();
        }

        System.Collections.Generic.IList<ReconciliationRoute> Atlas.Link.Client.IPollClient<ReconciliationRoute>.FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}