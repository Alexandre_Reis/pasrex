﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Recon.Model;
using Atlas.Link.Net;

namespace Atlas.Recon.Client
{
    [ServiceContract]
    public interface IReconciliationDashboardService
    {
        #region REST Methods

        [OperationContract]
        ReconciliationDashboard GetAllDashBoard(DateTime date);


        #endregion
    }

    [ServiceHost("", BaseURL = "/recondashboard")]
    [XmlSerializerFormat]
    public class ReconciliationDashboardClient : SpringRestClient<IReconciliationDashboardService>, IReconciliationDashboardService
    {
        #region IRouteService

        [WebInvoke(Method = "GET", UriTemplate = "/list?date={date}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationDashboard GetAllDashBoard(DateTime date)
        {
            return Execute<ReconciliationDashboard>(System.Reflection.MethodInfo.GetCurrentMethod(), Atlas.Link.Client.Utils.ToServerDatetime(date));
        }

        #endregion

    }
}