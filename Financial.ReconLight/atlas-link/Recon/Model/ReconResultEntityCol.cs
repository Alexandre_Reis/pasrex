﻿using System;

namespace Atlas.Recon.Model
{
    public class ReconResultEntityCol : Link.Model.DynamicFieldDescriptor
    {
        #region Constructors

        public ReconResultEntityCol(String fieldName, String displayName, Int32 colNbr, ColumnType colType)
            : base(fieldName, displayName)
        {
            ColNbr = colNbr;
            ColType = colType;
        }

        #endregion

        #region Properties

        public Int32 ColNbr { get; private set; }
        public ColumnType ColType { get; private set; }

        #endregion
    }
}