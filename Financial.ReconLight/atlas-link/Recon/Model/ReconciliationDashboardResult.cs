﻿using Atlas.Link.Model.Recon;
using Atlas.Link.Serialization;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Recon.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_result", Namespace = "")]
    public class ReconciliationDashboardResult : Atlas.Link.Model.BaseEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "reconciliation_statistics")]
        public ReconciliationStatistics ReconciliationStatistics { get; set; }

        [XmlElement(ElementName = "reconciled_date")]
        public DateTime ReconciledDate { get; set; }

        [XmlElement(ElementName = "status")]
        public Atlas.Link.Model.Enums.MessageStatus MessageStatus { get; set; }

        [XmlElement(ElementName = "message_id")]
        public string MessageId { get; set; }

        [XmlArray(ElementName = "reconciliation_rows")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconciliationRow> ReconciliationRows { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "reconciliation_results", Namespace = "")]
    public class ReconciliationDashboardResultList : XmlSerializableList<ReconciliationDashboardResult> { }
  
}