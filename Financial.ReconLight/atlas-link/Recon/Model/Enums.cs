﻿namespace Atlas.Recon.Model
{
    /// <summary>
    /// Identifies the source fo the recon data.
    /// </summary>
    public enum ReconDataSource
    {
        Left = 0,
        Right = 1
    }

    /// <summary>
    /// The match-state for 2 recon data source values or cells.
    /// </summary>
    public enum ReconDataMatchState
    {
        None = 0,
        Matched = 1,
        MatchVar = 2,
        Unmatched = 3,        
    }

    public enum ColumnType
    {
        Key = 0,
        Comp = 1,
    }

}