﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Recon.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_route", Namespace = "")]
    public class ReconciliationRoute : Atlas.Link.Model.BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "left_in_component")]
        public Atlas.Link.Model.Endpoint LeftInEndpoint { get; set; }

        [XmlElement(ElementName = "right_in_component")]
        public Atlas.Link.Model.Endpoint RightInEndpoint { get; set; }

        [XmlElement(ElementName = "left_transformer_component")]
        public Atlas.Link.Model.TransformerComponent LeftInComponent { get; set; }

        [XmlElement(ElementName = "right_transformer_component")]
        public Atlas.Link.Model.TransformerComponent RightInComponent { get; set; }

        [XmlArray(ElementName = "out_components")]
        [XmlArrayItem(ElementName = "out_component")]
        public List<Atlas.Link.Model.Endpoint> OutEndpoints { get; set; }

        [XmlElement(ElementName = "reconciliation")]
        public Atlas.Link.Model.Reconciliation Reconciliation { get; set; }

        [XmlElement(ElementName = "status")]
        public Atlas.Link.Model.Enums.Status Status { get; set; }

        [XmlElement(ElementName = "category")]
        public String Category { get; set; }

        [XmlElement(ElementName = "description")]
        public String Description { get; set; }

        [XmlElement(ElementName = "reconciliation_schedule")]
        public ReconciliationSchedule Schedule{ get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "reconciliation_routes", Namespace = "")]
    public class ReconciliationRouteList : Atlas.Link.Serialization.XmlSerializableList<ReconciliationRoute> { }
}