﻿using System;

namespace Atlas.Recon.Model
{
    /// <summary>
    /// Contains the cell value and origin meta data for a Recon Result Entity.
    /// </summary>
    public class ReconResultEntityValue
    {
        #region Properties

        public String Value { get; set; }
        public ReconDataSource Source { get; set; }
        public Int32 OffsetStart { get; set; }
        public Int32 OffsetEnd { get; set; }

        #endregion

        #region Overrides

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Value;
        }

        #endregion
    }
}