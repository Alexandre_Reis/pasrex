﻿using Atlas.Link.Serialization;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Recon.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_route", Namespace = "")]
    public class ReconciliationDashboardRoute : Atlas.Link.Model.BaseEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

        [XmlArray(ElementName = "reconciliation_results")]
        [XmlArrayItem(ElementName = "reconciliation_result")]
        public List<ReconciliationDashboardResult> ReconciliationDashboardResults { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "reconciliation_routes", Namespace = "")]
    public class ReconciliationDashboardRouteList : XmlSerializableList<ReconciliationDashboardRoute> { }
}