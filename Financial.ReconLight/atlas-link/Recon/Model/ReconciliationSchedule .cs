﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Recon.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_schedule", Namespace = "")]
    public class ReconciliationSchedule : Atlas.Link.Model.BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "cron_string")]
        public String CronString { get; set; }

        [XmlElement(ElementName = "manual_trigger")]
        public Boolean IsManualTrigger { get; set; }

        #endregion
    }
}