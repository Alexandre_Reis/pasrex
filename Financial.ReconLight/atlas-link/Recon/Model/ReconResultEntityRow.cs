﻿using System;
using System.Collections.Generic;

namespace Atlas.Recon.Model
{
    public class ReconResultEntityRow : Link.Model.DynamicEntityRow
    {
        #region Construction

        public ReconResultEntityRow(IDictionary<String, Object> data) : base(data) { }

        #endregion

        #region Properties

        public Int32? LeftRowNbr { get; set; }
        public Int32? RightRowNbr { get; set; }
        public Int32[] UnmatchedColumns { get; set; }
        public Int32[] MatchedByVarianceColumns { get; set; }

        #endregion
    }
}
