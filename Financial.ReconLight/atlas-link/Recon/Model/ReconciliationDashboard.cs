﻿using Atlas.Link.Serialization;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Recon.Model
{
    [Serializable]
    [XmlRoot(ElementName = "dashboard", Namespace = "")]
    public class ReconciliationDashboard : Atlas.Link.Model.BaseEntity
    {
        #region Serializable Properties

        [XmlArray(ElementName = "reconciliation_routes")]
        [XmlArrayItem(ElementName = "reconciliation_route")]
        public List<ReconciliationDashboardRoute> ReconciliationDashboardRoutes { get; set; }

        #endregion
    }
  
}