﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Model
{
    public class ReconResultFileEntity : DynamicEntityRow
    {
        #region Construction

        public ReconResultFileEntity(IDictionary<String, Object> data) : base(data) { }

        #endregion

        public Int32 RowNbr { get; set; }
        public Int32 ColNbr { get; set; }
    }

    public class ReconResultDiffEntity : DynamicEntityRow
    {
        #region Construction

        public ReconResultDiffEntity(IDictionary<String, Object> data) : base(data) { }

        #endregion

        public Int32 LeftRowNbr { get; set; }
        public Int32 RightRowNbr { get; set; }
        public Int32[] UnmatchedColumns { get; set; }
        public Int32[] MatchedByVarianceColumns { get; set; }
    }
}