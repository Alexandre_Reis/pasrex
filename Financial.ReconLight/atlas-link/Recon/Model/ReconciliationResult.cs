﻿using Atlas.Link.Model;
using Atlas.Link.Model.Recon;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Recon.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_result", Namespace = "")]
    public class ReconciliationResult : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlArray(ElementName = "right_missing")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconDataRow> LeftRowsMissingFromRight { get; set; }

        [XmlArray(ElementName = "left_missing")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconDataRow> RightRowsMissingFromLeft { get; set; }

        [XmlArray(ElementName = "left_duplicates")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconDataRow> LeftDuplicateRows { get; set; }

        [XmlArray(ElementName = "right_duplicates")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconDataRow> RightDuplicateRows { get; set; }

        [XmlArray(ElementName = "row_differences")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconRowDifference> UnmtachedRows { get; set; }

        [XmlArray(ElementName = "rows_matched")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconRowDifference> MatchedRows { get; set; }

        [XmlArray(ElementName = "row_by_variance")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconRowDifference> MatchedByVarRows { get; set; }

        [XmlElement(ElementName = "left_file_data")]
        public String LeftFileData { get; set; }

        [XmlElement(ElementName = "right_file_data")]
        public String RightFileData { get; set; }

        [XmlElement(ElementName = "message")]
        public Message Message { get; set; }

        [XmlArray(ElementName = "reconciliation_rows")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconciliationRow> ReconciliationRows { get; set; }

        #endregion
    }
}