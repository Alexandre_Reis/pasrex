﻿using System;

namespace Atlas.Recon.Model
{
    /// <summary>
    /// Contains the cell value and origin meta data for a Recon Result Entity.
    /// </summary>
    public class ReconResultEntityDualValue
    {
        #region Properties

        public String LeftValue { get; set; }
        public String RightValue { get; set; }
        public ReconDataMatchState MatchState { get; set; }
        public Int32 LeftOffsetStart { get; set; }
        public Int32 LeftOffsetEnd { get; set; }
        public Int32 RightOffsetStart { get; set; }
        public Int32 RightOffsetEnd { get; set; }

        #endregion

        #region Overrides

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            switch (MatchState)
            {
                case ReconDataMatchState.MatchVar:
                case ReconDataMatchState.Unmatched:
                    return String.Format("{0} / {1}", LeftValue, RightValue);
                default:
                    return LeftValue;
            }
        }

        #endregion
    }
}