﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Recon.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_message", Namespace = "")]
    public class TransportReconciliationMessage : Atlas.Link.Model.BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "status")]
        public Atlas.Link.Model.Enums.MessageStatus MessageStatus { get; set; }

        [XmlElement(ElementName = "route")]
        public ReconciliationRoute ReconciliationRoute { get; set; }

        [XmlElement(ElementName = "statistics")]
        public ReconciliationStatistics ReconciliationStatistics;

        [XmlElement(ElementName = "last_duration")]
        public int LastDuration { get; set; }

        #endregion

        #region Overrides

        //public override string ToString()
        //{
        //    return this.ID ;
        //}

        #endregion
    }

    [XmlRoot(ElementName = "reconciliation_messages", Namespace = "")]
    public class TransportReconciliationMessageList : Atlas.Link.Serialization.XmlSerializableList<TransportReconciliationMessage> { }
}