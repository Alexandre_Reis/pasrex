﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows;
using System.Xml.Schema;
using System.Reflection;

namespace Atlas.Link
{
    public static class TypeExtensions
    {
        #region Methods

        private static Boolean IsInputParamsEqual(MethodInfo method, Type[] inputParams)
        {
            var paramTypes = (from item in method.GetParameters()
                              select item.ParameterType).ToArray();

            if (inputParams == null && paramTypes == null)
                return true;

            else if (inputParams == null && paramTypes.Length == 0)
                return true;

            else if (inputParams.Length == 0 && paramTypes == null)
                return true;

            else if (inputParams.Length == paramTypes.Length)
            {
                for (int i = 0; i < inputParams.Length; i++)
                {
                    if (inputParams[i].Equals(paramTypes[i]) == false)
                        return false;
                }
            }

            return true;
        }
        private static Boolean IsInputParamsEmpty(MethodInfo method)
        {
            var paramTypes = method.GetParameters();

            return (paramTypes == null || paramTypes.Length == 0);
        }

        public static IList<MethodInfo> FindStaticMethods(this Type instance, Boolean publicOnly = false)
        {
            if (publicOnly)
                return instance.GetMethods(BindingFlags.Static | BindingFlags.Public).ToList();
            else
                return instance.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic).ToList();
        }
        public static IList<MethodInfo> FindInstanceMethods(this Type instance, Boolean publicOnly = false)
        {
            if (publicOnly)
                return instance.GetMethods(BindingFlags.Instance | BindingFlags.Public).ToList();
            else
                return instance.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).ToList();
        }
        public static IList<MethodInfo> WithName(this IList<MethodInfo> list, String methodName)
        {
            return list.Where(x => x.Name == methodName).ToList();
        }
        public static IList<MethodInfo> WithInputParams(this IList<MethodInfo> list, params Type[] inputParams)
        {
            return list.Where(x => IsInputParamsEqual(x, inputParams)).ToList();
        }
        public static IList<MethodInfo> WithNoInputParams(this IList<MethodInfo> list)
        {
            return list.Where(x => IsInputParamsEmpty(x)).ToList();
        }

        #endregion
    }

    public static class XmlExtentions
    {
        #region Methods

        public static IList<XmlSchemaElement> GetChildElements(this XmlSchemaElement element)
        {
            List<XmlSchemaElement> result = new List<XmlSchemaElement>();

            if (element.ElementSchemaType is XmlSchemaComplexType)
            {
                XmlSchemaComplexType est = element.ElementSchemaType as XmlSchemaComplexType;

                if (est.ContentTypeParticle is XmlSchemaSequence)
                {
                    XmlSchemaSequence sequence = (est.ContentTypeParticle as XmlSchemaSequence);

                    result.AddRange(sequence.Items.OfType<XmlSchemaElement>());
                }
            }

            return result;
        }

        #endregion
    }

    public static class ViewExtensions
    {
        #region Methods

        /// <summary>
        /// Finds the first child.
        /// </summary>
        /// <typeparam name="TChild">The type of the child.</typeparam>
        /// <param name="parent">The parent.</param>
        /// <param name="condition">The condition.</param>
        /// <returns></returns>
        //public static TChild FindFirstChild<TChild>(this DependencyObject parent, Predicate<TChild> condition) where TChild : DependencyObject
        //{
        //    if (parent == null)
        //        return null;

        //    if (condition == null)
        //        condition = (c) => true;

        //    TChild childResult = null;
        //    int childrenCount = System.Windows.Media.VisualTreeHelper.GetChildrenCount(parent);

        //    for (int i = 0; i < childrenCount; i++)
        //    {
        //        DependencyObject childItem = System.Windows.Media.VisualTreeHelper.GetChild(parent, i);
        //        childResult = (childItem as TChild);

        //        if ((childResult != null) && condition(childResult))
        //            return childResult;

        //        childResult = FindFirstChild<TChild>(childItem, condition);
        //        if (childResult != null)
        //            return childResult;
        //    }

        //    return null;
        //}

        #endregion
    }

    public static class GeneralExtensions
    {
        #region Methods

        public static TEnumType ToEnumValue<TEnumType>(this String itemString) where TEnumType : struct
        {
            System.Diagnostics.Debug.Assert(String.IsNullOrWhiteSpace(itemString) == false, "empty itemString argument");
            System.Diagnostics.Debug.Assert(typeof(TEnumType).IsEnum == true, "invalid generic TEnumType argument. must be an enum type");

            return (TEnumType)Enum.Parse(typeof(TEnumType), itemString);
        }

        /// <summary>
        /// Determines whether the given TimeSpan is within a time range.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="staTime">The sta time.</param>
        /// <param name="endTime">The end time.</param>
        /// <returns></returns>
        public static Boolean Within(this TimeSpan source, TimeSpan staTime, TimeSpan endTime)
        {
            return (source >= staTime && source <= endTime);
        }

        /// <summary>
        /// Returns the index of a list based on a predicate.
        /// </summary>
        /// <typeparam name="TItem">The type of the item.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        public static Int32 IndexWhere<TItem>(this IList<TItem> list, Predicate<TItem> predicate)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (predicate(list[i]))
                    return i;
            }
            return -1;
        }

        public static Boolean Exists<TItem>(this IList<TItem> list, Predicate<TItem> predicate)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (predicate(list[i]))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Resturns the first struct item in a list or a null value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        public static Nullable<T> FirstOrNull<T>(this IEnumerable<T> list) where T : struct
        {
            Nullable<T> result = null;

            foreach (T item in list)
            {
                result = item;
                break;
            }

            return result;
        }

        /// <summary>
        /// Determines whether an item is not in a list.
        /// </summary>
        /// <typeparam name="TItem">The type of the item.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public static Boolean DoesNotContain<TItem>(this IList<TItem> list, TItem item)
        {
            return (list.Contains(item) == false);
        }

        /// <summary>
        /// Adds an item to a list only if it does not contain an instance.
        /// </summary>
        /// <typeparam name="TItem">The type of the item.</typeparam>
        /// <param name="list">The list.</param>
        /// <param name="item">The item.</param>
        public static void AddIfDoesNotContain<TItem>(this IList<TItem> list, TItem item)
        {
            if (list.Contains(item) == false)
                list.Add(item);
        }

        public static bool ChangeAndNotify<T>(this PropertyChangedEventHandler handler, ref T field, T value, Expression<Func<T>> memberExpression)
        {
            if (memberExpression == null)
            {
                throw new ArgumentNullException("memberExpression");
            }
            var body = memberExpression.Body as MemberExpression;
            if (body == null)
            {
                throw new ArgumentException("Lambda must return a property.");
            }
            if (EqualityComparer<T>.Default.Equals(field, value))
            {
                return false;
            }

            var vmExpression = body.Expression as ConstantExpression;
            if (vmExpression != null)
            {
                LambdaExpression lambda = System.Linq.Expressions.Expression.Lambda(vmExpression);
                Delegate vmFunc = lambda.Compile();
                object sender = vmFunc.DynamicInvoke();

                if (handler != null)
                {
                    handler(sender, new PropertyChangedEventArgs(body.Member.Name));
                }
            }

            field = value;
            return true;
        }

        public static String SafeUpper(this String strValue)
        {
            if (String.IsNullOrWhiteSpace(strValue))
                return "";
            else
                return strValue.ToUpper();
        }

        public static String EnsureNotNull(this String strValue)
        {
            if (strValue == null)
                return "";
            else
                return strValue;
        }

        #endregion
    }
}