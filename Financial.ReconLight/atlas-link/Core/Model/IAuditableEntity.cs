﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Model
{
    /// <summary>
    /// Defines an Auditable Model Entity.
    /// </summary>
    public interface IAuditableEntity: IEntity
    {
        //String ID { get; }

        DateTime CreatedAt { get; }

        DateTime UpdatedAt { get; }

        Boolean? Deleted { get; }
    }
}
