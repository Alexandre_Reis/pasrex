﻿using System;
using System.Collections.Generic;

namespace Atlas.Link.Model
{
    public static class Utils
    {
        #region Public Methods

        /// <summary>
        /// Syncs 2 lists of items that derive from Auditable.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sourceList">The source list.</param>
        /// <param name="targetList">The target list.</param>
        /// <param name="cancelCheck">The cancel check.</param>
        public static void SyncAuditableLists<TEntity>(IList<TEntity> sourceList, IList<TEntity> targetList, Func<Boolean> cancelCheck) where TEntity : BaseAuditableEntity
        {
            // ensure that the lambda is not null
            Func<Boolean> doCancel = (cancelCheck != null) ? cancelCheck : () => false;

            //PERF: for...is faster than foreach
            for (int i = 0; i < sourceList.Count; i++)
            {
                // check whether Stop() has been requested
                if (doCancel())
                    break;

                //PERF: using indexes are faster
                TEntity srcItem = sourceList[i];
                Int32 trgIndex = targetList.IndexOf(srcItem);

                if (srcItem.Deleted.HasValue && srcItem.Deleted.Value) // IS Deleted
                {
                    if (trgIndex >= 0)
                    {
                        targetList.RemoveAt(trgIndex);
                    }
                    else
                    {
                        //nothing to see here, move along (IsDeleted and NOT in the master/target list)
                    }
                }
                else if (trgIndex < 0)
                {
                    targetList.Add(srcItem);
                }
                else if (srcItem.UpdatedAt > targetList[trgIndex].UpdatedAt) // why update when nothing has changed?
                {
                    targetList[trgIndex] = srcItem;
                }
            }
        }

        /// <summary>
        /// Clones a class using XM serialization/deserialization.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sourceEntity">The source entity.</param>
        /// <returns></returns>
        public static TEntity Clone<TEntity>(TEntity sourceEntity) where TEntity : class
        {
            TEntity clonedEntity = null;

            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(TEntity));

                serializer.Serialize(stream, sourceEntity);

                stream.Flush();
                stream.Position = 0;

                clonedEntity = (TEntity)serializer.Deserialize(stream);
            }

            return clonedEntity;
        }

        #endregion
    }
}