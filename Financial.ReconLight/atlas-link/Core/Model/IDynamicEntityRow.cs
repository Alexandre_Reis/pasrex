﻿using System;

namespace Atlas.Link.Model
{
    /// <summary>
    /// Dynamic Entity Row
    /// </summary>
    public interface IDynamicEntityRow
    {
        /// <summary>
        /// Determines whether the field exists.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns>flag indicating whether it exists</returns>
        Boolean ContainsField(String fieldName);

        /// <summary>
        /// Gets the field value.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <returns></returns>
        Object GetFieldValue(String fieldName);

        /// <summary>
        /// Sets the field value.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        /// <param name="value">The value.</param>
        void SetFieldValue(String fieldName, Object value);

        /// <summary>
        /// Resets the field value.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        void ResetFieldValue(String fieldName);

        /// <summary>
        /// Resets all field values.
        /// </summary>
        void ResetAllFieldValues();

        /// <summary>
        /// Indicated whether any values have been modified
        /// </summary>
        /// <value>
        /// The modified flag.
        /// </value>
        Boolean IsModified { get; }
    }
}