﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Settings
{
    public interface IApplicationSettings
    {
        /// <summary>
        /// Determines whether the application configuration file exists.
        /// </summary>
        /// <value>
        /// The application configuration exists.
        /// </value>
        Boolean ApplicationConfigExists { get; }

        /// <summary>
        /// Gets the application configuration.
        /// </summary>
        /// <returns></returns>
        AtlasLinkSection GetApplicationConfig();

        /// <summary>
        /// Saves the application configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        void SetApplicationConfig(AtlasLinkSection config);

    }
}