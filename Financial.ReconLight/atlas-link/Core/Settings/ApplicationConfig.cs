﻿using System;
using System.Configuration;

namespace Atlas.Link.Settings
{
    #region FolderCredentialsElement
    /// <summary>
    /// FolderCredentiaisElement
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationElement" />
    public class CredentialsElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        [ConfigurationProperty("usuario", DefaultValue = "", IsRequired = false)]
        public String UserName
        {
            get
            {
                return (String)this["usuario"];
            }
            set
            {
                this["usuario"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        [ConfigurationProperty("senha", DefaultValue = "", IsRequired = false)]
        public String Password
        {
            get
            {
                return (String)this["senha"];
            }
            set
            {
                this["senha"] = value;
            }
        }
    }
    #endregion


    #region FolderCredentiaisElement
    /// <summary>
    /// FolderCredentiaisElement
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationElement" />
    public class FolderCredentialsElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the usuario.
        /// </summary>
        /// <value>
        /// The usuario.
        /// </value>
        [ConfigurationProperty("usuario", DefaultValue = "", IsRequired = false)]
        public String Usuario
        {
            get
            {
                return (String)this["usuario"];
            }
            set
            {
                this["usuario"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the senha.
        /// </summary>
        /// <value>
        /// The senha.
        /// </value>
        [ConfigurationProperty("senha", DefaultValue = "", IsRequired = false)]
        public String Senha
        {
            get
            {
                return (String)this["senha"];
            }
            set
            {
                this["senha"] = value;
            }
        }
    }
    #endregion

    #region AtlasLinkInterfaceElement

    /// <summary>
    /// AtlasLinkInterfaceElement
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationElement" />
    public class AtlasLinkInterfaceElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the nome rota.
        /// </summary>
        /// <value>
        /// The nome rota.
        /// </value>
        [ConfigurationProperty("nomeRota", DefaultValue = "", IsRequired = true)]
        public String NomeRota
        {
            get
            {
                return (String)this["nomeRota"];
            }
            set
            {
                this["nomeRota"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the sufixo resultado.
        /// </summary>
        /// <value>
        /// The sufixo resultado.
        /// </value>
        [ConfigurationProperty("sufixoResultado", DefaultValue = "", IsRequired = true)]
        public String SufixoResultado
        {
            get
            {
                return (String)this["sufixoResultado"];
            }
            set
            {
                this["sufixoResultado"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [possui interface].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [possui interface]; otherwise, <c>false</c>.
        /// </value>
        [ConfigurationProperty("possuiInterfaceAtlasLink", DefaultValue = "false", IsRequired = true)]
        public bool PossuiInterface
        {
            get
            {
                return (bool)this["possuiInterfaceAtlasLink"];
            }
            set
            {
                this["possuiInterfaceAtlasLink"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the sufixo cliente.
        /// </summary>
        /// <value>
        /// The sufixo cliente.
        /// </value>
        [ConfigurationProperty("sufixoCliente", DefaultValue = "", IsRequired = true)]
        public String SufixoCliente
        {
            get
            {
                return (String)this["sufixoCliente"];
            }
            set
            {
                this["sufixoCliente"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the sufixo pas.
        /// </summary>
        /// <value>
        /// The sufixo pas.
        /// </value>
        [ConfigurationProperty("sufixoPas", DefaultValue = "", IsRequired = true)]
        public String SufixoPas
        {
            get
            {
                return (String)this["sufixoPas"];
            }
            set
            {
                this["sufixoPas"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the local folder.
        /// </summary>
        /// <value>
        /// The local folder.
        /// </value>
        [ConfigurationProperty("localFolder", DefaultValue = "", IsRequired = true)]
        public String LocalFolder
        {
            get
            {
                return (String)this["localFolder"];
            }
            set
            {
                this["localFolder"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the timeOut pending.
        /// </summary>
        /// <value>
        /// The timeOut pending.
        /// </value>
        [ConfigurationProperty("timeoutPending", DefaultValue = "5", IsRequired = true)]
        public Int64 TimeoutPending
        {
            get
            {
                return (Int64)this["timeoutPending"];
            }
            set
            {
                this["timeoutPending"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the timeOut sem mensagens.
        /// </summary>
        /// <value>
        /// The timeOut sem mensagens.
        /// </value>
        [ConfigurationProperty("timeoutSemMensagens", DefaultValue = "60", IsRequired = true)]
        public int TimeoutSemMensagens
        {
            get
            {
                return (int)this["timeoutSemMensagens"];
            }
            set
            {
                this["timeoutSemMensagens"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the timeOut atualizacao.
        /// </summary>
        /// <value>
        /// The timeOut atualizacao.
        /// </value>
        [ConfigurationProperty("timeoutAtualizacao", DefaultValue = "10000", IsRequired = true)]
        public int TimeoutAtualizacao
        {
            get
            {
                return (int)this["timeoutAtualizacao"];
            }
            set
            {
                this["timeoutAtualizacao"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [servidor atlas link remoto].
        /// </summary>
        /// <value>
        /// <c>true</c> if [servidor atlas link remoto]; otherwise, <c>false</c>.
        /// </value>
        [ConfigurationProperty("servidorAtlasLinkRemoto", DefaultValue = "false", IsRequired = true)]
        public bool ServidorAtlasLinkRemoto
        {
            get
            {
                return (bool)this["servidorAtlasLinkRemoto"];
            }
            set
            {
                this["servidorAtlasLinkRemoto"] = value;
            }
        }
    }

    #endregion

    #region ConnectionElement
    /// <summary>
    /// ConnectionElement
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationElement" />
    public class ConnectionElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the name of the host.
        /// </summary>
        /// <value>
        /// The name of the host.
        /// </value>
        [ConfigurationProperty("servidor", DefaultValue = "", IsRequired = true)]
        public String HostName
        {
            get
            {
                return (String)this["servidor"];
            }
            set
            {
                this["servidor"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the host port.
        /// </summary>
        /// <value>
        /// The host port.
        /// </value>
        [ConfigurationProperty("porta", DefaultValue = "8080", IsRequired = true)]
        public Int32 HostPort
        {
            get
            {
                return (Int32)this["porta"];
            }
            set
            {
                this["porta"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is encrypted.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is encrypted; otherwise, <c>false</c>.
        /// </value>
        [ConfigurationProperty("usaCriptografia", DefaultValue = "true", IsRequired = true)]
        public Boolean IsEncrypted
        {
            get
            {
                return (Boolean)this["usaCriptografia"];
            }
            set
            {
                this["usaCriptografia"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the polling read delay.
        /// </summary>
        /// <value>
        /// The polling read delay.
        /// </value>
        [ConfigurationProperty("tempoEsperaChamadasRede", DefaultValue = "2000", IsRequired = false)]
        public Int32 PollingReadDelay
        {
            get
            {
                return (Int32)this["tempoEsperaChamadasRede"];
            }
            set
            {
                this["tempoEsperaChamadasRede"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the fail over retry mins.
        /// </summary>
        /// <value>
        /// The fail over retry mins.
        /// </value>
        [ConfigurationProperty("tentativasChamadasFalhaDeRede", DefaultValue = "10", IsRequired = false)]
        public Int32 FailOverRetryMins
        {
            get
            {
                return (Int32)this["tentativasChamadasFalhaDeRede"];
            }
            set
            {
                this["tentativasChamadasFalhaDeRede"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the default server connection timeOut.
        /// </summary>
        /// <value>
        /// The default server connection timeOut.
        /// </value>
        [ConfigurationProperty("timeoutDefault", DefaultValue = "200000", IsRequired = false)]
        public Int32 DefaultServerConnectionTimeout
        {
            get
            {
                return (Int32)this["timeoutDefault"];
            }
            set
            {
                this["timeoutDefault"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the extended server connection timeOut.
        /// </summary>
        /// <value>
        /// The extended server connection timeOut.
        /// </value>
        [ConfigurationProperty("timeoutConexaoServidor", DefaultValue = "400000", IsRequired = false)]
        public Int32 ExtendedServerConnectionTimeout
        {
            get
            {
                return (Int32)this["timeoutConexaoServidor"];
            }
            set
            {
                this["timeoutConexaoServidor"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [client load balancing enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [client load balancing enabled]; otherwise, <c>false</c>.
        /// </value>
        [ConfigurationProperty("loadbalanceHabilitado", DefaultValue = "false", IsRequired = false)]
        public Boolean ClientLoadBalancingEnabled
        {
            get
            {
                return (Boolean)this["loadbalanceHabilitado"];
            }
            set
            {
                this["loadbalanceHabilitado"] = value;
            }
        }

    }
    #endregion

    #region LogElement
    /// <summary>
    /// ConnectionElement
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationElement" />
    public class LogElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the name of the host.
        /// </summary>
        /// <value>
        /// The name of the host.
        /// </value>
        [ConfigurationProperty("arquivoConfig", DefaultValue = "", IsRequired = false)]
        public String ArquivoConfig
        {
            get
            {
                return (String)this["arquivoConfig"];
            }
            set
            {
                this["arquivoConfig"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the host.
        /// </summary>
        /// <value>
        /// The name of the host.
        /// </value>
        [ConfigurationProperty("arquivoLog", DefaultValue = "", IsRequired = false)]
        public String ArquivoLog
        {
            get
            {
                return (String)this["arquivoLog"];
            }
            set
            {
                this["arquivoLog"] = value;
            }
        }

    }
    #endregion

    #region AtlasLinkSection
    /// <summary>
    /// ReconLightSection
    /// </summary>
    /// <seealso cref="System.Configuration.ConfigurationSection" />
    public class AtlasLinkSection : ConfigurationSection
    {
        #region Connection
        /// <summary>
        /// Gets or sets the connection.
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        [ConfigurationProperty("conexao")]
        public ConnectionElement Connection
        {
            get
            {
                return (ConnectionElement)this["conexao"];
            }
            set
            { this["conexao"] = value; }
        }
        #endregion

        #region Credentials


        /// <summary>
        /// Gets or sets the credentials.
        /// </summary>
        /// <value>
        /// The credentials.
        /// </value>
        [ConfigurationProperty("credenciais")]
        public CredentialsElement Credentials
        {
            get
            {
                return (CredentialsElement)this["credenciais"];
            }
            set
            { this["credenciais"] = value; }
        }
        #endregion


        #region FolderCredentials
        /// <summary>
        /// Gets or sets the folder credentiais.
        /// </summary>
        /// <value>
        /// The folder credentiais.
        /// </value>
        [ConfigurationProperty("credenciaisDiretorio")]
        public FolderCredentialsElement FolderCredentials
        {
            get
            {
                return (FolderCredentialsElement)this["credenciaisDiretorio"];
            }
            set
            { this["credenciaisDiretorio"] = value; }
        }
        #endregion

        #region Log
        /// <summary>
        /// Gets or sets the folder credentiais.
        /// </summary>
        /// <value>
        /// The folder credentiais.
        /// </value>
        [ConfigurationProperty("log")]
        public LogElement Log
        {
            get
            {
                return (LogElement)this["log"];
            }
            set
            { this["log"] = value; }
        }
        #endregion

        #region ReconLight

        /// <summary>
        /// Gets or sets the recon light.
        /// </summary>
        /// <value>
        /// The recon light.
        /// </value>
        [ConfigurationProperty("reconLight")]
        public AtlasLinkInterfaceElement ReconLight
        {
            get
            {
                return (AtlasLinkInterfaceElement)this["reconLight"];
            }
            set
            { this["reconLight"] = value; }
        }
        #endregion

        #region Efinanceira
        /// <summary>
        /// Gets or sets the efinanceira.
        /// </summary>
        /// <value>
        /// The efinanceira.
        /// </value>
        [ConfigurationProperty("eFinanceira")]
        public AtlasLinkInterfaceElement Efinanceira
        {
            get
            {
                return (AtlasLinkInterfaceElement)this["eFinanceira"];
            }
            set
            { this["eFinanceira"] = value; }
        } 
        #endregion

    }
    #endregion
}