﻿using System;
using System.Collections.Generic;
using Atlas.Link;
using Atlas.Link.Settings;

namespace Atlas.Presentation.Settings
{
    /// <summary>
    /// Concrete implementation of application settings.
    /// </summary>
    public class ApplicationSettingsProvider : Atlas.Link.Settings.IApplicationSettings
    {
        internal static String ApplicationConfigName = "application.config";
        internal static String MainFormConfigName = "main-form.config";
        internal static String UserSettingsConfigName = "user-settings.config";

        protected static Common.Logging.ILog Logger = Common.Logging.LogManager.GetLogger(typeof( ApplicationSettingsProvider));

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettingsProvider" /> class.
        /// </summary>
        public ApplicationSettingsProvider()
        {
            _settingsStore = ServiceLocator.Get<ISettingsStorageProvider>();
        }

        #endregion

        #region Fields

        private Atlas.Link.Settings.ISettingsStorageProvider _settingsStore;

        #endregion

        #region Application Configuration

        private Atlas.Link.Settings.AtlasLinkSection _applicationConfig = null;

        /// <summary>
        /// Determines whether the application configuration file exists.
        /// </summary>
        /// <value>
        /// The application configuration exists.
        /// </value>
        public Boolean ApplicationConfigExists
        {
            get { return _settingsStore.AppSettingsExists(ApplicationConfigName); }
        }

        /// <summary>
        /// Gets the application configuration.
        /// </summary>
        /// <returns></returns>
        public AtlasLinkSection GetApplicationConfig()
        {
            if (_applicationConfig == null)
            {
                try
                {
                    var config = (AtlasLinkSection)System.Configuration.ConfigurationManager.GetSection("atlasLinkGroup/atlasLink");

                    if (config == null)
                    {
                        Logger.Error("Unable to read atlas link server connection properties");
                        throw new RuntimeException("Error.ApplicationConfigRead", "Unable to read atlas link server connection properties");
                    }
                    
                    Logger.Debug(string.Format("setting: {0}/{1}" ,config.Connection.HostName, config.Connection.HostPort));

                    // attempt to get the configuration instance from storage
                    _applicationConfig = config;
                }
                catch (Exception)
                {
                    Logger.Error("Unable to read the application config file: exception");
                    throw new RuntimeException("Error.ApplicationConfigRead", "Unable to read the application config file: {0}", ApplicationConfigName);
                }

                if (_applicationConfig == null)
                {
                    Logger.Error("Unable to read the application config file: _applicationConfig is null");
                    throw new RuntimeException("Error.ApplicationConfigMissing", "Unable to find the application config file: {0}", ApplicationConfigName);
                }
            }

            Logger.Debug(string.Format("returning: {0}/{1}", _applicationConfig.Connection.HostName, _applicationConfig.Connection.HostPort));

            // return a clone of the Config Settings in case any code attempts a global modification
            return (_applicationConfig);
        }
        /// <summary>
        /// Saves the application configuration.
        /// </summary>
        /// <param name="config">The configuration.</param>
        public void SetApplicationConfig(Atlas.Link.Settings.AtlasLinkSection config)
        {
            // cache copy in memory
            _applicationConfig = config;

            // save to disk
            _settingsStore.SaveAppSettings<Atlas.Link.Settings.AtlasLinkSection>(ApplicationConfigName, config);
        }

        #endregion
    }
}