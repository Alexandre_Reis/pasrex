﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Settings
{
    public interface IApplicationConfig
    {
        String HostName { get; }

        Int32 HostPort { get; }

        List<NamedNetworkHost> ServiceHosts { get; }

        Int32 PollingReadDelay { get; }

        String UILocale { get; }

        String UITheme { get; }

        Int32 DefaultServerConnectionTimeout { get; }

        Int32 ExtendedServerConnectionTimeout { get; }
    }
}