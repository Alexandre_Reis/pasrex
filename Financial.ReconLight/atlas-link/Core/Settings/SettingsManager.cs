﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Settings
{
    /// <summary>
    /// Application Settings manager
    /// </summary>
    public static class SettingsManager
    {
        #region Properties

        /// <summary>
        /// Gets or sets the current application settings
        /// </summary>
        /// <value>
        /// The current.
        /// </value>
        public static IApplicationSettings Current
        {
            get
            {
                if (_current == null)
                    _current = ServiceLocator.Get<IApplicationSettings>();

                return _current;
            }
            set { _current = value; }
        }
        private static IApplicationSettings _current;

        #endregion
    }
}