﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Settings
{
    /// <summary>
    /// Provides an interface for persisting application settings.
    /// </summary>
    public interface ISettingsStorageProvider
    {
        /// <summary>
        /// Determines whether the application settings exists for the given name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        Boolean AppSettingsExists(string name);

        /// <summary>
        /// Saves the application setting.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="name">The name.</param>
        /// <param name="instance">The instance.</param>
        void SaveAppSettings<TSetting>(string name, TSetting instance) where TSetting : class;
        /// <summary>
        /// Reads the previously saved application settings.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        TSetting ReadAppSettings<TSetting>(string name) where TSetting: class;

        /// <summary>
        /// Determines whether the user settings exists for the given name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        Boolean UserSettingsExists(string name);

        /// <summary>
        /// Saves the user settings.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="name">The name.</param>
        /// <param name="instance">The instance.</param>
        void SaveUserSettings<TSetting>(string name, TSetting instance) where TSetting : class;

        /// <summary>
        /// Reads the user settings.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        TSetting ReadUserSettings<TSetting>(string name) where TSetting : class;

    }
}