﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Atlas.Link.Settings
{
    /// <summary>
    /// A SettingsProvider for persisting application settings to
    /// the local disk.
    /// </summary>
    public class FileSettingsProvider : ISettingsStorageProvider, IDisposable
    {
        #region types

        internal class SettingDetails
        {
            public String FileName = "";
            public Object Data = null;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSettingsProvider"/> class.
        /// </summary>
        public FileSettingsProvider()
        {
            //// cache existing settings
            //foreach (String fileName in System.IO.Directory.GetFiles(AppEnvironment.UserDataSettingsPath, "*" + SettingExt))
            //{
            //    String key = System.IO.Path.GetFileNameWithoutExtension(System.IO.Path.GetFileName(fileName)).ToLower();
            //    _cache.Add(key, new SettingDetails { FileName = fileName });
            //}

            _emptyNamespace = new System.Xml.Serialization.XmlSerializerNamespaces();
            _emptyNamespace.Add("", "");
        }

        #endregion

        #region Fields

        //private Dictionary<String, SettingDetails> _cache = new Dictionary<String, SettingDetails>();
        private XmlSerializerNamespaces _emptyNamespace;

        #endregion

        #region ISettingsProvider Members

        private static String GetFullAppSettingFileName(String fileName)
        {
            return System.IO.Path.Combine(AppEnvironment.AppDataConfigPath, fileName);
        }
        private static String GetFullUserSettingFileName(String fileName)
        {
            return System.IO.Path.Combine(AppEnvironment.UserDataSettingsPath, fileName);
        }

        private void InternalSaveSettings<TSetting>(string fileName, TSetting instance) where TSetting : class
        {
            using (var fileStream = System.IO.File.CreateText(fileName))
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(TSetting));
                serializer.Serialize(fileStream, instance, _emptyNamespace);
            }
        }
        private TSetting InternalReadSettings<TSetting>(string fileName) where TSetting : class
        {
            TSetting result = null;

            if (System.IO.File.Exists(fileName))
            {
                using (var fileStream = System.IO.File.OpenText(fileName))
                {
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(TSetting));
                    result = (TSetting)serializer.Deserialize(fileStream);
                }
            }

            return result;
        }


        /// <summary>
        /// Determines whether the application settings exists for the given name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public Boolean AppSettingsExists(string name)
        {
            return System.IO.File.Exists(GetFullAppSettingFileName(name));
        }

        /// <summary>
        /// Determines whether the user settings exists for the given name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public Boolean UserSettingsExists(string name)
        {
            return System.IO.File.Exists(GetFullUserSettingFileName(name));
        }

        /// <summary>
        /// Saves the settings.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="name">The name.</param>
        /// <param name="instance">The instance.</param>
        public void SaveAppSettings<TSetting>(string name, TSetting instance) where TSetting : class
        {
            System.Diagnostics.Contracts.Contract.Requires(String.IsNullOrWhiteSpace(name) == false, "name is required");
            System.Diagnostics.Contracts.Contract.Requires(instance != null, "instance is required");

            InternalSaveSettings<TSetting>(GetFullAppSettingFileName(name), instance);
        }
        /// <summary>
        /// Reads the previously saved settings.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public TSetting ReadAppSettings<TSetting>(string name) where TSetting : class
        {
            System.Diagnostics.Contracts.Contract.Requires(String.IsNullOrWhiteSpace(name) == false, "name is required");

            return InternalReadSettings<TSetting>(GetFullAppSettingFileName(name));
        }

        /// <summary>
        /// Saves the user settings.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="name">The name.</param>
        /// <param name="instance">The instance.</param>
        public void SaveUserSettings<TSetting>(string name, TSetting instance) where TSetting : class
        {
            System.Diagnostics.Contracts.Contract.Requires(String.IsNullOrWhiteSpace(name) == false, "name is required");
            System.Diagnostics.Contracts.Contract.Requires(instance != null, "instance is required");

            InternalSaveSettings<TSetting>(GetFullUserSettingFileName(name), instance);
        }
        /// <summary>
        /// Reads the user settings.
        /// </summary>
        /// <typeparam name="TSetting">The type of the setting.</typeparam>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public TSetting ReadUserSettings<TSetting>(string name) where TSetting : class
        {
            System.Diagnostics.Contracts.Contract.Requires(String.IsNullOrWhiteSpace(name) == false, "name is required");

            return InternalReadSettings<TSetting>(GetFullUserSettingFileName(name));
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            //TODO: flush writes to disk
        }

        #endregion
    }
}