﻿using System;

namespace Atlas.Link.Wpf.Behaviour
{
    /// <summary>
    /// Implemented by ViewModels that want the DataContext target to delay
    /// visual updates until all source data has been updated (batched data update).
    /// </summary>
    public interface IBatchedDataUpdate
    {
        /// <summary>
        /// Occurs when data-update is about to begin.
        /// </summary>
        event EventHandler DataUpdateStarted;
        /// <summary>
        /// Occurs when data-update has finished.
        /// </summary>
        event EventHandler DataUpdateFinished;
    }
}