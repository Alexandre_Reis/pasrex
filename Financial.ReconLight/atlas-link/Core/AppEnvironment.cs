﻿using Atlas.Link.Settings;
using System;
using System.Diagnostics;

namespace Atlas.Link
{
    /// <summary>
    /// The singleton class that contains the runtime Application Environment values.
    /// </summary>
    public static class AppEnvironment
    {
        #region Static Constructors

        static AppEnvironment()
        {
            var config = (AtlasLinkSection)System.Configuration.ConfigurationManager.GetSection("atlasLinkGroup/atlasLink");

            AppEnvironment.AppDataLogPath = config.Log.ArquivoLog;

            // ensure the directories exist
            ForceDirectory(AppEnvironment.AppDataLogPath);
        }

        #endregion

        #region Static Properties

        public static Boolean IsDesignMode { get; private set; }

        /// <summary>
        /// Gets the full name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
        public static String CompanyName { get; private set; }
        /// <summary>
        /// Gets the company code used for path names.
        /// </summary>
        /// <value>
        /// The company code.
        /// </value>
        public static String CompanyCode { get; private set; }
        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public static String ProductName { get; private set; }
        /// <summary>
        /// Gets the product code used for path names.
        /// </summary>
        /// <value>
        /// The product code.
        /// </value>
        public static String ProductCode { get; private set; }
        /// <summary>
        /// Gets the full path of the entry point assembly.
        /// </summary>
        /// <value>
        /// Gets the full path of the entry point assembly.
        /// </value>
        public static String RuntimeExe { get; private set; }
        /// <summary>
        /// Gets the directory of the entry point assembly.
        /// </summary>
        /// <value>
        /// Gets the directory of the entry point assembly.
        /// </value>
        public static String RuntimePath { get; private set; }
        /// <summary>
        /// Gets the runtime version.
        /// Used by the VLR for assembly binding.
        /// </summary>
        /// <value>
        /// The assembly version for the entry point exe.
        /// </value>
        public static String RuntimeVersion { get; private set; }
        /// <summary>
        /// Gets the compile version.
        /// Used by the build system to indicate build information.
        /// </summary>
        /// <value>
        /// The win32 file version for the entry point exe.
        /// </value>
        public static String CompileVersion { get; private set; }
        /// <summary>
        /// Gets the product version.
        /// Used to document the product for end-user display.
        /// </summary>
        /// <value>
        /// The product version.
        /// </value>
        public static String ProductVersion { get; private set; }
        /// <summary>
        /// Gets the root application data path.
        /// Application data is shared between user accounts.
        /// </summary>
        /// <value>
        /// The root application data path.
        /// </value>
        public static String AppDataPath { get; private set; }
        /// <summary>
        /// Gets the application configuration path.
        /// Configuration files are stored here.
        /// </summary>
        /// <value>
        /// The application configuration path.
        /// </value>
        public static String AppDataConfigPath { get; private set; }
        /// <summary>
        /// Gets the application log path.
        /// Log files are written here.
        /// </summary>
        /// <value>
        /// The application log path.
        /// </value>
        public static String AppDataLogPath { get; private set; }
        /// <summary>
        /// Gets the root user data path.
        /// User data is private for a user account.
        /// </summary>
        /// <value>
        /// The root user data path.
        /// </value>
        public static String UserDataPath { get; private set; }
        /// <summary>
        /// Gets the user settings path.
        /// User settings are stored here.
        /// </summary>
        /// <value>
        /// The user settings path.
        /// </value>
        public static String UserDataSettingsPath { get; private set; }
        /// <summary>
        /// Provides a full path to the LogViewer. This can be overridden in config.
        /// </summary>
        /// <value>
        /// The log viewer path.
        /// </value>
        public static String LogViewerPath { get; private set; }


        public static String UserDataLayoutPath { get; private set; }

        #endregion

        #region Utility Members

        public static void ForceDirectory(String directoryPath)
        {
            if (System.IO.Directory.Exists(directoryPath) == false)
                System.IO.Directory.CreateDirectory(directoryPath);
        }

        #endregion
    }
}