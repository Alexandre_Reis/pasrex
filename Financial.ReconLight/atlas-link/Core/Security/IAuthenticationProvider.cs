﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Atlas.Link.Security
{
    public interface IAuthenticationProvider
    {
        /// <summary>
        /// Begins asynchronous authentication.
        /// </summary>
        /// <returns></returns>
        WaitHandle BeginAuthenticate();

        /// <summary>
        /// Gets the has authentication error.
        /// </summary>
        /// <value>
        /// The has authentication error.
        /// </value>
        Boolean HasAuthenticationError { get; }

        /// <summary>
        /// Gets the current principal.
        /// </summary>
        /// <value>
        /// The principal.
        /// </value>
        IAtlasPrincipal Principal { get; }
    }
}