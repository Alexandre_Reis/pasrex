﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Security
{
    /// <summary>
    /// The security princcipal for Atlas applications.
    /// </summary>
    public interface IAtlasPrincipal : System.Security.Principal.IPrincipal
    {
        /// <summary>
        /// Gets the type of the authentication.
        /// </summary>
        /// <value>
        /// The type of the authentication.
        /// </value>
        String AuthenticationType { get; }

        /// <summary>
        /// Determines whether the principal is authenticated.
        /// </summary>
        /// <value>
        /// The is authenticated.
        /// </value>
        Boolean IsAuthenticated { get; }

        /// <summary>
        /// Gets the name of the principal
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        String Name { get; }

        /// <summary>
        /// Gets the login of the principal
        /// </summary>
        /// <value>
        /// The login.
        /// </value>
        String Login { get; }

        /// <summary>
        /// Determines whether the current principal has a permission.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        Boolean HasPermission(String code);

        /// <summary>
        /// Determines whether the current principal has a set of permissions.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        Boolean HasPermissions(IEnumerable<String> code);

    }
}