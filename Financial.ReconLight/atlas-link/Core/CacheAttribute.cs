﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link
{
    /// <summary>
    /// Instructs factory and service classes to cache the class that implements this attribute.
    /// </summary>
    [SerializableAttribute]
    [AttributeUsageAttribute(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class CacheAttribute : Attribute
    {
        #region Construction

        public CacheAttribute()
        {
            //
        }

        #endregion
    }
}