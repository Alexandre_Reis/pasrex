﻿using System;
using System.Text;
using System.Xml;

// ReSharper disable once CheckNamespace
namespace Atlas.Link
{
    /// <summary>
    /// Provides a standard way for writing to and reading from user storage.
    /// </summary>
    public static class LocalStorage
    {
        /// <summary>
        /// Serializes the instance to an XML format.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="instance">The instance.</param>
        public static void SaveXml<T>(String fileName, T instance) where T : class
        {
            using (var fileStream = System.IO.File.Create(fileName))
            {
                var writer = new XmlTextWriter(fileStream, Encoding.UTF8)
                {
                    Formatting = Formatting.Indented,
                    Indentation = 4
                };

                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

                serializer.Serialize(writer, instance);
            }
        }
        /// <summary>
        /// Deserializes the file from an XML format.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public static T ReadXml<T>(String fileName) where T : class
        {
            T result = null;

            if (System.IO.File.Exists(fileName))
            {
                using (var fileStream = System.IO.File.OpenRead(fileName))
                {
                    var xmlReader = new XmlTextReader(fileStream);
                    var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

                    result = (T)serializer.Deserialize(xmlReader);
                }
            }

            return result;
        }
        /// <summary>
        /// Deserializes the stream from an XML format.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream">The file stream.</param>
        /// <returns></returns>
        public static T ReadStream<T>(System.IO.Stream stream) where T : class
        {
            var xmlReader = new XmlTextReader(stream);
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

            return (T)serializer.Deserialize(xmlReader);
        }
        /// <summary>
        /// Deserializes the file from an XML format. Creates a ne winstance of the file does not exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="createOnException">if set to <c>true</c> [create on exception].</param>
        /// <returns></returns>
        public static T ReadOrCreateXml<T>(String fileName, Boolean createOnException = false) where T : class, new()
        {
            T result;

            try
            {
                if (System.IO.File.Exists(fileName))
                {
                    using (var fileStream = System.IO.File.OpenRead(fileName))
                    {
                        var xmlReader = new XmlTextReader(fileStream);
                        var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

                        result = (T)serializer.Deserialize(xmlReader);
                    }
                }
                else
                {
                    result = new T();
                }
            }
            catch (Exception)
            {
                if (createOnException)
                    result = new T();
                else
                    throw;
            }

            return result;
        }
    }
}