﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.IO
{
    /// <summary>
    /// Represents a Cell of a delimited text file.
    /// </summary>
    public class TextFileField
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="TextFileField"/> class.
        /// </summary>
        /// <param name="valueHandler">The value handler.</param>
        public TextFileField(Int32 offset, Int32 length, Func<Int32, Int32, String> valueHandler)
        {
            this.Offset = offset;
            this.Length = length;
            _valueHandler = valueHandler;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the cell offset.
        /// </summary>
        /// <value>
        /// The cell offset.
        /// </value>
        public Int32 Offset { get; private set; }
        /// <summary>
        /// Gets or sets the length of the cell.
        /// </summary>
        /// <value>
        /// The length of the cell.
        /// </value>
        public Int32 Length { get; private set; }

        /// <summary>
        /// Gets the field value.
        /// </summary>
        /// <value>
        /// The field value.
        /// </value>
        public String Value
        {
            get { return _valueHandler(this.Offset, this.Length); }
        }
        private Func<Int32, Int32, String> _valueHandler;

        #endregion

        #region Overrides

        private static Int32 Sequence = 0;
        private Int32 _hasKey = System.Threading.Interlocked.Increment(ref Sequence);

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return _hasKey;
        }
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return obj != null &&
                   obj.GetType() == this.GetType() &&
                   obj.GetHashCode() == this.GetHashCode();
        }
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return String.Format("Offset={0}, Length={1}, Value={2}", this.Offset, this.Length, this.Value);
        }

        #endregion
    }

    /// <summary>
    /// Represents a Column Index of a delimited text file.
    /// </summary>
    public class TextFileRow : List<TextFileField>
    {
        #region Overrides

        private static Int32 Sequence = 0;
        private Int32 _hasKey = System.Threading.Interlocked.Increment(ref Sequence);

        public override int GetHashCode()
        {
            return _hasKey;
        }
        public override bool Equals(object obj)
        {
            return obj != null &&
                   obj.GetType() == this.GetType() &&
                   obj.GetHashCode() == this.GetHashCode();
        }

        #endregion
    }

    /// <summary>
    /// Represents a Row Index of a delimited text file.
    /// </summary>
    public class TextFileRows : List<TextFileRow>
    {
        #region Overrides

        private static Int32 Sequence = 0;
        private Int32 _hasKey = System.Threading.Interlocked.Increment(ref Sequence);

        public override int GetHashCode()
        {
            return _hasKey;
        }
        public override bool Equals(object obj)
        {
            return obj != null &&
                   obj.GetType() == this.GetType() &&
                   obj.GetHashCode() == this.GetHashCode();
        }

        #endregion
    }
}