﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Atlas.Link.Model;

namespace Atlas.Link.Client
{
    public interface IClientFactory
    {
        IPollClient<TEntity> GetPollClient<TEntity>() where TEntity : BaseAuditableEntity;
        IUpdateClient<TEntity> GetUpdateClient<TEntity>() where TEntity : BaseAuditableEntity;

        void Register(Assembly assembly);
    }

    public class ClientFactory : IClientFactory
    {
        #region Type cache

        private Dictionary<Type, Type> _pollingClients = new Dictionary<Type, Type>();
        private Dictionary<Type, Type> _updateClients = new Dictionary<Type, Type>();

        #endregion

        #region IClientFactory Members

        public IPollClient<TEntity> GetPollClient<TEntity>() where TEntity : BaseAuditableEntity
        {
            Type clientType;

            if (_pollingClients.TryGetValue(typeof(TEntity), out clientType) == false)
                return null;

            Object clientInstance = Activator.CreateInstance(clientType);

            return (IPollClient<TEntity>)clientInstance;
        }
        public IUpdateClient<TEntity> GetUpdateClient<TEntity>() where TEntity : BaseAuditableEntity
        {
            Type clientType;

            if (_updateClients.TryGetValue(typeof(TEntity), out clientType) == false)
                return null;

            Object clientInstance = Activator.CreateInstance(clientType);

            return (IUpdateClient<TEntity>)clientInstance;
        }
        public void Register(Assembly assembly)
        {
            Type pollInterfaceType = typeof(IPollClient<>);
            Type updateInterfaceType = typeof(IUpdateClient<>);

            foreach (Type concreteType in assembly.GetTypes().Where(t => t.IsClass && t.IsAbstract == false && t.IsInterface == false))
            {
                Type[] interfaces = concreteType.GetInterfaces();

                foreach (Type intfType in interfaces)
                {
                    if (intfType.IsGenericType)
                    {
                        Type genericTypeDef = intfType.GetGenericTypeDefinition();

                        if (genericTypeDef == pollInterfaceType)
                        {
                            Type[] genericArgs = intfType.GetGenericArguments();

                            if ((genericArgs.Length == 1) && (genericArgs[0].IsSubclassOf(typeof(BaseAuditableEntity))))
                            {
                                Type entityType = genericArgs[0];

                                _pollingClients.Add(entityType, concreteType);
                            }
                        }

                        if (genericTypeDef == updateInterfaceType)
                        {
                            Type[] genericArgs = intfType.GetGenericArguments();

                            if ((genericArgs.Length == 1) && (genericArgs[0].IsSubclassOf(typeof(BaseAuditableEntity))))
                            {
                                Type entityType = genericArgs[0];

                                _updateClients.Add(entityType, concreteType);
                            }
                        }


                    }
                }
            }
        }

        #endregion
    }
}