﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Atlas.Link.Net
{
    /// <summary>
    /// Socket Utilities.
    /// </summary>
    public class SocketUtils
    {
        /// <summary>
        /// Determines whether the requested host is listening on the specified port.
        /// </summary>
        /// <param name="host">The host name or address.</param>
        /// <param name="port">The port number.</param>
        /// <returns></returns>
        public static Boolean IsTcpServerListening(String host, Int32 port)
        {
            Boolean result = true;

            try
            {
                Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sock.NoDelay = true;
                sock.Connect(host, port);
                sock.Close();
            }
            catch (System.Net.Sockets.SocketException)
            {
                result = false;
                //A first chance exception of type 'System.Net.Sockets.SocketException' occurred in System.dll
                //FAILED: 10061
                //Elapsed = 00:00:01.0924525
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Determines whether the host is in IpV6 address format.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <returns></returns>
        public static Boolean IsHostIpV6Address(String host)
        {
            //var ht = Uri.CheckHostName(hostAddress);

            IPAddress address;

            if (IPAddress.TryParse(host, out address))
            {
                return (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6);
            }

            return false;
        }

        /// <summary>
        /// Builds the HTTP URL.
        /// </summary>
        /// <param name="host">The host.</param>
        /// <param name="port">The port.</param>
        /// <param name="isEncrypted">The is encrypted.</param>
        /// <returns></returns>
        public static Uri BuildHttpUrl(String host, Int32 port, Boolean isEncrypted)
        {
            UriBuilder builder = new UriBuilder();
            builder.Scheme = (isEncrypted) ? Uri.UriSchemeHttps : Uri.UriSchemeHttp;
            builder.Host = host;
            builder.Port = port;

            return builder.Uri;
        }
    }
}