﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Net
{
    public class RestSession : ISession
    {
        public String Login { get; set; }
        public String Password { get; set; }
        public String SessionID { get; set; }
    }
}