﻿using System;
using System.Collections.Generic;

namespace Atlas.Link.Net
{
    /// <summary>
    /// Proves server connection details for network clients.
    /// </summary>
    public interface IConnectionProvider
    {
        /// <summary>
        /// Gets a list of available servers.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <returns></returns>
        IList<IServer> GetServers(String serviceName);

        /// <summary>
        /// Registers the server.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="hostName">Name of the host.</param>
        void RegisterServer(String serviceName, String hostName);
        /// <summary>
        /// Registers the server.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="hostName">Name of the host.</param>
        /// <param name="hostPort">The host port.</param>
        void RegisterServer(String serviceName, String hostName, Int32 hostPort);

        /// <summary>
        /// Unregisters the server.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="hostName">Name of the host.</param>
        void UnregisterServer(String serviceName, String hostName);
        /// <summary>
        /// Unregisters the server.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="hostName">Name of the host.</param>
        /// <param name="hostPort">The host port.</param>
        void UnregisterServer(String serviceName, String hostName, Int32 hostPort);
    }
}