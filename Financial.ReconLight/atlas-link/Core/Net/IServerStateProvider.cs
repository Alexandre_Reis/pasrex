﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Net
{
    public interface IServerStateProvider
    {
        IServerStateInfo Info { get; }
        event ServerStateChanged StateChanged;
    }

    public delegate void ServerStateChanged(IServerStateInfo info);


}