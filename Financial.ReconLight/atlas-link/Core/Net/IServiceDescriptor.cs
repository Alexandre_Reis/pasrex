﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Net
{
    public interface IServiceDescriptor<TService>
    {
        TService GetService();
    }
}
