﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Net
{
    public interface IServerStateInfo
    {
        String Status { get; }
        String Environment { get; }
        String Version { get; }
        Boolean IsConnected { get; }
        Boolean GlobalRoutesEnabled { get; }
        DateTime ServerDate { get; }
        IList<String> ServerNodes { get; }
    }
}