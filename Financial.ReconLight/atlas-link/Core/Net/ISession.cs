﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Net
{
    public interface ISession
    {
        String Login { get; }
        String SessionID { get; }
    }
}