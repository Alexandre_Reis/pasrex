﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Net
{
    public interface IServer
    {
        Uri URI { get; }
        Boolean IsActive { get; }
        DateTime LastConnected { get; }

        void UpdatedLastConnected();
    }
}
