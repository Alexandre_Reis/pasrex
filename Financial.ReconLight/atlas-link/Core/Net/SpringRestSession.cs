﻿using System;
using System.Net;
using System.Text;

namespace Atlas.Link.Net
{
    /// <summary>
    /// The session variable store for REST calls
    /// </summary>
    public class SpringRestSession
    {
        #region Construction

        /// <summary>
        /// Gets the global session instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        public static SpringRestSession Instance
        {
            get { return _instance; }
        }
        private static SpringRestSession _instance = new SpringRestSession();

#if DEBUG
        private static Boolean _byPassCertValidation = false;
#endif

        private SpringRestSession()
        {
            _syncObject = new Object();

#if DEBUG
            //----------------------------------------------------------------------------------
            // IMPORTANT: 
            //      NEVER EVER PUT INTO PRODUCTION BUILDS
            //      USE THIS WHEN YOU WANT TO TEST SSL USING SELF-SIGNED CERTS
            //----------------------------------------------------------------------------------
            if (_byPassCertValidation == false)
            {
                _byPassCertValidation = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            }
#endif
        }

        #endregion

        #region Fields

        private Object _syncObject;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the login.
        /// </summary>
        /// <value>
        /// The login.
        /// </value>
        public String Login
        {
            get
            {
                String result;
                lock (_syncObject)
                {
                    result = _login;
                }
                return result;
            }
        }
        private String _login = "";

        /// <summary>
        /// Gets the authorization header.
        /// </summary>
        /// <value>
        /// The authorization header.
        /// </value>
        public String AuthorizationHeader
        {
            get
            {
                String result;
                lock (_syncObject)
                {
                    result = _authorizationHeader;
                }
                return result;
            }
        }
        private String _authorizationHeader = "";

        /// <summary>
        /// Gets the session header.
        /// </summary>
        /// <value>
        /// The session header.
        /// </value>
        public String SessionHeader
        {
            get
            {
                String result;
                lock (_syncObject)
                {
                    result = _sessionHeader;
                }
                return result;
            }
        }
        private String _sessionHeader = "";

        #endregion

        #region Methods

        /// <summary>
        /// Sets the credentials.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <param name="password">The password.</param>
        public void SetCredentials(String login, String password)
        {
            // perform BASIC realm encoding
            String authInfoText = String.Format("{0}:{1}", login, password);
            String authInfoEncoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(authInfoText));

            lock (_syncObject)
            {
                _login = login;
                _authorizationHeader = "Basic " + authInfoEncoded;
            }
        }

        /// <summary>
        /// Sets the session header.
        /// </summary>
        /// <param name="header">The header.</param>
        public void SetSessionHeader(String header)
        {
            lock (_syncObject)
            {
                _sessionHeader = header;
            }
        }

        #endregion
    }
}