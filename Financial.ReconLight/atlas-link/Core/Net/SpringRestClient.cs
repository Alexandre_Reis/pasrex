﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel.Web;
using Atlas.Link.Serialization;
using Spring.Http;
using Spring.Http.Client;
using Spring.IO;
using Spring.Rest.Client;

namespace Atlas.Link.Net
{
    /// <summary>
    /// A generic REST client based on Spring.Rest.
    /// </summary>
    /// <typeparam name="TService">The type of the service interface.</typeparam>
    public class SpringRestClient<TService> : IServiceDescriptor<TService> where TService : class // (actually must be interface)
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="SpringRestClient{TService}" /> class.
        /// </summary>
        public SpringRestClient() : this(Atlas.Link.Settings.SettingsManager.Current.GetApplicationConfig().Connection.DefaultServerConnectionTimeout) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpringRestClient{TService}" /> class.
        /// </summary>
        /// <param name="connectionTimeout">The server connection timeout.</param>
        public SpringRestClient(Int32 connectionTimeout)
        {
            if (Logger == null)
                Logger = Common.Logging.LogManager.GetLogger(this.GetType()); // closed generic type

            // validate input parameters
            System.Diagnostics.Contracts.Contract.Assert(typeof(TService).IsInterface, "Generic type ServiceIntf must be an interface");

            // get connection parameters
            _connectionTimeout = connectionTimeout;
            _connectionProvider = ServiceLocator.Get<Atlas.Link.Net.IConnectionProvider>();
            _session = SpringRestSession.Instance;
        }

        #endregion

        #region Meta Data Cache

        internal class HttpRequestBinding
        {
            public String ServiceName = "";
            public String Url = "";
            public HttpMethod Method = HttpMethod.GET;
        }

        private static Dictionary<Int32, HttpRequestBinding> _requestBindingCache = new Dictionary<Int32, HttpRequestBinding>();
        private static String UrlPathSep = "/";

        private static String ConcatUrlPath(String urlPath1, String urlPath2)
        {
            if (String.IsNullOrWhiteSpace(urlPath1) && String.IsNullOrWhiteSpace(urlPath2))
                return "";

            else if (String.IsNullOrWhiteSpace(urlPath2))
                return urlPath1;

            else if (String.IsNullOrWhiteSpace(urlPath1))
                return urlPath2;

            Boolean path1end = urlPath1.EndsWith(UrlPathSep);
            Boolean path2sta = urlPath2.StartsWith(UrlPathSep);

            if ((path1end == true) && (path2sta == true))
                return urlPath1 + urlPath2.Substring(1);

            else if ((path1end == false) && (path2sta == false))
                return urlPath1 + UrlPathSep + urlPath2;

            else
                return urlPath1 + urlPath2;
        }
        private static HttpRequestBinding GeHttpRequestBinding(MethodBase callerMethod)
        {
            Int32 callerID = callerMethod.MetadataToken;
            HttpRequestBinding response = null;

            lock (_requestBindingCache)
            {
                if (_requestBindingCache.TryGetValue(callerID, out response) == false)
                {
                    Object[] svcAttribs = callerMethod.DeclaringType.GetCustomAttributes(typeof(ServiceHostAttribute), false);
                    Object[] webAttribs = callerMethod.GetCustomAttributes(typeof(WebInvokeAttribute), true);

                    ServiceHostAttribute svcAttrib = svcAttribs.OfType<ServiceHostAttribute>().FirstOrDefault();
                    WebInvokeAttribute webAttrib = webAttribs.OfType<WebInvokeAttribute>().FirstOrDefault();

                    if (webAttrib == null)
                        throw new Exception(String.Format("Method {0}.{1} does not have an associated WebInvokeAttribute", callerMethod.DeclaringType.FullName, callerMethod.Name));

                    String serviceName = (svcAttrib != null) ? svcAttrib.ServiceName : "";
                    String baseURL = (svcAttrib != null) ? svcAttrib.BaseURL : "";
                    String fullURL = ConcatUrlPath(baseURL, webAttrib.UriTemplate);

                    response = new HttpRequestBinding
                    {
                        ServiceName = serviceName,
                        Url = fullURL,
                        Method = new HttpMethod(webAttrib.Method),
                    };

                    _requestBindingCache.Add(callerID, response);
                }
            }

            return response;
        }

        #endregion

        #region Fields

        private readonly Int32 _connectionTimeout;
        private readonly IConnectionProvider _connectionProvider;
        private readonly SpringRestSession _session;

        #endregion

        #region Properties

        protected static Common.Logging.ILog Logger { get; private set; }

        protected SpringRestSession Session
        {
            get { return _session; }
        }

        #endregion

        #region Utility Members

        /// <summary>
        /// Gets the rest template.
        /// </summary>
        /// <returns></returns>
        protected RestTemplate GetRestTemplate()
        {
            var requestFactory = new WebClientHttpRequestFactory();
            requestFactory.Timeout = _connectionTimeout;
            requestFactory.AutomaticDecompression = System.Net.DecompressionMethods.GZip;

            // create the Spring REST client
            var template = new RestTemplate();
            template.RequestFactory = requestFactory;

            // add our own serializer to properly deserialize generic list result types
            template.MessageConverters.Add(new LeanXmlSerializableHttpMessageConverter());

            // add our own interceptor into the pipeline to parse and send session state
            template.RequestInterceptors.Add(new SessionStateInterceptor(_session));

            return template;
        }
        private HttpResponseMessage<TResult> SafeExecute<TResult>(HttpRequestBinding binding, HttpEntity entity, params object[] uriVariables) where TResult : class
        {
            RestTemplate template = GetRestTemplate();
            HttpResponseMessage<TResult> response = null;

            foreach (IServer server in _connectionProvider.GetServers(binding.ServiceName))
            {
                if (server.IsActive)
                {
                    template.BaseAddress = server.URI;

                    try
                    {
                        response = template.Exchange<TResult>(binding.Url, binding.Method, entity, uriVariables);
                        server.UpdatedLastConnected(); // IMPORTANT
                        return response;
                    }
                    catch (System.Net.WebException ex)
                    {
                        Logger.TraceFormat("SafeExecute failed: {0}. Status = {1}. Timeout = {2}", ex.Message, ex.Status, _connectionTimeout);
                    }
                }
            }

            throw new RestClientException("Primary and standby servers failed to execute REST call. Timeout = {0}", _connectionTimeout);
        }
        private HttpResponseMessage SafeExecute(HttpRequestBinding binding, HttpEntity entity, params object[] uriVariables)
        {
            RestTemplate template = GetRestTemplate();
            HttpResponseMessage response = null;

            foreach (IServer server in _connectionProvider.GetServers(binding.ServiceName))
            {
                if (server.IsActive)
                {
                    template.BaseAddress = server.URI;

                    try
                    {
                        response = template.Exchange(binding.Url, binding.Method, entity, uriVariables);
                        server.UpdatedLastConnected(); // IMPORTANT
                        return response;
                    }
                    catch (System.Net.WebException ex)
                    {
                        Logger.TraceFormat("SafeExecute failed: {0}. Status = {1}. Timeout = {2}", ex.Message, ex.Status, _connectionTimeout);
                    }
                }
            }

            throw new RestClientException("Primary and standby servers failed to execute REST call. Timeout = {0}", _connectionTimeout);
        }

        #endregion

        #region REST Call Members

        [MethodImpl(MethodImplOptions.NoInlining)]
        protected TResult Execute<TResult>(MethodBase caller, params object[] uriVariables) where TResult : class
        {
            HttpRequestBinding binding = GeHttpRequestBinding(caller); // get the WebInvoke values from the caller
            HttpResponseMessage<TResult> response = SafeExecute<TResult>(binding, new HttpEntity(), uriVariables);

            return response.Body;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        protected TResult ExecuteWithBody<TResult>(MethodBase caller, Object body, params object[] uriVariables) where TResult : class
        {
            HttpRequestBinding binding = GeHttpRequestBinding(caller); // get the WebInvoke values from the caller
            HttpResponseMessage<TResult> response = SafeExecute<TResult>(binding, new HttpEntity(body), uriVariables);

            return response.Body;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        protected void Execute(MethodBase caller, params object[] uriVariables)
        {
            HttpRequestBinding binding = GeHttpRequestBinding(caller); // get the WebInvoke values from the caller
            HttpResponseMessage response = SafeExecute(binding, new HttpEntity(), uriVariables);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        protected void ExecuteWithBody(MethodBase caller, Object body, params object[] uriVariables)
        {
            HttpRequestBinding binding = GeHttpRequestBinding(caller); // get the WebInvoke values from the caller
            HttpResponseMessage response = SafeExecute(binding, new HttpEntity(body), uriVariables);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        protected TResult ExecuteWithFile<TResult>(MethodBase caller, String fileName, params object[] uriVariables) where TResult : class
        {
            HttpRequestBinding binding = GeHttpRequestBinding(caller); // get the WebInvoke values from the caller
            FileResource fileResource = new FileResource(fileName); // use a file resource as the body
            HttpEntity entity = new HttpEntity(fileResource);       // get the headers and body
            entity.Headers.Add("file", fileName);

            HttpResponseMessage<TResult> response = SafeExecute<TResult>(binding, entity, uriVariables);

            return response.Body;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        protected void ExecuteWithFile(MethodBase caller, String fileName, params object[] uriVariables)
        {
            HttpRequestBinding binding = GeHttpRequestBinding(caller); // get the WebInvoke values from the caller
            FileResource fileResource = new FileResource(fileName); // use a file resource as the body
            HttpEntity entity = new HttpEntity(fileResource);       // get the headers and body
            entity.Headers.Add("file", fileName);

            HttpResponseMessage result = SafeExecute(binding, entity, uriVariables);
        }

        #endregion

        #region IServiceDescriptor

        TService IServiceDescriptor<TService>.GetService()
        {
            return (this as TService);
        }

        #endregion
    }
}