﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spring.Http.Client.Interceptor;
using Spring.Http.Client;

namespace Atlas.Link.Net
{
    /// <summary>
    /// Sprint REST Client Interceptor for managing sessions.
    /// </summary>
    public class SessionStateInterceptor : IClientHttpRequestSyncInterceptor
    {
        private static Common.Logging.ILog Logger = Common.Logging.LogManager.GetLogger(typeof(SessionStateInterceptor));

        #region Constants

        private static String AuthorizationHeader = "Authorization";
        private static String CookieHeader = "Cookie";
        private static String SetCookieHeader = "Set-Cookie";

        private static String JavaSessionKey = "JSESSIONID";

        #endregion

        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionStateInterceptor" /> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public SessionStateInterceptor(SpringRestSession session)
        {
            _session = session;
        }

        #endregion

        #region Fields

        private SpringRestSession _session;

        #endregion

        #region Utility Methods

        private String ParseSessionCookie(IClientHttpResponse response)
        {
            //Set-Cookie: JSESSIONID=zp7y7kpjmwzxqzlipa1816ry;Path=/
            //Set-Cookie: rememberMe=deleteMe; Path=/; Max-Age=0; Expires=Sun, 19-May-2013 12:03:15 GMT

            try
            {
                String[] cookies = response.Headers.GetMultiValues(SetCookieHeader);

                if ((cookies != null) && (cookies.Length > 0))
                {
                    for (int i = 0; i < cookies.Length; i++)
                    {
                        String line = cookies[i];

                        if (line.IndexOf(JavaSessionKey, StringComparison.InvariantCultureIgnoreCase) >= 0)
                        {
                            return line;
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Logger.Error("ParseSessionCookie", ex);
                return null;
            }
        }

        #endregion

        #region Overrides

        //HTTP/1.1 500 Subject does not have permission [MESSAGE_READ]
        //Set-Cookie: JSESSIONID=zp7y7kpjmwzxqzlipa1816ry;Path=/
        //Set-Cookie: rememberMe=deleteMe; Path=/; Max-Age=0; Expires=Sun, 19-May-2013 12:03:15 GMT
        //Content-Type: text/html;charset=ISO-8859-1
        //Cache-Control: must-revalidate,no-cache,no-store
        //Content-Length: 7357
        //Server: Jetty(8.1.1.v20120215)

        /// <summary>
        /// Intercept the given synchronous request execution.
        /// The given <see cref="T:Spring.Http.Client.Interceptor.IClientHttpRequestSyncExecution" /> allows the interceptor
        /// to pass on the request and the response to the next entity in the chain.
        /// </summary>
        /// <param name="execution">The request execution context.</param>
        /// <returns>
        /// The response result of the execution
        /// </returns>
        /// <remarks>
        /// A typical implementation of this method would follow the following pattern:
        /// <ul>
        /// <li>Examine the HTTP request.</li>
        /// <li>Optionally modify the request headers.</li>
        /// <li>Optionally modify the request body.</li>
        /// <li><strong>Either</strong>
        /// <ul>
        /// <li>execute the request synchronous using the <see cref="M:IClientHttpRequestExecution.Execute()" /> method,</li>
        /// <strong>or</strong>
        /// <li>do not execute the request to block the execution altogether.</li>
        /// </ul>
        /// </li>
        /// <li>Optionally wrap the response.</li>
        /// </ul>
        /// </remarks>
        public IClientHttpResponse Execute(IClientHttpRequestSyncExecution execution)
        {
            //-- (1) populate request headers --
            String clientAuthHeader = _session.AuthorizationHeader;
            String clientSessHeader = _session.SessionHeader;

            if (String.IsNullOrWhiteSpace(clientAuthHeader) == false)
                execution.Headers[AuthorizationHeader] = clientAuthHeader;

            if (String.IsNullOrWhiteSpace(clientSessHeader) == false)
                execution.Headers[CookieHeader] = clientSessHeader;

            //-- (2) perform the actual HTTP call --
            IClientHttpResponse response = execution.Execute();

            //-- (3) parser response headers --
            String serverSessHeader = ParseSessionCookie(response);

            if (String.IsNullOrWhiteSpace(serverSessHeader) == false)
            {
                if (String.IsNullOrWhiteSpace(clientSessHeader))
                {
                    _session.SetSessionHeader(serverSessHeader);
                    Logger.Trace("Initial Session ID received from the server");
                }
                else if (serverSessHeader != clientSessHeader)
                {
                    _session.SetSessionHeader(serverSessHeader);
                    Logger.Trace("Updated Session ID received from the server");
                }
            }

            return response;
        }

        #endregion
    }
}