﻿using System;

namespace Atlas.Link.Net
{
    /// <summary>
    /// Allows a REST service call to be configured via a Service Name and a Base Url.
    /// The Service Name points to configuration setting for specifying the host address and port.
    /// The Base Url is used to prefix REST client method Url's.
    /// </summary>
    [AttributeUsageAttribute(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class ServiceHostAttribute : Attribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceHostAttribute"/> class.
        /// </summary>
        /// <param name="serviceName">Name of the service as defined in configuration.</param>
        public ServiceHostAttribute(String serviceName)
        {
            this.ServiceName = String.IsNullOrWhiteSpace(serviceName) ? Constants.DefaultNetworkServiceName : serviceName;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        /// <value>
        /// The name of the service.
        /// </value>
        public String ServiceName { get; private set; }

        /// <summary>
        /// Gets or sets the base URL for the service.
        /// </summary>
        /// <value>
        /// The base URL.
        /// </value>
        public String BaseURL
        {
            get { return _baseURL; }
            set { _baseURL = value; }
        }
        private String _baseURL = "";

        #endregion
    }
}