﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;
using System.Collections.Generic;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "endpoint", Namespace = "")]
    public class Endpoint : BaseAuditableEntity
    {
        #region Construction

        public Endpoint()
        {
            this.ID = "";
            this.Name = "";
            this.Type = Enums.EndpointType.FILE;
            this.Status = Enums.Status.ENABLED;
            this.CreatedAt = DateTime.Now;
            this.UpdatedAt = DateTime.Now;
            this.Deleted = null;
            this.Attributes = null;
            this.Children = new List<Endpoint>();
            this.ChildIndex = 0;
        }
        public Endpoint(Enums.EndpointType type)
            : this()
        {
            this.Type = type;
        }

        #endregion

        #region Serializable Properties

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        [XmlElement(ElementName = "status")]
        public Enums.Status Status { get; set; }

        [XmlElement(ElementName = "type")]
        public Enums.EndpointType Type { get; set; }

        [XmlElement(ElementName = "attributes")]
        public String Attributes { get; set; }

        [XmlElement(ElementName = "embedded")]
        public Endpoint EmbeddedEndpoint { get; set; }

        [XmlArray(ElementName = "children")]
        [XmlArrayItem(ElementName = "endpoint")]
        public List<Endpoint> Children { get; set; }

        [XmlElement(ElementName = "child_index")]
        public Int32 ChildIndex { get; set; }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.ID + " " + this.Name;
        }

        #endregion
    }

    [XmlRoot(ElementName = "endpoints", Namespace = "")]
    public class EndpointList : XmlSerializableList<Endpoint> { }
}