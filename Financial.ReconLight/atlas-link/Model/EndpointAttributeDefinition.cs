﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "endpoint_attribute_definition", Namespace = "")]
    public class EndpointAttributeDefinition : BaseEntity
    {
        #region Serializable Properties

        //[XmlElement(ElementName = "id")]
        //public String ID { get; set; }

        [XmlElement(ElementName = "endpoint_type")]
        public Enums.EndpointType Type { get; set; }

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        [XmlElement(ElementName = "attribute_type")]
        public Enums.EndpointAttributeType AttributeType { get; set; }

        [XmlElement(ElementName = "mandatory")]
        public Boolean Mandatory { get; set; }

        [XmlElement(ElementName = "default_value")]
        public String DefaultValue { get; set; }

        [XmlElement(ElementName = "field_description")]
        public String FieldDescription { get; set; }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.ID + " " + this.Name;
        }

        #endregion
    }

    [XmlRoot(ElementName = "endpoint_attribute_definitions", Namespace = "")]
    public class EndpointAttributeDefinitionList : XmlSerializableList<EndpointAttributeDefinition> { }
}