﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// The transformation group chain class
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "group_of_groups", Namespace = "")]
    public class GroupOfGroups : BaseAuditableEntity
    {
        #region Serializable Properties

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the route groups.
        /// </summary>
        /// <value>
        /// The route groups.
        /// </value>
        [XmlArray(ElementName = "route_groups")]
        [XmlArrayItem(ElementName = "route_group")]
        public List<RouteGroup> RouteGroups { get; set; }

        #endregion
    }

    /// <summary>
    /// Rout group chain list class
    /// </summary>
    [XmlRoot(ElementName = "groups_of_groups", Namespace = "")]
    public class GroupOfGroupsList : XmlSerializableList<GroupOfGroups> { }
}