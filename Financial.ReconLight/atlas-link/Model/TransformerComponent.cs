﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "transformer_component", Namespace = "")]
    public class TransformerComponent : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        [XmlElement(ElementName = "jar_file_name")]
        public String JarFileName { get; set; }

        [XmlElement(ElementName = "category")]
        public String Category { get; set; }

        [XmlElement(ElementName = "description")]
        public String Description { get; set; }

        [XmlElement(ElementName = "pre_recon_jar_file_name")]
        public String PreReconJarFileName { get; set; }

        [XmlElement(ElementName = "post_recon_jar_file_name")]
        public String PostReconJarFileName { get; set; }

        [XmlElement(ElementName = "reconciliation")]
        public Reconciliation Reconciliation { get; set; }

        [XmlArray(ElementName = "fields")]
        [XmlArrayItem(ElementName = "field")]
        public List<TransformerComponentField> ComponentFields { get; set; }

        [XmlElement(ElementName = "type")]
        public Enums.ComponentType Type { get; set; }

        [XmlElement(ElementName = "direction")]
        public Enums.ComponentDirection Direction { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "transformer_components", Namespace = "")]
    public class TransformerComponentList : XmlSerializableList<TransformerComponent> { }
}