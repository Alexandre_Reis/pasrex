﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// Message Document.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "document", Namespace = "")]
    public class MessageDocument: BaseEntity
    {
        #region Serializable Properties

        /// <summary>
        /// Gets or sets the input data.
        /// </summary>
        /// <value>
        /// The input data file.
        /// </value>
        [XmlElement(ElementName = "in_data")]
        public String InData { get; set; }

        /// <summary>
        /// Gets or sets the output data.
        /// </summary>
        /// <value>
        /// The out data.
        /// </value>
        [XmlElement(ElementName = "data")]
        public String OutData { get; set; }

        /// <summary>
        /// Gets or sets the name of the jar file.
        /// </summary>
        /// <value>
        /// The name of the jar file.
        /// </value>
        [XmlElement(ElementName = "jar_file_name")]
        public String JarFileName { get; set; }

        /// <summary>
        /// Gets or sets the name of the component.
        /// </summary>
        /// <value>
        /// The name of the component.
        /// </value>
        [XmlElement(ElementName = "transformer_component_name")]
        public String ComponentName { get; set; }

        #endregion
    }

    /// <summary>
    /// List of documents.
    /// </summary>
    [XmlRoot(ElementName = "documents", Namespace = "")]
    public class MessageDocumentList : XmlSerializableList<MessageDocument> { }
}