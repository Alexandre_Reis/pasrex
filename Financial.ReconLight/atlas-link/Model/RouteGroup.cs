﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// The transformation route group class
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "route_group", Namespace = "")]
    public class RouteGroup : BaseAuditableEntity
    {
        #region Serializable Properties

        /// <summary>
        /// Gets or sets the type of the group.
        /// </summary>
        /// <value>
        /// The type of the group.
        /// </value>
        [XmlElement(ElementName = "group_type")]
        public Enums.GroupType GroupType { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the routes.
        /// </summary>
        /// <value>
        /// The routes.
        /// </value>
        [XmlArray(ElementName = "transformation_routes")]
        [XmlArrayItem(ElementName = "transformation_route")]
        public List<Route> Routes { get; set; }

        /// <summary>
        /// Gets or sets the route group status.
        /// </summary>
        /// <value>
        /// The route group status.
        /// </value>
        [XmlElement(ElementName = "status")]
        public Enums.RouteGroupStatus RouteGroupStatus { get; set; }

        /// <summary>
        /// Gets or sets the dependencies.
        /// </summary>
        /// <value>
        /// The dependencies.
        /// </value>
        [XmlArray(ElementName = "dependencies")]
        [XmlArrayItem(ElementName = "dependency")]
        public HashSet<TransformationRouteDependency> Dependencies { get; set; }

        #endregion
    }

    /// <summary>
    /// Rout group list class
    /// </summary>
    [XmlRoot(ElementName = "route_groups", Namespace = "")]
    public class RouteGroupList : XmlSerializableList<RouteGroup> { }
}