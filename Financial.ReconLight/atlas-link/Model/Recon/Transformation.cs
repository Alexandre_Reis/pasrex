﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "transformation", Namespace = "")]
    public class Transformation : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        [XmlElement(ElementName = "has_header")]
        public Boolean HasHeader { get; set; }

        [XmlElement(ElementName = "field_separator_value")]
        public String FieldSeparatorValue { get; set; }

        [XmlElement(ElementName = "row_separator_value")]
        public String RowSeparatorValue { get; set; }

        //TODO: remove this. Lee is not 100% sure this is not used, so leave here until verified.
        [XmlArray(ElementName = "transformation_fields")]
        [XmlArrayItem(ElementName = "transformation_field", IsNullable=true)]
        public List<TransformationField> Fields { get; set; }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.ID + " " + this.Name;
        }

        #endregion
    }
}