﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "transformation_field", Namespace = "")]
    public class TransformationField : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        [XmlElement(ElementName = "position")]
        public Int32 Position { get; set; }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}