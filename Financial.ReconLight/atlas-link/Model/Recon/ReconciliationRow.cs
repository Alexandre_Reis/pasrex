﻿using System;
using System.Xml.Serialization;

namespace Atlas.Link.Model.Recon
{
    [Serializable]
    public class ReconciliationRow
    {
        [XmlAttribute(AttributeName = "key")]
        public String Key { get; set; }

        [XmlAttribute(AttributeName = "source_a_row")]
        public Int32 SourceARowIndex { get; set; }

        [XmlAttribute(AttributeName = "source_b_row")]
        public Int32 SourceBRowIndex { get; set; }

        [XmlAttribute(AttributeName = "category")]
        public Enums.ReconciliationCategory Category { get; set; }

        [XmlAttribute(AttributeName = "unmatched_columns")]
        public String UnmatchedColumns { get; set; }

        [XmlAttribute(AttributeName = "matched_variance_columns")]
        public String MatchedByVarianceColumns { get; set; }
        
    }
}