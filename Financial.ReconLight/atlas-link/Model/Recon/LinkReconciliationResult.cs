﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Atlas.Link.Model.Recon
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_result", Namespace = "")]
    public class LinkReconciliationResult : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlArray(ElementName = "right_missing")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconDataRow> LeftRowsMissingFromRight { get; set; }

        [XmlArray(ElementName = "left_missing")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconDataRow> RightRowsMissingFromLeft { get; set; }

        [XmlArray(ElementName = "left_duplicates")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconDataRow> LeftDuplicateRows { get; set; }

        [XmlArray(ElementName = "right_duplicates")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconDataRow> RightDuplicateRows { get; set; }

        [XmlArray(ElementName = "row_differences")]
        [XmlArrayItem(ElementName = "row")]
        public List<ReconRowDifference> DifferentRows { get; set; }

        [XmlElement(ElementName = "left_file_data")]
        public String LeftFileData { get; set; }

        [XmlElement(ElementName = "right_file_data")]
        public String RightFileData { get; set; }

        [XmlElement(ElementName = "message")]
        public Message Message { get; set; }

        #endregion
    }
}