﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;
using System.Collections.Generic;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "reconciliation_comp_field", Namespace = "")]
    public class ReconciliationCompField : BaseEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "left_field")]
        public TransformationField LeftField { get; set; }

        [XmlElement(ElementName = "right_field")]
        public TransformationField RightField { get; set; }

        [XmlElement(ElementName = "field_type")]
        public Atlas.Link.Model.Enums.FieldType FieldType{ get; set; }

        [XmlElement(ElementName = "variance")]
        public Decimal Variance{ get; set; }

        [XmlElement(ElementName = "variance_type")]
        public Atlas.Link.Model.Enums.VarianceType VarianceType { get; set; }

        #endregion
    }
}