﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "editabilityRules", Namespace = "")]
    public class EditabilityRule : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "rules_schema")]
        public Byte[] RulesSchema { get; set; }

        [XmlElement(ElementName = "document_class")]
        public String DocumentClass { get; set; }

        #endregion
    }
}