﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// The transformation route flow class
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "route_flow", Namespace = "")]
    public class RouteFlow : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "sequence")]
        public Int32 Sequence { get; set; }

        [XmlElement(ElementName = "route_flow_group")]
        public string RouteFlowGroup { get; set; }

        [XmlElement(ElementName = "success_transformation_route")]
        public Route SuccessTransformationRoute { get; set; }

        [XmlElement(ElementName = "warning_transformation_route")]
        public Route WarningTransformationRoute { get; set; }

        [XmlElement(ElementName = "any_transformation_route")]
        public Route AnyTransformationRoute { get; set; }

        #endregion
    }

    /// <summary>
    /// Rout flow list class
    /// </summary>
    [XmlRoot(ElementName = "route_flows", Namespace = "")]
    public class RouteFlowList : XmlSerializableList<RouteFlow> { }
}