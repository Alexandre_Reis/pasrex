﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// The transformation route class
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "holiday", Namespace = "")]
    public class Holiday : BaseAuditableEntity
    {
        #region Serializable Properties

        /// <summary>
        /// Gets or sets the holiday date.
        /// </summary>
        /// <value>
        /// The holiday date.
        /// </value>
        [XmlElement(ElementName = "holiday")]
        public DateTime HolidayDate { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }

        #endregion
    }

    /// <summary>
    /// Rout list class
    /// </summary>
    [XmlRoot(ElementName = "holidays", Namespace = "")]
    public class HolidayList : XmlSerializableList<Holiday> { }
}