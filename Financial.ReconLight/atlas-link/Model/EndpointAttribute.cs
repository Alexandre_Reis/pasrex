﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    ///     <attributes><attribute name="fileLocation" value="T:\ATLASBUS\IN\"/></attributes> 
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "attribute", Namespace = "")]
    public class EndpointAttribute
    {
        #region Serializable Properties

        [XmlAttribute(AttributeName = "name")]
        public String Name { get; set; }

        [XmlAttribute(AttributeName = "value")]
        public String Value { get; set; }

        #endregion

        #region Overrides

        public override Int32 GetHashCode()
        {
            return this.Name.GetHashCode();
        }
        public override Boolean Equals(object obj)
        {
            return ((obj != null) &&
                    (obj is EndpointAttribute) &&
                    (((EndpointAttribute)obj).GetHashCode() == this.GetHashCode()));
        }
        public override String ToString()
        {
            return this.Name + " = " + this.Value;
        }

        #endregion
    }

    [XmlRoot(ElementName = "attributes", Namespace = "")]
    public class EndpointAttributeList : XmlSerializableList<EndpointAttribute>
    {
        #region Serialization Members

        public static EndpointAttributeList Deserialize(String xmlString)
        {
            EndpointAttributeList result = new EndpointAttributeList();

            if (String.IsNullOrWhiteSpace(xmlString) == false)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(EndpointAttributeList));

                using (System.IO.StringReader reader = new System.IO.StringReader(xmlString))
                {
                    result = (EndpointAttributeList)serializer.Deserialize(reader);
                }
            }

            return result;
        }
        public static String Serialize(EndpointAttributeList list)
        {
            String result = "";

                XmlSerializer serializer = new XmlSerializer(typeof(EndpointAttributeList));

                using (System.IO.StringWriter writer = new System.IO.StringWriter())
                {
                    serializer.Serialize(writer, list, null);
                    result = writer.ToString();
                }

            return result;
        }

        #endregion

        #region Construction

        public EndpointAttributeList() { }

        public EndpointAttributeList(IEnumerable<EndpointAttribute> items)
        {
            foreach (EndpointAttribute item in items)
            {
                this.Add(item);
            }
        }

        #endregion
    }
}