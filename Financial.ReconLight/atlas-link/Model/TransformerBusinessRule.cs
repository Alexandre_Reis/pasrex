﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "transformer_business_rule", Namespace = "")]
    public class TransformerBusinessRule : BaseAuditableEntity
    {
        [XmlArray(ElementName = "out_components")]
        [XmlArrayItem(ElementName = "out_component")]
        public List<Endpoint> OutComponents { get; set; }

        [XmlElement(ElementName = "transformer_component")]
        public TransformerComponent TransformerComponent { get; set; }
    }
}