﻿using System;
using System.Collections.Generic;
using Atlas.Link.Client;
using Atlas.Link.Model;

namespace Atlas.Link.Cache
{
    public class RestServerCache : IDataCache, IDisposable
    {
        #region Constructor

        public RestServerCache()
        {
            _endpointClient = new EndpointClient();
            _translationClient = new TranslationClient();

            //TODO: add polling support here if the need arises.
        }

        #endregion

        #region REST Clients

        private EndpointClient _endpointClient;
        private TranslationClient _translationClient;

        #endregion

        #region Cached Data

        private IList<EndpointAttributeDefinition> _endpointAttributeDefinition;
        private Dictionary<String, IList<Translation>> _translationDictionary;

        #endregion

        #region IDataCache
                    
        public IList<EndpointAttributeDefinition> GetEndpointAttributeDefinitions()
        {
            lock (_endpointClient)
            {
                if (_endpointAttributeDefinition == null)
                {
                    _endpointAttributeDefinition = _endpointClient.GetMetaData();
                }
            }

            return _endpointAttributeDefinition;
        }

        public IList<Translation> GetTranslations(String locale)
        {
            IList<Translation> resultList = null;

            lock (_translationClient)
            {
                if (_translationDictionary == null)
                    _translationDictionary = new Dictionary<String, IList<Translation>>();

                if (_translationDictionary.TryGetValue(locale.ToLower(), out resultList) == false)
                {
                    resultList = _translationClient.GetTranslationsForLocale(locale);
                    _translationDictionary.Add(locale.ToLower(), resultList);
                }
            }

            return resultList;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            //
        }

        #endregion
    }
}