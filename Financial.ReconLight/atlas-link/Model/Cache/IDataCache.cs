﻿using System.Collections.Generic;
using Atlas.Link.Model;
using System;

namespace Atlas.Link.Cache
{
    public interface IDataCache
    {
        /// <summary>
        /// Gets the list endpoint attribute definitions.
        /// </summary>
        /// <returns></returns>
        IList<EndpointAttributeDefinition> GetEndpointAttributeDefinitions();

        /// <summary>
        /// Gets the translation for a specific locale.
        /// </summary>
        /// <param name="locale">The locale.</param>
        /// <returns></returns>
        IList<Translation> GetTranslations(String locale);
    }
}