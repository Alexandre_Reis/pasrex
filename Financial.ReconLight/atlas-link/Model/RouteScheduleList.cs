﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [XmlRoot(ElementName = "route_schedules", Namespace = "")]
    public class RouteScheduleList : XmlSerializableList<RouteSchedule> { }
}