﻿
namespace Atlas.Link.Model
{
    /// <summary>
    /// Defines the type of Transformation Route.
    /// </summary>
    public enum RouteStructure
    {
        INBOUND_AND_OUTBOUND,
        INBOUND_ONLY,
        OUTBOUND_ONLY,
    }
}