﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// The routeDependency class
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "routeDependency", Namespace = "")]
    public class TransformationRouteDependency : BaseAuditableEntity
    {
        #region Serializable Properties

        /// <summary>
        /// Gets or sets the route.
        /// </summary>
        /// <value>
        /// The route.
        /// </value>
        [XmlElement(ElementName = "transformationRoute")]
        public Route Route { get; set; }

        /// <summary>
        /// Gets or sets the routes.
        /// </summary>
        /// <value>
        /// The routes.
        /// </value>
        [XmlArray(ElementName = "afferents")]
        [XmlArrayItem(ElementName = "afferent")]
        public HashSet<Route> Routes { get; set; }

        [XmlElement(ElementName = "id")]
        public override String ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value ;
            }
        }
        private String _id ;

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Atlas.Link.Serialization.XmlSerializableList{Atlas.Link.Model.TransformationRouteDependency}" />
    [XmlRoot(ElementName = "routeDependencys", Namespace = "")]
    public class TransformationRouteDependencyList : XmlSerializableList<TransformationRouteDependency> { }
}