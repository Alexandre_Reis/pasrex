﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "translation", Namespace = "")]
    public class Translation: BaseEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "key")]
        public String Key { get; set; }

        [XmlElement(ElementName = "locale")]
        public String Locale { get; set; }

        [XmlElement(ElementName = "text")]
        public String Text { get; set; }
        
        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.ID + " " + this.Key;
        }

        #endregion
    }

    [XmlRoot(ElementName = "translations", Namespace = "")]
    public class TranslationList : XmlSerializableList<Translation> { }
}