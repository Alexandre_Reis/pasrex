﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "route_schedule", Namespace = "")]
    public class RouteSchedule : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "route")]
        public Route Route { get; set; }

        [XmlElement(ElementName = "start_time")]
        public DateTime StartTime { get; set; }

        [XmlElement(ElementName = "end_time")]
        public DateTime EndTime{ get; set; }

        #endregion
    }
}