﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "field", Namespace = "")]
    public class TransformerComponentField : BaseEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "index")]
        public int Index { get; set; }

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        #endregion
    }
}