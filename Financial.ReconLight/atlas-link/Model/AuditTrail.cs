﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "audit_trail", Namespace = "")]
    public class AuditTrail : BaseAuditableEntity
    {
        #region Serializable Properties

        //[XmlElement(ElementName = "id")]
        //public String ID { get; set; }

        [XmlElement(ElementName = "entity_id")]
        public String EntityID { get; set; }

        [XmlElement(ElementName = "user_name")]
        public String User { get; set; }

        [XmlElement(ElementName = "audit_code")]
        public String AuditCode { get; set; }

        [XmlElement(ElementName = "audit_text")]
        public String AuditText { get; set; }

        [XmlElement(ElementName = "entity_type")]
        public String EntityType { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "audit_trails", Namespace = "")]
    public class AuditTrailList : XmlSerializableList<AuditTrail> { }
}