﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "server_node", Namespace = "")]
    public class ServerNode
    {
        #region Serializable Properties

        [XmlElement(ElementName = "ip_address")]
        public String IpAddress { get; set; }

        #endregion

    }
}
