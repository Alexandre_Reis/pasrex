﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "administratable_document", Namespace = "")]
    public class AdministratableDocument : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "document_status")]
        public Enums.DocumentStatus Status { get; set; }

        [XmlElement(ElementName = "transformation_route")]
        public Route Route { get; set; }

        [XmlElement(ElementName = "document_content")]
        public Byte[] Content { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "administratable_documents", Namespace = "")]
    public class AdministratableDocumentList : XmlSerializableList<AdministratableDocument> { }
}