﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Atlas.Link.Client;
using Atlas.Link.Model;
using System.Collections.Generic;

namespace Atlas.Link.Net
{
    public class ServerStateManager : IServerStateProvider, IDisposable
    {
        private static Common.Logging.ILog Logger = Common.Logging.LogManager.GetLogger(typeof(ServerStateManager));

        #region Factory Members

        public static IServerStateProvider Provider
        {
            get
            {
                if (_provider == null)
                    _provider = new ServerStateManager();

                return _provider;
            }
            set
            {
                _provider = value;
            }

        }
        private static IServerStateProvider _provider;

        #endregion

        #region Construction

        private ServerStateManager()
        {
            // initialize vars
            _connectionProvider = ServiceLocator.Get<Atlas.Link.Net.IConnectionProvider>();
            _heartbeatClient = new Client.HeartbeatClient(5000);

            // get the initial server state on the calling thread
            _curState = GetServerState();

            Logger.InfoFormat("Server Environment = {0}", _curState.Environment);
            Logger.InfoFormat("Server Version = {0}", _curState.Version);
            Logger.InfoFormat("Server Date = {0}", _curState.ServerDate);

            foreach (var server in _curState.ServerNodes)
            {
                Logger.InfoFormat("Server Node = {0}", server);
            }

            // register initial server nodes
            _curState.ServerNodes.ToList().ForEach(host => _connectionProvider.RegisterServer(Constants.DefaultNetworkServiceName, host));

            _event = new System.Threading.ManualResetEvent(false); // create an OS wait handle

            // start polling loop on background thread
            var thread = new System.Threading.Thread(HeartbeatThreadProc)
            {
                Name = "ServerStateManager",
                IsBackground = true
            };
            thread.Start();
        }

        #endregion

        #region Fields

        private IConnectionProvider _connectionProvider;
        private HeartbeatClient _heartbeatClient;
        private ServerState _curState;
        private System.Threading.ManualResetEvent _event;

        #endregion

        #region Thread Proc

        private void HeartbeatThreadProc()
        {
            // loop until event is set in Dispose()
            while (_event.WaitOne(2000) == false)
            {
                try
                {
                    ServerState newState = GetServerState();
                    List<String> delList = _curState.ServerNodes.Except(newState.ServerNodes).ToList();
                    List<String> addList = newState.ServerNodes.Except(_curState.ServerNodes).ToList();
                    Boolean modified = ((_curState.IsConnected != newState.IsConnected) || (_curState.GlobalRoutesEnabled != newState.GlobalRoutesEnabled));

                    // notify connection provider of change in state
                    if (newState.IsConnected)
                    {
                        delList.ForEach(host => _connectionProvider.UnregisterServer(Constants.DefaultNetworkServiceName, host));
                        addList.ForEach(host => _connectionProvider.RegisterServer(Constants.DefaultNetworkServiceName, host));
                    }

                    // notify all registered listeners of change in server state
                    if (modified)
                    {
                        // set the new state
                        _curState = newState;

                        // trigger the event
                        ServerStateChanged handler = StateChanged;
                        if (handler != null)
                            handler(newState);
                    }
                    else
                        _curState.ServerDate = newState.ServerDate;

                }
                catch (Exception ex)
                {
                    Logger.Error("HeartbeatThreadProc", ex);
                }
            }
        }
        private ServerState GetServerState()
        {
            ServerState result = new ServerState();

            try
            {
                HeartbeatMessage msg = _heartbeatClient.GetHeartbeat();

                result.IsConnected = true;
                result.Environment = msg.Environment;
                result.Status = msg.Status;
                result.Version = msg.Version;
                result.GlobalRoutesEnabled = (msg.RoutesStatus == Enums.RouteStatus.RUNNING.ToString());
                result.ServerDate = msg.ServerDate;

                msg.ServerNodes.ForEach(x => result.ServerNodes.Add(x.IpAddress));
            }
            catch (Exception ex)
            {
                Logger.Debug("GetServerState", ex); //????
            }

            return result;
        }

        #endregion

        #region IServerStateProvider

        public IServerStateInfo Info
        {
            get { return _curState; }
        }

        public event ServerStateChanged StateChanged;

        #endregion

        #region IDisposable

        public void Dispose()
        {
            _event.Set();
        }

        #endregion
    }
}