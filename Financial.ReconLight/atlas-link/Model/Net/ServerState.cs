﻿using System;
using System.Collections.Generic;

namespace Atlas.Link.Net
{
    [Serializable]
    public class ServerState : IServerStateInfo
    {
        #region Construction

        public ServerState()
        {
            this.Status = "";
            this.Environment = "";
            this.Version = "";
            this.IsConnected = false;
            this.GlobalRoutesEnabled = false;
            this.ServerNodes = new List<String>();
            // this.ServerDate = DateTime.Now;
        }

        #endregion

        #region Properties

        public String Status { get; set; }
        public String Environment { get; set; }
        public String Version { get; set; }
        public Boolean IsConnected { get; set; }
        public Boolean GlobalRoutesEnabled { get; set; }
        public IList<String> ServerNodes { get; private set; }
        public DateTime ServerDate { get; set; }

        #endregion
    }
}