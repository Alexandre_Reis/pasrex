﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;
using System.Collections.Generic;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "message", Namespace = "")]
    public class Message : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "transformation_route")]
        public Route Route { get; set; }

        [XmlElement(ElementName = "transformation_status")]
        public Enums.TransformationStatus TransformationStatus { get; set; }

        [XmlArray(ElementName = "endpoint_messages")]
        [XmlArrayItem(ElementName = "endpoint_message")]
        public List<EndpointMessage> EndpointMessages { get; set; }

        [XmlElement(ElementName = "received_at")]
        public DateTime ReceivedAt { get; set; }

        [XmlElement(ElementName = "message_status")]
        public Enums.MessageStatus MessageStatus { get; set; }

        [XmlElement(ElementName = "recon_available")]
        public Boolean ReconAvaliable { get; set; }

        [XmlElement(ElementName = "description")]
        public String Description { get; set; }

        [XmlElement(ElementName = "category")]
        public String Category { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "messages", Namespace = "")]
    public class MessageList : XmlSerializableList<Message> { }
}