﻿using System;

namespace Atlas.Link.Model
{
    [Serializable]
    public class Authenticate
    {
        #region Serializable Properties

        public String Login { get; set; }

        public String Password { get; set; }

        #endregion
    }

    public class AuthenticateResponse
    {
        public Boolean Success { get; set; }
        public String Message { get; set; }
    }
}