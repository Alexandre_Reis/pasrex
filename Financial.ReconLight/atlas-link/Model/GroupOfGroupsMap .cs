﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// The transformation route group map class
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "group_of_groups_map", Namespace = "")]
    public class GroupOfGroupsMap : BaseAuditableEntity
    {
        #region Serializable Properties

        /// <summary>
        /// Gets or sets the group of groups.
        /// </summary>
        /// <value>
        /// The group of groups.
        /// </value>
        [XmlElement(ElementName = "group_of_groups")]
        public GroupOfGroups GroupOfGroups { get; set; }

        /// <summary>
        /// Gets or sets the route group.
        /// </summary>
        /// <value>
        /// The route group.
        /// </value>
        [XmlElement(ElementName = "route_group")]
        public RouteGroup RouteGroup { get; set; }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        /// <value>
        /// The sequence.
        /// </value>
        [XmlElement(ElementName = "sequence")]
        public Int32 Sequence { get; set; }

        #endregion
    }

    /// <summary>
    /// Rout group list class
    /// </summary>
    [XmlRoot(ElementName = "groups_of_groups_map", Namespace = "")]
    public class GroupOfGroupsMapList : XmlSerializableList<GroupOfGroupsMap> { }
}