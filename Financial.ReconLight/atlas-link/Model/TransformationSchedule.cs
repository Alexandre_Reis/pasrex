﻿using System;
using System.Xml.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// Schecule model for transformation object
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "transformation_schedule", Namespace = "")]
    public class TransformationSchedule : BaseAuditableEntity
    {
        #region Serializable Properties

        /// <summary>
        /// Gets or sets the cron string.
        /// </summary>
        /// <value>
        /// The cron string.
        /// </value>
        [XmlElement(ElementName = "cron_string")]
        public String CronString { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is manual trigger.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is manual trigger; otherwise, <c>false</c>.
        /// </value>
        [XmlElement(ElementName = "manual_trigger")]
        public Boolean IsManualTrigger { get; set; }

        #endregion
    }
}