﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// The transformation group of group map class
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "route_group_map", Namespace = "")]
    public class RouteGroupMap : BaseAuditableEntity
    {
        #region Serializable Properties

        /// <summary>
        /// Gets or sets the route.
        /// </summary>
        /// <value>
        /// The route.
        /// </value>
        [XmlElement(ElementName = "transformation_route")]
        public Route Route { get; set; }

        /// <summary>
        /// Gets or sets the route group.
        /// </summary>
        /// <value>
        /// The route group.
        /// </value>
        [XmlElement(ElementName = "route_group")]
        public RouteGroup RouteGroup { get; set; }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        /// <value>
        /// The sequence.
        /// </value>
        [XmlElement(ElementName = "sequence")]
        public Int32 Sequence { get; set; }

        #endregion
    }

    /// <summary>
    /// Rout group list class
    /// </summary>
    [XmlRoot(ElementName = "route_groups_map", Namespace = "")]
    public class RouteGroupMapList : XmlSerializableList<RouteGroupMap > { }
}