﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "file_item", Namespace = "")]
    public class FileItem
    {
        #region Serializable Properties

        [XmlElement(ElementName = "file_name")]
        public String FileName { get; set; }

        #endregion

        #region Overrides

        public override int GetHashCode()
        {
            return this.FileName.GetHashCode();
        }
        public override string ToString()
        {
            return this.FileName;
        }
        public override bool Equals(object obj)
        {
            return obj != null &&
                   obj.GetType().Equals(this.GetType()) &&
                   obj.GetHashCode().Equals(this.GetHashCode());
        }

        #endregion
    }

    [XmlRoot(ElementName = "file_list", Namespace = "")]
    public class FileList : XmlSerializableList<FileItem> { }
}