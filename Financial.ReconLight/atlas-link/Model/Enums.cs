﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Atlas.Link.Model
{
    public class Enums
    {
        public enum Tipo
        {
            RECON,
            EFINANCEIRA
        }

        public enum DocumentStatus
        {
            PENDING,
            SUCCESS,
            ERROR
        }

        public enum UserStatus
        {
            ACTIVE,
            INACTIVE,
            PASSWORD_EXPIRED
        }

        public enum EndpointType
        {
            FILE,
            JMS_ACTIVEMQ,
            JMS_WEBSPHERE,
            EMAIL,
            FTP,
            SFTP,
            SSH,
            JDBC,
            SCHEDULE,
            FIX,
            PARENT,
            WEBSERVICE,
            MSMQ
        }

        public enum QueueType
        {
            PUBLISH,
            SUBSCRIBE
        }

        public enum Status
        {
            ENABLED,
            DISABLED
        }

        public enum MessageStatus
        {
            PENDING,
            SUCCESS,
            ERROR,
            WARNING
        }

        public enum SettlementStatus
        {
            PENDING,
            SENDING,
            ERROR,
            NO_FUNDS,
            SENT,
            CANCELED
        }

        public enum EndpointMessageStatus
        {
            SEND_PENDING,
            SEND_SUCCESS,
            SEND_ERROR
        }

        public enum TransformationStatus
        {
            NO_TRANSFORM,
            TRANSFORM_PENDING,
            TRANSFORM_SUCCESS,
            TRANSFORM_ERROR
        }

        public enum FieldType
        {
            INTEGER,
            DECIMAL,
            STRING,
            INFO
        }

        public enum VarianceType
        {
            NONE,
            INTEGER,
            DECIMAL,
            PERCENT
        }

        public enum EndpointAttributeType
        {
            STRING,
            INTEGER,
            DOUBLE,
            BOOLEAN,
            PASSWORD,
            FILE,
            FILELOCATION,
            TIME,
            DAYS
        }

        public enum RouteType
        {
            ATLAS_LINK,
            WEBSERVICE
        }

        public enum RouteStatus
        {
            PAUSED,
            RUNNING
        }

        public enum ComponentType
        {
            TRANSFORMATION,
            RECONCILIATION,
        }

        public enum ComponentDirection
        {
            INBOUND,
            OUTBOUND,
        }

        public enum ReconciliationCategory
        {
            UNMATCHED,
            MISSING_IN_A,
            MISSING_IN_B,
            DUPLICATE_IN_A,
            DUPLICATE_IN_B,
            MATCHED_BY_VARIANCE,
            MATCHED
        }

        public enum GroupType
        {
            TRANSFORMATION, RECONCILIATION
        }

        public enum PPProcessorType
        {
            PRE, POST
        }

        public enum TransformationRouteStatus 
        {
            STOPPED,
            STARTING,
            STARTED,
            STOPPING
        }

        public enum RouteGroupStatus 
        {
            RUNNING, STOPPED, BLOCKED
        }
    }
}