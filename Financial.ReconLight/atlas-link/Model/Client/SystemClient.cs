﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface ISystemService
    {
        #region REST Methods

        [OperationContract]
        StandardResponse DisableRoutes();

        [OperationContract]
        StandardResponse EnableRoutes();

        #endregion
    }

    [XmlSerializerFormat]
    public class SystemClient : SpringRestClient<ISystemService>, ISystemService
    {
        #region REST Method Implementation

        [WebInvoke(Method = "GET", UriTemplate = "/system/disableRoutes")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse DisableRoutes()
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "GET", UriTemplate = "/system/enableRoutes")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse EnableRoutes()
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        #endregion
    }
}