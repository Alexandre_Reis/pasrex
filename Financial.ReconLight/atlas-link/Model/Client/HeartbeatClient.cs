﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IHeartbeatService
    {
        #region REST Methods

        [OperationContract]
        HeartbeatMessage GetHeartbeat();

        #endregion
    }

    [XmlSerializerFormat]
    public class HeartbeatClient : SpringRestClient<IHeartbeatService>, IHeartbeatService
    {
        public HeartbeatClient(Int32 timeout = 5000) : base(timeout) { }

        #region REST Method Implementation

        [WebInvoke(Method = "GET", UriTemplate = "/heartbeat/check")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public HeartbeatMessage GetHeartbeat()
        {
            return Execute<HeartbeatMessage>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        #endregion
    }
}