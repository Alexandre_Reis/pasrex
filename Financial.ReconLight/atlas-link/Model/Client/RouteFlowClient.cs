﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IRouteFlowClientService
    {
        #region REST Methods

        [OperationContract]
        RouteFlowList List();

        [OperationContract]
        RouteFlowList ListAll();

        [OperationContract]
        RouteFlow Save(RouteFlow routeFlow);

        [OperationContract]
        StandardResponse Delete(String routeFlowId);

        [OperationContract]
        RouteFlowList GetUpdates(DateTime lastUpdatedDate);

         [OperationContract]
        StandardResponse Trigger(String routeFlowId);

        #endregion
    }

    [XmlSerializerFormat]
    public class RouteFlowClient : SpringRestClient<IRouteFlowClientService>, IRouteFlowClientService, IPollClient<RouteFlow>, IUpdateClient<RouteFlow>
    {
        #region IRouteService

        [WebInvoke(Method = "GET", UriTemplate = "/routegroup/routeflow/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteFlowList ListAll()
        {
            return Execute<RouteFlowList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "GET", UriTemplate = "/routegroup/routeflow/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteFlowList List()
        {
            return Execute<RouteFlowList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "POST", UriTemplate = "/routegroup/routeflow/saverouteflow")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteFlow Save(RouteFlow routeFlow)
        {
            ExecuteWithBody(System.Reflection.MethodInfo.GetCurrentMethod(), routeFlow);
            return null;
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/routegroup/routeflow/{routeId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String routeFlowId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), routeFlowId);
            return null;
        }

        [WebInvoke(Method = "GET", UriTemplate = "/routegroup/routeflow/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteFlowList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<RouteFlowList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "GET", UriTemplate = "/routegroup/routeflow/{routeFlowId}/trigger")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Trigger(String routeFlowId)
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), routeFlowId);
        }
        #endregion


        #region IPollClient

        System.Collections.Generic.IList<RouteFlow> IPollClient<RouteFlow>.FetchAll()
        {
            return (IList<RouteFlow>) List().Where(t=>t.Sequence==0).ToList();
        }

        System.Collections.Generic.IList<RouteFlow> IPollClient<RouteFlow>.FetchUpdates(DateTime lastUpdatedDate)
        {
            return (IList<RouteFlow>) GetUpdates(lastUpdatedDate).Where(t => t.Sequence == 0).ToList(); 
        }
        #endregion
    }
}