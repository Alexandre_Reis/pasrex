﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IAuditTrailService
    {
        #region REST Methods

        [OperationContract]
        AuditTrailList FindByEntityId(String entityId);

        #endregion
    }

    [XmlSerializerFormat]
    public class AuditTrailClient : SpringRestClient<IAuditTrailService>, IAuditTrailService
    {
        #region REST Method Implementation

        [WebInvoke(Method = "GET", UriTemplate = "/audit/{entityId}/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public AuditTrailList FindByEntityId(string entityId)
        {
            return Execute<AuditTrailList>(System.Reflection.MethodInfo.GetCurrentMethod(), entityId);
        }

        #endregion
    }
}