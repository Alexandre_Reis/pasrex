﻿using System;
using System.Dynamic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
/// <summary>
/// 
/// </summary>
    [ServiceContract]
    public interface IHolidayService
    {
        #region REST Methods

        /// <summary>
        /// Lists this instance.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        HolidayList List();

        /// <summary>
        /// Saves the specified holiday.
        /// </summary>
        /// <param name="holiday">The holiday.</param>
        /// <returns></returns>
        [OperationContract]
        Holiday Save(Holiday holiday);

        /// <summary>
        /// Gets the updates.
        /// </summary>
        /// <param name="lastUpdatedDate">The last updated date.</param>
        /// <returns></returns>
        [OperationContract]
        HolidayList GetUpdates(DateTime lastUpdatedDate);

        /// <summary>
        /// Deletes the specified holiday identifier.
        /// </summary>
        /// <param name="holidayId">The holiday identifier.</param>
        /// <returns></returns>
        [OperationContract]
        StandardResponse Delete(string holidayId);

    #endregion
    }

/// <summary>
/// 
/// </summary>
    [XmlSerializerFormat]
    public class HolidayClient : SpringRestClient<IHolidayService>, IHolidayService, IPollClient<Holiday>, IUpdateClient<Holiday>
    {

        [WebInvoke(Method = "GET", UriTemplate = "/holiday/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public HolidayList List()
        {
            return Execute<HolidayList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "POST", UriTemplate = "/holiday/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Holiday Save(Holiday holiday)
        {
            return ExecuteWithBody<Holiday>(System.Reflection.MethodInfo.GetCurrentMethod(), holiday);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/holiday/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public HolidayList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<HolidayList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }


        /// <summary>
        /// Deletes the specified holiday identifier.
        /// </summary>
        /// <param name="holidayId">The holiday identifier.</param>
        /// <returns></returns>
        [WebInvoke(Method = "DELETE", UriTemplate = "/holiday/{holidayId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String holidayId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), holidayId);
            return null; //TODO...
        }

        #region IPollClient

        System.Collections.Generic.IList<Holiday> IPollClient<Holiday>.FetchAll()
        {
            return List();
        }

        System.Collections.Generic.IList<Holiday> IPollClient<Holiday>.FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}