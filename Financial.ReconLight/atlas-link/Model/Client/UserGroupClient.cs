﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IUserGroupService
    {
        #region REST Methods

        [OperationContract]
        UserGroupList GetAllUserGroups();

        [OperationContract]
        UserGroup Save(UserGroup userGroup);

        [OperationContract]
        StandardResponse Delete(string groupId);

        [OperationContract]
        UserGroupList GetUpdates(DateTime lastUpdatedDate); 

        #endregion
    }

    [XmlSerializerFormat]
    public class UserGroupClient : SpringRestClient<IUserGroupService>, IUserGroupService, IPollClient<UserGroup>, IUpdateClient<UserGroup>
    {
        #region IUserGroupService

        [WebInvoke(Method = "GET", UriTemplate = "/group/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public UserGroupList GetAllUserGroups()
        {
            return Execute<UserGroupList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }


        [WebInvoke(Method = "POST", UriTemplate = "/group/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public UserGroup Save(UserGroup userGroup)
        {
            return ExecuteWithBody<UserGroup>(System.Reflection.MethodInfo.GetCurrentMethod(), userGroup);
        }


        [WebInvoke(Method = "DELETE", UriTemplate = "/group/{groupId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(string groupId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), groupId);
            return null; //TODO...
        }


        [WebInvoke(Method = "GET", UriTemplate = "/group/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public UserGroupList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<UserGroupList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        #endregion

        #region IPollClient<UserGroup>

        public IList<UserGroup> FetchAll()
        {
            return GetAllUserGroups();
        }

        public IList<UserGroup> FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}