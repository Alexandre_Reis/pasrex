﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;
using System.Collections.Generic;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IAdministratableDocumentService
    {
        #region REST Methods

        [OperationContract]
        AdministratableDocumentList GetAll();

        [OperationContract]
        AdministratableDocumentList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        AdministratableDocument Save(AdministratableDocument document);

        #endregion
    }

    [XmlSerializerFormat]
    public class AdministratableDocumentClient : SpringRestClient<IAdministratableDocumentService>, IAdministratableDocumentService, IPollClient<AdministratableDocument>
    {
        #region IAdministratableDocumentService

        [WebInvoke(Method = "GET", UriTemplate = "/administratableDocument/allPending")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public AdministratableDocumentList GetAll()
        {
            return Execute<AdministratableDocumentList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "GET", UriTemplate = "/administratableDocument/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public AdministratableDocumentList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<AdministratableDocumentList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "POST", UriTemplate = "/administratableDocument/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public AdministratableDocument Save(AdministratableDocument document)
        {
            return ExecuteWithBody<AdministratableDocument>(System.Reflection.MethodInfo.GetCurrentMethod(), document);
        }

        #endregion

        #region IPollClient<AdministratableDocument>

        public IList<AdministratableDocument> FetchAll()
        {
            return GetAll();
        }
        public IList<AdministratableDocument> FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}