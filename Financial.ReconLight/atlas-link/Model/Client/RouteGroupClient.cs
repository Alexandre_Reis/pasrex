﻿using System;
using System.Runtime.CompilerServices;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;
using System.Text;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IRouteGroupService
    {
        #region REST Methods

        [OperationContract]
        RouteGroupList GetAllGroups();

        [OperationContract]
        RouteGroup Save(RouteGroup route);

        [OperationContract]
        StandardResponse Delete(String routeId);

        [OperationContract]
        RouteGroupList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        StandardResponse Trigger(String groupId);

        #endregion
    }

    [XmlSerializerFormat]
    public class RouteGroupClient : SpringRestClient<IRouteGroupService>, IRouteGroupService, IPollClient<RouteGroup>, IUpdateClient<RouteGroup>
    {
        #region IRouteService

        [WebInvoke(Method = "GET", UriTemplate = "/routegroup/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteGroupList GetAllGroups()
        {
            return Execute<RouteGroupList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "POST", UriTemplate = "/routegroup/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteGroup Save(RouteGroup group)
        {
            return ExecuteWithBody<RouteGroup>(System.Reflection.MethodInfo.GetCurrentMethod(), group);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/routegroup/{routeId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String routeId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
            return null; //TODO...
        }

        [WebInvoke(Method = "GET", UriTemplate = "/routegroup/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteGroupList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<RouteGroupList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "GET", UriTemplate = "/routegroup/{groupId}/trigger")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Trigger(String groupId)
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), groupId);
        }
        #endregion

        #region IPollClient

        System.Collections.Generic.IList<RouteGroup> IPollClient<RouteGroup>.FetchAll()
        {
            return GetAllGroups();
        }

        System.Collections.Generic.IList<RouteGroup> IPollClient<RouteGroup>.FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }
        #endregion
    }
}