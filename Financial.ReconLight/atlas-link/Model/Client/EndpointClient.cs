﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IEndpointService
    {
        #region REST Methods

        [OperationContract]
        EndpointList GetAllEndpoints();

        [OperationContract]
        Endpoint Save(Endpoint endpoint);

        [OperationContract]
        StandardResponse Delete(String endpointId);

        [OperationContract]
        StandardResponse Enable(String endpointId);

        [OperationContract]
        StandardResponse Disable(String endpointId);

        [OperationContract]
        EndpointList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        EndpointAttributeDefinitionList GetMetaData();

        #endregion
    }

    [XmlSerializerFormat]
    public class EndpointClient : SpringRestClient<IEndpointService>, IEndpointService, IPollClient<Endpoint>, IUpdateClient<Endpoint>
    {
        #region IEndpointService

        [WebInvoke(Method = "GET", UriTemplate = "/endpoint/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public EndpointList GetAllEndpoints()
        {
            return Execute<EndpointList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "POST", UriTemplate = "/endpoint/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Endpoint Save(Endpoint endpoint)
        {
            return ExecuteWithBody<Endpoint>(System.Reflection.MethodInfo.GetCurrentMethod(), endpoint);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/endpoint/{endpointId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String endpointId)
        {
            return Execute<StandardValueResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), endpointId);
            //Execute(System.Reflection.MethodInfo.GetCurrentMethod(), endpointId);
        }

        [WebInvoke(Method = "POST", UriTemplate = "/endpoint/{endpointId}/enabled")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Enable(String endpointId)
        {
            return Execute<StandardValueResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), endpointId);
        }

        [WebInvoke(Method = "POST", UriTemplate = "/endpoint/{endpointId}/disabled")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Disable(String endpointId)
        {
            return Execute<StandardValueResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), endpointId);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/endpoint/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public EndpointList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<EndpointList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "GET", UriTemplate = "/endpoint/metadata")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public EndpointAttributeDefinitionList GetMetaData()
        {
            return Execute<EndpointAttributeDefinitionList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        #endregion

        #region IPollClient<Endpoint>

        public IList<Endpoint> FetchAll()
        {
            return GetAllEndpoints();
        }
        public IList<Endpoint> FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}