﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;
using Atlas.Link.Model.Recon;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IMessageService
    {
        #region REST Methods

        [OperationContract]
        MessageList GetMessages();

        [OperationContract]
        MessageList GetMessagesBetween(DateTime startDate, DateTime endDate, Boolean updatesOnly = false);

        [OperationContract]
        MessageList GetMessagesFrom(DateTime startDate, Boolean updatesOnly = false);

        [OperationContract]
        MessageList GetMessagesUntil(DateTime endDate, Boolean updatesOnly = false);

        [OperationContract]
        MessageDocumentList GetMessageDocuments(String messageId); 

        [OperationContract]
        LinkReconciliationResult GetReconciliationResult(String messageId);

        #endregion
    }

    [XmlSerializerFormat]
    public class MessageClient : SpringRestClient<IMessageService>, IMessageService, IPollClient<Message>
    {
        /// <summary>
        /// Gets a list of Transformation Messages based on conditional criteria.
        /// This method is a wrapper for the specific method calls below.
        /// </summary>
        /// <param name="startDateTime">The start date time.</param>
        /// <param name="endDateTime">The end date time.</param>
        /// <param name="updatesOnly">if set to <c>true</c> [updates only].</param>
        /// <param name="messageStatus">The message status.</param>
        /// <returns></returns>
        public MessageList GetMessages(DateTime? startDateTime, DateTime? endDateTime, Boolean updatesOnly, String messageStatus)
        {
            if (startDateTime.HasValue && endDateTime.HasValue)
            {
                if (String.IsNullOrWhiteSpace(messageStatus))
                    return this.GetMessagesBetween(startDateTime.Value, endDateTime.Value, updatesOnly);

                return this.GetMessagesBetween(startDateTime.Value, endDateTime.Value, updatesOnly, messageStatus);
            }
            if (startDateTime.HasValue)
            {
                if (String.IsNullOrWhiteSpace(messageStatus))
                    return this.GetMessagesFrom(startDateTime.Value, updatesOnly);

                return this.GetMessagesFrom(startDateTime.Value, updatesOnly, messageStatus);
            }
            if (endDateTime.HasValue)
            {
                if (String.IsNullOrWhiteSpace(messageStatus))
                    return this.GetMessagesUntil(endDateTime.Value, updatesOnly);

                return this.GetMessagesUntil(endDateTime.Value, updatesOnly, messageStatus);
            }

            return this.GetMessages();
        }

        #region IMessageService

        [WebInvoke(Method = "GET", UriTemplate = "/message/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public MessageList GetMessages()
        {
            return Execute<MessageList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "GET", UriTemplate = "/message/list?startDate={startDate}&endDate={endDate}&updatesOnly={updatesOnly}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public MessageList GetMessagesBetween(DateTime startDate, DateTime endDate, Boolean updatesOnly = false)
        {
            return Execute<MessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(startDate), Utils.ToServerDatetime(endDate), updatesOnly);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/message/list?startDate={startDate}&endDate={endDate}&updatesOnly={updatesOnly}&messageStatus={messageStatus}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public MessageList GetMessagesBetween(DateTime startDate, DateTime endDate, Boolean updatesOnly, String messageStatus)
        {
            return Execute<MessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(startDate), Utils.ToServerDatetime(endDate), updatesOnly, messageStatus);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/message/list?startDate={startDate}&updatesOnly={updatesOnly}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public MessageList GetMessagesFrom(DateTime startDate, Boolean updatesOnly = false)
        {
            return Execute<MessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(startDate), updatesOnly);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/message/list?startDate={startDate}&updatesOnly={updatesOnly}&messageStatus={messageStatus}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public MessageList GetMessagesFrom(DateTime startDate, Boolean updatesOnly, String messageStatus)
        {
            return Execute<MessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(startDate), updatesOnly, messageStatus);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/message/list?endDate={endDate}&updatesOnly={updatesOnly}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public MessageList GetMessagesUntil(DateTime endDate, Boolean updatesOnly = false)
        {
            return Execute<MessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(endDate), updatesOnly);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/message/list?endDate={endDate}&updatesOnly={updatesOnly}&messageStatus={messageStatus}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public MessageList GetMessagesUntil(DateTime endDate, Boolean updatesOnly, String messageStatus)
        {
            return Execute<MessageList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(endDate), updatesOnly, messageStatus);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/message/{messageId}/filedata")]
        [XmlSerializerFormat]
        public MessageDocumentList GetMessageDocuments(String messageId) 
        {
            return Execute<MessageDocumentList>(System.Reflection.MethodInfo.GetCurrentMethod(), messageId);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/message/{messageId}/reconResult")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public LinkReconciliationResult GetReconciliationResult(String messageId)
        {
            return Execute<LinkReconciliationResult>(System.Reflection.MethodInfo.GetCurrentMethod(), messageId);
        }

        #endregion

        #region IPollClient<Message>

        public IList<Message> FetchAll()
        {
            return GetMessages();
        }
        public IList<Message> FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetMessagesFrom(lastUpdatedDate, true);
        }

        #endregion

    }
}