﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IReconciliationService
    {
        #region REST Methods

        [OperationContract]
        ReconciliationList AllRecons();

        [OperationContract]
        Reconciliation Save(Reconciliation reconciliation);

        [OperationContract]
        StandardResponse Delete(String reconciliationId);

        [OperationContract]
        ReconciliationList GetUpdates(DateTime lastUpdatedDate);

        #endregion
    }

    [XmlSerializerFormat]
    public class ReconciliationClient : SpringRestClient<IReconciliationService>, IReconciliationService, IPollClient<Reconciliation>, IUpdateClient<Reconciliation>
    {
        #region IReconciliationService

        [WebInvoke(Method = "GET", UriTemplate = "/recon/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationList AllRecons()
        {
            return Execute<ReconciliationList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "POST", UriTemplate = "/recon/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Reconciliation Save(Reconciliation reconciliation)
        {
            return ExecuteWithBody<Reconciliation>(System.Reflection.MethodInfo.GetCurrentMethod(), reconciliation);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/recon/{reconId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String reconciliationId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), reconciliationId);
            return null; //TODO...
        }

        [WebInvoke(Method = "GET", UriTemplate = "/recon/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public ReconciliationList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<ReconciliationList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        #endregion

        public System.Collections.Generic.IList<Reconciliation> FetchAll()
        {
            return AllRecons();
        }

        public System.Collections.Generic.IList<Reconciliation> FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }
    }
}