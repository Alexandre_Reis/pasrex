﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IRouteScheduleService
    {
        #region REST Methods

        [OperationContract]
        RouteScheduleList GetAllSchedules();

        [OperationContract]
        RouteScheduleList GetSchedulesForRoute(String routeId);

        [OperationContract]
        RouteScheduleList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        RouteSchedule Save(RouteSchedule schedule);

        [OperationContract]
        StandardResponse Delete(String scheduleId);

        #endregion
    }

    [XmlSerializerFormat]
    public class RouteScheduleClient : SpringRestClient<IRouteScheduleService>, IRouteScheduleService, IPollClient<RouteSchedule>, IUpdateClient<RouteSchedule>
    {
        #region IRouteScheduleService

        [WebInvoke(Method = "GET", UriTemplate = "/schedule/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteScheduleList GetAllSchedules()
        {
            return Execute<RouteScheduleList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "GET", UriTemplate = "/schedule/forRoute?routeId={routeId}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteScheduleList GetSchedulesForRoute(String routeId)
        {
            return Execute<RouteScheduleList>(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
        }

        [WebInvoke(Method = "GET", UriTemplate = "/schedule/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteScheduleList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<RouteScheduleList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "POST", UriTemplate = "/schedule/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteSchedule Save(RouteSchedule schedule)
        {
            return ExecuteWithBody<RouteSchedule>(System.Reflection.MethodInfo.GetCurrentMethod(), schedule);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/schedule/{scheduleId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String scheduleId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), scheduleId);
            return null; //TODO...
        }

        #endregion

        #region IPollClient<Endpoint>

        public IList<RouteSchedule> FetchAll()
        {
            return GetAllSchedules();
        }
        public IList<RouteSchedule> FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}