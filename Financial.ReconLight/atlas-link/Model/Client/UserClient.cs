﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IUserService
    {
        #region REST Methods

        [OperationContract]
        UserList GetAllUsers();

        [OperationContract]
        StandardResponse ResetUserPassword(String userName);

        [OperationContract]
        User Save(User user);

        [OperationContract]
        StandardResponse Delete(string userId);

        [OperationContract]
        UserList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        PermissionList Permissions();

        #endregion
    }

    [XmlSerializerFormat]
    public class UserClient : SpringRestClient<IUserService>, IUserService, IPollClient<User>, IUpdateClient<User>
    {
        #region Construction

        /// <summary>
        /// Initializes a new instance of the <see cref="UserClient" /> class.
        /// </summary>
        public UserClient() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserClient" /> class.
        /// </summary>
        /// <param name="connectionTimeout">The connection timeout.</param>
        public UserClient(Int32 connectionTimeout) : base(connectionTimeout) { }

        #endregion

        #region IUserService

        [WebInvoke(Method = "GET", UriTemplate = "/user/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public UserList GetAllUsers()
        {
            return Execute<UserList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }


        [WebInvoke(Method = "POST", UriTemplate = "/user/{userName}/passwordReset")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse ResetUserPassword(string userName)
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), userName);
        }


        [WebInvoke(Method = "POST", UriTemplate = "/user/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public User Save(User user)
        {
            return ExecuteWithBody<User>(System.Reflection.MethodInfo.GetCurrentMethod(), user);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/user/{userId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String userId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), userId);
            return null; //TODO...
        }

        [WebInvoke(Method = "GET", UriTemplate = "/user/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public UserList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<UserList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "GET", UriTemplate = "/user/permissions")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public PermissionList Permissions()
        {
            return Execute<PermissionList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        #endregion

        #region IPollClient<User>

        public IList<User> FetchAll()
        {
            return GetAllUsers();
        }

        public IList<User> FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}
