﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Atlas.Link.Model;

namespace Atlas.Link.Client
{
    /// <summary>
    /// A Generic DataSource that is kept up-to-date on a separate thread
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public class PollingDataSource<TEntity> : IDisposable
        where TEntity : BaseAuditableEntity
    {
        #region Types

        // used to cache data by key
        private class DataFetchParams
        {
            public DateTime LastFetched = DateTime.MinValue;
            public DateTime LastUpdatedAt = DateTime.MinValue;
        }

        // used to send data across threads
        private class RestDataParam
        {
            public Boolean ClearPrevious = false;
            public IList<TEntity> DataList;
        }

        #endregion

        #region Constructors

        public PollingDataSource(IPollClient<TEntity> restClient, Action<IList<TEntity>> dataUpdateHandler)
        {
            System.Diagnostics.Contracts.Contract.Requires(restClient != null, "restClient parameter is mandatory");
            System.Diagnostics.Contracts.Contract.Requires(dataUpdateHandler != null, "dataUpdateHandler parameter is mandatory");
            System.Diagnostics.Contracts.Contract.Requires(System.Threading.SynchronizationContext.Current != null, "Calling thread SynchronizationContext is null");

            // initialize logging
            _logger = Common.Logging.LogManager.Adapter.GetLogger(this.GetType());

            // set the parameters
            _restClient = restClient;
            _dataUpdateHandler = dataUpdateHandler;
            _callingContext = System.Threading.SynchronizationContext.Current; // get the calling (UI) thread context

            // initialize state
            _threadHasStopped = new ManualResetEvent(true);
            _threadStopRequest = new ManualResetEvent(true);
        }

        #endregion

        #region Fields

        // eventually parameterize this
        private static Int32 DataUpdateWait = 2000; // 2000;
        private static Int32 PollInterval = 250;
        private static Int32 StartupDelay = 250;

        private Common.Logging.ILog _logger;

        //private Boolean _disposing = false;
        private SynchronizationContext _callingContext = null;
        private ManualResetEvent _threadHasStopped;  // the thread indicates it has stopped
        private ManualResetEvent _threadStopRequest; // tells the thread to stop processing

        private IPollClient<TEntity> _restClient = null;
        private Action<IList<TEntity>> _dataUpdateHandler = null;
        //private TransformationRouteClient transformationRouteClient;
        //private Action<IList<TransformationRoute>> action;

        #endregion

        #region Properties

        public Boolean IsStopped
        {
            get { return _threadHasStopped.WaitOne(0); }
        }

        #endregion

        #region Thread Members (executed on Worker thread)

        private void ThreadMainProc()
        {
            try
            {
                _logger.Debug("ThreadMainProc: enter");

                // indicate that the thread started (used by Start/Stop)
                _threadHasStopped.Reset();

                Thread.Sleep(StartupDelay);

                DataFetchParams cacheData = ThreadFetchAllData();

                // the main thread loop
                while (_threadStopRequest.WaitOne(PollInterval) == false)
                {
                    if (DateTime.Now.Subtract(cacheData.LastFetched).TotalMilliseconds >= DataUpdateWait)
                    {
                        ThreadFetchUpdates(cacheData);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("ThreadMainProc", ex);
            }
            finally
            {
                _logger.Debug("ThreadMainProc: leave");

                // indicate that the thread stopped (used by Start/Stop)
                _threadHasStopped.Set();
            }
        }
        private DataFetchParams ThreadFetchAllData()
        {
            _logger.Debug("ThreadFetchAllData: enter");

            try
            {
                // record when we fetch the data
                DataFetchParams cacheData = new DataFetchParams
                {
                    LastFetched = DateTime.Now,
                    LastUpdatedAt = DateTime.Now,
                };

                // fetch the data
                System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();

                IList<TEntity> data = _restClient.FetchAll();

                sw.Stop();
                if (sw.Elapsed.TotalMilliseconds > 1000)
                {
                    _logger.WarnFormat("ThreadFetchAllData: duration={0}", sw.Elapsed);
                }


                // get the last modified date
                if ((data != null) && (data.Count > 0))
                {
                    //PERF: get the Last Modified value in the worker thread
                    cacheData.LastUpdatedAt = data.Max(x => x.UpdatedAt);

                    // don't update list if Stop() requested
                    if (_threadStopRequest.WaitOne(0) == false)
                    {
                        CallDataUpdateHandler(data);
                    }
                }

                return cacheData;
            }
            finally
            {
                _logger.Debug("ThreadFetchAllData: leave");
            }
        }
        private void ThreadFetchUpdates(DataFetchParams cacheData)
        {
            _logger.Debug("ThreadFetchUpdates: enter");

            try
            {
                // record when we fetch the data
                cacheData.LastFetched = DateTime.Now;

                // fetch only the changes
                IList<TEntity> data = _restClient.FetchUpdates(cacheData.LastUpdatedAt);

                // get the last modified date
                if ((data != null) && (data.Count > 0))
                {
                    //PERF: get the Last Modified value in the worker thread
                    cacheData.LastUpdatedAt = data.Max(x => x.UpdatedAt);

                    // don't update list if Stop() requested
                    if (_threadStopRequest.WaitOne(0) == false)
                    {
                        // invoke the List Update logic asynchronously on the calling (UI) thread
                        CallDataUpdateHandler(data);
                    }
                }
            }
            finally
            {
                _logger.Debug("ThreadFetchUpdates: leave");
            }
        }
        //private void SyncLists(IList<TEntity> sourceList, IList<TEntity> targetList)
        //{
        //    Atlas.Link.Model.Utils.SyncAuditableLists(sourceList, targetList, () => _threadStopRequest.WaitOne(0));
        //}

        #endregion

        #region Event Handlers (executed on Caller thread)

        private void CallDataUpdateHandler(IList<TEntity> data)
        {
            _dataUpdateHandler(data);
            //_callingContext.Send(DataUpdateHandler, data);
            //System.Windows.Application.Current.MainWindow.Dispatcher.Invoke(_dataUpdateHandler, data);
        }

        private void DataUpdateHandler(Object objectData)
        {
            IList<TEntity> data = (objectData as IList<TEntity>);

            if (data != null)
                _dataUpdateHandler(data);
        }

        #endregion

        #region Public Methods

        public void Start()
        {
            // check whether the thread has stopped
            if (_threadHasStopped.WaitOne(0))
            {
                // create a new worker thread
                Thread worker = new Thread(ThreadMainProc);
                worker.IsBackground = true; // don't prevent main thread from shutting down

                // reset the main control event
                _threadStopRequest.Reset();   // allow the thread to run

                // get a new thread instance running
                worker.Start();
                Thread.Sleep(100); // give the thread some time to get going (only gets called On Create)
            }
        }
        public void Stop()
        {
            // check whether the thread is running
            if (_threadHasStopped.WaitOne(0) == false)
            {
                // tell the thread to stop
                _threadStopRequest.Set();

                // wait for the thread to finish
                _threadHasStopped.WaitOne(250);
            }
        }

        #endregion

        #region IDisposable

        void IDisposable.Dispose()
        {
            //_disposing = true;        // tell thread to bypass event handlers
            Stop();
        }

        #endregion
    }
}