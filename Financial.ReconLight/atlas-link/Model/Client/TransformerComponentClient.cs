﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;
using System.Collections.Generic;
using Spring.Rest.Client;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface ITransformerComponentService
    {
        #region REST Methods

        [OperationContract]
        TransformerComponentList GetAllComponents();

        [OperationContract]
        TransformerComponent Save(TransformerComponent component);

        [OperationContract]
        StandardResponse Delete(String componentId);

        [OperationContract]
        TransformerComponentList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        String Upload(String fileName);

        [OperationContract]
        String UploadTransformationResource(String fileName);

        [OperationContract]
        FileList ListTransformationResources();

        #endregion
    }

    [XmlSerializerFormat]
    public class TransformerComponentClient : SpringRestClient<ITransformerComponentService>, ITransformerComponentService, IPollClient<TransformerComponent>, IUpdateClient<TransformerComponent>
    {
        #region ITransformerComponentService

        [WebInvoke(Method = "GET", UriTemplate = "/component/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransformerComponentList GetAllComponents()
        {
            return Execute<TransformerComponentList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "POST", UriTemplate = "/component/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransformerComponent Save(TransformerComponent component)
        {
            return ExecuteWithBody<TransformerComponent>(System.Reflection.MethodInfo.GetCurrentMethod(), component);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/component/{componentId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String componentId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), componentId);
            return null; //TODO...
        }

        [WebInvoke(Method = "GET", UriTemplate = "/component/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TransformerComponentList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<TransformerComponentList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "POST", UriTemplate = "/component/upload?file={fileName}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public String Upload(String fileName)
        {
            //return ExecuteWithFile<String>(System.Reflection.MethodInfo.GetCurrentMethod(), fileName, System.IO.Path.GetFileName(fileName));

            //HttpRequestBinding binding = GeHttpRequestInfo(caller); // get the WebInvoke values from the caller
            //FileResource fileResource = new FileResource(fileName); // use a file resource as the body
            //HttpEntity entity = new HttpEntity(fileResource);       // get the headers and body
            //entity.Headers.Add("file", fileName);
            //RestTemplate template = GetRestTemplate();

            //HttpResponseMessage<TResult> response = template.Exchange<TResult>(binding.Url, binding.Method, entity, uriVariables);

            //return response.Body; ;


            String shortFileName = System.IO.Path.GetFileName(fileName);
            String url = "/component/upload?file=" + Spring.Http.HttpUtils.UrlEncode(shortFileName);

            Atlas.Link.Net.IConnectionProvider connectionProvider = ServiceLocator.Get<Atlas.Link.Net.IConnectionProvider>();
            RestTemplate restTemplate = GetRestTemplate();
            restTemplate.BaseAddress = connectionProvider.GetServers("")[0].URI;

            IDictionary<String, Object> multiParts = new Dictionary<String, Object>();

            multiParts.Add("file", new Spring.IO.FileResource(fileName));

            var responseMessage = restTemplate.PostForMessage<String>(url, multiParts);
            String responseFileName = responseMessage.Body;

            if (String.IsNullOrWhiteSpace(responseFileName))
            {
                Logger.Error("/component/upload returned a null result. Setting file name to original file name");
                return shortFileName;
            }
            else
            {
                return responseFileName;
            }
        }

        //@RequestMapping(value = "/transformationResourceUpload", method = RequestMethod.POST)
        //public @ResponseBody String uploadTransformationResource(@RequestParam("file") MultipartFile multipartFile) throws Exception {
        [WebInvoke(Method = "POST", UriTemplate = "/component/transformationResourceUpload?file={fileName}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public String UploadTransformationResource(String fileName)
        {
            String shortFileName = System.IO.Path.GetFileName(fileName);
            String url = "/component/transformationResourceUpload?file=" + Spring.Http.HttpUtils.UrlEncode(shortFileName);

            Atlas.Link.Net.IConnectionProvider connectionProvider = ServiceLocator.Get<Atlas.Link.Net.IConnectionProvider>();
            RestTemplate restTemplate = GetRestTemplate();
            restTemplate.BaseAddress = connectionProvider.GetServers("")[0].URI;

            IDictionary<String, Object> multiParts = new Dictionary<String, Object>();

            multiParts.Add("file", new Spring.IO.FileResource(fileName));

            var responseMessage = restTemplate.PostForMessage<String>(url, multiParts);
            String responseFileName = responseMessage.Body;

            if (String.IsNullOrWhiteSpace(responseFileName))
            {
                Logger.Error("/component/transformationResourceUpload returned a null result. Setting file name to original file name");
                return shortFileName;
            }
            else
            {
                return responseFileName;
            }
        }

        [WebInvoke(Method = "GET", UriTemplate = "/component/listTransformationResources")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public FileList ListTransformationResources()
        {
            return Execute<FileList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        #endregion

        #region IPollClient

        System.Collections.Generic.IList<TransformerComponent> IPollClient<TransformerComponent>.FetchAll()
        {
            return GetAllComponents();
        }

        System.Collections.Generic.IList<TransformerComponent> IPollClient<TransformerComponent>.FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}