﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;
using System.Collections.Generic;
using Spring.Rest.Client;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IPPProcessorComponentService
    {
        #region REST Methods

        [OperationContract]
        PPProcessorComponentList GetAllComponents();

        [OperationContract]
        PPProcessorComponent Save(PPProcessorComponent component);

        [OperationContract]
        StandardResponse Delete(String componentId);

        [OperationContract]
        PPProcessorComponentList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        String Upload(String fileName);

        [OperationContract]
        String UploadPPProcessorResource(String fileName);

        [OperationContract]
        FileList ListPPProcessorResources();

        #endregion
    }

    [XmlSerializerFormat]
    public class PPProcessorComponentClient : SpringRestClient<IPPProcessorComponentService>, IPPProcessorComponentService, IPollClient<PPProcessorComponent>, IUpdateClient<PPProcessorComponent>
    {
        #region IPPProcessorComponentService

        [WebInvoke(Method = "GET", UriTemplate = "/ppprocessor/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public PPProcessorComponentList GetAllComponents()
        {
            return Execute<PPProcessorComponentList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "POST", UriTemplate = "/ppprocessor/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public PPProcessorComponent Save(PPProcessorComponent component)
        {
            return ExecuteWithBody<PPProcessorComponent>(System.Reflection.MethodInfo.GetCurrentMethod(), component);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/ppprocessor/{componentId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String componentId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), componentId);
            return null; //TODO...
        }

        [WebInvoke(Method = "GET", UriTemplate = "/ppprocessor/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public PPProcessorComponentList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<PPProcessorComponentList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "POST", UriTemplate = "/ppprocessor/upload?file={fileName}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public String Upload(String fileName)
        {
            //return ExecuteWithFile<String>(System.Reflection.MethodInfo.GetCurrentMethod(), fileName, System.IO.Path.GetFileName(fileName));

            //HttpRequestBinding binding = GeHttpRequestInfo(caller); // get the WebInvoke values from the caller
            //FileResource fileResource = new FileResource(fileName); // use a file resource as the body
            //HttpEntity entity = new HttpEntity(fileResource);       // get the headers and body
            //entity.Headers.Add("file", fileName);
            //RestTemplate template = GetRestTemplate();

            //HttpResponseMessage<TResult> response = template.Exchange<TResult>(binding.Url, binding.Method, entity, uriVariables);

            //return response.Body; ;


            String shortFileName = System.IO.Path.GetFileName(fileName);
            String url = "/ppprocessor/upload?file=" + Spring.Http.HttpUtils.UrlEncode(shortFileName);

            Atlas.Link.Net.IConnectionProvider connectionProvider = ServiceLocator.Get<Atlas.Link.Net.IConnectionProvider>();
            RestTemplate restTemplate = GetRestTemplate();
            restTemplate.BaseAddress = connectionProvider.GetServers("")[0].URI;

            IDictionary<String, Object> multiParts = new Dictionary<String, Object>();

            multiParts.Add("file", new Spring.IO.FileResource(fileName));

            var responseMessage = restTemplate.PostForMessage<String>(url, multiParts);
            String responseFileName = responseMessage.Body;

            if (String.IsNullOrWhiteSpace(responseFileName))
            {
                Logger.Error("/ppprocessor/upload returned a null result. Setting file name to original file name");
                return shortFileName;
            }
            else
            {
                return responseFileName;
            }
        }

        
        [WebInvoke(Method = "POST", UriTemplate = "/ppprocessor/ppprocessorResourceUpload?file={fileName}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public String UploadPPProcessorResource(String fileName)
        {
            String shortFileName = System.IO.Path.GetFileName(fileName);
            String url = "/ppprocessor/ppprocessorResourceUpload?file=" + Spring.Http.HttpUtils.UrlEncode(shortFileName);

            Atlas.Link.Net.IConnectionProvider connectionProvider = ServiceLocator.Get<Atlas.Link.Net.IConnectionProvider>();
            RestTemplate restTemplate = GetRestTemplate();
            restTemplate.BaseAddress = connectionProvider.GetServers("")[0].URI;

            IDictionary<String, Object> multiParts = new Dictionary<String, Object>();

            multiParts.Add("file", new Spring.IO.FileResource(fileName));

            var responseMessage = restTemplate.PostForMessage<String>(url, multiParts);
            String responseFileName = responseMessage.Body;

            if (String.IsNullOrWhiteSpace(responseFileName))
            {
                Logger.Error("/ppprocessor/ppprocessorResourceUpload returned a null result. Setting file name to original file name");
                return shortFileName;
            }
            else
            {
                return responseFileName;
            }
        }

        [WebInvoke(Method = "GET", UriTemplate = "/ppprocessor/listPPProcessorResources")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public FileList ListPPProcessorResources()
        {
            return Execute<FileList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        #endregion

        #region IPollClient

        System.Collections.Generic.IList<PPProcessorComponent> IPollClient<PPProcessorComponent>.FetchAll()
        {
            return GetAllComponents();
        }

        System.Collections.Generic.IList<PPProcessorComponent> IPollClient<PPProcessorComponent>.FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}