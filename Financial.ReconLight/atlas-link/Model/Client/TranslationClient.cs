﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface ITranslationService
    {
        #region REST Methods

        [OperationContract]
        TranslationList GetTranslationsForLocale(String locale);

        #endregion
    }

    [XmlSerializerFormat]
    public class TranslationClient : SpringRestClient<ITranslationService>, ITranslationService
    {
        #region REST Method Implementation

        [WebInvoke(Method = "GET", UriTemplate = "/translation/list?locale={locale}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public TranslationList GetTranslationsForLocale(String locale)
        {
            return Execute<TranslationList>(System.Reflection.MethodInfo.GetCurrentMethod(), locale);
        }

        #endregion
    }
}