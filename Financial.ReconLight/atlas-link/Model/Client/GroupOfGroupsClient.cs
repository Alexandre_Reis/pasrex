﻿using System;
using System.Runtime.CompilerServices;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;
using System.Text;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IGroupOfGroupsService
    {
        #region REST Methods

        [OperationContract]
        GroupOfGroupsList GetAllGroups();

        [OperationContract]
        GroupOfGroups Save(GroupOfGroups route);

        [OperationContract]
        StandardResponse Delete(String routeId);

        [OperationContract]
        GroupOfGroupsList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        StandardResponse Trigger(String groupId);

        #endregion
    }

    [XmlSerializerFormat]
    public class GroupOfGroupsClient : SpringRestClient<IGroupOfGroupsService>, IGroupOfGroupsService, IPollClient<GroupOfGroups>, IUpdateClient<GroupOfGroups>
    {
        #region IRouteService

        [WebInvoke(Method = "GET", UriTemplate = "/gog/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public GroupOfGroupsList GetAllGroups()
        {
            return Execute<GroupOfGroupsList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "POST", UriTemplate = "/gog/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public GroupOfGroups Save(GroupOfGroups group)
        {
            return ExecuteWithBody<GroupOfGroups>(System.Reflection.MethodInfo.GetCurrentMethod(), group);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/gog/{routeId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String routeId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
            return null; //TODO...
        }

        [WebInvoke(Method = "GET", UriTemplate = "/gog/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public GroupOfGroupsList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<GroupOfGroupsList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "GET", UriTemplate = "/gog/{groupId}/trigger")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Trigger(String groupId)
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), groupId);
        }
        #endregion

        #region IPollClient

        System.Collections.Generic.IList<GroupOfGroups> IPollClient<GroupOfGroups>.FetchAll()
        {
            return GetAllGroups();
        }

        System.Collections.Generic.IList<GroupOfGroups> IPollClient<GroupOfGroups>.FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }
        #endregion
    }
}