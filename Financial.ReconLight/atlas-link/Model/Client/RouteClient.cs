﻿using System;
using System.Runtime.CompilerServices;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using Atlas.Link.Model;
using Atlas.Link.Net;
using System.Text;

namespace Atlas.Link.Client
{
    [ServiceContract]
    public interface IRouteService
    {
        #region REST Methods

        [OperationContract]
        RouteList GetAllRoutes();

        [OperationContract]
        RouteList GetAllRoutesForType(params Enums.RouteType[] routeType);

        [OperationContract]
        RouteList GetUpdates(DateTime lastUpdatedDate);

        [OperationContract]
        RouteList GetUpdatesForType(DateTime lastUpdatedDate, params Enums.RouteType[] routeTypes);

        [OperationContract]
        Route Save(Route route);

        [OperationContract]
        StandardResponse Delete(String routeId);

        [OperationContract]
        StandardResponse Enable(String routeId);

        [OperationContract]
        StandardResponse Disable(String routeId);

        [OperationContract]
        StandardResponse Trigger(String routeId);


        #endregion
    }

    [XmlSerializerFormat]
    public class RouteClient : SpringRestClient<IRouteService>, IRouteService, IPollClient<Route>, IUpdateClient<Route>
    {
        #region IRouteService

        private static String GetRouteTypeParamString(params Enums.RouteType[] routeTypes)
        {
            StringBuilder routetypeParam = new StringBuilder();

            foreach (var rtyp in routeTypes)
            {
                if (routetypeParam.Length > 0)
                    routetypeParam.Append("|");
                routetypeParam.Append(rtyp.ToString());
            }

            return routetypeParam.ToString();
        }

        [WebInvoke(Method = "GET", UriTemplate = "/route/list")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteList GetAllRoutes()
        {
            return Execute<RouteList>(System.Reflection.MethodInfo.GetCurrentMethod());
        }

        [WebInvoke(Method = "GET", UriTemplate = "/route/list?route_type={routeType}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteList GetAllRoutesForType(params Enums.RouteType[] routeTypes)
        {
            return Execute<RouteList>(System.Reflection.MethodInfo.GetCurrentMethod(), GetRouteTypeParamString(routeTypes));
        }

        [WebInvoke(Method = "GET", UriTemplate = "/route/updates?date={lastUpdatedDate}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteList GetUpdates(DateTime lastUpdatedDate)
        {
            return Execute<RouteList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate));
        }

        [WebInvoke(Method = "GET", UriTemplate = "/route/updates?date={lastUpdatedDate}&route_type={routeType}")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public RouteList GetUpdatesForType(DateTime lastUpdatedDate, params Enums.RouteType[] routeTypes)
        {
            return Execute<RouteList>(System.Reflection.MethodInfo.GetCurrentMethod(), Utils.ToServerDatetime(lastUpdatedDate), GetRouteTypeParamString(routeTypes));
        }

        [WebInvoke(Method = "POST", UriTemplate = "/route/save")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public Route Save(Route route)
        {
            return ExecuteWithBody<Route>(System.Reflection.MethodInfo.GetCurrentMethod(), route);
        }

        [WebInvoke(Method = "DELETE", UriTemplate = "/route/{routeId}/delete")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Delete(String routeId)
        {
            Execute(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
            return null; //TODO...
        }

        [WebInvoke(Method = "POST", UriTemplate = "/route/{routeId}/enabled")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Enable(String routeId)
        {
            return Execute<StandardValueResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
        }

        [WebInvoke(Method = "POST", UriTemplate = "/route/{routeId}/disabled")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Disable(String routeId)
        {
            return Execute<StandardValueResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
        }


        [WebInvoke(Method = "GET", UriTemplate = "/route/{routeId}/trigger")]
        [XmlSerializerFormat]
        [MethodImpl(MethodImplOptions.NoInlining)]
        public StandardResponse Trigger(String routeId)
        {
            return Execute<StandardResponse>(System.Reflection.MethodInfo.GetCurrentMethod(), routeId);
        }

        #endregion

        #region IPollClient

        System.Collections.Generic.IList<Route> IPollClient<Route>.FetchAll()
        {
            return GetAllRoutes();
        }

        System.Collections.Generic.IList<Route> IPollClient<Route>.FetchUpdates(DateTime lastUpdatedDate)
        {
            return GetUpdates(lastUpdatedDate);
        }

        #endregion
    }
}