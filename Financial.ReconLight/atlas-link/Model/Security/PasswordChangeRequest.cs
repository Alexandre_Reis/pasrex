﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "password_change_request", Namespace = "")]
    public class PasswordChangeRequest
    {
        #region Serializable Properties
        
        [XmlElement(ElementName = "user_name")]
        public String UserName { get; set; }

        [XmlElement(ElementName = "old_password")]
        public String OldPassword { get; set; }

        [XmlElement(ElementName = "new_password")]
        public String NewPassword { get; set; }


        #endregion
    }
}