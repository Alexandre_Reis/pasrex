﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Model.Security
{
    public enum PermissionCode
    {
        ROUTE_SAVE,
        ROUTE_DELETE,
        ROUTE_READ,
        USER_PASSWORD_RESET,
        USER_SAVE,
        USER_DELETE,
        USER_READ,
        AUDIT_TRAIL_READ,
        ENDPOINT_READ,
        ENDPOINT_SAVE,
        ENDPOINT_DELETE,
        HEARTBEAT_SAVE,
        MESSAGE_READ,
        RECON_READ,
        RECON_SAVE,
        RECON_DELETE,
        RECON_TRIGGER,
        SCHEDULE_READ,
        SCHEDULE_SAVE,
        SCHEDULE_DELETE,
        COMPONENT_READ,
        COMPONENT_SAVE,
        COMPONENT_DELETE,
        ROUTES_SUSPENDED,
        ROUTES_ENABLED,
        SETTLEMENT_READ,
        SETTLEMENT_SEND,
        ECD_READ,
        ECD_SAVE, 
        ECD_DELETE,
        USER_ADMINISTRATION, 
        HOLIDAY_SAVE
    }
}