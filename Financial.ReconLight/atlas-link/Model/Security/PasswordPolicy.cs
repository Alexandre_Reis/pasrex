﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model.Security
{
    [Serializable]
    [XmlRoot(ElementName = "password_policy", Namespace = "")]
    public class PasswordPolicy
    {
        #region Serializable Properties

        [XmlElement(ElementName = "illegal_characters")]
        public String IllegalCharacters { get; set; }

        [XmlElement(ElementName = "repeat_characters")]
        public Int32 RepeatCharacters { get; set; }

        [XmlElement(ElementName = "restrict_qwerty")]
        public Boolean IsRestrictQwerty { get; set; }

        [XmlElement(ElementName = "restrict_numerical_sequence")]
        public Boolean IsRestrictNumericalSequence { get; set; }

        [XmlElement(ElementName = "restrict_alpha_sequence")]
        public Boolean IsRestrictAlphaSequence { get; set; }

        [XmlElement(ElementName = "length_min")]
        public Int32 LengthMin { get; set; }

        [XmlElement(ElementName = "length_max")]
        public Int32 LengthMax { get; set; }

        [XmlElement(ElementName = "allow_whitespaces")]
        public Boolean IsAllowWhitespaces { get; set; }

        [XmlElement(ElementName = "digits")]
        public Int32 Digits { get; set; }

        [XmlElement(ElementName = "non_alpha_numeric")]
        public Int32 NonAlphaNumeric { get; set; }

        [XmlElement(ElementName = "uppercase")]
        public Int32 Uppercase { get; set; }

        [XmlElement(ElementName = "lowercase")]
        public Int32 Lowercase { get; set; }

        [XmlElement(ElementName = "num_required_valid")]
        public Int32 NumRequiredValid { get; set; }

        #endregion
    }
}