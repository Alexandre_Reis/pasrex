﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "user_right", Namespace = "")]
    public class UserRight : BaseEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "code")]
        public string Code { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "rights", Namespace = "")]
    public class UserRightList : XmlSerializableList<UserRight> { }
}