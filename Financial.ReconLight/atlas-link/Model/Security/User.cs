﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;
using System.Collections.Generic;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "user", Namespace = "")]
    public class User : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "email_address")]
        public String EmailAddress { get; set; }

        [XmlElement(ElementName = "first_name")]
        public String FirstName { get; set; }

        [XmlElement(ElementName = "last_logon")]
        public DateTime? LastLogon { get; set; }

        [XmlElement(ElementName = "last_name")]
        public String LastName { get; set; }

        [XmlElement(ElementName = "last_password_update_at")]
        public DateTime? LastPasswordUpdateAt { get; set; }

        [XmlElement(ElementName = "password_expiry")]
        public DateTime? PasswordExpiry { get; set; }

        [XmlArray(ElementName = "rights")]
        [XmlArrayItem(ElementName = "user_right")]
        public List<UserRight> Rights { get; set; }

        [XmlElement(ElementName = "status")]
        public Enums.UserStatus Status { get; set; }

        [XmlElement(ElementName = "user_name")]
        public String UserName { get; set; }
        #endregion

    }

    [XmlRoot(ElementName = "users", Namespace = "")]
    public class UserList : XmlSerializableList<User> { }

}