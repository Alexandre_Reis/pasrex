﻿using System;
using System.Xml.Serialization;
using Atlas.Link.Serialization;
using System.Collections.Generic;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "user_group", Namespace = "")]
    public class UserGroup : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "description")]
        public String Description { get; set; }

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        [XmlArray(ElementName = "rights")]
        [XmlArrayItem(ElementName = "user_right")]
        public List<UserRight> Rights { get; set; }

        [XmlElement(ElementName = "status")]
        public Enums.Status Status { get; set; }

        [XmlArray(ElementName = "users")]
        [XmlArrayItem(ElementName = "user")]
        public List<User> Users { get; set; }

        #endregion
    }

    [XmlRoot(ElementName = "user_groups", Namespace = "")]
    public class UserGroupList : XmlSerializableList<UserGroup> { }

}
