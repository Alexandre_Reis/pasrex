﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    public enum LogonStatus
    {
        SUCCESS, ERROR, INVALID_PASSWORD, ACCOUNT_DISABLED, PASSWORD_EXPIRED, INVALID_CREDENTIALS
    }

    [Serializable]
    [XmlRoot(ElementName = "logon_result", Namespace = "")]
    public class LogonResult
    {
        #region Serializable Properties

        [XmlElement(ElementName = "status")]
        public LogonStatus Status { get; set; }

        [XmlElement(ElementName = "message")]
        public String Message { get; set; }

        [XmlArray(ElementName = "permissions")]
        [XmlArrayItem(ElementName = "permission")]
        public List<Permission> Permissions { get; set; }
        #endregion
    }
}