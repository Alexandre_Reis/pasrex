﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    [Serializable]
    [XmlRoot(ElementName = "ppprocessor_component", Namespace = "")]
    public class PPProcessorComponent : BaseAuditableEntity
    {
        #region Serializable Properties

        [XmlElement(ElementName = "name")]
        public String Name { get; set; }

        [XmlElement(ElementName = "jar_file_name")]
        public String JarFileName { get; set; }

        [XmlElement(ElementName = "category")]
        public String Category { get; set; }

        [XmlElement(ElementName = "description")]
        public String Description { get; set; }

        [XmlElement(ElementName = "type")]
        public Enums.PPProcessorType Type { get; set; }

        [XmlElement(ElementName = "arguements")]
        public String Arguements { get; set; }
        
        #endregion
    }

    [XmlRoot(ElementName = "ppprocessor_components", Namespace = "")]
    public class PPProcessorComponentList : XmlSerializableList<PPProcessorComponent> { }
}