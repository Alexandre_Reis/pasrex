﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Atlas.Link.Serialization;

namespace Atlas.Link.Model
{
    /// <summary>
    /// The transformation route class
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "transformation_route", Namespace = "")]
    public class Route : BaseAuditableEntity
    {
        #region Serializable Properties

        /// <summary>
        /// Gets or sets the in component.
        /// </summary>
        /// <value>
        /// The in component.
        /// </value>
        [XmlElement(ElementName = "in_component")]
        public Endpoint InComponent { get; set; }

        // Web Service Roue only
        /// <summary>
        /// Gets or sets the transformer component.
        /// </summary>
        /// <value>
        /// The transformer component.
        /// </value>
        [XmlElement(ElementName = "transformer_component")]
        public TransformerComponent TransformerComponent { get; set; }

        /// <summary>
        /// Gets or sets the out components.
        /// </summary>
        /// <value>
        /// The out components.
        /// </value>
        [XmlArray(ElementName = "out_components")]
        [XmlArrayItem(ElementName = "out_component")]
        public List<Endpoint> OutComponents { get; set; }

        // Web Service Roue only
        /// <summary>
        /// Gets or sets the response component.
        /// </summary>
        /// <value>
        /// The response component.
        /// </value>
        [XmlElement(ElementName = "response_transformer")]
        public TransformerComponent ResponseComponent { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [XmlElement(ElementName = "status")]
        public Enums.Status Status { get; set; }

        /// <summary>
        /// Gets or sets the type of the route.
        /// </summary>
        /// <value>
        /// The type of the route.
        /// </value>
        [XmlElement(ElementName = "routeType")]
        public Enums.RouteType RouteType { get; set; }

        /// <summary>
        /// Gets or sets the transformer business rules.
        /// </summary>
        /// <value>
        /// The transformer business rules.
        /// </value>
        [XmlArray(ElementName = "transformer_business_rules")]
        [XmlArrayItem(ElementName = "transformer_business_rule")]
        public List<TransformerBusinessRule> TransformerBusinessRules { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        [XmlElement(ElementName = "category")]
        public String Category { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [XmlElement(ElementName = "description")]
        public String Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance can trigger.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance can trigger; otherwise, <c>false</c>.
        /// </value>
        [XmlElement(ElementName = "can_trigger")]
        public Boolean CanTrigger { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has schedule.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has schedule; otherwise, <c>false</c>.
        /// </value>
        [XmlElement(ElementName = "has_schedule")]
        public Boolean HasSchedule { get; set; }

        // Web Service Roue only???
        /// <summary>
        /// Gets or sets the structure.
        /// </summary>
        /// <value>
        /// The structure.
        /// </value>
        [XmlElement(ElementName = "route_structure")]
        public RouteStructure Structure { get; set; }

        /// <summary>
        /// Gets or sets the schedule.
        /// </summary>
        /// <value>
        /// The schedule.
        /// </value>
        [XmlElement(ElementName = "transformation_schedule")]
        public TransformationSchedule Schedule { get; set; }

        /// <summary>
        /// Gets or sets the pre processor component.
        /// </summary>
        /// <value>
        /// The pre processor component.
        /// </value>
        [XmlElement(ElementName = "pre_processor_component")]
        public PPProcessorComponent PreProcessorComponent { get; set; }

        /// <summary>
        /// Gets or sets the post processor component.
        /// </summary>
        /// <value>
        /// The post processor component.
        /// </value>
        [XmlElement(ElementName = "post_processor_component")]
        public PPProcessorComponent PostProcessorComponent { get; set; }

        /// <summary>
        /// Gets or sets the camel status.
        /// </summary>
        /// <value>
        /// The camel status.
        /// </value>
        [XmlElement(ElementName = "camelStatus")]
        public Enums.TransformationRouteStatus TransformationRouteStatus { get; set; }


        #endregion
    }

    /// <summary>
    /// Rout list class
    /// </summary>
    [XmlRoot(ElementName = "transformation_routes", Namespace = "")]
    public class RouteList : XmlSerializableList<Route> { }
}