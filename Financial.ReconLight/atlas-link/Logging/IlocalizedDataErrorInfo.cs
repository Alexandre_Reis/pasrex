﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Atlas.Link.Localization
{
    public interface IlocalizedDataErrorInfo
    {
        /// <summary>
        /// Returns a flag indicating whether errors exist;
        /// </summary>
        /// <value>
        /// The has errors.
        /// </value>
        Boolean HasErrors { get; }

        /// <summary>
        /// Validates the whole entity.
        /// </summary>
        Boolean PerformValidation();
    }
}