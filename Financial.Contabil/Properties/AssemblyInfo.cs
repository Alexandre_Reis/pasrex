﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Financial.Contabil")]
[assembly: AssemblyDescription("The Atatika Financial Contabil Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Atatika")]
[assembly: AssemblyProduct("Financial.Contabil")]
[assembly: AssemblyCopyright("Copyright © Atatika, 2006")]
[assembly: AssemblyTrademark("Financial Contabil is a legal trademark of Atatika")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("Financial.Contabil.snk")]

//[assembly: ReflectionPermission(SecurityAction.RequestRefuse, Unrestricted = true)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ba7b724e-a27d-4aa7-8474-e239be6e5f29")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
