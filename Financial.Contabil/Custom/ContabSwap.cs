﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.Swap;
using Financial.Contabil.Enums;
using Financial.Investidor;
using Financial.Util;

namespace Financial.Contabil
{
    public class ContabSwap
    {
        private Dictionary<int, int> roteiroContabil;
        private ContabRoteiroCollection contabRoteiroCollection;

        public ContabSwap(Dictionary<int, int> roteiroContabil, ContabRoteiroCollection contabRoteiroCollection)
        {
            this.roteiroContabil = roteiroContabil;
            this.contabRoteiroCollection = contabRoteiroCollection;
        }

        /// <summary>
        /// Executa todos os processos para geração dos eventos contábeis relativos a Swaps.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        public void ExecutaProcesso(Cliente cliente, DateTime data)
        {
            bool historico = cliente.DataDia.Value > data;
            this.GeraEventosAjuste(cliente, data, historico);
        }

        /// <summary>
        /// Eventos de Provisao, Pagamento para despesas diversas.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraEventosAjuste(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoSwapCollection posicaoSwapCollection = new PosicaoSwapCollection();            
            if (historico)
            {
                PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
                posicaoSwapHistoricoCollection.Query.Select(posicaoSwapHistoricoCollection.Query.Saldo,
                                                            posicaoSwapHistoricoCollection.Query.DataVencimento,
                                                            posicaoSwapHistoricoCollection.Query.IdPosicao);
                posicaoSwapHistoricoCollection.Query.Where(posicaoSwapHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoSwapHistoricoCollection.Query.ValorBase.NotEqual(0),
                                                           posicaoSwapHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoSwapHistoricoCollection.Query.Load();

                PosicaoSwapCollection posicaoSwapCollectionAux = new PosicaoSwapCollection(posicaoSwapHistoricoCollection);
                posicaoSwapCollection.Combine(posicaoSwapCollectionAux);
            }
            else
            {                
                posicaoSwapCollection.Query.Select(posicaoSwapCollection.Query.Saldo,
                                                   posicaoSwapCollection.Query.DataVencimento,
                                                   posicaoSwapCollection.Query.IdPosicao);
                posicaoSwapCollection.Query.Where(posicaoSwapCollection.Query.IdCliente.Equal(idCliente),
                                                  posicaoSwapCollection.Query.ValorBase.NotEqual(0));
                posicaoSwapCollection.Query.Load();
            }

            decimal saldoTotal = 0;
            decimal saldoVencimentoTotal = 0;           
            
            PosicaoSwapHistoricoQuery posicaoSwapQuery = new PosicaoSwapHistoricoQuery();
            posicaoSwapQuery.Select(posicaoSwapQuery.IdPosicao);
            posicaoSwapQuery.Where(posicaoSwapQuery.IdCliente.Equal(idCliente),
                                   posicaoSwapQuery.DataHistorico.Equal(data),
                                   posicaoSwapQuery.ValorBase.NotEqual(0));           
            
            PosicaoSwapAberturaCollection posicaoSwapAberturaCollection = new PosicaoSwapAberturaCollection();
            posicaoSwapAberturaCollection.Query.Select(posicaoSwapAberturaCollection.Query.Saldo,
                                                        posicaoSwapAberturaCollection.Query.DataVencimento,
                                                        posicaoSwapAberturaCollection.Query.IdPosicao);
            posicaoSwapAberturaCollection.Query.Where(posicaoSwapAberturaCollection.Query.IdCliente.Equal(idCliente),                                                       
                                                       posicaoSwapAberturaCollection.Query.ValorBase.NotEqual(0),
                                                       posicaoSwapAberturaCollection.Query.DataHistorico.Equal(data),
                                                       posicaoSwapAberturaCollection.Query.IdPosicao.NotIn(posicaoSwapQuery));

            posicaoSwapAberturaCollection.Query.Load();

            foreach (PosicaoSwapAbertura posicaoSwapAb in posicaoSwapAberturaCollection)
            {                

                if (posicaoSwapAb.DataVencimento.Value == data)
                {
                    saldoVencimentoTotal += posicaoSwapAb.Saldo.Value;
                }
            }


            

            DateTime dataProxima = new DateTime();
            if (posicaoSwapCollection.Count > 0)
            {
                dataProxima = Calendario.AdicionaDiaUtil(data, 1);
            }

            foreach (PosicaoSwap posicaoSwap in posicaoSwapCollection)
            {
                saldoTotal += posicaoSwap.Saldo.Value;
                
                if (posicaoSwap.DataVencimento.Value == data)
                {
                    saldoVencimentoTotal += posicaoSwap.Saldo.Value;
                }
            }
            if (saldoTotal > 0)
            {
                #region Evento de AjustePositivo/ReversaoAjustePositivo
                int origem = ContabilOrigem.Swap.AjustePositivo;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = saldoTotal;
                }

                origem = ContabilOrigem.Swap.ReversaoAjustePositivo;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = saldoTotal;
                }
                #endregion
            }
            else if (saldoTotal < 0)
            {
                #region Evento de AjusteNegativo/ReversaoAjusteNegativo
                int origem = ContabilOrigem.Swap.AjusteNegativo;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = saldoTotal;
                }

                origem = ContabilOrigem.Swap.ReversaoAjusteNegativo;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = saldoTotal;
                }
                #endregion
            }

            if (saldoVencimentoTotal > 0)
            {
                #region Evento de LiquidacaoReceber
                int origem = ContabilOrigem.Swap.LiquidacaoReceber;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = saldoVencimentoTotal;
                }
                #endregion
            }
            else if (saldoVencimentoTotal < 0)
            {
                #region Evento de LiquidacaoPagar
                int origem = ContabilOrigem.Swap.LiquidacaoPagar;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = saldoVencimentoTotal;
                }
                #endregion
            }

            contabLancamentoCollection.Save();
        }
    }
}
