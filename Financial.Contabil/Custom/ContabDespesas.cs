﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.Contabil.Enums;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Util;

namespace Financial.Contabil
{
    public class ContabDespesas
    {
        /// <summary>
        /// Executa todos os processos para geração dos eventos contábeis relativos às despesas recorrentes, taxa adm e taxa pfee.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        public void ExecutaProcesso(Cliente cliente, DateTime data)
        {
            DateTime dataProxima = Calendario.AdicionaDiaUtil(data, 1);

            bool historico = cliente.DataDia.Value > data;

            this.GeraEventosDespesasDiversas(cliente, data, dataProxima, historico);
            this.GeraEventosTaxaAdministracao(cliente, data, dataProxima, historico);
            this.GeraEventosTaxaPerformance(cliente, data, dataProxima, historico);
        }

        /// <summary>
        /// Eventos de Provisao, Pagamento para despesas diversas.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="dataProxima"></param>
        private void GeraEventosDespesasDiversas(Cliente cliente, DateTime data, DateTime dataProxima, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            CalculoProvisaoCollection calculoProvisaoCollection = new CalculoProvisaoCollection();

            if (historico)
            {
                CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();
                calculoProvisaoHistoricoCollection.Query.Select(calculoProvisaoHistoricoCollection.Query.IdTabela,
                                                       calculoProvisaoHistoricoCollection.Query.ValorDia,
                                                       calculoProvisaoHistoricoCollection.Query.ValorAcumulado,
                                                       calculoProvisaoHistoricoCollection.Query.DataFimApropriacao,
                                                       calculoProvisaoHistoricoCollection.Query.DataPagamento);
                calculoProvisaoHistoricoCollection.Query.Where(calculoProvisaoHistoricoCollection.Query.IdCarteira.Equal(idCliente),
                                                               calculoProvisaoHistoricoCollection.Query.DataHistorico.Equal(data));
                calculoProvisaoHistoricoCollection.Query.Load();

                CalculoProvisaoCollection calculoProvisaoCollectionAux = new CalculoProvisaoCollection(calculoProvisaoHistoricoCollection);

                calculoProvisaoCollection.Combine(calculoProvisaoCollectionAux);
            }
            else
            {                
                calculoProvisaoCollection.Query.Select(calculoProvisaoCollection.Query.IdTabela,
                                                       calculoProvisaoCollection.Query.ValorDia,
                                                       calculoProvisaoCollection.Query.ValorAcumulado,
                                                       calculoProvisaoCollection.Query.DataFimApropriacao,
                                                       calculoProvisaoCollection.Query.DataPagamento);
                calculoProvisaoCollection.Query.Where(calculoProvisaoCollection.Query.IdCarteira.Equal(idCliente));
                calculoProvisaoCollection.Query.Load();
            }

            foreach (CalculoProvisao calculoProvisao in calculoProvisaoCollection)
            {
                int idTabela = calculoProvisao.IdTabela.Value;
                decimal valorDia = calculoProvisao.ValorDia.Value;
                decimal valorAcumulado = calculoProvisao.ValorAcumulado.Value;
                DateTime dataFimApropriacao = calculoProvisao.DataFimApropriacao.Value;
                DateTime dataPagamento = calculoProvisao.DataPagamento.Value;

                TabelaProvisao tabelaProvisao = new TabelaProvisao();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tabelaProvisao.Query.IdEventoPagamento);
                campos.Add(tabelaProvisao.Query.IdEventoProvisao);
                tabelaProvisao.LoadByPrimaryKey(campos, idTabela);
                
                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;
                                
                if (Math.Abs(valorDia) > 0 && tabelaProvisao.IdEventoProvisao.HasValue)
                {
                    #region Evento de Provisao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(tabelaProvisao.IdEventoProvisao.Value),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.Despesas.Provisao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorDia;
                    }
                    #endregion
                }
                                
                if (dataFimApropriacao == dataProxima && Math.Abs(valorAcumulado) > 0 && tabelaProvisao.IdEventoPagamento.HasValue)
                {
                    #region Evento de Pagamento
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(tabelaProvisao.IdEventoPagamento.Value),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = dataPagamento;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.Despesas.Pagamento;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorAcumulado;
                    }
                    #endregion
                }
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de Provisao, Pagamento para despesas de taxa de administração/gestão ou correlatos.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="dataProxima"></param>
        private void GeraEventosTaxaAdministracao(Cliente cliente, DateTime data, DateTime dataProxima, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            CalculoAdministracaoCollection calculoAdministracaoCollection = new CalculoAdministracaoCollection();

            if (historico)
            {
                CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();
                calculoAdministracaoHistoricoCollection.Query.Select(calculoAdministracaoHistoricoCollection.Query.IdTabela,
                                                       calculoAdministracaoHistoricoCollection.Query.ValorDia,
                                                       calculoAdministracaoHistoricoCollection.Query.ValorAcumulado,
                                                       calculoAdministracaoHistoricoCollection.Query.DataFimApropriacao,
                                                       calculoAdministracaoHistoricoCollection.Query.DataPagamento);
                calculoAdministracaoHistoricoCollection.Query.Where(calculoAdministracaoHistoricoCollection.Query.IdCarteira.Equal(idCliente),
                                                               calculoAdministracaoHistoricoCollection.Query.DataHistorico.Equal(data));
                calculoAdministracaoHistoricoCollection.Query.Load();

                CalculoAdministracaoCollection calculoAdministracaoCollectionAux = new CalculoAdministracaoCollection(calculoAdministracaoHistoricoCollection);

                calculoAdministracaoCollection.Combine(calculoAdministracaoCollectionAux);
            }
            else
            {
                calculoAdministracaoCollection.Query.Select(calculoAdministracaoCollection.Query.IdTabela,
                                                       calculoAdministracaoCollection.Query.ValorDia,
                                                       calculoAdministracaoCollection.Query.ValorAcumulado,
                                                       calculoAdministracaoCollection.Query.DataFimApropriacao,
                                                       calculoAdministracaoCollection.Query.DataPagamento);
                calculoAdministracaoCollection.Query.Where(calculoAdministracaoCollection.Query.IdCarteira.Equal(idCliente));
                calculoAdministracaoCollection.Query.Load();
            }

            foreach (CalculoAdministracao calculoAdministracao in calculoAdministracaoCollection)
            {
                int idTabela = calculoAdministracao.IdTabela.Value;
                decimal valorDia = calculoAdministracao.ValorDia.Value;
                decimal valorAcumulado = calculoAdministracao.ValorAcumulado.Value;
                DateTime dataFimApropriacao = calculoAdministracao.DataFimApropriacao.Value;
                DateTime dataPagamento = calculoAdministracao.DataPagamento.Value;

                TabelaTaxaAdministracao tabelaTaxaAdministracao = new TabelaTaxaAdministracao();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tabelaTaxaAdministracao.Query.IdEventoPagamento);
                campos.Add(tabelaTaxaAdministracao.Query.IdEventoProvisao);
                tabelaTaxaAdministracao.LoadByPrimaryKey(campos, idTabela);

                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                if (Math.Abs(valorDia) > 0 && tabelaTaxaAdministracao.IdEventoProvisao.HasValue)
                {
                    #region Evento de Provisao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(tabelaTaxaAdministracao.IdEventoProvisao.Value),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.Despesas.Provisao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorDia;
                    }
                    #endregion
                }

                if (dataFimApropriacao == dataProxima && Math.Abs(valorAcumulado) > 0 && tabelaTaxaAdministracao.IdEventoPagamento.HasValue)
                {
                    #region Evento de Pagamento
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(tabelaTaxaAdministracao.IdEventoPagamento.Value),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = dataPagamento;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.Despesas.Pagamento;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorAcumulado;
                    }
                    #endregion
                }
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de Provisao, Pagamento para despesas de taxa de performance.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="dataProxima"></param>
        private void GeraEventosTaxaPerformance(Cliente cliente, DateTime data, DateTime dataProxima, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            CalculoPerformanceCollection calculoPerformanceCollection = new CalculoPerformanceCollection();

            if (historico)
            {
                CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();
                calculoPerformanceHistoricoCollection.Query.Select(calculoPerformanceHistoricoCollection.Query.IdTabela,
                                                       calculoPerformanceHistoricoCollection.Query.ValorDia,
                                                       calculoPerformanceHistoricoCollection.Query.ValorAcumulado,
                                                       calculoPerformanceHistoricoCollection.Query.DataFimApropriacao,
                                                       calculoPerformanceHistoricoCollection.Query.DataPagamento);
                calculoPerformanceHistoricoCollection.Query.Where(calculoPerformanceHistoricoCollection.Query.IdCarteira.Equal(idCliente),
                                                                  calculoPerformanceHistoricoCollection.Query.DataHistorico.Equal(data));
                calculoPerformanceHistoricoCollection.Query.Load();

                CalculoPerformanceCollection calculoPerformanceCollectionAux = new CalculoPerformanceCollection(calculoPerformanceHistoricoCollection);

                calculoPerformanceCollection.Combine(calculoPerformanceCollectionAux);
            }
            else
            {
                calculoPerformanceCollection.Query.Select(calculoPerformanceCollection.Query.IdTabela,
                                                       calculoPerformanceCollection.Query.ValorDia,
                                                       calculoPerformanceCollection.Query.ValorAcumulado,
                                                       calculoPerformanceCollection.Query.DataFimApropriacao,
                                                       calculoPerformanceCollection.Query.DataPagamento);
                calculoPerformanceCollection.Query.Where(calculoPerformanceCollection.Query.IdCarteira.Equal(idCliente));
                calculoPerformanceCollection.Query.Load();
            }

            foreach (CalculoPerformance calculoPerformance in calculoPerformanceCollection)
            {
                int idTabela = calculoPerformance.IdTabela.Value;
                decimal valorAcumulado = calculoPerformance.ValorAcumulado.Value;
                DateTime dataFimApropriacao = calculoPerformance.DataFimApropriacao.Value;
                DateTime dataPagamento = calculoPerformance.DataPagamento.Value;

                CalculoPerformanceHistorico calculoPerformanceHistorico = new CalculoPerformanceHistorico();
                calculoPerformanceHistorico.Query.Select(calculoPerformanceHistorico.Query.ValorAcumulado,
                                                         calculoPerformanceHistorico.Query.DataPagamento);
                calculoPerformanceHistorico.Query.Where(calculoPerformanceHistorico.Query.IdCarteira.Equal(idCliente),
                                                        calculoPerformanceHistorico.Query.DataHistorico.Equal(dataAnterior),
                                                        calculoPerformanceHistorico.Query.IdTabela.Equal(idTabela));
                calculoPerformanceHistorico.Query.Load();

                decimal valorDia = 0;
                if (calculoPerformanceHistorico.DataPagamento.HasValue && calculoPerformanceHistorico.DataPagamento.Value == dataAnterior)
                {
                    valorDia = valorAcumulado;
                }
                else
                {
                    if (calculoPerformanceHistorico.ValorAcumulado.HasValue)
                    {
                        valorDia = valorAcumulado - calculoPerformanceHistorico.ValorAcumulado.Value;
                    }
                    else
                    {
                        valorDia = valorAcumulado;
                    }
                }

                TabelaTaxaPerformance tabelaTaxaPerformance = new TabelaTaxaPerformance();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tabelaTaxaPerformance.Query.IdEventoPagamento);
                campos.Add(tabelaTaxaPerformance.Query.IdEventoProvisao);
                tabelaTaxaPerformance.LoadByPrimaryKey(campos, idTabela);

                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                if (valorDia != 0 && tabelaTaxaPerformance.IdEventoProvisao.HasValue)
                {
                    #region Evento de Provisao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(tabelaTaxaPerformance.IdEventoProvisao.Value),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;

                        if (valorDia > 0)
                        {
                            contaDebito = contabRoteiroCollection[0].ContaDebito;
                            contaCredito = contabRoteiroCollection[0].ContaCredito;
                            idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                            idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                        }
                        else
                        {
                            contaDebito = contabRoteiroCollection[0].ContaCredito;
                            contaCredito = contabRoteiroCollection[0].ContaDebito;
                            idContaDebito = contabRoteiroCollection[0].IdContaCredito.Value;
                            idContaCredito = contabRoteiroCollection[0].IdContaDebito.Value;
                        }

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.Despesas.Provisao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorDia;
                    }
                    #endregion
                }

                if (dataFimApropriacao == dataProxima && Math.Abs(valorAcumulado) > 0 && tabelaTaxaPerformance.IdEventoPagamento.HasValue)
                {
                    #region Evento de Pagamento
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(tabelaTaxaPerformance.IdEventoPagamento.Value),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = dataPagamento;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.Despesas.Pagamento;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorAcumulado;
                    }
                    #endregion
                }
            }

            contabLancamentoCollection.Save();
        }

    }
}
