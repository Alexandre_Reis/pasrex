﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Util;
using Financial.Contabil.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Fundo;
using Financial.Swap;
using Financial.InvestidorCotista;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.CRM.Enums;

namespace Financial.Contabil
{
    public class Balancete
    {
        int idConta;
        public int IdConta
        {
            get { return idConta; }
            set { idConta = value; }
        }

        string codigoConta;
        public string CodigoConta
        {
            get { return codigoConta; }
            set { codigoConta = value; }
        }

        string codigoReduzida;
        public string CodigoReduzida
        {
            get { return codigoReduzida; }
            set { codigoReduzida = value; }
        }

        string descricao;
        public string Descricao
        {
            get { return descricao; }
            set { descricao = value; }
        }

        decimal saldoAnterior;
        public decimal SaldoAnterior
        {
            get { return saldoAnterior; }
            set { saldoAnterior = value; }
        }

        decimal debitoMes;
        public decimal DebitoMes
        {
            get { return debitoMes; }
            set { debitoMes = value; }
        }

        decimal creditoMes;
        public decimal CreditoMes
        {
            get { return creditoMes; }
            set { creditoMes = value; }
        }

        decimal net;
        public decimal Net
        {
            get { return net; }
            set { net = value; }
        }

        decimal saldoFinal;
        public decimal SaldoFinal
        {
            get { return saldoFinal; }
            set { saldoFinal = value; }
        }

        bool contaDetalhe;
        public bool ContaDetalhe
        {
            get { return contaDetalhe; }
            set { contaDetalhe = value; }
        }

        string tipoConta;
        public string TipoConta
        {
            get { return tipoConta; }
            set { tipoConta = value; }
        }
    }

    public partial class ContabSaldo : esContabSaldo
    {
        /// <summary>
        /// Calcula o saldo contábil das contas referentes à data passada.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        public void CalculaSaldo(Cliente cliente, DateTime data)
        {
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            ContabPlano contabPlano = new ContabPlano();
            contabPlano.LoadByPrimaryKey(cliente.IdPlano.Value);

            int idCliente = cliente.IdCliente.Value;
            int idPlano = contabPlano.IdPlano.Value;


            ContabContaQuery contabContaQuery = new ContabContaQuery("C");
            ContabSaldoQuery contabSaldoQuery = new ContabSaldoQuery("S");
            contabContaQuery.Select(contabContaQuery.IdConta,
                                    contabContaQuery.TipoConta);
            contabContaQuery.InnerJoin(contabSaldoQuery).On(contabSaldoQuery.IdConta == contabContaQuery.IdConta);
            contabContaQuery.Where(contabContaQuery.IdPlano.Equal(idPlano),
                                   contabSaldoQuery.IdCliente.Equal(idCliente));
            contabContaQuery.es.Distinct = true;

            ContabContaCollection contabContaCollection1 = new ContabContaCollection();
            contabContaCollection1.Load(contabContaQuery);

            List<int> listaIdConta = new List<int>();
            foreach (ContabConta contabConta in contabContaCollection1)
            {
                listaIdConta.Add(contabConta.IdConta.Value);
            }

            contabContaQuery = new ContabContaQuery("C");
            ContabLancamentoQuery contabLancamentoQuery = new ContabLancamentoQuery("L");
            contabContaQuery.Select(contabContaQuery.IdConta,
                                    contabContaQuery.TipoConta);
            contabContaQuery.InnerJoin(contabLancamentoQuery).On(contabLancamentoQuery.IdContaCredito == contabContaQuery.IdConta);
            contabContaQuery.Where(contabContaQuery.IdPlano.Equal(idPlano),
                                   contabLancamentoQuery.IdCliente.Equal(idCliente));
            if (listaIdConta.Count > 0)
            {
                contabContaQuery.Where(contabContaQuery.IdConta.NotIn(listaIdConta));
            }

            contabContaQuery.es.Distinct = true;

            ContabContaCollection contabContaCollection2 = new ContabContaCollection();
            contabContaCollection2.Load(contabContaQuery);

            foreach (ContabConta contabConta in contabContaCollection2)
            {
                listaIdConta.Add(contabConta.IdConta.Value);
            }

            contabContaQuery = new ContabContaQuery("C");
            contabLancamentoQuery = new ContabLancamentoQuery("L");
            contabContaQuery.Select(contabContaQuery.IdConta,
                                    contabContaQuery.TipoConta);
            contabContaQuery.InnerJoin(contabLancamentoQuery).On(contabLancamentoQuery.IdContaDebito == contabContaQuery.IdConta);
            contabContaQuery.Where(contabContaQuery.IdPlano.Equal(idPlano),
                                   contabLancamentoQuery.IdCliente.Equal(idCliente));
            if (listaIdConta.Count > 0)
            {
                contabContaQuery.Where(contabContaQuery.IdConta.NotIn(listaIdConta));
            }
            contabContaQuery.es.Distinct = true;

            ContabContaCollection contabContaCollection3 = new ContabContaCollection();
            contabContaCollection3.Load(contabContaQuery);

            foreach (ContabConta contabConta in contabContaCollection3)
            {
                listaIdConta.Add(contabConta.IdConta.Value);
            }

            ContabContaCollection contabContaCollection = new ContabContaCollection();
            contabContaCollection.Combine(contabContaCollection1);
            contabContaCollection.Combine(contabContaCollection2);
            contabContaCollection.Combine(contabContaCollection3);


            ContabSaldoCollection contabSaldoCollection = new ContabSaldoCollection();
            contabSaldoCollection.Query.Where(contabSaldoCollection.Query.IdCliente.Equal(idCliente),
                                              contabSaldoCollection.Query.Data.GreaterThanOrEqual(data));
            contabSaldoCollection.Query.Load();
            contabSaldoCollection.MarkAllAsDeleted();
            contabSaldoCollection.Save();


            foreach (ContabConta contabConta in contabContaCollection)
            {
                int idConta = contabConta.IdConta.Value;
                string tipoConta = contabConta.TipoConta;

                decimal saldoAnterior = 0;
                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, idConta, dataAnterior))
                {
                    saldoAnterior = contabSaldo.SaldoFinal.Value;
                }

                ContabLancamento contabLancamento = new ContabLancamento();
                contabLancamento.Query.Select(contabLancamento.Query.Valor.Sum());
                contabLancamento.Query.Where(contabLancamento.Query.IdCliente.Equal(idCliente),
                                             contabLancamento.Query.DataLancamento.Equal(data),
                                             contabLancamento.Query.IdContaCredito.Equal(idConta));
                contabLancamento.Query.Load();

                decimal valorCredito = 0;
                if (contabLancamento.Valor.HasValue)
                {
                    valorCredito = contabLancamento.Valor.Value;
                }

                contabLancamento = new ContabLancamento();
                contabLancamento.Query.Select(contabLancamento.Query.Valor.Sum());
                contabLancamento.Query.Where(contabLancamento.Query.IdCliente.Equal(idCliente),
                                             contabLancamento.Query.DataLancamento.Equal(data),
                                             contabLancamento.Query.IdContaDebito.Equal(idConta));
                contabLancamento.Query.Load();

                decimal valorDebito = 0;
                if (contabLancamento.Valor.HasValue)
                {
                    valorDebito = contabLancamento.Valor.Value;
                }

                decimal saldoFinal = 0;
                if (tipoConta == TipoContaContabil.Ativo)
                {
                    saldoFinal = saldoAnterior + valorDebito - valorCredito;
                }
                else
                {
                    saldoFinal = saldoAnterior - valorDebito + valorCredito;
                }

                ContabSaldo contabSaldoNovo = new ContabSaldo();
                contabSaldoNovo.Data = data;
                contabSaldoNovo.IdCliente = idCliente;
                contabSaldoNovo.IdConta = idConta;
                contabSaldoNovo.IdPlano = idPlano;
                contabSaldoNovo.SaldoAnterior = saldoAnterior;
                contabSaldoNovo.SaldoFinal = saldoFinal;
                contabSaldoNovo.TotalCredito = valorCredito;
                contabSaldoNovo.TotalDebito = valorDebito;
                contabSaldoNovo.Save();

            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="mes"></param>
        /// <param name="ano"></param>
        /// <returns></returns>
        public List<Balancete> RetornaBalancete(int idCliente, int mes, int ano)
        {
            List<Balancete> listaBalancete = new List<Balancete>();

            DateTime primeiroDiaMes = new DateTime(ano, mes, 1);
            DateTime ultimoDiaMesAnterior = Calendario.SubtraiDiaUtil(primeiroDiaMes, 1);
            DateTime ultimoDiaMes = Calendario.RetornaUltimoDiaUtilMes(primeiroDiaMes, 0);

            ContabContaCollection contabContaCollection = new ContabContaCollection();
            contabContaCollection.LoadAll();

            foreach (ContabConta contabConta in contabContaCollection)
            {
                //int idConta = contabConta.
            }

            return listaBalancete;
        }

        /// <summary>
        /// Implanta saldos contabeis de forma automatica a partir das posicoes do sistema.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idPlano"></param>
        public void ImplantaSaldo(int idCliente, DateTime data)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (!cliente.IdPlano.HasValue)
            {
                return;
            }

            int idPlano = cliente.IdPlano.Value;

            ContabSaldoCollection contabSaldoCollection = new ContabSaldoCollection();
            contabSaldoCollection.Query.Where(contabSaldoCollection.Query.IdCliente.Equal(idCliente));
            contabSaldoCollection.Query.Load();
            contabSaldoCollection.MarkAllAsDeleted();
            contabSaldoCollection.Save();

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
            contabLancamentoCollection.Query.Select(contabLancamentoCollection.Query.IdLancamento);
            contabLancamentoCollection.Query.Where(contabLancamentoCollection.Query.IdCliente.Equal(idCliente),
                                                   contabLancamentoCollection.Query.DataRegistro.GreaterThanOrEqual(data),
                                                   contabLancamentoCollection.Query.Fonte.Equal((byte)FonteLancamentoContabil.Interno));
            contabLancamentoCollection.Query.Load();
            contabLancamentoCollection.MarkAllAsDeleted();
            contabLancamentoCollection.Save();


            #region Acoes
            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
            posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                              posicaoBolsaHistorico.Query.DataHistorico.Equal(data),
                                              posicaoBolsaHistorico.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
            posicaoBolsaHistorico.Query.Load();

            if (posicaoBolsaHistorico.ValorMercado.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoAcao),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = posicaoBolsaHistorico.ValorMercado.Value;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }
            #endregion

            #region OPC Comprada e Vendida
            posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
            posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                              posicaoBolsaHistorico.Query.DataHistorico.Equal(data),
                                              posicaoBolsaHistorico.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                                              posicaoBolsaHistorico.Query.Quantidade.GreaterThan(0));
            posicaoBolsaHistorico.Query.Load();

            if (posicaoBolsaHistorico.ValorMercado.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoOPCComprada),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = Math.Abs(posicaoBolsaHistorico.ValorMercado.Value);
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }

            posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
            posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                              posicaoBolsaHistorico.Query.DataHistorico.Equal(data),
                                              posicaoBolsaHistorico.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                                              posicaoBolsaHistorico.Query.Quantidade.LessThan(0));
            posicaoBolsaHistorico.Query.Load();

            if (posicaoBolsaHistorico.ValorMercado.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoOPCVendida),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = Math.Abs(posicaoBolsaHistorico.ValorMercado.Value);
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }
            #endregion

            #region OPV Comprada e Vendida
            posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
            posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                              posicaoBolsaHistorico.Query.DataHistorico.Equal(data),
                                              posicaoBolsaHistorico.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda),
                                              posicaoBolsaHistorico.Query.Quantidade.GreaterThan(0));
            posicaoBolsaHistorico.Query.Load();

            if (posicaoBolsaHistorico.ValorMercado.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoOPVComprada),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = Math.Abs(posicaoBolsaHistorico.ValorMercado.Value);
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }

            posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.ValorMercado.Sum());
            posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                              posicaoBolsaHistorico.Query.DataHistorico.Equal(data),
                                              posicaoBolsaHistorico.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda),
                                              posicaoBolsaHistorico.Query.Quantidade.LessThan(0));
            posicaoBolsaHistorico.Query.Load();

            if (posicaoBolsaHistorico.ValorMercado.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoOPVVendida),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = Math.Abs(posicaoBolsaHistorico.ValorMercado.Value);
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }
            #endregion

            #region Termo Comprado e Vendido
            PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
            posicaoTermoBolsaHistorico.Query.Select(posicaoTermoBolsaHistorico.Query.ValorMercado.Sum());
            posicaoTermoBolsaHistorico.Query.Where(posicaoTermoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                                   posicaoTermoBolsaHistorico.Query.DataHistorico.Equal(data),
                                                   posicaoTermoBolsaHistorico.Query.Quantidade.GreaterThan(0));
            posicaoTermoBolsaHistorico.Query.Load();

            if (posicaoTermoBolsaHistorico.ValorMercado.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoTermoComprado),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = posicaoTermoBolsaHistorico.ValorMercado.Value;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }

            posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
            posicaoTermoBolsaHistorico.Query.Select(posicaoTermoBolsaHistorico.Query.ValorMercado.Sum());
            posicaoTermoBolsaHistorico.Query.Where(posicaoTermoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                                   posicaoTermoBolsaHistorico.Query.DataHistorico.Equal(data),
                                                   posicaoTermoBolsaHistorico.Query.Quantidade.LessThan(0));
            posicaoTermoBolsaHistorico.Query.Load();

            if (posicaoTermoBolsaHistorico.ValorMercado.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoTermoVendido),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = Math.Abs(posicaoTermoBolsaHistorico.ValorMercado.Value);
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }
            #endregion

            #region BTC Tomado e Doado
            PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
            posicaoEmprestimoBolsaHistorico.Query.Select(posicaoEmprestimoBolsaHistorico.Query.ValorMercado.Sum(),
                                                         posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoJuros.Sum(),
                                                         posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoComissao.Sum(),
                                                         posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoCBLC.Sum(),
                                                         posicaoEmprestimoBolsaHistorico.Query.ValorBase.Sum());
            posicaoEmprestimoBolsaHistorico.Query.Where(posicaoEmprestimoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                                        posicaoEmprestimoBolsaHistorico.Query.DataHistorico.Equal(data),
                                                        posicaoEmprestimoBolsaHistorico.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Tomador));
            posicaoEmprestimoBolsaHistorico.Query.Load();

            if (posicaoEmprestimoBolsaHistorico.ValorMercado.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.BTC_Tomado),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    decimal valorMercado = posicaoEmprestimoBolsaHistorico.ValorMercado.Value;
                    decimal valorJuros = posicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros.Value - posicaoEmprestimoBolsaHistorico.ValorBase.Value;
                    decimal valorComissao = posicaoEmprestimoBolsaHistorico.ValorCorrigidoComissao.Value - posicaoEmprestimoBolsaHistorico.ValorBase.Value;
                    decimal valorCBLC = posicaoEmprestimoBolsaHistorico.ValorCorrigidoCBLC.Value - posicaoEmprestimoBolsaHistorico.ValorBase.Value;

                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = valorMercado + valorJuros + valorComissao + valorCBLC;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }

            posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
            posicaoEmprestimoBolsaHistorico.Query.Select(posicaoEmprestimoBolsaHistorico.Query.ValorMercado.Sum(),
                                                         posicaoEmprestimoBolsaHistorico.Query.ValorCorrigidoJuros.Sum(),
                                                         posicaoEmprestimoBolsaHistorico.Query.ValorBase.Sum());
            posicaoEmprestimoBolsaHistorico.Query.Where(posicaoEmprestimoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                                        posicaoEmprestimoBolsaHistorico.Query.DataHistorico.Equal(data),
                                                        posicaoEmprestimoBolsaHistorico.Query.PontaEmprestimo.Equal((byte)PontaEmprestimoBolsa.Doador));
            posicaoEmprestimoBolsaHistorico.Query.Load();

            if (posicaoEmprestimoBolsaHistorico.ValorMercado.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.BTC_Doado),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    decimal valorMercado = posicaoEmprestimoBolsaHistorico.ValorMercado.Value;
                    decimal valorJuros = posicaoEmprestimoBolsaHistorico.ValorCorrigidoJuros.Value - posicaoEmprestimoBolsaHistorico.ValorBase.Value;

                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = valorMercado + valorJuros;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }
            #endregion

            #region Proventos Bolsa em Liquidacao
            LiquidacaoHistorico liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCliente),
                                            liquidacaoHistorico.Query.DataVencimento.GreaterThan(data),
                                            liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(data),
                                            liquidacaoHistorico.Query.DataHistorico.Equal(data),
                                            liquidacaoHistorico.Query.Valor.GreaterThan(0),
                                            liquidacaoHistorico.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.Dividendo));
            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ProvisaoDividendosReceber),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                    {
                        contabSaldo.SaldoFinal += liquidacaoHistorico.Valor.Value;
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = liquidacaoHistorico.Valor.Value;
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
            }


            liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCliente),
                                            liquidacaoHistorico.Query.DataVencimento.GreaterThan(data),
                                            liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(data),
                                            liquidacaoHistorico.Query.DataHistorico.Equal(data),
                                            liquidacaoHistorico.Query.Valor.GreaterThan(0),
                                            liquidacaoHistorico.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.Rendimento));
            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ProvisaoRendimentosReceber),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                    {
                        contabSaldo.SaldoFinal += liquidacaoHistorico.Valor.Value;
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = liquidacaoHistorico.Valor.Value;
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
            }


            liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCliente),
                                            liquidacaoHistorico.Query.DataVencimento.GreaterThan(data),
                                            liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(data),
                                            liquidacaoHistorico.Query.DataHistorico.Equal(data),
                                            liquidacaoHistorico.Query.Valor.GreaterThan(0),
                                            liquidacaoHistorico.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes,
                                                                      (int)OrigemLancamentoLiquidacao.Bolsa.JurosCapital,
                                                                      (int)OrigemLancamentoLiquidacao.Bolsa.OutrosProventos,
                                                                      (int)OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital));
            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ProvisaoJurosCapitalReceber),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                    {
                        contabSaldo.SaldoFinal += liquidacaoHistorico.Valor.Value;
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = liquidacaoHistorico.Valor.Value;
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
            }

            liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCliente),
                                            liquidacaoHistorico.Query.DataVencimento.GreaterThan(data),
                                            liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(data),
                                            liquidacaoHistorico.Query.DataHistorico.Equal(data),
                                            liquidacaoHistorico.Query.Valor.LessThan(0),
                                            liquidacaoHistorico.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.Dividendo));
            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ProvisaoDividendosPagar),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                    {
                        contabSaldo.SaldoFinal += Math.Abs(liquidacaoHistorico.Valor.Value);
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = Math.Abs(liquidacaoHistorico.Valor.Value);
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
            }

            liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCliente),
                                            liquidacaoHistorico.Query.DataVencimento.GreaterThan(data),
                                            liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(data),
                                            liquidacaoHistorico.Query.DataHistorico.Equal(data),
                                            liquidacaoHistorico.Query.Valor.LessThan(0),
                                            liquidacaoHistorico.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.Rendimento));
            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ProvisaoRendimentosPagar),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                    {
                        contabSaldo.SaldoFinal += Math.Abs(liquidacaoHistorico.Valor.Value);
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = Math.Abs(liquidacaoHistorico.Valor.Value);
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
            }

            liquidacaoHistorico = new LiquidacaoHistorico();
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Select(liquidacaoHistorico.Query.Valor.Sum());
            liquidacaoHistorico.Query.Where(liquidacaoHistorico.Query.IdCliente.Equal(idCliente),
                                            liquidacaoHistorico.Query.DataVencimento.GreaterThan(data),
                                            liquidacaoHistorico.Query.DataLancamento.LessThanOrEqual(data),
                                            liquidacaoHistorico.Query.DataHistorico.Equal(data),
                                            liquidacaoHistorico.Query.Valor.LessThan(0),
                                            liquidacaoHistorico.Query.Origem.In((int)OrigemLancamentoLiquidacao.Bolsa.CreditoFracoesAcoes,
                                                                      (int)OrigemLancamentoLiquidacao.Bolsa.IRProventos,
                                                                      (int)OrigemLancamentoLiquidacao.Bolsa.JurosCapital,
                                                                      (int)OrigemLancamentoLiquidacao.Bolsa.OutrosProventos,
                                                                      (int)OrigemLancamentoLiquidacao.Bolsa.RestituicaoCapital));
            liquidacaoHistorico.Query.Load();

            if (liquidacaoHistorico.Valor.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ProvisaoJurosCapitalPagar),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                    {
                        contabSaldo.SaldoFinal += Math.Abs(liquidacaoHistorico.Valor.Value);
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = Math.Abs(liquidacaoHistorico.Valor.Value);
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
            }
            #endregion

            #region Renda Fixa (Final e Compromissada)
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("R");

            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.ValorMercado.Sum(),
                                                  papelRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                                 posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(data),
                                                 posicaoRendaFixaHistoricoQuery.ValorMercado.NotEqual(0),
                                                 posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));
            posicaoRendaFixaHistoricoQuery.GroupBy(papelRendaFixaQuery.IdPapel);

            PosicaoRendaFixaHistoricoCollection coll = new PosicaoRendaFixaHistoricoCollection();
            coll.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in coll)
            {
                int idPapel = Convert.ToInt32(posicaoRendaFixaHistorico.GetColumn(PapelRendaFixaMetadata.ColumnNames.IdPapel));

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.RendaFixa.CompraFinal),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(idPapel));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = posicaoRendaFixaHistorico.ValorMercado.Value;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }

            papelRendaFixaQuery = new PapelRendaFixaQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("R");

            posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.ValorMercado.Sum(),
                                                  papelRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                                 posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(data),
                                                 posicaoRendaFixaHistoricoQuery.Quantidade.NotEqual(0),
                                                 posicaoRendaFixaHistoricoQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda));
            posicaoRendaFixaHistoricoQuery.GroupBy(papelRendaFixaQuery.IdPapel);

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
            {
                int idPapel = Convert.ToInt32(posicaoRendaFixaHistorico.GetColumn(PapelRendaFixaMetadata.ColumnNames.IdPapel));

                if (posicaoRendaFixaHistorico.ValorMercado.HasValue)
                {
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.RendaFixa.CompraCompromissada),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(idPapel));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabSaldo contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = posicaoRendaFixaHistorico.ValorMercado.Value;
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;

                        contabSaldo.Save();
                    }
                }
            }
            #endregion

            #region Cotas de Fundos


            decimal totalValorIR = 0;
            decimal totalValorIOF = 0;

            string categoriaContabil = "";

            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
            ClienteInterfaceQuery clienteInterfaceQuery = new ClienteInterfaceQuery("C");

            posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.ValorBruto.Sum(),
                                              posicaoFundoHistoricoQuery.ValorIR.Sum(),
                                              posicaoFundoHistoricoQuery.ValorIOF.Sum(),
                                              clienteInterfaceQuery.CategoriaContabil);
            posicaoFundoHistoricoQuery.InnerJoin(clienteInterfaceQuery).On(clienteInterfaceQuery.IdCliente == posicaoFundoHistoricoQuery.IdCarteira);
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente),
                                             posicaoFundoHistoricoQuery.DataHistorico.Equal(data),
                                             posicaoFundoHistoricoQuery.ValorBruto.NotEqual(0));
            posicaoFundoHistoricoQuery.GroupBy(clienteInterfaceQuery.CategoriaContabil);

            PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

            foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
            {
                categoriaContabil = Convert.ToString(posicaoFundoHistorico.GetColumn(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil));

                totalValorIR = posicaoFundoHistorico.ValorIR.Value;
                totalValorIOF = posicaoFundoHistorico.ValorIOF.Value;


                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.CotaInvestimento.Aplicacao),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(categoriaContabil));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = posicaoFundoHistorico.ValorBruto.Value;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }

                if (totalValorIR > 0 )
                {
                    ContabRoteiroCollection contabRoteiroCollectionIr = new ContabRoteiroCollection();
                    contabRoteiroCollectionIr.Query.Select(contabRoteiroCollectionIr.Query.IdContaDebito,
                                                           contabRoteiroCollectionIr.Query.IdContaCredito);
                    contabRoteiroCollectionIr.Query.Where(contabRoteiroCollectionIr.Query.Origem.Equal((int)ContabilOrigem.CotaInvestimento.ProvisaoIR),
                                                        contabRoteiroCollectionIr.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollectionIr.Query.Identificador.Equal(categoriaContabil));
                    contabRoteiroCollectionIr.Query.Load();

                    if (contabRoteiroCollectionIr.Count > 0)
                    {
                        int idContaDebito = contabRoteiroCollectionIr[0].IdContaDebito.Value;
                        ContabConta contabConta = new ContabConta();
                        contabConta.LoadByPrimaryKey(idContaDebito);                        
                        
                        ContabSaldo contabSaldo = new ContabSaldo();
                        if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollectionIr[0].IdContaDebito.Value, data))
                        {
                            contabSaldo.SaldoFinal += contabConta.TipoConta == "P" ? totalValorIR * -1 : totalValorIR;
                        }
                        else
                        {
                            contabSaldo = new ContabSaldo();
                            contabSaldo.Data = data;
                            contabSaldo.IdCliente = idCliente;
                            contabSaldo.IdConta = contabRoteiroCollectionIr[0].IdContaDebito.Value;
                            contabSaldo.IdPlano = idPlano;
                            contabSaldo.SaldoAnterior = 0;
                            contabSaldo.SaldoFinal = contabConta.TipoConta == "P" ? totalValorIR * -1 : totalValorIR;
                            contabSaldo.TotalCredito = 0;
                            contabSaldo.TotalDebito = 0;
                        }

                        contabSaldo.Save();

                        contabSaldo = new ContabSaldo();
                        if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollectionIr[0].IdContaCredito.Value, data))
                        {
                            contabSaldo.SaldoFinal += totalValorIR;
                        }
                        else
                        {
                            contabSaldo = new ContabSaldo();
                            contabSaldo.Data = data;
                            contabSaldo.IdCliente = idCliente;
                            contabSaldo.IdConta = contabRoteiroCollectionIr[0].IdContaCredito.Value;
                            contabSaldo.IdPlano = idPlano;
                            contabSaldo.SaldoAnterior = 0;
                            contabSaldo.SaldoFinal = totalValorIR;
                            contabSaldo.TotalCredito = 0;
                            contabSaldo.TotalDebito = 0;
                        }

                        contabSaldo.Save();
                    }
                }

                if (totalValorIOF > 0 )
                {
                    ContabRoteiroCollection contabRoteiroCollectionIof = new ContabRoteiroCollection();
                    contabRoteiroCollectionIof.Query.Select(contabRoteiroCollectionIof.Query.IdContaDebito,
                                                            contabRoteiroCollectionIof.Query.IdContaCredito);
                    contabRoteiroCollectionIof.Query.Where(contabRoteiroCollectionIof.Query.Origem.Equal((int)ContabilOrigem.CotaInvestimento.ProvisaoIOF),
                                                        contabRoteiroCollectionIof.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollectionIof.Query.Identificador.Equal(categoriaContabil));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollectionIof.Count > 0)
                    {
                        int idContaDebito = contabRoteiroCollectionIof[0].IdContaDebito.Value;
                        ContabConta contabConta = new ContabConta();
                        contabConta.LoadByPrimaryKey(idContaDebito);      

                        ContabSaldo contabSaldo = new ContabSaldo();
                        if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollectionIof[0].IdContaDebito.Value, data))
                        {
                            contabSaldo.SaldoFinal += contabConta.TipoConta == "P" ? totalValorIOF * -1 : totalValorIOF;
                        }
                        else
                        {
                            contabSaldo = new ContabSaldo();
                            contabSaldo.Data = data;
                            contabSaldo.IdCliente = idCliente;
                            contabSaldo.IdConta = contabRoteiroCollectionIof[0].IdContaDebito.Value;
                            contabSaldo.IdPlano = idPlano;
                            contabSaldo.SaldoAnterior = 0;
                            contabSaldo.SaldoFinal = contabConta.TipoConta == "P" ? totalValorIOF * -1 : totalValorIOF;
                            contabSaldo.TotalCredito = 0;
                            contabSaldo.TotalDebito = 0;
                        }

                        contabSaldo.Save();

                        contabSaldo = new ContabSaldo();
                        if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollectionIof[0].IdContaCredito.Value, data))
                        {
                            contabSaldo.SaldoFinal += totalValorIOF;
                        }
                        else
                        {

                            contabSaldo = new ContabSaldo();
                            contabSaldo.Data = data;
                            contabSaldo.IdCliente = idCliente;
                            contabSaldo.IdConta = contabRoteiroCollectionIof[0].IdContaCredito.Value;
                            contabSaldo.IdPlano = idPlano;
                            contabSaldo.SaldoAnterior = 0;
                            contabSaldo.SaldoFinal = totalValorIOF;
                            contabSaldo.TotalCredito = 0;
                            contabSaldo.TotalDebito = 0;
                        }

                        contabSaldo.Save();
                    }

                }
            } 

            #endregion

            #region SaldoCaixa
            SaldoCaixa saldoCaixa = new SaldoCaixa();
            saldoCaixa.BuscaSaldoCaixa(idCliente, data);

            if (saldoCaixa.SaldoFechamento.HasValue)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.LiquidacaoBovespaReceber),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = saldoCaixa.SaldoFechamento.Value;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;

                    contabSaldo.Save();
                }
            }
            #endregion

            #region Swap
            PosicaoSwapHistorico posicaoSwapHistorico = new PosicaoSwapHistorico();
            posicaoSwapHistorico.Query.Select(posicaoSwapHistorico.Query.Saldo.Sum());
            posicaoSwapHistorico.Query.Where(posicaoSwapHistorico.Query.IdCliente.Equal(idCliente),
                                             posicaoSwapHistorico.Query.DataHistorico.Equal(data));
            posicaoSwapHistorico.Query.Load();

            if (posicaoSwapHistorico.Saldo.HasValue)
            {
                if (posicaoSwapHistorico.Saldo.Value > 0)
                {
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito);
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Swap.LiquidacaoReceber),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabSaldo contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = posicaoSwapHistorico.Saldo.Value;
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;

                        contabSaldo.Save();
                    }
                }
                else if (posicaoSwapHistorico.Saldo.Value < 0)
                {
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito);
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Swap.LiquidacaoPagar),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabSaldo contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = Math.Abs(posicaoSwapHistorico.Saldo.Value);
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;

                        contabSaldo.Save();
                    }
                }



            }
            #endregion

            #region Taxas de Adm e Gestao
            CalculoAdministracaoHistoricoCollection calculoAdministracaoHistoricoCollection = new CalculoAdministracaoHistoricoCollection();

            TabelaTaxaAdministracaoQuery tabelaTaxaAdministracaoQuery = new TabelaTaxaAdministracaoQuery("T");
            CalculoAdministracaoHistoricoQuery calculoAdministracaoHistoricoQuery = new CalculoAdministracaoHistoricoQuery("C");

            calculoAdministracaoHistoricoQuery.Select(calculoAdministracaoHistoricoQuery.ValorAcumulado.Sum(),
                                                      tabelaTaxaAdministracaoQuery.IdEventoProvisao);
            calculoAdministracaoHistoricoQuery.InnerJoin(tabelaTaxaAdministracaoQuery).On(tabelaTaxaAdministracaoQuery.IdTabela == calculoAdministracaoHistoricoQuery.IdTabela);
            calculoAdministracaoHistoricoQuery.Where(calculoAdministracaoHistoricoQuery.IdCarteira.Equal(idCliente),
                                                     calculoAdministracaoHistoricoQuery.DataHistorico.Equal(data),
                                                     tabelaTaxaAdministracaoQuery.IdEventoProvisao.IsNotNull());
            calculoAdministracaoHistoricoQuery.GroupBy(tabelaTaxaAdministracaoQuery.IdEventoProvisao);

            calculoAdministracaoHistoricoCollection.Load(calculoAdministracaoHistoricoQuery);

            foreach (CalculoAdministracaoHistorico calculoAdministracaoHistorico in calculoAdministracaoHistoricoCollection)
            {
                int idEventoProvisao = Convert.ToInt32(calculoAdministracaoHistorico.GetColumn(TabelaTaxaAdministracaoMetadata.ColumnNames.IdEventoProvisao));

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(idEventoProvisao));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                    {
                        contabSaldo.SaldoFinal += Math.Abs(calculoAdministracaoHistorico.ValorAcumulado.Value);
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = Math.Abs(calculoAdministracaoHistorico.ValorAcumulado.Value);
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
            }
            #endregion

            #region Taxa de Performance
            CalculoPerformanceHistoricoCollection calculoPerformanceHistoricoCollection = new CalculoPerformanceHistoricoCollection();

            TabelaTaxaPerformanceQuery tabelaTaxaPerformanceQuery = new TabelaTaxaPerformanceQuery("T");
            CalculoPerformanceHistoricoQuery calculoPerformanceHistoricoQuery = new CalculoPerformanceHistoricoQuery("C");

            calculoPerformanceHistoricoQuery.Select(calculoPerformanceHistoricoQuery.ValorAcumulado.Sum(),
                                                    tabelaTaxaPerformanceQuery.IdEventoProvisao);
            calculoPerformanceHistoricoQuery.InnerJoin(tabelaTaxaPerformanceQuery).On(tabelaTaxaPerformanceQuery.IdTabela == calculoPerformanceHistoricoQuery.IdTabela);
            calculoPerformanceHistoricoQuery.Where(calculoPerformanceHistoricoQuery.IdCarteira.Equal(idCliente),
                                                   calculoPerformanceHistoricoQuery.DataHistorico.Equal(data),
                                                   tabelaTaxaPerformanceQuery.IdEventoProvisao.IsNotNull());
            calculoPerformanceHistoricoQuery.GroupBy(tabelaTaxaPerformanceQuery.IdEventoProvisao);

            calculoPerformanceHistoricoCollection.Load(calculoPerformanceHistoricoQuery);

            foreach (CalculoPerformanceHistorico calculoPerformanceHistorico in calculoPerformanceHistoricoCollection)
            {
                int idEventoProvisao = Convert.ToInt32(calculoPerformanceHistorico.GetColumn(TabelaTaxaPerformanceMetadata.ColumnNames.IdEventoProvisao));

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(idEventoProvisao));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                    {
                        contabSaldo.SaldoFinal += Math.Abs(calculoPerformanceHistorico.ValorAcumulado.Value);
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = Math.Abs(calculoPerformanceHistorico.ValorAcumulado.Value);
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
            }
            #endregion

            #region Provisoes (Cetip, Selic etc)
            CalculoProvisaoHistoricoCollection calculoProvisaoHistoricoCollection = new CalculoProvisaoHistoricoCollection();

            TabelaProvisaoQuery tabelaProvisaoQuery = new TabelaProvisaoQuery("T");
            CalculoProvisaoHistoricoQuery calculoProvisaoHistoricoQuery = new CalculoProvisaoHistoricoQuery("C");

            calculoProvisaoHistoricoQuery.Select(calculoProvisaoHistoricoQuery.ValorAcumulado.Sum(),
                                                 tabelaProvisaoQuery.IdEventoProvisao);
            calculoProvisaoHistoricoQuery.InnerJoin(tabelaProvisaoQuery).On(tabelaProvisaoQuery.IdTabela == calculoProvisaoHistoricoQuery.IdTabela);
            calculoProvisaoHistoricoQuery.Where(calculoProvisaoHistoricoQuery.IdCarteira.Equal(idCliente),
                                                calculoProvisaoHistoricoQuery.DataHistorico.Equal(data),
                                                tabelaProvisaoQuery.IdEventoProvisao.IsNotNull());
            calculoProvisaoHistoricoQuery.GroupBy(tabelaProvisaoQuery.IdEventoProvisao);

            calculoProvisaoHistoricoCollection.Load(calculoProvisaoHistoricoQuery);

            foreach (CalculoProvisaoHistorico calculoProvisaoHistorico in calculoProvisaoHistoricoCollection)
            {
                int idEventoProvisao = Convert.ToInt32(calculoProvisaoHistorico.GetColumn(TabelaProvisaoMetadata.ColumnNames.IdEventoProvisao));

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(idEventoProvisao));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                    {
                        contabSaldo.SaldoFinal += Math.Abs(calculoProvisaoHistorico.ValorAcumulado.Value);
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = Math.Abs(calculoProvisaoHistorico.ValorAcumulado.Value);
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
            }
            #endregion

            bool contabilNet = cliente.ContabilLiquidacao.HasValue && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net;

            
            List<int> listaOrigemBolsa = new List<int>();
            List<int> listaOrigemBMF = new List<int>();
            listaOrigemBolsa.Add((int)OrigemLancamentoLiquidacao.Bolsa.CompraAcoes);
            listaOrigemBolsa.Add((int)OrigemLancamentoLiquidacao.Bolsa.CompraOpcoes);
            listaOrigemBolsa.Add((int)OrigemLancamentoLiquidacao.Bolsa.Corretagem);
            listaOrigemBolsa.Add((int)OrigemLancamentoLiquidacao.Bolsa.DespesasTaxas);
            listaOrigemBolsa.Add((int)OrigemLancamentoLiquidacao.Bolsa.ExercicioCompra);
            listaOrigemBolsa.Add((int)OrigemLancamentoLiquidacao.Bolsa.ExercicioVenda);
            listaOrigemBolsa.Add((int)OrigemLancamentoLiquidacao.Bolsa.Outros);
            listaOrigemBolsa.Add((int)OrigemLancamentoLiquidacao.Bolsa.VendaAcoes);
            listaOrigemBolsa.Add((int)OrigemLancamentoLiquidacao.Bolsa.VendaOpcoes);

            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.AjusteOperacao);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.AjustePosicao);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.CompraOpcoes);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.CompraVista);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.Corretagem);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.DespesasTaxas);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.Emolumento);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.ExercicioCompra);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.ExercicioVenda);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.Outros);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.OutrosCustos);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.Registro);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.TaxaPermanencia);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.VendaOpcoes);
            listaOrigemBMF.Add((int)OrigemLancamentoLiquidacao.BMF.VendaVista);


            #region Valores a pagar e receber de Bolsa
            decimal valorPagar = 0;
            decimal valorReceber = 0;
            LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
            liquidacaoHistoricoCollection.Query.Select(liquidacaoHistoricoCollection.Query.Valor);
            liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                      liquidacaoHistoricoCollection.Query.DataHistorico.Equal(data),
                                                      liquidacaoHistoricoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                                      liquidacaoHistoricoCollection.Query.DataVencimento.GreaterThan(data),
                                                      liquidacaoHistoricoCollection.Query.Origem.In(listaOrigemBolsa));
            liquidacaoHistoricoCollection.Query.Load();

            foreach (LiquidacaoHistorico liquidacaoHistoricoFluxo in liquidacaoHistoricoCollection)
            {
                if (liquidacaoHistoricoFluxo.Valor.Value > 0)
                {
                    valorReceber += liquidacaoHistoricoFluxo.Valor.Value;
                }
                else
                {
                    valorPagar += Math.Abs(liquidacaoHistoricoFluxo.Valor.Value);
                }
            }

            if (contabilNet)
            {
                if (valorReceber > valorPagar)
                {
                    valorReceber = valorReceber - valorPagar;
                    valorPagar = 0;
                }
                else
                {
                    valorPagar = valorPagar - valorReceber;
                    valorReceber = 0;
                }
            }

            if (valorReceber > 0)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito,
                                                     contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                {
                    contabSaldo.SaldoFinal += valorReceber;
                }
                else
                {
                    contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = valorReceber;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;
                }

                contabSaldo.Save();

                #region lança debito              

                contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                {
                    contabSaldo.SaldoFinal += valorReceber;
                }
                else
                {
                    contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = valorReceber;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;
                }

                contabSaldo.Save();
                #endregion

            }

            if (valorPagar > 0)
            {
                

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito,
                                                     contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                    {
                        contabSaldo.SaldoFinal += valorPagar;
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = valorPagar;
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }

                #region lanca credito
               
                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                    {
                        contabSaldo.SaldoFinal += valorPagar;
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = valorPagar;
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
                #endregion 
            }
            #endregion

            #region Valores a pagar e receber de BMF
            valorPagar = 0;
            valorReceber = 0;
            liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
            liquidacaoHistoricoCollection.Query.Select(liquidacaoHistoricoCollection.Query.Valor);
            liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                      liquidacaoHistoricoCollection.Query.DataHistorico.Equal(data),
                                                      liquidacaoHistoricoCollection.Query.DataLancamento.LessThanOrEqual(data),
                                                      liquidacaoHistoricoCollection.Query.DataVencimento.GreaterThan(data),
                                                      liquidacaoHistoricoCollection.Query.Origem.In(listaOrigemBMF));
            liquidacaoHistoricoCollection.Query.Load();

            foreach (LiquidacaoHistorico liquidacaoHistoricoFluxo in liquidacaoHistoricoCollection)
            {
                if (liquidacaoHistoricoFluxo.Valor.Value > 0)
                {
                    valorReceber += liquidacaoHistoricoFluxo.Valor.Value;
                }
                else
                {
                    valorPagar += Math.Abs(liquidacaoHistoricoFluxo.Valor.Value);
                }
            }

            if (contabilNet)
            {
                if (valorReceber > valorPagar)
                {
                    valorReceber = valorReceber - valorPagar;
                    valorPagar = 0;
                }
                else
                {
                    valorPagar = valorPagar - valorReceber;
                    valorReceber = 0;
                }
            }

            if (valorReceber > 0)
            {
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaCredito,
                                                     contabRoteiroCollection.Query.IdContaDebito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.BMF.AjustePositivo),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                {
                    contabSaldo.SaldoFinal += valorReceber;
                }
                else
                {
                    contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = valorReceber;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;
                }

                contabSaldo.Save();

                #region lança debito
                
                contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                {
                    contabSaldo.SaldoFinal += valorReceber;
                }
                else
                {
                    contabSaldo = new ContabSaldo();
                    contabSaldo.Data = data;
                    contabSaldo.IdCliente = idCliente;
                    contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                    contabSaldo.IdPlano = idPlano;
                    contabSaldo.SaldoAnterior = 0;
                    contabSaldo.SaldoFinal = valorReceber;
                    contabSaldo.TotalCredito = 0;
                    contabSaldo.TotalDebito = 0;
                }

                contabSaldo.Save();
                #endregion


            }

            if (valorPagar > 0)
            {
                valorPagar = valorPagar * -1;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito,
                                                     contabRoteiroCollection.Query.IdContaCredito);
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.BMF.AjusteNegativo),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                    {
                        contabSaldo.SaldoFinal += valorPagar;
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaDebito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = valorPagar;
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
                #region lança debito
                if (contabRoteiroCollection.Count > 0)
                {
                    ContabSaldo contabSaldo = new ContabSaldo();
                    if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                    {
                        contabSaldo.SaldoFinal += valorPagar;
                    }
                    else
                    {
                        contabSaldo = new ContabSaldo();
                        contabSaldo.Data = data;
                        contabSaldo.IdCliente = idCliente;
                        contabSaldo.IdConta = contabRoteiroCollection[0].IdContaCredito.Value;
                        contabSaldo.IdPlano = idPlano;
                        contabSaldo.SaldoAnterior = 0;
                        contabSaldo.SaldoFinal = valorPagar;
                        contabSaldo.TotalCredito = 0;
                        contabSaldo.TotalDebito = 0;
                    }

                    contabSaldo.Save();
                }
                #endregion

            }
            #endregion

            #region Cotas de Pessoa Fisica e Pessoa Juridica (Capital Social) e Lucros/Prej Acumulados
            PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            CotistaQuery cotistaQuery = new CotistaQuery("C");

            posicaoCotistaHistoricoQuery.Select(posicaoCotistaHistoricoQuery.Quantidade,
                                                posicaoCotistaHistoricoQuery.CotaAplicacao);
            posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == posicaoCotistaHistoricoQuery.IdCotista);
            posicaoCotistaHistoricoQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == cotistaQuery.IdPessoa);
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCliente),
                                               posicaoCotistaHistoricoQuery.DataHistorico.Equal(data),
                                               posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0),
                                               pessoaQuery.Tipo.Equal((byte)TipoPessoa.Fisica));

            PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();

            posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

            decimal valorPF = 0;
            foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
            {
                valorPF += Math.Round(posicaoCotistaHistorico.Quantidade.Value * posicaoCotistaHistorico.CotaAplicacao.Value, 2);
            }

            posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("P");
            pessoaQuery = new PessoaQuery("E");
            cotistaQuery = new CotistaQuery("C");
            posicaoCotistaHistoricoQuery.Select(posicaoCotistaHistoricoQuery.Quantidade,
                                                posicaoCotistaHistoricoQuery.CotaAplicacao);
            posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == posicaoCotistaHistoricoQuery.IdCotista);
            posicaoCotistaHistoricoQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == cotistaQuery.IdPessoa);
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(idCliente) &
                                               posicaoCotistaHistoricoQuery.DataHistorico.Equal(data) &
                                               posicaoCotistaHistoricoQuery.Quantidade.NotEqual(0) & 
                                               ( pessoaQuery.Tipo.Equal((byte)TipoPessoa.Juridica) |
                                                 pessoaQuery.OffShore.Equal("S")));

            posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();

            posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

            decimal valorPJ = 0;
            foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
            {
                valorPJ += Math.Round(posicaoCotistaHistorico.Quantidade.Value * posicaoCotistaHistorico.CotaAplicacao.Value, 2);
            }

            ContabRoteiroCollection contabRoteiroCollectionCotas = new ContabRoteiroCollection();
            contabRoteiroCollectionCotas.Query.Select(contabRoteiroCollectionCotas.Query.IdContaCredito);
            contabRoteiroCollectionCotas.Query.Where(contabRoteiroCollectionCotas.Query.Origem.Equal((int)ContabilOrigem.Cotista.AplicacaoPF),
                                                     contabRoteiroCollectionCotas.Query.IdPlano.Equal(idPlano));
            contabRoteiroCollectionCotas.Query.Load();

            if (valorPF > 0 && contabRoteiroCollectionCotas.Count > 0)
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                contabSaldo.Data = data;
                contabSaldo.IdCliente = idCliente;
                contabSaldo.IdConta = contabRoteiroCollectionCotas[0].IdContaCredito.Value;
                contabSaldo.IdPlano = idPlano;
                contabSaldo.SaldoAnterior = 0;
                contabSaldo.SaldoFinal = valorPF;
                contabSaldo.TotalCredito = 0;
                contabSaldo.TotalDebito = 0;

                contabSaldo.Save();
            }


            contabRoteiroCollectionCotas = new ContabRoteiroCollection();
            contabRoteiroCollectionCotas.Query.Select(contabRoteiroCollectionCotas.Query.IdContaCredito);
            contabRoteiroCollectionCotas.Query.Where(contabRoteiroCollectionCotas.Query.Origem.Equal((int)ContabilOrigem.Cotista.AplicacaoPJ),
                                                     contabRoteiroCollectionCotas.Query.IdPlano.Equal(idPlano));
            contabRoteiroCollectionCotas.Query.Load();

            if (valorPJ > 0 && contabRoteiroCollectionCotas.Count > 0 )
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                contabSaldo.Data = data;
                contabSaldo.IdCliente = idCliente;
                contabSaldo.IdConta = contabRoteiroCollectionCotas[0].IdContaCredito.Value;
                contabSaldo.IdPlano = idPlano;
                contabSaldo.SaldoAnterior = 0;
                contabSaldo.SaldoFinal = valorPJ;
                contabSaldo.TotalCredito = 0;
                contabSaldo.TotalDebito = 0;

                contabSaldo.Save();
            }

            decimal capitalSocial = valorPF + valorPJ;

            HistoricoCota h = new HistoricoCota();
            decimal valorPL = 0;
            try
            {
                h.BuscaValorPatrimonioDia(idCliente, data);
                valorPL = h.PLFechamento.HasValue ? h.PLFechamento.Value : 0;
            }
            catch (Exception)
            {
                throw new Exception("Não há PL definido para a carteira " + idCliente.ToString() + " na data.");
            }

            decimal resultadoAcumulado = valorPL - capitalSocial;

            ContabRoteiroCollection contabRoteiroCollectionZeragem = new ContabRoteiroCollection();

            int origem = 0;
            ContabContaCollection contabContaCollection = new ContabContaCollection();
            contabContaCollection.Query.Select(contabContaCollection.Query.IdConta);
            contabContaCollection.Query.Where(contabContaCollection.Query.FuncaoConta.Equal((int)FuncaoContaContabil.ResultadoAcumulado));
            contabContaCollection.Query.Load();

            int idContaResultadoAcumulado = 0;
            foreach (ContabConta contabConta in contabContaCollection)
            {
                ContabContaCollection contabContaMaeCollection = new ContabContaCollection();
                contabContaMaeCollection.Query.Select(contabContaMaeCollection.Query.IdConta);
                contabContaMaeCollection.Query.Where(contabContaMaeCollection.Query.IdContaMae.Equal(contabConta.IdConta.Value));
                contabContaMaeCollection.Query.Load();

                if (contabContaMaeCollection.Count == 0)
                {
                    idContaResultadoAcumulado = contabConta.IdConta.Value;
                    break;
                }
            }

            if (idContaResultadoAcumulado != 0)
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                contabSaldo.Data = data;
                contabSaldo.IdCliente = idCliente;
                contabSaldo.IdConta = idContaResultadoAcumulado;
                contabSaldo.IdPlano = idPlano;
                contabSaldo.SaldoAnterior = 0;
                contabSaldo.SaldoFinal = resultadoAcumulado;
                contabSaldo.TotalCredito = 0;
                contabSaldo.TotalDebito = 0;

                contabSaldo.Save();
            }
            #endregion



        }

        /// <summary>
        /// Zera os saldos de resultado para a conta de resultados acumulados.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        public void ZeraSaldoResultados(Cliente cliente, DateTime data)
        {
            int idPlano = cliente.IdPlano.Value;
            int idCliente = cliente.IdCliente.Value;
            
            DateTime ultimoDiaAno = Calendario.RetornaUltimoDiaUtilAno(data);

            if (ultimoDiaAno != data)
                return;

            ContabContaCollection contabContaCollection = new ContabContaCollection();
            contabContaCollection.Query.Select(contabContaCollection.Query.IdConta,
                                               contabContaCollection.Query.Codigo);
            contabContaCollection.Query.Where(contabContaCollection.Query.FuncaoConta.Equal((int)FuncaoContaContabil.ResultadoAcumulado));
            contabContaCollection.Query.Load();

            int idContaResultadoAcumulado = 0;
            string contaResultadoAcumulado = "";
            foreach (ContabConta contabConta in contabContaCollection)
            {
                ContabContaCollection contabContaMaeCollection = new ContabContaCollection();
                contabContaMaeCollection.Query.Select(contabContaMaeCollection.Query.IdConta);
                contabContaMaeCollection.Query.Where(contabContaMaeCollection.Query.IdContaMae.Equal(contabConta.IdConta.Value));
                contabContaMaeCollection.Query.Load();

                if (contabContaMaeCollection.Count == 0)
                {
                    idContaResultadoAcumulado = contabConta.IdConta.Value;
                    contaResultadoAcumulado = contabConta.Codigo;
                    break;
                }
            }

            ContabSaldoQuery contabSaldoQuery = new ContabSaldoQuery("C2");
            ContabContaQuery contabContaQuery = new ContabContaQuery("O2");
            contabSaldoQuery.Select(contabContaQuery, contabSaldoQuery);
            contabSaldoQuery.InnerJoin(contabContaQuery).On(contabContaQuery.IdConta == contabSaldoQuery.IdConta);
            contabSaldoQuery.Where(contabSaldoQuery.IdCliente.Equal(idCliente),
                                   contabSaldoQuery.Data.Equal(data),
                                   contabContaQuery.FuncaoConta.In((byte)FuncaoContaContabil.ResultadoCredito, (byte)FuncaoContaContabil.ResultadoDebito));
            ContabSaldoCollection contabSaldoCollection = new ContabSaldoCollection();
            contabSaldoCollection.Load(contabSaldoQuery);

            decimal resultadoDebito = 0;
            decimal resultadoCredito = 0;
            foreach (ContabSaldo contabSaldo in contabSaldoCollection)
            {
                byte funcaoConta = Convert.ToByte(contabSaldo.GetColumn(ContabContaMetadata.ColumnNames.FuncaoConta));
                string codigoConta = Convert.ToString(contabSaldo.GetColumn(ContabContaMetadata.ColumnNames.Codigo));
                int idConta = contabSaldo.IdConta.Value;
                decimal valorSaldo = contabSaldo.SaldoFinal.Value;

                int idContaDebito = 0;
                string contaDebito = "";
                int idContaCredito = 0;
                string contaCredito = "";
                int origemZeragem = 0;
                if (funcaoConta == (byte)FuncaoContaContabil.ResultadoDebito)
                {
                    resultadoDebito += valorSaldo;
                    origemZeragem = ContabilOrigem.ZeragemResultado.ZeragemPrejuizo;
                    idContaCredito = idConta;
                    contaCredito = codigoConta;

                    idContaDebito = idContaResultadoAcumulado;
                    contaDebito = contaResultadoAcumulado;

                    contabSaldo.TotalCredito += valorSaldo;
                }
                else
                {
                    resultadoCredito += valorSaldo;
                    origemZeragem = ContabilOrigem.ZeragemResultado.ZeragemLucro;
                    idContaDebito = idConta;
                    contaDebito = codigoConta;

                    idContaCredito = idContaResultadoAcumulado;
                    contaCredito = contaResultadoAcumulado;

                    contabSaldo.TotalDebito += valorSaldo;
                }

                contabSaldo.SaldoFinal = 0;

                ContabLancamento contabLancamento = new ContabLancamento();
                contabLancamento.ContaCredito = contaCredito;
                contabLancamento.ContaDebito = contaDebito;
                contabLancamento.DataLancamento = data;
                contabLancamento.DataRegistro = data;
                contabLancamento.Descricao = "ZERAGEM RESULTADOS";
                contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                contabLancamento.IdCliente = idCliente;
                contabLancamento.IdContaCredito = idContaCredito;
                contabLancamento.IdContaDebito = idContaDebito;
                contabLancamento.IdPlano = idPlano;
                contabLancamento.Origem = origemZeragem;
                contabLancamento.Valor = valorSaldo;
                contabLancamento.Save();
            }
            contabSaldoCollection.Save();

            decimal valorNet = resultadoCredito + resultadoDebito;

            if (valorNet != 0)
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, idContaResultadoAcumulado, data))
                {
                    contabSaldo.TotalCredito += Math.Abs(resultadoCredito);
                    contabSaldo.TotalDebito += Math.Abs(resultadoDebito);
                    contabSaldo.SaldoFinal += valorNet;
                    contabSaldo.Save();
                }
                else
                {
                    ContabSaldo contabSaldoNovo = new ContabSaldo();
                    contabSaldoNovo.Data = data;
                    contabSaldoNovo.IdCliente = idCliente;
                    contabSaldoNovo.IdConta = idContaResultadoAcumulado;
                    contabSaldoNovo.IdPlano = idPlano;
                    contabSaldoNovo.SaldoAnterior = 0;
                    contabSaldoNovo.SaldoFinal = valorNet;
                    contabSaldoNovo.TotalCredito = Math.Abs(resultadoCredito);
                    contabSaldoNovo.TotalDebito = Math.Abs(resultadoDebito);

                    contabSaldoNovo.Save();
                }

            }

            List<int> listaOrigem = new List<int>();
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoDesvalorizacaoAcao);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoDesvalorizacaoAcaoVendida);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoDesvalorizacaoOPCComprada);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoDesvalorizacaoOPCVendida);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoDesvalorizacaoOPVComprada);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoDesvalorizacaoOPVVendida);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoValorizacaoAcao);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoValorizacaoAcaoVendida);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoValorizacaoOPCComprada);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoValorizacaoOPCVendida);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoValorizacaoOPVComprada);
            listaOrigem.Add(ContabilOrigem.Bolsa.ReversaoValorizacaoOPVVendida);
            //Deleta todo e qualquer lançamento de reversão em Bolsa lançado na data
            ContabLancamentoCollection contabLancamentoCollectionReversao = new ContabLancamentoCollection();
            contabLancamentoCollectionReversao.Query.Select(contabLancamentoCollectionReversao.Query.IdLancamento);
            contabLancamentoCollectionReversao.Query.Where(contabLancamentoCollectionReversao.Query.IdCliente.Equal(idCliente),
                                                           contabLancamentoCollectionReversao.Query.DataRegistro.Equal(data),
                                                           contabLancamentoCollectionReversao.Query.Origem.In(listaOrigem));
            contabLancamentoCollectionReversao.Query.Load();
            contabLancamentoCollectionReversao.MarkAllAsDeleted();
            contabLancamentoCollectionReversao.Save();

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente));
            posicaoBolsaCollection.Query.Load();

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                posicaoBolsa.PUCustoLiquido = posicaoBolsa.PUMercado.Value;
            }
            posicaoBolsaCollection.Save();

            PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente));
            posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoBolsaHistoricoCollection.Query.Load();

            foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
            {
                posicaoBolsaHistorico.PUCustoLiquido = posicaoBolsaHistorico.PUMercado.Value;
            }
            posicaoBolsaHistoricoCollection.Save();

        }


    }
}
