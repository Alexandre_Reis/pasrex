using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.Contabil.Enums;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Util;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;

namespace Financial.Contabil
{
    public class ContabLiquidacao
    {        
        /// <summary>
        /// Eventos de Provisao, Pagamento para despesas diversas.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        public void GeraEventosLiquidacao(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            
            // Procura liquida��o provisionada nessa data
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.DataLancamento.Equal(data),
                                             liquidacaoCollection.Query.IdCliente.Equal(idCliente),
                                             liquidacaoCollection.Query.IdEvento.IsNotNull(),
                                             liquidacaoCollection.Query.Origem.In(OrigemLancamentoLiquidacao.Outros, 
                                                                                  OrigemLancamentoLiquidacao.Margem.ChamadaMargem,
                                                                                  OrigemLancamentoLiquidacao.Margem.DevolucaoMargem),
                                             liquidacaoCollection.Query.Fonte.NotEqual((byte)FonteLancamentoLiquidacao.Interno),
                                             liquidacaoCollection.Query.DataVencimento.GreaterThanOrEqual(data));
            if (liquidacaoCollection.Query.Load())
            {
                LancaEventosLiquidacao(liquidacaoCollection, idCliente, idPlano, data, "Provis�o");
            }

            // Procura liquida��o vencendo nessa data
            liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Where(liquidacaoCollection.Query.DataVencimento.Equal(data),
                                             liquidacaoCollection.Query.IdCliente.Equal(idCliente),
                                             liquidacaoCollection.Query.IdEventoVencimento.IsNotNull(),
                                             liquidacaoCollection.Query.Origem.In(OrigemLancamentoLiquidacao.Outros,
                                                                                  OrigemLancamentoLiquidacao.Margem.ChamadaMargem,
                                                                                  OrigemLancamentoLiquidacao.Margem.DevolucaoMargem),
                                             liquidacaoCollection.Query.Fonte.NotEqual((byte)FonteLancamentoLiquidacao.Interno),
                                             liquidacaoCollection.Query.DataVencimento.NotEqual(liquidacaoCollection.Query.DataLancamento));
            if (liquidacaoCollection.Query.Load())
            {
                LancaEventosLiquidacao(liquidacaoCollection, idCliente, idPlano, data, "Vencimento");
            }
                
        }

        public void LancaEventosLiquidacao(LiquidacaoCollection liquidacaoCollection, int idCliente, int idPlano, DateTime dataLancamento, string tipoLancamento )
        {
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                decimal valor = liquidacao.Valor.Value;
                DateTime dataVencimento = liquidacao.DataVencimento.Value;
                int idEvento;
                if (tipoLancamento == "Provis�o")
                {
                    idEvento = liquidacao.IdEvento.Value;
                }
                else
                {
                    idEvento = liquidacao.IdEventoVencimento.Value;
                }

                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                if (Math.Abs(valor) > 0)
                {
                    #region Evento de Liquidacao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(idEvento),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    int? origem = ContabRoteiro.RetornaOrigem(idEvento);
                    if (contabRoteiroCollection.Count > 0 && origem.HasValue)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = dataLancamento;
                        contabLancamento.DataRegistro = dataLancamento;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem.Value;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Liquidacao;
                        contabLancamento.Valor = valor;
                    }
                    #endregion
                }
            }

            contabLancamentoCollection.Save();
        }

    }
}
