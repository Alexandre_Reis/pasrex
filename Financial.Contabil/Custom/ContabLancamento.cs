using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Contabil
{
	public partial class ContabLancamento : esContabLancamento
	{
        //Teste
        /// <summary>
        /// Maps to ContabLancamento.Valor
        /// </summary>
        override public System.Decimal? Valor {
            set {
                decimal valor = Convert.ToDecimal(value);
                valor = Math.Abs(valor);
                base.SetSystemDecimal(ContabLancamentoMetadata.ColumnNames.Valor, valor);  
            }
        }
	}
}
