﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.Contabil.Enums;
using Financial.Investidor;
using Financial.InvestidorCotista.Enums;

namespace Financial.Contabil
{
    public class ContabCotista
    {
        private Dictionary<int, int> roteiroContabil;
        private ContabRoteiroCollection contabRoteiroCollection;

        public ContabCotista(Dictionary<int, int> roteiroContabil, ContabRoteiroCollection contabRoteiroCollection)
        {
            this.roteiroContabil = roteiroContabil;
            this.contabRoteiroCollection = contabRoteiroCollection;
        }

        /// <summary>
        /// Executa todos os processos para geração dos eventos contábeis do componente de cotista.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        public void ExecutaProcesso(Cliente cliente, DateTime data)
        {
            this.GeraEventosAplicacao(cliente, data);
            this.GeraEventosResgate(cliente, data);
        }

        /// <summary>
        /// Eventos de AplicacaoPF, AplicacaoPJ.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraEventosAplicacao(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            CotistaQuery cotistaQuery = new CotistaQuery("C");

            operacaoCotistaQuery.Select(operacaoCotistaQuery.ValorBruto.Sum(),
                                        operacaoCotistaQuery.DataConversao, 
                                        pessoaQuery.Tipo,
                                        pessoaQuery.OffShore);
            operacaoCotistaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == operacaoCotistaQuery.IdCotista);
            operacaoCotistaQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == cotistaQuery.IdPessoa);
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira.Equal(idCliente),
                                       operacaoCotistaQuery.DataLiquidacao.Equal(data),
                                       operacaoCotistaQuery.TipoOperacao.Equal((byte)TipoOperacaoCotista.Aplicacao));
            operacaoCotistaQuery.GroupBy(operacaoCotistaQuery.DataConversao,
                                         pessoaQuery.Tipo,
                                         pessoaQuery.OffShore);
            
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Load(operacaoCotistaQuery);

            decimal totalValor = 0;
            DateTime dataConversao = new DateTime();
            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                decimal valor = operacaoCotista.ValorBruto.Value;
                dataConversao = operacaoCotista.DataConversao.Value;
                byte tipoPessoa = (byte)TipoPessoa.Juridica;

                if (operacaoCotista.GetColumn(PessoaMetadata.ColumnNames.OffShore).ToString() != "S")
                    tipoPessoa = Convert.ToByte(operacaoCotista.GetColumn(PessoaMetadata.ColumnNames.Tipo));

                if (tipoPessoa == (byte)TipoPessoa.Fisica)
                {
                    #region Evento de AplicacaoPF
                    origem = ContabilOrigem.Cotista.AplicacaoPF;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valor;
                    }
                    #endregion
                }
                else if (tipoPessoa == (byte)TipoPessoa.Juridica)
                {
                    #region Evento de AplicacaoPJ
                    origem = ContabilOrigem.Cotista.AplicacaoPJ;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valor;
                    }
                    #endregion
                }

                totalValor += valor;
            }

            #region Evento de ConversaoAplicacao
            origem = ContabilOrigem.Cotista.ConversaoAplicacao;
            if (totalValor != 0 && roteiroContabil.ContainsKey(origem))
            {
                int idEvento = roteiroContabil[origem];

                ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                contabLancamento.DataLancamento = dataConversao;
                contabLancamento.DataRegistro = data;
                contabLancamento.IdCliente = idCliente;
                contabLancamento.IdPlano = idPlano;
                contabLancamento.Origem = origem;
                contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                contabLancamento.Valor = totalValor;
            }
            #endregion
            
            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de ResgateCustoPF, ResgateVariacaoPositivaPF, ResgateVariacaoPositivaPF, ResgateProvisaoIRPF, ResgateProvisaoIRPF,
        /// ResgateCustoPJ, ResgateVariacaoPositivaPJ, ResgateVariacaoPositivaPJ, ResgateProvisaoIRPJ, ResgateProvisaoIRPJ,
        /// ResgateProvisaoIRPF, ResgateProvisaoIRPF, LiquidacaoResgateIOF.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraEventosResgate(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
            PessoaQuery pessoaQuery = new PessoaQuery("P");
            CotistaQuery cotistaQuery = new CotistaQuery("C");
            operacaoCotistaQuery.Select(operacaoCotistaQuery.ValorBruto,
                                        operacaoCotistaQuery.ValorLiquido,
                                        operacaoCotistaQuery.ValorIR,
                                        operacaoCotistaQuery.ValorIOF,
                                        operacaoCotistaQuery.VariacaoResgate,
                                        operacaoCotistaQuery.DataLiquidacao,
                                        pessoaQuery.Tipo);
            operacaoCotistaQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista == operacaoCotistaQuery.IdCotista);
            operacaoCotistaQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == cotistaQuery.IdPessoa);
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira.Equal(idCliente),
                                       operacaoCotistaQuery.DataConversao.Equal(data),
                                       operacaoCotistaQuery.TipoOperacao.In((byte)TipoOperacaoCotista.ResgateBruto,
                                                                            (byte)TipoOperacaoCotista.ResgateCotas,
                                                                            (byte)TipoOperacaoCotista.ResgateLiquido,
                                                                            (byte)TipoOperacaoCotista.ResgateTotal));
            
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Load(operacaoCotistaQuery);

            decimal custoPF = 0;
            decimal custoPJ = 0;
            decimal variacaoPositivaPF = 0;
            decimal variacaoNegativaPF = 0;
            decimal variacaoPositivaPJ = 0;
            decimal variacaoNegativaPJ = 0;
            decimal provisaoIR_PF = 0;
            decimal provisaoIOF_PF = 0;
            decimal provisaoIR_PJ = 0;
            decimal provisaoIOF_PJ = 0;
            decimal totalLiquido = 0;
            decimal totalIR = 0;
            decimal totalIOF = 0;
            
            DateTime dataLiquidacao = new DateTime();

            foreach (OperacaoCotista operacaoCotista in operacaoCotistaCollection)
            {
                decimal valorBruto = operacaoCotista.ValorBruto.Value;
                decimal valorLiquido = operacaoCotista.ValorLiquido.Value;
                decimal valorIR = operacaoCotista.ValorIR.Value;
                decimal valorIOF = operacaoCotista.ValorIOF.Value;
                decimal variacao = operacaoCotista.VariacaoResgate.Value;
                dataLiquidacao = operacaoCotista.DataLiquidacao.Value;
                
                decimal custo = valorLiquido - variacao;
                totalLiquido += valorLiquido;
                totalIR += valorIR;
                totalIOF += valorIOF;
                byte tipoPessoa = Convert.ToByte(operacaoCotista.GetColumn(PessoaMetadata.ColumnNames.Tipo));
                
                if (tipoPessoa == (byte)TipoPessoa.Fisica)
                {
                    custoPF += custo;
                    provisaoIR_PF += valorIR;
                    provisaoIOF_PF += valorIOF;

                    if (variacao > 0)
                    {
                        variacaoPositivaPF += variacao;
                    }
                    else
                    {
                        variacaoNegativaPF += variacao;
                    }
                }
                else if (tipoPessoa == (byte)TipoPessoa.Juridica)
                {
                    custoPJ += custo;
                    provisaoIR_PJ += valorIR;
                    provisaoIOF_PJ += valorIOF;

                    if (variacao > 0)
                    {
                        variacaoPositivaPJ += variacao;
                    }
                    else
                    {
                        variacaoNegativaPJ += variacao;
                    }                    
                }
            }

            if (operacaoCotistaCollection.Count > 0)
            {
                #region Evento de ResgateCustoPF
                origem = ContabilOrigem.Cotista.ResgateCustoPF;
                if (roteiroContabil.ContainsKey(origem) && custoPF != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = custoPF;
                }
                #endregion

                #region Evento de ResgateProvisaoIRPF
                origem = ContabilOrigem.Cotista.ResgateProvisaoIRPF;
                if (roteiroContabil.ContainsKey(origem) && provisaoIR_PF != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoIR_PF;
                }
                #endregion

                #region Evento de ResgateProvisaoIOFPF
                origem = ContabilOrigem.Cotista.ResgateProvisaoIOFPF;
                if (roteiroContabil.ContainsKey(origem) && provisaoIOF_PF != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoIOF_PF;
                }
                #endregion

                #region Evento de ResgateVariacaoPositivaPF
                origem = ContabilOrigem.Cotista.ResgateVariacaoPositivaPF;
                if (roteiroContabil.ContainsKey(origem) && variacaoPositivaPF != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoPositivaPF;
                }
                #endregion

                #region Evento de ResgateVariacaoNegativaPF
                origem = ContabilOrigem.Cotista.ResgateVariacaoNegativaPF;
                if (roteiroContabil.ContainsKey(origem) && variacaoNegativaPF != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoNegativaPF;
                }
                #endregion

                #region Evento de ResgateCustoPJ
                origem = ContabilOrigem.Cotista.ResgateCustoPJ;
                if (roteiroContabil.ContainsKey(origem) && custoPJ != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = custoPJ;
                }
                #endregion

                #region Evento de ResgateProvisaoIRPJ
                origem = ContabilOrigem.Cotista.ResgateProvisaoIRPJ;
                if (roteiroContabil.ContainsKey(origem) && provisaoIR_PJ != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoIR_PJ;
                }
                #endregion

                #region Evento de ResgateProvisaoIOFPJ
                origem = ContabilOrigem.Cotista.ResgateProvisaoIOFPJ;
                if (roteiroContabil.ContainsKey(origem) && provisaoIOF_PJ != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoIOF_PJ;
                }
                #endregion

                #region Evento de ResgateVariacaoPositivaPJ
                origem = ContabilOrigem.Cotista.ResgateVariacaoPositivaPJ;
                if (roteiroContabil.ContainsKey(origem) && variacaoPositivaPJ != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoPositivaPJ;
                }
                #endregion

                #region Evento de ResgateVariacaoNegativaPJ
                origem = ContabilOrigem.Cotista.ResgateVariacaoNegativaPJ;
                if (roteiroContabil.ContainsKey(origem) && variacaoNegativaPJ != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoNegativaPJ;
                }
                #endregion
                
                #region Evento de LiquidacaoResgate
                origem = ContabilOrigem.Cotista.LiquidacaoResgate;
                if (roteiroContabil.ContainsKey(origem) && totalLiquido != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalLiquido;
                }
                #endregion

                #region Evento de LiquidacaoResgateIR
                origem = ContabilOrigem.Cotista.LiquidacaoResgateIR;
                if (roteiroContabil.ContainsKey(origem) && totalIR != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalIR;
                }
                #endregion

                #region Evento de LiquidacaoResgateIOF
                origem = ContabilOrigem.Cotista.LiquidacaoResgateIOF;
                if (roteiroContabil.ContainsKey(origem) && totalIOF != 0)
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalIOF;
                }
                #endregion

                contabLancamentoCollection.Save();
            }
        }
    }
}
