using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Contabil.Enums;

namespace Financial.Contabil
{
	public partial class ContabRoteiro : esContabRoteiro
	{
        public static int? RetornaOrigem(int idEvento)
        {
            int? origem = null;
            ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
            contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.Origem);
            contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.IdEvento.Equal(idEvento));
            contabRoteiroCollection.Query.Load();

            if (contabRoteiroCollection.Count > 0)
            {
                origem = contabRoteiroCollection[0].Origem.Value;
            }

            return origem;
        }

        public static int? RetornaIdEvento(int origem)
        {
            int? idEvento = null;
            ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
            contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdEvento);
            contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem));
            contabRoteiroCollection.Query.Load();

            if (contabRoteiroCollection.Count > 0)
            {
                idEvento = contabRoteiroCollection[0].IdEvento.Value;
            }

            return idEvento;
        }

	}
}
