﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.Investidor;
using Financial.Contabil.Enums;
using Financial.Fundo;
using Financial.Util;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;

namespace Financial.Contabil
{
    public class ContabFundo
    {
        /// <summary>
        /// Executa todos os processos para geração dos eventos contábeis do componente de bolsa.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        public void ExecutaProcesso(Cliente cliente, DateTime data)
        {
            bool historico = cliente.DataDia.Value > data;

            if (cliente.ContabilFundos == (byte)TipoContabilCotasInvestimento.Analitico)
            {
                this.GeraValorizacaoFundoAnalitico(cliente, data, historico);
                this.GeraAplicacaoResgateAnalitico(cliente, data);
                //this.GeraProvisaoIRAnalitico(cliente, data, historico);
            }
            else
            {
                this.GeraValorizacaoFundoConsolidado(cliente, data);
                this.GeraAplicacaoResgateConsolidado(cliente, data);
                this.GeraProvisaoIRConsolidado(cliente, data, historico);
            }

        }

        /// <summary>
        /// Eventos de Valorizacao, Desvalorizacao.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        private void GeraValorizacaoFundoAnalitico(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
                        
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            if (historico)
            {
                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                            posicaoFundoHistoricoCollection.Query.IdOperacao,
                                                            posicaoFundoHistoricoCollection.Query.Quantidade,
                                                            posicaoFundoHistoricoCollection.Query.CotaDia);
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                            posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoFundoHistoricoCollection.Query.Load();

                PosicaoFundoCollection posicaoFundoCollectionAux = new PosicaoFundoCollection(posicaoFundoHistoricoCollection);

                posicaoFundoCollection.Combine(posicaoFundoCollectionAux);
            }
            else
            {
                posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira,
                                                    posicaoFundoCollection.Query.IdOperacao,
                                                    posicaoFundoCollection.Query.Quantidade,
                                                    posicaoFundoCollection.Query.CotaDia);
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoCollection.Query.Load();
            }

            DateTime dataAnterior = new DateTime();
            if (posicaoFundoCollection.Count > 0)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {               
                int idCarteira = posicaoFundo.IdCarteira.Value;
                int idOperacao = posicaoFundo.IdOperacao.Value;
                decimal quantidade = posicaoFundo.Quantidade.Value;
                decimal cotaDia = posicaoFundo.CotaDia.Value;
                

                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.CotaDia,
                                                             posicaoFundoHistoricoCollection.Query.Quantidade);
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCarteira.Equal(idCarteira),
                                                            posicaoFundoHistoricoCollection.Query.IdOperacao.Equal(idOperacao),
                                                  posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(dataAnterior));
                posicaoFundoHistoricoCollection.Query.Load();

                decimal rendimento = 0;
                if (posicaoFundoHistoricoCollection.Count > 0)
                {
                    decimal quantidadeAnterior = posicaoFundoHistoricoCollection[0].Quantidade.Value; ; 
                    decimal cotaDiaAnterior = posicaoFundoHistoricoCollection[0].CotaDia.Value;
                    rendimento = Utilitario.Truncate(quantidadeAnterior * (cotaDia - cotaDiaAnterior), 2);
                }

                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                if (rendimento > 0)
                {
                    #region Evento de Valorizacao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.Valorizacao),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(idCarteira.ToString()));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = idCarteira.ToString();
                        contabLancamento.IdPlano = cliente.IdPlano.Value;
                        contabLancamento.Origem = ContabilOrigem.CotaInvestimento.Valorizacao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = rendimento;
                    }
                    #endregion
                }
                else if (rendimento < 0)
                {
                    #region Evento de Desvalorizacao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.Desvalorizacao),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(idCarteira.ToString()));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = idCarteira.ToString();
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.CotaInvestimento.Desvalorizacao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = Math.Abs(rendimento);
                    }
                    #endregion
                }

            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de Aplicacao, Resgate.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraAplicacaoResgateAnalitico(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            #region Aplicações
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.IdCarteira,
                                                 operacaoFundoCollection.Query.ValorBruto.Sum());
            operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoFundoCollection.Query.DataConversao.Equal(data),
                                                operacaoFundoCollection.Query.TipoOperacao.In((byte)TipoOperacaoFundo.Aplicacao));
            operacaoFundoCollection.Query.GroupBy(operacaoFundoCollection.Query.IdCarteira);
            operacaoFundoCollection.Query.Load();

            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
            {
                int idCarteira = operacaoFundo.IdCarteira.Value;
                decimal valor = operacaoFundo.ValorBruto.Value;

                int? idCentroCusto = null;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.Aplicacao),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(idCarteira.ToString()));

                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                }

                if (contaDebito != "" && contaCredito != "")
                {
                    int origem = ContabilOrigem.CotaInvestimento.Aplicacao;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = idCarteira.ToString();
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valor;
                }
                
            }
            #endregion

            #region Resgates
            operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Query.Select(operacaoFundoCollection.Query.IdCarteira,
                                                 operacaoFundoCollection.Query.DataLiquidacao,
                                                 operacaoFundoCollection.Query.ValorBruto.Sum());
            operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoFundoCollection.Query.DataConversao.Equal(data),
                                                operacaoFundoCollection.Query.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateBruto,
                                                                                              (byte)TipoOperacaoFundo.ResgateLiquido,
                                                                                              (byte)TipoOperacaoFundo.ResgateCotas,
                                                                                              (byte)TipoOperacaoFundo.ResgateTotal));
            operacaoFundoCollection.Query.GroupBy(operacaoFundoCollection.Query.IdCarteira, operacaoFundoCollection.Query.DataLiquidacao);
            operacaoFundoCollection.Query.Load();

            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
            {
                int idCarteira = operacaoFundo.IdCarteira.Value;
                DateTime dataLiquidacao = operacaoFundo.DataLiquidacao.Value;
                decimal valor = operacaoFundo.ValorBruto.Value;
                
                int? idCentroCusto = null;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.Resgate),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(idCarteira.ToString()));

                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                }                

                if (contaDebito != "" && contaCredito != "")
                {
                    int origem = ContabilOrigem.CotaInvestimento.Resgate;
                
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = idCarteira.ToString();
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valor;
                }


                idCentroCusto = null;
                descricao = "";
                contaDebito = "";
                contaCredito = "";
                idContaDebito = 0;
                idContaCredito = 0;

                contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.LiquidacaoResgate),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(idCarteira.ToString()));

                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                }

                if (contaDebito != "" && contaCredito != "")
                {
                    int origem = ContabilOrigem.CotaInvestimento.LiquidacaoResgate;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = idCarteira.ToString();
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valor;
                }
            }
            #endregion

            contabLancamentoCollection.Save();

        }


        /// <summary>
        /// Eventos de Provisão de IR sobre Posição de Fundos.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        private void GeraProvisaoIRAnalitico(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            if (historico)
            {
                PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
                posicaoFundoHistoricoCollection.Query.Select(posicaoFundoHistoricoCollection.Query.IdCarteira,
                                                            posicaoFundoHistoricoCollection.Query.ValorIR.Sum(),
                                                            posicaoFundoHistoricoCollection.Query.ValorIOF.Sum());
                posicaoFundoHistoricoCollection.Query.Where(posicaoFundoHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoFundoHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                            posicaoFundoHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoFundoHistoricoCollection.Query.GroupBy(posicaoFundoHistoricoCollection.Query.IdCarteira);
                posicaoFundoHistoricoCollection.Query.Load();

                PosicaoFundoCollection posicaoFundoCollectionAux = new PosicaoFundoCollection(posicaoFundoHistoricoCollection);

                posicaoFundoCollection.Combine(posicaoFundoCollectionAux);
            }
            else
            {
                posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira,
                                                    posicaoFundoCollection.Query.ValorIR.Sum(),
                                                    posicaoFundoCollection.Query.ValorIOF.Sum());
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoFundoCollection.Query.Quantidade.NotEqual(0));
                posicaoFundoCollection.Query.GroupBy(posicaoFundoCollection.Query.IdCarteira);
                posicaoFundoCollection.Query.Load();
            }

            DateTime dataAnterior = new DateTime();
            if (posicaoFundoCollection.Count > 0)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            foreach (PosicaoFundo posicaoFundo in posicaoFundoCollection)
            {
                decimal valorIR = 0;
                decimal valorIOF = 0;

                int idCarteira = posicaoFundo.IdCarteira.Value;
                valorIR = posicaoFundo.ValorIR.Value;
                valorIOF = posicaoFundo.ValorIOF.Value;
                

                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                if (valorIR > 0)
                {
                    #region Evento de Provisão de IR
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.ProvisaoIR),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(idCarteira.ToString()));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = idCarteira.ToString();
                        contabLancamento.IdPlano = cliente.IdPlano.Value;
                        contabLancamento.Origem = ContabilOrigem.CotaInvestimento.ProvisaoIR;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorIR;
                    }
                    #endregion
                }
                if (valorIOF > 0)
                {
                    #region Evento de Provisão de IOF
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.ProvisaoIOF),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(idCarteira.ToString()));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = idCarteira.ToString();
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.CotaInvestimento.ProvisaoIOF;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = Math.Abs(valorIOF);
                    }
                    #endregion
                }

            }

            contabLancamentoCollection.Save();
        }


        /// <summary>
        /// Eventos de Provisão de IR / IOF sobre Posição de Fundos (consolidado)
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        private void GeraProvisaoIRConsolidado(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;            

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();

            if (historico)
            {
                PosicaoFundoHistoricoCollection posicaoFundoHistoricoColl = new PosicaoFundoHistoricoCollection();

                posicaoFundoHistoricoColl.Query.Select(posicaoFundoHistoricoColl.Query.IdCarteira,
                                                  posicaoFundoHistoricoColl.Query.ValorIOF,
                                                  posicaoFundoHistoricoColl.Query.ValorIR,
                                                  posicaoFundoHistoricoColl.Query.IdPosicao);
                posicaoFundoHistoricoColl.Query.Where(posicaoFundoHistoricoColl.Query.IdCliente.Equal(idCliente),
                                                posicaoFundoHistoricoColl.Query.Quantidade.NotEqual(0),
                                                posicaoFundoHistoricoColl.Query.DataHistorico.Equal(data));

                posicaoFundoHistoricoColl.Query.Load();

                PosicaoFundoCollection posicaoFundoCollectionAux = new PosicaoFundoCollection(posicaoFundoHistoricoColl);
                posicaoFundoCollection.Combine(posicaoFundoCollectionAux);
            }
            else
            {
                posicaoFundoCollection.Query.Select(posicaoFundoCollection.Query.IdCarteira,
                                         posicaoFundoCollection.Query.ValorIOF,
                                         posicaoFundoCollection.Query.ValorIR,
                                         posicaoFundoCollection.Query.IdPosicao);
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoFundoCollection.Query.Quantidade.NotEqual(0));
               
                posicaoFundoCollection.Query.Load();
            }
        

            DateTime dataAnterior = new DateTime();
            if (posicaoFundoCollection.Count > 0)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            int i = 0;
            while (i < posicaoFundoCollection.Count)
            {

                int idCarteira = posicaoFundoCollection[i].IdCarteira.Value;
                
                ClienteInterface clienteInterface = new ClienteInterface();
                clienteInterface.LoadByPrimaryKey(idCarteira);
                if (! clienteInterface.CategoriaContabil.HasValue)
                {
                    continue;
                }
                int categoria = clienteInterface.CategoriaContabil.Value;
                                
                
                decimal valorTotalIOF =0;
                decimal valorTotalIR = 0;                
                
                
                int idCarteiraAnterior = idCarteira;
                while (idCarteiraAnterior == idCarteira && i < posicaoFundoCollection.Count)
                {
                   
                    int idPosicao = posicaoFundoCollection[i].IdPosicao.Value;
                    decimal valorIRAnterior = 0;
                    decimal valorIOFAnterior = 0;
                    decimal valorIOFDia = posicaoFundoCollection[i].ValorIOF.Value;
                    decimal valorIRDia = posicaoFundoCollection[i].ValorIR.Value;        


                    PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                    if (posicaoFundoHistorico.LoadByPrimaryKey(idPosicao, dataAnterior))
                    {
                        valorIRAnterior = posicaoFundoHistorico.ValorIR.Value;
                        valorIOFAnterior = posicaoFundoHistorico.ValorIOF.Value;
                    }
                    valorTotalIR += (valorIRDia -  valorIRAnterior ) ;
                    valorTotalIOF += (valorIOFDia - valorIOFAnterior);

                    i++;

                    if (i < posicaoFundoCollection.Count)
                    {
                        idCarteira = posicaoFundoCollection[i].IdCarteira.Value;
                    }
                }

                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;                
                
                int origem = 0;
                if (valorTotalIR > 0)
                {
                    origem = ContabilOrigem.CotaInvestimento.ProvisaoIR;
                }
                else
                {
                    origem = ContabilOrigem.CotaInvestimento.EstornoProvisaoIR;
                }

                #region Evento de provisao IR
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(categoria));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0 && valorTotalIR != 0 )
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorTotalIR;
                }
                #endregion


                //iof 
                 if (valorTotalIOF > 0)
                 {
                     origem = ContabilOrigem.CotaInvestimento.ProvisaoIOF;
                 }
                 else
                 {
                     origem = ContabilOrigem.CotaInvestimento.EstornoProvisaoIOF;
                 }


                 #region Evento de provisão IOF
                 ContabRoteiroCollection contabRoteiroCollectionIof = new ContabRoteiroCollection();
                 contabRoteiroCollectionIof.Query.Where(contabRoteiroCollectionIof.Query.Origem.Equal(origem),
                                                     contabRoteiroCollectionIof.Query.IdPlano.Equal(idPlano),
                                                     contabRoteiroCollectionIof.Query.Identificador.Equal(categoria));
                 contabRoteiroCollectionIof.Query.Load();

                 if (contabRoteiroCollectionIof.Count > 0 && valorTotalIOF != 0)
                 {
                     descricao = contabRoteiroCollectionIof[0].Descricao;
                     contaDebito = contabRoteiroCollectionIof[0].ContaDebito;
                     contaCredito = contabRoteiroCollectionIof[0].ContaCredito;
                     idCentroCusto = contabRoteiroCollectionIof[0].IdCentroCusto;
                     idContaDebito = contabRoteiroCollectionIof[0].IdContaDebito.Value;
                     idContaCredito = contabRoteiroCollectionIof[0].IdContaCredito.Value;

                     ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                     contabLancamento.IdContaDebito = idContaDebito;
                     contabLancamento.IdContaCredito = idContaCredito;
                     contabLancamento.ContaCredito = contaCredito;
                     contabLancamento.ContaDebito = contaDebito;
                     contabLancamento.DataLancamento = data;
                     contabLancamento.DataRegistro = data;
                     contabLancamento.Descricao = descricao;
                     contabLancamento.IdCentroCusto = idCentroCusto;
                     contabLancamento.IdCliente = idCliente;
                     contabLancamento.IdPlano = idPlano;
                     contabLancamento.Origem = origem;
                     contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                     contabLancamento.Valor = valorTotalIOF;
                 }
                 #endregion

                 
                
            }            

            contabLancamentoCollection.Save();
        }


        /// <summary>
        /// Eventos de Valorizacao, Desvalorizacao.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraValorizacaoFundoConsolidado(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("P");
            ClienteInterfaceQuery clienteInterfaceQuery = new ClienteInterfaceQuery("C");
            posicaoFundoHistoricoQuery.Select(clienteInterfaceQuery.CategoriaContabil,
                                              posicaoFundoHistoricoQuery.IdCarteira,
                                              posicaoFundoHistoricoQuery.Quantidade,
                                              posicaoFundoHistoricoQuery.CotaDia,
                                              posicaoFundoHistoricoQuery.IdPosicao);
            posicaoFundoHistoricoQuery.InnerJoin(clienteInterfaceQuery).On(clienteInterfaceQuery.IdCliente == posicaoFundoHistoricoQuery.IdCarteira);
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(idCliente),
                                            posicaoFundoHistoricoQuery.Quantidade.NotEqual(0),
                                            posicaoFundoHistoricoQuery.DataHistorico.Equal(dataAnterior),
                                            clienteInterfaceQuery.CategoriaContabil.IsNotNull());

            PosicaoFundoHistoricoCollection posicaoFundoCollection = new PosicaoFundoHistoricoCollection();
            posicaoFundoCollection.Load(posicaoFundoHistoricoQuery);

            int i = 0;
            while (i < posicaoFundoCollection.Count)
            {
                if (Convert.IsDBNull(posicaoFundoCollection[i].GetColumn(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil)))
                {
                    continue;
                }

                int categoria = Convert.ToInt32(posicaoFundoCollection[i].GetColumn(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil));
                int idCarteira = posicaoFundoCollection[i].IdCarteira.Value;
                decimal cotaAnterior = posicaoFundoCollection[i].CotaDia.Value;

                Carteira carteira = new Carteira();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(carteira.Query.TipoCota);
                campos.Add(carteira.Query.TruncaFinanceiro);
                carteira.LoadByPrimaryKey(campos, idCarteira);

                decimal totalRendimento = 0;
                int idCarteiraAnterior = idCarteira;
                while (idCarteiraAnterior == idCarteira && i < posicaoFundoCollection.Count)
                {
                    decimal quantidade = posicaoFundoCollection[i].Quantidade.Value;
                    decimal cotaDia = posicaoFundoCollection[i].CotaDia.Value;

                    decimal rendimentoDia = 0;
                    HistoricoCota historicoCota = new HistoricoCota();
                    if (historicoCota.BuscaValorCota(idCarteira, data) && historicoCota.CotaFechamento.HasValue)
                    {
                        decimal valorAnterior = 0;
                        decimal valorDia = 0;
                        if (carteira.TruncaFinanceiro == "S")
                        {
                            valorAnterior = Utilitario.Truncate(cotaAnterior * quantidade, 2);
                            valorDia = Utilitario.Truncate(historicoCota.CotaFechamento.Value * quantidade, 2);
                        }
                        else
                        {
                            valorAnterior = Math.Round(cotaAnterior * quantidade, 2);
                            valorDia = Math.Round(historicoCota.CotaFechamento.Value * quantidade, 2);
                        }

                        rendimentoDia = valorDia - valorAnterior;
                        totalRendimento += rendimentoDia;
                    }

                    i++;

                    if (i < posicaoFundoCollection.Count)
                    {
                        idCarteira = posicaoFundoCollection[i].IdCarteira.Value;
                    }
                }

                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                if (totalRendimento != 0)
                {
                    int origem = 0;
                    if (totalRendimento > 0)
                    {
                        origem = ContabilOrigem.CotaInvestimento.Valorizacao;
                    }
                    else
                    {
                        origem = ContabilOrigem.CotaInvestimento.Desvalorizacao;
                    }

                    #region Evento de Valorizacao/Desvalorizacao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(categoria));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = totalRendimento;
                    }
                    #endregion
                }
            }


            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de Aplicacao, Resgate, LiquidacaoResgate.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraAplicacaoResgateConsolidado(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            #region Aplicações
            OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
            ClienteInterfaceQuery clienteInterfaceQuery = new ClienteInterfaceQuery("C");

            operacaoFundoQuery.Select(clienteInterfaceQuery.CategoriaContabil,
                                      operacaoFundoQuery.IdCarteira,
                                      operacaoFundoQuery.ValorBruto.Sum());
            operacaoFundoQuery.InnerJoin(clienteInterfaceQuery).On(clienteInterfaceQuery.IdCliente == operacaoFundoQuery.IdCarteira);
            operacaoFundoQuery.Where(operacaoFundoQuery.IdCliente.Equal(idCliente),
                                    operacaoFundoQuery.DataConversao.Equal(data),
                                    operacaoFundoQuery.TipoOperacao.In((byte)TipoOperacaoFundo.Aplicacao));
            operacaoFundoQuery.GroupBy(clienteInterfaceQuery.CategoriaContabil, operacaoFundoQuery.IdCarteira);

            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Load(operacaoFundoQuery);

            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
            {
                if (Convert.IsDBNull(operacaoFundo.GetColumn(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil)))
                {
                    continue;
                }

                int categoria = Convert.ToInt32(operacaoFundo.GetColumn(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil));
                int idCarteira = operacaoFundo.IdCarteira.Value;
                decimal valor = operacaoFundo.ValorBruto.Value;                

                int? idCentroCusto = null;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.Aplicacao),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(categoria));

                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                }

                if (contaDebito != "" && contaCredito != "")
                {
                    int origem = ContabilOrigem.CotaInvestimento.Aplicacao;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = idCarteira.ToString();
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valor;
                }

            }
            #endregion

            #region Resgates
            operacaoFundoQuery = new OperacaoFundoQuery("O");
            clienteInterfaceQuery = new ClienteInterfaceQuery("C");

            operacaoFundoQuery.Select(clienteInterfaceQuery.CategoriaContabil,
                                      operacaoFundoQuery.IdCarteira,
                                      operacaoFundoQuery.DataLiquidacao,
                                      operacaoFundoQuery.ValorIR.Sum(),
                                      operacaoFundoQuery.ValorIOF.Sum(),
                                      operacaoFundoQuery.ValorBruto.Sum());
            operacaoFundoQuery.InnerJoin(clienteInterfaceQuery).On(clienteInterfaceQuery.IdCliente == operacaoFundoQuery.IdCarteira);
            operacaoFundoQuery.Where(operacaoFundoQuery.IdCliente.Equal(idCliente),
                                    operacaoFundoQuery.DataConversao.Equal(data),
                                    operacaoFundoQuery.TipoOperacao.In((byte)TipoOperacaoFundo.ResgateBruto,
                                                                      (byte)TipoOperacaoFundo.ResgateLiquido,
                                                                      (byte)TipoOperacaoFundo.ResgateCotas,
                                                                      (byte)TipoOperacaoFundo.ResgateTotal));
            operacaoFundoQuery.GroupBy(clienteInterfaceQuery.CategoriaContabil, 
                                       operacaoFundoQuery.IdCarteira, 
                                       operacaoFundoQuery.DataLiquidacao);

            operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Load(operacaoFundoQuery);
                        
            foreach (OperacaoFundo operacaoFundo in operacaoFundoCollection)
            {
                if (Convert.IsDBNull(operacaoFundo.GetColumn(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil)))
                {
                    continue;
                }

                int categoria = Convert.ToInt32(operacaoFundo.GetColumn(ClienteInterfaceMetadata.ColumnNames.CategoriaContabil));
                int idCarteira = operacaoFundo.IdCarteira.Value;
                DateTime dataLiquidacao = operacaoFundo.DataLiquidacao.Value;
                decimal valor = operacaoFundo.ValorBruto.Value;

                decimal valorIOF = operacaoFundo.ValorIOF.Value;
                decimal valorIR = operacaoFundo.ValorIR.Value;

                int? idCentroCusto = null;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.Resgate),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(categoria));

                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                }

                if (contaDebito != "" && contaCredito != "")
                {
                    int origem = ContabilOrigem.CotaInvestimento.Resgate;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = idCarteira.ToString();
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valor;
                }


                idCentroCusto = null;
                descricao = "";
                contaDebito = "";
                contaCredito = "";
                idContaDebito = 0;
                idContaCredito = 0;

                contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.LiquidacaoResgate),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));

                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                }

                if (contaDebito != "" && contaCredito != "")
                {
                    int origem = ContabilOrigem.CotaInvestimento.LiquidacaoResgate;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = idCarteira.ToString();
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valor;
                }
                #region IR/IOF no resgate
                idCentroCusto = null;
                descricao = "";
                contaDebito = "";
                contaCredito = "";
                idContaDebito = 0;
                idContaCredito = 0;

                contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.ResgateProvisaoIR),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));

                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                }

                if (contaDebito != "" && contaCredito != "")
                {
                    int origem = ContabilOrigem.CotaInvestimento.ResgateProvisaoIR;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = idCarteira.ToString();
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorIR;
                }

                //iof
                idCentroCusto = null;
                descricao = "";
                contaDebito = "";
                contaCredito = "";
                idContaDebito = 0;
                idContaCredito = 0;

                contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.CotaInvestimento.ResgateProvisaoIOF),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));

                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                }

                if (contaDebito != "" && contaCredito != "")
                {
                    int origem = ContabilOrigem.CotaInvestimento.ResgateProvisaoIOF;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = idCarteira.ToString();
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorIOF;
                }
                #endregion


            }
            #endregion

            contabLancamentoCollection.Save();

        }
    }
}
