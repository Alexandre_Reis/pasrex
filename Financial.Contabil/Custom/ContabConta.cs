using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.Contabil
{
	public partial class ContabConta : esContabConta
	{
        /// <summary>
        /// Calcula de digito verificador conforme descrito na instrucaoo IN CVM N 438, 
        /// Anexo Plano Contabil dos Fundos de Investimento  COFI
        /// </summary>
        /// <param name="conta">The conta.</param>
        /// <returns></returns>
        public int CalculaDigitoContaCOFI(string conta)
        {
            int[] ctrl = { 3, 1, 7, 3, 1, 7, 3 };
            int soma = 0;

            for (int i = 0; i <= 6; i++)
            {
                soma += Convert.ToInt32(conta.Substring(i, 1)) * ctrl[i];
            }

            int dv = (soma % 10) == 0 ? 0 : 10 - (soma % 10);

            return dv;
        }

        public bool ValidaDigitoContaCOFI(string conta)
        {
            string validarConta = conta.Replace(".", "");
            if (validarConta.Length < 8)
            {
                throw new Exception(string.Format("Formato de conta contabil invalido: {0}", conta));
                return false;
            }
            string digito = validarConta[validarConta.Length - 1].ToString();

            int dv = CalculaDigitoContaCOFI(validarConta);

            return (dv.ToString() == digito);
        }

	}
}
