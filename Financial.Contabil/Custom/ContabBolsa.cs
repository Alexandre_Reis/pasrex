﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.Bolsa;
using Financial.Util;
using Financial.Contabil.Enums;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.ContaCorrente;
using Financial.Bolsa.Exceptions;
using Financial.ContaCorrente.Enums;
using Financial.Tributo.Enums;

namespace Financial.Contabil
{    
    public class ContabBolsa
    {
        private Dictionary<int, int> roteiroContabil;
        private ContabRoteiroCollection contabRoteiroCollection;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roteiroContabil"></param>
        /// <param name="contabRoteiroCollection"></param>
        public ContabBolsa(Dictionary<int, int> roteiroContabil, ContabRoteiroCollection contabRoteiroCollection)
        {
            this.roteiroContabil = roteiroContabil;
            this.contabRoteiroCollection = contabRoteiroCollection;
        }

        /// <summary>
        /// Executa todos os processos para geração dos eventos contábeis do componente de bolsa.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="dataSaldoInicial"></param>
        public void ExecutaProcesso(Cliente cliente, DateTime data, DateTime? dataSaldoInicial)
        {
            //Importante o booleano processaAjusteReversao, para jogar em resultado acumulado o 1o evento de reversão de valorização/desvalorização
            bool processaAjusteReversao = (dataSaldoInicial.HasValue && dataSaldoInicial.Value >= data);
            //*************************************************************************************************************************************

            bool historico = cliente.DataDia.Value > data;

            DateTime dataProxima = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            DateTime? dataZeragem = null;
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
            contabLancamentoCollection.Query.Select(contabLancamentoCollection.Query.DataLancamento);
            contabLancamentoCollection.Query.Where(contabLancamentoCollection.Query.IdCliente.Equal(cliente.IdCliente.Value),
                                                   contabLancamentoCollection.Query.Origem.In(ContabilOrigem.ZeragemResultado.ZeragemLucro,
                                                                                              ContabilOrigem.ZeragemResultado.ZeragemPrejuizo));
            contabLancamentoCollection.Query.OrderBy(contabLancamentoCollection.Query.DataLancamento.Descending);
            contabLancamentoCollection.Query.es.Distinct = true;
            contabLancamentoCollection.Query.Load();

            if (contabLancamentoCollection.Count > 0)
            {
                dataZeragem = contabLancamentoCollection[0].DataLancamento.Value;
            }

            if (cliente.ContabilAcoes == (byte)TipoContabilAcoes.Analitico)
            {
                this.GeraValorizacaoAcaoAnalitico(cliente, data, historico, dataProxima);
                this.GeraOperacaoAcaoAnalitico(cliente, data);
            }
            else if (cliente.ContabilAcoes == (byte)TipoContabilAcoes.Consolidado)
            {
                this.GeraValorizacaoAcaoConsolidado(cliente, data, historico, dataProxima, processaAjusteReversao);
                this.GeraValorizacaoOPCConsolidado(cliente, data, historico, dataProxima, processaAjusteReversao);
                this.GeraValorizacaoOPVConsolidado(cliente, data, historico, dataProxima, processaAjusteReversao);
                
                this.GeraOperacaoAcaoConsolidado(cliente, data, true); //Roda para IPO
                this.GeraOperacaoAcaoConsolidado(cliente, data, false); //Roda para Não IPO
                this.GeraOperacaoAcaoResultados(cliente, data);

                this.GeraOperacaoOPCConsolidado(cliente, data);
                this.GeraOperacaoOPVConsolidado(cliente, data);

                this.GeraOperacaoExercicioOpcao(cliente, data);

                if (cliente.ContabilDaytrade.Value == (byte)TipoContabilDaytrade.Net)
                {
                    this.GeraDayTradeAcao(cliente, data);
                    this.GeraDayTradeOpcao(cliente, data);
                }
                else
                {
                    this.GeraDayTradeConsolidado(cliente, data);
                }

                this.GeraExpiracaoOpcaoConsolidado(cliente, data);

                this.GeraEventosBolsa(cliente, data);

                this.GeraEventosBTCOperacao(cliente, data, historico, dataProxima);
                this.GeraEventosBTCPosicao(cliente, data, historico, dataProxima);
                this.GeraEventosBTCLiquidacao(cliente, data, historico, dataProxima);

                this.GeraOperacaoTermo(cliente, data);
                this.GeraLiquidacaoFinanceiraTermo(cliente, data);
                this.GeraLiquidacaoFisicaTermo(cliente, data, historico);
                this.GeraEventosTermoPosicao(cliente, data, historico);
            }
        }

        /// <summary>
        /// Eventos de ValorizacaoAcao, ReversaoValorizacaoAcao, DesvalorizacaoAcao, ReversaoDesvalorizacaoAcao
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <param name="dataProxima"></param>
        private void GeraValorizacaoAcaoAnalitico(Cliente cliente, DateTime data, bool historico, DateTime dataProxima)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            if (historico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                            posicaoBolsaHistoricoCollection.Query.ValorCustoLiquido.Sum(),
                                                            posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                           posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario),
                                                           posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaCollection.Query.ValorCustoLiquido.Sum(),
                                                    posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.Quantidade.NotEqual(0),
                                                   posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();
            }

            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                
                decimal valorCustoLiquido = posicaoBolsa.ValorCustoLiquido.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                decimal valorizacao = Utilitario.Truncate(valorMercado - valorCustoLiquido, 2);

                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                if (valorizacao > 0)
                {
                    #region Evento de ValorizacaoAcao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.ValorizacaoAcao),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBolsa));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = cdAtivoBolsa;
                        contabLancamento.IdPlano = cliente.IdPlano.Value;
                        contabLancamento.Origem = ContabilOrigem.Bolsa.ValorizacaoAcao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorizacao;
                    }
                    #endregion

                    #region Evento de ReversaoValorizacaoAcao
                    contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.ReversaoValorizacaoAcao),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBolsa));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;                        
                        contabLancamento.DataLancamento = dataProxima;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = cdAtivoBolsa;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.Bolsa.ReversaoValorizacaoAcao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorizacao;
                    }
                    #endregion
                }
                else if (valorizacao < 0)
                {
                    #region Evento de DesvalorizacaoAcao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.DesvalorizacaoAcao),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBolsa));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = cdAtivoBolsa;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.Bolsa.DesvalorizacaoAcao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = Math.Abs(valorizacao);
                    }
                    #endregion

                    #region Evento de ReversaoDesvalorizacaoAcao
                    contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.ReversaoDesvalorizacaoAcao),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBolsa));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = dataProxima;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = cdAtivoBolsa;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.Bolsa.ReversaoDesvalorizacaoAcao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = Math.Abs(valorizacao);
                    }
                    #endregion
                }
                
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de CompraAcaoBruto, LiquidacaoCompraAcaoBruto, VendaAcaoBruto, LiquidacaoVendaAcaoBruto, TotalAcaoDespesas, LiquidacaoAcaoDespesas
        /// LucroAcao, PrejuizoAcao.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraOperacaoAcaoAnalitico(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            #region Eventos de CompraAcao
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.CdAtivoBolsa,
                                                 operacaoBolsaCollection.Query.Valor.Sum(),
                                                 operacaoBolsaCollection.Query.Corretagem.Sum(),
                                                 operacaoBolsaCollection.Query.Emolumento.Sum(),
                                                 operacaoBolsaCollection.Query.LiquidacaoCBLC.Sum(),
                                                 operacaoBolsaCollection.Query.RegistroBolsa.Sum(),
                                                 operacaoBolsaCollection.Query.RegistroCBLC.Sum());
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra),
                                                operacaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
            operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.CdAtivoBolsa);
            operacaoBolsaCollection.Query.Load();
            
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                decimal valor = operacaoBolsa.Valor.Value;
                decimal corretagem = operacaoBolsa.Corretagem.Value;
                decimal emolumento = operacaoBolsa.Emolumento.Value;
                decimal liquidacaoCBLC = operacaoBolsa.LiquidacaoCBLC.Value;
                decimal registroBolsa = operacaoBolsa.RegistroBolsa.Value;
                decimal registroCBLC = operacaoBolsa.RegistroCBLC.Value;

                decimal valorLiquido = Math.Abs(valor + corretagem + emolumento + liquidacaoCBLC + registroBolsa + registroCBLC);
                
                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                #region Lança Evento
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.CompraAcao),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBolsa));                

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = cdAtivoBolsa;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = ContabilOrigem.Bolsa.CompraAcao;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorLiquido;
                }
                #endregion
            }
            #endregion

            #region Eventos de VendaAcao
            operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.CdAtivoBolsa,
                                                 operacaoBolsaCollection.Query.Valor.Sum(),
                                                 operacaoBolsaCollection.Query.Corretagem.Sum(),
                                                 operacaoBolsaCollection.Query.Emolumento.Sum(),
                                                 operacaoBolsaCollection.Query.LiquidacaoCBLC.Sum(),
                                                 operacaoBolsaCollection.Query.RegistroBolsa.Sum(),
                                                 operacaoBolsaCollection.Query.RegistroCBLC.Sum());
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda),
                                                operacaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
            operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.CdAtivoBolsa);
            operacaoBolsaCollection.Query.Load();

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                decimal valor = operacaoBolsa.Valor.Value;
                decimal corretagem = operacaoBolsa.Corretagem.Value;
                decimal emolumento = operacaoBolsa.Emolumento.Value;
                decimal liquidacaoCBLC = operacaoBolsa.LiquidacaoCBLC.Value;
                decimal registroBolsa = operacaoBolsa.RegistroBolsa.Value;
                decimal registroCBLC = operacaoBolsa.RegistroCBLC.Value;

                decimal valorLiquido = Math.Abs(valor - corretagem - emolumento - liquidacaoCBLC - registroBolsa - registroCBLC);
                
                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;
                
                #region Lança Evento
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.VendaAcao),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBolsa));
                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.Identificador = cdAtivoBolsa;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = ContabilOrigem.Bolsa.VendaAcao;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorLiquido;
                }
                #endregion
            }
            #endregion
            
            #region Eventos de LucroAcao, PrejuizoAcao (apenas operações normais)
            operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.CdAtivoBolsa,
                                                 operacaoBolsaCollection.Query.ResultadoRealizado.Sum());
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.Venda),
                                                operacaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
            operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.CdAtivoBolsa);
            operacaoBolsaCollection.Query.Load();

            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                string cdAtivoBolsa = operacaoBolsa.CdAtivoBolsa;
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value;
                
                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                if (resultado > 0)
                {
                    #region Evento de LucroAcao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.LucroAcao),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBolsa));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = cdAtivoBolsa;
                        contabLancamento.IdPlano = cliente.IdPlano.Value;
                        contabLancamento.Origem = ContabilOrigem.Bolsa.LucroAcao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = resultado;
                    }
                    #endregion
                }
                else if (resultado < 0)
                {
                    #region Evento de PrejuizoAcao
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.Bolsa.PrejuizoAcao),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBolsa));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.Identificador = cdAtivoBolsa;
                        contabLancamento.IdPlano = cliente.IdPlano.Value;
                        contabLancamento.Origem = ContabilOrigem.Bolsa.PrejuizoAcao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = Math.Abs(resultado);
                    }
                    #endregion
                }
                
            }
            #endregion

            contabLancamentoCollection.Save();
                                                 
        }

        /// <summary>
        /// Eventos de ValorizacaoAcao, ReversaoValorizacaoAcao, DesvalorizacaoAcao, ReversaoDesvalorizacaoAcao,
        /// ValorizacaoAcaoVendida, ReversaoValorizacaoAcaoVendida, DesvalorizacaoAcaoVendida, ReversaoDesvalorizacaoAcaoVendida
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <param name="dataProxima"></param>
        private void GeraValorizacaoAcaoConsolidado(Cliente cliente, DateTime data, bool historico, DateTime dataProxima, bool processaAjusteReversao)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            int? idContaResultadoAcumulado = null;
            string contaResultadoAcumulado = "";
            #region Busca conta de resultado acumulado, para jogar os débitos e créditos das reversões (válido apenas para o 1o dia após a implentação dos saldos)
            if (processaAjusteReversao)
            {
                ContabContaCollection contabContaCollection = new ContabContaCollection();
                contabContaCollection.Query.Select(contabContaCollection.Query.IdConta,
                                                   contabContaCollection.Query.Codigo);
                contabContaCollection.Query.Where(contabContaCollection.Query.IdPlano.Equal(idPlano),
                                                  contabContaCollection.Query.FuncaoConta.Equal((byte)FuncaoContaContabil.ResultadoAcumulado));
                contabContaCollection.Query.Load();

                if (contabContaCollection.Count > 0)
                {
                    foreach (ContabConta contabConta in contabContaCollection)
                    {
                        int idConta = contabConta.IdConta.Value;

                        //Checa para garantir que a conta de resultado NÃO é conta mãe
                        ContabContaCollection contabContaMaeCollection = new ContabContaCollection();
                        contabContaMaeCollection.Query.Select(contabContaMaeCollection.Query.IdConta);
                        contabContaMaeCollection.Query.Where(contabContaMaeCollection.Query.IdContaMae.Equal(idConta));
                        contabContaMaeCollection.Query.Load();

                        if (contabContaMaeCollection.Count == 0)
                        {
                            idContaResultadoAcumulado = contabConta.IdConta.Value;
                            contaResultadoAcumulado = contabConta.Codigo;
                        }
                    }
                }
            }
            #endregion
            
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            if (historico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoBolsaHistoricoCollection.Query.ValorCustoLiquido.Sum(),
                                                             posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.Quantidade.GreaterThan(0),
                                                            posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaCollection.Query.ValorCustoLiquido.Sum(),
                                                    posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.Quantidade.GreaterThan(0),
                                                   posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();
            }

            decimal totalValorizacao = 0;
            decimal totalDesvalorizacao = 0;
            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;
                decimal valorCustoLiquido = posicaoBolsa.ValorCustoLiquido.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                decimal valorizacao = Utilitario.Truncate(valorMercado - valorCustoLiquido, 2);

                if (valorizacao > 0)
                {
                    totalValorizacao += valorizacao;
                }
                else
                {
                    totalDesvalorizacao += valorizacao;
                }
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoAcao
                origem = ContabilOrigem.Bolsa.ValorizacaoAcao;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoAcao
                origem = ContabilOrigem.Bolsa.ReversaoValorizacaoAcao;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaDebito = null;
                    string contaDebito = "";
                    if (processaAjusteReversao)
                    {
                        idContaDebito = idContaResultadoAcumulado;
                        contaDebito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    }

                    if (idContaDebito != null)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = dataProxima;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = totalValorizacao;
                    }
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoAcao
                origem = ContabilOrigem.Bolsa.DesvalorizacaoAcao;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoAcao
                origem = ContabilOrigem.Bolsa.ReversaoDesvalorizacaoAcao;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaCredito = null;
                    string contaCredito = "";
                    if (processaAjusteReversao)
                    {                        
                        idContaCredito = idContaResultadoAcumulado;
                        contaCredito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    }

                    if (idContaCredito != null)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.DataLancamento = dataProxima;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = totalDesvalorizacao;
                    }
                }
                #endregion
            }

            posicaoBolsaCollection = new PosicaoBolsaCollection();

            if (historico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaHistoricoCollection.Query.ValorCustoLiquido.Sum(),
                                                    posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.Quantidade.LessThan(0),
                                                            posicaoBolsaHistoricoCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaCollection.Query.ValorCustoLiquido.Sum(),
                                                    posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.Quantidade.LessThan(0),
                                                   posicaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();
            }

            totalValorizacao = 0;
            totalDesvalorizacao = 0;
            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;

                decimal valorCustoLiquido = posicaoBolsa.ValorCustoLiquido.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                decimal valorizacao = Utilitario.Truncate(valorMercado - valorCustoLiquido, 2);

                if (valorizacao > 0)
                {
                    totalValorizacao += valorizacao;
                }
                else
                {
                    totalDesvalorizacao += valorizacao;
                }
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoAcaoVendida
                origem = ContabilOrigem.Bolsa.ValorizacaoAcaoVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];
                    
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoAcaoVendida
                origem = ContabilOrigem.Bolsa.ReversaoValorizacaoAcaoVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaDebito = null;
                    string contaDebito = "";
                    if (processaAjusteReversao)
                    {
                        idContaDebito = idContaResultadoAcumulado;
                        contaDebito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoAcaoVendida
                origem = ContabilOrigem.Bolsa.DesvalorizacaoAcaoVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoAcaoVendida
                origem = ContabilOrigem.Bolsa.ReversaoDesvalorizacaoAcaoVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaCredito = null;
                    string contaCredito = "";
                    if (processaAjusteReversao)
                    {
                        idContaCredito = idContaResultadoAcumulado;
                        contaCredito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de ValorizacaoOPCComprada, ReversaoValorizacaoOPCComprada, DesvalorizacaoOPCComprada, ReversaoDesvalorizacaoOPCComprada,
        /// ValorizacaoOPCVendida, ReversaoValorizacaoOPCVendida, DesvalorizacaoOPCVendida, ReversaoDesvalorizacaoOPCVendida
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <param name="dataProxima"></param>
        private void GeraValorizacaoOPCConsolidado(Cliente cliente, DateTime data, bool historico, DateTime dataProxima, bool processaAjusteReversao)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            int? idContaResultadoAcumulado = null;
            string contaResultadoAcumulado = "";
            #region Busca conta de resultado acumulado, para jogar os débitos e créditos das reversões (válido apenas para o 1o dia após a implentação dos saldos)
            if (processaAjusteReversao)
            {
                ContabContaCollection contabContaCollection = new ContabContaCollection();
                contabContaCollection.Query.Select(contabContaCollection.Query.IdConta,
                                                   contabContaCollection.Query.Codigo);
                contabContaCollection.Query.Where(contabContaCollection.Query.IdPlano.Equal(idPlano),
                                                  contabContaCollection.Query.FuncaoConta.Equal((byte)FuncaoContaContabil.ResultadoAcumulado));
                contabContaCollection.Query.Load();

                if (contabContaCollection.Count > 0)
                {
                    foreach (ContabConta contabConta in contabContaCollection)
                    {
                        int idConta = contabConta.IdConta.Value;

                        //Checa para garantir que a conta de resultado NÃO é conta mãe
                        ContabContaCollection contabContaMaeCollection = new ContabContaCollection();
                        contabContaMaeCollection.Query.Select(contabContaMaeCollection.Query.IdConta);
                        contabContaMaeCollection.Query.Where(contabContaMaeCollection.Query.IdContaMae.Equal(idConta));
                        contabContaMaeCollection.Query.Load();

                        if (contabContaMaeCollection.Count == 0)
                        {
                            idContaResultadoAcumulado = contabConta.IdConta.Value;
                            contaResultadoAcumulado = contabConta.Codigo;
                        }
                    }
                }
            }
            #endregion

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            if (historico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoBolsaHistoricoCollection.Query.ValorCustoLiquido.Sum(),
                                                             posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.Quantidade.GreaterThan(0),
                                                            posicaoBolsaHistoricoCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaCollection.Query.ValorCustoLiquido.Sum(),
                                                    posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.Quantidade.GreaterThan(0),
                                                   posicaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();
            }

            decimal totalValorizacao = 0;
            decimal totalDesvalorizacao = 0;
            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;

                decimal valorCustoLiquido = posicaoBolsa.ValorCustoLiquido.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                decimal valorizacao = Utilitario.Truncate(valorMercado - valorCustoLiquido, 2);

                if (valorizacao > 0)
                {
                    totalValorizacao += valorizacao;
                }
                else
                {
                    totalDesvalorizacao += valorizacao;
                }
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoOPCComprada
                origem = ContabilOrigem.Bolsa.ValorizacaoOPCComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoOPCComprada
                origem = ContabilOrigem.Bolsa.ReversaoValorizacaoOPCComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaDebito = null;
                    string contaDebito = "";
                    if (processaAjusteReversao)
                    {
                        idContaDebito = idContaResultadoAcumulado;
                        contaDebito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoOPCComprada
                origem = ContabilOrigem.Bolsa.DesvalorizacaoOPCComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoOPCComprada
                origem = ContabilOrigem.Bolsa.ReversaoDesvalorizacaoOPCComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaCredito = null;
                    string contaCredito = "";
                    if (processaAjusteReversao)
                    {
                        idContaCredito = idContaResultadoAcumulado;
                        contaCredito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion
            }

            posicaoBolsaCollection = new PosicaoBolsaCollection();

            if (historico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoBolsaHistoricoCollection.Query.ValorCustoLiquido.Sum(),
                                                             posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.Quantidade.LessThan(0),
                                                            posicaoBolsaHistoricoCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaCollection.Query.ValorCustoLiquido.Sum(),
                                                    posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.Quantidade.LessThan(0),
                                                   posicaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();
            }

            totalValorizacao = 0;
            totalDesvalorizacao = 0;
            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;

                decimal valorCustoLiquido = posicaoBolsa.ValorCustoLiquido.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                decimal valorizacao = Utilitario.Truncate(valorMercado - valorCustoLiquido, 2);

                if (valorizacao > 0)
                {
                    totalValorizacao += valorizacao;
                }
                else
                {
                    totalDesvalorizacao += valorizacao;
                }
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoOPCVendida
                origem = ContabilOrigem.Bolsa.ValorizacaoOPCVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoOPCVendida
                origem = ContabilOrigem.Bolsa.ReversaoValorizacaoOPCVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaDebito = null;
                    string contaDebito = "";
                    if (processaAjusteReversao)
                    {
                        idContaDebito = idContaResultadoAcumulado;
                        contaDebito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoOPCVendida
                origem = ContabilOrigem.Bolsa.DesvalorizacaoOPCVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoOPCVendida
                origem = ContabilOrigem.Bolsa.ReversaoDesvalorizacaoOPCVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaCredito = null;
                    string contaCredito = "";
                    if (processaAjusteReversao)
                    {
                        idContaCredito = idContaResultadoAcumulado;
                        contaCredito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de ValorizacaoOPVComprada, ReversaoValorizacaoOPVComprada, DesvalorizacaoOPVComprada, ReversaoDesvalorizacaoOPVComprada,
        /// ValorizacaoOPVVendida, ReversaoValorizacaoOPVVendida, DesvalorizacaoOPVVendida, ReversaoDesvalorizacaoOPVVendida
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <param name="dataProxima"></param>
        private void GeraValorizacaoOPVConsolidado(Cliente cliente, DateTime data, bool historico, DateTime dataProxima, bool processaAjusteReversao)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            int? idContaResultadoAcumulado = null;
            string contaResultadoAcumulado = "";
            #region Busca conta de resultado acumulado, para jogar os débitos e créditos das reversões (válido apenas para o 1o dia após a implentação dos saldos)
            if (processaAjusteReversao)
            {
                ContabContaCollection contabContaCollection = new ContabContaCollection();
                contabContaCollection.Query.Select(contabContaCollection.Query.IdConta,
                                                   contabContaCollection.Query.Codigo);
                contabContaCollection.Query.Where(contabContaCollection.Query.IdPlano.Equal(idPlano),
                                                  contabContaCollection.Query.FuncaoConta.Equal((byte)FuncaoContaContabil.ResultadoAcumulado));
                contabContaCollection.Query.Load();

                if (contabContaCollection.Count > 0)
                {
                    foreach (ContabConta contabConta in contabContaCollection)
                    {
                        int idConta = contabConta.IdConta.Value;

                        //Checa para garantir que a conta de resultado NÃO é conta mãe
                        ContabContaCollection contabContaMaeCollection = new ContabContaCollection();
                        contabContaMaeCollection.Query.Select(contabContaMaeCollection.Query.IdConta);
                        contabContaMaeCollection.Query.Where(contabContaMaeCollection.Query.IdContaMae.Equal(idConta));
                        contabContaMaeCollection.Query.Load();

                        if (contabContaMaeCollection.Count == 0)
                        {
                            idContaResultadoAcumulado = contabConta.IdConta.Value;
                            contaResultadoAcumulado = contabConta.Codigo;
                        }
                    }
                }
            }
            #endregion

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();

            if (historico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoBolsaHistoricoCollection.Query.ValorCustoLiquido.Sum(),
                                                             posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.Quantidade.GreaterThan(0),
                                                            posicaoBolsaHistoricoCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaCollection.Query.ValorCustoLiquido.Sum(),
                                                    posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.Quantidade.GreaterThan(0),
                                                   posicaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();
            }

            decimal totalValorizacao = 0;
            decimal totalDesvalorizacao = 0;
            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;

                decimal valorCustoLiquido = posicaoBolsa.ValorCustoLiquido.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                decimal valorizacao = Utilitario.Truncate(valorMercado - valorCustoLiquido, 2);

                if (valorizacao > 0)
                {
                    totalValorizacao += valorizacao;
                }
                else
                {
                    totalDesvalorizacao += valorizacao;
                }
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoOPVComprada
                origem = ContabilOrigem.Bolsa.ValorizacaoOPVComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoOPVComprada
                origem = ContabilOrigem.Bolsa.ReversaoValorizacaoOPVComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaDebito = null;
                    string contaDebito = "";
                    if (processaAjusteReversao)
                    {
                        idContaDebito = idContaResultadoAcumulado;
                        contaDebito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoOPVComprada
                origem = ContabilOrigem.Bolsa.DesvalorizacaoOPVComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoOPVComprada
                origem = ContabilOrigem.Bolsa.ReversaoDesvalorizacaoOPVComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaCredito = null;
                    string contaCredito = "";
                    if (processaAjusteReversao)
                    {
                        idContaCredito = idContaResultadoAcumulado;
                        contaCredito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion
            }

            posicaoBolsaCollection = new PosicaoBolsaCollection();

            if (historico)
            {
                PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                posicaoBolsaHistoricoCollection.Query.Select(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                             posicaoBolsaHistoricoCollection.Query.ValorCustoLiquido.Sum(),
                                                             posicaoBolsaHistoricoCollection.Query.ValorMercado.Sum());
                posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                            posicaoBolsaHistoricoCollection.Query.Quantidade.LessThan(0),
                                                            posicaoBolsaHistoricoCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda),
                                                            posicaoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoBolsaHistoricoCollection.Query.GroupBy(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa);
                posicaoBolsaHistoricoCollection.Query.Load();

                PosicaoBolsaCollection posicaoBolsaCollectionAux = new PosicaoBolsaCollection(posicaoBolsaHistoricoCollection);
                posicaoBolsaCollection.Combine(posicaoBolsaCollectionAux);
            }
            else
            {
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.CdAtivoBolsa,
                                                    posicaoBolsaCollection.Query.ValorCustoLiquido.Sum(),
                                                    posicaoBolsaCollection.Query.ValorMercado.Sum());
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoBolsaCollection.Query.Quantidade.LessThan(0),
                                                   posicaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda));
                posicaoBolsaCollection.Query.GroupBy(posicaoBolsaCollection.Query.CdAtivoBolsa);
                posicaoBolsaCollection.Query.Load();
            }

            totalValorizacao = 0;
            totalDesvalorizacao = 0;
            foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoBolsa.CdAtivoBolsa;

                decimal valorCustoLiquido = posicaoBolsa.ValorCustoLiquido.Value;
                decimal valorMercado = posicaoBolsa.ValorMercado.Value;

                decimal valorizacao = Utilitario.Truncate(valorMercado - valorCustoLiquido, 2);

                if (valorizacao > 0)
                {
                    totalValorizacao += valorizacao;
                }
                else
                {
                    totalDesvalorizacao += valorizacao;
                }
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoOPVVendida
                origem = ContabilOrigem.Bolsa.ValorizacaoOPVVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoOPVVendida
                origem = ContabilOrigem.Bolsa.ReversaoValorizacaoOPVVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaDebito = null;
                    string contaDebito = "";
                    if (processaAjusteReversao)
                    {
                        idContaDebito = idContaResultadoAcumulado;
                        contaDebito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoOPVVendida
                origem = ContabilOrigem.Bolsa.DesvalorizacaoOPVVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoOPVVendida
                origem = ContabilOrigem.Bolsa.ReversaoDesvalorizacaoOPVVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    int? idContaCredito = null;
                    string contaCredito = "";
                    if (processaAjusteReversao)
                    {
                        idContaCredito = idContaResultadoAcumulado;
                        contaCredito = contaResultadoAcumulado;
                    }
                    else
                    {
                        idContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de ExercicioOPCComprada, ExercicioOPCVendida, ExercicioOPVComprada, ExercicioOPVVendida.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraOperacaoExercicioOpcao(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"
            
            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.Quantidade.Sum(),
                                                 operacaoBolsaCollection.Query.CdAtivoBolsaOpcao,
                                                 operacaoBolsaCollection.Query.TipoOperacao,
                                                 operacaoBolsaCollection.Query.Origem,
                                                 operacaoBolsaCollection.Query.IdAgenteCorretora);
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.Origem.In((byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra, (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda),
                                                operacaoBolsaCollection.Query.CdAtivoBolsaOpcao.IsNotNull());
            operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.CdAtivoBolsaOpcao,
                                                  operacaoBolsaCollection.Query.TipoOperacao,
                                                  operacaoBolsaCollection.Query.Origem,
                                                  operacaoBolsaCollection.Query.IdAgenteCorretora);
            operacaoBolsaCollection.Query.Load();

            decimal valorExercicioOPCComprada = 0;
            decimal valorExercicioOPCVendida = 0;
            decimal valorExercicioOPVComprada = 0;
            decimal valorExercicioOPVVendida = 0;
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                decimal quantidade = operacaoBolsa.Quantidade.Value;
                string cdAtivoOpcao = operacaoBolsa.CdAtivoBolsaOpcao;
                string tipoOperacao = operacaoBolsa.TipoOperacao;
                origem = operacaoBolsa.Origem.Value;
                int idAgente = operacaoBolsa.IdAgenteCorretora.Value;

                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoOpcao, data);

                PosicaoBolsaAbertura posicaoBolsaAbertura = new PosicaoBolsaAbertura();
                posicaoBolsaAbertura.Query.Select(posicaoBolsaAbertura.Query.PUCustoLiquido.Sum());
                posicaoBolsaAbertura.Query.Where(posicaoBolsaAbertura.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsaAbertura.Query.DataHistorico.Equal(data),
                                                 posicaoBolsaAbertura.Query.CdAtivoBolsa.Equal(cdAtivoOpcao),
                                                 posicaoBolsaAbertura.Query.IdAgente.Equal(idAgente));
                posicaoBolsaAbertura.Query.Load();

                decimal puCustoLiquido = posicaoBolsaAbertura.PUCustoLiquido.Value;

                decimal valorCusto = Math.Round(quantidade * puCustoLiquido / fatorCotacaoBolsa.Fator.Value, 2);

                if (tipoOperacao == TipoOperacaoBolsa.Compra || tipoOperacao == TipoOperacaoBolsa.CompraDaytrade)
                {
                    if (origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra)
                    {
                        valorExercicioOPCComprada += valorCusto;
                    }
                    else
                    {
                        valorExercicioOPVComprada += valorCusto;
                    }
                }
                else if (tipoOperacao == TipoOperacaoBolsa.Venda || tipoOperacao == TipoOperacaoBolsa.VendaDaytrade)
                {
                    if (origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra)
                    {
                        valorExercicioOPCVendida += valorCusto;
                    }
                    else
                    {
                        valorExercicioOPVVendida += valorCusto;
                    }
                }

            }


            #region Evento de ExercicioOPCComprada
            if (valorExercicioOPCComprada > 0)
            {
                origem = ContabilOrigem.Bolsa.ExercicioOPCComprada ;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorExercicioOPCComprada;
                }
            }
            #endregion

            #region Evento de ExercicioOPVComprada
            if (valorExercicioOPVComprada > 0)
            {
                origem = ContabilOrigem.Bolsa.ExercicioOPVComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorExercicioOPVComprada;
                }
            }
            #endregion

            #region Evento de ExercicioOPCVendida
            if (valorExercicioOPCVendida > 0)
            {
                origem = ContabilOrigem.Bolsa.ExercicioOPCVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorExercicioOPCVendida;
                }
            }
            #endregion

            #region Evento de ExercicioOPVVendida
            if (valorExercicioOPVVendida > 0)
            {
                origem = ContabilOrigem.Bolsa.ExercicioOPVVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorExercicioOPVVendida;
                }
            }
            #endregion
            

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de CompraOPC, CompraOPCCorretagem, CompraOPCEmolumento, CompraOPCTaxaCBLC, 
        /// VendaOPC, VendaOPCCorretagem, VendaOPCEmolumento, VendaOPCTaxaCBLC, LucroOPC, PrejuizoOPC.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraOperacaoOPCConsolidado(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            decimal valorNet = 0;
            decimal totalPagar = 0;
            decimal totalReceber = 0;

            decimal quantidadePosicao = 0;

            #region Tratamento de Compra de OPC
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.Valor,
                                                 operacaoBolsaCollection.Query.Corretagem,
                                                 operacaoBolsaCollection.Query.Emolumento,
                                                 operacaoBolsaCollection.Query.LiquidacaoCBLC,
                                                 operacaoBolsaCollection.Query.RegistroBolsa,
                                                 operacaoBolsaCollection.Query.RegistroCBLC,
                                                 operacaoBolsaCollection.Query.Quantidade,
                                                 operacaoBolsaCollection.Query.CdAtivoBolsa,
                                                 operacaoBolsaCollection.Query.IdAgenteCorretora);
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),                                                
                                                operacaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra));

            if (cliente.ContabilDaytrade.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra));
            }
            else
            {
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
            }

            operacaoBolsaCollection.Query.OrderBy(operacaoBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                                  operacaoBolsaCollection.Query.IdOperacao.Ascending);
            operacaoBolsaCollection.Query.Load();

            decimal valorCompraAtivo = 0;
            decimal corretagemCompraAtivo = 0;
            decimal emolumentoCompraAtivo = 0;
            decimal taxasCBLCCompraAtivo = 0;
            decimal valorCompraReversao = 0;
            decimal corretagemCompraReversao = 0;
            decimal emolumentoCompraReversao = 0;
            decimal taxasCBLCCompraReversao = 0;

            string cdAtivoBolsa = "";
            int idAgente = 0;
            if (operacaoBolsaCollection.Count > 0)
            {
                cdAtivoBolsa = operacaoBolsaCollection[0].CdAtivoBolsa;
                idAgente = operacaoBolsaCollection[0].IdAgenteCorretora.Value;
            }

            int i = 0;
            while (i < operacaoBolsaCollection.Count)
            {
			    string cdAtivoBolsaAnterior = cdAtivoBolsa;
                int idAgenteAnterior = idAgente;

                quantidadePosicao = 0;
                PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
                posicaoBolsaAberturaCollection.Query.Select(posicaoBolsaAberturaCollection.Query.Quantidade);
                posicaoBolsaAberturaCollection.Query.Where(posicaoBolsaAberturaCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoBolsaAberturaCollection.Query.IdAgente.Equal(idAgenteAnterior),
                                                           posicaoBolsaAberturaCollection.Query.DataHistorico.Equal(data),
                                                           posicaoBolsaAberturaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsaAnterior));
                posicaoBolsaAberturaCollection.Query.Load();

                quantidadePosicao = posicaoBolsaAberturaCollection.Count > 0 ? posicaoBolsaAberturaCollection[0].Quantidade.Value : 0;

                while (cdAtivoBolsaAnterior == cdAtivoBolsa && idAgenteAnterior == idAgente && i < operacaoBolsaCollection.Count)
                {
                    decimal quantidade = operacaoBolsaCollection[i].Quantidade.Value;
                    decimal valorCompra = operacaoBolsaCollection[i].Valor.Value;
                    decimal corretagemCompra = operacaoBolsaCollection[i].Corretagem.Value;
                    decimal emolumentoCompra = operacaoBolsaCollection[i].Emolumento.Value + operacaoBolsaCollection[i].RegistroBolsa.Value;
                    decimal taxasCBLCCompra = operacaoBolsaCollection[i].LiquidacaoCBLC.Value + operacaoBolsaCollection[i].RegistroCBLC.Value;
                    
                    if (quantidadePosicao >= 0)
                    {
                        valorCompraAtivo += valorCompra;
                        corretagemCompraAtivo += corretagemCompra;
                        emolumentoCompraAtivo += emolumentoCompra;
                        taxasCBLCCompraAtivo += taxasCBLCCompra;

                        quantidadePosicao += quantidade;
                    }
                    else
                    {
                        if (Math.Abs(quantidadePosicao) >= quantidade)
                        {
                            valorCompraReversao += valorCompra;
                            corretagemCompraReversao += corretagemCompra;
                            emolumentoCompraReversao += emolumentoCompra;
                            taxasCBLCCompraReversao += taxasCBLCCompra;

                            quantidadePosicao += quantidade;
                        }
                        else
                        {
                            decimal valorReversao = Utilitario.Truncate(valorCompra * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorCorretagemReversao = Utilitario.Truncate(corretagemCompra * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorEmolumentoReversao = Utilitario.Truncate(corretagemCompra * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorTaxasCBLCReversao = Utilitario.Truncate(corretagemCompra * (Math.Abs(quantidadePosicao) / quantidade), 2);

                            valorCompraReversao += valorReversao;
                            corretagemCompraReversao += valorCorretagemReversao;
                            emolumentoCompraReversao += valorEmolumentoReversao;
                            taxasCBLCCompraReversao += valorTaxasCBLCReversao;

                            if (valorCompra > valorReversao)
                            {
                                valorCompraAtivo += (valorCompra - valorReversao);
                                corretagemCompraAtivo += (corretagemCompra - valorCorretagemReversao);
                                emolumentoCompraAtivo += (emolumentoCompra - valorEmolumentoReversao);
                                taxasCBLCCompraAtivo += (taxasCBLCCompra - valorTaxasCBLCReversao);
                            }

                            quantidadePosicao = 0;
                        }

                    }

                    totalPagar += valorCompra + corretagemCompra + emolumentoCompra + taxasCBLCCompra;
                    
                    i++;

                    if (i < operacaoBolsaCollection.Count)
                    {
                        cdAtivoBolsa = operacaoBolsaCollection[i].CdAtivoBolsa;
                        idAgente = operacaoBolsaCollection[i].IdAgenteCorretora.Value;
                    }
                }
            }
            #endregion

            #region Tratamento de Venda de OPC
            operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.Valor,
                                                 operacaoBolsaCollection.Query.Corretagem,
                                                 operacaoBolsaCollection.Query.Emolumento,
                                                 operacaoBolsaCollection.Query.LiquidacaoCBLC,
                                                 operacaoBolsaCollection.Query.RegistroBolsa,
                                                 operacaoBolsaCollection.Query.RegistroCBLC,
                                                 operacaoBolsaCollection.Query.Quantidade,
                                                 operacaoBolsaCollection.Query.CdAtivoBolsa,
                                                 operacaoBolsaCollection.Query.IdAgenteCorretora);
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),                                                
                                                operacaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra));

            if (cliente.ContabilDaytrade.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda));
            }
            else
            {
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
            }

            operacaoBolsaCollection.Query.OrderBy(operacaoBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                                  operacaoBolsaCollection.Query.IdOperacao.Ascending);
            operacaoBolsaCollection.Query.Load();

            decimal valorVendaPassivo = 0;
            decimal corretagemVendaPassivo = 0;
            decimal emolumentoVendaPassivo = 0;
            decimal taxasCBLCVendaPassivo = 0;
            decimal valorVendaReversao = 0;
            decimal corretagemVendaReversao = 0;
            decimal emolumentoVendaReversao = 0;
            decimal taxasCBLCVendaReversao = 0;

            cdAtivoBolsa = "";
            idAgente = 0;
            if (operacaoBolsaCollection.Count > 0)
            {
                cdAtivoBolsa = operacaoBolsaCollection[0].CdAtivoBolsa;
                idAgente = operacaoBolsaCollection[0].IdAgenteCorretora.Value;
            }

            i = 0;
            while (i < operacaoBolsaCollection.Count)
            {
                string cdAtivoBolsaAnterior = cdAtivoBolsa;
                int idAgenteAnterior = idAgente;

                quantidadePosicao = 0;
                PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
                posicaoBolsaAberturaCollection.Query.Select(posicaoBolsaAberturaCollection.Query.Quantidade);
                posicaoBolsaAberturaCollection.Query.Where(posicaoBolsaAberturaCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoBolsaAberturaCollection.Query.IdAgente.Equal(idAgenteAnterior),
                                                           posicaoBolsaAberturaCollection.Query.DataHistorico.Equal(data),
                                                           posicaoBolsaAberturaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsaAnterior));
                posicaoBolsaAberturaCollection.Query.Load();

                quantidadePosicao = posicaoBolsaAberturaCollection.Count > 0 ? posicaoBolsaAberturaCollection[0].Quantidade.Value : 0;

                while (cdAtivoBolsaAnterior == cdAtivoBolsa && idAgenteAnterior == idAgente && i < operacaoBolsaCollection.Count)
                {
                    decimal quantidade = operacaoBolsaCollection[i].Quantidade.Value;
                    decimal valorVenda = operacaoBolsaCollection[i].Valor.Value;
                    decimal corretagemVenda = operacaoBolsaCollection[i].Corretagem.Value;
                    decimal emolumentoVenda = operacaoBolsaCollection[i].Emolumento.Value + operacaoBolsaCollection[i].RegistroBolsa.Value;
                    decimal taxasCBLCVenda = operacaoBolsaCollection[i].LiquidacaoCBLC.Value + operacaoBolsaCollection[i].RegistroCBLC.Value;

                    if (quantidadePosicao <= 0)
                    {
                        valorVendaPassivo += valorVenda;
                        corretagemVendaPassivo += corretagemVenda;
                        emolumentoVendaPassivo += emolumentoVenda;
                        taxasCBLCVendaPassivo += taxasCBLCVenda;

                        quantidadePosicao += (quantidade * -1);
                    }
                    else
                    {
                        if (Math.Abs(quantidadePosicao) >= quantidade)
                        {
                            valorVendaReversao += valorVenda;
                            corretagemVendaReversao += corretagemVenda;
                            emolumentoVendaReversao += emolumentoVenda;
                            taxasCBLCVendaReversao += taxasCBLCVenda;

                            quantidadePosicao -= quantidade;
                        }
                        else
                        {
                            decimal valorReversao = Utilitario.Truncate(valorVenda * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorCorretagemReversao = Utilitario.Truncate(corretagemVenda * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorEmolumentoReversao = Utilitario.Truncate(emolumentoVenda * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorTaxasCBLCReversao = Utilitario.Truncate(taxasCBLCVenda * (Math.Abs(quantidadePosicao) / quantidade), 2);

                            valorVendaReversao += valorReversao;
                            corretagemVendaReversao += valorCorretagemReversao;
                            emolumentoVendaReversao += valorEmolumentoReversao;
                            taxasCBLCVendaReversao += valorTaxasCBLCReversao;

                            if (valorVenda > valorReversao)
                            {
                                valorVendaPassivo += (valorVenda - valorReversao);
                                corretagemVendaPassivo += (corretagemVenda - valorCorretagemReversao);
                                emolumentoVendaPassivo += (emolumentoVenda - valorEmolumentoReversao);
                                taxasCBLCVendaPassivo += (taxasCBLCVenda - valorTaxasCBLCReversao);
                            }

                            quantidadePosicao = 0;
                        }

                    }

                    totalPagar += corretagemVenda + emolumentoVenda + taxasCBLCVenda;
                    totalReceber += valorVenda;
                    
                    i++;

                    if (i < operacaoBolsaCollection.Count)
                    {
                        cdAtivoBolsa = operacaoBolsaCollection[i].CdAtivoBolsa;
                        idAgente = operacaoBolsaCollection[i].IdAgenteCorretora.Value;
                    }
                }
            }
            #endregion


            valorNet = totalReceber - totalPagar;


            #region Ajusta contas crédito ou débito dos eventos de compra e venda, para jogar a conta de net a liquidar
            string contaCreditoReversao = "";
            string contaDebitoReversao = "";
            int idContaCreditoReversao = 0;
            int idContaDebitoReversao = 0;
            bool usaContaReversao = false;
            if (valorNet > 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                    contaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    idContaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito.Value;

                    usaContaReversao = true;
                }
            }
            else if (valorNet < 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                    contaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    idContaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito.Value;

                    usaContaReversao = true;
                }
            }
            #endregion


            #region Evento de CompraOPC (Ativo)
            if (valorCompraAtivo > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorCompraAtivo;
                }
            }
            #endregion

            #region Evento de CompraOPCCorretagem (Ativo)
            if (corretagemCompraAtivo > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraOPCCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemCompraAtivo;
                }
            }
            #endregion

            #region Evento de CompraOPCEmolumento (Ativo)
            if (emolumentoCompraAtivo > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraOPCEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoCompraAtivo;
                }
            }
            #endregion

            #region Evento de CompraOPCTaxaCBLC (Ativo)
            if (taxasCBLCCompraAtivo > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraOPCTaxaCBLC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = taxasCBLCCompraAtivo;
                }
            }
            #endregion

            ContabRoteiroCollection contabRoteiroCollectionReversao = new ContabRoteiroCollection();
            if (valorCompraReversao != 0)
            {
                //Busca a conta de reversão de posição                
                contabRoteiroCollectionReversao.Query.Where(contabRoteiroCollectionReversao.Query.Origem.Equal((int)ContabilOrigem.Bolsa.VendaOPC),
                                                            contabRoteiroCollectionReversao.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollectionReversao.Query.Load();                
            }
            if (contabRoteiroCollectionReversao.Count > 0)
            {
                #region Evento de CompraOPC (Reversão)
                if (valorCompraReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.CompraOPC;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        if (contabRoteiroCollectionReversao.Count > 0)
                        {
                            ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                            contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                            contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                            contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaCredito;
                            contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                            contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaCredito;
                            contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = origem;
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.Valor = valorCompraReversao;
                        }
                    }
                }
                #endregion

                #region Evento de CompraOPCCorretagem (Reversão)
                if (corretagemCompraReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.CompraOPCCorretagem;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaCredito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaCredito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = corretagemCompraReversao;
                    }
                }
                #endregion

                #region Evento de CompraOPCEmolumento (Reversão)
                if (emolumentoCompraReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.CompraOPCEmolumento;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaCredito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaCredito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = emolumentoCompraReversao;
                    }
                }
                #endregion

                #region Evento de CompraOPCTaxaCBLC (Reversão)
                if (taxasCBLCCompraReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.CompraOPCTaxaCBLC;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaCredito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaCredito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = taxasCBLCCompraReversao;
                    }
                }
                #endregion
            }

            #region Evento de VendaOPC (Passivo)
            if (valorVendaPassivo > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorVendaPassivo;
                }
            }
            #endregion

            #region Evento de VendaOPCCorretagem (Passivo)
            if (corretagemVendaPassivo > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaOPCCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemVendaPassivo;
                }
            }
            #endregion

            #region Evento VendaOPCEmolumento (Passivo)
            if (emolumentoVendaPassivo > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaOPCEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoVendaPassivo;
                }
            }
            #endregion

            #region Evento de VendaOPCTaxaCBLC (Passivo)
            if (taxasCBLCVendaPassivo > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaOPCTaxaCBLC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = taxasCBLCVendaPassivo;
                }
            }
            #endregion


            contabRoteiroCollectionReversao = new ContabRoteiroCollection();
            if (valorVendaReversao != 0)
            {
                //Busca a conta de reversão de posição                
                contabRoteiroCollectionReversao.Query.Where(contabRoteiroCollectionReversao.Query.Origem.Equal((int)ContabilOrigem.Bolsa.CompraOPC),
                                                            contabRoteiroCollectionReversao.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollectionReversao.Query.Load();
            }
            if (contabRoteiroCollectionReversao.Count > 0)
            {
                #region Evento de VendaOPC (Reversão)
                if (valorVendaReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.VendaOPC;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        if (contabRoteiroCollectionReversao.Count > 0)
                        {
                            ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                            contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                            contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                            contabLancamento.IdContaDebito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                            contabLancamento.IdContaCredito = contabRoteiroCollectionReversao[0].IdContaDebito;
                            contabLancamento.ContaDebito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                            contabLancamento.ContaCredito = contabRoteiroCollectionReversao[0].ContaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = origem;
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.Valor = valorVendaReversao;
                        }
                    }
                }
                #endregion

                #region Evento de VendaOPCCorretagem (Reversão)
                if (corretagemVendaReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.VendaOPCCorretagem;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = corretagemVendaReversao;
                    }
                }
                #endregion

                #region Evento de VendaOPCEmolumento (Reversão)
                if (emolumentoVendaReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.VendaOPCEmolumento;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = emolumentoVendaReversao;
                    }
                }
                #endregion

                #region Evento de VendaOPCTaxaCBLC (Reversão)
                if (taxasCBLCVendaReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.VendaOPCTaxaCBLC;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = taxasCBLCVendaReversao;
                    }
                }
                #endregion
            }


            if (cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                #region Evento de Liquidacao (Pagar/Receber)
                int idEventoLiquidacao = 0;
                if (valorNet > 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaReceber;
                    }
                }
                if (valorNet < 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaPagar;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorNet;
                }
                #endregion
            }
            else
            {
                #region Evento de Liquidacao (Receber)
                int idEventoLiquidacao = 0;
                if (totalReceber != 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaReceber;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalReceber;
                }
                #endregion

                #region Evento de Liquidacao (Pagar)
                idEventoLiquidacao = 0;
                if (totalPagar != 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaPagar;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalPagar;
                }
                #endregion
            }


            #region Eventos de Lucro e Prejuizo
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                                    operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra),
                                    operacaoBolsa.Query.ResultadoRealizado.GreaterThan(0));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value;

                #region Evento de LucroCompraOPC
                origem = ContabilOrigem.Bolsa.LucroCompraOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                                    operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra),
                                    operacaoBolsa.Query.ResultadoRealizado.LessThan(0));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value;

                #region Evento de PrejuizoCompraOPC
                origem = ContabilOrigem.Bolsa.PrejuizoCompraOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                                    operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda),
                                    operacaoBolsa.Query.ResultadoRealizado.GreaterThan(0));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value;

                #region Evento de LucroVendaOPC
                origem = ContabilOrigem.Bolsa.LucroVendaOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoCompra),
                                    operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda),
                                    operacaoBolsa.Query.ResultadoRealizado.LessThan(0));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value;

                #region Evento de PrejuizoVendaOPC
                origem = ContabilOrigem.Bolsa.PrejuizoVendaOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }
            #endregion

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de CompraOPV, CompraOPVCorretagem, CompraOPVEmolumento, CompraOPVTaxaCBLC, 
        /// VendaOPV, VendaOPVCorretagem, VendaOPVEmolumento, VendaOPVTaxaCBLC, LucroOPV, PrejuizoOPV.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraOperacaoOPVConsolidado(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            decimal valorNet = 0;
            decimal totalPagar = 0;
            decimal totalReceber = 0;

            decimal quantidadePosicao = 0;

            #region Tratamento de Compra de OPV
            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.Valor,
                                                 operacaoBolsaCollection.Query.Corretagem,
                                                 operacaoBolsaCollection.Query.Emolumento,
                                                 operacaoBolsaCollection.Query.LiquidacaoCBLC,
                                                 operacaoBolsaCollection.Query.RegistroBolsa,
                                                 operacaoBolsaCollection.Query.RegistroCBLC,
                                                 operacaoBolsaCollection.Query.Quantidade,
                                                 operacaoBolsaCollection.Query.CdAtivoBolsa,
                                                 operacaoBolsaCollection.Query.IdAgenteCorretora);
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda));

            if (cliente.ContabilDaytrade.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra));
            }
            else
            {
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
            }

            operacaoBolsaCollection.Query.OrderBy(operacaoBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                                  operacaoBolsaCollection.Query.IdOperacao.Ascending);
            operacaoBolsaCollection.Query.Load();

            decimal valorCompraAtivo = 0;
            decimal corretagemCompraAtivo = 0;
            decimal emolumentoCompraAtivo = 0;
            decimal taxasCBLCCompraAtivo = 0;
            decimal valorCompraReversao = 0;
            decimal corretagemCompraReversao = 0;
            decimal emolumentoCompraReversao = 0;
            decimal taxasCBLCCompraReversao = 0;

            string cdAtivoBolsa = "";
            int idAgente = 0;
            if (operacaoBolsaCollection.Count > 0)
            {
                cdAtivoBolsa = operacaoBolsaCollection[0].CdAtivoBolsa;
                idAgente = operacaoBolsaCollection[0].IdAgenteCorretora.Value;
            }

            int i = 0;
            while (i < operacaoBolsaCollection.Count)
            {
                string cdAtivoBolsaAnterior = cdAtivoBolsa;
                int idAgenteAnterior = idAgente;

                quantidadePosicao = 0;
                PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
                posicaoBolsaAberturaCollection.Query.Select(posicaoBolsaAberturaCollection.Query.Quantidade);
                posicaoBolsaAberturaCollection.Query.Where(posicaoBolsaAberturaCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoBolsaAberturaCollection.Query.IdAgente.Equal(idAgenteAnterior),
                                                           posicaoBolsaAberturaCollection.Query.DataHistorico.Equal(data),
                                                           posicaoBolsaAberturaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsaAnterior));
                posicaoBolsaAberturaCollection.Query.Load();

                quantidadePosicao = posicaoBolsaAberturaCollection.Count > 0 ? posicaoBolsaAberturaCollection[0].Quantidade.Value : 0;

                while (cdAtivoBolsaAnterior == cdAtivoBolsa && idAgenteAnterior == idAgente && i < operacaoBolsaCollection.Count)
                {
                    decimal quantidade = operacaoBolsaCollection[i].Quantidade.Value;
                    decimal valorCompra = operacaoBolsaCollection[i].Valor.Value;
                    decimal corretagemCompra = operacaoBolsaCollection[i].Corretagem.Value;
                    decimal emolumentoCompra = operacaoBolsaCollection[i].Emolumento.Value + operacaoBolsaCollection[i].RegistroBolsa.Value;
                    decimal taxasCBLCCompra = operacaoBolsaCollection[i].LiquidacaoCBLC.Value + operacaoBolsaCollection[i].RegistroCBLC.Value;

                    if (quantidadePosicao >= 0)
                    {
                        valorCompraAtivo += valorCompra;
                        corretagemCompraAtivo += corretagemCompra;
                        emolumentoCompraAtivo += emolumentoCompra;
                        taxasCBLCCompraAtivo += taxasCBLCCompra;

                        quantidadePosicao += quantidade;
                    }
                    else
                    {
                        if (Math.Abs(quantidadePosicao) >= quantidade)
                        {
                            valorCompraReversao += valorCompra;
                            corretagemCompraReversao += corretagemCompra;
                            emolumentoCompraReversao += emolumentoCompra;
                            taxasCBLCCompraReversao += taxasCBLCCompra;

                            quantidadePosicao += quantidade;
                        }
                        else
                        {
                            decimal valorReversao = Utilitario.Truncate(valorCompra * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorCorretagemReversao = Utilitario.Truncate(corretagemCompra * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorEmolumentoReversao = Utilitario.Truncate(corretagemCompra * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorTaxasCBLCReversao = Utilitario.Truncate(corretagemCompra * (Math.Abs(quantidadePosicao) / quantidade), 2);

                            valorCompraReversao += valorReversao;
                            corretagemCompraReversao += valorCorretagemReversao;
                            emolumentoCompraReversao += valorEmolumentoReversao;
                            taxasCBLCCompraReversao += valorTaxasCBLCReversao;

                            if (valorCompra > valorReversao)
                            {
                                valorCompraAtivo += (valorCompra - valorReversao);
                                corretagemCompraAtivo += (corretagemCompra - valorCorretagemReversao);
                                emolumentoCompraAtivo += (emolumentoCompra - valorEmolumentoReversao);
                                taxasCBLCCompraAtivo += (taxasCBLCCompra - valorTaxasCBLCReversao);
                            }

                            quantidadePosicao = 0;
                        }

                    }

                    totalPagar += valorCompra + corretagemCompra + emolumentoCompra + taxasCBLCCompra;

                    i++;

                    if (i < operacaoBolsaCollection.Count)
                    {
                        cdAtivoBolsa = operacaoBolsaCollection[i].CdAtivoBolsa;
                        idAgente = operacaoBolsaCollection[i].IdAgenteCorretora.Value;
                    }
                }
            }
            #endregion

            #region Tratamento de Venda de OPV
            operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.Valor,
                                                 operacaoBolsaCollection.Query.Corretagem,
                                                 operacaoBolsaCollection.Query.Emolumento,
                                                 operacaoBolsaCollection.Query.LiquidacaoCBLC,
                                                 operacaoBolsaCollection.Query.RegistroBolsa,
                                                 operacaoBolsaCollection.Query.RegistroCBLC,
                                                 operacaoBolsaCollection.Query.Quantidade,
                                                 operacaoBolsaCollection.Query.CdAtivoBolsa,
                                                 operacaoBolsaCollection.Query.IdAgenteCorretora);
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda));

            if (cliente.ContabilDaytrade.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda));
            }
            else
            {
                operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
            }

            operacaoBolsaCollection.Query.OrderBy(operacaoBolsaCollection.Query.CdAtivoBolsa.Ascending,
                                                  operacaoBolsaCollection.Query.IdOperacao.Ascending);
            operacaoBolsaCollection.Query.Load();

            decimal valorVendaPassivo = 0;
            decimal corretagemVendaPassivo = 0;
            decimal emolumentoVendaPassivo = 0;
            decimal taxasCBLCVendaPassivo = 0;
            decimal valorVendaReversao = 0;
            decimal corretagemVendaReversao = 0;
            decimal emolumentoVendaReversao = 0;
            decimal taxasCBLCVendaReversao = 0;

            cdAtivoBolsa = "";
            idAgente = 0;
            if (operacaoBolsaCollection.Count > 0)
            {
                cdAtivoBolsa = operacaoBolsaCollection[0].CdAtivoBolsa;
                idAgente = operacaoBolsaCollection[0].IdAgenteCorretora.Value;
            }

            i = 0;
            while (i < operacaoBolsaCollection.Count)
            {
                string cdAtivoBolsaAnterior = cdAtivoBolsa;
                int idAgenteAnterior = idAgente;

                quantidadePosicao = 0;
                PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
                posicaoBolsaAberturaCollection.Query.Select(posicaoBolsaAberturaCollection.Query.Quantidade);
                posicaoBolsaAberturaCollection.Query.Where(posicaoBolsaAberturaCollection.Query.IdCliente.Equal(idCliente),
                                                           posicaoBolsaAberturaCollection.Query.IdAgente.Equal(idAgenteAnterior),
                                                           posicaoBolsaAberturaCollection.Query.DataHistorico.Equal(data),
                                                           posicaoBolsaAberturaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsaAnterior));
                posicaoBolsaAberturaCollection.Query.Load();

                quantidadePosicao = posicaoBolsaAberturaCollection.Count > 0 ? posicaoBolsaAberturaCollection[0].Quantidade.Value : 0;

                while (cdAtivoBolsaAnterior == cdAtivoBolsa && idAgenteAnterior == idAgente && i < operacaoBolsaCollection.Count)
                {
                    decimal quantidade = operacaoBolsaCollection[i].Quantidade.Value;
                    decimal valorVenda = operacaoBolsaCollection[i].Valor.Value;
                    decimal corretagemVenda = operacaoBolsaCollection[i].Corretagem.Value;
                    decimal emolumentoVenda = operacaoBolsaCollection[i].Emolumento.Value + operacaoBolsaCollection[i].RegistroBolsa.Value;
                    decimal taxasCBLCVenda = operacaoBolsaCollection[i].LiquidacaoCBLC.Value + operacaoBolsaCollection[i].RegistroCBLC.Value;

                    if (quantidadePosicao <= 0)
                    {
                        valorVendaPassivo += valorVenda;
                        corretagemVendaPassivo += corretagemVenda;
                        emolumentoVendaPassivo += emolumentoVenda;
                        taxasCBLCVendaPassivo += taxasCBLCVenda;

                        quantidadePosicao += (quantidade * -1);
                    }
                    else
                    {
                        if (Math.Abs(quantidadePosicao) >= quantidade)
                        {
                            valorVendaReversao += valorVenda;
                            corretagemVendaReversao += corretagemVenda;
                            emolumentoVendaReversao += emolumentoVenda;
                            taxasCBLCVendaReversao += taxasCBLCVenda;

                            quantidadePosicao -= quantidade;
                        }
                        else
                        {
                            decimal valorReversao = Utilitario.Truncate(valorVenda * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorCorretagemReversao = Utilitario.Truncate(corretagemVenda * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorEmolumentoReversao = Utilitario.Truncate(emolumentoVenda * (Math.Abs(quantidadePosicao) / quantidade), 2);
                            decimal valorTaxasCBLCReversao = Utilitario.Truncate(taxasCBLCVenda * (Math.Abs(quantidadePosicao) / quantidade), 2);

                            valorVendaReversao += valorReversao;
                            corretagemVendaReversao += valorCorretagemReversao;
                            emolumentoVendaReversao += valorEmolumentoReversao;
                            taxasCBLCVendaReversao += valorTaxasCBLCReversao;

                            if (valorVenda > valorReversao)
                            {
                                valorVendaPassivo += (valorVenda - valorReversao);
                                corretagemVendaPassivo += (corretagemVenda - valorCorretagemReversao);
                                emolumentoVendaPassivo += (emolumentoVenda - valorEmolumentoReversao);
                                taxasCBLCVendaPassivo += (taxasCBLCVenda - valorTaxasCBLCReversao);
                            }

                            quantidadePosicao = 0;
                        }

                    }

                    totalPagar += corretagemVenda + emolumentoVenda + taxasCBLCVenda;
                    totalReceber += valorVenda;

                    i++;

                    if (i < operacaoBolsaCollection.Count)
                    {
                        cdAtivoBolsa = operacaoBolsaCollection[i].CdAtivoBolsa;
                        idAgente = operacaoBolsaCollection[i].IdAgenteCorretora.Value;
                    }
                }
            }
            #endregion


            valorNet = totalReceber - totalPagar;


            #region Ajusta contas crédito ou débito dos eventos de compra e venda, para jogar a conta de net a liquidar
            string contaCreditoReversao = "";
            string contaDebitoReversao = "";
            int idContaCreditoReversao = 0;
            int idContaDebitoReversao = 0;
            bool usaContaReversao = false;
            if (valorNet > 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                    contaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    idContaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito.Value;

                    usaContaReversao = true;
                }
            }
            else if (valorNet < 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                    contaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    idContaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito.Value;

                    usaContaReversao = true;
                }
            }
            #endregion


            #region Evento de CompraOPV (Ativo)
            if (valorCompraAtivo > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorCompraAtivo;
                }
            }
            #endregion

            #region Evento de CompraOPVCorretagem (Ativo)
            if (corretagemCompraAtivo > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraOPVCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemCompraAtivo;
                }
            }
            #endregion

            #region Evento de CompraOPVEmolumento (Ativo)
            if (emolumentoCompraAtivo > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraOPVEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoCompraAtivo;
                }
            }
            #endregion

            #region Evento de CompraOPVTaxaCBLC (Ativo)
            if (taxasCBLCCompraAtivo > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraOPVTaxaCBLC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = taxasCBLCCompraAtivo;
                }
            }
            #endregion

            ContabRoteiroCollection contabRoteiroCollectionReversao = new ContabRoteiroCollection();
            if (valorCompraReversao != 0)
            {
                //Busca a conta de reversão de posição                
                contabRoteiroCollectionReversao.Query.Where(contabRoteiroCollectionReversao.Query.Origem.Equal((int)ContabilOrigem.Bolsa.VendaOPV),
                                                            contabRoteiroCollectionReversao.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollectionReversao.Query.Load();
            }
            if (contabRoteiroCollectionReversao.Count > 0)
            {
                #region Evento de CompraOPV (Reversão)
                if (valorCompraReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.CompraOPV;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        if (contabRoteiroCollectionReversao.Count > 0)
                        {
                            ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                            contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                            contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                            contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaCredito;
                            contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                            contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaCredito;
                            contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = origem;
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.Valor = valorCompraReversao;
                        }
                    }
                }
                #endregion

                #region Evento de CompraOPVCorretagem (Reversão)
                if (corretagemCompraReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.CompraOPVCorretagem;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaCredito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaCredito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = corretagemCompraReversao;
                    }
                }
                #endregion

                #region Evento de CompraOPVEmolumento (Reversão)
                if (emolumentoCompraReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.CompraOPVEmolumento;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaCredito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaCredito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = emolumentoCompraReversao;
                    }
                }
                #endregion

                #region Evento de CompraOPVTaxaCBLC (Reversão)
                if (taxasCBLCCompraReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.CompraOPVTaxaCBLC;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaCredito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaCredito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = taxasCBLCCompraReversao;
                    }
                }
                #endregion
            }

            #region Evento de VendaOPV (Passivo)
            if (valorVendaPassivo > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorVendaPassivo;
                }
            }
            #endregion

            #region Evento de VendaOPVCorretagem (Passivo)
            if (corretagemVendaPassivo > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaOPVCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemVendaPassivo;
                }
            }
            #endregion

            #region Evento VendaOPVEmolumento (Passivo)
            if (emolumentoVendaPassivo > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaOPVEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoVendaPassivo;
                }
            }
            #endregion

            #region Evento de VendaOPVTaxaCBLC (Passivo)
            if (taxasCBLCVendaPassivo > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaOPVTaxaCBLC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = taxasCBLCVendaPassivo;
                }
            }
            #endregion


            contabRoteiroCollectionReversao = new ContabRoteiroCollection();
            if (valorVendaReversao != 0)
            {
                //Busca a conta de reversão de posição                
                contabRoteiroCollectionReversao.Query.Where(contabRoteiroCollectionReversao.Query.Origem.Equal((int)ContabilOrigem.Bolsa.CompraOPV),
                                                            contabRoteiroCollectionReversao.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollectionReversao.Query.Load();
            }
            if (contabRoteiroCollectionReversao.Count > 0)
            {
                #region Evento de VendaOPV (Reversão)
                if (valorVendaReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.VendaOPV;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        if (contabRoteiroCollectionReversao.Count > 0)
                        {
                            ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                            contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                            contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                            contabLancamento.IdContaDebito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                            contabLancamento.IdContaCredito = contabRoteiroCollectionReversao[0].IdContaDebito;
                            contabLancamento.ContaDebito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                            contabLancamento.ContaCredito = contabRoteiroCollectionReversao[0].ContaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = origem;
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.Valor = valorVendaReversao;
                        }
                    }
                }
                #endregion

                #region Evento de VendaOPVCorretagem (Reversão)
                if (corretagemVendaReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.VendaOPVCorretagem;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = corretagemVendaReversao;
                    }
                }
                #endregion

                #region Evento de VendaOPVEmolumento (Reversão)
                if (emolumentoVendaReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.VendaOPVEmolumento;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = emolumentoVendaReversao;
                    }
                }
                #endregion

                #region Evento de VendaOPVTaxaCBLC (Reversão)
                if (taxasCBLCVendaReversao > 0)
                {
                    origem = ContabilOrigem.Bolsa.VendaOPVTaxaCBLC;
                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollectionReversao[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollectionReversao[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = taxasCBLCVendaReversao;
                    }
                }
                #endregion
            }


            if (cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                #region Evento de Liquidacao (Pagar/Receber)
                int idEventoLiquidacao = 0;
                if (valorNet > 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaReceber;
                    }
                }
                if (valorNet < 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaPagar;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorNet;
                }
                #endregion
            }
            else
            {
                #region Evento de Liquidacao (Receber)
                int idEventoLiquidacao = 0;
                if (totalReceber != 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaReceber;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalReceber;
                }
                #endregion

                #region Evento de Liquidacao (Pagar)
                idEventoLiquidacao = 0;
                if (totalPagar != 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaPagar;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalPagar;
                }
                #endregion
            }


            #region Eventos de Lucro e Prejuizo
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda),
                                    operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra),
                                    operacaoBolsa.Query.ResultadoRealizado.GreaterThan(0));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value;

                #region Evento de LucroCompraOPV
                origem = ContabilOrigem.Bolsa.LucroCompraOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda),
                                    operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra),
                                    operacaoBolsa.Query.ResultadoRealizado.LessThan(0));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value;

                #region Evento de PrejuizoCompraOPV
                origem = ContabilOrigem.Bolsa.PrejuizoCompraOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda),
                                    operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda),
                                    operacaoBolsa.Query.ResultadoRealizado.GreaterThan(0));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value;

                #region Evento de LucroVendaOPV
                origem = ContabilOrigem.Bolsa.LucroVendaOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.Equal(TipoMercadoBolsa.OpcaoVenda),
                                    operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda),
                                    operacaoBolsa.Query.ResultadoRealizado.LessThan(0));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value;

                #region Evento de PrejuizoVendaOPV
                origem = ContabilOrigem.Bolsa.PrejuizoVendaOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }
            #endregion

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de CompraAcao, CompraAcaoCorretagem, CompraAcaoEmolumento, CompraAcaoTaxaCBLC, 
        /// VendaAcao, VendaAcaoCorretagem, VendaAcaoEmolumento, VendaAcaoTaxaCBLC.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="liquidaD0"></param>
        private void GeraOperacaoAcaoConsolidado(Cliente cliente, DateTime data, bool liquidaD0)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            decimal valorNet = 0;
            decimal totalReceber = 0;
            decimal totalPagar = 0;
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.Valor.Sum(),
                                     operacaoBolsa.Query.Corretagem.Sum(),
                                     operacaoBolsa.Query.Emolumento.Sum(),
                                     operacaoBolsa.Query.LiquidacaoCBLC.Sum(),
                                     operacaoBolsa.Query.RegistroBolsa.Sum(),
                                     operacaoBolsa.Query.RegistroCBLC.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));

            if (cliente.ContabilDaytrade.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBolsa.Query.Where(operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Compra));
            }
            else
            {
                operacaoBolsa.Query.Where(operacaoBolsa.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra, TipoOperacaoBolsa.CompraDaytrade));
            }

            if (liquidaD0)
            {
                operacaoBolsa.Query.Where(operacaoBolsa.Query.DataLiquidacao.Equal(operacaoBolsa.Query.Data));
            }
            else
            {
                operacaoBolsa.Query.Where(operacaoBolsa.Query.DataLiquidacao.NotEqual(data));
            }

            operacaoBolsa.Query.Load();

            decimal valorCompra = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Valor.Value : 0;
            decimal corretagemCompra = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Corretagem.Value : 0;
            decimal emolumentoCompra = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Emolumento.Value + operacaoBolsa.RegistroBolsa.Value : 0;
            decimal taxasCBLCCompra = operacaoBolsa.Valor.HasValue ? operacaoBolsa.LiquidacaoCBLC.Value + operacaoBolsa.RegistroCBLC.Value : 0;

            totalPagar += valorCompra + corretagemCompra + emolumentoCompra + taxasCBLCCompra;

            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.Valor.Sum(),
                                     operacaoBolsa.Query.Corretagem.Sum(),
                                     operacaoBolsa.Query.Emolumento.Sum(),
                                     operacaoBolsa.Query.LiquidacaoCBLC.Sum(),
                                     operacaoBolsa.Query.RegistroBolsa.Sum(),
                                     operacaoBolsa.Query.RegistroCBLC.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));

            if (cliente.ContabilDaytrade.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBolsa.Query.Where(operacaoBolsa.Query.TipoOperacao.Equal(TipoOperacaoBolsa.Venda));
            }
            else
            {
                operacaoBolsa.Query.Where(operacaoBolsa.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda, TipoOperacaoBolsa.VendaDaytrade));
            }

            if (liquidaD0)
            {
                operacaoBolsa.Query.Where(operacaoBolsa.Query.DataLiquidacao.Equal(operacaoBolsa.Query.Data));
            }
            else
            {
                operacaoBolsa.Query.Where(operacaoBolsa.Query.DataLiquidacao.NotEqual(data));
            }

            operacaoBolsa.Query.Load();

            decimal valorVenda = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Valor.Value : 0;
            decimal corretagemVenda = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Corretagem.Value : 0;
            decimal emolumentoVenda = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Emolumento.Value + operacaoBolsa.RegistroBolsa.Value : 0;
            decimal taxasCBLCVenda = operacaoBolsa.Valor.HasValue ? operacaoBolsa.LiquidacaoCBLC.Value + operacaoBolsa.RegistroCBLC.Value : 0;

            totalPagar += corretagemVenda + emolumentoVenda + taxasCBLCVenda;
            totalReceber += valorVenda;

            valorNet = totalReceber - totalPagar;


            #region Ajusta contas crédito ou débito dos eventos de compra e venda, para jogar a conta de net a liquidar
            string contaCreditoReversao = "";
            string contaDebitoReversao = "";
            int idContaCreditoReversao = 0;
            int idContaDebitoReversao = 0;
            bool usaContaReversao = false;
            if (valorNet > 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                    contaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    idContaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito.Value;

                    usaContaReversao = true;
                }
            }
            else if (valorNet < 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                    contaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    idContaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito.Value;

                    usaContaReversao = true;
                }
            }
            #endregion

            #region Evento de CompraAcao
            if (valorCompra > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraAcao;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorCompra;
                }
            }
            #endregion

            #region Evento de CompraAcaoCorretagem
            if (corretagemCompra > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraAcaoCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemCompra;
                }
            }
            #endregion

            #region Evento de CompraAcaoEmolumento
            if (emolumentoCompra > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraAcaoEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoCompra;
                }
            }
            #endregion

            #region Evento de CompraAcaoTaxaCBLC
            if (taxasCBLCCompra > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraAcaoTaxaCBLC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = taxasCBLCCompra;
                }
            }
            #endregion

            #region Evento de VendaAcao
            if (valorVenda > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaAcao;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorVenda;
                }
            }
            #endregion

            #region Evento de VendaAcaoCorretagem
            if (corretagemVenda > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaAcaoCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemVenda;
                }
            }
            #endregion

            #region Evento VendaAcaoEmolumento
            if (emolumentoVenda > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaAcaoEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoVenda;
                }
            }
            #endregion

            #region Evento de VendaAcaoTaxaCBLC
            if (taxasCBLCVenda > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaAcaoTaxaCBLC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = taxasCBLCVenda;
                }
            }
            #endregion

            if (cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                #region Evento de Liquidacao (Pagar/Receber)
                int idEventoLiquidacao = 0;
                if (valorNet > 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaReceber;
                    }
                }
                if (valorNet < 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaPagar;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = new DateTime();
                    if (liquidaD0)
                    {
                        dataLiquidacao = data;
                    }
                    else
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorNet;
                }
                #endregion
            }
            else
            {
                #region Evento de Liquidacao (Receber)
                int idEventoLiquidacao = 0;
                if (totalReceber != 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaReceber;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = new DateTime();
                    if (liquidaD0)
                    {
                        dataLiquidacao = data;
                    }
                    else
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalReceber;
                }
                #endregion

                #region Evento de Liquidacao (Pagar)
                idEventoLiquidacao = 0;
                if (totalPagar != 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                        origem = ContabilOrigem.Bolsa.LiquidacaoBovespaPagar;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = new DateTime();
                    if (liquidaD0)
                    {
                        dataLiquidacao = data;
                    }
                    else
                    {
                        dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
                    }
                    
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalPagar;
                }
                #endregion
            }
            
            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de LucroAcao, PrejuizoAcao.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraOperacaoAcaoResultados(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
                        
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum(),
                                       operacaoBolsa.Query.ResultadoExercicio.Sum(),
                                       operacaoBolsa.Query.ResultadoTermo.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente) &
                                    operacaoBolsa.Query.Data.Equal(data) &
                                    operacaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario, TipoMercadoBolsa.Termo) &
                                    operacaoBolsa.Query.Origem.NotIn((byte)OrigemOperacaoBolsa.AberturaEmprestimo, (byte)OrigemOperacaoBolsa.LiquidacaoEmprestimo) &
                                    (operacaoBolsa.Query.ResultadoRealizado.GreaterThan(0) |
                                     operacaoBolsa.Query.ResultadoExercicio.GreaterThan(0) |
                                     operacaoBolsa.Query.ResultadoTermo.GreaterThan(0)));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value + operacaoBolsa.ResultadoExercicio.Value + operacaoBolsa.ResultadoTermo.Value;

                #region Evento de LucroAcao
                origem = ContabilOrigem.Bolsa.LucroAcao;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.ResultadoRealizado.Sum(),
                                       operacaoBolsa.Query.ResultadoExercicio.Sum(),
                                       operacaoBolsa.Query.ResultadoTermo.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente) &
                                    operacaoBolsa.Query.Data.Equal(data) &
                                    operacaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario, TipoMercadoBolsa.Termo) &
                                    operacaoBolsa.Query.Origem.NotIn((byte)OrigemOperacaoBolsa.AberturaEmprestimo, (byte)OrigemOperacaoBolsa.LiquidacaoEmprestimo) &
                                    (operacaoBolsa.Query.ResultadoRealizado.LessThan(0) |
                                     operacaoBolsa.Query.ResultadoExercicio.LessThan(0) |
                                     operacaoBolsa.Query.ResultadoTermo.LessThan(0)));
            operacaoBolsa.Query.Load();

            if (operacaoBolsa.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBolsa.ResultadoRealizado.Value + operacaoBolsa.ResultadoExercicio.Value + operacaoBolsa.ResultadoTermo.Value;

                #region Evento de PrejuizoAcao
                origem = ContabilOrigem.Bolsa.PrejuizoAcao;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }


            contabLancamentoCollection.Save();
        }
        
        /// <summary>
        /// Eventos de LucroAcaoDayTrade, PrejuizoAcaoDayTrade, jogando em valores a liquidar contra resultado.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraDayTradeAcao(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.TipoOperacao,
                                                 operacaoBolsaCollection.Query.ValorLiquido.Sum());
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                        operacaoBolsaCollection.Query.Data.Equal(data),
                                        operacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade),
                                        operacaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
            operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.TipoOperacao);
            operacaoBolsaCollection.Query.Load();

            decimal resultadoDayTrade = 0;
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                decimal valorLiquido = operacaoBolsa.ValorLiquido.Value;
                if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade)
                {
                    valorLiquido = valorLiquido * -1;
                }

                resultadoDayTrade += valorLiquido;
            }

            #region Eventos LucroAcaoDayTrade/PrejuizoAcaoDayTrade
            if (resultadoDayTrade != 0)
            {
                if (resultadoDayTrade > 0)
                {
                    origem = ContabilOrigem.Bolsa.LucroAcaoDayTrade;
                }
                else
                {
                    origem = ContabilOrigem.Bolsa.PrejuizoAcaoDayTrade;
                }

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTrade;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de Liquidacao (Pagar/Receber)
            int idEventoLiquidacao = 0;
            if (resultadoDayTrade > 0)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                {
                    idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                    origem = ContabilOrigem.Bolsa.LiquidacaoBovespaReceber;
                }
            }
            else if (resultadoDayTrade < 0)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                {
                    idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                    origem = ContabilOrigem.Bolsa.LiquidacaoBovespaPagar;
                }
            }

            if (resultadoDayTrade != 0)
            {
                ContabRoteiro contabRoteiro = new ContabRoteiro();
                if (contabRoteiro.LoadByPrimaryKey(idEventoLiquidacao))
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = new ContabLancamento();

                    contabLancamento.Descricao = contabRoteiro.Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiro.IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiro.IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiro.IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiro.ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiro.ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTrade;

                    contabLancamento.Save();
                }
            }
            #endregion
        }

        /// <summary>
        /// Eventos de LucroOpcaoDayTrade, PrejuizoOpcaoDayTrade, jogando em valores a liquidar contra resultado.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraDayTradeOpcao(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.TipoMercado,
                                                 operacaoBolsaCollection.Query.TipoOperacao,
                                                 operacaoBolsaCollection.Query.ValorLiquido.Sum());
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                        operacaoBolsaCollection.Query.Data.Equal(data),
                                        operacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade),
                                        operacaoBolsaCollection.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda));
            operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.TipoMercado, operacaoBolsaCollection.Query.TipoOperacao);
            operacaoBolsaCollection.Query.Load();

            decimal resultadoDayTrade = 0;
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                decimal valorLiquido = operacaoBolsa.ValorLiquido.Value;
                if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade)
                {
                    valorLiquido = valorLiquido * -1;
                }

                resultadoDayTrade += valorLiquido;
            }

            #region Eventos LucroOpcaoDayTrade/PrejuizoOpcaoDayTrade
            if (resultadoDayTrade != 0)
            {
                if (resultadoDayTrade > 0)
                {
                    origem = ContabilOrigem.Bolsa.LucroOpcaoDayTrade;
                }
                else
                {
                    origem = ContabilOrigem.Bolsa.PrejuizoOpcaoDayTrade;
                }

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTrade;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de Liquidacao (Pagar/Receber)
            int idEventoLiquidacao = 0;
            if (resultadoDayTrade > 0)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaReceber))
                {
                    idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaReceber];
                    origem = ContabilOrigem.Bolsa.LiquidacaoBovespaReceber;
                }
            }
            else if (resultadoDayTrade < 0)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                {
                    idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                    origem = ContabilOrigem.Bolsa.LiquidacaoBovespaPagar;
                }
            }

            if (resultadoDayTrade != 0)
            {
                ContabRoteiro contabRoteiro = new ContabRoteiro();
                if (contabRoteiro.LoadByPrimaryKey(idEventoLiquidacao))
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBolsa, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = new ContabLancamento();

                    contabLancamento.Descricao = contabRoteiro.Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiro.IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiro.IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiro.IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiro.ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiro.ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTrade;

                    contabLancamento.Save();
                }
            }
            #endregion

        }

        /// <summary>
        /// Eventos de LucroAcaoDayTrade, PrejuizoAcaoDayTrade, LucroOPCDayTrade, PrejuizoOPCDayTrade, LucroOPVDayTrade, PrejuizoOPVDayTrade.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraDayTradeConsolidado(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.TipoMercado,
                                                 operacaoBolsaCollection.Query.TipoOperacao,
                                                 operacaoBolsaCollection.Query.ValorLiquido.Sum());
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                        operacaoBolsaCollection.Query.Data.Equal(data),
                                        operacaoBolsaCollection.Query.TipoOperacao.In(TipoOperacaoBolsa.CompraDaytrade, TipoOperacaoBolsa.VendaDaytrade),
                                        operacaoBolsaCollection.Query.TipoMercado.NotIn(TipoMercadoBolsa.Futuro, TipoMercadoBolsa.Termo));
            operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.TipoMercado, operacaoBolsaCollection.Query.TipoOperacao);
            operacaoBolsaCollection.Query.Load();

            decimal resultadoDayTradeAcoes = 0;
            decimal resultadoDayTradeOPC = 0;
            decimal resultadoDayTradeOPV = 0;
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                decimal valorLiquido = operacaoBolsa.ValorLiquido.Value;
                if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.CompraDaytrade)
                {
                    valorLiquido = valorLiquido * -1;
                }

                switch (operacaoBolsa.TipoMercado)
                {
                    case TipoMercadoBolsa.MercadoVista:
                    case TipoMercadoBolsa.Imobiliario:
                        resultadoDayTradeAcoes += valorLiquido;
                        break;
                    case TipoMercadoBolsa.OpcaoCompra:
                        resultadoDayTradeOPC += valorLiquido;
                        break;
                    case TipoMercadoBolsa.OpcaoVenda:
                        resultadoDayTradeOPV += valorLiquido;
                        break;
                }
            }

            #region Eventos LucroAcaoDayTrade/PrejuizoAcaoDayTrade
            if (resultadoDayTradeAcoes != 0)
            {
                int origem = 0;
                if (resultadoDayTradeAcoes > 0)
                {
                    origem = ContabilOrigem.Bolsa.LucroAcaoDayTrade;
                }
                else
                {
                    origem = ContabilOrigem.Bolsa.PrejuizoAcaoDayTrade;
                }

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTradeAcoes;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Eventos LucroOPCDayTrade/PrejuizoOPCDayTrade
            if (resultadoDayTradeOPC != 0)
            {
                int origem = 0;
                if (resultadoDayTradeOPC > 0)
                {
                    origem = ContabilOrigem.Bolsa.LucroOpcaoDayTrade;
                }
                else
                {
                    origem = ContabilOrigem.Bolsa.PrejuizoOpcaoDayTrade;
                }

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTradeOPC;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Eventos LucroOPVDayTrade/PrejuizoOPVDayTrade
            if (resultadoDayTradeOPV != 0)
            {
                int origem = 0;
                if (resultadoDayTradeOPV > 0)
                {
                    origem = ContabilOrigem.Bolsa.LucroOPVDayTrade;
                }
                else
                {
                    origem = ContabilOrigem.Bolsa.PrejuizoOPVDayTrade;
                }

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTradeOPV;

                    contabLancamento.Save();
                }
            }
            #endregion

        }

        /// <summary>
        /// Eventos de NaoExercicioOPCComprada, NaoExercicioOPCVendida, NaoExercicioOPVComprada, NaoExercicioOPVVendida.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraExpiracaoOpcaoConsolidado(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.TipoMercado,
                                                 operacaoBolsaCollection.Query.TipoOperacao,
                                                 operacaoBolsaCollection.Query.ResultadoRealizado.Sum());
            operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                operacaoBolsaCollection.Query.Data.Equal(data),
                                                operacaoBolsaCollection.Query.Origem.Equal((byte)OrigemOperacaoBolsa.VencimentoOpcao));
            operacaoBolsaCollection.Query.GroupBy(operacaoBolsaCollection.Query.TipoMercado, operacaoBolsaCollection.Query.TipoOperacao);
            operacaoBolsaCollection.Query.Load();

            decimal resultadoOPCComprada = 0;
            decimal resultadoOPCVendida = 0;
            decimal resultadoOPVComprada = 0;
            decimal resultadoOPVVendida = 0;
            foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
            {
                if (operacaoBolsa.TipoMercado == TipoMercadoBolsa.OpcaoCompra)
                {
                    if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Deposito)
                    {
                        resultadoOPCVendida = operacaoBolsa.ResultadoRealizado.Value;
                    }
                    else
                    {
                        resultadoOPCComprada = operacaoBolsa.ResultadoRealizado.Value;
                    }
                }
                else
                {
                    if (operacaoBolsa.TipoOperacao == TipoOperacaoBolsa.Deposito)
                    {
                        resultadoOPVVendida = operacaoBolsa.ResultadoRealizado.Value;
                    }
                    else
                    {
                        resultadoOPVComprada = operacaoBolsa.ResultadoRealizado.Value;
                    }
                }
            }

            #region Eventos NaoExercicioOPCComprada/NaoExercicioOPCVendida/NaoExercicioOPVComprada/NaoExercicioOPVVendida
            if (resultadoOPCComprada != 0)
            {
                int origem = ContabilOrigem.Bolsa.NaoExercicioOPCComprada;
                
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoOPCComprada;

                    contabLancamento.Save();
                }
            }

            if (resultadoOPCVendida != 0)
            {
                int origem = ContabilOrigem.Bolsa.NaoExercicioOPCVendida;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoOPCVendida;

                    contabLancamento.Save();
                }
            }

            if (resultadoOPVComprada != 0)
            {
                int origem = ContabilOrigem.Bolsa.NaoExercicioOPVComprada;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoOPVComprada;

                    contabLancamento.Save();
                }
            }

            if (resultadoOPVVendida != 0)
            {
                int origem = ContabilOrigem.Bolsa.NaoExercicioOPVVendida;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoOPVVendida;

                    contabLancamento.Save();
                }
            }
            #endregion
        }

        /// <summary>
        /// Eventos de LucroAcaoDayTrade, PrejuizoAcaoDayTrade.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraEventosBolsa(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            ProventoBolsaClienteCollection proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollection.Query.Select(proventoBolsaClienteCollection.Query.CdAtivoBolsa,
                                                        proventoBolsaClienteCollection.Query.DataEx,
                                                        proventoBolsaClienteCollection.Query.DataPagamento,
                                                        proventoBolsaClienteCollection.Query.Quantidade,
                                                        proventoBolsaClienteCollection.Query.TipoProvento,
                                                        proventoBolsaClienteCollection.Query.Valor);
            proventoBolsaClienteCollection.Query.Where(proventoBolsaClienteCollection.Query.IdCliente.Equal(idCliente),
                                                       proventoBolsaClienteCollection.Query.DataPagamento.GreaterThan(data),
                                                       proventoBolsaClienteCollection.Query.DataLancamento.Equal(data));
            proventoBolsaClienteCollection.Query.Load();

            decimal provisaoDividendosReceber = 0;
            decimal provisaoDividendosPagar = 0;
            decimal provisaoJurosReceber = 0;
            decimal provisaoJurosPagar = 0;
            decimal provisaoRendimentosReceber = 0;
            decimal provisaoRendimentosPagar = 0;
            foreach (ProventoBolsaCliente proventoBolsaCliente in proventoBolsaClienteCollection)
            {
                string cdAtivoBolsa = proventoBolsaCliente.CdAtivoBolsa;
                DateTime dataEx = proventoBolsaCliente.DataEx.Value;
                DateTime dataPagamento = proventoBolsaCliente.DataPagamento.Value;
                decimal quantidade = proventoBolsaCliente.Quantidade.Value;
                byte tipoProvento = proventoBolsaCliente.TipoProvento.Value;
                decimal valor = proventoBolsaCliente.Valor.Value;

                ProventoBolsaClienteCollection proventoBolsaClienteCollectionExiste = new ProventoBolsaClienteCollection();
                proventoBolsaClienteCollectionExiste.Query.Select(proventoBolsaClienteCollectionExiste.Query.IdProvento);
                proventoBolsaClienteCollectionExiste.Query.Where(proventoBolsaClienteCollection.Query.IdCliente.Equal(idCliente),
                                                       proventoBolsaClienteCollection.Query.DataLancamento.LessThan(data),
                                                       proventoBolsaClienteCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                       proventoBolsaClienteCollection.Query.DataEx.Equal(dataEx),
                                                       proventoBolsaClienteCollection.Query.DataPagamento.Equal(dataPagamento),
                                                       proventoBolsaClienteCollection.Query.Quantidade.Equal(quantidade),
                                                       proventoBolsaClienteCollection.Query.TipoProvento.Equal(tipoProvento));
                proventoBolsaClienteCollectionExiste.Query.Load();

                if (proventoBolsaClienteCollectionExiste.Count == 0) //Checa se já tinha sido lançado em datas anteriores
                {
                    if (valor > 0)
                    {
                        if (tipoProvento == (byte)TipoProventoBolsa.Dividendo || tipoProvento == (byte)TipoProventoBolsa.BonificacaoDinheiro)
                        {
                            provisaoDividendosReceber += valor;
                        }
                        else if (tipoProvento == (byte)TipoProventoBolsa.JurosSobreCapital)
                        {
                            provisaoJurosReceber += valor;
                        }
                        else
                        {
                            provisaoRendimentosReceber += valor;
                        }
                    }
                    else if (valor < 0)
                    {
                        if (tipoProvento == (byte)TipoProventoBolsa.Dividendo || tipoProvento == (byte)TipoProventoBolsa.BonificacaoDinheiro)
                        {
                            provisaoDividendosPagar += valor;
                        }
                        else if (tipoProvento == (byte)TipoProventoBolsa.JurosSobreCapital)
                        {
                            provisaoJurosPagar += valor;
                        }
                        else
                        {
                            provisaoRendimentosPagar += valor;
                        }
                    }
                }
            }

            #region Eventos ProvisaoDividendosReceber/ProvisaoDividendosPagar/ProvisaoJurosCapitalReceber/ProvisaoJurosCapitalPagar/ProvisaoRendimentosReceber/ProvisaoRendimentosPagar
            if (provisaoDividendosReceber != 0)
            {
                int origem = ContabilOrigem.Bolsa.ProvisaoDividendosReceber;                

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoDividendosReceber;

                    contabLancamento.Save();
                }
            }

            if (provisaoDividendosPagar != 0)
            {
                int origem = ContabilOrigem.Bolsa.ProvisaoDividendosPagar;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoDividendosPagar;

                    contabLancamento.Save();
                }
            }

            if (provisaoJurosReceber != 0)
            {
                int origem = ContabilOrigem.Bolsa.ProvisaoJurosCapitalReceber;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoJurosReceber;

                    contabLancamento.Save();
                }
            }
            
            if (provisaoJurosPagar != 0)
            {
                int origem = ContabilOrigem.Bolsa.ProvisaoJurosCapitalPagar;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoJurosPagar;

                    contabLancamento.Save();
                }
            }

            if (provisaoRendimentosReceber != 0)
            {
                int origem = ContabilOrigem.Bolsa.ProvisaoRendimentosReceber;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoRendimentosReceber;

                    contabLancamento.Save();
                }
            }

            if (provisaoRendimentosPagar != 0)
            {
                int origem = ContabilOrigem.Bolsa.ProvisaoRendimentosPagar;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoRendimentosPagar;

                    contabLancamento.Save();
                }
            }
            #endregion


            proventoBolsaClienteCollection = new ProventoBolsaClienteCollection();
            proventoBolsaClienteCollection.Query.Select(proventoBolsaClienteCollection.Query.CdAtivoBolsa,
                                                        proventoBolsaClienteCollection.Query.DataEx,
                                                        proventoBolsaClienteCollection.Query.DataPagamento,
                                                        proventoBolsaClienteCollection.Query.Quantidade,
                                                        proventoBolsaClienteCollection.Query.TipoProvento,
                                                        proventoBolsaClienteCollection.Query.Valor);
            proventoBolsaClienteCollection.Query.Where(proventoBolsaClienteCollection.Query.IdCliente.Equal(idCliente),
                                                       proventoBolsaClienteCollection.Query.DataPagamento.Equal(data));
            proventoBolsaClienteCollection.Query.Load();

            bool geraEventosPagamento = false;
            if (proventoBolsaClienteCollection.Count > 0)
            {
                #region Faz checagem se já será lançado via consulta à Liquidacao os pagamentos vindos de CMDF ou Sinacor
                List<int> listaEventosPagamento = new List<int>();
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.PagamentoDividendosPagar))
                {
                    listaEventosPagamento.Add(roteiroContabil[ContabilOrigem.Bolsa.PagamentoDividendosPagar]);
                }
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.PagamentoDividendosReceber))
                {
                    listaEventosPagamento.Add(roteiroContabil[ContabilOrigem.Bolsa.PagamentoDividendosReceber]);
                }
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.PagamentoJurosCapitalPagar))
                {
                    listaEventosPagamento.Add(roteiroContabil[ContabilOrigem.Bolsa.PagamentoJurosCapitalPagar]);
                }
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.PagamentoJurosCapitalReceber))
                {
                    listaEventosPagamento.Add(roteiroContabil[ContabilOrigem.Bolsa.PagamentoJurosCapitalReceber]);
                }
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.PagamentoRendimentosPagar))
                {
                    listaEventosPagamento.Add(roteiroContabil[ContabilOrigem.Bolsa.PagamentoRendimentosPagar]);
                }
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.PagamentoRendimentosReceber))
                {
                    listaEventosPagamento.Add(roteiroContabil[ContabilOrigem.Bolsa.PagamentoRendimentosReceber]);
                }

                if (listaEventosPagamento.Count > 0)
                {
                    LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                    liquidacaoCollection.Query.Select(liquidacaoCollection.Query.IdLiquidacao);
                    liquidacaoCollection.Query.Where(liquidacaoCollection.Query.DataLancamento.Equal(data),
                                                     liquidacaoCollection.Query.IdCliente.Equal(idCliente),
                                                     liquidacaoCollection.Query.IdEvento.In(listaEventosPagamento),
                                                     liquidacaoCollection.Query.Origem.Equal(OrigemLancamentoLiquidacao.Outros),
                                                     liquidacaoCollection.Query.Fonte.NotEqual((byte)FonteLancamentoLiquidacao.Interno),
                                                     liquidacaoCollection.Query.DataVencimento.GreaterThanOrEqual(data));
                    liquidacaoCollection.Query.Load();

                    if (liquidacaoCollection.Count == 0)
                    {
                        geraEventosPagamento = true;
                    }
                }
                #endregion
            }

            if (!geraEventosPagamento)
                return;

            decimal pagamentoDividendosReceber = 0;
            decimal pagamentoDividendosPagar = 0;
            decimal pagamentoJurosReceber = 0;
            decimal pagamentoJurosPagar = 0;
            decimal pagamentoRendimentosReceber = 0;
            decimal pagamentoRendimentosPagar = 0;
            foreach (ProventoBolsaCliente proventoBolsaCliente in proventoBolsaClienteCollection)
            {
                string cdAtivoBolsa = proventoBolsaCliente.CdAtivoBolsa;
                DateTime dataEx = proventoBolsaCliente.DataEx.Value;
                DateTime dataPagamento = proventoBolsaCliente.DataPagamento.Value;
                decimal quantidade = proventoBolsaCliente.Quantidade.Value;
                byte tipoProvento = proventoBolsaCliente.TipoProvento.Value;
                decimal valor = proventoBolsaCliente.Valor.Value;

                if (valor > 0)
                {
                    if (tipoProvento == (byte)TipoProventoBolsa.Dividendo || tipoProvento == (byte)TipoProventoBolsa.BonificacaoDinheiro)
                    {
                        pagamentoDividendosReceber += valor;
                    }
                    else if (tipoProvento == (byte)TipoProventoBolsa.JurosSobreCapital)
                    {
                        pagamentoJurosReceber += valor;
                    }
                    else
                    {
                        pagamentoRendimentosReceber += valor;
                    }
                }
                else if (valor < 0)
                {
                    if (tipoProvento == (byte)TipoProventoBolsa.Dividendo || tipoProvento == (byte)TipoProventoBolsa.BonificacaoDinheiro)
                    {
                        pagamentoDividendosPagar += valor;
                    }
                    else if (tipoProvento == (byte)TipoProventoBolsa.JurosSobreCapital)
                    {
                        pagamentoJurosPagar += valor;
                    }
                    else
                    {
                        pagamentoRendimentosPagar += valor;
                    }
                }
            }

            #region Eventos PagamentoDividendosReceber/PagamentoDividendosPagar/PagamentoJurosCapitalReceber/PagamentoJurosCapitalPagar/PagamentoRendimentosReceber/PagamentoRendimentosPagar
            if (pagamentoDividendosReceber != 0)
            {
                int origem = ContabilOrigem.Bolsa.PagamentoDividendosReceber;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = pagamentoDividendosReceber;

                    contabLancamento.Save();
                }
            }
            
            if (pagamentoDividendosPagar != 0)
            {
                int origem = ContabilOrigem.Bolsa.PagamentoDividendosPagar;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = pagamentoDividendosPagar;

                    contabLancamento.Save();
                }
            }

            if (pagamentoJurosReceber != 0)
            {
                int origem = ContabilOrigem.Bolsa.PagamentoJurosCapitalReceber;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = pagamentoJurosReceber;

                    contabLancamento.Save();
                }
            }

            if (pagamentoJurosPagar != 0)
            {
                int origem = ContabilOrigem.Bolsa.PagamentoJurosCapitalPagar;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = pagamentoJurosPagar;

                    contabLancamento.Save();
                }
            }

            if (pagamentoRendimentosReceber != 0)
            {
                int origem = ContabilOrigem.Bolsa.PagamentoRendimentosReceber;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = pagamentoRendimentosReceber;

                    contabLancamento.Save();
                }
            }

            if (pagamentoRendimentosPagar != 0)
            {
                int origem = ContabilOrigem.Bolsa.PagamentoRendimentosPagar;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = pagamentoRendimentosPagar;

                    contabLancamento.Save();
                }
            }
            #endregion

        }

        /// <summary>
        /// Eventos de BTC_Doado, BTC_Tomado, BTC_Doado_PagtoTaxas, BTC_Tomado_PagtoTaxas.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <param name="dataProxima"></param>
        private void GeraEventosBTCOperacao(Cliente cliente, DateTime data, bool historico, DateTime dataProxima)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            bool controlaCustodiaBTC = (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraTodas ||
                    (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.NaoApura));

            if (!controlaCustodiaBTC)
            {
                return;
            }

            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            if (historico)
            {
                PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                posicaoEmprestimoBolsaHistoricoCollection.Query.Select(posicaoEmprestimoBolsaHistoricoCollection.Query.PontaEmprestimo,
                                                                       posicaoEmprestimoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                                       posicaoEmprestimoBolsaHistoricoCollection.Query.Quantidade,
                                                                       posicaoEmprestimoBolsaHistoricoCollection.Query.ValorMercado,
                                                                       posicaoEmprestimoBolsaHistoricoCollection.Query.PULiquidoOriginal);
                posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.DataRegistro.Equal(data),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoEmprestimoBolsaHistoricoCollection.Query.Load();

                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollectionAux = new PosicaoEmprestimoBolsaCollection(posicaoEmprestimoBolsaHistoricoCollection);

                posicaoEmprestimoBolsaCollection.Combine(posicaoEmprestimoBolsaCollectionAux);
            }
            else
            {                
                posicaoEmprestimoBolsaCollection.Query.Select(posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo,
                                                              posicaoEmprestimoBolsaCollection.Query.ValorMercado,
                                                              posicaoEmprestimoBolsaCollection.Query.Quantidade,
                                                              posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa,
                                                              posicaoEmprestimoBolsaCollection.Query.PULiquidoOriginal);
                posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                             posicaoEmprestimoBolsaCollection.Query.DataRegistro.Equal(data));                
                posicaoEmprestimoBolsaCollection.Query.Load();
            }

            decimal valorDoado = 0;
            decimal valorTomado = 0;
            decimal variacaoDoadorPositiva = 0;
            decimal variacaoDoadorNegativa = 0;
            foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
            {
                byte pontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo.Value;
                string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                decimal valor = posicaoEmprestimoBolsa.ValorMercado.Value;
                decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;

                if (pontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador)
                {
                    valorDoado += valor;                    

                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                    decimal variacao = Utilitario.Truncate(valor - (quantidade * puLiquidoOriginal / fatorCotacaoBolsa.Fator.Value), 2);

                    if (variacao > 0)
                    {
                        variacaoDoadorPositiva += variacao;
                    }
                    else
                    {
                        variacaoDoadorNegativa += variacao;
                    }
                }
                else
                {
                    valorTomado += valor;
                }
            }

            
            #region Evento de BTC_Doado
            if (valorDoado != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Doado;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorDoado;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de BTC_Tomado
            if (valorTomado != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Tomado;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorTomado;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de ValorizacaoAcao
            if (variacaoDoadorPositiva != 0)
            {
                int origem = ContabilOrigem.Bolsa.ValorizacaoAcao;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoDoadorPositiva;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de DesvalorizacaoAcao
            if (variacaoDoadorNegativa != 0)
            {
                int origem = ContabilOrigem.Bolsa.DesvalorizacaoAcao;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoDoadorNegativa;

                    contabLancamento.Save();
                }
            }
            #endregion            
            
            #region Evento de BTC_Doado_VariacaoPos_Reversao
            if (variacaoDoadorPositiva != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Doado_VariacaoPos_Reversao;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoDoadorPositiva;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de BTC_Doado_VariacaoNeg_Reversao
            if (variacaoDoadorNegativa != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Doado_VariacaoNeg_Reversao;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoDoadorNegativa;

                    contabLancamento.Save();
                }
            }
            #endregion
            
        }

        /// <summary>
        /// Eventos de BTC_Tomado_VariacaoPos, BTC_Tomado_VariacaoNeg, BTC_Doado_VariacaoPos, BTC_Doado_VariacaoNeg.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <param name="dataProxima"></param>
        private void GeraEventosBTCPosicao(Cliente cliente, DateTime data, bool historico, DateTime dataProxima)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            decimal valorMinimoCBLC = 0;
            TabelaCustosBolsa tabelaCustosBolsa = new TabelaCustosBolsa();
            tabelaCustosBolsa.BuscaTabelaCustosBolsa(data, (Int16)TipoTaxaBolsa.EmprestimoBolsa.Voluntario);

            if (tabelaCustosBolsa.MinimoCBLC.HasValue)
            {
                valorMinimoCBLC = tabelaCustosBolsa.MinimoCBLC.Value;
            }
                        
            PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollection = new PosicaoEmprestimoBolsaCollection();
            if (historico)
            {
                PosicaoEmprestimoBolsaHistoricoCollection posicaoEmprestimoBolsaHistoricoCollection = new PosicaoEmprestimoBolsaHistoricoCollection();
                posicaoEmprestimoBolsaHistoricoCollection.Query.Select(posicaoEmprestimoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.PontaEmprestimo,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.Quantidade,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.ValorBase,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.ValorDiarioCBLC,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.ValorCorrigidoCBLC,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.ValorDiarioComissao,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.ValorDiarioJuros,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.PUMercado,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.PULiquidoOriginal,
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.DataRegistro);
                posicaoEmprestimoBolsaHistoricoCollection.Query.Where(posicaoEmprestimoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.Quantidade.NotEqual(0),
                                                                      posicaoEmprestimoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));                
                posicaoEmprestimoBolsaHistoricoCollection.Query.Load();

                PosicaoEmprestimoBolsaCollection posicaoEmprestimoBolsaCollectionAux = new PosicaoEmprestimoBolsaCollection(posicaoEmprestimoBolsaHistoricoCollection);
                posicaoEmprestimoBolsaCollection.Combine(posicaoEmprestimoBolsaCollectionAux);
            }
            else
            {
                posicaoEmprestimoBolsaCollection.Query.Select(posicaoEmprestimoBolsaCollection.Query.CdAtivoBolsa,
                                                              posicaoEmprestimoBolsaCollection.Query.PontaEmprestimo,
                                                              posicaoEmprestimoBolsaCollection.Query.Quantidade,
                                                              posicaoEmprestimoBolsaCollection.Query.ValorBase,
                                                              posicaoEmprestimoBolsaCollection.Query.ValorDiarioCBLC,
                                                              posicaoEmprestimoBolsaCollection.Query.ValorCorrigidoCBLC,
                                                              posicaoEmprestimoBolsaCollection.Query.ValorDiarioComissao,
                                                              posicaoEmprestimoBolsaCollection.Query.ValorDiarioJuros,
                                                              posicaoEmprestimoBolsaCollection.Query.PUMercado,
                                                              posicaoEmprestimoBolsaCollection.Query.PULiquidoOriginal,
                                                              posicaoEmprestimoBolsaCollection.Query.DataRegistro);
                posicaoEmprestimoBolsaCollection.Query.Where(posicaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                             posicaoEmprestimoBolsaCollection.Query.Quantidade.NotEqual(0));                
                posicaoEmprestimoBolsaCollection.Query.Load();
            }

            decimal variacaoPositivoDoador = 0;
            decimal variacaoNegativoDoador = 0;
            decimal variacaoPositivoTomador = 0;
            decimal variacaoNegativoTomador = 0;
            decimal provisaoTaxasDoador = 0;
            decimal provisaoTaxasTomador = 0;
            foreach (PosicaoEmprestimoBolsa posicaoEmprestimoBolsa in posicaoEmprestimoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoEmprestimoBolsa.CdAtivoBolsa;
                byte pontaEmprestimo = posicaoEmprestimoBolsa.PontaEmprestimo.Value;
                decimal quantidade = posicaoEmprestimoBolsa.Quantidade.Value;
                decimal puMercado = posicaoEmprestimoBolsa.PUMercado.Value;
                decimal puLiquidoOriginal = posicaoEmprestimoBolsa.PULiquidoOriginal.Value;
                DateTime dataRegistro = posicaoEmprestimoBolsa.DataRegistro.Value;

                if (pontaEmprestimo == (byte)PontaEmprestimoBolsa.Tomador) //Provisório pq o Financial está jogando o PU do empréstimo como sendo o PU base, qdo deveria ser o PUMercado do dia
                {
                    puLiquidoOriginal = 0;
                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    cotacaoBolsa.Query.Select(cotacaoBolsa.Query.PUFechamento);
                    cotacaoBolsa.Query.Where(cotacaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                             cotacaoBolsa.Query.Data.Equal(dataRegistro));
                    if (cotacaoBolsa.Query.Load())
                    {
                        puLiquidoOriginal = cotacaoBolsa.PUFechamento.HasValue? cotacaoBolsa.PUFechamento.Value: 0;
                    }
                }

                decimal valorDiarioCBLC = posicaoEmprestimoBolsa.ValorDiarioCBLC.Value;
                decimal valorDiarioComissao = posicaoEmprestimoBolsa.ValorDiarioComissao.Value;
                decimal valorDiarioJuros = posicaoEmprestimoBolsa.ValorDiarioJuros.Value;

                decimal variacao = 0;
                if (dataRegistro < data)
                {
                    FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                    fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                    variacao = Utilitario.Truncate(((puMercado - puLiquidoOriginal) * quantidade) / fatorCotacao.Fator.Value, 2);

                    if (pontaEmprestimo == (byte)PontaEmprestimoBolsa.Tomador)
                    {
                        variacao = variacao * -1;
                    }

                    if (pontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador)
                    {
                        if (variacao > 0)
                        {
                            variacaoPositivoDoador += variacao;
                        }
                        else
                        {
                            variacaoNegativoDoador += variacao;
                        }
                    }
                    else
                    {
                        if (variacao > 0)
                        {
                            variacaoPositivoTomador += variacao;
                        }
                        else
                        {
                            variacaoNegativoTomador += variacao;
                        }
                    }
                }

                if (pontaEmprestimo == (byte)PontaEmprestimoBolsa.Doador)
                {
                    provisaoTaxasDoador += valorDiarioJuros;
                }
                else
                {                    
                    if (dataRegistro == data)
                    {
                        valorDiarioCBLC = valorMinimoCBLC;
                    }

                    provisaoTaxasTomador += valorDiarioCBLC + valorDiarioComissao + valorDiarioJuros;
                }
            }

            if (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraTodas ||
                    (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.NaoApura))
            {
                #region Evento de BTC_Doado_VariacaoPos
                if (variacaoPositivoDoador != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_Doado_VariacaoPos;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = variacaoPositivoDoador;

                        contabLancamento.Save();
                    }
                }
                #endregion

                #region Evento de BTC_Doado_VariacaoPos_Reversao
                if (variacaoPositivoDoador != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_Doado_VariacaoPos_Reversao;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = dataProxima;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = variacaoPositivoDoador;

                        contabLancamento.Save();
                    }
                }
                #endregion

                #region Evento de BTC_Doado_VariacaoNeg
                if (variacaoNegativoDoador != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_Doado_VariacaoNeg;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = variacaoNegativoDoador;

                        contabLancamento.Save();
                    }
                }
                #endregion

                #region Evento de BTC_Doado_VariacaoNeg_Reversao
                if (variacaoNegativoDoador != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_Doado_VariacaoNeg_Reversao;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = dataProxima;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = variacaoNegativoDoador;

                        contabLancamento.Save();
                    }
                }
                #endregion

                #region Evento de BTC_Tomado_VariacaoPos
                if (variacaoPositivoTomador != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_Tomado_VariacaoPos;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = variacaoPositivoTomador;

                        contabLancamento.Save();
                    }
                }
                #endregion

                #region Evento de BTC_Tomado_VariacaoPos_Reversao
                if (variacaoPositivoTomador != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_Tomado_VariacaoPos_Reversao;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = dataProxima;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = variacaoPositivoTomador;

                        contabLancamento.Save();
                    }
                }
                #endregion

                #region Evento de BTC_Tomado_VariacaoNeg
                if (variacaoNegativoTomador != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_Tomado_VariacaoNeg;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = variacaoNegativoTomador;

                        contabLancamento.Save();
                    }
                }
                #endregion

                #region Evento de BTC_Tomado_VariacaoNeg_Reversao
                if (variacaoNegativoTomador != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_Tomado_VariacaoNeg_Reversao;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = dataProxima;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = variacaoNegativoTomador;

                        contabLancamento.Save();
                    }
                }
                #endregion
            }

            #region Evento de BTC_Doado_ProvTaxas
            if (provisaoTaxasDoador != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Doado_ProvTaxas;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoTaxasDoador;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de BTC_Tomado_ProvTaxas
            if (provisaoTaxasTomador != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Tomado_ProvTaxas;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoTaxasTomador;

                    contabLancamento.Save();
                }
            }
            #endregion
        }

        /// <summary>
        /// Eventos de BTC_Doado, BTC_Tomado, BTC_Doado_PagtoTaxas, BTC_Tomado_PagtoTaxas.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        /// <param name="dataProxima"></param>
        private void GeraEventosBTCLiquidacao(Cliente cliente, DateTime data, bool historico, DateTime dataProxima)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            LiquidacaoEmprestimoBolsaCollection liquidacaoEmprestimoBolsaCollection = new LiquidacaoEmprestimoBolsaCollection();
            liquidacaoEmprestimoBolsaCollection.Query.Select(liquidacaoEmprestimoBolsaCollection.Query.IdOperacao,
                                                             liquidacaoEmprestimoBolsaCollection.Query.IdPosicao,
                                                             liquidacaoEmprestimoBolsaCollection.Query.ValorLiquidacao,
                                                             liquidacaoEmprestimoBolsaCollection.Query.Quantidade,
                                                             liquidacaoEmprestimoBolsaCollection.Query.CdAtivoBolsa);
            liquidacaoEmprestimoBolsaCollection.Query.Where(liquidacaoEmprestimoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                            liquidacaoEmprestimoBolsaCollection.Query.Data.Equal(data));
            liquidacaoEmprestimoBolsaCollection.Query.Load();

            decimal valorLiquidadoDoado = 0;
            decimal valorLiquidadoTomado = 0;
            decimal valorDoadoDevolucao = 0;
            decimal valorTomadoDevolucao = 0;
            decimal variacaoDoadorPositiva = 0; //Para lançar a última variação do BTC doado no dia da liquidação
            decimal variacaoDoadorNegativa = 0; //Para lançar a última variação do BTC doado no dia da liquidação
            decimal provisaoTaxasDoador = 0; //Para lançar a provisão do dia da liquidação
            decimal provisaoTaxasTomador = 0; //Para lançar a provisão do dia da liquidação
            foreach (LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa in liquidacaoEmprestimoBolsaCollection)
            {
                decimal valor = liquidacaoEmprestimoBolsa.ValorLiquidacao.Value;
                int? idOperacao = liquidacaoEmprestimoBolsa.IdOperacao;
                int idPosicao = liquidacaoEmprestimoBolsa.IdPosicao.Value;
                decimal quantidade = liquidacaoEmprestimoBolsa.Quantidade.Value;
                string cdAtivoBolsa = liquidacaoEmprestimoBolsa.CdAtivoBolsa;

                byte? pontaPosicao = null;
                if (idOperacao.HasValue)
                {
                    OperacaoEmprestimoBolsa operacaoEmprestimoBolsaExiste = new OperacaoEmprestimoBolsa();
                    if (operacaoEmprestimoBolsaExiste.LoadByPrimaryKey(idOperacao.Value))
                    {
                        pontaPosicao = operacaoEmprestimoBolsaExiste.PontaEmprestimo;
                    }
                }

                if (pontaPosicao.HasValue)
                {
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                    CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                    bool existeCotacao = cotacaoBolsa.BuscaCotacaoBolsa(cdAtivoBolsa, data);

                    decimal puCotacaoFechamento = 0;
                    decimal valorMercado = 0;
                    if (existeCotacao)
                    {
                        puCotacaoFechamento = cotacaoBolsa.PUFechamento.Value;
                        valorMercado = Utilitario.Truncate((puCotacaoFechamento * quantidade) / fatorCotacaoBolsa.Fator.Value, 2);
                    }

                    PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsaAbertura = new PosicaoEmprestimoBolsaAbertura();
                    posicaoEmprestimoBolsaAbertura.Query.Select(posicaoEmprestimoBolsaAbertura.Query.PULiquidoOriginal,
                                                                posicaoEmprestimoBolsaAbertura.Query.ValorDiarioCBLC,
                                                                posicaoEmprestimoBolsaAbertura.Query.ValorDiarioComissao,
                                                                posicaoEmprestimoBolsaAbertura.Query.ValorDiarioJuros);
                    posicaoEmprestimoBolsaAbertura.Query.Where(posicaoEmprestimoBolsaAbertura.Query.DataHistorico.Equal(data),
                                                               posicaoEmprestimoBolsaAbertura.Query.IdPosicao.Equal(idPosicao));
                    posicaoEmprestimoBolsaAbertura.Query.Load();
                    
                    if (pontaPosicao.Value == (byte)PontaEmprestimoBolsa.Doador)
                    {
                        valorLiquidadoDoado += valor;
                        valorDoadoDevolucao += valorMercado;

                        if (posicaoEmprestimoBolsaAbertura.es.HasData)
                        {
                            provisaoTaxasDoador += posicaoEmprestimoBolsaAbertura.ValorDiarioJuros.Value;
                        }

                        #region Calcula o valor da variação do último dia (dia da liquidação), a ser jogada como última variação do BTC
                        if (posicaoEmprestimoBolsaAbertura.es.HasData)
                        {
                            decimal variacao = Utilitario.Truncate(valorMercado - (quantidade * posicaoEmprestimoBolsaAbertura.PULiquidoOriginal.Value / fatorCotacaoBolsa.Fator.Value), 2);

                            if (variacao > 0)
                            {
                                variacaoDoadorPositiva += variacao;
                            }
                            else
                            {
                                variacaoDoadorNegativa += variacao;
                            }
                        }
                        #endregion
                    }
                    else if (pontaPosicao.Value == (byte)PontaEmprestimoBolsa.Tomador)
                    {
                        valorLiquidadoTomado += valor;
                        valorTomadoDevolucao += valorMercado;

                        if (posicaoEmprestimoBolsaAbertura.es.HasData)
                        {
                            provisaoTaxasTomador += (posicaoEmprestimoBolsaAbertura.ValorDiarioJuros.Value + posicaoEmprestimoBolsaAbertura.ValorDiarioComissao.Value +
                                                    posicaoEmprestimoBolsaAbertura.ValorDiarioCBLC.Value);
                        }
                    }
                }
            }

            DateTime dataLiquidacao = new DateTime();
            if (valorLiquidadoDoado != 0 || valorLiquidadoTomado != 0)
            {
                dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            }
            
            #region Evento de BTC_Doado_ProvTaxas
            if (provisaoTaxasDoador != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Doado_ProvTaxas;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoTaxasDoador;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de BTC_Tomado_ProvTaxas
            if (provisaoTaxasTomador != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Tomado_ProvTaxas;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = provisaoTaxasTomador;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de BTC_Doado_PagtoTaxas
            if (valorLiquidadoDoado != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Doado_PagtoTaxas;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorLiquidadoDoado;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de BTC_Tomado_PagtoTaxas
            if (valorLiquidadoTomado != 0)
            {
                int origem = ContabilOrigem.Bolsa.BTC_Tomado_PagtoTaxas;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorLiquidadoTomado;

                    contabLancamento.Save();
                }
            }
            #endregion

                        

            if (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraTodas ||
                    (ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC == (int)CustodiaBTC.AlteraClientesSemIR && cliente.ApuraGanhoRV == TipoApuracaoGanhoRV.NaoApura))
            {
                ContabRoteiroCollection contabRoteiroCollectionBTC = new ContabRoteiroCollection();
                int idContaBTCDoado = 0;
                string contaBTCDoado = "";
                if (variacaoDoadorPositiva != 0 || variacaoDoadorNegativa != 0)
                {
                    contabRoteiroCollectionBTC.Query.Where(contabRoteiroCollectionBTC.Query.Origem.Equal((int)ContabilOrigem.Bolsa.BTC_Doado_VariacaoPos),
                                                           contabRoteiroCollectionBTC.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollectionBTC.Query.Load();

                    if (contabRoteiroCollectionBTC.Count > 0)
                    {
                        idContaBTCDoado = contabRoteiroCollectionBTC[0].IdContaDebito.Value;
                        contaBTCDoado = contabRoteiroCollectionBTC[0].ContaDebito;
                    }
                }

                #region Evento de BTC_DoadoDevolucao
                if (valorDoadoDevolucao != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_DoadoDevolucao;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorDoadoDevolucao;

                        contabLancamento.Save();
                    }
                }
                #endregion

                #region Evento de BTC_TomadoDevolucao
                if (valorTomadoDevolucao != 0)
                {
                    int origem = ContabilOrigem.Bolsa.BTC_TomadoDevolucao;

                    if (roteiroContabil.ContainsKey(origem))
                    {
                        int idEvento = roteiroContabil[origem];

                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorTomadoDevolucao;

                        contabLancamento.Save();
                    }
                }
                #endregion

                ContabRoteiroCollection contabRoteiroCollectionAcao = new ContabRoteiroCollection();
                contabRoteiroCollectionAcao.Query.Select(contabRoteiroCollectionAcao.Query.IdContaDebito,
                                                         contabRoteiroCollectionAcao.Query.ContaDebito);
                contabRoteiroCollectionAcao.Query.Where(contabRoteiroCollectionAcao.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollectionAcao.Query.Origem.Equal(ContabilOrigem.Bolsa.ValorizacaoAcao));
                contabRoteiroCollectionAcao.Query.Load();

                if (contabRoteiroCollectionAcao.Count > 0)
                {
                    #region Evento de BTC_Doado_VariacaoPos
                    if (variacaoDoadorPositiva != 0)
                    {
                        int origem = ContabilOrigem.Bolsa.BTC_Doado_VariacaoPos;

                        if (roteiroContabil.ContainsKey(origem))
                        {
                            int idEvento = roteiroContabil[origem];

                            ContabLancamento contabLancamento = new ContabLancamento();
                            contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                            contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                            contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                            contabLancamento.IdContaCredito = contabRoteiroCollectionAcao[0].IdContaDebito.Value;
                            contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                            contabLancamento.ContaCredito = contabRoteiroCollectionAcao[0].ContaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = origem;
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.Valor = variacaoDoadorPositiva;

                            contabLancamento.Save();
                        }
                    }
                    #endregion

                    #region Evento de BTC_Doado_VariacaoNeg
                    if (variacaoDoadorNegativa != 0)
                    {
                        int origem = ContabilOrigem.Bolsa.BTC_Doado_VariacaoNeg;

                        if (roteiroContabil.ContainsKey(origem))
                        {
                            int idEvento = roteiroContabil[origem];

                            ContabLancamento contabLancamento = new ContabLancamento();
                            contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                            contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                            contabLancamento.IdContaDebito = contabRoteiroCollectionAcao[0].IdContaDebito.Value;
                            contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                            contabLancamento.ContaDebito = contabRoteiroCollectionAcao[0].ContaDebito;
                            contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = origem;
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.Valor = variacaoDoadorNegativa;

                            contabLancamento.Save();
                        }
                    }
                    #endregion
                }

                //#region Evento de ReversaoValorizacaoAcao
                //if (variacaoDoadorPositiva != 0)
                //{
                //    int origem = ContabilOrigem.Bolsa.ReversaoValorizacaoAcao;

                //    if (roteiroContabil.ContainsKey(origem))
                //    {
                //        int idEvento = roteiroContabil[origem];
                        
                //        ContabLancamento contabLancamento = new ContabLancamento();
                //        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                //        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                //        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                //        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                //        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                //        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                //        contabLancamento.DataLancamento = dataProxima;
                //        contabLancamento.DataRegistro = data;
                //        contabLancamento.IdCliente = idCliente;
                //        contabLancamento.IdPlano = idPlano;
                //        contabLancamento.Origem = origem;
                //        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                //        contabLancamento.Valor = variacaoDoadorPositiva;

                //        contabLancamento.Save();
                //    }
                //}
                //#endregion

                //#region Evento de ReversaoDesvalorizacaoAcao
                //if (variacaoDoadorNegativa != 0)
                //{
                //    int origem = ContabilOrigem.Bolsa.ReversaoDesvalorizacaoAcao;

                //    if (roteiroContabil.ContainsKey(origem))
                //    {
                //        int idEvento = roteiroContabil[origem];

                //        ContabLancamento contabLancamento = new ContabLancamento();
                //        contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                //        contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                //        contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                //        contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                //        contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                //        contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                //        contabLancamento.DataLancamento = dataProxima;
                //        contabLancamento.DataRegistro = data;
                //        contabLancamento.IdCliente = idCliente;
                //        contabLancamento.IdPlano = idPlano;
                //        contabLancamento.Origem = origem;
                //        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                //        contabLancamento.Valor = variacaoDoadorNegativa;

                //        contabLancamento.Save();
                //    }
                //}
                //#endregion
            }
        }

        /// <summary>
        /// Eventos de CompraAcao, CompraAcaoCorretagem, CompraAcaoEmolumento, CompraAcaoTaxaCBLC, 
        /// VendaAcao, VendaAcaoCorretagem, VendaAcaoEmolumento, VendaAcaoTaxaCBLC, LucroAcao, PrejuizoAcao.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraOperacaoTermo(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            decimal totalPagar = 0;
            OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.Valor.Sum(),
                                     operacaoBolsa.Query.Corretagem.Sum(),
                                     operacaoBolsa.Query.Emolumento.Sum(),
                                     operacaoBolsa.Query.LiquidacaoCBLC.Sum(),
                                     operacaoBolsa.Query.RegistroBolsa.Sum(),
                                     operacaoBolsa.Query.RegistroCBLC.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoOperacao.In(TipoOperacaoBolsa.Compra),
                                    operacaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.Termo));
            operacaoBolsa.Query.Load();

            decimal valorCompra = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Valor.Value : 0;
            decimal corretagemCompra = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Corretagem.Value : 0;
            decimal emolumentoCompra = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Emolumento.Value + operacaoBolsa.RegistroBolsa.Value : 0;
            decimal taxasCBLCCompra = operacaoBolsa.Valor.HasValue ? operacaoBolsa.LiquidacaoCBLC.Value + operacaoBolsa.RegistroCBLC.Value : 0;

            totalPagar += corretagemCompra + emolumentoCompra + taxasCBLCCompra;

            operacaoBolsa = new OperacaoBolsa();
            operacaoBolsa.Query.Select(operacaoBolsa.Query.Valor.Sum(),
                                     operacaoBolsa.Query.Corretagem.Sum(),
                                     operacaoBolsa.Query.Emolumento.Sum(),
                                     operacaoBolsa.Query.LiquidacaoCBLC.Sum(),
                                     operacaoBolsa.Query.RegistroBolsa.Sum(),
                                     operacaoBolsa.Query.RegistroCBLC.Sum());
            operacaoBolsa.Query.Where(operacaoBolsa.Query.IdCliente.Equal(idCliente),
                                    operacaoBolsa.Query.Data.Equal(data),
                                    operacaoBolsa.Query.TipoOperacao.In(TipoOperacaoBolsa.Venda),
                                    operacaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.Termo));
            operacaoBolsa.Query.Load();

            decimal valorVenda = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Valor.Value : 0;
            decimal corretagemVenda = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Corretagem.Value : 0;
            decimal emolumentoVenda = operacaoBolsa.Valor.HasValue ? operacaoBolsa.Emolumento.Value + operacaoBolsa.RegistroBolsa.Value : 0;
            decimal taxasCBLCVenda = operacaoBolsa.Valor.HasValue ? operacaoBolsa.LiquidacaoCBLC.Value + operacaoBolsa.RegistroCBLC.Value : 0;

            totalPagar += corretagemVenda + emolumentoVenda + taxasCBLCVenda;
            
            #region Evento de CompraTermo
            if (valorCompra > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraTermo;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorCompra;
                }
            }
            #endregion

            #region Evento de CompraTermoCorretagem
            if (corretagemCompra > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraTermoCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;                    
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemCompra;
                }
            }
            #endregion

            #region Evento de CompraTermoEmolumento
            if (emolumentoCompra > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraTermoEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoCompra;
                }
            }
            #endregion

            #region Evento de CompraTermoTaxaCBLC
            if (taxasCBLCCompra > 0)
            {
                origem = ContabilOrigem.Bolsa.CompraTermoTaxaCBLC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = taxasCBLCCompra;
                }
            }
            #endregion

            #region Evento de VendaTermo
            if (valorVenda > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaTermo;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorVenda;
                }
            }
            #endregion

            #region Evento de VendaTermoCorretagem
            if (corretagemVenda > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaTermoCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemVenda;
                }
            }
            #endregion

            #region Evento VendaTermoEmolumento
            if (emolumentoVenda > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaTermoEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoVenda;
                }
            }
            #endregion

            #region Evento de VendaTermoTaxaCBLC
            if (taxasCBLCVenda > 0)
            {
                origem = ContabilOrigem.Bolsa.VendaTermoTaxaCBLC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = taxasCBLCVenda;
                }
            }
            #endregion
                        
            #region Evento de Liquidacao (Pagar)
            int idEventoLiquidacao = 0;
            if (totalPagar != 0)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.Bolsa.LiquidacaoBovespaPagar))
                {
                    idEventoLiquidacao = roteiroContabil[ContabilOrigem.Bolsa.LiquidacaoBovespaPagar];
                    origem = ContabilOrigem.Bolsa.LiquidacaoBovespaPagar;
                }
            }

            if (idEventoLiquidacao != 0)
            {
                DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.Acoes, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                contabLancamento.DataLancamento = dataLiquidacao;
                contabLancamento.DataRegistro = data;
                contabLancamento.IdCliente = idCliente;
                contabLancamento.IdPlano = idPlano;
                contabLancamento.Origem = origem;
                contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                contabLancamento.Valor = totalPagar;
            }
            #endregion
            

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de LiquidacaoFinanceiraTermoComprado, LiquidacaoFinanceiraTermoVendido.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraLiquidacaoFinanceiraTermo(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
            liquidacaoTermoBolsa.Query.Select(liquidacaoTermoBolsa.Query.Valor.Sum());
            liquidacaoTermoBolsa.Query.Where(liquidacaoTermoBolsa.Query.IdCliente.Equal(idCliente),
                                             liquidacaoTermoBolsa.Query.DataMovimento.Equal(data),
                                             liquidacaoTermoBolsa.Query.Quantidade.GreaterThan(0));
            liquidacaoTermoBolsa.Query.Load();

            decimal valorLiquidacaoComprado = liquidacaoTermoBolsa.Valor.HasValue ? liquidacaoTermoBolsa.Valor.Value : 0;

            liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
            liquidacaoTermoBolsa.Query.Select(liquidacaoTermoBolsa.Query.Valor.Sum());
            liquidacaoTermoBolsa.Query.Where(liquidacaoTermoBolsa.Query.IdCliente.Equal(idCliente),
                                             liquidacaoTermoBolsa.Query.DataMovimento.Equal(data),
                                             liquidacaoTermoBolsa.Query.Quantidade.LessThan(0));
            liquidacaoTermoBolsa.Query.Load();

            decimal valorLiquidacaoVendido = liquidacaoTermoBolsa.Valor.HasValue ? liquidacaoTermoBolsa.Valor.Value : 0;


            #region Evento de LiquidacaoFinanceiraTermoComprado
            if (valorLiquidacaoComprado > 0)
            {
                origem = ContabilOrigem.Bolsa.LiquidacaoFinanceiraTermoComprado;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorLiquidacaoComprado;
                }
            }
            #endregion

            #region Evento de LiquidacaoFinanceiraTermoVendido
            if (valorLiquidacaoVendido > 0)
            {
                origem = ContabilOrigem.Bolsa.LiquidacaoFinanceiraTermoVendido;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorLiquidacaoVendido;
                }
            }
            #endregion
            

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de LiquidacaoFisicaTermoComprado, LiquidacaoFisicaTermoVendido.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        private void GeraLiquidacaoFisicaTermo(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();

            LiquidacaoTermoBolsaCollection liquidacaoTermoBolsaCollection = new LiquidacaoTermoBolsaCollection();
            liquidacaoTermoBolsaCollection.Query.Select(liquidacaoTermoBolsaCollection.Query.CdAtivoBolsa,
                                                        liquidacaoTermoBolsaCollection.Query.Quantidade,
                                                        liquidacaoTermoBolsaCollection.Query.IdPosicao,
                                                        liquidacaoTermoBolsaCollection.Query.Pu);
            liquidacaoTermoBolsaCollection.Query.Where(liquidacaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                            liquidacaoTermoBolsaCollection.Query.DataMovimento.Equal(data));
            liquidacaoTermoBolsaCollection.Query.Load();
                        
            decimal liquidacaoTermoComprado = 0;
            decimal liquidacaoTermoVendido = 0;
            decimal variacaoPositivaTermoComprado = 0;
            decimal variacaoNegativaTermoComprado = 0;
            decimal variacaoPositivaTermoVendido = 0;
            decimal variacaoNegativaTermoVendido = 0;
            foreach (LiquidacaoTermoBolsa liquidacaoTermoBolsa in liquidacaoTermoBolsaCollection)
            {
                string cdAtivoBolsa = AtivoBolsa.RetornaCdAtivoBolsaAcao(liquidacaoTermoBolsa.CdAtivoBolsa);
                decimal quantidade = liquidacaoTermoBolsa.Quantidade.Value;
                int idPosicao = liquidacaoTermoBolsa.IdPosicao.Value;
                decimal puLiquidacao = liquidacaoTermoBolsa.Pu.Value;
                
                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data);

                decimal valorLiquidacao = Math.Abs(Utilitario.Truncate(quantidade * puLiquidacao / fatorCotacaoBolsa.Fator.Value, 2));

                if (quantidade > 0)
                {
                    liquidacaoTermoComprado += valorLiquidacao;
                }
                else
                {
                    liquidacaoTermoVendido += valorLiquidacao;
                }                

                // Dado que a saida do termo e entrada do a vista ocorre pelo valor de custo e não pelo de mercado, é necessário "reverter" a diferença do custo para o mercado de D-1
                // Para isto, calcula-se o MTM referente ao custo do termo vs mercado (do dia anterior) e lança-se um evento de MTM
                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                if (cotacaoBolsa.LoadByPrimaryKey(dataAnterior, cdAtivoBolsa))
                {
                    decimal variacao = Utilitario.Truncate(((puLiquidacao - cotacaoBolsa.PUFechamento.Value) * quantidade) / fatorCotacaoBolsa.Fator.Value, 2);

                    if (quantidade > 0)
                    {
                        if (variacao > 0)
                        {
                            variacaoPositivaTermoComprado += variacao;
                        }
                        else
                        {
                            variacaoNegativaTermoComprado += Math.Abs(variacao);
                        }
                    }
                    else
                    {
                        if (variacao > 0)
                        {
                            variacaoPositivaTermoVendido += variacao;
                        }
                        else
                        {
                            variacaoNegativaTermoVendido += Math.Abs(variacao);
                        }
                    }
                }
                
            }

            //Eventos de baixa do termo e entrada no a vista pelo valor de custo liquido do termo
            #region Evento de LiquidacaoFisicaTermoComprado
            if (liquidacaoTermoComprado != 0)
            {
                int origem = ContabilOrigem.Bolsa.LiquidacaoFisicaTermoComprado;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = liquidacaoTermoComprado;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de LiquidacaoFisicaTermoVendido
            if (liquidacaoTermoVendido != 0)
            {
                int origem = ContabilOrigem.Bolsa.LiquidacaoFisicaTermoVendido;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = liquidacaoTermoVendido;

                    contabLancamento.Save();
                }
            }
            #endregion            
            //***********************************************************************************

            //Eventos de MTM para baixar a diferença do custo do termo para o mercado de d-1
            #region Evento de ValorizacaoTermoComprado
            if (variacaoPositivaTermoComprado != 0)
            {
                int origem = ContabilOrigem.Bolsa.ValorizacaoTermoComprado;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoPositivaTermoComprado;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de DesvalorizacaoTermoComprado
            if (variacaoNegativaTermoComprado != 0)
            {
                int origem = ContabilOrigem.Bolsa.DesvalorizacaoTermoComprado;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoNegativaTermoComprado;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de ValorizacaoTermoVendido
            if (variacaoPositivaTermoVendido != 0)
            {
                int origem = ContabilOrigem.Bolsa.ValorizacaoTermoVendido;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoPositivaTermoVendido;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de DesvalorizacaoTermoVendido
            if (variacaoNegativaTermoVendido != 0)
            {
                int origem = ContabilOrigem.Bolsa.DesvalorizacaoTermoVendido;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = variacaoNegativaTermoVendido;

                    contabLancamento.Save();
                }
            }
            #endregion
            //***********************************************************************************
        }

        /// <summary>
        /// Eventos de ValorizacaoTermoComprado, DesvalorizacaoTermoComprado, ValorizacaoTermoVendido, DesvalorizacaoTermoVendido, JurosComprado, JurosVendido.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        private void GeraEventosTermoPosicao(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, (byte)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            PosicaoTermoBolsaCollection posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();

            #region Eventos de Termos Comprados
            if (historico)
            {
                PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
                posicaoTermoBolsaHistoricoCollection.Query.Select(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                                  posicaoTermoBolsaHistoricoCollection.Query.Quantidade,
                                                                  posicaoTermoBolsaHistoricoCollection.Query.PUMercado,
                                                                  posicaoTermoBolsaHistoricoCollection.Query.PUTermo,
                                                                  posicaoTermoBolsaHistoricoCollection.Query.DataOperacao);
                posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                                 posicaoTermoBolsaHistoricoCollection.Query.Quantidade.GreaterThan(0),
                                                                 posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoTermoBolsaHistoricoCollection.Query.Load();

                PosicaoTermoBolsaCollection posicaoTermoBolsaCollectionAux = new PosicaoTermoBolsaCollection(posicaoTermoBolsaHistoricoCollection);
                posicaoTermoBolsaCollection.Combine(posicaoTermoBolsaCollectionAux);
            }
            else
            {
                posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.CdAtivoBolsa,
                                                         posicaoTermoBolsaCollection.Query.Quantidade,
                                                         posicaoTermoBolsaCollection.Query.PUMercado,
                                                         posicaoTermoBolsaCollection.Query.PUTermo,
                                                         posicaoTermoBolsaCollection.Query.DataOperacao);
                posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                        posicaoTermoBolsaCollection.Query.Quantidade.GreaterThan(0));
                posicaoTermoBolsaCollection.Query.Load();                
            }
            
            decimal valorizacaoTermoComprado = 0;
            decimal desvalorizacaoTermoComprado = 0;
            decimal jurosTermoComprado = 0;            
            foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                decimal puMercado = posicaoTermoBolsa.PUMercado.Value;
                decimal puCusto = posicaoTermoBolsa.PUTermo.Value;
                DateTime dataOperacao = posicaoTermoBolsa.DataOperacao.Value;
                
                FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                if (fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                {
                    decimal variacao = 0;
                    if (dataOperacao != data)
                    {
                        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                        decimal cotacaoAnterior = cotacaoBolsa.RetornaCotacaoAjustada(AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa), dataAnterior, dataOperacao, true);

                        variacao = Utilitario.Truncate(((puMercado - cotacaoAnterior) * quantidade) / fatorCotacao.Fator.Value, 2);
                    }
                    else
                    {
                        jurosTermoComprado += Utilitario.Truncate(((puMercado - puCusto) * quantidade) / fatorCotacao.Fator.Value, 2);
                    }

                    if (variacao > 0)
                    {
                        valorizacaoTermoComprado += variacao;
                    }
                    else
                    {
                        desvalorizacaoTermoComprado += variacao;
                    }
                }
            }

            #region Evento de ValorizacaoTermoComprado
            if (valorizacaoTermoComprado != 0)
            {
                int origem = ContabilOrigem.Bolsa.ValorizacaoTermoComprado;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorizacaoTermoComprado;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de DesvalorizacaoTermoComprado
            if (desvalorizacaoTermoComprado != 0)
            {
                int origem = ContabilOrigem.Bolsa.DesvalorizacaoTermoComprado;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = desvalorizacaoTermoComprado;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de JurosTermoComprado
            if (jurosTermoComprado != 0)
            {
                int origem = ContabilOrigem.Bolsa.JurosTermoComprado;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = jurosTermoComprado;

                    contabLancamento.Save();
                }
            }
            #endregion
            #endregion

            posicaoTermoBolsaCollection = new PosicaoTermoBolsaCollection();
            #region Eventos de Termos Vendidos
            if (historico)
            {
                PosicaoTermoBolsaHistoricoCollection posicaoTermoBolsaHistoricoCollection = new PosicaoTermoBolsaHistoricoCollection();
                posicaoTermoBolsaHistoricoCollection.Query.Select(posicaoTermoBolsaHistoricoCollection.Query.CdAtivoBolsa,
                                                                  posicaoTermoBolsaHistoricoCollection.Query.Quantidade,
                                                                  posicaoTermoBolsaHistoricoCollection.Query.PUMercado,
                                                                  posicaoTermoBolsaHistoricoCollection.Query.PUTermoLiquido,
                                                                  posicaoTermoBolsaHistoricoCollection.Query.DataOperacao);
                posicaoTermoBolsaHistoricoCollection.Query.Where(posicaoTermoBolsaHistoricoCollection.Query.IdCliente.Equal(idCliente),
                                                                 posicaoTermoBolsaHistoricoCollection.Query.Quantidade.LessThan(0),
                                                                 posicaoTermoBolsaHistoricoCollection.Query.DataHistorico.Equal(data));
                posicaoTermoBolsaHistoricoCollection.Query.Load();

                PosicaoTermoBolsaCollection posicaoTermoBolsaCollectionAux = new PosicaoTermoBolsaCollection(posicaoTermoBolsaHistoricoCollection);
                posicaoTermoBolsaCollection.Combine(posicaoTermoBolsaCollectionAux);
            }
            else
            {
                posicaoTermoBolsaCollection.Query.Select(posicaoTermoBolsaCollection.Query.CdAtivoBolsa,
                                                         posicaoTermoBolsaCollection.Query.Quantidade,
                                                         posicaoTermoBolsaCollection.Query.PUMercado,
                                                         posicaoTermoBolsaCollection.Query.PUTermoLiquido,
                                                         posicaoTermoBolsaCollection.Query.DataOperacao);
                posicaoTermoBolsaCollection.Query.Where(posicaoTermoBolsaCollection.Query.IdCliente.Equal(idCliente),
                                                        posicaoTermoBolsaCollection.Query.Quantidade.LessThan(0));
                posicaoTermoBolsaCollection.Query.Load();
            }

            decimal valorizacaoTermoVendido = 0;
            decimal desvalorizacaoTermoVendido = 0;
            decimal jurosTermoVendido = 0;
            foreach (PosicaoTermoBolsa posicaoTermoBolsa in posicaoTermoBolsaCollection)
            {
                string cdAtivoBolsa = posicaoTermoBolsa.CdAtivoBolsa;
                decimal quantidade = posicaoTermoBolsa.Quantidade.Value;
                decimal puMercado = posicaoTermoBolsa.PUMercado.Value;
                decimal puCusto = posicaoTermoBolsa.PUTermoLiquido.Value;
                DateTime dataOperacao = posicaoTermoBolsa.DataOperacao.Value;

                FatorCotacaoBolsa fatorCotacao = new FatorCotacaoBolsa();
                if (fatorCotacao.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data))
                {
                    decimal variacao = 0;
                    if (dataOperacao != data)
                    {
                        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                        decimal cotacaoAnterior = cotacaoBolsa.RetornaCotacaoAjustada(AtivoBolsa.RetornaCdAtivoBolsaAcao(cdAtivoBolsa), dataAnterior, dataOperacao, true);

                        variacao = Utilitario.Truncate(((puMercado - cotacaoAnterior) * quantidade) / fatorCotacao.Fator.Value, 2);
                    }
                    else
                    {
                        jurosTermoVendido += Utilitario.Truncate(((puMercado - puCusto) * quantidade) / fatorCotacao.Fator.Value, 2);
                    }

                    if (variacao > 0)
                    {
                        valorizacaoTermoVendido += variacao;
                    }
                    else
                    {
                        desvalorizacaoTermoVendido += variacao;
                    }
                }
            }

            #region Evento de ValorizacaoTermoVendido
            if (valorizacaoTermoVendido != 0)
            {
                int origem = ContabilOrigem.Bolsa.ValorizacaoTermoVendido;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorizacaoTermoVendido;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de DesvalorizacaoTermoVendido
            if (desvalorizacaoTermoVendido != 0)
            {
                int origem = ContabilOrigem.Bolsa.DesvalorizacaoTermoVendido;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = desvalorizacaoTermoVendido;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de JurosTermoVendido
            if (jurosTermoVendido != 0)
            {
                int origem = ContabilOrigem.Bolsa.JurosTermoVendido;

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = jurosTermoVendido;

                    contabLancamento.Save();
                }
            }
            #endregion
            #endregion
        }

        /// <summary>
        /// Gera Ajuste contábil para cravar as posições do contábil com a carteira.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        public void GeraAjusteContabilAcoes(Cliente cliente, DateTime data)
        {
            bool historico = cliente.DataDia.Value > data;

            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            decimal valorMercado = 0;
            if (historico)
            {
                PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.Imobiliario, TipoMercadoBolsa.MercadoVista),
                                         posicaoBolsa.Query.DataHistorico.Equal(data));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }
            else
            {
                PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.Imobiliario, TipoMercadoBolsa.MercadoVista));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }

            ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
            contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito,
                                                 contabRoteiroCollection.Query.ContaDebito,
                                                 contabRoteiroCollection.Query.IdContaCredito,
                                                 contabRoteiroCollection.Query.ContaCredito);
            contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoAcao),
                                                contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
            contabRoteiroCollection.Query.Load();

            if (contabRoteiroCollection.Count > 0)
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                {
                    decimal diferenca = valorMercado - contabSaldo.SaldoFinal.Value;

                    if (Math.Abs(diferenca) < 0.10M) //Tolerância até 10 centavos
                    {
                        if (diferenca > 0)
                        {
                            ContabLancamento contabLancamento = new ContabLancamento();
                            contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                            contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.Descricao = "Ajuste Valor Mercado Ações";
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                            contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = (int)ContabilOrigem.Bolsa.ValorizacaoAcao;
                            contabLancamento.Valor = Math.Abs(diferenca);
                            contabLancamento.Save();

                            ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                            if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                            {
                                contabSaldoContrapartida.TotalCredito += Math.Abs(diferenca);
                                contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal + Math.Abs(diferenca);
                                contabSaldoContrapartida.Save();
                            }
                        }
                        else if (diferenca < 0)
                        {
                            ContabRoteiroCollection contabRoteiroCollectionDespesa = new ContabRoteiroCollection();
                            contabRoteiroCollectionDespesa.Query.Select(contabRoteiroCollectionDespesa.Query.IdContaDebito,
                                                                        contabRoteiroCollectionDespesa.Query.ContaDebito,
                                                                        contabRoteiroCollectionDespesa.Query.IdContaCredito,
                                                                        contabRoteiroCollectionDespesa.Query.ContaCredito);
                            contabRoteiroCollectionDespesa.Query.Where(contabRoteiroCollectionDespesa.Query.Origem.Equal((int)ContabilOrigem.Bolsa.DesvalorizacaoAcao),
                                                                       contabRoteiroCollectionDespesa.Query.IdPlano.Equal(idPlano));
                            contabRoteiroCollectionDespesa.Query.Load();

                            if (contabRoteiroCollectionDespesa.Count > 0)
                            {
                                ContabLancamento contabLancamento = new ContabLancamento();
                                contabLancamento.ContaCredito = contabRoteiroCollectionDespesa[0].ContaCredito;
                                contabLancamento.ContaDebito = contabRoteiroCollectionDespesa[0].ContaDebito;
                                contabLancamento.DataLancamento = data;
                                contabLancamento.DataRegistro = data;
                                contabLancamento.Descricao = "Ajuste Valor Mercado Ações";
                                contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                                contabLancamento.IdCliente = idCliente;
                                contabLancamento.IdContaCredito = contabRoteiroCollectionDespesa[0].IdContaCredito.Value;
                                contabLancamento.IdContaDebito = contabRoteiroCollectionDespesa[0].IdContaDebito.Value;
                                contabLancamento.IdPlano = idPlano;
                                contabLancamento.Origem = (int)ContabilOrigem.Bolsa.DesvalorizacaoAcao;
                                contabLancamento.Valor = Math.Abs(diferenca);
                                contabLancamento.Save();

                                ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                                if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollectionDespesa[0].IdContaDebito.Value, data))
                                {
                                    contabSaldoContrapartida.TotalDebito += Math.Abs(diferenca);
                                    contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal - Math.Abs(diferenca);
                                    contabSaldoContrapartida.Save();
                                }
                            }
                        }

                        contabSaldo.SaldoFinal = valorMercado;
                        contabSaldo.Save();
                        
                    }


                }
            }
            
        }

        /// <summary>
        /// Gera Ajuste contábil para cravar as posições do contábil com a carteira.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        public void GeraAjusteContabilOPCComprada(Cliente cliente, DateTime data)
        {
            bool historico = cliente.DataDia.Value > data;

            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            decimal valorMercado = 0;
            if (historico)
            {
                PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra),
                                         posicaoBolsa.Query.DataHistorico.Equal(data),
                                         posicaoBolsa.Query.Quantidade.GreaterThan(0));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }
            else
            {
                PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra),
                                         posicaoBolsa.Query.Quantidade.GreaterThan(0));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }

            ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
            contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito,
                                                 contabRoteiroCollection.Query.ContaDebito,
                                                 contabRoteiroCollection.Query.IdContaCredito,
                                                 contabRoteiroCollection.Query.ContaCredito);
            contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoOPCComprada),
                                                contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
            contabRoteiroCollection.Query.Load();

            if (contabRoteiroCollection.Count > 0)
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                {
                    decimal diferenca = valorMercado - contabSaldo.SaldoFinal.Value;

                    if (Math.Abs(diferenca) < 0.10M) //Tolerância até 10 centavos
                    {
                        if (diferenca > 0)
                        {
                            ContabLancamento contabLancamento = new ContabLancamento();
                            contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                            contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.Descricao = "Ajuste Valor Mercado Opções de Compra Compradas";
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                            contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = (int)ContabilOrigem.Bolsa.ValorizacaoOPCComprada;
                            contabLancamento.Valor = Math.Abs(diferenca);
                            contabLancamento.Save();

                            ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                            if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                            {
                                contabSaldoContrapartida.TotalCredito += Math.Abs(diferenca);
                                contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal + Math.Abs(diferenca);
                                contabSaldoContrapartida.Save();
                            }
                        }
                        else if (diferenca < 0)
                        {
                            ContabRoteiroCollection contabRoteiroCollectionDespesa = new ContabRoteiroCollection();
                            contabRoteiroCollectionDespesa.Query.Select(contabRoteiroCollectionDespesa.Query.IdContaDebito,
                                                                        contabRoteiroCollectionDespesa.Query.ContaDebito,
                                                                        contabRoteiroCollectionDespesa.Query.IdContaCredito,
                                                                        contabRoteiroCollectionDespesa.Query.ContaCredito);
                            contabRoteiroCollectionDespesa.Query.Where(contabRoteiroCollectionDespesa.Query.Origem.Equal((int)ContabilOrigem.Bolsa.DesvalorizacaoOPCComprada),
                                                                       contabRoteiroCollectionDespesa.Query.IdPlano.Equal(idPlano));
                            contabRoteiroCollectionDespesa.Query.Load();

                            if (contabRoteiroCollectionDespesa.Count > 0)
                            {
                                ContabLancamento contabLancamento = new ContabLancamento();
                                contabLancamento.ContaCredito = contabRoteiroCollectionDespesa[0].ContaCredito;
                                contabLancamento.ContaDebito = contabRoteiroCollectionDespesa[0].ContaDebito;
                                contabLancamento.DataLancamento = data;
                                contabLancamento.DataRegistro = data;
                                contabLancamento.Descricao = "Ajuste Valor Mercado Opções de Compra Compradas";
                                contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                                contabLancamento.IdCliente = idCliente;
                                contabLancamento.IdContaCredito = contabRoteiroCollectionDespesa[0].IdContaCredito.Value;
                                contabLancamento.IdContaDebito = contabRoteiroCollectionDespesa[0].IdContaDebito.Value;
                                contabLancamento.IdPlano = idPlano;
                                contabLancamento.Origem = (int)ContabilOrigem.Bolsa.DesvalorizacaoOPCComprada;
                                contabLancamento.Valor = Math.Abs(diferenca);
                                contabLancamento.Save();
                            }

                            ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                            if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollectionDespesa[0].IdContaDebito.Value, data))
                            {
                                contabSaldoContrapartida.TotalDebito += Math.Abs(diferenca);
                                contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal - Math.Abs(diferenca);
                                contabSaldoContrapartida.Save();
                            }
                        }

                        contabSaldo.SaldoFinal = valorMercado;
                        contabSaldo.Save();
                    }


                }
            }
            
        }

        /// <summary>
        /// Gera Ajuste contábil para cravar as posições do contábil com a carteira.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        public void GeraAjusteContabilOPVComprada(Cliente cliente, DateTime data)
        {
            bool historico = cliente.DataDia.Value > data;

            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            decimal valorMercado = 0;
            if (historico)
            {
                PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoVenda),
                                         posicaoBolsa.Query.DataHistorico.Equal(data),
                                         posicaoBolsa.Query.Quantidade.GreaterThan(0));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }
            else
            {
                PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoVenda),
                                         posicaoBolsa.Query.Quantidade.GreaterThan(0));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }

            ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
            contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito,
                                                 contabRoteiroCollection.Query.ContaDebito,
                                                 contabRoteiroCollection.Query.IdContaCredito,
                                                 contabRoteiroCollection.Query.ContaCredito);
            contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoOPVComprada),
                                                contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
            contabRoteiroCollection.Query.Load();

            if (contabRoteiroCollection.Count > 0)
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                {
                    decimal diferenca = valorMercado - contabSaldo.SaldoFinal.Value;

                    if (Math.Abs(diferenca) < 0.10M) //Tolerância até 10 centavos
                    {
                        if (diferenca > 0)
                        {
                            ContabLancamento contabLancamento = new ContabLancamento();
                            contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                            contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.Descricao = "Ajuste Valor Mercado Opções de Venda Compradas";
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                            contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = (int)ContabilOrigem.Bolsa.ValorizacaoOPVComprada;
                            contabLancamento.Valor = Math.Abs(diferenca);
                            contabLancamento.Save();

                            ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                            if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                            {
                                contabSaldoContrapartida.TotalCredito += Math.Abs(diferenca);
                                contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal + Math.Abs(diferenca);
                                contabSaldoContrapartida.Save();
                            }
                        }
                        else if (diferenca < 0)
                        {
                            ContabRoteiroCollection contabRoteiroCollectionDespesa = new ContabRoteiroCollection();
                            contabRoteiroCollectionDespesa.Query.Select(contabRoteiroCollectionDespesa.Query.IdContaDebito,
                                                                        contabRoteiroCollectionDespesa.Query.ContaDebito,
                                                                        contabRoteiroCollectionDespesa.Query.IdContaCredito,
                                                                        contabRoteiroCollectionDespesa.Query.ContaCredito);
                            contabRoteiroCollectionDespesa.Query.Where(contabRoteiroCollectionDespesa.Query.Origem.Equal((int)ContabilOrigem.Bolsa.DesvalorizacaoOPVComprada),
                                                                       contabRoteiroCollectionDespesa.Query.IdPlano.Equal(idPlano));
                            contabRoteiroCollectionDespesa.Query.Load();

                            if (contabRoteiroCollectionDespesa.Count > 0)
                            {
                                ContabLancamento contabLancamento = new ContabLancamento();
                                contabLancamento.ContaCredito = contabRoteiroCollectionDespesa[0].ContaCredito;
                                contabLancamento.ContaDebito = contabRoteiroCollectionDespesa[0].ContaDebito;
                                contabLancamento.DataLancamento = data;
                                contabLancamento.DataRegistro = data;
                                contabLancamento.Descricao = "Ajuste Valor Mercado Opções de Venda Compradas";
                                contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                                contabLancamento.IdCliente = idCliente;
                                contabLancamento.IdContaCredito = contabRoteiroCollectionDespesa[0].IdContaCredito.Value;
                                contabLancamento.IdContaDebito = contabRoteiroCollectionDespesa[0].IdContaDebito.Value;
                                contabLancamento.IdPlano = idPlano;
                                contabLancamento.Origem = (int)ContabilOrigem.Bolsa.DesvalorizacaoOPVComprada;
                                contabLancamento.Valor = Math.Abs(diferenca);
                                contabLancamento.Save();
                            }

                            ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                            if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollectionDespesa[0].IdContaDebito.Value, data))
                            {
                                contabSaldoContrapartida.TotalDebito += Math.Abs(diferenca);
                                contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal - Math.Abs(diferenca);
                                contabSaldoContrapartida.Save();
                            }
                        }

                        contabSaldo.SaldoFinal = valorMercado;
                        contabSaldo.Save();
                    }


                }
            }

        }

        /// <summary>
        /// Gera Ajuste contábil para cravar as posições do contábil com a carteira.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        public void GeraAjusteContabilOPCVendida(Cliente cliente, DateTime data)
        {
            bool historico = cliente.DataDia.Value > data;

            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            decimal valorMercado = 0;
            if (historico)
            {
                PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra),
                                         posicaoBolsa.Query.DataHistorico.Equal(data),
                                         posicaoBolsa.Query.Quantidade.LessThan(0));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }
            else
            {
                PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoCompra),
                                         posicaoBolsa.Query.Quantidade.LessThan(0));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }

            valorMercado = Math.Abs(valorMercado);

            ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
            contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito,
                                                 contabRoteiroCollection.Query.ContaDebito,
                                                 contabRoteiroCollection.Query.IdContaCredito,
                                                 contabRoteiroCollection.Query.ContaCredito);
            contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.DesvalorizacaoOPCVendida),
                                                contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
            contabRoteiroCollection.Query.Load();

            if (contabRoteiroCollection.Count > 0)
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                {
                    decimal diferenca = valorMercado - contabSaldo.SaldoFinal.Value;

                    if (Math.Abs(diferenca) < 0.10M) //Tolerância até 10 centavos
                    {
                        if (diferenca > 0)
                        {
                            ContabLancamento contabLancamento = new ContabLancamento();
                            contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                            contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.Descricao = "Ajuste Valor Mercado Opções de Compra Vendidas";
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                            contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = (int)ContabilOrigem.Bolsa.DesvalorizacaoOPCVendida;
                            contabLancamento.Valor = Math.Abs(diferenca);
                            contabLancamento.Save();

                            ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                            if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                            {
                                contabSaldoContrapartida.TotalDebito += Math.Abs(diferenca);
                                contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal - Math.Abs(diferenca);
                                contabSaldoContrapartida.Save();
                            }
                        }
                        else if (diferenca < 0)
                        {
                            ContabRoteiroCollection contabRoteiroCollectionReceita = new ContabRoteiroCollection();
                            contabRoteiroCollectionReceita.Query.Select(contabRoteiroCollectionReceita.Query.IdContaCredito,
                                                                        contabRoteiroCollectionReceita.Query.ContaCredito,
                                                                        contabRoteiroCollectionReceita.Query.IdContaDebito,
                                                                        contabRoteiroCollectionReceita.Query.ContaDebito);
                            contabRoteiroCollectionReceita.Query.Where(contabRoteiroCollectionReceita.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoOPCVendida),
                                                                       contabRoteiroCollectionReceita.Query.IdPlano.Equal(idPlano));
                            contabRoteiroCollectionReceita.Query.Load();

                            if (contabRoteiroCollectionReceita.Count > 0)
                            {
                                ContabLancamento contabLancamento = new ContabLancamento();
                                contabLancamento.ContaCredito = contabRoteiroCollectionReceita[0].ContaCredito;
                                contabLancamento.ContaDebito = contabRoteiroCollectionReceita[0].ContaDebito;
                                contabLancamento.DataLancamento = data;
                                contabLancamento.DataRegistro = data;
                                contabLancamento.Descricao = "Ajuste Valor Mercado Opções de Compra Vendidas";
                                contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                                contabLancamento.IdCliente = idCliente;
                                contabLancamento.IdContaCredito = contabRoteiroCollectionReceita[0].IdContaCredito.Value;
                                contabLancamento.IdContaDebito = contabRoteiroCollectionReceita[0].IdContaDebito.Value;
                                contabLancamento.IdPlano = idPlano;
                                contabLancamento.Origem = (int)ContabilOrigem.Bolsa.ValorizacaoOPCVendida;
                                contabLancamento.Valor = Math.Abs(diferenca);
                                contabLancamento.Save();

                                ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                                if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollectionReceita[0].IdContaCredito.Value, data))
                                {
                                    contabSaldoContrapartida.TotalCredito += Math.Abs(diferenca);
                                    contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal + Math.Abs(diferenca);
                                    contabSaldoContrapartida.Save();
                                }
                            }
                        }

                        contabSaldo.SaldoFinal = Math.Abs(valorMercado);
                        contabSaldo.Save();
                    }


                }
            }

        }

        /// <summary>
        /// Gera Ajuste contábil para cravar as posições do contábil com a carteira.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        public void GeraAjusteContabilOPVVendida(Cliente cliente, DateTime data)
        {
            bool historico = cliente.DataDia.Value > data;

            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            decimal valorMercado = 0;
            if (historico)
            {
                PosicaoBolsaHistorico posicaoBolsa = new PosicaoBolsaHistorico();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoVenda),
                                         posicaoBolsa.Query.DataHistorico.Equal(data),
                                         posicaoBolsa.Query.Quantidade.LessThan(0));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }
            else
            {
                PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
                posicaoBolsa.Query.Select(posicaoBolsa.Query.ValorMercado.Sum());
                posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                         posicaoBolsa.Query.TipoMercado.In(TipoMercadoBolsa.OpcaoVenda),
                                         posicaoBolsa.Query.Quantidade.LessThan(0));
                posicaoBolsa.Query.Load();

                valorMercado = posicaoBolsa.ValorMercado.HasValue ? posicaoBolsa.ValorMercado.Value : 0;
            }

            valorMercado = Math.Abs(valorMercado);

            ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
            contabRoteiroCollection.Query.Select(contabRoteiroCollection.Query.IdContaDebito,
                                                 contabRoteiroCollection.Query.ContaDebito,
                                                 contabRoteiroCollection.Query.IdContaCredito,
                                                 contabRoteiroCollection.Query.ContaCredito);
            contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal((int)ContabilOrigem.Bolsa.DesvalorizacaoOPVVendida),
                                                contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
            contabRoteiroCollection.Query.Load();

            if (contabRoteiroCollection.Count > 0)
            {
                ContabSaldo contabSaldo = new ContabSaldo();
                if (contabSaldo.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaCredito.Value, data))
                {
                    decimal diferenca = valorMercado - contabSaldo.SaldoFinal.Value;

                    if (Math.Abs(diferenca) < 0.10M) //Tolerância até 10 centavos
                    {
                        if (diferenca > 0)
                        {
                            ContabLancamento contabLancamento = new ContabLancamento();
                            contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                            contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.Descricao = "Ajuste Valor Mercado Opções de Venda Vendidas";
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                            contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = (int)ContabilOrigem.Bolsa.DesvalorizacaoOPVVendida;
                            contabLancamento.Valor = Math.Abs(diferenca);
                            contabLancamento.Save();

                            ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                            if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollection[0].IdContaDebito.Value, data))
                            {
                                contabSaldoContrapartida.TotalDebito += Math.Abs(diferenca);
                                contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal - Math.Abs(diferenca);
                                contabSaldoContrapartida.Save();
                            }
                        }
                        else if (diferenca < 0)
                        {
                            ContabRoteiroCollection contabRoteiroCollectionReceita = new ContabRoteiroCollection();
                            contabRoteiroCollectionReceita.Query.Select(contabRoteiroCollectionReceita.Query.IdContaCredito,
                                                                        contabRoteiroCollectionReceita.Query.ContaCredito,
                                                                        contabRoteiroCollectionReceita.Query.IdContaDebito,
                                                                        contabRoteiroCollectionReceita.Query.ContaDebito);
                            contabRoteiroCollectionReceita.Query.Where(contabRoteiroCollectionReceita.Query.Origem.Equal((int)ContabilOrigem.Bolsa.ValorizacaoOPVVendida),
                                                                       contabRoteiroCollectionReceita.Query.IdPlano.Equal(idPlano));
                            contabRoteiroCollectionReceita.Query.Load();

                            if (contabRoteiroCollectionReceita.Count > 0)
                            {
                                ContabLancamento contabLancamento = new ContabLancamento();
                                contabLancamento.ContaCredito = contabRoteiroCollectionReceita[0].ContaCredito;
                                contabLancamento.ContaDebito = contabRoteiroCollectionReceita[0].ContaDebito;
                                contabLancamento.DataLancamento = data;
                                contabLancamento.DataRegistro = data;
                                contabLancamento.Descricao = "Ajuste Valor Mercado Opções de Venda Vendidas";
                                contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                                contabLancamento.IdCliente = idCliente;
                                contabLancamento.IdContaCredito = contabRoteiroCollectionReceita[0].IdContaCredito.Value;
                                contabLancamento.IdContaDebito = contabRoteiroCollectionReceita[0].IdContaDebito.Value;
                                contabLancamento.IdPlano = idPlano;
                                contabLancamento.Origem = (int)ContabilOrigem.Bolsa.ValorizacaoOPVVendida;
                                contabLancamento.Valor = Math.Abs(diferenca);
                                contabLancamento.Save();

                                ContabSaldo contabSaldoContrapartida = new ContabSaldo();
                                if (contabSaldoContrapartida.LoadByPrimaryKey(idCliente, contabRoteiroCollectionReceita[0].IdContaCredito.Value, data))
                                {
                                    contabSaldoContrapartida.TotalCredito += Math.Abs(diferenca);
                                    contabSaldoContrapartida.SaldoFinal = contabSaldoContrapartida.SaldoFinal + Math.Abs(diferenca);
                                    contabSaldoContrapartida.Save();
                                }
                            }
                        }

                        contabSaldo.SaldoFinal = Math.Abs(valorMercado);
                        contabSaldo.Save();
                    }


                }
            }

        }
        
    }
}
