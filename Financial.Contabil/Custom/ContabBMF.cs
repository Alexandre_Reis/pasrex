﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.Common.Enums;
using Financial.Util;
using Financial.Contabil.Enums;
using Financial.Investidor;
using Financial.ContaCorrente.Enums;

namespace Financial.Contabil
{
    public class ContabBMF
    {
        private Dictionary<int, int> roteiroContabil;
        private ContabRoteiroCollection contabRoteiroCollection;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roteiroContabil"></param>
        /// <param name="contabRoteiroCollection"></param>
        public ContabBMF(Dictionary<int, int> roteiroContabil, ContabRoteiroCollection contabRoteiroCollection)
        {
            this.roteiroContabil = roteiroContabil;
            this.contabRoteiroCollection = contabRoteiroCollection;
        }

        /// <summary>
        /// Executa todos os processos para geração dos eventos contábeis do componente de bmf.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        public void ExecutaProcesso(Cliente cliente, DateTime data)
        {
            bool historico = cliente.DataDia.Value > data;

            this.GeraValorizacaoOPCConsolidado(cliente, data, historico);
            this.GeraValorizacaoOPVConsolidado(cliente, data, historico);

            decimal valorNet = this.GeraValoresLiquidar(cliente, data);

            if (cliente.ContabilBMF.Value == (byte)TipoContabilBMF.Consolidado)
            {
                this.GeraOperacaoOPCConsolidado(cliente, data, valorNet);
                this.GeraOperacaoOPVConsolidado(cliente, data, valorNet);

                if (cliente.ContabilDaytrade.Value == (byte)TipoContabilDaytrade.Net)
                {
                    this.GeraDayTradeOpcao(cliente, data); //Somente OPC e OPV
                }
                else
                {
                    this.GeraDayTradeConsolidado(cliente, data); //Somente OPC e OPV
                }

                this.GeraOperacaoFuturoConsolidado(cliente, data, valorNet);
            }
            else
            {
                this.GeraOperacaoOPCAnalitico(cliente, data, valorNet);
                this.GeraOperacaoOPVAnalitico(cliente, data, valorNet);
                this.GeraDayTradeAnalitico(cliente, data); //Somente OPC e OPV
                this.GeraOperacaoFuturoAnalitico(cliente, data, valorNet);
            }

            this.GeraTaxaPermanencia(cliente, data);
        }

        /// <summary>
        /// Eventos de ValorizacaoOPCComprada, ReversaoValorizacaoOPCComprada, DesvalorizacaoOPCComprada, ReversaoDesvalorizacaoOPCComprada,
        /// ValorizacaoOPCVendida, ReversaoValorizacaoOPCVendida, DesvalorizacaoOPCVendida, ReversaoDesvalorizacaoOPCVendida
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        private void GeraValorizacaoOPCConsolidado(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            DateTime dataProxima = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();

            if (historico)
            {
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();

                PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
                AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
                posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.CdAtivoBMF,
                                                posicaoBMFHistoricoQuery.Serie,
                                                posicaoBMFHistoricoQuery.ValorCustoLiquido.Sum(),
                                                posicaoBMFHistoricoQuery.ValorMercado.Sum());
                posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCliente),
                                               posicaoBMFHistoricoQuery.Quantidade.GreaterThan(0),
                                               ativoBMFQuery.TipoSerie.Equal("C"));
                posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &&
                                                                     ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
                posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.CdAtivoBMF,
                                                 posicaoBMFHistoricoQuery.Serie);

                posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

                PosicaoBMFCollection posicaoBMFCollectionAux = new PosicaoBMFCollection(posicaoBMFHistoricoCollection);
                posicaoBMFCollection.Combine(posicaoBMFCollectionAux);
            }
            else
            {
                PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("P");
                AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
                posicaoBMFQuery.Select(posicaoBMFQuery.CdAtivoBMF,
                                       posicaoBMFQuery.Serie, 
                                       posicaoBMFQuery.ValorCustoLiquido.Sum(),
                                       posicaoBMFQuery.ValorMercado.Sum());
                posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idCliente),
                                      posicaoBMFQuery.Quantidade.GreaterThan(0),
                                      ativoBMFQuery.TipoSerie.Equal("C"));
                posicaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFQuery.CdAtivoBMF &&
                                                            ativoBMFQuery.Serie == posicaoBMFQuery.Serie);
                posicaoBMFQuery.GroupBy(posicaoBMFQuery.CdAtivoBMF,
                                       posicaoBMFQuery.Serie);

                posicaoBMFCollection.Load(posicaoBMFQuery);
            }
            
            decimal totalValorizacao = 0;
            decimal totalDesvalorizacao = 0;

            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                if (posicaoBMF.ValorMercado.HasValue)
                {
                    decimal valorizacao = Utilitario.Truncate((posicaoBMF.ValorMercado.Value - posicaoBMF.ValorCustoLiquido.Value), 2);

                    if (valorizacao > 0)
                    {
                        totalValorizacao += valorizacao;
                    }
                    else
                    {
                        totalDesvalorizacao += valorizacao;
                    }
                }                
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoOPCComprada
                origem = ContabilOrigem.BMF.ValorizacaoOPCComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoOPCComprada
                origem = ContabilOrigem.BMF.ReversaoValorizacaoOPCComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoOPCComprada
                origem = ContabilOrigem.BMF.DesvalorizacaoOPCComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoOPCComprada
                origem = ContabilOrigem.BMF.ReversaoDesvalorizacaoOPCComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion
            }

            posicaoBMFCollection = new PosicaoBMFCollection();

            if (historico)
            {
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();

                PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
                AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
                posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.CdAtivoBMF,
                                                posicaoBMFHistoricoQuery.Serie,
                                                posicaoBMFHistoricoQuery.ValorCustoLiquido.Sum(),
                                                posicaoBMFHistoricoQuery.ValorMercado.Sum());
                posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCliente),
                                               posicaoBMFHistoricoQuery.Quantidade.LessThan(0),
                                               ativoBMFQuery.TipoSerie.Equal("C"));
                posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &&
                                                                     ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
                posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.CdAtivoBMF,
                                                 posicaoBMFHistoricoQuery.Serie);

                posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

                PosicaoBMFCollection posicaoBMFCollectionAux = new PosicaoBMFCollection(posicaoBMFHistoricoCollection);
                posicaoBMFCollection.Combine(posicaoBMFCollectionAux);
            }
            else
            {
                PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("P");
                AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
                posicaoBMFQuery.Select(posicaoBMFQuery.CdAtivoBMF,
                                       posicaoBMFQuery.Serie,
                                       posicaoBMFQuery.ValorCustoLiquido.Sum(),
                                       posicaoBMFQuery.ValorMercado.Sum());
                posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idCliente),
                                      posicaoBMFQuery.Quantidade.LessThan(0),
                                      ativoBMFQuery.TipoSerie.Equal("C"));
                posicaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFQuery.CdAtivoBMF &&
                                                            ativoBMFQuery.Serie == posicaoBMFQuery.Serie);
                posicaoBMFQuery.GroupBy(posicaoBMFQuery.CdAtivoBMF,
                                       posicaoBMFQuery.Serie);

                posicaoBMFCollection.Load(posicaoBMFQuery);
            }

            totalValorizacao = 0;
            totalDesvalorizacao = 0;
            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                if (posicaoBMF.ValorMercado.HasValue)
                {
                    decimal valorizacao = Utilitario.Truncate((posicaoBMF.ValorMercado.Value - posicaoBMF.ValorCustoLiquido.Value), 2);

                    if (valorizacao > 0)
                    {
                        totalValorizacao += valorizacao;
                    }
                    else
                    {
                        totalDesvalorizacao += valorizacao;
                    }
                }
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoOPCVendida
                origem = ContabilOrigem.BMF.ValorizacaoOPCVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoOPCVendida
                origem = ContabilOrigem.BMF.ReversaoValorizacaoOPCVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoOPCVendida
                origem = ContabilOrigem.BMF.DesvalorizacaoOPCVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoOPCVendida
                origem = ContabilOrigem.BMF.ReversaoDesvalorizacaoOPCVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de ValorizacaoOPVComprada, ReversaoValorizacaoOPVComprada, DesvalorizacaoOPVComprada, ReversaoDesvalorizacaoOPVComprada,
        /// ValorizacaoOPVVendida, ReversaoValorizacaoOPVVendida, DesvalorizacaoOPVVendida, ReversaoDesvalorizacaoOPVVendida
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        private void GeraValorizacaoOPVConsolidado(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            DateTime dataProxima = Calendario.AdicionaDiaUtil(data, 1, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();

            if (historico)
            {
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();

                PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
                AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
                posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.CdAtivoBMF,
                                                posicaoBMFHistoricoQuery.Serie,
                                                posicaoBMFHistoricoQuery.ValorCustoLiquido.Sum(),
                                                posicaoBMFHistoricoQuery.ValorMercado.Sum());
                posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCliente),
                                               posicaoBMFHistoricoQuery.Quantidade.GreaterThan(0),
                                               ativoBMFQuery.TipoSerie.Equal("C"));
                posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &&
                                                                     ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
                posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.CdAtivoBMF,
                                                 posicaoBMFHistoricoQuery.Serie);

                posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

                PosicaoBMFCollection posicaoBMFCollectionAux = new PosicaoBMFCollection(posicaoBMFHistoricoCollection);
                posicaoBMFCollection.Combine(posicaoBMFCollectionAux);
            }
            else
            {
                PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("P");
                AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
                posicaoBMFQuery.Select(posicaoBMFQuery.CdAtivoBMF,
                                       posicaoBMFQuery.Serie,
                                       posicaoBMFQuery.ValorCustoLiquido.Sum(),
                                       posicaoBMFQuery.ValorMercado.Sum());
                posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idCliente),
                                      posicaoBMFQuery.Quantidade.GreaterThan(0),
                                      ativoBMFQuery.TipoSerie.Equal("C"));
                posicaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFQuery.CdAtivoBMF &&
                                                            ativoBMFQuery.Serie == posicaoBMFQuery.Serie);
                posicaoBMFQuery.GroupBy(posicaoBMFQuery.CdAtivoBMF,
                                       posicaoBMFQuery.Serie);

                posicaoBMFCollection.Load(posicaoBMFQuery);
            }

            decimal totalValorizacao = 0;
            decimal totalDesvalorizacao = 0;

            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                if (posicaoBMF.ValorMercado.HasValue)
                {
                    decimal valorizacao = Utilitario.Truncate((posicaoBMF.ValorMercado.Value - posicaoBMF.ValorCustoLiquido.Value), 2);

                    if (valorizacao > 0)
                    {
                        totalValorizacao += valorizacao;
                    }
                    else
                    {
                        totalDesvalorizacao += valorizacao;
                    }
                }
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoOPVComprada
                origem = ContabilOrigem.BMF.ValorizacaoOPVComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoOPVComprada
                origem = ContabilOrigem.BMF.ReversaoValorizacaoOPVComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoOPVComprada
                origem = ContabilOrigem.BMF.DesvalorizacaoOPVComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoOPVComprada
                origem = ContabilOrigem.BMF.ReversaoDesvalorizacaoOPVComprada;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion
            }

            posicaoBMFCollection = new PosicaoBMFCollection();

            if (historico)
            {
                PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();

                PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("P");
                AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
                posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.CdAtivoBMF,
                                                posicaoBMFHistoricoQuery.Serie,
                                                posicaoBMFHistoricoQuery.ValorCustoLiquido.Sum(),
                                                posicaoBMFHistoricoQuery.ValorMercado.Sum());
                posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente.Equal(idCliente),
                                               posicaoBMFHistoricoQuery.Quantidade.LessThan(0),
                                               ativoBMFQuery.TipoSerie.Equal("C"));
                posicaoBMFHistoricoQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFHistoricoQuery.CdAtivoBMF &&
                                                   ativoBMFQuery.Serie == posicaoBMFHistoricoQuery.Serie);
                posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.CdAtivoBMF,
                                                 posicaoBMFHistoricoQuery.Serie);

                posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

                PosicaoBMFCollection posicaoBMFCollectionAux = new PosicaoBMFCollection(posicaoBMFHistoricoCollection);
                posicaoBMFCollection.Combine(posicaoBMFCollectionAux);
            }
            else
            {
                PosicaoBMFQuery posicaoBMFQuery = new PosicaoBMFQuery("P");
                AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
                posicaoBMFQuery.Select(posicaoBMFQuery.CdAtivoBMF,
                                       posicaoBMFQuery.Serie,
                                       posicaoBMFQuery.ValorCustoLiquido.Sum(),
                                       posicaoBMFQuery.ValorMercado.Sum());
                posicaoBMFQuery.Where(posicaoBMFQuery.IdCliente.Equal(idCliente),
                                      posicaoBMFQuery.Quantidade.LessThan(0),
                                      ativoBMFQuery.TipoSerie.Equal("C"));
                posicaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == posicaoBMFQuery.CdAtivoBMF &&
                                          ativoBMFQuery.Serie == posicaoBMFQuery.Serie);
                posicaoBMFQuery.GroupBy(posicaoBMFQuery.CdAtivoBMF,
                                       posicaoBMFQuery.Serie);

                posicaoBMFCollection.Load(posicaoBMFQuery);
            }

            totalValorizacao = 0;
            totalDesvalorizacao = 0;
            foreach (PosicaoBMF posicaoBMF in posicaoBMFCollection)
            {
                if (posicaoBMF.ValorMercado.HasValue)
                {
                    decimal valorizacao = Utilitario.Truncate((posicaoBMF.ValorMercado.Value - posicaoBMF.ValorCustoLiquido.Value), 2);

                    if (valorizacao > 0)
                    {
                        totalValorizacao += valorizacao;
                    }
                    else
                    {
                        totalDesvalorizacao += valorizacao;
                    }
                }
            }

            if (totalValorizacao != 0)
            {
                #region Evento de ValorizacaoOPVVendida
                origem = ContabilOrigem.BMF.ValorizacaoOPVVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion

                #region Evento de ReversaoValorizacaoOPVVendida
                origem = ContabilOrigem.BMF.ReversaoValorizacaoOPVVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalValorizacao;
                }
                #endregion
            }

            if (totalDesvalorizacao != 0)
            {
                #region Evento de DesvalorizacaoOPVVendida
                origem = ContabilOrigem.BMF.DesvalorizacaoOPVVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion

                #region Evento de ReversaoDesvalorizacaoOPVVendida
                origem = ContabilOrigem.BMF.ReversaoDesvalorizacaoOPVVendida;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = dataProxima;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalDesvalorizacao;
                }
                #endregion
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de CompraOPC, CompraOPCCorretagem, CompraOPCEmolumento, CompraOPCRegistro, 
        /// VendaOPC, VendaOPCCorretagem, VendaOPCEmolumento, VendaOPCRegistro, LucroOPC, PrejuizoOPC.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="valorNet"></param>
        private void GeraOperacaoOPCConsolidado(Cliente cliente, DateTime data, decimal valorNet)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Valor.Sum(),
                                     operacaoBMFQuery.Corretagem.Sum(),
                                     operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                     operacaoBMFQuery.Emolumento.Sum(),
                                     operacaoBMFQuery.Registro.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),                                    
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    ativoBMFQuery.TipoSerie.Equal("C"));

            if (cliente.ContabilBMF.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBMFQuery.Where(operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Compra));
            }
            else
            {
                operacaoBMFQuery.Where(operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade)); 
            }

            OperacaoBMF operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            decimal valorCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Valor.Value : 0;
            decimal corretagemCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Corretagem.Value : 0;
            decimal taxaClearingCompra = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
            decimal emolumentoCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Emolumento.Value : 0;
            decimal registroCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Registro.Value : 0;


            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Valor.Sum(),
                                     operacaoBMFQuery.Corretagem.Sum(),
                                     operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                     operacaoBMFQuery.Emolumento.Sum(),
                                     operacaoBMFQuery.Registro.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    ativoBMFQuery.TipoSerie.Equal("C"));

            if (cliente.ContabilBMF.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBMFQuery.Where(operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Venda));
            }
            else
            {
                operacaoBMFQuery.Where(operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade));
            }

            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            decimal valorVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Valor.Value : 0;
            decimal corretagemVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Corretagem.Value : 0;
            decimal taxaClearingVenda = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
            decimal emolumentoVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Emolumento.Value : 0;
            decimal registroVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Registro.Value : 0;

            #region Ajusta contas crédito ou débito dos eventos de compra e venda, para jogar a conta de net a liquidar
            string contaCreditoReversao = "";
            string contaDebitoReversao = "";
            int idContaCreditoReversao = 0;
            int idContaDebitoReversao = 0;
            bool usaContaReversao = false;
            if (valorNet > 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFReceber))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFReceber];
                    contaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    idContaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito.Value;

                    usaContaReversao = true;
                }
            }
            if (valorNet < 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFPagar))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFPagar];
                    contaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    idContaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito.Value;

                    usaContaReversao = true;
                }
            }
            #endregion

            #region Evento de CompraOPC
            if (valorCompra > 0)
            {
                origem = ContabilOrigem.BMF.CompraOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;                    
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;                    
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorCompra;
                }
            }
            #endregion

            #region Evento de CompraOPCCorretagem
            if (corretagemCompra + taxaClearingCompra > 0)
            {
                origem = ContabilOrigem.BMF.CompraOPCCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemCompra + taxaClearingCompra;
                }
            }
            #endregion

            #region Evento de CompraOPCEmolumento
            if (emolumentoCompra > 0)
            {
                origem = ContabilOrigem.BMF.CompraOPCEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoCompra;
                }
            }
            #endregion

            #region Evento de CompraOPCRegistro
            if (registroCompra > 0)
            {
                origem = ContabilOrigem.BMF.CompraOPCRegistro;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = registroCompra;
                }
            }
            #endregion

            #region Evento de VendaOPC
            if (valorVenda > 0)
            {
                origem = ContabilOrigem.BMF.VendaOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;                    
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorVenda;
                }
            }
            #endregion

            #region Evento de VendaOPCCorretagem
            if (corretagemVenda + taxaClearingVenda > 0)
            {
                origem = ContabilOrigem.BMF.VendaOPCCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemVenda + taxaClearingVenda;
                }
            }
            #endregion

            #region Evento VendaOPCEmolumento
            if (emolumentoVenda > 0)
            {
                origem = ContabilOrigem.BMF.VendaOPCEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoVenda;
                }
            }
            #endregion

            #region Evento de VendaOPCRegistro
            if (registroVenda > 0)
            {
                origem = ContabilOrigem.BMF.VendaOPCRegistro;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = registroVenda;
                }
            }
            #endregion


            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Compra),
                                    operacaoBMFQuery.ResultadoRealizado.GreaterThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            if (operacaoBMF.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de LucroCompraOPC
                origem = ContabilOrigem.BMF.LucroCompraOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Compra),
                                    operacaoBMFQuery.ResultadoRealizado.LessThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            if (operacaoBMF.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de PrejuizoCompraOPC
                origem = ContabilOrigem.BMF.PrejuizoCompraOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Venda),
                                    operacaoBMFQuery.ResultadoRealizado.GreaterThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            if (operacaoBMF.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de LucroVendaOPC
                origem = ContabilOrigem.BMF.LucroVendaOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Venda),
                                    operacaoBMFQuery.ResultadoRealizado.LessThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            if (operacaoBMF.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de PrejuizoVendaOPC
                origem = ContabilOrigem.BMF.PrejuizoVendaOPC;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }


            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de CompraOPV, CompraOPVCorretagem, CompraOPVEmolumento, CompraOPVRegistro, 
        /// VendaOPV, VendaOPVCorretagem, VendaOPVEmolumento, VendaOPVRegistro, LucroOPV, PrejuizoOPV.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="valorNet"></param>
        private void GeraOperacaoOPVConsolidado(Cliente cliente, DateTime data, decimal valorNet)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Valor.Sum(),
                                     operacaoBMFQuery.Corretagem.Sum(),
                                     operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                     operacaoBMFQuery.Emolumento.Sum(),
                                     operacaoBMFQuery.Registro.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    ativoBMFQuery.TipoSerie.Equal("V"));

            if (cliente.ContabilBMF.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBMFQuery.Where(operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Compra));
            }
            else
            {
                operacaoBMFQuery.Where(operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade));
            }

            OperacaoBMF operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            decimal valorCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Valor.Value : 0;
            decimal corretagemCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Corretagem.Value : 0;
            decimal taxaClearingCompra = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
            decimal emolumentoCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Emolumento.Value : 0;
            decimal registroCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Registro.Value : 0;


            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Valor.Sum(),
                                     operacaoBMFQuery.Corretagem.Sum(),
                                     operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                     operacaoBMFQuery.Emolumento.Sum(),
                                     operacaoBMFQuery.Registro.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    ativoBMFQuery.TipoSerie.Equal("V"));

            if (cliente.ContabilBMF.Value == (byte)TipoContabilDaytrade.Net)
            {
                operacaoBMFQuery.Where(operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Venda));
            }
            else
            {
                operacaoBMFQuery.Where(operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade));
            }

            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            decimal valorVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Valor.Value : 0;
            decimal corretagemVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Corretagem.Value : 0;
            decimal taxaClearingVenda = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
            decimal emolumentoVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Emolumento.Value : 0;
            decimal registroVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Registro.Value : 0;

            #region Ajusta contas crédito ou débito dos eventos de compra e venda, para jogar a conta de net a liquidar
            string contaCreditoReversao = "";
            string contaDebitoReversao = "";
            int idContaCreditoReversao = 0;
            int idContaDebitoReversao = 0;
            bool usaContaReversao = false;
            if (valorNet > 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFReceber))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFReceber];
                    contaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    idContaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito.Value;

                    usaContaReversao = true;
                }
            }
            if (valorNet < 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFPagar))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFPagar];
                    contaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    idContaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito.Value;

                    usaContaReversao = true;
                }
            }
            #endregion

            #region Evento de CompraOPV
            if (valorCompra > 0)
            {
                origem = ContabilOrigem.BMF.CompraOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorCompra;
                }
            }
            #endregion

            #region Evento de CompraOPVCorretagem
            if (corretagemCompra + taxaClearingCompra > 0)
            {
                origem = ContabilOrigem.BMF.CompraOPVCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemCompra + taxaClearingCompra;
                }
            }
            #endregion

            #region Evento de CompraOPVEmolumento
            if (emolumentoCompra > 0)
            {
                origem = ContabilOrigem.BMF.CompraOPVEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoCompra;
                }
            }
            #endregion

            #region Evento de CompraOPVRegistro
            if (registroCompra > 0)
            {
                origem = ContabilOrigem.BMF.CompraOPVRegistro;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = registroCompra;
                }
            }
            #endregion

            #region Evento de VendaOPV
            if (valorVenda > 0)
            {
                origem = ContabilOrigem.BMF.VendaOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorVenda;
                }
            }
            #endregion

            #region Evento de VendaOPVCorretagem
            if (corretagemVenda + taxaClearingVenda > 0)
            {
                origem = ContabilOrigem.BMF.VendaOPVCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagemVenda + taxaClearingVenda;
                }
            }
            #endregion

            #region Evento VendaOPVEmolumento
            if (emolumentoVenda > 0)
            {
                origem = ContabilOrigem.BMF.VendaOPVEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumentoVenda;
                }
            }
            #endregion

            #region Evento de VendaOPVRegistro
            if (registroVenda > 0)
            {
                origem = ContabilOrigem.BMF.VendaOPVRegistro;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = registroVenda;
                }
            }
            #endregion


            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Compra),
                                    operacaoBMFQuery.ResultadoRealizado.GreaterThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            if (operacaoBMF.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de LucroCompraOPV
                origem = ContabilOrigem.BMF.LucroCompraOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Compra),
                                    operacaoBMFQuery.ResultadoRealizado.LessThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            if (operacaoBMF.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de PrejuizoCompraOPV
                origem = ContabilOrigem.BMF.PrejuizoCompraOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Venda),
                                    operacaoBMFQuery.ResultadoRealizado.GreaterThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            if (operacaoBMF.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de LucroVendaOPV
                origem = ContabilOrigem.BMF.LucroVendaOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Venda),
                                    operacaoBMFQuery.ResultadoRealizado.LessThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMF = new OperacaoBMF();
            operacaoBMF.Load(operacaoBMFQuery);

            if (operacaoBMF.ResultadoRealizado.HasValue)
            {
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de PrejuizoVendaOPV
                origem = ContabilOrigem.BMF.PrejuizoVendaOPV;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }


            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de LucroOpcaoDayTrade, PrejuizoOpcaoDayTrade, jogando em valores a liquidar contra resultado.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraDayTradeOpcao(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");            
            operacaoBMFQuery.Select(ativoBMFQuery.TipoSerie,
                                 operacaoBMFQuery.TipoOperacao,
                                 operacaoBMFQuery.Valor.Sum(),
                                 operacaoBMFQuery.Corretagem.Sum(),
                                 operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                 operacaoBMFQuery.Emolumento.Sum(),
                                 operacaoBMFQuery.Registro.Sum(),
                                 operacaoBMFQuery.Ajuste.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                operacaoBMFQuery.Data.Equal(data),
                                operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade),
                                operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Termo, TipoMercadoBMF.Futuro));
            operacaoBMFQuery.GroupBy(ativoBMFQuery.TipoSerie, operacaoBMFQuery.TipoOperacao);            

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            decimal resultadoDayTrade = 0;
            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                decimal valorLiquido = 0;                                
                if (operacaoBMF.TipoOperacao == TipoOperacaoBMF.CompraDaytrade)
                {
                    valorLiquido = -1 * (operacaoBMF.Valor.Value + operacaoBMF.Corretagem.Value  + operacaoBMF.TaxaClearingVlFixo.Value + operacaoBMF.Emolumento.Value + operacaoBMF.Registro.Value);
                }
                else
                {
                    valorLiquido = operacaoBMF.Valor.Value - operacaoBMF.Corretagem.Value - operacaoBMF.TaxaClearingVlFixo.Value - operacaoBMF.Emolumento.Value - operacaoBMF.Registro.Value;
                }

                resultadoDayTrade += valorLiquido;
            }
            
            #region Eventos LucroOPCDayTrade/PrejuizoOPCDayTrade
            if (resultadoDayTrade != 0)
            {
                if (resultadoDayTrade > 0)
                {
                    origem = ContabilOrigem.BMF.LucroOpcaoDayTrade;
                }
                else
                {
                    origem = ContabilOrigem.BMF.PrejuizoOpcaoDayTrade;
                }

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTrade;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Evento de Liquidacao (Pagar/Receber)
            int idEventoLiquidacao = 0;
            if (resultadoDayTrade > 0)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFReceber))
                {
                    idEventoLiquidacao = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFReceber];
                    origem = ContabilOrigem.BMF.LiquidacaoBMFReceber;
                }
            }
            else if (resultadoDayTrade < 0)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFPagar))
                {
                    idEventoLiquidacao = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFPagar];
                    origem = ContabilOrigem.BMF.LiquidacaoBMFPagar;
                }
            }

            if (resultadoDayTrade != 0)
            {
                DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.OpcoesBMF, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                ContabLancamento contabLancamento = new ContabLancamento();

                contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                contabLancamento.DataLancamento = dataLiquidacao;
                contabLancamento.DataRegistro = data;
                contabLancamento.IdCliente = idCliente;
                contabLancamento.IdPlano = idPlano;
                contabLancamento.Origem = origem;
                contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                contabLancamento.Valor = resultadoDayTrade;

                contabLancamento.Save();
            }
            #endregion

        }

        /// <summary>
        /// Eventos de LucroOPCDayTrade, PrejuizoOPCDayTrade, LucroOPVDayTrade, PrejuizoOPVDayTrade.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraDayTradeConsolidado(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(ativoBMFQuery.TipoSerie,
                                 operacaoBMFQuery.TipoOperacao,
                                 operacaoBMFQuery.Valor.Sum(),
                                 operacaoBMFQuery.Corretagem.Sum(),
                                 operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                 operacaoBMFQuery.Emolumento.Sum(),
                                 operacaoBMFQuery.Registro.Sum(),
                                 operacaoBMFQuery.Ajuste.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                operacaoBMFQuery.Data.Equal(data),
                                operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade),
                                operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Termo, TipoMercadoBMF.Futuro));
            operacaoBMFQuery.GroupBy(ativoBMFQuery.TipoSerie, operacaoBMFQuery.TipoOperacao);

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            decimal resultadoDayTradeOPC = 0;
            decimal resultadoDayTradeOPV = 0;
            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                decimal valorLiquido = 0;
                string tipoSerie = Convert.ToString(operacaoBMF.GetColumn(AtivoBMFMetadata.ColumnNames.TipoSerie));

                if (operacaoBMF.TipoOperacao == TipoOperacaoBMF.CompraDaytrade)
                {
                    valorLiquido = -1 * (operacaoBMF.Valor.Value + operacaoBMF.Corretagem.Value + operacaoBMF.TaxaClearingVlFixo.Value + operacaoBMF.Emolumento.Value + operacaoBMF.Registro.Value);
                }
                else
                {
                    valorLiquido = operacaoBMF.Valor.Value - operacaoBMF.Corretagem.Value - operacaoBMF.TaxaClearingVlFixo.Value - operacaoBMF.Emolumento.Value - operacaoBMF.Registro.Value;
                }

                switch (tipoSerie)
                {
                    case "C": //Opções de Compra
                        resultadoDayTradeOPC += valorLiquido;
                        break;
                    case "V": //Opções de Venda
                        resultadoDayTradeOPV += valorLiquido;
                        break;
                }
            }

            #region Eventos LucroOPCDayTrade/PrejuizoOPCDayTrade
            if (resultadoDayTradeOPC != 0)
            {
                int origem = 0;
                if (resultadoDayTradeOPC > 0)
                {
                    origem = ContabilOrigem.BMF.LucroOpcaoDayTrade;
                }
                else
                {
                    origem = ContabilOrigem.BMF.PrejuizoOpcaoDayTrade;
                }

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTradeOPC;

                    contabLancamento.Save();
                }
            }
            #endregion

            #region Eventos LucroOPVDayTrade/PrejuizoOPVDayTrade
            if (resultadoDayTradeOPV != 0)
            {
                int origem = 0;
                if (resultadoDayTradeOPV > 0)
                {
                    origem = ContabilOrigem.BMF.LucroOPVDayTrade;
                }
                else
                {
                    origem = ContabilOrigem.BMF.PrejuizoOPVDayTrade;
                }

                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = new ContabLancamento();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultadoDayTradeOPV;

                    contabLancamento.Save();
                }
            }
            #endregion

        }

        /// <summary>
        /// Eventos de AjustePositivo, AjusteNegativo, FuturoCorretagem, FuturoEmolumento, FuturoRegistro.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="valorNet"></param>
        private void GeraOperacaoFuturoConsolidado(Cliente cliente, DateTime data, decimal valorNet)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBMF operacaoBMF = new OperacaoBMF();
            operacaoBMF.Query.Select(operacaoBMF.Query.Ajuste.Sum(),
                                     operacaoBMF.Query.Corretagem.Sum(),
                                     operacaoBMF.Query.TaxaClearingVlFixo.Sum(),
                                     operacaoBMF.Query.Emolumento.Sum(),
                                     operacaoBMF.Query.Registro.Sum());
            operacaoBMF.Query.Where(operacaoBMF.Query.IdCliente.Equal(idCliente),
                                    operacaoBMF.Query.Data.Equal(data),
                                    operacaoBMF.Query.TipoMercado.Equal(TipoMercadoBMF.Futuro));
            operacaoBMF.Query.Load();

            decimal ajuste = operacaoBMF.Ajuste.HasValue ? operacaoBMF.Ajuste.Value : 0;
            decimal corretagem = operacaoBMF.Ajuste.HasValue ? operacaoBMF.Corretagem.Value : 0;
            decimal taxaClearing = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
            decimal emolumento = operacaoBMF.Ajuste.HasValue ? operacaoBMF.Emolumento.Value : 0;
            decimal registro = operacaoBMF.Ajuste.HasValue ? operacaoBMF.Registro.Value : 0;


            #region Ajusta contas crédito ou débito dos eventos de compra e venda, para jogar a conta de net a liquidar
            string contaCreditoReversao = "";
            int idContaCreditoReversao = 0;
            bool usaContaReversao = false;
            if (valorNet > 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFReceber))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFReceber];
                    contaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    idContaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito.Value;

                    usaContaReversao = true;
                }
            }
            #endregion

            #region Eventos de AjustePositivo/AjusteNegativo
            origem = 0;
            if (ajuste > 0)
            {
                origem = ContabilOrigem.BMF.AjustePositivo;
            }
            else if (ajuste < 0)
            {
                origem = ContabilOrigem.BMF.AjusteNegativo;
            }

            if (ajuste != 0)
            {
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = ajuste;
                }
            }
            #endregion

            #region Evento de FuturoCorretagem
            if (corretagem + taxaClearing > 0)
            {
                origem = ContabilOrigem.BMF.FuturoCorretagem;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;                    
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = corretagem + taxaClearing;
                }
            }
            #endregion

            #region Evento de FuturoEmolumento
            if (emolumento > 0)
            {
                origem = ContabilOrigem.BMF.FuturoEmolumento;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = emolumento;
                }
            }
            #endregion

            #region Evento de FuturoRegistro
            if (registro > 0)
            {
                origem = ContabilOrigem.BMF.FuturoRegistro;
                if (roteiroContabil.ContainsKey(origem))
                {
                    int idEvento = roteiroContabil[origem];

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEvento).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito;
                    contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = registro;
                }
            }
            #endregion


            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de LiquidacaoBMFReceber, LiquidacaoBMFPagar.
        /// Retorna o valorNet das operações.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private decimal GeraValoresLiquidar(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            decimal totalPagar = 0;
            decimal totalReceber = 0;
            decimal valorNet = 0;
            OperacaoBMF operacaoBMFTotal = new OperacaoBMF();
            operacaoBMFTotal.Query.Select(operacaoBMFTotal.Query.Ajuste.Sum(),
                                          operacaoBMFTotal.Query.Corretagem.Sum(),
                                          operacaoBMFTotal.Query.TaxaClearingVlFixo.Sum(),
                                          operacaoBMFTotal.Query.Emolumento.Sum(),
                                          operacaoBMFTotal.Query.Registro.Sum());
            operacaoBMFTotal.Query.Where(operacaoBMFTotal.Query.IdCliente.Equal(idCliente),
                                         operacaoBMFTotal.Query.Data.Equal(data),
                                         operacaoBMFTotal.Query.TipoMercado.Equal(TipoMercadoBMF.Futuro));
            operacaoBMFTotal.Query.Load();

            if (operacaoBMFTotal.Ajuste.HasValue)
            {
                decimal ajuste = operacaoBMFTotal.Ajuste.Value;
                decimal despesas = operacaoBMFTotal.Corretagem.Value + operacaoBMFTotal.TaxaClearingVlFixo.Value + operacaoBMFTotal.Emolumento.Value + operacaoBMFTotal.Registro.Value;
                if (operacaoBMFTotal.Ajuste.Value > 0)
                {
                    totalReceber += ajuste;
                    totalPagar += despesas;
                }
                else
                {
                    totalPagar += Math.Abs(ajuste) + Math.Abs(despesas);
                }
            }

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.Valor.Sum(),
                                              operacaoBMFCollection.Query.Corretagem.Sum(),
                                              operacaoBMFCollection.Query.TaxaClearingVlFixo.Sum(),
                                              operacaoBMFCollection.Query.Emolumento.Sum(),
                                              operacaoBMFCollection.Query.Registro.Sum(),
                                              operacaoBMFCollection.Query.TipoOperacao);
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente.Equal(idCliente),
                                              operacaoBMFCollection.Query.Data.Equal(data),
                                              operacaoBMFCollection.Query.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                              operacaoBMFCollection.Query.TipoOperacao.NotIn(TipoOperacaoBMF.Deposito, TipoOperacaoBMF.Retirada));
            operacaoBMFCollection.Query.GroupBy(operacaoBMFCollection.Query.TipoOperacao);
            operacaoBMFCollection.Query.Load();

            
            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                decimal despesas = operacaoBMF.Corretagem.Value + operacaoBMF.TaxaClearingVlFixo.Value + operacaoBMF.Emolumento.Value + operacaoBMF.Registro.Value;
                totalPagar += Math.Abs(despesas);

                if (operacaoBMF.TipoOperacao == TipoOperacaoBMF.Compra || operacaoBMF.TipoOperacao == TipoOperacaoBMF.CompraDaytrade)
                {
                    totalPagar += Math.Abs(operacaoBMF.Valor.Value);
                }
                else
                {
                    totalReceber += operacaoBMF.Valor.Value;
                }                
            }

            valorNet = totalReceber - totalPagar;

            if (cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                #region Evento de Liquidacao (Pagar/Receber)
                int idEventoLiquidacao = 0;
                if (valorNet > 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFReceber))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFReceber];
                        origem = ContabilOrigem.BMF.LiquidacaoBMFReceber;
                    }
                }
                if (valorNet < 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFPagar))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFPagar];
                        origem = ContabilOrigem.BMF.LiquidacaoBMFPagar;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.FuturosBMF, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorNet;
                }
                #endregion
            }
            else
            {
                #region Evento de Liquidacao (Receber)
                int idEventoLiquidacao = 0;
                if (totalReceber != 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFReceber))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFReceber];
                        origem = ContabilOrigem.BMF.LiquidacaoBMFReceber;
                    }
                }                

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.FuturosBMF, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalReceber;
                }
                #endregion

                #region Evento de Liquidacao (Pagar)
                idEventoLiquidacao = 0;
                if (totalPagar != 0)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFPagar))
                    {
                        idEventoLiquidacao = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFPagar];
                        origem = ContabilOrigem.BMF.LiquidacaoBMFPagar;
                    }
                }

                if (idEventoLiquidacao != 0)
                {
                    DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, LiquidacaoMercado.FuturosBMF, (int)LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection.FindByPrimaryKey(idEventoLiquidacao).ContaCredito;
                    contabLancamento.DataLancamento = dataLiquidacao;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = totalPagar;
                }
                #endregion
            }


            contabLancamentoCollection.Save();

            return valorNet;
        }

        /// <summary>
        /// Eventos de CompraOPC, CompraOPCCorretagem, CompraOPCEmolumento, CompraOPCRegistro, 
        /// VendaOPC, VendaOPCCorretagem, VendaOPCEmolumento, VendaOPCRegistro, LucroOPC, PrejuizoOPC.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="valorNet"></param>
        private void GeraOperacaoOPCAnalitico(Cliente cliente, DateTime data, decimal valorNet)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"
            
            #region Ajusta contas crédito ou débito dos eventos de compra e venda, para jogar a conta de net a liquidar
            string contaCreditoReversao = "";
            string contaDebitoReversao = "";
            int idContaCreditoReversao = 0;
            int idContaDebitoReversao = 0;
            bool usaContaReversao = false;
            if (valorNet > 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFReceber))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFReceber];
                    contaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    idContaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito.Value;

                    usaContaReversao = true;
                }
            }
            if (valorNet < 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFPagar))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFPagar];
                    contaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    idContaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito.Value;

                    usaContaReversao = true;
                }
            }
            #endregion

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Valor.Sum(),
                                     operacaoBMFQuery.Corretagem.Sum(),
                                     operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                     operacaoBMFQuery.Emolumento.Sum(),
                                     operacaoBMFQuery.Registro.Sum(),
                                     operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal valorCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Valor.Value : 0;
                decimal corretagemCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Corretagem.Value : 0;
                decimal taxaClearingCompra = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
                decimal emolumentoCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Emolumento.Value : 0;
                decimal registroCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Registro.Value : 0;

                #region Evento de CompraOPC
                if (valorCompra > 0)
                {
                    origem = ContabilOrigem.BMF.CompraOPC;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;                        
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorCompra;
                    }
                }
                #endregion

                #region Evento de CompraOPCCorretagem
                if (corretagemCompra + taxaClearingCompra > 0)
                {
                    origem = ContabilOrigem.BMF.CompraOPCCorretagem;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = corretagemCompra + taxaClearingCompra;
                    }
                }
                #endregion

                #region Evento de CompraOPCEmolumento
                if (emolumentoCompra > 0)
                {
                    origem = ContabilOrigem.BMF.CompraOPCEmolumento;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = emolumentoCompra;
                    }
                }
                #endregion

                #region Evento de CompraOPCRegistro
                if (registroCompra > 0)
                {
                    origem = ContabilOrigem.BMF.CompraOPCRegistro;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = registroCompra;
                    }
                }
                #endregion
            }


            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Valor.Sum(),
                                     operacaoBMFQuery.Corretagem.Sum(),
                                     operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                     operacaoBMFQuery.Emolumento.Sum(),
                                     operacaoBMFQuery.Registro.Sum(),
                                     operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal valorVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Valor.Value : 0;
                decimal corretagemVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Corretagem.Value : 0;
                decimal taxaClearingVenda = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
                decimal emolumentoVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Emolumento.Value : 0;
                decimal registroVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Registro.Value : 0;

                #region Evento de VendaOPC
                if (valorVenda > 0)
                {
                    origem = ContabilOrigem.BMF.VendaOPC;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorVenda;
                    }
                }
                #endregion

                #region Evento de VendaOPCCorretagem
                if (corretagemVenda + taxaClearingVenda > 0)
                {
                    origem = ContabilOrigem.BMF.VendaOPCCorretagem;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = corretagemVenda + taxaClearingVenda;
                    }
                }
                #endregion

                #region Evento VendaOPCEmolumento
                if (emolumentoVenda > 0)
                {
                    origem = ContabilOrigem.BMF.VendaOPCEmolumento;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = emolumentoVenda;
                    }
                }
                #endregion

                #region Evento de VendaOPCRegistro
                if (registroVenda > 0)
                {
                    origem = ContabilOrigem.BMF.VendaOPCRegistro;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = registroVenda;
                    }
                }
                #endregion
            }


            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum(),
                                    operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Compra),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    operacaoBMFQuery.ResultadoRealizado.GreaterThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de LucroCompraOPC
                origem = ContabilOrigem.BMF.LucroCompraOPC;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum(),
                                    operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Compra),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    operacaoBMFQuery.ResultadoRealizado.LessThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de PrejuizoCompraOPC
                origem = ContabilOrigem.BMF.PrejuizoCompraOPC;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum(),
                                    operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Venda),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    operacaoBMFQuery.ResultadoRealizado.GreaterThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de LucroVendaOPC
                origem = ContabilOrigem.BMF.LucroVendaOPC;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum(),
                                    operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Venda),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    operacaoBMFQuery.ResultadoRealizado.LessThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("C"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de PrejuizoVendaOPC
                origem = ContabilOrigem.BMF.PrejuizoVendaOPC;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }


            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de CompraOPV, CompraOPVCorretagem, CompraOPVEmolumento, CompraOPVRegistro, 
        /// VendaOPV, VendaOPVCorretagem, VendaOPVEmolumento, VendaOPVRegistro, LucroOPV, PrejuizoOPV.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="valorNet"></param>
        private void GeraOperacaoOPVAnalitico(Cliente cliente, DateTime data, decimal valorNet)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            #region Ajusta contas crédito ou débito dos eventos de compra e venda, para jogar a conta de net a liquidar
            string contaCreditoReversao = "";
            string contaDebitoReversao = "";
            int idContaCreditoReversao = 0;
            int idContaDebitoReversao = 0;
            bool usaContaReversao = false;
            if (valorNet > 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFReceber))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFReceber];
                    contaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                    idContaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito.Value;

                    usaContaReversao = true;
                }
            }
            if (valorNet < 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
            {
                if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFPagar))
                {
                    int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFPagar];
                    contaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaDebito;
                    idContaDebitoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaDebito.Value;

                    usaContaReversao = true;
                }
            }
            #endregion

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Valor.Sum(),
                                     operacaoBMFQuery.Corretagem.Sum(),
                                     operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                     operacaoBMFQuery.Emolumento.Sum(),
                                     operacaoBMFQuery.Registro.Sum(),
                                     operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Compra, TipoOperacaoBMF.CompraDaytrade),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal valorCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Valor.Value : 0;
                decimal corretagemCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Corretagem.Value : 0;
                decimal taxaClearingCompra = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
                decimal emolumentoCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Emolumento.Value : 0;
                decimal registroCompra = operacaoBMF.Valor.HasValue ? operacaoBMF.Registro.Value : 0;

                #region Evento de CompraOPV
                if (valorCompra > 0)
                {
                    origem = ContabilOrigem.BMF.CompraOPV;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorCompra;
                    }
                }
                #endregion

                #region Evento de CompraOPVCorretagem
                if (corretagemCompra + taxaClearingCompra > 0)
                {
                    origem = ContabilOrigem.BMF.CompraOPVCorretagem;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = corretagemCompra + taxaClearingCompra ;
                    }
                }
                #endregion

                #region Evento de CompraOPVEmolumento
                if (emolumentoCompra > 0)
                {
                    origem = ContabilOrigem.BMF.CompraOPVEmolumento;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = emolumentoCompra;
                    }
                }
                #endregion

                #region Evento de CompraOPVRegistro
                if (registroCompra > 0)
                {
                    origem = ContabilOrigem.BMF.CompraOPVRegistro;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = registroCompra;
                    }
                }
                #endregion
            }


            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.Valor.Sum(),
                                     operacaoBMFQuery.Corretagem.Sum(),
                                     operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                     operacaoBMFQuery.Emolumento.Sum(),
                                     operacaoBMFQuery.Registro.Sum(),
                                     operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.Venda, TipoOperacaoBMF.VendaDaytrade),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal valorVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Valor.Value : 0;
                decimal corretagemVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Corretagem.Value : 0;
                decimal taxaClearingVenda = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
                decimal emolumentoVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Emolumento.Value : 0;
                decimal registroVenda = operacaoBMF.Valor.HasValue ? operacaoBMF.Registro.Value : 0;

                #region Evento de VendaOPV
                if (valorVenda > 0)
                {
                    origem = ContabilOrigem.BMF.VendaOPV;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valorVenda;
                    }
                }
                #endregion

                #region Evento de VendaOPVCorretagem
                if (corretagemVenda + taxaClearingVenda > 0)
                {
                    origem = ContabilOrigem.BMF.VendaOPVCorretagem;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = corretagemVenda + taxaClearingVenda;
                    }
                }
                #endregion

                #region Evento VendaOPVEmolumento
                if (emolumentoVenda > 0)
                {
                    origem = ContabilOrigem.BMF.VendaOPVEmolumento;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = emolumentoVenda;
                    }
                }
                #endregion

                #region Evento de VendaOPVRegistro
                if (registroVenda > 0)
                {
                    origem = ContabilOrigem.BMF.VendaOPVRegistro;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet < 0 && usaContaReversao) ? idContaDebitoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet < 0 && usaContaReversao) ? contaDebitoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = registroVenda;
                    }
                }
                #endregion
            }


            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum(),
                                    operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Compra),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    operacaoBMFQuery.ResultadoRealizado.GreaterThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de LucroCompraOPV
                origem = ContabilOrigem.BMF.LucroCompraOPV;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum(),
                                    operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Compra),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    operacaoBMFQuery.ResultadoRealizado.LessThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de PrejuizoCompraOPV
                origem = ContabilOrigem.BMF.PrejuizoCompraOPV;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum(),
                                    operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Venda),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    operacaoBMFQuery.ResultadoRealizado.GreaterThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de LucroVendaOPV
                origem = ContabilOrigem.BMF.LucroVendaOPV;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }

            operacaoBMFQuery = new OperacaoBMFQuery("O");
            ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(operacaoBMFQuery.ResultadoRealizado.Sum(),
                                    operacaoBMFQuery.CdAtivoBMF);
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                    operacaoBMFQuery.Data.Equal(data),
                                    operacaoBMFQuery.TipoOperacao.Equal(TipoOperacaoBMF.Venda),
                                    operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Futuro, TipoMercadoBMF.Termo),
                                    operacaoBMFQuery.ResultadoRealizado.LessThan(0),
                                    ativoBMFQuery.TipoSerie.Equal("V"));
            operacaoBMFQuery.GroupBy(operacaoBMFQuery.CdAtivoBMF);

            operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                decimal resultado = operacaoBMF.ResultadoRealizado.Value;

                #region Evento de PrejuizoVendaOPV
                origem = ContabilOrigem.BMF.PrejuizoVendaOPV;

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                    contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                    contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                    contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                    contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                    contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = resultado;
                }
                #endregion
            }


            contabLancamentoCollection.Save();
        }        

        /// <summary>
        /// Eventos de AjustePositivo, AjusteNegativo, FuturoCorretagem, FuturoEmolumento, FuturoRegistro.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="valorNet"></param>
        private void GeraOperacaoFuturoAnalitico(Cliente cliente, DateTime data, decimal valorNet)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;
            int origem = 0; //Inicialização "nula"

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.Ajuste.Sum(),
                                               operacaoBMFCollection.Query.Corretagem.Sum(),
                                               operacaoBMFCollection.Query.TaxaClearingVlFixo.Sum(),
                                               operacaoBMFCollection.Query.Emolumento.Sum(),
                                               operacaoBMFCollection.Query.Registro.Sum(),
                                               operacaoBMFCollection.Query.CdAtivoBMF);
            operacaoBMFCollection.Query.Where(operacaoBMFCollection.Query.IdCliente.Equal(idCliente),
                                              operacaoBMFCollection.Query.Data.Equal(data),
                                              operacaoBMFCollection.Query.TipoMercado.Equal(TipoMercadoBMF.Futuro));
            operacaoBMFCollection.Query.GroupBy(operacaoBMFCollection.Query.CdAtivoBMF);
            operacaoBMFCollection.Query.Load();

            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;

                decimal ajuste = operacaoBMF.Ajuste.HasValue ? operacaoBMF.Ajuste.Value : 0;
                decimal corretagem = operacaoBMF.Ajuste.HasValue ? operacaoBMF.Corretagem.Value : 0;
                decimal taxaClearing = operacaoBMF.TaxaClearingVlFixo.HasValue ? operacaoBMF.TaxaClearingVlFixo.Value : 0;
                decimal emolumento = operacaoBMF.Ajuste.HasValue ? operacaoBMF.Emolumento.Value : 0;
                decimal registro = operacaoBMF.Ajuste.HasValue ? operacaoBMF.Registro.Value : 0;
                

                #region Ajusta contas crédito ou débito dos eventos de compra e venda, para jogar a conta de net a liquidar
                string contaCreditoReversao = "";
                int idContaCreditoReversao = 0;
                bool usaContaReversao = false;
                if (valorNet > 0 && cliente.ContabilLiquidacao.Value == (byte)TipoContabilLiquidacao.Net)
                {
                    if (roteiroContabil.ContainsKey(ContabilOrigem.BMF.LiquidacaoBMFReceber))
                    {
                        int idEvento = roteiroContabil[ContabilOrigem.BMF.LiquidacaoBMFReceber];
                        contaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).ContaCredito;
                        idContaCreditoReversao = contabRoteiroCollection.FindByPrimaryKey(idEvento).IdContaCredito.Value;

                        usaContaReversao = true;
                    }
                }
                #endregion

                #region Eventos de AjustePositivo/AjusteNegativo
                origem = 0;
                if (ajuste > 0)
                {
                    origem = ContabilOrigem.BMF.AjustePositivo;
                }
                else if (ajuste < 0)
                {
                    origem = ContabilOrigem.BMF.AjusteNegativo;
                }

                if (ajuste != 0)
                {   
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = ajuste;
                    }
                }
                #endregion

                #region Evento de FuturoCorretagem
                if (corretagem + taxaClearing > 0)
                {
                    origem = ContabilOrigem.BMF.FuturoCorretagem;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = corretagem + taxaClearing;
                    }
                }
                #endregion

                #region Evento de FuturoEmolumento
                if (emolumento > 0)
                {
                    origem = ContabilOrigem.BMF.FuturoEmolumento;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = emolumento;
                    }
                }
                #endregion

                #region Evento de FuturoRegistro
                if (registro > 0)
                {
                    origem = ContabilOrigem.BMF.FuturoRegistro;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                        contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = (valorNet > 0 && usaContaReversao) ? idContaCreditoReversao : contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = (valorNet > 0 && usaContaReversao) ? contaCreditoReversao : contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = registro;
                    }
                }
                #endregion
            }


            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de LucroOPCDayTrade, PrejuizoOPCDayTrade, LucroOPVDayTrade, PrejuizoOPVDayTrade.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraDayTradeAnalitico(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoBMFQuery operacaoBMFQuery = new OperacaoBMFQuery("O");
            AtivoBMFQuery ativoBMFQuery = new AtivoBMFQuery("A");
            operacaoBMFQuery.Select(ativoBMFQuery.TipoSerie,
                                 operacaoBMFQuery.CdAtivoBMF,   
                                 operacaoBMFQuery.TipoOperacao,
                                 operacaoBMFQuery.Valor.Sum(),
                                 operacaoBMFQuery.Corretagem.Sum(),
                                 operacaoBMFQuery.Emolumento.Sum(),
                                 operacaoBMFQuery.TaxaClearingVlFixo.Sum(),
                                 operacaoBMFQuery.Registro.Sum());
            operacaoBMFQuery.InnerJoin(ativoBMFQuery).On(ativoBMFQuery.CdAtivoBMF == operacaoBMFQuery.CdAtivoBMF &&
                                                         ativoBMFQuery.Serie == operacaoBMFQuery.Serie);
            operacaoBMFQuery.Where(operacaoBMFQuery.IdCliente.Equal(idCliente),
                                operacaoBMFQuery.Data.Equal(data),
                                operacaoBMFQuery.TipoOperacao.In(TipoOperacaoBMF.CompraDaytrade, TipoOperacaoBMF.VendaDaytrade),
                                operacaoBMFQuery.TipoMercado.NotIn(TipoMercadoBMF.Termo, TipoMercadoBMF.Futuro));
            operacaoBMFQuery.GroupBy(ativoBMFQuery.TipoSerie, 
                                     operacaoBMFQuery.CdAtivoBMF, 
                                     operacaoBMFQuery.TipoOperacao);
            operacaoBMFQuery.OrderBy(ativoBMFQuery.TipoSerie.Ascending,
                                     operacaoBMFQuery.CdAtivoBMF.Ascending,
                                     operacaoBMFQuery.TipoOperacao.Ascending); //Traz ordenado para virem sempre as Compras DT antes das Vendas DT!!!!!

            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();
            operacaoBMFCollection.Load(operacaoBMFQuery);

            decimal valorLiquido = 0;
            foreach (OperacaoBMF operacaoBMF in operacaoBMFCollection)
            {
                string cdAtivoBMF = operacaoBMF.CdAtivoBMF;
                string tipoSerie = Convert.ToString(operacaoBMF.GetColumn(AtivoBMFMetadata.ColumnNames.TipoSerie));
                string tipoOperacao = operacaoBMF.TipoOperacao;

                //Artifício usado para não ter que fazer um loop interno para tratar apenas 2 registros (CD e VD)
                if (operacaoBMF.TipoOperacao == TipoOperacaoBMF.VendaDaytrade)
                {
                    valorLiquido += operacaoBMF.Valor.Value - operacaoBMF.Corretagem.Value - operacaoBMF.Emolumento.Value - operacaoBMF.Registro.Value - operacaoBMF.TaxaClearingVlFixo.Value;
                }
                else
                {
                    valorLiquido = -1 * (operacaoBMF.Valor.Value - operacaoBMF.Corretagem.Value - operacaoBMF.Emolumento.Value - operacaoBMF.Registro.Value - operacaoBMF.TaxaClearingVlFixo.Value);
                }

                if (tipoOperacao == TipoOperacaoBMF.VendaDaytrade) //Artifício usado para não ter que fazer um loop interno para tratar apenas 2 registros (CD e VD)
                {
                    #region Eventos LucroOpcaoDayTrade/PrejuizoOpcaoDayTrade
                    if (valorLiquido != 0)
                    {
                        int origem = 0;
                        if (valorLiquido > 0)
                        {
                            origem = ContabilOrigem.BMF.LucroOpcaoDayTrade;
                        }
                        else
                        {
                            origem = ContabilOrigem.BMF.PrejuizoOpcaoDayTrade;
                        }

                        ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                        contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                            contabRoteiroCollection.Query.IdPlano.Equal(idPlano),
                                                            contabRoteiroCollection.Query.Identificador.Equal(cdAtivoBMF));
                        contabRoteiroCollection.Query.Load();

                        if (contabRoteiroCollection.Count > 0)
                        {                    
                            ContabLancamento contabLancamento = new ContabLancamento();
                            contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                            contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                            contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                            contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                            contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                            contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = origem;
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.Valor = valorLiquido;

                            contabLancamento.Save();
                        }
                    }
                    #endregion
                }
            }

            

        }

        /// <summary>
        /// Eventos de ProvisaoTaxaPermanencia, PagamentoTaxaPermanencia.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraTaxaPermanencia(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            CustodiaBMFCollection custodiaBMFCollection = new CustodiaBMFCollection();
            custodiaBMFCollection.Query.Select(custodiaBMFCollection.Query.CdAtivoBMF,
                                               custodiaBMFCollection.Query.DataPagamento, 
                                               custodiaBMFCollection.Query.ValorPagar.Sum());
            custodiaBMFCollection.Query.Where(custodiaBMFCollection.Query.IdCliente.Equal(idCliente),
                                              custodiaBMFCollection.Query.Data.Equal(data));
            custodiaBMFCollection.Query.GroupBy(custodiaBMFCollection.Query.CdAtivoBMF,
                                                custodiaBMFCollection.Query.DataPagamento);
            custodiaBMFCollection.Query.Load();

            foreach (CustodiaBMF custodiaBMF in custodiaBMFCollection)
            {
                string cdAtivoBMF = custodiaBMF.CdAtivoBMF;
                decimal valor = custodiaBMF.ValorPagar.Value;
                DateTime dataPagamento = custodiaBMF.DataPagamento.Value;

                #region Eventos ProvisaoTaxaPermanencia
                if (valor != 0)
                {
                    int origem = ContabilOrigem.BMF.ProvisaoTaxaPermanencia;
                    
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valor;

                        contabLancamento.Save();
                    }


                    origem = ContabilOrigem.BMF.PagamentoTaxaPermanencia;

                    contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        ContabLancamento contabLancamento = new ContabLancamento();
                        contabLancamento.Descricao = contabRoteiroCollection[0].Descricao;
                        contabLancamento.IdCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        contabLancamento.IdContaDebito = contabRoteiroCollection[0].IdContaDebito;
                        contabLancamento.IdContaCredito = contabRoteiroCollection[0].IdContaCredito;
                        contabLancamento.ContaDebito = contabRoteiroCollection[0].ContaDebito;
                        contabLancamento.ContaCredito = contabRoteiroCollection[0].ContaCredito;
                        contabLancamento.DataLancamento = dataPagamento;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valor;

                        contabLancamento.Save();
                    }
                }
                #endregion
                
            }



        }
    }
}
