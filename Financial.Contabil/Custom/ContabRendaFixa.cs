﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Contabil.Enums;
using Financial.Util;

namespace Financial.Contabil
{
    public class ContabRendaFixa
    {
        /// <summary>
        /// Executa todos os processos para geração dos eventos contábeis relativos às despesas recorrentes, taxa adm e taxa pfee.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        public void ExecutaProcesso(Cliente cliente, DateTime data)
        {
            bool historico = cliente.DataDia.Value > data;

            this.GeraEventosOperacaoFinal(cliente, data);
            this.GeraEventosOperacaoCompromissada(cliente, data);
            this.GeraEventosRendimentoDiario(cliente, data, historico);
            this.GeraEventosRendDiarioCompromissada(cliente, data,historico);
            this.GeraEventosVencimento(cliente, data);
            this.GeraEventosLiquidacaoAgenda(cliente, data);
        }

        /// <summary>
        /// Eventos de CompraFinal, VendaFinal, LucroVendaFinal, PrejuizoVendaFinal.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraEventosOperacaoFinal(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();
            
            #region Eventos de CompraFinal
            int idPapel = 0;
            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            operacaoRendaFixaQuery.Select(tituloRendaFixaQuery.IdPapel,
                                          operacaoRendaFixaQuery.Valor.Sum());
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                         operacaoRendaFixaQuery.DataOperacao.Equal(data),
                                         operacaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                                         operacaoRendaFixaQuery.Valor.NotEqual(0),
                                         operacaoRendaFixaQuery.Quantidade.NotEqual(0));
            operacaoRendaFixaQuery.GroupBy(tituloRendaFixaQuery.IdPapel);
            operacaoRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdPapel.Ascending);
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);

            if (operacaoRendaFixaCollection.Count > 0)
            {
                foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
                {
                    idPapel = Convert.ToInt32(operacaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                    decimal valor = operacaoRendaFixa.Valor.Value;

                    int? idCentroCusto;
                    string descricao = "";
                    string contaDebito = "";
                    string contaCredito = "";
                    int idContaDebito = 0;
                    int idContaCredito = 0;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapel.ToString()),
                                                        contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.RendaFixa.CompraFinal),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;
                           
                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.RendaFixa.CompraFinal;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valor;
                        contabLancamento.Identificador = idPapel.ToString();
                    }
                }
            }
            #endregion

            #region Eventos de VendaFinal e VendaTotal ( juntas )
            idPapel = 0;
            operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            operacaoRendaFixaQuery.Select(tituloRendaFixaQuery.IdPapel,
                                          operacaoRendaFixaQuery.Valor.Sum());
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                         operacaoRendaFixaQuery.DataOperacao.Equal(data),
                                         operacaoRendaFixaQuery.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                                                (byte)TipoOperacaoTitulo.VendaTotal),
                                         operacaoRendaFixaQuery.Valor.NotEqual(0),
                                         operacaoRendaFixaQuery.Quantidade.NotEqual(0));
            operacaoRendaFixaQuery.GroupBy(tituloRendaFixaQuery.IdPapel);
            operacaoRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdPapel.Ascending);
            operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);

            if (operacaoRendaFixaCollection.Count > 0)
            {
                foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
                {
                    idPapel = Convert.ToInt32(operacaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                    decimal valor = operacaoRendaFixa.Valor.Value;

                    int? idCentroCusto;
                    string descricao = "";
                    string contaDebito = "";
                    string contaCredito = "";
                    int idContaDebito = 0;
                    int idContaCredito = 0;

                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapel.ToString()),
                                                        contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.RendaFixa.VendaFinal),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = ContabilOrigem.RendaFixa.VendaFinal;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = valor;
                        contabLancamento.Identificador = idPapel.ToString();
                    }
                }
            }
            #endregion

            #region Eventos de Renda e Lucro/Prej
            LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("L");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            liquidacaoRendaFixaQuery.Select(tituloRendaFixaQuery.IdPapel,
                                            liquidacaoRendaFixaQuery.IdPosicaoResgatada,
                                            liquidacaoRendaFixaQuery.PULiquidacao,
                                            liquidacaoRendaFixaQuery.Quantidade,
                                            liquidacaoRendaFixaQuery.ValorBruto);
            liquidacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == liquidacaoRendaFixaQuery.IdTitulo);
            liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                           liquidacaoRendaFixaQuery.DataLiquidacao.Equal(data),
                                           liquidacaoRendaFixaQuery.TipoLancamento.Equal((byte)TipoLancamentoLiquidacao.Venda),
                                           liquidacaoRendaFixaQuery.Quantidade.NotEqual(0));
            liquidacaoRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdPapel.Ascending);
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollection.Load(liquidacaoRendaFixaQuery);

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            decimal puOperacao = 0;
            if (liquidacaoRendaFixaCollection.Count > 0)
            {
                LiquidacaoRendaFixa liquidacaoRendaFixa = liquidacaoRendaFixaCollection[0];
                idPapel = Convert.ToInt32(liquidacaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                puOperacao = liquidacaoRendaFixa.PULiquidacao.Value;
            
                int i = 0;
                while (i < liquidacaoRendaFixaCollection.Count)
                {
                    int idPapelAnterior = idPapel;

                    decimal totalRendimento = 0;
                    decimal totalResultado = 0;
                    while ((idPapel == idPapelAnterior) && (i < liquidacaoRendaFixaCollection.Count))
                    {
                        decimal quantidadeBaixada = liquidacaoRendaFixa.Quantidade.Value;
                        int idPosicaoBaixada = liquidacaoRendaFixa.IdPosicaoResgatada.Value;

                        PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                        posicaoRendaFixaHistorico.Query.Select(posicaoRendaFixaHistorico.Query.PUCurva);
                        posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdCliente.Equal(idCliente),
                                                              posicaoRendaFixaHistorico.Query.DataHistorico.Equal(dataAnterior),
                                                              posicaoRendaFixaHistorico.Query.IdPosicao.Equal(idPosicaoBaixada));

                        PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = new PosicaoRendaFixaAbertura();
                        posicaoRendaFixaAbertura.Query.Select(posicaoRendaFixaAbertura.Query.PUCurva);
                        posicaoRendaFixaAbertura.Query.Where(posicaoRendaFixaAbertura.Query.IdCliente.Equal(idCliente),
                                                             posicaoRendaFixaAbertura.Query.DataHistorico.Equal(data),
                                                             posicaoRendaFixaAbertura.Query.IdPosicao.Equal(idPosicaoBaixada));

                        if (posicaoRendaFixaHistorico.Query.Load() && posicaoRendaFixaAbertura.Query.Load())
                        {
                            decimal puCurva = posicaoRendaFixaAbertura.PUCurva.Value;

                            decimal puAnterior = posicaoRendaFixaHistorico.PUCurva.Value;

                            decimal rendimento = Math.Round(quantidadeBaixada * puCurva, 2) - Math.Round(quantidadeBaixada * puAnterior, 2);

                            decimal resultado = Math.Round(quantidadeBaixada * puOperacao, 2) - Math.Round(quantidadeBaixada * puCurva, 2);

                            totalRendimento += rendimento;
                            totalResultado += resultado;
                        }

                        i += 1;
                        if (i < liquidacaoRendaFixaCollection.Count)
                        {
                            liquidacaoRendaFixa = liquidacaoRendaFixaCollection[i];
                            idPapel = Convert.ToInt32(liquidacaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                            puOperacao = liquidacaoRendaFixa.PULiquidacao.Value;
                        }
                    }

                    int? idCentroCusto;
                    string descricao = "";
                    string contaDebito = "";
                    string contaCredito = "";
                    int idContaDebito = 0;
                    int idContaCredito = 0;

                    #region Evento de RendaPositiva ou RendaNegativa
                    int origem = 0;
                    if (totalRendimento > 0)
                    {
                        origem = ContabilOrigem.RendaFixa.RendaPositiva;
                    }
                    else if (totalRendimento < 0)
                    {
                        origem = ContabilOrigem.RendaFixa.RendaNegativa;
                    }

                    if (totalRendimento != 0)
                    {
                        ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                        contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapelAnterior.ToString()),
                                                            contabRoteiroCollection.Query.Origem.Equal(origem),
                                                            contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                        contabRoteiroCollection.Query.Load();

                        if (contabRoteiroCollection.Count > 0 && totalRendimento != 0)
                        {
                            descricao = contabRoteiroCollection[0].Descricao;
                            contaDebito = contabRoteiroCollection[0].ContaDebito;
                            contaCredito = contabRoteiroCollection[0].ContaCredito;
                            idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                            idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                            idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                            ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                            contabLancamento.IdContaDebito = idContaDebito;
                            contabLancamento.IdContaCredito = idContaCredito;
                            contabLancamento.ContaCredito = contaCredito;
                            contabLancamento.ContaDebito = contaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.Descricao = descricao;
                            contabLancamento.IdCentroCusto = idCentroCusto;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = origem;
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.Valor = totalRendimento;
                            contabLancamento.Identificador = idPapelAnterior.ToString();
                        }
                    }
                    #endregion

                    #region Evento de Lucro ou Prejuizo
                    origem = 0;
                    if (totalResultado > 0)
                    {
                        origem = ContabilOrigem.RendaFixa.LucroVendaFinal;
                    }
                    else if (totalResultado < 0)
                    {
                        origem = ContabilOrigem.RendaFixa.PrejuizoVendaFinal;
                    }

                    if (totalResultado != 0)
                    {
                        ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                        contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapelAnterior.ToString()),
                                                            contabRoteiroCollection.Query.Origem.Equal(origem),
                                                            contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                        contabRoteiroCollection.Query.Load();

                        if (contabRoteiroCollection.Count > 0 && totalRendimento != 0)
                        {
                            descricao = contabRoteiroCollection[0].Descricao;
                            contaDebito = contabRoteiroCollection[0].ContaDebito;
                            contaCredito = contabRoteiroCollection[0].ContaCredito;
                            idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                            idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                            idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                            ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                            contabLancamento.IdContaDebito = idContaDebito;
                            contabLancamento.IdContaCredito = idContaCredito;
                            contabLancamento.ContaCredito = contaCredito;
                            contabLancamento.ContaDebito = contaDebito;
                            contabLancamento.DataLancamento = data;
                            contabLancamento.DataRegistro = data;
                            contabLancamento.Descricao = descricao;
                            contabLancamento.IdCentroCusto = idCentroCusto;
                            contabLancamento.IdCliente = idCliente;
                            contabLancamento.IdPlano = idPlano;
                            contabLancamento.Origem = origem;
                            contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                            contabLancamento.Valor = totalResultado;
                            contabLancamento.Identificador = idPapelAnterior.ToString();
                        }
                    }
                    #endregion
                }
            }
            #endregion

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de CompraCompromissada, Revenda, LucroRevenda.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraEventosOperacaoCompromissada(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            operacaoRendaFixaQuery.Select(tituloRendaFixaQuery.IdPapel,
                                          operacaoRendaFixaQuery.Valor,
                                          operacaoRendaFixaQuery.ValorVolta,
                                          operacaoRendaFixaQuery.DataVolta);
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == operacaoRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                         operacaoRendaFixaQuery.DataOperacao.Equal(data),
                                         operacaoRendaFixaQuery.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda),
                                         operacaoRendaFixaQuery.Valor.NotEqual(0),
                                         operacaoRendaFixaQuery.Quantidade.NotEqual(0),
                                         operacaoRendaFixaQuery.ValorVolta.NotEqual(0),
                                         operacaoRendaFixaQuery.ValorVolta.IsNotNull(),
                                         operacaoRendaFixaQuery.DataVolta.IsNotNull());
            operacaoRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdPapel.Ascending);
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Load(operacaoRendaFixaQuery);

            if (operacaoRendaFixaCollection.Count == 0)
            {
                return;
            }

            foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
            {
                int idPapel = Convert.ToInt32(operacaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                decimal valor = operacaoRendaFixa.Valor.Value;
                decimal valorVolta = operacaoRendaFixa.ValorVolta.Value;
                decimal rendimento = valorVolta - valor;
                DateTime dataVolta = operacaoRendaFixa.DataVolta.Value;
                
                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                #region Evento de CompraCompromissada
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapel.ToString()),
                                                    contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.RendaFixa.CompraCompromissada),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = ContabilOrigem.RendaFixa.CompraCompromissada;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valor;
                    contabLancamento.Identificador = idPapel.ToString();
                }
                #endregion

                #region Evento de Revenda
                contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapel.ToString()),
                                                    contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.RendaFixa.Revenda),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = dataVolta;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = ContabilOrigem.RendaFixa.Revenda;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valorVolta;
                    contabLancamento.Identificador = idPapel.ToString();
                }
                #endregion

                
                /*
                #region Evento de LucroRevenda
                contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapel.ToString()),
                                                    contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.RendaFixa.LucroRevenda),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    descricao = contabRoteiroCollection[0].Descricao;
                    contaDebito = contabRoteiroCollection[0].ContaDebito;
                    contaCredito = contabRoteiroCollection[0].ContaCredito;
                    idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = dataVolta;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = ContabilOrigem.RendaFixa.LucroRevenda;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = rendimento;
                    contabLancamento.Identificador = idPapel.ToString();
                }                 
                #endregion
                */
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de AjusteMTMPositivo/ReversaoAjusteMTMPositivo, AjusteMTMNegativo/ReversaoAjusteMTMNegativo, RendaPositiva, RendaNegativa.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        private void GeraEventosRendimentoDiario(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.CreateColumnsForBinding();
            posicaoRendaFixaCollection.AddColumn("IdPapel", typeof(System.Int32));
            

            if (historico)
            {
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                posicaoRendaFixaHistoricoQuery.Select(tituloRendaFixaQuery.IdPapel,
                                              posicaoRendaFixaHistoricoQuery.IdPosicao,
                                              posicaoRendaFixaHistoricoQuery.PUCurva,
                                              posicaoRendaFixaHistoricoQuery.PUMercado,
                                              posicaoRendaFixaHistoricoQuery.Quantidade,
                                              posicaoRendaFixaHistoricoQuery.ValorCurva,
                                              posicaoRendaFixaHistoricoQuery.ValorMercado);
                posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaHistoricoQuery.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraFinal),
                                                    posicaoRendaFixaHistoricoQuery.Quantidade.NotEqual(0),
                                                    posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(data));
                posicaoRendaFixaHistoricoQuery.OrderBy(tituloRendaFixaQuery.IdPapel.Ascending);
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);

                PosicaoRendaFixaCollection posicaoRendaFixaCollectionAux = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);
                posicaoRendaFixaCollectionAux.AddColumn("IdPapel", typeof(System.Int32));

                for (int j = 0; j < posicaoRendaFixaHistoricoCollection.Count; j++) {
                    posicaoRendaFixaCollectionAux[j].SetColumn("IdPapel", posicaoRendaFixaHistoricoCollection[j].GetColumn("IdPapel"));                    
                }

                //
                foreach (PosicaoRendaFixa posicaoRendaFixa1 in posicaoRendaFixaCollectionAux){
                    posicaoRendaFixaCollection.AttachEntity(posicaoRendaFixa1);                    
                }
            }
            else
            {
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                posicaoRendaFixaQuery.Select(tituloRendaFixaQuery.IdPapel,
                                              posicaoRendaFixaQuery.IdPosicao,
                                              posicaoRendaFixaQuery.PUCurva,
                                              posicaoRendaFixaQuery.PUMercado,
                                              posicaoRendaFixaQuery.Quantidade,
                                              posicaoRendaFixaQuery.ValorCurva,
                                              posicaoRendaFixaQuery.ValorMercado);
                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraFinal),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0));
                posicaoRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdPapel.Ascending);                
                posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
            }
            
            if (posicaoRendaFixaCollection.Count == 0)
            {
                return;
            }

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
            DateTime dataProxima = Calendario.AdicionaDiaUtil(data, 1);

            PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[0];
            int idPapel = Convert.ToInt32(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));

            int i = 0;
            while (i < posicaoRendaFixaCollection.Count)
            {
                int idPapelAnterior = idPapel;
                decimal totalRendimento = 0;
                decimal totalAjusteMTM = 0;

                while ((idPapel == idPapelAnterior) && (i < posicaoRendaFixaCollection.Count))
                {
                    int idPosicao = posicaoRendaFixa.IdPosicao.Value;
                    decimal quantidade = posicaoRendaFixa.Quantidade.Value;
                    decimal puCurva = posicaoRendaFixa.PUCurva.Value;
                    decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                    decimal valorCurva = posicaoRendaFixa.ValorCurva.Value;
                    decimal valorMercado = posicaoRendaFixa.ValorMercado.Value;

                    PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                    posicaoRendaFixaHistorico.Query.Select(posicaoRendaFixaHistorico.Query.PUCurva);
                    posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdCliente.Equal(idCliente),
                                                          posicaoRendaFixaHistorico.Query.DataHistorico.Equal(dataAnterior),
                                                          posicaoRendaFixaHistorico.Query.IdPosicao.Equal(idPosicao));
                    if (posicaoRendaFixaHistorico.Query.Load())
                    {
                        decimal rendimento = valorCurva - Math.Round(quantidade * posicaoRendaFixaHistorico.PUCurva.Value, 2);
                        decimal ajusteMTM = valorMercado - valorCurva;

                        totalRendimento += rendimento;
                        totalAjusteMTM += ajusteMTM;
                    }

                    i += 1;
                    if (i < posicaoRendaFixaCollection.Count)
                    {
                        posicaoRendaFixa = posicaoRendaFixaCollection[i];
                        idPapel = Convert.ToInt32(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                    }
                }

                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                if (totalRendimento != 0)
                {
                    int origem = 0;
                    if (totalRendimento > 0)
                    {
                        origem = ContabilOrigem.RendaFixa.RendaPositiva;
                    }
                    else if (totalRendimento < 0)
                    {
                        origem = ContabilOrigem.RendaFixa.RendaNegativa;
                    }

                    #region Evento de RendaPositiva/RendaNegativa
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapelAnterior.ToString()),
                                                        contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = totalRendimento;
                        contabLancamento.Identificador = idPapelAnterior.ToString();
                    }
                    #endregion
                }

                if (totalAjusteMTM != 0)
                {
                    int origem = 0;
                    int origemReversao = 0;
                    if (totalAjusteMTM > 0)
                    {
                        origem = ContabilOrigem.RendaFixa.AjusteMTMPositivo;
                        origemReversao = ContabilOrigem.RendaFixa.ReversaoAjusteMTMPositivo;
                    }
                    else if (totalAjusteMTM < 0)
                    {
                        origem = ContabilOrigem.RendaFixa.AjusteMTMNegativo;
                        origemReversao = ContabilOrigem.RendaFixa.ReversaoAjusteMTMNegativo;
                    }

                    #region Evento de AjusteMTMPositivo/AjusteMTMNegativo e ReversaoAjusteMTMPositivo/ReversaoAjusteMTMNegativo
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapelAnterior.ToString()),
                                                        contabRoteiroCollection.Query.Origem.Equal(origem),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origem;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = totalAjusteMTM;
                        contabLancamento.Identificador = idPapelAnterior.ToString();
                    }

                    contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapelAnterior.ToString()),
                                                        contabRoteiroCollection.Query.Origem.Equal(origemReversao),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = dataProxima;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = origemReversao;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = totalAjusteMTM;
                        contabLancamento.Identificador = idPapelAnterior.ToString();
                    }
                    #endregion
                }
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de AjusteMTMPositivo/ReversaoAjusteMTMPositivo, AjusteMTMNegativo/ReversaoAjusteMTMNegativo, RendaPositiva, RendaNegativa.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        /// <param name="historico"></param>
        private void GeraEventosRendDiarioCompromissada(Cliente cliente, DateTime data, bool historico)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.CreateColumnsForBinding();
            posicaoRendaFixaCollection.AddColumn("IdPapel", typeof(System.Int32));


            if (historico)
            {
                PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                posicaoRendaFixaHistoricoQuery.Select(tituloRendaFixaQuery.IdPapel,
                                              posicaoRendaFixaHistoricoQuery.IdPosicao,
                                              posicaoRendaFixaHistoricoQuery.IdOperacao,
                                              posicaoRendaFixaHistoricoQuery.PUCurva,
                                              posicaoRendaFixaHistoricoQuery.PUMercado,
                                              posicaoRendaFixaHistoricoQuery.Quantidade,
                                              posicaoRendaFixaHistoricoQuery.ValorCurva,
                                              posicaoRendaFixaHistoricoQuery.ValorMercado);
                posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaHistoricoQuery.IdTitulo);
                posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente),
                                                    posicaoRendaFixaHistoricoQuery.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda),
                                                    posicaoRendaFixaHistoricoQuery.Quantidade.NotEqual(0),
                                                    posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(data));
                posicaoRendaFixaHistoricoQuery.OrderBy(tituloRendaFixaQuery.IdPapel.Ascending);
                PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
                posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);

                PosicaoRendaFixaCollection posicaoRendaFixaCollectionAux = new PosicaoRendaFixaCollection(posicaoRendaFixaHistoricoCollection);
                posicaoRendaFixaCollectionAux.AddColumn("IdPapel", typeof(System.Int32));

                for (int j = 0; j < posicaoRendaFixaHistoricoCollection.Count; j++)
                {
                    posicaoRendaFixaCollectionAux[j].SetColumn("IdPapel", posicaoRendaFixaHistoricoCollection[j].GetColumn("IdPapel"));
                }

                //
                foreach (PosicaoRendaFixa posicaoRendaFixa1 in posicaoRendaFixaCollectionAux)
                {
                    posicaoRendaFixaCollection.AttachEntity(posicaoRendaFixa1);
                }
            }
            else
            {
                PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
                TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                posicaoRendaFixaQuery.Select(tituloRendaFixaQuery.IdPapel,
                                              posicaoRendaFixaQuery.IdPosicao,
                                              posicaoRendaFixaQuery.IdOperacao,
                                              posicaoRendaFixaQuery.PUCurva,
                                              posicaoRendaFixaQuery.PUMercado,
                                              posicaoRendaFixaQuery.Quantidade,
                                              posicaoRendaFixaQuery.ValorCurva,
                                              posicaoRendaFixaQuery.ValorMercado);
                posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                            posicaoRendaFixaQuery.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda),
                                            posicaoRendaFixaQuery.Quantidade.NotEqual(0));
                posicaoRendaFixaQuery.OrderBy(tituloRendaFixaQuery.IdPapel.Ascending);
                posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);
            }

            if (posicaoRendaFixaCollection.Count == 0)
            {
                return;
            }

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
            
            foreach(PosicaoRendaFixa posicaoRendaFixaColl in posicaoRendaFixaCollection)
            {
                
                decimal totalRendimento = 0;
                decimal totalAjusteMTM = 0;
                decimal valorMercadoOntem = 0;
                decimal valorMercadoHoje = posicaoRendaFixaColl.ValorMercado.Value;
                int idOperacao = posicaoRendaFixaColl.IdOperacao.Value;

                int idPapel = Convert.ToInt32(posicaoRendaFixaColl.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                int? idCentroCusto;
                string descricao = "";
                string contaDebito = "";
                string contaCredito = "";
                int idContaDebito = 0;
                int idContaCredito = 0;

                PosicaoRendaFixaHistorico posicaoRendaFixaOntem = new PosicaoRendaFixaHistorico();
                posicaoRendaFixaOntem.Query.Where(posicaoRendaFixaOntem.Query.IdCliente.Equal(idCliente),
                                                  posicaoRendaFixaOntem.Query.DataHistorico.Equal(dataAnterior),
                                                  posicaoRendaFixaOntem.Query.IdOperacao.Equal(idOperacao));


                if (posicaoRendaFixaOntem.Query.Load())
                {
                    valorMercadoOntem = posicaoRendaFixaOntem.ValorMercado.Value;
                    totalRendimento = valorMercadoHoje - valorMercadoOntem;
                }   

                if (totalRendimento != 0)
                {
                    
                    #region Evento de RendaPositiva/RendaNegativa
                    ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                    contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapel.ToString()),
                                                        contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.RendaFixa.LucroRevenda),
                                                        contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                    contabRoteiroCollection.Query.Load();

                    if (contabRoteiroCollection.Count > 0)
                    {
                        descricao = contabRoteiroCollection[0].Descricao;
                        contaDebito = contabRoteiroCollection[0].ContaDebito;
                        contaCredito = contabRoteiroCollection[0].ContaCredito;
                        idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                        idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                        idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                        ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                        contabLancamento.IdContaDebito = idContaDebito;
                        contabLancamento.IdContaCredito = idContaCredito;
                        contabLancamento.ContaCredito = contaCredito;
                        contabLancamento.ContaDebito = contaDebito;
                        contabLancamento.DataLancamento = data;
                        contabLancamento.DataRegistro = data;
                        contabLancamento.Descricao = descricao;
                        contabLancamento.IdCentroCusto = idCentroCusto;
                        contabLancamento.IdCliente = idCliente;
                        contabLancamento.IdPlano = idPlano;
                        contabLancamento.Origem = (int)ContabilOrigem.RendaFixa.LucroRevenda;
                        contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                        contabLancamento.Valor = totalRendimento;
                        contabLancamento.Identificador = idPapel.ToString();
                    }
                    #endregion
                }
                
            }

            contabLancamentoCollection.Save();


            //agora lança o dia da liquidação.


        }

        /// <summary>
        /// Eventos de Vencimento.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraEventosVencimento(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("L");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            liquidacaoRendaFixaQuery.Select(tituloRendaFixaQuery.IdPapel,
                                            liquidacaoRendaFixaQuery.IdPosicaoResgatada,
                                            liquidacaoRendaFixaQuery.TipoLancamento,
                                            liquidacaoRendaFixaQuery.Quantidade,
                                            liquidacaoRendaFixaQuery.ValorBruto);
            liquidacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == liquidacaoRendaFixaQuery.IdTitulo);
            liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                           liquidacaoRendaFixaQuery.DataLiquidacao.Equal(data),
                                           liquidacaoRendaFixaQuery.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Vencimento,
                                                                                      (byte)TipoLancamentoLiquidacao.PagtoPrincipal,
                                                                                      (byte)TipoLancamentoLiquidacao.Revenda));
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollection.Load(liquidacaoRendaFixaQuery);

            DateTime dataAnterior = new DateTime();

            if (liquidacaoRendaFixaCollection.Count > 0)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
            }

            foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
            {
                int idPosicao = liquidacaoRendaFixa.IdPosicaoResgatada.Value;
                int idPapel = Convert.ToInt32(liquidacaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                decimal valor = liquidacaoRendaFixa.ValorBruto.Value;
                decimal quantidade = liquidacaoRendaFixa.Quantidade.Value;

                PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
                posicaoRendaFixaHistorico.Query.Select(posicaoRendaFixaHistorico.Query.PUMercado);
                posicaoRendaFixaHistorico.Query.Where(posicaoRendaFixaHistorico.Query.IdPosicao.Equal(idPosicao),
                                                      posicaoRendaFixaHistorico.Query.DataHistorico.Equal(dataAnterior));
                posicaoRendaFixaHistorico.Query.Load();

                decimal renda = 0;
                if (posicaoRendaFixaHistorico.PUMercado.HasValue)
                {
                    decimal valorAnterior = Math.Round(posicaoRendaFixaHistorico.PUMercado.Value * quantidade, 2);
                    renda = valor - valorAnterior;
                }

                #region Evento de Vencimento
                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();                
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapel.ToString()),
                                                    contabRoteiroCollection.Query.Origem.Equal(ContabilOrigem.RendaFixa.Vencimento),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                
                
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0 && liquidacaoRendaFixa.TipoLancamento != (byte)TipoLancamentoLiquidacao.Revenda)
                {
                    string descricao = contabRoteiroCollection[0].Descricao;
                    string contaDebito = contabRoteiroCollection[0].ContaDebito;
                    string contaCredito = contabRoteiroCollection[0].ContaCredito;
                    int? idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    int idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    int idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = ContabilOrigem.RendaFixa.Vencimento;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valor;
                    contabLancamento.Identificador = idPapel.ToString();
                }
                #endregion

                int origemRenda = 0;
                if (renda > 0)
                {
                    origemRenda = ContabilOrigem.RendaFixa.RendaPositiva;
                }
                else
                {
                    origemRenda = ContabilOrigem.RendaFixa.RendaNegativa;
                }

                if (liquidacaoRendaFixa.TipoLancamento == (byte)TipoLancamentoLiquidacao.Revenda)
                {
                    origemRenda = ContabilOrigem.RendaFixa.LucroRevenda;
                } 


                #region Evento de Renda
                contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapel.ToString()),
                                                    contabRoteiroCollection.Query.Origem.Equal(origemRenda),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    string descricao = contabRoteiroCollection[0].Descricao;
                    string contaDebito = contabRoteiroCollection[0].ContaDebito;
                    string contaCredito = contabRoteiroCollection[0].ContaCredito;
                    int? idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    int idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    int idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origemRenda;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = renda;
                    contabLancamento.Identificador = idPapel.ToString();
                }
                #endregion
            }

            contabLancamentoCollection.Save();
        }

        /// <summary>
        /// Eventos de PagamentoJuros, PagamentoAmortizacao.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="data"></param>
        private void GeraEventosLiquidacaoAgenda(Cliente cliente, DateTime data)
        {
            int idCliente = cliente.IdCliente.Value;
            int idPlano = cliente.IdPlano.Value;

            ContabLancamentoCollection contabLancamentoCollection = new ContabLancamentoCollection();

            LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("L");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            liquidacaoRendaFixaQuery.Select(tituloRendaFixaQuery.IdPapel,
                                            liquidacaoRendaFixaQuery.TipoLancamento,
                                            liquidacaoRendaFixaQuery.Rendimento.Sum());
            liquidacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == liquidacaoRendaFixaQuery.IdTitulo);
            liquidacaoRendaFixaQuery.Where(liquidacaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                           liquidacaoRendaFixaQuery.DataLiquidacao.Equal(data),
                                           liquidacaoRendaFixaQuery.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Amortizacao,
                                                                                      (byte)TipoLancamentoLiquidacao.Juros));
            liquidacaoRendaFixaQuery.GroupBy(tituloRendaFixaQuery.IdPapel, liquidacaoRendaFixaQuery.TipoLancamento);
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollection.Load(liquidacaoRendaFixaQuery);

            foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
            {
                int idPapel = Convert.ToInt32(liquidacaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdPapel));
                decimal valor = liquidacaoRendaFixa.Rendimento.Value;
                byte tipoLancamento = liquidacaoRendaFixa.TipoLancamento.Value;

                #region Evento de Vencimento
                int origem = 0;
                if (tipoLancamento == (byte)TipoLancamentoLiquidacao.Amortizacao)
                {
                    origem = ContabilOrigem.RendaFixa.PagamentoAmortizacao;
                }
                else
                {
                    origem = ContabilOrigem.RendaFixa.PagamentoJuros;
                }

                ContabRoteiroCollection contabRoteiroCollection = new ContabRoteiroCollection();
                contabRoteiroCollection.Query.Where(contabRoteiroCollection.Query.Identificador.Equal(idPapel.ToString()),
                                                    contabRoteiroCollection.Query.Origem.Equal(origem),
                                                    contabRoteiroCollection.Query.IdPlano.Equal(idPlano));
                contabRoteiroCollection.Query.Load();

                if (contabRoteiroCollection.Count > 0)
                {
                    string descricao = contabRoteiroCollection[0].Descricao;
                    string contaDebito = contabRoteiroCollection[0].ContaDebito;
                    string contaCredito = contabRoteiroCollection[0].ContaCredito;
                    int? idCentroCusto = contabRoteiroCollection[0].IdCentroCusto;
                    int idContaDebito = contabRoteiroCollection[0].IdContaDebito.Value;
                    int idContaCredito = contabRoteiroCollection[0].IdContaCredito.Value;

                    ContabLancamento contabLancamento = contabLancamentoCollection.AddNew();
                    contabLancamento.IdContaDebito = idContaDebito;
                    contabLancamento.IdContaCredito = idContaCredito;
                    contabLancamento.ContaCredito = contaCredito;
                    contabLancamento.ContaDebito = contaDebito;
                    contabLancamento.DataLancamento = data;
                    contabLancamento.DataRegistro = data;
                    contabLancamento.Descricao = descricao;
                    contabLancamento.IdCentroCusto = idCentroCusto;
                    contabLancamento.IdCliente = idCliente;
                    contabLancamento.IdPlano = idPlano;
                    contabLancamento.Origem = origem;
                    contabLancamento.Fonte = (byte)FonteLancamentoContabil.Interno;
                    contabLancamento.Valor = valor;
                    contabLancamento.Identificador = idPapel.ToString();
                }
                #endregion
            }

            contabLancamentoCollection.Save();
        }
        
    }
}
