﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Financial.Contabil.Enums
{
    public static class ContabilOrigem
    {
        public static class Bolsa
        {
            public const int ValorizacaoAcao = 100;
            public const int ReversaoValorizacaoAcao = 101;
            public const int DesvalorizacaoAcao = 102;
            public const int ReversaoDesvalorizacaoAcao = 103;
            public const int ValorizacaoAcaoVendida = 200;
            public const int ReversaoValorizacaoAcaoVendida = 201;
            public const int DesvalorizacaoAcaoVendida = 202;
            public const int ReversaoDesvalorizacaoAcaoVendida = 203;

            public const int ValorizacaoOPCComprada = 500;
            public const int ReversaoValorizacaoOPCComprada = 501;
            public const int DesvalorizacaoOPCComprada = 502;
            public const int ReversaoDesvalorizacaoOPCComprada = 503;
            public const int ValorizacaoOPCVendida = 600;
            public const int ReversaoValorizacaoOPCVendida = 601;
            public const int DesvalorizacaoOPCVendida = 602;
            public const int ReversaoDesvalorizacaoOPCVendida = 603;

            public const int ValorizacaoOPVComprada = 1000;
            public const int ReversaoValorizacaoOPVComprada = 1001;
            public const int DesvalorizacaoOPVComprada = 1002;
            public const int ReversaoDesvalorizacaoOPVComprada = 1003;
            public const int ValorizacaoOPVVendida = 1100;
            public const int ReversaoValorizacaoOPVVendida = 1101;
            public const int DesvalorizacaoOPVVendida = 1102;
            public const int ReversaoDesvalorizacaoOPVVendida = 1103;
            
            public const int CompraAcao = 1500;
            public const int CompraAcaoCorretagem = 1501;
            public const int CompraAcaoEmolumento = 1502;
            public const int CompraAcaoTaxaCBLC = 1503;

            public const int VendaAcao = 2000;
            public const int VendaAcaoCorretagem = 2001;
            public const int VendaAcaoEmolumento = 2002;
            public const int VendaAcaoTaxaCBLC = 2003;

            public const int CompraOPC = 2500;
            public const int CompraOPCCorretagem = 2501;
            public const int CompraOPCEmolumento = 2502;
            public const int CompraOPCTaxaCBLC = 2503;

            public const int VendaOPC = 3000;
            public const int VendaOPCCorretagem = 3001;
            public const int VendaOPCEmolumento = 3002;
            public const int VendaOPCTaxaCBLC = 3003;
            
            public const int CompraOPV = 3500;
            public const int CompraOPVCorretagem = 3501;
            public const int CompraOPVEmolumento = 3502;
            public const int CompraOPVTaxaCBLC = 3503;

            public const int VendaOPV = 4000;
            public const int VendaOPVCorretagem = 4001;
            public const int VendaOPVEmolumento = 4002;
            public const int VendaOPVTaxaCBLC = 4003;

            public const int LucroAcao = 4500;
            public const int PrejuizoAcao = 4501;

            public const int LucroCompraOPC = 4600;
            public const int PrejuizoCompraOPC = 4601;
            public const int LucroVendaOPC = 4650;
            public const int PrejuizoVendaOPC = 4651;

            public const int LucroCompraOPV = 4700;
            public const int PrejuizoCompraOPV = 4701;
            public const int LucroVendaOPV = 4750;
            public const int PrejuizoVendaOPV = 4751;

            public const int LucroAcaoDayTrade = 5000;
            public const int PrejuizoAcaoDayTrade = 5001;
            public const int LucroOpcaoDayTrade = 5100;
            public const int PrejuizoOpcaoDayTrade = 5101;
            public const int LucroOPVDayTrade = 5200; //Só usada em caso de parâmetro Daytrade = Segregado, separando OPC de OPV
            public const int PrejuizoOPVDayTrade = 5201; //Só usada em caso de parâmetro Daytrade = Segregado, separando OPC de OPV 
            
            public const int NaoExercicioOPCComprada = 5500;
            public const int NaoExercicioOPCVendida = 5600;
            public const int NaoExercicioOPVComprada = 5700;
            public const int NaoExercicioOPVVendida = 5800;

            public const int ExercicioOPCComprada = 6000; 
            public const int ExercicioOPCVendida = 6100;
            public const int ExercicioOPVComprada = 6200;
            public const int ExercicioOPVVendida = 6300;

            public const int LiquidacaoBovespaPagar = 7000;
            public const int LiquidacaoBovespaReceber = 7001;

            public const int ProvisaoCustosEmprestimoTomador = 7500;
            public const int LiquidacaoCustosEmprestimoTomador = 7600;
            public const int ProvisaoJurosEmprestimoDoador = 7700;
            public const int LiquidacaoJurosEmprestimoDoador = 7800;

            public const int ProvisaoDividendosReceber = 8000;
            public const int PagamentoDividendosReceber = 8100;
            public const int ProvisaoDividendosPagar = 8200;
            public const int PagamentoDividendosPagar = 8300;
            public const int ProvisaoJurosCapitalReceber = 8400; 
            public const int PagamentoJurosCapitalReceber = 8500;
            public const int ProvisaoJurosCapitalPagar = 8600;
            public const int PagamentoJurosCapitalPagar = 8700;
            public const int ProvisaoRendimentosReceber = 8800;
            public const int PagamentoRendimentosReceber = 8900;
            public const int ProvisaoRendimentosPagar = 9000;
            public const int PagamentoRendimentosPagar = 9100;

            public const int BTC_Tomado = 10000;
            public const int BTC_TomadoDevolucao = 10100;
            public const int BTC_Doado = 10200;
            public const int BTC_DoadoDevolucao = 10300;
            public const int BTC_Tomado_VariacaoPos = 10400;
            public const int BTC_Tomado_VariacaoPos_Reversao = 10401;
            public const int BTC_Tomado_VariacaoNeg = 10500;
            public const int BTC_Tomado_VariacaoNeg_Reversao = 10501;
            public const int BTC_Doado_VariacaoPos = 10600;
            public const int BTC_Doado_VariacaoPos_Reversao = 10601;
            public const int BTC_Doado_VariacaoNeg = 10700;
            public const int BTC_Doado_VariacaoNeg_Reversao = 10701;
            public const int BTC_Tomado_ProvTaxas = 10800;
            public const int BTC_Tomado_PagtoTaxas = 10900;
            public const int BTC_Doado_ProvTaxas = 11000;
            public const int BTC_Doado_PagtoTaxas = 11100;

            public const int CompraTermo = 14000;
            public const int CompraTermoCorretagem = 14001;
            public const int CompraTermoEmolumento = 14002;
            public const int CompraTermoTaxaCBLC = 14003;

            public const int VendaTermo = 14500;
            public const int VendaTermoCorretagem = 14501;
            public const int VendaTermoEmolumento = 14502;
            public const int VendaTermoTaxaCBLC = 14503;

            public const int LiquidacaoFisicaTermoComprado = 14700;
            public const int LiquidacaoFisicaTermoVendido = 14750;
            public const int LiquidacaoFinanceiraTermoComprado = 14800;
            public const int LiquidacaoFinanceiraTermoVendido = 14850;            

            public const int ValorizacaoTermoComprado = 15000;
            public const int DesvalorizacaoTermoComprado = 15010;
            public const int ValorizacaoTermoVendido = 15050;
            public const int DesvalorizacaoTermoVendido = 15060;

            public const int JurosTermoComprado = 15100;
            public const int JurosTermoVendido = 15110;
        }
        public static class BMF
        {
            public const int ValorizacaoOPCComprada = 20000;
            public const int ReversaoValorizacaoOPCComprada = 20001;
            public const int DesvalorizacaoOPCComprada = 20002;
            public const int ReversaoDesvalorizacaoOPCComprada = 20003;
            public const int ValorizacaoOPCVendida = 20100;
            public const int ReversaoValorizacaoOPCVendida = 20101;
            public const int DesvalorizacaoOPCVendida = 20102;
            public const int ReversaoDesvalorizacaoOPCVendida = 20103;

            public const int ValorizacaoOPVComprada = 20500;
            public const int ReversaoValorizacaoOPVComprada = 20501;
            public const int DesvalorizacaoOPVComprada = 20502;
            public const int ReversaoDesvalorizacaoOPVComprada = 20503;
            public const int ValorizacaoOPVVendida = 20600;
            public const int ReversaoValorizacaoOPVVendida = 20601;
            public const int DesvalorizacaoOPVVendida = 20602;
            public const int ReversaoDesvalorizacaoOPVVendida = 20603;

            public const int AjustePositivo = 21000;
            public const int AjusteNegativo = 21001;
            public const int FuturoCorretagem = 21100;
            public const int FuturoEmolumento = 21101;
            public const int FuturoRegistro = 21102;

            public const int CompraOPC = 21500;
            public const int CompraOPCCorretagem = 21600;
            public const int CompraOPCEmolumento = 21601;
            public const int CompraOPCRegistro = 21602;

            public const int VendaOPC = 22000;
            public const int VendaOPCCorretagem = 22100;
            public const int VendaOPCEmolumento = 22101;
            public const int VendaOPCRegistro = 22102;

            public const int CompraOPV = 22500;
            public const int CompraOPVCorretagem = 22600;
            public const int CompraOPVEmolumento = 22601;
            public const int CompraOPVRegistro = 22602;

            public const int VendaOPV = 23000;
            public const int VendaOPVCorretagem = 23100;
            public const int VendaOPVEmolumento = 23101;
            public const int VendaOPVRegistro = 23102;
            
            public const int LucroCompraOPC = 23500;
            public const int PrejuizoCompraOPC = 23501;
            public const int LucroVendaOPC = 23550;
            public const int PrejuizoVendaOPC = 23551;

            public const int LucroCompraOPV = 24000;
            public const int PrejuizoCompraOPV = 24001;
            public const int LucroVendaOPV = 24050;
            public const int PrejuizoVendaOPV = 24051;

            public const int LucroOpcaoDayTrade = 25100;
            public const int PrejuizoOpcaoDayTrade = 25101;
            public const int LucroOPVDayTrade = 25200; //Só usada em caso de parâmetro Daytrade = Segregado, separando OPC de OPV
            public const int PrejuizoOPVDayTrade = 25201; //Só usada em caso de parâmetro Daytrade = Segregado, separando OPC de OPV
                        
            public const int LiquidacaoBMFPagar = 26000;
            public const int LiquidacaoBMFReceber = 26001;

            public const int ProvisaoTaxaPermanencia = 27000;
            public const int PagamentoTaxaPermanencia = 27001;
        }
        public static class RendaFixa
        {
            public const int CompraFinal = 30000;

            public const int VendaFinal = 30500;
            public const int LucroVendaFinal = 30600;
            public const int PrejuizoVendaFinal = 30601;

            public const int CompraCompromissada = 31000;
            public const int Revenda = 31100;
            public const int LucroRevenda = 31200;

            public const int AjusteMTMPositivo = 32000; //Diferença entre contábil e mercado
            public const int AjusteMTMNegativo = 32001; //Diferença entre contábil e mercado
            public const int ReversaoAjusteMTMPositivo = 32002;
            public const int ReversaoAjusteMTMNegativo = 32003;

            public const int RendaPositiva = 32500; //Diferença entre D0 e D-1 no contábil
            public const int RendaNegativa = 32501; //Diferença entre D0 e D-1 no contábil

            public const int Vencimento = 33000;

            public const int PagamentoJuros = 34000; //TODO Não está sendo contabilizado, precisa ser criada uma tabela de agenda por cliente
            public const int PagamentoAmortizacao = 34100; //TODO Não está sendo contabilizado, precisa ser criada uma tabela de agenda por cliente
        }
        public static class Swap
        {
            public const int AjustePositivo = 40000;
            public const int ReversaoAjustePositivo = 40001;
            public const int AjusteNegativo = 40100;
            public const int ReversaoAjusteNegativo = 40101;

            public const int LiquidacaoPagar = 41000;
            public const int LiquidacaoReceber = 41001;
        }
        public static class CotaInvestimento
        {
            public const int Aplicacao = 50000;

            public const int Resgate = 51000;
            public const int LiquidacaoResgate = 51100;

            public const int Valorizacao = 52000;
            public const int Desvalorizacao = 52001;
           
            public const int ProvisaoIR = 52100;            
            public const int EstornoProvisaoIR = 52200;
            public const int ProvisaoIOF = 52300;

            public const int ResgateProvisaoIR = 52400;
            public const int ResgateProvisaoIOF = 52500;

            public const int EstornoProvisaoIOF = 52600;

        }        
        public static class Cotista
        {
            public const int AplicacaoPF = 60000;

            public const int ResgateCustoPF = 60500;
            public const int ResgateVariacaoPositivaPF = 60600;
            public const int ResgateVariacaoNegativaPF = 60601;
            public const int ResgateProvisaoIRPF = 60700;
            public const int ResgateProvisaoIOFPF = 60800;
            
            public const int AplicacaoPJ = 62000;

            public const int ResgateCustoPJ = 62500;
            public const int ResgateVariacaoPositivaPJ = 62600;
            public const int ResgateVariacaoNegativaPJ = 62601;
            public const int ResgateProvisaoIRPJ = 62700;
            public const int ResgateProvisaoIOFPJ = 62800;

            public const int ConversaoAplicacao = 63000;

            public const int LiquidacaoResgate = 64000;
            public const int LiquidacaoResgateIR = 64100;
            public const int LiquidacaoResgateIOF = 64200;          
          

        }
        public static class Despesas
        {
            public const int Provisao = 80000;
            public const int Pagamento = 80100;            
        }

        public static class ZeragemResultado
        {
            public const int ZeragemLucro = 200000;
            public const int ZeragemPrejuizo = 200001;
        }

        public static int Outros = 999999;        
    }

    public enum TipoContabilAcoes
    {
        Consolidado = 1,
        Analitico = 2
    }

    public enum TipoContabilFuturos
    {
        Consolidado = 1,
        Analitico = 2
    }

    public enum TipoContabilCotasInvestimento
    {
        Consolidado = 1,
        Analitico = 2
    }

    public enum TipoContabilBMF
    {
        Consolidado = 1,
        Analitico = 2
    }

    public enum TipoContabilLiquidacao
    {
        Net = 1,
        Segregado = 2
    }

    public enum TipoContabilDaytrade
    {
        Net = 1,
        Segregado = 2
    }

    public enum FonteLancamentoContabil
    {
        Interno = 1,
        Manual = 2,
        Liquidacao = 3
    }

    public static class TipoContaContabil
    {
        public const string Ativo = "A";
        public const string Passivo = "P";
    }

    public enum FuncaoContaContabil
    {
        Operacional = 1,
        ResultadoCredito = 20,
        ResultadoDebito = 21,
        Compensado = 30,
        Patrimonial = 40,
        ResultadoAcumulado = 50
    }
}