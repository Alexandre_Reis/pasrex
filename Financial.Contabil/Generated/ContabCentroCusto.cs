/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/07/2013 21:55:28
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Contabil
{

	[Serializable]
	abstract public class esContabCentroCustoCollection : esEntityCollection
	{
		public esContabCentroCustoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ContabCentroCustoCollection";
		}

		#region Query Logic
		protected void InitQuery(esContabCentroCustoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esContabCentroCustoQuery);
		}
		#endregion
		
		virtual public ContabCentroCusto DetachEntity(ContabCentroCusto entity)
		{
			return base.DetachEntity(entity) as ContabCentroCusto;
		}
		
		virtual public ContabCentroCusto AttachEntity(ContabCentroCusto entity)
		{
			return base.AttachEntity(entity) as ContabCentroCusto;
		}
		
		virtual public void Combine(ContabCentroCustoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ContabCentroCusto this[int index]
		{
			get
			{
				return base[index] as ContabCentroCusto;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ContabCentroCusto);
		}
	}



	[Serializable]
	abstract public class esContabCentroCusto : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esContabCentroCustoQuery GetDynamicQuery()
		{
			return null;
		}

		public esContabCentroCusto()
		{

		}

		public esContabCentroCusto(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCentroCusto)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCentroCusto);
			else
				return LoadByPrimaryKeyStoredProcedure(idCentroCusto);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCentroCusto)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esContabCentroCustoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCentroCusto == idCentroCusto);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCentroCusto)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCentroCusto);
			else
				return LoadByPrimaryKeyStoredProcedure(idCentroCusto);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCentroCusto)
		{
			esContabCentroCustoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCentroCusto == idCentroCusto);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCentroCusto)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCentroCusto",idCentroCusto);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCentroCusto": this.str.IdCentroCusto = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCentroCusto":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCentroCusto = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ContabCentroCusto.IdCentroCusto
		/// </summary>
		virtual public System.Int32? IdCentroCusto
		{
			get
			{
				return base.GetSystemInt32(ContabCentroCustoMetadata.ColumnNames.IdCentroCusto);
			}
			
			set
			{
				base.SetSystemInt32(ContabCentroCustoMetadata.ColumnNames.IdCentroCusto, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabCentroCusto.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(ContabCentroCustoMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(ContabCentroCustoMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esContabCentroCusto entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCentroCusto
			{
				get
				{
					System.Int32? data = entity.IdCentroCusto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCentroCusto = null;
					else entity.IdCentroCusto = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esContabCentroCusto entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esContabCentroCustoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esContabCentroCusto can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ContabCentroCusto : esContabCentroCusto
	{

				
		#region ContabLancamentoCollectionByIdCentroCusto - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContabCentroCusto_ContabLancamento_FK1
		/// </summary>

		[XmlIgnore]
		public ContabLancamentoCollection ContabLancamentoCollectionByIdCentroCusto
		{
			get
			{
				if(this._ContabLancamentoCollectionByIdCentroCusto == null)
				{
					this._ContabLancamentoCollectionByIdCentroCusto = new ContabLancamentoCollection();
					this._ContabLancamentoCollectionByIdCentroCusto.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ContabLancamentoCollectionByIdCentroCusto", this._ContabLancamentoCollectionByIdCentroCusto);
				
					if(this.IdCentroCusto != null)
					{
						this._ContabLancamentoCollectionByIdCentroCusto.Query.Where(this._ContabLancamentoCollectionByIdCentroCusto.Query.IdCentroCusto == this.IdCentroCusto);
						this._ContabLancamentoCollectionByIdCentroCusto.Query.Load();

						// Auto-hookup Foreign Keys
						this._ContabLancamentoCollectionByIdCentroCusto.fks.Add(ContabLancamentoMetadata.ColumnNames.IdCentroCusto, this.IdCentroCusto);
					}
				}

				return this._ContabLancamentoCollectionByIdCentroCusto;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ContabLancamentoCollectionByIdCentroCusto != null) 
				{ 
					this.RemovePostSave("ContabLancamentoCollectionByIdCentroCusto"); 
					this._ContabLancamentoCollectionByIdCentroCusto = null;
					
				} 
			} 			
		}

		private ContabLancamentoCollection _ContabLancamentoCollectionByIdCentroCusto;
		#endregion

				
		#region ContabRoteiroCollectionByIdCentroCusto - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - ContabCentroCusto_ContabRoteiro_FK1
		/// </summary>

		[XmlIgnore]
		public ContabRoteiroCollection ContabRoteiroCollectionByIdCentroCusto
		{
			get
			{
				if(this._ContabRoteiroCollectionByIdCentroCusto == null)
				{
					this._ContabRoteiroCollectionByIdCentroCusto = new ContabRoteiroCollection();
					this._ContabRoteiroCollectionByIdCentroCusto.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ContabRoteiroCollectionByIdCentroCusto", this._ContabRoteiroCollectionByIdCentroCusto);
				
					if(this.IdCentroCusto != null)
					{
						this._ContabRoteiroCollectionByIdCentroCusto.Query.Where(this._ContabRoteiroCollectionByIdCentroCusto.Query.IdCentroCusto == this.IdCentroCusto);
						this._ContabRoteiroCollectionByIdCentroCusto.Query.Load();

						// Auto-hookup Foreign Keys
						this._ContabRoteiroCollectionByIdCentroCusto.fks.Add(ContabRoteiroMetadata.ColumnNames.IdCentroCusto, this.IdCentroCusto);
					}
				}

				return this._ContabRoteiroCollectionByIdCentroCusto;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ContabRoteiroCollectionByIdCentroCusto != null) 
				{ 
					this.RemovePostSave("ContabRoteiroCollectionByIdCentroCusto"); 
					this._ContabRoteiroCollectionByIdCentroCusto = null;
					
				} 
			} 			
		}

		private ContabRoteiroCollection _ContabRoteiroCollectionByIdCentroCusto;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "ContabLancamentoCollectionByIdCentroCusto", typeof(ContabLancamentoCollection), new ContabLancamento()));
			props.Add(new esPropertyDescriptor(this, "ContabRoteiroCollectionByIdCentroCusto", typeof(ContabRoteiroCollection), new ContabRoteiro()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._ContabLancamentoCollectionByIdCentroCusto != null)
			{
				foreach(ContabLancamento obj in this._ContabLancamentoCollectionByIdCentroCusto)
				{
					if(obj.es.IsAdded)
					{
						obj.IdCentroCusto = this.IdCentroCusto;
					}
				}
			}
			if(this._ContabRoteiroCollectionByIdCentroCusto != null)
			{
				foreach(ContabRoteiro obj in this._ContabRoteiroCollectionByIdCentroCusto)
				{
					if(obj.es.IsAdded)
					{
						obj.IdCentroCusto = this.IdCentroCusto;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esContabCentroCustoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ContabCentroCustoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCentroCusto
		{
			get
			{
				return new esQueryItem(this, ContabCentroCustoMetadata.ColumnNames.IdCentroCusto, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, ContabCentroCustoMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ContabCentroCustoCollection")]
	public partial class ContabCentroCustoCollection : esContabCentroCustoCollection, IEnumerable<ContabCentroCusto>
	{
		public ContabCentroCustoCollection()
		{

		}
		
		public static implicit operator List<ContabCentroCusto>(ContabCentroCustoCollection coll)
		{
			List<ContabCentroCusto> list = new List<ContabCentroCusto>();
			
			foreach (ContabCentroCusto emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ContabCentroCustoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContabCentroCustoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ContabCentroCusto(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ContabCentroCusto();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ContabCentroCustoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContabCentroCustoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ContabCentroCustoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ContabCentroCusto AddNew()
		{
			ContabCentroCusto entity = base.AddNewEntity() as ContabCentroCusto;
			
			return entity;
		}

		public ContabCentroCusto FindByPrimaryKey(System.Int32 idCentroCusto)
		{
			return base.FindByPrimaryKey(idCentroCusto) as ContabCentroCusto;
		}


		#region IEnumerable<ContabCentroCusto> Members

		IEnumerator<ContabCentroCusto> IEnumerable<ContabCentroCusto>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ContabCentroCusto;
			}
		}

		#endregion
		
		private ContabCentroCustoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ContabCentroCusto' table
	/// </summary>

	[Serializable]
	public partial class ContabCentroCusto : esContabCentroCusto
	{
		public ContabCentroCusto()
		{

		}
	
		public ContabCentroCusto(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ContabCentroCustoMetadata.Meta();
			}
		}
		
		
		
		override protected esContabCentroCustoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContabCentroCustoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ContabCentroCustoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContabCentroCustoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ContabCentroCustoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ContabCentroCustoQuery query;
	}



	[Serializable]
	public partial class ContabCentroCustoQuery : esContabCentroCustoQuery
	{
		public ContabCentroCustoQuery()
		{

		}		
		
		public ContabCentroCustoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ContabCentroCustoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ContabCentroCustoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ContabCentroCustoMetadata.ColumnNames.IdCentroCusto, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabCentroCustoMetadata.PropertyNames.IdCentroCusto;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabCentroCustoMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ContabCentroCustoMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ContabCentroCustoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCentroCusto = "IdCentroCusto";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCentroCusto = "IdCentroCusto";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ContabCentroCustoMetadata))
			{
				if(ContabCentroCustoMetadata.mapDelegates == null)
				{
					ContabCentroCustoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ContabCentroCustoMetadata.meta == null)
				{
					ContabCentroCustoMetadata.meta = new ContabCentroCustoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCentroCusto", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "ContabCentroCusto";
				meta.Destination = "ContabCentroCusto";
				
				meta.spInsert = "proc_ContabCentroCustoInsert";				
				meta.spUpdate = "proc_ContabCentroCustoUpdate";		
				meta.spDelete = "proc_ContabCentroCustoDelete";
				meta.spLoadAll = "proc_ContabCentroCustoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ContabCentroCustoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ContabCentroCustoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
