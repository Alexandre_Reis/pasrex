/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/07/2013 21:55:28
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;











































































































































































































namespace Financial.Contabil
{

    [Serializable]
    abstract public class esContabContaCollection : esEntityCollection
    {
        public esContabContaCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "ContabContaCollection";
        }

        #region Query Logic
        protected void InitQuery(esContabContaQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esContabContaQuery);
        }
        #endregion

        virtual public ContabConta DetachEntity(ContabConta entity)
        {
            return base.DetachEntity(entity) as ContabConta;
        }

        virtual public ContabConta AttachEntity(ContabConta entity)
        {
            return base.AttachEntity(entity) as ContabConta;
        }

        virtual public void Combine(ContabContaCollection collection)
        {
            base.Combine(collection);
        }

        new public ContabConta this[int index]
        {
            get
            {
                return base[index] as ContabConta;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(ContabConta);
        }
    }



    [Serializable]
    abstract public class esContabConta : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esContabContaQuery GetDynamicQuery()
        {
            return null;
        }

        public esContabConta()
        {

        }

        public esContabConta(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Int32 idConta)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idConta);
            else
                return LoadByPrimaryKeyStoredProcedure(idConta);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idConta)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esContabContaQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.IdConta == idConta);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idConta)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idConta);
            else
                return LoadByPrimaryKeyStoredProcedure(idConta);
        }

        private bool LoadByPrimaryKeyDynamic(System.Int32 idConta)
        {
            esContabContaQuery query = this.GetDynamicQuery();
            query.Where(query.IdConta == idConta);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idConta)
        {
            esParameters parms = new esParameters();
            parms.Add("IdConta", idConta);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "IdConta": this.str.IdConta = (string)value; break;
                        case "IdPlano": this.str.IdPlano = (string)value; break;
                        case "TipoConta": this.str.TipoConta = (string)value; break;
                        case "Descricao": this.str.Descricao = (string)value; break;
                        case "IdContaMae": this.str.IdContaMae = (string)value; break;
                        case "Codigo": this.str.Codigo = (string)value; break;
                        case "CodigoReduzida": this.str.CodigoReduzida = (string)value; break;
                        case "FuncaoConta": this.str.FuncaoConta = (string)value; break;
                        case "Digito": this.str.Digito = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "IdConta":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdConta = (System.Int32?)value;
                            break;

                        case "IdPlano":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdPlano = (System.Int32?)value;
                            break;

                        case "IdContaMae":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdContaMae = (System.Int32?)value;
                            break;

                        case "FuncaoConta":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.FuncaoConta = (System.Int32?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to ContabConta.IdConta
        /// </summary>
        virtual public System.Int32? IdConta
        {
            get
            {
                return base.GetSystemInt32(ContabContaMetadata.ColumnNames.IdConta);
            }

            set
            {
                base.SetSystemInt32(ContabContaMetadata.ColumnNames.IdConta, value);
            }
        }

        /// <summary>
        /// Maps to ContabConta.IdPlano
        /// </summary>
        virtual public System.Int32? IdPlano
        {
            get
            {
                return base.GetSystemInt32(ContabContaMetadata.ColumnNames.IdPlano);
            }

            set
            {
                if (base.SetSystemInt32(ContabContaMetadata.ColumnNames.IdPlano, value))
                {
                    this._UpToContabPlanoByIdPlano = null;
                }
            }
        }

        /// <summary>
        /// Maps to ContabConta.TipoConta
        /// </summary>
        virtual public System.String TipoConta
        {
            get
            {
                return base.GetSystemString(ContabContaMetadata.ColumnNames.TipoConta);
            }

            set
            {
                base.SetSystemString(ContabContaMetadata.ColumnNames.TipoConta, value);
            }
        }

        /// <summary>
        /// Maps to ContabConta.Descricao
        /// </summary>
        virtual public System.String Descricao
        {
            get
            {
                return base.GetSystemString(ContabContaMetadata.ColumnNames.Descricao);
            }

            set
            {
                base.SetSystemString(ContabContaMetadata.ColumnNames.Descricao, value);
            }
        }

        /// <summary>
        /// Maps to ContabConta.IdContaMae
        /// </summary>
        virtual public System.Int32? IdContaMae
        {
            get
            {
                return base.GetSystemInt32(ContabContaMetadata.ColumnNames.IdContaMae);
            }

            set
            {
                if (base.SetSystemInt32(ContabContaMetadata.ColumnNames.IdContaMae, value))
                {
                    this._UpToContabContaByIdContaMae = null;
                }
            }
        }

        /// <summary>
        /// Maps to ContabConta.Codigo
        /// </summary>
        virtual public System.String Codigo
        {
            get
            {
                return base.GetSystemString(ContabContaMetadata.ColumnNames.Codigo);
            }

            set
            {
                base.SetSystemString(ContabContaMetadata.ColumnNames.Codigo, value);
            }
        }

        /// <summary>
        /// Maps to ContabConta.CodigoReduzida
        /// </summary>
        virtual public System.String CodigoReduzida
        {
            get
            {
                return base.GetSystemString(ContabContaMetadata.ColumnNames.CodigoReduzida);
            }

            set
            {
                base.SetSystemString(ContabContaMetadata.ColumnNames.CodigoReduzida, value);
            }
        }

        /// <summary>
        /// Maps to ContabConta.FuncaoConta
        /// </summary>
        virtual public System.Int32? FuncaoConta
        {
            get
            {
                return base.GetSystemInt32(ContabContaMetadata.ColumnNames.FuncaoConta);
            }

            set
            {
                base.SetSystemInt32(ContabContaMetadata.ColumnNames.FuncaoConta, value);
            }
        }

        /// <summary>
        /// Maps to ContabConta.Digito
        /// </summary>
        virtual public System.String Digito
        {
            get
            {
                return base.GetSystemString(ContabContaMetadata.ColumnNames.Digito);
            }

            set
            {
                base.SetSystemString(ContabContaMetadata.ColumnNames.Digito, value);
            }
        }

        [CLSCompliant(false)]
        internal protected ContabConta _UpToContabContaByIdContaMae;
        [CLSCompliant(false)]
        internal protected ContabPlano _UpToContabPlanoByIdPlano;
        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esContabConta entity)
            {
                this.entity = entity;
            }


            public System.String IdConta
            {
                get
                {
                    System.Int32? data = entity.IdConta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdConta = null;
                    else entity.IdConta = Convert.ToInt32(value);
                }
            }

            public System.String IdPlano
            {
                get
                {
                    System.Int32? data = entity.IdPlano;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdPlano = null;
                    else entity.IdPlano = Convert.ToInt32(value);
                }
            }

            public System.String TipoConta
            {
                get
                {
                    System.String data = entity.TipoConta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.TipoConta = null;
                    else entity.TipoConta = Convert.ToString(value);
                }
            }

            public System.String Descricao
            {
                get
                {
                    System.String data = entity.Descricao;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Descricao = null;
                    else entity.Descricao = Convert.ToString(value);
                }
            }

            public System.String IdContaMae
            {
                get
                {
                    System.Int32? data = entity.IdContaMae;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdContaMae = null;
                    else entity.IdContaMae = Convert.ToInt32(value);
                }
            }

            public System.String Codigo
            {
                get
                {
                    System.String data = entity.Codigo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Codigo = null;
                    else entity.Codigo = Convert.ToString(value);
                }
            }

            public System.String CodigoReduzida
            {
                get
                {
                    System.String data = entity.CodigoReduzida;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.CodigoReduzida = null;
                    else entity.CodigoReduzida = Convert.ToString(value);
                }
            }

            public System.String FuncaoConta
            {
                get
                {
                    System.Int32? data = entity.FuncaoConta;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.FuncaoConta = null;
                    else entity.FuncaoConta = Convert.ToInt32(value);
                }
            }

            public System.String Digito
            {
                get
                {
                    System.String data = entity.Digito;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Digito = null;
                    else entity.Digito = Convert.ToString(value);
                }
            }

            private esContabConta entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esContabContaQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esContabConta can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class ContabConta : esContabConta
    {


        #region ContabContaCollectionByIdContaMae - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabConta_ContabConta_FK1
        /// </summary>

        [XmlIgnore]
        public ContabContaCollection ContabContaCollectionByIdContaMae
        {
            get
            {
                if (this._ContabContaCollectionByIdContaMae == null)
                {
                    this._ContabContaCollectionByIdContaMae = new ContabContaCollection();
                    this._ContabContaCollectionByIdContaMae.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabContaCollectionByIdContaMae", this._ContabContaCollectionByIdContaMae);

                    if (this.IdConta != null)
                    {
                        this._ContabContaCollectionByIdContaMae.Query.Where(this._ContabContaCollectionByIdContaMae.Query.IdContaMae == this.IdConta);
                        this._ContabContaCollectionByIdContaMae.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabContaCollectionByIdContaMae.fks.Add(ContabContaMetadata.ColumnNames.IdContaMae, this.IdConta);
                    }
                }

                return this._ContabContaCollectionByIdContaMae;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabContaCollectionByIdContaMae != null)
                {
                    this.RemovePostSave("ContabContaCollectionByIdContaMae");
                    this._ContabContaCollectionByIdContaMae = null;

                }
            }
        }

        private ContabContaCollection _ContabContaCollectionByIdContaMae;
        #endregion


        #region UpToContabContaByIdContaMae - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - ContabConta_ContabConta_FK1
        /// </summary>

        [XmlIgnore]
        public ContabConta UpToContabContaByIdContaMae
        {
            get
            {
                if (this._UpToContabContaByIdContaMae == null
                    && IdContaMae != null)
                {
                    this._UpToContabContaByIdContaMae = new ContabConta();
                    this._UpToContabContaByIdContaMae.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToContabContaByIdContaMae", this._UpToContabContaByIdContaMae);
                    this._UpToContabContaByIdContaMae.Query.Where(this._UpToContabContaByIdContaMae.Query.IdConta == this.IdContaMae);
                    this._UpToContabContaByIdContaMae.Query.Load();
                }

                return this._UpToContabContaByIdContaMae;
            }

            set
            {
                this.RemovePreSave("UpToContabContaByIdContaMae");


                if (value == null)
                {
                    this.IdContaMae = null;
                    this._UpToContabContaByIdContaMae = null;
                }
                else
                {
                    this.IdContaMae = value.IdConta;
                    this._UpToContabContaByIdContaMae = value;
                    this.SetPreSave("UpToContabContaByIdContaMae", this._UpToContabContaByIdContaMae);
                }

            }
        }
        #endregion



        #region ContabLancamentoCollectionByIdContaCredito - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabLancamento_ContabConta_FK1
        /// </summary>

        [XmlIgnore]
        public ContabLancamentoCollection ContabLancamentoCollectionByIdContaCredito
        {
            get
            {
                if (this._ContabLancamentoCollectionByIdContaCredito == null)
                {
                    this._ContabLancamentoCollectionByIdContaCredito = new ContabLancamentoCollection();
                    this._ContabLancamentoCollectionByIdContaCredito.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabLancamentoCollectionByIdContaCredito", this._ContabLancamentoCollectionByIdContaCredito);

                    if (this.IdConta != null)
                    {
                        this._ContabLancamentoCollectionByIdContaCredito.Query.Where(this._ContabLancamentoCollectionByIdContaCredito.Query.IdContaCredito == this.IdConta);
                        this._ContabLancamentoCollectionByIdContaCredito.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabLancamentoCollectionByIdContaCredito.fks.Add(ContabLancamentoMetadata.ColumnNames.IdContaCredito, this.IdConta);
                    }
                }

                return this._ContabLancamentoCollectionByIdContaCredito;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabLancamentoCollectionByIdContaCredito != null)
                {
                    this.RemovePostSave("ContabLancamentoCollectionByIdContaCredito");
                    this._ContabLancamentoCollectionByIdContaCredito = null;

                }
            }
        }

        private ContabLancamentoCollection _ContabLancamentoCollectionByIdContaCredito;
        #endregion


        #region ContabLancamentoCollectionByIdContaDebito - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabLancamento_ContabConta_FK2
        /// </summary>

        [XmlIgnore]
        public ContabLancamentoCollection ContabLancamentoCollectionByIdContaDebito
        {
            get
            {
                if (this._ContabLancamentoCollectionByIdContaDebito == null)
                {
                    this._ContabLancamentoCollectionByIdContaDebito = new ContabLancamentoCollection();
                    this._ContabLancamentoCollectionByIdContaDebito.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabLancamentoCollectionByIdContaDebito", this._ContabLancamentoCollectionByIdContaDebito);

                    if (this.IdConta != null)
                    {
                        this._ContabLancamentoCollectionByIdContaDebito.Query.Where(this._ContabLancamentoCollectionByIdContaDebito.Query.IdContaDebito == this.IdConta);
                        this._ContabLancamentoCollectionByIdContaDebito.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabLancamentoCollectionByIdContaDebito.fks.Add(ContabLancamentoMetadata.ColumnNames.IdContaDebito, this.IdConta);
                    }
                }

                return this._ContabLancamentoCollectionByIdContaDebito;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabLancamentoCollectionByIdContaDebito != null)
                {
                    this.RemovePostSave("ContabLancamentoCollectionByIdContaDebito");
                    this._ContabLancamentoCollectionByIdContaDebito = null;

                }
            }
        }

        private ContabLancamentoCollection _ContabLancamentoCollectionByIdContaDebito;
        #endregion


        #region ContabRoteiroCollectionByIdContaCredito - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabRoteiro_ContabConta_FK1
        /// </summary>

        [XmlIgnore]
        public ContabRoteiroCollection ContabRoteiroCollectionByIdContaCredito
        {
            get
            {
                if (this._ContabRoteiroCollectionByIdContaCredito == null)
                {
                    this._ContabRoteiroCollectionByIdContaCredito = new ContabRoteiroCollection();
                    this._ContabRoteiroCollectionByIdContaCredito.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabRoteiroCollectionByIdContaCredito", this._ContabRoteiroCollectionByIdContaCredito);

                    if (this.IdConta != null)
                    {
                        this._ContabRoteiroCollectionByIdContaCredito.Query.Where(this._ContabRoteiroCollectionByIdContaCredito.Query.IdContaCredito == this.IdConta);
                        this._ContabRoteiroCollectionByIdContaCredito.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabRoteiroCollectionByIdContaCredito.fks.Add(ContabRoteiroMetadata.ColumnNames.IdContaCredito, this.IdConta);
                    }
                }

                return this._ContabRoteiroCollectionByIdContaCredito;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabRoteiroCollectionByIdContaCredito != null)
                {
                    this.RemovePostSave("ContabRoteiroCollectionByIdContaCredito");
                    this._ContabRoteiroCollectionByIdContaCredito = null;

                }
            }
        }

        private ContabRoteiroCollection _ContabRoteiroCollectionByIdContaCredito;
        #endregion


        #region ContabRoteiroCollectionByIdContaDebito - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabRoteiro_ContabConta_FK2
        /// </summary>

        [XmlIgnore]
        public ContabRoteiroCollection ContabRoteiroCollectionByIdContaDebito
        {
            get
            {
                if (this._ContabRoteiroCollectionByIdContaDebito == null)
                {
                    this._ContabRoteiroCollectionByIdContaDebito = new ContabRoteiroCollection();
                    this._ContabRoteiroCollectionByIdContaDebito.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabRoteiroCollectionByIdContaDebito", this._ContabRoteiroCollectionByIdContaDebito);

                    if (this.IdConta != null)
                    {
                        this._ContabRoteiroCollectionByIdContaDebito.Query.Where(this._ContabRoteiroCollectionByIdContaDebito.Query.IdContaDebito == this.IdConta);
                        this._ContabRoteiroCollectionByIdContaDebito.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabRoteiroCollectionByIdContaDebito.fks.Add(ContabRoteiroMetadata.ColumnNames.IdContaDebito, this.IdConta);
                    }
                }

                return this._ContabRoteiroCollectionByIdContaDebito;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabRoteiroCollectionByIdContaDebito != null)
                {
                    this.RemovePostSave("ContabRoteiroCollectionByIdContaDebito");
                    this._ContabRoteiroCollectionByIdContaDebito = null;

                }
            }
        }

        private ContabRoteiroCollection _ContabRoteiroCollectionByIdContaDebito;
        #endregion


        #region ContabSaldoCollectionByIdConta - Zero To Many
        /// <summary>
        /// Zero to Many
        /// Foreign Key Name - ContabSaldo_ContabConta_FK1
        /// </summary>

        [XmlIgnore]
        public ContabSaldoCollection ContabSaldoCollectionByIdConta
        {
            get
            {
                if (this._ContabSaldoCollectionByIdConta == null)
                {
                    this._ContabSaldoCollectionByIdConta = new ContabSaldoCollection();
                    this._ContabSaldoCollectionByIdConta.es.Connection.Name = this.es.Connection.Name;
                    this.SetPostSave("ContabSaldoCollectionByIdConta", this._ContabSaldoCollectionByIdConta);

                    if (this.IdConta != null)
                    {
                        this._ContabSaldoCollectionByIdConta.Query.Where(this._ContabSaldoCollectionByIdConta.Query.IdConta == this.IdConta);
                        this._ContabSaldoCollectionByIdConta.Query.Load();

                        // Auto-hookup Foreign Keys
                        this._ContabSaldoCollectionByIdConta.fks.Add(ContabSaldoMetadata.ColumnNames.IdConta, this.IdConta);
                    }
                }

                return this._ContabSaldoCollectionByIdConta;
            }

            set
            {
                if (value != null) throw new Exception("'value' Must be null");

                if (this._ContabSaldoCollectionByIdConta != null)
                {
                    this.RemovePostSave("ContabSaldoCollectionByIdConta");
                    this._ContabSaldoCollectionByIdConta = null;

                }
            }
        }

        private ContabSaldoCollection _ContabSaldoCollectionByIdConta;
        #endregion


        #region UpToContabPlanoByIdPlano - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - ContabConta_ContabPlano_FK1
        /// </summary>

        [XmlIgnore]
        public ContabPlano UpToContabPlanoByIdPlano
        {
            get
            {
                if (this._UpToContabPlanoByIdPlano == null
                    && IdPlano != null)
                {
                    this._UpToContabPlanoByIdPlano = new ContabPlano();
                    this._UpToContabPlanoByIdPlano.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
                    this._UpToContabPlanoByIdPlano.Query.Where(this._UpToContabPlanoByIdPlano.Query.IdPlano == this.IdPlano);
                    this._UpToContabPlanoByIdPlano.Query.Load();
                }

                return this._UpToContabPlanoByIdPlano;
            }

            set
            {
                this.RemovePreSave("UpToContabPlanoByIdPlano");


                if (value == null)
                {
                    this.IdPlano = null;
                    this._UpToContabPlanoByIdPlano = null;
                }
                else
                {
                    this.IdPlano = value.IdPlano;
                    this._UpToContabPlanoByIdPlano = value;
                    this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
                }

            }
        }
        #endregion



        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();

            props.Add(new esPropertyDescriptor(this, "ContabContaCollectionByIdContaMae", typeof(ContabContaCollection), new ContabConta()));
            props.Add(new esPropertyDescriptor(this, "ContabLancamentoCollectionByIdContaCredito", typeof(ContabLancamentoCollection), new ContabLancamento()));
            props.Add(new esPropertyDescriptor(this, "ContabLancamentoCollectionByIdContaDebito", typeof(ContabLancamentoCollection), new ContabLancamento()));
            props.Add(new esPropertyDescriptor(this, "ContabRoteiroCollectionByIdContaCredito", typeof(ContabRoteiroCollection), new ContabRoteiro()));
            props.Add(new esPropertyDescriptor(this, "ContabRoteiroCollectionByIdContaDebito", typeof(ContabRoteiroCollection), new ContabRoteiro()));
            props.Add(new esPropertyDescriptor(this, "ContabSaldoCollectionByIdConta", typeof(ContabSaldoCollection), new ContabSaldo()));

            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
            if (!this.es.IsDeleted && this._UpToContabContaByIdContaMae != null)
            {
                this.IdContaMae = this._UpToContabContaByIdContaMae.IdConta;
            }
            if (!this.es.IsDeleted && this._UpToContabPlanoByIdPlano != null)
            {
                this.IdPlano = this._UpToContabPlanoByIdPlano.IdPlano;
            }
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
            if (this._ContabContaCollectionByIdContaMae != null)
            {
                foreach (ContabConta obj in this._ContabContaCollectionByIdContaMae)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdContaMae = this.IdConta;
                    }
                }
            }
            if (this._ContabLancamentoCollectionByIdContaCredito != null)
            {
                foreach (ContabLancamento obj in this._ContabLancamentoCollectionByIdContaCredito)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdContaCredito = this.IdConta;
                    }
                }
            }
            if (this._ContabLancamentoCollectionByIdContaDebito != null)
            {
                foreach (ContabLancamento obj in this._ContabLancamentoCollectionByIdContaDebito)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdContaDebito = this.IdConta;
                    }
                }
            }
            if (this._ContabRoteiroCollectionByIdContaCredito != null)
            {
                foreach (ContabRoteiro obj in this._ContabRoteiroCollectionByIdContaCredito)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdContaCredito = this.IdConta;
                    }
                }
            }
            if (this._ContabRoteiroCollectionByIdContaDebito != null)
            {
                foreach (ContabRoteiro obj in this._ContabRoteiroCollectionByIdContaDebito)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdContaDebito = this.IdConta;
                    }
                }
            }
            if (this._ContabSaldoCollectionByIdConta != null)
            {
                foreach (ContabSaldo obj in this._ContabSaldoCollectionByIdConta)
                {
                    if (obj.es.IsAdded)
                    {
                        obj.IdConta = this.IdConta;
                    }
                }
            }
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esContabContaQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return ContabContaMetadata.Meta();
            }
        }


        public esQueryItem IdConta
        {
            get
            {
                return new esQueryItem(this, ContabContaMetadata.ColumnNames.IdConta, esSystemType.Int32);
            }
        }

        public esQueryItem IdPlano
        {
            get
            {
                return new esQueryItem(this, ContabContaMetadata.ColumnNames.IdPlano, esSystemType.Int32);
            }
        }

        public esQueryItem TipoConta
        {
            get
            {
                return new esQueryItem(this, ContabContaMetadata.ColumnNames.TipoConta, esSystemType.String);
            }
        }

        public esQueryItem Descricao
        {
            get
            {
                return new esQueryItem(this, ContabContaMetadata.ColumnNames.Descricao, esSystemType.String);
            }
        }

        public esQueryItem IdContaMae
        {
            get
            {
                return new esQueryItem(this, ContabContaMetadata.ColumnNames.IdContaMae, esSystemType.Int32);
            }
        }

        public esQueryItem Codigo
        {
            get
            {
                return new esQueryItem(this, ContabContaMetadata.ColumnNames.Codigo, esSystemType.String);
            }
        }

        public esQueryItem CodigoReduzida
        {
            get
            {
                return new esQueryItem(this, ContabContaMetadata.ColumnNames.CodigoReduzida, esSystemType.String);
            }
        }

        public esQueryItem FuncaoConta
        {
            get
            {
                return new esQueryItem(this, ContabContaMetadata.ColumnNames.FuncaoConta, esSystemType.Int32);
            }
        }

        public esQueryItem Digito
        {
            get
            {
                return new esQueryItem(this, ContabContaMetadata.ColumnNames.Digito, esSystemType.String);
            }
        }

    }



    [Serializable]
    [XmlType("ContabContaCollection")]
    public partial class ContabContaCollection : esContabContaCollection, IEnumerable<ContabConta>
    {
        public ContabContaCollection()
        {

        }

        public static implicit operator List<ContabConta>(ContabContaCollection coll)
        {
            List<ContabConta> list = new List<ContabConta>();

            foreach (ContabConta emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return ContabContaMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new ContabContaQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new ContabConta(row);
        }

        override protected esEntity CreateEntity()
        {
            return new ContabConta();
        }


        #endregion


        [BrowsableAttribute(false)]
        public ContabContaQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new ContabContaQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(ContabContaQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public ContabConta AddNew()
        {
            ContabConta entity = base.AddNewEntity() as ContabConta;

            return entity;
        }

        public ContabConta FindByPrimaryKey(System.Int32 idConta)
        {
            return base.FindByPrimaryKey(idConta) as ContabConta;
        }


        #region IEnumerable<ContabConta> Members

        IEnumerator<ContabConta> IEnumerable<ContabConta>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as ContabConta;
            }
        }

        #endregion

        private ContabContaQuery query;
    }


    /// <summary>
    /// Encapsulates the 'ContabConta' table
    /// </summary>

    [Serializable]
    public partial class ContabConta : esContabConta
    {
        public ContabConta()
        {

        }

        public ContabConta(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return ContabContaMetadata.Meta();
            }
        }



        override protected esContabContaQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new ContabContaQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public ContabContaQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new ContabContaQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(ContabContaQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private ContabContaQuery query;
    }



    [Serializable]
    public partial class ContabContaQuery : esContabContaQuery
    {
        public ContabContaQuery()
        {

        }

        public ContabContaQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class ContabContaMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected ContabContaMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(ContabContaMetadata.ColumnNames.IdConta, 0, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ContabContaMetadata.PropertyNames.IdConta;
            c.IsInPrimaryKey = true;
            c.IsAutoIncrement = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ContabContaMetadata.ColumnNames.IdPlano, 1, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ContabContaMetadata.PropertyNames.IdPlano;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(ContabContaMetadata.ColumnNames.TipoConta, 2, typeof(System.String), esSystemType.String);
            c.PropertyName = ContabContaMetadata.PropertyNames.TipoConta;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(ContabContaMetadata.ColumnNames.Descricao, 3, typeof(System.String), esSystemType.String);
            c.PropertyName = ContabContaMetadata.PropertyNames.Descricao;
            c.CharacterMaxLength = 2147483647;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(ContabContaMetadata.ColumnNames.IdContaMae, 4, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ContabContaMetadata.PropertyNames.IdContaMae;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ContabContaMetadata.ColumnNames.Codigo, 5, typeof(System.String), esSystemType.String);
            c.PropertyName = ContabContaMetadata.PropertyNames.Codigo;
            c.CharacterMaxLength = 20;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ContabContaMetadata.ColumnNames.CodigoReduzida, 6, typeof(System.String), esSystemType.String);
            c.PropertyName = ContabContaMetadata.PropertyNames.CodigoReduzida;
            c.CharacterMaxLength = 8;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(ContabContaMetadata.ColumnNames.FuncaoConta, 7, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = ContabContaMetadata.PropertyNames.FuncaoConta;
            c.NumericPrecision = 10;
            _columns.Add(c);

            c = new esColumnMetadata(ContabContaMetadata.ColumnNames.Digito, 8, typeof(System.String), esSystemType.String);
            c.PropertyName = ContabContaMetadata.PropertyNames.Digito;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.IsNullable = true;
            _columns.Add(c);

        }
        #endregion

        static public ContabContaMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string IdConta = "IdConta";
            public const string IdPlano = "IdPlano";
            public const string TipoConta = "TipoConta";
            public const string Descricao = "Descricao";
            public const string IdContaMae = "IdContaMae";
            public const string Codigo = "Codigo";
            public const string CodigoReduzida = "CodigoReduzida";
            public const string FuncaoConta = "FuncaoConta";
            public const string Digito = "Digito";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string IdConta = "IdConta";
            public const string IdPlano = "IdPlano";
            public const string TipoConta = "TipoConta";
            public const string Descricao = "Descricao";
            public const string IdContaMae = "IdContaMae";
            public const string Codigo = "Codigo";
            public const string CodigoReduzida = "CodigoReduzida";
            public const string FuncaoConta = "FuncaoConta";
            public const string Digito = "Digito";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(ContabContaMetadata))
            {
                if (ContabContaMetadata.mapDelegates == null)
                {
                    ContabContaMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (ContabContaMetadata.meta == null)
                {
                    ContabContaMetadata.meta = new ContabContaMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdPlano", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("TipoConta", new esTypeMap("char", "System.String"));
                meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("IdContaMae", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("Codigo", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("CodigoReduzida", new esTypeMap("varchar", "System.String"));
                meta.AddTypeMap("FuncaoConta", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("Digito", new esTypeMap("char", "System.String"));


                meta.Source = "ContabConta";
                meta.Destination = "ContabConta";

                meta.spInsert = "proc_ContabContaInsert";
                meta.spUpdate = "proc_ContabContaUpdate";
                meta.spDelete = "proc_ContabContaDelete";
                meta.spLoadAll = "proc_ContabContaLoadAll";
                meta.spLoadByPrimaryKey = "proc_ContabContaLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private ContabContaMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
