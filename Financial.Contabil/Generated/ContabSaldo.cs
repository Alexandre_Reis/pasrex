/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/07/2013 21:55:29
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.Contabil
{

	[Serializable]
	abstract public class esContabSaldoCollection : esEntityCollection
	{
		public esContabSaldoCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ContabSaldoCollection";
		}

		#region Query Logic
		protected void InitQuery(esContabSaldoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esContabSaldoQuery);
		}
		#endregion
		
		virtual public ContabSaldo DetachEntity(ContabSaldo entity)
		{
			return base.DetachEntity(entity) as ContabSaldo;
		}
		
		virtual public ContabSaldo AttachEntity(ContabSaldo entity)
		{
			return base.AttachEntity(entity) as ContabSaldo;
		}
		
		virtual public void Combine(ContabSaldoCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ContabSaldo this[int index]
		{
			get
			{
				return base[index] as ContabSaldo;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ContabSaldo);
		}
	}



	[Serializable]
	abstract public class esContabSaldo : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esContabSaldoQuery GetDynamicQuery()
		{
			return null;
		}

		public esContabSaldo()
		{

		}

		public esContabSaldo(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 idConta, System.DateTime data)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idConta, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idConta, data);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int32 idConta, System.DateTime data)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esContabSaldoQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.IdConta == idConta, query.Data == data);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 idConta, System.DateTime data)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idConta, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idConta, data);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 idConta, System.DateTime data)
		{
			esContabSaldoQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.IdConta == idConta, query.Data == data);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 idConta, System.DateTime data)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("IdConta",idConta);			parms.Add("Data",data);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "IdPlano": this.str.IdPlano = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "SaldoAnterior": this.str.SaldoAnterior = (string)value; break;							
						case "TotalDebito": this.str.TotalDebito = (string)value; break;							
						case "TotalCredito": this.str.TotalCredito = (string)value; break;							
						case "SaldoFinal": this.str.SaldoFinal = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "IdPlano":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPlano = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "SaldoAnterior":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.SaldoAnterior = (System.Decimal?)value;
							break;
						
						case "TotalDebito":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TotalDebito = (System.Decimal?)value;
							break;
						
						case "TotalCredito":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TotalCredito = (System.Decimal?)value;
							break;
						
						case "SaldoFinal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.SaldoFinal = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ContabSaldo.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ContabSaldoMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabSaldoMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabSaldo.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(ContabSaldoMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabSaldoMetadata.ColumnNames.IdConta, value))
				{
					this._UpToContabContaByIdConta = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabSaldo.IdPlano
		/// </summary>
		virtual public System.Int32? IdPlano
		{
			get
			{
				return base.GetSystemInt32(ContabSaldoMetadata.ColumnNames.IdPlano);
			}
			
			set
			{
				if(base.SetSystemInt32(ContabSaldoMetadata.ColumnNames.IdPlano, value))
				{
					this._UpToContabPlanoByIdPlano = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ContabSaldo.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(ContabSaldoMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(ContabSaldoMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabSaldo.SaldoAnterior
		/// </summary>
		virtual public System.Decimal? SaldoAnterior
		{
			get
			{
				return base.GetSystemDecimal(ContabSaldoMetadata.ColumnNames.SaldoAnterior);
			}
			
			set
			{
				base.SetSystemDecimal(ContabSaldoMetadata.ColumnNames.SaldoAnterior, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabSaldo.TotalDebito
		/// </summary>
		virtual public System.Decimal? TotalDebito
		{
			get
			{
				return base.GetSystemDecimal(ContabSaldoMetadata.ColumnNames.TotalDebito);
			}
			
			set
			{
				base.SetSystemDecimal(ContabSaldoMetadata.ColumnNames.TotalDebito, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabSaldo.TotalCredito
		/// </summary>
		virtual public System.Decimal? TotalCredito
		{
			get
			{
				return base.GetSystemDecimal(ContabSaldoMetadata.ColumnNames.TotalCredito);
			}
			
			set
			{
				base.SetSystemDecimal(ContabSaldoMetadata.ColumnNames.TotalCredito, value);
			}
		}
		
		/// <summary>
		/// Maps to ContabSaldo.SaldoFinal
		/// </summary>
		virtual public System.Decimal? SaldoFinal
		{
			get
			{
				return base.GetSystemDecimal(ContabSaldoMetadata.ColumnNames.SaldoFinal);
			}
			
			set
			{
				base.SetSystemDecimal(ContabSaldoMetadata.ColumnNames.SaldoFinal, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected ContabConta _UpToContabContaByIdConta;
		[CLSCompliant(false)]
		internal protected ContabPlano _UpToContabPlanoByIdPlano;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esContabSaldo entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPlano
			{
				get
				{
					System.Int32? data = entity.IdPlano;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPlano = null;
					else entity.IdPlano = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String SaldoAnterior
			{
				get
				{
					System.Decimal? data = entity.SaldoAnterior;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SaldoAnterior = null;
					else entity.SaldoAnterior = Convert.ToDecimal(value);
				}
			}
				
			public System.String TotalDebito
			{
				get
				{
					System.Decimal? data = entity.TotalDebito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TotalDebito = null;
					else entity.TotalDebito = Convert.ToDecimal(value);
				}
			}
				
			public System.String TotalCredito
			{
				get
				{
					System.Decimal? data = entity.TotalCredito;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TotalCredito = null;
					else entity.TotalCredito = Convert.ToDecimal(value);
				}
			}
				
			public System.String SaldoFinal
			{
				get
				{
					System.Decimal? data = entity.SaldoFinal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.SaldoFinal = null;
					else entity.SaldoFinal = Convert.ToDecimal(value);
				}
			}
			

			private esContabSaldo entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esContabSaldoQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esContabSaldo can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ContabSaldo : esContabSaldo
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabSaldo_ContabLancamento_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabContaByIdConta - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabSaldo_ContabConta_FK1
		/// </summary>

		[XmlIgnore]
		public ContabConta UpToContabContaByIdConta
		{
			get
			{
				if(this._UpToContabContaByIdConta == null
					&& IdConta != null					)
				{
					this._UpToContabContaByIdConta = new ContabConta();
					this._UpToContabContaByIdConta.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabContaByIdConta", this._UpToContabContaByIdConta);
					this._UpToContabContaByIdConta.Query.Where(this._UpToContabContaByIdConta.Query.IdConta == this.IdConta);
					this._UpToContabContaByIdConta.Query.Load();
				}

				return this._UpToContabContaByIdConta;
			}
			
			set
			{
				this.RemovePreSave("UpToContabContaByIdConta");
				

				if(value == null)
				{
					this.IdConta = null;
					this._UpToContabContaByIdConta = null;
				}
				else
				{
					this.IdConta = value.IdConta;
					this._UpToContabContaByIdConta = value;
					this.SetPreSave("UpToContabContaByIdConta", this._UpToContabContaByIdConta);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContabPlanoByIdPlano - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - ContabSaldo_ContabPlano_FK1
		/// </summary>

		[XmlIgnore]
		public ContabPlano UpToContabPlanoByIdPlano
		{
			get
			{
				if(this._UpToContabPlanoByIdPlano == null
					&& IdPlano != null					)
				{
					this._UpToContabPlanoByIdPlano = new ContabPlano();
					this._UpToContabPlanoByIdPlano.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
					this._UpToContabPlanoByIdPlano.Query.Where(this._UpToContabPlanoByIdPlano.Query.IdPlano == this.IdPlano);
					this._UpToContabPlanoByIdPlano.Query.Load();
				}

				return this._UpToContabPlanoByIdPlano;
			}
			
			set
			{
				this.RemovePreSave("UpToContabPlanoByIdPlano");
				

				if(value == null)
				{
					this.IdPlano = null;
					this._UpToContabPlanoByIdPlano = null;
				}
				else
				{
					this.IdPlano = value.IdPlano;
					this._UpToContabPlanoByIdPlano = value;
					this.SetPreSave("UpToContabPlanoByIdPlano", this._UpToContabPlanoByIdPlano);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToContabContaByIdConta != null)
			{
				this.IdConta = this._UpToContabContaByIdConta.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToContabPlanoByIdPlano != null)
			{
				this.IdPlano = this._UpToContabPlanoByIdPlano.IdPlano;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esContabSaldoQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ContabSaldoMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ContabSaldoMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, ContabSaldoMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPlano
		{
			get
			{
				return new esQueryItem(this, ContabSaldoMetadata.ColumnNames.IdPlano, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, ContabSaldoMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem SaldoAnterior
		{
			get
			{
				return new esQueryItem(this, ContabSaldoMetadata.ColumnNames.SaldoAnterior, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TotalDebito
		{
			get
			{
				return new esQueryItem(this, ContabSaldoMetadata.ColumnNames.TotalDebito, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TotalCredito
		{
			get
			{
				return new esQueryItem(this, ContabSaldoMetadata.ColumnNames.TotalCredito, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem SaldoFinal
		{
			get
			{
				return new esQueryItem(this, ContabSaldoMetadata.ColumnNames.SaldoFinal, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ContabSaldoCollection")]
	public partial class ContabSaldoCollection : esContabSaldoCollection, IEnumerable<ContabSaldo>
	{
		public ContabSaldoCollection()
		{

		}
		
		public static implicit operator List<ContabSaldo>(ContabSaldoCollection coll)
		{
			List<ContabSaldo> list = new List<ContabSaldo>();
			
			foreach (ContabSaldo emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ContabSaldoMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContabSaldoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ContabSaldo(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ContabSaldo();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ContabSaldoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContabSaldoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ContabSaldoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ContabSaldo AddNew()
		{
			ContabSaldo entity = base.AddNewEntity() as ContabSaldo;
			
			return entity;
		}

		public ContabSaldo FindByPrimaryKey(System.Int32 idCliente, System.Int32 idConta, System.DateTime data)
		{
			return base.FindByPrimaryKey(idCliente, idConta, data) as ContabSaldo;
		}


		#region IEnumerable<ContabSaldo> Members

		IEnumerator<ContabSaldo> IEnumerable<ContabSaldo>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ContabSaldo;
			}
		}

		#endregion
		
		private ContabSaldoQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ContabSaldo' table
	/// </summary>

	[Serializable]
	public partial class ContabSaldo : esContabSaldo
	{
		public ContabSaldo()
		{

		}
	
		public ContabSaldo(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ContabSaldoMetadata.Meta();
			}
		}
		
		
		
		override protected esContabSaldoQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ContabSaldoQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ContabSaldoQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ContabSaldoQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ContabSaldoQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ContabSaldoQuery query;
	}



	[Serializable]
	public partial class ContabSaldoQuery : esContabSaldoQuery
	{
		public ContabSaldoQuery()
		{

		}		
		
		public ContabSaldoQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ContabSaldoMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ContabSaldoMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ContabSaldoMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabSaldoMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabSaldoMetadata.ColumnNames.IdConta, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabSaldoMetadata.PropertyNames.IdConta;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabSaldoMetadata.ColumnNames.IdPlano, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ContabSaldoMetadata.PropertyNames.IdPlano;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabSaldoMetadata.ColumnNames.Data, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = ContabSaldoMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabSaldoMetadata.ColumnNames.SaldoAnterior, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ContabSaldoMetadata.PropertyNames.SaldoAnterior;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabSaldoMetadata.ColumnNames.TotalDebito, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ContabSaldoMetadata.PropertyNames.TotalDebito;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabSaldoMetadata.ColumnNames.TotalCredito, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ContabSaldoMetadata.PropertyNames.TotalCredito;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ContabSaldoMetadata.ColumnNames.SaldoFinal, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = ContabSaldoMetadata.PropertyNames.SaldoFinal;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ContabSaldoMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdConta = "IdConta";
			 public const string IdPlano = "IdPlano";
			 public const string Data = "Data";
			 public const string SaldoAnterior = "SaldoAnterior";
			 public const string TotalDebito = "TotalDebito";
			 public const string TotalCredito = "TotalCredito";
			 public const string SaldoFinal = "SaldoFinal";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdConta = "IdConta";
			 public const string IdPlano = "IdPlano";
			 public const string Data = "Data";
			 public const string SaldoAnterior = "SaldoAnterior";
			 public const string TotalDebito = "TotalDebito";
			 public const string TotalCredito = "TotalCredito";
			 public const string SaldoFinal = "SaldoFinal";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ContabSaldoMetadata))
			{
				if(ContabSaldoMetadata.mapDelegates == null)
				{
					ContabSaldoMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ContabSaldoMetadata.meta == null)
				{
					ContabSaldoMetadata.meta = new ContabSaldoMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPlano", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("SaldoAnterior", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TotalDebito", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TotalCredito", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("SaldoFinal", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "ContabSaldo";
				meta.Destination = "ContabSaldo";
				
				meta.spInsert = "proc_ContabSaldoInsert";				
				meta.spUpdate = "proc_ContabSaldoUpdate";		
				meta.spDelete = "proc_ContabSaldoDelete";
				meta.spLoadAll = "proc_ContabSaldoLoadAll";
				meta.spLoadByPrimaryKey = "proc_ContabSaldoLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ContabSaldoMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
